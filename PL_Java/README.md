## PL/Java - Building and installing in a docker container

Starting with Redhat 8 / Postgresql 14 docker container in `docker-playground` repo.

- If there are errors creating a container, try adding "RUN yum update -y" after yum installs.

- From the host machine, start a command line
```
docker ps
docker exec -u root -it bc2013 /bin/bash
```

- Once inside a container...

- Install tools for SDKMAN

`yum install zip unzip curl`

- Install SDKMAN for Java - https://sdkman.io/install

```
curl -s "https://get.sdkman.io" | bash
source "$HOME/.sdkman/bin/sdkman-init.sh"
sdk version
```

- Install JDK 1.8. As an alternative, JRE 8 can be installed with `yum install java-1.8.0-openjdk`

```
sdk list java
sdk install java 8.0.302-open
java -version
```

- Install other tools

```
yum install maven gcc-c++ openssl-devel wget
```

- Check if pg_config is configured by calling `pg_config`. If not look for pg_config and update path
    - If command is not found, update path - `export PATH="/usr/pgsql-14/bin:$PATH"`

- Get PL/Java source code and build - https://tada.github.io/pljava/build/build.html

```
cd ~ 
wget https://github.com/tada/pljava/archive/refs/tags/V1_5_8.tar.gz
tar -xvf V1_5_8.tar.gz
cd pljava-1_5_8/
mvn clean install
```

- If Maven error occurs above? WARNING: Error injecting: org.apache.maven.plugin.surefire.SurefirePlugin
    - Update Maven to the latest.
    ```
    wget https://dlcdn.apache.org/maven/maven-3/3.8.5/binaries/apache-maven-3.8.5-bin.tar.gz
    tar -xvf apache-maven-3.8.5-bin.tar.gz
    export PATH=/home/postgres/apache-maven-3.8.5/bin:$PATH
    ```

- Install PL/Java

```
java -jar pljava-packaging/target/pljava-pg14.2-amd64-Linux-gpp.jar

[root@bc2013bc9f61 pljava-1_5_8]# java -jar pljava-packaging/target/pljava-pg14.2-amd64-Linux-gpp.jar
/usr/pgsql-14/lib/libpljava-so-1.5.8.so as bytes
/usr/pgsql-14/share/pljava/pljava-1.5.8.jar as bytes
/usr/pgsql-14/share/pljava/pljava-api-1.5.8.jar as bytes
/usr/pgsql-14/share/pljava/pljava-examples-1.5.8.jar as bytes
/usr/pgsql-14/share/extension/pljava.control as lines (ASCII)
/usr/pgsql-14/share/pljava/pljava--1.5.8.sql as lines (UTF8)
/usr/pgsql-14/share/pljava/pljava--unpackaged--1.5.8.sql as lines (UTF8)
/usr/pgsql-14/share/pljava/pljava--1.5.7--1.5.8.sql as lines (UTF8)
/usr/pgsql-14/share/pljava/pljava--1.5.6--1.5.8.sql as lines (UTF8)
/usr/pgsql-14/share/pljava/pljava--1.5.5--1.5.8.sql as lines (UTF8)
/usr/pgsql-14/share/pljava/pljava--1.5.4--1.5.8.sql as lines (UTF8)
/usr/pgsql-14/share/pljava/pljava--1.5.3--1.5.8.sql as lines (UTF8)
/usr/pgsql-14/share/pljava/pljava--1.5.2--1.5.8.sql as lines (UTF8)
/usr/pgsql-14/share/pljava/pljava--1.5.1--1.5.8.sql as lines (UTF8)
/usr/pgsql-14/share/pljava/pljava--1.5.1-BETA3--1.5.8.sql as lines (UTF8)
/usr/pgsql-14/share/pljava/pljava--1.5.1-BETA2--1.5.8.sql as lines (UTF8)
/usr/pgsql-14/share/pljava/pljava--1.5.1-BETA1--1.5.8.sql as lines (UTF8)
/usr/pgsql-14/share/pljava/pljava--1.5.0--1.5.8.sql as lines (UTF8)
/usr/pgsql-14/share/pljava/pljava--1.5.0-BETA3--1.5.8.sql as lines (UTF8)
/usr/pgsql-14/share/pljava/pljava--1.5.0-BETA2--1.5.8.sql as lines (UTF8)
/usr/pgsql-14/share/pljava/pljava--1.5.0-BETA1--1.5.8.sql as lines (UTF8)
/usr/pgsql-14/share/pljava/pljava--unpackaged.sql as lines (UTF8)
```

- Install PL/java in Postgres. Find libjvm.so in your JDK path
    - Use your superuser account in place of aqms_superuser
    - may need to exit and start new session when "ERROR:  cannot use PL/Java before successfully completing its setup" 

    ```
    psql -U aqms_superuser -d myarch
    alter database myarch SET pljava.libjvm_location TO '/home/postgres/.sdkman/candidates/java/8.0.302-open/jre/lib/amd64/server/libjvm.so';
    ```

- Exit current session and start new session

```
psql -U aqms_superuser -d myarch
create extension pljava;
```

- Add Jiggle.jar and install stored procedures

```
select sqlj.install_jar('file:/home/postgres/jiggle_name.jar','jigglejar',true);
```

- Set classpath for schemas

```
select sqlj.set_classpath('assocamp', 'jigglejar');
select sqlj.set_classpath('db_cleanup', 'jigglejar');
select sqlj.set_classpath('eventpriority', 'jigglejar');
select sqlj.set_classpath('file_util', 'jigglejar');
select sqlj.set_classpath('formats', 'jigglejar');
select sqlj.set_classpath('util', 'jigglejar');
select sqlj.set_classpath('wavearc', 'jigglejar');
select sqlj.set_classpath('wheres', 'jigglejar');
```

- Test installation with where_from_type

```
myarch=# select wheres.where_from_type(35.7, -117.7, 1., 1, 'bigtown');
                        where_from_type
---------------------------------------------------------------
    9.1 km (   5.6 mi) NNW ( 346. azimuth) from Ridgecrest, CA
(1 row)
```

- Test getcubeemailformat

```
myarch=# select formats.getcubeemailformat(71397551);
                                   getcubeemailformat
----------------------------------------------------------------------------------------
              >>> UPDATE OF PREVIOUSLY REPORTED EVENT <<<                              +
 Southern California Seismic Network (SCSN) operated by Caltech and USGS               +
                                                                                       +
 Version 2: This report supersedes any earlier reports of this event.                  +
 This is a computer generated solution and has not yet been reviewed by a seismologist.+
                                                                                       +
  Magnitude   :   0.76 Ml (micro earthquake)                                           +
  Time        :   23 Jan 2021   04:41:41 PM, PST                                       +
              :   24 Jan 2021   00:41:41 UTC                                           +
  Coordinates :   33 deg. 30.17 min. N, 116 deg. 47.25 min. W                          +
              :   33.5028 N, 116.7875 W                                                +
  Depth       :     3.0 miles (  4.8 km)                                               +
  Quality     :   Excellent                                                            +
  Location    :     6 mi. (  9 km) NE  of Aguanga, CA                                  +
              :    26 mi. ( 42 km) SW  of Palm Springs, CA                             +
              :    17 mi. ( 28 km) ENE of OLY QRY                                      +
  Event ID    :   CI 71397551                                                          +
                                                                                       +
 More Information about this event and other earthquakes is available at:              +
 http://www.scsn.org                                                                   +
                                                                                       +
 ADDITIONAL EARTHQUAKE PARAMETERS                                                      +
 ________________________________                                                      +
 Stations used                : 13                                                     +
 Phases used                  : 26                                                     +
 S phases used                : 13                                                     +
 Rms misfit                   : 0.21 seconds                                           +
 Horizontal location error    : 0.4 km                                                 +
 Vertical location error      : 1.6 km                                                 +
 Maximum azimuthal gap        : 62 degrees                                             +
 Distance to nearest station  : 1.6 km SE of 5241                                      +
 Event magnitudes             : Me=2.03 Ml=0.76                                        +
                                                                                       +

(1 row)
```

## Other Database Updates

- Create schemas in docker-playground\postgresXX\DBpg\create\users\create_users.template

```
create schema if not exists db_cleanup authorization code;
grant usage on schema db_cleanup to trinetdb_read,trinetdb_execute;

create schema if not exists eventpriority authorization code;
grant usage on schema eventpriority to trinetdb_read,trinetdb_execute;

create schema if not exists file_util authorization code;
grant usage on schema file_util to trinetdb_read,trinetdb_execute;

create schema if not exists formats authorization code;
grant usage on schema formats to trinetdb_read,trinetdb_execute;

create schema if not exists wavearc authorization code;
grant usage on schema wavearc to trinetdb_read,trinetdb_execute;

```

- Permission required on "sqlj" schema created by "create extension pljava"

```
grant usage on schema sqlj to trinetdb_read,trinetdb_execute;
```

- If need to replace Jiggle jar file
```
myarch=# select sqlj.replace_jar('file:/home/postgres/jiggle_name.jar', 'jigglejar',true);
 replace_jar
-------------

(1 row)
```

## Java Functions Requiring Manual Creation

Internal Java functions cannot be annotated, but can be created manually.

```
-- Used by both db_cleanup and file_util
CREATE OR REPLACE FUNCTION db_cleanup.delete_file(
        path pg_catalog.varchar)
        RETURNS integer
        LANGUAGE java VOLATILE
        AS 'int=org.trinet.util.FileUtil.delete(java.lang.String)';

-- Java internal getProperty function
CREATE OR REPLACE FUNCTION file_util.get_java_sys_prop(VARCHAR)
RETURNS VARCHAR
AS 'java.lang.System.getProperty'
LANGUAGE java;
```

## List of Oracle Package Names/Functions Exported from Java to Postgres

- assocamp
    -     FUNCTION AssocAmps RETURN NUMBER AS LANGUAGE JAVA
              NAME 'org.trinet.storedprocs.association.AmpAssocEngine.associateAmps() return int';
      
    -     FUNCTION getAssocEvent(p_net      VARCHAR2,
                                 p_sta      VARCHAR2,
                                 p_seedchan VARCHAR2,
                                 p_loc      VARCHAR2,
                                 p_datetime NUMBER,
                                 p_ampvalue    NUMBER) RETURN NUMBER AS LANGUAGE JAVA
              NAME 'org.trinet.storedprocs.association.AmpAssoc.getAssocEventId
                    (java.lang.String,java.lang.String,java.lang.String,java.lang.String,double,double) return long';
    
    -     FUNCTION getAssocEvent(p_ampid NUMBER) RETURN NUMBER AS LANGUAGE JAVA
            NAME 'org.trinet.storedprocs.association.AmpAssoc.getAssocEventId(long) return long';
       
- db_cleanup

    -     FUNCTION delete_file(p_fileName VARCHAR2) RETURN NUMBER IS
            LANGUAGE JAVA NAME 'org.trinet.util.FileUtil.delete(java.lang.String) return int';

- eventpriority

    -     FUNCTION getPriority(p_mag NUMBER, p_origintime NUMBER, p_quality NUMBER) RETURN NUMBER AS
           LANGUAGE JAVA NAME
           'org.trinet.storedprocs.eventpriority.EventPriority.getPriority(double, double, double) return double';

    -     FUNCTION getPriority(p_evid NUMBER) RETURN NUMBER AS
           LANGUAGE JAVA NAME 'org.trinet.storedprocs.eventpriority.EventPriority.getPriority(long) return double';
      
- file_util
  
    -     FUNCTION delete_file(p_name VARCHAR2) RETURN NUMBER AS
           LANGUAGE JAVA NAME 'org.trinet.util.FileUtil.delete(java.lang.String) return int';

    -     FUNCTION create_file(p_name VARCHAR2) RETURN NUMBER AS
            LANGUAGE JAVA NAME 'org.trinet.util.FileUtil.create(java.lang.String) return int';

    -     FUNCTION user_home RETURN VARCHAR2 AS
            LANGUAGE JAVA NAME 'org.trinet.util.FileUtil.getUserHomeDir() return java.lang.String';

    -     FUNCTION user_dir RETURN VARCHAR2 AS
            LANGUAGE JAVA NAME 'org.trinet.util.FileUtil.getUserDir() return java.lang.String';

- formats - all 20 functions

- util

    -     FUNCTION epochToString(epochsecs NUMBER) RETURN VARCHAR2
            AS LANGUAGE JAVA
            NAME 'org.trinet.util.EpochTime.epochToString(double) return String';

    -     FUNCTION epochToString(epochsecs NUMBER, pattern VARCHAR2) RETURN VARCHAR2
             AS LANGUAGE JAVA
             NAME 'org.trinet.util.EpochTime.epochToString(double,java.lang.String) return String';

    -     FUNCTION epochToString(epochsecs NUMBER, pattern VARCHAR2, timezone VARCHAR2) RETURN VARCHAR2
           AS LANGUAGE JAVA
           NAME 'org.trinet.util.EpochTime.epochToString
           (double,java.lang.String,java.lang.String) return String';
        
    -     FUNCTION stringToEpoch(timeString VARCHAR2, pattern VARCHAR2) RETURN NUMBER
           AS LANGUAGE JAVA
           NAME 'org.trinet.util.EpochTime.stringToEpoch
           (java.lang.String,java.lang.String) return double';

- wave

    - isJavaUTC - Exists in Postgres. Skipped.
    - fill_wf_blob - Cannot convert java.sql.blob in fillWaveformBLOB. Skipped. 
        However, it appears they are used in get_waveform_blob which have been ported and using a PostgreSQL extension written in C.
    
- wavearc

    -     FUNCTION GenerateRequests(p_evid NUMBER) RETURN NUMBER
              AS LANGUAGE JAVA NAME
              'org.trinet.storedprocs.waveformrequest.RequestGenerator.generateRequests(long) return int';

    -     FUNCTION GenerateRequestCount(p_evid NUMBER) RETURN NUMBER
              AS LANGUAGE JAVA NAME
              'org.trinet.storedprocs.waveformrequest.RequestGenerator.generateRequestCount(long) return int';
    
- wheres

    -     PROCEDURE wf_du_km AS LANGUAGE JAVA
            NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.setDistanceUnitsKm()';
         
    -     PROCEDURE wf_du_mi AS LANGUAGE JAVA
            NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.setDistanceUnitsMiles()';
    
    -    compass_pt - Exists in Postgres. Skipped.

    -    az_to - Exists in Postgres. Skipped.     
      
    -     FUNCTION elev_to ( p_lat1 IN NUMBER,
          p_lon1 IN NUMBER,
          p_z1   IN NUMBER,
          p_lat2 IN NUMBER,
          p_lon2 IN NUMBER,
          p_z2   IN NUMBER ) RETURN NUMBER AS LANGUAGE JAVA
          NAME 'org.trinet.util.gazetteer.GeoidalConvert.elevationDegreesTo
          (double, double, double, double, double, double) return double';
    
    -    separation_km - both overloaded functions exists in Postgres. Skipped.
    
    -     FUNCTION separation_mi ( p_lat1 IN NUMBER,
                          p_lon1 IN NUMBER,
                          p_lat2 IN NUMBER,
                          p_lon2 IN NUMBER ) RETURN NUMBER AS LANGUAGE JAVA 
              NAME 'org.trinet.util.gazetteer.GeoidalConvert.horizontalDistanceMilesBetween
              (double, double, double, double) return double';

    -      FUNCTION ddeg ( p_deg IN NUMBER, p_min IN NUMBER ) RETURN NUMBER AS LANGUAGE JAVA
                NAME 'org.trinet.util.gazetteer.GeoidalConvert.toDecimalDegrees(int, double) return double';
      
    -       FUNCTION ddeg ( p_deg IN NUMBER, p_min IN NUMBER, p_sec IN NUMBER ) RETURN NUMBER AS LANGUAGE JAVA
                NAME 'org.trinet.util.gazetteer.GeoidalConvert.toDecimalDegrees(int, double, double) return double';

    -      FUNCTION km_to_ft ( p_km IN NUMBER ) RETURN NUMBER AS LANGUAGE JAVA 
              NAME 'org.trinet.util.gazetteer.GeoidalConvert.kmToFeet(double) return double';

    -       FUNCTION ft_to_km ( p_feet IN NUMBER ) RETURN NUMBER AS LANGUAGE JAVA
              NAME 'org.trinet.util.gazetteer.GeoidalConvert.feetToKm(double) return double';

    -       FUNCTION km_to_mi ( p_km IN NUMBER ) RETURN NUMBER AS LANGUAGE JAVA
              NAME 'org.trinet.util.gazetteer.GeoidalConvert.kmToMiles(double) return double';

    -       FUNCTION mi_to_km ( p_miles IN NUMBER ) RETURN NUMBER AS LANGUAGE JAVA
              NAME 'org.trinet.util.gazetteer.GeoidalConvert.milesToKm(double) return double';

    -       FUNCTION km_to_fault(p_lat NUMBER, p_lon NUMBER, p_name IN VARCHAR2) RETURN NUMBER AS LANGUAGE JAVA
            NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.kmToFault(double, double, java.lang.String) return double';

    -       PROCEDURE town ( p_lat    IN NUMBER, 
                  p_lon    IN NUMBER,
                  p_z      IN NUMBER,
                  p_dist  OUT NUMBER,
                  p_az    OUT NUMBER,
                  p_elev  OUT NUMBER,
                  p_place OUT VARCHAR2 ) AS LANGUAGE JAVA 
                  NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestTown
                  (double, double, double, double [], double [], double [], java.lang.String [])';
        - PostgreSQL example - `select * from wheres.town(32.0, -116.0, 0.0) as (dist numeric,  az numeric, elev numeric, place text);`
    
    -       PROCEDURE bigtown ( p_lat    IN NUMBER, 
                     p_lon    IN NUMBER,
                     p_z      IN NUMBER,
                     p_dist  OUT NUMBER,
                     p_az    OUT NUMBER,
                     p_elev  OUT NUMBER,
                     p_place OUT VARCHAR2 ) AS LANGUAGE JAVA 
                  NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestBigTown
                  (double, double, double, double [], double [], double [], java.lang.String [])';
        - PostgreSQL example - `select * from wheres.bigtown(32.0, -116.0, 0.0) as (dist numeric,  az numeric, elev numeric, place text);`

    -        PROCEDURE eqpoi ( p_lat    IN NUMBER, 
                   p_lon    IN NUMBER,
                   p_z      IN NUMBER,
                   p_dist  OUT NUMBER,
                   p_az    OUT NUMBER,
                   p_elev  OUT NUMBER,
                   p_place OUT VARCHAR2 ) AS LANGUAGE JAVA 
                  NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestEqPOI
                  (double, double, double, double [], double [], double [], java.lang.String [])';
        - PostgreSQL example - `select * from wheres.eqpoi(32.0, -116.0, 0.0) as (dist numeric,  az numeric, elev numeric, place text);`

    -        PROCEDURE quarry (   p_lat    IN NUMBER, 
                      p_lon    IN NUMBER,
                      p_z      IN NUMBER,
                      p_dist  OUT NUMBER,
                      p_az    OUT NUMBER,
                      p_elev  OUT NUMBER,
                      p_place OUT VARCHAR2 ) AS LANGUAGE JAVA 
                  NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestQuarry
                  (double, double, double, double [], double [], double [], java.lang.String [])';
        - PostgreSQL example - `select * from wheres.quarry(32.0, -116.0, 0.0) as (dist numeric,  az numeric, elev numeric, place text);`

    -        PROCEDURE quake ( p_lat    IN NUMBER, 
                   p_lon    IN NUMBER,
                   p_z      IN NUMBER,
                   p_dist  OUT NUMBER,
                   p_az    OUT NUMBER,
                   p_elev  OUT NUMBER,
                   p_place OUT VARCHAR2 ) AS LANGUAGE JAVA 
              NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestQuake
              (double, double, double, double [], double [], double [], java.lang.String [])';
        - PostgreSQL example - `select * from wheres.quake(32.0, -116.0, 0.0) as (dist numeric,  az numeric, elev numeric, place text);`

    -        PROCEDURE station ( p_lat    IN NUMBER, 
                     p_lon    IN NUMBER,
                     p_z      IN NUMBER,
                     p_dist  OUT NUMBER,
                     p_az    OUT NUMBER,
                     p_elev  OUT NUMBER,
                     p_place OUT VARCHAR2 ) AS LANGUAGE JAVA 
                  NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestStation
                  (double, double, double, double [], double [], double [], java.lang.String [])';
        - PostgreSQL example - `select * from wheres.station(32.0, -116.0, 0.0) as (dist numeric,  az numeric, elev numeric, place text);`

    -        PROCEDURE fault ( p_lat    IN NUMBER, 
                   p_lon    IN NUMBER,
                   p_z      IN NUMBER,
                   p_dist  OUT NUMBER,
                   p_az    OUT NUMBER,
                   p_elev  OUT NUMBER,
                   p_place OUT VARCHAR2 ) AS LANGUAGE JAVA 
              NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestFault
              (double, double, double, double [], double [], double [], java.lang.String [])';
        - PostgreSQL example - `select * from wheres.fault(32.0, -116.0, 0.0) as (dist numeric,  az numeric, elev numeric, place text);`

    -         PROCEDURE volcano ( p_lat    IN NUMBER, 
                      p_lon    IN NUMBER,
                      p_z      IN NUMBER,
                      p_dist  OUT NUMBER,
                      p_az    OUT NUMBER,
                      p_elev  OUT NUMBER,
                      p_place OUT VARCHAR2 ) AS LANGUAGE JAVA 
              NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.getClosestVolcano
              (double, double, double, double [], double [], double [], java.lang.String [])';
      
        - PostgreSQL example - `select * from wheres.volcano(32.0, -116.0, 0.0) as (dist numeric,  az numeric, elev numeric, place text);`

    -   point - Exists in Postgres. Skipped. 
    
    -       FUNCTION locale ( p_lat IN NUMBER, p_lon IN NUMBER ) RETURN VARCHAR2 AS LANGUAGE JAVA
                NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.where(double, double) return java.lang.String';
       
    -       FUNCTION locale ( p_lat  IN NUMBER,
               p_lon  IN NUMBER,
               p_z    IN NUMBER,
               p_num  IN NUMBER ) RETURN VARCHAR2 AS LANGUAGE JAVA
               NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.where(double, double, double, int) return java.lang.String';
      
    -       FUNCTION locale ( p_lat    IN NUMBER,
               p_latmin IN NUMBER,
               p_lon    IN NUMBER,
               p_lonmin IN NUMBER,
               p_z      IN NUMBER,
               p_num    IN NUMBER ) RETURN VARCHAR2 AS LANGUAGE JAVA
               NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.where
               (int, double, int, double, double, int) return java.lang.String';

    -       FUNCTION where_from_type ( p_lat    IN NUMBER,
                p_lon    IN NUMBER,
                p_z      IN NUMBER,
                p_num    IN NUMBER,
                p_type   IN VARCHAR2 ) RETURN VARCHAR2 AS LANGUAGE JAVA
                NAME 'org.trinet.util.gazetteer.TN.WheresFromServer.whereType
                (double, double, double, int, java.lang.String) return java.lang.String';
    
