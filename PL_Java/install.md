# PL/Java for PostgreSQL Installation Guide

AQMS database utilizes several Java functions as stored procedures. 
This document describes how to install PL/Java and Jiggle.jar that are required to use Java functions from PostgreSQL database.  

## Requirements

- JRE/JDK version 8 or later is required. 
- PL/Java 1.5.8 is used. There is a newer release, but 1.5.8 is the latest version that supports Java 8. 
- Custom build of PL/Java for an operating system and PostgreSQL may be required if a prebuilt package is not available.

## Install PL/Java

To install PL/Java, find the prebuilt package for your operating system and PostgreSQL version from `aqms-jiggle/PL_Java`.
For example, PostgreSQL 14 and Redhat 8 is available in `aqms-jiggle/PL_Java/Redhat 8/pljava-pg14.2-amd64-Linux-gpp.jar`. 
The `pg14.2` in the filename is the version of PostgreSQL.

If a prebuilt package is not available, create a ticket in aqms-jiggle project with details about your operating system and PostgreSQL version.

1. Find the prebuilt PL/Java package for your database server.
1. Download a copy of PL/Java JAR to your database server.
1. Download the latest `Jiggle.jar` for your Java version from `https://gitlab.com/aqms-swg/aqms-jiggle/-/releases` to your database server. Note the full path to the file.
1. Confirm JRE or JDK is installed. Execute `java -version` to verify.
1. Execute the command `pg_config` from the command line. If this does not work, update your `PATH` environment variable.
   For example, assuming that the path to `pg_config` is `/usr/pgsql-14/bin`, the path may be updated with 
   `export PATH="usr/pgsql-14/bin:$PATH"`.
1. Installing PL/Java may require elevated access that can write to the PostgreSQL installation directory. 
With a proper access, find your prebuilt PL/Java package file, and execute the command:
    ```
    java -jar /path-to-your-file/your-prebuild-pljava.jar
    ```
1. This should install PL/Java files in PostgreSQL directories.

## Install PL/Java PostgreSQL Extension

1. Find the location of `libjvm.so` file. This file is part of JRE/JDK installation. Note the full path to the file.
1. Start `psql` as superuser that can create extensions and alter database. For example, `psql -U your-user -d your-db`.
1. Once in `psql`:
    - Update path to `libjvm.so` file. For example,
   
    ```
    alter database myarch SET pljava.libjvm_location TO '/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.322.b06-1.el7_9.x86_64/jre/lib/amd64/server/libjvm.so';
    ```
   
    - Exit the current session and start a new `psql` session.
      
    - Create any missing schemas and grant permissions:
    
    ```
      create schema if not exists db_cleanup authorization code;
      grant usage on schema db_cleanup to trinetdb_read,trinetdb_execute;
   
      create schema if not exists eventpriority authorization code;
      grant usage on schema eventpriority to trinetdb_read,trinetdb_execute;
      
      create schema if not exists file_util authorization code;
      grant usage on schema file_util to trinetdb_read,trinetdb_execute;
      
      create schema if not exists formats authorization code;
      grant usage on schema formats to trinetdb_read,trinetdb_execute;
      
      create schema if not exists wavearc authorization code;
      grant usage on schema wavearc to trinetdb_read,trinetdb_execute;
    ```
   
    - Create PL/Java extension
   
    ```
    create extension pljava;
    ```
   
    - Find the path to the `jiggle.jar` file downloaded earlier and install `jiggle.jar`:   
    ```
    select sqlj.install_jar('file:/your-path/your-jiggle.jar','jigglejar',true);
    ```
    
    - Set schema path for each schema affected:
    ```
      select sqlj.set_classpath('assocamp', 'jigglejar');
      select sqlj.set_classpath('db_cleanup', 'jigglejar');
      select sqlj.set_classpath('eventpriority', 'jigglejar');
      select sqlj.set_classpath('file_util', 'jigglejar');
      select sqlj.set_classpath('formats', 'jigglejar');
      select sqlj.set_classpath('util', 'jigglejar');
      select sqlj.set_classpath('wavearc', 'jigglejar');
      select sqlj.set_classpath('wheres', 'jigglejar');
    ```
    
    - Grant permission to `sqlj` schema created by PL/Java. This allows other non-superusers to access the stored procedures. 
    ```
    grant usage on schema sqlj to trinetdb_read,trinetdb_execute;
    ```
   
## Updating Jiggle Java Functions

When an update to Jiggle contains changes to Java database functions, installed `jiggle.jar` can be updated by:
```
select sqlj.replace_jar('file:/home/postgres/jiggle_name.jar', 'jigglejar',true);
```

## Test Installation

To test the installation, start `psql` and try the following command:

```
select wheres.where_from_type(35.7, -117.7, 1., 1, 'bigtown');
```

## Known Issues

1. `postgres` superuser may not be able to query using the Java exported functions when used with SELinux.
More information is available at https://tada.github.io/pljava/install/selinux.html, but the workaround has not been tried. 
The following error was noticed when calling `wheres.where_from_type`.  

```markdown
INFO:  19 Apr 22 11:38:23 org.postgresql.pljava.sqlj.Loader Failed to load class
java.sql.SQLException: An attempt was made to call a PostgreSQL backend function after an elog(ERROR) had been issued
at org.postgresql.pljava.internal.Oid._forSqlType(Native Method)
at org.postgresql.pljava.internal.Oid.forSqlType(Oid.java:86)
at org.postgresql.pljava.jdbc.SPIPreparedStatement.setObject(SPIPreparedStatement.java:248)
at org.postgresql.pljava.jdbc.SPIPreparedStatement.setObject(SPIPreparedStatement.java:233)
at org.postgresql.pljava.jdbc.SPIPreparedStatement.setInt(SPIPreparedStatement.java:124)
at org.postgresql.pljava.sqlj.Loader.findClass(Loader.java:378)
at java.lang.ClassLoader.loadClass(ClassLoader.java:418)
at java.lang.ClassLoader.loadClass(ClassLoader.java:351)
at org.trinet.util.gazetteer.TN.WhereAmI.<init>(WhereAmI.java:11)
at org.trinet.util.gazetteer.TN.WhereBasicPt.<init>(WhereBasicPt.java:21)
at org.trinet.util.gazetteer.TN.WheresFrom.<clinit>(WheresFrom.java:32)
at java.lang.Class.forName0(Native Method)
at java.lang.Class.forName(Class.java:264)
at org.trinet.util.gazetteer.WhereIsEngine.create(WhereIsEngine.java:83)
at org.trinet.util.gazetteer.WhereIsEngine.create(WhereIsEngine.java:66)
at org.trinet.util.gazetteer.WhereIsEngine.create(WhereIsEngine.java:51)
at org.trinet.util.gazetteer.TN.WheresFromServer.<clinit>(WheresFromServer.java:26)
ERROR:  java.lang.NoClassDefFoundError: org/trinet/util/gazetteer/GeoidalUnits
```

2. Missing `ASSOCEVENTS` may cause an exception if the Java function is trying to retrieve a solution that has associated events. For example, `select Formats.getcubeemailformat(60000001);` failed with the following error:

```
2024-05-21 22:08:31.003 PDT [2894868] ERROR:  java.lang.NoClassDefFoundError: org/trinet/jiggle/common/JiggleExceptionHandler
2024-05-21 22:08:31.003 PDT [2894868] STATEMENT:  Select Formats.getcubeemailformat(60165591);
```

It appears that the exception was thrown, but the exception could not be handled as PL/Java was not able to find the `JiggleExceptionHandler` class. Not sure why yet, but the `NoClassDefFoundError` for `JiggleExceptionHandler` may indicate a missing table. 