FROM registry.access.redhat.com/ubi8/ubi:8.1

USER root

ENV PG_SCRIPT_HOME=/var/lib/pgsql/DBpg/create
ENV ALLOWED_SUBNETS=172.20.2.0/24
ENV MSEEDDIR=/usr/local/libmseed-2.19.6
ENV PGCONFIGDIR=/usr/pgsql-14/bin
ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1

# Create user postgres
# Install epel, epel provides ansible
# Install sudo as some database install scripts need to run as root

RUN dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm

RUN groupadd -r postgres && useradd --no-log-init -r -m -d /home/postgres -g postgres postgres \
    && yum install -y epel-release \
    && yum install -y vim \
    && yum install -y sudo

RUN dnf -y install python3 python3-pip

RUN pip3 install -U pip

RUN pip3 install ansible --user


# DEBUG only, can be removed. net-tools provides netstat
#RUN yum install -y net-tools

# Copy ansible config file to container
COPY aqms-postgres14.yml /tmp

# Copy aqms schema setup and install scripts to container
COPY DBpg /var/lib/pgsql/DBpg
RUN chown -R postgres:postgres /var/lib/pgsql/DBpg

# Install postgres, related packages, postgis,
# configure postgres networking,
# build and install libmseed
RUN dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
RUN dnf -qy module disable postgresql
#RUN dnf config-manager --enable codeready-builder-for-rhel-8-x86_64-rpms
RUN dnf install -y make

RUN dnf clean all

RUN curl -L -o /tmp/stable-2.9.tar.gz "https://github.com/ansible/ansible/archive/stable-2.9.tar.gz"

WORKDIR /tmp

RUN tar xvfz stable-2.9.tar.gz

WORKDIR /tmp/ansible-stable-2.9

RUN pip3 install .



RUN ansible-playbook --connection=local --inventory 127.0.0.1, /tmp/aqms-postgres14.yml

WORKDIR /var/lib/pgsql/DBpg/create

# Following 2 steps install AQMS schema
RUN ./generate_sql_scripts.sh

USER postgres
ENV USER=postgres
ENV HOME=/home/postgres

RUN /usr/pgsql-14/bin/pg_ctl start -D /data/postgres -l /tmp/postgres_server.log \
    && ./run_as_postgres.sh \
    && ./run_sql_scripts.sh

EXPOSE 5432

#CMD ["/usr/pgsql-14/bin/pg_ctl", "start",  "-D',  "/data/postgres", "-l", "/tmp/postgres_server.log"]

CMD ["/usr/pgsql-14/bin/postgres", "-D", "/data/postgres", "-c", "config_file=/data/postgres/postgresql.conf"]
