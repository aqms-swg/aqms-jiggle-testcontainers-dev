package gov.usgs.swarm.wave;
import java.awt.event.MouseEvent;

// Derivative of WaveViewPanelAdapter by Dan Cervelli in AVO Swarm distribution
public class JiggleSwarmWaveViewPanelAdapter implements JiggleSwarmWaveViewPanelListener {
    public void waveZoomed(JiggleSwarmWaveViewPanel src, double st, double et, double nst, double net) {}
    
    public void mousePressed(JiggleSwarmWaveViewPanel src, MouseEvent e, boolean dragging) {}
    
    public void waveClosed(JiggleSwarmWaveViewPanel src) {}

    public void waveTimePressed(JiggleSwarmWaveViewPanel src, MouseEvent e, double j2k) {}
}
