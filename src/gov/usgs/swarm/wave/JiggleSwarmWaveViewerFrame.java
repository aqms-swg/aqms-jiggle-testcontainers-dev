// Derivative of WaveViewerFrame by Dan Cervelli in AVO Swarm distribution
package gov.usgs.swarm.wave;

import gov.usgs.swarm.Images;
//import gov.usgs.swarm.Swarm;
import gov.usgs.swarm.SwarmUtil;
import gov.usgs.swarm.TimeListener;
import gov.usgs.swarm.data.SeismicDataSource;

import gov.usgs.util.Util; // need source or class jar for this

import gov.usgs.vdx.data.wave.Wave;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
//import java.awt.MouseInfo;
//import java.awt.Point;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
//import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
//import javax.swing.event.InternalFrameAdapter;
//import javax.swing.event.InternalFrameEvent;

import org.trinet.jasi.Solution;
import org.trinet.jasi.PhaseList;
import org.trinet.jiggle.Jiggle;
import org.trinet.jiggle.JiggleSwarmDataSource;
import org.trinet.jiggle.MasterView;
import org.trinet.jiggle.WFView;
import org.trinet.jiggle.WFPanel;
import org.trinet.util.LeapSeconds;
import org.trinet.util.TimeSpan;
import org.trinet.util.graphics.IconImage;

public class JiggleSwarmWaveViewerFrame extends JPanel implements PropertyChangeListener, WindowListener, TimeListener, ComponentListener {
    
    private final static int[] SPANS = new int[] {15, 30, 60, 120, 180, 240, 300};
    private int spanIndex;
    private JiggleSwarmDataSource jiggleSDS; // SeismicDataSource;
    
    private JiggleSwarmWaveViewSettings settings;
    private JiggleSwarmWaveViewPanel waveViewPanel;
    
    private JLabel title = new JLabel("No selected WFV channel");
    private JToolBar toolBar;
    private JPanel wavePanel;
    private WFWindowChangeListener mvWindowModelChangeListener = new WFWindowChangeListener();
    private WFViewChangeListener   mvWfvChangeListener = new WFViewChangeListener(); 

    //private MasterView mv = null; // don't keep private copy assume Jiggle cleanup of MasterView purged listeners

    // Create instance with a JiggleSwarmDataSource 
    public JiggleSwarmWaveViewerFrame(JiggleSwarmDataSource jsds)
    {
        //super(jsds.toString(), true, true, false, true);

        jiggleSDS = jsds;
        settings = new JiggleSwarmWaveViewSettings();
        jiggleSDS.jiggle.addPropertyChangeListener(this);
        //this.mv = jiggleSDS.getMasterView();
        //mv.masterWFWindowModel.addChangeListener(mvWindowModelChangeListener); // removed 2009/03/13 -aww
        //mv.masterWFViewModel.addChangeListener(mvWfvChangeListener); // removed 2009/03/13 -aww
        jiggleSDS.getMasterView().masterWFWindowModel.addChangeListener(mvWindowModelChangeListener); // enabled 2009/03/13 -aww
        jiggleSDS.getMasterView().masterWFViewModel.addChangeListener(mvWfvChangeListener); // enabled 2009/03/13 -aww
        spanIndex = 3;
        createUI();
    }
    
    public void timeChanged(double j2k) {
        //Point p = MouseInfo.getPointerInfo().getLocation(); 
        //SwingUtilities.convertPointFromScreen(p, waveViewPanel);
        //if (j2k == Double.NaN || !waveViewPanel.contains(p) )
            waveViewPanel.setCursorMark(j2k);
    }

    class WFWindowChangeListener implements ChangeListener {
        public void stateChanged(ChangeEvent changeEvent) {
            //System.out.println("WFWindow change");
            getWave();
        }
    }

    class WFViewChangeListener implements ChangeListener {
        public void stateChanged (ChangeEvent changeEvent) {
              SwingUtilities.invokeLater(
                  new Runnable() {
                      public void run() {
                          //System.out.println("WFView change");
                          jiggleSDS.setWF();
                          getWave();

                          //JiggleSwarmWaveClipboard cb = jiggleSDS.jiggle.getSwarmClipboard();
                          //if (cb != null && waveViewPanel != null) {
                          //   cb.dataChooser.setNearest(waveViewPanel.getChannel());
                          //}

                      }
                  }
              );
        }
    }

    public void createUI()
    {
        //this.setFrameIcon(Images.getIcon("wave")); // move to Dialog -aww
        this.setLayout(new BorderLayout());
        setFocusable(true); // ??
        waveViewPanel = new JiggleSwarmWaveViewPanel(settings);
        wavePanel = new JPanel(new BorderLayout());
        wavePanel.add(waveViewPanel, BorderLayout.CENTER);
        Border border = BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(0, 2, 3, 3), 
                LineBorder.createGrayLineBorder());
        wavePanel.setBorder(border);

        this.add(wavePanel, BorderLayout.CENTER);
        
        toolBar = SwarmUtil.createToolBar();
        

        JButton jiggleButton = SwarmUtil.createToolBarButton(
               new ImageIcon(IconImage.getImage("mini-jiggle.gif")),
               "Show Jiggle frame",
               new ActionListener() {
                   public void actionPerformed(ActionEvent e) {
                       if (jiggleSDS.jiggle != null) {
                           jiggleSDS.jiggle.setVisible(true);
                           if ((jiggleSDS.jiggle.getExtendedState() & Frame.ICONIFIED) == 1) {
                               if ((jiggleSDS.jiggle.getExtendedState() | Frame.ICONIFIED) == 1) 
                                  jiggleSDS.jiggle.setExtendedState(Frame.NORMAL); // restored from iconified -aww
                               else
                                  jiggleSDS.jiggle.setExtendedState(Frame.MAXIMIZED_BOTH); // restored from iconified -aww
                           }
                           jiggleSDS.jiggle.toFront();
                           jiggleSDS.jiggle.requestFocus();
                       }
                   }
               });
        Util.mapKeyStrokeToButton(this, "J", "jiggle", jiggleButton);
        toolBar.add(jiggleButton);



        // Test here or comment out SPANS -aww
        JButton compXButton = SwarmUtil.createToolBarButton(
                Images.getIcon("xminus"),
                "Shrink time axis (Alt-left arrow)",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        if (spanIndex != 0) spanIndex--;
                        getWave(true); // added this -aww
                    }
                });
        Util.mapKeyStrokeToButton(this, "alt LEFT", "compx", compXButton);
        toolBar.add(compXButton);
        
        JButton expXButton = SwarmUtil.createToolBarButton(
                Images.getIcon("xplus"),
                "Expand time axis (Alt-right arrow)",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        if (spanIndex < SPANS.length - 1) spanIndex++;
                        getWave(true); // added this -aww
                    }
                });
        Util.mapKeyStrokeToButton(this, "alt RIGHT", "expx", expXButton);                
        toolBar.add(expXButton);
        
        toolBar.addSeparator();
        JButton mvWaveButton = SwarmUtil.createToolBarButton(
                Images.getIcon("timeback"),
                "Reset wave timespan to jiggle bounds",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        getWave(); // added this -aww
                    }
                });
        toolBar.add(mvWaveButton);

        toolBar.addSeparator();
        // end of comment out of SPANS test block  -aww
        //

        new JiggleSwarmWaveViewSettingsToolbar(settings, toolBar, this);
        
        JButton clipboardButton = SwarmUtil.createToolBarButton(
                Images.getIcon("clipboard"),
                "Copy wave to clipboard (C or Ctrl-C)",
                new ActionListener()
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        if (waveViewPanel != null)
                        {
                            JiggleSwarmWaveClipboard cb = jiggleSDS.jiggle.getSwarmClipboard();
                            if (cb != null) {
                               cb.setVisible(true);
                               cb.addWave(new JiggleSwarmWaveViewPanel(waveViewPanel));
                            }
                        }
                    }
                });
        Util.mapKeyStrokeToButton(this, "C", "clipboard1", clipboardButton);
        Util.mapKeyStrokeToButton(this, "control C", "clipboard2", clipboardButton);
        toolBar.add(clipboardButton);
        
        toolBar.addSeparator();
        
        toolBar.add(Box.createHorizontalGlue());
        
        
        this.add(toolBar, BorderLayout.NORTH);
        this.add(title, BorderLayout.SOUTH);
        
        // Frame only block
        /*
        this.addInternalFrameListener(new InternalFrameAdapter()
                {
                    public void internalFrameActivated(InternalFrameEvent e)
                    {
                        //String channel = (String) jiggleSDS.getChannels().get(0);
                        //if (channel != null) Swarm.getApplication().getDataChooser().setNearest(channel);
                    }
                    
                    public void internalFrameClosing(InternalFrameEvent e)
                    {
                        //Swarm.getApplication().removeInternalFrame(JiggleSwarmWaveViewerFrame.this);
                        mv.masterWFWindowModel.removeChangeListener(mvWindowModelChangeListener);
                        mv.masterWFViewModel.removeChangeListener(mvWfvChangeListener);
                        jiggleSDS.close();
                    }
                });
                
        this.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE); // -aww
        this.setContentPane(mainPanel);
        */ // Frame only block
        
        this.setPreferredSize(new Dimension(790, 320));
        this.setMinimumSize(new Dimension(150, 100));
        this.getWave(); // instead of Thread
        this.setVisible(true);
    }

    public void componentHidden(ComponentEvent e) {}
    public void componentMoved(ComponentEvent e) {}
    public void componentResized(ComponentEvent e) {
        if (waveViewPanel != null) waveViewPanel.createImage();
    }
    public void componentShown(ComponentEvent e) {}
 
    public void windowActivated(WindowEvent e) {}
    public void windowClosed(WindowEvent e) {}

    public void windowClosing(WindowEvent e) {
        //mv.masterWFWindowModel.removeChangeListener(mvWindowModelChangeListener); // removed 2009/03/13 -aww
        //mv.masterWFViewModel.removeChangeListener(mvWfvChangeListener); // removed 2009/03/13 -aww
        jiggleSDS.getMasterView().masterWFWindowModel.removeChangeListener(mvWindowModelChangeListener); // enabled 2009/03/13 -aww
        jiggleSDS.getMasterView().masterWFViewModel.removeChangeListener(mvWfvChangeListener); // enabled 2009/03/13 -aww
        //jiggleSDS.close();
    }

    public void windowDeactivated(WindowEvent e) {}
    public void windowDeiconified(WindowEvent e) {}
    public void windowIconified(WindowEvent e) {}
    public void windowOpened(WindowEvent e) {}

    public void propertyChange(PropertyChangeEvent evt) {
        String propName = evt.getPropertyName();
        if (propName.equals(Jiggle.NEW_MV_PROPNAME)) {
            //this.mv.masterWFWindowModel.removeChangeListener(mvWindowModelChangeListener); // removed 2009/03/13 -aww
            //this.mv.masterWFViewModel.removeChangeListener(mvWfvChangeListener); // removed 2009/03/13 -aww
            //this.mv = (MasterView) evt.getNewValue(); // removed 2009/03/13 -aww
            jiggleSDS.getMasterView().masterWFWindowModel.addChangeListener(mvWindowModelChangeListener); // enabled 2009/03/13 -aww
            jiggleSDS.getMasterView().masterWFViewModel.addChangeListener(mvWfvChangeListener); // enabled 2009/03/13 -aww
            //this.mv.masterWFWindowModel.addChangeListener(mvWindowModelChangeListener); // removed 2009/03/13 -aww
            //this.mv.masterWFViewModel.addChangeListener(mvWfvChangeListener); // removed 2009/03/13 -aww
            jiggleSDS.setWF();
            getWave();
        }
        else if (propName.equals(WFPanel.FILTER_CHANGED)) {
            //System.out.println("NOTE: Swarm Viewer received WFPanel.FILTER_CHANGED");
            jiggleSDS.setWF();
            getWave();
        }
        else if (propName.equals(JSplitPane.DIVIDER_LOCATION_PROPERTY)) {
            if (waveViewPanel != null) waveViewPanel.createImage();
        }
        /*
        else if (propName.equals(Jiggle.NEW_MV_WFVIEW_PROPNAME)) {
            jiggleSDS.setWF();
            getWave();
        }
        */
    }

    public void getWave() { // from Jiggle MasterView currently selected WFV waveform
        getWave(false);
    }
    public void getWave(boolean useSPAN) {
        String channel = (String) jiggleSDS.getChannels().get(0);
        //Wave sw = jiggleSDS.getWave(channel, now - SPANS[spanIndex], now);
        setTitle(jiggleSDS.toTitleString());
        TimeSpan ts = jiggleSDS.getWaveformTimeSpan(); // jiggleSDS.getMasterView().masterWFWindowModel.getTimeSpan();
        double start = ts.getStart();
        double end   = ts.getEnd();
        double centerTime = ts.getCenter();

        if (useSPAN) {
            start = centerTime - SPANS[spanIndex]; // try else comment out SPANS -aww
            end   = centerTime + SPANS[spanIndex]; // try else comment out SPANS -aww
        }

        Wave sw = jiggleSDS.getWave(channel, start, end);
        //System.out.println("JWVF DEBUG getWave(): " + sw);
 
        //WFView wfv = mv.masterWFViewModel.get(); // removed 2009/03/13 -aww
        WFView wfv = jiggleSDS.getMasterView().masterWFViewModel.get(); // enabled 2009/03/13 -aww
        if (wfv != null) {
            Solution sol = jiggleSDS.getMasterView().getSelectedSolution();
            if (sol != null) {
                PhaseList pList = new PhaseList(sol.getPhaseList().getAssociatedByChannel(wfv.getChannelObj()));
                if (pList.size() > 0) {
                    if (pList.size() > 1) {
                        pList.sortByTime();
                        waveViewPanel.setMarks(
                            Util.ewToJ2K( LeapSeconds.trueToNominal(pList.getPhase(0).getTime()) ),
                            Util.ewToJ2K( LeapSeconds.trueToNominal(pList.getPhase(1).getTime()) )
                        ); 
                    }
                    else waveViewPanel.setMarks(Util.ewToJ2K( LeapSeconds.trueToNominal(pList.getPhase(0).getTime())), Double.NaN ); 
                }
            }
        }
        waveViewPanel.setWorking(true);
        waveViewPanel.setWave(sw, start, end);
        waveViewPanel.setChannel(channel);
        waveViewPanel.setDataSource(jiggleSDS);
        waveViewPanel.setWorking(false);
        waveViewPanel.repaint();
    }
    
    public void setTitle(String title) {
       this.title.setText(title);
    }

}
