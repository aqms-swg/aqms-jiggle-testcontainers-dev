package org.trinet.apps;
/**
 * Scan for strong motion amplitudes.
 */
import java.io.*;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.sql.PreparedStatement;
import java.util.*;

import org.trinet.filters.TotalGroundMotionFilter;
import org.trinet.jiggle.WaveServerGroup;
import org.trinet.jasi.*;
import org.trinet.util.*;
//import org.trinet.jasi.TN.ChannelTN;
//import org.trinet.jdbc.ResultSetDb;
//import org.trinet.jdbc.StringSQL;
import org.trinet.jdbc.datasources.*;
//import org.trinet.jdbc.datatypes.DataDouble;
//import org.trinet.util.gazetteer.GeoidalUnits;

public class AmpGenPp {

    /**The jiggle-style properties object. */
    private static SolutionWfEditorPropertyList props;

    private static boolean debug = true;

    private static DataSource ds = null;

    /** Defines the channels that will be scanned for strong motions.
     * The list is specified via a Chanloader channel group name
     * (as found in Program.Name attribute).
     * Default is "AmpGen". This is case sensitive.
     */
    private static String channelGroupName = "AmpGen";
    /** The string to be written to the subsource column of the Amplitude row.
      * It cannot exceed 8 characters.*/
    private static String sourceString = "AmpGenPp";
    /** The local tow-character FDSN network code.
     * This is written to the "auth" field of the resulting Amplitude rows.*/
    private static String authString = "CI";

    /** seconds before P-wave to include in the scanned window */
    private static double preEventTime = 1.0;
    /** minimum length in seconds to scan */
    private static double minTimeWindow = 6.0;
    /** maximum length in seconds to scan */
    private static double maxTimeWindow = 120.0;
    /** the maximum distance in km out to which include stations. */
    private static double maxDistance = 500.0;
    /** If true write resulting Amplitudes to the database. */
    private static boolean writeToDB = true;

    /** accelerations below this value (cm/sec**2) are marked "bn" (Below Noise)*/
    private static double minAcceleration=1.0;
    /** velocities above this value (cm/sec) are marked "os" (Off Scale)*/
    private static double maxVelocity=1.0;

    private static ChannelTimeWindowModel ctwModel; // = new EnergyWindowModel();

    private static void setChannelGroupName(String name) {
        channelGroupName = name;
    }

    private static String getChannelGroupName() {
        return channelGroupName;
    }

    /** Set and check clipping. Assumes all amps in list are from the same channel.
     * Return true if amps are on scale. Checks only 'primary' amp;
     * i.e. vel for EH & HH, accel for HL & HN and sets ALL amp's clipped
     * flag the same.
     * @see Amplitude.clipped
    */
    private static boolean checkOnScale(AmpList list) {
        if (list == null || list.isEmpty()) return false;
        //    Amplitude ON_SCALE = 0;
        //    Amplitude CLIPPED  = 1;
        //    Amplitude BELOW_NOISE = 2;

        Channel ch = ((Channelable) list.get(0)).getChannelObj();
        Amplitude amp = null;

        if (ch.isAcceleration()) {
          amp = list.getType(AmpType.PGA);
          if (amp == null) return false;

          if (Math.abs(amp.getValue().doubleValue()) < minAcceleration) {  // accel too low
            setClipFlags(list, Amplitude.BELOW_NOISE);
          }

        } else if (ch.isVelocity()) {
          amp = list.getType(AmpType.PGV);
          if (amp == null) return false;

          if (Math.abs(amp.getValue().doubleValue()) > maxVelocity) {      // vel too high
            setClipFlags(list, Amplitude.CLIPPED);
          }

        } else {
          return false;
        }

        return true;
    }

      /** Set clip flag of all amps in the list to the given value. */
    private static void setClipFlags(AmpList list, int value) {
        for (int i = 0; i < list.size(); i++) {
          ((Amplitude) list.get(i)).setClipped(value);
        }
    }

      /** Read properties file. Need waveserver and dbase info. */
    private static boolean setPropertiesFile(String propertiesFile) {

        props = new SolutionWfEditorPropertyList();
        props.setUserPropertiesFileName(propertiesFile);
        props.reset();    // reads all the property files
        props.setup();

        // set values
        if (getProperties() != null) {
          if (props.isSpecified("channelGroupName")) channelGroupName = props.getProperty("channelGroupName");
          if (props.isSpecified("localNetCode"))     authString = props.getProperty("localNetCode");
          if (props.isSpecified("sourceName"))       sourceString = props.getProperty("sourceName");

          if (props.isSpecified("maxDistance"))      maxDistance = props.getDouble("maxDistance");
          if (props.isSpecified("maxTimeWindow"))    maxTimeWindow = props.getDouble("maxTimeWindow");
          if (props.isSpecified("minTimeWindow"))    minTimeWindow = props.getDouble("minTimeWindow");
          if (props.isSpecified("preEventTime"))     preEventTime = props.getDouble("preEventTime");
          if (props.isSpecified("writeToDB"))        writeToDB = props.getBoolean("writeToDB");
          if (props.isSpecified("debug"))            debug = props.getBoolean("debug");

          if (props.isSpecified("minAcceleration"))  minAcceleration = props.getDouble("minAcceleration");
          if (props.isSpecified("maxVelocity"))      maxVelocity = props.getDouble("maxVelocity");
      }

      return (props != null);
    }

    private static SolutionWfEditorPropertyList getProperties() {
        return props;
    }

    // ////////////////////////////////////////
    public static final void main(String[] args) {
    
        String propfilename = "AmpGenPp.props";
    
        // Print syntax if no args
        if (args.length < 2) {
          System.err.println ("Usage: AmpGenPp [evid] [properties-file]");
          stop(-1);
        }
    
        // Get event ID
        long evid = 0;;
        if (args.length > 0) {
          Long val = Long.valueOf(args[0]);
          evid = val.longValue();
        }
    
        // Get input props filename
        if (args.length > 1) {
          propfilename = args[1];
        }
    
        // Read properties from file
        if (!setPropertiesFile(propfilename)) {
          System.err.println("AmpGenPp: Bad properties file: "+propfilename) ;
          stop(-1);
        } else if (debug) {
          getProperties().dumpProperties();
          System.out.println();
        }

/*
//Slow query, only works for case where amp SNCL and amptype are specified in condition,
//but does work for obtaining complete set of amps, see analytic function below:
select t.* from (select a.* from amp a, ampset b, assocevampset c where
      a.ampid=b.ampid and b.ampsetid=c.ampsetid and c.ampsettype=? and c.subsource=? and c.isvalid=1
      and a.net=? and a.sta=? and a.seedchan=? and a.location=? and a.amptype=?
  order by a.net,a.sta,a.seedchan,a.location,a.lddate desc
) t where rownum < 2

// Next 2 queries are about the same speed, 1st uses the row_number function each row# is unique but order nondeterministic when lddates are same
select t.* from (select a.*, row_number() over (partition by a.net,a.sta,a.seedchan,a.location,a.amptype order by a.lddate desc) arow
    from amp a, ampset b, assocevampset c where
      a.ampid=b.ampid and b.ampsetid=c.ampsetid and c.ampsettype=? and c.subsource=? and c.isvalid=1
     -- and a.net=? and a.sta=? and a.seedchan=? and a.location=?
) t where t.arow=1

// 2nd uses rank() partition function, returns multiple rows with same rank# when lddates are the same DON'T USE THIS
select t.* from (select a.*, rank() over (partition by a.net,a.sta,a.seedchan,a.location,a.amptype order by a.lddate desc) arank
  from amp a, ampset b, assocevampset c where
  a.ampid=b.ampid and b.ampsetid=c.ampsetid and c.ampsettype=? and c.subsource=? and c.isvalid=1
  -- and a.net=? and a.sta=? and a.seedchan=? and a.location=?
) t where t.arank=1
        
   // select a.*  from amp a, ampset b, assocevampset c where
   // a.ampid=b.ampid and b.ampsetid=c.ampsetid and c.ampsettype=? and c.subsource=? and c.isvalid=1 
   // and a.net=? and a.sta=? and a.seedchan=? and a.location=? 
   // order by a.net,a.sta,a.seedchan,a.location,a.lddate desc 
   // if (! list.hasSameSeedchanType(jr)) list.add(jr); 

*/
        // Set global app name from property
        EnvironmentInfo.setApplicationName(sourceString);
    
        // Setup benchmark for timing
        BenchMark bm = new BenchMark ();
    
        // Create db connection
        DbaseConnectionDescription dbd = getProperties().getDbaseDescription();
        if (debug) System.out.println ("AmpGenPp: Making connection to "+dbd.getURL());
        ds = new DataSource();
        ds.set(dbd); // aww 2008/06/26
    
        System.out.println("AmpGenPp: Start at " + new DateTime());

        // Get waveforms input mode 
        int wfMode = getProperties().getInt("waveformReadMode"); // =0 database, =1 waveserver

        if (wfMode == AbstractWaveform.LoadFromDataSource) { // from database

          AbstractWaveform.setWaveDataSource(getProperties().getWaveSource());  //'waveformReadMode' doesn't matter
          ctwModel = new DataSourceChannelTimeModel(); // define the CTW model

        } else if (wfMode == AbstractWaveform.LoadFromWaveServer) { // from WaveServer
    
          // Make a WaveClient
          try {
            org.trinet.jiggle.WaveServerGroup waveClient = getProperties().getWaveServerGroup();
    
            if (waveClient == null || waveClient.numberOfServers() <= 0) {
              System.err.println(
                  "AmpGenPp: Error, no or bad waveservers in properties file. " );
              System.err.println("waveServerGroupList = " + getProperties().getProperty("waveServerGroupList"));
              stop(-1);
            }
    
            AbstractWaveform.setWaveDataSource(waveClient);  //'waveformReadMode' doesn't matter
    
            ctwModel = new EnergyWindowModel(); // define the CTW model ??
    
          } catch (Exception ex) {
            System.err.println(ex.toString());
            ex.printStackTrace();
          }
        } else {
          System.err.println("AmpGenPp: Error, you must define a legal value of 'waveformReadMode'.");
          stop(-1);
        }
    
        if (debug) bm.printTimeStamp("Connections established,");
    
        // Load event info
        Solution sol = Solution.create().getById(evid);
        if (sol == null) {
           System.err.println("AmpGenPp: Error? unable to create Solution for evid, also check your database for valid event : "+evid);
           stop(-1);
        }
        if (debug) {
            System.out.println(sol.toSummaryString());
        }
    
        ChannelableList chanList = null;

        // If cache file name is specified load channels
        String cacheFileName = ( getProperties() == null) ? 
            null : getProperties().getProperty("channelCacheFilename");
        if (cacheFileName != null && cacheFileName.trim().length() > 0) {
            cacheFileName = new java.io.File(propfilename).getParent() + GenericPropertyList.FILE_SEP + cacheFileName;
            System.out.println("AmpGenPp: Loading MasterChannelList from cache file : "  + cacheFileName);
            MasterChannelList.set(ChannelList.readFromCache(cacheFileName));
            // Don't load all active channels, instead revert to the named group channels !!!
            //chanList = MasterChannelList.get();
            //if (chanList == null | chanList.isEmpty()) MasterChannelList.smartLoad(sol.getDateTime());
            //chanList = MasterChannelList.get();
        }

        chanList = MasterChannelList.get();
        if (chanList == null || chanList.isEmpty()) { // no cache, so use named group candidate channel list
          Channel ch = Channel.create();
          // Speed up channel data loading by limiting what you want
          Channel.setLoadClippingAmps(false); // clipping levels not used by model or filter
          Channel.setLoadKnownCorrections(false); // !!!!! don't need station magnitude ?
          Channel.setLoadResponse(true);
    
          // Abandoned local ChannelTN_Quick class
          //ChannelableList chanList = ChannelTN_Quick.getChannelList("RCG-TRINET");
          System.out.println("AmpGenPp: Loading ChannelList from db for group name: "  + getChannelGroupName());
          chanList = ch.getNamedChannelList(getChannelGroupName());
        } 
        
        if (chanList == null || chanList.isEmpty()) {
            System.err.println("AmpGenPp: Channel list resulting from channelGroupName="+ getChannelGroupName()+" is empty.");
            stop(-1);
        } else if (debug) {
            System.out.println("AmpGenPp: Channel list <"+getChannelGroupName()+ "> contains "+chanList.size()+" channels.");
        }
    
        // Setup time window model
        ctwModel.setProperties(props);

        ctwModel.setCandidateList(chanList);
        ctwModel.setSolution(sol); // do this after setting list, else it loads one from db, does distance sorts, too.

        // Override of abouve setup of time window model props (legacy config - deprecate this?)
        ctwModel.setMinWindowSize(minTimeWindow);
        ctwModel.setMaxWindowSize(maxTimeWindow);
        ctwModel.setPreEventSize(preEventTime);
        ctwModel.setMaxDistance(maxDistance) ;
    
        if (debug) bm.printTimeStamp("Channel time windows defined,");
    
        TotalGroundMotionFilter tgmf = new TotalGroundMotionFilter();

        // make CTW's
        ChannelableList wfList = (ChannelableList) ctwModel.getWaveformList();
    
        // Group stations by Nt.Sta and sort by chn w/ HH_ 1st, then HL_ (or HN_)
        // Note: we're not distance sorted anymore! They're alphabetical.
        StationGrouper stationGrouper = new StationGrouper(wfList);
        AmpList amplist = AmpList.create();
        ChannelableListIF sublist = null;
        AmpList smlist = null;
        Waveform wf = null;
        int knt = 0;
    
        while (stationGrouper.hasMoreGroups()) {
    
          sublist = stationGrouper.getNext();  // all comps of one sta
    
          for ( int i=0; i < sublist.size(); i++) {
    
            wf = (Waveform) sublist.get(i);
            if (wf.loadTimeSeries()) {
              //if (debug) System.out.println("AmpGenPp: Processing: "+wf.getChannelObj().toDelimitedSeedNameString());
                
              //tgmf.filter(AbstractWaveform.copyWf(wf)); // I don't believe we need a copy here -aww 2010/11/16
              tgmf.filter(wf); // create ground motions
              smlist =  tgmf.getPeaks();                // retrieve the 7 peaks
              if (checkOnScale(smlist) ) {
                 amplist.fastAddAll(smlist);            // save the 7 peaks
                 //if (debug) {
                 //    System.out.println("AmpGenPp: Total =" + amplist.size());   // dump list
                 //    System.out.println(smlist.toString(false));  
                 //}
              } 
    
              wf.unloadTimeSeries();
              knt++;
            } else {
              if (debug) System.out.println("AmpGenPp: No timeseries for "+wf.getChannelObj().toDelimitedSeedNameString());
            }
            // grouper insures loop will always do HH_ before HL_, if they're all good skip HL_'s
    
          } // end of for loop
    
          // show progress every 100 channels processed
          if ( debug && ( knt % 100 ) == 0 ) bm.printTimeStamp("*Finishing "+ knt +" took");
        }
    
        // set auth, source of all amps
        Amplitude amp[] = amplist.getArray();
    
        for ( int i=0; i < amp.length; i++) {
          amp[i].setAuthority(authString);
          amp[i].setSource(sourceString);
          amp[i].associate(sol);        // required for dbase write to be right
          amp[i].magnitude = null;    // just to be sure no AssocAmM rows happen
        }
    
        if (debug) {
            bm.printTimeStamp("Calculations done,");
            System.out.println("AmpGenPp: Calculated "+amplist.size()+" amps.");
            //System.out.println(amplist.toNeatString(false));
            for (int ii = 0; ii < amplist.size(); ii++) {
               System.out.println(((Amplitude) amplist.get(ii)).toNeatString());
            }
            //System.out.println(amplist.toString(false));
            //amplist.dump();
        }
    
        // write results to dbase.
        int count = amplist.size();
        if (count <= 0 ) {
          System.out.println("AmpGenPp: "+count+" amps generated, check for invalid evid, missing waveforms");
        }
        else if (writeToDB) {
          System.out.println("AmpGenPp: Writing "+count+" amps to database ...");
          // Need to first Invalidate those valid ampsets associated with same evid and having amps from RT and/or AmpGenPp source
          // Too much overhead to find and  delete pre-existing amps of same type for same channels in this set
          // before inserting the new amp associations into ampset table?
          final String ampSetType = "sm";
          // DISABLED TWO LINES HERE UNTIL AMPSET tables are READY:
          amplist.setAmpSetType(ampSetType); // if we set this field then amplist commit does commitAmpSet(evid) and:
          // invalidate set isValid=0 for existing sets of same type as ampSetType associated with solution evid of first amplitude in set 
          amplist.invalidateAmpSets("ampgen%");
          //
          amplist.commit(); 
          /*
          if (debug)
              System.out.printf("AmpGenPp: Committed %d %s amps in ampsetid=%d for evid=%d%n",
                      amplist.size(), amplist.getAmpSetType(), amplist.getAmpSetId(), evid);
          */
          DataSource.commit();
          DataSource.close();
          if (debug) bm.printTimeStamp("Database write done,");
        }
    
        if (debug) bm.printTimeStamp("Program exiting,");

        stop(0);
    }

    private static final void stop(int code) {
        if (ds != null) ds.close(); 
        System.out.println("AmpGenPp: Stop with exit code " + code + " at " + new DateTime());
        System.exit(code);
    }

    /* DEPRECATED, NOT MAINTAINED
    // Grab a simple, named list of channels with all the info needed for most RT-sytle tasks.
    // This is necessary for efficiency and speed. The existing, more generic
    // classes do multiple dbase transits to build up a list with all needed attributes.
    static class ChannelTN_Quick extends ChannelTN {
    
        {
            jasiDataReader.setFactoryClassName(getClass().getName());
        }

        private static ChannelableList getChannelList(String name) {
            jasiDataReader.setFactoryClassName("org.trinet.apps.AmpGenPp$ChannelTN_Quick");
            Collection col = jasiDataReader.getBySQL(toNamedProgramChannelListSelectString(name, null));
            return (col == null) ? new ChannelableList() : new ChannelableList(col);
        }

        private static ChannelableList getChannelList(String name, java.util.Date date) {
            Collection col = jasiDataReader.getBySQL(toNamedProgramChannelListSelectString(name, date));
            return (col == null) ? new ChannelableList() : new ChannelableList(col);
        }
        
        private static String toNamedProgramChannelListSelectString(String progName, java.util.Date date) {
          String dateStr = (date == null) ? "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)" : StringSQL.toDATE(date);
          StringBuffer sb = new StringBuffer(2048);
          sb.append("SELECT jcv.net, jcv.sta, jcv.seedchan, jcv.location, cd.lat, cd.lon, cd.elev, '50', ap.clip, sr.gain, sr.gain_units ");      
          sb.append(" From channel_data cd, simple_response sr, channelmap_ampparms ap, config_channel jcv ");
          sb.append(" WHERE jcv.progid = (select progid from program where name = '").append(progName).append("') ");
          sb.append(" AND ");
          sb = SNCLjoinString(sb, "cd", dateStr);
          sb.append(" AND ");
          sb = SNCLjoinString(sb, "sr", dateStr);
          sb.append(" AND ");
          sb = SNCLjoinString(sb, "ap", dateStr);
          sb.append(" order by jcv.net, jcv.sta, jcv.seedchan, jcv.location ");
          return sb.toString();
        }
    
        private static StringBuffer SNCLjoinString (StringBuffer sb, String tableName, String dateStr) {
            sb.append(" jcv.net = ").append(tableName).append(".net(+) ");
            sb.append(" AND jcv.sta = ").append(tableName).append(".sta(+) ");
            sb.append(" AND jcv.seedchan = ").append(tableName).append(".seedchan(+) ");
            sb.append(" AND jcv.location = ").append(tableName).append(".location(+) ");
            sb.append(" AND ").append(tableName).append(".offdate(+) > ").append(dateStr);      
            return sb;
        }
    
        private Channel my_parseResultSet(ResultSetDb rsdb){
            int offset = 0;
            ChannelTN chan = new ChannelTN();
    
            ResultSet rs = rsdb.getResultSet();
            ChannelIdIF chanId = chan.getChannelId();
            
                try {
                if (chan.getChannelId() == null) chanId = new ChannelName();
                chanId.setNet(rs.getString(++offset));
                chanId.setSta(rs.getString(++offset));
                chanId.setSeedchan(rs.getString(++offset));
                chanId.setLocation(rs.getString(++offset));
    
                DataDouble val = rsdb.getDataDouble(++offset);
                if (val != null && val.isValidNumber()) {
                    chan.getLatLonZ().setLat(val.doubleValue());
                }
                val = rsdb.getDataDouble(++offset);
                if (val != null && val.isValidNumber()) {
                    chan.getLatLonZ().setLon(val.doubleValue());
                }
                val = rsdb.getDataDouble(++offset); // meters?
                if (val != null && val.isValidNumber()) {
                    // convert elevation to km, which is what LatLonZ uses.
                    //chan.getLatLonZ().setZ(val.doubleValue()/1000.0, GeoidalUnits.KILOMETERS); // aww 06/11/2004
                    chan.getLatLonZ().setZ(-val.doubleValue()/1000.0, GeoidalUnits.KILOMETERS); // change to depth-axis convention aww 06/11/2004
                }
    
                // SHOULD WE CORRECT Z BY EMPLACEMENT DEPTH?
                //val = (DataDouble) rsdb.getDataDouble(++offset);   //m
                //if (val != null) chan.setSensorDepth(val.doubleValue());

                // noise floor
            
                // Clip level
            
                // gain w/ units
                
                val = rsdb.getDataDouble(++offset);
                if (val != null && val.isValidNumber()) chan.setSampleRate(val.doubleValue());

            }
            catch (SQLException ex) {
                System.err.println(ex);
                ex.printStackTrace();
            }
    
            return chan;
        }
        
        public Channel parseResultSetDb(Object rsdb) {
            return my_parseResultSet((ResultSetDb)rsdb);        
        }
    } // end of QuickChannelList
    */
}
