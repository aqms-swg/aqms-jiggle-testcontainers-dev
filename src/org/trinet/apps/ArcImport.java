package org.trinet.apps;

import org.trinet.hypoinv.ArcSummary;
import org.trinet.jasi.*;
import org.trinet.jdbc.datasources.DbaseConnectionDescription;
import org.trinet.jdbc.datatypes.DataString;
import org.trinet.jiggle.JiggleProperties;
import org.trinet.util.gazetteer.GeoidalLatLonZ;
import org.trinet.util.gazetteer.LatLonZ;
import org.trinet.util.locationengines.LocationEngineHypoInverse;
import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.util.List;
import java.util.Scanner;

/**
 * ARCOUT file import created based on LocationEngineHypoInverse.java.
 */
public class ArcImport extends JasiPropertyList {

  final String APPNAME = "HYGPD";

  JiggleProperties jp;
  String locatorName;

  public ArcImport(String propFileName) {
    jp = this.getProps(propFileName);
    getDbConnection(jp);
    this.locatorName = jp.getProperty("locationEngineType","Unknown");
    
    String appname = jp.getProperty("appName", APPNAME);
    if (appname.length() > 8) {
      // database constraint
      System.err.println("appName cannot be more than 8 characters. Default to " + APPNAME);
      EnvironmentInfo.setApplicationName(APPNAME);
    } else {
      EnvironmentInfo.setApplicationName(appname);      
    }
  }

  public static void main(String[] args) {
    if (args.length < 2) {
      System.err.println("ArcImport requires two command line arguments");
      System.err.println("Usage: ArcImport [ARCOUT File] [Properties-file]");
      System.exit(1);
    } else {
      String arcOutFile = args[0];
      String propFile = args[1];

      ArcImport arc = new ArcImport(propFile);
      arc.importArc(arcOutFile);
    }
  }

  public int importArc(String arcFileName) {
    boolean status = readArcFile(arcFileName);
    return status ? 0 : 1;
  }

  private String getLocatorName() {
    return locatorName;
  }

  /**
   * Process ARCOUT file, create a solution, create a phaselist, and save to DB
   */
  private boolean readArcFile(String arcFileName){
    int total = 0;
    PhaseList currentPhaseList = new PhaseList();
    PhaseList existingPhaseList = new PhaseList();
    boolean isHeader = true;
    Solution currentSol = null;
    LatLonZ oldLatLonZ = null;
    String arcHeader = "";
    ArcSummary arcSummary = new ArcSummary();

    File arcFile = new File(arcFileName);
    try (Scanner arcReader = new Scanner(arcFile)) {
      while (arcReader.hasNextLine()) {
        String data = arcReader.nextLine();

        // read summary line
        if (isHeader) {
          isHeader = false;
          arcHeader = data;

          if (arcSummary.parseArcSummary(arcHeader)) {
            currentSol = Solution.create().getById(arcSummary.evid);

            if (currentSol != null) {
              currentSol = createSolution(currentSol, arcSummary, arcHeader); // returns null when error
              existingPhaseList = currentSol.getPhaseList();
            } else {
              return false;
            }
          } else {
            System.err.println("Bad Arc Summary Header parseStatus");
            return false; // added short-circuit here -aww 2010/08/24
          }

          System.out.println("Processed summary header - Solution is " + currentSol);
        } else if (data.length() != 0 && !data.startsWith("     ")) {
          // Process phase list
          total = setPhaseFromARC(currentSol, data, currentPhaseList, existingPhaseList, total);
        } else {
          System.out.println("Skipping empty or last line of ARC output: " + data);
        }
        System.out.println(data);
      }
      arcReader.close();
    } catch (FileNotFoundException e) {
      System.err.println("Error reading ARCOUT file. Error: ");
      e.printStackTrace();
    }

    if (currentSol.getPhaseList() != null) {
      currentSol.getPhaseList().clear();
      currentSol.getPhaseList().addAll(currentPhaseList);

      oldLatLonZ = currentSol.getLatLonZ();
      currentSol = setEventDetails(currentSol, oldLatLonZ);
      currentSol.source.setValue(EnvironmentInfo.getApplicationName());

      String dumpStr = currentSol.fullDump();
      System.out.println("********* OUTPUT CHANGED **********");
      System.out.println(dumpStr);

      try {
        System.out.println("Committing solution...");
        currentSol.commit();
      } catch (JasiCommitException e) {
        System.err.println("Error reading ARCOUT file. Error: ");
        e.printStackTrace();
      }
      System.out.println(currentSol.getCommitStatus());
    } else {
      System.err.println("Empty phase list in the solution. Cannot process phase list.");
    }

    return true;
  }

  /**
   * Code from LocationEngineHypoInverse.readLocationResults to set solution details
   */
  private Solution setEventDetails(Solution currentSol, LatLonZ oldLatLonZ) {
    currentSol.gap.setValue(currentSol.getPhaseList().getMaximumAzimuthalGap());

    currentSol.totalReadings.setValue(currentSol.phaseList.getChannelWithInWgtCount()); // all with inWgt> 0 -aww 2011/05/11
    currentSol.usedReadings.setValue(currentSol.phaseList.getChannelUsedCount());
    currentSol.sReadings.setValue(currentSol.phaseList.getChannelUsedCount("S")); // all S with weight > 0 -aww 2011/05/11

    currentSol.setStale(false); // reset flag since solution is now "fresh"
    currentSol.setSolveDate(); // set current time

    //Check for location change from previous, if so, set "stale" state for dependent magnitudes -aww
    double deltaKm = oldLatLonZ.distanceFrom(currentSol.getLatLonZ());
    // Flag a commit if its origin change > 10 meters
    if ( deltaKm > 0.01) {
      // Only redo its magnitudes if origin change > 100 meters
      if (deltaKm >= 0.10) {
        currentSol.setMagnitudeStatusStale(); // force new magnitude calculation
      }
      else {
        currentSol.setMagnitudeNeedsCommit(); // force synch orid update - aww 2008/03/11
      }
    }

    return currentSol;
  }

  /**
   * Calculates the distance from input location to the Channelable elements
   * of the input list and sets the distance attribute of these elements.
   */
  public void calcDistances(List list, GeoidalLatLonZ latLonZ) { // aww 06/11/2004
    int count = list.size();
    if (count == 0) return;
    for (int i = 0; i<count; i++) {
      ((Channelable) list.get(i)).calcDistance(latLonZ);
    }
  }

  /**
   * Create a new phase from ARCOUT and save to a list
   */
  private int setPhaseFromARC(Solution currentSol, String data, PhaseList currentPhaseList, PhaseList existingPhaseList, int total) {
    Phase phNew = HypoFormat.parseArcToPhase(data);
    // find matching phase in currentPhaseList & transfer 'result' part of newly
    // parsed phase (ie the 'assoc' object) into original input phase.
    if (phNew != null) {
      Phase matchingPhase = getMatchingPhase(phNew, existingPhaseList);

      if (matchingPhase != null) {
        currentPhaseList.addOrReplace(matchingPhase);
      } else {
        phNew.assign(currentSol); // associate with current solution
        currentPhaseList.addOrReplace(phNew);
      }
      total++;
    } else {
      System.err.println("HypoFormat phase line parse failure");
    }
    return total;
  }

  /**
   * Based on copyDependentData function
   * Find the original phase that returned in the Location output and copy all
   * the Solution dependent values to the original phase.
   * */
  private Phase getMatchingPhase(Phase newPhase, PhaseList phaseList) {
    //Note: phaselist method below does not check for a sol association match:
    Phase oldPhase = (Phase) phaseList.getLikeWithSameTime(newPhase);
    if (oldPhase == null)  {
      return null;
    } else {
      // copy stuff that depends on the solution (dist, rms, etc.)
      oldPhase.copySolutionDependentData(newPhase);
//      if (! oldPhase.getChannelObj().hasLatLonZ() ) {
//        System.out.println(" WARNING! LocationEngine phase missing LatLonZ for dist,az data: "+ oldPhase.toString());
//      }
      
      // Copy Phase description from the new phase data
      oldPhase.description.fm = newPhase.description.fm;
      oldPhase.description.ei = newPhase.description.ei;
      oldPhase.description.iphase = newPhase.description.iphase;
      oldPhase.description.setQuality(newPhase.getQuality());
      
      return oldPhase;
    }
  }

  /**
   * Code from LocationEngineHypoInverse.readLocationResults to setup a solution from the summary line in ARCOUT
   */
  private Solution createSolution(Solution currentSol, ArcSummary arcSummary, String arcHeader) {
    // id not formatted correctly patch for arc summary parse
    if (!currentSol.getId().isNull() && currentSol.getId().longValue() > 0 ) {
      arcSummary.evid = currentSol.getId().longValue();
    }
    System.out.println(arcSummary.getFormattedErrorEllipseString());
    System.out.println(ArcSummary.getTitle());
    System.out.println(arcSummary.getFormattedOriginString());

    DataString savedEventType = (DataString) currentSol.eventType.clone(); // aww change
    DataString savedGType = (DataString) currentSol.gtype.clone();
    currentSol.clearLocationAttributes(); // reset all solution dependent fields
    currentSol.eventType = savedEventType; // restore the event type
    currentSol.gtype = savedGType; // restore the origin gtype

    // read new solution dependent fields, method returns input
    // WARNING! Hypoformat method doesn't throw exception or
    // set any status after a bad parse so junk could be returned.
    currentSol = HypoFormat.parseArcSummary(currentSol, arcHeader);

    if (currentSol == null) {
      return null; // returns null solution for exception, otherwise the input currentSol
    } else {
      // set all the fields that HypoFormat doesn't have
      currentSol.getEventAuthority(); // resets the authority if region changed -aww 2011/09/21
      //
      // NOTE: regional eventAuthority or solution's origin authority could be different
      //if ( !currentSol.authority.equalsValue(currentSol.getEventAuthority()) ) // -aww 2011/08/17, removed 2011/09/21
      //    currentSol.authority.setValue(currentSol.getEventAuthority()); // -aww 2011/08/17, removed 2011/09/21
      if (!currentSol.authority.equalsValue(EnvironmentInfo.getNetworkCode())) // reverted back -aww 2011/09/21
        currentSol.authority.setValue(EnvironmentInfo.getNetworkCode());

      if (!currentSol.source.equalsValue(EnvironmentInfo.getApplicationName()))
        currentSol.source.setValue(EnvironmentInfo.getApplicationName());

      if (!currentSol.algorithm.equalsValue(getLocatorName()))
        currentSol.algorithm.setValue(getLocatorName());

      if (!currentSol.type.equalsValue(Solution.HYPOCENTER))
        currentSol.type.setValue(Solution.HYPOCENTER); // a Hypocenter

      // saved state effected by solCommitResetsRflag property which sets currentSol.commitResetsRflag -aww 2015/04/03
      if (currentSol.commitResetsRflag && !currentSol.processingState.equalsValue(EnvironmentInfo.getAutoString())) {
        currentSol.processingState.setValue(EnvironmentInfo.getAutoString()); //automatic or human?
      }

      if (!currentSol.validFlag.equalsValue(1)) {
        System.out.println("INFO: LocationEngine currentSol (deleted?) selectflag (validFlag) = 0");
        //currentSol.validFlag.setValue(1);  // valid? - remove 2014/10/30 why is it changing the "delete" status? -aww
      }

      // however could have phase parsing error try block exception below
      // or could have too few phases for valid solution
      if (!currentSol.dummyFlag.equalsValue(0))
        currentSol.dummyFlag.setValue(0); // - added 07/28/2006 aww

      if (!currentSol.horizDatum.equalsValue(LocationEngineHypoInverse.datumHorizontal))
        currentSol.horizDatum.setValue(LocationEngineHypoInverse.datumHorizontal);

      if (!currentSol.vertDatum.equalsValue(LocationEngineHypoInverse.datumVertical))
        currentSol.vertDatum.setValue(LocationEngineHypoInverse.datumVertical);

      //load phase list
      currentSol.loadPhaseList();

      return currentSol;
    }
  }

  /**
   * Create database connection
   */
  private Connection getDbConnection(JiggleProperties jp){
    // get current data source description from properties
    DbaseConnectionDescription dbDesc = jp.getDbaseDescription();
    new DataSource().set(dbDesc); // added 2008/05/14 -aww
    return DataSource.getConnection();
  }

  /**
   * Read Jiggle properties file
   */
  private JiggleProperties getProps(String propFileName) {
    JiggleProperties jp = new JiggleProperties();
    jp.setFilename(propFileName);
    jp.initialize("jiggle", propFileName, null);

    if (! jp.readPropertiesFile(propFileName)) {
      String str = "Error: unable to load command line properties file: " + propFileName;
      System.err.println(str);
      System.exit(1);
    } else {
      // success, so reconfigure properties object after read
      System.out.println("INFO: Loading properties from: " + propFileName);
      jp.setup();
    }
    setProperties(jp);
    return jp;
  }
}
