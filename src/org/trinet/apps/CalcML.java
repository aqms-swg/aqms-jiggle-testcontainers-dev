package org.trinet.apps;

import java.util.*;
import java.io.*;

import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.jdbc.datasources.DbaseConnectionDescription;
import org.trinet.jiggle.*;
import org.trinet.util.*;

/**
   Stand-alone program for calculating the ML of one event using only the peak
   amps that are already associated with this event in the data source. It will
   NOT rescan waveforms for amps.<p>

   Usage: CalcML <evid> or CalcML <@filename> <p>

   If @filename is used the event ID's must be the first token on each line.
*/
public class CalcML {

    static boolean debug = false;
    static boolean doBM = false;
    static boolean fileMode = false;
    static String filename = "";
    static ArrayList eventList = new ArrayList();
    static boolean testCommit = false; // set for testing

    public CalcML() { }


    public static void main(String args[]) {

        BenchMark bm = new BenchMark();

        int evid = 0;

        if (args.length <= 0) {
            System.out.println("Usage: CalcML <evid> or CalcML <@filename> followed by [<property-file>]");
            System.exit(0);
        }

        if (args.length > 0) {

            if (args[0].startsWith("@")) {
                fileMode = true;
                filename = args[0].substring(1);  // trim off the "@"
                
                eventList = readEventFile(filename);
            } else {
                fileMode = false;
                Integer val = Integer.valueOf(args[0]);
                eventList.add(val);
            }
            //evid = (int) val.intValue();
        }

        String propfile = "calcml.props";
        if (args.length > 1) {
            propfile = args[1];
        }        

        EnvironmentInfo.setApplicationName("CalcML");
        EnvironmentInfo.setNetworkCode("CI");
        // will mark data as "human" not automatic.
        EnvironmentInfo.setAutomatic(false);

        System.out.println("Making connection...");

        CalcMLProperties calcMlProp = new CalcMLProperties(propfile);        
        DbaseConnectionDescription dbaseDescr = calcMlProp.getDbaseDescription();
        if (!dbaseDescr.isValid()) {
            System.err.println("Property file does not contain a valid data source description.");
            System.err.println(dbaseDescr.toString());
            calcMlProp.dumpProperties();
            System.exit(1);
        }

        DataSource ds = null;
        int status = 0;
        try {
            ds = new DataSource();
            // make connection
            ds.set(dbaseDescr);


            for (int i = 0; i < eventList.size(); i++) {

                evid = (int) ((Integer) eventList.get(i)).intValue();

                if (debug) System.out.println("Processing: " + evid);

                if (debug) System.out.println("Making MasterView for evid = " + evid);

                // Make the "superset" MasterView
                MasterView mv = new MasterView();

                // no wf's used
                mv.setWaveformLoadMode(MasterView.LoadNone);

                //mv.setTimeAlign(true);
                if (doBM) {
                    bm.print("BenchMark: startup done ");
                    bm.reset();
                }
                mv.defineByDataSource(evid);

                if (doBM) {
                    bm.print("BenchMark: event parameters loaded ");
                    bm.reset();
                }

                // get the first solution in the list
                //Solution sol = (Solution) mv.solList.solList.get(0);
                Solution sol = null;
                if (mv.solList.size() > 0) {
                    sol = (Solution) mv.solList.get(0);
                    System.out.println("SOL: " + sol.toString());
                    System.out.println("There are " + mv.getPhaseCount() +
                            " phases, " + mv.getWFViewCount() + " time series" +
                            " and " + mv.getAmpCount() + " amps");
                } else {
                    System.out.println("No dbase entry found for evid = " + evid);
                    System.exit(0);
                }

                // load channel info
                if (sol.waveformList.size() > 100 && MasterChannelList.isEmpty()) {
                    if (debug) System.out.println("Loading station info...");
                    MasterChannelList.set(ChannelList.readCurrentList());
                    bm.print("BenchMark: MasterChannelList loaded ");
                    bm.reset();
                }

                /****************
                 * Use CreateMagnitudeMethod() instead of SoCalML constructor.
                 * Configuration Params are hardcoded in SoCalML.setDefaultParams()
                 * Call ConfigureMagnitudeMethod() to setup method and default params.
                 // ML
                 SoCalML ml = new SoCalML();

                 ml.setTrimResidual(1.0);
                 ml.setMinSNR(8.0);    //per Kate Hutton 4/2002
                 ml.setRequireCorrection(true);
                 **********************************/

                MagnitudeMethodIF ml = AbstractMagnitudeMethod.create("org.trinet.jasi.magmethods.TN.SoCalMlMagMethod");
                ml.configure();

                MagnitudeEngineIF magEng = AbstractMagnitudeEngine.create();
                magEng.setMagMethod(ml);

                Magnitude newMag = (magEng.solve(sol)) ? magEng.getSummaryMagnitude() : null;

                if (newMag != null && magEng.success()) sol.setPreferredMagnitude(newMag);

                // dump result
                System.out.println("Amps in sol = " + sol.ampList.getAssociatedWith(sol).size());
                System.out.println("Amps in mag = " + sol.magnitude.ampList.size());
                System.out.println("Total used  = " + sol.magnitude.getReadingsUsed());

                System.out.println(sol.ampList.toNeatString());
                System.out.println(sol.magnitude.neatDump());

                if (doBM) bm.print("BenchMark: ");
                System.out.println("Total time = " + bm.getSeconds() + " sec");

                // You must commit the sol not just the mag, so that prefmag etc. gets set.

                if (testCommit) { // if testing
                    try {
                        sol.commit();
                    } catch (JasiCommitException ex) {
                    }
                }

            } // end of loop
        } catch (Exception ex) {
                ex.printStackTrace();
                status = 1;
        } finally {
            if (ds != null) {
                ds.close();
            }
        }

        System.exit(status);
    } // end of main
      
    /** * Read in list of evids */
    static private ArrayList readEventFile(String filename) {
        ArrayList list = new ArrayList();
        String instr;
        String idstr;

        try {
          // Open the file
          //FileInputStream fstream = new FileInputStream(filename);
          FileReader fr = new FileReader(filename);
          BufferedReader buff = new BufferedReader(fr);
      
          // Continue to read lines while there are still some left to read
          while ((instr = buff.readLine()) != null) {
              StringTokenizer tok = new StringTokenizer(instr);
               idstr = tok.nextToken(); // 1st token
               list.add(Integer.valueOf(idstr));
               if (debug) System.out.println("/"+idstr+"/");
          }
      
          fr.close();
        }
        catch (Exception e) {
            System.err.println("File input error");
        }
        return list;
    }
      
} // end of class
