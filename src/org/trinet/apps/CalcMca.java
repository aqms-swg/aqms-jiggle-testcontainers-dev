package org.trinet.apps;
import java.util.*;
import org.trinet.jiggle.*;
import org.trinet.util.WaveClient;
import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.jasi.coda.*;
import org.trinet.util.*;

/**
   Stand-alone program for calculating the Mc of one event.
*/
public class CalcMca {

    static boolean debug = true;
    static boolean useWaveserver = false;

    public CalcMca() { }

    public static void main(String args[]) {

        BenchMark bm = new BenchMark();

          int evid = 0;

        if (args.length <= 0) {
            System.out.println("Usage: CalcMca <evid> ");
            System.exit(0);
        }

        if (args.length > 0) {
          Integer val = Integer.valueOf(args[0]);
          evid = (int) val.intValue();
        }

        System.out.println("Making connection...");
        DataSource init = TestDataSource.create(); // make connection
        init.setWriteBackEnabled(true);

        WaveClient waveClient = null;

        if (useWaveserver) {
            // cheating
            String propFile = "/tpp/www/waveserver.cfg";

            try { // Make a WaveClient
                 System.out.println("Creating WaveClient using: "+propFile);
                 waveClient = WaveClient.createWaveClient().configureWaveClient(propFile); // property file name
                 //System.out.println(waveClient.toString());

                 int nservers = waveClient.numberOfServers();
                 if (nservers <= 0) {
                     System.err.println("getDataFromWaveServer Error:"+
                        " no data servers specified in input file: " +
                        propFile);
                     System.exit(-1);
                 }
            } catch (Exception ex) {
                System.err.println(ex.toString());
                ex.printStackTrace();
            }
            finally {
                //if (waveClient != null) waveClient.close();
            }
            AbstractWaveform.setWaveDataSource(waveClient);
        }

        if (debug) System.out.println("Making MasterView for evid = "+evid);

        // Make the "superset" MasterView
        MasterView mv = new MasterView();

        // wave scanner will load them
        mv.setWaveformLoadMode(MasterView.LoadNone);

        //mv.setTimeAlign(true);
        bm.print("BenchMark: startup done ");
        bm.reset();

        mv.defineByDataSource(evid);

        bm.print("BenchMark: event parameters loaded ");
        bm.reset();

        // get the first solution in the list
        //Solution sol = (Solution) mv.solList.solList.get(0);
         Solution sol = null;
        if (mv.solList.size() > 0) {
              sol = (Solution) mv.solList.get(0);

              System.out.println("SOL: "+sol.toString());
              System.out.println("There are " + mv.getPhaseCount() +
                   " phases, "+ mv.getWFViewCount() + " time series"+
                   " and "+mv.getAmpCount() + " amps");
        } else {
              System.out.println("No dbase entry found for evid = "+evid);
              System.exit(0);
        }

        // load channel info
 /*
        if (sol.waveformList.size() > 100) {
          System.out.println("Reading in channel list...");
          MasterChannelList.set(ChannelList.smartLoad());
          //MasterChannelList.set(ChannelList.readCurrentList());
          bm.print("BenchMark: MasterChannelList loaded ");
          bm.reset();
       }
 */
       System.out.println(" Channel list "+MasterChannelList.get().size());

       // Mca
       String propfile = "MCA.properties";

       System.out.println(sol.fullDump());


       MagnitudeEngineIF magEng = AbstractMagnitudeEngine.create();
       magEng.configure(ConfigurationSourceIF.CONFIG_SRC_FILE,
                                     propfile, null,
                                     MasterChannelList.get()
                                    );

       System.out.println(sol.fullDump());


       Magnitude newMag = (magEng.solveFromWaveforms(sol, (List)mv.getWaveformList())) ?
                                magEng.getSummaryMagnitude() : null;

       if (newMag != null && magEng.success()) sol.magnitude = newMag;

       // dump result
       System.out.println(sol.magnitude.neatDump());
       bm.print("BenchMark: ");
       System.out.println("Avg. time per reading = "+
                        bm.getSeconds()/mv.wfvList.size() + " sec");

       sol.getPreferredMagnitude().getReadingList().clear();

       // recalc with existing codas
       newMag = (magEng.solve(sol)) ? magEng.getSummaryMagnitude() : null;
       if (newMag != null && magEng.success()) sol.magnitude = newMag;

       // dump result
       System.out.println(sol.magnitude.neatDump());
       bm.print("BenchMark: ");
       System.out.println("Avg. time per reading = "+
                        bm.getSeconds()/mv.wfvList.size() + " sec");

       // sol.magnitude.commit();

    } // end of main

} // end of class
