package org.trinet.apps;

import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jiggle.JiggleProperties;
import org.trinet.util.DateTime;

public class ChannelTimeWindowModelTester {

    public static void main(String args[]) {

      int nargs = args.length;

      if (nargs <= 0) {
        System.out.println ("ChannelTimeWindowModelTester {evid} [propFileName D=user_dir/jiggle.props] [useChannelCacheForCandidateList D=false] [dumpWindows]");
        System.exit(0);
      }

      // 1st arg event id e.g. CI evid = 10100169; // 3.2
      long evid = Long.valueOf(args[0]).longValue();

      // 2nd arg property file name 
      String propFile = (nargs > 1) ? args[1] : "jiggle.props";
      JiggleProperties props = new JiggleProperties(propFile);
      props.setupDataSource();

      Solution sol = Solution.create().getById(evid);

      if (sol == null) {
        System.out.println("No such event: " + evid);
        System.exit(0);
      }
      System.out.println (sol.toString());


      // Get a model setup with the args in the properties file
      // 4th arg model name (optional) 
      ChannelTimeWindowModel model = props.getCurrentChannelTimeWindowModel();
      if (model == null) {
          System.err.println("ERROR: currentChannelTimeWindowModel not defined in input properties.");
          System.exit(1);
      }
      model.setProperties(props);

      // Do we use set candidate list from cache?
      boolean useCache = (nargs > 2) ? Boolean.valueOf(args[2]).booleanValue() : false; 
      if (useCache) {
        System.out.println("Start loading candidate list :" + new DateTime().toString());
        System.out.println("Loading candidate list from : " + props.getUserFileNameFromProperty("channelCacheFilename") );
   // NOTE : have to set MasterList list here since included readings and channelable lists use it to synch db lookup info like LatLonz
        MasterChannelList.set(ChannelList.readFromCache( props.getUserFileNameFromProperty("channelCacheFilename")));
        model.setCandidateList(MasterChannelList.get());
        System.out.println("Done loading candidate list :" + new DateTime().toString());
      }
      model.setSolution(sol); // set candidate list before setSolution (which has side effect of creating default list)

      // Get size of the model's channel time window list
      System.out.println("Now getting channel time windows...");
      ArrayList ctwList = (ArrayList) model.getChannelTimeWindowList();
      int count = (ctwList == null) ? 0 : ctwList.size();
      System.out.println("Channel count = " + count);
      boolean listWindows = (nargs > 3) ? Boolean.valueOf(args[3]).booleanValue() : false; 
      if (count > 0 && listWindows) { // Do we list time windows?
        for (int i = 0; i < count; i++) {
            System.out.println( ((ChannelTimeWindow) ctwList.get(i)).toString() );
          }
        if (count > 25) System.out.println("Channel count = " + ( (ctwList == null) ? "null" : String.valueOf(ctwList.size()) ) );
      }

    }
}
