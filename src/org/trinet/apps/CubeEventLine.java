package org.trinet.apps;

import org.trinet.jasi.*;
import org.trinet.formats.*;
import org.trinet.jdbc.datasources.AbstractSQLDataSource;
/*
Produce Cube "E " formated string for only seismic events.
Fixed format by column.
If CUBE "E " fields contain null values, 'white' space is inserted.
*/

public class CubeEventLine {

    public CubeEventLine() { }

    public static void main(String[] args) {

      if (args.length < 5) {
        System.out.println("Args: [user] [pwd] [host.domain] [dbName] [evid] <E, DE, cancelmail, or eventmail) default = E>");
         System.exit(0);
      }

      String user   = args[0];
      String passwd = args[1];
      String host = args[2];        // default.
      String dbname = args[3];

      long evid = Long.parseLong(args[4]);

      String url= "jdbc:" + AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL +":@"+ host +":"+ AbstractSQLDataSource.DEFAULT_DS_PORT +":"+ dbname;
      String driver = AbstractSQLDataSource.DEFAULT_DS_DRIVER;
      System.out.println("CubeEventLine: making connection... ");
      DataSource ds = new DataSource (url, driver, user, passwd);    // make connection
      System.out.println("CubeEventLine dataSource : " + ds.describeConnection());

      Solution sol = Solution.create().getById(evid);

      if (sol == null) {
          System.out.println("No event found with evid = "+evid);
      } else {
          // Prints out cube format string.
          if (args.length > 5) {
              if (args[5].equalsIgnoreCase("eventmail"))
                  System.out.println(CubeFormat.toEmailString(sol, null, ds.getConnection())); //null = version
              else if (args[5].equalsIgnoreCase("cancelmail"))
                  System.out.println(CubeFormat.toCancelEmailString(sol, null, ds.getConnection())); //null = version
              else if (args[5].equalsIgnoreCase("E"))
                  System.out.println(CubeFormat.toEventString(sol));
              else if (args[5].equalsIgnoreCase("DE"))
                  System.out.println(CubeFormat.toDeleteString(sol));
              else 
                  System.out.println("Unknown output type input option='"+ args[5] + "', value must be 'E', 'DE', 'cancelmail', or 'eventmail'");
          }
          else System.out.println(CubeFormat.toEventString(sol));
      }
    } // end of main

} // End of class

