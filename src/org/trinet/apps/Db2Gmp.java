package org.trinet.apps;

import java.io.*;
import java.util.Collection;

import org.trinet.formats.GroMoPacket;
import org.trinet.util.BenchMark;
import org.trinet.jasi.*;
import org.trinet.jdbc.datasources.*;

/**
 * Write files for amplitudes for a given event.
 * For the given event ID number do the following:<p>
 * o Extract all amps from the dbase<br>
 * o Filter out all but those with a netcode defined in the 'netcodeList' property<br>
 * o Group them by channel (SNCL) and compose a file in EW2 format<br>
 * o Write them to a file in 'sourceDir' with a name of the form: [evid].[SCNL].[suffix] where suffix is the
 *   first token in the 'suffixList' property.<br>
 * o Each file must apppear in the destination dir "atomically", otherwise the
 *   SendfileII process that exports the file will send partial or temp files.
 *   Therefore, we must write a temp file to a temp dir then MOVE the file to the
 *   final destination. Even COPY can be problematic to Sendfile.
 */

public class Db2Gmp extends GMPEngine implements AutoCloseable {

  private String ampGetByTable = "ampset";

  public Db2Gmp() {
    appName = "Db2Gmp";
  }

  public Db2Gmp(int type) {
    this();
    GroMoPacket.setFormatType(type);
    setProperties();
  }

  @Override
  public void close() {
      DataSource.close();
  }

  /** Set program behavior variables using parameters from properties file.
   Returns 'false' if any of the required properties are invalid. Non-required
   properties that are not set in the properties file remain at the default value.
   Note that not all properties must be specified for all child classes so only
   required properties are checked. */
  public boolean setProperties() {

    if (props.isSpecified("tempSubDir"))
        setTempSubDir(props.getProperty("tempSubDir"));

    if (getTempSubDir() == null) {
      dumpError("Error 'tempSubDir' is not specified. ");
      return false;
    }

    ampGetByTable = props.getProperty("ampGetByTable", "ampset");

    return super.setProperties();
  }

/*
//Slow query, only works for case where amp SNCL and amptype are specified in condition,
//but does work for obtaining complete set of amps, see analytic function below:
select t.* from (select a.* from amp a, ampset b, assocevampset c where
      a.ampid=b.ampid and b.ampsetid=c.ampsetid and c.ampsettype=? and c.subsource=? and c.isvalid=1
      and a.net=? and a.sta=? and a.seedchan=? and a.location=? and a.amptype=?
  order by a.net,a.sta,a.seedchan,a.location,a.lddate desc
) t where rownum < 2

// Next 2 queries are about the same speed, 1st uses the row_number function each row# is unique but order nondeterministic when lddates are same
select t.* from (select a.*, row_number() over (partition by a.net,a.sta,a.seedchan,a.location,a.amptype order by a.lddate desc) arow
    from amp a, ampset b, assocevampset c where
      a.ampid=b.ampid and b.ampsetid=c.ampsetid and c.ampsettype=? and c.subsource=? and c.isvalid=1
     -- and a.net=? and a.sta=? and a.seedchan=? and a.location=?
) t where t.arow=1

// 2nd uses dense_rank() or rank() partition function, returns multiple rows with same rank# when lddates are the same DON'T USE THIS
select t.* from (select a.*, rank() over (partition by a.net,a.sta,a.seedchan,a.location,a.amptype order by a.lddate desc) arank
  from amp a, ampset b, assocevampset c where
  a.ampid=b.ampid and b.ampsetid=c.ampsetid and c.ampsettype=? and c.subsource=? and c.isvalid=1
  -- and a.net=? and a.sta=? and a.seedchan=? and a.location=?
) t where t.arank=1
        
// select a.*  from amp a, ampset b, assocevampset c where
// a.ampid=b.ampid and b.ampsetid=c.ampsetid and c.ampsettype=? and c.subsource=? and c.isvalid=1 
// and a.net=? and a.sta=? and a.seedchan=? and a.location=? 
// order by a.net,a.sta,a.seedchan,a.location,a.lddate desc 

*/

/** Return the amplist of amps for this event that match the net codes defined
 in the 'netcodeList' property. */
  public AmpList getAmps(long evid) {
   /*
    // Only enable if both RT and PostProc code all use the assocevampset
    String selectSmAmpsStr =
      "SELECT t.* FROM ( SELECT a.AMPID,TRUETIME.getEpoch(a.DATETIME,'UTC'),a.STA,a.NET,a.AUTH,a.SUBSOURCE,a.CHANNEL,a.CHANNELSRC," +
           "a.SEEDCHAN,a.LOCATION,a.AMPLITUDE,a.AMPTYPE,a.UNITS,a.AMPMEAS,a.PER,a.SNR,a.QUALITY,a.RFLAG,a.CFLAG," +
           "TRUETIME.getEpoch(a.WSTART,'UTC'),a.DURATION," +
           "ROW_NUMBER() OVER (PARTITION BY a.net,a.sta,a.seedchan,a.location,a.amptype ORDER BY a.lddate DESC) rnum" +
           " FROM amp a, ampset s, assocevampset ss WHERE" +
           " (a.ampid=s.ampid) AND (s.ampsetid=ss.ampsetid) AND (ss.ampsettype='sm') AND (ss.isvalid=1) AND (ss.evid=" +evid+ ") ) t WHERE t.rnum=1)"
    */


    Solution aSol = (Solution) Solution.create().getById(evid);
    if (aSol == null) return new AmpList(0);

    Amplitude amp = Amplitude.create();
    // Get amps associated with a event's prefor and only the amptypes you want
    //Collection c = amp.getBySolution(aSol, getAmpTypeList(), false);
    //Collection c = amp.getByValidAmpSet(evid, "sm", "ampgen%");
    Collection c = (ampGetByTable.equalsIgnoreCase("assocamo")) ? 
        amp.getBySolution(aSol, getAmpTypeList(), false) : amp.getByValidAmpSet(evid, "sm", null);

    if (c == null || c.size() == 0) return new AmpList(0);

    AmpList amplist = AmpList.create();
    amplist.fastAddAll(c);
    amplist.assignAllTo(aSol);

    amplist = cullAmpsByNet(amplist);   // only from nets we want
    
    amplist = cullAmpsByAuth(amplist);   // only from auth we want - added 7/2006 DDG - we were not sending amps for NP stations that we get in real-time

    amplist = cullAmpsBySubsource(amplist); // only from subsources we want -aww 2009/05/08
    
    amplist = cullAmpsByQuality(amplist);   // only those ONSCALE and quality > 0. -aww 2010/02/10, enabled 2010/03/10

    //amplist = cullAmpsByType(amplist); // NOT needed here since requested from db  are valid types we want (generally all). -aww 2010/12/20

    return amplist;
  }

  /**  The main workhorse method.
   * Given and AmpList containing many stations, write one file per station.
   * Each file name has the form: [evid].[SCNL].[suffix] where the suffix is the
   * first suffix in the property 'suffixList'.
   * Files are written to the sourceDir.
   * Returns a count or the number of files written. */

    private int writeFiles(long evid, AmpList amplist) {

        AmpList ampgrp, newList;
        String datastr;
        int knt = 0;

        GroMoPacket.setNetCode(getLocalNetCode());

        // ChannelGrouper will sort alphabetically and by component and hand us
        // all the amps for a single channel.
        // This is necessary because the format lumps many amps from a single channel together
        //ChannelGrouper changrp = new ChannelGrouper(amplist);
        StationGrouper grper = new StationGrouper(amplist);

        boolean writeBothFormats = props.getBoolean("writeBothFileFormats"); 

        while (grper.hasMoreGroups()) {
          ampgrp = (AmpList) grper.getNext();       // one station's worth of amps
          newList = GroMoPacket.cullAmplist(ampgrp);  // only use amps of the correct type
          if (!newList.isEmpty()) {
              if (writeBothFormats) {

                  int formatType = GroMoPacket.getFormatType();

                  GroMoPacket.setFormatType(GroMoPacket.TYPE_EW2);
                  datastr = GroMoPacket.format(evid, newList);
                  if ( dumpToFiles( datastr, evid, newList, ".sm"+getLocalNetCode()) )  knt++;

                  GroMoPacket.setFormatType(GroMoPacket.TYPE_GMXY);
                  datastr = GroMoPacket.formatXY(evid, eventAuth, newList);
                  //if ( dumpToFiles( datastr, evid, newList, ".gm"+getLocalNetCode()) )  knt++; // Lombard says format should be 3-char 2011/05/18
                  if ( dumpToFiles( datastr, evid, newList, ".g"+getLocalNetCode()) )  knt++; // new file suffix -aww 2011/05/18

                  GroMoPacket.setFormatType(formatType);

              }
              else {
                  datastr = (GroMoPacket.getFormatType() == GroMoPacket.TYPE_GMXY) ?
                        GroMoPacket.formatXY(evid, eventAuth, newList) : GroMoPacket.format(evid, newList);
                  if ( dumpToFiles( datastr, evid, newList, null) )  knt++;
              }
          }
        }

        return knt;
    }

  /** Write the string to the files defined by property 'destinationDirs'.
    If there is some problem with the files 'false' is returned. */
    private boolean dumpToFiles(String str, long evid, AmpList amplist, String suffix) {

      boolean status = true;
      // This create File objects but doesn't actually create the files on disk
      File[] file = createDestinationFiles(evid, amplist, suffix);

      if (file == null || file.length == 0) return false;

      for (int i=0;i<file.length;i++) {
        status &= dumpToFile(file[i], str);
      }
      return status;
    }
    /** Write the string to the filename.  The 'filename' should include a complete path.
    If it does not the default directory where the file will be created will be platform dependent.
    This method insures that the file appears "atomically"
    so that a process reacting to the appearence of the file will not start to work
    on it while still being written.
    If there is some problem with the file it will we moved to 'badDir'.*/
    private boolean dumpToFile(String filename, String str) {
       return dumpToFile(new File(filename), str);
    }

    /** Write the string to the file. This method
     insures that the file appears "atomically" by writing to a temporary filename
     and then renaming it when done. This guarentees that a process reacting to the
     appearence of the file will not start to work on it while its still being written.
     */
    private boolean dumpToFile(File file, String str) {
      // Expects ./temp.dir to exist in the destination dir
      File tmpFile = createTempFile(file);
      try (FileWriter fw = new FileWriter(tmpFile)) {
        PrintWriter pw = new PrintWriter(fw);
        pw.print(str);
      }
      catch (IOException ex) {
        dumpError(ex.getMessage());
        dumpError("Error creating: "+tmpFile.getName());
        if (dispatchFile(tmpFile, false)) {  //move from 'sourceDir' to 'errorDir'
          return false;
        } else {               // if the move fails just delete it.
          tmpFile.delete();
          return false;
        }
      }
      // rename from tmp to actual, overwrite old version if necessary
      return renameFile(tmpFile, file, true);
    }

    /*
    protected boolean setupDbConnection() {

        DbaseConnectionDescription dbdesc = props.getDbaseDescription();

        if (dbdesc == null || !dbdesc.isValid()) {
          dumpError("Error creating database description - check properties file");
          dumpError(dbdesc.toString());
          return false;
        }

        //
        //System.out.println("Attempting connection to: "+dbdesc.toString());
        //DataSource datasource = new DataSource(dbdesc);   // sets Static DataSource

        ds = new DataSource();
        ds.set(dbdesc); // aww 2008/06/26
        if (ds.getConnection() == null) {
          dumpError("Error creating database connection - check properties file");
          return false;
        } else {
          //System.out.println("Connection successful.");
        }

        return true;
    }
    */

// ////////////////////////////////////////////////////////////////////////////
  public static void main(String[] args) {

    boolean debug = false;

    String propFilename = "export.props";

    long evid = 0l; 
    if (args.length < 2)	// not enough args
    {
      dumpError("Usage: java Db2Gmp [prop-filename] [evid]");
      if (!debug) System.exit(0);

    } else {

      propFilename = args[0];	    // 1st arg

      try {
        evid = Long.parseLong(args[1]);  // 2nd arg convert
      }
      catch (NumberFormatException ex) {
        dumpError("Malformed argument list.");
        dumpError("Usage: java Db2Gmp [prop-filename] [evid]");
        if (!debug) System.exit(1);
      }
    }

    try  (Db2Gmp db2gmp = new Db2Gmp()) {
        if (!db2gmp.setPropertiesFile(propFilename)) {
            dumpError("Exiting - bad properties file: "+propFilename);
            dumpError(" === Dumping properties ===\n");
            // NOTE configure method dumps too if "dumpProperties=true"
            if (db2gmp.props != null) db2gmp.props.list(System.out);
            dumpError(" === End of properties dump ===\n");
            System.exit(2);
          }
          // Dump here to see appName:
          EnvironmentInfo.dump();

          /* create dbase connection
          if (!db2gmp.setupDbConnection()) {
            System.exit(-1);
          }
          */

          db2gmp.processSolution(evid);
    }
  }

  public boolean processSolution(long evid) {

    BenchMark bm = new BenchMark();

    int knt = 0;

    if (checkEvent(evid)) { // Check the event -- process it if good

      // get the amps...
      AmpList amplist = getAmps(evid);
      if (debug) {
          dump("DEBUG amplist dump of size: " + amplist.size());
          amplist.dump();
      }
      if (amplist.size() == 0) {
          bm.printTimeStamp("Event "+evid+": no desired amps found,");
          return true;
      }

      // write the files...
      knt = writeFiles(evid, amplist);
      bm.printTimeStamp("Event "+evid+": wrote "+knt+" files,");

      return (knt > 0) ? true : false;

    }
    else {
      bm.printTimeStamp("Event "+evid+": rejected by check filter, no files written,");
      return true;
    }

  }

  public void setDebug(boolean tf) {
      debug = tf;
      GroMoPacket.debug = tf;
  }
}
