package org.trinet.apps;

import org.trinet.jiggle.*;
import java.awt.*;

import org.trinet.jasi.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.Dimension;

/**
 * EventViewer.java
 *
 *
 * Created: Tue Nov  2 14:01:02 1999
 *
 * @author Doug Given
 * @version
 */

public class EventDump {

    //    static boolean debug = true;
    static boolean debug = false;

    //    static boolean useWaveserver = true;
        static boolean useWaveserver = false;

    public EventDump() {

    }


    public static void main (String args[])
    {
      int evid = 9135516;
      String host = "serverma";  // default

      if (args.length < 1)        // no args
      {
        System.out.println
        ("Usage: EventDump <evid> [<dbase-host>]");
        //System.exit(0);

      } else {

        Integer val = Integer.valueOf(args[0]);
        evid = (int) val.intValue();
      }

      if (args.length > 1) {
        host = args[0];
      }

      System.out.println ("Making connection...");
      TestDataSource.create(host);  // make connection
      DataSource.setWriteBackEnabled(true);

      if (debug) System.out.println ("Making MasterView for evid = "+evid);

      // Make the "superset" MasterView
      MasterView mv = new MasterView();

      mv.setWaveformLoadMode(MasterView.LoadNone);
      mv.defineByDataSource(evid);

      // get the first solution in the list
      //        Solution sol = (Solution) mv.solList.solList.get(0);
      Solution sol = mv.getSelectedSolution();

      if (sol == null) {
        System.out.println ("No dbase entry found for evid = "+evid);
        System.exit(0);
      }

      System.out.println (sol.fullDump());

    }

} // EventViewer
