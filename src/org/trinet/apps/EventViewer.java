package org.trinet.apps;

import java.awt.*;
import java.awt.event.*;

import java.awt.Dimension;
import javax.swing.*;

import org.trinet.jasi.*;
import org.trinet.jiggle.*;
import org.trinet.util.WaveClient;
import org.trinet.util.graphics.IconImage;

/**
 * EventViewer.java
 * GUI for interactive viewing of waveforms and parameters for a single event.
 * @author Doug Given
 */
public class EventViewer {

  static boolean debug = true;
  //static boolean debug = false;
  //static boolean useWaveserver = true;
  static boolean useWaveserver = false;

  public EventViewer() { }

  public static void main(String args[]) {
      // event 13307456 is a good test because it has no WF in 1st panel
      // 3113276 is an event from 1993
      // event 13692644 has PASA with Steim2 compression
  
      int evid =  14131264;
      int channelsPerPage = 10;
  
      if (args.length <= 0) {
        System.out.println("Usage: EventViewer <evid> [<channels/page>] (default= "+ channelsPerPage+")");
        System.exit(0);
      }
  
      if (args.length > 0) {
        Integer val = Integer.valueOf(args[0]);
        evid = (int) val.intValue();
      }
  
      if (args.length > 1) {
        Integer val = Integer.valueOf(args[1]);
        channelsPerPage = (int) val.intValue();
  
        if (channelsPerPage < 0 || channelsPerPage > 100) {
          System.out.println (" ! channels/page must be between 0 & 100.");
          //debug// System.exit(0);
        }
      }
  
      String propertyFileName = "properties";  // default name
      JiggleProperties props = null;
      if (args.length > 2) propertyFileName = args[2]; // aww override default name
  
      props = new JiggleProperties(propertyFileName);
  
      // set from properties file
      System.out.println ("Making connection...");
      new DataSource();
      DataSource.set(props.getDbaseDescription());  // make connection
      System.err.println(props.getDbaseDescription());
  
      if (useWaveserver) { // read props file to get waveserver filename
          try {
            // Make a WaveClient
            WaveServerGroup waveClient = props.getWaveServerGroup();
  
            if (waveClient == null || waveClient.numberOfServers() <= 0) {
              System.err.println(
                  "getDataFromWaveServer Error: no or bad waveservers in properties file. " );
              System.err.println("waveServerGroupList = " + props.getProperty("waveServerGroupList"));
              System.exit(-1);
            }
            AbstractWaveform.setWaveDataSource(waveClient);
          } catch (Exception ex) {
            System.err.println(ex.toString());
            ex.printStackTrace();
         }
      }
  
  
      if (debug) System.out.println ("Making MasterView for evid = "+evid);
  
      // Make the "superset" MasterView
      MasterView mv = new MasterView();
  
      mv.setWaveformLoadMode(MasterView.Cache);
      //mv.setWaveformLoadMode(MasterView.LoadAllInForeground);
  
      int above = 50;
      int below = 50;
      mv.setCacheSize (above, below);
  
      mv.setAlignmentMode(MasterView.AlignOnTime);
  
      mv.setChannelTimeWindowModel(props.getCurrentChannelTimeWindowModelInstance());
      mv.defineByCurrentModel(evid);
  
      // get the first solution in the list
      //        Solution sol = (Solution) mv.solList.solList.get(0);
      Solution sol = null;
      if (mv.solList.size() > 0) {
        sol = (Solution) mv.solList.get(0);
  
        System.out.println("SOL: "+sol.toString());
        System.out.println("There are " + mv.getPhaseCount() +
                            " phases, "+ mv.getWFViewCount() + " time series"+
                            " and "+mv.getAmpCount() + " amps");
      } else {
        System.out.println("No dbase entry found for evid = "+evid);
        System.exit(0);
      }

      // Make graphics components
      if (debug) System.out.println("Creating GUI...");
  
      int height = 900;
      int width  = 640;
  
      //VirtScroller wfScroller = new VirtScroller(mv, channelsPerPage, true);
      final WFScroller wfScroller = new WFScroller(mv, true) ;            // make scroller
  
      PickingPanel zpanel = new PickingPanel(mv);
  
      // enable filtering, default will be Butterworth. See: ZoomPanel
      // Use zpanel.zwfp.setFilter(FilterIF) to change the filter
      zpanel.setFilterEnabled(true);
  
      // Retain previously selected WFPanel if there is one,
      // if none default to the first WFPanel in the list
      WFView wfvSel = mv.masterWFViewModel.get();
      // none selected, use the 1st one in the scroller
      if (wfvSel == null && mv.getWFViewCount() > 0) {
        wfvSel = (WFView) mv.wfvList.get(0);
  
        //////
        wfvSel.wf = null;     // crash
        //((WFView) mv.wfvList.get(3)).wf = null;  // no crash
        //////
      }
  
      // Must reset selected WFPanel because PickingPanel and WFScroller are
      // new and must be notified (via listeners) of the selected WFPanel.  It might
      // be null if no data is loaded.
      if (wfvSel != null ) {
        ((ActiveWFPanel)wfScroller.groupPanel.getWFPanel(wfvSel)).setSelected(true);
        mv.masterWFViewModel.set(wfvSel);
        mv.masterWFWindowModel.setFullView(wfvSel.getWaveform());
      }
  
      zpanel.setMinimumSize(new Dimension(300, 100) );
      wfScroller.setMinimumSize(new Dimension(300, 100) );
  
      ///////////    TEST //////////////////
      wfScroller.setSecondsInViewport(60.0);
  
      // make a split pane with the zpanel and wfScroller
      JSplitPane split =
          new JSplitPane(JSplitPane.VERTICAL_SPLIT,
          false,       // don't repaint until resizing is done
          zpanel,      // top component
          wfScroller); // bottom component
  
      /*  Couldn't get this to work!
      RepaintManager repaintManager = RepaintManager.currentManager(split);
      repaintManager.setDoubleBufferingEnabled(false);
      //split.setDebugGraphicsOptions(DebugGraphics.LOG_OPTION );
      split.setDebugGraphicsOptions(DebugGraphics.FLASH_OPTION );
      split.setDebugGraphicsOptions(DebugGraphics.BUFFERED_OPTION );
      //        DebugGraphics.setFlashTime(5);
      */
          //debugOptions - determines how the component should display the information; one of the following options:
          /*
          * DebugGraphics.LOG_OPTION - causes a text message to be printed.
          * DebugGraphics.FLASH_OPTION - causes the drawing to flash several times.
          * DebugGraphics.BUFFERED_OPTION - creates an ExternalWindow that displays the operations performed on the View's offscreen buffer.
          * DebugGraphics.NONE_OPTION disables debugging.
          * A value of 0 causes no changes to the debugging options.  debugOptions is bitwise OR'd into the current value
          */
      //
      // make a main frame
      //JFrame frame = new JFrame("SnapShot of "+evid);
      JFrame frame = new JFrame(sol.toSummaryString());
  
      String fileName = "JiggleLogo.gif";
      IconImage.setDebug(true);
      Image image = IconImage.getImage(fileName);
      frame.setIconImage(image);
  
      frame.addWindowListener(new WindowAdapter() {
        public void windowClosing(WindowEvent e) {System.exit(0);}
      });
      frame.getContentPane().add(split, BorderLayout.CENTER);            // add splitPane to frame
  
      // Add the channelname finder
      JPanel finderPanel = new JPanel();
      finderPanel.add(new JLabel("Find: "));
      finderPanel.add(new ChannelFinderTextField(mv));
      frame.getContentPane().add(finderPanel, BorderLayout.SOUTH);
  
      //SelectablePhaseList list = new SelectablePhaseList(mv);
      //frame.getContentPane().add(list, BorderLayout.SOUTH);
  
      // ///////////////////////////////
      frame.pack();
      frame.setVisible(true);
  
      frame.setSize(width, height);        // must be done AFTER setVisible
  
      // put divider at 25/75% position
      split.setOneTouchExpandable(true);
      split.setDividerLocation(0.25);
  
      //debug
  
      System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++");
      System.out.println("WFView count        = "+ mv.getWFViewCount());
      System.out.println("masterWFPanelModel  = "+ mv.masterWFViewModel.countListeners());
      System.out.println("masterWFWindowModel = "+ mv.masterWFWindowModel.countListeners());
      System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++");
  
  }
} // EventViewer
