package org.trinet.apps;

import java.awt.EventQueue;
import java.io.*;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.SQLException;

import org.trinet.formats.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.datasources.*;
import org.trinet.jdbc.table.SeqIds;
import org.trinet.storedprocs.association.AmpAssocEngine;
import org.trinet.storedprocs.association.AmpAssoc;
import org.trinet.util.*;

/**
 *
 * Process import files containing Ground Motion Data Message Format amplitude
 * data and insert the resulting Amplitudes into the database.<p>
 *
 * SYNTAX: Gmp2Db [properties-filename] <p>
 *
 * The properties file contains the following runtime behavior argments:
 * <pre>
 *
<b>Example Arguments:</b><br>
verbose=false
sourceDirs=/home/cisn/BERKELEY/indir /home/cisn/MENLO/indir /home/cisn/CGS/indir
destinationDirs=/home/tpp/ampexc/import/done
errorDirs=/home/tpp/ampexc/import/err
prefixList=
suffixList=.smUCB .smMP .EW2
formatType=EW2
sleepSeconds=60
localNetCode=CI
netcodeList=
dbaseHost=workhorse
dbaseDriver=oracle.jdbc.driver.OracleDriver
dbaseDomain=gps.caltech.edu
dbaseName=quakesdb
dbasePort=1521
dbaseUser=myworkingdb
dbasePasswd=mypwd
jasiObjectType=TRINET
doAssociation=true
makeAmpSet=true
assocAdHocFile=/home/tpp/ampexc/import/cfg/allnets.adhoc
assocSleepSeconds=1800
assocExpirationDays=1.0
assocGroupGap=120
logFileName=/home/tpp/ampexc/import/logs/import2.log
logRotationInterval=24H
</pre>
*
* sourceDir       is a dir or dirs where code looks for files to process
* destinationDir  is a dir or dirs where code puts files when its done
 *
 * @see: GroMoPacket
 */

/*
 Implementation notes: 

o EWII messages don't specify units internally but doc says its cm, cms, cmss. So that what I assume.
o Spectral amps are given units of 'spa'
o We don't check that the network, station, or channel are sane. But they won't
  associate unless then match something the the channel lists.
o "ALT:" time is usless so it's not used.
o "Scanned window" is not in message but required by NCEDC schema so I fake it with
  WSTART = amptime & DURATION = 1 sec.
o Other default values are left as set in GroMoPacket.newAmp() or Amplitude.create() method.
     o Weigth=1.0
     o quality=1.0
     o CFLAG = 'OS'
     o Auth = same as owner net

 */
public class Gmp2Db extends GMPEngine implements AutoCloseable {

/** Schedules regular runs of the amp associator. */
/*
 A word about the associator. It is run as a thread from here for several reasons.
 I would have prefered it be a stand-alone process.
 o Import and assoc are related so this centralizes configuration settings in one file.
 o This facilitates IPC - amp import can easily initiate an associator run after it
   imports a batch of amps. Signaling and coordination would have been difficult if
   the two processes were independent. Especially if the associator is in the middle of
   a scheduled run when Gmp2Db finishes and tries to initiate association.
 o Was going to run assoc as a dbase stored proc but you can't do decent logging from
   inside Oracle, especially of errors and exceptions.
   This also allows more flexibility on where you run the assoc job don't have to burden a dbase CPU.
*/
  ThreadScheduler threadScheduler = null; 
  Thread myForceAssocThread = null;

  /** Attempt to associate amps at this interval (seconds).
   * If assocInterval is <= 0 associator thread will not be run. */
  int assocInterval = 60 * 30;  // 1800 sec = auto-assoc every 30 minutes
  
  /** Delete unassocamp rows after this interval days older than current time */
  double assocExpirationDays = 1.0;  // one day
  
  /** If false association thread will not run.*/
  boolean doAssoc = true;
  boolean makeAmpSet = true;

  /** Optional AdHoc file of station info. */
  String adHocFilename = null;
  long adHocFileModDate = 0;   // file mod date, epoch millisecs - used to check for mods
  ChannelList adHocList = null;

/** Log file name - if not specified in props there is no log file, output goes to stdout, stderr. */
  String logFilename;

/** Event selection properties for association candidates. */
  EventSelectionProperties evtProps = new EventSelectionProperties();

/** Logfile rotation interval (seconds). If not specified logfile is NOT automatically rotated. */
  int logFileRotationInterval;   //  24 *60 *60 = 86,400sec = daily

  DbaseConnectionDescription dbd = null;

  //Construct the application
  public Gmp2Db() {
      appName = "Gmp2Db";
  }

  public Gmp2Db(String propertiesFile) {
    this();
    setPropertiesFile(propertiesFile);
  }
    /*
     * Load the Channel list. This is done one time at program startup.
     * The list is used to lookup lat/lon for new data as it is read in.
     */
    protected boolean loadChannelList() {

        boolean status = true;

       // ChannelList cList = MasterChannelList.get();

        //DEBUG: here disable loading of associated channel data like corrections if db tables are missing
        if ( props.isSpecified("channelListReadOnlyLatLonZ") &&
             props.getBoolean("channelListReadOnlyLatLonZ") ) {
            MasterChannelList.readOnlyChannelLatLonZ();
        }

        boolean readCache = props.getBoolean("channelListCacheRead");
        String cacheFileName = props.getUserFileNameFromProperty("channelCacheFilename");

        String str = null;
        if (readCache) {
          str = "Reading Channels from file cache "+cacheFileName;
        } else {
          str = "Reading Channels from data source:<p> "+DataSource.getDbaseName();
        }
        dump(str);

        // read Channel list from cache using the filename in input properties
        // uses default location if cacheFileName is null:
        String chanGroupName = props.getChannelGroupName(); // is there one?

        String dbLoadStr = ">>> Load of CURRENT Channels associated chanGroupName = " + chanGroupName +
            " from : " + DataSource.getDbaseName() + " might take minutes ...";

        if (readCache) {
          MasterChannelList.set(ChannelList.readFromCache(cacheFileName));

          // cache read failed, read from dbase and write to cache for future use
          // uses default location if cacheFileName is null:
          if (MasterChannelList.isEmpty()) {
            String failStr = "Cache read failed.";
            dump(failStr);
            dump(dbLoadStr);
            if (chanGroupName == null) MasterChannelList.set(ChannelList.readList(null));
            else {
              MasterChannelList.set(ChannelList.readListByName(chanGroupName, null));
            }

            //db read succeeded -- write this "current" list to cache file in background
            status = MasterChannelList.get().writeToCacheInBackground();

          } else { // cache read succeeded
            // check to overwrite old Channel cache data with update of  "current" date Channel data
            // if (props.getBoolean("autoRefreshChannelList") && !cacheRefreshed) refreshMasterCache();
          }
        }
        else { // don't read cache file, instead read "current" list and write it to cache.
          dump(dbLoadStr);
          if (chanGroupName == null) MasterChannelList.set(ChannelList.readList(null));
          else {
            MasterChannelList.set(ChannelList.readListByName(chanGroupName, null));
          }
          MasterChannelList.get().writeToCacheInBackground();
        }

        // create HashMap of channels in MasterChannelList, if not already one
        if (! MasterChannelList.get().hasLookupMap()) {
            MasterChannelList.get().createLookupMap();
        }
        dump(">>> Finished loading of MasterChannelList, total channels: " + MasterChannelList.get().size());
        return status;
    }

    protected void refreshMasterCache() {
        ChannelList cList = MasterChannelList.get();
        if (cList != null) {
            dump(appName + " INFO: Auto refresh of channel list in background.");
            cList.refreshCache(); // BEWARE only "currently" active Channels are cached
        }
    }

    public void setDebug(boolean tf) {
        super.setDebug(tf);
        AmpAssocEngine.setDebug(tf);
        GroMoPacket.debug = tf;
    }

  /** Set program behavior variables using parameters from properties file.
   Returns 'false' if any of the required properties are invalid. Non-required
   properties that are not set in the properties file remain at the default value.
   Note that not all properties must be specified for all child classes so only
   required properties are checked. */
  boolean setProperties() {

    boolean status = super.setProperties();

    if (props != null) AmpAssoc.plm.setProperties(props); // added to test using PowerLawTimeWindowModel -aww 2013/06/26

    // Specifies the log file path/name, must configure static pattern and zone before call to set name
    if (props.isSpecified("logTimestampPattern")) {
      Logger.TIMESTAMP_PATTERN = props.getProperty("logTimestampPattern");
      dumpVerbose("Logger timestamp pattern= "+Logger.TIMESTAMP_PATTERN);
    }

    if (props.isSpecified("logTimestampZone")) {
      Logger.TIMESTAMP_ZONE = props.getProperty("logTimestampZone");
      dumpVerbose("Logger timestamp zone= "+Logger.TIMESTAMP_ZONE);
    }

    if (props.isSpecified("logFileName")) {
        setLogFilename(props.getProperty("logFileName"));
        // The logger redirects System.out & System.err to a file.
        // Must print this message BEFORE Logger call for it to show up on stdout
        dump("All output is being redirected to "+getLogFilename());
        Logger.setFilename(getLogFilename());

        // NOTE: Set Logger.appProps here so that if log rotates and appProps dumpProperties=true
        // the properties are written at beginning new log
        Logger.setAppProps(props);
    }

    if (props.isSpecified("logRotationInterval")) {
      Logger.setRotationInterval(props.getProperty("logRotationInterval"));
      dump("Logger rotation interval= "+Logger.getRotationInterval());
    }

    // should association be done at all?
    if (props.isSpecified("doAssociation"))
        status &= setDoAssociation(props.getBoolean("doAssociation"));

    // how long between association runs?
    if (props.isSpecified("assocSleepSeconds"))
        status &= setAssocSleepSeconds(props.getDouble("assocSleepSeconds"));

    // how long do amps exist before we give up and delete them?
    if (props.isSpecified("assocExpirationDays"))
        status &= setAssocExpirationDays(props.getDouble("assocExpirationDays"));
    
    // associate amps with ampset in db 
    if (props.isSpecified("makeAmpSet"))
        makeAmpSet = props.getBoolean("makeAmpSet");

    // log missing lat lon z channels
    if (props.isSpecified("logMissingLatLonZ"))
        AmpAssocEngine.setLogMissingLatLonZ(props.getBoolean("logMissingLatLonZ"));

    // the grouping gap for association
    if (props.isSpecified("assocGroupGap"))
        AmpAssocEngine.setGroupGap(props.getDouble("assocGroupGap"));

    // the max amps in one group for association
    if (props.isSpecified("assocMaxGroupSize"))
        AmpAssocEngine.setMaxInGroup(props.getInt("assocMaxGroupSize"));

    // the max amps in one group for association
    if (props.isSpecified("assocMaxCommitSize"))
        AmpAssocEngine.setMaxCommitSize(props.getInt("assocMaxCommitSize"));

    // reject new amp if like already exists in db 
    if (props.isSpecified("assocRejectDups"))
        AmpAssocEngine.setRejectDups(props.getBoolean("assocRejectDups"));

    // the max window seconds preceeding the earliest amp time in group to query for for possible associated events
    if (props.isSpecified("assocEventCandidateWindowSize"))
        AmpAssocEngine.setEventCandidateWindowSize(props.getDouble("assocEventCandidateWindowSize"));

    // Reads in an Ad Hoc station file if defined
    if (props.isSpecified("assocAdHocFile"))
        setAdHocFile(props.getProperty("assocAdHocFile"));

    // configure event selection properties that specifies candiates for association
    if (props.isSpecified("eventSelectionProperties")) {
        String filename = props.getProperty("eventSelectionProperties");
//        evtProps.initialize("", filename, filename);
        // This form of Constructor uses explicite path/file: does not user <userhome>
        evtProps = new EventSelectionProperties(filename, null);
        AmpAssocEngine.setEventSelectionProperties(evtProps);
    } else {
        // default to "<userhome>/.jiggle/eventProperties"
//        String subdir = "jiggle";
//        evtProps.initialize(subdir,
//                            EventSelectionProperties.DEFAULT_FILENAME,
//                            EventSelectionProperties.DEFAULT_FILENAME);
        status = false;   // don't default - eventselection MUST BE SPEC'ED
    }

    // GroMoPacket source default is "GMP"
    //GroMoPacket.source = EnvironmentInfo.getApplicationName();
    if ( props.isSpecified("ampSubsource") ) {
        GroMoPacket.setAmpSubsource(props.getProperty("ampSubsource"));
        AmpAssocEngine.setAmpSubsource(GroMoPacket.getAmpSubsource());
    }

    return status;
  }

  /*
  DbaseConnectionDescription getDbD() {
      return dbd;
  }
  void setDbD(DbaseConnectionDescription dbd) {
      this.dbd = dbd;
  }
  */

  void setLogFilename(String name) {
    logFilename = name;
  }
  String getLogFilename() {
    return logFilename;
  }

  int getLogFileRotationInterval() {
   return Logger.getRotationInterval();
  }
  
  /** Set the interval at which the amp association thread should run
   * when there have been no new amps. This is set by the property 'assocExpirationDays'.*/  
  boolean setAssocExpirationDays(double days) {
    assocExpirationDays = (Double.isNaN(days)) ? 0. : days;

    if (assocExpirationDays*86400. <= getAssocSleepSeconds()) {
      dumpError("Warning: short association expiration time value = "+ assocExpirationDays
                 + " Amps may expire before next association.");
      return false;
    }
    return true;
  }
 
  /** Return the interval at which the amp association thread should run
  * when there have been no new amps. This is set by the property 'assocSleepSeconds'.*/
  double getAssocExpirationDays() {
    return assocExpirationDays;
  }
  
  /** Set name of and read in the optional Ad Hoc station file.
   * This remembers the modification date of the file. On subsequent calls to this method
   * the file will be REREAD if the current mod-date for the file does not match the previous
   * one. Thus, later calls to this method will refresh the list from the modified file if it changed.*/
  void setAdHocFile(String filename) {
    adHocFilename = filename;

    // actually read in the file if 1st time or it has been modified since last read
    try {
      File file = new File(getAdHocFilename() );
      long modMillis = file.lastModified();  // milliseconds since the epoch (00:00:00 GMT, January 1, 1970)

      if (adHocFileModDate != modMillis) {
        adHocList = AdHocFormat.readInFile(adHocFilename);
        if (adHocList != null && ! adHocList.hasLookupMap()) adHocList.createLookupMap(); // -aww 2010/01/19
        dump("Read in "+adHocList.size()+" channels from "+ adHocFilename);
        adHocFileModDate = modMillis;

        if (debug) adHocList.printDumpString();
      }
    }
    catch (Exception ex) {
      ex.printStackTrace(System.err);
      dumpError("No such file or no read access: "+ adHocFilename);
    }
  }
  /** Get name of optional Ad Hoc station file.*/
  String getAdHocFilename() {
    return adHocFilename;
  }

  boolean setAssocSleepSeconds(double secs) {
    assocInterval = (int) secs;

    if (assocInterval <= 0) {
      dumpError("Warning: association sleep time value = "+secs+ " Associator will not run.");
      return false;
    }
    return true;
  }

  /** Return the interval at which the amp association thread should run
  * when there have been no new amps. This is set by the property 'assocSleepSeconds'.*/
  int getAssocSleepSeconds() {
    return assocInterval;
  }

  public boolean setDoAssociation(boolean tf) {
    doAssoc = tf;
    return true;
  }

  public boolean getDoAssociation(){
    return doAssoc;
  }

  /** After each sleep interval, process all the files in the source directory.  Returns count of files processed. */
  protected void processFilesContinuously() {

    int knt = 0;

    boolean redoAssocThread = true;

    // This is the Main Loop, loop forever...
    while (true) {

      // reconnection code below added to loop, only creates new connection if connection null or closed - aww 2009/09/01
      DataSource.set(dbd); // creates new connection when dbd is changed or connection is closed, else reuses existing connection

      if (DataSource.isClosed()) {
        sleep(Math.max(300., getSleepSeconds())); // hibernate for at least 5 minutes 
        DataSource.set(dbd); // try to reset connection once more
        if (DataSource.isClosed()) {  // no connection again, so bail, exit loop
          break;
        }
      }

      if ( doProcessing(props) ) {

        if (getVerbose()) dumpVerbose("Looking for GMP files @ "+ new DateTime().toString() + " UTC in:\n" + sourceDirsToString());
        else Logger.touchLogFile(); // modification time update -aww 2012/10/10


        // Start regularly scheduled runs. Interval is in config as "assocSleepSecs"
        if ( redoAssocThread) {
            startAssociationThread();
            redoAssocThread = false;
            // force an immediate initial run
            forceAssociationThread();
        }

        knt = processFiles();

        // if there were new amps try to associate (in another thread) if association
        // thread is currently running reschedule it to run again upon completion.
        if (knt > 0) forceAssociationThread();
      }
      else {
          redoAssocThread = true;

          // Try cleanup ?
          if (threadScheduler != null) {
              threadScheduler.stop(); 
              threadScheduler.kill();
          }
          threadScheduler = null;

          try {
              if (myForceAssocThread != null) myForceAssocThread.interrupt();
          }
          catch (Exception ex) {
          }
          myForceAssocThread = null;
      }

      sleep();   // zzzzz

    }

    // Done with all processing close out log - aww 2009/09/01
    dumpError("processFilesContinuously() dataSource connection null/closed, forced loop exit ... ");
    Logger.closeLogFile();

    if (!DataSource.isNull()) DataSource.close();  // db cleanup too, -aww 2010/04/23
    //AmpAssocEngine.closeAmpGetterConnection(); // added for insurance - aww 2010/04/23
    AmpAssocEngine.closeConnections(); // added logic closes separate static AmpAssocEngine.conn, plus one in UnassocAmplitudeGetterTN -aww 2011/01/18
  }

/** Process all the files in the source directory.
 Returns count of files processed. One file may contain many amps. */
  protected int processFiles() {

    BenchMark bm = new BenchMark();    // measures elapse time to do the work
    int goodknt = 0;
    int totknt = 0;

    // get list of files to process by looking at the source directories
    File files[] = getFileList(getSourceDirList());

    if (files != null && files.length > 0) {

      dumpVerbose("Found "+files.length+" files to process @ "+ new DateTime().toString());

      for (int i = 0; i< files.length; i++) {    // for each file
        totknt++;
        if (processOneFile(files[i])) {
          goodknt++;
        }
      }

      bm.printTimeStamp("Gmp2db processed " +goodknt+ " of "+totknt +" new files in", System.out);

    }
    return totknt;
  }

/** Read and process one file. Add amps to the common amplist. */
  boolean processOneFile(File file) {

    AmpList amplist;

    dumpVerbose("** Processing file: "+file.toString());

    try {
      amplist = GroMoPacket.parse(file);   // main work here
    }
    catch (Exception ex) {

      dumpError("Bad File: "+file.getName());
      ex.printStackTrace(System.err);

      dispatchFile(file, false);  //move file to where bad files go (File Hell?)
      return false;
    }

    amplist = cullAmpsByNet(amplist); // only those networks we want.

    amplist = cullAmpsBySubsource(amplist); // only those subsources we want (always GMP?) -aww 2009/05/08

    amplist = cullAmpsByAuth(amplist);   // only those authorities we want -aww 2010/12/20

    amplist = cullAmpsByType(amplist); // only those types we want (generally all). -aww 2010/12/20

    boolean status = true;

    if (amplist.size() > 0) {
      // write amps to dbase
      if (getCommitToDb()) {
        //
        long fileid = SeqIds.getNextSeq(DataSource.getConnection(), "UNASSOCSEQ");
        for (int i=0; i<amplist.size(); i++) {
            ((UnassocAmplitude)amplist.get(i)).setFileid(fileid);
        }
        dumpVerbose("Processed "+amplist.size()+" amps in " +file.getName() + " db fileid: " + fileid);
        //
        // Need stored procedure in package to process "update" or "delete" using collections for all loop over
        // net,sta,seedchan,location look for old valid amp, if found, replace association with new
        // Could have any single amp associated with multiple sets, all valid!
        // select b.setid, b.ampid from amp a, ampset b where a.net=? and a.sta=? and a.seedchan=? and
        //    a.location=? and a.amptype=? and b.ampsettype=? and b.isvalid=? and b.evid=?
        // update ampset set ampid=? where setid = ? and ampid = ?
        //amplist.setAmpSetType("sm"); // NO! if we set this, AmpTN commit does ampset commit but it UNASSOCIATED, NO LOCAL EVID !
        status = amplist.commit();
        //Can't do this here since amps are unassociated ? Don't know the local evid
        //dump("%d %s amps in ampsetid=% for evid=%d%n", amplist.size(), amplist.getAmpSetType(), amplist.getAmpSetId(), evid);
        // Do file-level commit, rollback and bail if problems
        if (status) status = dbaseCommit();

        if (!status) {
           dumpError("Fatal DataSource commit error, exiting ....");
           System.exit(2);  // sudden death -aww 2009/08/31 
        }

      } else {
        dumpError("WARNING: commitToDb=false, Valid amps are NOT being written to database!");
      }
    } else {
      dump("No usable amps found in file: " + file.getName());
      status = true;
    }

    dispatchFile(file, status);  // good or bad

    return status;
  }

/** Do final dbase commit. */
  boolean dbaseCommit() {
      try {
        //dumpVerbose("DataSource.commit event >>> ");
        DataSource.getConnection().commit();
      }
      catch (SQLException ex)  {
        ex.printStackTrace();
        try {
          DataSource.getConnection().rollback();
        }
        catch (SQLException ex2)  {
          ex.printStackTrace();
          return false;
        }
      }
      return true;
    }

/** Create a ThreadScheduler that will run the association thread at a regular
 * interval. */
    protected boolean startAssociationThread() {
      if ( getDoAssociation() && getAssocSleepSeconds() > 0) {
        boolean isDaemon = true;  // sudden death when main thread exits
        AssocThread mySchedAssocThread = new AssocThread(); 
        threadScheduler = new ThreadScheduler(mySchedAssocThread, getAssocSleepSeconds(), isDaemon);
        threadScheduler.setVerbose(getVerbose());
        return true;
      } else {
        return false;
      }
    }

/** Run the associator now, or if its currently running run it again when its done. */
    protected void forceAssociationThread() {

      if (!getDoAssociation()) return;  // don't do it!

      // wake-up scheduled thread
      if (threadScheduler != null) {
        threadScheduler.runASAP();

      } else {
      // scheduler not running, do a one shot run
        AssocThread thread = new AssocThread();
        myForceAssocThread = new Thread(thread);
        myForceAssocThread.setDaemon(true); // sudden death if main user thread exits
        myForceAssocThread.start(); // executes in separate thread -aww  2011/01/03
        //EventQueue.invokeLater(thread);   // execute in EventQueue thread
      }
    }

    /** Association Work happens here. */
      protected void doAssociations() {
        BenchMark bm = new BenchMark();    // measures elapse time to do the work
        dumpVerbose("AssocThread starting @ "+ new DateTime().toString());

        // NOTE: gap and max in group were set in static class at time of properties setup
        AmpAssocEngine.setVerbose(getVerbose());

        try {
          if (AmpAssocEngine.getConnection() == null || AmpAssocEngine.getConnection().isClosed())
              AmpAssocEngine.setConnection(DataSource.getNewConnect());
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return;
        }

        if (makeAmpSet) AmpAssocEngine.setAmpSetType("sm");
        else AmpAssocEngine.setAmpSetType(null);

        // AdHoc list was already read in, this just passes the list to the Assoc thread
        // could reread here to make more dynamic
        setAdHocFile(getAdHocFilename());   // this will REREAD the file if it has changed
        AmpAssocEngine.setAlternateChannelList(adHocList); // note changed method name -aww 2010/01/18
        // also set MasterChannelList for engine -aww
        AmpAssocEngine.setMasterChannelList(MasterChannelList.get()); // added -aww 2010/01/18

        AmpAssocEngine.setDaysBack(assocExpirationDays); // default = 1.0

        int cnt = 0;
        try {
          cnt = AmpAssocEngine.associateAmps() ;
          // could run as storedproc dbase call, also but wouldn't get easy access to feedback
        }
        catch(Exception ex) {
          AmpAssocEngine.closeAmpGetterConnection(); // added insurance to close connection -aww 2010/04/23
          ex.printStackTrace();
        }

        bm.printTimeStamp("AssocThread done cnt = " + cnt + " time=", System.out);
        System.out.println("-----------------------------------------------------");
      }

/* **************************************************************************** */
    /**
     * This is a one-shot, self-starting thread that associates amps in background.
     */
    class AssocThread implements Runnable {
      public void run() {
          doAssociations();
      }
    } // end of internal class

/* **************************************************************************** */

  //Main method
  public static void main(String[] args) {

    //String propFilename = "c:/temp/UCB/gmp2db.props";
    //String adHocFile = "C:\\Documents and Settings\\doug\\My Documents\\Trinet\\StaInfo\\Ad Hoc Lists\\AllNets.adhoc";
    //String propFilename = "";
      String propFilename = "properties";

    if (args.length <= 0) {
      dumpError("Usage: java Gmp2Db [prop-filename]");
      // comment this next line for debugging
      System.exit(0);
    }
    else {
      propFilename = args[0]; //
    }

    try (Gmp2Db gmp2db = new Gmp2Db()) {
        dump("Properties file: "+propFilename);
        // bail if invalid properties
        if (!gmp2db.setPropertiesFile(propFilename)) {
            dumpError("Exiting - bad properties file: "+propFilename);
            dumpError(" === Dumping properties ===\n");
            // NOTE configure method dumps too if "dumpProperties=true"
            //if (gmp2db.props != null) gmp2db.props.list(System.out);
            if (gmp2db.props != null) gmp2db.props.dumpProperties();
            dumpError(" === End of properties dump ===\n");
            System.exit(1);
        }

        if ( ! gmp2db.props.getBoolean("dumpProperties") ) {
            dumpVerbose("\nSource dirs: \n"+gmp2db.sourceDirsToString());
            dumpVerbose("Destination dirs: \n"+gmp2db.destinationDirsToString());
            dumpVerbose("Error dir: \n"+gmp2db.errorDir);
        }

        // Dump here to see appName:
        EnvironmentInfo.dump();

        // create dbase connection description for re-use in loop for reconnect
        gmp2db.dbd = gmp2db.props.getDbaseDescription();
        if (gmp2db.dbd == null) {
            dumpError("Error creating database description - check properties file");
        }
        /*
    dumpVerbose("Making connection to: "+gmp2db.dbd.toString());
    //DataSource datasource = new DataSource(gmp2db.dbd);  // calls configure, doesn't setup connection URL correctly ?
    DataSource datasource = new DataSource();
    datasource.set(gmp2db.dbd); // - aww 2008/06/28
    if (DataSource.getConnection() == null) {
      dumpError("Error creating database connection - check properties file");
      System.exit(2);  // sudden death -aww 2009/08/31 
    }
         */

        gmp2db.loadChannelList(); // added -aww 2010/01/18

        if (gmp2db.props.getBoolean("oneshot")) { // don't loop only process once, for testing only
            if ( gmp2db.processFiles() > 0 && gmp2db.getDoAssociation()) gmp2db.doAssociations(); // if there were any new amps try to associate them
            if (!DataSource.isNull()) DataSource.close();
            //AmpAssocEngine.closeAmpGetterConnection();
            AmpAssocEngine.closeConnections(); // added logic closes separate static AmpAssocEngine.conn, plus one in UnassocAmplitudeGetterTN -aww 2011/01/18
            System.exit(0);
        } 

        // Start regularly scheduled runs. Interval is in config as "assocSleepSecs"
        //gmp2db.startAssociationThread();

        // force an immediate initial run
        //gmp2db.forceAssociationThread();

        // look for files to process FOREVER...
        gmp2db.processFilesContinuously();
    }
  }

  private boolean doProcessing(GenericPropertyList props) {

        boolean doit = true;

        if (props.getBoolean("pcsCheckHostAppRole")) {

            String myhostname = "unknown";
            try {
              myhostname = InetAddress.getLocalHost().getHostName();
            } catch (UnknownHostException e) { }

            int idx = myhostname.indexOf(".");
            if (idx > 0) {
                myhostname = myhostname.substring(0, idx);
            }

            String app  = props.getProperty("pcsProcessorAppName", "importamps");
            String state = props.getProperty("pcsStateName", "none");
            String mode = props.getProperty("pcsProcessorModeName", "P");

            boolean primaryHost = DataSource.isPrimaryHost(myhostname);
            String myDbHostAppRole = DataSource.getHostAppRole(myhostname, app, state); // where role maps to mode of P, S, A or *


            if (debug) {
                dumpVerbose("DEBUG Gmp2Db myhostname:" + myhostname +
                    " db host primary:" + primaryHost + " hostAppRole:" + myDbHostAppRole +
                    " runtime appName:" + app + " mode:" + mode + " state:" + state );
            }

            if ( mode.equals("P")  && myDbHostAppRole.startsWith("P") ) {
                //doit = DataSource.hasPrimaryRtRole(); // generates requests only if db is primary
                doit = DataSource.isPrimaryHost(myhostname); // generates requests only if host is primary
            }
            else if ( mode.equals("S") && myDbHostAppRole.startsWith("S") ) {
                //doit = !DataSource.hasPrimaryRtRole(); // generates requests only if db is shadow
                doit = !DataSource.isPrimaryHost(myhostname); // generates requests only if host is primary
            }
            else { // DEFAULT
                // if the host's role unknown doit by default
                doit = true;
                if (DataSource.isPrimaryHost(myhostname)) { // it's the primary host
                    // if app mode is All, Any, or Primary doit, but don't doit if app mode is run on shadow host
                    doit = !myDbHostAppRole.startsWith("S");
                }
                else { // host is the shadow standby 
                    // if app mode is All, Any, or Shadow doit, but don't if app mode is run on primary host 
                    doit = !myDbHostAppRole.startsWith("P");
                }
            }

            if (!doit) { // a no-op, no requests generated, just result posting -aww 2012/03/06
                System.out.println("-----------------------------------------------------"); 
                dump(app + " app NO-OP, db host primary: " + primaryHost + " app role: " +
                        myDbHostAppRole + " does not match configured property mode: " + mode + " @ " + new DateTime().toString());
                System.out.println("-----------------------------------------------------");        
                doit = false;
            }
        }

        return doit;
  }

  @Override
  public void close() {
      DataSource.close();
      //AmpAssocEngine.closeAmpGetterConnection(); // added for insurance - aww 2010/04/23
      AmpAssocEngine.closeConnections(); // added logic closes separate static AmpAssocEngine.conn, plus one in UnassocAmplitudeGetterTN -aww 2011/01/18
  }

} // end of Gmp2Db
