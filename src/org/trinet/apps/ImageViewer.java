package org.trinet.apps;

import java.awt.*;
import java.awt.event.*;
import java.net.*;
import javax.swing.*;

/**
 * <p>Title: jasi project</p>
 * <p>Description: View an arbitrary image. Only works for .jpg, .gif, or .png </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: USGS</p>
 * @author Doug Given
 * @version 1.0
 *
 * Based on code example from http://www.geocities.com/marcoschmidt.geo/java-load-image-toolkit.html
 */

public class ImageViewer extends JFrame {
//public class ImageViewer extends Frame {

  static int titleBarHeight = 20;

        private Image image;

        public ImageViewer(String fileName) {
                Toolkit toolkit = Toolkit.getDefaultToolkit();

                // if it starts with "http" its a URL
                if (fileName.startsWith("http")) {
                  try {
                    image = toolkit.getImage(new URL(fileName));
                  } catch (Exception ex) {
                    System.err.println("URL Failed: "+fileName);
                    return ;
                  }
                // else its a local file
                } else {
                  image = toolkit.getImage(fileName);
                }
                //System.out.println(image.toString());

                // allows asyncronous loading of images and helps keep track of them
                MediaTracker mediaTracker = new MediaTracker(this);
                mediaTracker.addImage(image, 0);

                try
                {
                        mediaTracker.waitForID(0);
                }
                catch (InterruptedException ie)
                {
                        System.err.println(ie);
                        System.exit(1);
                }
                addWindowListener(new WindowAdapter() {
                      public void windowClosing(WindowEvent e) {
                        System.exit(0);
                      }
                });

                // image doesn't seem to be loaded until here...
                if (image.getHeight(null) == -1) {
                  System.out.println("Bad file or image: "+fileName+" " +
                                      image.getWidth(null)+" "+ image.getHeight(null));
                  return;
                }

                setSize(image.getWidth(null), image.getHeight(null)+titleBarHeight);
                setTitle(fileName);
                setVisible(true);
        }

        public void paint(Graphics graphics) {
                graphics.drawImage(image, 0, titleBarHeight, null);
        }

        public static void main(String[] args) {
                new ImageViewer(args[0]);
        }
}
