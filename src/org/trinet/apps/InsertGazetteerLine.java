/** INPUT FILE FORMAT:
 For each fault line segment:
 a line declaring 2 CSV: the name of fault, the relative segment number
 1st line followed by one or more lines of CSV pairs of lat,lon ending with lat,lon pair = 0.,0.
 Repeat same pattern for each fault line segment until end of input file.
*/
package org.trinet.apps;

import java.io.*;
import java.sql.*;
import javax.sql.rowset.serial.SerialBlob;
import java.util.StringTokenizer;
import java.sql.Blob;
//import oracle.sql.BLOB;
//import oracle.jdbc.OraclePreparedStatement;
import org.trinet.jasi.DataSource;
import org.trinet.jdbc.datasources.DbaseConnectionDescription;
import org.trinet.jdbc.datasources.AbstractSQLDataSource;

public class InsertGazetteerLine {
            
    private static int NSIZE = 4096; 

    // fault code from gazetteertype table, could lookup value by query: "select code from gazetteertype where name='fault'";
    private static int type = 601;

    private static String FORMAT = "double";

    private static StringTokenizer stokec = null;

    private static String segName = null;
    private static int segNumb = 1;

    private static Connection conn = null;
    private static PreparedStatement ps = null;
    private static java.sql.Blob sblob = null;


    private static final String INSERT_SQL =
        "insert into gazetteerline (gazid,type,name,line,format,points) values (?, ?, ?, ?, ?, ?)";

    public static final void main(String[] args) {

        if (args.length != 7) {
            System.err.println("Usage: java InsertGazetteerLine dbhost dbname dbport user passwd gazid faultfilename");
            System.exit(-1);
        }


        String dbhost = "";
        String dbname = "";
        String dbport = "";
        String dbuser = "";
        String dbpass = "";
        int gazid = 0;
        String filename = "";

        dbhost = args[0];
        dbname = args[1];
        dbport = args[2];
        dbuser = args[3];
        dbpass = args[4];

        try {
          gazid = Integer.parseInt(args[5]);
        }
        catch ( Exception ex) {
            System.err.println("Error: input gazid must be integer: " + args[5]);
            System.exit(1);
        }

        filename = args[6];

        // Create a DataSource connection to input db here
        String url = String.format("jdbc:%s:@%s:%s:%s", AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL, dbhost, dbport, dbname);
        System.out.println("url: " + url);
        DataSource ds = new DataSource(url, AbstractSQLDataSource.DEFAULT_DS_DRIVER, dbuser, dbpass);    // make connection

        ds.setWriteBackEnabled(true);

        System.out.println("InsertGazetteerLine db:\n " + ds.describeConnection());
        System.out.println(" input file = " + filename);

        if (ds.getConnection() == null) {
            System.err.println("InsertGazetteerLine Error: null DataSource db connection!");
            System.exit(0);
        }

        ResultSet rs = null;
        Statement sm = null;
        int maxid = -1;
        try {
            sm = ds.getConnection().createStatement();
            rs = sm.executeQuery( "select max(gazid) from gazetteerline" ); 
            while (rs.next()) {;
               maxid = rs.getInt(1);
            }

            if ( maxid >= gazid) {
                System.err.println("Error: GazetteerLine table max gazid " + maxid + " >= " + gazid + " input gazid");
                ds.close();
                System.exit(-1);
            }
            rs.close();
            rs = null;
            rs = sm.executeQuery( "select code from gazetteertype where name='fault'" ); 
            while (rs.next()) {;
              type = rs.getInt(1);
            }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            try {
              if (rs != null) {
                  rs.close();
              }
              if (sm != null) {
                  sm.close();
              }
            }
            catch (SQLException ex) { }
        }

        int tcnt = 0; 
        int lcnt = 0;

        double lat = 0.;
        double lon = 0.;

        String line = null;
        byte[] b = null;

        boolean status = true;

        BufferedReader brdr = null;

        try {

            brdr = new BufferedReader(new FileReader(filename));

            // Read input file text line
            ReadInputFile:
            while ((line = brdr.readLine()) != null) {
                lcnt++;
                stokec = new StringTokenizer(line, ",");
                tcnt = stokec.countTokens(); 
                if (tcnt != 2) { // each segment begins with 2-token line of: name,number
                    System.err.println("InsertGazetteerLine Error: Missing line defining fault name,segment#");
                    status = false;
                    break ReadInputFile; // bail
                } 
                // Get fault segment name and line number
                while (stokec.hasMoreTokens()) {
                   segName = stokec.nextToken(); 
                   segNumb = Integer.parseInt(stokec.nextToken().trim());
                }

                // followed by one of more lines of lat,lon pairs ending with lat,lon = 0.
                ByteArrayOutputStream bout = new ByteArrayOutputStream(NSIZE);        
                DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(bout));
                ProcessBlob: 

                while ((line = brdr.readLine()) != null) {
                    stokec = new StringTokenizer(line, ", ");
                    while (stokec.hasMoreTokens()) {
                        lat = Double.parseDouble(stokec.nextToken());
                        if (lat == 0.0) break ProcessBlob; // assume done for this segment
                        lon =  Double.parseDouble(stokec.nextToken());
                        if (lon == 0.0) break ProcessBlob; // assume done for this segment

                        // write to byte buffer
                        dos.writeDouble(lat);
                        dos.writeDouble(lon);
                    }
                }

                // Done with input for fault segment flush output to byte buffer
                dos.flush();
                dos.close();

                // Create blob from byte buffer
                //sblob = new SerialBlob(bout.toByteArray());


                // Create new gazetteerline table row
                if (ps == null) {
                    ps = DataSource.getConnection().prepareStatement(INSERT_SQL);
                }
                //"insert into gazetterline (gazid,type,name,line,format,points) values (?, ?, ?, ?, ?, ?)";
                ps.setInt(1, gazid);
                ps.setInt(2, type);
                ps.setString(3, segName);
                ps.setInt(4, segNumb);
                ps.setString(5, FORMAT);
                //ps.setBlob(6, sblob); // doesn't work
                b = bout.toByteArray();
                //((OraclePreparedStatement)ps).setBytesForBlob(6, b);
                ps.setBlob(6, new ByteArrayInputStream(b), b.length);
                System.out.println("insert into gazetteerline (gazid,type,name,line,format,points) values ("+ gazid 
                        + "," + type+",'"+segName+"',"+segNumb+",'"+FORMAT+"', BlobBytes="+b.length+")");
                
                int cnt = ps.executeUpdate();
                if (cnt != 1) {
                      status = false;
                      System.err.println("Insert GazetteerLine row failed for fault name: " + segName + " segment line: " + segNumb);
                      break ReadInputFile;
                }
                gazid++;
               

                //sblob.free();

                //
                //Loop to top for next segment, if any more lines
                //

            } // End of ReadInputFile

            ds.commit();

        }
        catch (Exception ex) {
            status = false;
            ex.printStackTrace();
            ds.rollback();
        }
        finally {
            if (status == false) {
                System.err.printf("InsertGazetteerLine Error processing file:  %s line#: %d %n%s%n", filename, lcnt, line);
            }
            try {
                if (ps != null) ps.close();
                if (ds != null) ds.close();
            }
            catch (SQLException ex) {
            }

            try {
              if (brdr != null) brdr.close();
            }
            catch (IOException ex) { }
        }

        System.exit((status ? 0 : -1));
    }
} // end of InsertGazetteerLine class
