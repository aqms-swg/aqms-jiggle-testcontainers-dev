/** INPUT FILE FORMAT:<br>
 Each input line consists of 10 tokens (9 comma separated values ending with newline).
 Blank lines and lines starting with # are ignored. A column value corresponding to a zero length token is set NULL.
 <pre>
# The table row gazid is incremented after each iteration starting with input value for first iteration 
# Input is mapped in order to the table columns: type,lat,lon,z,name,state,county,map,datumh,datumv
# The minimal non-null input token values needed are: type,lat,lon,name e.g.:
10,+34.156700,-118.755000,,Agoura Hills,,,,,
</pre>
*/
package org.trinet.apps;

import java.io.*;
import java.sql.*;
import org.trinet.jasi.DataSource;
import org.trinet.jdbc.datasources.DbaseConnectionDescription;
import org.trinet.jdbc.datasources.AbstractSQLDataSource;
public class InsertGazetteerPt {

    private static String segName = null;
    private static int segNumb = 1;

    private static Connection conn = null;
    private static PreparedStatement ps = null;
    private static java.sql.Blob sblob = null;

    private static final String INSERT_SQL =
        "insert into gazetteerpt (gazid,type,lat,lon,z,name,state,county,map,datumh,datumv) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    public static final void main(String[] args) {

        if (args.length != 7) {
            System.err.println("Usage: java InsertGazetteerPt dbhost dbname dbport user passwd gazid ptfilename");
            System.exit(-1);
        }


        String dbhost = "";
        String dbname = "";
        String dbport = "";
        String dbuser = "";
        String dbpass = "";
        int gazid = 0;
        String filename = "";

        dbhost = args[0];
        dbname = args[1];
        dbport = args[2];
        dbuser = args[3];
        dbpass = args[4];
        try {
            gazid = Integer.parseInt(args[5]);
        }
        catch ( Exception ex) {
            System.err.println("input gazid must be integer: " + args[5]);
            System.exit(1);
        }
        filename = args[6];

        // Create a DataSource connection to input db here
        String url = String.format("jdbc:%s:@%s:%s:%s", AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL, dbhost, dbport, dbname);
        System.out.println("url: " + url);
        DataSource ds = new DataSource(url, AbstractSQLDataSource.DEFAULT_DS_DRIVER, dbuser, dbpass);    // make connection

        ds.setWriteBackEnabled(true);

        System.out.println("InsertGazetteerPt db:\n " + ds.describeConnection());
        System.out.println(" input file = " + filename);

        if (ds.getConnection() == null) {
            System.err.println("InsertGazetteerPt Error: null DataSource db connection!");
            System.exit(0);
        }

        ResultSet rs = null;
        Statement sm = null;
        int maxid = -1;
        try {
            sm = ds.getConnection().createStatement();

            rs = sm.executeQuery( "select max(gazid) from gazetteerpt" ); 
            while (rs.next()) {;
               maxid = rs.getInt(1);
            }

            if ( maxid >= gazid) {
                System.err.println("Error: GazetteerPt table max gazid " + maxid + " >= " + gazid + " input gazid");
                ds.close();
                System.exit(-1);
            }

        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            try {
              rs.close();
              sm.close();
            }
            catch (SQLException ex) { }
        }

        int lcnt = 0;

        String line = null;

        boolean status = true;

        BufferedReader brdr = null;

        int type = 0; // not null

        double lat = 0.; // not null
        double lon = 0.; // not null
        Integer z = null;

        String name = null; // not null
        String state = null;
        String county = null;
        String map = null;
        String datumv = null;
        String datumh = null;
        String[] tokens = null;

        try {

            brdr = new BufferedReader(new FileReader(filename));

            // Read input file text line
            ReadInputFile:
            while ((line = brdr.readLine()) != null) {
                lcnt++;
                if (line.length() == 0 || line.startsWith("#")) continue; // skip blanks and # lines
                tokens = line.split(",",11);
                if (tokens.length != 10) { // 
                    System.err.println("InsertGazetteerPt Error: Input line CSV token count incorrect, expected 10, found: " + tokens.length);
                    status = false;
                    break ReadInputFile; // bail
                } 

                // Get pt type,lat,lon,z,name,state,county,map,datumv,datumh
                int idx = 0;

                    type = Integer.parseInt(tokens[idx]); 
                    idx++;
                    try {
                      lat = Double.parseDouble(tokens[idx].trim()); 
                      idx++;
                      lon =  Double.parseDouble(tokens[idx].trim()); 
                      idx++;
                      z = (tokens[idx].length() == 0) ? null : Double.valueOf(Double.parseDouble(tokens[idx])).intValue();
                      idx++;
                    }
                    catch (NumberFormatException ex) {
                        System.err.println("InsertGazetteerPt Error: invalid number parse from string token"+idx+ ":\"" + tokens[idx] + "\"");
                        status = false;
                        break ReadInputFile; // bail

                    }
                    name = (tokens[idx].length() == 0) ? null : tokens[idx];
                    idx++;
                    if (name == null) {
                        System.err.println("InsertGazetteerPt Error: Input line empty name token");
                        status = false;
                        break ReadInputFile; // bail
                    }

                    state = (tokens[idx].length() == 0) ? null : tokens[idx];
                    idx++;
                    county = (tokens[idx].length() == 0) ? null : tokens[idx];
                    idx++;
                    map = (tokens[idx].length() == 0) ? null : tokens[idx];
                    idx++;
                    datumv = (tokens[idx].length() == 0) ? null : tokens[idx];
                    idx++;
                    datumh = (tokens[idx].length() == 0) ? null : tokens[idx];

                    // Create new gazetteerline table row
                    if (ps == null) {
                        ps = DataSource.getConnection().prepareStatement(INSERT_SQL);
                    }

                    ps.setInt(1, gazid); // not null
                    ps.setInt(2, type); // not null
                    ps.setDouble(3, lat); // not null
                    ps.setDouble(4, lon); // not null
                    if (z == null) ps.setNull(5, java.sql.Types.INTEGER);
                    else ps.setInt(5, z.intValue());
                    ps.setString(6, name); // not null
                    ps.setString(7, state);
                    ps.setString(8, county);
                    ps.setString(9, map);
                    ps.setString(10, datumh);
                    ps.setString(11, datumv);
                
                    int cnt = ps.executeUpdate();
                    if (cnt != 1) {
                        status = false;
                        System.err.println("Insert GazetteerPt row failed for fault name: " + name );
                        break ReadInputFile;
                    }
                    System.out.println("Last gazid in: " + gazid);
                    gazid++;

                //
                //Loop to top for next segment, if any more lines
                //

            } // End of ReadInputFile

            ds.commit();

        }
        catch (Exception ex) {
            status = false;
            ex.printStackTrace();
            ds.rollback();
        }
        finally {
            if (status == false) {
                System.err.printf("InsertGazetteerPt Error processing file:  %s line#: %d %n%s%n", filename, lcnt, line);
            }
            try {
                if (ps != null) ps.close();
                if (ds != null) ds.close();
            }
            catch (SQLException ex) {
            }

            try {
              if (brdr != null) brdr.close();
            }
            catch (IOException ex) { }
        }
        System.exit(status ? 0 : -1);
    }
} // end of InsertGazetteerPt class
