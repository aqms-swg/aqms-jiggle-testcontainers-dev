package org.trinet.apps;

import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.*;
import java.net.*;

import org.trinet.hypoinv.*;
import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.jiggle.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datasources.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;
import org.trinet.util.locationengines.*;

public class Locate {
    static boolean debug;
    static JiggleProperties props;
    static long evid;

    public static void main (String args[]) {

        // switch for debugging
        boolean commitIt = false;

        long evid = 14155424 ;

        System.out.println ("Making connection...");

        EnvironmentInfo.setApplicationName("Locate");
        EnvironmentInfo.setAutomatic(false);

        // Need loction server info, + net name
        props = new JiggleProperties("propertiesSolServer");

        System.out.println("prop file = "+props.getFilename());

        DbaseConnectionDescription dbd = props.getDbaseDescription();
        // The data source is static
        //DataSource ds = new DataSource(dbd);
        DataSource ds = new DataSource(); // -aww 2008/06/28
        ds.set(dbd); // aww 2008/06/26

        String locEngAddr = props.getProperty("locationEngineAddress");
        int locEngPort    = props.getInt("locationEnginePort");

        Solution sol = Solution.create().getById(evid);

        Channel.setLoadOnlyLatLonZ();
        if (sol == null) {
            System.out.println ("No solution found for "+evid);
            System.exit(-1);
        }
        sol.loadPhaseList();

        sol.getPhaseList().matchChannelsWithDataSource(sol.getDateTime());

        System.out.println ("--- Before location ------------------------------");
        System.out.println (sol.fullDump());
        System.out.println ("--------------------------------------------------");

        LocationEngineServiceIF locEng =
              (LocationEngineServiceIF) AbstractLocationEngine.create("org.trinet.util.locationengines.LocationEngineHypoInverse");
        locEng.setServer(locEngAddr, locEngPort);

        System.out.println("solserver = "+locEngAddr+" port= "+ locEngPort);
        locEng.setChannelList(MasterChannelList.get()) ;

        // locate it
        boolean status = false;
        String oldStr = "";
        int knt=0;
        //while (forever) { // infinite run-away loop, will it force a server crash? - aww
            status = locEng.solve(sol);
            System.out.println (locEng.getStatusString());
            String dumpStr = sol.fullDump();

            if (!oldStr.equals(dumpStr)) {
              System.out.println ("********* OUTPUT CHANGED **********");
              System.out.println (dumpStr);
            }
            System.out.println ("# "+knt++);
            oldStr = dumpStr;
         //} //test repeated calls

        if (status) {
          if (! EnvironmentInfo.isAutomatic())
               sol.setProcessingState(JasiProcessingConstants.STATE_HUMAN_TAG);

          //sol.authority.setValue(sol.getEventAuthority()); // added -aww 2011/08/17, removed 2011/09/21
          sol.authority.setValue(EnvironmentInfo.getNetworkCode()); // reverted -aww 2011/09/21
          sol.source.setValue(EnvironmentInfo.getApplicationName());
          sol.eventType.setValue(EventTypeMap3.getMap().toJasiType(EventTypeMap3.EARTHQUAKE)); // 2015/04/27 schema change split eq into gtype 
          sol.gtype.setValue(GTypeMap.LOCAL); // 2015/04/27 schema change split eq into gtype 

          System.out.println ("--- After Location -------------------------------");
          System.out.println (sol.fullDump());
          System.out.println ("--------------------------------------------------");
          //        sol.phaseList.dump();

          recalcMag(sol);

          // sets the flag but won't send alarms
          sol.setProcessingState(JasiProcessingConstants.STATE_FINAL_TAG);

          if (commitIt) {
            System.out.println ("Committing solution...");
            System.out.println ("Message = "+locEng.getStatusString());
            try {
               sol.commit();
               System.out.println (sol.getCommitStatus());
            } catch (JasiCommitException ex) {
               System.out.println("Commit error.");
            }
          } else {
            System.out.println("NOT Committing solution...");
          }
        } else {
          System.out.println("Solution failed.");
          System.out.println(locEng.getStatusString());
        }

        locEng.disconnect();

    }  // end of main()

    static void recalcMag(Solution sol) {
        if (sol.magnitude == null || sol.magnitude.hasNullValue()) return;
        // should check method??
        if (sol.magnitude.isCodaMag()) {
            calcMC(sol);
        } else {
            calcML(sol);
        }
    }

    /** Calculate the MC magnitude of this solution using the existing coda data.
    Does NOT re-examine the waveforms. */
    public static void calcMC(Solution sol) {
        if (sol.magnitude == null) return;        // no mag
        String configFile = props.getProperty("mcaConfigFile");
        ChannelList chanList =  MasterChannelList.get();
        // create on 1st use then reuse
        /***********    OLD Code
        JiggleMCA mcaMagEng = new JiggleMCA(configFile, chanList);
        Magnitude newMag = mcaMagEng.calcSummaryMag(sol);
        ***********/
        MagnitudeEngineIF mcaMagEng = AbstractMagnitudeEngine.create();
        mcaMagEng.configure(ConfigurationSourceIF.CONFIG_SRC_FILE, configFile, null, chanList);
        Magnitude newMag = (mcaMagEng.solve(sol)) ? mcaMagEng.getSummaryMagnitude() : null;
        if (debug) {
            System.out.println(newMag);
        }
    }

    /** Calculate the ML magnitude of this solution. Does NOT re-examine the waveforms. */
    public static void calcML(Solution sol) {
      if (sol.magnitude == null) return;        // no mag
      MagnitudeMethodIF ml = AbstractMagnitudeMethod.create("org.trinet.jasi.magmethods.TN.SoCalMlMagMethod");
      ml.configure();
      MagnitudeEngineIF magEng = AbstractMagnitudeEngine.create();
      magEng.setMagMethod(ml);
      Magnitude newMag = (magEng.solve(sol.magnitude)) ? magEng.getSummaryMagnitude() : null;
      if (debug) {
          System.out.println(newMag);
      }
    }

}  // end of class
