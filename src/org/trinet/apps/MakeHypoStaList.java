package org.trinet.apps;
import org.trinet.jasi.*;
/** Make a Hypoinverse #2 format station list for the currently active channels
* in the default dbase. Only returns "HH_", "EH_", "HL_" channels. */
public class MakeHypoStaList {

    public MakeHypoStaList() { }

    public static void main (String args[]) {
      //   System.out.println ("Making connection... ");
      TestDataSource.create();
      //
      //   System.out.println (dbase.toString());
      //ChannelList list = ChannelList.readCurrentList();
      //
      String compList[] = {"HH_", "EH_", "HL_"};
      ChannelList list = ChannelList.getByComponent(compList);
      Channel ch[] = list.getArray();

      for (int i = 0; i< ch.length; i++ ) {
        System.out.println (HypoFormat.toH2StationString(ch[i]));
      }
    }  // end of Main
} 
