package org.trinet.apps;

import java.sql.*;
import java.util.*;

import org.trinet.jasi.*;
import org.trinet.jasi.TN.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.LeapSeconds;


public class OriginFixer {

    static boolean debug = false;

    public OriginFixer() { }

    static SolutionTN[] getEvidList (String startTime, String stopTime) {

      String sql = "origin.datetime > TRUETIME.putEpoch("+LeapSeconds.stringToTrue(startTime)+",'UTC') and  "+
                   "origin.datetime < TRUETIME.putEpoch("+LeapSeconds.stringToTrue(stopTime)+",'UTC') and  "+
                   "origin.subsource like 'CUSP' and event.etype not like 'uk' and "+
                   "origin.TotalArr <> (select count(*) from assocArO where orid = origin.orid) "+
                   " order by origin.datetime";
      Collection list = new SolutionTN().getWhere(sql);
      int count = (list == null) ? 0 : list.size();
      return (count == 0) ? new SolutionTN[0] : (SolutionTN []) list.toArray(new SolutionTN[count]);
    }

    static boolean fixIt (long evid) {
      // get 1st and last orid for this evid
      String sql = "select "+Origin.COLUMN_NAMES+" from origin where evid =  "+evid+" order by lddate asc";

      // because "order by lddate asc" the oldest (original) comes first
      Origin origTable = new Origin(DataSource.getConnection());
      //Origin org[] = (Origin[]) origTable.getRowsByEventId(evid);
      Origin org[] = (Origin[]) origTable.getRowsBySql(sql);

      if (org.length < 2) return false; // only 1 row, nothing to fix

      // fix it
      try {
        return fixAssocArO(org[0].getLongValue("ORID"), org[org.length-1].getLongValue("ORID") );
      }
      catch (NoSuchFieldException ex) {
        return false;
      }

    }

    static boolean fixAssocArO (long origOrid, long newOrid) {
      String fieldNames = "ARID,COMMID,AUTH,SUBSOURCE,IPHASE,IMPORTANCE,DELTA,SEAZ,IN_WGT,WGT,TIMERES,"+
       "EMA,SLOW,VMODELID,SCORR,SDELAY,RFLAG,CCSET,LDDATE";

      String sql = "Insert into AssocArO  (ORID,"+ fieldNames +") "+
                   "Select "+ newOrid + ", "+ fieldNames +
                   " from assocArO where orid = "+ origOrid +" and "+
                   " arid not in (select arid from assocArO where orid = "+newOrid+" )" ;

      System.out.println(sql);
      // execute it

      try {
        if(doSql(sql)) {
          DataSource.commit();
          return true;
        } else {
          return false;
        }
      }
      catch (JasiCommitException ex) {
        return false;
      }
    }

    /** Execute the sql string, return false on error */
    static boolean doSql (String sql) throws JasiCommitException {

      Connection conn = DataSource.getConnection();

      /// make sure there's a connection
      if (conn == null) {
        throw new JasiCommitException("No connection to data source.");
        //return false;
      }
      Statement sm = null;
      boolean status = true;
      try {
        sm = conn.createStatement();
        sm.execute(sql);
      } catch (SQLException ex) {
        System.out.println ("SQL: "+sql);
        System.err.println(ex);
        ex.printStackTrace();
        status = false;

        // rollback on error
        try {
          conn.rollback();
        } catch (SQLException ex2)  {          // rollback failed
          System.err.println("Rollback failed:\n"+ex2);
          ex2.printStackTrace();
          status = false;
        } // end of try/catch
      }
      finally {
        try {
          if (sm != null) sm.close();
        } catch (SQLException ex) { } 
      }

      return status;

  }

  // ////////////////////////////////////////
    public static void main(String args[]) {

      String host = "serverma";  // default

      System.out.println ("Making connection...");
      TestDataSource.create(host);  // make connection

      String startTime = "1999-01-01 00:00:00.000";
      String stopTime  = "1999-12-31 00:00:00.000";

      Solution sol[] = getEvidList (startTime, stopTime) ;

      if (sol == null) {
        System.out.println ("No solutions between "+startTime+" and "+stopTime);
      } else {
        for (int i = 0; i< sol.length;i++) {
          System.out.println (sol[i].toNeatString());
        }
      }

      System.out.println ();

    }
}

