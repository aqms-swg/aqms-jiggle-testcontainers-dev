package org.trinet.apps;

import java.util.Date;

import org.trinet.jasi.*;
import org.trinet.pcs.*;
import org.trinet.util.*;


/**
 * <p>Title: RequestGeneratorCont</p>
 * <p>Description: Create Waveform Requests for events in a continuous, RT processing context.
 * 
 * Waveform requests are channel/time-windows that should include all seismic signal (energy)
 * of interest for an event. It writes Waveform request rows to a table using the
 * RequestCardTN class. </p>
 * 
 * Overview:<p>
 * This class is a "controller" that extends the AbstractChannelListProcessingController 
 * which handles the PCS processing aspects of the work. 
 * The RCG work is done by "processor module" is defined in by the property 
 * pcsModuleClassName (e.g. org.trinet.pcs.RcgProcessingModule) which implements
 * AbstractSolutionChannelDataProcessor. That module expects a property called
 * 'pcsEngineDelegateProps' which contains the property "model" 
 * (e.g. "org.trinet.jasi.PowerLawTimeWindowModel") which is the channel time window model
 * used by the RCG processor. (Are you paying attention?)
 *
 * Controller properties:<p>
 * model - full class name of ChannelTimeWindowModel to use. Default = org.trinet.jasi.JBChannelTimeWindowModel<br>
 * auth - character string to be written to the <code>Auth</code> field of the request row (Max length = 15) This is usually the NET code.<br>
 * staAuth - character string to be written to the <code>StaAuth</code> field of the request row (Max length = 15). Something like <code>SCEDC</code>.<br>
 * subsource - character string to be written to the <code>Subnet</code> field of the request row (Max length = 8)  The creator of the requests; defaults to <code>ReqGen</code>.<br>
 * priority - integer value indicating priority of retrieval. Usually set = 1.<br>
 * logfile - path/name of the logfile<br>
 * logfileDuration - length of time (in seconds) an instance of a log file is open.<br>
 * verbose - use verbose info and error logging (true or false)
 * <p>
<pre>
Command line switches:
        -h              Help
        -g <group>      Override pcsGroupName property
        -t <thread>     Override pcsThreadName property
        -s <state>      Override pcsStateName property
        -u <username>   Override dbaseUser property
        -p <password>   Override dbasePasswd property
        -P <Props-file>
        -e <evid>       one-shot processing (terminates when done)
        -f <filename>   containing evid list (terminates when done)
        -X              do not write request cards to dbase (test mode)
</pre>  
 * Processor properties (specific to the model). For example:<p>
 * org.trinet.jasi.MyTimeWindowModel.includeAllComponents=true<br>
 * org.trinet.jasi.MyTimeWindowModel.includeAllMag=3.5<br>
 * org.trinet.jasi.MyTimeWindowModel.maxDistance=10000<br>
 * org.trinet.jasi.MyTimeWindowModel.maxWindowSize=300<br>
 * org.trinet.jasi.MyTimeWindowModel.minDistance=30<br>
 * org.trinet.jasi.MyTimeWindowModel.minWindowSize=60<br>
 * org.trinet.jasi.MyTimeWindowModel.noiseThreshold=25<br>
 *
 <pre>
 PCS Properties

pcsLogFileName=C:\\Temp\\requestgen.log
pcsLogging=true
pcsGroupName=PostProc
pcsThreadName=RCG_RT
pcsStateName=RCG
pcsRunMode=0
pcsSleepMilliSecs=30000
pcsDefaultPostingRank=100
pcsForceStateProcessing=true
pcsFastFailLoops=true
pcsSolutionLocking=false
pcsAutoCommit=false
pcsModuleClassName=org.trinet.pcs.RcgProcessingModule
pcsListExclusive=false

See: http://pasadena.wr.usgs.gov/jiggle/pcshypomag.html
/* TODO
 * 
 * -- check existence of evid
 * -- check for currently exisiting requests (don't duplicate)
 * -- check for currently existing waveforms (don't duplicate)
 * 
 * -- Read Candidate list once - at startup
 * -- Reread on signal
 * -- Respond to CMS message
 * 
 * -- better logging (grep-able)
 * 
 */

public class RequestGeneratorCont extends AbstractChannelListProcessingController {
    
// made defaults static unless subclass needs instance level settings
    
    /**
     * This string is used to define an env variable that can be used
     * to set the home dir for props file, etc.
     * Use "-DRCG_CONT_USER_HOMEDIR=<dir-name>" on the command like.
     */ 
    static String AppName  = "rcg_cont";    

    public RequestGeneratorCont() { }

    public RequestGeneratorCont(String groupName, String threadName, String stateName) {
        this(AppName, groupName, threadName, stateName, null);
    }

    public RequestGeneratorCont(String groupName, String threadName, String stateName, String propFileName) {
        this(AppName, groupName, threadName, stateName, propFileName);
    }

    public RequestGeneratorCont(String appName, String groupName, String threadName, String stateName,
                                   String propFileName) {

        super(appName, groupName, threadName, stateName);
        setChanListLock(createLock()); // use lock for channel list
        // override default locking, locking not needed - aww 2016/10/25
        enableLocking = false;

        // If not specified, load "default" properties file which should be in the
        // subdir below users home named: rcg_rt/RCG_RT.props
        if ( propFileName == null || propFileName.equals("") ) {
            setProperties((PcsPropertyList)null); 
        } else {
            setProperties(propFileName);
        }
    }

    // Need a static handle for oracle db stored procedure implementation 
    public static final int processEvents(String groupName, String threadName, String stateName,
                                                            String propFileName) {
        return processEvents(groupName, threadName, stateName, propFileName, null);
    }

    public static final int processEvents(String groupName, String threadName, String stateName,
                                                String propFileName, String idFileName) {
        // create a processor instance configure by properties in specified input file
        try (RequestGeneratorCont rcgPC =
                new RequestGeneratorCont(groupName, threadName, stateName, propFileName)) {
            // if "module" should always be configured from an input property comment out next line
            // which sets a default module when none is specified -aww
            // TEST : if (rcgPC.module == null) rcgPC.setProcessingModule(new HypoMagSolutionProcessor(rcgPC.props));
            return (idFileName == null) ?
                    rcgPC.setupAndProcessIds() : rcgPC.setupAndProcessIds(idFileName);
        }
    }

    /* OVERRIDE AbstractSolutionProcessingController - to access and customize Property list ?
    //Sets instance properties, read specified user property files * and configures environment.
    //If PcsPropertyList is null will attempt to open the file called <app-name>.props (all lower-case). 
    public boolean setProperties(PcsPropertyList newProps) {
        initEnvironmentInfo();
        if (newProps == null) {
            //newProps = new RcgPropertyList(); // do one with most options 
            newProps = new PcsPropertyList(); // do one with most options            
            newProps.setFiletype(appName.toLowerCase()); // like "rcg_cont"
            newProps.setFilename(appName.toLowerCase()+".props"); // like "rcg_cont.props" 
        }
        props = newProps;   // <-- global variable
        props.reset(); // re-read user property files?
        // set properties class static field for leap/nominal secs

        return (props.setup() && initFromProperties());
    }

    // Sets instance properties, read specified input user property file and configures environment.
    public boolean setProperties(String propFileName) {
        // reset() reads file, setup()
        PcsPropertyList newProps = new PcsPropertyList(); // the one with the most options 
        newProps.setFiletype(appName.toLowerCase()); // like "rcg_cont"
        newProps.setFilename(propFileName);
        return setProperties(newProps);
    }
   */

   /**
    * Print out all legal command options.
    *
    */
    public static void printCommandOptions() {
       System.out.println("\nSwitches:");
       System.out.println(" -h              Help");
       System.out.println(" -g <group>      Override pcsGroupName property");
       System.out.println(" -t <thread>     Override pcsThreadName property");
       System.out.println(" -s <state>      Override pcsStateName property");
       System.out.println(" -u <username>   Override dbaseUser property");
       System.out.println(" -p <password>   Override dbasePasswd property");
       System.out.println(" -n <appName>    Override 'rcg_cont' default name");
       System.out.println(" -P <Props-file>");
       System.out.println(" -e <evid> one-shot processing (terminates when done)");
       System.out.println(" -f <filename> containing evid list (terminates when done)");
       System.out.println(" -X do not write request cards to dbase (test mode)"); 
    }
/**
 * Syntax:
 * Mode #1: do a one shot run
 * %  RequestGeneratorCont -e 92759437
 * 
 * @param args
 */
    public static final void main(String [] args) {

        /*
         * defaults - should be superceeded by properties
         */
        String propFileName     = "rcg.props";           // property file name
        
        // Default PCS group/thread/state
        String groupName        = "PostProc";            // PCS group
        String threadName       = "RCG";                 // PCS thread (aka table)
        String stateName        = "rcg_cont";            // PCS state
        String appName          = AppName;    
        String user = null;
        String pass = null;
        boolean noWrite = false;
        
        /**
         * File containing IDs to process, one per line, id is 1st token on each line
         */
        String idFileName = null;
        
        /**
         * One-shot event id to process
         */
        long eventId = 0;

        GetOpt getopt = new GetOpt(args, "hs:g:n:t:e:f:p:u:P:");
        getopt.optErr = true;
        int ch = -1;
        
        // process options in command line arguments
        System.out.print("Command line options: ");
        while ((ch = getopt.getopt()) != GetOpt.optEOF) {
           if ((char)ch == 'h') {
               printCommandOptions();
               System.exit(0);
           } else if ((char)ch == 'n'){
               appName = getopt.optArgGet();
               System.out.print("-n  "+appName);
           } else if ((char)ch == 'g'){
               groupName = getopt.optArgGet();   
               System.out.print("-g "+groupName);
           } else if ((char)ch == 't'){
               threadName = getopt.optArgGet();   
               System.out.print("-t "+threadName);
           } else if ((char)ch == 's'){
               stateName = getopt.optArgGet();
               System.out.print("-s "+stateName);
           } else if ((char)ch == 'u'){
               user = getopt.optArgGet(); 
               System.out.print("-u "+user);
           } else if ((char)ch == 'p'){
               pass = getopt.optArgGet(); 
               System.out.print("-p XXXX");
           } else if ((char)ch == 'P'){
               propFileName = getopt.optArgGet(); 
               System.out.print("-P " + propFileName);
           } else if ((char)ch == 'f'){
               idFileName = getopt.optArgGet(); 
               System.out.print("-f " + idFileName);
           } else if ((char)ch == 'e'){
               eventId = getopt.processArg(getopt.optArgGet(), eventId); 
               System.out.print("-e " +  eventId );
           } else if ((char)ch == 'X'){
               noWrite = true;             
               System.out.print("-X");
           } else {
               // illegal option on line
               printCommandOptions();
               System.exit(1);
           }        
        } 
        System.out.println();
             
        int result = 0;
        //
        //Test 2 : parse the props file, setup everything, instatiate the processing engine, etc.
        try (RequestGeneratorCont rcgPC = 
                new RequestGeneratorCont(appName, groupName, threadName, stateName, propFileName)) {

            // Override config setting if they're set on command-line
            if (user != null) rcgPC.props.setDbaseUser(user);
            if (pass != null) rcgPC.props.setDbasePasswd(pass);
            if (noWrite) rcgPC.props.setProperty("writeReqs", false);

            System.out.println("Using propertyfile: " + ((propFileName.equals("")) ? "DEFAULT" : propFileName));       

            // set what to read from dbase in creating candidate list
            // skipping clipping and corrections speeds things up
            if ( rcgPC.props.isSpecified("channelListReadOnlyLatLonZ") && rcgPC.props.getBoolean("channelListReadOnlyLatLonZ") )
                Channel.setLoadOnlyLatLonZ();
            else {
                Channel.setLoadClippingAmps(false);
                Channel.setLoadKnownCorrections(false);
                Channel.setLoadResponse(true);
            }

            // Create processor instance, model, read properties, read in channel list, etc.
            result = rcgPC.setupForProcessing();

            // dump setup info
            System.out.println(">>> Processing Settings Setup <<<");
            System.out.println(rcgPC.getProcessingAttributesString());
            System.out.println(">>><<<");

            // Setup is OK - start processing events
            // This is designed tangled up in PCS logic, which makes on-shot and list processing
            // convoluted. 
            // In single evid and list mode the evids are still posted and resulted which has
            // some potentially bad affects.
            // 1) your request to process can get rank-blocked by outside activity
            // 2) your request may cause transitions into other states defined for RT
            //    processing that you don't want
            // 3) if you use a private group/table/state the final result will be left as
            //    debris in the PCS table (unless you define a terminal transition).
            // THE FIX: when running in one-shot or list mode always use a private group/table/state
            if (result > 0) {
                // MODE #1: one-shot processing of IDs from a file
                if (idFileName != null) {
                    result = rcgPC.processIdsBulkPost(rcgPC.getIdListToProcess(idFileName));

                    // MODE #2: one-shot processing of a single ID
                } else if (eventId > 0) {
                    int rank = 5000;
                    // post with a huge RANK to avoid getting rank blocked
                    result = rcgPC.postId(eventId, rank); 
                    if (result > 0) {
                        System.out.println(">>>Posted id: " + eventId + " Rank: "+rank);
                        result = rcgPC.processId(eventId); 
                    } else {
                        System.err.println("ERROR RequestGeneratorCont: Unable to post input evid: " + eventId + " result: " + result);
                    }
                    // MODE #3: continuous mode, enter PCS loop to process posted events 
                } else {
                    rcgPC.startReloadThread();
                    result = rcgPC.processIds() ;   // Where the real work gets done
                }
            } else {
                System.err.println("ERROR RequestGeneratorCont : Check for configuration or DB ERROR.");
            }
            //
            if (result > 0) {
                System.out.println("\nRequestGeneratorCont events processed: " + result);
            } else if (result == 0) {
                System.out.println("RequestGeneratorCont: Check for valid event id state postings or rank BLOCKING.");
            } else if (result < 0) {
                System.err.println("ERROR RequestGeneratorCont: Check LOG for processing error messages.");
            }
            //
            rcgPC.closeProcessing();
        }
    }
    /** Overrides parent version.
     * Populates ChannelList with Channels found in file cache specified by 
     * configuration property <i>channelCacheFilename</i> otherwise creates a
     * new list with Channels active in the database on current date or date 
     * specified by the configuration property <i>channelLoadDate</i>.
     * If <i>isMaster=true</i>, the MasterChannelList is also set to 
     * the loaded ChannelList.
     */
    public boolean loadChannelList(boolean isMaster) {
        boolean status = super.loadChannelList(isMaster);  // load list according to props
     
        dumpCandidateList();  // dumps only if property is set
        return status;
    }
    
    /**
     * Determines if the dump candidate list.
     * @return true if dump candidate list, false otherwise.
     */
    private boolean isDumpCandidateList() {
        return props.getBoolean("dumpChannelGroup");
    }

    /**
     * Dump the candidate list if property is set.
     */
    private void dumpCandidateList() {
        if (isDumpCandidateList()) {
            dumpCandidateList(getChannelList());
        }
    }

    /**
     * Dump the candidate list.
     * 
     * @param chanList the channel list or null if none.
     * @see #isDumpCandidateList()
     */
    private void dumpCandidateList(final ChannelList chanList) {
        if (chanList != null) {
            System.out.println("-------- Candidate Channel List --------");
            System.out.println("NT STA CHL LC   LAT      LON      DEPTH");
            chanList.dump();
        }
    }

    /**
     * Start a timer thread that will reload the channel list at some interval.
     * @return
     */
    public boolean startReloadThread() {
            
        //final int MIN_SLEEP_SECS = 0;          // for testing
        final int MIN_SLEEP_SECS = 60*30;        // 30 min
        final int DEFAULT_SLEEP_SECS = 60*60*24; // 24 hrs
       
        // Schedule the reload thread if auto refresh property is true
        if (!props.getBoolean("autoRefreshChannelList")) return false;

        // getInt() returns -1 if property is undefined
        int sleepsecs = props.getInt("autoRefreshChannelListInterval");
        if (sleepsecs == -1) sleepsecs = DEFAULT_SLEEP_SECS; // undefined, default to 24hrs
        
        // sanity check on interval
         if (sleepsecs < MIN_SLEEP_SECS) {
             System.out.println("Warning: autoRefreshChannelListInterval too short");
             sleepsecs = MIN_SLEEP_SECS;
             System.out.println("         Resetting to minimum value of "+sleepsecs+" seconds");
        }

        // Create the thing to be scheduled
         // NOTE: these properties come from the Module and not the Controller props
        ChannelLoaderThread thread = new ChannelLoaderThread(this);
              
        // create the scheduler
        boolean isDaemon =true; // set daemon true, sudden death when main exits
        ThreadScheduler scheduler = new ThreadScheduler(thread, sleepsecs, isDaemon);
        scheduler.setVerbose(true);
        
        // force a reload immediately
        // Why? Because the list read at startup could be from a stale cache file.
        scheduler.runNow();  

        return true;
    }   
    /* ****************************************************************************
     * Internal class handles channel list reload thread 
     * **************************************************************************** */
    /**
     * This is a one-shot "Runnable" thread that does the following:
     * 1) reload channellist using the 'name', 'data' options
     * 2) if the cachefilename is defined, rewrite the cache file
     * 3) replace the current MasterChannelList (?)
     */
    private static class ChannelLoaderThread implements Runnable {      
      private final RequestGeneratorCont pc;

      public ChannelLoaderThread(RequestGeneratorCont procCtlr) {
        pc = procCtlr;
      }
      
      /** Work happens here. */
      public void run () {
                     
        BenchMark bm = new BenchMark();    // measures elapse time to do the work
        System.out.println ("ChannelLoaderThread starting @ "+ new Date().toString());
   
        // Re-read the list from the data source
        ChannelList newList = reloadChannelList();
        
         // Replace cache file
        if (newList != null) {
            // replace the channel list and write to cache
            // this will change it in the module also
            pc.setChannelList(newList, true, true);
            System.out.println("INFO: >>> Wrote new ChannelList to cache: " + ChannelList.getCacheFilename());
            if (pc.isDumpCandidateList()) {
                pc.dumpCandidateList(newList);
            }
        } else {
            bm.printTimeStamp("ChannelLoaderThread failed, time=");                
        }

        bm.printTimeStamp("ChannelLoaderThread done, time=");
        System.out.println("-----------------------------------------------------");
      }

      /**
       * Reloads named channel list from DataSource 
       * @return the channel list.
      */      
      public ChannelList reloadChannelList() {
          StringBuffer sb = new StringBuffer(512);
          sb.append("INFO: Re-reading Channels from data source: ").append(DataSource.getDbaseName());
          
          java.util.Date date = null;
          if (pc.props.isSpecified("channelLoadDate")) {  // is there a channel load date ?
            date = pc.props.getDateTime("channelLoadDate");
            sb.append(" loadDate: ").append(date);
          }
          
          String channelListName = pc.props.getChannelGroupName();
          sb.append("     list name: ").append(channelListName);
          
          System.out.println(sb.toString());
          
          // Don't use .smartLoad() it reads from cache :. doesn't refresh list
          //ChannelList chList = ChannelList.smartLoad(date, channelListName);
          ChannelList chList = ChannelList.readListByName(channelListName, date);
          if (chList == null) {
              System.out.println("Warning: reload failed, no list returned.");
              return null;
          }

          //   already done        ChannelList.setCacheFilename(cacheFileName);
          
          // create HashMap of channels in ChannelList, if not already one
          if (! chList.hasLookupMap()) chList.createLookupMap();
          System.out.println("INFO: >>> Reloaded ChannelList total channels: " + chList.size());

          return chList;
      }
    } // end of internal class    
}

