package org.trinet.apps;

import org.trinet.jiggle.*;
import java.awt.*;
//import java.io.File;
import java.io.*;
import org.trinet.jasi.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.Dimension;

import org.trinet.jasi.seed.*;
import org.trinet.util.WaveClient;

/**
 * SeedFileViewer.java
 * @author Doug Given
 * @version
 */

public class SeedSummary{

    public SeedSummary() {

    }
    public static void main (String args[])  {

  if (args.length < 1)	// insufficient args
  {
    System.out.println ("Usage: SeedSummary <file-name>");
    System.exit(0);
  }

        File file = new File(args[0]);

        System.out.println ("File: "+file.getPath());
  // returns a Waveform and adds it to the list
        ChannelableList wflist = SeedReader.getDataFromFile(file.getPath(), 0) ;

        Waveform wf[] = (Waveform[]) wflist.toArray(new Waveform[0]);
        for (int i = 0; i < wf.length; i++ ){
      System.out.println(wf[i].toString());
        }
  // test
/*	System.out.println("Copy file...");
  try {
  fileCopy (args[0], "seedtest.out");
  } catch (IOException e) {}
*/
    }

    /** Binary file copy. Returns count of bytes*/
    public static int fileCopy (String inFile, String outFile) throws IOException {
        File inputFile = new File(inFile);
        File outputFile = new File(outFile);

        FileInputStream in = new FileInputStream(inputFile);
        FileOutputStream out = new FileOutputStream(outputFile);
        int c;

        while ((c = in.read()) != -1) {
           out.write(c);
        }
        in.close();
        out.close();
  return c;
    }
}
