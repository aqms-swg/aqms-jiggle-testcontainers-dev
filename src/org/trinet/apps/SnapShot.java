package org.trinet.apps;

import org.trinet.jiggle.*;

import java.text.*;
import java.awt.*;
import java.util.*;
import java.io.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

import org.trinet.jasi.*;
import org.trinet.jdbc.*;
import org.trinet.util.TimeSpan;
import org.trinet.util.WaveClient;

/**
 * Make a snapshot of the first 'n' channels with phase picks. <p>
 * Usage: SnapShot [evid] [ntraces] [max_secs] [waveServer-file] [dbase-host] [dbase-name]
 */
// DEPRECATED ? Is this class used ?
public class SnapShot {

    public static MasterView   mv;      // the master view

    public static final int NTRACES = 15;

    public static int ntraces = NTRACES ;

    public static int maxSecs = 120;
    public static String waveServerFile = "";

    //static boolean debug = true;
    static boolean debug = false;

    public Dimension singlePanelSize = new Dimension(640, 60);

    public static String host;
    public static String dbasename;
    public static String defaulthost = "defhost";
    public static String defaultdbase = defaulthost+"db";

    public static void main(String args[]) {
        long evid = 0l;

        if (args.length <= 0) {
          System.out.println ("Usage: SnapShot [evid] [ntraces] [max_secs] [waveServer-file] [dbase-host] [dbase-name]");
          System.exit(0);
        }

        // event ID
        if (args.length > 0) {
          Long val = Long.valueOf(args[0]);
          evid = (long) val.longValue();
        }
        if (debug) System.out.println("Using evid "+ evid);

        // # of traces
        if (args.length > 1) {
          Integer val = Integer.valueOf(args[1]);
          ntraces = (int) val.intValue();
        }
      
        // max. seconds to plot
        if (args.length > 2) {
          Integer val = Integer.valueOf(args[2]);
          maxSecs = (int) val.intValue();
        }
        // get data from waveServer?
        if (args.length > 3) {
      
           waveServerFile = args[3];
      
           WaveClient waveClient = null;
      
           try {
               // Make a WaveClient
      
               System.out.println("Creating WaveClient using: "+waveServerFile);
      
               waveClient = WaveClient.createWaveClient().configureWaveClient(waveServerFile); // property file name
      
               int nservers = waveClient.numberOfServers();
               if (nservers <= 0) {
                   System.err.println("Error: no valid waveservers specified in file " + waveServerFile);
                   System.exit(-1);
               }
      
           }
           catch (Exception ex) {
               System.err.println(ex.toString());
               ex.printStackTrace();
           }
      
           if (waveClient != null) AbstractWaveform.setWaveDataSource(waveClient);
      
        }
      
        host = defaulthost;   // default
        if (args.length > 4) {
            host = args[4];
        }
      
        dbasename = host+"db";    // default
        if (args.length > 5) {
            dbasename = args[5];
        }
      
        System.out.println("Making db connection...");
        DataSource init = TestDataSource.create(host);  // make connection
        init.setWriteBackEnabled(true);
      
        SnapShot snapShot =  new SnapShot();
        Component view = snapShot.makeViewWithPhases(evid, ntraces, maxSecs);

        // make a frame
        JFrame frame = new JFrame("SnapShot of "+evid);
      
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });
      
        frame.getContentPane().add( view );       // add scroller to frame
      
        frame.pack();
        frame.setVisible(true);
      
        int height = 900;
        int width  = 640;
        frame.setSize(width, height); // must be done AFTER setVisible
      
    }

    public Component makeViewWithPhases(long evid, int ntraces, int maxSecs) { 

        double maxDist = 99999.;
      
        // make preOT time 15% of maxSec up to a max of 20 sec
        double preOT = maxSecs * 0.15 ;
        if (preOT > 20.0) preOT = 20.0;
        double postOT = maxSecs - preOT;
      
        // Load the Solution
        Solution sol = Solution.create().getById(evid);
      
        // no solution
        if (sol == null) return null;
      
        if (debug) System.out.println(": "+sol.toString());
      
        // add phases associate with this solution
        //sol.addPhases(Phase.create().getBySolution(sol));
        //sol.getPhases();
        sol.loadPhaseList();
        if (debug) sol.phaseList.dump();
      
        double ot   = sol.datetime.doubleValue();
        TimeSpan ts = new TimeSpan(ot - preOT, ot + postOT);
      
        // define the Master View
        MasterView mv = new MasterView();
        mv.setWaveformLoadMode(MasterView.LoadAllInForeground);
        mv.loadPrefMags = false;
        mv.defineByPhaseList(sol, maxDist, ntraces, ts);
      
        //mv.setLoadChannelData(true);
        //mv.loadChannelData();
        mv.distanceSort( );
      
        mv.loadWaveforms();
      
        return makeView(mv);
      
    }
      
    public Component makeView(long evid, int ntraces, int maxSecs) {
      

        if (debug) System.out.println("SnapShot: Making MasterView for evid = "+evid);
      
        // Make the "superset" MasterView, need to know what's available
        MasterView mv1 = new MasterView();
        mv1.phaseRetrieveFlag = true;  // getProperties().getBoolean("masterViewRetrievePhases", true);
        mv1.ampRetrieveFlag = true;
        mv1.codaRetrieveFlag = true;
        mv1.loadSpectralAmps = false;
        mv1.loadPeakGroundAmps = false;
        mv1.loadPrefMags = false;
      
        // Don't load waveforms or channel data until we've winnowed down to
        // channels with phases
        mv1.setWaveformLoadMode(MasterView.LoadNone);
        mv1.setLoadChannelData(false);
      
        boolean status = mv1.defineByDataSource(evid);
      
        if (!status) return null; // no such event
      
        if (mv1.wfvList.size() == 0) return new Label("No waveform views");       // bail if no wfViews
        if (debug) System.out.println("SnapShot: Total wfviews = "+mv1.wfvList.size());
      
        // make new MasterView w/ verticals only, then load the waveforms
        // MasterView mv = MasterView.vertOnly(mvBig, ntraces);       // make MasterView
      
        // Create MasterView of channels with phases
        MasterView mv2 = MasterView.withPhasesOnly(mv1) ;
        if (mv2.wfvList.size() == 0) {
            if (debug) System.out.println("SnapShot: No phases found.");
            return new Label("No waveforms have phases");     // bail if no wfViews
        }
        if (debug) System.out.println("SnapShot: Total wfviews w/ phases = "+
                     mv2.wfvList.size());
      
        // lookup lat/lon of the channels in the MasterView
        //mv2.setLoadChannelData(true);
        //mv2.loadChannelData();
        if (debug) System.out.println("SnapShot: Distance sorting...");
        mv2.distanceSort( );
      
        // limit to 'ntraces'
        MasterView mv = MasterView.subset(mv2, ntraces);
      
        if (debug) System.out.println("SnapShot: Loading waveforms... "+ mv.wfvList.size());
        mv.setWaveformLoadMode(MasterView.LoadAllInForeground);
      
        mv.loadWaveforms();
      
        return makeView(mv);
      
    }

    /**
    * Make a master view Panel. By default show the phase picks.
    */
    public Component makeView(MasterView mv) {
        return makeView(mv, true);
    }

    /**
    *  Make a master view Panel. If 'showPicks' is true show the phase picks
    * otherwise don't.
    */
    public Component makeView(MasterView mv, boolean showPhaseDescriptions) {
      
        if (debug) System.out.println("SnapShot: Making WFGroupPanel...");
      
        WFGroupPanel wfgPanel = new WFGroupPanel(mv) ;     // make Group Panel
      
        wfgPanel.showPhaseDescriptions(showPhaseDescriptions);
      
        //    Dimension wfDim = new Dimension(width, height);
      
        //if (debug) System.out.println("SinglePanelSize = "+ singlePanelSize.toString());
      
        wfgPanel.setSinglePanelSize(singlePanelSize);
      
        //limit time shown to maxSecs;
        if (mv.timeSpan.size() > maxSecs) {
      
            // 2/14/2000 changed to OT - 15 secs. Bogus waveform retrievals made
            // starting with earliest data shift all real data out of frame.
            double startTime = mv.getSelectedSolution().datetime.doubleValue() - 15.0;
      
            //double startTime = mv.timeSpan.getStart();
            wfgPanel.setTimeBounds(new TimeSpan(startTime, startTime+maxSecs));
        }
      
        // make another panel to support a border
        JPanel borderPanel = new JPanel();
        borderPanel.setBorder(BorderFactory.createRaisedBevelBorder());
      
        borderPanel.setLayout(new BorderLayout());
      
        borderPanel.add(wfgPanel, BorderLayout.CENTER);
      
        // make a label
      
        String str = mv.getSelectedSolution().toSummaryString();
        JLabel label = new JLabel(str);
        int fontSize = 18;
        label.setFont(new Font("Serif", Font.BOLD, fontSize));
      
        label.setBackground(Color.white);
        label.setForeground(Color.red);
      
        borderPanel.add(label, BorderLayout.NORTH);
      
        return borderPanel;
    }
      
      
    public void setSinglePanelSize(Dimension singlePanelSize) {
        this.singlePanelSize = singlePanelSize;
    }

} // end of class
