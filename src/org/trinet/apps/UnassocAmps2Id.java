package org.trinet.apps;

import java.io.*;
import java.sql.*;
import java.util.*;

import org.trinet.jdbc.datasources.DbaseConnectionDescription;
import org.trinet.jasi.*;
import org.trinet.jasi.TN.UnassocAmplitudeTN;
import org.trinet.util.*;

// Module associates unassocamp table amps with an event
public class UnassocAmps2Id {

    private static BenchMark bm = new BenchMark();

    public static final String DEFAULT_APP_NAME = "Uamps2Id";

    /** Model used to define channel time windows for unassoc amps.*/
    private ChannelTimeWindowModel model = new org.trinet.jasi.PowerLawTimeWindowModel();

    private static String [] ampTypes = new String[] {"PGA"};
    
    /** If writeAmps is false no event ampset update will be written to the db, for debugging, can set value with 'writeAmps' property */
    private boolean writeAmps = true;

    /** If true verbose output is written; set with the 'verbose' property. */
    private static boolean verbose = true;
    private static boolean debug = false;

    /** The instance properties object. */
    private SolutionEditorPropertyList props = null;

    public UnassocAmps2Id() {}
    
    public UnassocAmps2Id(SolutionEditorPropertyList props) {
          this.props = props;
    }
    
    /** Create a list of channel time windows for db event matching input Solution.*/
    private ChannelableList createCTWlist(Solution sol) throws Exception {
        if (model == null) {
            return null;
        }

        // Do this first so that if model is synched with solution time, a new channellist is created
        model.setSolution(sol);

        // Generate a ChannelTimeWindow list using the model and solution
        ChannelableList ctwList = (ChannelableList) model.getChannelTimeWindowList();
        if (ctwList != null) {
            bm.printTimeStamp("channel time windows defined,");
        }
        return ctwList;

    }

/*
1) getfileII accumulates CJ GroMoPacket files
2) Gmp2Db instance inserts CJ amps into UnassocAmp table (configure props "doAssociation=false" so setDoAssociation(false) in main setup).
   New app UnassocAmp2Id does association with evid - as single pass, what about "late or re-sent" amps, if old amps are still in table too?

3) PCS processing: Event id polled from PCS id posting after NewEvent transition.
   If primary system, proceed to process event id, else result id post.
   Result id posting (reject) when event magnitude is less than minimum set for amp association processing (< 2.95 or  < 3.45? Ml)
   Else wait for event age seconds seconds before proceeding, like 5 minutes age for CJ imported data to be in ampset after origin time?

4) For input event derive a ChannelTimeWindow object list from PowerLawTimeWindowModel configured for candidatelist of CJ network channels.

5) Revise each channel's ChannelTimeWindow start and end times to match the timespan returned by AbstractWaveform's
   getSWaveEnergyTimeSpan(originTimeSecs, epiCentralDistKm, mdepthKm) method.
     Waveform wf = AbstractWaveform.createDefaultWaveform();
   NOTE: may need to configure properties to reset default smp multiplier scalar!

6) Query db's unassoc amp table for the list of CJ network amps found between the min and max time range
   of all the ChanneTimeWindows where query results are sorted by sncl and amp datetime ascending
    UnassocAmplitudeTN uampTN = (UnassocAmplitudeTN) UnassocAmplitude.create();
    Collection col = uampTN.getByTimeNet(minTime,maxTime,"CJ");
    AmpList ampList = new AmpList(col);

7) Loop through list of candidate CTW sorted by distance range (within the magnitude energy cutoff range)
   Extract collection of amps returned by query those matching current candidate sncl.
     Loop through that channel's amp collection finding the max amp found inside the CTW S-window span.
     if any, associate the max amp found inside window with current event Solution object's amplist

   After looping through collection of CTW, save PGA amps from CJ channels associated with current event back to db,
   ampList commit, must set "type" and "subsource" ? (updates or creates an ampset for event, uses assoceveampset ampset and amp tables)

   For list parsed from results getBy and wrapped in Channelable list (AmpList) getAssociatedByChannel(Channel chan) ???
*/
    private AmpList getUnassocAmpList(Solution sol, ChannelableList ctwList) {

        int ctwCnt = ctwList.size(); // input list size
        if (ctwCnt == 0) return new AmpList(0);

        double otime = sol.getTime(); // use time for determining model window based upon phase travel times 
        ChannelTimeWindow ctw = null;
        TimeSpan ts = null;
        //NOTE: can set property "wfSmPWindowMultiplier", default multiplier value is 3.
        AbstractWaveform wf = (AbstractWaveform) AbstractWaveform.createDefaultWaveform();
        double minTime = Double.MAX_VALUE;
        double maxTime = 0.;
        for (int idx=0; idx < ctwCnt; idx++) {
            ctw = (ChannelTimeWindow) ctwList.get(idx);
            ts = ctw.getTimeSpan();
            if (ctw.getChannelObj().isHorizontal()) {
                // update ChannelTimeWindow time spans for S-Wave window
                ts = wf.getSWaveEnergyTimeSpan(otime, ctw.getChannelObj().getHorizontalDistance(), sol.getModelDepth());
            }
            else { // vertical?
                // update ChannelTimeWindow time span in also include the P-wave window ?
                ts = wf.getEnergyTimeSpan(otime, ctw.getChannelObj().getHorizontalDistance(), sol.getModelDepth());
            }
            if (ts.getStart() < minTime) minTime = ts.getStart();
            if (ts.getEnd() > maxTime) maxTime = ts.getEnd();
            ctw.setStart(ts.getStart());
            ctw.setEnd(ts.getEnd());
        }
        // query db for UnassocAmp rows whose peak amp time is inside the min, max time span for all channels
        UnassocAmplitudeTN uampTN = (UnassocAmplitudeTN) UnassocAmplitude.create();
        Collection collection = null;
        if ( props.isSpecified("importNetCode") ) {
            collection = uampTN.getByTimeNet( minTime, maxTime, props.getProperty("importNetCode") );
        }
        else if ( props.isSpecified("ampSubSource") ) {
          // Alternative collection requires Gmp2Db import to have been configured using property ampSubsource
          // since for GroMoPacket the default subsource is GMP, thus ampSubsource=GMP-CJ for which case
          collection = uampTN.getByTimeSubsource( minTime, maxTime, props.getProperty("ampSubsource") );
        }
        else { // all nets and subsources
            collection = uampTN.getByTime( minTime, maxTime );
        }

        // Create new list to hold peak amps for channel, if any
        AmpList ampListOut = AmpList.create(); 
        AmpList ampList = new AmpList(collection);
        int queryCnt = collection.size();
        System.out.println("Uamps2Id INFO: " + queryCnt + " unassoc amps found in window timespan: " +
                new TimeSpan(minTime,maxTime) + " for evid: " + sol.getId().longValue());

        if ( queryCnt == 0 ) return ampListOut; // empty list, none returned from query
    
        for (int idx=0; idx < ctwCnt; idx++) { // loop over all channel windows returned by model

            ctw = (ChannelTimeWindow) ctwList.get(idx);

            AmpList ampListCh = (AmpList) ampList.getAssociatedByChannel(ctw.getChannelObj());
            int chCnt = ampListCh.size();// total #amps db query returned for a channel window
            if (debug) System.out.println("Uamps2Id DEBUG: " + chCnt + " unassoc amps found in db for: " + ctw); 
            if ( chCnt == 0 ) continue; // none so go to next channel window

            for (int idx2=0; idx2 < ampTypes.length; idx2++) { // loop over configured amp types (e.g. PGA default)

                AmpList ampListChByType = (AmpList) ampListCh.getListByType(ampTypes[idx2]);
                int typeCnt = ampListChByType.size();// #channel amps in window of specified amp type
                if (debug) System.out.println("Uamps2Id DEBUG: " + typeCnt + " " + ampTypes[idx2] + " unassoc amps found for " +
                            ctw.getChannelObj().toDelimitedSeedNameString('.')); 
                if ( typeCnt == 0 ) {
                    continue; // none so go to next amptype, if any
                }

                Amplitude maxAmp = null;
                double maxValue = 0.;
                Amplitude amp = null;
                for (int idx3 = 0; idx3 < typeCnt; idx3++) { // loop through channel's amps of the active amptype to find the max
                    amp = (Amplitude) ampListChByType.get(idx3);
                    if ( ctw.contains(amp.getTime()) ) { // save amp only if it's inside the channel window's timespan
                        double value = Math.abs(amp.getValueDoubleValue());
                        if (value > maxValue) { // it's the new max amp for the channel
                            maxValue = value; 
                            maxAmp = amp;
                        }
                    }
                    else {
                      if (debug) {
                          double delta = amp.getTime() - ctw.getEnd(); 
                          if ( delta > 0. ) {
                              System.out.printf("Uamps2Id DEBUG: amp at: %s is %.2f secs beyond window %s\n", amp.getDateTime(), delta, ctw); 
                          }
                          else {
                              delta = ctw.getStart() - amp.getTime();
                              System.out.printf("Uamps2Id DEBUG: amp at: %s is %.2f secs before window %s\n", amp.getDateTime(), delta, ctw); 
                          }

                      }
                    }
                } // end of loop through a channel's amps of same type to find the max amp

                //  if above loop set a max amp for the channel time window, add it to the return amp list
                if (maxAmp != null) {
                    maxAmp.assign(sol);         // required for db AssocAmO row insert 
                    maxAmp.magnitude = null;    // to prevent AssocAmM rows happenning
                    Amplitude newAmp = Amplitude.create(); // 
                    newAmp.replace(maxAmp); // convert max UnassocAmp to Amp; sets hasChanged=true and fromDbase=false, so needs commit
                    ampListOut.add(newAmp, false);
                }

            } // end of loop over configured amptypes

        } // end of loop over all channel windows

        return ampListOut;

    }

    /**
     * Associate the unassoc amps, if any, for solution and model. Amps will be written to the db unless 'writeAmps' is set false.
     */
    public int associateAmps(Solution sol) throws Exception {

        bm.reset();

        if (sol == null) throw new NullPointerException("Input Solution parameter is null");
        if (model.getRequiresLocation() && !sol.hasLatLonZ()) {
             System.err.println("Uamps2Id ERROR: unable to generate channel time windows for evid: " + sol.getId().longValue() + ", has zero LAT,LON, check etype!");
             throw new Exception("Input Solution has no location.");
        }

        // Create CTW's using the default model
        ChannelableList ctwList = createCTWlist(sol);
        if (ctwList == null) throw new Exception("Null channel time window list.");

        int ctwCnt = ctwList.size();
        if (verbose) {
            System.out.println("Uamps2Id INFO: count of channel time windows to check for amps = "+ctwCnt);
        }
        long evid = sol.getId().longValue();
        if (debug || props.getBoolean("dumpChannelTimeWindows")) dumpChannelTimeWindowList(ctwList, evid);

        // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -AWW
        // look for peak unassoc amps inside each of the channel time windows
        AmpList ampList = getUnassocAmpList(sol, ctwList);
        // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -AWW

        bm.printTimeStamp("Calculations done,");
        System.out.println("Uamps2Id INFO: Associated "+ampList.size()+" amps.");
        if (debug) {
            for (int ii = 0; ii < ampList.size(); ii++) {
               System.out.println(((Amplitude) ampList.get(ii)).toNeatString());
            }
        }
    
        // write results to dbase.
        int count = ampList.size();
        if (count <= 0 ) {
          System.out.println("Uamps2Id INFO: "+count+" amps associated, check event distance, magnitude, or for missing amp imports");
        }
        else if (writeAmps) { // test for debugging w/o writting to the db
          System.out.println("Uamps2Id INFO: Writing "+count+" amps to database ...");
          final String ampSetType = "sm";
          ampList.setAmpSetType(ampSetType); // if we set this field then ampList commit does commitAmpSet(evid) and:
          // invalidate set isValid=0 for existing sets of same type as ampSetType associated with solution evid of first amplitude in set 
          //ampList.invalidateAmpSets(props.getProperty("subsource",EnvironmentInfo.getApplicationName()));
          //ampList.commit(); 
          ampList.commit(false); 
          System.out.printf("Uamps2Id INFO: Committed %d %s amps to ampsetid=%d for evid=%d%n",
                      ampList.size(), ampList.getAmpSetType(), ampList.getAmpSetId(), evid);
          DataSource.commit();
          bm.printTimeStamp("Database amp insert committed,");
        } else {
          System.out.println("Uamps2Id INFO: No db rows written, because property 'writeAmps=false'");
        }

        return count;
    }

    private void dumpChannelTimeWindowList(ChannelableList ctwList, long evid) {
        if (ctwList != null) {
            String dumpFile = props.getProperty("dumpChannelTimeWindowsPath");
            if ( dumpFile != null ) {
              try {
                dumpFile += GenericPropertyList.FILE_SEP + evid + "_" + "ctw.lis";
                BufferedWriter bw = new BufferedWriter(new FileWriter(new File(dumpFile)));
                for (int i = 0; i < ctwList.size(); i++) {
                    bw.write( ((ChannelTimeWindow) ctwList.get(i)).toString() );
                    bw.newLine();
                }
                bw.close();
              }
              catch (IOException ex) {
                  System.err.println("Uamps2Id IO error for filename: " + dumpFile);
                  ex.printStackTrace();
              }
            }
            else {
              for (int i = 0; i < ctwList.size(); i++) {
                  System.out.println( ((ChannelTimeWindow) ctwList.get(i)).toString() );
              }
            }
        }
    }


    public void setCandidateList(ChannelableList candidateListIn) {
        model.setCandidateList(candidateListIn);
    }

    public static void setDebug(boolean tf) {
        debug = tf;
        if (tf) verbose=true;
    }

    public static boolean getDebug() {
        return debug;
    }

    public static void setVerbose(boolean tf) {
        verbose = tf;
    }

    public static boolean getVerbose() {
        return verbose;
    }

    public void setModel(String modelname) {
        this.model = ChannelTimeWindowModel.getInstance(modelname);
    }

    public void setModel(ChannelTimeWindowModel model) {
        this.model = model;
    }
    
    public ChannelTimeWindowModel getModel() {
        return model;
    }
    
    public SolutionEditorPropertyList getProperties() {
        return props;
    }

    public boolean setProperties(String propFileName) {
        // reset() reads file, setup()
        SolutionEditorPropertyList newProps = new SolutionEditorPropertyList(); // the one with the most options 
        newProps.setFiletype(DEFAULT_APP_NAME);
        newProps.setFilename(propFileName);
        newProps.setRequiredProperties();
        boolean status = newProps.readUserProperties(); // read user property files
        setProperties(newProps);
        props.dumpProperties();
        return status;
    }
    
    public void setProperties(SolutionEditorPropertyList props) {

      this.props = props;

      setRequiredProperties();

      if (props.isSpecified("verbose") ) verbose = props.getBoolean("verbose");
      if (props.isSpecified("debug") ) debug = props.getBoolean("debug");
      

      // NOTE: first set timestamp zone and pattern in Logger before setFileName:
      if (props.isSpecified("logTimestampZone"))
          Logger.TIMESTAMP_ZONE = props.getProperty("logTimestampZone");

      if (props.isSpecified("logTimestampPattern"))
          Logger.TIMESTAMP_PATTERN = props.getProperty("logTimestampPattern");

      if (props.isSpecified("logFileName")) {
          Logger.setFilename(props.getProperty("logFileName"));
      }
      // Redirect subsequent output to the log file
      if (props.isSpecified("logRotationInterval")) {
          Logger.setRotationInterval(props.getProperty("logRotationInterval"));
      }
      else if (props.isSpecified("logfileDuration") ) {
          Logger.setRotationInterval(props.getProperty("logfileDuration"));
      }

      if ( props.isSpecified("model") )   // model is specified
          setModel(props.getProperty("model"));

      if (model == null) {
          System.err.println("Uamps2Id ERROR: channel time window model is null; model property value: "+props.getProperty("model"));
      } else { // whether default or specified - read model specific properties
          model.setProperties(props);
      }
      
      if (props.isSpecified("writeAmps"))
          writeAmps = props.getBoolean("writeAmps");

      if (props.isSpecified("ampTypes")) {
          ampTypes = props.getStringArray("ampTypes");
      }
    }

    public void setWriteAmps(boolean tf) {
        writeAmps = tf;
    }

    public void setRequiredProperties() {
        setRequiredProperties(props); 
    }

    public void setRequiredProperties(GenericPropertyList props) {
       // filetype this string is used by GenericPropertyList to build environmental variables
       // called <app>_USER_HOMEDIR and <app>_HOME. If the full path to the properties
       // file is not specified these env's are used as the path.
        props.setFiletype(DEFAULT_APP_NAME);

        if (!props.isSpecified("auth")) props.setProperty("auth", EnvironmentInfo.getNetworkCode());
        if (!props.isSpecified("subsource")) props.setProperty("subsource", DEFAULT_APP_NAME);
        if (!props.isSpecified("writeAmps")) props.setProperty("writeAmps", "true");
        if (!props.isSpecified("ampTypes")) props.setProperty("ampTypes", "PGA");
    }

    /** Run as stand-alone app, main method is not used by continuous pcs processor module, it uses method associateAmps(sol)*/
    public static void main(String[] args) {

        System.out.println("Uamps2Id INFO: Start at " + new DateTime());

        if (args.length <= 0) {        // no args
            System.err.println("Usage: UnassocAmps2Id <evid> [<property-file>]");
            System.err.println("Note: Full path/filename of property-file must be supplied.");
            System.err.println("      unless default <home> is defined with java command line option -DUAMPS2ID_USER_HOMEDIR=");
            System.exit(-1);
        }

        long evid =  0l;
        try {
            evid = Long.parseLong(args[0]);
        }
        catch (NumberFormatException ex) {
            System.err.println("Uamps2Id ERROR: Input evid cannot be parsed: " + args[0]);
            System.err.println(ex.getMessage());
            System.exit(-1);
        }

        String propfile = "Uamps2Id.props";
        if (args.length > 1) {
            propfile = args[1];
        }

        UnassocAmps2Id ua2id = new UnassocAmps2Id();
        EnvironmentInfo.setApplicationName(ua2id.DEFAULT_APP_NAME);

        File file = new File(propfile);
        if (file.isAbsolute()) {
            //initialize(DEFAULT_APP_NAME, String userFileName, String defaultFileName) 
            SolutionEditorPropertyList newProps = new SolutionEditorPropertyList(propfile, null);
            ua2id.setProperties(newProps);
        }
        else ua2id.setProperties(propfile);

        if (ua2id.model == null) {
            System.err.println("Uamps2Id ERROR: Channel time window model is null."); 
            ua2id.props.dumpProperties();
            System.exit(-1);
        }

        if (ua2id.getVerbose()) ua2id.props.dumpProperties();
        System.out.println( "Uamps2Id INFO: channel dbLookUp = " + Channel.getDbLookup());

        DbaseConnectionDescription dbd = ua2id.props.getDbaseDescription();
        if (!dbd.isValid()) {
            System.err.println("Uamps2Id ERROR: Property file does not contain a valid database connection description.");
            System.err.println(dbd.toString());
            ua2id.props.dumpProperties();
            System.exit(-1);
        }
        else System.out.println ("Uamps2Id INFO: Making connection to "+dbd.getURL());

        DataSource ds = new DataSource();

            // make connection
            if ( ! ds.set(dbd) ) {
              System.err.println("Uamps2Id ERROR: unable to open database connection.");
              System.err.println(dbd.toString());
              System.exit(-1);
            }

            bm.printTimeStamp("Uamps2Id: Db connection established,");

            // Load event info from db
            Solution sol = Solution.create().getById(evid);
            if (sol == null) {
               System.err.println("Uamps2Id ERROR: unable to create Solution for evid, also check your database for valid event : "+evid);
               ua2id.stop(-1);
            }
            if (ua2id.getVerbose()) System.out.println(sol.toSummaryString());

            // Setup channel list
            if ( ua2id.props.isSpecified("channelListReadOnlyLatLonZ") && ua2id.props.getBoolean("channelListReadOnlyLatLonZ") ) {
                Channel.setLoadOnlyLatLonZ();
                System.out.println("Uamps2Id INFO: channelListReadOnlyLatLonZ=true");
            }
            else {
                if (ua2id.getVerbose())
                    System.out.println( "Uamps2Id INFO: channel associated load settings: response=true, corrections=false, clipping=false" );
                Channel.setLoadResponse(true);
                Channel.setLoadKnownCorrections(false);
                Channel.setLoadClippingAmps(false);
            }
    
           ChannelableList chanList = null;

           // If cache file name is specified in props then load its channels
           String cacheFileName = ( ua2id.getProperties() == null) ?  null : ua2id.getProperties().getProperty("channelCacheFilename");
           if (cacheFileName != null && cacheFileName.trim().length() > 0) {
            cacheFileName = new java.io.File(propfile).getParent() + GenericPropertyList.FILE_SEP + cacheFileName;
            System.out.println("Uamps2Id INFO: loading MasterChannelList from cache file : "  + cacheFileName);
            MasterChannelList.set(ChannelList.readFromCache(cacheFileName));
          }

          chanList = MasterChannelList.get();
          System.out.println("Uamps2Id INFO: cache file channel list contains "+chanList.size()+" channels.");

          // no cache, so instead query db for a named group candidate channel list
          if (chanList == null || chanList.isEmpty()) {
            Channel ch = Channel.create();
            // Speed up channel data loading by limiting what you want
            Channel.setLoadClippingAmps(false); // clipping levels not used by model or filter
            Channel.setLoadKnownCorrections(false); // !!!!! don't need station magnitude ?
            Channel.setLoadResponse(true);
    
            String chanGroupName = ua2id.props.getChannelGroupName(); // is there one?
            System.out.println("Uamps2Id INFO: loading ChannelList from db for group name: "  + chanGroupName);
            chanList = ch.getNamedChannelList(chanGroupName);
            MasterChannelList.set(chanList);
            System.out.println("Uamps2Id INFO: channel list <" + chanGroupName + "> contains "+chanList.size()+" channels.");
          } 
        
          if (chanList == null || chanList.isEmpty()) {
            System.err.println("Uamps2Id ERROR: channel list is empty, check channelGroupName property? Exiting ...");
            ua2id.stop(-1);
          }
    
        ua2id.setCandidateList(chanList);


        try {
            ua2id.associateAmps(sol); // after everything is setup, do the amps
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        ua2id.bm.printTimeStamp("Uamps2Id INFO: program exiting,");
        ua2id.stop(0);
    }

    private static final void stop(int code) {
        DataSource.close(); 
        System.out.println("Uamps2Id INFO: Stop with exit code " + code + " at " + new DateTime());
        System.exit(code);
    }
}
