package org.trinet.apps;

import java.awt.*;

import org.trinet.jiggle.*;
import org.trinet.jasi.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.Dimension;

import org.trinet.util.WaveClient;
import org.trinet.filters.WAFilter;
/**
 * WAViewer.java
 * Display Wood-Anderson waveform for review of amp selection.
 * @author Doug Given
 */

public class WAViewer {

    static boolean debug = true;

    public WAViewer() { }


    public static void main(String args[]) {
        int evid = 0;
        int channelsPerPage = 10;

        if (args.length <= 0) {
          System.out.println("Usage: WAViewer <evid> [<channels/page>] (default= "+ channelsPerPage+")");
          System.exit(0);
        }

        if (args.length > 0) {
          Integer val = Integer.valueOf(args[0]);
          evid = (int) val.intValue();
        }

        if (args.length > 1) {
          Integer val = Integer.valueOf(args[1]);
          channelsPerPage = (int) val.intValue();

          if (channelsPerPage < 0 || channelsPerPage > 100) {
              System.out.println(" ! channels/page must be between 0 & 100. ["+
                channelsPerPage+"]");
              System.exit(0);
          }
        }

        System.out.println("Making connection...");
        DataSource init = TestDataSource.create();  // make connection
        init.setWriteBackEnabled(true);

        // station info
        MasterChannelList.smartLoad();

        if (debug) System.out.println("Making MasterView for evid = "+evid);

        // Make the "superset" MasterView
        MasterView mv = new MasterView();
        mv.setWaveformLoadMode(MasterView.Cache);

        int above = 50;
        int below = 50;
        mv.setCacheSize(above, below);

        mv.setAlignmentMode(MasterView.AlignOnTime);

        // read in the data!
        mv.defineByDataSource(evid);

        Solution sol = mv.getSelectedSolution();
        if (sol != null) {
          System.out.println("SOL: "+sol.toString());
          System.out.println("There are " + mv.getPhaseCount() +
                  " phases, "+ mv.getWFViewCount() + " time series"+
                  " and "+mv.getAmpCount() + " amps");
        } else {
          System.out.println("No dbase entry found for evid = "+evid);
          System.exit(0);
        }
        // Make graphics components

        if (debug) System.out.println("Creating GUI...");

        int height = 900;
        int width  = 640;

        // VirtScroller wfScroller = new VirtScroller(mv, channelsPerPage, true);
        final WFScroller wfScroller = new WFScroller(mv, true) ;     // make scroller

        // set WA filtering
        wfScroller.getWFGroupPanel().setFilter(new WAFilter());
        wfScroller.getWFGroupPanel().setShowAlternateWf(true);

        // make an empth Zoomable panel
        ZoomPanel zpanel = new ZoomPanel(mv);

        // enable filtering, default will be Butterworth. See: ZoomPanel
        // Use zpanel.zwfp.setFilter(FilterIF) to change the filter
        zpanel.setFilterEnabled(true);
        zpanel.zwfp.setFilter(new WAFilter());
        zpanel.zwfp.setShowAlternateWf(true);

        // Retain previously selected WFPanel if there is one,
        // if none default to the first WFPanel in the list
        WFView wfvSel = mv.masterWFViewModel.get();
        // none selected, use the 1st one in the scroller
        if (wfvSel == null && mv.getWFViewCount() > 0) {
          wfvSel = (WFView) mv.wfvList.get(0);
        }

        // Must reset selected WFPanel because PickingPanel and WFScroller are
        // new and must be notified (via listeners) of the selected WFPanel.  It might
        // be null if no data is loaded.
        if (wfvSel != null ) {
          ((ActiveWFPanel)wfScroller.groupPanel.getWFPanel(wfvSel)).setSelected(true);
          mv.masterWFViewModel.set(wfvSel);
          mv.masterWFWindowModel.setFullView(wfvSel.getWaveform());
        }

        // amps in the dbase have no distance attribute, calc. it
        sol.getAmpList().calcDistances(sol); // changed input arg type aww 06/11/2004
        SelectableReadingList amppane =
            new SelectableReadingList(mv, mv.getSelectedSolution(),
                SelectableReadingList.Amps);

        // set minsize or splitpane won't move
        zpanel.setMinimumSize(new Dimension(100, 100) );
        wfScroller.setMinimumSize(new Dimension(100, 100) );
        amppane.setMinimumSize(new Dimension(100, 100) );

        ///////////    TEST //////////////////
        wfScroller.setSecondsInViewport(60.0);

        // make a split pane with the zpanel and wfScroller
        JSplitPane waveSplit =
            new JSplitPane(JSplitPane.VERTICAL_SPLIT,
               false,       // don't repaint until resizing is done
               zpanel,      // top component
               wfScroller); // bottom component
        waveSplit.setOneTouchExpandable(true);

        JSplitPane mainSplit =
            new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
               false,       // don't repaint until resizing is done
               waveSplit,      // top component
               amppane); // bottom component
        waveSplit.setOneTouchExpandable(true);

        // make a main frame
        JFrame frame = new JFrame("Wood-Anderson: "+sol.toSummaryString());

        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });

        frame.getContentPane().add(mainSplit, BorderLayout.CENTER);     // add splitPane to frame
        frame.pack();
        frame.setVisible(true);

        frame.setSize(width, height); // must be done AFTER setVisible

        // put divider at 25/75% position
        waveSplit.setDividerLocation(0.5);
        mainSplit.setDividerLocation(0.25);

        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("WFView count        = "+ mv.getWFViewCount());
        System.out.println("masterWFPanelModel  = "+ mv.masterWFViewModel.countListeners());
        System.out.println("masterWFWindowModel = "+ mv.masterWFWindowModel.countListeners());
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++");
    }

} // EventViewer

