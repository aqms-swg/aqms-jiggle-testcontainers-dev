package org.trinet.apps;

import org.trinet.jasi.*;
import org.trinet.util.gazetteer.*;

/**
 * Dump "Where" info for this lat/lon
 *
 * Created: Tue Aug 15 17:13:54 2000
 *
 * @author Doug Given
 * @version
 */

public class Where  {

    public Where() { }

    public static void syntax() {
        System.out.println("Syntax: Where <lat> <min> <lon> <min>");
        System.out.println("Syntax: Where <lat> <lon>");
    }

    public static void main (String args[]) {

        /** The WHERE engine, returns info about close landmarks, etc.*/
        double dlat = 0.0, dlon = 0.0, z = 0.0;

        if (args.length == 4) {
            double lat  = Double.valueOf(args[0]).doubleValue();
            double latm = Double.valueOf(args[1]).doubleValue();
            double lon  = Double.valueOf(args[2]).doubleValue();
            double lonm = Double.valueOf(args[3]).doubleValue();

            dlat = lat + latm/60.0;
            dlon = -(Math.abs(lon) + lonm/60.0);

        } else if  (args.length == 2) {
            dlat = Double.valueOf(args[0]).doubleValue();
            dlon = Double.valueOf(args[1]).doubleValue();
        } else {
            syntax();
//            System.exit(0);
           dlat = 33.3427;
           dlon = -116.3117;
           z = 16.0;
        }

        TestDataSource.create("host","db","user","pass");      // make connection

        WhereIsEngine whereEngine =
            WhereIsEngine.create("org.trinet.util.gazetteer.TN.WheresFrom");
        whereEngine.setDistanceUnitsMiles();

        String whereString = whereEngine.where(dlat, dlon, z);

        System.out.println (whereString);

    }

}
