package org.trinet.filters;

import org.trinet.filters.FilterTypes;
import org.trinet.jasi.AbstractWaveform;
import org.trinet.jasi.Channel;
import org.trinet.jasi.Units;
import org.trinet.jasi.Waveform;
import org.trinet.jasi.WFSegment;
import org.trinet.util.FilterIF;
import org.trinet.util.WaveformFilterIF;

public class EHGFilter implements WaveformFilterIF {

    protected String description = "EHG-Filter";

    protected boolean copyInput = true;

    protected Waveform ehgWf = null;

    /** The segment being filtered. */
    protected WFSegment seg;

    /** The Waveform object to be filtered*/
    protected Waveform wfIn = null;

    public EHGFilter() { }

    public EHGFilter(Waveform ehgWf) {
        setEhgWf(ehgWf);
    }

    public EHGFilter(int type, int sampleRate) {
    }

    public void setEhgWf(Waveform ehgWf) {
        //System.out.println("DEBUG EHGFilter setting gain channel waveform=\n" + ehgWf); // getChannelObj().getSeedchan());
        this.ehgWf = ehgWf;
    }

    public Waveform filter(Waveform wf) {

      if (setWaveform(wf) == null) return null;

      Waveform wf2 = getWaveform();
      //System.out.println("DEBUG EHGFilter filter(wf) input wf:\n" + wf2.toString());

      // Filter each segment, otherwise time gaps would wreak havoc.
      // filter modifies data samples stored in waveform segment list
      WFSegment [] segA = wf2.getArray();
      if (segA == null || segA.length == 0) {
          System.err.println("EHGFilter ERROR: aborting filter, input wf has no timeseries!");
          return wf2; // no timeseries to filter, bail
      }

      if (ehgWf != null && ! ehgWf.hasTimeSeries()) {
          ehgWf.loadTimeSeries();
      }
      WFSegment []  ehgSegA = ehgWf.getArray();
      if (ehgSegA == null || ehgSegA.length == 0) {
          System.err.println("EHGFilter ERROR: aborting filter, gain channel has no timeseries!");
          return wf2; // no timeseries to filter, bail
      }

      if (ehgSegA.length != segA.length) {
        System.err.println("EHGFilter ERROR: aborting filter, gain channel segments != input wf segments:" + ehgSegA.length + " " + segA.length);
        return wf2;
      }
      if (ehgSegA.length > 1 || segA.length > 1) {
        System.err.println("EHGFilter WARNING: waveform has time tear, segment counts =" + ehgSegA.length + ":" + segA.length);
        //return wf2;
      }

      // =======================================================================
      // Loop over all WFSegments in list
      boolean wfSegFilterStatus = true;
      java.util.ArrayList newSegList = new java.util.ArrayList(wf2.getSegmentList().size());
      double startE = 0.;
      double endE = 0.;
      double startW = 0.;
      double endW = 0.;
      double startT = 0.;
      double endT = 0.;
      for (int i=0; i < segA.length; i++) {

        // get time span of corresponding segment of the ehgWf member of this class instance 
        WFSegment ehgSeg = ehgSegA[i];
        startE = ehgSeg.getEpochStart();
        endE   = ehgSeg.getEpochEnd();

        // get time span of corresponding input wf segment to filter (degain)
        WFSegment seg = segA[i];
        startW = seg.getEpochStart();
        endW   = seg.getEpochEnd();

        // set default timespan to be that of the ehg gain channel
        startT = startE;
        endT   = endE;

        // determine the overlap of the 2 WFSegment timespans
        if ( Math.abs(startW - startE) > (0.5*seg.getSampleInterval()) ) {
          startT = Math.max(startE, startW); // latest time
        }
        if ( Math.abs(endW - endE) > (0.5*seg.getSampleInterval()) ) {
          endT = Math.min(endE, endW); // earliest time
        }
        // if the timespans differ, extract the overlap into new WFSegments
        if ( startT != startE || endT != endE ) {
          ehgSeg = ehgSeg.getSubSegment(startT, endT);
          seg = seg.getSubSegment(startT, endT);
          System.out.println("EHGFilter: EHG/"+wf2.getChannelObj().getSeedchan()+
                  " timespans differ, for segment " +i+ " span reset to: "+seg.getTimeSpan());
        }

        // add the WFSegment to be filtered to a the new segment list
        newSegList.add(seg);

        // should now have same number of samples in input segments for filtering
        filterSegment(ehgSeg, seg);

        wfSegFilterStatus &= seg.isFiltered();

        if (! wfSegFilterStatus)  {
          System.err.println("EHGFilter FAILED: for segment idx: " + i);
          System.err.println("  wf:" + seg.toString() + "\n  ehg:" + ehgSeg.toString());
        }
      }

      // replace existing wf2 segment list with the list of filtered segments
      wf2.setSegmentList(newSegList, true);

      // unless overridden by subtype, setFilterName forces also
      // sets waveform "isFiltered()" boolean attribute TRUE.
      wf2.setFilterName(getDescription());

      // Set true only if filterSegment succeeded for all - what should default be, all or any? -aww
      wf2.setIsFiltered(wfSegFilterStatus);

      // recalculate bias, max, min, etc. for resulting waveform
      wf2.scanForBias();
      wf2.scanForAmps();

      // set units only after all is said and done
      //setUnits(wf2);

      //if (localLoad) ehgWf.unloadTimeSeries();

      return wf2;

    }

    protected void setWFSegment(WFSegment seg) {
      this.seg = seg;
      //setSampleRate(1.0/seg.getSampleInterval());
    }

    protected WFSegment filterSegment(WFSegment ehgWf, WFSegment seg) {

        // This is where the work is done with the ehgWf data!
        setWFSegment(seg);

        // filter time series
        if ( wfIn != null && wfIn.getAmpUnits() == Units.COUNTS) {
            String Gtype = "X";
            if (ehgWf.getChannelObj().getNet().equals("NM")) Gtype = "isis";
            else if (ehgWf.getChannelObj().getNet().equals("ET")) Gtype = "pda2";
            if( ! Gtype.equals("isis") && !Gtype.equals("pda2") ) { 
              System.err.println("EHGFilter ERROR: filterSegment, unrecognized gain type " + Gtype +
                      ",  only pda2/isis are supported, filter no-op");
              //seg.setIsFiltered(false);     // flag segment as unfiltered
              return seg;
            }

            float [] gs = ehgWf.getTimeSeries();
            float [] ds = seg.getTimeSeries();
            float rate =  (float) (1./getSps());
            if (gs.length != ds.length) {
                System.err.println("EHGFilter ERROR: filterSegment gain channel samples != data samples, " + gs.length + " != " + ds.length);
            }
            int n = ds.length;
            //System.out.println("DEBUG EHGFilter filterSegment " + seg.getChannelObj().toDelimitedSeedNameString() +
            //        " Gtype: "+ Gtype +", samples:" + n + ", rate:" + rate + "\n"); 

            // COMMENT OUT degain line below to get original wf timeseries returned!
            seg.setTimeSeries( degain(ds, gs, n, rate, Gtype) ); // ! Does the amp scaling calc here !
            //

            seg.setIsFiltered(true);       // flag segment as filtered

        } else {
            System.err.println("EHGFilter: illegal waveform units, check amp units of: " + seg.getChannelObj().toDelimitedSeedNameString());
            //seg.setIsFiltered(false);     // flag segment as unfiltered
        }

        // Remove bias: note 2nd arg 'scanIt' is 'false' because we don't yet want to recalc bias, max/min, etc.
        // which done in filter(wf) method after all segments are processed
        //seg.setTimeSeries( demean(seg.getTimeSeries()), false );

        return seg;

    }

    //public void setUnits(Waveform wf) {
    //    wf.setAmpUnits(Units.COUNTS);
    //}

    public Waveform getWaveform() {
      return (copyInput) ? AbstractWaveform.copyWf(wfIn) : wfIn;
    }
    public Waveform setWaveform(Waveform wf) {

      // reset current references
      wfIn = null;

      if (wf == null) {
        System.err.println("EHGFilter ERROR: cannot filter, null input waveform.");
        return null;
      }

      Channel chan = wf.getChannelObj(); // input wf channel
      if (chan == null) {
        System.err.println("EHGFilter ERROR: cannot filter, input waveform channel is null.");
        return null; 
      }

      if (ehgWf == null) {
        System.err.println("EHGFilter ERROR: cannot filter, no associated gain channel EHG waveform, it's null.");
        return null; 
      }

      if ( !isValidForChannel(chan) ) {
        System.err.println("EHGFilter ERROR: filter invalid for input wf channel " +
                chan.toDelimitedSeedNameString());
        return null;
      }

      // check for matching sample rate
      if ( ehgWf.getSampleRate() != wf.getSampleRate() ) {
        System.err.println("EHGFilter ERROR: input wf sample rate " + ehgWf.getSampleRate()  + " != " +
                ehgWf.getSampleRate() + " gain channel sample rate for " + chan.toDelimitedSeedNameString());
        return null;
      }

      //boolean localLoad = false;
      //if (ehgWf != null && ! ehgWf.hasTimeSeries()) {
      //    ehgWf.loadTimeSeries();
      //    if (eghWf.hasTimeSeries()) localLoad = true;
      //}
      // check for matching sample counts
      //if ( ehgWf.samplesInMemory() != wf.samplesInMemory() ) {
      //  System.err.println("EHGFilter ERROR: input wf samples " + wf.samplesInMemory() + " != " +
      //          ehgWf.samplesInMemory() + " gain channel samples for " + chan.toDelimitedSeedNameString());
      //  return null;
      //}

      // check for matching timespans
      //if ( ! ehgWf.getTimeSpan().contains(wf.getTimeSpan()) {
      if ( ! ehgWf.getTimeSpan().equals(wf.getTimeSpan()) ) {
        System.err.println("EHGFilter ERROR: input wf timespan " + wf.getTimeSpan() + " != " +
                ehgWf.getTimeSpan()  + " gain channel timespan for " + chan.toDelimitedSeedNameString());
        return null;
      }

      // check for legal waveform units
      if ( !unitsAreLegal(wf.getAmpUnits()) ) {
        System.err.println("EHGFilter ERROR: invalid wf amp units=" + Units.getString(wf.getAmpUnits()) + " for " +
                chan.toDelimitedSeedNameString());
        return null;
      }

      wfIn = wf; // alias this instance member to input reference

      //if (localLoad) ehgWf.unloadTimeSeries();

      return wfIn; // all OK
    }

    public boolean isValidForChannel(Channel chan) {

        if ( !chan.getSeedchan().substring(0,2).equals("EH") ) {
            System.err.println("EHGFilter INFO: cannot filter, input wf channel seedchan is non-EH.");
            return false; 
        }

        if ( ehgWf == null || !chan.sameNetworkAs(ehgWf) ||  !chan.sameStationAs(ehgWf) ) {
            System.err.println("EHGFilter INFO: cannot filter, input wf channel's network/station do not match those of gain channel EHG.");
            return false; 
        }

        return true;
    }

    protected boolean sampleRateIsLegal(double rate) {
        return (ehgWf != null && ehgWf.getSampleRate() == getSps());
    }

    public void copyInputWaveform(boolean tf) {
        copyInput = tf;
    }
    public boolean unitsAreLegal(Waveform wf) {
        return unitsAreLegal(wf.getAmpUnits());
    }
    public boolean unitsAreLegal(int units) {
        return (units == Units.COUNTS);
    }

    /** Return a filtered COPY of the passed object. */
    public Object filter(Object wave) {
        // only know how to filter Waveform objects so check.
        return (wave instanceof Waveform) ? filter( (Waveform) wave ) : null;
    }

    /** Create a filter of the type described by String 'type'. The valid values
     *  and meanings of 'type' is filter dependent.
     *  For example: "BANDPASS", "HIGHPASS", "LOWPASS", etc.*/
    public FilterIF getFilter(String type) {
        return ( FilterTypes.getType(type) ==  FilterTypes.EHG) ? this : null;
    }

    /** Create a filter of the type id corresponding to input value.*/ 
    public FilterIF getFilter(int type, int sampleRate) {
        return ( type == FilterTypes.EHG && sampleRate > 0 && sampleRate == getSampleRate() ) ? this : null;
    }

    /** Create a filter for timeseries of the given sample rate. This assumes
     *  that sample rate is the only significant parameter for the filter. */
    public FilterIF getFilter(int sampleRate) {
        return this;
    }

    /** Short string describing the filter. */
    public void setDescription(String str) {
        description = str;
    }

    /** Short string describing the filter. */
    public String getDescription() {
        return description;
    }

    public int getType() {
        return FilterTypes.EHG;
    }

    public void setType(int type) {
        // no-op
    }

    public int getSampleRate() {
        return (int) Math.round(getSps());
    }
    public void setSampleRate(int rate) {
        // no-op
    }

    public double getSps() {
        return (ehgWf == null) ? 0. : ehgWf.getSampleRate();
    }
    
    public int getOrder() {
        return 0;
    }
    public void setOrder(int norder) {
        // no-op
    }

    public boolean isReversed() {
        return false;
    }
    public void setReversed(boolean tf) {
        // no-op
    }
    public String toString() {
        return getDescription() + " " + " type: " + FilterTypes.EHG + " sps: " + getSampleRate();
    }

  /* 
  //----------------------------------------------------------------------------------
   From: degain_pda2.c Mitch Withers Jan 2000

   given two data vectors and the number of points
   return a new data vector that has been
   degained for pandaII

   Gain vector is checked for expected operation and we
   attempt to fix the amplified offset problem.
   But it ain't perfect, eh.

   fptr is a pointer to a vector of floats, the raw data
   gptr is a pointer to a vector of floats, the gain data
   n is the number of points (assumed the same between fptr, and gptr)
   tovolts is the number of volts/count
   rate is the sample rate in seconds per sample
   returns a pointer to a vector of floats in units of volts

   =================================================================================
   Added some explanatory comments January, 2013

   PandaII has 5 possible gain states.  A normal state and 4 steps.  Each step has
   a gain that is 8 times lower than the previous (i.e. the gain multiplier is 8 times
   greater).

   ISIS has only two states.  A normal and a single step.

   When the amplitude of the seismic channels reaches 99% full scale, the gain is
   automatically reduce and the gain channel goes one step higher to signal this
   change.  To remove this effect, we multiple the seismic data by 1, under
   normal circumstances, and by a larger value during periods of gain range.  These
   values and the voltage of the gain channel that corresponds to the particular
   step, are found in get_gain_scalar.h.

   The gain channel is permitted to go from the lowest state, to highest state
   immediately.  Once it increases to a higher step, it may not reduce to a lower
   step until after a preset time delay expires.  We have this delay set to 1 second.

   For PandaII, it may not move more than one gain step down after the time delay.
   And for each down step, it must remain at each step for the preset delay (1 second).
   
  */

  /* gain ranging information used by get_gain.c (withers 4/98) */
  private static float [] PandaII_low_level = new float [] {-5.f, 3.f, 1.f, 1.f, 3.f};
  private static float [] PandaII_high_level = new float [] {-3.f, 1.f, 1.f, 3.f, 5.f};
  private static float [] PandaII_gain = new float [] {1.f,8.f, 64.f, 512.f, 4096.f};
  private static float [] Isis_low_level = new float [] {-5.f, 0.f};                /* a guess */
  private static float [] Isis_high_level = new float [] {0.f, 5.f};                /* a guess */
  private static float [] Isis_gain = new float [] {1.f, 31.6f}; 
  private static float Min_G_time = 1.f;
  private static float ZERO = 0.001f;
  private static float TO_VOLTS = 0.0024414f;

  private float [] degain(float [] fptr, float [] gptr, int n, float rate, String Gtype) {


      int bogus = 0;
      int tmpN = 0;
      int counter = 0;

      float prev_gain = 0.f;
      float yy = 0.f;
      float yyy = 0.f;
      float tmpmean = 0.f;
      float dgglitch = 0.f;
      float maxg = 0.f;
      float ming = 0.f;
      float predicted = 0.f;

      float [] out_data = new float[n];
      float [] clean_gain = new float[n];
      float [] tmp_gain_vec = new float[n];

      float current_g = 1.f;

      int Min_G_pts = Math.round(Min_G_time/rate); // samples in 1 second

      clean_gain[0] = 1.f; // Init first value, default multiplier?

      /* get the multipliers based on the input gain vector */
      for (int i=1; i<n; i++) tmp_gain_vec[i] = getGainScalar(Gtype, gptr[i]);

      /* check the gain vector for conformance with the preset delay mentioned above */
      for (int i=1; i<n; i++) {
     
        bogus = 0;

        prev_gain = current_g;
        current_g = tmp_gain_vec[i];
      
        yy = current_g - prev_gain;

        /* if the difference is less than 0, we potentially have a down step */
        if ( yy < 0.f ) {
          /* if its pandaII, it can be only a single down step */
          if( Gtype.equals("pda2") ) {
            if( Math.abs(yy) > 8.f * current_g )
              bogus = 1;
          }
          /* if it hasn't been at least Min_G_pts since the last up step, this is bogus */
          if (counter < Min_G_pts) bogus = 1;
        }
        /* if the difference is greater than 0, we have an upstep */
        if( yy > 0.f ){
          /* make sure it remains an upstep for at least Min_G_pts */
          for (int j=0; j<Min_G_pts; j++){
            yyy = tmp_gain_vec[i+j] - current_g;
            if ( yyy < 0.f ) bogus = 1;
          }  
        }
      
        /* if we found any bogus gain points, ignore them and set it to the previous gain step */
        if ( bogus == 1 ) {
          clean_gain[i] = prev_gain;
          current_g = prev_gain;
        }
        else clean_gain[i] = current_g;
      
        yy = prev_gain - current_g;
       
        /* increment or reset the counter depending on whether its stepped up or down (or stayed the same)*/
        if ( yy <= 0.f ) {
          counter++;
        }
        else counter = 0;

      }
      
      maxg = 65536.f;
      ming = -65536.f;

      for (int i=1; i<n; i++) {
        if ( clean_gain[i] < maxg ) maxg = clean_gain[i];
        if ( clean_gain[i] > ming ) ming = clean_gain[i];
      }

      dgglitch = ((Math.abs(maxg) - Math.abs(ming)) * 0.1f);

      /* When gain ranging occurs, any offsets are also multiplied by the new gain.
         So you have to remove the mean for each individual gain step, let you have
         offsets of different values in the time series */

      /* Gain ranging occurs in less time than the sample rate of 0.01 seconds.  So if we're
         lucky, no points are in the middle of a gain range.  If we're not lucky, at most
         one point is affected and we have no idea what the real value of that point is.
         So we just set it to half the difference of the nearest neighbors.
         This is what we're calling a degain glitch though its not really a glitch */
       
      for (int i=1; i<n; i++) {

        if ( Math.abs(clean_gain[i] - clean_gain[i-1]) > ZERO ) {

          tmpN = 1;
          tmpmean = 0.0f;

          /* find the mean and the length of the gain step */
          while ( Math.abs( clean_gain[i+tmpN] - clean_gain[i+tmpN-1]) < ZERO && (i+tmpN) < n-1 ) {
            tmpmean += fptr[i+tmpN];
            tmpN++;
          }
          /* normalize the sum by the number of points in the gain step */
          tmpmean = tmpmean/(float)tmpN;

          if (tmpN > 0) {
            /* remove the mean for this gain step */
            for (int j=i; j<i+tmpN; j++) out_data[j] = clean_gain[j] * (fptr[j] - tmpmean);

            /* check for degain glitch */
            predicted = (float) (out_data[i-1] + out_data[i+1])/2.0f ;
            /* if we find one, report it and smooth it */
            if ( Math.abs(predicted - out_data[i]) > dgglitch ) {
                System.err.println("EHGFilter WARNING: probable degain glitch; changing " + out_data[i] + " to " + predicted + " at point " + i);
                out_data[i] = predicted;
            }
            i += tmpN;
          }
          else {
            out_data[i] = clean_gain[i] * fptr[i];
          }
        }
        else{
          out_data[i] = clean_gain[i] * fptr[i];
        }
      }

      return out_data;
  }

  /**
     get_gain_scalar                                    Mitch Withers 4/1998
     subroutine to convert gain channel voltage into a scalar multiplier.
     required inputs:
       char Gtype       isis or pda2 or fba
       float x          single gain channel point
     returns:
       float y          single scalar point
      usage: y = get_gain_scalar(char *Gtype, float x);
  */
     
  private float getGainScalar(String Gtype, float gx ) {

      float gy = 0.f;

      /* x = gx*c; don't do this, keep it in counts */
      float x = gx*TO_VOLTS;

      if ( Gtype.equals("pda2") ) {
     
          if (x <= PandaII_high_level[4] && x > PandaII_low_level[4])
            gy = PandaII_gain[4];
     
          else if (x <= PandaII_high_level[3] && x > PandaII_low_level[3])
            gy = PandaII_gain[3];
     
          else if (x <= PandaII_high_level[2] && x > PandaII_low_level[2])
            gy = PandaII_gain[2];
     
          else if (x <= PandaII_high_level[1] && x > PandaII_low_level[1])
            gy = PandaII_gain[1];
     
          else if (x <= PandaII_high_level[0] && x > PandaII_low_level[0])
            gy = PandaII_gain[0];
     
          else gy = 1.0f;
     
      }
      else if ( Gtype.equals("isis") ) { 
          if (x <= Isis_high_level[1] && x > Isis_low_level[1])
            gy = Isis_gain[1];
     
          else if (x <= Isis_high_level[0] && x > Isis_low_level[0])
            gy = Isis_gain[0];
     
          else
            gy = 1.0f;
     
      }
      else gy = 1.0f;
     
      return gy;
  }

    
}
