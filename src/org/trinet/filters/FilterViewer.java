package org.trinet.filters;

import java.awt.*;

import org.trinet.jiggle.*;
import org.trinet.jasi.*;
import org.trinet.util.*;

import javax.swing.*;
import java.awt.event.*;
import java.awt.Dimension;


/**
 * FilterViewer.java
 * *
 * GUI for interactive viewing of waveforms and parameters for a single event.
 *
 * Created: Tue Nov  2 14:01:02 1999
 *
 * @author Doug Given
 * @version
 */

public class FilterViewer {

  protected static boolean debug = true;
  //protected static boolean useWaveserver = false;

  public FilterViewer() { }


  public static void main(String args[])
  {
//    int wfid = 124285176;  // CI.PHL.HLN  80 sps <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
//    int wfid = 124285178;  // CI.PHL.HHN <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    int wfid = 124286198 ;   //  124286198   CI SIO   HLE 01 (HLE)  SCDC 100.00    31232 2

    if (args.length <= 0) // no args
    {
      System.out.println
      ("Usage: FilterViewer <wfid> ");

    }

    if (args.length > 0) {

      Integer val = Integer.valueOf(args[0]);
      wfid = (int) val.intValue();
    }

    System.out.println ("Making connection...");
//  new TNDataSource();  // make connection
//  new TestDataSource("serverma", "databasema");  // make connection
    TestDataSource.create("serverk2", "databasek2");  // make connection

    BenchMark bm = new BenchMark ();

    if (debug) System.out.println ("Getting waveform for wfid = "+wfid);
    Wavelet rawWf = Wavelet.create();

    rawWf = rawWf.getByWaveformId(wfid);
    if (rawWf == null) {
      System.err.println ("Waveform retreival failed.");
      System.exit (0);
    }

    // must look up the gain
    Channel ch = rawWf.getChannelObj(); 
    rawWf.setChannelObjData(ch.lookUp(ch, rawWf.getLookUpDate()));

    System.err.println ("Fetching timeseries for: "+rawWf.getChannelObj().toDumpString());
    System.err.println (rawWf.toString());

    if (!rawWf.loadTimeSeries()) {
      System.err.println ("Timeseries retreival failed.");
      System.exit (0);
    } else {
      System.out.println ("Got waveform: nsegs =  "+ rawWf.getSegmentList().size() );
      System.out.println ("Actual samps   = "+ rawWf.samplesInMemory() );
      System.out.println ("max = "+ rawWf.getMaxAmp());
      System.out.println ("min = "+ rawWf.getMinAmp());
      System.out.println ("bias= "+ rawWf.getBias());
      System.out.println ("gain= "+ rawWf.getChannelObj().getGain(rawWf.getLookUpDate()).toString() ); // should this do a db lookUp ?
    }

    // create wfviews
    Waveform waWf    = new WAFilter().filter(AbstractWaveform.copyWf(rawWf));
    Waveform accelWf = new AccelFilter().filter(AbstractWaveform.copyWf(rawWf));
    Waveform velWf   = new VelocityFilter().filter(AbstractWaveform.copyWf(rawWf));
    Waveform dispWf  = new DisplacementFilter().filter(AbstractWaveform.copyWf(rawWf));
    Waveform sp03Wf  = new RSAFilter(FilterTypes.SP03, 0).filter(AbstractWaveform.copyWf(rawWf));
    Waveform sp10Wf  = new RSAFilter(FilterTypes.SP10, 0).filter(AbstractWaveform.copyWf(rawWf));
    Waveform sp30Wf  = new RSAFilter(FilterTypes.SP30, 0).filter(AbstractWaveform.copyWf(rawWf));

    bm.print("All filters run in ");

    debugAmp(rawWf);      // debug
    debugAmp(waWf);       // debug
    debugAmp(accelWf);    // debug
    debugAmp(velWf);      // debug
    debugAmp(dispWf);      // debug
    debugAmp(sp03Wf);    // debug
    debugAmp(sp10Wf);    // debug
    debugAmp(sp30Wf);    // debug

    System.out.println ("++++++++ Whole Waveform +++++++");
    System.out.println ("rawWf   = "+ rawWf.getPeakAmplitude().getValue().doubleValue());
    System.out.println ("waWf    = "+ waWf.getPeakAmplitude().getValue().doubleValue());
    System.out.println ("accelWf = "+ accelWf.getPeakAmplitude().getValue().doubleValue());
    System.out.println ("velWf   = "+ velWf.getPeakAmplitude().getValue().doubleValue());
    System.out.println ("dispWf  = "+ dispWf.getPeakAmplitude().getValue().doubleValue());
    System.out.println ("sp03Wf  = "+ sp03Wf.getPeakAmplitude().getValue().doubleValue());
    System.out.println ("sp10Wf  = "+ sp10Wf.getPeakAmplitude().getValue().doubleValue());
    System.out.println ("sp30Wf  = "+ sp30Wf.getPeakAmplitude().getValue().doubleValue());
    System.out.println ("+++++++++++++++");

    double start = rawWf.getStart().doubleValue() + 20.0;
    double end = start + 60.0;

    System.out.println ("++++++++ Windowed +++++++");
    System.out.println ("rawWf   = "+ rawWf.getPeakAmplitude(start, end).getValue().doubleValue());
    System.out.println ("waWf    = "+ waWf.getPeakAmplitude(start, end).getValue().doubleValue());
    System.out.println ("accelWf = "+ accelWf.getPeakAmplitude(start, end).getValue().doubleValue());
    System.out.println ("velWf   = "+ velWf.getPeakAmplitude(start, end).getValue().doubleValue());
    System.out.println ("dispWf  = "+ dispWf.getPeakAmplitude(start, end).getValue().doubleValue());
    System.out.println ("sp03Wf  = "+ sp03Wf.getPeakAmplitude(start, end).getValue().doubleValue());
    System.out.println ("sp10Wf  = "+ sp10Wf.getPeakAmplitude(start, end).getValue().doubleValue());
    System.out.println ("sp30Wf  = "+ sp30Wf.getPeakAmplitude(start, end).getValue().doubleValue());
    System.out.println ("+++++++++++++++");

// ////////////////////////////////////////////////////////
    boolean doGraphics = true;

    if (doGraphics) {
      // Make the MasterView
      MasterView mv = new MasterView();
      mv.setAlignmentMode(MasterView.AlignOnTime);

      // add wfviews
      mv.addWFView(new WFView(rawWf));
      mv.addWFView(new WFView(waWf));
      mv.addWFView(new WFView(accelWf));
      mv.addWFView(new WFView(velWf));
      mv.addWFView(new WFView(dispWf));
      mv.addWFView(new WFView(sp03Wf));
      mv.addWFView(new WFView(sp10Wf));
      mv.addWFView(new WFView(sp30Wf));

// Make graphics components

      if (debug) System.out.println ("Creating GUI...");

      int height = 900;
      int width  = 640;

      // VirtScroller wfScroller = new VirtScroller(mv, channelsPerPage, true);
      final WFScroller wfScroller = new WFScroller(mv, true) ;    // make scroller

      // make an empty Zoomable panel
      ZoomPanel zpanel = new ZoomPanel(mv);
//    PickingPanel zpanel = new PickingPanel(mv);

      // enable filtering, default will be Butterworth. See: ZoomPanel
      // Use zpanel.zwfp.setFilter(FilterIF) to change the filter
      zpanel.setFilterEnabled(true);

      // Retain previously selected WFPanel if there is one,
      // if none default to the first WFPanel in the list
      WFView wfvSel = mv.masterWFViewModel.get();
      // none selected, use the 1st one in the scroller
      if (wfvSel == null && mv.getWFViewCount() > 0) {
        wfvSel = (WFView) mv.wfvList.get(0);
      }

      // Must reset selected WFPanel because PickingPanel and WFScroller are
      // new and must be notified (via listeners) of the selected WFPanel.  It might
      // be null if no data is loaded.
      if (wfvSel != null ) {
        ((ActiveWFPanel)wfScroller.groupPanel.getWFPanel(wfvSel)).setSelected(true);
        mv.masterWFViewModel.set(wfvSel);
        mv.masterWFWindowModel.setFullView(wfvSel.getWaveform());
      }

      zpanel.setMinimumSize(new Dimension(300, 100) );
      wfScroller.setMinimumSize(new Dimension(300, 100) );

      ///////////    TEST //////////////////
      //wfScroller.setSecondsInViewport(60.0);
      wfScroller.setShowFullTime(true);

      // make a split pane with the zpanel and wfScroller
      JSplitPane split =
          new JSplitPane(JSplitPane.VERTICAL_SPLIT,
          false,       // don't repaint until resizing is done
          zpanel,      // top component
          wfScroller); // bottom component

// make a main frame
      JFrame frame = new JFrame("Waveform number "+wfid);

      frame.addWindowListener(new WindowAdapter() {
        public void windowClosing(WindowEvent e) {System.exit(0);}
      });
      frame.getContentPane().add(split, BorderLayout.CENTER);    // add splitPane to frame

// Add the channelname finder
      JPanel finderPanel = new JPanel();
      finderPanel.add(new JLabel("Find: "));
      finderPanel.add(new ChannelFinderTextField(mv));
      frame.getContentPane().add(finderPanel, BorderLayout.SOUTH);

// ///////////////////////////////

      frame.pack();
      frame.setVisible(true);

      frame.setSize(width, height); // must be done AFTER setVisible

      // put divider at 25/75% position
      split.setOneTouchExpandable(true);
      split.setDividerLocation(0.25);

      //debug

      System.out.println ("++++++++++++++++++++++++++++++++++++++++++++++++++++");
      System.out.println ("WFView count        = "+ mv.getWFViewCount());
      System.out.println ("masterWFPanelModel  = "+ mv.masterWFViewModel.countListeners());
      System.out.println ("masterWFWindowModel = "+ mv.masterWFWindowModel.countListeners());
      System.out.println ("++++++++++++++++++++++++++++++++++++++++++++++++++++");

    }
  }
  // data dump for debugging
  public static void debugAmp(Waveform wfx) {

    if (wfx == null) {
      System.out.println ( " ----------- Dump ");
      System.out.println ( " Waveform is null. ");
      return;
    }
    // after filtering
    System.out.println ( " ----------- Dump "+ wfx.getChannelObj().toDelimitedSeedNameString(" "));
    System.out.println (" Filter Type = "+wfx.getFilterName());

    System.out.println ("max = "+ wfx.getMaxAmp() );
    System.out.println ("min = "+ wfx.getMinAmp() );

  }
} // EventViewer
