package org.trinet.filters;
import org.trinet.jasi.*;
import org.trinet.util.*;

public class WAHighPassFilter extends WAFilter {

    public WAHighPassFilter() {
        this(FilterTypes.WOOD_ANDERSON_HP, 0);
    }
  
    public WAHighPassFilter(int sampleRate) {
        this(FilterTypes.WOOD_ANDERSON_HP, sampleRate);
    }
  
    public WAHighPassFilter(int type, int sampleRate) {
        setApplyTaper(WAFilter.DEFAULT_APPLY_TAPER);
        setSampleRate(sampleRate);
        setDescription(WAFilter.FILTER_NAME+"_HP_.5_4");
        this.type = FilterTypes.WOOD_ANDERSON_HP; // input type not relevant only one type 
    }
  
    public FilterIF getFilter(String type) {
        return new WAHighPassFilter();
    }
  
    public FilterIF getFilter(int sampleRate) {
        WAHighPassFilter f = (WAHighPassFilter) this.clone(); // uses current type settings
        f.setSampleRate(sampleRate);
        return f; 
    }
  
    public void setSampleRate(int sampleRate) {
        super.setSampleRate(sampleRate);
        if (sampleRate != 0) {
          if (noiseFilter == null) noiseFilter = ButterworthFilterSMC.createHighpass(sampleRate, 0.50, 4, true);
          else noiseFilter.setSampleRate(sampleRate);
        }
    }

    public FilterIF getFilter(int type, int sampleRate) {
        return new WAHighPassFilter(type, sampleRate);
    }
  
    public Waveform filter(Waveform wf) {
        if (super.setWaveform(wf) == null) return null;
        if (noiseFilter == null) {
            noiseFilter = ButterworthFilterSMC.createHighpass((int)wf.getSampleRate(), 0.50, 4, true);
            noiseFilter.copyInputWaveform(false);
        }
        // Note for jiggle need to copy the input wf else original is changed (ie. in wfview in masterview -aww 02/28/2007)
        //return super.filter(noiseFilter.filter(super.getWaveform()));
        Waveform filteredWf = noiseFilter.filter(super.filter(super.getWaveform()));
        filteredWf.setFilterName(this.getDescription());
        return filteredWf;
    }

} // end of class
