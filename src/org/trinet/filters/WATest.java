package org.trinet.filters;

import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import org.trinet.jiggle.*;
import org.trinet.jasi.*;

public class WATest {

     public WATest() { }


// ////////////////
/**
 * Main for testing
 */
    public static void main(String args[])
    {
    /** Model of MVC for cursorLocLabel */
//    CursorLocModel cursorLocModel1 = new CursorLocModel();
//    CursorLocModel cursorLocModel2 = new CursorLocModel();
//    CursorLocModel cursorLocModel3 = new CursorLocModel();
//    CursorLocModel cursorLocModel4 = new CursorLocModel();

        System.out.println ("Making connection...");
        TestDataSource.create();  // make connection

//        int wfid = 10014410;        // 100 sps
//        int wfid = 10014230;        // 100 sps
//        int wfid = 46056227;         // BOR HHE
     int wfid = 46056785; // CI.FON.HLE
     Wavelet rawWf = Wavelet.create();

     rawWf = rawWf.getByWaveformId(wfid);

     if (rawWf == null) {
       System.err.println ("Waveform retreival failed.");
       System.exit (0);
     }

     System.err.println ("Fetching waveform: "+rawWf.getChannelObj().toString());

     if (!rawWf.loadTimeSeries()) {
       System.err.println ("Timeseries retreival failed.");
       System.exit (0);
     }

     // must look up the gain
     rawWf.setChannelObjData(Channel.create().lookUp(rawWf.getChannelObj()));

     System.out.println ("channel= "+rawWf.getChannelObj()+
                         "  gain= "+rawWf.getChannelObj().getCurrentGain().toString());

     System.out.println ("Full  noise scan = "+rawWf.scanForNoiseLevel());
     double start = rawWf.getEpochStart();
     double end   = start + 10;
     System.out.println ("10sec noise scan = "+rawWf.scanForNoiseLevel(start, end));

     //
//     Waveform waWf = AbstractWaveform.copyWf(wfx);
//     AbstractWaveformFilter wafilter = new WAFilter();
//     waWf = wafilter.filter(waWf);

     Waveform waWf = new WAFilter().filter(AbstractWaveform.copyWf(rawWf));
     Waveform velWf = new VelocityFilter().filter(AbstractWaveform.copyWf(rawWf));
     Waveform accelWf = new AccelFilter().filter(AbstractWaveform.copyWf(rawWf));
     Waveform sp03Wf = new RSAFilter(FilterTypes.SP03, 0).filter(AbstractWaveform.copyWf(rawWf));
     Waveform sp10Wf = new RSAFilter(FilterTypes.SP10, 0).filter(AbstractWaveform.copyWf(rawWf));
     Waveform sp30Wf = new RSAFilter(FilterTypes.SP30, 0).filter(AbstractWaveform.copyWf(rawWf));

      AbstractWaveformFilter.debugDump(rawWf);    // debug
      AbstractWaveformFilter.debugDump(waWf);     // debug
      AbstractWaveformFilter.debugDump(velWf);    // debug
      AbstractWaveformFilter.debugDump(accelWf);  // debug
      AbstractWaveformFilter.debugDump(sp03Wf);   // debug
      AbstractWaveformFilter.debugDump(sp10Wf);   // debug
      AbstractWaveformFilter.debugDump(sp30Wf);   // debug
     // make a main frame
      JFrame frame = new JFrame("Filter Test"+waWf.toString());

       Box box0 = Box.createVerticalBox();

// create the graphics
       box0.add(makePanel(rawWf, "Raw waveform "+rawWf.getPeakAmplitude().value.toString()));
       box0.add(makePanel(waWf,  "WA waveform "+waWf.getPeakAmplitude().value.toString()));
       box0.add(makePanel(velWf, "Velocity "+velWf.getPeakAmplitude().value.toString()));
       box0.add(makePanel(accelWf, "Acceleration "+accelWf.getPeakAmplitude().value.toString()));
       box0.add(makePanel(sp03Wf, "RSA SP03 "+sp03Wf.getPeakAmplitude().value.toString()));
       box0.add(makePanel(sp10Wf, "RSA SP10 "+sp10Wf.getPeakAmplitude().value.toString()));
       box0.add(makePanel(sp30Wf, "RSA SP30 "+sp30Wf.getPeakAmplitude().value.toString()));

       frame.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));

        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });

           frame.getContentPane().add(box0);

        frame.pack();
        frame.setVisible(true);

        }

// ///////////
         static JPanel makePanel (Waveform wf, String text) {
          JPanel jpanel = new JPanel(new BorderLayout());
          ActiveWFPanel panel = new ActiveWFPanel(new WFView(wf));
          CursorLocModel cursorLocModel = new CursorLocModel();
          panel.setCursorLocModel(cursorLocModel);
          // cursor position labels
          CursorLocPanel cursorPanel = new CursorLocPanel(cursorLocModel);
          cursorPanel.setAmpFormat(wf.getAmpUnits(), wf.getMaxAmp());
          jpanel.add(panel, BorderLayout.CENTER);
          JLabel label = new JLabel(text);
          JPanel bottomPanel =new JPanel();  // flow layout

          bottomPanel.add(cursorPanel);
          bottomPanel.add(label);

          jpanel.add(bottomPanel, BorderLayout.SOUTH);

          return jpanel;
        }
}
