package org.trinet.formats;

import org.trinet.util.*;
import org.trinet.util.gazetteer.*;
import org.trinet.util.gazetteer.TN.GazetteerType;
import org.trinet.jasi.*;

import java.sql.*;  // Connection class
import java.util.*;
//import org.trinet.util.Format; // CoreJava printf-like Format class
import java.util.regex.*;
import org.trinet.jdbc.datasources.AbstractSQLDataSource;

/*
 Support for various of the Cube data formats.
 @Author: Alan Yong & Doug Given
 */
// 8/21/03 DDG - Added behavior to mark quarries & indicate human reviewed or not
// by setting the case for the "location method" character.

// NOTE: this is not yet Leap Second compliant

public class CubeFormat {
  // Replace KK:mm:ss below with hh:mm:ss  KK:0-11 vs hh:1-12 hrs PM
  protected static String pstPattern = "dd MMM yyyy   hh:mm:ss a, z"; // 1-12 -aww 2010/07/06
  //NOTE: format 'k' puts midnight at 24 not 00 hrs, so replaced by HH below -aww
  protected static String gmtPattern = "dd MMM yyyy   HH:mm:ss z"; // 0 to 24 -aww 2010/01/11

  protected static String cr = "\n";

  protected static WhereIsEngine where = null;

  protected static final Pattern pattern = Pattern.compile("\\w");

  public CubeFormat() { }

/**
 * Return int value of CUBE-style one character checksum.
 * 
 * 8/16/07 DDG - fixed but that caused intermittant wrong checksum value.
   EXPLANATION: Origial C function was design for use with "unsigned short" integer
   Java has no such data type so we must use int (4-bytes) and "ignore" the upper
   two bytes. The java bitshift right operation ">>" is "signed but ">>>" is not 
   supposed to "carry" a bit into the high-order bit, but apparently it does (see below).
   You must use the 0xFFFF mask to only use the lower 16 bits otherwise
   higher order bits may appear during the addition.
   
   From: http://www.cs.utsa.edu/~wagner/laws/Abytes.html
 According to The Java Programming Language, Third Edition, page 164, the >>> operator 
 should fill new high-order bits with zeros. In fact, though, Java is converting b to 
 int type with sign-extended value 0xffffff80, right shifting this and putting just 
 four zeros at the right, to give 0x0ffffff8. <p>
 
 In the Java language, to right shift an integer amount shiftAmount, use the code<p>

                int shiftedValue = (byteValue & 0xff) >> shiftAmount; 
 */
  
    public static int menloCheckInt(String str) {

        int sum = 0;
        byte b[] = str.getBytes();
        // int x,y,z;
            
        for (int i=0; i<str.length();i++) {

//                if (1==(sum & 1)) {
//                        x = 0x8000;   // wrap the low bit (if set) before the right shift
//                } else {
//                        x = 0 ;
//                }
//                y = ((sum&0xffff)>>1);   // right shift once (= /2)
//                z = b[i];
//                sum = (x + y + z) ;
                
           //sum =  ( (1==((int)sum & 1)) ? 0x8000 : 0 ) + ((sum&0xffff)>>1) + b[i];
           sum = ((((sum&1)!=0)?0x8000:0) + (sum>>1) + b[i]) & 0xFFFF;               
        }
        return 36 + sum%91;  //91 printable characters starting at ascii 36
    }
  
  /**
   * CUBE-style one character checksum.<p>
   *  Returns:   Menlo Park check char as a string of length-1.<p>
  <pre>
  
// C-code implementation
  int MenloCheckChar( char* pch )
  {
       unsigned short sum;

       for( sum=0; *pch; pch++ )
           sum = ((sum&01)?0x8000:0) + (sum>>1) + *pch;

       return (int)(36+sum%91);
  }
<code>
  Examples of CUBE event messages(char appears as last character in string):
  E 09082344CI21999040217051050339860-1169945017316000014001800120009004332C0002hP
  E meav    US3199904021838195-201884 1681247 33054 19 192283 062 387  00  B 8   v
  E 14312972CI2200708122201006 336842-1170962 15614    38  90  15   5   322L 802hC
 </code>
 */
// Tested on 5 known-good CUBE "E " messages. Results were correct.
  public static char menloCheckChar(String str) {
    return (char) CubeFormat.menloCheckInt(str);
  }
// /////////////////////////////////////////////////////////////////////////
/*
         *         *         *         *         *     xxxx*         *
12345678901234567890123456789012345678901234567890123456789012345678901234567890

E 09082344CI21999040217051050339860-1169945017316000014001800120009004332C0002hP
E meav    US3199904021838195-201884 1681247 33054 19 192283 062 387  00  B 8   v
E 09872117CIL2002122115593100360201-1178858006211***015001000060006000649L0102h~
E 09872021CIL2002122108522400348103-1162616006310***016007000110007000854L0101h^
E 09872985CIL2002122313233600339693-1171616016918***035009000210003001113L1602hE

Alan's output
E  9872985CI 200212231323367+339693-1171617 16918    35  90  21   4   413L1602H~

*/
//
  public static void main(String[] args) {

      //System.out.println("Starting");
      //String str  =   "E 14295380CI2200706010208378 326753-1161105 11839    76  20  30   4   631L9902h";
      //String str1 =   "E 09082344CI21999040217051050339860-1169945017316000014001800120009004332C0002h";
      //String str2 =   "E 09082344CI21999040217051050339860-1169945017316000014001800120009004332C0002h";
      //String str3 =   "E meav    US3199904021838195-201884 1681247 33054 19 192283 062 387  00  B 8   ";
      //String str4 =   "E 09872117CIL2002122115593100360201-1178858006211***015001000060006000649L0102h";
      //String str5 =   "E 09872021CIL2002122108522400348103-1162616006310***016007000110007000854L0101h";
      //String str6 =   "E 09872985CIL2002122313233600339693-1171616016918***035009000210003001113L1602h";
      //String str1 =   "E 08170808UU2200708170808093 423592-1114588  1508     6 380   8  37999977D    h"; // "i"
      //String str2 =   "E 14312892CI2200708120645040 344008-1177285  77 9    35  50  15   3  1324H6502h"; // "i"
      //
      //String str1 = "E 09082344CI21999040217051050339860-1169945017316000014001800120009004332C0002h";
      //String str2 = "E meav    US3199904021838195-201884 1681247 33054 19 192283 062 387  00  B 8   ";
      //String str3 = "E 09872117CIL2002122115593100360201-1178858006211***015001000060006000649L0102h";
      //String str4 = "E 09872021CIL2002122108522400348103-1162616006310***016007000110007000854L0101h";
      //String str5 = "E 09872985CIL2002122313233600339693-1171616016918***035009000210003001113L1602h";
      //
      //System.out.println(CubeFormat.menloCheckChar(str1));
      //System.out.println(str1+CubeFormat.menloCheckChar(str1));
      //
      //System.out.println(CubeFormat.menloCheckChar(str2));
      //System.out.println(CubeFormat.menloCheckChar(str3));
      //System.out.println(CubeFormat.menloCheckChar(str4));
      //System.out.println(CubeFormat.menloCheckChar(str5));

        if (args.length < 5) {
            System.out.println("Args: [user] [pwd] [host.domain] [dbName] [evid]");
            System.exit(0);
        }

        String user   = args[0];
        String passwd = args[1];
        String host   = args[2];
        String dbname = args[3];

        long evid = Long.parseLong(args[0]);

        String url= "jdbc:" + AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL +":@"+ host +":"+ AbstractSQLDataSource.DEFAULT_DS_PORT +":"+ dbname;

        System.out.println("CubeFormat: making connection... ");
        DataSource ds = new DataSource (url, AbstractSQLDataSource.DEFAULT_DS_DRIVER, user, passwd);    // make connection
        System.out.println("CubeFormat dataSource : " + ds.describeConnection());

        //Look up this solution in the datasource
        Solution sol = Solution.create("TN").getById(evid);
        if (sol == null) {
           System.out.println("No event found matching input evid = "+evid);
        } else {
           //System.out.println(sol.toFingerFormat());
           // default version is "0"
           System.out.println(CubeFormat.toEventString(sol));
           sol.setSourceVersion(1l); // version "1"
           System.out.println(CubeFormat.toEventString(sol));
           sol.setSourceVersion(10l); // version "A"
           System.out.println(CubeFormat.toEventString(sol));
           sol.setSourceVersion(35l); // version "Z"
           System.out.println(CubeFormat.toEventString(sol));
           sol.setSourceVersion(36l); // version "a"
           System.out.println(CubeFormat.toEventString(sol));
           sol.setSourceVersion(61l); // version "z"
           System.out.println(CubeFormat.toEventString(sol));
           sol.setSourceVersion(62l); // version stays at  "z"
           System.out.println(CubeFormat.toEventString(sol));
   
           System.out.println(CubeFormat.toEventString(sol, "25")); // version "P"
           System.out.println(CubeFormat.toEventString(sol, "ABC")); // version "z"
           System.out.println(CubeFormat.toEventString(sol, "123")); // version "z"

           System.out.println(CubeFormat.toDeleteString(sol));
           System.out.println(CubeFormat.toDeleteString(sol, "default to solution values"));
           System.out.println(CubeFormat.toDeleteString(sol, "BK", "set net to BK"));
           System.out.println(CubeFormat.toDeleteString(sol, "X", "BK", "set version X and auth BK"));

           System.out.println(CubeFormat.toEmailString(sol, DataSource.getConnection()));
           //System.out.println(CubeFormat.toEmailString(sol, "3", DataSource.getConnection()));

           //datetime: 01-Jul-2010 19:34:01 GMT, Test AM/PM hours here
           sol.datetime.setValue(1278012841.);

           //System.out.println(CubeFormat.getEventSummaryBlock(sol));
           System.out.println(CubeFormat.getEventSummaryBlock(sol, null, 0)); // all
           System.out.println(CubeFormat.getEventSummaryBlock(sol, null, 1)); // town
           System.out.println(CubeFormat.getEventSummaryBlock(sol, null, 2)); // bigtown
           System.out.println(CubeFormat.getEventSummaryBlock(sol, null, 4)); // quarry
           System.out.println(CubeFormat.getEventSummaryBlock(sol, null, 8)); // fault
           System.out.println(CubeFormat.getEventSummaryBlock(sol, null, 16)); // volcano
           System.out.println(CubeFormat.getEventSummaryBlock(sol, null, 13)); //fault+quarry+town
           System.out.println(CubeFormat.getEventSummaryBlock(sol, null, 21)); //volcano+quarry+town
       }

       //List aList = (List) Solution.create().getValidByTime(
       //LeapSeconds.stringToTrue("2003-08-17 22:00:00.000"),
       //LeapSeconds.stringToTrue("2003-08-30 22:00:00.000"));
       //int count = aList.size();
       //System.out.println("Found "+count);
       //if (count > 0) {
       //    Solution sol[] = (Solution []) aList.toArray(new Solution[count]);
       //    String str = "";
       //    for (int i = 0; i< sol.length;i++) {
       //        str = CubeFormat.toEventString(sol[i]);
       //        System.out.println(str);
       //        System.out.println(CubeFormat.toDeleteString(sol[i], "CI", "Test of delete"));
       //    }
       //}

  }
//
// END OF MAIN

/**

 * Format this solution in the Cube "E" event format.
 * @param sol
 * @param version
 * @return - string
 * @author Alan Yong & Doug Given
 * @version 2.0 <p>

<pre>
 Source: http://neic.usgs.gov/neis/qdds/cube_fmt.html .
 CUBE "E " format for seismic events,
           fixed format by column:
!!!NOTE:  Field mapping(below) modified to line up 'vert' "ID-Code" with start of each field.
 ID-Code  N V                                                                 L");
          e e                                                            M    o");
          t r                                    #  #                     #   c");
          w s                                          D                 t");
          o i                                  M S  P  m   R   E   E   G yM E M");
          r o                                  a t  h  i   M   r   r   a pa r e");
          k nYr  MoDyHrMnSecLat    Lon     Z   g a  a  n   S   h   z   p eg m t");
12345678901234567890123456789012345678901234567890123456789012345678901234567890");
         1         2         3         4         5         6         7         8");
E 15305753CI3201303150957127 335012-1164592 109 8 35 55  40  20   3   615L2102hd
E 15306073CI0201303151843086 331332-1164363 10214 24 28 150  31   8  2626L 905BQ

a2 * Tp   = Message type = "E " (seismic event)
a8 * Eid  = Event identification number  (any string)
a2 * So   = Data Source =  regional network designation
a1 * V    = Event Version     (ASCII char, except [,])
i4 * Year = Calendar year                (GMT) (-999-6070)
i2 * Mo   = Month of the year            (GMT) (1-12)
i2 * Dy   = Day of the month             (GMT) (1-31)
i2 * Hr   = Hours since midnight         (GMT) (0-23)
i2 * Mn   = Minutes past the hour        (GMT) (0-59)
i3 * Sec  = Seconds past the minute * 10 (GMT) (0-599)
i7 * Lat  = Latitude:  signed decimal degrees*10000 north>0
i8 * Long = Longitude: signed decimal degrees*10000 west <0
i4   Dept = Depth below sea level, kilometers * 10
i2   Mg   = Magnitude * 10
i3   Nst  = Number of stations used for location
i3   Nph  = Number of phases used for location
i4   Dmin = Distance to 1st station;   kilometers * 10
i4   Rmss = Rms time error; sec * 100
i4   Erho = Horizontal standard error; kilometers * 10
i4   Erzz = Vertical standard error;   kilometers * 10
i2   Gp   = Azimuthal gap, percent of circle; degrees/3.6
a1   M    = Magnitude type
i2   Nm   = Number of stations for magnitude determination
i2   Em   = Standard error of the magnitude * 10
a1   L    = Location method
a1 * C    = Menlo Park check character, defined below

"Message Type" field:
     The second character, following the 'E', is no
     longer used for magnitude validation.  Prior "En"
     format that used the 2nd char for magnitude
     validation is no longer supported.

"Event Identification Number" field:
     Any string, excluding '[' and ']' characters.
     Must be a unique identifier for the event.
     Two events with the same identifier from the same
     source will be the same event.  Format varies by
     source - either numeric or alpha-numeric.

"Data Source" field: regional seismic network designation:
      BK = U.C.Berkeley
      CI = SCSN Caltech/USGS, Pasadena,   CA
      NC = NCSN USGS, Menlo Park, CA
      US = NEIC USGS, Golden,     CO
      UW = University of Washington

"Version Number" field:
     May have any value (ASCII 32-126, except 91 or 93).
     Meaning varies by source. Used to distinguish between
     different versions of the same event.

"Location Method" field: varies by source:
     Upper-case indicates an unconfirmed event,
     Lower-case indicates event is confirmed by human review

     R = RTP
     I = Isaiah
     A = Auryn
     B = Binder
     H = Hypoinverse
     S = st_relp     (Doug Neuhauser, UCB)

"Magnitude Type" field:
     B = Mb  = body wave magnitude
     C = Mca = coda amplitude magnitude
      D = Mcd = coda duration magnitude
      E = Me  = energy magnitude
      G = Mgn = MAGNUM (pseudo-empirical local magnitude)
      L = Ml  = local magnitude (synthetic Wood-Anderson)
      O = Mo  = moment magnitude
      P = Mp  = P-wave magnitude (Doug Neuhauser, UCB)
      S = Ms  = surface wave magnitude

"Menlo Park Check Character" field:
      Menlo-Park checksum, calculated 1st through 79th
      char in the message.  Checksum method defined by
      C language source code, below.
      NB: Square bracket characters ARE ACCEPTED in this
          field.

 If quarry blast information is available, the  "Location method" field
 (character 79) in the Cube format can be set to "Q" or "q" to indicate
 a quarry blast, and it will be flagged as such on the recenteqs pages:

 "Location Method" field: varies by source (in parentheses):
      Upper-case indicates an unconfirmed event,
      Lower-case indicates event is confirmed by human review
      
Example:
E 10251433CI1200705301740514 337133-1180627   016    10 430  49  27  8149L 704Hk    
  </pre>
**/
  
    public static char toVersionChar(Solution sol, String version) {
        if (version == null || version.length() == 0) {
          int srcVer = (int) sol.getSourceVersion();
          if (srcVer < 0) srcVer = 0;
          return intToVersion(srcVer);
        }
        return versionToChar(version); 
    }

    // Note CUBE historical event versions (e.g teleseisms) as lowercase letters were stored in event row cast to int value a=>97 to z=>122 
    // Local event generated db versions would be <= 61
    public static char intToVersion(int v) {
        if (v >= 0 && v < 10) v += 48; // 0-9 number (48-57)  <-- range of (int)char of import // added >= 0 was >0 -aww 2010/10/04
        else if (v >= 10 && v < 36) v += 55; // A-Z (65-90)  <-- range of (int)char of import
        else if (v >= 36 && v < 62) v += 61; // a-z (97-122) <-- range of (int)char of import
        else v = 122; // when input is not in 0-9, A-Z, a-z or the db source version > 61
        return (char) v; // 122 => 'z' max version
    }

    public static char versionToChar(String version) {
        int v = 122;
        String vers = version.trim();
        if (vers.length() == 1) {
            Matcher m = pattern.matcher(vers);
            if (m.matches()) return vers.charAt(0);

        }
        else if (vers.length() > 1) {
            try {
                v = Integer.parseInt(vers);
            }
            catch (NumberFormatException ex) { }
        }
        return intToVersion(v);
    }

    /**
     * Return a CUBE format "E" message. Use database version
     */
     public static String toEventString(Solution sol) {
         return toEventString(sol, null); 
     }
    
    /**
     * Return a CUBE format "E" message. Use input version
     */
    public static String toEventString(Solution sol, String version) {
      StringBuffer sb = new StringBuffer(80);

      // Appends "Tp"("Message type = "E " (seismic event)").
      //    "E ", "Message Type", is a static field.
      sb.append("E ");

      // Concatenates "Eid"("Event identification number  (any string)").
      // DDG - 2/15/08
      // Make sure the string NEVER exceeds 8 digits, right justify
      sb.append(getSafeEvidString(sol.getId().longValue()));

      // Concatenates "So"("Data Source =  regional network designation").
      // Concatenate.leftJustify(sb, sol.eventAuthority.toString(), 2);
      if (sol.eventAuthority.isNull()) {
        sb.append("  ");
      } else {
        Concatenate.leftJustify(sb, sol.eventAuthority.toString().substring(0, 2), 2);
      }

      // Concatenates "V"("Event Version     (ASCII char, except [,])").
      //System.out.println("Input version= \""+version+"\" sol version= \""+sol.getSourceVersion()+"\"");

      // version stops incrementing at version 61
      sb.append(toVersionChar(sol, version));

      DateTime dt = sol.getDateTime(); // for UTC leap - aww 2008/02/22 
    
      // Concatenates "Year"("Calendar year                (GMT) (-999-6070)    ").
       Concatenate.format(sb, (long)dt.getYear(), 4);

//    Concatenates "Mo"("Month of the year            (GMT) (1-12)").
//    January = "0", February = "1+1", ...
      Concatenate.format(sb, (long)dt.getMonth()+1, 2, 2);

//    Concatenates "Dy"("Day of the month             (GMT) (1-31)").
      Concatenate.format(sb, (long)dt.getDay(), 2, 2);

//    Concatenates "Hr"("Hours since midnight         (GMT) (0-23)").
      Concatenate.format(sb, (long)dt.getHour(), 2, 2);

//    Concatenates "Mn"("Minutes past the hour        (GMT) (0-59)").
      Concatenate.format(sb, (long)dt.getMinute(), 2, 2);

//    Concatenates "Sec"("Seconds past the minute * 10 (GMT) (0-599)").
// PROBLEM: rounding causes "59.95" => "600" which causes QDM to puke
//    Concatenate.format(sb, (long)(Math.round(dt.getDoubleSecond()*10)), 3, 3);
      Concatenate.format(sb, (long) (dt.getDoubleSecond()*10), 3, 3);

//    Concatenates "Lat"(North>0 convention).
//    "http://neic.usgs.gov/neis/qdds/cube_fmt.html" site utilizes "+" 'sign' prepended to "Lat".
      if (sol.lat.isNull()) {
      sb.append("      "); }

//    Concatenates "Lat"("Latitude:  signed decimal degrees*10000 north>0").
//    NOTE: "i7"-"i2"(frm 'sign' excluded in sol.lat) = "i5".
      if (sol.lat.doubleValue() == 0) {    //    Accounts for "0.0" lat values.
        sb.append("      0");     //    'white' space for no 'sign'.
      } else {
        Concatenate.format(sb, Math.round(sol.lat.doubleValue()*10000), 7);
      }

//    Concatenates "Long"("Longitude: signed decimal degrees*10000 west <0").
//    "sol.lon" value is prepended w/"-" 'sign' in "Lon".
//    NOTE: "i8"-"i1"(frm 'sign' included in sol.lon) = "i7"
      if (sol.lon.doubleValue() == 0) {    //    Accounts for "0.0" lon values.
        sb.append("       0");     //    'white' space for no 'sign'.
      } else {
        Concatenate.format(sb, Math.round(sol.lon.doubleValue()*10000), 8);
      }

//    Concatenates "Dept"("Depth below sea level, kilometers * 10").
      if (sol.depth.isNull()) {
        sb.append("    ");
      } else {
        Concatenate.format(sb, Math.round(sol.depth.doubleValue()*10), 4);
      }

//    Concatenates "Mg"("Magnitude * 10").
      if (sol.magnitude.value.isNull()) {
        sb.append("  ");
      } else {
        Concatenate.format(sb, Math.round(sol.magnitude.value.doubleValue()*10), 2);
      }

//    Concatenates "Nst"("Number of stations used for location").
//    SCSN database does not account for "Nst";
//    'white' space implicates NULL, spec., 'no value'.
//    sb.append("   ");
      if (sol.getPhaseList().size() == 0) sol.loadPhaseList();
      int myStaCnt = sol.getPhaseList().getStationUsedCount();
      if (myStaCnt > 999) myStaCnt = 999;
      Concatenate.format(sb, myStaCnt, 3);

//    Concatenates "Nph"("Number of phases used for location").
      if (sol.usedReadings.isNull()) {
        sb.append("   ");
      } else {
        Concatenate.format(sb, sol.usedReadings.longValue(), 3);
      }

//    Concatenates "Dmin"("Distance to 1st station;   kilometers * 10").
      if (sol.distance.isNull()) {
        sb.append("    ");
      } else {
        Concatenate.format(sb, Math.round(sol.distance.doubleValue()*10), 4);
      }

//    Concatenates "Rmss"("Rms time error; sec * 100").
      if (sol.rms.isNull()) {
        sb.append("    ");
      } else {
        Concatenate.format(sb, Math.round(sol.rms.doubleValue()*100), 4);
      }

//    Concatenates "Erho"("Horizontal standard error; kilometers * 10").
      if (sol.errorHoriz.isNull()) {
        sb.append("    ");
      } else {
        Concatenate.format(sb, Math.round(sol.errorHoriz.doubleValue()*10), 4);
      }

//    Concatenates "Erzz"("Vertical standard error;   kilometers * 10").
      if (sol.errorVert.isNull()) {
        sb.append("    ");
      } else {
        Concatenate.format(sb, Math.round(sol.errorVert.doubleValue()*10), 4);
      }

//    Concatenates "Gp"("Azimuthal gap, percent of circle; degrees/3.6").
      if (sol.gap.isNull()) {
        sb.append("  ");
      } else {
          // DDG 7/11/09 - had event with 100% gap which blew out the format.
          // format function does NOT trucate if value is longer than intFieldWidth
          long gap =  Math.round(sol.gap.doubleValue()/3.6);
          if (gap > 99) gap = 99; 
        Concatenate.format(sb, gap, 2);  
      }

//    Concatenates "M"("Magnitude type").
      if (sol.magnitude.value.isNull()) {
        sb.append(" ");
      } else {
        sb.append(sol.magnitude.getTypeSubString().toUpperCase().substring(0,1));
      }

//    Concatenates "Nm"("Number of stations for magnitude determination").
//    char width "i2" inadequate for nsta.netmag >99!!!
      myStaCnt = sol.magnitude.getStationsUsed();
      if (myStaCnt > 99) myStaCnt = 99;
      Concatenate.format(sb, myStaCnt, 2);

//    Concatenates "Em"("Standard error of the magnitude * 10").
      if (sol.magnitude.error.isNull()) {
        sb.append("  ");
      } else {
        Concatenate.format(sb, Math.round(sol.magnitude.error.doubleValue()*10), 2, 2);
      }

//    Concatenates "L"("Location method").
/*
      "Location Method" field: varies by source:
           Upper-case indicates an unconfirmed event,
           Lower-case indicates event is confirmed by human review

           R = RTP
           I = Isaiah
           A = Auryn
           B = Binder
           H = Hypoinverse
           S = st_relp     (Doug Neuhauser, UCB)
*/

      String type = sol.algorithm.toString().substring(0, 1);    // Default to 1st char of method
      EventType et = EventTypeMap3.getEventTypeByJasiType(sol.getEventTypeString());
      if ("explosion".equals(et.group)) {
          type= "Q";
      }

//  Upper-case indicates an unconfirmed event
//  Lower-case indicates event is confirmed by human review
      int solStateId = sol.getProcessingStateInt();
      if ( solStateId == JasiProcessingConstants.STATE_HUMAN ||
           solStateId == JasiProcessingConstants.STATE_FINAL ||
           solStateId == JasiProcessingConstants.STATE_CANCELLED
         || solStateId == JasiProcessingConstants.STATE_INTERMEDIATE) // could be screwy though - aww 2013/03/15 enabled here
      {
        type = type.toLowerCase();   // just "type.toLowerCase()" doesn't work!
      } else {
        type = type.toUpperCase();
      }

      sb.append(type);

      String str = sb.toString();
      str += menloCheckChar(str);

      return str;

    }

  public static String toDeleteString(Solution sol) {
      return toDeleteString(sol, null, null, null);
  }

  /** Return an event delete message string for this Solution.
   * The comment is optional and may be null. */
  public static String toDeleteString(Solution sol, String comment) {
      return toDeleteString(sol, null, null, comment);
  }


  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
  /** Return an event delete message string for this Solution.
   * Default the NET field to the Origin.Auth value. Leave comment blank.
   * The comment is optional and may be null. */
  public static String toDeleteString(Solution sol, String netcode, String comment) {
      return toDeleteString(sol, null, netcode, comment);
  }

/** Return an event delete message string for this event id.
 * Null input version default to database event table version.
 * Netcode is the 2-char FDSN network code, null input default to Solution origin authority.
 * The comment is optional and may be null.
 * If too long, comment will be truncated to 65 characters.<p>
 * Example: "DE 9884157CI  Deleted by operator"
 * */
  public static String toDeleteString(Solution sol, String version, String netcode, String comment) {
    // final Format fi8 = new Format("%8i");
    final Format fs2 = new Format("%2s");
    final Format fs65 = new Format("%-65s");

    StringBuffer sb = new StringBuffer(80);
    sb.append("DE");
    sb.append(getSafeEvidString(sol.getId().longValue()));

    String code = netcode;
    if (code == null) code = sol.getEventAuthority(); // Origin auth, changed to eventAuth -aww 2011/08/16

    // insure code doesn't exceed 2-char
    if (code.length() > 2) code = code.substring(0, 2);
    sb.append(fs2.form(code));

    sb.append(toVersionChar(sol, version));

    if (comment != null) sb.append(fs65.form(comment));
    /*
    else {
    // if no comment, append remark with origin time,lat,lon,depth, and mag value to tenth -aww 
     sb.append(" ");
     DateTime dt = sol.getDateTime(); // -aww 2008/02/10
     String dtStr = dt.toDateString("MMM dd, yyyy HH:mm:") + dt.getSecondsStringToPrecisionOf(2);
     sb.append(dtStr).append("  ");
     Format df1 = new Format("%8.4f");
     sb.append(df1.form(sol.lat.doubleValue())).append(" ");
     Format df2 = new Format("%9.4f");
     sb.append(df2.form(sol.lon.doubleValue())).append(" ");
     Format df3 = new Format("%4.1f");
     sb.append(df3.form(sol.depth.doubleValue()));
     if (sol.magnitude != null) {
       Format df4 = new Format("%4.1f");
       sb.append(" ").append(df4.form(sol.magnitude.value.doubleValue()));
       sb.append(" ").append(sol.magnitude.getTypeString());
     }
    }
    */

    return sb.toString();
  }

// DDG 2/20/08 - added use of the CubeMessageText table to get network specific strings,
// AWW 12/15/08 - changed CubeMessageText to MessageText table in HeaderStrings class

/**
 * Return a long string containing the e-mail message describing an event.
 * 
<code>
             >>> UPDATE OF PREVIOUSLY REPORTED EVENT <<<
                   == PRELIMINARY EVENT REPORT ==
 Southern California Seismic Network (TriNet) operated by Caltech and USGS

 Version 2: This report supersedes any earlier reports about this event.
 This is a computer generated solution and has not yet been reviewed by a human.

 Magnitude   :   1.8  Ml  (A micro quake)
 Time        :   20 Jun 2005   09:38:06 AM PDT
             :   20 Jun 2005   16:38:06 UTC
 Coordinates :   33 deg. 24.65 min. N,  117 deg.  4.32 min. W
 Depth       :    10.5 miles ( 16.8 km)
 Quality     :   Fair
 Location    :     10 mi. E   of Fallbrook, CA
             :     42 mi. SW  of Palm Springs, CA
             :      5 mi. NNW of OLY (quarry)
             :      0 mi. SSW of the Elsinore Fault
 Event ID    :   14156768
 
 More information is available on the Worldwide Web at:
 http://www.trinet.org/scsn/scsn.html
</code>
 * @param sol - event object
 * @param evtver - 1-char event version number or letter.
 * @param conn - dbase connection object
 * @return
 */  
  public static String toEmailString(Solution sol, String version, Connection conn) {
      return toEmailString(sol, version, conn, 0);
  }
  public static String toEmailString(Solution sol, String version, Connection conn, int flag) {
          
      //String net = EnvironmentInfo.getNetworkCode();  // will be "??" if not set
      String net = sol.getEventAuthority(); // changed to event auth -aww 2011/08/16
      //net = "NC";
      //System.out.println("net = "+net);
          
      // if net="??" (unknown) code will use value set in the dbase table
      HeaderStrings hs = null;
      try {
          hs = new HeaderStrings(net, conn);
      }
      catch (Exception ex) {
          ex.printStackTrace();
      }
      if (hs == null) {
          return "Error: CubeFormat.toEmailString unable to create text header from MessageText table rows";
      }
          
      StringBuffer sb = new StringBuffer();

      // MAIN HEADER
      // sort of a kludge - sol version is not reliable
      char vers = toVersionChar(sol, version);
      if (vers == '0' || vers == '1') {
          sb.append(hs.getString("PrelimHeader").toString("\n")+cr);
      } else {
          sb.append(hs.getString("UpdateHeader").toString("\n")+cr);
      }
      
      // ORIGINATING NETWORK
      sb.append(hs.getString("SrcHeader").toString("\n")+cr+cr);
      
      // VERSION INFO
      sb.append("Version "+ vers + 
                      ": This report supersedes any earlier reports of this event."+cr);
      
      // OPTIONAL QUARRY NOTICE
      if (sol.isEventDbType(EventTypeMap3.QUARRY)) {
          sb.append(hs.getString("QuarryHeader").toString("\n")+cr);
      }

      // REVIEW/NOT REVIEWED INFO
      String state = sol.getProcessingState().toString();  // string like 'A', 'F'
      if (state.equals(JasiProcessingConstants.STATE_AUTO_TAG)) { 
          sb.append(hs.getString("UnreviewedHeader").toString("\n")+cr);
      } else {
          sb.append(hs.getString("ReviewedHeader").toString("\n")+cr);
      }

      // EVENT INFO
      sb.append(cr);
      sb.append(getEventSummaryBlock(sol, hs, flag));
      sb.append(cr);
      
      // FOOTER
      sb.append(hs.getString("SrcFooter").toString("\n")+cr);
      sb.append(cr);
      
      // ADDITIONAL INFO
      sb.append(additionalInfoBlock(sol)+cr);

      return sb.toString();
  }

  public static String toEmailString(Solution sol, Connection conn) {
      return toEmailString(sol, null, conn, 0);
  }

  public static String toEmailString(Solution sol, Connection conn, int flag) {
      return toEmailString(sol, null, conn, flag);
  }

  public static String toCancelEmailString(Solution sol, Connection conn) {
      return toCancelEmailString(sol, null, conn, 0);
  }

  public static String toCancelEmailString(Solution sol, String version, Connection conn) {
      return toCancelEmailString(sol, version, conn, 0);
  }

  public static String toCancelEmailString(Solution sol, String version, Connection conn, int flag) {
      String cancelHeader = "\n!!!!!            EVENT CANCELLED AFTER HUMAN REVIEW            !!!!!\n\n"; 
      return cancelHeader + toEmailString(sol, version, conn, flag) + cancelHeader;
  }

  /**
   * Return an event summary block as a string like:
   * 
<code>
 Magnitude   :   2.99 Ml  (A minor quake)
 Time        :   20 Feb 2008   09:54:38 AM, PST
             :   20 Feb 2008   17:54:38 GMT
 Coordinates :   32 deg. 26.09 min. N, 115 deg. 18.07 min. W
             :   32.4348 N, 115.3012 W
 Depth       :     4.4 miles (  7.0 km)
 Quality     :   Poor
 Location    :    19 mi. ( 31 km) SE  from Calexico, CA
             :    28 mi. ( 46 km) SSE from El Centro, CA
 Event ID    :   CI 100804742
</code>
   * @param sol - the event
   * @return - the string
   */
  public static String getEventSummaryBlock(Solution sol) {
      return getEventSummaryBlock(sol, null, 0);
  }

  public static String getEventSummaryBlock(Solution sol, HeaderStrings hs, int flag) {
          
      StringBuffer sb = new StringBuffer();
      double ot = sol.getTime(); // for UTC leap - aww 2008/02/22 
      LatLonZ llz = sol.getLatLonZ();
      
      // EVENT INFO
      double mag = (sol.magnitude == null) ? 0. : sol.magnitude.getMagValue();
      String type = (sol.magnitude == null) ? "Mn" :sol.magnitude.getTypeString();
      EventType et = EventTypeMap3.getEventTypeByJasiType(sol.getEventTypeString());

      Format f = new  Format("%4.2f");
      sb.append(" Magnitude   :   ").append(f.form(mag)).append(" ").append(type);
      sb.append(" (");
      if (et.group.equals("earthquake")) sb.append(SeisFormat.magClassString(mag)).append(" earthquake");
      else sb.append(et.desc);
      sb.append(")").append(cr);
      //sb.append(" Time        :   "+ LeapSeconds.trueToPST(ot, pstPattern) +cr); // for UTC leap - aww 2008/02/22 
      //Use HeaderString LocalTimeZone text
      String tzStr = (hs != null) ? hs.getString("LocalTimeZone").toString() : null;
      if (tzStr == null || tzStr.length() == 0) {
          sb.append(" Time        :   "+ LeapSeconds.trueToDefaultZone(ot, pstPattern) +cr); // use local host timezone -aww 2010/07/06
      }
      else sb.append(" Time        :   "+ LeapSeconds.trueToString(ot, pstPattern, tzStr) +cr); // use messagetext header local timezone -aww 2010/07/14

      sb.append("             :   "+ LeapSeconds.trueToString(ot, gmtPattern)+cr); // for UTC leap - aww 2008/02/22 
      sb.append(" Coordinates :   "+ LLZstringDM(llz) +cr);
      sb.append("             :   "+ LLZstringDec(llz) +cr);
      sb.append(" Depth       :   "+ depthString(sol.getDepth()) +cr);
      sb.append(" Quality     :   "+ SeisFormat.wordQuality(sol) +cr);
      sb.append(whereBlock(llz, flag));         
      sb.append(" Event ID    :   "+ sol.getEventAuthority()+" "+sol.getId()+cr); // changed to eventAuth -aww 2011/08/16
      
      return sb.toString();
  }

  /**
   * Make sure the evid never exceeds 8 chars. Truncate high digits if necessary.
   * @param evid
   * @return
   */
  public static String getSafeEvidString(long evid ){
      StringBuffer sb = new StringBuffer();
      String idstr = String.valueOf(evid);
      idstr = idstr.substring(Math.max(0, idstr.length()-8));
      Concatenate.rightJustify(sb, idstr, 8);
      return sb.toString();
  }
  
  /**
   * Return an awful string like...
   *    "33 deg. 24.65 min. N,  117 deg.  4.32 min. W"
   * @param llz
   * @return
   */
  public static String LLZstringDM(LatLonZ llz) {
      final Format f2 = new Format("%5.2f");

      String latHemi = "N";
      if (llz.getLat() < 0.0) latHemi = "S";

      String lonHemi = "E";
      if (llz.getLon() < 0.0) lonHemi = "W";

      return Math.abs(llz.getLatDeg()) + " deg. "+
             f2.form(llz.getLatMin())+ " min. "+latHemi+", "+
             Math.abs(llz.getLonDeg()) + " deg. "+
             f2.form(llz.getLonMin())+ " min. "+lonHemi;
  }
  /**
   * Return an awful string like...
   *    "33 deg. 24.65 min. N,  117 deg.  4.32 min. W"
   * @param llz
   * @return
   */
  public static String LLZstringDec(LatLonZ llz) {
      final Format f2 = new Format("%5.4f");

      String latHemi = "N";
      if (llz.getLat() < 0.0) latHemi = "S";

      String lonHemi = "E";
      if (llz.getLon() < 0.0) lonHemi = "W";

      return f2.form(Math.abs(llz.getLat())) + " " +latHemi+", "+
             f2.form(Math.abs(llz.getLon())) + " " +lonHemi;
  }
  /**
   * another awful string like:
   * "10.5 miles ( 16.8 km)"
   * @param depth
   * @return
   */
  public static String depthString(double depth) {
       final Format f1 = new Format("%5.1f");
       //double km = Math.abs(depth); // sign matters, see below
       double km = depth; // aww 2012/09/18 as per Utah request
       double mi = GeoidalConvert.kmToMiles(km);
      return f1.form(mi) + " miles (" +f1.form(km)+" km)";

  }

  public static final int TOWN_MASK = 1;
  public static final int BIGTOWN_MASK = 2;
  public static final int QUARRY_MASK = 4;
  public static final int FAULT_MASK = 8;
  public static final int VOLCANO_MASK = 16;

  public static String whereBlock(LatLonZ llz, int flag) {

      StringBuffer sb = new StringBuffer();

      if (where == null) where =  WhereIsEngine.create("org.trinet.util.gazetteer.TN.WheresFrom");

      where.setReference(llz);

      String tmp = null;
      if (flag == 0 || (flag & TOWN_MASK) != 0) {
        tmp = whereLine(where, GazetteerType.TOWN);
        if (tmp.indexOf("lookup ") < 0) sb.append(" Location    :   ").append(tmp).append(cr);
      }

      if (flag == 0 || (flag & BIGTOWN_MASK) != 0) {
        tmp = whereLine(where, GazetteerType.BIG_TOWN);
        if (tmp.indexOf("lookup ") < 0) sb.append("             :   ").append(tmp).append(cr);
      }
        
      if (flag == 0 || (flag & QUARRY_MASK) != 0) {
        tmp = whereLine(where, GazetteerType.QUARRY);
        if (tmp.indexOf("lookup ") < 0) sb.append("             :   ").append(tmp).append(cr);
      }

      if (flag == 0 || (flag & VOLCANO_MASK) != 0) {
        tmp = whereLine(where, GazetteerType.VOLCANO);
        if (tmp.indexOf("lookup ") < 0) sb.append("             :   ").append(tmp).append(cr);
      }

      if (flag == 0 || (flag & FAULT_MASK) != 0) {
        tmp = whereLine(where, GazetteerType.FAULT);
        if (tmp.indexOf("lookup ") < 0) sb.append("             :   ").append(tmp).append(cr);
      }

      return sb.toString();
  }
  
  public static String whereBlockShort(LatLonZ llz) {

      StringBuffer sb = new StringBuffer();

      if (where == null) where =  WhereIsEngine.create("org.trinet.util.gazetteer.TN.WheresFrom");

      where.setReference(llz);

      String tmp = whereLine(where, GazetteerType.TOWN);
      if (tmp.indexOf("lookup ") < 0) sb.append(" Location    :   ").append(tmp).append(cr);

      tmp = whereLine(where, GazetteerType.BIG_TOWN);
      if (tmp.indexOf("lookup ") < 0) sb.append("             :   ").append(tmp).append(cr);


      return sb.toString();
  }

  /**
   * Compose a single line like:<p>
   * 29 mi. ESE of EASTSIDE RES. QUARRY, CA
   */
  public static String whereLine(WhereIsEngine where, String typeStr) {
      WhereItem ws = (WhereItem) where.whereSummaryType(typeStr);
      if (ws == null) return "lookup did not find a closest " + typeStr;

      double az = ws.getDistanceAzimuthElevation().getAzimuth();
      String azStr = GeoidalConvert.directionString(az);
      int mi = (int) ws.getDistance(GeoidalUnits.MILES);
      int km = (int) ws.getDistance(GeoidalUnits.KILOMETERS);
      String place = ws.fromPlaceString(false);
      final Format fd3 = new Format("%3d");
      //return fd3.form(mi) + " mi. ("+ fd3.form(km)+" km) "+azStr+ " from " + place;
      return fd3.form(mi) + " mi. ("+ fd3.form(km)+" km) "+azStr+ " of " + place;
  }

  /*
ADDITIONAL EARTHQUAKE PARAMETERS
________________________________
rms misfit                   :  0.29 seconds
horizontal location error    :   0.3 km
vertical location error      :   1.2 km
maximum azimuthal gap        :    35 degrees
distance to nearest station  :   9.0 km
event ID                     : 14157872

SOURCE OF INFORMATION/CONTACTS
________________________________
*/
public static String additionalInfoBlock(Solution sol) {
    final Format f1 = new Format("%.1f");
    final Format f2 = new Format("%.2f");
    final Format f0 = new Format("%3.0f");

    StringBuffer sb = new StringBuffer(256);
    sb.append("ADDITIONAL EARTHQUAKE PARAMETERS"+cr);
    sb.append("________________________________"+cr);
    int pscount = sol.getPhasesUsed();
    int scount = sol.sReadings.intValue();

    // Assume a single P and S phase pick per station (no multiple p and s per site)
    if (sol.phaseList.isEmpty()) {
        sol.loadPhaseList();
    }
    Phase cPhase = null;
    if (sol.phaseList.isEmpty())  {
        sb.append("Stations used                : " + (pscount-scount) + cr);
    }
    else {
        sb.append("Stations used                : " + sol.phaseList.getStationUsedCount() + cr);
        sol.phaseList.sort( new CubeFormat.DistComparator() );
        //sol.phaseList.dump();
        cPhase = (Phase) sol.phaseList.get(0);
    }

    sb.append("Phases used                  : " + pscount + cr);

    if (!sol.sReadings.isNull()) sb.append("S phases used                : " + scount + cr);

    sb.append("Rms misfit                   : "+ f2.form(sol.rms.doubleValue()) + " seconds"+cr);
    sb.append("Horizontal location error    : "+ f1.form(sol.errorHoriz.doubleValue()) + " km"+cr);
    sb.append("Vertical location error      : "+ f1.form(sol.errorVert.doubleValue()) + " km"+cr);
    sb.append("Maximum azimuthal gap        : "+ f0.form(sol.gap.doubleValue()).trim() + " degrees"+cr);

    String staUsed = null;
    if (cPhase != null) { 
        sb.append("Distance to nearest used sta : ");
        sb.append(f1.form(cPhase.getHorizontalDistance())).append(" km ");
        double az = cPhase.getAzimuth();
        az = ( az >= 180.) ? az-180. : az+180.; 
        sb.append(GeoidalConvert.directionString(az).trim()).append(" of ");
        staUsed = cPhase.getChannelObj().getSta();
        sb.append(staUsed);
        sb.append(cr);
    }

    if (where == null) where =  WhereIsEngine.create("org.trinet.util.gazetteer.TN.WheresFrom");
    where.setReference(sol.getLatLonZ());
    WhereItem ws = (WhereItem) where.whereSummaryType(GazetteerType.STATION);
    if (ws != null) { 
        String place = ws.fromPlaceString(false);
        int idx = place.indexOf(" ");
        String sta = place.substring(0,idx);
        if ( ! sta.equals(staUsed) ) {
          sb.append("Distance to nearest station  : ");
          sb.append(f1.form(ws.getDistance())).append(" km ");
          double az = ws.getDistanceAzimuthElevation().getAzimuth();
          sb.append(GeoidalConvert.directionString(az).trim()).append(" of ");
          sb.append(sta);
          sb.append(cr);
        }
    }

    sol.loadPrefMags();
    Collection list = sol.getPrefMags();
    Iterator iter = list.iterator();
    Magnitude mag = null;
    Format f = new  Format("%4.2f");
    sb.append("Event magnitudes             :");
    while (iter.hasNext()) {
        mag = (Magnitude) iter.next();
        sb.append(" ").append(mag.getTypeString()).append("=").append(f.form(mag.getMagValue()));
    }
    sb.append(cr);

    return sb.toString();
}

public static class DistComparator implements java.util.Comparator {

    public DistComparator() {}

    public boolean equals(Object obj) {
        return (obj instanceof DistComparator);
    }

    public int compare(Object ob1, Object ob2) {
        if (ob1 instanceof Phase) {
            if (ob2 instanceof Phase) {
                return ( ((Phase)ob1).getHorizontalDistance() < ((Phase)ob2).getHorizontalDistance() ) ? -1 : 1;
            }
        }
        return 0;
    }
}

} // end of class

