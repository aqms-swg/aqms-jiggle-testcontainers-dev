package org.trinet.formats;

import java.util.*;
import java.io.*;
//import java.text.ParseException;

import org.trinet.jasi.*;
import org.trinet.jasi.TN.UnassocAmpListTN;

import org.trinet.util.*;
import org.trinet.formats.FormatException;

/**
 * Support for the CISN Ground Motion Data Message Packet format. This format
 * is needed to support exchange of strong motion amplitude data between CISN centers
 * in a format sufficient for the needs of the EDC (Engineering Data Center). This
 * class parses and writes the format to and from the org.trinet.jasi.Amplitude() class. <p>
 *
 * Note that the "GMP" format stuffs information about several amplitudes into
 * one message and is a weird mix of Earthworm, ShakeMap and CSMIP worldviews.<p>
 *
 * One message can contain: PGA, PGV, PGD, SP0.3, SP1.0, SP3.0 and a "maximum"
 * spectral peak acceleration. One "file" can contain multiple components.
 *
 * This format is derived from the Earthworm StrongMotionII format. <p>
 *
 * @see org.trinet.formats.StrongMotionII()
 * @see org.trinet.jasi.Amplitude()
 *
<pre>
<strong>Example:</strong>
SCNL: CMB.BHZ.BK.--  
TIME: 2001/02/25 02:37:02.000
ALT: 2001/02/25 02:36:57.000 CODE: 4
PGA: 6.8462 TPGA: 2001/02/25 02:37:04.000
PGV: 0.1400 TPGV: 2001/02/25 02:37:07.000
PGD: 0.0250 TPGD: 2001/02/25 02:37:10.000
RSA: 3/0.30 4.4154/1.00 0.9256/3.00 0.2979//.50 5.1233
QID: 41059467 014024003:UCB 1233
QUAL: 1/0.0/1/2
POST: CI 2001/02/25 02:40:30.000
<p>
<strong>Triggered record:</strong>

SCNL: 24611.HNN.CE. (34.059, -118.260, LA - Temple & Hope)
TIME: 2001/09/09 23:59:23.000
ALT: 2001/09/09 23:59:18.000 CODE: 4
PGA: 46.123 TPGA: 2001/09/09 23:59:23.800
PGV: 2.010 TPGV: 2001/09/09 23:59:23.800
PGD: .0250 TPGD: 0000/00/00 00:00:00.000
RSA: 3/0.30 88.10/1.0 7.21/3.00 1.05//.19 130.5
QID: - -
QUAL: 1/-/1/1/611AS012
POST: CE 2001/09/10 00:14:42.000

SCNL: 8040.HNE.NP.
TIME: 2004/02/25 00:08:50.500
ALT: 2004/03/01 19:41:52.000 CODE: 1
PGA: 0.284498 TPGA: 2004/02/25 00:08:20.000
PGV: 0.022049 TPGV: 2004/02/25 00:09:16.460
PGD: 0.006908 TPGD: 2004/02/25 00:08:51.665
RSA: 3/0.30 0.254576/1.00 0.153482/3.00 0.043354
QID: ? ?
</pre>
 */
/*
 WARNING! All measurements in the Earthworm TYPE_STRONGMOTION2 message are assumed
 to be positive. SM_NULL indicates that a measurement is missing, and any other
 negative value will result in unpredictable behavior from strong motion applications.
 If you have sources that may produce signed values for PGA, PGV, PGD, or RSA values,
 you must be sure to change these to absolute values!
 */
public class GroMoPacket {

    public static boolean debug = false;

    /** The list of amps that result from parsing file (one or more channels). */
    static AmpList groupAmpList = new UnassocAmpListTN();

    // Single Channel 
    static AmpList amplist = new UnassocAmpListTN();
    static HashMap dataMap = new HashMap(11);
    static ArrayList saList = new ArrayList(3);

    // default units
    static final int PGAunits = Units.CMSS;
    static final int PGVunits = Units.CMS;
    static final int PGDunits = Units.CM;
    static final int SPunits  = Units.CMSS; // DDG 6/2/08 - changed to physical units "cm/s/s", SPunits  = Units.SPA;

    static final double TwoPi =  2.0 * Math.PI;  // for conversions later - save a cycle

    /** This string represents "null" in ASCII strings for SCNL = "--",  a single "-" may also be used. */
    static final String NullString = "--";

    static ChannelName chanName = new ChannelName();

    /** Some time vaguely related to the data...*/
    static double baseTime = Double.NaN;
    static double baseLength = 0.;

    /** An alternate time (not used here) */
    static double altTime = 0.;

    /** The ID number of the event, if the amps are associated */
    static long evid = 0l;

    /** Written in subsource field to give clue to the origin of the amp.*/
    static String authNet = null;
    static String eventAuth = null;

    /** Written in subsource field to give clue to the origin of the amp.*/
    static String source = "GMP";

    /** Default network code */
    static String netCode = "--";

    // keep track of where we are for error reporting
    static File currentFile = null;
    static int lineNo = 0;

    /** Flag to toggle behavior to produce Earthworm StrongMotionII format.<p> @see: StrongMotionII */
    public static final int TYPE_GMP = 0;
    public static final int TYPE_EW2 = 1;
    public static final int TYPE_GMXY = 2;

    static int formatType = TYPE_GMP;

    /** End-of-line string specific to the local machine. */
    static final String eol = System.getProperty("line.separator" );

    /** List of valid amp types for a GroMoPacket message. */
    static final int[] validTypes = {
      AmpType.PGA,
      AmpType.PGD,
      AmpType.PGV,
      AmpType.SP03,
      AmpType.SP10,
      AmpType.SP30
    };
    static final IntArray validTypeArray = new IntArray(validTypes);

    // Version 3.0 amp meth descriptions
    public static final String methMS = "MS";  // measured acc cmss
    public static final String methDV = "DV";  // derivative of velocity acc cmss
    public static final String methIF = "IF";  // recursive integration freq domain for V or D with low freq corner period
    public static final String methIT = "IT";  // recursive integration time domain for V or D with low freq corner period
    public static final String methIR = "IR";  // integration for V or D no corner KMH
    public static final String methNJ = "NJ";  // nigam jennings
    public static final String methKH = "KMH"; // Kanmori Mechling Hauksonn

    /** List of valid message tags. */
    static final String lineTagGMXY[] = {
      "SCNL:",
      "WINDOW:",
      "LENGTH:",
      "METH:",
      "PGA:",
      "PGV:",
      "PGD:",
      "TPGA:",
      "TPGV:",
      "TPGD:",
      "SA:",
      "TSA:",
      "EVID:",
      "AUTH:"
    };

    static final String lineTagGMP[] = {
      "SCNL:",
      "TIME:",
      "PGA:",
      "PGV:",
      "PGD:",
      "RSA:",
      "QID:",
      "QUAL:",
      "POST:"
    };

    // Tokenizer variables used by several routines
    static String delim = " ";
    static String tok = null;
    static StringTokenizer strtok = null;

    /** Max length of a valid SEED channel */
    final static int MAX_CHAN_LENGTH = 3;

    /** 0000/00/00 00:00:00.000 in UTC true seconds*/
    //static double crapTime = -62135769600.;
    static double crapTime   = -62170156800.;


    // Number formats
    static final Format df32 = new Format("%3.2f");
    static final Format df64 = new Format("%6.4f");

  public GroMoPacket() { }

  public static String getAmpSubsource() {
      return source;
  }

  public static void setAmpSubsource(String ampSubsource) {
      GroMoPacket.source = ampSubsource;
  }

  /** Set the network code to be stuck in QID line.
   * If code is longer than 2-char it will be truncated. Default is "UK". */
  public static void setNetCode(String code) {
    if (code.length() > 2) {
      netCode = code.substring(0, 1);
    } else {
      netCode = code;
    }
  }
  public static String getNetCode() {
    return netCode;
  }

  public static void setFormatType(int type) {
    formatType = type;
  }
  public static int getFormatType() {
    return formatType;
  }

  public static void setFile(File file) {
    currentFile = file;
    String name = currentFile.getName(); 
    if ( name.endsWith(".EW2") || name.indexOf(".sm") > 0 ) {
        formatType = TYPE_EW2;
    }
    else if (name.endsWith(".gmp") ) { 
        formatType = TYPE_GMP;
    }
    else {
        formatType = TYPE_GMXY;
    }
  }
  public static File getFile() {
    return currentFile;
  }

  /** Parse the given Reader i/o stream and return the resulting * Amplitudes as an array. */
  public static AmpList parse(File file) { // throws Exception {

    if (debug) System.out.println("DEBUG GroMoPacket parse file: " + file.toString());

    setFile(file);

    BufferedReader in = null;
    try {
      in = new BufferedReader(new FileReader(file));
      parseAmpsFromReader(in);
    }
    catch (Exception ex) {
        ex.printStackTrace();
    }

    return groupAmpList;
  }

  private static void initValues() {
    baseTime = Double.NaN;
    baseLength = 0.;
    altTime = 0.;
    evid = 0l;
    authNet = null;
    eventAuth = null;
    lineNo = 0;
  }

  private static void parseAmpsFromReader(BufferedReader in) {

    try {

      // reset starting values
      initValues();

      // clear the static group amp list list at start
      groupAmpList.clear();

      String instr = null;
      /* Continue to read lines while there are still some left to read.
       * A line is terminated by ('\n'), ('\r'), or ('\n\r') */
      while ((instr = in.readLine()) != null) {
        if (instr.length() == 0)  continue; // added to skip over blank lines, if any -aww 2009/01/12
        if (debug) System.out.println("DEBUG GroMoPacket parseAmpsFromReader:\"" + instr + "\"");
        if (instr.startsWith("SCNL:")) {
            groupAmpList.addAll(parseOneChannel(instr, in)); 
        }
      }
    }
    catch (Exception ex) {
      ex.printStackTrace(System.err); // "* GroMoPacket: File input error");
    }
    finally {
      try {
        if (in != null) in.close();
      }
      catch (Exception ex2) { }
    }
  }

  /** Parse from input Reader i/o stream all amp data for a single channel and return the resulting Amplitudes as an array. */
  public static AmpList parseOneChannel(String scnl, BufferedReader in) { // throws Exception {

    // clear the single channel static maps and lists
    amplist.clear();
    saList.clear();
    dataMap.clear();
    
    try {

      mapOneLine(scnl);
      if (debug) System.out.println("DEBUG GroMoPacket parseOneChannel1:\"" + scnl + "\"");

      String instr = null;
      /* Continue to read lines while there are still some left to read.
       * A line is terminated by ('\n'), ('\r'), or ('\n\r') */
      while (true) {

        in.mark(132);  // mark current position
        instr = in.readLine(); // get next line
        if (instr == null) break; // eof, process channel amps

        if (instr.length() == 0) continue; // skip over blank lines

        if (instr.startsWith("SCNL:")) { // another channel
            in.reset(); // go back to mark
            break;      // now process existing channel amps if any
        }

        if (debug) System.out.println("DEBUG GroMoPacket parseOneChannel2:\"" + instr + "\"");
        mapOneLine(instr);

      }

    }
    catch (Exception ex) {
      ex.printStackTrace(System.err); // "* GroMoPacket: File input error");
    }

    boolean status = processMap();
    if (!status) return amplist; // bail with empty list

    // Make a stripped down Solution to associate amps with
    Solution sol = Solution.create();
    //eventType not in message so to not bump version -aww 2008/04/15
    sol.eventType.setValue(EventTypeMap3.getMap().toJasiType(EventTypeMap3.EARTHQUAKE));
    sol.gtype.setValue(GTypeMap.LOCAL);
    sol.setId(evid); // evid from the file, if any probably not a viable local evid
    if (eventAuth != null) sol.eventAuthority.setValue(eventAuth);
    else if (authNet != null) sol.eventAuthority.setValue(authNet);
    Amplitude amp = null;
    for (int idx = 0; idx< amplist.size(); idx++) {
        amp = (Amplitude) amplist.get(idx);
        amp.setAuthority(authNet);
    }
    amplist.associateAllWith(sol);

    return amplist;
  }

/*
  public static AmpList parse(String str) { // throws FormatException  {

    String lineDelim = "\n\r";
    StringTokenizer lineTok = new StringTokenizer(str, lineDelim);

    amplist.clear();
    saList.clear();
    dataMap.clear();

    while (lineTok.hasMoreTokens()) {
      //lineNo++;
      //parseOneLine(lineTok.nextToken()) ;
      mapOneLine(lineTok.nextToken());
    }

    boolean status = processMap();
    if (!status) return amplist; // bail with empty list

    // Make a stripped down Solution to associate amps with
    Solution sol = Solution.create();
    //eventType not in message so to not bump version -aww 2008/04/15
    sol.eventType.setValue(EventTypeMap3.getMap().toJasiType(EventTypeMap3.EARTHQUAKE));
    sol.gtype.setValue(GTypeMap.LOCAL);
    sol.setId(evid);                // evid from the file, if any - probably not a viable local evid
    if (eventAuth != null) sol.eventAuthority.setValue(eventAuth);
    else if (authNet != null) sol.eventAuthority.setValue(authNet);
    amplist.associateAllWith(sol);

    return amplist;
  }
*/

/** Parse the given String containing all the necessary lines to define a amp set 
 return the resulting Amplitudes as an array assumes the format type. Resets internal static list. */
  public static AmpList parse(String str) { // throws FormatException  {

    BufferedReader in = null;

    try {
      in = new BufferedReader(new StringReader(str));
      parseAmpsFromReader(in);
    }
    catch (Exception ex) {
        ex.printStackTrace();
    }

    return groupAmpList;
  }

  /* Because of the way multiple amps are entangled in a single file, Amplitude objects
  are created and added to the static amplist in the parseXXXX() methods.*/
  private static void mapOneLine(String line)  {
      int idx = line.indexOf(":") + 1;
      String key = line.substring(0, idx);
      if (formatType == TYPE_GMXY) {
           if (!key.equals("SA:")) dataMap.put(key, line);
           else saList.add(line);
      }
      else {
          dataMap.put(key, line);
      }
  }

  private static boolean processMap() {
      return (formatType == TYPE_GMXY) ? processMapGMXY() : processMapGMP();
  }

  private static boolean processMapGMP() {
      boolean status = true;
      String str = null;
      str = (String) dataMap.get("SCNL:");
      if (str != null) status &= parseSCNL(str);
      if (!status) return false;  // what's the point if you don't know the SCNL?

      str = (String) dataMap.get("TIME:");
      if (str != null) status &= parseTIME(str);
      str = (String) dataMap.get("ALT:");
      if (str != null) status &= parseALT(str);
      str = (String) dataMap.get("PGA:");
      if (str != null) status &= (parsePGA(str) != null);
      str = (String) dataMap.get("PGV:");
      if (str != null) status &= (parsePGV(str) != null);
      str = (String) dataMap.get("PGD:");
      if (str != null) status &= (parsePGD(str) != null);
      str = (String) dataMap.get("RSA:");
      if (str != null) status &= parseRSA(str);
      str = (String) dataMap.get("QID:");
      if (str != null) status &= parseQID(str);
      str = (String) dataMap.get("QUAL:");
      if (str != null) status &= parseQUAL(str);
      str = (String) dataMap.get("POST:");
      if (str != null) status &= parsePOST(str);

      return true;
  }

  private static boolean processMapGMXY() {
      boolean status = true;
      String str = null;
      str = (String) dataMap.get("SCNL:");
      if (str != null) status &= parseSCNL(str);
      if (!status) return false;  // with bad SCNL whats the point of continuing

      str = (String) dataMap.get("EVID:");
      if (str != null) status &= parseEVID(str);
      str = (String) dataMap.get("AUTH:");
      if (str != null) status &= parseAUTH(str);
      str = (String) dataMap.get("WINDOW:");
      if (str != null) status &= parseWINDOW(str);
      str = (String) dataMap.get("PGA:");
      if (str != null) status &= (parsePGA_XY(str) != null);
      str = (String) dataMap.get("PGV:");
      if (str != null) status &= (parsePGV_XY(str) != null);
      str = (String) dataMap.get("PGD:");
      if (str != null) status &= (parsePGD_XY(str) != null);

      for (int idx = 0; idx < saList.size(); idx++) {
          str = (String) saList.get(idx);
          if (str != null) status &= (parseSA(str) != null);
      }

      return true;
  }

  private static boolean parseOneLine(String line)  {
      return (formatType == TYPE_GMXY) ? parseOneLineGMXY(line) : parseOneLineGMP(line);
  }

  private static boolean parseOneLineGMP(String line)  {
    String tag = getLineType(line);

    try {
      if (tag.equals("SCNL:")) {
        return parseSCNL(line);
      } else if (tag.equals("TIME:")){
        return parseTIME(line);
      } else if (tag.equals("ALT:")){
        return parseALT(line);
      } else if (tag.equals("PGA:")){
        return (parsePGA(line) != null);
      } else if (tag.equals("PGV:")){
        return (parsePGV(line) != null);
      } else if (tag.equals("PGD:")){
        return (parsePGD(line) != null);
      } else if (tag.equals("RSA:")){
        return parseRSA(line);
      } else if (tag.equals("QID:")){
        return parseQID(line);
      } else if (tag.equals("QUAL:")){
        return parseQUAL(line);
      } else if (tag.equals("POST:")){
        return parsePOST(line);
      }
    }
    catch (Exception ex) {
      System.err.println(ex.toString());   // treat as non-fatal
    }
    System.err.println("Bad or unknown line with tag: "+tag+ " at line # "+lineNo+
                       " of file "+getFile().getName());
    return false;

  }

  private static boolean parseOneLineGMXY(String line)  {

    String tag = getLineType(line);

    try {
      if (tag.equals("SCNL:")) {
        return parseSCNL(line);
      } else if (tag.equals("WINDOW:")){
        return parseWINDOW(line);
      } else if (tag.equals("PGA:")){
        return (parsePGA_XY(line) != null);
      } else if (tag.equals("PGV:")){
        return (parsePGV_XY(line) != null);
      } else if (tag.equals("PGD:")){
        return (parsePGD_XY(line) != null);
      } else if (tag.equals("SA:")){
        return (parseSA(line) != null);
      } else if (tag.equals("EVID:")){
        return parseEVID(line);
      } else if (tag.equals("AUTH:")){
        return parseAUTH(line);
      }
    }
    catch (Exception ex) {
      System.err.println(ex.toString());
    }
    System.err.println("Bad or unknown line with tag: "+tag+ " at line # "+lineNo+
                       " of file "+getFile().getName());
    return false;
  }

  private static boolean parseWINDOW(String str) {
      delim = " ";
      strtok = new StringTokenizer(str, delim);
      if (isMalformed(str, strtok, "WINDOW:", 4)) return false;   // format check
      boolean status = true;
      try {
        baseTime = parseTimeString(strtok.nextToken()+" "+strtok.nextToken());
        baseLength = 0.;
        if (baseTime == crapTime) {
          baseTime = Double.NaN;
        }

        if ( !strtok.nextToken().equals("LENGTH:")) {
          System.err.println("* GroMoPacket Error: Missing LENGTH tag: "+str);
          return false;
        }

        baseLength = Double.parseDouble(strtok.nextToken());
        if (baseLength < 0) baseLength = 0.;
      }
      catch (Exception ex) {
         System.err.println("* GroMoPacket Error: Malformed line: "+ex.getMessage()+"\n"+str);
         status = false;
      }
      
      return status;
  }

  private static Amplitude parsePGA_XY(String str) {
      Amplitude amp = parseAmpXY(str, AmpType.PGA);
      if (amp != null) amp.setUnits(GroMoPacket.PGAunits);
      return amp;
  }

  private static Amplitude parsePGV_XY(String str) {
      Amplitude amp = parseAmpXY(str, AmpType.PGV);
      if (amp != null) amp.setUnits(GroMoPacket.PGVunits);
      return amp;
  }

  private static Amplitude parsePGD_XY(String str) {
      Amplitude amp = parseAmpXY(str, AmpType.PGD);
      if (amp != null) amp.setUnits(GroMoPacket.PGDunits);
      return amp;
  }

    private static Amplitude parseAmpXY(String str, int ampType) { //  throws FormatException  {
    
      double time = Double.NaN;
      double val = 0.;

      strtok = new StringTokenizer(str, " ");
      String tokString = AmpType.getString(ampType)+":";

      // format check not all types have the same max token count
      if (isMalformed(str, strtok, tokString, 5)) return null;

      Amplitude amp = null; 
      try {
        // get amp value
        val = Double.parseDouble(strtok.nextToken());

        // get amp time
        if ( !strtok.nextToken().equals("T"+tokString)) {
          System.err.println("* GroMoPacket Error: Bad time tag: "+str);
          return null;
        }
        tok = strtok.nextToken()+" "+strtok.nextToken();
        time = parseTimeString(tok);
        // if the time is crap (0000/00/00 00:00:00.000), set null 
        if (time <= crapTime) time = Double.NaN;

        /* ? STOP HERE or continue parsing ? since rest of input line data is not mapped to NCEDC schema tables:
        if (!strtok.nextToken().equals("METH:")) {
          System.err.println("* GroMoPacket Error: Bad METH: tag: "+str);
          return null;
        }
        tok = strtok.nextToken();
        if (tok.equals("IF")) {
            tok += strtok.nextToken();
        }
        else if (tok.equals("IR")) {
            tok += strtok.nextToken();
        }
        meth = tok;
        */
    
        // Sanity check of time
        if (Double.isNaN(time)) {
          System.err.println(" * GroMoPacket: Unknown time: "+str);
          //return null;  // unknown time valid ?
        }
  
        // Sanity check of value: PNL 2009/03/20
        if (val <= 0.0) {
            System.err.println(" * GroMoPacket Error: Bad amp value:"+str);
            return null;
        }

        TimeSpan ts = null;
        if (!Double.isNaN(baseTime)) ts = new TimeSpan(baseTime, baseTime+baseLength);
        else if (!Double.isNaN(time)) ts = new TimeSpan(time, time);
        else { // malformed missing valid time construct
            System.err.println(" * GroMoPacket Error: amp and window times cannot both be null value:"+str);
            return null;
        }
    
        // all is well - make an AMP
        amp = newAmp(ampType);
        if (!Double.isNaN(time)) amp.setTime(time);
        amp.value.setValue(val);
    
        // the window bounds are NOT NULL but I have no reasonable values to put in them
        // change below for SCWG update of windowStart, duration rules:
        // For each amplitude where we know the time and the window  set all the fields (datetime, wstart, duration).
        // For each amplitude where we know the time but not the window: Amp.datetime = Amp.wstart and Amp.duration = 0
        // For each amplitude where we don't know the time nor the window: Amp.datetime = NULL, Amp.wstart = <Origin Time>, Amp.duration = NULL
        amp.setWindow(ts);
    
        // set correct units - the spec says 'cm' is required.
        if (ampType == AmpType.PGA) {
          amp.setUnits(Units.CMSS);
        } else if (ampType == AmpType.PGV) {
          amp.setUnits(Units.CMS);
        } else if (ampType == AmpType.PGD) {
          amp.setUnits(Units.CM);
        }
    
        // add the new amp to the static list
        amplist.add(amp, false);
    
      }
      catch (Exception ex) {
         System.err.println("* GroMoPacket Error: Malformed PG line: "+ex.getMessage()+"\n"+str);
      }

      return amp;
    }

  private static Amplitude parseSA(String str) {

      delim = " ";
      strtok = new StringTokenizer(str, delim);
      if (isMalformed(str, strtok, "SA:", 5)) return null;   // format check

      Amplitude amp = null;
      double per = 0.;
      double acc = 0.;
      double time = Double.NaN;
      try {

          // Parse period, -1 if unknown
          per = Double.parseDouble( strtok.nextToken() );

          // Parse acc value
          acc = Double.parseDouble( strtok.nextToken() );

          // Sanity check of value
          if (acc <= 0.0) {
              System.err.println(" * GroMoPacket Error: Bad SA value:"+str);
              return null;
          }

          // get amp time
          if ( !strtok.nextToken().equals("TSA:")) {
            System.err.println("* GroMoPacket Error: Bad time tag: "+str);
            return null;
          }

          tok = strtok.nextToken()+" "+strtok.nextToken();
          time = parseTimeString(tok);
          // if the time is crap (0000/00/00 00:00:00.000), set null 
          if (time <= crapTime) time = Double.NaN;

          TimeSpan ts = null;
          if (!Double.isNaN(baseTime)) ts = new TimeSpan(baseTime, baseTime+baseLength);
          else if (!Double.isNaN(time)) ts = new TimeSpan(time, time);
          else { // malformed missing valid time construct
              System.err.println(" * GroMoPacket Error: amp and window times cannot both be null value:"+str);
              return null;
          }
    
          amp = makeSA(per, acc, time, ts);
      }
      catch (Exception ex) {
        System.err.println("* GroMoPacket Error: Malformed SA line: "+ex.getMessage()+"\n"+str);
        return null;
      }

      return amp;
  }

  private static boolean parseEVID(String str) {
      delim = " ";
      strtok = new StringTokenizer(str, delim);
      if (isMalformed(str, strtok, "EVID:", 2)) return false;   // format check

      evid = 0l;
      eventAuth = null;
      try {
          tok = strtok.nextToken();
          if (!tok.startsWith("-")) {
              eventAuth = tok;

              tok = strtok.nextToken();
              if (!tok.startsWith("-")) {
                  evid = Long.parseLong(tok);
              }
          }
      }
      catch (Exception ex) {
          System.err.println("* GroMoPacket Error: bad EVID: "+str);
          return false;
      }
      return true;
  }

  private static boolean parseAUTH(String str) {

      delim = " ";
      strtok = new StringTokenizer(str, delim);
      if (isMalformed(str, strtok, "AUTH:", 3)) return false;   // format check

      try {
          tok = strtok.nextToken();
          if (!tok.startsWith("-")) {
              authNet = tok;
          }
          else authNet = null;

          // Time of creation not used for import into db?
          tok = strtok.nextToken()+" "+strtok.nextToken();
          double time = parseTimeString(tok);
          // if the time is crap (0000/00/00 00:00:00.000), set null 
          if (time <= crapTime) time = Double.NaN;
      }
      catch (Exception ex) {
          System.err.println("* GroMoPacket Error: bad AUTH: "+str);
          return false;
      }
      return true;
  }

  /** Returns the line type, that is the first token on the string that indentifies how to parse it.*/
  private static String getLineType(String str) {
      return str.substring(0, str.indexOf(":")+1); // find 1st token
  }

  /** Given AmpList with many stations, return a valid formated string. Amps must be
   * from the same channel and should represent the amp types that go into this
   * packet: PGA, PGV, PGD, SP0.3, SP1.0, SP3.0 although additional "RSA" values
   * are allowed. Returns empty string on error. */
  public static String format(long evid, AmpList amplist) {

      StringBuilder sb = new StringBuilder((amplist.size()+4)*80);

      // ChannelGrouper will sort alphabetically and by component and hand us groups by STATION.
      // This is necessary because the format lumps many amps from a single STATION together
      ChannelGrouper changrp = new ChannelGrouper(amplist);

      AmpList newList = null;
      AmpList ampgrp = null;
      while (changrp.hasMoreGroups()) {
        ampgrp = (AmpList) changrp.getNext();   // one station's worth of amps

        // check that list
        newList = cullAmplist(ampgrp);
        if (!newList.isEmpty()) sb.append(formatOneChannel(evid, newList));
      }

      return sb.toString();
  }

  public static String formatXY(long evid, String eventAuth, AmpList amplist) {

      StringBuilder sb = new StringBuilder((amplist.size()+4)*80);

      // ChannelGrouper will sort alphabetically and by component and hand us groups by STATION.
      // This is necessary because the format lumps many amps from a single STATION together
      ChannelGrouper changrp = new ChannelGrouper(amplist);

      AmpList newList = null;
      AmpList ampgrp = null;
      while (changrp.hasMoreGroups()) {
        ampgrp = (AmpList) changrp.getNext();   // one station's worth of amps

        // check that list
        newList = cullAmplist(ampgrp);
        if (!newList.isEmpty())  sb.append(formatOneChannelXY(evid, eventAuth, newList));
      }

      return sb.toString();
  }

/** Given an AmpList with amps from only one station, return a valid formated string. Amps must be
 * from the same channel and should represent the amp types that go into this
 * packet: PGA, PGV, PGD, SP0.3, SP1.0, SP3.0 although additional "RSA" values
 * are allowed. Returns empty string on error. */
  public static String formatOneChannel(long evid, AmpList amplist) {

    Amplitude amp;

    // sanity checks on AmpList
    if (amplist == null || amplist.isEmpty()) {
      System.err.println("* GroMoPacket Error: AmpList is empty.");
      return "";
    }
    if (amplist.getChannelCount() > 1) {
      System.err.println("* GroMoPacket Error: AmpList contains more than one station.");
      return "";
    }

    StringBuilder sb = new StringBuilder((amplist.size()+4)*80);
    Amplitude amps[] = amplist.getArray();

    // SCNL:
    sb.append(formSCNLline(amps[0].getChannelObj()));
    sb.append(eol);

    // TIME:
    sb.append("TIME: ");
    sb.append(getTimeString((Amplitude)amplist.getNearestToTime(0.0)));
    sb.append(eol);

    // ALT: -- not used. Not meaningfull for datasources that have a @%^!#$ clue.

    // PGA:
    amp = amplist.getType(AmpType.PGA);
    if (amp != null) {
      String rsastr = formPGline(amp, AmpType.PGA);
      if (rsastr != null) {
        sb.append(rsastr);
        sb.append(eol);
      }
    }

    // PGV:
    amp = amplist.getType(AmpType.PGV);
    if (amp != null) {
      sb.append(formPGline(amp, AmpType.PGV));
      sb.append(eol);
    }

    // PGD:
    amp = amplist.getType(AmpType.PGD);
    if (amp != null) {
      sb.append(formPGline(amp, AmpType.PGD));
      sb.append(eol);
    }
    // RSA:
    String rsaStr = formRSAline(amplist);
    if (rsaStr != null) {
      sb.append(rsaStr);
      sb.append(eol);
    }

    // QID:
    sb.append("QID: " + evid);
    sb.append(" "+getWormLogo());
    sb.append(eol);

    if (getFormatType() == TYPE_GMP) {  // GMP only NOT EWII
      // QUAL:
      sb.append("QUAL: "+getQual());
      sb.append(eol);
      // POST:
      DateTime dt = new DateTime();  // now
      sb.append("POST: ");
      sb.append(netCode);
      sb.append(" ");
      sb.append(dt.toString()); // UTC string - aww 2008/02/08
      sb.append(eol);
    }
    // final space for readablity
    sb.append(eol);
    return sb.toString();
  }

  public static String formatOneChannelXY(long evid, String eventAuth, AmpList amplist) {

    Amplitude amp = null;

    // sanity checks on AmpList
    if (amplist == null || amplist.isEmpty()) {
      System.err.println("* GroMoPacket Error: AmpList is empty.");
      return "";
    }
    if (amplist.getChannelCount() > 1) {
      System.err.println("* GroMoPacket Error: AmpList contains more than one station.");
      return "";
    }

    StringBuilder sb = new StringBuilder((amplist.size()+4)*80);
    Amplitude amps[] = amplist.getArray();

    // SCNL:
    sb.append(formSCNLline(amps[0].getChannelObj()));
    sb.append(eol);

    String astr = null;

    // EVID:
    if (evid > 0l) { 
        astr = formLineEVID(evid, eventAuth);
        if (astr != null) {
            sb.append(astr);
            sb.append(eol);
        }
    }

    // AUTH: creating network is local net code
    astr = formLineAUTH();
    if (astr != null) {
        sb.append(astr);
        sb.append(eol);
    }

    // WINDOW:
    astr = formLineWINDOW(amplist);
    if (astr != null) {
        sb.append(astr);
        sb.append(eol);
    }

    // PGA:
    amp = amplist.getType(AmpType.PGA);
    if (amp != null) {
      astr = formLinePG(amp, AmpType.PGA);
      if (astr != null) {
          sb.append(astr);
          sb.append(eol);
      }
    }

    // PGV:
    amp = amplist.getType(AmpType.PGV);
    if (amp != null) {
      astr = formLinePG(amp, AmpType.PGV);
      if (astr != null) {
          sb.append(astr);
          sb.append(eol);
      }
    }

    // PGD:
    amp = amplist.getType(AmpType.PGD);
    if (amp != null) {
      astr = formLinePG(amp, AmpType.PGD);
      if (astr != null) {
          sb.append(astr);
          sb.append(eol);
      }
    }
    // SA:
    amp = amplist.getType(AmpType.SP03);
    if (amp != null) {
      astr= formLineSA(amp);
      if (astr != null) {
          sb.append(astr);
          sb.append(eol);
      }
    }
    // SA:
    amp = amplist.getType(AmpType.SP10);
    if (amp != null) {
      astr= formLineSA(amp);
      if (astr != null) {
          sb.append(astr);
          sb.append(eol);
      }
    }
    // SA:
    amp = amplist.getType(AmpType.SP30);
    if (amp != null) {
      astr= formLineSA(amp);
      if (astr != null) {
          sb.append(astr);
          sb.append(eol);
      }
    }

    // final space for readablity
    sb.append(eol);

    return sb.toString();
  }

  private static String formLinePG(Amplitude amp, int type) {
    String tstr = AmpType.getString(type);
    StringBuilder sb = new StringBuilder(80);

    double val = amp.getValueAsCGS();     // must be cm/s/s
    if (Double.isNaN(val)) {
      return null;
    } else {
      val = Math.abs(val);          // must be positive
      sb.append(tstr+": ");
      sb.append(df64.form(val));
      sb.append(" T"+tstr+": ");
      sb.append(getTimeString(amp));
      if (tstr.equals("PGA")) {
          if (amp.getChannelObj().isLowGain()) sb.append(" METH: MS");
          else sb.append(" METH: DV");
      }
      else {
          if (amp.getSource().startsWith("GMP")) sb.append(" METH: -"); 
          else sb.append(" METH: IR KMH");
      }
    }
    return sb.toString();
}

  private static String formLineSA(Amplitude amp) {
    StringBuilder sb = new StringBuilder(80);

    double val = amp.getValueAsCGS();     // must be cm/s/s
    if (Double.isNaN(val)) {
      return null;
    } else {
      val = Math.abs(val);          // must be positive
      sb.append("SA: ");
      //double per = amp.period.doubleValue();
      double per = AmpType.getPeriod(amp.getType());
      
      if (Double.isNaN(per) || per <= 0.) per = -1.0;
      sb.append(df64.form(per));
      sb.append(" ");
      sb.append(df64.form(val));
      sb.append(" TSA: ");
      sb.append(getTimeString(amp));
      if (amp.getSource().startsWith("GMP")) sb.append(" METH: -"); 
      else sb.append(" METH: KMH");
    }
    return sb.toString();
  }

//
  private static String formLineWINDOW(AmpList amplist) {
    if (amplist == null || amplist.size() == 0) return null;
    //TimeSpan ts = amplist.getMinMaxTimeSpan(); // assume all amps from channel had same window as first amp in list
    StringBuilder sb = new StringBuilder(80);
    Amplitude amp = (Amplitude) amplist.get(0);
    double start = amp.windowStart.doubleValue();
    double dur = amp.windowDuration.doubleValue();
    if (Double.isNaN(dur)) {
        dur = -1.;
    }
    if (Double.isNaN(start)) {
        start = amp.getTime();
    }
    sb.append("WINDOW: ");
    sb.append(getTimeString(start));
    sb.append(" LENGTH: ");
    sb.append(String.format("%7.3f", dur));
    return sb.toString();
  }

  private static String formLineEVID(long evid, String auth) {
    if (evid <= 0l) return null;
    StringBuilder sb = new StringBuilder(80);
    sb.append("EVID: ");
    sb.append((auth == null || auth.length() == 0) ? "-" : auth);
    sb.append(" ");
    sb.append(evid);
    return sb.toString();
  }

  private static String formLineEVID(Solution sol) {
    if (sol == null) return null;
    return formLineEVID(sol.getId().longValue(), sol.eventAuthority.toString());
  }

  private static String formLineAUTH() {
    StringBuilder sb = new StringBuilder(80);
    sb.append("AUTH: ");
    sb.append(netCode);
    sb.append(" ");
    sb.append(LeapSeconds.trueToString(new DateTime().getTrueSeconds(), "yyyy/MM/dd HH:mm:ss.fff"));
    return sb.toString();
  }
//

/** Return an new AmpList containing only "valid" amp. This means that the amptype
 * is of a known type and the amp value is > 0.0.
 @see: AmpType */

  public static AmpList cullAmplist(AmpList list) {

    AmpList newList = AmpList.create();
    Amplitude amp[] = list.getArray();

    for (int i = 0; i< list.size();i++){
      /* Check that amp value > 0.0 else it will bounce off Dbase constraint.*/
      if ( !validTypeArray.contains(amp[i].type)) {
        System.err.println("* GroMoPacket Error: Invalid amp type for GroMo packets: ");
        System.err.println(amp[i].toNeatString());
        }
      else if ( amp[i].getValue().doubleValue() < 0.0)  {
        System.err.println("* GroMoPacket Error: Illegal value, must be > 0.0: ");
        System.err.println(amp[i].toNeatString());
      } else {
        newList.add(amp[i], false);
      }
    }
    return newList;

  }
// From Earthworm v5.2 transport.h
//  typedef struct {                 /******** description of message *********/
//        unsigned char    type;     /* message is of this type               */
//        unsigned char    mod;      /* was created by this module id         */
//        unsigned char    instid;   /* at this installation                  */
//} MSG_LOGO;                        /*****************************************/
/** Return an Earthworm logo for the QID field. */
private static String getWormLogo() {
  int installation = org.usgs.util.EarthwormTypes.INST_CIT;
  int msgType = 14;  // makes no sense, perhaps example is bogus
  int modid = org.usgs.util.EarthwormTypes.MOD_GROMOPACKET;

  Format dfi3 = new Format("%3.3d");

  return dfi3.form(installation) + dfi3.form(msgType) + dfi3.form(modid)+ ":"+getNetCode();

}
private static String getQual() {
  return "1/-/2/1";   // old convention
}

  // FORMAT LINE FORMATTERS:
  /** Return a string formated asl yyyy/mm/dd hh:mm:ss.ssss */
  public static String getTimeString(Amplitude amp) {
      return getTimeString(amp.getTime());
  }

  public static String getTimeString(double time) {
    if (time == 0.) return "0000/00/00 00:00:00.000";
    return LeapSeconds.trueToString(time, "yyyy/MM/dd HH:mm:ss.fff"); // for UTC seconds -aww 2008/02/08
  }

/** Make the SCNL line with the fields delimited by "."
 * Don't mess with the "optional" stuff in ()'s.  */
private static String formSCNLline(Channel chan) {
  return "SCNL: " + formSCNL(chan);
}
/** Return the SCNL with the subfields delimited by "." and an "-" in any null field.
 * Example: "CMB.BHZ.BK.--" */
public static String formSCNL(Channel chan) {
  StringBuilder sb = new StringBuilder(16);
  ChannelName cn =  chan.getChannelName();
  tackOn (sb, cn.getSta());
  sb.append(".");
  tackOn (sb, cn.getSeedchan());
  sb.append(".");
  tackOn (sb, cn.getNet());
  sb.append(".");
  tackOn (sb, cn.getLocationString());
//  tackOn (sb, cn.getLocation());
  // Location Code Kludge !!!!!!
  //tackOnLoc (sb, cn.getLocation());
  return sb.toString();
}

private static void tackOn(StringBuilder sb, String str) {
  if (str == null || str.equalsIgnoreCase("NULL")) {
    sb.append(NullString);   // "--"
  } else {
    sb.append(str);
  }
}

/** Location Code Kludge for CI - added (then removed at Lynn Dietz request - 2/20/04) !!!!!!
 * RT system not yet putting Location Code on amps. 2/17/04 */
//private static void tackOnLoc (StringBuilder sb, String str) {
//  if (str == null || str.equalsIgnoreCase("NULL")) {
//    sb.append("01");
//  } else {
//    sb.append(str);
//  }
//}
/** Make a PGx line for the given type. Converts to CGS units. */
private static String formPGline(Amplitude amp, int type) {
  String tstr = AmpType.getString(type);
  StringBuilder sb = new StringBuilder(80);

  double val = amp.getValueAsCGS(); // must be cm/s/s
  //double val = amp.getValue().doubleValue(); // spectral values can't be cgs
  if (Double.isNaN(val)) {
    return null;
  } else {
    val = Math.abs(val);          // must be positive
    sb.append(tstr+": ");
    sb.append(df64.form(val));
    sb.append(" T"+tstr+": ");
    sb.append(getTimeString(amp));
    return sb.toString();
  }

}

/** Spectral accelerations. Returns null if there are none. */
private static String formRSAline(AmpList sublist) {

  final int rsaTypes[] = {AmpType.SP03, AmpType.SP10, AmpType.SP30};

  Amplitude amp = null;
  double per= 0.;
  double val = 0.;
  int knt = 0;

  StringBuilder sb = new StringBuilder(80);

// spin thru the list
  // makes a string like "0.30 0.932/ 1.0 3.234/ 3.0 32.23/"
  int unitsType = Units.SPA; // 
  for (int i = 0; i< rsaTypes.length; i++ ){

    amp = sublist.getType(rsaTypes[i]);       // get amp of each type
    if (amp != null && !amp.value.isNull()) {
      val = Math.abs(amp.getValue().doubleValue());
      per = AmpType.getPeriod(rsaTypes[i]);
      if (!Double.isNaN(val) && per > 0.0) {
        sb.append(df32.form(per));
        sb.append(" ");

        // Convert from spectral velocity to spectral accel. 6/15/04
        // NOTE: AmpGenPP uses TotalGroundMotionFilter class whose filter calc produces spectral velocities cms but 
        // calls AmpType default which which incorrectly assign them ACC cmss units !
        // Likewise RSAFilter (only directly used in Jiggle wfpanel filter?) generates spectral VEL but has its units incorrectly assigned as ACC cmss too.
        //sb.append(df64.form(vel2Accel(per, val))); // Lombard says RT RAD outputs amps as ACCEL - 2009/03/05 aww
        // -aww 2010/01/14 fix replaced append above with :
        unitsType = amp.getUnits(); // get amp units
        if (Units.isVelocity(unitsType) || unitsType == Units.SPA) {
           val = vel2Accel(per, val);
        } // else val assumed to be acceleration
        sb.append(df64.form(val));
        sb.append("/");
        knt++;
      }
    }
  }
  if (knt == 0)
    return null;
  else
    return "RSA: "+ knt +"/" + sb.toString();

}

/**
 * Convert from spectral velocity to spectral accel.<p>
 *
 * Accel = Vel * 2 * PI * (freq)
 *<p>
 * Formula from Bruce Worden.
 * @param  spectral velocity
 * @return spectral acceleration
 */
  static double vel2Accel(double period, double vel) {

    return vel * TwoPi * (1.0/period);
  }
  /**
   * Convert from spectral acceleration to spectral velocity.<p>
   *
   * Vel = Accel / ( 2 * PI * (freq) )
   *<p>
   * Formula from Bruce Worden.
   * @param  spectral velocity
   * @return spectral acceleration
   */
    static double accel2Vel(double period, double accel) {
      return accel / (TwoPi * (1.0/period));
  }
// FORMAT LINE PARSERS: all results are put in ampList
/**
 * Parse the "SCNL" line. Does not parse the crap in ()'s
 * <pre>
SCNL: 24611.HNN.CE. (34.059, -118.260, LA - Temple & Hope)
SCNL: CMB.BHZ.BK.--
</pre>
  */
private static boolean parseSCNL(String str) {  // throws FormatException  {
  delim = " ";
  strtok = new StringTokenizer(str, delim);
  // sanity check - this should never happen
  if ( !strtok.hasMoreTokens() || !strtok.nextToken().equals("SCNL:")) {
      System.err.println("* GroMoPacket Error SCNL: Bad tag: "+str);
      return false;
  }

  // parse the line starting w/ 1st field past the tag
  if (!strtok.hasMoreTokens()) {
      System.err.println("* GroMoPacket Error: SCNL lime missing channel name: "+str);
      return false;
  }

  tok = strtok.nextToken();
  chanName = parseDottedSCNL(tok);

  // Assume channel owner is the auth of these amps the format doesnt really tell us
  authNet = chanName.getNet();

  return true;
}

/** Parse a dotted format string of the form sta.chn.net.loc, Example: 24611.HNN.CE.
 Each field is optional and may be mission or "-"*/
private static ChannelName parseDottedSCNL(String string) { // throws FormatException  {

  ChannelName cname = new ChannelName();
  String str = null;
  // don't use static StringTokenizer here because calling method may be using it
  StringTokenizer strtok = new StringTokenizer(string, ".");

  // station
  if (strtok.hasMoreTokens()) {
    str = strtok.nextToken();
    if (!tokenIsNull(str)) cname.setSta(str);
  }
  // channel
  if (strtok.hasMoreTokens()) {
    str = strtok.nextToken();
    if (!tokenIsNull(str)) {
      // truncate if too long - Kludge needed because CGS sends crap like "HNH-180"
      if (str.length()>MAX_CHAN_LENGTH) {
        System.out.print("* GroMoPacket WARNING: truncating bad SeedChan: "+cname.getSta() + "."+str);
        str = str.substring(0, MAX_CHAN_LENGTH);
        System.out.println(" to "+str);
      }
      cname.setChannel(str);
      cname.setSeedchan(str); // set seedchan the same
    }
  }
  // net
  if (strtok.hasMoreTokens()) {
    str = strtok.nextToken();
    if (!tokenIsNull(str)) cname.setNet(str);
  }
  // location
  if (strtok.hasMoreTokens()) {
    str = strtok.nextToken();
    if (!tokenIsNull(str)) cname.setLocation(str);  // treats "-" and "--" correctly
  }

  // set channel "namespace" to default value of "SEED"
  cname.setChannelsrc("SEED");

  return cname;
}

/**
Parse time of the format:
<pre>
2001/02/25 02:37:02.000
</pre>
* Note that "unknown" time is represented by:
<pre>
0000/00/00 00:00:00.000
</pre>
* which parses just fine.
 */
private static boolean parseTIME(String str)  { // throws FormatException  {
// don't use static StringTokenizer here because calling method may be using it
  StringTokenizer stok = new StringTokenizer(str, " ");
  if (isMalformed(str, stok, "TIME:", 2)) return false;   // format check

  try {
    // parse the line starting w/ 1st field past the tag
    // put date & time backtogether into a parsable string
    baseTime = parseTimeString(stok.nextToken()+" "+stok.nextToken());
    //if (debug) System.out.println("DEBUG GroMoPacket parseTIME: " + LeapSeconds.trueToString(baseTime));
  }
  catch (Exception ex) {
    System.err.println("* GroMoPacket Error: Malformed TIME line: "+ex.getMessage()+"\n"+str);
    return false;
  }
  return true;
}
/** Format expected is "yyyy/MM/dd HH:mm:ss.SSS"
 * Note that "unknown" time is represented by:
 <pre>
 0000/00/00 00:00:00.000
 </pre>
 * */
private static double parseTimeString(String string) { // throws FormatException  {
  return LeapSeconds.stringToTrue(string, "yyyy/MM/dd HH:mm:ss.SSS"); // UTC string - aww 2008/02/08
}
/** Parse the ALT time (whatever that is)
 <pre>
 ALT: 2001/02/25 02:36:57.000 CODE: 4
 <\pre>
 * */
private static boolean parseALT(String str) { // throws FormatException  {
  strtok = new StringTokenizer(str, " ");
  // sanity check - this should never happen
  if (isMalformed(str, strtok, "ALT:", 4)) return false;   // format check

  // parse the line starting w/ 1st field past the tag
  if (strtok.countTokens() > 2) {
    tok = strtok.nextToken()+" "+strtok.nextToken();
    altTime = parseTimeString(tok);
  }
  else {
      System.err.println("* GroMoPacket Error: Malformed ALT line:\n"+str);
      return false;
  }
  return true;
}

/** Parse the PGA value and time
 <pre>
 PGA: 6.8462 TPGA: 2001/02/25 02:37:04.000
</pre>
 * */
private static Amplitude parsePGA(String str) { //  throws FormatException  {
  Amplitude amp = parseAmp(str, AmpType.PGA);
  if (amp != null) amp.setUnits(GroMoPacket.PGAunits);
  return amp;
}
/** Parse the PGV value and time
 <pre>
 PGV: 0.1400 TPGV: 2001/02/25 02:37:07.000
 </pre>
 * */
private static Amplitude parsePGV(String str) {  // throws FormatException  {
  Amplitude amp = parseAmp(str, AmpType.PGV);
  if (amp != null) amp.setUnits(GroMoPacket.PGVunits);
  return amp;
}

/** Parse the PGD value and time
 <pre>
 PGD: 0.0250 TPGD: 2001/02/25 02:37:10.000
 </pre>
 * */
private static Amplitude parsePGD(String str)  { // throws FormatException  {
  Amplitude amp = parseAmp(str, AmpType.PGD);
  if (amp != null) amp.setUnits(GroMoPacket.PGDunits);
  return amp;
}
/**
 * Generic PGx parser used to parse PGA, PGV, PGD
 */
private static Amplitude parseAmp(String str, int ampType) { // throws FormatException  {

  double time, val;
  strtok = new StringTokenizer(str, " ");
  String tokString = AmpType.getString(ampType)+":";
  if (isMalformed(str, strtok, tokString, 2)) return null;   // format check

  try {

    val = Double.parseDouble(strtok.nextToken());

    if ( !strtok.nextToken().equals("T"+tokString)) {
      System.err.println("* GroMoPacket Error: Bad tag: "+str);
      return null;
    }
    tok = strtok.nextToken()+" "+strtok.nextToken();
    time = parseTimeString(tok);

    // if the time is crap (0000/00/00 00:00:00.000) use the base time
    if (time < crapTime) time = baseTime;

  }
  catch (Exception ex) {
    System.err.println("* GroMoPacket Error: Malformed line: "+ex.getMessage()+"\n"+str);
    return null;
  }

  // Sanity check of time
  if (time < crapTime) {
    System.err.println(" * GroMoPacket Error: Bad time: "+str);
    return null;
  }
  // Sanity check of value: PNL 2009/03/20
  if (val <= 0.0) {
      System.err.println(" * GroMoPacket Error: Bad amp value:"+str);
      return null;
  }
  // all is well - make an AMP
  Amplitude amp = newAmp(ampType);
  amp.setTime(time);
  amp.value.setValue(val);

  // the window bounds are NOT NULL but I have no reasonable values to put in them
  // change below for SCWG update of windowStart, duration rules:
  // For each amplitude where we know the time and the window  set all the fields (datetime, wstart, duration).
  // For each amplitude where we know the time but not the window: Amp.datetime = Amp.wstart and Amp.duration = 0
  // For each amplitude where we don't know the time nor the window: Amp.datetime = NULL, Amp.wstart = <Origin Time>, Amp.duration = NULL
  //amp.setWindow(new TimeSpan(time, time)); // new style
  amp.setWindow(new TimeSpan(time, time+1.0)); // old style

  // set correct units - the spec says 'cm' is required.
  if (ampType == AmpType.PGA) {
    amp.setUnits(Units.CMSS);
  } else if (ampType == AmpType.PGV) {
    amp.setUnits(Units.CMS);
  } else if (ampType == AmpType.PGD) {
    amp.setUnits(Units.CM);
  }

  // add the new amp to the static list
  amplist.add(amp, false);

  return amp;
}

/** Return an empty UnassocAmplitude object stamped with the correct Channel Name. */
private static Amplitude newAmp(int ampType) {
  // ////////////////////////////////
  // NOTE: this breaks the generality rules
  // ////////////////////////////////
  Amplitude amp = UnassocAmplitude.create();

  Channel ch = Channel.create();
  ch.setChannelName(chanName);
  amp.setChannelObj(ch);
  amp.setAuthority(authNet);
  //amp.setWeightUsed(1.0); // AssocAm? weight used vs. inWgt -aww
  amp.setInWgt(1.0); // AssocAm? weight used vs. inWgt -aww
  amp.setQuality(1.0); // how does this relate to inWgt -aww
  amp.setType(ampType);
  amp.setSource(source);

  return (Amplitude) amp;
}

/** Response spectrum amplitudes. Usually the three used by ShakeMap at
 * 0.30, 1.0, and 3.0 seconds. Line may contain a maximum of 20 pairs.
<pre>
       |--------fixed spectral amps--------|-max amp-|
tag  #  per  val    per    val  per    bal   per  val
RSA: 3/0.30 4.4154/1.00 0.9256/3.00 0.2979//.50 5.1233
</pre>
* */

private static boolean parseRSA(String str)  { // throws FormatException  {
  delim = " /";   //  two delimiters
  strtok = new StringTokenizer(str, delim);
  if (isMalformed(str, strtok, "RSA:", 3)) return false;   // format check

  String tmp = strtok.nextToken();  // #of per,amp pairs

  double per =0.;
  double acc = 0.;

  try {
    while (strtok.hasMoreTokens()) {
      tmp = strtok.nextToken();
      if (tmp.length() == 0) continue;  // skip "//"
      per = Double.parseDouble(tmp);
      acc = Double.parseDouble(strtok.nextToken());
      // Sanity check of value: PNL 2009/03/20
      if (acc <= 0.0) {
          System.err.println(" * GroMoPacket Error: Bad RSA value:"+str);
          continue;
      }
      makeSP(per, acc);
    }
  }
  catch (Exception ex) {
    ex.printStackTrace();
    System.err.println("* GroMoPacket Error: Malformed line: "+"\n"+str);
    return false;
  }
  return true;
}
/** Create an Amplitude object for a spectral acceleration at this period and add
 it to the static amp list. Only periods of 0.3, 1.0 & 3.0 are defined so any other period
 will be of type "UNKNOWN".
 @see: AmpType
 */
private static Amplitude makeSP(double period, double accel) {

  int ampType = AmpType.UNKNOWN;
  if (period == 0.3) ampType = AmpType.SP03;
  if (period == 1.0) ampType = AmpType.SP10;
  if (period == 3.0) ampType = AmpType.SP30;

  Amplitude amp = newAmp(ampType);
  amp.setTime(baseTime);
  //
  // Convert accel to vel (So Cal convention)  6/15/04
  //
  // NOTE: imported RAD RT generated amps are ACC 
  // 20100113 - Changing java code so that AmpGenPP amps will be multiplied by omega to go from spectral velocity cms to cmss ACC
  // AmpGenPP "GMP" source amps that were exported after 03/05/2009 up to change were VEL (cms) but units assigned incorrectly as ACC (cmss) 
  // BUG: import rescaled an exported  RT source amp ACC as VEL (cm/s), but still saved it with units assigned as SPUnits=cmss
  //      import rescaled an exported GMP source amp VEL as DIS (cm), but still saved it with units assigned as SPUnits=cmss
  // amp.value.setValue(accel2Vel(period, accel)); // BUG between 20090305 and 20100113 -aww

  // Preserve amp as ACC where SPunits=cmss units - 20100113 aww
  amp.value.setValue(accel);

  amp.period.setValue(period);
  amp.setUnits(SPunits); // preserved as cmss

  // the window bounds are NOT NULL but I have no reasonable values to put in them
  // change below for SCWG update of windowStart, duration rules:
  // For each amplitude where we know the time and the window  set all the fields (datetime, wstart, duration).
  // For each amplitude where we know the time but not the window: Amp.datetime = Amp.wstart and Amp.duration = 0
  // For each amplitude where we don't know the time nor the window: Amp.datetime = NULL, Amp.wstart = <Origin Time>, Amp.duration = NULL
  // amp.setWindow(new TimeSpan(,)); // ??  new style ?? 
  amp.setWindow(new TimeSpan(baseTime, baseTime+1.0)); // old style
  amplist.add(amp);

  return amp;

}
private static Amplitude makeSA(double period, double accel, double time, TimeSpan ts) {

  int ampType = AmpType.UNKNOWN;
  if (period == 0.3) ampType = AmpType.SP03;
  if (period == 1.0) ampType = AmpType.SP10;
  if (period == 3.0) ampType = AmpType.SP30;

  Amplitude amp = newAmp(ampType);
  amp.setTime(time);
  amp.setWindow(ts);
  // Preserve amp as ACC where SPunits=cmss units
  amp.value.setValue(accel);
  if (period > 0.) amp.period.setValue(period);
  amp.setUnits(SPunits); // preserved as cmss
  amplist.add(amp);

  return amp;

}

  private static boolean isMalformed(String str, StringTokenizer strtok, String tagStr, int minToks) { // throws FormatException  {
    // sanity check - does the 1st token on the line match the tag? This should never happen.
    int cnt = strtok.countTokens();
    if (cnt == 0 || cnt < minToks) {
        System.err.println("GroMoPacket ERROR: Too few tokens: /"+str+"/"); // throw new FormatException("* GroMoPacket: Too few tokens: /"+str+"/");
        return true;
    }
    else if (!strtok.nextToken().equals(tagStr)) {
        System.err.println("GroMoPacket ERROR: Bad tag: /"+str+"/"); // throw new FormatException("* GroMoPacket: Bad tag: /"+str+"/");
        return true;
    }
    return false;
  }

/** Parse QID: line. Ignores the Earthworm junk. */
//                 EW horseshit
//              vvvvvvvvvvvvvvvvvv
//QID: 41059467 014024003:UCB 1233
private static boolean parseQID(String str)  { // throws FormatException   {
  delim = " ";
  strtok = new StringTokenizer(str, delim);
  if (isMalformed(str, strtok, "QID:", 2)) return false;   // format check

  String nt = strtok.nextToken();
  if (nt.equals("-")) return true;  // id not defined -aww 2011/01/16

  if (!tokenIsNull(nt)) {
    try {
      evid = Long.parseLong(nt);               // sets static evid used elsewhere
    }
    catch (NumberFormatException ex) {
      System.err.println("* GroMoPacket: QID bad number format: "+nt+ " "+ex.toString()+"\n"+str);
      //throw new FormatException("* GroMoPacket: QID bad number format: "+nt+ " "+ex.toString()+"\n"+str);
      return false;
    }
  }

  // the rest is useless information
  //  long logo = 0;
  //  try {
  //    logo = Long.parseLong(strtok.nextToken());
  //  } catch (NumberFormatException ex) {
  //    System.err.println("Malformed line: "+str);
  //    return;
  //  }

  return true;
}

/** Return true if the string is the NULL placeholder. */
// DDG 4/4/08 - Changed to stop treating "--" or "-" as null
  static boolean tokenIsNull(String tok) {
    if (tok == null || tok.length() == 0 || tok.equals("?")) {  // "?" bogusness from MP
        return true;
    } else {
        return false;
    }
  }

/**
 1) time quality
 2) time error est.
 3) amp quality
 4) Site type
 */
//QUAL: 1/0.0/1/2
  @SuppressWarnings("unused")
private static boolean parseQUAL(String str)  { // throws FormatException  {
  delim = " /";  // note two delimiters " " & "/"
  strtok = new StringTokenizer(str, delim);
  if (isMalformed(str, strtok, "QUAL:", 4)) return false;   // format check

  boolean status = true;
  try {
      int   timeQual = Integer.parseInt(strtok.nextToken());
      double timeErr = 0.0;
      String err = strtok.nextToken();
      if (!tokenIsNull(err)) timeErr = Double.parseDouble(err);
      int ampQual = Integer.parseInt(strtok.nextToken());
      int siteType = Integer.parseInt(strtok.nextToken());
      String qaString = null;
      // optional field for, who knows what
      if (strtok.hasMoreTokens()) qaString = strtok.nextToken();

  }
  catch (Exception ex) {
      System.err.println("* GroMoPacket Error: Malformed line: "+ex.getMessage()+"\n"+str);
      status = false;
  }

  return status;

}

/** "Postmark" of sending entity. May be other than the data source.
 Theoretically a packet can have multiple POST: lines listing all the places it was
 resent through. (Sort of like a tracert?) <p>
 * Net ID followed by time sent.
<pre>
POST: CI 2001/02/25 02:40:30.000
</pre>
*/
private static boolean parsePOST(String str) { // throws FormatException  {
  delim = " ";
  strtok = new StringTokenizer(str, delim);
  if (isMalformed(str, strtok, "POST:", 3)) return false;   // format check
  return true;
  // nothing useful in this line, skip it

}

/** A test message. */
private static String getTestMessage() {
  return "SCNL: CMB.BHZ.BK.--\n"+
      "TIME: 2001/02/25 02:37:02.000\n"+
      "ALT: 2001/02/25 02:36:57.000 CODE: 4\n"+
      "PGA: 6.8462 TPGA: 2001/02/25 02:37:04.000\n"+
      "PGV: 0.1400 TPGV: 2001/02/25 02:37:07.000\n"+
      "PGD: 0.0250 TPGD: 2001/02/25 02:37:10.000\n"+
      "RSA: 3/0.30 4.4154/1.00 0.9256/3.00 0.2979/\n"+
      "QID: 41059467 014024003:UCB 1233\n"+
      "QUAL: 1/0.0/1/2\n"+
      "POST: CI 2001/02/25 02:40:30.000\n";
}
private static String getTestMessage2() {
    StringBuilder sb = new StringBuilder(512);
    sb.append("SCNL: CMB.BHZ.BK.--");
    sb.append("\n");
    sb.append("WINDOW: 0000/00/00 00:00:00.000 LENGTH: 0.0");
    sb.append("\n");
    sb.append("PGA: 6.8462 TPGA: 2001/02/25 02:37:04.000 METH: MS");
    sb.append("\n");
    sb.append("PGV: 1.0400 TPGV: 2001/02/25 02:37:07.000 METH: IF 3.3");
    sb.append("\n");
    sb.append("PGD: 0.0250 TPGD: 2001/02/25 02:37:10.000 METH: IR KMH");
    sb.append("\n");
    sb.append("SA: 0.3 4.41541 TSA: 2001/02/25 02:37:05.000 METH: NJ");
    sb.append("\n");
    sb.append("SA: 1.0 0.92562 TSA: 2001/02/25 02:37:08.000 METH: NJ");
    sb.append("\n");
    sb.append("SA: 3.0 0.29793 TSA: 2001/02/25 02:37:12.000 METH: KMH");
    sb.append("\n");
    sb.append("EVID: NC 41059467");
    sb.append("\n");
    sb.append("AUTH: CI 2001/02/25 02:40:30.000");
    sb.append("\n");
    return sb.toString();
}

// Inner class
    private static class IntArray {
      int[] array;
      public IntArray(int[] iarray) {
        array = iarray;
      }
      public boolean contains(int val) {
        for (int i = 0; i<array.length; i++) {
          if (array[i] == val) return true;
        }
        return false;
      }
    }

//
  public static void main(String[] args) {

//    long evid =     13951892;
//    long evid =     9983625;
//
//    if (args.length <= 0)        // no args
//    {
//      System.out.println("Usage: java GroMoPacket [evid])");
//
//      System.out.println("Using evid "+ evid+" as a test...");
//
//    } else {
//
//      Integer val = Integer.valueOf(args[0]);            // convert arg String
//      evid = (int) val.intValue();
//    }
//
//    DataSource ds = TestDataSource.create();
//    Amplitude amp = Amplitude.create();
//    AmpList amplist = amp.getBySolution(evid);
//
//    String megastr = GroMoPacket.format(evid, amplist);
//
//    System.out.println(megastr);

// parse test

//    File file = new File("C:\\temp\\UCB\\In\\0214331.smMP");

//    String msg =
//          "SCNL: CMB.BHZ.BK.--\n"+
//          "TIME: 2001/02/25 02:37:02.000\n"+
//          "ALT: 2001/02/25 02:36:57.000 CODE: 4\n"+
//          "PGA: 6.8462 TPGA: 2001/02/25 02:37:04.000\n"+
//          "PGV: 0.1400 TPGV: 2001/02/25 02:37:07.000\n"+
//          "PGD: 0.0250 TPGD: 2001/02/25 02:37date:10.000\n"+
//          "RSA: 3/0.30 4.4154/1.00 0.9256/3.00 0.2979//.50 5.1233\n"+
//          "QID: 41059467 014024003:UCB 1233\n"+
//          "QUAL: 1/0.0/1/2\n"+
//          "POST: CI 2001/02/25 02:40:30.000\n";
    try {
      //AmpList amps = parse(file);

      GroMoPacket.setNetCode("CI");

      AmpList amps;
      /*
      System.out.println("GroMoPacket Old parse:");
      GroMoPacket.setFormatType(GroMoPacket.TYPE_EW2);
      amps = parse(GroMoPacket.getTestMessage());
      System.out.println("GroMoPacket amps.size():" + ((amps == null) ? 0 :amps.size()));
      System.out.println(amps.toNeatString());
      if (amps.size() > 0) {
        Solution sol = ((Amplitude)amps.get(0)).getAssociatedSolution();
        System.out.println("sol id:"+sol.getId()+" eAuth:" +sol.eventAuthority + " eType:" + sol.getEventTypeString());
      }
      */


      System.out.println("GroMoPacket New parse:");
      GroMoPacket.setFormatType(GroMoPacket.TYPE_GMXY);
      amps = parse(GroMoPacket.getTestMessage2());
      System.out.println("GroMoPacket amps.size():" + ((amps == null) ? 0 :amps.size()));
      System.out.println(amps.toNeatString());
      //if (amps.size() > 0) {
      //  Solution sol = ((Amplitude)amps.get(0)).getAssociatedSolution();
      //  System.out.println("sol id:"+sol.getId()+" eAuth:" +sol.eventAuthority + " eType:" + sol.getEventTypeString());
      //}

      //System.out.println("GroMoPacket OLD format:");
      //System.out.println(GroMoPacket.format(41059467, amps));
      //System.out.println("GroMoPacket NEW format:");
      //System.out.println(GroMoPacket.formatXY(41059467, "NC", amps));
    }
    catch (Exception ex) {
        ex.printStackTrace();
    }

  }
//
}
