package org.trinet.formats;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.trinet.util.StringList;

/**
 * Access site-specific event message strings from the MessageText 
 * table of the database.
 *
 */
public class HeaderStrings {

    Connection conn = null;
    String net = "??";   // default net code
        
    public HeaderStrings (Connection conn) {
            this.conn = conn;
    }
        
    public HeaderStrings (String net, Connection conn) {
        this.net = net;
        this.conn = conn;
                
        // Network is unknown to caller - set from table value
        if (net.equals("??")) {
            this.net = getString ("??", "LocalNet").toString();
        }
    }

    /**
    * Return all the strings of the given type.
    * @param textType
    * @return
    */
    public StringList getString (String textType) {
        return getString(net, textType);
    }

    /**
    * Return all the strings of the given type from the MessageText table.
    * @param textType
    * @return
    */
    public StringList getString (String net, String textType) {

        StringList list = new StringList();
        
        String sql = "Select Text from MessageText where TextType like '"+ // changed from  CubeMessageText on 12/15/2008 -aww
        textType+ "' and SrcNet like '"+ net +"' order by LineNo";
        Statement stmt = null;
        ResultSet rs = null;
        
        try {
        
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
                
            while ( rs.next() ) {    // There may be more than 1 line
                list.add(rs.getString("Text"));
                // the "\n" will be stripped off later by StringList.toString("\n")
            }
                
        } catch (SQLException ex) {
                list.add("ERROR HeaderStrings " + ex.getMessage());
                list.add(sql); // add sql text for debug -aww 2010/08/02
                ex.printStackTrace(System.err);
        }
        finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException ex) {}
        }
        return list;            
    }
}
