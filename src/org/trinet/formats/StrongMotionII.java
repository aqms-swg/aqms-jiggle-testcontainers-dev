package org.trinet.formats;

import java.util.*;
import java.io.*;

import org.trinet.jasi.*;
import org.trinet.util.*;

/**
 * <p>Support for the Earthworm Ground Motion II Data Message Packet format. This format
 * is used to exchange strong motion amplitude data between CISN centers. This
 * class parses and writes the format to and from the org.trinet.jasi.Amplitude() class. </p>
 *
 * Note that the "GMP" format stuffs information about several amplitudes into
 * one message.<p>
 *
 * One message can contain: PGA, PGV, PGD, SP0.3, SP1.0, SP3.0 and a "maximum"
 * spectral peak acceleration. One "file" can contain multiple components.<p>
 *
 * This format is closely related to the CISN Ground Motion Data Message Packet format.<p>
 *
 * See: http://gldbrick.cr.usgs.gov/ew-doc/PROGRAMMER/strongmotionII_format.html<br>
 * See: http://gldbrick.cr.usgs.gov/DBMS/API-DOC/EW_LIBSRC/RW_STRONGMOTION_II/C/function_comments.html<br>
 *
 * <p>Copyright: Copyright (c) 2003 USGS</p>
 * @author Doug Given
 * @version 1.0
 * @see: org.trinet.formats.GroMoPacket()
 *  *
<pre>
<strong>Example:</strong>
SCNL: CMB.BHZ.BK.
TIME: 2001/02/25 02:37:02.000
ALT: 2001/02/25 02:36:57.000 CODE: 4
PGA: 6.8462 TPGA: 2001/02/25 02:37:04.000
PGV: 0.1400 TPGV: 2001/02/25 02:37:07.000
PGD: 0.0250 TPGD: 2001/02/25 02:37:10.000
RSA: 3/0.30 4.4154/1.00 0.9256/3.00 0.2979//.50 5.1233
QID: 41059467 014024003:UCB 1233
<p>
<strong>Triggered record:</strong>

SCNL: 24611.HNN.CE. (34.059, -118.260, LA - Temple & Hope)
TIME: 2001/09/09 23:59:23.000
ALT: 2001/09/09 23:59:18.000 CODE: 4
PGA: 46.123 TPGA: 2001/09/09 23:59:23.800
PGV: 2.010 TPGV: 2001/09/09 23:59:23.800
PGD: .0250 TPGD: 2001/09/09 23:59:23.800
RSA: 3/0.30 88.10/1.0 7.21/3.00 1.05//.19 130.5
QID: - -
</pre>
* @deprecated: use GroMoPacke.setFormatType();
 */

public class StrongMotionII extends GroMoPacket {

  // set the flag to make EarthwormII and not GMP
  {
    //EWII = true;
    source = "SM2";
  }

/*
  public static void main(String[] args) {

    long evid =     13951892;

    if (args.length <= 0) // no args
    {
      System.out.println ("Usage: java StrongMotionII [evid])");

      System.out.println ("Using evid "+ evid+" as a test...");

    } else {

      Integer val = Integer.valueOf(args[0]); // convert arg String
      evid = (int) val.intValue();
    }

    DataSource ds = TestDataSource.create();
    Amplitude amp = Amplitude.create();
    AmpList amplist = new AmpList(amp.getBySolution(evid));

    String megastr = GroMoPacket.format(evid, amplist);

    System.out.println(megastr);
  }
*/
}
