package org.trinet.hypoinv;
// notes: add color coding for editable/not and/or nullable/not?
// notes: require key columns in table; fix keys in row header
// notes: add custom editors based on columnname or datatype
// notes: add methods for deleting row from table/dbase
import java.util.Vector;
import javax.swing.table.AbstractTableModel;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import java.sql.Types;
import java.util.StringTokenizer;
import org.trinet.hypoinv.ArcStation;
import org.trinet.util.Concat;

public class ArcStationTableModel extends AbstractTableModel {

    Concat cc = new Concat();
    Vector rows = new Vector();
    static String[] colNames = {"SITE", "NET", "CHL", "DATE", "HRMN", "PSEC", "DIST", "AZM", "AN",
            "PD", "PWT", "PRES", "PWTU", "SD", "SWT", "SSEC", "SRES", "SWTU", "IMPP", "IMPS"};
    static boolean[] colAKey = {true, true, true, false, false, false, false, false, false, false, 
                    false, false, false, false, false, false, false, false, false, false}; 
    static int[] colTypes= {Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.VARCHAR, Types.FLOAT,
		 Types.FLOAT, Types.FLOAT, Types.FLOAT, Types.VARCHAR, Types.INTEGER, Types.FLOAT, Types.FLOAT,
		 Types.VARCHAR, Types.INTEGER, Types.FLOAT, Types.FLOAT, Types.FLOAT, Types.FLOAT, Types.FLOAT};

    public ArcStationTableModel() {
      super();
    }

    public void addRow(ArcStation arcStn) {
      Vector newRow = new Vector();
      newRow.addElement((Object) arcStn.site);
      newRow.addElement((Object) arcStn.net);
      newRow.addElement((Object) arcStn.chan);
      newRow.addElement((Object) (arcStn.year + "-" + cc.format(new StringBuffer(), arcStn.month, 2, 2).toString() + "-" +
		cc.format(new StringBuffer(), arcStn.day, 2, 2).toString() ) );
      newRow.addElement((Object) (cc.format(new StringBuffer(), arcStn.hr, 2, 2).toString() + ":" + 
		cc.format(new StringBuffer(), arcStn.mn, 2, 2).toString() ) );
      newRow.addElement((Object) Float.valueOf(arcStn.psec));
      newRow.addElement((Object) Float.valueOf(arcStn.dist));
      newRow.addElement((Object) Float.valueOf(arcStn.azes));
      newRow.addElement((Object) Float.valueOf(arcStn.eangle));
      newRow.addElement((Object) (arcStn.prmk.trim() + arcStn.pfm));
      newRow.addElement((Object) Integer.valueOf(arcStn.pwta));
      newRow.addElement((Object) Float.valueOf(arcStn.pttr));
      newRow.addElement((Object) Float.valueOf(arcStn.pwtu));
      newRow.addElement((Object) arcStn.srmk);
      newRow.addElement((Object) Integer.valueOf(arcStn.swta));
      newRow.addElement((Object) Float.valueOf(arcStn.ssec));
      newRow.addElement((Object) Float.valueOf(arcStn.sttr));
      newRow.addElement((Object) Float.valueOf(arcStn.swtu));
      newRow.addElement((Object) Float.valueOf(arcStn.impp));
      newRow.addElement((Object) Float.valueOf(arcStn.imps));
      rows.addElement(newRow);
//    System.out.println("rows.size()=" + rows.size());
      fireTableChanged(null); // Tell the listeners a new table has arrived.
    }

    public static int getColumnType(int column) {
	return colTypes[column];
    }

//////////////////////////////////////////////////////////////////////////
//
//             Implementation of the TableModel Interface
//
//////////////////////////////////////////////////////////////////////////

    public int findColumn(String name) {
	for (int i = 0; i < colNames.length; i++) {
	    if (colNames[i].equals(name)) return i;
	}
	return -1;
    }

    public String getColumnName(int column) {
        if (colNames[column] != null) {
            return colNames[column];
        } else {
            return "";
        }
    }

    public Class getColumnClass(int column) {
	int type = colTypes[column];

        switch(type) {
        case Types.CHAR:
        case Types.VARCHAR:
        case Types.LONGVARCHAR:
//	    System.out.println("String.class");
            return String.class;

        case Types.BIT:
	    System.out.println("Boolean.class");
            return Boolean.class;

        case Types.TINYINT:
        case Types.SMALLINT:
        case Types.INTEGER:
//	    System.out.println("Integer.class");
            return Integer.class;

        case Types.BIGINT:
	    System.out.println("Long.class");
            return Long.class;
	
	case Types.DECIMAL:
	case Types.NUMERIC:
	    System.out.println("BigDecimal.class");
// 	    return BigDecimal.class;
// 	    return Number.class;

//      case Types.REAL:
	case Types.FLOAT:
            return Float.class;
        case Types.DOUBLE:
//	    System.out.println("Double.class");
            return Double.class;

//	case Types.TIME:
//	case Types.TIMESTAMP:
//	    return java.sql.Timestamp.class;
	case Types.DATE:
	    System.out.println("Date.class");
            return java.sql.Date.class;

        default:
//	    System.out.println("other(default).class");
            return Object.class;


         }
    }

    public boolean isCellEditable(int row, int column) {
	boolean retVal = true;
	switch (column) {
	  case 0:
	  case 1:
	  case 2:
	    retVal = false;
	    break;
	}
	return retVal;
    }

    public int getColumnCount() {
        return colNames.length;
    }

    // Data cell methods

    public int getRowCount() {
        return rows.size();
    }

    public Object getValueAt(int aRow, int aColumn) {
        Vector row = (Vector)rows.elementAt(aRow);
        return row.elementAt(aColumn);
    }

    public String dbRepresentation(int column, Object value) {
	int type = colTypes[column];
//	System.out.println("type:" + type);
	if (value == null) return "null" ;
        switch(type) {
        case Types.INTEGER:
        case Types.DOUBLE:
        case Types.FLOAT:
	case Types.REAL:
	case Types.NUMERIC:
	case Types.DECIMAL:
            return value.toString();
        case Types.BIT:
            return ((Boolean)value).booleanValue() ? "1" : "0";
        case Types.DATE:
            return value.toString();
	case Types.TIME:
           return value.toString();
	case Types.TIMESTAMP:
           return value.toString();
        case Types.CHAR:
        case Types.VARCHAR:
        case Types.LONGVARCHAR:
            return "'" + ((String)value).toString() + "'";
        case Types.OTHER:
            return value.toString() ;
        default:
            return "'" + value.toString() + "'";
        }
    }

    public void setValueAt(Object value, int row, int column) {
        Vector dataRow = (Vector)rows.elementAt(row);
        dataRow.setElementAt(value, column);
    }
    
    public void addRow() {
    // must have correspondence between column names and values expected
    // for single table loop through tablemodel columns and values to
    // create new single row table to get input values
    // getValues objects from table and put into a "Table row class object[]"
	Object [] values = new Object[getColumnCount()];
    	Vector newRow = makeNewRow(values);
	rows.addElement(newRow);
	int rowid = rows.size() - 1;
	fireTableRowsInserted(rowid,rowid);
	return;
    }
    
    public Vector makeNullRow() {
    	Vector nullRow = new Vector();
	for (int i = 1; i <= getColumnCount(); i++) {
	    try {
		nullRow.addElement(getColumnClass(i).newInstance());
	    }
	    catch (InstantiationException ex) {
		return null;
	    }
	    catch (IllegalAccessException ex) {
		return null;
	    }
	}
	return nullRow;
    }    

    public Vector makeNewRow(Object[] values) {
	Vector newRow = new Vector();
	if (values.length != getColumnCount()) return null;
	for (int i = 1; i <= getColumnCount(); i++) {
	    if (values[i].getClass().isInstance(getColumnClass(i))) {
		newRow.addElement(values[i]);
	    }
	    else return null;
	}
	return newRow;
    }
    
// add methods for table row/db deletion here ...
    public void delRow(JTable tbl, int rowid) {
  	if (rowid > -1 && rowid < rows.size()) {
	    rows.removeElementAt(rowid);
	    fireTableRowsDeleted(rowid,rowid);
	}
	tbl.clearSelection();
    }
    
    public void delRow(JTable tbl) {
	int rowid = tbl.getSelectedRow();
	if (rowid > -1) {
	    rows.removeElementAt(rowid);
	    fireTableRowsDeleted(rowid,rowid);
	}
	tbl.clearSelection();
    }
}
