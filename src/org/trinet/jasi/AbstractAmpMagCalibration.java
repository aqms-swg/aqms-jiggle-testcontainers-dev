package org.trinet.jasi;
import org.trinet.jasi.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

/**
* Abstract type for a generic amplitude calibration for a station channel.
*/
public abstract class AbstractAmpMagCalibration extends AbstractChannelCalibration implements MagnitudeCalibrationIF {

    // below 1000mv vs. 800mv => 655L cnts see TIMIT$DEV$CHAR; 360L old cusp value from MCA.for
    public static double   MAX_AMP_E_DEFAULT  = 2048.;
    public static double   MAX_AMP_H_DEFAULT  = 8388608.;

    public static double   CLIP_AMP_E_DEFAULT = 1250.; // VCO?
    public static double   CLIP_AMP_H_DEFAULT = 8388608.;

    public static double DEFAULT_CLIP_FRAC = 1.0;

    protected double summaryWt = 1.0;   // assign full weight by default -aww 03/17/2006

    // Could use UnitsAmp or UnitsDataDouble type for max and clip amps
    protected DataDouble clipAmp = new DataDouble();
    protected DataDouble maxAmp = new DataDouble();
    protected int clipAmpUnits = CalibrUnits.COUNTS;
    protected int maxAmpUnits = CalibrUnits.COUNTS;

    // Could use UnitsDataDouble type for gainCorr 
    protected DataDouble gainCorr = new DataDouble(); // channel gain scaling
    protected int gainCorrUnits = CalibrUnits.MAGNITUDE; // in lieu of log Units.MM, Units.COUNTS

    protected AbstractAmpMagCalibration() {
        this(null, null);
    }

    protected AbstractAmpMagCalibration(ChannelIdIF id) {
        this(id, null);
    }
    protected AbstractAmpMagCalibration(ChannelIdIF id, DateRange dateRange) {
        super(id, dateRange);
    }
    protected AbstractAmpMagCalibration(ChannelIdIF id, DateRange dateRange, double value, String corrType) {
        this(id, dateRange, value, corrType, null, null);
    }
    protected AbstractAmpMagCalibration(ChannelIdIF id, DateRange dateRange, double value, String corrType,
                    String corrFlag) {
        this(id, dateRange, value, corrType, corrFlag, null);
    }
    protected AbstractAmpMagCalibration(ChannelIdIF id, DateRange dateRange, double value, String corrType,
                    String corrFlag, String authority) {
        super(id, dateRange, value, corrType, corrFlag, authority);
    }

    // added summaryWt attribute to scale channel contribution to median summary magnitude -03/17/2006 aww
    public void setSummaryWt(double wt) {
        summaryWt = wt;
    }
    public double getSummaryWt() {
        return summaryWt;
    }
    public Double getSummaryWt(MagnitudeAssocJasiReadingIF jr) {
      return ( isValidFor(jr, jr.getLookUpDate()) ) ?  Double.valueOf(summaryWt) : null;
    }

    public Double getClipAmp(MagnitudeAssocJasiReadingIF jr) {
      return ( isValidFor(jr, jr.getLookUpDate()) ) ?  Double.valueOf(clipAmp.doubleValue()) : null;
    }
    public double getClipAmpValue() {
        //return (clipAmp.isNull()) ? getDefaultClipAmp(): clipAmp.doubleValue();
        return (clipAmp.isNull()) ? 0. : clipAmp.doubleValue(); // assume no default -aww 2009/03/10
    }
    public double getDefaultClipAmp() {
        String chan = channelId.getSeedchan();
        if (chan.length() > 0 && chan.substring(0,1).equals("E")) return CLIP_AMP_E_DEFAULT;
        else return CLIP_AMP_H_DEFAULT;
    }
    public void setClipAmp(double clipAmp) {
        this.clipAmp.setValue(clipAmp);
    }

    public double getMaxAmpValue() {
        //return (maxAmp.isNull()) ? getDefaultMaxAmp() : maxAmp.doubleValue();
        return (maxAmp.isNull()) ? 0. : maxAmp.doubleValue(); // assume no default -aww 2009/03/10
    }
    public double getDefaultMaxAmp() {
        String chan = channelId.getSeedchan();
        if (chan.length() > 0 && chan.substring(0,1).equals("E")) return MAX_AMP_E_DEFAULT;
        else return MAX_AMP_H_DEFAULT;
    }
    public void setMaxAmp(double maxAmp) {
        this.maxAmp.setValue(maxAmp);
    }


    public void setMaxAmpUnits(int units) {
        maxAmpUnits = units;
    }
    public int getMaxAmpUnits() {
        return maxAmpUnits;
    }

    public void setClipAmpUnits(int units) {
        clipAmpUnits = units;
    }
    public int getClipAmpUnits() {
        return clipAmpUnits;
    }

    public boolean hasGainCorr(MagnitudeAssocJasiReadingIF jr) {
      return ( (jr instanceof Amplitude) && ! gainCorr.isNull() );
    }
    public Double getGainCorr(MagnitudeAssocJasiReadingIF jr) {
      return (hasGainCorr(jr)) ?  Double.valueOf(gainCorr.doubleValue()) : null;
    }
    public double getDefaultGainCorr() {
        return 0.;
    }
    public void setGainCorr(double gc) {
        gainCorr.setValue(gc);
    }
    public void setGainCorrUnits(int units) {
        gainCorrUnits = units;
    }
    public int getGainCorrUnits() {
        return gainCorrUnits;
    }
// override super methods:
/**
* Assigns the data member values of the input instance to this instance
* Returns false if input is null or input is cannot be cast as instance of this class.
*/
    public boolean copy(ChannelDataIF calibr) {
        return ( getClass().isInstance(calibr) ) ?
            copy((AbstractAmpMagCalibration) calibr) : false;
    }

    public boolean copy(AbstractAmpMagCalibration calibr) {
        if ( ! super.copy(calibr) ) return false;
        this.maxAmp.setValue(calibr.maxAmp);
        this.clipAmp.setValue(calibr.clipAmp);
        this.gainCorr.setValue(calibr.gainCorr);
        this.summaryWt = calibr.summaryWt;  // aww 03/17/2006
        return true;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(132);
        sb.append(super.toString());
        sb.append(" ").append(clipAmp.toStringSQL());
        sb.append(" ").append(maxAmp.toStringSQL());
        sb.append(" ").append(gainCorr.toStringSQL());
        sb.append(" ").append(summaryWt);  // aww 03/17/2006
        return sb.toString();
    }

    public boolean equals(Object object) {
        if ( ! super.equals(object) ) return false;
        AbstractAmpMagCalibration calibr = (AbstractAmpMagCalibration) object;
        return ( clipAmp.equals(calibr.clipAmp) &&
                 maxAmp.equals(calibr.maxAmp) &&
                 gainCorr.equals(calibr.gainCorr)
               );
    }

    public Object clone() {
        AbstractAmpMagCalibration calibr = (AbstractAmpMagCalibration) super.clone();
        calibr.maxAmp = (DataDouble) this.maxAmp.clone();
        calibr.clipAmp = (DataDouble) this.clipAmp.clone();
        calibr.gainCorr = (DataDouble) this.gainCorr.clone();
        // note if summaryWt is scalar it propragates
        return calibr;
    }

    public boolean hasMagCorr(MagnitudeAssocJasiReadingIF jr) {
      return ( (jr instanceof Amplitude) &&
               ! corr.isNull() &&
               isValidFor(jr, jr.getLookUpDate())
             );
    }
    public Double getMagCorr(MagnitudeAssocJasiReadingIF jr) {
      return (hasMagCorr(jr)) ?  Double.valueOf(corr.doubleValue()) : null;
    }

    public String toNeatString() {
      StringBuffer sb = new StringBuffer(132);
      sb.append(super.toNeatString());
      Format df1 = new Format("%10.2f");
      sb.append(df1.form(clipAmp.doubleValue())).append(" ");
      sb.append(df1.form(maxAmp.doubleValue())).append(" ");
      sb.append(df1.form(gainCorr.doubleValue())).append(" ");
      sb.append(df1.form(summaryWt)).append(" "); // aww 03/17/2006 
      return sb.toString();
    }

    public String getNeatHeader() {
       return super.getNeatHeader() + "  ClipAmp   MaxAmp  GainCorr   SummaryWt";
    }

    public void printNeatStringHeader() {
        System.out.println(getNeatHeader());
    }

} // end of AbstractAmpMagCalibration
