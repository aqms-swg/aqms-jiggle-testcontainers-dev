package org.trinet.jasi;

import org.trinet.jdbc.table.*;
import org.trinet.util.*;

public abstract class AbstractChannelCalibration extends AbstractChannelCorrection {

    protected AbstractChannelCalibration() {
        this(null, null);
    }

    protected AbstractChannelCalibration(ChannelIdIF id) {
        this(id, null);
    }
    protected AbstractChannelCalibration(ChannelIdIF id, DateRange dateRange) {
        super(id, dateRange);
    }
    protected AbstractChannelCalibration(ChannelIdIF id, DateRange dateRange, double value, String corrType) {
        this(id, dateRange, value, corrType, null, null);
    }
    protected AbstractChannelCalibration(ChannelIdIF id, DateRange dateRange, double value, String corrType,
                    String corrFlag) {
        this(id, dateRange, value, corrType, corrFlag, null);
    }
    protected AbstractChannelCalibration(ChannelIdIF id, DateRange dateRange, double value, String corrType,
                    String corrFlag, String authority ) {
        super(id, dateRange, value, corrType, corrFlag, authority);
    }

    abstract public AbstractChannelCalibration createDefaultCalibration(ChannelIdIF chan) ;
}
