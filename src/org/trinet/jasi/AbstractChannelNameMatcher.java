package org.trinet.jasi;
import org.trinet.util.*;
//import java.util.regex.*;

abstract public class AbstractChannelNameMatcher extends DbStringMatcher {

    protected AbstractChannelNameMatcher() { }

    protected AbstractChannelNameMatcher(String [] regex) {
      this(regex, 0);
    }
    protected AbstractChannelNameMatcher(String [] regex, int flags) {
      super(regex, flags);
    }
    public boolean matches(Channelable chl) {
      return matches((ChannelIdIF) chl.getChannelObj()); 
    }
    abstract public boolean matches(ChannelIdIF cid) ;
}
