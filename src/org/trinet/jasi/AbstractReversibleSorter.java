package org.trinet.jasi;
import java.util.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;

public abstract class AbstractReversibleSorter implements Comparator {

    protected boolean ascending = true;

    private static final SeedChanSorter seedSorter = new SeedChanSorter();
    private static final TimeSorter timeSorter = new TimeSorter();
    
    public AbstractReversibleSorter() {}

    public boolean isAscending()  { return ascending; }
    public void setAscending()    { ascending = true; }
    public boolean isDescending() { return ! ascending; }
    public void setDescending()   { ascending = false; }

    abstract public int compare(Object o1, Object o2) ;

    public static final CompositeComparator createCompositeComparator(Comparator major, Comparator minor) {
      return new CompositeComparator(major, minor);
    }

    public static final CompositeComparator createSiteTimeSorter(boolean ascend) {
      return createCompositeComparator(createSiteSorter(ascend), timeSorter);
    }

    public static final CompositeComparator createSeedChannelSorter(boolean ascend) {
      return createCompositeComparator(createSiteSorter(ascend), seedSorter);
    }

    public static final SiteSorter createSiteSorter(boolean ascend) {
      return new SiteSorter(ascend);
    }

    public static final ChannelSorter createChannelSorter(boolean ascend) {
      return new ChannelSorter(ascend);
    }

    public static final TimeSorter createTimeSorter(boolean ascend) {
      return new TimeSorter(ascend);
    }

    public static final DistanceSorter createDistanceSorter(boolean ascend) {
      return new DistanceSorter(ascend);
    }

    private static final class SiteSorter extends AbstractReversibleSorter {
      public SiteSorter() {}
      public SiteSorter(boolean ascend) {
         ascending = ascend;
      }
      public int compare(Object o1, Object o2) {
        if (o1 == o2) return 0;
        ChannelIdIF cn1 = ((Channelable)o1).getChannelObj().getChannelId();
        ChannelIdIF cn2 = ((Channelable)o2).getChannelObj().getChannelId();
        String site1 = cn1.getNet()+" "+cn1.getSta();
        int retVal = site1.compareTo(cn2.getNet()+" "+cn2.getSta());
        return (ascending) ? retVal : -retVal;
      }
    }
    private static final class SeedChanSorter extends AbstractReversibleSorter {
      private static final ComponentSorter compSorter = new ComponentSorter();
      public SeedChanSorter() {}
      public SeedChanSorter(boolean ascend) {
         ascending = ascend;
      }
      public int compare(Object o1, Object o2) {
        if (o1 == o2) return 0;
        String sc1 = ((Channelable)o1).getChannelObj().getSeedchan();
        String sc2 = ((Channelable)o2).getChannelObj().getSeedchan();
        return compSorter.compare(sc1, sc2);
      }
    }
    private static final class ChannelSorter extends AbstractReversibleSorter {
      public ChannelSorter() {}
      public ChannelSorter(boolean ascend) {
         ascending = ascend;
      }
      public int compare(Object o1, Object o2) {
        if (o1 == o2) return 0;
        int retVal = ((Channelable)o1).getChannelObj().toDelimitedSeedNameString().compareTo(
            ((Channelable) o2).getChannelObj().toDelimitedSeedNameString());
        return (ascending) ? retVal : -retVal;
      }
    }
    private static final class TimeSorter extends AbstractReversibleSorter {
      public TimeSorter() {}
      public TimeSorter(boolean ascend) {
         ascending = ascend;
      }
      public int compare(Object o1, Object o2) {
        if (o1 == o2) return 0;
        double diff = ((TimeDataIF)o1).getTime() - ((TimeDataIF) o2).getTime();

        if (Math.abs(diff) < .00001) diff = 0.; // added this test to handle weird roundoff? -aww 2010/10/20

        if (diff == 0.) return 0;
        int retVal = (diff < 0.0) ? -1 : 1;
        return (ascending) ? retVal : -retVal;
      }
    }
    private static final class DistanceSorter extends AbstractReversibleSorter{
      public DistanceSorter() {}
      public DistanceSorter(boolean ascend) {
        ascending = ascend;
      }
      public int compare(Object o1, Object o2) {
        // TODO: redefine IF's and class member implementations such that:
        //   get/setHorizontalDistance()=> get/setDistance() (HorizontalDistanceBearingIF => DistanceBearingIF)
        //   get/setDistance() => get/setSlantDistance() (SlantDistanceBearingIF subclasses HorizontalDistanceBearingIF)
        // Changed from "distance" to "horizontalDistance" for comparison below -aww 02/14/2005
        //double diff = ((DistanceBearingIF)o1).getDistance() - ((DistanceBearingIF) o2).getDistance(); // 02/14/2005 -aww
        if (o1 == o2) return 0;
        double diff =
            ((HorizontalDistanceBearingIF) o1).getHorizontalDistance() -
            ((HorizontalDistanceBearingIF) o2).getHorizontalDistance();

        if (Math.abs(diff) < .00001) diff = 0.; // added this test to handle weird roundoff? -aww 2010/10/20

        if (diff == 0.) return 0;
        int retVal = (diff < 0.0) ? -1 : 1;
        return (ascending) ? retVal : -retVal;
      }
    }
}
