package org.trinet.jasi;

import java.io.*;
import java.sql.*;
import java.util.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datasources.SQLDataSourceIF;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;

/**
 * Represents the waveform time series for one channel for a time window (TimeSpan).
 * Because a time window may contain gaps or no data, the TimeSeries may contain
 * from zero to 'n' WFSegments. Each WFSegment is a CONTIGUOUS time series.<p>
 *
 * Fires ChangeEvents when the time-series is loaded so that time-series can be
 * loaded asynchronously (in background thread) then notify a graphics object when
 * loading is complete.<p>
 *
 * Note that a waveform may be "empty", that is contain no time series. This may be
 * true before the loader thread has retreived the time-series. Check this
 * condition using the boolean method hasTimeSeries(). <p>
 *
 * <b>Note on synchronization: </b> Waveforms can be loaded in a background thread.
 * Therefore, care must be taken to make concrete methods that implement and extend
 * this class THREAD SAFE. This is best done by synchronizing on the specific
 * waveform instance ("this").
 * For example:
 <tt>
public class WaveformConcrete extends AbstractWaveform {

     public void someMethod() {
       synchronize (this) {
         // modify the waveform here
       }
     }
}

 <\tt>

 * Extensions of this class are also responsible for knowing how to read in
 * timeseries from various data sources.
 * @see: loadTimeSeries()
 * @see: loadTimeSeries(double startTime, double endTime)
 * <p>
 *  The wave source is set using
 *  @see: setWaveSource(Object source)
 */

public abstract class AbstractWaveform extends JasiTimeSeries implements Waveform
{

    public static final int SINGLE = 0; // old SCEDC way
    public static final int GROUP  = 1; // new NCEDC way

    public static int defaultType = GROUP; 

    public static final int PEAK2PEAK = 0;
    public static final int ZERO2PEAK = 1;

    public static double smpWindowMultiplier = 3.;

    /** Time-series encoding format as defined in SEED v2.3, see SEED
     * Ref. Man. pg. 106
     *
     * @See: org.trinet.jasi.SeedEncodingFormat() */
    protected DataLong encoding = new DataLong();
    //protected DataLong wordOrder = new DataLong(); // if wordOrder added to waveform query

    /** "C" = continuous, "T" = triggered. */
    //protected DataString  dataMode     = new DataString();
    protected DataString waveType = new DataString(); // ie. dataMode
    protected DataString waveArchive = new DataString();
    protected DataString waveStatus = new DataString();

    /** If the waveform is associated with a particular event in the underlying
    data source, this is the unique ID of that event. */
    protected DataString savingEvent = new DataString(); // event ID for which data was saved

// private, implimentation specific data members

    /** The maximum amplitude sample. */
    protected Sample maxSample;
    /** The minimum amplitude sample. */
    protected Sample minSample;

    protected boolean clipped = false;
    protected boolean isSelected = false;

    /** Units of the amplitude dimension.
    * @See: Units */
    protected int ampUnits = Units.UNKNOWN;

    /** The value of the bias (DC offset).*/
    protected float biasVal = 0.0f;

    /* Enumerate time-series load source options */
    public static final int LoadFromDataSource = 0;
    public static final int LoadFromWaveServer = 1;
    public static final int LoadFromFile = 2;

    /** Default source of time-series data. These are static so ALL waveforms must
     * be coming from the same data source. */
    protected static int waveDataSourceType = LoadFromDataSource;

    /** Name of filter applied to this waveform is any. */
    protected String filterName = "";

    /* This will be either a DataSource (default) or a WaveClient */
    // The following blows up if default data source is not available
    //    static Object waveDataSource  = new DataSource();
    protected static Object waveDataSource;

    /** The travel-time model used to calculate the energy window. */
    //TravelTime ttModel = new TravelTime();
    //protected TravelTime ttModel = TravelTime.getInstance();

    /** Directory path to the waveform file. */
    protected String path;
    /** Filename of the waveform file. */
    protected String filename;
    /** Byte offset to data in file */
    protected int fileOffset;
    /** Number of bytes of data in the file for this waveform. */
    protected int byteCount;

    public static boolean cache2File = false; // set true to enable segment packet list caching

    public static String cache2FileDir = null; // e.g. set to C:\\Temp\\wfcache\\";

    private boolean wasCached = false; // written to disk, else subsequently read and scan for amps etc.

    protected AbstractWaveform() { }

// ////////////////////////////////////////////////////////////////////////////

    // Screens all events when evid=0l, a particular channel when chnl is not null, a particular station when input sta not null.
    // If input evid is non-zero, the filename must contain evid, otherwise all evid are valid.
    // If input chnl is not null, the filename must contain SNCL match, otherwise all SNCL are valid.
    // If input sta is not null, the filename must contain "sta" string; however, to prevent
    // case of net, seedchan, location matching input sta, set both sta and chnl non-null,
    // then the Channel's getSta() must match the input sta too. 
    // (e.g. setting evid=0l chnl=null and sta=null clears all files from cache)
    public static int clearCacheDir(final long evid, final Channelable chnl, final String sta) {

      int count = 0;

      if (cache2FileDir != null) {
         File file = new File(cache2FileDir);  
         if (file.exists()) {
             File [] fileArray = file.listFiles(
                  new FileFilter() {
                      public boolean accept(File file) {

                          String filename = file.getAbsolutePath();

                          if (! filename.endsWith(".seg")) return false;

                          // if input evid is non-zero filnename must contain evid
                          boolean tf = (evid == 0) ? true : (filename.indexOf(String.valueOf(evid)) >= 0);

                          // if channel not null filename must contain NSCL match 
                          String name = (chnl == null) ? null : chnl.toDelimitedSeedNameString("_");
                          if (name != null) tf &= (filename.indexOf(name) >= 0);

                          // if sta not null filname sta must match, when input chnl=null and input evid=0l
                          if (sta != null) {
                              // all matching filename must contain "sta" string
                              tf &= (filename.indexOf(sta) >= 0);
                              // if chnl not null its station name must match input "sta" string
                              if (chnl != null) tf &= sta.equalsIgnoreCase(chnl.getChannelObj().getSta());
                          }

                          return tf;
                      }
                  }
             );
             try {
               //System.out.printf("AbstractWaveform deleting %d files from wf cache: %s%n", fileArray.length, cache2FileDir);
               for (int ii = 0; ii < fileArray.length; ii++) { 
                 if (fileArray[ii].delete()) count++;
               }
               System.out.println("AbstractWaveform deleted " + count + " files from wf cache: " + cache2FileDir);
             }
             catch (Exception ex) {
                 ex.printStackTrace();
             }
         }
      }

      return count;
    }

    // Does a disk file containing serialized contents of WFSegment list exist
    public boolean hasCacheFile() {
      if (cache2FileDir == null) return false;
      //File file = new File(cache2FileDir + GenericPropertyList.FILE_SEP + savingEvent.toString() + "-" + chan.toDelimitedSeedNameString("_") + ".seg");
      //return file.exists();
      return new File(cache2FileDir + GenericPropertyList.FILE_SEP + savingEvent.toString() + "-" + chan.toDelimitedSeedNameString("_") + ".seg").exists();
    }

    public boolean readFromCache() {

      if (!cache2File) return false;

      if (cache2FileDir == null) return false;

      String filename = cache2FileDir + GenericPropertyList.FILE_SEP + savingEvent.toString() + "-" + chan.toDelimitedSeedNameString("_") + ".seg";
      File file = new File(filename);
      // Is there a disk file containing serialized contents of WFSegment list ?
      if (! file.exists()) { // output directory ?
          if (debug) System.out.println("DEBUG>>> AbstractWaveform readFromCache failed, NO .seg file : " + filename);
          return false;
      }

      segList = (ArrayList) readFromCache(file);
      boolean status = (segList != null);
      if (!status) segList = new ArrayList();

      /*
      if (status) {
        filename  = cache2FileDir + GenericPropertyList.FILE_SEP + chan.toDelimitedSeedNameString() + ".pkt";
        file = new File(filename);
        if (! file.exists()) {
          //System.out.println("AbstractWaveform readFromCache NO .pkt file : " + filename);
          return false;
        }
        packetSpans = (ArrayList) readFromCache(file);
        status &= (packetSpans != null);
      }
      */

      //System.out.println("AbstractWaveform readFromCache status: " + status);
      return status;

    }

    private static Object readFromCache(File file) {
      Object obj = null;
      ObjectInputStream stream = null;
      if (debug) System.out.println("DEBUG>>> AbstractWaveform readFromCache file: " + file);
      try {
        stream = new ObjectInputStream(new FileInputStream(file));
        obj = stream.readObject();
      } catch (FileNotFoundException ex) {
        System.err.println("AbstractWaveform readFromCache unable to find cache file named: " + file);
      } catch (InvalidClassException ex) {
        System.err.println("AbstractWaveform readFromCache version of cached file object predates its class byte code recompilation time.");
      } catch (EOFException ex) {
        System.err.println("AbstractWaveform readFromCache EOF on input.");
      } catch (Exception ex) {
        ex.printStackTrace();
      }
      finally {
        try {
          if (stream != null) stream.close();
        }
        catch (Exception ex2) { }
      }
      return obj;
    }

    public boolean writeToCache() {
      // Now serialize contents of packet Timespan and WFSegment lists:
      if (segList == null || segList.isEmpty()) return false;

      if (cache2FileDir == null) return false;

      String filename = cache2FileDir + GenericPropertyList.FILE_SEP + savingEvent.toString() + "-" +  chan.toDelimitedSeedNameString("_") + ".seg";
      File file = new File(filename);
      File dir = file.getParentFile();
      if (dir == null || ! dir.exists()) {
          System.err.println("AbstractWaveform writeToCache NO directory for .seg file : " + filename);
          return false;
      }

      // Don't overwrite again, use existing cache file
      boolean status = (file.exists()) ? true : writeToCache(file, segList);

      /*
      if (status) {
          filename = cache2FileDir + GenericPropertyList.FILE_SEP + chan.toDelimitedSeedNameString() + ".pkt";
          file = new File(filename);
          dir = file.getParentFile();
          if (dir == null || ! dir.exists()) {
              System.out.println("AbstractWaveform writeToCache NO directory for .pkt file : " + filename);
              return false;
          }
          if (packetSpans == null || packetSpans.isEmpty()) return false;
          status &= (file.exists()) ? true : writeToCache(file, packetSpans);
      }
      */

      wasCached = status;
      return status;
   }

   private static boolean writeToCache(File file, Object obj) {
      boolean status = true;
      ObjectOutputStream stream = null;
      try {
        stream = new ObjectOutputStream(new FileOutputStream(file));
        stream.writeObject(obj);
      } catch (IOException ex) {
        ex.printStackTrace();
        status = false ;
      }
      finally {
        if (stream != null) {
          try {
            stream.flush();
            stream.close();
          } catch (Exception ex2) { }
        }
      }
      return status;
    }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. Creates a Solution of the DEFAULT type.
     * @See: JasiObject
     */
    private static Waveform createWaveformOfType(int type) {
        if (type == GROUP) return WaveletGroup.create();
        else if (type == SINGLE) return Wavelet.create();
        return null;
    }

    /** Create instance of the current default waveform type GROUP or SINGLE. */
    public static Waveform createDefaultWaveform() {
          //System.out.println("Default waveform type = " + defaultType);
          return createWaveformOfType(defaultType);
    }

    public abstract Waveform createWaveform();

    public Waveform copy() {
        return copyWf(this);
    }

    /** Return a "deep" copy of the passed waveform. */
    public static Waveform copyWf(Waveform wf) {

           if (wf == null) return null;

           Waveform newWf = wf.createWaveform();

           newWf.copyHeader(wf);

           newWf.copyTimeSeries(wf);

           return newWf;

    } // end of copy

    /** Do "deep" copy of the header, not the data, portion of the Waveform. */
    public void copyHeader(Waveform wf) {
       synchronized (wf) {           // make sure another thread doesn't change it under us
           //setChannelObj(Channel.create().setChannelObj(wf.getChannelObj()));
           // don't believe cloning needed here, depends on implementation of setChannelObj -aww
           //setChannelObj((Channel)wf.getChannelObj().clone()); // 04/07/2005 -aww
           setChannelObj((Channel)wf.getChannelObj());
           copySegmentPersistent(wf);
           copyTimeSpan(wf);

           // The default TravelTime instance is created on Waveform init, so 
           // if input wf changed from the default tt model it is not copied 
           // ttModel = wf.getTravelTimeModel(); // TravelTime is not cloneable!

            // below pertains only to timeseries data read from file system
            path = wf.getFilePath();
            filename = wf.getFilename();
            fileOffset = wf.getFileOffset();
            byteCount = wf.getFileByteCount();

           // note filterName, maxSample; minSample; biasVal; are set by copyTimeSeries

       } // end of synch

    }  // end of copyHeader

    protected void copySegmentPersistent(Waveform wf) {
           format = new DataLong(wf.getFormat());
           //wordOrder = new DataLong(wf.getWordOrder()); // if wordOrder added to waveform query
           encoding = new DataLong(wf.getEncoding());
           //dataMode = new DataString(wf.getDataMode());
           waveType = new DataString(wf.getWaveType());
           waveArchive = new DataString(wf.getWaveArchive());
           waveStatus = new DataString(wf.getWaveStatus());
           savingEvent = new DataString(wf.getSavingEvent());
           waveDataSourceType = wf.getWaveSourceType();
           waveDataSource = wf.getWaveSource(); // alias reference 
           ampUnits = wf.getAmpUnits();
           // Added logic below to finesse Waveform row having samplerate undefined -aww 12/14/2006
           double rate = wf.getSampleRate();
           if (rate <= 0. && segList.size() > 0) {
               rate = 1./((WFSegment)segList.get(0)).getSampleInterval();
           }
           if (rate > 0) setSampleRate(rate); // NOTE: need to set used when alternate filtered waveform is a copy (segList may empty, wf not loaded)

    }

    private void copyTimeSpan(Waveform wf) {
        setStart(wf.getStart().doubleValue());
        setEnd(wf.getEnd().doubleValue());
    }

    //
    public void trimSpanToWFSegmentSpan() {
        int cnt = segList.size();
        if (cnt == 0) return;
        WFSegment wfseg = (WFSegment) segList.get(0);
        setStart(wfseg.getEpochStart());
        wfseg = (WFSegment) segList.get(cnt-1);
        setEnd(wfseg.getEpochEnd());
    }
    //


    /** Do "deep" copy of the WFSegments in the Waveform. */
    public void copyTimeSeries(Waveform wf) {

        if (! wf.hasTimeSeries()) return;

        synchronized (wf) {
            WFSegment seg[] = wf.getArray();

            biasVal = wf.getBias();

            if (wf.getMaxSample() != null)
                maxSample = wf.getMaxSample().copy();
            if (wf.getMinSample() != null)
                minSample = wf.getMinSample().copy();

            for (int i=0; i < seg.length; i++) {
                WFSegment wfs = new WFSegment();
                wfs.copy(seg[i]);
                segList.add(wfs);
            }
        } // end of synch

        packetSpans = wf.getPacketSpanList();

        filterName  = wf.getFilterName(); // describes filter if any - added 10/26/2004 aww  
        clipped = wf.isClipped();

    }


    /** Set the source of the time-series data */
    public boolean setWaveSource(Object source) {
        return setWaveDataSource(source);
    }

    /** Set the source of the time-series data */
    public static boolean setWaveDataSource(Object source) {

      Object src = source;
      // Static wrapper of DataSource around concrete instance
      // so lookup real source that implements interface
      // e.g. in lieu of invoking DataSource.someMethod(),
      // could invoke DataSource.getSource().someMethod().
      if (src instanceof DataSource) {
         src = ((DataSource) source).getSource(); // generic type
         // if not defined type drops thru to error?
      }

      if (src instanceof SQLDataSourceIF) {
        if (debug) System.out.println("AbstractWaveform WaveDataSource set to db.");
        waveDataSourceType = LoadFromDataSource;
        waveDataSource = src;
        return true;
      } else if (src instanceof org.trinet.waveserver.rt.WaveServerSource) {
        if (debug) System.out.println("AbstractWaveform WaveDataSource set to waveserver "+source.getClass().getName());
        waveDataSource = src;
        waveDataSourceType = LoadFromWaveServer;
        return true;
      }
      System.out.println("WARNING AbstractWaveform: unknown waveform data source: " +
      ( (src == null) ? "null" : src.getClass().getName() ));
      return false;
    }

    public Object getWaveSource() {
        return getWaveDataSource();
    }
    public static Object getWaveDataSource() {
        return waveDataSource;
    }

    /** Return a string describing the data source. It its a WaveClient is
     *  returned. If its a dbase a string of the form "dbase@host" is returned.*/
    public String getWaveSourceName() {
        return getWaveDataSourceName();
    }

    public static String getWaveDataSourceName() {
        int type = getWaveDataSourceType(); 
        if (type  == LoadFromDataSource) {
          SQLDataSourceIF src = (SQLDataSourceIF) waveDataSource;
          if (src == null) return "null";
          String str = src.getDbaseName() + "@" + src.getHostName();
          if (str.equals("@")) return "DS";
          return str;
        }
        else if (type == LoadFromWaveServer) {
          return "WS";
        }
        return "";
    }

    public int getWaveSourceType() {
        return getWaveDataSourceType();
    }

    public static int getWaveDataSourceType() {
        return waveDataSourceType;
    }

/**
 * Get a Waveform from the DataSource associated with input Solution and instance Channelable object.
 */
    public abstract Collection getByChannel(Channelable ch, long evid);

    public Collection getByChannel(Channelable ch, Solution sol) {
        long evid = sol.getId().longValue();
        return (evid > 0l) ? getByChannel(ch, evid) : null;
    }

    //abstract public boolean makeRequest(long evid, String auth, String staAuth, String archiveSrc, double start, double end);

/**
 * Get a Collection of Waveforms from the DataSource associated with input Solution id.
 */
    public abstract Collection getBySolution(long id);

/**
 * Get a Collection of waveforms from the DataSource by solution ID #
 */
    public Collection getBySolution(Solution sol) {
        long evid = sol.getId().longValue();
        return (evid > 0l) ? getBySolution(evid) : null;
    }

/**
 * Get an count of Channels with Waveforms associated with input event id in the DataSource 
 */
    public abstract int getCountBySolution(long evid);

/**
 * Get an count of Channels with Waveforms associated with Solution's id in the DataSource 
 */
    public int getCountBySolution(Solution sol) {
        long evid = sol.getId().longValue();
        return (evid > 0l) ? getCountBySolution(evid) : 0;
    }

    /**
     * Returns 'true' if generic waveform path as returned by the dbase are local
     *  i.e. are on a file system where they can be opened either on the client
     * machine or NFS mounted.
     * Gets generic waveform file
     * root from the database and checks that is can be reached. Needs a
     * DataSource open connection.
     */
    public boolean filesAreLocal() {
        String root = getFileRoot();
        //  System.out.println ("DEBUG AbstractWaveform getFileRoot = "+root);
        if (root == null || root.equals("")) return false;
        // check for existance
        File file = new File(root);
        //  System.out.println ("DEBUG AbstractWaveform file: "+file.toString());
        boolean exists = false;
        try {
            exists = file.isDirectory();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return exists;
    }

    /**
     * Return the generic waveform file root.  Access to this path is tested
     * to determine if files can be read locally.
     */
    public abstract String getFileRoot();

    /**
     * Return the waveform file root for a particular event id.
     * Access to this path is tested
     * to determine if files can be read locally.
     */
    public abstract String getFileRoot(long eventId);
     

/**
 * Load the time series data for this Waveform. This is done as a separate step
 * because it is often more efficient to only load selected waveforms after
 * you have examined or sorted the Waveform objects by their header info.<p>
 *
 * The waveform is collapsed into the minimum number of WFSegments possible. More
 * then one WFSegment would only be necessary if there are time tears. The start-stop
 * times of the original data packets is preserved in the packetSpans array.<p>
 *
 * Fires ChangeEvents when the time-series is loaded so that time-series can be
 * loaded asynchronously (in background thread) then notify a graphics object when
 * loading is complete. <p>
 *
 * This method must synchronize on the waveform object for thread safty. Simply
 * synchronizing the method is not enough.
 */

    public boolean loadTimeSeries() {
        return loadTimeSeries(true);  // try disk cache first
    }

    public boolean loadTimeSeries(boolean readCache) {
       if (readCache && loadTimeSeriesFromCache()) return true; // read disk file serialized segments
       return loadTimeSeries(getStart().doubleValue(), getEnd().doubleValue()); // read from wavesource
    }

    public boolean loadTimeSeriesFromCache() {

        if (!readFromCache()) return false;

        if (!wasCached) {
            // set max, min, bias values
            scanForAmps();
            scanForBias();
            // Added logic below to finesse database Waveform row having samplerate undefined -aww 12/14/2006
            double rate = getSampleRate();
            if (rate <= 0. && segList.size() > 0) {
                rate = 1./((WFSegment)segList.get(0)).getSampleInterval();
                setSampleRate(rate);
            }
            wasCached = true;
        }

        fireStateChanged(); // notify timeseries successfully loaded, WFPanels will null alternateWF ?

        return true;
    }

    public abstract boolean loadTimeSeries(double startTime, double endTime);

    /**
     * Delete the time series for this waveform and allow the GC to reclaim
     * the memory space. This is used to manage memory and prevent overflows.
     * */
    public boolean unloadTimeSeries() {
        synchronized (this) {
          //segList = new ArrayList();
          //packetSpans = null;
          //packetSpans = new ArrayList();
          segList.clear();
          //packetSpans.clear();  // removed, instead preserved, so it doesn't have to be written out to disk serialized object
        }
        fireStateChanged(); // TEST notify timeseries unloaded, then WFPanels will null alternateWF -aww 2009/02/03
        return true;
    }

    public boolean unloadTimeSeriesToCache() {
          if (cache2File && hasTimeSeries() && !isFiltered()) { // don't write-out secondary filtered timerseries -aww 2010/10/19
              //System.out.println("AbstractWaveform.unloadTimeSeriesToCache");
              //boolean status = 
                  writeToCache();
              //System.out.println("unloadTimeSeriesToCache return status: " + status);
          }
          return unloadTimeSeries();
    }

    /** Reduce waveform to the minimum possible number of WFSegments. Preserve
    * WFSegment boundaries in packetSpans array. */
    public void collapse() {
        if (! segList.isEmpty()) {
          // Segments are presumed to be in chronological ondate order
          // We could force a time sort of the segment list here? -aww

          // save original data packet bounds in an array for later use
          savePacketSpans();  // only set the 1st time called
          // collapse multiple segments into minimum possible
          setSegmentList(WFSegment.collapse(segList));
        }
    }

    /** Return true if there are actual samples (time-series) loaded for this
        waveform. */
    public boolean hasTimeSeries() {
        return (segList == null || segList.size() == 0) ? false : true;
    }
 /**
 * Convert this Collection of WFSegments to an Array of WFSegments.
 */
    public WFSegment[] getArray() {
        if (segList.size() == 0) return new WFSegment[0]; // changed from null -aww 2008/08/29

        WFSegment segArray[] = new WFSegment[segList.size()];
        return (WFSegment[]) segList.toArray(segArray);
    }

    /**
     * Return total samples in memory for this Waveform. Note that they may not
     * be contiguous.
     */
     public int samplesInMemory() {
        if (!hasTimeSeries()) { return 0; } // no time series

        int k = 0;
        WFSegment seg[] = WFSegment.getArray(segList);

        for (int i=0; i < seg.length; i++) {
          k += seg[i].ts.length;   // size of timeseries array = # of samples
        }
        return k;
    }

    /** Return the units of the amplitude dimension of this Waveform.
    @See: Units */
    public int getAmpUnits() {
        return ampUnits;
    }
    public String getAmpUnitsTag() {
        String tag = "UNK";
        if (Units.isAcceleration(ampUnits)) tag = "ACC"; 
        else if (Units.isVelocity(ampUnits)) tag = "VEL"; 
        else if (Units.isDisplacement(ampUnits)) tag = "DIS"; 
        else if (Units.isCounts(ampUnits)) tag = "CNT"; 
        return tag;
    }
    /** Set the units of the amplitude dimension of this Waveform.
     * Only works if 'units' is legal.
     * @See: Units */
    public void setAmpUnits(int units) {
        if (Units.isLegal(units)) ampUnits = units;
    }
    /** Set the units of the amplitude dimension of this Waveform.
    @See: Units */
    public void setAmpUnits(String units) {
        ampUnits = Units.getInt(units);
    }

    /** Return the Sample Interval (secs) for this Waveform */
    public double getSampleInterval() {
        if (samplesPerSecond.isNull()) return 0.;
        return 1.0/samplesPerSecond.doubleValue();  //Samp/sec -> sec/Samp
    }

/**
 *  Return the time of where the sample closest to the given time would be.
 *  NOTE: there may be no sample there if there is a time tear or the time is beyond
 *  either end of the data! Times are raw epoch seconds.
 *  If there is no time series the original time is returned.
 */
    public double closestSampleInterval(double dtime) {
        if (!hasTimeSeries()) { return dtime; } // no time series

        double dt0 = timeStart.doubleValue();
        double sps = samplesPerSecond.doubleValue();

        // integral number of samples to from startTime to dtime
        double nsamps = (double) Math.round((dtime - dt0) * sps);

        // offset in sec's to that sample
        return dt0 + (nsamps / sps);

    }

/**
 * Calculate the bias of this waveform by examining all waveform segments.
 * Sets bias = 0.0 if there is no time-series.
 * For efficiency this should only be called the first time a time-series
 * is loaded.
 */
  public void scanForBias() {
    synchronized (this) {
      if (!hasTimeSeries()) {
         biasVal = 0.0f;
         return;
      } // no time series

      double sum = 0.0;
      double totalSamples = 0.0;

      WFSegment seg[] = WFSegment.getArray(segList);

      //Segments aren't equal in length so weight the bias by sample count
      for (int i=0; i < seg.length; i++) {
          sum += seg[i].getBias() * seg[i].ts.length;
          totalSamples += seg[i].ts.length;
      }

      biasVal = (float) (sum / totalSamples);
    } // end of synch
  } // end of bias

  /** Remove the bias from this time series in place. */
  public void demean() {
      if (!hasTimeSeries()) return;
      scanForBias();
      WFSegment seg[] = WFSegment.getArray(segList);
      float val = - (getBias());  // must subtract  "-"
      for (int i=0; i < seg.length; i++) {
          seg[i].addAmpOffset(val);
      }
      //scanForBias(); // why was this removed here? -aww
      biasVal = 0.f; // so I added this -aww 2010/09/24 not sure about consequences in display
      scanForAmps();
  }
  /** Scale time series in place, time series sample amps are multiplied by input value. */
  public void scaleAmp(float value) {
      if (!hasTimeSeries()) return;
      WFSegment seg[] = WFSegment.getArray(segList);
      for (int i=0; i < seg.length; i++) {
          seg[i].scaleAmp(value);
      }
      scanForAmps();
  }

  /** Return this waveform filtered.
   * To reduce edge effects the WFSegments are concatenated before they are filtered.
   * */
  public Waveform filter(FilterIF filter) {
      return (hasTimeSeries()) ? (Waveform) filter.filter(this) : this;
  }

  /** Name of filter <b>applied</b> to this waveform is any. Empty string implies no filter. */
  public String getFilterName() {
    return filterName;
  }

  /** Sets name of filter <b>applied</b> to this waveform is any.
   * If input is null or blank the instance filtered attribute is set <i>false</i>
   * otherwise it is set <i>true</i>.
   * @see #isFiltered() 
   * */
  public void setFilterName(String str) {
    filterName = str;
//    setIsFiltered(NullValueDb.isBlank(str)); // NOTE: if null or blank set filtering false
    // logic was backward - fixed 6/12/06 DDG
    setIsFiltered(!NullValueDb.isBlank(str)); // NOTE: if null or blank set filtering false

  }
  /** Override sets the filter name to an empty String ("") if input is <i>false</i>.
   * @see #setFilterName(String) 
   * */
  public void setIsFiltered(boolean tf) {
    super.setIsFiltered(tf);
    if (! isFiltered()) filterName = "";
  }

/**
* Scan the whole time-series for the recified peak. Returns the Sample of the
* peak which may have a negative value; the bias is NOT removed.
* Returns null if no timeseries.
*/
  public Sample scanForPeak() {
    return scanForPeak(getTimeSpan());
  }
/**
 * Return the Sample with the maximum amplitude in the waveform.
 * "Maximum" value could be the largest excursion downward. Bias is NOT removed.
 * Because of large biases even "positive" peak
 * could be a negative number. Returns 'null' if no time-series.
 */
  public Sample scanForPeak(TimeSpan timeSpan) {
    return scanForPeak(timeSpan.getStart(), timeSpan.getEnd());
  }
/**
 * Return the Sample with the maximum amplitude in the waveform.
 * "Maximum" value could be the largest excursion downward. Bias is NOT removed.
 * Because of large biases even "positive" peak could be a negative number.
 * Returns 'null' if no time-series in TimeSpan
 */
  public Sample scanForPeak(double startTime, double endTime) {

      if (!hasTimeSeries()) return null;    // no time series

      synchronized (this) {
          WFSegment seg[] = WFSegment.getArray(segList);

          Sample peakSample = null;
          Sample tmpSample  = null;

          // scan segs
          // int clipCounter = 0;
          for (int i=0; i < seg.length; i++) {
            if (seg[i].getEnd().doubleValue() < startTime) continue;  // seg before timespan
            if (seg[i].getStart().doubleValue() > endTime) break;     // seg later than timespan
            tmpSample = seg[i].getPeakSampleWithPeriod(startTime, endTime);
            if (Double.isNaN(tmpSample.value)) continue; // Reject undefined values, why are there NaN ? - aww 01/16/2007
            peakSample = Sample.absMax(peakSample, tmpSample);   // reset if bigger or equal to previous peak
            //period = seg[i].getPeriodAt(peakSample.datetime); // replaced by getPeakSampleWithPeriod -aww 2009/05/15
            //if (peakSample == tmpSample)) clipCounter++;
          }
          // Question is, what is "clipping" amp, lookup may have too much overhead 
          //NOTE: Below requires JASI_AMPPARMS_VIEW to be defined in db, else throws SQL exception -aww 12/20/2006
          //double clipAmp = chan.getClipAmpValue(LeapSeconds.trueToDate(peakSample.datetime));
          //Crude kludge to flag a seedchan such as EHZ clipped by # of equals amps like sinisoidal ?
          //if (clipCounter > 4) setClipped(true);
          //System.out.println(getChannelObj().toString()+" DEBUG scan4Peak: "+((peakSample == null) ? "null" : peakSample.toString()));
          //if (peakSample != null) peakSample = new SampleWithPeriod(peakSample, period); // replaced by getPeakSampleWithPeriod -aww 2009/05/15
          return peakSample;

      } // end of synch
  }

  public Sample scanForPeak2Peak() {
    return scanForPeak2Peak(getTimeSpan());
  }
  public Sample scanForPeak2Peak(TimeSpan timeSpan) {
    return scanForPeak2Peak(timeSpan.getStart(), timeSpan.getEnd());
  }
  public Sample scanForPeak2Peak(double startTime, double endTime) {

      if (!hasTimeSeries()) return null;    // no time series

      synchronized (this) {
          WFSegment seg[] = WFSegment.getArray(segList);

          Peak2PeakSample p2pSample = null;
          SampleWithPeriod peakSample  = null;
          SampleWithPeriod tmpSample  = null;
          float period = Float.NaN;

          // scan segs
          // int clipCounter = 0;
          int i = 0;
          int istart = 0;
          int iend = 0;
          for (i=0; i<seg.length; i++) {
            if (seg[i].getEnd().doubleValue() < startTime) continue;  // seg before timespan
            if (istart == 0) istart = i;
            if (seg[i].getStart().doubleValue() > endTime) {
                break;     // seg later than timespan
            }
            iend = i; // last segment processed
            tmpSample = seg[i].getPeakSampleWithPeriod(startTime, endTime);
            if (Double.isNaN(tmpSample.value)) continue; // Reject undefined values, why are there NaN ? - aww 01/16/2007
            peakSample = (SampleWithPeriod) Sample.absMax(peakSample, tmpSample);   // reset if bigger or equal to previous peak
          }

          //System.out.println(getChannelObj().toString() +" z2p: " +  peakSample.toUnitsString(Units.getString(ampUnits)));
          if (peakSample == null || peakSample.isNull()) return peakSample;

          tmpSample = (SampleWithPeriod) peakSample;
          if (Float.isNaN(tmpSample.period) || period <= 0.f) return tmpSample;

          // Now after you have the "absolute peak" with period
          Sample tmpS = null;
          double minVal =   Double.MAX_VALUE;
          double maxVal =  -Double.MAX_VALUE;

          double tmpVal = 0.;
          //double delta = tmpSample.period * 0.9; // too inclusive of other cycles ?
          double delta = tmpSample.period * 0.75; // try smaller window

          p2pSample = new Peak2PeakSample(peakSample);

          double startT = 0.;
          double endT = 0.;
          for (i=istart; i<=iend; i++) {

            // Look delta fractional period around absolute peak for the next, peak, the secondary one for the wavelet cycle 
            //tmpS = seg[i].getMaxSample(p2pSample.datetime-delta, p2pSample.datetime+delta);
            // Restrict search in specified input time span bounds - aww 2011/08/11 as per J.Pechmann UU request
            //startT = Math.min(p2pSample.datetime-delta, startTime); // bugfix below the start,end times min,max were reversed
            //endT = Math.max(p2pSample.datetime+delta, endTime);
            startT = Math.max(p2pSample.datetime-delta, startTime);
            endT = Math.min(p2pSample.datetime+delta, endTime);
            tmpS = seg[i].getMaxSample(startT, endT);
            if (tmpS != null  && tmpS.value > maxVal) {
                maxVal= tmpS.value;
                if (p2pSample.datetime != tmpS.datetime) { // The max is not abs peak save its values
                    p2pSample.datetime2 = tmpS.datetime;
                    p2pSample.setValue2(maxVal);
                }
            }

            //tmpS = seg[i].getMinSample(p2pSample.datetime-delta, p2pSample.datetime+delta);
            tmpS = seg[i].getMinSample(startT, endT);
            if (tmpS != null && tmpS.value < minVal) {
                minVal= tmpS.value;
                if (p2pSample.datetime != tmpS.datetime) { // The min is not abs peak save its values
                    p2pSample.datetime2 = tmpS.datetime;
                    p2pSample.setValue2(minVal);
                }
            }
          }
          if (maxVal >= minVal) {
              tmpVal = maxVal-minVal;
              p2pSample.setValue(tmpVal);
              //System.out.println(getChannelObj().toString() +" p2p: " + p2pSample.toUnitsString(Units.getString(ampUnits)));
          }

          return p2pSample;

      } // end of synch
  }

/**
* Calculate the background noise level. It is
* the weighted mean of the peaks between zero-crossings for a rectified time-series
* with the bias removed for the whole time-series.
* Weight of segment amp is its number of peaks.
* Returns Float.NaN if it cannot be calculated.
*/
  public float scanForNoiseLevel() {
     return scanForNoiseLevel(getEpochStart(), getEpochEnd());
  }


/**
* Calculate the background noise level. Sets value of 'noiseLevel'. It is
* the weighted mean of the peaks between zero-crossings for a rectified time-series
* with the bias removed for the time window given.
* Weight of segment amp is its number of peaks.
* Returns Float.NaN if it cannot be calculated.
*/
  public float scanForNoiseLevel(TimeSpan ts) {
         return scanForNoiseLevel(ts.getStart(), ts.getEnd());
  }

/**
* Calculate the background noise level. Sets value of 'noiseLevel'. It is
* the weighted mean of the peaks between zero-crossings for a rectified time-series
* with the bias removed for the time window given.
* Weight of segment amp is its number of peaks.
* Returns Float.NaN if it cannot be calculated.
*/
  public float scanForNoiseLevel(double startTime, double endTime) {

      if (!hasTimeSeries()) {
         noiseLevel = Float.NaN;
         return noiseLevel;
      } // no time series

    synchronized (this) {
      WFSegment seg[] = WFSegment.getArray(segList);

      noiseLevel = 0.0f;
      //Segments aren't equal in length :. weight the bias by sample count
      float nwts = 0.f;
      float [] level = new float [seg.length];
      float [] wts = new float [seg.length];
      float total = 0.f;

      for (int i=0; i < seg.length; i++) {

          level[i] = seg[i].scanForNoiseLevel(startTime, endTime);
          wts[i] = seg[i].npeak;

          if (level[i] != Float.NaN) {
            //noiseLevel = Math.max(noiseLevel, level[i]); // max segment level removed 2007/02/13 - aww
            // Do weighted average of all contributing segment levels
            nwts += wts[i];
            total += level[i]*wts[i];
            noiseLevel = total/nwts;
            //System.out.printf("WFSegments NoiseLevel: %9.3f %5.2f %d%n", noiseLevel, nwts, seg.length);
          }

          // past end of time window, bail
          if (seg[i].getEnd().doubleValue() > endTime) break;
      }
    } // end of synch

      return noiseLevel;
  }

/** Return true if the Waveform has time tears.
* Because waveforms are ALWAYS collapsed, > 1 segment means there are time tears.
*/
    public boolean hasTimeTears() {
        //return (segList.size() > 1);
        if (segList.size() <= 1) return false;

        WFSegment seg[] = WFSegment.getArray(segList);
        boolean retVal = false;
        for (int i=0; i < seg.length; i++) {
            if ( (i > 0) && ! WFSegment.areContiguous(seg[i-1], seg[i])) {
                retVal = true;
                break;
            }
        }
        return retVal;
    }


    /** Return true if the Waveform has time tears within this timespan.
     * If the passed TimeSpan is not completely contained by the TimeSpan of the waveform
     * this is considered a time-tear and the method returns true.
    */
    public boolean hasTimeTears(TimeSpan ts) {
        // if the passed TimeSpan is not completely contained by the TimeSpan of the waveform
        // this is considered a time-tear and the method returns true.
        if (!getTimeSpan().contains(ts)) return true;

        // There will be only one seg for the timespan if there are no gaps
        //return (getSubSegments(ts.getStart(), ts.getEnd()).size() > 1);
        Collection list = getSubSegments(ts.getStart(), ts.getEnd());
        int cnt = list.size();
        if (cnt <= 1) return false;

        // Else check for gap between segments (maybe contiguous with different sps?)
        WFSegment[] seg = (WFSegment []) list.toArray(new WFSegment[cnt]);
        boolean retVal = false;
        for (int i=0; i < cnt; i++) {
            if ( (i > 0) && ! WFSegment.areContiguous(seg[i-1], seg[i])) {
                retVal = true;
                break;
            }
        }
        return retVal;
    }
/**
 * Return the bias of this waveform.
 */
    public float getBias() {
        return biasVal;
    }

/**
* Scans the given time window for a peak amplitude. Used to limit the scan for
* efficiency and to avoid energy from other events. If there is no time-series
* null is returned.
*/
    public Amplitude getPeakAmplitude(double startTime, double endTime) {
        return getPeakAmplitude(new TimeSpan(startTime, endTime));
    }
/**
* Scans the whole time series for a peak amplitude. If there is no time-series
* null is returned.
*/
    public Amplitude getPeakAmplitude() {
        return getPeakAmplitude(getTimeSpan());
    }
/**
* Scans the given time window for a peak amplitude. Used to limit the scan for
* efficiency and to avoid energy from other events. If there is no time-series
* null is returned.
*/
    public Amplitude getPeakAmplitude(TimeSpan timeSpan) {
        return getPeakAmplitude(timeSpan, ZERO2PEAK); 
    }

    public Amplitude getPeakAmplitude(TimeSpan timeSpan, int peakType) {
        // get the peak
        Sample peak = (peakType == ZERO2PEAK) ? scanForPeak(timeSpan) : scanForPeak2Peak(timeSpan);
        if (peak == null) return null;

        Amplitude amp = Amplitude.create();

        if (peak instanceof SampleWithPeriod) {
            amp.period.setValue(((SampleWithPeriod)peak).period);
        }
        if (peak instanceof Peak2PeakSample) {
            amp.datetime2 = ((Peak2PeakSample)peak).datetime2;
        }

        // Amp time-series completeness flag indicates if the window scanned for the peak amplitude
        // was complete. If not the peak amp may have been missed. 
        // 0.0 = incomplete data<br>
        // 0.5 = incomplete but human considers OK (peak was measured)
        // 1.0 = complete data <br>
        // Below set the channel, value, amptype, quality & time of the amp object
        // the actual type should be set by the filter -aww 12/20/2006 
        amp.setAmp(this, peak, AmpType.UNKNOWN, timeSpan);
        //System.out.println(peak + " DEBUG AbstractWF getPeakAmplitude: " + amp.toNeatString());

        // zero-to-peak or peak-to-peak ?
        amp.halfAmp = (peakType == ZERO2PEAK);

        /* Don't believe this override of the auto setQuality in Amplitude set(...) method called above is needed -aww 2010/02/23
        if (this.hasTimeTears(timeSpan)) {
          amp.setQuality(0.0);
        } else {
          amp.setQuality(1.0);
        }
        */

        return amp;
  }

/**
 * Find samples with the minimum and maximum amplitudes of this waveform by examining all
 * waveform segments.
 */
  public void scanForAmps() {

     if (!hasTimeSeries()) return;  // no time series

     synchronized (this) {
      WFSegment seg[] = WFSegment.getArray(segList);
      if ( seg == null || seg.length == 0 || seg[0] == null ) {
          System.out.println("DEBUG scanForAmps, NO timeseries:" + toAmpInfoString());
          return;
      }
      maxSample = seg[0].getMaxSample();
      minSample = seg[0].getMinSample();

      for (int i=1; i < seg.length; i++) {
          if (seg[i].getMaxSample() != null &&
              seg[i].getMaxSample().value > maxSample.value)
                   maxSample = seg[i].getMaxSample();
          if (seg[i].getMinSample() != null &&
              seg[i].getMinSample().value < minSample.value)
                   minSample = seg[i].getMinSample();
      }
     } // end of synch

     // NOTE : BELOW REQUIRES JASI_AMPPARMS_VIEW to be defined in db, else throws SQL exception -aww 12/20/2006
     /*
     double clipAmp = chan.getClipAmpValue(LeapSeconds.trueToDate(maxSample.datetime));
     if (! Double.isNaN(clipAmp) ) {
         clipped = (Math.abs(clipAmp) <= maxSample.value);
     }
     */
 }

 public void setClipped(boolean tf) {
     clipped = tf;
 }
 public boolean isClipped() {
     return clipped;
 }

/**
 * Return the Sample with the maximum amplitude of this waveform.
 * Return's null if no time-series. NOTE: bias is NOT removed.
 */
  public Sample getMaxSample() {
    if (maxSample == null) scanForAmps();
    return maxSample;
  }
/**
 * Return the maximum amplitude value of this waveform.
 * Return's 0 if no time-series.  NOTE: bias is NOT removed.
 */
  public double getMaxAmp() {
    if (maxSample == null) return 0.;
    return maxSample.value;
  }

/**
 * Return the Sample with the minimum amplitude of this waveform.
 * Return's null if no time-series.  NOTE: bias is NOT removed.
 */
  public Sample getMinSample() {
    if (minSample == null) scanForAmps();
    return minSample;
  }
/**
 * Return the minimum amplitude of this waveform.
 * Returns 0 if no time-series.   NOTE: bias is NOT removed.
 */
  public double getMinAmp() {
    if (minSample == null) return 0.;
    return minSample.value;
  }
/**
 * Return the total range of amplitudes in the trace
 */
  public double rangeAmp() {
    return getMaxAmp() - getMinAmp();
  }

/**
 * Return a Sample with the maximum <b>rectified</b> amplitude of this waveform with
 * the bias removed. Thus the Sample.value will always be positive.
 * Return's null if no time-series.
 */
  public Sample getPeakSample() {
    // bias is always subtracted (removed)
    Sample maxSmp = getMaxSample();
    Sample minSmp = getMinSample();

    if (minSmp == null || maxSmp == null) return new Sample(); // null sample

    double maxVal = maxSmp.value - getBias();
    double minVal = minSmp.value - getBias();

    maxVal = Math.abs(maxVal);
    minVal = Math.abs(minVal);
    // compare absolute values to determine greatest deviation from mean
    if (minVal > maxVal) {
      return new Sample(minSmp.datetime, minVal);
    } else {
      return new Sample(maxSmp.datetime, maxVal);
    }
  }

    /**
     * Return string describing the Waveform object, primarily for debugging.
     */
    public String toString() {
      StringBuffer sb = new StringBuffer(512);
      sb.append("Waveform: channel = ").append(chan.toDelimitedSeedNameString());
      sb.append(", timeStart = ").append(LeapSeconds.trueToString(timeStart.doubleValue())); // for UTC true time -aww 2008/02/11
      sb.append(", timeEnd = ").append(LeapSeconds.trueToString(timeEnd.doubleValue())).append("\n"); // for UTC true time -aww 2008/02/11
      sb.append("   sps = ").append(samplesPerSecond.toString());
      sb.append(", #samples = ").append(samplesInMemory());
      sb.append(", timeQual = ").append(getTimeQuality().doubleValue());
      sb.append(", encoding = ");
      sb.append(SeedEncodingFormat.getTypeString( encoding.intValue() ));
      sb.append(", format = ").append(format.toString());
      //sb.append(", wordOrder = ").append(wordOrder.toString()); // if wordOrder added to waveform query
      sb.append(", segments = ").append(segList.size()).append("\n");

      sb.append("   archive = ").append(waveArchive);
      sb.append(", wavetype = ").append(waveType);
      sb.append(", status = ").append(waveStatus);
      sb.append(", filterName =").append(filterName).append("\n");

      sb.append("   ampUnits = ").append(Units.getString(ampUnits));
      sb.append(", clipped = ").append(clipped);
      sb.append(", minAmp = ").append(getMinAmp());
      sb.append(", maxAmp = ").append(getMaxAmp());
      sb.append(", bias = " ).append(getBias());
      if (getMinAmp() == getMaxAmp()) sb.append(" <<<<---- FLAT-LINED, NO ACTIVITY");
      sb.append("\n");

      sb.append("   path/file = ").append((path == null) ? "" : path).append((filename == null) ? "" : filename).append("\n");
      sb.append("   wcopy = ").append(getWcopy());
      sb.append(", savingEvent = ").append(savingEvent.toString());
      sb.append(", velocityModel = ").append(((getTravelTimeModel() == null) ? "null" : getTravelTimeModel().getModel().getName()));

      return sb.toString();
    }

    private static final  Format s20 = new Format("%-20s");
    private static final  Format d2 = new Format("%2d");
    private static final  Format d6 = new Format("%6d");
    private static final  Format e14 = new Format("%+14.2f");
    public String toAmpInfoString() {
      StringBuffer sb = new StringBuffer(512);
      sb.append(s20.form(chan.toDelimitedSeedNameString()));
      sb.append(" ").append(LeapSeconds.trueToString(timeStart.doubleValue()));
      sb.append(" ").append(LeapSeconds.trueToString(timeEnd.doubleValue()));
      sb.append(" ").append(d2.form(segList.size()));
      sb.append(" ").append(d6.form(samplesInMemory()));
      sb.append(" ").append(Units.getString(ampUnits));
      sb.append(" ").append((clipped) ? "1" : "0");
      sb.append(" ").append(e14.form(getMinAmp()));
      sb.append(" ").append(e14.form(getMaxAmp()));
      sb.append(" ").append(e14.form(getBias()));
      return sb.toString();
    }


    /**
     * Return string describing the Waveform object, one line per data field, primarily for debugging.<p>
     */
    public String toBlockString() {
        final String fmtString = "yyyy-MM-dd HH:mm:ss.fff";
        StringBuffer sb = new StringBuffer(512);

        sb.append("Waveform: channel = ").append(chan.toDelimitedSeedNameString());
        sb.append("\ntimeStart = ").append(LeapSeconds.trueToString(timeStart.doubleValue(), fmtString));
        sb.append("\ntimeEnd   = ").append(LeapSeconds.trueToString(timeEnd.doubleValue(), fmtString));

        sb.append("\nsps = ").append(samplesPerSecond.toString());
        sb.append("\nencoding = ");
        sb.append(SeedEncodingFormat.getTypeString( encoding.intValue() ));
        sb.append(" (").append(encoding.toString()).append(")");
        sb.append("\n#samples = ").append(samplesInMemory());
        sb.append("\ntimeQual = ").append(getTimeQuality().doubleValue());
        sb.append("\nformat = " ).append(format.toString());
        //sb.append("\nwordOrder = " ).append(wordOrder.toString()); // if wordOrder added to waveform query
        sb.append("\nsegments = ").append(segList.size());

        sb.append("\narchive = ").append(waveArchive);
        sb.append("\nwavetype = ").append(waveType);
        sb.append("\nstatus = ").append(waveStatus);
        sb.append("\nfilterName =").append(filterName);

        sb.append("\nampUnits = " ).append(Units.getString(ampUnits));
        sb.append("\nclipped = " ).append(clipped);
        sb.append("\nminAmp = " ).append(getMinAmp());
        sb.append("\nmaxAmp = " ).append(getMaxAmp());
        sb.append("\nbias = " ).append(getBias());
        sb.append("\npath/file = ").append((path == null) ? "" : path).append((filename == null) ? "" : filename);
        sb.append("\nwcopy = ").append(getWcopy());
        sb.append("\nsavingEvent = ").append(savingEvent.toString());
        sb.append("\nvelocityModel = ").append(((getTravelTimeModel() == null) ? "null" : getTravelTimeModel().getModel().getName()));

        return sb.toString();
    }

    // /** Set the travel-time model used to calculate the energy window. */
    //public void setTravelTimeModel(TravelTime model) {
    //    ttModel = model;
    //}

    /** Return the travel-time model used to calculate the energy window. */
    public TravelTime getTravelTimeModel() {
        //if (ttModel == null) ttModel = TravelTime.getInstance();
        //return ttModel;
        return TravelTime.getInstance();
    }

     /** Return a TimeSpan representing a window where you are likely to find
    * the peak seismic energy. Distance and depth are in km.
    * The window begins 4 sec before the calculated P-wave onset and
    * ends at P-wave onset plus 3x the S-P time.<p>
    * Uses the standard TravelTime model.
    *
    * @see: TravelTime()
    */
// DDG 5/27/2003 -- require depth
    public TimeSpan getEnergyTimeSpan(double originTime, double horizDistance, double depth) {

        TravelTime tt = getTravelTimeModel();
        double ttp = tt.getTTp(horizDistance, depth);
        double tts = tt.getTTs(horizDistance, depth);

        //double energyStart = originTime + ttp - 1.0;
        //double energyEnd   = energyStart + 2.0*(tts-ttp);
        double energyStart = originTime + ttp - 4.0;   // 11/16/2006 - in some cases P is earlier than predicted -aww
        double energyEnd   = energyStart + smpWindowMultiplier*(tts-ttp) + 4.0;   // 11/16/2006 in some cases BK late surface waves -aww
        TimeSpan ts = new TimeSpan(energyStart, energyEnd);
        //System.out.println (chan.toDelimitedSeedNameString() + " ts:"+ ts.toString() + " : " + ts.getDuration());

        //BK - Below relations used in REDI system for waveform windowing b4 FFT convolution for W-A
        //energyStart = originTime + horizDistance/8.5;
        //energyEnd = originTime + ((horizDistance <= 100.) ? horizDistance/2.0 : horizDistance/1.8);
        //ts = new TimeSpan(energyStart, energyEnd);
        //System.out.println(chan.toDelimitedSeedNameString() + " ts_bk:"+ ts.toString() + " : " + ts.getDuration());

        return ts;
    }

    public TimeSpan getSWaveEnergyTimeSpan(double originTime, double horizDistance, double depth) {
        TravelTime tt = getTravelTimeModel();
        double ttp = tt.getTTp(horizDistance, depth);
        double tts = tt.getTTs(horizDistance, depth);
        double smp = tts - ttp;
        double delta = (smp < 4.) ? smp : 4.;
        double energyStart = (originTime + tts) - delta;
        double energyEnd   = energyStart + (smpWindowMultiplier-1.)*smp + delta; 
        return new TimeSpan(energyStart, energyEnd);
    }

     /** Return a TimeSpan representing a window before the seismic energy.
    * The window begins at the start of the waveform and ends 4 sec before the
    * calculated P-wave onset.
    * Uses the standard TravelTime model.
    */
    public TimeSpan getPreEnergyTimeSpan(double originTime, double horizDistance, double depth) {

       return new TimeSpan(getEpochStart(),
                            getEnergyTimeSpan(originTime, horizDistance, depth).getStart());
    }

/**
 *  Return the sample closest to the given time. Times are raw epoch seconds.
 *  Null is returns if  there is no time series.
 *  This is used to force operator picks to lie on a sample. We actually scan the data
 *  rather than calculate because there may be a time-tear. If the time is
 *  equidistant between samples the earlier sample will be returned.
 *  Also we want to look up the amp for the sample .
 */
    public Sample closestSample(double dtime) {
        synchronized (this) {

            if (!hasTimeSeries()) return null;  // no time series

            // before time range
            if (dtime < timeStart.doubleValue()) return getFirstSample();
            // after time range
            if (dtime > timeEnd.doubleValue())   return getLastSample();

            //  must be in the time range
            Sample samp;
            Sample closeSamp = null;
            double dtNew;
            double dtOld = Double.MAX_VALUE;

            WFSegment seg[] = WFSegment.getArray(segList);

            for (int i=0; i < seg.length; i++) {

                samp = seg[i].closestSample(dtime);

                // closer?
                dtNew = Math.abs((samp.datetime - dtime));
                if (dtNew < dtOld) {
                    dtOld = dtNew;
                    closeSamp = samp;
                }

            }

            return closeSamp;
        } // end of synch
    }

    /** Return the first sample in the Waveform */
    public Sample getFirstSample() {
        if (!hasTimeSeries()) { return null; }  // no time series
        return ((WFSegment) segList.get(0)).getFirstSample();
    }

    /** Return the last sample in the Waveform */
    public Sample getLastSample() {
      if (segList.size() == 0) { return null; } // no time series
        return ((WFSegment) segList.get(segList.size()-1)).getLastSample();
    }

    /**
    * Return a collection of WFSegments that contains the time-series between
    * the given times. Only one WFSegment will result if there are no time-tears.
    * If either the start or end time is beyond what is available in the waveform
    * the returned WFSegments will be limited by the available data. Returns an
    * empty Collection if there is no data within the time window.
    */

    public Collection getSubSegments(double startTime, double endTime) {
        synchronized (this) {
            ArrayList newSegList = new ArrayList();
            if (!hasTimeSeries()) return newSegList;  // no time series
            WFSegment seg[] = WFSegment.getArray(segList);
            WFSegment newSeg;
            for (int i=0; i < seg.length; i++) {
                newSeg = seg[i].getSubSegment(startTime, endTime);
                if (newSeg != null && newSeg.size() > 0) {
                   newSegList.add(newSeg);
                }
            }
            return newSegList;
        } // end of synch
    }

    public long getEncoding() {
        return encoding.longValue();
    }
    public void setEncoding(long value) {
        encoding.setValue(value);
    }

    /*
    public String getDataMode() {
        return dataMode.toString();
    }
    public void setDataMode(String value) {
        dataMode.setValue(value);
    }
    */
    public String getWaveType() {
        return waveType.toString();
    }
    public void setWaveType(String value) {
        waveType.setValue(value);
    }

    public String getWaveStatus() {
        return waveStatus.toString();
    }
    public void setWaveStatus(String value) {
        waveStatus.setValue(value);
    }

    public String getWaveArchive() {
        return waveArchive.toString();
    }
    public void setWaveArchive(String value) {
        waveArchive.setValue(value);
    }

    public String getSavingEvent() {
        return (savingEvent.isNull()) ? null : savingEvent.toString();
    }
    public void setSavingEvent(String evid) {
        savingEvent.setValue(evid);
    }

    /**
    * Return string with path/filename for this waveform
    */
    public String getPathFilename() {
        return path+filename;
    }

    /**
     * Set the Path string. Expects a trailing "/" in UNIX or
     *  "\" in WINTEL
     */
    public void setFilePath(String pname) {
        path = pname;
    }

    /**
     *
     */
    public String getFilePath() {
        return path;
    }

    /**
     * Set the filename string.
     */
    public void setFilename(String fname) {
        filename = fname;
    }

    /**
     *
     */
    public String getFilename() {
        return filename ;
    }

    /**
     *
     */
    public int getFileOffset() {
        return fileOffset;
    }

    /**
     *
     */
    public void setFileOffset(int offset) {
        fileOffset  = offset;
    }
    /**
     *
     */
    public int getFileByteCount() {
        return byteCount;
    }

    public void setFileByteCount(int count) {
        byteCount = count;
    }

    public String getDescription() {
        return "";
    }

} // end of AbstractWaveform class
