package org.trinet.jasi;

import java.util.*;
import java.sql.*;      // need for Connection

import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

import org.trinet.util.Format;      // CoreJava printf-like Format class

/**
 * An Amplitude object represents an amplitude reading made by any number of
 * techniques. Defaults are: half-amp (not peak-to-peak), units = Units.UNKNOWN, and type is
 * AmpType.UNKNOWN.
 *
 * @See: AmpType
 * @See: Units
 */

// DDG 3/28/03 -- added units conversion methods
    public abstract class Amplitude extends MagnitudeAssocJasiReading {

    protected static boolean debug = false;

    /** Optional. If the algorithm averages or scans a time window this is
        the start time of the window. */
    public DataDouble  windowStart  = new DataDouble();

    /**  Optional. If the algorithm averages or scans a time window this is
        the duration of the window in seconds. */
    public DataDouble  windowDuration   = new DataDouble();

    /** The amplitude value */
    public DataDouble  value        = new DataDouble();

    /** Units of the amplitude measure. (e.g. cm, mm, counts)
     * @See: Units */
    public int units = Units.UNKNOWN;

    /** The amplitude type.
     @see: AmpType */
    public int type = AmpType.UNKNOWN;

    /** True if the reading is zero-to-peak. This is the default. False means
        the amp is measured peak-to-peak. */
    public boolean halfAmp = true;

    /** Estimated or measured uncertainty of the reading. Always in the same
        units as the reading itself. */
    public DataDouble uncertainty = new DataDouble();

    /** Period of the signal. */
    public DataDouble period = new DataDouble();

    /** Time of the secondary peak (+/-), if known, when the value is Peak to Peak measurement, i.e halfAmp=false. */
    public double datetime2 = Double.NaN;

    /** Signal/noise ratio. */
    public DataDouble snr = new DataDouble();

    /**  Time-series completeness flag indicates if the window scanned for the peak amplitude
     * was complete. If not the peak amp may have been missed.  <p>
        0.0 = incomplete data<br>
        0.5 = incomplete but human considers OK (peak was measured)<br>
        1.0 = complete data <br>

     * @see setWeightUsed()
     * @see getWeightUsed()
     * */
    public DataDouble quality   = new DataDouble();

    /** Clipped flag:  0 = on scale, 1 = clipped, 2 = below noise floor
    @see: isClipped() */
    protected int clipped = 0; // attribute of measured amplitude value
    public static final int ON_SCALE = 0;
    public static final int CLIPPED  = 1;
    public static final int BELOW_NOISE = 2;

    public static final int PEAK_TO_PEAK = 0;
    public static final int ZERO_TO_PEAK = 1;

    /** Completeness flag indicates if the window scanned for the peak amplitude
     * was complete. If not the peak amp may have been missed. */
    public static final double COMPLETENESS_TRUE = 1.0;
    public static final double COMPLETENESS_UNKNOWN = 0.5;
    public static final double COMPLETENESS_FALSE = 0.0;

    /** Type of phase from which the amplitude was measured. ('P', 'S', etc.)*/
    public DataString  phaseType    = new DataString();

// ///////////////////////////////////////////////////////////////////
// -- Concrete FACTORY METHODS ---
    /**
     * Factory Method: This is how you instantiate a JasiObject. You do
     * NOT use a constructor. This is so that this "factory" method can create
     * the type of object that is appropriate for your site's data source.
     */

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. Creates a Solution of the DEFAULT type.
     * @See: JasiObject
     */
     public static Amplitude create() {
       return create(DEFAULT);
     }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is an integer implementation type.
     * @See: JasiObject
     */
     public static Amplitude create(int schemaType) {
       return create(JasiFactory.suffix[schemaType]);
     }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is as 2-char implementation suffix.
     * @See: JasiObject
     */
     public static Amplitude create(String suffix) {
       return (Amplitude) JasiObject.newInstance("org.trinet.jasi.Amplitude", suffix);
     }
// ///////////////////////////////////////////////////////////////////

    abstract public boolean copySolutionDependentData(Amplitude newAmp);

    public boolean copySolutionDependentData(JasiReadingIF jr) {
      return copySolutionDependentData((Amplitude)jr);
    }

    /** Map input Amp data to this Amp.*/
    public boolean replace(Amplitude aAmp) {
        setChannelObj(aAmp.getChannelObj());
        assign(aAmp.getAssociatedSolution());
        assign(aAmp.getAssociatedMag());
        channelMag = (ChannelMag) aAmp.channelMag.clone();
        datetime.setValue(aAmp.datetime);
        residual.setValue(aAmp.residual);
        comment.setValue(aAmp.comment);
        windowStart.setValue(aAmp.windowStart);
        windowDuration.setValue(aAmp.windowDuration);
        value.setValue(aAmp.value);
        units = aAmp.units;
        type =  aAmp.type;
        halfAmp =  aAmp.halfAmp;
        uncertainty.setValue(aAmp.uncertainty);
        period.setValue(aAmp.period);
        snr.setValue(aAmp.snr);
        quality.setValue(aAmp.quality);
        clipped = aAmp.clipped;
        phaseType.setValue(aAmp.phaseType);

        return true;
    }

    public boolean replace(JasiReadingIF jr) {
      return replace((Amplitude) jr);
    }

    public abstract AmpList getDuplicate(Amplitude amp);

    /** Populate this Amplitude object with values as described in this waveform,
    * sample, and time window. */
    public Amplitude setAmp(Waveform wf, Sample peak, int amptype, TimeSpan timeSpan) {
        setChannelObj(wf.getChannelObj()); // assuming does as code below:
        //if (chan == null) chan = Channel.create();
        //chan.setChannelObj(wf.getChannelObj());

        datetime.setValue(peak.datetime);
        value.setValue(peak.value);

        // Set AmpType value
        type = amptype;

        // zero-to-peak or peak-to-peak ?
        halfAmp = true;

        // Set units
        units = wf.getAmpUnits();

        //  uncertainty //  period //  snr //  quality

        clipped = 0;         // not clipped

        setWindow(timeSpan);

        autoSetWindowCompleteness(wf, timeSpan);

        return this;
    }

    /** Enforce "completeness" rule for defining quality. */
    private DataDouble autoSetWindowCompleteness(Waveform wf, TimeSpan ts) {
       if (wf.hasTimeTears(ts)) {
         String ps = getProcessingStateString();
         if (ps.equals(JasiProcessingConstants.STATE_HUMAN_TAG) || ps.equals(JasiProcessingConstants.STATE_FINAL_TAG)) {
           setQuality(COMPLETENESS_UNKNOWN);   // incomplete but human-reviewed
         } else {
           setQuality(COMPLETENESS_FALSE);     // incomplete automatic
         }
       } else {
         setQuality(COMPLETENESS_TRUE);        // complete
       }
       return getWindowCompleteness();
    }
    /** Set the window that was scanned for this amp. */
    public void setWindow(TimeSpan timeSpan) {
      windowStart.setValue(timeSpan.getStart());
      windowDuration.setValue(timeSpan.getDuration());;
    }

    /** Set the window that was scanned for this amp and the completeness value. */
    public void setWindow(TimeSpan timeSpan, double completeness) {
      setWindow(timeSpan);
      setWindowCompleteness(completeness);
    }
    /**  Completeness flag indicates if the window scanned for the peak
     * amplitude was complete. If not the peak amp may have been missed.  */
    public void setWindowCompleteness(double value) {
      quality.setValue(value);
    }
    /**  Completeness flag indicates if the window scanned for the peak
     * amplitude was complete. If not the peak amp may have been missed.  */
    public DataDouble getWindowCompleteness( ) {
      return quality;
    }

    /**
     * Associate this reading with this Solution.
     */
    public void associate(Solution sol) {
      this.sol = sol;
      if (sol != null) sol.ampList.add(this);
    }

    // method for JasiCommitableIF
    public Object getTypeQualifier() { return AmpType.getString(type);}

    /** Returns true if input is an Amplitude and its type and phase
     * descriptions match those of this instance.
     * @see: #isLikeChanType(JasiReading) */
    public boolean isSameType(JasiReadingIF jr) {
      if (! (jr instanceof Amplitude)) return false;
      Amplitude amp = (Amplitude) jr;
      boolean status = ((this.type == amp.type) || AmpType.equivalentData(this.type, amp.type));
      return (status && this.phaseType.equals(amp.phaseType)) ?  true : false;
      //return (this.type.equals(amp.type) && this.phaseType.equals(amp.phaseType)) ?  true : false;
    }

    /** Returns the integer value equivalent to a type implemented by AmpType.
     * @see AmpType
     */
    public int getType() {
        return type;
    }

    /** Set the amplitude type. Returns false if the the argument is not a legal
     * type that is defined in the enumeration AmpType class. Returns true if OK.
     * @See: AmpType
     */
    public boolean setType(int type) {
      if (AmpType.isLegal(type)) {
        this.type = type;
        return true;
      } else {
        return false;
      }
    }
    /** Set the amplitude type. Returns false if the the argument is not a legal
     * type that is defined in the enumeration AmpType class. Returns true if OK.
     * @See: AmpType
     */
    public boolean setType(String str) {
      if (AmpType.isLegal(str)) {
        this.type = AmpType.getInt(str);
        return true;
      } else {
        return false;
      }
    }
    /** Set the value of the amplitude reading.
     * Returns 'false' if the units are not valid.
     * @see: org.trinet.jasi.Units */
    public boolean setValue(double val, int units) {
      value.setValue(val);
      return setUnits(units);
    }
    /** Set the value of the amplitude reading.
     * Returns 'false' if the units are not valid.
     * @see: org.trinet.jasi.Units */
    public boolean setValue(DataDouble val, int units) {
      value.setValue(val);
      return setUnits(units);
    }
    /** Get the value of the amplitude reading.
     * @see: org.trinet.jdbc.datatypes.DataDouble */
    public DataDouble getValue() {
      return value;
    }

    /** Get the value of the amplitude reading as a DataDouble in the given Units.
     * @see: org.trinet.jdbc.datatypes.DataDouble
     * @see: org.trinet.jasi.Units */
    public DataDouble getValueAs(int units) {
      if (getUnits() == units) return getValue();
      return value;
    }
    /** Get the value of the amplitude reading as a double in CGS unit. Returns
     Double.NaN if conversion fails.
     * @see: org.trinet.jasi.Units */
    public double getValueAsCGS() {
      int cgsUnit = Units.getCGSunit(getUnits());
      return Units.convert(value.doubleValue(), getUnits(), cgsUnit);
    }

    /** Get the value of the amplitude reading as a double. If internall DataDouble
     * value is null this will return Double.NaN. */
    public double getValueDoubleValue() {
      return value.doubleValue();
    }
    /** Set the physical units of the Amplitude.
     * Returns 'false' if the units are not valid.
     * @see: org.trinet.jasi.Units */
    public boolean setUnits(int units) {
      if (Units.isLegal(units)) {
        this.units = units;
        return true;
      } else {
        return false;
      }
    }
    /** Return the physical units of the Amplitude.
     * @see: org.trinet.jasi.Units */
    public int getUnits() {
      return units;
    }
    /** Convert amp value from the original units to the 'newUnits'. Only works for compatible unit
     types that can be converted by shifting the decimal point.
     Returns false if the units are illegal or incompatible.
     @See: Units.convert */
    public boolean convertUnits(int newUnits) {
      double newval = Units.convert(getValue().doubleValue(), getUnits(), newUnits);
      if (Double.isNaN(newval)) return false;

      this.setValue(newval, newUnits);
      return true;

    }
    /** Convert amp value from the original units to CGS units. Only works for compatible unit
     types that can be converted by shifting the decimal point.
     Returns false if the units are illegal or incompatible.
     @See: Units.convert()
     @See: Units.getCGSunit() */
    public boolean convertToCGSunits() {
      return convertUnits(Units.getCGSunit(getUnits()));
    }
    /** Return true if the amplitude itself is already corrected and a channel
     * correction should not be applied. */
    public boolean isCorrected() {
      return AmpType.isCorrected(this.type);
    }

    /** Set the quality. */
    public void setQuality(double value) {
      // Note: AbstractWaveform getPeakAmplitude(TimeSpan) sets quality to 0.(Auto) or 0.5 (Human)
      // if there's a time tear inside the input timespan, amp may or not be legit
      quality.setValue(value);
//    channelMag.weight.setValue(value); // weight used is set by mag algorithm
    }
    /** Return the quality. Range is 0.0 to 1.0 (returns 1.0 if null, value unknown).
     *
     * */
    public double getQuality() {
      // What should be the "default" value for unknown, null value?
      //if (quality == null || quality.isNull()) return 0.0; // should it be 1.0 ?
      if (quality == null || quality.isNull()) return 1.0; // made 1.0  -aww 2006/10/04
      return quality.doubleValue();
    }

    /** A value between 0.0 and 1.0 used to indicate problem or bad readings in
    * a generic way. Gives a "grade" to a reading
    * that is used by components that want to highlight "bad" readings.
    * 0.0 is best, 1.0 is worst.
    */
    //public double getWarningLevel() {
      //return Math.min(Math.abs(this.channelMag.residual.doubleValue()), 1.0);
      //return Math.abs(this.channelMag.residual.doubleValue());
    //}

    /** Return true if the amplitude is clipped or below noise floor. */
    public boolean isClipped() {
      return ! (clipped == ON_SCALE);
    }
    /** Returns one of the following: <p> <tt>
       ON_SCALE=0, CLIPPED =1, BELOW_NOISE=2
       </tt>
     */
    public int getClipped() { return clipped;}

    /** Set clip value. Must be one of the following: <p><tt>
       ON_SCALE = 0
       CLIPPED  = 1
       BELOW_NOISE = 2
       </tt><p>
       Returns 'false' if val is an illegal value.
    */
    public boolean setClipped(int val) {
      if (val < ON_SCALE || val > BELOW_NOISE) return false;
      clipped = val;
      return true;
    }

    /** Return true if the amplitude is on scale, that is not clipped or
    below noise floor. */
    public boolean isOnScale() {
      return ( clipped == ON_SCALE && value.isValidNumber() );
    }

    public String toString() {
      StringBuffer sb = new StringBuffer(300);
      sb.append(toAssocIdString());
      sb.append(" Auth: ").append(authority.toString());
      sb.append(" Src: " ).append(source.toString());
      //sb.append(" Time: ").append(getDateTime().toString());
      sb.append(" Time: ").append(getDateTime().toDateString("yyyy-MM-dd HH:mm:ss.fff")); // instead aww 2015/11/20
      sb.append(" Val: ").append(value.toString());
      sb.append(" Units: ").append(units).append("=").append(Units.getString(units));
      sb.append(" SNR: ").append(snr.toString());
      sb.append(" Per: ").append(period.toString());
      sb.append(" Phs: ").append(phaseType.toString());
      sb.append(" Typ: ").append(AmpType.getString(type));
      sb.append(" Clip: ").append(clipped);

      sb.append(" slantDist: ").append(getDistance()); // slant awwDist 06/11/2004
      sb.append(" horizDist: ").append(getHorizontalDistance()); // aww 06/11/2004
      sb.append(" Az: "  ).append(getAzimuth());
      sb.append(" Unc: " ).append(uncertainty.toString());
      sb.append(" Qual: ").append(getQuality());
      if (channelMag != null)
              sb.append(" | ").append(channelMag.toString());
      return sb.toString();
    }
/*
      Return a fixed format string of the form:
<tt>
Nt Sta  Chn     Dist     Value Typ  Uncert     SNR
CI DGR  HHE    13.82    0.0060 WAS    0.00    5.12
xxxxxxxxxxx xxxxx.xx xxxx.xxxx xxx xxxx.xx xxxx.xx
</tt>
You must call getNeatStringHeader() to get the header line shown above.

@see getNeatStringHeader()
    */
    public String toNeatString() {

        Format df = (type == AmpType.M0) ? new Format("%9.2e"): new Format("%9.4f"); 

        Format df1 = new Format("%8.2f");
        Format df2 = new Format("%9.2f");
        Format df3 = new Format("%4.2f");
        Format df4 = new Format("%6.2f");
        Format dfs = new Format("%-6s");
        Format df0 = new Format("%3d");

        String chl    = "xxx";
        String chlmag = "-1.";
        try {
          chl = getChannelObj().getChannelName().toNeatString();
          chlmag = channelMag.toNeatString();
        }
        catch (NullPointerException ex) {
          System.out.println("Amp.neatString() " + ex.toString());
        }
        StringBuffer sb = new StringBuffer(128);
            sb.append(chl).append(" ");
        //sb.append(df1.form(getDistance())).append(" "); // don't use slant ? aww 06/11/2004
        sb.append(df1.form(getHorizontalDistance())).append(" "); // aww 06/11/2004
        sb.append(df0.form(Math.round(getAzimuth()))).append(" "); // aww 07/15/2010
        //sb.append(getDateTime().toString().substring(11)).append(" ");        
        sb.append(getDateTime().toDateString("yyyy-MM-dd HH:mm:ss.fff").substring(11)).append(" "); // instead aww 2015/11/20
        sb.append(df.form(value.doubleValue())).append(" ");
        sb.append(dfs.form(AmpType.getString(type))).append(" ");
        sb.append(dfs.form(Units.getString(units))).append(" ");
        sb.append(halfAmp ? " T " : " F ");
        //sb.append(df1.form(uncertainty.doubleValue())).append(" ");
        sb.append(df2.form(snr.doubleValue())).append(" ");
        sb.append(df4.form(period.doubleValue())).append(" ");
        sb.append(df3.form(quality.doubleValue())).append(" ");
        Concatenate.rightJustify(sb, getSource(), 8).append(" ");
        Concatenate.rightJustify(sb, ((isClipped()) ? "F" : "T"), 2).append(" ");
        Concatenate.rightJustify(sb, chlmag, 29).append(" ");
        sb.append(getProcessingStateString()).append(" ");
        if (isDeleted()) sb.append(" DELETED");
        if (isReject()) sb.append(" REJECT");
        if (! comment.isNull()) sb.append(" ").append(comment.toString());
        return sb.toString();
    }
    /**
     * Return a fixed format header to match output from toNeatString(). Has the form:
     * @see: toNeatString()
    */
    public static String getNeatStringHeader() {
      StringBuffer sb = new StringBuffer(300);
      sb.append(ChannelName.getNeatStringHeader());
      sb.append("     Dist  Az     Datetime     Value Type   Units  Z2P      SNR    Per Qual   Source OS");
      sb.append(ChannelMag.getNeatStringHeader()); // include channelmag
      sb.append(" F");
      return sb.toString();
    }

    public String getNeatHeader() {
        return getNeatStringHeader();
    }

    // Clone the amplitude dependent data of this object.
    public Object clone() {
      Amplitude amp      = (Amplitude) super.clone();
      amp.windowStart    = (DataDouble) this.windowStart.clone();
      amp.windowDuration = (DataDouble) this.windowDuration.clone();
      amp.value          = (DataDouble) this.value.clone();
      amp.uncertainty    = (DataDouble) this.uncertainty.clone();
      amp.period         = (DataDouble) this.period.clone();
      amp.snr            = (DataDouble) this.snr.clone();
      amp.quality        = (DataDouble) this.quality.clone();
      amp.phaseType      = (DataString) this.phaseType.clone();
      return amp;
    }

    /* IF methods below may not be needed to vacate lists but not null associations.
    public void removeFromAssocLists() { removeFromAssocSolList(); }
    public void removeFromAssocSolList() {
       if (sol != null) {
         // sol list removal should invoke removal from mag list
         if (sol.remove(this)) removeFromAssocMagList();
       }
    }
    public void removeFromAssocMagList() { if (magnitude != null) magnitude.remove(this); }
    */

    // Do we want to implement this method for all JasiReading types (Amplitude, Coda, Phase)? aww
    abstract public Collection getByAmpSet(long evid, long ampsetid);
    abstract public Collection getByValidAmpSet(long evid, String ampsettype, String subsrcExpr);

    abstract public Collection getBySolutionNets(long id, String[] netList);
    abstract public Collection getByEarliestLoadedOridForSolutionNets(long id, String[] netList);

} // Amplitude
