package org.trinet.jasi;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

public abstract class ChannelClipAmpData extends AbstractChannelData implements Cloneable, java.io.Serializable {

   protected UnitsAmp maxAmp  = new UnitsAmp();
   protected UnitsAmp clipAmp = new UnitsAmp();

    protected ChannelClipAmpData() {}

    protected ChannelClipAmpData(ChannelIdIF id) {
        super(id);
    }
    protected ChannelClipAmpData(ChannelIdIF id, DateRange dateRange) {
        super(id, dateRange);
    }
    protected ChannelClipAmpData(ChannelIdIF id, DateRange dateRange, UnitsAmp maxAmp, UnitsAmp clipAmp) {
        super(id, dateRange);
        this.maxAmp = maxAmp;
        this.clipAmp = clipAmp;
    }
    protected ChannelClipAmpData(ChannelIdIF id, DateRange dateRange, double maxAmp, int maxAmpUnits,
                    double clipAmp, int clipAmpUnits) {
        super(id, dateRange);
        this.maxAmp = new UnitsAmp(maxAmp, maxAmpUnits);
        this.clipAmp = new UnitsAmp(clipAmp, clipAmpUnits);
    }

    public static final ChannelClipAmpData create() {
        return create(JasiObject.DEFAULT);
    }
    public static final ChannelClipAmpData create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
    }
    public static final ChannelClipAmpData create(String suffix) {
        return (ChannelClipAmpData)
            JasiObject.newInstance("org.trinet.jasi.ChannelClipAmpData", suffix);
    }

    public static ChannelClipAmpData getByChannel(Channel chan, java.util.Date date) {
        return (ChannelClipAmpData)
           ChannelClipAmpData.create().getByChannelId(chan.getChannelObj().getChannelId(), date);
    }

    public boolean copy(ChannelDataIF cd) {
      if (cd instanceof ChannelClipAmpData) return false;
      ChannelClipAmpData data = (ChannelClipAmpData) cd;
      maxAmp.setValue(data.maxAmp.doubleValue());
      maxAmp.setUnits(data.maxAmp.getUnits());
      clipAmp.setValue(data.clipAmp.doubleValue());
      clipAmp.setUnits(data.clipAmp.getUnits());
      return true;
    }

    public void setAmpUnits(int iunits) {
      maxAmp.setUnits(iunits);
      clipAmp.setUnits(iunits);
    }
    public boolean setAmpUnits(String sunits) {
       return (maxAmp.setUnits(sunits) && clipAmp.setUnits(sunits));
    }

    public int getMaxAmpUnits() {
      return maxAmp.getUnits();
    }
    public double getMaxAmpValue() {
      return (maxAmp.isNull()) ? 0. : maxAmp.doubleValue(); // was NaN for null -aww 2009/03/10
    }
    public void setMaxAmpValue(double maxValue) {
      maxAmp.setValue(maxValue);
    }
    public boolean isMaxAmpNull() {
      return maxAmp.isNull();
    }
    protected void setMaxAmp(UnitsAmp maxAmp) {
      this.maxAmp = maxAmp;
    }
    protected UnitsAmp getMaxAmp() {
      return maxAmp;
    }

    public int getClipAmpUnits() {
      return clipAmp.getUnits();
    }
    public double getClipAmpValue() {
      return (clipAmp.isNull()) ? 0. : clipAmp.doubleValue(); // was NaN for null -aww 2009/03/10
    }
    public void setClipAmpValue(double clipValue) {
      clipAmp.setValue(clipValue);
    }
    public boolean isClipAmpNull() {
      return clipAmp.isNull();
    }
    protected void setClipAmp(UnitsAmp clipAmp) {
      this.clipAmp = clipAmp;
    }
    protected UnitsAmp getClipAmp() {
      return clipAmp;
    }

    public String toString(){
      return maxAmp.toString() + " " + clipAmp.toString();
    }

    public Object clone() {
      ChannelClipAmpData data = (ChannelClipAmpData) super.clone();
      data.maxAmp = (UnitsAmp) maxAmp.clone();
      data.clipAmp = (UnitsAmp) clipAmp.clone();
      return data;
    }
    public String toNeatString() { return toString(); }
    public String getNeatHeader() { return getNeatStringHeader(); }
    public static String getNeatStringHeader() {
        return "MaxAmp     Units    ClipAmp   Units";
    }
}
