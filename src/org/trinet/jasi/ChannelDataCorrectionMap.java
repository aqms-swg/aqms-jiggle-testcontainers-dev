package org.trinet.jasi;
import java.util.*;
import org.trinet.util.*;
import org.trinet.jdbc.table.*;

public class ChannelDataCorrectionMap extends ChannelDataMap implements ChannelDataTypeMapListIF {
/**
* Store in a hashmap keyed by the ChannelIdIF a set of valid dates ranges for the ChannelIdIF
* If an input Date lies within a DateRange (ondate offdate) of the set, cached data object is returned.
* Otherwise, object if a new AbstractChannelData object can be created from the database it is added to the cache
* and returned. Returns null if the data is not recoverable.
* The cached AbstractChannelData object's DateRange and ChannelId are used to compose the key that is mapped to the
* AbstractChannelData object in the hashmap.
*/
    public ChannelDataCorrectionMap() {
      this((ChannelCorrectionDataIF) null);
    }
    public ChannelDataCorrectionMap(ChannelCorrectionDataIF corr) {
      this(DEFAULT_CAPACITY, corr);
    }
    public ChannelDataCorrectionMap(int capacity, ChannelCorrectionDataIF corr) {
      super(capacity, corr);
    }
    public ChannelDataCorrectionMap(Collection data) {
      super(data);
    }
    public ChannelDataCorrectionMap(ChannelDataCorrectionMap data) {
      super(data);
    }

    public void setDefaultDataFactory(ChannelDataIF defaultDataFactory) {
      this.defaultData = (ChannelCorrectionDataIF) defaultDataFactory;
    }

    public String getDefaultType() {
      return ((ChannelCorrectionDataIF) defaultData).getCorrType();
    }

    protected Object makeDateMapHashKey(ChannelIdIF id) {
      return new DateKey(id, getDefaultType());
    }
    protected Object makeDateMapHashKey(ChannelDataIF cd) {
      return new DateKey(cd.getChannelId(), getDefaultType());
    }
    protected Object makeParmMapHashKey(ChannelDataIF cd) {
      return new ParmKey(cd.getChannelId(), cd.getDateRange(), getDefaultType());
    }

    static private class DateKey {
      ChannelIdIF id = null;
      String type = null;

      public DateKey(ChannelIdIF id, String type) {
        this.id = id;
        this.type = type;
      }
      public boolean equals(Object obj) {
        if (obj == this) return true;
        if (! this.getClass().isInstance(obj)) return false;
        DateKey dk = (DateKey) obj;
        return type.equals(dk.type);
      }
      public int hashCode() {
        return (id.hashCode() + type.hashCode());
      }
      public String toString() {
        return id.toDelimitedSeedNameString('.') +"|"+ type;
      }
    }
    static private class ParmKey {
      ChannelIdIF id = null;
      DateRange   dr = null;
      String      type = null;

      public ParmKey(ChannelIdIF id, DateRange dr, String type) {
        this.id = id;
        this.dr = dr;
        this.type = type;
      }
      public boolean equals(Object obj) {
        if (obj == this) return true;
        if (! this.getClass().isInstance(obj)) return false;
        ParmKey pk = (ParmKey) obj;
        return ( id.equals(pk.id) && dr.equals(pk.dr) && type.equals(pk.type) );
      }
      public int hashCode() {
        return ( id.hashCode()+dr.hashCode()+type.hashCode() );
      }
      public String toString() {
        return id.toDelimitedSeedNameString('.') +"|"+ dr.toString() +"|"+ type;
      }
    }
}   // end of ChannelDataDataCorrectionMap class
