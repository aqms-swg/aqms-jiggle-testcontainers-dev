package org.trinet.jasi;
import java.util.*;
import org.trinet.jdbc.table.*;
public interface ChannelDataTypeMapListIF extends ChannelDataMapListIF {
    //public ChannelDataIF get(Channelable chan, java.util.Date date, String type) ;
    //public ChannelDataIF get(ChannelIdIF id, java.util.Date date, String type) ;
    public String getDefaultType();
}
