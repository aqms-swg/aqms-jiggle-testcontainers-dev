package org.trinet.jasi;

import java.util.*;
import java.sql.*;

import org.trinet.jdbc.ResultSetDb;
//import org.trinet.jdbc.StringSQL;
import org.trinet.jdbc.table.ExecuteSQL;
import org.trinet.util.EpochTime;
import org.trinet.util.Utils;

public class ChannelIdLoader implements ChannelIdLoaderIF {

    public static boolean debug = false;

    public static ChannelIdLoaderIF create() {
        return new ChannelIdLoader();
    }

    /**
    * Return a Set of ChannelIdIF objects from those SNCL in the JASI_CONFIG_VIEW table whose ProgId key
    * is mapped to the input name String <i>progName</i> in the applications table and that are in
    * an the currently active configuration.
    **/
    public Set loadChannelConfigFor(String progName) {
        return loadChannelConfigFor(progName, (java.util.Date) null);
    }

    /**
    * Return a Set of ChannelIdIF objects from those SNCL in the JASI_CONFIG_VIEW table whose applications
    * ProgId key is mapped to the input name <i>progName</i> and is valid for the input date.
    **/
    public Set loadChannelConfigFor(String progName, java.util.Date date) {
        return loadChannelConfigFor(progName, date, null);
    }

    /**
    * Return a Set of ChannelIdIF objects from those SNCL in the JASI_CONFIG_VIEW table whose ProgId key
    * is mapped to the input name <i>progName</i> and configuration <i>configStr</i>
    * and is valid for the input date.
    **/
    public Set loadChannelConfigFor(String progName, java.util.Date date, String configStr) {

      Connection conn = DataSource.getConnection();
      if (conn == null) {
        System.err.println (" ERROR ChannelIdLoader : No DataSource is open.");
        return null;
      }

      ArrayList aList = new ArrayList();
      ResultSet rs = null;

      boolean status = true;
      //java.sql.Statement sm = null;
      java.sql.PreparedStatement sm = null;
      boolean haveConfig = false;
      String sql = null;
      try {
        if ( conn == null || conn.isClosed() ) {
          System.err.println (" ERROR ChannelIdLoader: DataSource connection is closed");
          return null;
        }
        //String dateStr = (date == null) ? "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)" : StringSQL.toDATE(date);
        String dateStr = (date == null) ? "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)" : EpochTime.dateToString(date,"yyyy-MM-dd HH:mm:ss");
        //java.sql.Date sqlDate = (date == null) ? new java.sql.Date(System.currentTimeMillis()) : new java.sql.Date(date.getTime());;

        StringBuffer sb = new StringBuffer(256);
        //sb.append("Select net,sta,seedchan,location from JASI_CONFIG_VIEW cc WHERE cc.name = '");
        //sb.append(progName).append("'");
        //if (configStr != null && configStr.trim().length() > 0) { // added 2008/01/21 -aww
            //sb.append(" and cc.config = '").append(configStr).append("'");
        //}
        //// Only if table/view has offdate column attribute -aww  
        //sb.append(" and cc.offdate > ").append(dateStr);
        //sb.append(" and cc.ondate <= ").append(dateStr);
        //sm = conn.createStatement();
        //rs = sm.executeQuery(sb.toString());

        sb.append("SELECT net,sta,seedchan,location FROM JASI_CONFIG_VIEW cc WHERE (cc.name=?)");
        if (configStr != null && configStr.trim().length() > 0) { // added 2008/01/21 -aww
            sb.append(" AND (cc.config =?)");
            haveConfig = true;
        }
        // Only if table/view has offdate column attribute -aww  
        if (date == null) {
          sb.append(" AND ( SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) between cc.ondate AND cc.offdate)");
        }
        else {
          sb.append(" AND (TO_DATE(?,'YYYY-MM-DD HH24:MI:SS') between cc.ondate AND cc.offdate)");
        }

        sql = sb.toString();
        sm = conn.prepareStatement(sql);

        int offset = 1;
        sm.setString(offset++, progName);
        if (haveConfig) sm.setString(offset++, configStr);
        //sm.setDate(offset++, sqlDate);
        if (date != null) sm.setString(offset++, dateStr);
        rs = sm.executeQuery();

        if (rs != null) {
          while ( rs.next() ) {
            aList.add(parseResultSet(rs));
          }
        }
        else if (debug)
          System.out.println(" DEBUG ChannelIdLoader : No JASI_CONFIG_VIEW found for program name = " + progName);
      }
      catch (SQLException ex) {
        System.err.println(ex);
        ex.printStackTrace();
        status = false;
      }
      finally {
          JasiDatabasePropertyList.debugSQL(sql, sm, debug);
          Utils.closeQuietly(rs, sm);
      }

      // db retrieval ok ?
      int count = aList.size();
      if (debug)
          System.out.println("DEBUG ChannelIdLoader : JASI_CONFIG_VIEW parsed channel count = " + count);
      if (! status || count == 0) return null;

      HashSet hs = new HashSet(count);
      for (int idx = 0; idx < count; idx++) {
          hs.add(aList.get(idx));
      }
      if (hs.size() != count)
          System.out.println(" ERROR ChannelIdLoader: JASI_CONFIG_VIEW channel count != set size: " +
                  count + " != " + hs.size() + " for progName: " + progName);
      return hs;
    }

    private static ChannelIdIF parseResultSet(ResultSet rs) {
        int offset = 0;
        ChannelName cn = new ChannelName();
        try {
            cn.setNet(rs.getString(++offset));
            cn.setSta(rs.getString(++offset));
            cn.setSeedchan(rs.getString(++offset));
            cn.setLocation(rs.getString(++offset));
            //if (debug) System.out.println(cn.toString());
        }
        catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }
        return cn;
    }

}
