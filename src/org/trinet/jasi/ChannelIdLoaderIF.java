package org.trinet.jasi;

import java.util.Set;

public interface ChannelIdLoaderIF {
    /** Load the "current" configuration for input program **/
    public Set loadChannelConfigFor(String progName);
    /** Load the configuration active on input date for input program **/
    public Set loadChannelConfigFor(String progName, java.util.Date aDate);
    /** Load the configuration active on input date for input program and configuration**/
    public Set loadChannelConfigFor(String progName, java.util.Date aDate, String config);
}
