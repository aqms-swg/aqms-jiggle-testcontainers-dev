package org.trinet.jasi;

import java.io.*;
import java.util.*;
import javax.swing.event.EventListenerList;

import org.trinet.jiggle.common.LogUtil;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;

import com.isti.util.UtilFns;

//
// aww - 11/25 switched input Channel args in methods to Channelable type
//
public class ChannelList extends ChannelableList {

    public static boolean debug = false;

    public static final String DEFAULT_CHANNEL_CACHE_FILENAME = "channelList.cache";
    private static String cacheFilename = null;

    //protected static final int  DEFAULT_MAP_CAPACITY = 5759;
    //protected HashMap lookupMap;

    public static final int   DEFAULT_MAP_CAPACITY = 4649;
    public static final float DEFAULT_MAP_LOAD_FACTOR = 0.9f;
    private ChannelDataMap lookupMap = null;
    private ListDataStateListener listDataStateListener = null;

/**
   A list of channel objects that can be sorted, etc.<p>
   This class also supports local caching of a channel list which is usually
   faster then reading from a database, especially over slow links. The cache
   contains serialized Channel objects. The smartLoad() method
   attempts to read from a local cache. If it fails it reads from the DataSource
   then writes a cache file for later use.
*/

    public ChannelList() {
      this(getCacheFilename());
    }
    public ChannelList(int initCapacity) {
      super(initCapacity);
    }
    /** Input Collection must contain Channelable object elements or a
     * ClassCastException is thrown. Extracts the Channel object
     * of each Channelable element and adds it to this list.
     * */
    public ChannelList(Collection col) {
      if ( col != null && ! col.isEmpty() ) {
        // Confirm input Collection has Channelable objects (todo: use jdk1.5 generic types)
        // cast, wrong type throws exception  -aww 03/4/2005
        Channelable [] ch = (Channelable []) col.toArray(new Channelable[col.size()]);
        ensureCapacity(ch.length);
        for (int idx = 0; idx < ch.length; idx++) {
           add(ch[idx].getChannelObj(), false);
        }
      }
    }
    public ChannelList(String cacheFilename) {
      super();
      setCacheFilename(cacheFilename);
    }

  /** Register CacheRefreshListener with list */
  private javax.swing.event.EventListenerList cListenerList = null;
  public void addCacheRefreshListener(CacheRefreshListener l) {
    removeCacheRefreshListener(l);
    if (cListenerList == null) cListenerList = new EventListenerList();
    cListenerList.add(CacheRefreshListener.class, l);
  }
  public void removeCacheRefreshListener(CacheRefreshListener l) {
    if (cListenerList == null) return;
    cListenerList.remove(CacheRefreshListener.class, l);
  }
  public void clearCacheRefreshListeners() {
    cListenerList = null;
  }
  public int countCacheRefreshListeners() {
    return (cListenerList == null) ? 0 : cListenerList.getListenerCount();
  }
  public CacheRefreshListener [] getCacheRefreshListeners() {
    return (CacheRefreshListener []) cListenerList.getListeners(CacheRefreshListener.class);
  }
  protected void fireCacheRefreshEvent(int type) {
    if (cListenerList == null || cListenerList.getListenerCount() == 0 ) {
      if (debug) System.out.println("DEBUG: ChannelList fireCacheRefreshEvent, no CacheRefreshListeners, so a no-op");
      return; // bail if no listeners
    }
    else if (debug) 
        System.out.println("DEBUG ChannelList fireCacheRefreshEvent countCacheRefreshListeners: " + ChannelList.this.countCacheRefreshListeners());

    CacheRefreshEvent cEvent = new CacheRefreshEvent(this, type);
    // get listener snapshot before looping notification
    // synchronize getListenerList access if collection is not a simple array
    Object[] listeners = cListenerList.getListenerList();
    // Notify the listeners last added to first.
    for (int i = listeners.length-2; i>=0; i-=2) {
      if (listeners[i] == CacheRefreshListener.class) {
          ((CacheRefreshListener)listeners[i+1]).channelCacheRefresh(cEvent);
      }
    }
  }

    /** Create a HashMap of the list for more efficient lookups by the
     * Channel matching methods. */
    public final void createLookupMap() {
      if (lookupMap == null) {
        // Removed clear below since external classes may be already
        // registered as ListDataStateListeners:
        //if (countListDataStateListeners() > 0) clearListDataStateListeners();
        if (listDataStateListener == null) { // virgin
          listDataStateListener = new ChannelListDataStateListener();
        }
        else { // probably not needed here, but as insurance
          removeListDataStateListener(listDataStateListener);
        }
        // now add listener to notify the map of list modifications
        addListDataStateListener(listDataStateListener);
      }

      // avoid null pointer, constructor expects default factory as first element
      if (this.size() <= 0) this.add(Channel.create());
      lookupMap = new ChannelDataMap(this, DEFAULT_MAP_LOAD_FACTOR);
    }

    public boolean hasLookupMap() { return (lookupMap != null); }

    /*
    public void createLookupMap() {
      lookupMap = new ChannelDataMap(this, DEFAULT_MAP_LOAD_FACTOR);
      lookupMap = new HashMap(DEFAULT_MAP_CAPACITY, DEFAULT_MAP_LOAD_FACTOR);
      for (int idx = 0; idx < count; idx++) {
        Channel channel = (Channel) get(idx);
        lookupMap.put(channel.getChannelName(), channel); // note key doesn't descriminate by dateRange
      }
    }
    */

    /**
     * Returns the first Channel element found similar to that the input.
     * "Similar" means matching the net, sta, seedchan, location and
     * active on the current date.
     * If no entry is found, method returns the input reference.
     */
    public Channel lookUp(Channelable ch) {
      return lookUp(ch, null);
    }
    /** Returns a 'similar' matching Channel if found in the Collection,
     * else returns the input Channelable argument.
     * For faster response method uses collection hashmap if it already
     * exists, otherwise it loops through the list.
     * A 'similar' Channel is one that matches net, sta, seedchan, location
     * and has an active date range that contains the input date.
     * If the input date is null, it uses the current date.
     * @see #createLookupMap()
     * @throws NullPointerException when input Channelable is null
     */
    public Channel lookUp(Channelable ch, java.util.Date date) {
      Channel chan = (lookupMap != null && ! lookupMap.isEmpty()) ?
         findSimilarInMap(ch, date) : findSimilarInList(ch, date);
      /*Could force query datasource for data and append it to this collection:
      if (chan == null && ch.getChannelObj() != null) {
        chan = Channel.create().lookUp(ch.getChannelObj(), date);
        if (chan != chan.getChannelObj()) {
          add(chan); // without listener on map, we would need next line:
          if (lookupMap != null) lookupMap.add(chan);
        }
      }
      */
      // below throws null pointer exception if channelable input is null - aww
      return (chan == null) ? ch.getChannelObj() : chan ;
    }

    /** Returns a Channel matching the input's from the hashmap of this List.
     * "Similar" means matching net, sta, seedchan, location and
     * having an active date range that contains the input date.
     * If the input date is null, it uses the current date.
     * Returns null if no match is found.
     * @see: #equals()
     * */
    public Channel findSimilarInMap(Channelable ch, java.util.Date date) {
        if (ch == null) return null;
        Channel chan = ch.getChannelObj(); // aww 11/25
        if (chan == null) return null;

        if (lookupMap == null) createLookupMap();
        Channel chnl = null;
        if (! lookupMap.isEmpty()) {
            if (date == null) date = new java.util.Date();
            chnl = (Channel) lookupMap.get(chan.getChannelId(), date); // need a date range too!
        }
        /*
        if (debug) {
          String mapChan = (chnl == null) ? "  null" : chnl.toDumpString();
          System.out.println("DEBUG ChannelList findSimilarInMap map chan:\n"+mapChan+
              "\n   versus input lookup chan: \n"+chan.toDumpString());
        }
        */
        return chnl;
    }

    public Channel findExactInMap(Channelable ch) {
        if (ch == null) return null;
        Channel chan = ch.getChannelObj(); // aww 11/25
        if (chan == null) return null;

        if (lookupMap == null) createLookupMap();
        Channel chnl = null;
        if (! lookupMap.isEmpty()) {
            // return exact match of Channelable input object in map collection
            chnl = (Channel) lookupMap.get(chan);
        }
        if (debug) System.out.println("DEBUG findExactInMap map: " +chnl+
                        " input: " + chan + " hash: " + chan.hashCode());
        return chnl;
    }

    /**
     * Returns the Channel element in this list matching the input's name and date range.
     * Returns null if no element is found.
     * @see: Channel.equals()
     */
    public Channel findExactInList(Channelable ch) {
        if (ch == null) return null;
        Channel chan = ch.getChannelObj(); // aww 11/25
        if (chan == null) return null;

        int count = size();
        // Not efficient better to use map hash table
        Channel chnl = null;
        for (int i = 0; i < count; i++) {
          chnl = (Channel) get(i);
          if (chnl.equals(chan)) {
            return chnl;
          }
        }
        return null;
    }
    /**
     * Returns the Channel element in this list whose name is similar to that of the input.
     * "Similar" means matching the net, sta, seedchan, location and active on the current date.
     * If no entry is found, method returns null.
     */
    public Channel findSimilarInList(Channelable chan) {
      return findSimilarInList(chan, null);
    }
    /**
     * Returns the Channel element in this list whose name is similar to that of the input.
     * "Similar" means matching the net, sta, seedchan, location and having an active date
     * range that contains the input date.  If input date is null, it uses current date.
     * If no entry is found, method returns null.
     */
    public Channel findSimilarInList(Channelable ch, java.util.Date date) {
        if (ch == null) return null;
        Channel chan = ch.getChannelObj(); // aww 11/25
        if (chan == null) return null;

        int count = size();
        Channel chnl = null;
        if (date == null) date = new java.util.Date();
        for (int i = 0; i < count; i++) {
          chnl = (Channel) get(i);
// DEBUG if (chnl.getSta().equals("xxx")) {System.out.println("DEBUG ids match:" +(chnl.equalsChannelId((ChannelIdIF) chan))+" dates match:"+(chnl.getDateRange().contains(date)));} //
          if ( chnl.equalsChannelId((ChannelIdIF) chan) ) {
            if (chnl.getDateRange().contains(date)) return chnl;
          }
        }
        return null;
    }

    /** Note: can't override to change returned type */
    public Channel[] getArray() {
       return (Channel[]) toArray(new Channel[size()]);
    }

/*  FILE READ/WRITE NOTES:
    There are three types of files that you can read/write channel list to/from.
    1) ASCII
    2) Binary
    3) Objects
    These were done to compare speeds and space used.
    Results: there is no dramatic speed difference but objects are a bit faster
    to read. There was a scatter of up to 20% in different runs.
    Test read/write of 2876 channels:
    Source          Read    Write    File
                    (sec)   (sec)    Size
    dbase           34.0     ---     ---     (over 10Mbit link)
    ASCII           20.5     4.5     224k
    Binary          20.7     4.7     228k
    Objects         14.8     5.9     650k
*/

/**
* Write serialized version of the channel list to a file.
* The list will be written to a file named with the currently set
* channel cache path filename.
* Default is "channelList.cache" in directory returned by getDefaultPath(),
* usually the user's home directory for an application.
* @see #getCacheFilename()
* @see #getDefaultPath()
*/
    public boolean writeToCache() {
      return writeToCache(getCacheFilename());
    }

/**
* Write serialized version of the channel list to a file.
* The filename should include the path, otherwise, it's
* written into the directory from which application
* was invoked.
*/
   public boolean writeToCache(String filename) {
      boolean status = true;
      ObjectOutputStream stream = null;
      //could null map and listener to force a map rebuild by user after reload ?
      //lookupMap = null;
      //listDataStateListener = null;
      //
      //Destroy listener list, to purge non-serializable objects (like OpenMap objects)
      clearListDataStateListeners(); // 02/06/2006 aww
      //Add this instance's internal listener (needed for lookupMap synch) to a new list: 
      addListDataStateListener(listDataStateListener); // 02/06/2006 aww
      // Now serialize contents:
      try {
        stream = new ObjectOutputStream(new FileOutputStream(filename));
        stream.writeObject( (Object) this );
      } catch (IOException ex) {
        ex.printStackTrace();
        status = false ;
      } finally {
        if (stream != null) {
          try {
            stream.flush();
            stream.close();
          } catch (Exception ex2) { }
        }
      }
      return status;
    }
/**
* Write serialized version of the channel list to a file in background thread.
* The list will be written to a file named with the currently set
* channel cache filename.
* Default is file "channelList.cache" located in the directory returned by
* getDefaultPath(), usually the user's home directory for an application.
* @see #getCacheFilename()
*/
    public boolean writeToCacheInBackground() {
      new CacheWriterThread(this);
      return true;
    }
/** Reads channel list from the data source and writes it to the cache file
 *  from a background thread. This keeps the cache current with Channels
 *  currently active in the data source. */
    public void refreshCache() {
      new CacheRefreshThread(null);
    }
/** Reads channel list from the data source and writes it to the cache file
 *  from a background thread.
 *  This written cache contains only those Channels active on the input date. */
    public void refreshCache(java.util.Date date) {
      new CacheRefreshThread(date);
    }
/** Reads channel list from the data source and writes it to the cache file
 *  from a background thread.
 *  This written cache contains only those Channels active on the input date
 *  and associated with the given name. 
 *  */
    public void refreshCache(java.util.Date date, String name) {
      new CacheRefreshThread(date, name);
    }
    //////// INNER CLASS
/**
 * This is a one-shot, start-on-instantiation thread that rereads the datasource and
 * rewrites the cache file in background.
 * NOTE that it will NOT replace the in-memory channel list of the calling program.
 */
// 5/15/07 DDG - added ability to specify named list, fixed bug(?) in date option
    private class CacheRefreshThread implements Runnable {

        private final java.util.Date date; // data date for active Channels
        private final String name;
    
        public CacheRefreshThread(java.util.Date date) {     // constructor - also starts thread
            this(date, (String) null);
        }
 
        public CacheRefreshThread(java.util.Date date, String name) {     // constructor - also starts thread
            this.date = date;  // added 5/15/07 DDG - was a bug?        
            this.name = name;
            Thread thread = new Thread(this);
            System.out.println("INFO: Cache refresh is starting");
            thread.start();
        }

        @Override
        public void run() {
           BenchMark bm = new BenchMark();
           ChannelList chanList;
           
           // Call the right method for the combo of name/date
           // Could simplify if methods handle null arg correctly
           // but I don't think we can count on that.
           if (name == null) {
                   if (date == null) {
                       chanList = ChannelList.readCurrentList();
                   } else {
                       chanList = ChannelList.readList(date);
                   }        
           } else  {   // name NOT null
                   if (date == null) {
                       chanList = ChannelList.readCurrentListByName(name);   
                   } else {
                       chanList = ChannelList.readListByName(name, date);
                   }
           }
  
           if (chanList != null && !chanList.isEmpty()) {
               boolean status = chanList.writeToCache();
               bm.printTimeStamp("CacheRefreshThread finished. Total channel count= "+chanList.size() + " name: " + name + " active date: " + date);
               fireCacheRefreshEvent((status) ? CacheRefreshEvent.SUCCESS : CacheRefreshEvent.FAILURE);
           } else {
               bm.printTimeStamp("CacheRefreshThread finished. List is NULL or empty, no channels,");
               fireCacheRefreshEvent(CacheRefreshEvent.FAILURE);
           }
        }
    } // end of CacheRefreshThread class

/**
 * This is a one-shot thread that writes cache file in background.
 */
private class CacheWriterThread implements Runnable {

    ChannelList list;
//    public Thread thread;

    // constructor - also starts thread
    public CacheWriterThread(ChannelList list ) {
        this.list = list;
        Thread thread = new Thread(this);
        thread.start();
    }

    public void run() {
        list.writeToCache();
    }
} // end of CacheWriterThread class

/**
* Read serialized version of the channel list from a file.
* The list will read from a file named with the currently set
* channel cache filename.
* Default is "channelList.cache" located in the directory returned by
* getDefaultPath(), usually the user's home directory for an application.
* Returns empty ChannelList if it fails.<p>
* The Java loader checks "version" number of the class so if the ChannelList class
* has been recompiled since the cache was written it will throw an exception and
* not load it.
* @see #getCacheFilename()
*/
    public static ChannelList readFromCache() {
        return readFromCache(getCacheFilename());
    }

/**
* Read serialized version of the channel list from a file with the input filename.
* The input filename should include the path, otherwise it looks for the file
* in the local directory of the application window invoking the method.
* Returns empty ChannelList if it fails. <p>
* The Java loader checks "version" number of the class so if the ChannelList class
* has been recompiled since the cache was written it will throw an exception and
* not load it.<p>
* Sets the current ChannelList class cache filename to the input String.
*/
    public static ChannelList readFromCache(String filename) {
      ChannelList cl = null;
      FileInputStream instream = null;
      ObjectInputStream stream = null;
      setCacheFilename(filename);
      try { // if input filename is null, use default file location - aww
          filename = getCacheFilename();
          instream = new FileInputStream(filename);
          stream = new ObjectInputStream(instream);
          cl = (ChannelList) stream.readObject() ;
      } catch (FileNotFoundException ex) {
          LogUtil.error("Unable to find ChannelList cache file named: " + getCacheFilename());
      } catch (InvalidClassException ex) {
          // this happens if ChannelList has been recompiled since last cache write
          LogUtil.error("Version of cached ChannelList file object predates its class byte code recompilation time: " + filename);
      } catch (IOException ex) {
          LogUtil.error("Error reading ChannelList cache file named: " + filename);
      } catch (Exception ex) {
          ex.printStackTrace();
      } finally {
          UtilFns.closeQuietly(stream);
          UtilFns.closeQuietly(instream);
      }
      return (cl == null) ? new ChannelList() : cl;
    }

    /** Returns the class ChannelList cache filename to be used
     * for any input/output of a serialized ChannelList instance.
     * */
    public static String getCacheFilename() {
      if (cacheFilename == null) setCacheFilename(getFullDefaultCacheFilename());
      return cacheFilename;
    }
    /** Set filename of cache file. Name should include the full path, otherwise
     * the local directory context of the invoking instance is used. */
    public static void setCacheFilename(String filename) {
      cacheFilename = filename;
    }

    /** Concatenates path+FILE_SEP+filename, sets the cache filename to resulting String.*/
// Is this static method used by any oracle server scripts? It's not used by any other java code - aww 03/04
    public static void setCacheFilename(String path, String fileName) {
      StringBuffer sb = new StringBuffer(path.length() + 2 + fileName.length());
      sb.append(path).append(GenericPropertyList.FILE_SEP).append(fileName);
      setCacheFilename(sb.toString());
    }

  /** Returns the default path for the user's ChannelList cache.
   * System property "JIGGLE_USER_HOMEDIR" if defined, else "user.home".
   * */
    public static String getDefaultPath() {
      return  // why should it reference Jiggle dir, how about JASI_USER_HOMEDIR? - aww ?
        System.getProperty(
                  "JIGGLE_USER_HOMEDIR",
                   System.getProperty("user.home",".")
               );
    }

    /** Returns the default ChannelList cache filename ("channelList.cache").
     * */
    public static String getDefaultCacheFilename() {
      return DEFAULT_CHANNEL_CACHE_FILENAME;
    }
    /** Returns the default ChannelList cache path file name.
     * Concatenates the String returned by the System property
     * JIGGLE_USER_HOMEDIR if defined, otherwise, the user's home
     * directory String, file separator concatenated with default
     * channel cache filename ("channelList.cache").
     * @see #getDefaultPath()
     * @see #getDefaultCacheFilename()
     * */
    public static String getFullDefaultCacheFilename() {
      StringBuffer sb = new StringBuffer(132);
      sb.append(getDefaultPath());
      sb.append(GenericPropertyList.FILE_SEP);
      sb.append(getDefaultCacheFilename());
      return sb.toString();
    }

    /** Trys reading the local cached channel list, if that fails
    * reads currently active channels from the data source
    * then creates the ChannelList cache file. */
    public static ChannelList smartLoad() {
        return smartLoad(null);
    }
    /** Trys reading the local cached channel list, if that fails
     * reads currently active channels from the data source
     * then creates the ChannelList cache file. */
    public static ChannelList smartLoad(java.util.Date date) {
        return smartLoad(date, null);
    }    
    /** Trys reading the local cached channel list, if that fails
    * reads channels active on the input date from the data source
    * then creates the ChannelList cache file. */
    public static ChannelList smartLoad(java.util.Date date, String listName) { // Added 5/28/08 - DDG : for RCG
      ChannelList chanList = ChannelList.readFromCache();
      // cache read failed, read channels active on date from source
      if (chanList.isEmpty()) {
        chanList = ChannelList.readListByName(listName, date);
          if (chanList != null && !chanList.isEmpty()) {
              chanList.writeToCache(); // write cache file for future use
          }
          else {
              System.out.println("WARNING! ChannelList.smartLoad("+date+","+listName+") cache is empty, check database/candidate list name.");
          }
      }
      return chanList;
    }

    /** Return count of all channels in data source. */
    public static int getCurrentCount() {
        return Channel.create().getCurrentCount();
    }

    /** Return count of all channels in data source. */
    public static int getCount(java.util.Date date) {
        return Channel.create().getCount(date);
    }

    public static void readOnlyChannelLatLonZ() {
        //Channel.create().setAutoLoadingOfAssocDataOff();
        Channel.setLoadOnlyLatLonZ(); // same as above with text message
    }

    /**
     * Return Collection of currently active Channels from the default
     * DataSource.
     */
    public static ChannelList readCurrentList() {
        return Channel.create().readList();
    }
    public static ChannelList readCurrentListByName(String name) {
        return Channel.create().readListByName(name, (java.util.Date) null);
    }
    /**
     * Return Collection of Channels that were active on the given date.
     * Uses the default DataSource.
     */
    public static ChannelList readList(java.util.Date date) {
        return Channel.create().readList(date);
    }
    
    public static ChannelList readListByName(String name, java.util.Date date) {
        return Channel.create().readListByName(name, date);
    }

    /** Return a list of currently active channels that match the list of
     *  component types given in
    * the array of strings. The comp is compared to the SEEDCHAN field in the NCDC
    * schema.<p>
    *
    * SQL wildcards are allowed as follows:<br>
    * "%" match any number of characters<br>
    * "_" match one character.<p>
    *
    * For example "H%" would match HHZ, HHN, HHE, HLZ, HLN & HLE. "H_" wouldn't
    * match any of these but "H__" would match them all. "_L_" would match
    * all components with "L" in the middle of three charcters (low gains).
    * The ANSI SQL wildcards [] and ^ are not supported by Oracle.*/
    public static ChannelList getByComponent(String[] compList) {
      return Channel.create().getByComponent(compList);
    }

    /** Return a list of channels that were active on the given date and
    *  that match the list of component types given in
    * the array of strings. The comp is compared to the SEEDCHAN field in the NCDC
    * schema.<p>
    *
    * SQL wildcards are allowed as follows:<br>
    * "%" match any number of characters<br>
    * "_" match one character.<p>
    *
    * For example "H%" would match HHZ, HHN, HHE, HLZ, HLN & HLE. "H_" wouldn't
    * match any of these but "H__" would match them all. "_L_" would match
    * all components with "L" in the middle of three charcters (low gains).
    * The ANSI SQL wildcards [] and ^ are not supported by Oracle.*/
    public static ChannelList getByComponent(String[] compList,  java.sql.Date date) {
      return Channel.create().getByComponent(compList, date);
    }

    public String toDumpString() {
        int count = size();
        StringBuffer sb = new StringBuffer((count+1)*1024);
        if (count == 0) {
          sb.append("No channels found.");
        } else {
          for (int i = 0; i < count; i++) {
            sb.append(((Channel)get(i)).toDumpString()).append("\n");
          }
        }
        return sb.toString();
    }

/** Print the ChannelList channel dump String description directly to standard output. */
    public void printDumpString() {
      System.out.println(toDumpString());
    }

/**
 * For checking integrity.
 * Writes ChannelList channel dump String descriptions to the specified filename.
 * Checks for any existing internal lookup hashmap name inconsistent with list element.
 */
    public void writeToFile(String fileName) {
        FileWriter w = null;
        try {
          w = new FileWriter(fileName);
          int count = size();
          if (count == 0) {
            w.write("No channels found.\n");
          } else {
            w.write(count + " channels found in list.\n");
            if (lookupMap != null) {
              w.write("Has Channel lookupMap to check, size:");
              w.write(String.valueOf(lookupMap.size()));
              w.write("\n");
            }
            else w.write("No Channel lookupMap\n");

            Channel ch = null;
            Channel ch2 = null;
            String dump = null;
            String dump2 = null;
            // sort it first
            sortByStation();
            //
            for (int i = 0; i < count; i++) {
              ch = (Channel) get(i);
              dump = ch.toDumpString();
              w.write(dump);
              w.write("\n");

              if (lookupMap != null) {
                ch2 = (Channel) lookupMap.get(ch);
                if (ch2 != null) {
                  dump2 = ch2.toDumpString();
                  if (! dump2.equals(dump)) {
                    w.write("ERROR ! lookupMap Channel Mismatch:\n");
                    w.write(dump2);
                    w.write("\n");
                  }
                }
                else {
                  w.write("ERROR ! lookupMap Channel Missing !\n");
                }
              }

            }
          }
        }
        catch (IOException ex) {
          System.err.println("Error creating dumping ChannelList to file: " + fileName);
          ex.printStackTrace();
        }
        finally {
          if (w != null) {
            try {
              w.flush();
              w.close();
            }
            catch (IOException ex2) { }
          }
        }
    }

// WARNING - any modification of ChannelList contents through ArrayList interface
// methods invalidates a pre-existing lookup map, unless this implemented listener
// interface has methods that properly update lookupMap contents as well !
  private class ChannelListDataStateListener implements ListDataStateListener {
    public void intervalAdded(ListDataStateEvent e) {
      if (lookupMap == null) return;
      if (debug) System.out.println("DEBUG ChannelListDataStateListener intervalAdded");
      int first = e.getIndex0();
      int last = e.getIndex1();
      if (first > -1 && last >= first) {
        for (int idx = 0; idx<=last; idx++) {
          lookupMap.add((ChannelDataIF)ChannelList.this.get(idx), false);
        }
        return;
      }
      Object[] theAdded = (Object []) e.getStateChange().getValue();
      for (int idx = 0; idx<=theAdded.length; idx++) {
        lookupMap.add((ChannelDataIF)theAdded[idx], false);
      }
    }
    public void intervalRemoved(ListDataStateEvent e) {
      if (lookupMap == null) return;
      if (debug) System.out.println("DEBUG ChannelListDataStateListener intervalRemoved");
      int first = e.getIndex0();
      int last = e.getIndex1();
      Object data = e.getStateChange().getValue();
      // have to use the "removed" element returned in the StateChange
      if (first > -1 && last == first) {
        lookupMap.remove((ChannelDataIF)data);
        return;
      }
      // assume array of removed elements scattered in list:
      Object [] theRemoved = (Object []) data;
      for (int idx = 0; idx<=theRemoved.length; idx++) {
        lookupMap.remove((ChannelDataIF)theRemoved[idx]);
      }
    }
    public void contentsChanged(ListDataStateEvent e) {
      if (lookupMap == null) return;
      if (debug) System.out.println("DEBUG ChannelListDataStateListener contentsChanged");
      int first = e.getIndex0();
      int last = e.getIndex1();
      if (first > -1 && last == first) { // probably set() fired event
        lookupMap.add((ChannelDataIF) ChannelList.this.get(first), false);
      }
      else { // don't know what all happened so punt here
        createLookupMap();
      }
    }

    public void stateChanged(ListDataStateEvent e) {
      if (lookupMap == null) return;
      if (debug) System.out.println("DEBUG ChannelListDataStateListener stateChanged no-op");
    }
    public void orderChanged(ListDataStateEvent e) {
      if (lookupMap == null) return;
      if (debug) System.out.println("DEBUG ChannelListDataStateListener orderChanged no-op");
    }

  } //end of ChannelListDataStateListener class


/*
 static public final class Tester {
    public static void main(String args[]) {

      if (args.length < 2) {
        System.out.println("Main args: User Passwd [Host (serveri.gps.caltech.edu)] [DbName (databasei)] [x=dump]");
        System.exit(0);
      }

      String user   = args[0];
      String passwd = args[1];
      String host = (args.length > 2) ? args[2] : "serverc2.gps.caltech.edu";
      String dbname = (args.length > 3) ? args[3] : "databasear";

      String url = "jdbc:"+AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL+":@"+host+":"+AbstractSQLDataSource.DEFAULT_DS_PORT+":"+dbname;
      String driver = AbstractSQLDataSource.DEFAULT_DS_DRIVER;
      System.out.println("ChannelListMain: making connection to url: " + url);
      DataSource ds = new DataSource (url, driver, user, passwd);    // make connection
      System.out.println("ChannelListMain ds : " + ds.describeConnection());

      int count = 0;
      ChannelList cl  = null;
      String [] compList = {"HH_", "HL_", "EH_", "EL_"};
      java.util.Date date = EpochTime.stringToDate("2002-01-01:00:00:00.000");
      LatLonZ latLonZ = new LatLonZ(34.5,-118.5,-5.); // 5 km depth
      //Channel chan = Channel.create().setChannelName("CI", "PAS", "EHZ", "01");
      // Note: location element matching in channel name might be "switched" off
      cl = new ChannelList();
      Channel chan = Channel.create().setChannelName("NC", "MMI", "EHZ", "01");
      cl.add(chan);
      Channel chan2 = Channel.create().setChannelName("NC", "MMI", "EHH", "01");
      cl.add(chan2);
      chan2 = Channel.create().setChannelName("NC", "MMI", "EHE", "01");
      cl.add(chan2);
      chan2 = Channel.create().setChannelName("NC", "MMI", "EHG", "01");
      //cl.add(chan2);
      System.out.println("MasterChannel list is: \n" + cl); // 

      System.out.println("------------------------------------------------");
      ChannelableSetList csl = new ChannelableSetList();
      csl.add(chan2);
      System.out.println("add EHG no master csl:\n" + csl.toString()); //
      System.out.println("------------------------------------------------");
      csl.clear();
      csl.add(chan);
      System.out.println("add EHZ no master csl:\n" + csl.toString()); //
      System.out.println("------------------------------------------------");

      csl.setMasterList(cl);
      csl.clear();
      csl.add(chan2);
      System.out.println("add EHG master csl:\n" + csl.toString()); //
      System.out.println("------------------------------------------------");
      csl.clear();
      csl.add(chan);
      System.out.println("add EHZ master csl:\n" + csl.toString()); //
      System.out.println("------------------------------------------------");

      if (true) { System.exit(0); }

      BenchMark bm  = new BenchMark();

      count = ChannelList.getCurrentCount();
      //count = ChannelList.getCount(date);
      bm.print("ChannelListMain: Channels count = " + count);

      chan.setAutoLoadingOfAssocDataOff(); // don't lookup corrections etc. just lat,lon,z
      bm.reset();
      cl = ChannelList.readCurrentList(); // should be quick < 30 secs
      bm.print("ChannelListMain: Done list loading WITHOUT AssocData (gains, corrs)");

      System.out.println("\nChannelListMain: Loading list WITH AssocData (gains, corrs)");
      System.out.println(" queries with AssocData table joins may take 8 minutes ...");
      chan.setAutoLoadingOfAssocDataOn();
      bm.reset();
      cl = ChannelList.readCurrentList(); // not quick loading takes 8 or more minutes
      bm.print("ChannelListMain: Done list loading with ASSOC");
      System.out.println ("ChannelListMain: Loaded "+cl.size()+
          " current channels, getCurrentCount() = " + count);

      // Alternatives to ask for list by specific date
      //cl  = ChannelList.readList(date);
      //cl  = ChannelList.getByComponent(compList, date);
      //cl  = ChannelList.getByComponent(compList);

      // test object serialization out
      cl.setCacheFilename("testChannelCache");

      System.out.println("ChannelListMain: Test writing to testChannelCache");
      bm.reset();
      boolean retVal = cl.writeToCache();
      bm.print( "ChannelListMain: Write cache = " + retVal + " size = "+ cl.size());

      // test object serialization in
      cl.setCacheFilename("testChannelCache");
      System.out.println("ChannelListMain: Test reading from testChannelCache");
      bm.reset();
      cl = ChannelList.readFromCache();
      bm.print( "ChannelListMain: Read channelList from cache = "+ cl.size());

      // full diagnostic ASCII dump of list data
      if (args.length > 4) {
        System.out.println("ChannelListMain: dumping channel data to testChanneList.dump");
        cl.writeToFile("testChannelList.dump");
      }

      bm.reset();
      cl.distanceSort(latLonZ);
      bm.print("ChannelListMain: Done distanceSort(latLonZ)");

      bm.reset();
      double gap = cl.getMaximumAzimuthalGap();
      bm.print("ChannelListMain: Done getMaximumAzimuthalGap() = " + gap);

      // List test data channel as configured above
      System.out.println("ChannelListMain test lookup channel: " + chan.toDumpString());

      bm.reset();
      Channel ch =  cl.findExactInList(chan);
      bm.print("ChannelListMain: Done findExactInList w/o dates");
      System.out.println("ChannelListMain : FOUND false = " + (ch != null));
      if (ch != null) {
        System.out.println("ChannelListMain: error FOUND chan w/o dates, it should be null");
        System.out.println(ch.toDumpString());
      }

      bm.reset();
      ch = cl.findSimilarInList(chan);
      bm.print("ChannelListMain: Done findSimilarInList");
      System.out.println("ChannelListMain: FOUND true = " + (ch != null));
      if (ch != null) System.out.println(ch.toDumpString());
      else {
        System.out.println("ChannelListMain: error NOT FOUND, but it should match input chan: ");
        System.out.println(chan.toDumpString());
        System.out.println("ABORTING...  Check your tables to see if test channel exists for date!");
        ds.close();
        System.exit(0);
      }

      // To get exact match, force date range agreement
      chan.setDateRange(ch.getDateRange());
      bm.reset();
      ch =  cl.findExactInList(chan); // should be "true"
      bm.print("ChannelListMain: Done list exact with dates");
      System.out.println("ChannelListMain: FOUND true = " + (ch != null));
      if (ch != null) System.out.println(ch.toDumpString());
      else {
        System.out.println("ChannelListMain: error NOT FOUND, but it should match input chan: ");
        System.out.println(chan.toDumpString());
      }

      bm.reset();
      cl.createLookupMap();
      bm.print("ChannelListMain: Done createLookupMap ");

      bm.reset();
      ch = cl.findSimilarInMap(chan, null); // same id active on current date
      bm.print("ChannelListMain: Done findSimilarInMap");
      System.out.println("ChannelListMain: FOUND true = " + (ch != null));
      if (ch != null) System.out.println(ch.toDumpString());
      else {
        System.out.println("ChannelListMain: error NOT FOUND, but it should match input chan: ");
        System.out.println(chan.toDumpString());
        System.out.println("ABORTING...  Check your tables to see if test channel exists for date!");
        ds.close();
        System.exit(0);
      }

      chan2 = ch; // ch not null, so should have active dates

      bm.reset();
      ch = cl.findExactInMap(chan2); // should be "exact" from above
      bm.print("ChannelListMain: Done findExactInMap");
      System.out.println("ChannelListMain: FOUND true = " + (ch != null));
      if (ch != null) System.out.println(ch.toDumpString());
      else {
        System.out.println("ChannelListMain: error NOT FOUND, but it should match input chan: ");
        System.out.println(chan2.toDumpString());
      }

      // Make a new channel list just by selected components

      System.out.println("\nChannelListMain: Making a new channelList of current data for only selected components");
      compList = new String [] {"HH_", "EL_"};
      bm.reset();
      cl = ChannelList.getByComponent(compList);
      bm.print("ChannelListMain: db channelList retrieval completed for getByComponent()");

      // Test ChannelListDataListener for list map add,remove
      bm.reset();
      cl.createLookupMap();
      bm.print("ChannelListMain: Done createLookupMap ");

      // Use test data channel as configured from above
      System.out.println("ChannelListMain test lookup channel for map test: " + chan2.toDumpString());
      // Now do map probe for test channel
      ch = cl.findExactInMap(chan2); // not in component list
      System.out.println("ChannelListMain: map FOUND before add(ch)   false = " + (ch != null));
      if (ch != null) {
        System.out.println("ChannelListMain: error FOUND before add to map , it should be null");
        System.out.println(ch.toDumpString());
      }

      cl.add(chan2); // force add to map
      ch = cl.findExactInMap(chan2); // should find it in map
      System.out.println("ChannelListMain: map FOUND after  add(ch)   true  = " + (ch != null));

      cl.remove(chan2); // remove from map
      ch = cl.findExactInMap(chan2); // should not be found in map
      System.out.println("ChannelListMain: map FOUND after remove(ch) false = " + (ch != null));
      bm.print("ChannelListMain: Done ChannelListDataListener add,remove to map test");
      if (ch != null) {
        System.out.println("ChannelListMain: error FOUND after remove from map, it should be null");
        System.out.println(ch.toDumpString());
      }

      ds.close(); // close connection resource
      System.exit(0);

    } // end of main
  } // end of Tester
*/
}  // end of ChannelList
