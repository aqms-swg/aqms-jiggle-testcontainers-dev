//TODO: Need separate out ChannelFormatter class with static String methods
// also a separate a ChannelReader/ChannelWriter class(s) for the binary/ascii i/o caching
package org.trinet.jasi;

import java.sql.*;
import java.util.*;
import java.io.*;

import org.trinet.jdbc.*;
import org.trinet.jdbc.table.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;


/**
 * ChannelLocation object. Contains a ChannelName member which includes
 * descriptive name information for the channel  its location (latlonz). <p>
 * @See: org.trinet.jasi.ChannelName
 */

public abstract class ChannelLocation extends AbstractChannelData
   implements java.io.Serializable {
   //AuthChannelIdIF,

    static boolean debug = false;


    /** Channel name description identifier */
    { channelId = new ChannelName(); }

    /** Location of the sensor. Note that 'z' is in km with down negative. */
    public LatLonZ latlonz = new LatLonZ();

    /** Emplacement depth in meters. Zero unless this is a downhole sensor. */
    public DataDouble depth = new DataDouble(0.0);

    //
    // Combine dist,hdist,azimuth into DistanceAzimuthBearing(Elevation) paired with
    // the latlonZ - aww
    //
    /** Distance of this site from the last point for which calcDistance was called.
    * Usually it the associated solution in km. Used for distance sorting, etc. Defaults to
    * DistanceBearingIF.NULL_DIST so channels with unknown distances show up at the END of a list.
    * This is the true distance taking into account depth and elevation. */
    public DataDouble  dist = new DataDouble(DistanceBearingIF.NULL_DIST);
    /** Horizontal distance of this channel.*/
    public DataDouble hdist = new DataDouble(DistanceBearingIF.NULL_DIST);
    /** Azimuth from the site to the associated Solution. Degrees
        clockwise from north.*/
    public DataDouble azimuth = new DataDouble();

    /**
     * Factory Method: This is how you instantiate a jasi object. You do
     * NOT use a constructor. This is so that this "factory" method can create
     * the type of object that is appropriate for your site's database.
     * If no type is specifed it creates a ChannelLocation object of the DEFAULT type.<br>
     * @See: JasiObject
     */
    public static final ChannelLocation create() {
      return create(DEFAULT);
    }

    /**
     * Instantiate an object of this type. You do NOT use a constructor.
     * This "factory" method creates various concrete implementations.
     * The argument is an integer implementation type.<br>
     * @See: JasiObject
     */
    public static final ChannelLocation create(int schemaType) {
      return create(JasiFactory.suffix[schemaType]);
    }

    /**
     * Instantiate an object of this type. You do NOT use a constructor.
     * This "factory" method creates various concrete implementations.
     * The argument is as 2-char implementation suffix.<br>
     * @See: JasiObject
     */
    public static final ChannelLocation create(String suffix) {
      return (ChannelLocation) JasiObject.newInstance("org.trinet.jasi.ChannelLocation", suffix);
    }

    /** Return the ChannelName object of this ChannelLocation */
    public ChannelName getChannelName() {
        //return this;
        return (ChannelName) channelId;
    }
    /** Set this channel's name.
     * The name attributes are set to those of the input.
    *  Returns this ChannelLocation so you can construct with call like:
    *  ChannelLocation chan = Channel.create().setChannelName("CI", "COK", "EHZ", "01");
    */

    public ChannelLocation setChannelName(ChannelName cname) {
      //channelId = (ChannelName) cname.clone(); // why create another object?
      ((ChannelName) channelId).copy(cname);
      return this;
    }
    public ChannelLocation setChannelName(String net, String sta, String seedchan, String location) {
      return setChannelName(net, sta, seedchan, location, null, null, null, null);
    }
    public ChannelLocation setChannelName(String net, String sta, String seedchan, String location,
                    String auth, String subsource) {
      return setChannelName(net, sta, seedchan, location, auth, subsource, null, null);
    }
    public ChannelLocation setChannelName(String net, String sta, String seedchan, String location,
                    String auth, String subsource, String comp, String channelsrc) {
      if (channelId == null) channelId =
                        new ChannelName(net, sta, seedchan, location, auth, subsource, comp, channelsrc);
      else ((ChannelName)channelId).set(net, sta, seedchan, location, auth, subsource, comp, channelsrc);
      return this;
    }

/*
    public String getSta() {
        return channelId.getSta();
    }
    public String getNet() {
        return channelId.getNet();
    }
    public String getSeedchan() {
        return channelId.getSeedchan();
    }
    public String getLocation() {
        return channelId.getLocation();
    }
    public String getChannel() {
        return channelId.getChannel();
    }
    public String getChannelsrc() {
        return channelId.getChannelsrc();
    }
    public String getAuth() {
        return ((AuthChannelIdIF) channelId).getAuth();
    }
    public String getSubsource() {
        return ((AuthChannelIdIF) channelId).getSubsource();
    }
    public void setSta(String sta) {
        channelId.setSta(sta);
    }
    public void setNet(String net) {
        channelId.setNet(net);
    }
    public void setLocation(String location) {
        channelId.setLocation(location);
    }
    public void setChannel(String channel) {
        channelId.setChannel(channel);
    }
    public void setSeedchan(String seedchan) {
        channelId.setSeedchan(seedchan);
    }
    public void setChannelsrc(String channelsrc) {
        channelId.setChannelsrc(channelsrc);
    }
    public void setAuth(String auth) {
        ((AuthChannelIdIF) channelId).setAuth(auth);
    }
    public void setSubsource(String subsource) {
        ((AuthChannelIdIF) channelId).setSubsource(subsource);
    }
*/
///////////////////////////////////////////////////////////////////////////////////

    /** Calculate and set both epicentral (horizontal) distance and true (hypocentral)
     * distance of this ChannelLocation from the given location in km. Returns hypocentral
     * distance. Also sets azimuth.
     * Note: this is better than setDistance() or setHorizontalDistance() because
     * it does both and insures they are consistent. If the ChannelLocation's LatLonZ or
     * the LatLonZ in the argument is null, both distances are set to Channel.NULL_DIST.
     * */
    public double calcDistance(GeoidalLatLonZ loc) {
        LatLonZ llz = (LatLonZ) loc.getLatLonZ().clone();
        if (llz == null || llz.isNull() || !hasLatLonZ()) {  // lat/lon unknown
              setDistance(DistanceBearingIF.NULL_DIST); // so they'll be at END of a sorted list
              setHorizontalDistance(DistanceBearingIF.NULL_DIST);
              azimuth.setNull(true); // reset azimuth null
        } else {
              // do we need IF method to test for depth vs. elevation sign - aww?
              setDistance(this.latlonz.distanceFrom(llz));
              setHorizontalDistance(this.latlonz.horizontalDistanceFrom(llz));
              //setAzimuth(this.latlonz.azimuthTo(llz)); // SEAZ removed 06/11/2004
              //setAzimuth(this.latlonz.azimuthFrom(llz)); // ESAZ -aww like below
              setAzimuth(llz.azimuthTo(this.latlonz)); // ESAZ -aww like hypo2000 06/11/2004
        }
        return getDistance();
    }

    /** Set hypocentral distance ONLY. NOTE: calcDistance(GeoidalLatLonZ) is prefered because
    * it sets both hypocentral and horizontal distance. */
    public void setDistance(double distance) {
      dist.setValue(distance);
    }
    public double getDistance() {
      return dist.doubleValue();
    }
    /** Set horizontal distance ONLY. NOTE: calcDistance(GeoidalLatLonZ) is prefered because
    * it sets both hypocentral and horizontal distance. */
    public void setHorizontalDistance(double distance) {
      hdist.setValue(distance);
    }
    public double getHorizontalDistance() {
      return hdist.doubleValue();
    }
    public double getVerticalDistance() {
      double distance = getDistance();
      double horizDistance = getHorizontalDistance();
      return (distance == DistanceBearingIF.NULL_DIST || horizDistance == DistanceBearingIF.NULL_DIST) ?
         DistanceBearingIF.NULL_DIST : Math.sqrt((Math.pow(distance,2)-Math.pow(horizDistance,2)));
    }

    public void setAzimuth(double az) {
      azimuth.setValue(az);
    }
    public double getAzimuth() {
      return (azimuth == null || azimuth.isNull() ||
              Double.isNaN(azimuth.doubleValue()) ) ? 0. : azimuth.doubleValue();
    }

     /** Return true if the ChannelLocation has a vaild LatLonZ, false if not. */
     public boolean hasLatLonZ () {
        return  !(this.latlonz == null || this.latlonz.isNull()) ;
     }

    /**
     * Make a "deep" copy, except for corrections in map.
     */
    public boolean copy(ChannelDataIF cd) {
      //if (cd == null || ! (cd instanceof ChannelLocation)) return false;
      return (copy((ChannelLocation) cd) != null); 
    }
    public ChannelLocation copy(ChannelLocation cl) {
        if (cl == this) return this;
        ((ChannelName) channelId).copy(cl.getChannelName());
        latlonz.copy(cl.latlonz);
        depth   = (DataDouble) cl.depth.clone();
        dist    = (DataDouble) cl.dist.clone();
        azimuth = (DataDouble) cl.azimuth.clone();
        return this;
    }

    public Object clone() {
        ChannelLocation cl = (ChannelLocation) super.clone();
        cl.channelId = (ChannelName) this.channelId.clone();
        cl.latlonz = (LatLonZ) latlonz.clone();
        cl.depth   = (DataDouble) depth.clone();
        cl.dist    = (DataDouble) dist.clone();
        cl.azimuth = (DataDouble) azimuth.clone();
        return cl;
    }

    /**
     * Case sensitive compare. Returns true if sta, net and seedchannel are
     * equal. Does not check the other fields.  The "extra" fields like
     * 'channel' and 'location' are populated in some data sources and not
     * others.
     */
    //
    //how about public boolean equalsName(ChannelLocation x) { }
    //
    public boolean equals(Object x) {
        if (this == x) return true;
        else if ( x == null || ! (x instanceof ChannelLocation)) return false;
        ChannelLocation cl = (ChannelLocation) x;
        return(this.channelId.equals(cl.getChannelId()));
    }

    /**
     * Case insensitive compare. Returns true if sta, net, and  
     * seedchan and location attributes are equal. <p>
     */
    public boolean equalsNameIgnoreCase(ChannelIdIF id) {
        if (id == null) return false;
        if (this.channelId == id) return true;
        return channelId.equalsNameIgnoreCase(id);
    }

    public boolean sameStationAs(ChannelIdentifiable ci) {
       return (this == ci) ? true : super.sameStationAs((ChannelIdIF) ci.getChannelId());
    }

    /**
     * This uses just the essentials as in equals().
     */
    public int hashCode() {
        return channelId.hashCode();
    }

    /** Returns the result of comparing the strings generated by invoking toString()
    * for this instance and the input object.
    * Throws ClassCastException if input object is not an instanceof Channel.
    * A return of 0 == value equals, <0 == value less than, >0 == value greater than,
    *  the string value of the input argument.
    */
    public int compareTo(Object x) {
        if (x == null ||  x.getClass() != getClass() )
                 throw new ClassCastException("compareTo(object) argument must be a ChannelLocation class type: "
                                + x.getClass().getName());
        return channelId.toString().compareTo( ((ChannelLocation) x).getChannelId().toString());
    }

    /**
     * Look up the ChannelLocation described by the input in the DataSource and 
     * return a fully populated ChannelLocation object.
     * This is how you look up LatLonZ and response info for a single channel.
     * Note this finds the LATEST entry if more then one entry exists 
     * (returns record with largest 'ondate').
     * Does NOT require that the ChannelLocation data be "active".
     * If no entry is found, this method return the input reference.
     */
    public abstract ChannelLocation lookUp(ChannelLocation cl) ;
    public abstract ChannelLocation lookUp(ChannelLocation cl, java.util.Date date) ;

    /**
     * Returns from the default DataSource a ChannelList 
     * containing currently active channels.
     */
    public ChannelDataMap readList() { return readList((java.util.Date) null); }
    /**
     * Return from the default DataSource a ChannelDataMap of
     * ChannelLocation elements for those channels active on the input date.
     */
    public abstract ChannelDataMap readList(java.util.Date date);

    /** Return a list of channels that were active on the given date and
    *  that match the list of component types given in
    * the array of strings. The comp is compared to the SEEDCHAN field in the NCDC
    * schema.<p>
    *
    * SQL wildcards are allowed as follows:<br>
    * "%" match any number of characters<br>
    * "_" match one character.<p>
    *
    * For example "H%" would match HHZ, HHN, HHE, HLZ, HLN & HLE. "H_" wouldn't
    * match any of these but "H__" would match them all. "_L_" would match
    * all components with "L" in the middle of three charcters (low gains).
    * The ANSI SQL wildcards [] and ^ are not supported by Oracle.
    */

    public abstract ChannelDataMap getByComponent(String[] compList, java.util.Date date) ;

    /** Return a list of currently acive channels that match the list of
     *  component types given in
    * the array of strings. The comp is compared to the SEEDCHAN field in the NCDC
    * schema.<p>
    *
    * SQL wildcards are allowed as follows:<br>
    * "%" match any number of characters<br>
    * "_" match one character.<p>
    *
    * For example "H%" would match HHZ, HHN, HHE, HLZ, HLN & HLE. "H_" wouldn't
    * match any of these but "H__" would match them all. "_L_" would match
    * all components with "L" in the middle of three charcters (low gains).
    * The ANSI SQL wildcards [] and ^ are not supported by Oracle.
    */

    public ChannelDataMap getByComponent(String[] compList) {
        return getByComponent(compList, (java.util.Date) null ) ;
    }


///////////////////////////////////////////////////////////////////////////////////
    /** Return ASCII representation of the channel name with the fields delimited
     *  by the given string. */
    public String toDelimitedNameString(String delimiter) {
        return channelId.toDelimitedNameString(delimiter);
    }
    /** Return ASCII representation of the channel name with the fields delimited
     *  by "." */
    public String toDelimitedNameString() {
        return ((ChannelName)channelId).toDelimitedNameString();
    }
    /** Return ASCII representation of the channel SEED name with the fields delimited
     *  by the given char. */
    public String toDelimitedSeedNameString(char delimiter) {
        return channelId.toDelimitedSeedNameString(delimiter);
    }
    /** Return ASCII representation of the channel SEED name with the fields delimited
     *  by the given string. */
    public String toDelimitedSeedNameString(String delimiter) {
        return channelId.toDelimitedSeedNameString(delimiter);
    }
    /** Return ASCII representation of the channel SEED name with the fields delimited
     *  by "." */
    public String toDelimitedSeedNameString() {
        return ((ChannelName)channelId).toDelimitedSeedNameString();
    }

    /**
     * Returns delimited channel name and LatLonZ.
     */
    public String toString(){
        return channelId.toDelimitedNameString(' ') + " " + latlonz.toString() ;
    }
    public String toNeatString(){
        return toString();
    }
    public String getNeatHeader(){
        return "StaNetSeedchan  LatLonZ";
    }
    /**
     * Complete object dump. For debugging. Overides ChannelName.toDumpString()
     * to include LatLonZ.
     */
    public String toDumpString(){
        String str =
            " net: " + channelId.getNet()+
            " sta: " + channelId.getSta() +
            " channel: " + channelId.getChannel() +
            " seedchan: " + channelId.getSeedchan() +
            " location: " + channelId.getLocation()+
            " channelsrc: " + channelId.getChannelsrc() +
            " LatLonZ= "+ latlonz.toString();
        return str;
    }

///////////////////////////////////////////////////////////////////////////////////
    /** Return a string with all the members of a ChannelLocation object, delimited
    * by the given string. The string must be a single character if you
    * will read it back with parseCompleteString(). It also, should not be
    * a character that would appear in any of the members (letters and numbers). */
    public String toCompleteString(String delim) {
       return  toCompleteString (this, delim);
    }

    /** Return a string with all the members of a ChannelLocation object, delimited
    * by the given string. The string must be a single character if you
    * will read it back with parseCompleteString(). It also, should not be
    * a character that would appear in any of the members (letters and numbers). */
    public static String toCompleteString(ChannelLocation cl, String delim) {
            StringBuffer sb = new StringBuffer(128);
            sb.append(((ChannelName)cl.channelId).toCompleteString(delim));
            sb.append(delim).append(cl.latlonz.toString(delim));
            return sb.toString();
    }

///////////////////////////////////////////////////////////////////////////////////
    public boolean binaryWrite (DataOutputStream out) {
       return binaryWrite(this, out);
    }
    public boolean binaryWrite (ChannelLocation cl, DataOutputStream out) {
      boolean status = true;
      try {
       out.writeUTF(cl.channelId.getNet());
       out.writeUTF(cl.channelId.getSta());
       out.writeUTF(cl.channelId.getChannel());
       out.writeUTF(cl.channelId.getSeedchan());
       out.writeUTF(cl.channelId.getChannelsrc());
       out.writeUTF(cl.channelId.getLocation());

       out.writeDouble(cl.latlonz.getLat());
       out.writeDouble(cl.latlonz.getLon());
       out.writeDouble(cl.latlonz.getZ());

      } catch (IOException ex) {
            ex.printStackTrace();
            status = false ;
      }
      return status;
    }

    public static ChannelLocation binaryRead (DataInputStream in) {
      ChannelLocation cl = ChannelLocation.create();
      try {
        cl.channelId.setNet(in.readUTF());
        cl.channelId.setSta(in.readUTF());
        cl.channelId.setChannel(in.readUTF());
        cl.channelId.setSeedchan(in.readUTF());
        cl.channelId.setChannelsrc(in.readUTF());
        cl.channelId.setLocation(in.readUTF());
        cl.latlonz.set(in.readDouble(), in.readDouble(), in.readDouble());

      } catch (IOException ex) {
        ex.printStackTrace();
      }
      return cl;
    }

    /** Parse a string for the format created by toCompleteString(). */
    public static ChannelLocation parseCompleteString (String str, String delim) {
       StringTokenizer strTok = new StringTokenizer(str, delim);
       ChannelLocation cl = ChannelLocation.create();
       try {
            cl.channelId = ChannelName.parseCompleteString(strTok);
            cl.latlonz = LatLonZ.parse(strTok);
       } catch (NoSuchElementException ex) {}
       return cl;
    }
}   // end of ChannelLocation class
