package org.trinet.jasi;

import java.util.*;
import org.trinet.jdbc.table.*;
import org.trinet.jdbc.*;
import org.trinet.util.Format;

/**
 * Seed channel name identification object.
 */

public class ChannelName extends Object implements java.io.Serializable, AuthChannelIdIF, ChannelIdentifiable,
        Comparable, Cloneable {

    private String sta        = "";
    private String net        = "";
    private String auth       = "";
    private String subsource  = "";
    private String channel    = "";
    private String channelsrc = "";
    private String seedchan   = "";
    private String location   = "";

    /** Switch controls if SEED Location code is used in name matching comparisons. */
    public static boolean useLoc = true; // 04/04/2006 changed default to use location code -aww
    public static boolean useFullName = false; // for debug print string
    // 03/25/2005 use below option to preserve existing auth.subsource.channel.channelsrc declarations
    public static boolean copyOnlySNCL = false; // true = copy only channelname SNCL id key

  /**
  * Null constructor
  */
  public ChannelName() { }

  /**
  * Constructor with another ChannelName instance. Note this is a copy and not
  * a reference to the original data.
  */
  public ChannelName(ChannelName chan) { copy(chan); }

 /**
  * Constructor with partial sta name. Missing fields are set to "".
  * It trims off spaces, else string compares
  * won't be correct later. Note this copies the strings rather then making
  * a reference to the original string objects.
  */
  public ChannelName(String net, String sta, String channel, String seedchan) {
     set(net, sta, seedchan, null, null, null, channel, null);
  }

 /**
  * Constructor with with full spec. It trims off spaces, else string compares
  * won't be correct later. Note this copies the strings rather then making
  * a reference to the original string objects.
  */
  public ChannelName(String net, String sta, String seedchan, String location, String auth,
                     String subsource, String channel, String channelsrc) {

    set(net, sta, seedchan, location, auth, subsource, channel, channelsrc);
  }

  /** Sets all data member attributes to empty string "".*/
  public final void clear() {
    set(null,null,null,null,null,null,null,null);
  }

  /** Sets all data member attributes to input values, where nulls are set to "".
   * @see StringSQL#trimToCanonical(String)
   * */
  //  DDG 1/19/06 - changed assignments to method calls
  public final void set(String net, String sta, String seedchan, String location,
                  String auth, String subsource, String channel, String channelsrc) {

    setNet(net);
    setSta(sta);
    setSeedchan(seedchan);
    setLocation(location); 

    // Kludge to preserve "existing" data channel description not part of SNCL
    // when copying reference from a match in a ChannelList to data. 
    if (copyOnlySNCL) return; // 03/25/2005 aww
    
    setAuth(auth);
    setSubsource(subsource);
    setChannel(channel);
    setChannelsrc(channelsrc);

  }

  public Object clone() {
    ChannelName cn = null;
    try {
      cn = (ChannelName) super.clone();
    }
    catch (CloneNotSupportedException ex) {
      ex.printStackTrace();
    }
    return cn;
  }

  /**
   * Set member attributes to the equivalent of those of the input.
   */
  public ChannelName copy(ChannelName cn) {
    set(cn.net, cn.sta, cn.seedchan, cn.location, cn.auth, cn.subsource, cn.channel, cn.channelsrc);
    return this;
  }

  /**
  * Case sensitive compare.
  * Returns true if sta, net, seedchannel and location are equal.
  * Does not check the other name fields like 'channel'.
  */
  public boolean equals(Object x) {
    if (this == x) return true;
    else if ( x == null || ! (x instanceof ChannelName)) return false;
    ChannelName cn = (ChannelName) x;
    return equalsName(cn);
  }

  public boolean equalsName(ChannelName id) {
    // switch to use loc code when it is consistant throughout the system
    if (this == id ) return true;
    boolean retVal =
          ( sta.equals(id.getSta()) &&           // most variable value
            seedchan.equals(id.getSeedchan()) && // next most variable value
            net.equals(id.getNet())
          );
   return (useLoc) ?
     (retVal && location.equals(id.getLocation())) : retVal; // add location tie-breaker
  }
  public boolean equalsNameIgnoreCase(ChannelName id) {
    // switch to use loc code when it is consistant throughout the system
    if (this == id ) return true;
    boolean retVal =
          ( sta.equalsIgnoreCase(id.getSta()) &&           // most variable value
            seedchan.equalsIgnoreCase(id.getSeedchan()) && // next most variable value
            net.equalsIgnoreCase(id.getNet())
          );
   return (useLoc) ?
     (retVal && location.equalsIgnoreCase(id.getLocation())) : retVal; // add location tie-breaker
  }

  /**
  * Case sensitive compare of blank-trimmed input field attributes.
  * Returns true if sta, net, and seedchan attributes are equal.
  * */
  public boolean equalsName(ChannelIdIF id) {
    if (this == id ) return true;
    boolean retVal =
          ( sta.equals(id.getSta().trim()) &&           // most variable value
            seedchan.equals(id.getSeedchan().trim()) && // next most variable value
            net.equals(id.getNet().trim())
          );
    return (useLoc) ?
     // add location as tie-breaker (blanks ok, don't trim location blanks - aww 07/11/05)
     ( retVal && location.equals(id.getLocation()) ) : retVal;
  }

  /**
  * Case insensitive compare of blank-trimmed input field attributes.
  * Returns true if sta, net, and seedchan attributes are equal.
  * */
  public boolean equalsNameIgnoreCase(ChannelIdIF id) {
    boolean retVal = sameSeedChanAs(id);
    return (useLoc) ?  // switch to use loc code when it is consistant throughout the system
     // add location as tie-breaker (blanks ok, don't trim location blanks - aww 07/11/05)
     ( retVal && location.equalsIgnoreCase(id.getLocation()) ) : retVal;
  }
  /** Returns true if input's "net" are case insensitive equal to
   *  those of this instance. */
  public boolean sameNetworkAs(ChannelIdIF id) {
    if (this == id ) return true;
    return net.equalsIgnoreCase(id.getNet().trim());
  }
  /** Returns true if input's "net and sta" are case insensitive equal to
   *  those of this instance. */
  public boolean sameStationAs(ChannelIdIF id) {
    if (this == id ) return true;
    return (
            sta.equalsIgnoreCase(id.getSta().trim()) &&
            net.equalsIgnoreCase(id.getNet().trim())
           );
  }
  /** Returns true if input's "net and sta" and "seedchan" are case insensitive equal to
   *  those of this instance. */
  public boolean sameSeedChanAs(ChannelIdIF id) {
    if (this == id ) return true;
    return (
            sta.equalsIgnoreCase(id.getSta().trim()) && // most variable value
            seedchan.equalsIgnoreCase(id.getSeedchan().trim()) && // next most variable value
            net.equalsIgnoreCase(id.getNet().trim())
           );
  }
  /** Returns true if input's "net,sta,location" and 1st 2-char of its seedchan are case insensitive equal to
   *  those of this instance. */
  public boolean sameTriaxialAs(ChannelIdIF id) {
    if (this == id ) return true;
    if (! sameStationAs(id)) return false;
    if ( useLoc && ! getLocation().equalsIgnoreCase(id.getLocation()) ) return false; // add location tie-breaker
    // match 1st 2 chars of seedname string
    return (
             getSeedchan().substring(0,2).equalsIgnoreCase( id.getSeedchan().substring(0,2) )
           );
  }


  /**
  * Returns true if the input's "sta, net, seedchan and location" strings
  * are case insensitive equal to those of this instance.
  * Forces a trim of input trailing blanks just in case the reader didn't.
  */
  public boolean equalsChannelId(ChannelIdIF id) {
    return equalsNameIgnoreCase(id);
  }

  public ChannelIdIF getChannelId() {
      return this;
  }
  public void setChannelId(ChannelIdIF id) {
      if (this == id ) return;
      setNet(id.getNet());
      setSta(id.getSta());
      setSeedchan(id.getSeedchan());
      setLocation(id.getLocation());
      if (copyOnlySNCL) return; // 03/25/2005 aww
      setChannel(id.getChannel());
      setChannelsrc(id.getChannelsrc());
      if (id instanceof AuthChannelIdIF) {
        setAuth(((AuthChannelIdIF) id).getAuth());
        setSubsource(((AuthChannelIdIF) id).getSubsource());
      }
  }

 /**
 * The default Object hashCode uses ALL fields of ChannelName but many are not
 * populated because not all data sets contain all the Seed detail.
 * This uses just the essentials as in equals().
 */
  private Integer hashKey;
  public int hashCode() {
    //return (net+sta+seedchan).hashCode();
    if (hashKey == null) setHashValue();
    return hashKey.hashCode();
  }
  private void setHashValue() {
    hashKey = Integer.valueOf((sta+seedchan).hashCode());
  }


// Implement AuthChannelIdIF
    public String getSta() {
        return sta;
    }
    public String getNet() {
        return net;
    }
    public String getSeedchan() {
        return seedchan;
    }
    public String getLocation() {
        return location;
    }
    /** Returns the location code string with blanks converted to "-".
 	 * This is done because blanks are allowed and common in location 
     * codes but are difficult to read in ASCII listings and can cause 
     * havoc with white-space delimited output.
     * */
    public String getLocationString () {
    	return location.replace(' ', '-'); 
    }    
    public String getChannel() {
        return channel;
    }
    public String getChannelsrc() {
        return channelsrc;
    }
    public String getAuth() {
        return auth;
    }
    public String getSubsource() {
        return subsource;
    }

    public void setSta(String sta) {
        this.sta = StringSQL.trimToCanonical(sta);
        setHashValue();
    }
    public void setNet(String net) {
        this.net = StringSQL.trimToCanonical(net);
    }
    public void setSeedchan(String seedchan) {
        this.seedchan = StringSQL.trimToCanonical(seedchan);
        setHashValue();
    }
    /** 
     * Set the Location Code. Note that this requires special handling
     * because of the CISN "Location Code Policy". Instances of "-" in an
     * ASCII string are converted to " " (blank) when parsed. This is done
     * because blanks are allowed and common in location codes but are 
     * difficult to read in ASCII listings and can cause havoc with white-space
     * delimited output.*/
    public void setLocation(String location) {
        //this.location = StringSQL.trimToCanonical(location);
        // removed above since "blanks" are valid aww 07/14/2005 
        // only a null input location is forced to be empty string "".

        if (location == null) {
        	this.location = "";
        } else {                   // map dashes to blanks - DDG 1/19/06
        	this.location = location.replace('-', ' ').intern();
        }

    }
    
    public void setChannel(String channel) {
        this.channel = StringSQL.trimToCanonical(channel);
    }
    public void setChannelsrc(String channelsrc) {
        this.channelsrc = StringSQL.trimToCanonical(channelsrc);
    }
    public void setAuth(String auth) {
        this.auth = StringSQL.trimToCanonical(auth);
    }
    public void setSubsource(String subsource) {
        this.subsource = StringSQL.trimToCanonical(subsource);
    }

    /** Returns the result of comparing the strings generated by invoking toString() for this instance
    * and the input object.
    * Throws ClassCastException if input object is not an instanceof ChannelName.
    * A return of 0 == value equals, <0 == value less than, >0 == value greater than,
    * the string value of the input argument.
    */
    public int compareTo(Object x) {
      if (x == null ||  x.getClass() != getClass() )
         throw new ClassCastException("compareTo(object) argument must be a ChannelName class type: "
               + x.getClass().getName());

      return toString().compareTo(((ChannelName) x).toString());
    }

    /**
     * Return a brief channel name string. Ex: "CI ABC HHZ"
     */
    public String toString() {
      return toDelimitedNameString(' ');
    }

    /**
     * Return an IP address style name string. Ex: "CI.ABC.HHZ"
     */
    public String toDottedString() {
      return toDelimitedNameString('.');
    }

    /**
     * Return a compact name string. Ex: "CIABCHHZ"
     */
    public String toCompactString() {
      return toCompactSeedString();
    }

    /**
     * Return a compact name string. Ex: "CIABCHHZ"
     */
    public String toCompactSeedString() {
      return toDelimitedSeedNameString("");
    }

    /**
     * Return a name string delimited by the given character. Ex: dlm = " " ->  "CI ABC HHZ"
     */
    public String toDelimitedNameString(char dlm) {
      return toDelimitedNameString(String.valueOf(dlm));
    }

    /**
     * Return a name string delimited by the given character. Ex: dlm = " " ->  "CI ABC HHZ"
     */
    public String toDelimitedNameString(String dlm) {
      return (useFullName) ? toCompleteString(dlm) : toDelimitedSeedNameString(dlm);
    }

    /**
     * Return a name string delimited by spaces
     */
    public String toDelimitedNameString()  {
      //  return net +dlm+ sta +dlm+ seedchan;
      return toDelimitedNameString(" ");
    }

    /**
     * Return ChannelName based on parse of a name string delimited by spaces
     */
    public static ChannelName fromDelimitedString(String str)  {
      return fromDelimitedString(str, " ");
    }

    /**
     * Return a name string delimited by the given character. Ex: dlm = " " ->  "CI ABC HHZ"
     */
    public String toDelimitedSeedNameString(char dlm) {
      //return net + dlm + sta + dlm + seedchan;
      return toDelimitedSeedNameString(String.valueOf(dlm));     // can't cast char to String
    }

    /**
     * Return a name string delimited by the given String. 
     * Ex: dlm = "_." ->  "CI_.ABC_.HHZ"
     * Blanks will be converted to "-".
     * This is done because blanks are allowed and common in location 
     * codes but are difficult to read in ASCII listings and can cause 
     * havoc with white-space delimited output.
     */
    public String toDelimitedSeedNameString(String dlm) {
      StringBuffer sb = new StringBuffer(32);
      sb.append(net).append(dlm).append(sta).append(dlm).append(seedchan);
//      if (useLoc) sb.append(dlm).append( (location.equals("  ") ?  "--" : location) );
      if (useLoc) sb.append(dlm).append( getLocationString() );

      return sb.toString();
    }

    /**
     * Return ChannelName based on parse of a name string delimited by the given string.
     * dlm is a string containing a list of acceptable one-character delimiters;
     * e.g. ":/.,"
     * Never use a legal SEED name character as a delimiter: (A-Z, 0-9, blank).
     * Never use "-" which is a placeholder for "blank" in ASCII strings.
     * Assumes order of: "nt.sta.seed.loc" for example "CI.PDW.EHZ.01".
     * If the delimited string is malformed or incomplete the returned ChannelName
     * object may also be malformed or incomplete.
     * 
     */
    public static ChannelName fromDelimitedString(String str, String dlm)  {
    	// ToDo: check for legal delimiter here
      StringTokenizer strTok = new StringTokenizer(str, dlm);
      ChannelName cn = new ChannelName();
      try {
        cn.setNet(strTok.nextToken());
        cn.setSta(strTok.nextToken());
        cn.setSeedchan(strTok.nextToken());
        cn.setChannel(cn.getSeedchan());
        if (useLoc) cn.setLocation(strTok.nextToken());

      } catch (NoSuchElementException ex) {};
      return cn;
    }

    /**
    * Return a name string delimited by the given String. And with the Net/Sta/Chan fields
    * the size of the three fields passed as args. For example:
 <tt>
      chan.toFormattedString( " ", "xx", "xxxxx", "xxx");

      yields:  "CI ABC   HHZ"
                xx xxxxx xxx
 </tt>
    */
    public String toFormattedString(String dlm, String field1, String field2, String field3, String field4) {
      return toFormattedString(dlm, field1.length(), field2.length(), field3.length(), field4.length());
    }

    public String toCompleteString(String dlm) {
      return toCompleteString(this, dlm);
    }

  /** Return a string with all the members of a ChannelName object, delimited
  * by the given string. The string must be a single character if you
  * will read it back with parseCompleteString(). It also, should not be
  * a character that would appear in any of the members (letters and numbers). */
  public static String toCompleteString(ChannelName channelId, String dlm) {
    StringBuffer sb = new StringBuffer(48);
    sb.append(channelId.getNet());
    sb.append(dlm).append(channelId.getSta());
    sb.append(dlm).append(channelId.getChannel());
    sb.append(dlm).append(channelId.getSeedchan());
    //sb.append(dlm).append(channelId.getLocation());   // DDG 1/19/06
    sb.append(dlm).append(channelId.getLocationString());
    sb.append(dlm).append(channelId.getChannelsrc());
    return sb.toString();
  }

  /** Parse a string for the format created by toCompleteString(). */
  public static ChannelName parseCompleteString (String str, String dlm) {
    return parseCompleteString(new StringTokenizer(str, dlm));
  }

  /** Parse a string for the format created by toCompleteString().
  * Using a StringTokenizer allows us to parse the ChannelName from a longer
  * string and keep the "pointer" to parse other parts of the string. */
  public static ChannelName parseCompleteString (StringTokenizer strTok) {
    ChannelName cn = new ChannelName();
    try {
      cn.setNet(strTok.nextToken());
      cn.setSta(strTok.nextToken());
      cn.setChannel(strTok.nextToken());
      cn.setSeedchan(strTok.nextToken());
      cn.setLocation(strTok.nextToken());
      cn.setChannelsrc(strTok.nextToken());
    } catch (NoSuchElementException ex) {}
    return cn;
  }

 /**
 * Return a channel name string delimited by the given String. And with the Net/Sta/Chan fields
 * the size of the three args. A negative arg means left align, otherwise it will be
 * right aligned. For example:
 <tt>
      chan.toFormattedString( " ", -2, -5, -3, -2);

      yields:  "CI ABC   HHZ O1"
                xx xxxxx xxx xx
 </tt>
 */
    public String toFormattedString(String dlm, int field1, int field2, int field3, int field4) {
      StringBuffer sb = new StringBuffer(64);

      // 04/26/2005 added logic below to use ? whenever name field has value "NULL" (ie db value null) -aww
      // 01/25/2006 added logic below to use ? whenever name field has value "" (ie db value null) -aww
      sb.append(formatOneField((net.equalsIgnoreCase("")) ? "??" : net, field1)).append(dlm);
      sb.append(formatOneField((sta.equalsIgnoreCase("")) ? "???" : sta, field2)).append(dlm);
      sb.append(formatOneField((seedchan.equalsIgnoreCase("")) ? "???" : seedchan, field3)).append(dlm);
      String lc = getLocationString();
      sb.append(formatOneField((lc.equalsIgnoreCase("")) ? "??" : lc, field4));

      return sb.toString();
    }
    /** Format one field */
    private String formatOneField (String valStr, int size) {
      // size may be negative (to indicate justification)
      int len = Math.abs(size);
      StringBuffer sb = new StringBuffer(24);
      if (valStr.length() > len) {  //too long, must truncate
        sb.append(valStr.substring(0, len));
      } else {                                 // <=
        StringBuffer sb2 = new StringBuffer("%").append(size).append("s");
        // OK or space padded
        sb.append(new Format(sb2.toString()).form(valStr));
      }
      return sb.toString();
    }

 /**
 * Return a string that neatly and consistantly formats the Net/Sta/Chan fields.
 * This is the equivalent of:   toFormattedString (" ", -2, -5, -3, -2);
 * the size of the three fields passed as args. For example:
 <tt>
      yields:  "CI ABC   HHZ 01"
                xx xxxxx xxx xx
 </tt>
 */
  public String toNeatString() {
    return toFormattedString (" ", -2, -5, -3, -2);  // changed default sta len to 5, seed allowed  - 10/01/2006 aww 
  }

  /** Returns  "Nt Sta  Chn Lc", the header that matches the output of toNeatString(). */
  public static String getNeatStringHeader(String dlm, int field1, int field2, int field3, int field4) {
    // make header look like a channel name (aren't we clever...)
    return
      new ChannelName("Nt", "Sta", "Chn", "Lc", "", "", "", "").toFormattedString(dlm, field1, field2, field3, field4);
  }

  /** Returns  "Nt Sta  Cmp", the header that matches the output of toNeatString(). */
  public static String getNeatStringHeader() {
    return new ChannelName("Nt", "Sta", "Chn", "Lc", "", "", "", "").toNeatString();
  }

 /**
 * Return a name string delimited by spaces.
 */
 public String toDelimitedSeedNameString() {
   return toDelimitedSeedNameString(" ");
 }

 /**
 * Complete object dump. For debugging.
 */
 public String toDumpString() {
   StringBuffer sb = new StringBuffer(132);
   sb.append("net: ").append(net);
   sb.append(" sta: ").append(sta);
   sb.append(" seedchan: ").append(seedchan);
   sb.append(" location:").append("\"").append(location).append("\"");;
   sb.append(" auth: ").append(auth);
   sb.append(" subsource: ").append(subsource);
   sb.append(" channel: ").append(channel);
   sb.append(" channelsrc: ").append(channelsrc);
   return sb.toString();
 }
}   // end of ChannelName class

