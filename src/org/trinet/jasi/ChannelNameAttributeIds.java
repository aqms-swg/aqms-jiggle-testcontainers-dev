package org.trinet.jasi;
public interface ChannelNameAttributeIds {
    public static final int NET         = 0;
    public static final int STA         = 1;
    public static final int SEEDCHAN    = 2;
    public static final int LOCATION    = 3;
    public static final int CHANNEL     = 4;
    public static final int CHANNELSRC  = 5;
    public static final int AUTH        = 6;
    public static final int SUBSOURCE   = 7;
}
