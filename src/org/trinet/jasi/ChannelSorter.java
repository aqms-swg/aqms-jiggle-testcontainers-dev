package org.trinet.jasi;
import java.util.*;

/**
 * Provides comparator() method to sort a list of Channelable objects by distance
 * and component name. The distance from the sort point must already be set
 * for the compared objects with channelable.setDistance(double). <p>
 * @DEPRECATED
 */
public class ChannelSorter implements Comparator {

    /** Comparator for component sort. */
    ChannelNameSorter cnSorter = new ChannelNameSorter();

    public int compare(Object o1, Object o2) {

        Channelable ch1 = (Channelable) o1;
        Channelable ch2 = (Channelable) o2;

        double diff = ch1.getDistance() - ch2.getDistance(); // ok to use slant for sort -aww

        if (Math.abs(diff) < .00001) diff = 0.; // added this test to handle weird roundoff? -aww 2010/10/20

         if (diff < 0.0) {
            return -1;
         }
         else if (diff > 0.0) {
            return  1;
         }
         else { // same dist sort, sort by channel name
           return cnSorter.compare( ch1.getChannelObj(), ch2.getChannelObj());
        }
    }
} // end of ChannelSorter class
