package org.trinet.jasi;

import java.util.*;
import org.trinet.util.*;

/**
 * ActiveList of ChannelTimeWindowModel objects
 */

public class ChannelTimeWindowModelList extends ActiveList {

  public ChannelTimeWindowModelList() { }

  /** Return a model from the list with this class name.
   * Returns null if string is null or no such class is in the list.*/
  public ChannelTimeWindowModel getByClassname(String classname) {
    if (classname == null) return null;
    ChannelTimeWindowModel model = null;
    for (Iterator it=this.iterator(); it.hasNext(); ) {
      model = (ChannelTimeWindowModel) it.next();
      if (classname.equals(model.getClassname())) return model;
    }
    return null;
  }

  /** Add a model to the list. */
  public boolean add(Object obj) {
    return ( obj != null && (obj instanceof ChannelTimeWindowModel) ) ? super.add(obj) : false;
  }

  /** Instantiate a model of this type, set its properties, and add it to the list.*/
  public boolean add(String modelClass, GenericPropertyList props) {
    if (modelClass == null) return false;  // bad string
    // try an instance, bail if no good
    if (getByClassname(modelClass) != null) return true; // already in list

    // create it
    ChannelTimeWindowModel model = ChannelTimeWindowModel.getInstance(modelClass);
    if (model == null) {
        return false;
    }
    model.setProperties(props);

    return add(model);
  }

  public Object clone() {
      ChannelTimeWindowModelList list = (ChannelTimeWindowModelList) super.clone();
      for (int i=0; i<size(); i++) {
          list.set(i, ((ChannelTimeWindowModel) get(i)).clone());
      }
      return list;
  }
}
