package org.trinet.jasi;

import java.util.*;

/**
 Comparator for sorting seismic channels by component type. You define the
 desired sort order with an array of three strings. The first string corresponds
 with the first character in the component type, the second with the second, and
 the third with the third. Each string is a series of characters in the order in
 which you want to sort each field in the 3-char component name. <p>

 For example the following array of strings:<br>
 <tt>
   String list[] = {"EHB", "HL", "ZNE"};
 </tt>
<p>
would put "E" before "H" before "B" when that character is in the 1st field.  It
would put "H" before "L" in the second field and order the last field "Z", "N",
"E".<p>

The characters in the string should be UPPERCASE but the comparison with the
components is NOT case sensitive. If any part of the component code is not
described in the comparitor string array it will fall to the end of the sort
order but matching fields will still sort correctly.<p>

The default order is the order in which band, instrument and orientation codes 
are described in the SEED Reference Manuel, v2.3, Appendix A, pg. 114 (revised Aug. 2000).<p>

{"ESHBMLVURAWX", "HLGMN", "ZNEABCTR123UVW"}

*/

public class ComponentSorter implements Comparator {
    //                       inst  gain   orient

    /** The default component ordering based on Seed instrument code convention.  */
    static String list[] = {"ESHBMLVURAWX", "HLGMN", "ZNEABCTR123UVW"};

    /** Sort using default comparitor list. <p>
     * String list[] = {"ESHBMLVURAWX", "HLGMN", "ZNEABCTR123UVW"};
     * */
    public ComponentSorter() { }

    public ComponentSorter(String[] comparitorList) {

      list = comparitorList;
    }

    /** */
    public int compare(Object obj1, Object obj2) {

      char char_a;
      char char_b;
      int rank_a, rank_b, result = 0;

      if ((obj1 instanceof String) &&
          (obj2 instanceof String)) {

        String str_a = ((String) obj1).toUpperCase();
        String str_b = ((String) obj2).toUpperCase();
        int len = Math.min(str_a.length(), str_b.length());

        for (int i = 0; i< len; i++) {
          // get the next char to compare
          char_a = str_a.charAt(i);
          char_b = str_b.charAt(i);
          // find the position of each in the ranking
          rank_a = list[i].indexOf(char_a);
          rank_b = list[i].indexOf(char_b);
          // handle case where char is NOT in the list
          if (rank_a == -1) rank_a = len;
          if (rank_b == -1) rank_b = len;
          result = rank_a - rank_b;
          if (result != 0) return result;      // if equal do next pass thru loop
        }
      }
      return result;
    }

/*
  public static final class Tester {
    public static void main (String args[]) {
      ArrayList list = new ArrayList();
      list.add("BHZ");
      list.add("EHZ");
      list.add("HHE");
      list.add("HHZ");
      list.add("HHN");
      list.add("hle");
      list.add("HLN");
      list.add("UL3");
      list.add("ABN");
      list.add("UL2");
      list.add("ABZ");
      list.add("ABE");
      list.add("UL1");

      System.out.println ("Before Sort...");
      for (int i=0; i<list.size();i++){
        System.out.println (list.get(i).toString());
      }
      
      Collections.sort(list, new ComponentSorter());

      System.out.println ("After Sort...");
      for (int i=0; i<list.size();i++){
        System.out.println (list.get(i).toString());
      }
    }
  } // end of Tester
*/
} // end of ComponentSorter 
