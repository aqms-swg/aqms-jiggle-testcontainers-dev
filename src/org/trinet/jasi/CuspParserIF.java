package org.trinet.jasi;
public interface CuspParserIF {
  public static final String NETWORK_CODE = "CI";
  public static final String OLD_SOURCE = "CUSP";
  public static final String LOCATION_CODE = "02";
  // Rob Clayton wants:
  public static final String [] SEEDCHAN_CODES =
      new String [] {"EHE","EHN","EHZ"};
  public static final String [] CHANNEL_CODES =
      new String [] {"E","N","Z"};
}
