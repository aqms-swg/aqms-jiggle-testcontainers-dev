// Should this be moved to a jdbc.datasources package? -aww
package org.trinet.jasi;
import java.sql.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.datasources.*;
import org.trinet.jiggle.common.JiggleExceptionHandler;
import org.trinet.jiggle.database.JiggleConnection;
import org.trinet.util.*;

// NOTE: ALL STATIC, NO INSTANCE DATA OR METHODS IMPLEMENTED IN THIS CLASS EXCEPT toString()
// References jdbc Connection. 
// No logic here for "closing" a prexisting jdbc connection in the case when
// the user set/resets a new connection via a static method call, thus
// either user or the subclass must manage the closing of any pre-existing
// connection! - aww

/**
 * Define the default data source for all JASI classes.  This data source will
 * be used in static calls and in other methods when no data source is given.
 * */
public class DataSource {

// Instantiation of an instance of this class only to get a local identifier
// reference for other classes to access via static data/methods, could use a
// static "set or create" method instead of constructor?

    // note for a singleton instance see suggested implementation of
    // thread safe singleton by other java programming experts
    // private static DataSource ds =  null;

    /** Class name of default JASI data source; org.trinet.jdbc.datasources.JDBCDataSource. */
    public static final String DEFAULT_DATASOURCE_STRING =
        "org.trinet.jdbc.datasources.JDBCDataSource";

    /** SQLDataSourceIF instance that does the data transfer work. */
    private static SQLDataSourceIF ds = null;

 // Define a global reference time basis for datetime data of database, i.e.
 // does database use "nominal" or "true" (leap corrected) epoch seconds.
 // This info is not yet available directly db by a query as of 03/04;
 // if the dbTimeBase logic is ever implemented into the databases,
 // then we can migrate the timebase code logic down to the "ds" instance
 // and use static wrappers here around the methods of the ds instance. - aww
/* removed static member block for UTC conversion stored package  -aww 2008/02/11 
    protected static final String NOMINAL = "NOMINAL";
    protected static final String LEAP  = "LEAP";
    private static final int NOMINAL_TYPE = 0;
    private static final int LEAP_TYPE    = 1;
    private static final String [] dbTimeBaseStr = {NOMINAL, LEAP};
    private static int dbTimeBase = NOMINAL_TYPE; // default type is nominal epoch seconds
*/

    /** Creates a data source of the subtype specifed by DEFAULT_DATASOURCE_STRING.
     * @see DataSource(String)
     * @see #createDefaultDataSource()
     * */
    public DataSource() {
      this(DEFAULT_DATASOURCE_STRING);
    }

    /** Creates a DataSource of the default subtype specified by the input classname String.
     * Does NOT make a connection to the data store. Subsequently, user must configure an
     * appropriate JDBC Connection for the source by using the setXXX methods, also
     * see AbstractSQLDataSource.configure(...) methods.
     * */
    public DataSource(String className) {
      ds = AbstractSQLDataSource.createDataSource(className);
    }

    /** Creates a DataSource of the default subtype that uses the input JDBC connection and its metadata.
     * With data access READONLY. That is writeBackEnabled = false.
     * If input connection reference is null, or conn.isClosed() == true, exceptions may result if static DataSource
     * methods are invoke that reference Connection methods.
     * */
    public DataSource(Connection conn) {
      this(conn, false);
    }

    /** Creates a DataSource of the default subtype that uses the existing input JDBC connection and its metadata.
     * If 'allowWriteBack' is true, caller will be allowed to update, delete, and insert data,
     * default is 'false'.
     * If input connection reference is null, or conn.isClosed() == true, exceptions may result if static DataSource
     * methods are invoke that reference Connection methods.
     * */
    public DataSource(Connection conn, boolean allowWriteBack) {
      this(); // creates default jdbc connection data source type
      ds.configure(conn, allowWriteBack);
    }


    /** Creates a DataSource of the default subtype that creates a new JDBC connection as described by the input metadata.
     * Data access is set READONLY. That is writeBackEnabled = false.
     * @see AbstractSQLDataSource
     */
    public DataSource(String dbaseURL,
               String driver,
               String username,
               String passwd)
    {
      this(dbaseURL, driver, username, passwd, false);
    }


    /** Creates a DataSource of the default subtype that creates a new JDBC connection
     * as described by the input metadata.
     * Data access will be READONLY if 'allowWriteBack' is false, that is the
     * connection user cannot update, delete, and insert data.
     * @see AbstractSQLDataSource
     */
    public DataSource(String dbaseURL,
               String driver,
               String username,
               String passwd,
               boolean allowWriteBack)
    {
      this(); // creates default jdbc connection data source type
      ds.configure(dbaseURL, driver, username, passwd, allowWriteBack);
    }


    /** Creates a DataSource of the default subtype that creates a new JDBC connection as described by the input
     * DbaseConnectionDescription.
     *  @see  DbaseConnectionDescription
     *  */
    public DataSource(DbaseConnectionDescription desc) {
      this(); // creates default jdbc connection data source type
      ds.configure(desc);
    }

    public static boolean enableSessionTrace() {
        if (isNull()) return false;
        Connection c =  getConnection();
        if (c == null) return false;
        boolean status = false;
        Statement s = null;
        try {
            s = c.createStatement();
            status = s.execute("ALTER SESSION SET SQL_TRACE = TRUE");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
          try {
            if (s != null) s.close();
          } catch (SQLException ex) {
            ex.printStackTrace();
          }
        }
        return status;
    }

    public static int utcCompliant() {
        if (isNull()) return -1;
        Connection c =  getConnection();
        if (c == null) return -1;
        int status = -1;
        Statement s = null;
        ResultSet rs = null;
        try {
            s = c.createStatement();
            if (getDriver().contains("oracle")) {
                rs = s.executeQuery("select WAVE.isJavaUTC from dual");
            } else {
                rs = s.executeQuery("select WAVE.isJavaUTC()");
            }
            if (rs.next()) {
              status = rs.getInt(1);
              if (rs.wasNull()) status = -1;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
          try {
            if (s != null) {
                if (rs != null) rs.close();
                s.close();
            }
          } catch (SQLException ex) {
            ex.printStackTrace();
          }
        }
        status = 1; // RH REMOVE
        return status;
    }

    public static boolean testConnection() {
        return tableExists( "EVENT" );
    }

    public static boolean tableExists(String table) {
        // checks if table exists and is accessible to this db user
        if (isNull()) return false;

        boolean status = false;
        Connection c = getConnection();
        if (c == null) return false;

        ResultSet rs = null;
        PreparedStatement ps = null;

        try {
            //
            //status = c.isValid(3); // only java 1.6 (wait max 3 secs for validation) -aww 2011/06/06
            //

            ps = c.prepareStatement(DataSource.getJiggleConnection().getTableExistQuery());
            ps.setString(1, table);
            rs = ps.executeQuery();

            int tableCount = 0;
            if (rs.next()) {
                tableCount = rs.getInt(1);
            }

            status = tableCount > 0 ? true : false;
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(c, ex, "Error - Cannot check if table " + table + " exists.", "tableExists sql error: ");
        } finally {
            try {
                if (ps != null) ps.close();
                if (rs != null) rs.close();
            } catch (SQLException ex2) {
                status = false;
                JiggleExceptionHandler.handleDbException(c, ex2);
            }
        }
        return status;
    }

    public static boolean isPrimaryHost(String hostname) {

        if (isNull()) return false;

        Connection c = getConnection();
        if (c == null) return false;

        Statement s = null;
        ResultSet rs = null;
        String role = "unknown";
        boolean status = true;
        try {
            s = c.createStatement();
            String sql = 
               "select PRIMARY_SYSTEM from AQMS_HOST_ROLE where MODIFICATION_TIME = " +
               "(select max(MODIFICATION_TIME) from AQMS_HOST_ROLE where HOST ='" + hostname + "')" +
               " and HOST ='" + hostname + "'";
            if (JasiDatabasePropertyList.debugTN) System.out.println(sql);
            rs = s.executeQuery(sql);
            if (rs.next()) {
              role = rs.getString(1);
            }
            status = role.equalsIgnoreCase("true");
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        finally {
          try {
            if (s != null) {
                if (rs != null) rs.close();
                s.close();
            }
          } catch (SQLException ex2) {
            System.err.println(ex2.getMessage());
          }
        }
        return status;

    }

    public static String getHostAppRole(String hostname, String app, String state) {

        String role = "unknown";

        if (isNull()) return role;

        Connection c = getConnection();
        if (c == null) return role; 

        Statement s = null;
        ResultSet rs = null;
        try {
            s = c.createStatement();
            String sql = "select ROLE_MODE from APP_HOST_ROLE where HOST='"+hostname+"' and APPNAME='"+app+"' and STATE='"+state+"'" +
                " AND offdate>CURRENT_TIMESTAMP";
            if (JasiDatabasePropertyList.debugTN) System.out.println(sql);
            rs = s.executeQuery(sql);
            if (rs.next()) {
              role = rs.getString(1);
            }
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        finally {
          try {
            if (s != null) {
                if (rs != null) rs.close();
                s.close();
            }
          } catch (SQLException ex2) {
            System.err.println(ex2.getMessage());
          }
        }
        return role;

    }

    public static boolean hasPrimaryRtRole() {

        if (isNull()) return false;

        boolean status = false;
        Connection c = getConnection();
        if (c == null) return false;

        Statement s = null;
        ResultSet rs = null;
        String role = "unknown";
        try {
            s = c.createStatement();
            String sql = 
               "Select primary_system from RT_ROLE where MODIFICATION_TIME = (select max(MODIFICATION_TIME) from RT_ROLE)";
            if (JasiDatabasePropertyList.debugTN) System.out.println(sql);
            rs = s.executeQuery(sql);
            if (rs.next()) {
              role = rs.getString(1);
            }
            status = role.equalsIgnoreCase("true");
        } catch (SQLException ex) {
            System.err.println(ex.getMessage());
        }
        finally {
          try {
            if (s != null) {
                if (rs != null) rs.close();
                s.close();
            }
          } catch (SQLException ex2) {
            status = false;
            System.err.println(ex2.getMessage());
          }
        }
        return status;
    }

    public static java.util.Date getDbVersionDate(String appName) {

        if (appName == null) return null;  // should default for null name be null?

        if (isNull()) return null;

        Connection c =  getConnection();
        if (c == null) return null;

        Statement s = null;
        ResultSet rs = null;

        java.util.Date dbVersionDate = null;

        try {
            s = c.createStatement();
            String sql = "select LDDATE from applications where NAME='"+appName+"'";
            if (JasiDatabasePropertyList.debugTN) System.out.println(sql);
            rs = s.executeQuery(sql);
            if (rs.next()) {
              dbVersionDate = rs.getDate(1);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
          try {
            if (s != null) {
                if (rs != null) rs.close();
                s.close();
            }
          } catch (SQLException ex) {
            ex.printStackTrace();
          }
        }
        return dbVersionDate;
    }

    /** Creates a SQLDataSourceIF of the default subtype for the static source.
    * No connection is created. 
    * @return source or null
    * @see #getSource()
    * @see #createDataSource(String)
    */
    public static SQLDataSourceIF createDefaultDataSource() {
      return createDataSource(DEFAULT_DATASOURCE_STRING);
    }

    /** If static source if null, creates the default subtype for the static source 
     * Uses source to create an internal database connection. 
     * that can be used by stored procedures on server. Not for client-side use.
     * @return database connection or null if failed
     * @see #getSource()
     * */
    public static Connection createInternalDbServerConnection() {

        if (ds == null) ds = createDefaultDataSource();
        if (ds == null) {
            return null; // failed
        }
        Connection conn = ds.createInternalDbServerConnection();
        if (conn == null) System.err.println("ERROR DataSource.createInternalDbServerConnection = NULL");
        else ds.set(conn); // set the connection description, in case a new connection is invoked

        return conn;
    }

    /** Creates a SQLDataSourceIF of the specified classname subtype for the static source.
      * No connection is created. */
    public static SQLDataSourceIF createDataSource(String className) {
      ds = AbstractSQLDataSource.createDataSource(className);
      if (ds == null) System.err.println("ERROR DataSource.createDefaultDataSource = NULL: " + className);
      return ds;
    }

    /** Return the static source 
     * @return null source not defined
     */
    public static SQLDataSourceIF getSource() {
       return ds;
    }

    /*****************************************************************************/
    //Static wrappers around the static SQLDataSourceIF data member instance methods follow.
    /*****************************************************************************/

    public static Connection getConnection() {
      if (ds == null) {
        try {
          System.err.println("ERROR DataSource.getConnection() null.");
          System.err.println();
          System.err.println(" First create a DataSource instance, e.g. invoke either:");
          System.err.println(" DataSource.createDefaultDataSource(); DataSource.createDataSource(className);");
          throw new NullPointerException();
        } catch (NullPointerException ex) {
          ex.printStackTrace();
        }
        return null;
      }
      else return ds.getConnection();
    }

    public static void setWriteBackEnabled(boolean tf) {
      ds.setWriteBackEnabled(tf);
    }

    public static boolean isWriteBackEnabled() {
      return ds.isWriteBackEnabled();
    }

    public static boolean set(DbaseConnectionDescription descript) {
      return ds.set(descript);
    }

    public static DbaseConnectionDescription getDbaseConnectionDescription() {
      return ds.getDbaseConnectionDescription();
    }

    public static String getDriver() {
        return ds.getDriver();
    }
    public static String getHostName() {
      return ds.getHostName();
    }

    public static String getIPAddress() {
      return ds.getIPAddress();
    }

    public static String getUserName() {
      return ds.getUserName();
    }

    public static int getPort() {
      return ds.getPort();
    }

    public static String getDbaseName() {
      return ds.getDbaseName();
    }

    /* removed block for UTC conversion -aww 2008/02/11 
    //Returns String describing whether the data times are simple nominal epoch times or leap seconds corrected epoch times. 
    public static String getDbTimeBaseString() {
      return dbTimeBaseStr[dbTimeBase];
    }
    // Data source epoch times are nominal seconds (the default).
    public static void setDbTimeBaseNominal() {
      dbTimeBase = NOMINAL_TYPE;
    }
    // Returns true if the data source epoch times are stored as nominal seconds.
    public static boolean isDbTimeBaseNominal() {
      return (dbTimeBase == NOMINAL_TYPE);
    }
    // Data source epoch times are leap seconds corrected. 
    public static void setDbTimeBaseLeap() {
      dbTimeBase = LEAP_TYPE;
    }
    //Returns true if the data source epoch times are stored as leap seconds corrected. 
    public static boolean isDbTimeBaseLeap() {
      return (dbTimeBase == LEAP_TYPE);
    }
    public static void dbTimeBaseToNominal(DataNumber datetime) {
        if (! datetime.isValidNumber() ) return; // null data
        datetime.setValue(dbTimeBaseToNominal(datetime.doubleValue()));
    }
    public static void nominalToDbTimeBase(DataNumber datetime) {
        if (! datetime.isValidNumber() ) return; // null data
        datetime.setValue(nominalToDbTimeBase(datetime.doubleValue()));
    }
    public static double dbTimeBaseToNominal(double datetime) {
        if (Double.isNaN(datetime)) return datetime;
        return (isDbTimeBaseLeap()) ?
              LeapSeconds.trueToNominal(datetime) : datetime; 
    }
    public static double nominalToDbTimeBase(double datetime) {
        if (Double.isNaN(datetime)) return datetime;
        return (isDbTimeBaseLeap()) ?
              LeapSeconds.nominalToTrue(datetime) : datetime; 
    }
    */

    public static String describeConnection() {
      return ds.describeConnection();
    }

    public static boolean set(Connection connection) {
       return ds.set(connection);
    }

    public static void set(String url,
                           String driver,
                           String username,
                           String passwd)
    {
        //ds.set(url, driver, username, passwd);// does it do the same as below-aww?
        DbaseConnectionDescription cd = getDbaseConnectionDescription();
        cd.setURL(url);
        cd.setDriver(driver);
        cd.setUserName(username);
        cd.setPassword(passwd);
    }

    public static Connection getNewConnect()
    {
      return ds.getNewConnect();
    }

    public static String getConnectionStatus() {
      if (isNull()) return "Null DataSource";
      return "Connection status: "+ds.getConnectionStatus() +
          (isClosed() ? " null/closed" : "");
    }

    public static boolean isReadOnly() {
      return ds.isReadOnly();
    }

    public static void commit(Connection connection) {
      ds.commit(connection);
    }

    public static void commit() {
      ds.commit();
    }

    public static void rollback() {
      ds.rollback();
    }

    public static void close() {
      final SQLDataSourceIF ds = DataSource.ds;
      if (ds != null && !ds.isClosed()) {
          ds.close();
      }
    }
    public static boolean isClosed() {
      return ds.isClosed();
    }
    public static boolean isNull() {
      return (ds == null);
    }

    public static boolean onServer() {
        if (ds == null) ds = createDefaultDataSource();
        return ds.onServer();
    }

    // Overrides Object instance method
    public String toString() {
      if (ds == null) return "NULL";
      StringBuffer sb = new StringBuffer(512);
      sb.append(ds.toString());
      //sb.append(" dbTimeBase: ").append(getDbTimeBaseString());
      return sb.toString();
    }

    public static String toDumpString() {
      if (ds == null) return "NULL";
      StringBuffer sb = new StringBuffer(512);
      sb.append(ds.toDumpString()).append(System.getProperty("line.separator"));
      //sb.append(" dbTimeBase: ").append(getDbTimeBaseString());
      return sb.toString();
    }

    public static void dump() {
      System.out.println(toDumpString());
    }

    public static JiggleConnection getJiggleConnection() {
        return ds.getJiggleConnection();
    }

    /*
    public static final class Tester {
      public static final void main(String args[]) {
       if (args.length < 3) {
         System.err.println("Syntax: DataSource$Tester <dbhost> <dbname> <dbuser> <dbpasswd>");
         System.exit(0);
       }
       final String driver = AbstractSQLDataSource.DEFAULT_DS_DRIVER;
       String host   = args[0];
       String dbname = args[1];
       String user   = args[2];
       String passwd = args[3];
       //TestDataSource jc = new TestDataSource.create(url, driver, user, passwd);    // make connection
       DataSource jc = TestDataSource.create(host, dbname, user, passwd);    // make connection
       if (jc != null) {
           System.out.println("Connection = "+jc.toString());
           System.out.println("Host  ="+jc.getHostName());
           System.out.println("Dbase ="+jc.getDbaseName());
           System.out.println("hasPrimaryRtRole       : "+ jc.hasPrimaryRtRole());
           System.out.println("isPrimaryHost('serverm1'): "+ jc.isPrimaryHost("serverm1"));
           System.out.println("isPrimaryHost('serverm2'): "+ jc.isPrimaryHost("serverm2"));
           System.out.println("isPrimaryHost('servermu'): "+ jc.isPrimaryHost("servermu"));
           System.out.println("getHostAppRole('serverm1','exportamps','ExportAmps'): " + getHostAppRole("serverm1", "exportamps", "ExportAmps")); 
           System.out.println("getHostAppRole('serverm2','exportamps','ExportAmps'): " + getHostAppRole("serverm2", "exportamps", "ExportAmps")); 
           System.out.println("getHostAppRole('serverc2','TEST','test-shadow'): " + getHostAppRole("serverc2", "TEST", "test-shadow")); 
           System.out.println("getHostAppRole('serverc2','DNE','test-dne'): " + getHostAppRole("serverc2", "DNE", "test-dne")); 
           jc.close();
       }
     }
   } // end of Tester
   */
} // DataSource
