package org.trinet.jasi;

import java.util.*;

import org.trinet.util.*;
import org.trinet.util.WaveClient; 
import org.trinet.util.gazetteer.LatLonZ;
import org.trinet.web.TriggerTN;

/**
 * ChannelTimeWindowModel that returns a list of ChannelTimeWindow that is
 * derived from the waveforms associated with this solution in the DataSource.
 */

public class DataSourceChannelTimeModel extends ChannelTimeWindowModel {

    {
        setModelName("DataSource");
        setExplanation("channels associated with database waveforms ");
    }

    private boolean triggerLLZearliestChan = false;

    public DataSourceChannelTimeModel() {
        setMyDefaultProperties();
    }

    public DataSourceChannelTimeModel(GenericPropertyList gpl, Solution sol, ChannelableList candidateList) {
        super(gpl, sol, candidateList);
    }

    public DataSourceChannelTimeModel(Solution sol, ChannelableList candidateList) {
        super(sol, candidateList);
    }

    public DataSourceChannelTimeModel(ChannelableList candidateList) {
        super(candidateList);
    }

    public DataSourceChannelTimeModel(Solution sol) {
        this();
        super.setSolution(sol);
    }

     /** Return a Collection of Waveform objects selected by the model.
     * They will not contain time-series yet. To load time-series you must set
     * the loader mode with a call to AbstractWaveform.setWaveDataSource(Object)
     * and then invoke the loadTimeSeries() of each Waveform instance.<p>
     *
     * @see: AbstractWaveform
     */
    public List getWaveformList() {
        if (debug) System.out.println(getModelName() + " DEBUG entering getWaveformList() ...");
        if (sol == null) {
          System.err.println(getModelName() + ": ERROR Required solution reference is null, no-op.");
          return null; // return null here, to be consistent with parent method fatal error.
        }

        // If input include (phases, amps etc.) properties are set, load required channel list 
        setRequiredChannelList(new ChannelableList()); // does sort

        int reqCount = requiredList.size();

        double maxDist = getMaxDistance();
        double dist = 0.;
        Channelable ch = null;
        ChannelableList reqList = (includeAllComponents) ? new ChannelTimeWindowSetList(reqCount) : new ChannelableList(reqCount);
        for (int i=0; i< reqCount; i++) {
          ch = (Channelable) requiredList.get(i);
          //System.out.print(ch.toString());
          dist = ch.getHorizontalDistance();
          if (dist > maxDist) { 
            if ( !sol.isEventJasiType(EventTypeMap3.getMap().toJasiType(EventTypeMap3.TRIGGER)) || triggerSortOrder.equalsIgnoreCase("DIST") ) {
                System.out.println(getModelName() + " DEBUG dist > maxDist, skipping : "+ ch.getChannelObj().toDelimitedSeedNameString()); 
                break;
            }
          }
          reqList.add(new ChannelTimeWindow(ch.getChannelObj(), getTimeWindow(dist)));
        }

        reqCount = reqList.size(); // total effected by maxDist truncation, if any
        if (debug) System.out.println(getModelName() + " DEBUG getWaveformList() required channel list size: " + reqCount);

        ChannelableList cwfList = new ChannelableList(reqCount);
        if (reqCount > 0) {
          // Create new Waveform from ChannelTimeWindow, some picks might not map to a db waveform entry
          ChannelTimeWindow ctw = null;
          for (int i = 0; i < reqCount; i++) {
              ctw = (ChannelTimeWindow) reqList.get(i);
              Waveform wf = AbstractWaveform.createDefaultWaveform();
              wf.setChannelObj(ctw.getChannelObj());
              wf.setTimeSpan(ctw.getTimeSpan());
              wf.setAmpUnits(Units.COUNTS); // default to this - aww 09/08/2006
              cwfList.add(wf);
          }
        }

        // Get db AssocWaE waveform data
        if (debug) System.out.println(getModelName() + " DEBUG getWaveformList()  getting solution's waveforms from db ...");
        ArrayList swfList = (ArrayList) AbstractWaveform.createDefaultWaveform().getBySolution(sol);
        int solWfCount = swfList.size();
        if (debug) System.out.println(getModelName() + " DEBUG getWaveformList() list size: " + solWfCount);

        if (swfList == null || solWfCount == 0) return cwfList; // return required list
        if (debug) System.out.println(getModelName() + " DEBUG getWaveformList() first/last elements: \n"+swfList.get(0)+"\n"+swfList.get(solWfCount-1));
       

        ChannelableList comboList = new ChannelableList(reqCount+solWfCount);

        // First add the pick required waveforms missing from the solution's db waveform list
        Outer:
        for (int idx=0; idx < reqCount; idx++) {
              Waveform cwf = (Waveform) cwfList.get(idx);
              for (int jdx=0; jdx < solWfCount; jdx++) {
                  if ( cwf.equalsChannelId((Waveform) swfList.get(jdx)) ) continue Outer;
              }
              System.out.println(
                  "INFO: DataSourceChannelTimeModel waveform is absent, but parametric data exist for chan: " +
                  cwf.getChannelObj().toDelimitedSeedNameString()
              );
              comboList.add(cwf); // add, since no matching db waveform channel for solution
        }
        
        int reqCnt = comboList.size();

        // Now add all of the solution's waveform list
        java.util.Date solDate = sol.getDateTime(); // aww
        AbstractWaveform awf = null;
        boolean resetSpan = !windowDurationType.equalsIgnoreCase("default"); // added -aww 2010/09/02
        for (int jdx=0; jdx < solWfCount; jdx++) {
            awf = (AbstractWaveform) swfList.get(jdx);
            // Need to truncate ? also need to consider pre-event, post-event properties for window start and stop ?
            /*
            if (awf.getTimeSpan().getDuration() > maxWindow) {
                awf.setEnd(awf.getEpochStart() + maxWindow);
                //awf.setTruncated();
            }
            */
            if (debug) System.out.println("DEBUG DataSourceChannelTimeModel getWaveformList() channel from AssocWaE: " +
                    awf.getChannelObj().toDelimitedSeedNameString());
            // Do we lookup LatLonZ to determine distances and possibly restrict window time span?
            if (maxDist < Double.MAX_VALUE || resetSpan) { // added condition -aww 2010/09/02
              awf.matchChannelWithDataSource(solDate); // uses masterlist else goes to db
              awf.calcDistance(sol);
              dist = awf.getHorizontalDistance();
              if (awf.getHorizontalDistance() > maxDist) {
                //if ( !sol.isEventJasiType(EventTypeMap3.getMap().toJasiType(EventTypeMap3.TRIGGER)) || triggerSortOrder.equalsIgnoreCase("DIST") ) {
                // 2013/06/19 - removed above condition because if the model trigger sort property is set to DIST and the maxDist<MAX_VALUE 
                // that causes all channels to be rejected here before the earliest triggered one can be set as origin location in next block below
                if ( !sol.isEventJasiType(EventTypeMap3.getMap().toJasiType(EventTypeMap3.TRIGGER)) ) {
                  if (debug) System.out.println("DEBUG DataSourceChannelTimeModel skipping " + awf.getChannelObj().toDelimitedSeedNameString() +
                          " dist="+new Format("%8.1f").form(dist)+ " > maxDist=" + maxDist );
                  continue; // skip at user's max distance request
                }
              }
              if (resetSpan) { // this may truncate a long trigger window -aww 2010/09/02
                  awf.setTimeSpan( getTimeWindow(dist) );
              }
            }
            comboList.add(awf);
        }

        if (comboList.size() == reqCnt) {
            System.out.println("INFO: DataSourceChannelTimeModel found 0 ! waveform channels within maxDist = " + maxDist);
        }

        if (debug) System.out.println(getModelName() + " DEBUG DSCTM: triggerSortOrder="+triggerSortOrder);

        // If's its located its type should no long be 'st' subnet trigger so:
        if (! sol.isEventJasiType(EventTypeMap3.getMap().toJasiType(EventTypeMap3.TRIGGER))) { // local, regional, quarry etc.
            //Note that if maxDist property is not set, Waveform's channel distance is undefined above,
            //in which case required channels with LatLonZ, but no waveform entry will sort to top of list
            //otherwise these channels are interleaved with those having waveform's in sorted distance order

            if ( !triggerSortOrder.equalsIgnoreCase("PICK") ) { // default to distance order
                comboList.distanceSort(sol);
            }
            else { // do phase pick sort
                // NOTE: added "PICK" sort option here for case of reviewing whether picks are "cross-feed" analog EHZ components  - aww 2012/02/06
                // usually waveform panels/phases are sorted in channel distance order, for the PICK case here panels are sorted in arrival time 
                // order, user should configure display to show only panels with picks (i.e. hiding those panels without picks).

                // For phases to be loaded here, model must have "includePhases=false" or no phases,
                // NOTE: the model loads phases before the masterview loads phases
                if ( sol.getPhaseList().size() == 0 && !getIncludePhases() ) { // try to load
                    sol.listenToPhaseListChange(false);
                    if (sol.getPhaseList().isEmpty()) sol.loadPhaseList(); // load if empty list
                    sol.listenToPhaseListChange(true);
                }

                if ( sol.getPhaseList().size() == 0) { // have none
                    comboList.distanceSort(sol); // so default to distance sort, no phases
                }
                else { // have we have phases, so sort waveforms by arrival times
                    ChannelableList ctwList = new ChannelableList(comboList.size());
                    Waveform wf = null;
                    for (int idx=0; idx<comboList.size(); idx++) {
                      wf = (Waveform) comboList.get(idx);
                      ctwList.add( new TriggerChannelTimeWindow( wf.getChannelObj(), wf.getTimeSpan()) );
                    }
                    PhaseList phList = sol.getPhaseList();
                    Phase ph = null;
                    TriggerChannelTimeWindow ctw = null;
                    for (int idx=0; idx<phList.size(); idx++) {
                        ph = (Phase) phList.get(idx);
                        ctw = (TriggerChannelTimeWindow) ctwList.getByChannel(ph);
                        if (ctw != null) {
                            ctw.setTriggerTime(ph.getTime());
                            ctw.setTriggerType(ph.description.iphase);
                        }
                    }

                    // NOTE: waveforms without arrivals are in name order at end of list
                    comboList = getWfListSortedByTriggerTimes(comboList, ctwList);
                }

            }
        }
        else { // trigger

            // A list of triggered channels as stored in the Trig_Channel table by CarlSubTrig
            // It only has H_Z channels and all start and end at the same time.
            // Triggered channels may not be in this model's Candidate list.
            // Can't Limit distance to user's requested maximum Triggers have lat/lon = 0.0/0.0 !!
            // So they'er all 99,999 km away and a distance cut-off will throw them all out!

            boolean nullLLZ = sol.getLatLonZ().isNull();
            if (! nullLLZ) { // When solution's llz is not null, it trumps!
                // Use existing solution's location and sort by distance, location may have been hand-set by user !!!
                comboList.distanceSort(sol);
            }
            else if (triggerSortOrder.equalsIgnoreCase("NAME")) { // station alpha order
                // this is the "old way" if input Solution LatLonZ null (0,0,0)
                comboList.sortByStation();
            }
            else if (triggerSortOrder.equalsIgnoreCase("TIME") || triggerSortOrder.equalsIgnoreCase("PICK") ) { // only for null solution llz
              // Sort by trigger times
              TriggerTN trigger = new TriggerTN();
              trigger.setSolution(sol);  //this creates trigger ChannelTimeWindow list
              ChannelableList ctwList = trigger.getChannelTimeWindows();
              if (ctwList.size() > 0) { // have windows
                comboList = getWfListSortedByTriggerTimes(comboList, ctwList);
              }
              else System.out.println(getModelName() + " Error : No TriggerChannelTimeWindow found for event trigger: " + sol.getId().longValue());
            }
            else { // only for null solution llz, distance from channel with earliest trigger time
                TriggerTN trigger = new TriggerTN();
                trigger.setSolution(sol);  //this creates the trigger ChannelTimeWindow list

                TriggerChannelTimeWindow tctw = trigger.getEarliestChannel();
                Channel chan1 = tctw.getChannelObj();
                LatLonZ loc = null;
                if (chan1 != null) {
                    chan1 = chan1.lookUp(chan1, sol.getDateTime()); // lookup channel lat/lon info
                    loc = chan1.getLatLonZ();
                }
                if (loc != null) {
                  // Set event origin at the location of earliest channel then sort by distance
                  if (triggerLLZearliestChan) {
                      boolean tf = sol.hasChanged();
                      sol.setLatLonZ(loc.getLat(), loc.getLon(), 0.1); //make depth=0.1 here - aww
                      sol.setTime(tctw.triggerTime);
                      if (!tf) sol.setUpdate(false);
                  }
                  // NOTE above causes trigger.sol.hasChanged() to return true, thus prompting for save on program exit/new event load
                  comboList.distanceSort(loc); // sort by distance from channel
                }
                else System.out.println(getModelName() + " Warning : No earliest channel lat,lon found for event trigger: " + sol.getId().longValue());
            }
        }
        //System.out.println("filterWfListByChannelList="+filterWfListByChannelList);
        // keep only those requested by filtering attributes the candidate list 
        comboList = filterListByChannelPropertyAttributes(comboList);
        if (filterWfListByChannelList) comboList = filterListByChannelList(comboList);

        // truncate list at user's requested channel limit
        return (comboList.size() > getMaxChannels()) ? new ChannelableList(comboList.subList(0, getMaxChannels())) : comboList;
    }

    private ChannelableList getWfListSortedByTriggerTimes(ChannelableList comboList, ChannelableList ctwList) {

                int count = ctwList.size(); // ChannelTimeWindow list 

                // create a new "set" here so all orientations are are added
                ChannelableList newList1 = new TriggerChannelTimeWindowSetList(count);

                for (int idx=0; idx< count; idx++) { // add all triaxial channels to new trigger list set
                  newList1.add(((Channelable) ctwList.get(idx))); // adds related triaxial components
                }

                // sort new triaxial list by earliest triggered station
                newList1.sort(new TriggerChannelTimeWindowSorter(TriggerChannelTimeWindowSorter.OLDEST_FIRST));

                // new waveform list
                ChannelableList newList2 = new ChannelableList(comboList.size()); // max size

                // Note guarantee that sizes of new trigger list and waveform combo list are the same
                count = newList1.size();
                ChannelTimeWindow ctw = null;
                AbstractWaveform awf = null;
                for (int idx = 0; idx < count; idx++) { // loop over trigger ctw list
                    ctw = (ChannelTimeWindow) newList1.get(idx); // the sorted trigger ctw
                    awf = (AbstractWaveform) comboList.getByChannel(ctw); // find the matching channel in waveform list
                    if (awf != null) newList2.add(awf); // append to new list in trigger list order
                }

                return newList2; // reshuffled ctw windows

    }

  private class TriggerChannelTimeWindowSorter implements Comparator {

    public static final int RECENT_FIRST = 0;
    public static final int OLDEST_FIRST = 1;

    int order = RECENT_FIRST;

    ChannelNameSorter cnSorter = new ChannelNameSorter();

    /** 'ORDER' is either RECENT_FIRST or OLDEST_FIRST */
    TriggerChannelTimeWindowSorter(int order) {
      this.order = order;
    }

    public int compare(Object o1, Object o2) {

      if ( ! (o1 instanceof TriggerChannelTimeWindow) && (o2 instanceof TriggerChannelTimeWindow)) {
         return 0;  // wrong object types, void class cast exception
      }

      TriggerChannelTimeWindow jr1 = (TriggerChannelTimeWindow) o1;
      TriggerChannelTimeWindow jr2 = (TriggerChannelTimeWindow) o2;

      double tt1 = jr1.getTriggerTime();
      double tt2 = jr2.getTriggerTime();
      // Sort unknown times to end
      if (tt1 == 0.) {
         if (tt2 == 0.) return cnSorter.compare( jr1.getChannelObj(), jr2.getChannelObj());
         else if (tt2 != 0.) return 1;
      }
      else if (tt1 != 0. && tt2 == 0.) return -1;

      double diff = tt1 - tt2;

      // double diff = jr1.getTriggerTime() - jr2.getTriggerTime(); // checked ok -aww 2008/02/11

      if (Math.abs(diff) < .00001) diff = 0.; // added this test to handle weird roundoff? -aww 2010/10/20

      if (diff < 0.0) {
        return (order == RECENT_FIRST) ? 1 : -1;
      } else if (diff > 0.0) {
        return (order == RECENT_FIRST) ? -1 : 1;
      } else { // same time sort by component type
        return cnSorter.compare( jr1.getChannelObj(), jr2.getChannelObj());
      }
    }
  }

    // Override doesn't need to set the waveform's savingEvent since it's done via parse of result set returned from db query
    public List getWaveformList(Solution sol) {
        if (sol == null) {
          System.err.println(getModelName() + " : ERROR getWaveformList input solution is null");
          return null;
        }
        setSolution(sol);

        return getWaveformList();
    }

    public ChannelableList getChannelTimeWindowList() {
        if (debug) System.out.println(getModelName() + " DEBUG entering getChannelTimeWindowList() ...");

        ArrayList wfList = (ArrayList) getWaveformList();
        if (wfList == null) return null;

        int count = wfList.size();
        if (count <= 0) return new ChannelableList(0);

        Waveform wf = null;
        ChannelableList ctwList = new ChannelableList(count);
        // Change Waveforms to ChannelTimeWindow's, filtering was done already
        for (int i = 0; i < count; i++) {
          wf = (Waveform) wfList.get(i);
          ctwList.add( new ChannelTimeWindow(wf.getChannelObj(), wf.getTimeSpan()) );
        }
        return ctwList;
    }

    public void setDefaultProperties() {
        super.setDefaultProperties();
        setMyDefaultProperties();
    }

    private void setMyDefaultProperties() {
        includePhases = false;
        includeAllComponents = false;
        includeMagAmps = false;
        includePeakAmps = false;
        includeCodas = false;
        triggerLLZearliestChan=false;
        filterWfListByChannelList = false;
    }

    public void setProperties(GenericPropertyList props) {
        super.setProperties(props);   /// get all the standard stuff
        if (props != null) {
          String pre = getPropertyPrefix();
          if (props.isSpecified(pre+"triggerLLZearliestChan") )
              triggerLLZearliestChan = props.getBoolean(pre+"triggerLLZearliestChan");
        }
    }

    public String getParameterDescriptionString () {
        StringBuffer sb = new StringBuffer(1024);
        sb.append(super.getParameterDescriptionString());
        String pre = getPropertyPrefix(); 
        sb.append(pre).append("triggerLLZearliestChan = ").append(triggerLLZearliestChan).append("\n"); 
        return sb.toString();
    }

    public ArrayList getKnownPropertyKeys() {
        ArrayList list = super.getKnownPropertyKeys();
        String pre = getPropertyPrefix(); 
        list.add(pre + "triggerLLZearliestChan");
        return list;
    }

/* ///////////////////////////////////////////////////////
  public static class Tester {
    public static void main(String args[]) {
        if (args.length <= 0) {

          System.out.println ("Usage: DataSourceChannelTimeModel [evid] ");
          System.exit(0);

        }

        // event ID
        Long val = Long.valueOf(args[0]);
        long evid = (long) val.longValue();

        DataSource db = TestDataSource.create();    // make connection
        System.out.println (db.toString());

        Solution sol    = Solution.create().getById(evid);

        if (sol == null) {
            System.out.println ("No such event: "+evid);
            System.exit(0);
        }

        System.out.println (sol.toString());

        System.out.println ("Reading in current channel info...");
        String compList[] = {"EH_", "HH_", "HL_", "AS_"};
        ChannelList chanList = ChannelList.getByComponent(compList);

        DataSourceChannelTimeModel model =
            new DataSourceChannelTimeModel(sol, chanList);

        //ArrayList wfList = (ArrayList) model.getWaveformList();
        //System.out.println ("Channel count = "+wfList.size());

        // test loading an event from this model
        org.trinet.jiggle.MasterView mv = new org.trinet.jiggle.MasterView();
        mv.setWaveformLoadMode(org.trinet.jiggle.MasterView.LoadNone);
        mv.addSolution(sol);
        mv.setSelectedSolution(sol);
        mv.defineByChannelTimeWindowModel(model);

        System.out.println ("wfview count = "+ mv.wfvList.size());
        System.out.println (mv.dumpToString());

    } // end of main
  } // end of Tester
*/
} // end of DataSourceChannelTimeModel 
