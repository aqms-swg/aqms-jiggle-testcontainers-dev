package org.trinet.jasi;
import java.util.*;
import org.trinet.jdbc.*;
public interface DbReadableJasiChannelObjectIF extends DbReadableJasiObjectIF {
  /* Select rows without any date restriction. */
  public String toChannelSQLSelectPrefix();
  /* Select rows valid for date, null defaults to current time. */
  public String toChannelSQLSelectPrefix(java.util.Date date);
}
