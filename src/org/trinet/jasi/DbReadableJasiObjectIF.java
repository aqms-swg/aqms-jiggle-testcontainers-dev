package org.trinet.jasi;
import org.trinet.jdbc.*;
public interface DbReadableJasiObjectIF extends ParseableJasiObjectIF {
  public JasiObject parseResultSetDb(ResultSetDb rsdb);
  public JasiObject parseData(Object rsdb);
}
