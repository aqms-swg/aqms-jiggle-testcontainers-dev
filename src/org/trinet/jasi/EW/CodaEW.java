package org.trinet.jasi.EW;

import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;
import org.trinet.jasi.TN.CodaTN;
import org.trinet.jdbc.datatypes.*;
/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

//
// For now convenience to compile just extend TN class
// still need to implement abstract Coda methods here -aww
//
public class CodaEW extends CodaTN
{

    EWCoda ewCoda = null;
    long idTCoda;
    long idCodaDur;
    long idMagLink;
  
  
    public CodaEW() { }
  
    public Collection getBySolution(Solution sol)
    {
      long Temp_idMag = 0;
      Magnitude Temp_Mag = null;
  
      if(ewCoda == null) ewCoda = new EWCoda();
  
      if(sol.magnitude != null)
      {
        if(sol.magnitude.subScript.toString().charAt(0) == 'd')
        {
          Temp_Mag = sol.magnitude;
          Temp_idMag = sol.magnitude.magid.longValue();
        }
      }
  
      if(Temp_idMag == 0)
      {
        //      (check altmaglist)
        if(sol.altMagList != null)
        {
          for(int i=0; i < sol.altMagList.size(); i++)
          {
            if(sol.altMagList.get(i) != null)
            {
              Temp_Mag = (Magnitude)(sol.altMagList.get(i));
              if(Temp_Mag.subScript.toString().charAt(0) == 'd')
              {
                Temp_idMag = Temp_Mag.magid.longValue();
                break;
              }
            }  // if maglist(i) != null
          }  // end for i in maglist
        }  // end if maglist != null
      }  // end if Temp_idMag == 0 (check altmaglist)
  
      if(Temp_idMag == 0)
      {
        // we didn't find any relevant magnitudes.
        //  Return an empty collection.
        return((Collection)new ArrayList());
      }
      else
      {
        // we got a mag, go get the data.
  
        Vector vEWCodaList;
        CodaEW codaEW;
  
        if(ewCoda == null)
          ewCoda = new EWCoda();
  
        ewCoda.idMagnitude = Temp_idMag;
        if(ewCoda.idMagnitude <= 0)
          return((Collection)new ArrayList());
  
  
        vEWCodaList = ewCoda.ReadList();
        if(vEWCodaList == null)
          return((Collection)new ArrayList());
  
        Vector vCodaEWList = new Vector(vEWCodaList.size());
  
        // Phase[] Phases = sol.phaseList.getArray();
        for(int i=0; i < vEWCodaList.size(); i++)
        {
          codaEW = CodaEW.EWCoda_2_CodaEW((EWCoda)(vEWCodaList.get(i)));
          codaEW.sol = sol;  // relationships are checked X-ways from Wednesday
          codaEW.magnitude = Temp_Mag;  // relationships are checked X-ways from Wednesday
          if(codaEW != null)
          {
            codaEW.magnitude = Temp_Mag;
            vCodaEWList.add((Object)codaEW);
  /*****************************
   * NO Md CODE (no coda member in Phase)
          int j;
          for(j=0; j < Phases.length; j++)
            {
              if(Phases[j].getChannelObj().equalsName(codaEW.getChannelObj()) &&
                 Phases[j].description.iphase.equals("P"))
              {
                Phases[j].coda = (Coda)codaEW;
                break;
              }
            }
            if(j == Phases.length)
              System.out.println("No phase found for coda " + codaEW.getChannelObj().getSta().toString());
   **********************************/
          }
        }
  
        if(Temp_Mag.codaList == null)
           Temp_Mag.codaList = CodaList.create();
        else
           Temp_Mag.codaList.clear();
        Temp_Mag.codaList.fastAddAll((Collection)vCodaEWList);
        sol.codaList = Temp_Mag.codaList;
  
        return((Collection)vCodaEWList);
      }  // end else
    }  // end getBySolution()
  
  
    protected static CodaEW EWCoda_2_CodaEW(EWCoda ewCoda)
    {
      CodaEW codaEW = new CodaEW();
  
      if(ewCoda == null)
        return(null);
  
      codaEW.idTCoda = ewCoda.idTCoda;
      codaEW.idCodaDur = ewCoda.idCodaDur;
      codaEW.tCodaTerm = new DataDouble(ewCoda.tCodaTermXtp);
      codaEW.tau = new DataDouble(ewCoda.tCodaDurXtp);
      codaEW.channelMag.setMagValue(ewCoda.dMag);
      codaEW.channelMag.setWeight(Coda.toQuality((int)ewCoda.dWeight));
      codaEW.channelMag.setType("d");
      codaEW.setQuality(Coda.toQuality((int)ewCoda.dWeight));
      codaEW.setChannelObj(ChannelEW.lookUp(ewCoda.idChan));
      codaEW.idMagLink = ewCoda.idMagLink;
      return(codaEW);
    }
  
    protected EWCoda toEWCoda()
    {
      ewCoda = new EWCoda();
      ewCoda.idTCoda = this.idTCoda;
      ewCoda.idCodaDur = this.idCodaDur;
      ewCoda.tCodaTermXtp = this.tCodaTerm.doubleValue();
      ewCoda.tCodaDurXtp = this.tau.doubleValue();
      ewCoda.tCodaTermObs = 0;
      ewCoda.tCodaDurObs  = 0;
      ewCoda.dMag = this.channelMag.value.floatValue();
      ewCoda.dWeight = toWeight(this.channelMag.weight.doubleValue());
      ewCoda.idChan = ((ChannelEW)this.getChannelObj()).idChan;
      return(ewCoda);
    }
  
    protected EWCoda toEWCoda(Phase IN_Phase)
    {
      toEWCoda();
      if(IN_Phase instanceof PhaseEW)
      {
        ewCoda.idPick = ((PhaseEW)IN_Phase).idPick;
        ewCoda.idMagLink = this.idMagLink;
        ewCoda.idMagnitude = this.magnitude.magid.longValue();
        return(ewCoda);
      }
      else
      {
        ewCoda.idPick = -1;
        return(null);
      }
    }
  
  
    public boolean commit()
    {
      if(this.idTCoda <= 0)
      {
        toEWCoda();
        ewCoda.Write();
  
        if(ewCoda.idTCoda <= 0)
          return(false);
        else
        {
          this.idTCoda = ewCoda.idTCoda;
          return(true);
        }
      }
      return(false);
    }
  
    public boolean equals(Object obj)
    {
        if(obj == this)
        {
          return(true);
        }
        else
        {
          if(this.getChannelObj() == ((CodaEW)obj).getChannelObj())
          {
            return(true);
          }
          else
          {
            return(false);
          }
        }
    }
  
    public boolean commitCodaDur(Phase IN_Phase)
    {
        if(this.idCodaDur <= 0)
        {
          toEWCoda(IN_Phase);
  
          ewCoda.Associate();
          if(ewCoda.idCodaDur <= 0)
            return(false);
          else
          {
            this.idCodaDur = ewCoda.idCodaDur;
            ewCoda.idDatum = ewCoda.idCodaDur;
          }
  
          ewCoda.WriteStaMag();
          if(ewCoda.idMagLink <= 0)
            return(false);
          else
          {
            this.idMagLink = ewCoda.idMagLink;
            return(true);
          }
  
        }
        return(false);
    }
}
//
// end CodaEW class
//
