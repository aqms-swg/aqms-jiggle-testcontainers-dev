
// *** PACKAGE NAME *** \
package org.trinet.jasi.EW;


// *** IMPORTS *** \
import java.util.*;
import java.sql.Connection;
import org.trinet.jasi.EW.SQL.EWSQLStatement;

// individual SQL calls
import org.trinet.jasi.EW.SQL.GetStaMagCodaStatement;
import org.trinet.jasi.EW.SQL.CreateTCodaStatement;
import org.trinet.jasi.EW.SQL.CreateCodaDurStatement;
import org.trinet.jasi.EW.SQL.CreateStaMagStatement;

/**
 * <!-- Class Description>
 *
 *
 * Created:  2002/07/19
 *
 * @author   ew_oci2java Tool / DK
 * @version  0.99
 **/



public class EWCoda
{


  // *** CLASS ATTRIBUTES *** \
  public long idMagLink; // Description
  public long idCodaDur; // Description
  public float dMag; // Description
  public float dWeight; // Description
  public long idChan; // Description
  public double tCodaDurObs; // Description
  public double tCodaDurXtp; // Description
  public double tCodaTermObs; // Description
  public double tCodaTermXtp; // Description
  public long idTCoda; // Description
  public int iMagType; // Description
  public long idMagnitude; // Description

  public long idPick; // Description

  public String sExternalTableName; // Description
  public String xidExternal; // Description

  public long idDatum; // Description

  // *** CLASS SQL STATEMENTS *** \
  EWSQLStatement es1;
  EWSQLStatement es2;
  EWSQLStatement es3;
  EWSQLStatement es4;


  // *** CONSTRUCTORS *** \
  public EWCoda()
  {
    es1 = new GetStaMagCodaStatement(org.trinet.jasi.DataSource.getConnection());
    es2 = new CreateTCodaStatement(org.trinet.jasi.DataSource.getConnection());
    es3 = new CreateCodaDurStatement(org.trinet.jasi.DataSource.getConnection());
    es4 = new CreateStaMagStatement(org.trinet.jasi.DataSource.getConnection());
  }


  // *** CLASS METHODS *** \
  public void SetConnection(Connection IN_conn)
  {
    es1.SetConnection(IN_conn);
    es2.SetConnection(IN_conn);
    es3.SetConnection(IN_conn);
    es4.SetConnection(IN_conn);
  }


  public Vector ReadList()
  {
    Vector ResultList = new Vector();
    es1.CallQueryStatement((Object)this, ResultList);
    return(ResultList);
  }  // end EWCoda:ReadList()

  public EWCoda Write()
  {

    if(es2.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWCoda:Write()

  public EWCoda Associate()
  {

    if(es3.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWCoda:Associate()

  public EWCoda WriteStaMag()
  {

    if(es4.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWCoda:WriteStaMag()


}  // end class EWCoda
