
// *** PACKAGE NAME *** \
package org.trinet.jasi.EW;


// *** IMPORTS *** \
import java.util.*;
import java.sql.Connection;
import org.trinet.jasi.EW.SQL.EWSQLStatement;

// individual SQL calls
import org.trinet.jasi.EW.SQL.CreateEventLockStatement;
import org.trinet.jasi.EW.SQL.DeleteEventLockStatement;
import org.trinet.jasi.EW.SQL.GetLockListStatement;

/**
 * <!-- Class Description>
 *
 *
 * Created:  2002/07/19
 *
 * @author   ew_oci2java Tool / DK
 * @version  0.99
 **/



public class EWSolutionLock
{


  // *** CLASS ATTRIBUTES *** \
  public long idEvent; // id of the Lock/Event
  public long idSource; // id of the owner of the Lock
  public String sSource; // Name of the owner of the Lock
  public String sMachineName; // Machine the lock was applied from
  public int iLockTime;       // Time the lock was made.
  public String sNote;        // Note regarding the lock
  public int iRetCode;       // Return code from SQL call

  // *** CLASS SQL STATEMENTS *** \
  EWSQLStatement es1;
  EWSQLStatement es2;
  EWSQLStatement es3;
  EWSQLStatement es4;
  EWSQLStatement es5;


  // *** CONSTRUCTORS *** \
  public EWSolutionLock()
  {
    es1 = new CreateEventLockStatement(org.trinet.jasi.DataSource.getConnection());
    es2 = new DeleteEventLockStatement(org.trinet.jasi.DataSource.getConnection());
    es3 = new GetLockListStatement(org.trinet.jasi.DataSource.getConnection());
//    es4 = new GetAllLocksStatement(org.trinet.jasi.DataSource.getConnection());
//    es5 = new DeleteAllLocksStatement(org.trinet.jasi.DataSource.getConnection());
  }


  // *** CLASS METHODS *** \
  public void SetConnection(Connection IN_conn)
  {
    es1.SetConnection(IN_conn);
    es2.SetConnection(IN_conn);
    es3.SetConnection(IN_conn);
    es4.SetConnection(IN_conn);
    es5.SetConnection(IN_conn);
  }

  public EWSolutionLock Write()
  {

    if(es1.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWSolutionLock:Write()

  public EWSolutionLock Delete()
  {

    if(es2.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWSolutionLock:Write()


  public Vector ReadList()
  {
    Vector ResultList = new Vector();
    es3.CallQueryStatement((Object)this, ResultList);
    return(ResultList);
  }  // end EWSolutionLock:ReadList()


  public Vector ReadFullList()
  {
    Vector ResultList = new Vector();
    es4.CallQueryStatement((Object)this, ResultList);
    return(ResultList);
  }  // end EWSolutionLock:ReadList()


  public EWSolutionLock DeleteAll()
  {

    if(es5.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWSolutionLock:Write()



}  // end class EWSolutionLock
