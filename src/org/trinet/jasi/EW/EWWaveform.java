
// *** PACKAGE NAME *** \
package org.trinet.jasi.EW;


// *** IMPORTS *** \
import java.util.*;
import java.sql.Connection;
import org.trinet.jasi.EW.SQL.EWSQLStatement;

// individual SQL calls
import org.trinet.jasi.EW.SQL.UpdateWaveformStatement;
import org.trinet.jasi.EW.SQL.CheckidWaveformStatement;
import org.trinet.jasi.EW.SQL.UpdateWaveformDescStatement;
import org.trinet.jasi.EW.SQL.CreateWaveformDescStatement;
import org.trinet.jasi.EW.SQL.GetWaveformListwCompTStatement;
import org.trinet.jasi.EW.SQL.GetWaveformListStatement;
import org.trinet.jasi.EW.SQL.GetWaveformDescStatement;
import org.trinet.jasi.EW.SQL.CreateSnippetStatement;
import org.trinet.jasi.EW.SQL.GetWaveformStatement;

/**
 * <!-- Class Description>
 *
 *
 * Created:  2002/06/17
 *
 * @author   ew_oci2java Tool / DK
 * @version  0.99
 **/



public class EWWaveform
{


  // *** CLASS ATTRIBUTES *** \
  public long idWaveform; // Description
  public byte[] RawSnippet; // Description
  public int iByteLen; // Description
  public int RetCode; // Description
  public double tStart; // Description
  public double tEnd; // Description
  public int iDataFormat; // Description

  public long idChan; // Description

	// Event Binding Params
  public int bBindToEvent; // Description
  public long idEvent; // Description


  // Channel Info
	//EWChannel  ewChannel;
  // Channel public long idChan; // Description
  public double tOff; // Description
  public double tOn; // Description
  public String sSta; // Description
  public String sComp; // Description
  public String sNet; // Description
  public String sLoc; // Description
  public float dLat; // Description
  public float dLon; // Description
  public float dElev; // Description
  public float dAzm; // Description
  public float dDip; // Description
  // Channel public long idEvent; // Description



  // *** CLASS SQL STATEMENTS *** \
  EWSQLStatement es1;
  EWSQLStatement es2;
  EWSQLStatement es3;
  EWSQLStatement es4;
  EWSQLStatement es5;
  EWSQLStatement es6;
  EWSQLStatement es7;
  EWSQLStatement es8;
  EWSQLStatement es9;

  // *** CONSTRUCTORS *** \
  public EWWaveform()
  {
    es1 = new UpdateWaveformStatement(org.trinet.jasi.DataSource.getConnection());
    es2 = new CheckidWaveformStatement(org.trinet.jasi.DataSource.getConnection());
    es3 = new UpdateWaveformDescStatement(org.trinet.jasi.DataSource.getConnection());
    es4 = new CreateWaveformDescStatement(org.trinet.jasi.DataSource.getConnection());
    es5 = new GetWaveformListwCompTStatement(org.trinet.jasi.DataSource.getConnection());
    es6 = new GetWaveformListStatement(org.trinet.jasi.DataSource.getConnection());
    es7 = new GetWaveformDescStatement(org.trinet.jasi.DataSource.getConnection());
    es8 = new CreateSnippetStatement(org.trinet.jasi.DataSource.getConnection());
    es9 = new GetWaveformStatement(org.trinet.jasi.DataSource.getConnection());
  }


  // *** CLASS METHODS *** \
  public void SetConnection(Connection IN_conn)
  {
    es1.SetConnection(IN_conn);
    es2.SetConnection(IN_conn);
    es3.SetConnection(IN_conn);
    es4.SetConnection(IN_conn);
    es5.SetConnection(IN_conn);
    es6.SetConnection(IN_conn);
    es7.SetConnection(IN_conn);
    es8.SetConnection(IN_conn);
    es9.SetConnection(IN_conn);
  }


  public EWWaveform UpdateSnippet()
  {

    if(es1.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWWaveform:UpdateSnippet()


  public EWWaveform Check()
  {

    if(es2.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWWaveform:Check()


  public EWWaveform Update()
  {

    if(es3.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWWaveform:Update()


  public EWWaveform Write()
  {

    if(es4.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWWaveform:Write()


  public Vector ReadListwCompInfo()
  {
    Vector ResultList = new Vector();
    es5.CallQueryStatement((Object)this, ResultList);
    return(ResultList);
  }  // end EWWaveform:ReadListwCompInfo()


  public Vector ReadList()
  {
    Vector ResultList = new Vector();
    es6.CallQueryStatement((Object)this, ResultList);
    return(ResultList);
  }  // end EWWaveform:ReadList()


  public EWWaveform Read()
  {

    if(es7.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWWaveform:Read()


  public EWWaveform WriteSnippet()
  {

    if(es8.CallStatement((Object)this))
      return(this);
    else
      return(null);
  }  // end EWWaveform:WriteSnippet()


  public EWWaveform ReadSnippet()
  {
    Vector ResultList = new Vector();

    es9.CallQueryStatement((Object)this, ResultList);
    if(ResultList.size() > 0)
	this.RawSnippet = ((EWWaveform)(ResultList.get(0))).RawSnippet;
    else
        this.RawSnippet = null;
    return(this);
  }  // end EWWaveform:ReadSnippet()


}  // end class EWWaveform
