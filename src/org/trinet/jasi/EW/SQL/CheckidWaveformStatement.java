// *** PACKAGE NAME *** \                                     
package org.trinet.jasi.EW.SQL;                                
                                                               
                                                               
// *** IMPORTS *** \                                          
import java.util.*;                                            
import java.sql.Connection;                                    
import java.sql.SQLException;                                  
import org.trinet.jasi.EW.EWWaveform;                                  
                                                               
                                                               
/**                                    
 * <!-- Class Description>             
 *                                     
 *                                     
 * Created:  2002/06/17                        
 *                                     
 * @author   ew_oci2java Tool / DK                        
 * @version  0.99                        
 **/                                   
                                       
public class CheckidWaveformStatement extends EWSQLStatement 
{                                      
                                       
  // *** CLASS ATTRIBUTES *** \       
                                       
  // *** CONSTRUCTORS *** \           
  public CheckidWaveformStatement()                          
  {                                    
    sSQLStatement = "Begin   select idWaveform into :OUT_idWaveform    from Waveform    where idWaveform = :IN_idWaveform; EXCEPTION  WHEN OTHERS THEN   :OUT_idWaveform := -1; END;";
    bIsQuery = false;                
    init();                            
  }                                    
                                       
                                       
  public CheckidWaveformStatement(Connection IN_conn)        
  {                                    
    this();                            
    SetConnection(IN_conn);            
  }                                    
                                       
                                       
  // *** CLASS METHODS *** \          
                                       
  protected boolean SetInputParams(Object obj)    
  {                                  
                                     
    EWWaveform objEWWaveform = (EWWaveform)obj;              
                                     
    try                              
    {                                
      cs.setLong(1, objEWWaveform.idWaveform);    
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in CheckidWaveformStatement:SetInputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end CheckidWaveformStatement::SetInputParams()    
                                     
                                     
  protected boolean RegisterOutputParams()    
  {                                  
    try                              
    {                                
      cs.registerOutParameter(2, java.sql.Types.BIGINT); 
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in CheckidWaveformStatement:RegisterOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
    return(true);                    
  }  // end CheckidWaveformStatement:RegisterOutputParams()
                                     
                                     
  protected boolean RetrieveOutputParams(Object obj)
  {                                  
    EWWaveform objEWWaveform = (EWWaveform)obj;             
                                     
    try                              
    {                                
      objEWWaveform.idWaveform=cs.getLong(2);     
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in CheckidWaveformStatement:RetrieveOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end CheckidWaveformStatement::RetrieveOutputParams()
                                     
                                     
} // end class CheckidWaveformStatement                      
                                       
                                       
//             <EOF>                   
