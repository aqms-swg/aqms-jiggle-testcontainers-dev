// *** PACKAGE NAME *** \
package org.trinet.jasi.EW.SQL;


// *** IMPORTS *** \
import java.util.*;
import java.sql.Connection;
import java.sql.SQLException;
import org.trinet.jasi.EW.EWCoda;


/**
 * <!-- Class Description>
 *
 *
 * Created:  2002/08/28
 *
 * @author   ew_oci2java Tool / DK
 * @version  0.99
 **/

public class CreateCodaDurStatement extends EWSQLStatement
{

  // *** CLASS ATTRIBUTES *** \

  // *** CONSTRUCTORS *** \
  public CreateCodaDurStatement()
  {
    sSQLStatement = "Begin Create_CodaDur(OUT_idCodaDur => :OUT_idCodaDur,IN_idTCoda => :IN_idTCoda,IN_idPick => :IN_idPick); End;";
    bIsQuery = false;
    init();
  }


  public CreateCodaDurStatement(Connection IN_conn)
  {
    this();
    SetConnection(IN_conn);
  }


  // *** CLASS METHODS *** \

  protected boolean SetInputParams(Object obj)
  {

    EWCoda objEWCoda = (EWCoda)obj;

    try
    {
      cs.setLong(2, objEWCoda.idTCoda);
      cs.setLong(3, objEWCoda.idPick);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in CreateCodaDurStatement:SetInputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end CreateCodaDurStatement::SetInputParams()


  protected boolean RegisterOutputParams()
  {
    try
    {
      cs.registerOutParameter(1, java.sql.Types.INTEGER);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in CreateCodaDurStatement:RegisterOutputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }
    return(true);
  }  // end CreateCodaDurStatement:RegisterOutputParams()


  protected boolean RetrieveOutputParams(Object obj)
  {
    EWCoda objEWCoda = (EWCoda)obj;

    try
    {
      objEWCoda.idCodaDur=cs.getInt(1);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in CreateCodaDurStatement:RetrieveOutputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end CreateCodaDurStatement::RetrieveOutputParams()


} // end class CreateCodaDurStatement


//             <EOF>
