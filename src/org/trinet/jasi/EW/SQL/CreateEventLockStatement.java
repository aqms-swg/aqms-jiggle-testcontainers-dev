// *** PACKAGE NAME *** \
package org.trinet.jasi.EW.SQL;


// *** IMPORTS *** \
import java.util.*;
import java.sql.Connection;
import java.sql.SQLException;
import org.trinet.jasi.EW.EWSolutionLock;


/**
 * <!-- Class Description>
 *
 *
 **/


public class CreateEventLockStatement extends EWSQLStatement
{

  // *** CLASS ATTRIBUTES *** \

  // *** CONSTRUCTORS *** \
  public CreateEventLockStatement()
  {
    sSQLStatement = "Begin Lock_Event_For_Review(OUT_Retcode => :OUT_Retcode,IN_idEvent => :IN_idEvent, IN_iLockTime => :IN_iLockTime, IN_idSource => :IN_idSource, IN_sSource => :IN_sSource, IN_sMachineName => :IN_sMachineName,IN_sNote => :IN_sNote); End;";
    bIsQuery = false;
    init();
  }


  public CreateEventLockStatement(Connection IN_conn)
  {
    this();
    SetConnection(IN_conn);
  }


  // *** CLASS METHODS *** \


  protected boolean SetInputParams(Object obj)
  {

    EWSolutionLock objEWSolutionLock = (EWSolutionLock)obj;

    try
    {
      cs.setLong(2, objEWSolutionLock.idEvent);
      System.out.println("idEvent is " + objEWSolutionLock.idEvent);
      cs.setInt(3, objEWSolutionLock.iLockTime);
      System.out.println("iLockTime is " + objEWSolutionLock.iLockTime);
      cs.setLong(4, objEWSolutionLock.idSource);
      System.out.println("idSource is " + objEWSolutionLock.idSource);
      cs.setString(5, objEWSolutionLock.sSource);
      System.out.println("sSource is " + objEWSolutionLock.sSource);
      cs.setString(6, objEWSolutionLock.sMachineName);
      System.out.println("sMachineName is " + objEWSolutionLock.sMachineName);
      cs.setString(7, objEWSolutionLock.sNote);
      System.out.println("sNote is " + objEWSolutionLock.sNote);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in CreateEventLockStatement:SetInputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end CreateEventLockStatement::SetInputParams()


  protected boolean RegisterOutputParams()
  {
    try
    {
      cs.registerOutParameter(1, java.sql.Types.INTEGER);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in CreateEventLockStatement:RegisterOutputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }
    return(true);
  }  // end CreateEventLockStatement:RegisterOutputParams()


  protected boolean RetrieveOutputParams(Object obj)
  {
    EWSolutionLock objEWSolutionLock = (EWSolutionLock)obj;

    try
    {
      System.out.println("iRetCode is " + objEWSolutionLock.iRetCode);
      objEWSolutionLock.iRetCode=cs.getInt(1);
      System.out.println("iRetCode-2 is " + objEWSolutionLock.iRetCode);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in CreateEventLockStatement:RetrieveOutputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end CreateEventLockStatement::RetrieveOutputParams()


} // end class CreateEventLockStatement


//             <EOF>
