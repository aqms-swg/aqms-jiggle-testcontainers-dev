// *** PACKAGE NAME *** \
package org.trinet.jasi.EW.SQL;


// *** IMPORTS *** \
import java.util.*;
import java.sql.Connection;
import java.sql.SQLException;
import org.trinet.jasi.EW.EWSolution;


/**
 * <!-- Class Description>
 *
 *
 * Created:  2002/06/12
 *
 * @author   ew_oci2java Tool / DK
 * @version  0.99
 **/

public class CreateEventStatement extends EWSQLStatement
{

  // *** CLASS ATTRIBUTES *** \

  // *** CONSTRUCTORS *** \
  public CreateEventStatement()
  {
    sSQLStatement = "Begin Create_Event(OUT_idEvent => :OUT_idEvent,IN_tiEventType => :IN_tiEventType,IN_iDubiocity => :IN_iDubiocity, IN_sComment => :IN_sComment,IN_sSource => :IN_sSource,IN_sSourceEventID => :IN_sSourceEventID); End;";
    bIsQuery = false;
    init();
  }


  public CreateEventStatement(Connection IN_conn)
  {
    this();
    SetConnection(IN_conn);
  }


  // *** CLASS METHODS *** \

  protected boolean SetInputParams(Object obj)
  {

    EWSolution objEWSolution = (EWSolution)obj;

    try
    {
      cs.setInt(2, objEWSolution.tiEventType);
      cs.setInt(3, objEWSolution.iDubiocity);
      cs.setString(4, objEWSolution.sComment);
      cs.setString(5, objEWSolution.sSource);
      cs.setString(6, objEWSolution.sSourceEventID);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in CreateEventStatement:SetInputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end CreateEventStatement::SetInputParams()


  protected boolean RegisterOutputParams()
  {
    try
    {
      cs.registerOutParameter(1, java.sql.Types.INTEGER);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in CreateEventStatement:RegisterOutputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }
    return(true);
  }  // end CreateEventStatement:RegisterOutputParams()


  protected boolean RetrieveOutputParams(Object obj)
  {
    EWSolution objEWSolution = (EWSolution)obj;

    try
    {
      objEWSolution.idEvent=cs.getInt(1);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in CreateEventStatement:RetrieveOutputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end CreateEventStatement::RetrieveOutputParams()


} // end class CreateEventStatement


//             <EOF>
