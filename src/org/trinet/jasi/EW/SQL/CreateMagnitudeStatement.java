// *** PACKAGE NAME *** \                                     
package org.trinet.jasi.EW.SQL;                                
                                                               
                                                               
// *** IMPORTS *** \                                          
import java.util.*;                                            
import java.sql.Connection;                                    
import java.sql.SQLException;                                  
import org.trinet.jasi.EW.EWMagnitude;                                  
                                                               
                                                               
/**                                    
 * <!-- Class Description>             
 *                                     
 *                                     
 * Created:  2002/06/12                        
 *                                     
 * @author   ew_oci2java Tool / DK                        
 * @version  0.99                        
 **/                                   
                                       
public class CreateMagnitudeStatement extends EWSQLStatement 
{                                      
                                       
  // *** CLASS ATTRIBUTES *** \       
                                       
  // *** CONSTRUCTORS *** \           
  public CreateMagnitudeStatement()                          
  {                                    
    sSQLStatement = "Begin Create_Magnitude(OUT_idMag => :OUT_idMag,IN_sExternalTableName => '',IN_xidExternal => '',IN_sSource => :IN_sSource,IN_iMagType => :IN_iMagType,IN_dMagAvg => :IN_dMagAvg,IN_dMagErr => :IN_dMagErr,IN_iNumMags => :IN_iNumMags,IN_bBindToEvent => :IN_bBindToEvent,IN_bSetPreferred => :IN_bSetPreferred,IN_idEvent => :IN_idEvent,IN_idOrigin => :IN_idOrigin); End;";
    bIsQuery = false;                
    init();                            
  }                                    
                                       
                                       
  public CreateMagnitudeStatement(Connection IN_conn)        
  {                                    
    this();                            
    SetConnection(IN_conn);            
  }                                    
                                       
                                       
  // *** CLASS METHODS *** \          
                                       
  protected boolean SetInputParams(Object obj)    
  {                                  
                                     
    EWMagnitude objEWMagnitude = (EWMagnitude)obj;              
                                     
    try                              
    {                                
      cs.setString(2, objEWMagnitude.sSource);    
      cs.setInt(3, objEWMagnitude.iMagType);    
      cs.setFloat(4, objEWMagnitude.dMagAvg);    
      cs.setFloat(5, objEWMagnitude.dMagErr);    
      cs.setInt(6, objEWMagnitude.iNumMags);    
      cs.setInt(7, objEWMagnitude.bBindToEvent);    
      cs.setInt(8, objEWMagnitude.bSetPreferred);    
      cs.setLong(9, objEWMagnitude.idEvent);    
      cs.setLong(10, objEWMagnitude.idOrigin);    
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in CreateMagnitudeStatement:SetInputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end CreateMagnitudeStatement::SetInputParams()    
                                     
                                     
  protected boolean RegisterOutputParams()    
  {                                  
    try                              
    {                                
      cs.registerOutParameter(1, java.sql.Types.BIGINT); 
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in CreateMagnitudeStatement:RegisterOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
    return(true);                    
  }  // end CreateMagnitudeStatement:RegisterOutputParams()
                                     
                                     
  protected boolean RetrieveOutputParams(Object obj)
  {                                  
    EWMagnitude objEWMagnitude = (EWMagnitude)obj;             
                                     
    try                              
    {                                
      objEWMagnitude.idMag=cs.getLong(1);     
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in CreateMagnitudeStatement:RetrieveOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end CreateMagnitudeStatement::RetrieveOutputParams()
                                     
                                     
} // end class CreateMagnitudeStatement                      
                                       
                                       
//             <EOF>                   
