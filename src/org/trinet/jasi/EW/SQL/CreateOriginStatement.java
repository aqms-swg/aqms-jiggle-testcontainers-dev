// *** PACKAGE NAME *** \
package org.trinet.jasi.EW.SQL;


// *** IMPORTS *** \
import java.util.*;
import java.sql.Connection;
import java.sql.SQLException;
import org.trinet.jasi.EW.EWOrigin;


/**
 * <!-- Class Description>
 *
 *
 * Created:  2002/06/12
 *
 * @author   ew_oci2java Tool / DK
 * @version  0.99
 **/

public class CreateOriginStatement extends EWSQLStatement
{

  // *** CLASS ATTRIBUTES *** \

  // *** CONSTRUCTORS *** \
  public CreateOriginStatement()
  {
    sSQLStatement = "Begin Create_Origin(OUT_idOrigin => :OUT_idOrigin,IN_tOrigin => :IN_tOrigin,IN_dLat => :IN_dLat,IN_dErLat => :IN_dErLat,IN_dLon => :IN_dLon,IN_dErLon => :IN_dErLon,IN_dDepth => :IN_dDepth,IN_dErZ => :IN_dErZ,IN_bBindToEvent => :IN_bBindToEvent,IN_idEvent => :IN_idEvent,IN_bSetPrefer => :IN_bSetPrefer,IN_sExternal_Table_Name => :IN_sExternal_Table_Name,IN_xidExternal => :IN_xidExternal,IN_sSource => :IN_sSource,IN_iGap => :IN_iGap,IN_dDmin => :IN_dDmin,IN_dRms => :IN_dRms,IN_iAssocRd => :IN_iAssocRd,IN_iAssocPh => :IN_iAssocPh,IN_iUsedRd => :IN_iUsedRd,IN_iUsedPh => :IN_iUsedPh,IN_iE0Azm => :IN_iE0Azm,IN_iE0Dip => :IN_iE0Dip,IN_iE1Azm => :IN_iE1Azm,IN_iE1Dip => :IN_iE1Dip,IN_iE2Azm => :IN_iE2Azm,IN_iE2Dip => :IN_iE2Dip,IN_dE0 => :IN_dE0,IN_dE1 => :IN_dE1,IN_dE2 => :IN_dE2,IN_sComment => :IN_sComment,IN_tMCI => :IN_tMCI,IN_iFixedDepth => :IN_iFixedDepth); End;";
    bIsQuery = false;
    init();
  }


  public CreateOriginStatement(Connection IN_conn)
  {
    this();
    SetConnection(IN_conn);
  }


  // *** CLASS METHODS *** \

  protected boolean SetInputParams(Object obj)
  {

    EWOrigin objEWOrigin = (EWOrigin)obj;

    try
    {
      cs.setDouble(2, objEWOrigin.tOrigin);
      cs.setFloat(3, objEWOrigin.dLat);
      cs.setFloat(4, objEWOrigin.dErLat);
      cs.setFloat(5, objEWOrigin.dLon);
      cs.setFloat(6, objEWOrigin.dErLon);
      cs.setFloat(7, objEWOrigin.dDepth);
      cs.setFloat(8, objEWOrigin.dErZ);
      cs.setInt(9, objEWOrigin.bBindToEvent);
      cs.setLong(10, objEWOrigin.idEvent);
      cs.setInt(11, objEWOrigin.bSetPrefer);
      cs.setString(12, objEWOrigin.sExternal_Table_Name);
      cs.setString(13, objEWOrigin.xidExternal);
      cs.setString(14, objEWOrigin.sSource);
      cs.setInt(15, objEWOrigin.iGap);
      cs.setFloat(16, objEWOrigin.dDmin);
      cs.setFloat(17, objEWOrigin.dRMS);
      cs.setInt(18, objEWOrigin.iAssocRd);
      cs.setInt(19, objEWOrigin.iAssocPh);
      cs.setInt(20, objEWOrigin.iUsedRd);
      cs.setInt(21, objEWOrigin.iUsedPh);
      cs.setInt(22, objEWOrigin.iE0Azm);
      cs.setInt(23, objEWOrigin.iE0Dip);
      cs.setInt(24, objEWOrigin.iE1Azm);
      cs.setInt(25, objEWOrigin.iE1Dip);
      cs.setInt(26, objEWOrigin.iE2Azm);
      cs.setInt(27, objEWOrigin.iE2Dip);
      cs.setFloat(28, objEWOrigin.dE0);
      cs.setFloat(29, objEWOrigin.dE1);
      cs.setFloat(30, objEWOrigin.dE2);
      cs.setString(31, objEWOrigin.sComment);
      cs.setDouble(32, objEWOrigin.tMCI);
      cs.setInt(33, objEWOrigin.iFixedDepth);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in CreateOriginStatement:SetInputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end CreateOriginStatement::SetInputParams()


  protected boolean RegisterOutputParams()
  {
    try
    {
      cs.registerOutParameter(1, java.sql.Types.BIGINT);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in CreateOriginStatement:RegisterOutputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }
    return(true);
  }  // end CreateOriginStatement:RegisterOutputParams()


  protected boolean RetrieveOutputParams(Object obj)
  {
    EWOrigin objEWOrigin = (EWOrigin)obj;

    try
    {
      objEWOrigin.idOrigin=cs.getLong(1);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in CreateOriginStatement:RetrieveOutputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end CreateOriginStatement::RetrieveOutputParams()


} // end class CreateOriginStatement


//             <EOF>
