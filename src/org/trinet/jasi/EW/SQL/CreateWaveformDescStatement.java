// *** PACKAGE NAME *** \                                     
package org.trinet.jasi.EW.SQL;                                
                                                               
                                                               
// *** IMPORTS *** \                                          
import java.util.*;                                            
import java.sql.Connection;                                    
import java.sql.SQLException;                                  
import org.trinet.jasi.EW.EWWaveform;                                  
                                                               
                                                               
/**                                    
 * <!-- Class Description>             
 *                                     
 *                                     
 * Created:  2002/06/17                        
 *                                     
 * @author   ew_oci2java Tool / DK                        
 * @version  0.99                        
 **/                                   
                                       
public class CreateWaveformDescStatement extends EWSQLStatement 
{                                      
                                       
  // *** CLASS ATTRIBUTES *** \       
                                       
  // *** CONSTRUCTORS *** \           
  public CreateWaveformDescStatement()                          
  {                                    
    sSQLStatement = "Begin Create_Waveform_Desc(OUT_idWaveform => :OUT_idWaveform, IN_idChan => :IN_idChan, IN_tStart => :IN_tStart, IN_tEnd => :IN_tEnd, IN_iDataFormat => :IN_iDataFormat, IN_iByteLen => :IN_iByteLen, IN_bBindToEvent => :IN_bBindToEvent, IN_idEvent => :IN_idEvent); End;";
    bIsQuery = false;                
    init();                            
  }                                    
                                       
                                       
  public CreateWaveformDescStatement(Connection IN_conn)        
  {                                    
    this();                            
    SetConnection(IN_conn);            
  }                                    
                                       
                                       
  // *** CLASS METHODS *** \          
                                       
  protected boolean SetInputParams(Object obj)    
  {                                  
                                     
    EWWaveform objEWWaveform = (EWWaveform)obj;              
                                     
    try                              
    {                                
      cs.setLong(2, objEWWaveform.idChan);    
      cs.setDouble(3, objEWWaveform.tStart);    
      cs.setDouble(4, objEWWaveform.tEnd);    
      cs.setInt(5, objEWWaveform.iDataFormat);    
      cs.setInt(6, objEWWaveform.iByteLen);    
      cs.setInt(7, objEWWaveform.bBindToEvent);    
      cs.setLong(8, objEWWaveform.idEvent);    
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in CreateWaveformDescStatement:SetInputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end CreateWaveformDescStatement::SetInputParams()    
                                     
                                     
  protected boolean RegisterOutputParams()    
  {                                  
    try                              
    {                                
      cs.registerOutParameter(1, java.sql.Types.BIGINT); 
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in CreateWaveformDescStatement:RegisterOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
    return(true);                    
  }  // end CreateWaveformDescStatement:RegisterOutputParams()
                                     
                                     
  protected boolean RetrieveOutputParams(Object obj)
  {                                  
    EWWaveform objEWWaveform = (EWWaveform)obj;             
                                     
    try                              
    {                                
      objEWWaveform.idWaveform=cs.getLong(1);     
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in CreateWaveformDescStatement:RetrieveOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end CreateWaveformDescStatement::RetrieveOutputParams()
                                     
                                     
} // end class CreateWaveformDescStatement                      
                                       
                                       
//             <EOF>                   
