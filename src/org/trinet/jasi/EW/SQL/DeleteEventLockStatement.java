// *** PACKAGE NAME *** \
package org.trinet.jasi.EW.SQL;


// *** IMPORTS *** \
import java.util.*;
import java.sql.Connection;
import java.sql.SQLException;
import org.trinet.jasi.EW.EWSolutionLock;


/**
 * <!-- Class Description>
 *
 *
 **/


public class DeleteEventLockStatement extends EWSQLStatement
{

  // *** CLASS ATTRIBUTES *** \


  // *** CONSTRUCTORS *** \
  public DeleteEventLockStatement()
  {
    sSQLStatement = "Begin Release_Locked_Event(OUT_Retcode => :OUT_Retcode,IN_idEvent => :IN_idEvent,		IN_iLockTime => :IN_iLockTime); End;";
    bIsQuery = false;
    init();
  }


  public DeleteEventLockStatement(Connection IN_conn)
  {
    this();
    SetConnection(IN_conn);
  }


  // *** CLASS METHODS *** \


  protected boolean SetInputParams(Object obj)
  {

    EWSolutionLock objEWSolutionLock = (EWSolutionLock)obj;

    try
    {
      cs.setLong(2, objEWSolutionLock.idEvent);
      cs.setInt(3, objEWSolutionLock.iLockTime);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in DeleteEventLockStatement:SetInputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end DeleteEventLockStatement::SetInputParams()


  protected boolean RegisterOutputParams()
  {
    try
    {
      cs.registerOutParameter(1, java.sql.Types.INTEGER);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in DeleteEventLockStatement:RegisterOutputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }
    return(true);
  }  // end DeleteEventLockStatement:RegisterOutputParams()


  protected boolean RetrieveOutputParams(Object obj)
  {
    EWSolutionLock objEWSolutionLock = (EWSolutionLock)obj;

    try
    {
      objEWSolutionLock.iRetCode=cs.getInt(1);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in DeleteEventLockStatement:RetrieveOutputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end DeleteEventLockStatement::RetrieveOutputParams()


} // end class DeleteEventLockStatement


//             <EOF>
