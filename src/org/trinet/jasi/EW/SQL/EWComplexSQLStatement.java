package org.trinet.jasi.EW.SQL;

/**
 * <p>Title: EWSQLComplexStatement Class</p>
 * <p>Description: Base Class for creating complex EWDB SQL Calls</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author DavidK
 * @version 0.99
 */

import java.sql.Connection;
import java.util.Vector;

public abstract class EWComplexSQLStatement extends EWSQLStatement
{


  /*** CLASS METHODS ***
  public EWComplexSQLStatement(Connection conn);

  / * protected methods * /
  boolean init();                 // called by CallStatement() and CallQueryStatement()
  boolean PrepareStatement();     // called by init()
  boolean RegisterOutputParams(); // called by init()
  boolean SetInputParams(Object InputObject);       // called by CallStatement() and CallQueryStatement()


  /*NOTE:  You will also almost always have to overload several of the
           following functions:
           RetrieveOutputParams()  // for PL/SQL calls returning one set of params
           RetrieveOutputParamsList() // for queries returning potentially multiple rows
    **********************************************/

  /*** END CLASS METHODS ***/


  /*** PROTECTED CLASSES ***/
  class StatementParam
  {
    boolean bUseParam;
    String  sSQLStringSegment;
    int     iType;
    boolean bInputParam;
    boolean bOutputParam;
    int     iParamNum;

    StatementParam(boolean IN_bUseParam , String IN_sSQLStringSegment, int IN_iType,
                          boolean IN_bInputParam, boolean IN_bOutputParam, int IN_iParamNum)
    {
      bUseParam         = IN_bUseParam;
      sSQLStringSegment = IN_sSQLStringSegment;
      iType             = IN_iType;
      bInputParam       = IN_bInputParam;
      bOutputParam      = IN_bOutputParam;
      iParamNum         = IN_iParamNum;
    }
  }

  /*** CLASS ATTRIBUTES ***/
  Vector  ParamList;
  protected boolean bSQLStatementCreated;
  boolean bRequiresWhereClause;
  String  sSQLStatementBase;

  /*** CONSTRUCTORS ***/

  /***
   You need to override this constructor and do atleast the following
   in your own constructor:
   call super(IN_conn);
   ***/

  public EWComplexSQLStatement()
  {
    // Call the EW
    super();
    ParamList = new Vector();
    bSQLStatementCreated = false;
    bRequiresWhereClause = false;
    CreateStatementParams();
  }

  public EWComplexSQLStatement(Connection IN_conn)
  {
    // Call the EW
    this();
    SetConnection(IN_conn);
  }

  protected abstract void CreateStatementParams();
  /*
  { // sample body
    // for each INPUT param in a query
    ParamList.addElement((Object)(new StatementParam(0,            //bUseParam
                                                     "idChan = ?", //sSQLStringSegment
                                                     TYPES.long,   //iType
                                                     true,         //bInputParam
                                                     false         //bOutputParam
                                                    );
    //repeat last statement for other params

    sSQLStatementBase = "select foo, bar, nothing, zip from test where tfoo > 100000000";
    bRequiresWhereClause = false;
  }
  ****** END SAMPLE CreateStatementParams()  ******************/

  protected boolean init()
  {
    if(!super.init())
      return(false);

    if(!PrepareStatement())
      return(false);

    return(true);
  }


  public boolean UseParam(int iParam)
  {
      if(iParam >= ParamList.size())
      {
        return(false);
      }

      if(((StatementParam)(ParamList.get(iParam))).bUseParam == false)
      {
        ((StatementParam)(ParamList.get(iParam))).bUseParam = true;
        bSQLStatementCreated = false;
      }

    return(true);
  }


  public boolean DoNotUseParam(int iParam)
  {
      if(iParam >= ParamList.size())
      {
        return(false);
      }

      if(((StatementParam)(ParamList.get(iParam))).bUseParam == true)
      {
        ((StatementParam)(ParamList.get(iParam))).bUseParam = false;
        bSQLStatementCreated = false;
      }

    return(true);
  }


  protected boolean AssembleStatement()
  {
    int i;
    int ctr = 0;
    int inctr = 0;
    String tempStr="";

      /* first copy over the base statement */
      sSQLStatement=sSQLStatementBase;
      for(i=0; i<ParamList.size();i++)
      {
        if(inctr<=1)
        {
          if(inctr==0 && bRequiresWhereClause)
             tempStr = "WHERE ";
          else
             tempStr = "AND ";
        }
        if(((StatementParam)ParamList.get(i)).bUseParam)
        {
          ctr++;
          ((StatementParam)ParamList.get(i)).iParamNum = ctr;
          // worst case the sSQLStringSegment is blank
          sSQLStatement += tempStr + ((StatementParam)ParamList.get(i)).sSQLStringSegment;
          if(((StatementParam)ParamList.get(i)).bInputParam)
            inctr++;
        }  // if bUseParam
      }  // end for i in ParamList
      // DK sSQLStatement += ";";
    return(true);
  }  // end EWComplexSQLStatement:AssembleStatement()


  protected boolean PrepareStatement()
  {
    if(bSQLStatementCreated)
    {
      if(sSQLStatement == null || conn == null)
      {
        // handle null errors
        System.out.println("ERROR:  EWComplexSQLStatement:PrepareStatement() null params passed!");
        return(false);
      }
    }
    else
    {
      /* this is where we have to do the grunt work of assembling
         the statement
       ***************/
      AssembleStatement();
      super.PrepareStatement();

    }

    return(true);
  }  // end EWComplexSQLStatement:PrepareStatement()


  /** if you're calling a stored proc or PL/SQL
      then you need to overload this call and
      register your output variables
  **/
  protected boolean RegisterOutputParams()
  {
    return(true);
  }  // end EWSQLStatement:RegisterOutputParams()


  protected boolean RetrieveOutputParams(Object OutputObject)
  {
    return(false);
  }

  protected boolean RetrieveOutputParamsList(Vector OutputObjectList)
  {
    return(false);
  }


}
//
// end class  EWComplexSQLStatement
//
