// *** PACKAGE NAME *** \                                     
package org.trinet.jasi.EW.SQL;                                
                                                               
                                                               
// *** IMPORTS *** \                                          
import java.util.*;                                            
import java.sql.Connection;                                    
import java.sql.SQLException;                                  
import org.trinet.jasi.EW.EWSolutionLock;                                  
                                                               
                                                               
/**                                    
 * <!-- Class Description>             
 *                                     
 *                                     
 **/                                   
                  
/* 
CREATE OR REPLACE VIEW All_EventLock_Info as
select el.*, s.sSource
 from EventLock el, Source s
 where el.idSource = s.idSource;
*/
									                     
public class GetAllLocksStatement extends EWSQLStatement 
{                                      
                                       
  // *** CLASS ATTRIBUTES *** \       
                                       
  // *** CONSTRUCTORS *** \           
  public GetAllLocksStatement()                          
  {                                    
    sSQLStatement = "select idEvent, iLockTime, idSource, sSource, sMachineName, sNote from ALL_EVENTLOCK_INFO where sSource = :IN_sSource";
    bIsQuery = true;                 
    init();                            
  }                                    
                                       
                                       
  public GetAllLocksStatement(Connection IN_conn)        
  {                                    
    this();                            
    SetConnection(IN_conn);            
  }                                    
                                       
                                       
  // *** CLASS METHODS *** \          
                                       
  protected boolean SetInputParams(Object obj)    
  {                                  
                                     
    EWSolutionLock objEWSolutionLock = (EWSolutionLock)obj;              
                                     
    try                              
    {                                
      cs.setString(1, objEWSolutionLock.sSource);    
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetAllLocksStatement:SetInputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end GetAllLocksStatement::SetInputParams()    
                                     
                                     
  protected boolean RetrieveOutputParamsList(Vector OutputObjectList) 
  {                                                      
                                                         
     EWSolutionLock objEWSolutionLock;                                           
                                                         
    try                              
    {                                
    while(rs.next())                                     
    {                                                    
      objEWSolutionLock = new EWSolutionLock();                                  

      objEWSolutionLock.idEvent=rs.getLong(1);                      
      objEWSolutionLock.iLockTime=rs.getInt(2);  
      objEWSolutionLock.idSource=rs.getLong(3);                      
      objEWSolutionLock.sSource=rs.getString(4);                      
      objEWSolutionLock.sMachineName=rs.getString(5);                      
      objEWSolutionLock.sNote=rs.getString(6);                      
      OutputObjectList.addElement(objEWSolutionLock);                
    }  // end while(rs.next)                             
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetAllLocksStatement:RetrieveOutputParamsList()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                                         
    return(true);                                        
  }   // end GetAllLocksStatement::RetrieveOutputParamsList()              
                                                         
                                                         
} // end class GetAllLocksStatement                      
                                       
                                       
//             <EOF>                   
