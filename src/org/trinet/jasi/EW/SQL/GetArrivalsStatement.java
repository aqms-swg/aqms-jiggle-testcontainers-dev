// *** PACKAGE NAME *** \                                     
package org.trinet.jasi.EW.SQL;                                
                                                               
                                                               
// *** IMPORTS *** \                                          
import java.util.*;                                            
import java.sql.Connection;                                    
import java.sql.SQLException;                                  
import org.trinet.jasi.EW.EWPhase;                                  
                                                               
                                                               
/**                                    
 * <!-- Class Description>             
 *                                     
 *                                     
 * Created:  2002/06/19                        
 *                                     
 * @author   ew_oci2java Tool / DK                        
 * @version  0.99                        
 **/                                   
                                       
public class GetArrivalsStatement extends EWSQLStatement 
{                                      
                                       
  // *** CLASS ATTRIBUTES *** \       
                                       
  // *** CONSTRUCTORS *** \           
  public GetArrivalsStatement()                          
  {                                    
    sSQLStatement = "select idPick, xidExternal, idChan, sPhase, tPhase,  cMotion, cOnset, dSigma,  idOriginPick, idOrigin, sCalcPhase, tCalcPhase,  dWeight, dDist, dAzm, dTakeoff, tResPick from ALL_ARRIVAL_INFO where idOrigin = :IN_idOrigin";
    bIsQuery = true;                 
    init();                            
  }                                    
                                       
                                       
  public GetArrivalsStatement(Connection IN_conn)        
  {                                    
    this();                            
    SetConnection(IN_conn);            
  }                                    
                                       
                                       
  // *** CLASS METHODS *** \          
                                       
  protected boolean SetInputParams(Object obj)    
  {                                  
                                     
    EWPhase objEWPhase = (EWPhase)obj;              
                                     
    try                              
    {                                
      cs.setLong(1, objEWPhase.idOrigin);    
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetArrivalsStatement:SetInputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end GetArrivalsStatement::SetInputParams()    
                                     
                                     
  protected boolean RetrieveOutputParamsList(Vector OutputObjectList) 
  {                                                      
                                                         
     EWPhase objEWPhase;                                           
                                                         
    try                              
    {                                
    while(rs.next())                                     
    {                                                    
      objEWPhase = new EWPhase();                                  
                                                         
      objEWPhase.idPick=rs.getLong(1);                      
      objEWPhase.sSourcePickID=rs.getString(2);   // xidExternal                   
      objEWPhase.idChan=rs.getLong(3);                      
      objEWPhase.sPhase=rs.getString(4);                      
      objEWPhase.tPhase=rs.getDouble(5);                      
      objEWPhase.cMotion=rs.getString(6);                      
      objEWPhase.cOnset=rs.getString(7);                      
      objEWPhase.dSigma=rs.getDouble(8);                      
      objEWPhase.idOriginPick=rs.getLong(9);                      
      objEWPhase.idOrigin=rs.getLong(10);                      
      objEWPhase.sCalcPhase=rs.getString(11);                      
      objEWPhase.tCalcPhase=rs.getDouble(12);                      
      objEWPhase.dWeight=rs.getFloat(13);                      
      objEWPhase.dDist=rs.getFloat(14);                      
      objEWPhase.dAzm=rs.getFloat(15);                      
      objEWPhase.dTakeoff=rs.getFloat(16);                      
      objEWPhase.tResPick=rs.getDouble(17);                      
      OutputObjectList.addElement(objEWPhase);                
    }  // end while(rs.next)                             
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetArrivalsStatement:RetrieveOutputParamsList()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                                         
    return(true);                                        
  }   // end GetArrivalsStatement::RetrieveOutputParamsList()              
                                                         
                                                         
} // end class GetArrivalsStatement                      
                                       
                                       
//             <EOF>                   
