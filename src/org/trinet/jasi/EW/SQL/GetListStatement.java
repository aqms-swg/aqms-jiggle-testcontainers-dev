// *** PACKAGE NAME *** \
package org.trinet.jasi.EW.SQL;


// *** IMPORTS *** \
import java.util.*;
import java.sql.SQLException;
import java.sql.Connection;
import org.trinet.jasi.EW.EWSolution;


/**
 * <!-- Class Description>
 *
 *
 * Created:  2002/06/05
 *
 * @author   ew_oci2java Tool / DK
 * @version  0.99
 **/

public class GetListStatement extends EWComplexSQLStatement
{

  // *** CLASS ATTRIBUTES *** \

  // *** CONSTRUCTORS *** \
  public GetListStatement()
  {
    super();
    bIsQuery = true;
    init();
  }


  public GetListStatement(Connection IN_conn)
  {
    this();
    SetConnection(IN_conn);
  }


  // *** CLASS METHODS *** \

  protected boolean SetInputParams(Object obj)
  {

    EWSolution objEWSolution = (EWSolution)obj;

    try
    {
      if(((StatementParam)ParamList.get(0)).bUseParam)
        cs.setInt(((StatementParam)ParamList.get(0)).iParamNum, objEWSolution.starttime);

      if(((StatementParam)ParamList.get(1)).bUseParam)
        cs.setInt(((StatementParam)ParamList.get(1)).iParamNum, objEWSolution.endtime);

      if(((StatementParam)ParamList.get(2)).bUseParam)
        cs.setString(((StatementParam)ParamList.get(2)).iParamNum, objEWSolution.source);

      if(((StatementParam)ParamList.get(3)).bUseParam)
        cs.setFloat(((StatementParam)ParamList.get(3)).iParamNum, objEWSolution.minlat);

      if(((StatementParam)ParamList.get(4)).bUseParam)
        cs.setFloat(((StatementParam)ParamList.get(4)).iParamNum, objEWSolution.maxlat);

      if(((StatementParam)ParamList.get(5)).bUseParam)
        cs.setFloat(((StatementParam)ParamList.get(5)).iParamNum, objEWSolution.minlon);

      if(((StatementParam)ParamList.get(6)).bUseParam)
        cs.setFloat(((StatementParam)ParamList.get(6)).iParamNum, objEWSolution.maxlon);

      if(((StatementParam)ParamList.get(7)).bUseParam)
        cs.setFloat(((StatementParam)ParamList.get(7)).iParamNum, objEWSolution.minz);

      if(((StatementParam)ParamList.get(8)).bUseParam)
        cs.setFloat(((StatementParam)ParamList.get(8)).iParamNum, objEWSolution.maxz);

      if(((StatementParam)ParamList.get(9)).bUseParam)
        cs.setFloat(((StatementParam)ParamList.get(9)).iParamNum, objEWSolution.minmag);

      if(((StatementParam)ParamList.get(10)).bUseParam)
        cs.setFloat(((StatementParam)ParamList.get(10)).iParamNum, objEWSolution.maxmag);

      if(((StatementParam)ParamList.get(11)).bUseParam)
        cs.setInt(((StatementParam)ParamList.get(11)).iParamNum, objEWSolution.eventtype);

    }
    catch (SQLException ex)
    {
      System.err.println("Exception in GetListStatement:SetInputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end GetListStatement::SetInputParams()


  protected boolean AssembleStatement()
	{
	  boolean bRC;
	  bRC = super.AssembleStatement();
		sSQLStatement+= " order by tOrigin desc";
		return(bRC);
	}

  protected boolean RetrieveOutputParamsList(Vector OutputObjectList)
  {

     EWSolution objEWSolution;

    try
    {
    while(rs.next())
    {
      objEWSolution = new EWSolution();

      objEWSolution.idEvent=rs.getLong(1);
      objEWSolution.tiEventType=rs.getInt(2);
      objEWSolution.iDubiocity=rs.getInt(3);
      objEWSolution.bArchived=rs.getInt(4);
      objEWSolution.sSource=rs.getString(5);
      objEWSolution.sHumanReadable=rs.getString(6);
      objEWSolution.sSourceEventID=rs.getString(7);
      objEWSolution.idOrigin=rs.getLong(8);
      objEWSolution.tOrigin=rs.getDouble(9);
      objEWSolution.dLat=rs.getFloat(10);
      objEWSolution.dLon=rs.getFloat(11);
      objEWSolution.dDepth=rs.getFloat(12);
      objEWSolution.dPrefMag=rs.getFloat(13);
      OutputObjectList.addElement(objEWSolution);
    }  // end while(rs.next)
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in GetListStatement:RetrieveOutputParamsList()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end GetListStatement::RetrieveOutputParamsList()


  protected  void CreateStatementParams()
  {

    StatementParam spParam;

    spParam = new StatementParam(false, "tOrigin>=:starttime",
                                 java.sql.Types.INTEGER, true, false, -1);
    ParamList.addElement((Object)spParam);

    spParam = new StatementParam(false, "tOrigin<=:endtime",
                                 java.sql.Types.INTEGER, true, false, -1);
    ParamList.addElement((Object)spParam);

    spParam = new StatementParam(false, "sSource=:source",
                                 java.sql.Types.VARCHAR, true, false, -1);
    ParamList.addElement((Object)spParam);

    spParam = new StatementParam(false, "dLat>=:minlat",
                                 java.sql.Types.FLOAT, true, false, -1);
    ParamList.addElement((Object)spParam);

    spParam = new StatementParam(false, "dLat<=:maxlat",
                                 java.sql.Types.FLOAT, true, false, -1);
    ParamList.addElement((Object)spParam);

    spParam = new StatementParam(false, "dLon>=:minlon",
                                 java.sql.Types.FLOAT, true, false, -1);
    ParamList.addElement((Object)spParam);

    spParam = new StatementParam(false, "dLon<=:maxlon",
                                 java.sql.Types.FLOAT, true, false, -1);
    ParamList.addElement((Object)spParam);

    spParam = new StatementParam(false, "dDepth>=:minz",
                                 java.sql.Types.FLOAT, true, false, -1);
    ParamList.addElement((Object)spParam);

    spParam = new StatementParam(false, "dDepth<=:maxz",
                                 java.sql.Types.FLOAT, true, false, -1);
    ParamList.addElement((Object)spParam);

    spParam = new StatementParam(false, "dPrefMag>=:minmag",
                                 java.sql.Types.FLOAT, true, false, -1);
    ParamList.addElement((Object)spParam);

    spParam = new StatementParam(false, "dPrefMag<=:maxmag",
                                 java.sql.Types.FLOAT, true, false, -1);
    ParamList.addElement((Object)spParam);

    spParam = new StatementParam(false, "tiEventType=:eventtype",
                                 java.sql.Types.INTEGER, true, false, -1);
    ParamList.addElement((Object)spParam);

    sSQLStatementBase = "select idEvent, tiEventType, iDubiocity, bArchived, sSource, sHumanReadable, sSourceEventID, idOrigin, tOrigin, dLat, dLon, dDepth, dPrefMag from ALL_EVENTS_INFO";
    bRequiresWhereClause = true;
  } // end GetListStatement:CreateStatementParams


} // end class GetListStatement


//             <EOF>
