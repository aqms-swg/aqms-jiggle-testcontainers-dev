// *** PACKAGE NAME *** \                                     
package org.trinet.jasi.EW.SQL;                                
                                                               
                                                               
// *** IMPORTS *** \                                          
import java.util.*;                                            
import java.sql.Connection;                                    
import java.sql.SQLException;                                  
import org.trinet.jasi.EW.EWMagnitude;                                  
                                                               
                                                               
/**                                    
 * <!-- Class Description>             
 *                                     
 *                                     
 * Created:  2002/06/12                        
 *                                     
 * @author   ew_oci2java Tool / DK                        
 * @version  0.99                        
 **/                                   
                                       
public class GetMagnitudeStatement extends EWSQLStatement 
{                                      
                                       
  // *** CLASS ATTRIBUTES *** \       
                                       
  // *** CONSTRUCTORS *** \           
  public GetMagnitudeStatement()                          
  {                                    
    sSQLStatement = "Begin Get_Magnitude(OUT_Retcode => :OUT_Retcode, IN_idMag => :IN_idMag,  OUT_sSource => :OUT_sSource, OUT_dMagAvg => :OUT_dMagAvg,  OUT_iNumMags => :OUT_iNumMags, OUT_dMagErr => :OUT_dMagErr,  OUT_iMagType => :OUT_iMagType, OUT_idOrigin => :OUT_idOrigin); End;";
    bIsQuery = false;                
    init();                            
  }                                    
                                       
                                       
  public GetMagnitudeStatement(Connection IN_conn)        
  {                                    
    this();                            
    SetConnection(IN_conn);            
  }                                    
                                       
                                       
  // *** CLASS METHODS *** \          
                                       
  protected boolean SetInputParams(Object obj)    
  {                                  
                                     
    EWMagnitude objEWMagnitude = (EWMagnitude)obj;              
                                     
    try                              
    {                                
      cs.setLong(2, objEWMagnitude.idMag);    
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetMagnitudeStatement:SetInputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end GetMagnitudeStatement::SetInputParams()    
                                     
                                     
  protected boolean RegisterOutputParams()    
  {                                  
    try                              
    {                                
      cs.registerOutParameter(1, java.sql.Types.INTEGER); 
      cs.registerOutParameter(3, java.sql.Types.VARCHAR); 
      cs.registerOutParameter(4, java.sql.Types.FLOAT); 
      cs.registerOutParameter(5, java.sql.Types.INTEGER); 
      cs.registerOutParameter(6, java.sql.Types.FLOAT); 
      cs.registerOutParameter(7, java.sql.Types.INTEGER); 
      cs.registerOutParameter(8, java.sql.Types.BIGINT); 
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetMagnitudeStatement:RegisterOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
    return(true);                    
  }  // end GetMagnitudeStatement:RegisterOutputParams()
                                     
                                     
  protected boolean RetrieveOutputParams(Object obj)
  {                                  
    EWMagnitude objEWMagnitude = (EWMagnitude)obj;             
                                     
    try                              
    {                                
      objEWMagnitude.Retcode=cs.getInt(1);     
      objEWMagnitude.sSource=cs.getString(3);     
      objEWMagnitude.dMagAvg=cs.getFloat(4);     
      objEWMagnitude.iNumMags=cs.getInt(5);     
      objEWMagnitude.dMagErr=cs.getFloat(6);     
      objEWMagnitude.iMagType=cs.getInt(7);     
      objEWMagnitude.idOrigin=cs.getLong(8);     
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetMagnitudeStatement:RetrieveOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end GetMagnitudeStatement::RetrieveOutputParams()
                                     
                                     
} // end class GetMagnitudeStatement                      
                                       
                                       
//             <EOF>                   
