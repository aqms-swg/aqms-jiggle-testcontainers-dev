// *** PACKAGE NAME *** \
package org.trinet.jasi.EW.SQL;


// *** IMPORTS *** \
import java.util.*;
import java.sql.Connection;
import java.sql.SQLException;
import org.trinet.util.gazetteer.EW.EWWhereIs;


/**
 * <!-- Class Description>
 *
 *
 * Created:  2002/06/19
 *
 * @author   ew_oci2java Tool / DK
 * @version  0.99
 **/

public class GetNearestPlacesStatement extends EWSQLStatement
{

  // *** CLASS ATTRIBUTES *** \

  // *** CONSTRUCTORS *** \
  public GetNearestPlacesStatement()
  {
    sSQLStatement =
		   "select p.idPlace, p.iPlaceMajorType, p.iPlaceMinorType, "
		 + "  p.dLat, p.dLon, p.sName, p.sState,  "
     + "  pt.sDescription "
     + " from place p, placetype pt "
     + " where "
     + "   ((p.dLat - :IN_dLat)*(p.dLat - :IN_dLat)) + ((p.dLon - :IN_dLon) * (p.dLon - :IN_dLon)) "
     + "    in "
     + "    (select min(((dLat - :IN_dLat)*(dLat - :IN_dLat)) + ((dLon - :IN_dLon) * (dLon - :IN_dLon))) deg_dist "
     + "     from place                                 "
     + "     group by iPlaceMajorType, iPlaceMinorType) "
     + "   and p.iplacemajortype = pt.iplacemajortype   "
     + "   and p.iplaceminortype = pt.iplaceminortype   "
     + " order by iPlaceMajorType, iPlaceMinorType      ";

    bIsQuery = true;
    init();
  }


  public GetNearestPlacesStatement(Connection IN_conn)
  {
    this();
    SetConnection(IN_conn);
  }


  // *** CLASS METHODS *** \

  protected boolean SetInputParams(Object obj)
  {

    EWWhereIs objEWWhereIs = (EWWhereIs)obj;

    try
    {
      cs.setDouble(1, objEWWhereIs.dLat);
      cs.setDouble(2, objEWWhereIs.dLat);
      cs.setDouble(3, objEWWhereIs.dLon);
      cs.setDouble(4, objEWWhereIs.dLon);
      cs.setDouble(5, objEWWhereIs.dLat);
      cs.setDouble(6, objEWWhereIs.dLat);
      cs.setDouble(7, objEWWhereIs.dLon);
      cs.setDouble(8, objEWWhereIs.dLon);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in GetNearestPlacesStatement:SetInputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end GetNearestPlacesStatement::SetInputParams()


  protected boolean RetrieveOutputParamsList(Vector OutputObjectList)
  {

     EWWhereIs objEWWhereIs;

    try
    {
    while(rs.next())
    {
      objEWWhereIs = new EWWhereIs();

      objEWWhereIs.idPlace=rs.getLong(1);
      objEWWhereIs.iPlaceMajorType=rs.getInt(2);
      objEWWhereIs.iPlaceMinorType=rs.getInt(3);
      objEWWhereIs.dLat=rs.getDouble(4);
      objEWWhereIs.dLon=rs.getDouble(5);
      objEWWhereIs.sName=rs.getString(6);
      objEWWhereIs.sState=rs.getString(7);
      objEWWhereIs.sDescription=rs.getString(8);
      OutputObjectList.addElement(objEWWhereIs);
    }  // end while(rs.next)
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in GetNearestPlacesStatement:RetrieveOutputParamsList()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end GetNearestPlacesStatement::RetrieveOutputParamsList()


} // end class GetNearestPlacesStatement


//             <EOF>
