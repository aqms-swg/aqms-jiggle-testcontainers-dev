// *** PACKAGE NAME *** \                                     
package org.trinet.jasi.EW.SQL;                                
                                                               
                                                               
// *** IMPORTS *** \                                          
import java.util.*;                                            
import java.sql.Connection;                                    
import java.sql.SQLException;                                  
import org.trinet.jasi.EW.EWOrigin;                                  
                                                               
                                                               
/**                                    
 * <!-- Class Description>             
 *                                     
 *                                     
 * Created:  2002/06/12                        
 *                                     
 * @author   ew_oci2java Tool / DK                        
 * @version  0.99                        
 **/                                   
                                       
public class GetOriginListStatement extends EWSQLStatement 
{                                      
                                       
  // *** CLASS ATTRIBUTES *** \       
                                       
  // *** CONSTRUCTORS *** \           
  public GetOriginListStatement()                          
  {                                    
    sSQLStatement = "select idOrigin, tOrigin, dLat, dLon, dDepth, iAssocPh, iUsedPh, iFixedDepth,   sSource, sHumanReadable from ALL_EVENT_ORIGIN_INFO where idEvent = :idEvent";
    bIsQuery = true;                 
    init();                            
  }                                    
                                       
                                       
  public GetOriginListStatement(Connection IN_conn)        
  {                                    
    this();                            
    SetConnection(IN_conn);            
  }                                    
                                       
                                       
  // *** CLASS METHODS *** \          
                                       
  protected boolean SetInputParams(Object obj)    
  {                                  
                                     
    EWOrigin objEWOrigin = (EWOrigin)obj;              
                                     
    try                              
    {                                
      cs.setLong(1, objEWOrigin.idEvent);    
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetOriginListStatement:SetInputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end GetOriginListStatement::SetInputParams()    
                                     
                                     
  protected boolean RetrieveOutputParamsList(Vector OutputObjectList) 
  {                                                      
                                                         
     EWOrigin objEWOrigin;                                           
                                                         
    try                              
    {                                
    while(rs.next())                                     
    {                                                    
      objEWOrigin = new EWOrigin();                                  
                                                         
      objEWOrigin.idOrigin=rs.getLong(1);                      
      objEWOrigin.tOrigin=rs.getDouble(2);                      
      objEWOrigin.dLat=rs.getFloat(3);                      
      objEWOrigin.dLon=rs.getFloat(4);                      
      objEWOrigin.dDepth=rs.getFloat(5);                      
      objEWOrigin.iAssocPh=rs.getInt(6);                      
      objEWOrigin.iUsedPh=rs.getInt(7);                      
      objEWOrigin.iFixedDepth=rs.getInt(8);                      
      objEWOrigin.sSource=rs.getString(9);                      
      objEWOrigin.sHumanReadable=rs.getString(10);                      
      OutputObjectList.addElement(objEWOrigin);                
    }  // end while(rs.next)                             
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetOriginListStatement:RetrieveOutputParamsList()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                                         
    return(true);                                        
  }   // end GetOriginListStatement::RetrieveOutputParamsList()              
                                                         
                                                         
} // end class GetOriginListStatement                      
                                       
                                       
//             <EOF>                   
