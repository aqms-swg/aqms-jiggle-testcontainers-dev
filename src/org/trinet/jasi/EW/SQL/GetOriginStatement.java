// *** PACKAGE NAME *** \
package org.trinet.jasi.EW.SQL;


// *** IMPORTS *** \
import java.util.*;
import java.sql.Connection;
import java.sql.SQLException;
import org.trinet.jasi.EW.EWOrigin;


/**
 * <!-- Class Description>
 *
 *
 * Created:  2002/06/12
 *
 * @author   ew_oci2java Tool / DK
 * @version  0.99
 **/

public class GetOriginStatement extends EWSQLStatement
{

  // *** CLASS ATTRIBUTES *** \

  // *** CONSTRUCTORS *** \
  public GetOriginStatement()
  {
    sSQLStatement = "Begin Get_Origin(OUT_Retcode => :OUT_Retcode, IN_idOrigin => :IN_idOrigin,  OUT_xidExternal => :OUT_xidExternal,  OUT_sSource => :OUT_sSource, OUT_sRealSource => :OUT_sRealSource, OUT_tOrigin => :OUT_tOrigin,  OUT_dLat => :OUT_dLat, OUT_dLon => :OUT_dLon, OUT_dDepth => :OUT_dDepth,  OUT_iGap => :OUT_iGap, OUT_dDmin => :OUT_dDmin, OUT_dRMS => :OUT_dRMS,  OUT_iAssocRd => :OUT_iAssocRd, OUT_iAssocPh => :OUT_iAssocPh,  OUT_iUsedRd => :OUT_iUsedRd, OUT_iUsedPh => :OUT_iUsedPh,  OUT_iE0Azm => :OUT_iE0Azm, OUT_iE0Dip => :OUT_iE0Dip, OUT_dE0 => :OUT_dE0,  OUT_iE1Azm => :OUT_iE1Azm, OUT_iE1Dip => :OUT_iE1Dip,  OUT_dE1 => :OUT_dE1, OUT_iE2Azm => :OUT_iE2Azm, OUT_iE2Dip => :OUT_iE2Dip,  OUT_dE2 => :OUT_dE2, OUT_dErLat => :OUT_dErLat, OUT_dErLon => :OUT_dErLon,  OUT_dErZ => :OUT_dErZ, OUT_tMCI => :OUT_tMCI,  OUT_iFixedDepth => :OUT_iFixedDepth, OUT_Comment => :OUT_Comment); End;";
    bIsQuery = false;
    init();
  }


  public GetOriginStatement(Connection IN_conn)
  {
    this();
    SetConnection(IN_conn);
  }


  // *** CLASS METHODS *** \

  protected boolean SetInputParams(Object obj)
  {

    EWOrigin objEWOrigin = (EWOrigin)obj;

    try
    {
      cs.setLong(2, objEWOrigin.idOrigin);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in GetOriginStatement:SetInputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end GetOriginStatement::SetInputParams()


  protected boolean RegisterOutputParams()
  {
    try
    {
      cs.registerOutParameter(1, java.sql.Types.INTEGER);
      cs.registerOutParameter(3, java.sql.Types.VARCHAR);
      cs.registerOutParameter(4, java.sql.Types.VARCHAR);
      cs.registerOutParameter(5, java.sql.Types.VARCHAR);
      cs.registerOutParameter(6, java.sql.Types.DOUBLE);
      cs.registerOutParameter(7, java.sql.Types.FLOAT);
      cs.registerOutParameter(8, java.sql.Types.FLOAT);
      cs.registerOutParameter(9, java.sql.Types.FLOAT);
      cs.registerOutParameter(10, java.sql.Types.INTEGER);
      cs.registerOutParameter(11, java.sql.Types.FLOAT);
      cs.registerOutParameter(12, java.sql.Types.FLOAT);
      cs.registerOutParameter(13, java.sql.Types.INTEGER);
      cs.registerOutParameter(14, java.sql.Types.INTEGER);
      cs.registerOutParameter(15, java.sql.Types.INTEGER);
      cs.registerOutParameter(16, java.sql.Types.INTEGER);
      cs.registerOutParameter(17, java.sql.Types.INTEGER);
      cs.registerOutParameter(18, java.sql.Types.INTEGER);
      cs.registerOutParameter(19, java.sql.Types.FLOAT);
      cs.registerOutParameter(20, java.sql.Types.INTEGER);
      cs.registerOutParameter(21, java.sql.Types.INTEGER);
      cs.registerOutParameter(22, java.sql.Types.FLOAT);
      cs.registerOutParameter(23, java.sql.Types.INTEGER);
      cs.registerOutParameter(24, java.sql.Types.INTEGER);
      cs.registerOutParameter(25, java.sql.Types.FLOAT);
      cs.registerOutParameter(26, java.sql.Types.FLOAT);
      cs.registerOutParameter(27, java.sql.Types.FLOAT);
      cs.registerOutParameter(28, java.sql.Types.FLOAT);
      cs.registerOutParameter(29, java.sql.Types.DOUBLE);
      cs.registerOutParameter(30, java.sql.Types.INTEGER);
      cs.registerOutParameter(31, java.sql.Types.VARCHAR);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in GetOriginStatement:RegisterOutputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }
    return(true);
  }  // end GetOriginStatement:RegisterOutputParams()


  protected boolean RetrieveOutputParams(Object obj)
  {
    EWOrigin objEWOrigin = (EWOrigin)obj;

    try
    {
      objEWOrigin.Retcode=cs.getInt(1);
      objEWOrigin.xidExternal=cs.getString(3);
      objEWOrigin.sHumanReadable=cs.getString(4);     // DK was sSource
      objEWOrigin.sSource=cs.getString(5);        // DK was sRealSource
      objEWOrigin.tOrigin=cs.getDouble(6);
      objEWOrigin.dLat=cs.getFloat(7);
      objEWOrigin.dLon=cs.getFloat(8);
      objEWOrigin.dDepth=cs.getFloat(9);
      objEWOrigin.iGap=cs.getInt(10);
      objEWOrigin.dDmin=cs.getFloat(11);
      objEWOrigin.dRMS=cs.getFloat(12);
      objEWOrigin.iAssocRd=cs.getInt(13);
      objEWOrigin.iAssocPh=cs.getInt(14);
      objEWOrigin.iUsedRd=cs.getInt(15);
      objEWOrigin.iUsedPh=cs.getInt(16);
      objEWOrigin.iE0Azm=cs.getInt(17);
      objEWOrigin.iE0Dip=cs.getInt(18);
      objEWOrigin.dE0=cs.getFloat(19);
      objEWOrigin.iE1Azm=cs.getInt(20);
      objEWOrigin.iE1Dip=cs.getInt(21);
      objEWOrigin.dE1=cs.getFloat(22);
      objEWOrigin.iE2Azm=cs.getInt(23);
      objEWOrigin.iE2Dip=cs.getInt(24);
      objEWOrigin.dE2=cs.getFloat(25);
      objEWOrigin.dErLat=cs.getFloat(26);
      objEWOrigin.dErLon=cs.getFloat(27);
      objEWOrigin.dErZ=cs.getFloat(28);
      objEWOrigin.tMCI=cs.getDouble(29);
      objEWOrigin.iFixedDepth=cs.getInt(30);
      objEWOrigin.sComment=cs.getString(31);     // DK  was Comment
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in GetOriginStatement:RetrieveOutputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end GetOriginStatement::RetrieveOutputParams()


} // end class GetOriginStatement


//             <EOF>
