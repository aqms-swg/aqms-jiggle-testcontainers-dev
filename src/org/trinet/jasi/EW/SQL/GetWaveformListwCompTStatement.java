// *** PACKAGE NAME *** \                                     
package org.trinet.jasi.EW.SQL;                                
                                                               
                                                               
// *** IMPORTS *** \                                          
import java.util.*;                                            
import java.sql.Connection;                                    
import java.sql.SQLException;                                  
import org.trinet.jasi.EW.EWWaveform;                                  
                                                               
                                                               
/**                                    
 * <!-- Class Description>             
 *                                     
 *                                     
 * Created:  2002/06/17                        
 *                                     
 * @author   ew_oci2java Tool / DK                        
 * @version  0.99                        
 **/                                   
                                       
public class GetWaveformListwCompTStatement extends EWSQLStatement 
{                                      
                                       
  // *** CLASS ATTRIBUTES *** \       
                                       
  // *** CONSTRUCTORS *** \           
  public GetWaveformListwCompTStatement()                          
  {                                    
    sSQLStatement = "select idWaveform, idChan, tStart, tEnd, iDataFormat, iByteLen, tOff, tOn, sSta, sComp, sNet, sLoc, dLat, dLon, dElev, dAzm, dDip from ALL_WAVES_W_COMPT_BY_EVENT where idEvent = :IN_idEvent";
    bIsQuery = true;                 
    init();                            
  }                                    
                                       
                                       
  public GetWaveformListwCompTStatement(Connection IN_conn)        
  {                                    
    this();                            
    SetConnection(IN_conn);            
  }                                    
                                       
                                       
  // *** CLASS METHODS *** \          
                                       
  protected boolean SetInputParams(Object obj)    
  {                                  
                                     
    EWWaveform objEWWaveform = (EWWaveform)obj;              
                                     
    try                              
    {                                
      cs.setLong(1, objEWWaveform.idEvent);    
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetWaveformListwCompTStatement:SetInputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end GetWaveformListwCompTStatement::SetInputParams()    
                                     
                                     
  protected boolean RetrieveOutputParamsList(Vector OutputObjectList) 
  {                                                      
                                                         
     EWWaveform objEWWaveform;                                           
                                                         
    try                              
    {                                
    while(rs.next())                                     
    {                                                    
      objEWWaveform = new EWWaveform();                                  
                                                         
      objEWWaveform.idWaveform=rs.getLong(1);                      
      objEWWaveform.idChan=rs.getLong(2);                      
      objEWWaveform.tStart=rs.getDouble(3);                      
      objEWWaveform.tEnd=rs.getDouble(4);                      
      objEWWaveform.iDataFormat=rs.getInt(5);                      
      objEWWaveform.iByteLen=rs.getInt(6);                      
      objEWWaveform.tOff=rs.getDouble(7);                      
      objEWWaveform.tOn=rs.getDouble(8);                      
      objEWWaveform.sSta=rs.getString(9);                      
      objEWWaveform.sComp=rs.getString(10);                      
      objEWWaveform.sNet=rs.getString(11);                      
      objEWWaveform.sLoc=rs.getString(12);                      
      objEWWaveform.dLat=rs.getFloat(13);                      
      objEWWaveform.dLon=rs.getFloat(14);                      
      objEWWaveform.dElev=rs.getFloat(15);                      
      objEWWaveform.dAzm=rs.getFloat(16);                      
      objEWWaveform.dDip=rs.getFloat(17);                      
      OutputObjectList.addElement(objEWWaveform);                
    }  // end while(rs.next)                             
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in GetWaveformListwCompTStatement:RetrieveOutputParamsList()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                                         
    return(true);                                        
  }   // end GetWaveformListwCompTStatement::RetrieveOutputParamsList()              
                                                         
                                                         
} // end class GetWaveformListwCompTStatement                      
                                       
                                       
//             <EOF>                   
