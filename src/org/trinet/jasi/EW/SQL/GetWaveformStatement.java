// *** PACKAGE NAME *** \
package org.trinet.jasi.EW.SQL;


// *** IMPORTS *** \
import java.util.*;
import java.sql.Connection;
import java.sql.SQLException;
import org.trinet.jasi.EW.EWWaveform;


/**
 * <!-- Class Description>
 *
 *
 * Created:  2002/06/17
 *
 * @author   ew_oci2java Tool / DK
 * @version  0.99
 **/

public class GetWaveformStatement extends EWSQLStatement
{

  // *** CLASS ATTRIBUTES *** \

  // *** CONSTRUCTORS *** \
  public GetWaveformStatement()
  {
    sSQLStatement = "select binSnippet from Waveform where idWaveform = :IN_idWaveform";
    bIsQuery = true;
    init();
  }


  public GetWaveformStatement(Connection IN_conn)
  {
    this();
    SetConnection(IN_conn);
  }


  // *** CLASS METHODS *** \

  protected boolean SetInputParams(Object obj)
  {

    EWWaveform objEWWaveform = (EWWaveform)obj;

    try
    {
      cs.setLong(1, objEWWaveform.idWaveform);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in GetWaveformStatement:SetInputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end GetWaveformStatement::SetInputParams()


  protected boolean RetrieveOutputParamsList(Vector OutputObjectList)
  {

     EWWaveform objEWWaveform;

    try
    {
    while(rs.next())
    {
      objEWWaveform = new EWWaveform();

      objEWWaveform.RawSnippet=rs.getBytes(1);    // DK was binSnippet
      OutputObjectList.addElement(objEWWaveform);
    }  // end while(rs.next)
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in GetWaveformStatement:RetrieveOutputParamsList()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end GetWaveformStatement::RetrieveOutputParamsList()


} // end class GetWaveformStatement


//             <EOF>
