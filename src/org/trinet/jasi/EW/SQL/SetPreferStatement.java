// *** PACKAGE NAME *** \                                     
package org.trinet.jasi.EW.SQL;                                
                                                               
                                                               
// *** IMPORTS *** \                                          
import java.util.*;                                            
import java.sql.Connection;                                    
import java.sql.SQLException;                                  
import org.trinet.jasi.EW.EWSolution;                                  
                                                               
                                                               
/**                                    
 * <!-- Class Description>             
 *                                     
 *                                     
 * Created:  2002/06/12                        
 *                                     
 * @author   ew_oci2java Tool / DK                        
 * @version  0.99                        
 **/                                   
                                       
public class SetPreferStatement extends EWSQLStatement 
{                                      
                                       
  // *** CLASS ATTRIBUTES *** \       
                                       
  // *** CONSTRUCTORS *** \           
  public SetPreferStatement()                          
  {                                    
    sSQLStatement = "Begin Set_Prefer (OUT_idPrefer => :OUT_idPrefer,IN_idEvent => :IN_idEvent,IN_PreferType => :IN_PreferType,IN_idPrefered => :IN_idPrefered); End;";
    bIsQuery = false;                
    init();                            
  }                                    
                                       
                                       
  public SetPreferStatement(Connection IN_conn)        
  {                                    
    this();                            
    SetConnection(IN_conn);            
  }                                    
                                       
                                       
  // *** CLASS METHODS *** \          
                                       
  protected boolean SetInputParams(Object obj)    
  {                                  
                                     
    EWSolution objEWSolution = (EWSolution)obj;              
                                     
    try                              
    {                                
      cs.setLong(2, objEWSolution.idEvent);    
      cs.setLong(3, objEWSolution.PreferType);    
      cs.setLong(4, objEWSolution.idPrefered);    
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in SetPreferStatement:SetInputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end SetPreferStatement::SetInputParams()    
                                     
                                     
  protected boolean RegisterOutputParams()    
  {                                  
    try                              
    {                                
      cs.registerOutParameter(1, java.sql.Types.BIGINT); 
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in SetPreferStatement:RegisterOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
    return(true);                    
  }  // end SetPreferStatement:RegisterOutputParams()
                                     
                                     
  protected boolean RetrieveOutputParams(Object obj)
  {                                  
    EWSolution objEWSolution = (EWSolution)obj;             
                                     
    try                              
    {                                
      objEWSolution.idPrefer=cs.getLong(1);     
    }                                
    catch (SQLException ex)          
    {                                
      System.err.println("Exception in SetPreferStatement:RetrieveOutputParams()"); 
      System.err.println(ex);        
      ex.printStackTrace();          
      return(false);                 
    }                                
                                     
    return(true);                    
  }   // end SetPreferStatement::RetrieveOutputParams()
                                     
                                     
} // end class SetPreferStatement                      
                                       
                                       
//             <EOF>                   
