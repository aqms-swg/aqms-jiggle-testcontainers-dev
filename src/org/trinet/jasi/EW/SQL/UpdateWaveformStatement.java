// *** PACKAGE NAME *** \
package org.trinet.jasi.EW.SQL;


// *** IMPORTS *** \
import java.util.*;
import java.sql.Connection;
import java.sql.SQLException;
import org.trinet.jasi.EW.EWWaveform;


/**
 * <!-- Class Description>
 *
 *
 * Created:  2002/06/17
 *
 * @author   ew_oci2java Tool / DK
 * @version  0.99
 **/

public class UpdateWaveformStatement extends EWSQLStatement
{

  // *** CLASS ATTRIBUTES *** \

  // *** CONSTRUCTORS *** \
  public UpdateWaveformStatement()
  {
    sSQLStatement = "update Waveform  set binSnippet = :IN_RawSnippet  where idWaveform = :IN_idWaveform";
    bIsQuery = false;
    init();
  }


  public UpdateWaveformStatement(Connection IN_conn)
  {
    this();
    SetConnection(IN_conn);
  }


  // *** CLASS METHODS *** \

  protected boolean SetInputParams(Object obj)
  {

    EWWaveform objEWWaveform = (EWWaveform)obj;

    try
    {
      cs.setLong(1, objEWWaveform.idWaveform);
      cs.setBinaryStream(2, isRawSnippet, objEWWaveform.iByteLen);
    }
    catch (SQLException ex)
    {
      System.err.println("Exception in UpdateWaveformStatement:SetInputParams()");
      System.err.println(ex);
      ex.printStackTrace();
      return(false);
    }

    return(true);
  }   // end UpdateWaveformStatement::SetInputParams()



  boolean ConfigureLongRaw(EWWaveform ewObject)
	 {
	   isRawSnippet = new java.io.ByteArrayInputStream(ewObject.RawSnippet);
	   return(true);
	 }


  java.io.InputStream isRawSnippet;

} // end class UpdateWaveformStatement


//             <EOF>
