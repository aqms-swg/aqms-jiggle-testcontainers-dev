package org.trinet.jasi.EW;

import java.util.*;
import java.sql.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.datatypes.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

public class SolutionEW extends Solution
{

  // CONSTANTS
  protected static final int EW_FLAG_EVENTS_VALID_ONLY     =  1;
  protected static final int EW_FLAG_EVENTS_INVALID_ONLY   =  0;
  protected static final int EW_FLAG_EVENTS_ALL            = -1;


  //protected static final int EW_FLAG_ET_ALL              = -1;

  protected static final int EW_FLAG_P_STATE_ALL           = -1;


  long idOrigin;
  EWSolution ewsSolution;
  String sOriginSource;

  public SolutionEW()
  {
    ewsSolution = new EWSolution();
    sOriginSource = "Jiggle0";
    idOrigin = 0;
  }
  // Abstract methods in Solution - aww
  // Id matches existing row key in datasource data are the same 
  public boolean isFromDataSource() { return true; }  // need to implement
  // A matching row key exists in datasource but its actually data may differ from instance
  public boolean existsInDataSource() { return false; }  // need to implement
  public JasiDataReaderIF getDataReader() { return null; } // need to implement
  public JasiObject parseData(Object rsdb) { return null;} // need to implement
  public int loadAlternateMagnitudes() { return 0; } // need to implement
  public int loadAltSolList(boolean clearList) { return 0;} // need to implement
  public int loadMagList(boolean clearList) { return 0;} // need to implement
  public int loadPrefMags() { return 0;} // need to implement
  public int loadPhaseList(boolean clearList) { return 0; } // need to implement
  public int loadWaveformList(boolean clearList) { return 0; } // need to implement
  public int loadAmpList(boolean clearList) { return 0; } // need to implement
  public int loadPrefMagAmpList(boolean clearList) { return 0; } // need to implement
  public int loadPrefMagCodaList(boolean clearList) { return 0; } // need to implement
  public int loadCodaList(boolean clearList) { return 0; } // need to implement
  public int loadMagAmpList(boolean clearList) { return 0; } // need to implement
  public int loadMagCodaList(boolean clearList) {return 0;} // need to implement
  public Collection getAlternatesById(long eventId){ return getAlternatesById(null, eventId); }
  public Collection getAlternatesById(Connection conn, long eventId){ return null; } // need to implement
  public Solution getByPreferredOrid(long orid){ return null; } // need to implement
  public int postId(String group, String source, String state, int rank, int result) { return 0; }
  public String getRegionAuthority() { return null; }
  public String getEventAuthority() { return eventAuthority.toString(); }

  public Collection getValidByTime(double start, double stop)
  {
    return(getByCriteria(start, stop, EW_FLAG_EVENTS_VALID_ONLY,
                         EventTypeMapEW.EW_FLAG_ET_ALL, EW_FLAG_P_STATE_ALL, null));
  }

  public Collection getValidByTime(Connection conn, double start, double stop)
  {
    return(getByCriteria(start, stop, EW_FLAG_EVENTS_VALID_ONLY,
                         EventTypeMapEW.EW_FLAG_ET_ALL, EW_FLAG_P_STATE_ALL, conn));
  }

  public Collection getByTime(Connection conn, double start, double stop)
  {
    return(getByTime(start,stop));
  }

  public Collection getByTime(double start, double stop)
  {
    return(getByCriteria(start, stop, EW_FLAG_EVENTS_ALL,
                         EventTypeMapEW.EW_FLAG_ET_ALL, EW_FLAG_P_STATE_ALL, null));
  }

  public Collection getByProperties(EventSelectionProperties properties)
  {
    return(getByProperties(null,properties));
  }


  public Solution getValidById(long id)
  {
    return(getById(null, id, EW_FLAG_EVENTS_VALID_ONLY));
  }


  public Solution getValidById(Connection conn, long id)
  {
    return(getById(conn, id, EW_FLAG_EVENTS_VALID_ONLY));
  }


  public Solution getById(long id)
  {
    return(getById(null, id, EW_FLAG_EVENTS_ALL));
  }


  public Solution getById(Connection conn, long id)
  {
    return(getById(conn, id, EW_FLAG_EVENTS_ALL));
  }


  public int countWaveforms()
  {
    if(this.waveformList == null)
    {
      Waveform wTemp = Wavelet.create();
      this.waveformList = new ChannelableList(wTemp.getBySolution(this.id.longValue()));
      waveRecords = new DataLong(waveformList.size());
    }
    return(this.waveRecords.intValue());
  }  // end SolutionEW::countWaveforms()


  public String getEventTypeString()
  {
    return(this.eventType.toString());
  }

  public long getOridValue()
  {
    return(this.idOrigin);
  }

  public boolean hasChanged()
  {
    /**@todo: implement this org.trinet.jasi.Solution abstract method*/
    /* DK CLEANUP  This may not be the best way to determine if a
     * solution has changed.  We are checking to see if we have created
     * a new origin, or if some part of jiggle code has set our
     * Solution to stale, indicating that something was changed.
     **/
    if(this.idOrigin <= 0 || this.isStale())
      return(true);
    else
      return(false);
  }

  public void setUpdate(boolean tf)
  {
    /**@todo: implement this org.trinet.jasi.Solution abstract method*/
    // DK CLEANUP
  }

  public boolean finalCommit() throws org.trinet.jasi.JasiCommitException
  {
    /**@todo: implement this org.trinet.jasi.Solution abstract method*/
    return(false);
  }


  public boolean setEventType(int type)
  {
    if(EventTypeMap.isValid(type))
    {
      this.eventType.setValue(EventTypeMap.get(type));
      return(true);
    }
    else
    {
      return(false);
    }
  }

  public boolean setEventType(String type)
  {
    if(EventTypeMap.isValid(type))
    {
      this.eventType.setValue(type);
      return(true);
    }
    else
    {
      return(false);
    }
  }

  public boolean setOriginGType(String type) {
      return false;
  }

  public long setUniqueId()
  {
    ewsSolution = new EWSolution();
    ewsSolution.Write();
    if(ewsSolution.idEvent <= 0)
    {
      // we bombed.  log it and return false.
      System.out.println("SolutionEW:  Failed with code " + ewsSolution.idEvent +
                          " while trying to Save event(" + ewsSolution.sSource +
                          "," + ewsSolution.sSourceEventID + ")");
    }
    this.id = new DataLong(ewsSolution.idEvent);

    return(ewsSolution.idEvent);
  }

  public boolean rollbackPrefs() {
      return false;
  }

  public boolean commitReadings() throws org.trinet.jasi.JasiCommitException
  {
          return false;  // not implemented, need to
  }
  public boolean commit() throws org.trinet.jasi.JasiCommitException
  {
    boolean bRetCode = true;

    /**@todo: implement this org.trinet.jasi.Solution abstract method*/
    /* don't write ANYTHING - phases, magnitudes, stamags - unless
       the origin is new.
       What we should really be doing, is checking to see if the origin
       and magnitudes are stale. ????
     */
    if(this.idOrigin <= 0)
    {
       // Check to make sure that we have the solution locked
       if(!this.lockIsMine())
       {
         // We don't have event locked. disallow the commit.
         return(false);
       }

       // copy this object back to an EWSolution object
       SolEW2EWSol();

       if(this.id.longValue() <= 0)
         // if the solution has no DBid, then write the Event back to the DB
         bRetCode = bRetCode && (ewsSolution.Write() == null);

       if(ewsSolution.idEvent <= 0)
       {
        // we bombed.  log it and return false.
        System.out.println("SolutionEW:  Failed with code " + ewsSolution.idEvent +
                            " while trying to Save event(" + ewsSolution.sSource +
                            "," + ewsSolution.sSourceEventID + ")");
        return(false);
       }
       this.id.setValue(ewsSolution.idEvent);

      // Now process the phase list
      Phase[] Phases = (Phase[])this.phaseList.toArray(new Phase[0]);
      for(int i=0; i < Phases.length; i++)
      {
        // call commit for each phase.  PhaseEW.Commit() will determine
        // what if anything needs to be done
        Phases[i].commit();
      }

      // Now write the Origin
      bRetCode = bRetCode && (ewsSolution.ewOrigin.Write() != null);
      if(ewsSolution.ewOrigin.idOrigin <= 0)
      {
        return(false);
      }
      else
      {
        // copy the idOrigin from ewOrigin back to this object
        this.idOrigin = ewsSolution.ewOrigin.idOrigin;
      }

      /* now we have to do the phase associations */
      for(int i=0; i < Phases.length; i++)
      {
        // Call Phase.associate() it will determine what if anything needs
        //  to be done
        ((PhaseEW)Phases[i]).idOrigin = this.idOrigin;
        ((PhaseEW)Phases[i]).associate();
      }

      this.ewsSolution.idEvent = this.id.longValue();

      this.ewsSolution.idPrefered = this.ewsSolution.idOrigin = this.idOrigin;
      this.ewsSolution.PreferType = this.ewsSolution.PREFER_TYPE_ORIGIN;
      this.ewsSolution.UpdatePrefer();

      this.commitStatusMessage = new StringBuffer("Solution "+this.id.longValue()+" successfully saved to EW DB.");
      this.setStale(false);
    }
    else
    {
      bRetCode = false;
      this.commitStatusMessage = new StringBuffer("Solution "+this.id.longValue()+" already in DB. Not updated.");
    }

    // Check for a magnitude, commit it if there is one.
    if(this.magnitude != null)
    {
      bRetCode = bRetCode && this.magnitude.commit();
      if(this.magnitude != null)
      {
         this.ewsSolution.idPrefered = this.ewsSolution.idMag   = this.magnitude.magid.longValue();
         this.ewsSolution.PreferType = this.ewsSolution.PREFER_TYPE_MAGNITUDE;
         this.ewsSolution.UpdatePrefer();
         this.commitStatusMessage = new StringBuffer(this.commitStatusMessage +
                         " Magnitude " + this.magnitude.magid.longValue() +
                         " successfully saved to EW DB.");
      }
    }

   return(bRetCode);
  }


  protected boolean SolEW2EWSol()
  {
    EWSolution ewSol;
    EWOrigin   ewOr;

    // We want to toss the existing ewSolution record, because
    // it might have old data, and we don't want any secret junk
    // hanging around.
    ewSol = ewsSolution = new EWSolution();
    ewOr = ewSol.ewOrigin = new EWOrigin();

    if(this.id.equals(new DataLong()))
    {
      // no idEvent Yet.  Need to create new event
      ewSol.idEvent = 0;
    }
    else
    {
      // already have an idEvent, copy it over
      ewSol.idEvent = this.id.longValue();
    }
    ewSol.tiEventType = 2;
    // DK CLEANUP We need to write an adapter to convert to from tiEventType
    //ewSol.tiEventType = EventTypeMap.getIntOfJasiCode(
    //                     EventTypeMap.toLocalCode(this.eventType.toString()));

    ewSol.iDubiocity = (this.validFlag.intValue()==0)?1:0;
    ewSol.sComment = this.comment.toString();
    ewSol.sSource =  this.source.toString();

    if(this.externalId.equals(new DataString()))
    {
     // no externalID either.  Don't know what we were expecting
     // make up our own id via cTime.
     long tNow = System.currentTimeMillis() / 1000;
     this.externalId = new DataString(tNow);
     ewSol.sSourceEventID = String.valueOf(tNow);
    }


    ewOr.idEvent = ewSol.idEvent;
    ewOr.idOrigin = 0;
    ewOr.dLat = this.lat.floatValue();
    ewOr.dLon = this.lon.floatValue();
    ewOr.dDepth = this.depth.floatValue();
    ewOr.tOrigin = this.datetime.doubleValue();

    ewOr.iFixedDepth = (this.depthFixed.booleanValue())?1:0;
    ewOr.dDmin = this.distance.floatValue();
    ewOr.dErLat = this.errorLat.floatValue();
    ewOr.dErLon = this.errorLon.floatValue();
    ewOr.dErZ = this.errorVert.floatValue();
    ewOr.sSource = this.source.toString();  // DK Cleanup this is probably sHumanReadable
    ewOr.iGap = this.gap.intValue();
    ewOr.dRMS = this.rms.floatValue();
    ewOr.iUsedPh = this.usedReadings.intValue();

    return(true);
  }

  protected static SolutionEW EWSol2SolEW(EWSolution ewSol)
  {
    SolutionEW solEW = new SolutionEW();


    solEW.lat = new DataDouble(ewSol.dLat);
    solEW.lon = new DataDouble(ewSol.dLon);
    solEW.depth = new DataDouble(ewSol.dDepth);
    solEW.id  = new DataLong(ewSol.idEvent);
    solEW.idOrigin = ewSol.idOrigin;
    solEW.datetime = new DataDouble(ewSol.tOrigin);
    solEW.magnitude = Magnitude.create();
    solEW.magnitude.value = new DataDouble(ewSol.dPrefMag);
    solEW.magnitude.sol = solEW;
    solEW.authority = new DataString(ewSol.sHumanReadable);  // DK CLEANUP
    solEW.dummyFlag = new DataLong((ewSol.iDubiocity != 0)?1:0);
    solEW.source = new DataString(ewSol.sHumanReadable);
    solEW.eventSource = new DataString(ewSol.sHumanReadable);
    solEW.sOriginSource = ewSol.sSource;
    solEW.eventType = new DataString(org.trinet.jasi.EventTypeMap.get(ewSol.tiEventType)); // DK CLEANUP
    solEW.externalId = new DataString(ewSol.sSourceEventID);
    if(ewSol.sHumanReadable != null)
    {
      if(ewSol.sHumanReadable.charAt(3) == '-')
      {
        // we have a standardized author string
        solEW.eventAuthority = new DataString(ewSol.sHumanReadable.substring(0,2));
        if(ewSol.sHumanReadable.indexOf("Automatic") != -1)
          solEW.processingState = new DataString(JasiProcessingConstants.STATE_AUTO_LABEL);
        else if(ewSol.sHumanReadable.indexOf("Reviewed") != -1)
          solEW.processingState = new DataString(JasiProcessingConstants.STATE_HUMAN_LABEL);
        else
          solEW.processingState = new DataString(JasiProcessingConstants.STATE_UNKNOWN_LABEL);
      }
      else
      {
        solEW.authority = new DataString(ewSol.sHumanReadable);
        solEW.processingState = new DataString(JasiProcessingConstants.STATE_UNKNOWN_LABEL);
      }
    }

    if(ewSol.bArchived != 0)
      solEW.processingState = new DataString(JasiProcessingConstants.STATE_FINAL_LABEL);

    solEW.validFlag =  new DataLong((ewSol.iDubiocity == 0)?1:0);


    if(ewSol.ewOrigin != null)
    {
      solEW.comment = new DataString(ewSol.ewOrigin.sComment);
      solEW.datetime = new DataDouble(ewSol.ewOrigin.tOrigin);
      solEW.depth = new DataDouble(ewSol.ewOrigin.dDepth);
      solEW.depthFixed = new DataBoolean((ewSol.ewOrigin.iFixedDepth != 0)?true:false);
      solEW.distance = new DataDouble(ewSol.ewOrigin.dDmin);
      //solEW.errorHoriz = new DataDouble(ewSol.ewOrigin.dEr?);
      solEW.errorLat = new DataDouble(ewSol.ewOrigin.dErLat);
      solEW.errorLon = new DataDouble(ewSol.ewOrigin.dErLon);
      solEW.errorVert = new DataDouble(ewSol.ewOrigin.dErZ);
      // ?  solEW.errorTime = new DataDouble(ewSol.ewOrigin.dRMS);
      if(ewSol.ewOrigin.sHumanReadable.charAt(3) == '-')
      {
        // we have a standardized author string
        solEW.authority = new DataString(ewSol.ewOrigin.sHumanReadable.substring(0,2));
        if(solEW.eventAuthority == null)
          solEW.eventAuthority = new DataString(ewSol.sHumanReadable.substring(0,2));

        if(solEW.processingState == null)
        {
          if(ewSol.ewOrigin.sHumanReadable.indexOf("Automatic") != -1)
            solEW.processingState = new DataString(JasiProcessingConstants.STATE_AUTO_LABEL);
          else if(ewSol.ewOrigin.sHumanReadable.indexOf("Reviewed") != -1)
            solEW.processingState = new DataString(JasiProcessingConstants.STATE_HUMAN_LABEL);
          else
            solEW.processingState = new DataString(JasiProcessingConstants.STATE_UNKNOWN_LABEL);
        }
      }
      else
      {
        if(solEW.eventAuthority == null)
          solEW.eventAuthority = new DataString(ewSol.sHumanReadable.substring(0,2));

        solEW.authority = new DataString(ewSol.ewOrigin.sHumanReadable);
        if(solEW.processingState == null)
          solEW.processingState = new DataString(JasiProcessingConstants.STATE_UNKNOWN_LABEL);
      }

      solEW.source = new DataString(ewSol.ewOrigin.sHumanReadable);
      solEW.gap    = new DataDouble(ewSol.ewOrigin.iGap);
      solEW.lat    = new DataDouble(ewSol.ewOrigin.dLat);
      solEW.lon    = new DataDouble(ewSol.ewOrigin.dLon);
      solEW.magnitude = MagnitudeEW.EWMag2MagEW(ewSol.ewOrigin.ewMagnitude);
      solEW.rms = new DataDouble(ewSol.ewOrigin.dRMS);
      solEW.type = new DataString(Solution.HYPOCENTER);
      solEW.usedReadings = new DataLong(ewSol.ewOrigin.iUsedPh);
      solEW.vertDatum = new DataString("km");
      solEW.comment = new DataString(ewSol.ewOrigin.sComment);
    }  // end if ewSol.ewOrigin != null

    solEW.ewsSolution = ewSol;
    return(solEW);
  }

  protected Vector EWSolutionList2SolutionEWList(Vector vEWSolutionList)
  {
      Vector vSolutionEWList = new Vector(vEWSolutionList.size());
      int iDebugTotal; // DK CLEANUP  WE ARE USING THIS TO LIMIT THE NUMBER
                                         // OF EVENTS IN LIST TO 200.
      iDebugTotal = (vEWSolutionList.size() < 200)?vEWSolutionList.size():200;

      for(int i=0; i < iDebugTotal; i++)
      {
        vSolutionEWList.addElement((Object)EWSol2SolEW((EWSolution)vEWSolutionList.get(i)));
      }
      return(vSolutionEWList);
  }


  protected Collection getByCriteria(double start, double stop, int iValidFlag, int iEventType,
                                     int iProcessingState, Connection IN_conn)
  {
    // Create a temporary EWSolution object for use in retrieving the list
    EWSolution tempEWS = new EWSolution();

    if(IN_conn != null)
      tempEWS.SetConnection(IN_conn);

    // Declare a Vector pointer to hold the list
    Vector vEWSolutionList;

    // Set the criteria params
    tempEWS.starttime = (int)start;
    tempEWS.endtime = (int)stop;
    if(iEventType != -1)
      tempEWS.eventtype = iEventType;

    // fetch the data
    vEWSolutionList = tempEWS.ReadList();

    if(vEWSolutionList == null || vEWSolutionList.size() == 0)
      return(null);
    else
    {
      Vector vSolutionEWList = EWSolutionList2SolutionEWList(vEWSolutionList);

      // Filter out events based upon validity(if the appropriate flag is set)
      if(iValidFlag != -1)
      {
        for(int i=0; i < vSolutionEWList.size(); i++)
        {
          if(((SolutionEW) vSolutionEWList.get(i)).validFlag.longValue() != iValidFlag)
         {
            vSolutionEWList.remove(i);
            i--;
          }
        }
      }

      // Filter out events based upon processing state(if the appropriate flag is set)
      if(iProcessingState != JasiProcessingConstants.STATE_UNKNOWN)
      {
        for(int i=0; i < vSolutionEWList.size(); i++)
        {
          if( (ProcessingState.labelToId(((SolutionEW) vSolutionEWList.get(i)).processingState.toString()))
             != iProcessingState)
          {
            vSolutionEWList.remove(i);
            i--;
          }
        }
      }

      //Solution[] SolutionArray = new Solution[vSolutionEWList.size()];
      //System.arraycopy(vSolutionEWList.toArray(), 0, SolutionArray, 0, vSolutionEWList.size());
      //return(SolutionArray);
      return(vSolutionEWList);
    }
  }  // end protected SolutionEW::getByCriteria()


  public Collection getByState(String state) { return null; }

  protected Solution getById(Connection IN_conn, long id, int iValidEvent)
  {
    // Create a temporary EWSolution object for use in retrieving the solution
    EWSolution tempEWS = new EWSolution();

    if(IN_conn != null)
      tempEWS.SetConnection(IN_conn);

    // Set the idEvent, used to retrieve the Event
    tempEWS.idEvent = id;

    // Grab the Event
    tempEWS.Read();

    if(iValidEvent == EW_FLAG_EVENTS_VALID_ONLY)
      if(tempEWS.iDubiocity != 0)
      {
        // Event is not valid, and VALID-ONLY was requested
        return(null);
      }
    else if(iValidEvent == EW_FLAG_EVENTS_INVALID_ONLY)
      if(tempEWS.iDubiocity == 0)
      {
        // Event is valid, and INVALID-ONLY was requested
        return(null);
      }

    // Get the preferred info (origin,mag,mech)
    tempEWS.ReadPreferred();

    // Get the preferred origin info
    if(tempEWS.idOrigin > 0)
    {
      tempEWS.ewOrigin = new EWOrigin();
      tempEWS.ewOrigin.idOrigin = tempEWS.idOrigin;
      tempEWS.ewOrigin.Read();

      // Get the list of Origins for this event
      tempEWS.ewOrigin.idEvent = id;
      tempEWS.vOriginList = tempEWS.ewOrigin.ReadList();

      // Get the list of Magnitudes for the preferred origin
      tempEWS.ewOrigin.ewMagnitude = new EWMagnitude();
      tempEWS.ewOrigin.ewMagnitude.idMag = tempEWS.idMag;
      tempEWS.ewOrigin.ewMagnitude.Read();
      tempEWS.ewOrigin.ewMagnitude.idOrigin = tempEWS.idOrigin;
      tempEWS.ewOrigin.vMagList = tempEWS.ewOrigin.ewMagnitude.ReadList();
  // DK DEBUG Shouldn't we be grabbing the preferred mag info somewhere here??
    }

    return((Solution)EWSol2SolEW(tempEWS));
  }  // end protected SolutionEW::getById()


  public Collection getByProperties (Connection conn,
               EventSelectionProperties props)
  {

    // Derived from the SolutionTN::getByProperties() function

    int iValidFlag = EW_FLAG_EVENTS_ALL;
    int iEventType = EventTypeMapEW.EW_FLAG_ET_ALL;
    int iProcessingState = EW_FLAG_P_STATE_ALL;
    String str;

    // Get time from properties. This will handle both "absolute" and "relative"
    // time definitions

    org.trinet.util.TimeSpan span = props.getTimeSpan();
    double start = span.getStart();
    double stop  = span.getEnd();

    // validFlag like NCEDC event.selectFlag
    Boolean tf = props.getEventValidFlag();
    if (tf != null) {
      if (tf.booleanValue())
        iValidFlag = EW_FLAG_EVENTS_VALID_ONLY;
      else
        iValidFlag = EW_FLAG_EVENTS_INVALID_ONLY;
    }

    // dummyFlag like NCEDC origin.bogusFlag
    tf = props.getOriginDummyFlag();
    if (tf != null) {
      if (tf.booleanValue())
        iValidFlag = 0;
      else
        iValidFlag = 1;
    }


    // Event types (e.g. "local", "sonic", etc.) - these must be OR'ed
    // These properties have the form:  SelectAttribute_trigger = FALSE
    // There may not be a property for each type, thus the check for != null.
    String typeChoice[] = EventTypeMap.getJasiTypeArray();

    for (int i = 0; i< typeChoice.length; i++)
    {
      str = props.getProperty(EventSelectionProperties.prefix+typeChoice[i]);
      if (str != null && str.equalsIgnoreCase("TRUE"))
      {
        iEventType = EventTypeMapEW.EWEventTypes[i];
        break;
      }
    }

    // Processing States
    String label[] = ProcessingState.getLabelArray();

    for (int i = 0; i< label.length; i++)
    {
      str = props.getProperty(EventSelectionProperties.prefix+label[i]);
      if (str != null && str.equalsIgnoreCase("TRUE"))
      {
        iProcessingState = i;
        break;
      }
    }

    return(getByCriteria(start, stop, iValidFlag, iEventType,
                                        iProcessingState, null));



  }  // end SolutionEW::getByProperties()

  public Solution refresh(EventSelectionProperties props) {
      return null;
  }

  long GetidOrigin()
  {
    return(idOrigin);
  }

public void clearLocationAttributes ()
{
  // reset idOrigin since this is a new solution
  this.idOrigin = 0;
  super.clearLocationAttributes();
}

public boolean equals(Object obj)
{
  if(obj instanceof SolutionEW)
  {
    return(this.id == ((SolutionEW)obj).id);
  }
  else
  {
    return(super.equals(obj));
  }
}

public long getNextId()
{
  return(-1);  // DK Not implemented
   //   TN version should look like:
   //   return(SeqIds.getNextSeq(CONNECTION, "EVSEQ"));
}

  public boolean isQuarry() {
      return false;
  }

  public boolean isInsideRegion(String regionName) {
      return true;
  }

  public long getAnyDuplicate() {
      return 0l;
  }
}
//
// end SolutionEW
//
