package org.trinet.jasi;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.HashMap;

/**
 * Static class with information about the application's environment including
 * the application name so that it can be set and accessed globally. Java does
 * not have the equivilent of C's arg[0] so the main application must identify
 * itself by calling setApplicationName() in this class so other classes can use
 * it.
 *
 * @author Doug Given
 * @version */

public class EnvironmentInfo {

    public static final String DEFAULT_NETCODE = "??";
/** The name of the application */
    private static String appName = "unknown";

    /** Then 2-char FDSN seismic network code. */
    private static String netCode  = DEFAULT_NETCODE; // 2 char, no blanks -aww 11/03/2004
    private static String [] authNetCodes = null;

    /** True if data is being produced automatically. False if by a human. */
    private static boolean automatic = true;

    // Removed location kludge here - aww 01/10/2008
    /*
    private static HashMap locationMap = new HashMap(11);
    static {
        locationMap.put("CI","  "); // changed default to "  " from 01 - aww 12/08/2005
        locationMap.put("BK","  ");
    }
    */

    private EnvironmentInfo() { }

    /**  Get application name. */
    public static String getApplicationName() {
        return appName;
    }

    public static boolean hasApplicationName() {
        return ! appName.equals("unknown") && ! appName.equals("");
    }

    /** Set application name. */
    public static void setApplicationName(String str) {
        appName = str.trim();
    }

    /*
    // Removed location kludge below here - aww 01/10/2008
    //
    //Get channel location code default for the local network code.
    //@see #getNetworkCode()
    //
    public static String getLocationCode() {
        return getLocationCode(netCode);
    }

    //Get default channel location code for the named network. 
    public static String getLocationCode(String net) {
        String loc = (String) locationMap.get(net);
        return (loc == null) ?  "??" : loc;
    }
    // Set default channel location code for the local network.
    // @see #setLocationCode(String net, String loc)
    // @see #getNetCode()
    //
    public static void setLocationCode(String loc) {
        locationMap.put(netCode, loc);
    }
    //Set default channel location code for the named network.
    public static void setLocationCode(String net, String loc) {
        locationMap.put(net, loc);
    }
    */

    /** Convenience method returns user name. If unknown return "". This is not
        setable. */
    public static String getUsername() {
        // could use appName as prefix string but JASI is more generic for bat scripts -aww
        String userName = System.getProperty("JASI_USER_NAME"); // changed from "JIGGLE" -aww 11/04/2004
        return ( userName != null) ? userName : System.getProperty("user.name", "");
    }

    /** Convenience method returns local host where method is invoked.
     *  If unknown returns "". This is not setable.
     *  Returned String is of the form: "serverbo.gps.caltech.edu".
     *  */
    public static String getLocalHostName() {
        try {
          // This returns form: "serverbo.gps.caltech.edu/XXX.XXX.XX.174"
          //return InetAddress.getLocalHost().toString();
          // This returns form: "serverbo.gps.caltech.edu"
          return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException ex) { }
        return "";
    }

    public static String getLocalHostShortName() {
        String name = getLocalHostName();
        int dot = name.indexOf(".");
        return (dot == -1 || name.length() == 0) ? name : name.substring(0, dot);
    }

    // for those operating more than 1-netcode region, lists those network region codes 
    // since on solution commit Jiggle test of event type as local/regional is based on
    // being inside these net regions or not - aww 2009/12/10
    public static boolean hasAuthNetCodes() {
        return (authNetCodes != null && authNetCodes.length > 0);
    }

    public static String [] getAuthNetCodes() {
        return (authNetCodes == null) ? new String[0] : authNetCodes;
    }

    public static void setAuthNetCodes(String[] codes) {
        authNetCodes = codes;
    }

    /**
    * Return the 2-char FDSN network ID.
    */
    public static String getNetworkCode() {
      return netCode;
    }

    public static boolean hasNetworkCode() {
        return ! netCode.equals("??"); // blank code ok
    }

    /**
    * Set the 2-char FDSN network ID. If the string is longer then 2 chars it
    * will be truncated. If it is shorter it will be padded with spaces.
    * A null input is a no-op; 
    */
    public static void setNetworkCode(String str) {
      if (str == null) return; // a no-op ? why not the same as "  "? - aww
      if (str.length() == 0) {
        netCode = "  ";
      } else if (str.length() == 1) {
        netCode = str + " ";
      } else {
        netCode = str.substring(0, 2);
      }
    }

    /** Set if automatic t/f. */
    public static void setAutomatic(boolean tf) {
      automatic = tf;
    }

    /** Returns true if automatic is set true. */
    public static boolean isAutomatic() {
      return automatic;
    }

    /** If 'automatic' is true return "A", if not return "H" (human) */
    public static String getAutoString() {
      if (automatic) return  JasiProcessingConstants.STATE_AUTO_TAG;
      else return  JasiProcessingConstants.STATE_HUMAN_TAG;
    }

    /** Return a summary string of the fields in this class. 
     * (can't call this 'toString()' because that conflicts with non-static method
     * in parent class. */
    public static String getString() {
        StringBuffer sb = new StringBuffer(128);
        sb.append(" application: ").append(appName);
        sb.append(" network: ").append(netCode);
        sb.append(" automatic: ").append(automatic);
        sb.append(" user: ").append(getUsername());
        sb.append(" localHost: ").append(getLocalHostName());
        sb.append(" authNetCodes:");
        if (authNetCodes == null) sb.append(" NULL");
        else {
           for (int ii = 0; ii < authNetCodes.length; ii++) {
               sb.append(" ").append(authNetCodes[ii]);

           }
        }
        //sb.append(" locationCodeMap: ").append(locationMap.toString()); // Removed - aww 01/10/2008
        return sb.toString();
    }

    public static void dump() {
        System.out.println("EnviromentInfo " + getString());
    }

} 
