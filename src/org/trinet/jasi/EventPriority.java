package org.trinet.jasi;

/** Given magnitude, origin time and quality for an event calculate the "priority" of the event.  Just a wrapper 
 * @deprecated
 * @see org.trinet.storedprocs.EventPriority
*/
public class EventPriority extends org.trinet.storedprocs.eventpriority.EventPriority {
    public EventPriority() {
        super();
    }
}
