package org.trinet.jasi;
import org.trinet.jiggle.common.type.EventDisplayType;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
//import java.util.StringTokenizer;

/**
 * Map database schema event types to jasi event type name labels and vice versa.
 */
public abstract class EventTypeMap3 extends JasiObject {

    // Event etype codes
    public static String EARTHQUAKE =   "eq";
    public static String QUARRY =       "qb";
    public static String EXPLOSION =    "ex";
    public static String SONIC =        "sn";
    public static String LONGPERIOD =   "lp";
    public static String V_TREMOR =     "vt";
    public static String NV_TREMOR =    "tr";
    public static String TRIGGER =      "st";
    public static String NUCLEAR =      "nt";
    public static String OTHER =        "ot";
    public static String UNKNOWN =      "uk";
    public static String SLOW =         "se";
    public static String TORNILLO =     "to";
    public static String CALIBR =       "ce";
    public static String SHOT =         "sh";
    public static String THUNDER =      "th";
    public static String ERUPTION =     "ve";
    public static String COLLAPSE =     "co";
    public static String DEBRIS =       "df";
    public static String AVALANCHE =    "av";
    public static String LANDSLIDE =    "ls";
    public static String ROCKBURST =    "rb";
    public static String ROCKSLIDE =    "rs";
    public static String BLDG =         "bc";
    public static String PLANE =        "pc";
    public static String METEOR =       "mi";
    public static String SURFACE =      "su";
    public static String LOWFREQ =      "lf";
    public static String PROBEX =       "px";
    
    protected static EventType [] eventTypes = null;

    protected static String jasiDefault = "unknown";
    protected static String dbDefault = UNKNOWN;

    public static final EventTypeMap3 MAP = EventTypeMap3.create();

    protected EventTypeMap3() {
    }

    public static final EventTypeMap3 create() {
        return create(DEFAULT);
    }

    public static final EventTypeMap3 create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
    }

    public static final EventTypeMap3 create(String suffix) {
        return (EventTypeMap3) JasiObject.newInstance("org.trinet.jasi.EventTypeMap3", suffix);
    }

    public abstract void loadEventTypes();

    public static EventTypeMap3 getMap() {
        if (MAP != null && MAP.getEventTypes() == null) MAP.loadEventTypes();
        return MAP;
    }

    /** Return the default event type database String. */
    public static String getDefaultDbType() {
        return dbDefault;
    }

    /** Return the default event type jasi String. */
    public static String getDefaultJasiType() {
        return jasiDefault;
    }

    /** Returns true if the input String is a valid jasi event type. This is case insensitive.
    */
    public static boolean isValidJasiType(String jasiType) {
        if (eventTypes == null || jasiType == null) return false;
        for (int i = 0; i < eventTypes.length; i++) {
            if (jasiType.equalsIgnoreCase(eventTypes[i].name)) return true;
        }
        return false;
    }
    public static boolean isValidDbType(String dbType) {
        if (eventTypes == null || dbType == null) return false;
        for (int i = 0; i < eventTypes.length; i++) {
            if (dbType.equalsIgnoreCase(eventTypes[i].code)) return true;
        }
        return false;
    }


    /** Returns true if the input is a valid event type. This is case insensitive.*/
    public static boolean isValidIndex(int idx) {
        return (idx > -1 && (eventTypes != null && idx < eventTypes.length)) ? true : false;
    }

    /** Given a jasi event type return the corresponding TriNet schema type. */
    public static String toDbType(String jasiType) {
        if (jasiType == null) return dbDefault;

        int count = (eventTypes == null) ? 0 : eventTypes.length;
        for (int i = 0; i < count; i++) {
            if (jasiType.equalsIgnoreCase(eventTypes[i].name)) return eventTypes[i].code;
        }
        return dbDefault;
    }

    /** Given a database schema type return the corresponding jasi event type. */
    public static String fromDbType(String dbType) {
        if (dbType == null) return jasiDefault;
        int count = (eventTypes == null) ? 0 : eventTypes.length;
        for (int i = 0; i < count; i++) {
            if (dbType.equalsIgnoreCase(eventTypes[i].code)) return eventTypes[i].name;
        }
        return jasiDefault;
    }

    /** Given a database schema type return the corrisponding jasi event type.*/
    public static String toJasiType(String dbType) {
        return fromDbType(dbType);
    }

    /** Returns index of matching EventType in EventType array. Returns -1 if not valid.*/
    public static int getIndexOfDbType(String dbType) {
        if (dbType == null) return -1;
        int count = (eventTypes == null) ? 0 : eventTypes.length;
        for (int i = 0; i < count; i++) {
            if (dbType.equalsIgnoreCase(eventTypes[i].code)) return i;
        }
        return -1;
    }

    /** Returns index of matching EventType in EventType array. Returns -1 if not valid.*/
    public static int getIndexOfJasiType(String jasiType) {
        if (jasiType == null) return -1;
        int count = (eventTypes == null) ? 0 : eventTypes.length;
        for (int i = 0; i < count; i++) {
            if (jasiType.equalsIgnoreCase(eventTypes[i].name)) return i;
        }
        return -1;
    }

    /** Return the EventType at index offset. If not a valid value, returns 'unknown'.*/
    public static EventType getEventTypeByIndex(int idx) {
        return (isValidIndex(idx)) ? eventTypes[idx] : null;
    }

    public static EventType getEventTypeByDbType(String dbType) {
        if (eventTypes == null || dbType == null) return null;
        for (int ii = 0; ii < eventTypes.length; ii++) {
            if (eventTypes[ii].code.equals(dbType)) return eventTypes[ii];
        }
        return null;
    }

    public static EventType getEventTypeByJasiType(String jasiType) {
        if (eventTypes == null || jasiType == null) return null;
        for (int ii = 0; ii < eventTypes.length; ii++) {
            if (eventTypes[ii].name.equals(jasiType)) return eventTypes[ii];
        }
        return null;
    }

    public static EventType [] getEventTypes() {
        return eventTypes;
    }

    public static ArrayList<EventDisplayType> getEventDisplayType() {
        ArrayList<EventDisplayType> events = new ArrayList();
        for (EventType event: eventTypes) {
            events.add(new EventDisplayType(event));
        }
        Collections.sort(events);
        return events;
    }

    public static String [] getDbTypeArray() {
        if (eventTypes == null) return new String[0];
        String [] types = new String[eventTypes.length];
        for (int ii = 0; ii < eventTypes.length; ii++) {
            types[ii] = eventTypes[ii].code;
        }
        return types;
    }

    public static String [] getJasiTypeArray() {
        if (eventTypes == null) return new String[0];
        String [] types = new String[eventTypes.length];
        for (int ii = 0; ii < eventTypes.length; ii++) {
            types[ii] = eventTypes[ii].name;
        }
        return types;
    }

    public static String [] getDescTypeArray() {
        if (eventTypes == null) return new String[0];
        String [] types = new String[eventTypes.length];
        for (int ii = 0; ii < eventTypes.length; ii++) {
            types[ii] = eventTypes[ii].desc;
        }
        return types;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(1024);
        for (int idx = 0; idx < eventTypes.length; idx++) {
            if (eventTypes != null) sb.append(eventTypes[idx].toString()+"\n");
        }
        return sb.toString();
    }

} // EventTypeMap3
