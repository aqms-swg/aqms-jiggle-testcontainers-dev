package org.trinet.jasi;

import java.sql.*;
import java.text.*;
import java.util.*;

import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;

/**
 * A list of solutions that make a catalog.  It is constrained by ranges or
 * limits to parameters like start-end time, magnitude range, region, etc. that
 * are set in an instance of EventSelectionProperties (only box regional
 * selection is implemented).  Specific Solutions objects can be added or
 * removed.<p>
 * NOTE: If many of the Solutions in the list have their data lists loaded
 * with elements their will be major heap memory consumption.
 **/


public class GenericSolutionList extends AbstractCommitableList
    implements StripableResidualIF {

    protected static final int INIT_CAPACITY = 50;

    public static final int ASCENDING = 0;
    public static final int DESCENDING = 1;

    /** Properties which define the filtering parameters for selecting
     * events from the data source.
     */
    protected EventSelectionProperties props;


    /** Connection that will be used for data source access.
     * Uses the default DataSource connection, unless the
     * connection is reset.
     * @see #setConnection(Connection)
     * */
    protected Connection conn = (DataSource.isNull()) ? null : DataSource.getConnection();

    /** Make an empty GenericSolutionList */
    public GenericSolutionList() {
        super(INIT_CAPACITY);
    }
    /** Make an empty GenericSolutionList with the specified initial capacity.*/
    public GenericSolutionList(int capacity) {
        super(capacity);
    }
    /** Add elements from the input GenericSolutionList.
     * A shallow reference copy, elements are not cloned.
     * */
    public GenericSolutionList(Collection coll) {
        super(coll);
    }

    /** Create a GenericSolutionList containing Solutions created from the
     * default DataSource based on input properties parameters. */
    public GenericSolutionList(EventSelectionProperties properties) {
        super(INIT_CAPACITY);
        props = properties;
        Collection aList = Solution.create().getByProperties(props);
        if (aList != null) addAll(aList, false);

    }

    /** Create a GenericSolutionList containing a Solution created from the
     * default DataSource whose id (evid) matches the input value.
     * */
    public GenericSolutionList(long evid) {
        super(INIT_CAPACITY);
        fetchById(evid);
    }

    /** Create a GenericSolutionList containing the Solutions created from
     * the default DataSource that fall with the specified input time window.
     * */
    public GenericSolutionList(double start, double stop) {
        super(INIT_CAPACITY);
        fetchByTime(start, stop);
    }

    /**
     * Sets the Connection used to access the data source.
     * Overrides the default DataSource Connection.
     */
    public void setConnection(Connection conn) {
        this.conn = conn;
    }

    /**
     * Returns the Connection used to access the data source.
     */
    public Connection getConnection() {
        return conn;
    }

    /**
     * Set the event selection properties.
     */
    public void setProperties(EventSelectionProperties object) {
        this.props = object;
    }

    /**
     * Returns the event selection properties
     */
    public EventSelectionProperties getProperties() {
        return this.props;
    }

    /**
     * Thin wrapper just does the cast from Object to Solution.
     */
    public Solution getSolution(int index) {
        return (Solution) get(index);
    }

    /**
     * Provides comparator() method to sort Solutions in list by time.
     * If times the same than order by event id. Event ids
     * have 0 values if not set.
     */
    public class SolutionTimeSorter implements Comparator {
        /** ComparatorIF for component sort. */
        public int compare (Object o1, Object o2) {
            // wrong object types, void class cast exception
            if ( ! (o1 instanceof Solution && o2 instanceof Solution) ) return 0;

            Solution s1 = (Solution) o1;
            Solution s2 = (Solution) o2;
            boolean v1 = s1.hasValidDateTime();
            boolean v2 = s2.hasValidDateTime();

            // check times
            double diff = 0.;
            if (v1 && v2) {
              diff = s1.getTime() - s2.getTime();
              if (Math.abs(diff) < .00001) diff = 0.; // added this test to handle weird roundoff? -aww 2010/10/20
              if (diff < 0.0) return -1;
              else if (diff > 0.0) return 1;
            }
            // Consider null times lesser value (since null id is 0)
            else if (!v1 && v2) return -1;
            else if (v1 && !v2) return  1;

            // Same times, so compare id values
            diff = s1.getId().longValue() - s2.getId().longValue();
            if (diff < 0.0) return -1;
            else if (diff > 0.0) return 1;

            return 0; // ids match
        }
    } // end of sorter inner class

    /** Sort the Solution elements of this list into datetime, id index order. */
    public void sortByTime() {
        this.sort(new SolutionTimeSorter());
    }

    /**
     * Returns a new list instance of same type as this list that contains
     * the same Solution elements as this list sorted by time.
     * The input is either GenericSolutionList.ASCENDING
     * or GenericSolutionList.DESCENDING where ASCENDING
     * is in chronological order and DESCENDING is reverse.
    */
    public GenericSolutionList getTimeSortedList(int order) {
        GenericSolutionList sl = (GenericSolutionList) newInstance();
        if (sl == null) return null;
        int size = size();
        if (size < 1) return sl; // empty no sort needed
        if (size == 1) {
          sl.add(get(0));
          return sl;
        }
        sl.ensureCapacity(size); // puff it up, if necessary

        // otherwise make the time array that is needed by IndexSort
        double tArray[] = new double[size];
        for ( int i = 0; i < size; i++) {
          tArray[i] = ((Solution) get(i)).getTime();
        }
        IndexSort indexSort = new IndexSort();
        int idx[] = indexSort.getSortedIndexes(tArray);
        // add elements in time sorted order
        if (order == ASCENDING) {
          for (int i = 0; i < size; i++) {
            sl.add( get(idx[i]) );
          }
        } else {
          for (int i = size-1; i >=0; i--) {
            sl.add( get(idx[i]) );
          }
        }
        return sl;
    }

    /*
    public Solution getClosest(double lat, double lon) {

        int cnt = size():
        if (cnt == 0) return null;

        Solution[] sols = getArray();
        double minDist = 999999.
        double km = 0;
        Solution minSol = null;
        LatLonZ llz = null;

        for ( int idx=0 ; idx < cnt; idx++ ) {  

           llz = sol[idx].getLatLonZ();
           if (llz.isNull()) continue;

           km = GeoidalConvert.horizontalDistanceKmBetween(lat, lon, llz.getLat(), llz.getLon());

           if ( km < minDist ) {
               minDist = km;
               minSol = sol[idx];
           }
        }

        return minSol;
    }
    */

    /**
    * Populate this list with Solutions created from the data source
    * that meet the EventSelectionProperties.
    */
    public void fetchByProperties(EventSelectionProperties props) {
        Collection aList = Solution.create().getByProperties(props);
        if (aList != null) addAll(aList);
        else System.out.println("SolutionList fetchByProperties returned null list.");
    }

    public void fetchByProperties(Connection conn, EventSelectionProperties props) {
        Collection aList = Solution.create().getByProperties(conn, props);
        if (aList != null) addAll(aList);
        else System.out.println("SolutionList fetchByProperties returned null list.");
    }

    /**
     * Populate this list with Solutions created from the data source
     * that meet the last set EventSelectionProperties.
     * Uses set connection, if any, else defaults to static DataSource connection.
     * Does nothing if properties have not been set.
     * @see #setProperties(EventSelectionProperties)
    */
    public void fetchByProperties() {
        if (props != null) {
            if (conn == null) fetchByProperties(props);
            else fetchByProperties(conn, props);
        }
    }

    /**
    * Add to list the Solution created from the data source
    * whose identifier matches the input evid.
    */
    public void fetchById(long evid) {
        if (conn == null) {
            this.add( Solution.create().getById(evid) );
        } else {
            this.add( Solution.create().getById(conn, evid) );
        }
    }

   /**
   * Add to the list the "valid" Solutions created from the data source
   * that occurred within the input time window.<p>
   * Valid Solutions are not "dummy" or "bogus" and are from the primary
   * data source.
   * Inclusive or exclusive?
   */
   public void fetchValidByTime(double start, double stop) {
        Collection solList = null;
        if (conn == null) {
             solList = Solution.create().getValidByTime(start, stop);
        } else {
             solList = Solution.create().getValidByTime(conn, start, stop);
        }
        addAll(solList);
    }

   /**
   * Add to the list the all Solutions created from the data source
   * that occurred within the input time window.<p>
   * Inclusive or exclusive?
    */
   public void fetchByTime(double start, double stop) {
        if (conn == null) {
          addAll(Solution.create().getByTime(start, stop));
        } else {
          addAll(Solution.create().getByTime(conn, start, stop));
        }
    }

    /** Adds the input array of Solution objects to this list.*/
    public void addSolutions(Solution [] sol) {
        if (sol == null) {
          //System.out.println("No data; input solution array null");
          return;
        }
        this.ensureCapacity(size()+sol.length);
        for (int i=0; i<sol.length;i++) {
          this.add(sol[i]); // appends new solution to end of list
        }
    }

    /**
     * Adds the input Solution to the list, maintain sorting by time.
     * List must already be in sorted order to acheive desired
     * results.
     */
    public void addByTime(Solution newSol) {
     //Could implement here a forced time sort of entire list after
     //simple add of new data to guarantee success. - aww
        int size = size();
        double newDateTime = newSol.getTime();
        for (int i = 0; i < size; i++) {
          if ( ((Solution) get(i)).getTime() > newDateTime) {
            this.add(i, newSol);
            return;
          }
        }
        //System.out.println ("Inserting at end "+sol.length);
        add(newSol); // add to end
    }
    public TimeSpan getTimeSpan() {
        int size = size();
        if (size == 0) return new TimeSpan();

        double minDateTime = Double.MAX_VALUE;
        double maxDateTime = -Double.MAX_VALUE;
        double datetime = Double.NaN;

        for (int i = 0; i < size; i++) {
            datetime = ((Solution) get(i)).getTime();
            if (! Double.isNaN(datetime)) {
                if (datetime > maxDateTime) maxDateTime = datetime;
                else if (datetime < minDateTime) minDateTime = datetime;
            }
        }
        return new TimeSpan(minDateTime, maxDateTime);
    }

    /**
     * Wrapper around toArray() returns list as a Solution[]
     */
    public Solution [] getArray() {
        return (Solution []) this.toArray(new Solution[0]);
    }

    /**
     * Returns true if this list contains a Solution with
     * an identifier equal to the input value.
     */
    public boolean contains(long id) {
        return containsIdentifier(Long.valueOf(id));
    }

    /** Returns Solution in list whose identifier value equals
     * the input.
     *  Returns null if none is found.
     */
    public Solution getById(long id) {
        return (Solution) getByIdentifier(Long.valueOf(id));
    }

    /**
     * Returns the index of the Solution in list whose identifier
     * equals the input id (evid).
     * Returns -1 if none is found.
     */
    public int getIndex(long id) {
        return indexOfIdentifier(Long.valueOf(id));
    }

    /**
     * Returns the index in the list of the Solution whose
     * identifier (evid) equals that of the input.
     * Returns -1 if none is found.
     */
    public int getIndex(JasiCommitableIF sol) {
        return indexOfIdentifier(sol.getIdentifier());
    }

    /**
     * Returns the first Solution found in this list whose
     * preferred origin identifier (id) equals the
     * input value. <p>
     * Returns null if no matching Solution is found.
     */
    public Solution getByOriginId(Object id) {
        if (id == null) return null; // assume bogus request
        int size = size();
        if (size == 0) return null;
        DataLong prefor = null;
        for (int i = 0; i < size; i++) {
            Solution sol = (Solution) get(i);
            prefor = (DataLong) sol.getPreferredOriginId();
            if (prefor.equalsValue(id)) return sol;
        }
        return null;
    }

    /** Convenience wrapper iterates over list, clearing the
     * loaded data lists of a Solution.
     * This method should only be invoked when the list data is
     * no longer used and should be garbage collected.
     * If verbose = true, warning message output when solution needs commit.
     * */
    public void clearDataLists(boolean verbose) {
        int size = size();
        for (int i = 0; i < size; i++)  {
          ((Solution) get(i)).clearDataLists(verbose);
        }
    }

    /** Virtually deletes, flags, all Solution elements in this list whose
     * residuals are greater than or equal to that of the input value.
     */
    public int stripByResidual(double val) {
        int knt = 0;
        int size = size();
        for (int i = 0; i < size; i++)  {
          Solution sol = (Solution) get(i);
          if (Math.abs(sol.getResidual()) >= val) {
            setElementDeleteState(i, true);
            knt++;
          }
        }
        return knt;
    }

    /** Returns header describing Solution data returned by dumpToString(). */
    public String getNeatHeader() {
        return Solution.getNeatStringHeader();
    }

   /**
    * Dumps the list contents in Solution.toSummaryString() format
    * with each Solution String element delineated by a linefeed.
    */
    public String dumpToString() {
        int size = size();
        StringBuffer sb = new StringBuffer(size*256);
        for (int i = 0; i < size; i++) {
          sb.append( ((Solution)get(i)).toSummaryString()).append("\n");
        }
        return sb.toString();
    }

} // end of GenericSolutionList class
