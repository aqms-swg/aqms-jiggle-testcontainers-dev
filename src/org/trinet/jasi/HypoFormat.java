package org.trinet.jasi;

import java.io.*;
import java.util.*;
//import java.text.*;
import java.text.SimpleDateFormat;
import java.lang.*;

import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;

/**
 * Create and parse Hypoinverse formatted lines
 */

public class HypoFormat {

    public static final int ARC_STRING_SIZE = 113; // used to be 110, expanded for location code -aww
    public static final int ARCSUM_STRING_SIZE = 179; 
    public static boolean locationCodeSpace2Dash = false;
    public static boolean debug = false;

//165 2 A2 Code for domain (ie. NC, CI) in which the location was made ORIGIN.CMODELID  
//167 2 A2 Code for model id (ie 01, 02) of set of location files.  ORIGIN.VMODELID?
//
//169 1 A1 Depth type (M model depth, G geoid depth) set with GEO command.
//170 1 A1 Dominant crust model type: T=CRT, H=CRH, E=CRE, V=CRV, L=CRL
//171 4 I4 Earthquake depth datum in m above the geoid (CRT model surface).
//175 5 F5.2 Geoid depth in km below the geoid or sea level.
//179 is the last filled column.
//

/**
 * Make a string in .arc format (hypo2000) from the contents of Phase.
 */

/*
      1         2         3         4         5         6         7         8         9        10        11
012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
STATNnt cCMP pr  yearmodyhrmnseconPresPwtS-secSr sSrespek2pekmmSwtPdlySdlyDistAngADPerRCodaAzmDurMagPimpSimpSFX
MCV  NC VVHZ  PU0199806262007 4310  -9110    0   0   0      0 0  0   0   0  1616400  0  159297404  0 502   0WD
HMT  CI VVHZ     199811102059    0   0  0 5632IS 0 -27    151 0  0   0   0 708 9000  0    0228  0  0   0
RDM  CI HHHZ     199902101612             1584iS 0
HMT  CI VVLN     200004071235        0  0 1285 S0 TPO   G  P V EP+2199811120254 3783  90  0    0   0   0      0 0  0   0   01334 7000  0    0154  0  0   0   0
QAL   G  P V    0199811120254    0   0  0 5709ES 2 301      0 0  0   0   01359 7000  0    0174  0  0   0   0
*/

  public static String toArchiveString(Phase ph) {

    StringBuffer sb = new StringBuffer(ARC_STRING_SIZE);   // make a space filled string buffer
    for (int i = 0; i < ARC_STRING_SIZE; i++) sb.append(' ');

    Format fmt1 = new Format("%1s");        // for efficiency

    Channel chan = ph.getChannelObj();

    sb = overlay(sb, 0, new Format("%-5s").form(chan.getSta()) );
    sb = overlay(sb, 5, new Format("%-2s ").form(chan.getNet()) );
    String comp = chan.getChannel();
    // get last character of component (changed from first 12/06/2005 -aww)
    int len = comp.length();
    if (len > 0) comp = comp.substring(len-1,len);
    sb = overlay(sb, 8, fmt1.form(comp));
//    sb = overlay(sb, 9, new Format("%3s ").form(chan.getChannel()));
 // changed to SEED component names
    sb = overlay(sb, 9, new Format("%3s ").form(chan.getSeedchan()));
/*
         1         2         3         4         5         6         7         8         9        10        11
012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
STATNnt cCMP pr  yearmodyhrmnseconPresPwtS-secSr sSrespek2pekmmSwtPdlySdlyDistAngADPerRCodaAzmDurMagPimpSimpSFX
MCV  NC VVHZ  PU0199806262007 4310  -9110    0   0   0      0 0  0   0   0  1616400  0  159297404  0 502   0WD
SIO  CI HHLE  P0 200001130934 2647 704 75
DTP  CI  EHZ    0200209122043    0   0  0 4582 S 3  -2      0 0 54   0   0 504 9000  0    0148  0  0   0 141
CLC  CI  HHZ  PU1200209122043 3985   2163    0   0   0      0 0  0   0   0 515 9000  0    0 69  0  0 362   0
WWP  CI VEHZ  PD2200209122043 3308  -1  0                                  106
WWP  CI VEHZ     200209122043             3458 S 1  -1           0         106
WSH  CI  EHZ  P 2199207010000 4351  95  0    0   0   0      0 0  0   0   01623 5500  0    0340  0  0   0   0
WRC  CI  EHZ  P 2199207010000 4989 197  0    0   0   0      0 0  0   0   02036 4400  0    0340  0  0   0   0
PEC  CI VEHZ    4199207010000             3049 S 2                        999999
PLM  CI VEHZ  PD0199207010000 3265                                        999999
*/
// <P> wave Phase description, example: "IPU1"
    if (ph.description.iphase.equals("P")) {

      sb = overlay(sb, 13, ph.description.toHypoinverseString(ph.isReject()));

    // phase time
      sb = overlay(sb, 17,  arcTimeString(ph.datetime.doubleValue()) );       // "YEARMODYHRMNSS.SS"

      // only exists if there was a previous location run
      if (!ph.residual.isNull())
       sb = overlay(sb, 34, new Format("%4d").form((int) (Math.round(ph.residual.doubleValue()*100.))));

      // only exists if there was a previous location run
      if (!ph.weightOut.isNull())
       //sb = overlay(sb, 38, new Format("%3d").form((int) (scaleWeightOutgoing(ph.weightOut.doubleValue())*100.) ));
       sb = overlay(sb, 38, new Format("%3d").form((int) (Math.round(ph.weightOut.doubleValue()*100.))));

      if (!ph.delay.isNull())
       sb = overlay(sb, 66, new Format("%4d").form((int) (Math.round(ph.delay.doubleValue()*100.))));

      if (!ph.importance.isNull()) // added importance 2008/07/14
       sb = overlay(sb, 100, new Format("%4d").form((int) (Math.round(ph.importance.doubleValue()*1000.))));
    }

/*
      1         2         3         4         5         6         7         8         9        10        11
012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
STATNnt cCMP pr  yearmodyhrmnseconPresPwtS-secSr sSrespek2pekmmSwtPdlySdlyDistAngADPerRCodaAzmDurMagPimpSimpSFX
QAL   G  P V    0199811120254    0   0  0 5709ES 2 301      0 0  0   0   01359 7000  0    0174  0  0   0   0
Note: a station that is not "setup" will return a line as follows:
LVA2 CI HHHN     199909072229        0  0        1018iSc1                    0
*/
// <S> wave
    else if (ph.description.iphase.equals("S")) {

      // Must set P-Weight to bogus = 4
      sb = overlay(sb, 16, Integer.toString(4) );

    // phase time, S arrival time description is split in the format
      sb = overlay(sb, 17, arcYMDHMString(ph.datetime.doubleValue()) );       // "YEARMODYHRMN"
    // field actually starts at 41, but we are doing 4 digits max so use 42.
      sb = overlay(sb, 42, arcSecString(ph.datetime.doubleValue()) );     // "SSss"= SS.ss*100.

      //      sb = overlay(sb, 46,  ph.description.toShortString());
      sb = overlay(sb, 46,  ph.description.toHypoinverseString(ph.isReject()));

      if (!ph.residual.isNull())
       sb = overlay(sb, 50, new Format("%4d").form((int) (Math.round(ph.residual.doubleValue()*100.))));

      if (!ph.weightOut.isNull())
       sb = overlay(sb, 63, new Format("%3d").form((int) (Math.round(ph.weightOut.doubleValue()*100.))));

      if (!ph.delay.isNull()) // aww added
       sb = overlay(sb, 70, new Format("%4d").form((int) (Math.round(ph.delay.doubleValue()*100.))));

      if (!ph.importance.isNull()) // added importance 2008/07/14
       sb = overlay(sb, 104, new Format("%4d").form((int) (Math.round(ph.importance.doubleValue()*1000.))));

    } else {

    System.out.println ("* Phase type that Hypoinverse can't handle: " +
                sb +" "+ ph.description.iphase);
    return (sb.toString());
    }

    // AssocArO parts
    // Analyst assigned weight = "quality"
//    if (!ph.weightIn.isNull())
//      sb = overlay(sb, 38, new Format("%3d").form((int) (Math.round(ph.weightIn.doubleValue()*100.))));

    // HYP2000 use station Epicentral not Slant distance in Archive output -aww
    if (! (ph.getHorizontalDistance() == Channel.NULL_DIST || Double.isNaN(ph.getHorizontalDistance()) ) ) {
        sb = overlay(sb, 74, new Format("%4d").form((int) Math.round(ph.getHorizontalDistance()*10.) ) ) ;

        // Emergence angle from source is relative to nadir (down is 0 degrees)
        if (! (ph.emergenceAngle.isNull() ||  Double.isNaN(ph.emergenceAngle.doubleValue())) )
        sb = overlay(sb, 78, new Format("%3d").form((int) ph.emergenceAngle.doubleValue()) ) ;

        if (! Double.isNaN(ph.getAzimuth()) )
        sb = overlay(sb, 91, new Format("%3d").form((int) ph.getAzimuth()) ) ;


/* DK 08/24/02
    *   if(ph.coda != null)
    *   {
 *  sb = overlay(sb, 87, new Format("%4d").form( ph.coda.tau.intValue()) ) ;
 *  sb = overlay(sb, 82, new Format("%1d").form( Coda.Weight_Jiggle2HI(ph.coda.weightIn.doubleValue())) ) ;
 *   }
 */
    }
    // add ChannelName location code 04/15/2005 -aww
    String str = (locationCodeSpace2Dash) ? chan.getChannelName().getLocationString() : chan.getLocation(); // -aww 2011/02/24 alternative

    //Kludge guess of code if unknown (database tables missing code) -aww
    if (str == null || str == "" || str.equalsIgnoreCase("NULL")) {
        //str = EnvironmentInfo.getLocationCode(chan.getNet());
        //str = EnvironmentInfo.getLocationCode(EnvironmentInfo.getNetworkCode()); // remove 2008/01/10 - aww
        str = "??";  // added 2008/01/10 -aww
    }
    if (str.length() > 2) str = str.substring(0,2);
    sb = overlay(sb, 111, new Format("%-2s").form(str));
    //
    return sb.toString();
    //    return (sb.toString().trim());
  }

/**
 * The StringBuffer doesn't really have a simple overlay method. Its insert()
 * method shoves bytes to the right as it inserts.
 */
  private static StringBuffer overlay(StringBuffer sb, int start, String str) {
    int end = start + str.length();
    return sb.replace(start, end, str);
  }

/*
      1         2         3         4         5         6         7         8         9        10        11
012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
STATNnt cCMP pr  yearmodyhrmnseconPresPwtS-secSr sSrespek2pekmmSwtPdlySdlyDistAngADPerRCodaAzmDurMagPimpSimpSFX
MCV  NC VVHZ  PU0199806262007 4310  -9110    0   0   0      0 0  0   0   0  1616400  0  159297404  0 502   0WD
HMT  CI VVHZ     199811102059    0   0  0 5632IS 0 -27    151 0  0   0   0 708 9000  0    0228  0  0   0
WOF   G  P V IPD0199811120254 2436   7172    0   0   0      0 0  0   0   0 49610100  0    0165  0  0 174   0

GTM  CI  EHZ  P00199912080004 4174  -9155    0   0   0      0 0  0   0   0 264 9000  0    0201  0  0 741   0

Note: a station that is not "setup" will return a line as follows:
LVA2 CI HHHN     199909072229        0  0        1018iSc1                    0
HDB  CI EEHZ  P3 200001130933 4050 312 25
*/

// TODO: Need to handle P and S on a single card

/**
 * Parse .arc format station line into new Phase object.
 * <tt>
          1         2         3         4         5         6         7         8         9         0         1
012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
DTP  CI  EHZ    0200209122043    0   0  0 4582 S 3  -2      0 0 54   0   0 504 9000  0    0148  0  0   0 141
CLC  CI  HHZ  PU1200209122043 3985   2163    0   0   0      0 0  0   0   0 515 9000  0    0 69  0  0 362   0
WWP  CI VEHZ  PD2200209122043 3308  -1  0                                  106
WWP  CI VEHZ     200209122043             3458 S 1  -1           0         106
WOR  CI VEHZ  PU1200209122043 3330  -1  0                                  118
 * </tt>
 */
  public static Phase parseArcToPhase(String str) {

    if (str.startsWith("    ")) return null;    // not an arrival line

    Phase ph = Phase.create();

//
// HEY BREAK BELOW CODE BELOW INTO PRIVATE METHODS parseChannel, parseP, parseS parseCoda etc TO CLEAN UP - AWW
//
   try {
    // Channel name incomplete. Note .trim() to remove leading & trailing blanks.
    // ChannelName uses String intern() to keep char array from propagating when input arg is large string.
    String sta  = str.substring(0,5).trim();
    String net  = str.substring(5,7).trim();
    String comp = str.substring(9,12).trim();

    // note putting channel in both 'channel' & 'seedchan'
    ph.setChannelObj(Channel.create().setChannelName(net, sta, comp, "", "","",comp, ""));

    // Phase description, example: "IPU1"

/*
 * WARNING: it is possible for Hypoinverse to combine p and S readings on a
 * single card.  If so, this method will miss the S on a P card.
 * Also, using str.substring(46,47).equals("S") seemed to return 'false' even
 * when it wasn't but str.substring(46,47).equalsIgnoreCase("S") seems to work.
*/

    // Read P info (parseP)
    if (str.substring(14,15).equalsIgnoreCase("P"))     // its a P-wave
    {
      // String intern() keeps char array ref from propagating when input arg is large string.
      String desc = str.substring(13, 17).intern();
      //ph.description.parseHypoinverseString(desc); // set below
      ph.setReject(ph.description.parseHypoinverseString(desc)); // 06/22/2006 - aww
      //ph.weightIn.setValue(ph.description.getQuality()); // operator assigned quality - aww
      ph.weightIn.setValue((ph.isReject() ? 0 : 1)); // 0 for +5wts and 1 for std quality - aww 11/09/2006
      ph.residual.setValue(parseDouble(str, 34, 38) / 100.) ;   // residual
      // Hypoinverse Weights (from the program) seem to be in the range 0.0 -> 2.0
      // To fit into dbase constraint of 0.0 -> 1.0 we just /100.
      //ph.weightOut.setValue(scaleWeightIncomming(parseDouble(str, 38, 41)));
      ph.weightOut.setValue(parseDouble(str, 38, 41) / 100.);
      ph.datetime.setValue(parseArcPTime(str));
      if (str.length() > 66) ph.delay.setValue(parseDouble(str,66, 70) / 100.); // added p phase delay -aww
    }

    // check for S data (parseS)
//    if (str.length() > 41)
    if (str.length() >= 74)
    {
      if (str.substring(47,48).equalsIgnoreCase("S"))       // its a S-wave
      {
        // String intern() keeps char array ref from propagating when input arg is large string.
        String desc = str.substring(46,50).intern();

        //ph.description.parseHypoinverseString(desc); // set below
        ph.setReject(ph.description.parseHypoinverseString(desc)); // 06/22/2006 - aww
        //ph.weightIn.setValue(ph.description.getQuality()); // operator assigned quality -aww
        ph.weightIn.setValue((ph.isReject() ? 0 : 1)); // 0 for +5wts and 1 for std quality - aww 11/09/2006

        ph.residual.setValue(parseDouble(str, 50, 54)/100.);

        // Hypoinverse Weights (from the program) seem to be in the range 0.0 -> 2.0
        // To fit into dbase constraint of 0.0 -> 1.0 I just /100.
        //ph.weightOut.setValue(scaleWeightIncomming(parseDouble(str, 63, 66)));
        ph.weightOut.setValue(parseDouble(str, 63, 66) / 100.);
        ph.delay.setValue(parseDouble(str,70, 74) / 100.); // added s phase delay -aww
        ph.datetime.setValue(parseArcSTime(str));
      }

// HYPOINVERSE indicates a phase that was not used because its not in its station list
// by putting "999999" in positions 74-79. :. dist = 999.9 and ema = 99. Weird!
// It also appears in the terminator line!
// *** SKIP PHASE CARD WITH UNKNOWN STATION:
/*
          1         2         3         4         5         6         7         8         9        10        11
01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012
STATNnt cCMP pr  yearmodyhrmnseconPresPwtS-secSr sSrespek2pekmmSwtPdlySdlyDistAngADPerRCodaAzmDurMagPimpSimpSFXlc
PEC  CI VEHZ    4199207010000             3049 S 2                        999999
PLM  CI VEHZ  PD0199207010000 3265                                        999999
      00000000                                                9516694     999999
SDD  CI HHLE wPU2200506130621 4388 -23 50                            0    1027 70          272                 01
SDD  CI HHLE    4200506130621          25 5593wS 3 -28           7       01027 70          272                 01

SDD  CI HHLE wPU2200506130621 4388 -23 50                            0    1027 70          272                 01
SDD  CI  HLE wPU2200506130621 4388 -23 30    0   0   0      0 0  0   0   01030 7000  0    0273  0  0   3   0      0

SDD  CI HHLE    4200506130621          25 5593wS 3 -28           7       01027 70          272                 01
SDD  CI  HLE    4200506130621    0   0  0 5593wS 3 -28      0 0 15   0   01030 7000  0    0273  0  0   0   1      0
FRD  AZ  HHZ wP 0200506130621 3025   8158    0   0   0      0 0  0   0   0  5215900  0    0241  0  0 145   0      0
*/
      if (str.length() >= 81) // data for (parseS)
      {
      // ! BEWARE !
      // HYP2000 phase distance is 2-D horizontal epicentral distance.
      // The "slant" distance, can be set only if solution depth is known,
      // this depth would need to be internal static data for access here.
        Number num = parseNumber(str, 74, 78);
        // Don't set values if parsed fields are blank !
        if (num != null) ph.setHorizontalDistance(num.doubleValue()/10.); // before /10

// special processing of "skipped" card
        if (ph.getHorizontalDistance() >= 999.) { // see SKIP comment above
          ph.weightOut.setValue(0.0);
        }
        else { // not skipped then ema and azimuth should be ok -aww
          num = parseNumber(str, 78, 81)  ; // emergence angle
          if (num != null) ph.emergenceAngle.setValue(num.doubleValue());

          if (str.length() >= 94) {
              num = parseNumber(str, 91, 94); // azimuth as epi-to-station, ESaz
              if (num != null) {
                  double az = num.doubleValue();
                  // Note to Convert hyp2000 ESaz to SEaz use below logic: - aww
                  //if (az < 180.) az += 180.;
                  //else az -= 180.;
                  ph.setAzimuth(az);
              }
          }

          if (str.length() > 100) { // parsing of importance added -aww 2008/07/14
            if (! str.substring(47,48).equalsIgnoreCase("S") ) { // its a P-wave
              num = parseNumber(str, 100, 104); // importance P
              if (num != null) ph.importance.setValue(num.doubleValue()/1000.); // has format 4.3
            }
            else { // it's an S-wave
              num = parseNumber(str, 104, 108) ; // importance S
              if (num != null) ph.importance.setValue(num.doubleValue()/1000.); // has format 4.3
            }
          }

        }
      }

//      if (str.length() >= 91) // data for (parseCoda)
      if (str.length() >= 113) // data for (parseCoda)
      {
        // get ChannelName location extension
        // location can have blanks, removed substring trim() -aww 07/11/05
        ph.getChannelObj().setLocation( str.substring(111,113) ); // added - aww 04/15/2005

        // 82,1   Dur Mag Weight Code 87,4   Coda Dur (seconds) 94,3,2 Dur Mag value
        Number num = parseNumber(str, 87, 91) ; // coda duration
        int itau = (num == null) ? 0 : num.intValue();
        if (itau > 0) // duaration valid create coda
        {
          Coda newCoda = Coda.create();
          newCoda.setChannelObj(ph.getChannelObj());
          newCoda.tau.setValue(itau);
          newCoda.tCodaTerm.setValue((double)itau + ph.getTime());

          num = parseNumber(str, 82, 83); // coda weight
          if (num != null) {
            newCoda.quality.setValue(Coda.toQuality( num.intValue() ));
          }

          num = parseNumber(str, 94, 97); // coda channel mag
          if (num != null) // create channelmag
          {
            ChannelMag newChanMag = new ChannelMag();
            newChanMag.setMagValue(num.doubleValue()/100.);
            newChanMag.weight.setValue(newCoda.quality);
            newCoda.setChannelMag(newChanMag);
          }
          // ph.coda = newCoda;  // this line requires Md mod to coda

        } // coda TAU > 0 test

      } // 91 char len CODA test
    } // 41 char len SWAVE test
   }
   catch (StringIndexOutOfBoundsException ex) {
      System.err.println(ex.getMessage());
   }
   catch (Exception ex) {
      System.err.println(ex.getMessage());
   }
   return (ph);
  }

/*
// Hypoinverse weights (from the program) seem to be in the range 0.0 -> 2.0
// So scale them to fit db constraint of 0.0 -> 1.0 by dividing by 10.0. 
  public static double scaleWeightIncomming(double val) {
    double newVal = val / 10.0;
    if (newVal > 1.0) newVal = 1.0;    // insure no constrain violations
    if (newVal < 0.0) newVal = 0.0;
    return newVal;
  }
  // Convert db weights to Hypoinverse weights (in the range 0.0 -> 2.0)
  // Does inverse of scaleWeightIncomming and multiples input value by 10.0. 
  public static double scaleWeightOutgoing(double val) {
      return val * 10.0;
  }
*/

/**
 * Return a time string of the form: "YEARMODYHRMN SSSS". Seconds are to 2 decimals with no "."
 */
  public static String arcTimeString(double dt) {
    StringBuffer sb = new StringBuffer(24);
    return sb.append(arcYMDHMString(dt)).append(" ").append(arcSecString(dt)).toString();
  }

/**
 * Return a time string of the form: "YEARMODYHRMN"
 */
   public static String arcYMDHMString(double dt)  {
     return LeapSeconds.trueToString(dt, "yyyyMMddHHmm"); // for leap secs - aww 2008/02/04
   }


/** Return a string for the epoch time with only hour/minute/second as required
 *  in the Hypoinverse terminator format. Ex: "HHmmssss". */
  public static String arcTerminatorTime(double dt) {
    StringBuffer sb = new StringBuffer(24);
    sb.append(arcYMDHMString(dt)).append(arcSecString(dt));  // "yyyyMMddHHmmssss"
    return sb.substring(8,sb.length());
  }


/**
 * Return a time string of the form: "SSSS" with no decimal point.
 */
  public static String arcSecString(double dt) {
    return to42String((new DateTime(dt, true)).getDoubleSecond()); // for true UTC input seconds - aww 2008/02/04
  }


/**
 * Make a Hypoinverse archive terminator line from a Solution object.
 * Note that the format uses Fortran style floating point format in some fields.
 * For example an "f4.2" field will appear like "3247" and be interpreted as
 * "32.47". <p>

 <tt>
     1         2         3         4         5         6         7
1234567890123456789012345678901234567890123456789012345678901234567890
      i2i2f4.2i2 f4.2i3  f4.2 f5.2                            i10
      hrmnsecsLT minsLON minszzzzz                            IDnumberxx
      0218152533 3050117 4512  600                            9876543
 </tt>
 */
  static public String toTerminatorString(Solution sol) {
    return toTerminatorString(sol, false, false, false, false, false);
  }

/**
 * Make a Hypoinverse archive terminator line from a Solution object.
 * If 'fixZ' is true the depth is fixed and will not be changed by the location process.
 * If 'fixLoc' is true both the location and depth are fixed but the origin time is
 * solved for.
 * If both are false, all parameters are solved for.
 * If 'trialLoc' is true the Solution (lat, lon, z and origin time) is used as
 * a trial (beginning) location by the location process. <p>
<tt>
         1         2         3         4         5         6         7
1234567890123456789012345678901234567890123456789012345678901234567890
    xxi2i2f4.2i2 f4.2i3  f4.2 f5.2                            i10
^^^^xxhrmnsecsLT minsLON minszzzzz                            IDnumberxx
      0218152533 3050117 4512  600                            9876543
     12102210036 0108117 4638 1.36X                           9082441
Examples:
fixLoc = false, fixZ = false
                                                              9223372
fixLoc = true, fixZ = true
              33 4015116 5965-0.03X                           9223372
fixLoc = true, fixZ = false
              33 4015116 5965 0.03X                           9223372
fixLoc = false, fixZ = true
                             -0.03                            9223372
</tt>
 */
  static public String toTerminatorString(Solution sol,
                                          boolean fixLoc,
                                          boolean fixZ,
                                          boolean fixT,
                                          boolean trialLoc,
                                          boolean trialTime) {

    StringBuffer sb = new StringBuffer(73);

    sb = sb.append("      "); // 6 blanks at start

    if (sol == null) return sb.toString();  // bail

    Format d2  = new Format("%2d");
    Format d3  = new Format("%3d");
    Format f5d = new Format("%5d");

    // trial origin time goes here (8 chars, buffer offset 6-13 inclusive)
    // use origin time if trialLoc only - 10/11/2005 aww 
    // use origin time if trialTime only - 10/18/2006 aww 
    sb.append( ((trialTime) ? arcTerminatorTime(sol.getTime()) : "        ") );

    LatLonZ latlonz = sol.getLatLonZ();

    // Test flags (but shouldn't it always "trial" a valid solution llz? -aww)
    if ( !fixZ  && !trialLoc)  {
      // use HYPOINV program's current settings
      sb.append("                     "); // 21 blank
    }
    else { // fixZ or trialLoc
      // first look at lat,lon for a non-null trialLoc
      boolean haveTrialLoc = false;
      // for robustness could make latlonz members be DataDoubles then test
      // for isNull() LatLonZ.NullValue would be that of DataDouble null.
      // for now just test for LatLonZ.NullValue (= 0.0 ?) here:
      if (!trialLoc || latlonz.getLat() == LatLonZ.NullValue || latlonz.getLon() == LatLonZ.NullValue) {
        sb.append("               "); // no trialLoc, 15 blank
      }
      else { // have trialLoc map it to buffer
        sb.append(d2.form(Math.abs(latlonz.getLatDeg())));
        sb.append((latlonz.getLatDeg() > 0.0) ? "N" : "S");
        sb.append(to42String(latlonz.getLatMin()));
        sb.append(d3.form(Math.abs(latlonz.getLonDeg())));
        sb.append((latlonz.getLonDeg() > 0.0) ? "E" : "W");
        sb.append(to42String(latlonz.getLonMin()));
        haveTrialLoc = true;
      }

      // now check the trial depth
      //double depth = Math.abs(latlonz.getZ()); // force always below datum - aww removed 2011/07/27
      //double depth = latlonz.getZ(); // for hyp2000 v1.3 depths can be above datum - aww  2011/07/27
      //double depth = ( sol.depthCrustType.equalsValue("M") && (sol.crustModelType.equalsValue("H") || sol.crustModelType.equalsValue("T")) ) ?  sol.getModelDepth() : sol.getDepth();
      // Fred Klein says in hyp1.41 always use model depth for trial depth - aww 2015/06/10
      double depth = sol.getModelDepth();

      // Negative depth fixing doesn't work as expected with hyp2000 v1.3 gradient models removed 2011/07/27 -aww
      if (fixZ && depth == 0.) { // we don't make depths negative here unless you have large mts/cone above, since puts event into air!
        // "-" char in col 35 indicates fixed so:
        depth = 0.02; // ? a 0.01 value may cause issues with convergence hyp1.40 ?
      }
      if (debug)
          System.out.println("DEBUG HypoFormat terminator " + sol.getId().longValue() + " depth value: " + depth);

      // Assuming the NullValue primitive 0.0 default depth
      // could also be set if the trialLoc is also set
      //if (depth != 0. || haveTrialLoc)
      if (fixZ || haveTrialLoc)
        sb.append(f5d.form(Math.round(depth*100.)));
      else {
        sb.append("     "); // 5 blank
      }

      // set fix location char at buffer offset 34 only if a valid trialLoc
      //if ((fixLoc || trialLoc) && haveTrialLoc) { // ? bug fix aww 02/25/2005 removed trialLoc condition
      if (haveTrialLoc) { // non-blank char 35 
        if (fixT) sb.append ("O");
        else if (fixLoc) sb.append ("X");
        else if (fixZ) sb.append("-"); // added 2011/07/27 for hyp2000 v1.3 -aww
        else sb.append (" "); // 1 blank
      }
      else if (fixZ) sb.append("-"); // added 2011/07/27 for hyp2000 v1.3 -aww
      else sb.append (" "); // 1 blank
    } // end of fixing, trialLocation block 14+21= 35 chars

    if (!sol.id.isNull()) {
      // id starts at buffer offset 62, so add 27 blanks
      sb.append("                           "); // 27 blank
      // BEWARE of format-width limit (was 10 char in H2000 doc) !
      sb.append(sol.id.toString()); // start at buffer offset 62
    }
    return sb.toString();
  }

 /** Format seconds in Hypoinverse F4.2 style with no decimal point.
  *  For example: "12.8364" = "1284"*/
  protected static String to42String(double secs) {
    Format d2  = new Format("%02d");
    long   whole = (long) secs;  // casting truncates to integral secs
    double frac = (secs - whole) * 100.; // fractional secs
    long longFrac = Math.round(frac); // 0 to 100 hundreths secs
    if (longFrac == 100l) { // bump integer secs by 1, zero fraction secs
      whole += 1l;
      longFrac = 0l;
    }
    return d2.form(whole) + d2.form(longFrac);
  }
/**
 * Make a Hypoinverse2000 "summary header" line from a Solution object <p>
 <tt>
      1         2         3         4         5         6         7         8         9        10        11        12        13
01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
199903080408538932 4205120  700    3  0 54329197 309223879900 43 29900  0   2659 -  098939893 71   0   0  0  0HAD      71    0  0   0  0         0   0   0   0   0
1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
yearmodyhrmnseccLT minsLON minszzzzzMagNphGapDmnRmssAziDpSize

199902101612100333 4015116 5965    3  0 31155  3  2316273 10134516  58  0     28 #  2  56  97 37   0   0  0  0HAD      39    0  0   0  0  13761428   0   0   0   0
</tt>
 */
  static public String toSummaryString(Solution sol) {

    StringBuffer sb = new StringBuffer(ARCSUM_STRING_SIZE);   // make a space filled string buffer
    for (int ii = 0; ii < ARCSUM_STRING_SIZE; ii++) sb.append(' ');

    Format f2d0 = new Format("%02d");
    Format f2d = new Format("%2d");
    Format f3d = new Format("%3d");
    Format f4d = new Format("%4d");
    Format f5d = new Format("%5d");
    Format f10d = new Format("%10d");

    //1 4 I4 Year. *
    //5 8 4I2 Month, day, hour and minute.
    //13 4 F4.2 Origin time seconds.
    double otime = sol.getTime();
    String str =
        LeapSeconds.trueToString(otime,"yyyyMMddHHmmss") + // -aww 2008/02/10
        f2d0.form( Math.round( ((otime - Math.floor(otime))*100.) ) );
    sb = overlay(sb, 0, str);

    double deg = sol.lat.doubleValue(); 
    str = (deg < 0.) ? "S" : " ";
    //17 2 F2.0 Latitude (deg). First character must not be blank.
    sb = overlay(sb, 16, f2d.form(Math.abs((long) deg)));
    //19 1 A1 S for south, blank otherwise.
    sb = overlay(sb, 18, str);
    //20 4 F4.2 Latitude (min).
    sb = overlay(sb, 19, f4d.form(Math.round(Math.abs(deg - (long)deg)*6000.)));
    //24 3 F3.0 Longitude (deg).
    deg = sol.lon.doubleValue(); 
    sb = overlay(sb, 23, f3d.form(Math.abs((long) deg)));
    //27 1 A1 E for east, blank otherwise.
    str = (deg > 0.) ? "E" : " ";
    sb = overlay(sb, 26, str);
    //28 4 F4.2 Longitude (min).
    sb = overlay(sb, 27, f4d.form(Math.round(Math.abs(deg - (long)deg)*6000.)));
    //32 5 F5.2 Depth (km).
    // CHANGE: note mdepth origin table schema change 2015/03/16 -aww
    if ( sol.depthCrustType.equalsValue("M") && sol.mdepth.isValidNumber() &&
         (sol.crustModelType.equalsValue("H") ||  sol.crustModelType.equalsValue("T") )
       ) {
        sb = overlay(sb, 31, f5d.form(Math.round(sol.mdepth.doubleValue()*100.)));
    }
    else {
        sb = overlay(sb, 31, f5d.form(Math.round(sol.depth.doubleValue()*100.)));
    }

      //Magnitude mag = sol.getPrefMagOfType("l");
      //if (mag != null && ! mag.hasNullValue() ) {
          ////37 3 F3.2 Amplitude magnitude. *
          //sb = overlay(sb, 36, f3d.form(Math.round(mag.getMagValue()*100.)));
          ////122 1 A1 Primary amplitude magnitude type code
          //sb = overlay(sb, 121, mag.getTypeSubString().substring(0,1).toUpperCase());
      //}

    //40 3 I3 Number of P & S times with final weights greater than 0.1.
    sb = overlay(sb, 39, f3d.form(sol.usedReadings.longValue()));
    //43 3 I3 Maximum azimuthal gap, degrees.
    sb = overlay(sb, 42, f3d.form(Math.round(sol.gap.doubleValue())));
    //46 3 F3.0 Distance to nearest station (km).
    sb = overlay(sb, 45, f3d.form(Math.round(sol.distance.doubleValue())));
    //49 4 F4.2 RMS travel time residual.
    sb = overlay(sb, 48, f4d.form(Math.round(sol.rms.doubleValue()*100.)));

    // OriginError attribute data - aww 07/12/2006
    if (sol.aziLarge.isValidNumber()) 
      sb = overlay(sb, 52, f3d.form(Math.round(sol.aziLarge.doubleValue())));
    if (sol.dipLarge.isValidNumber()) 
      sb = overlay(sb, 55, f2d.form(Math.round(sol.dipLarge.doubleValue())));
    if (sol.errLarge.isValidNumber()) 
      sb = overlay(sb, 57, f4d.form(Math.round(sol.errLarge.doubleValue()*100.)));
    if (sol.aziInter.isValidNumber()) 
      sb = overlay(sb, 61, f3d.form(Math.round(sol.aziInter.doubleValue())));
    if (sol.dipInter.isValidNumber()) 
      sb = overlay(sb, 64, f2d.form(Math.round(sol.dipInter.doubleValue())));
    if (sol.errInter.isValidNumber()) 
      sb = overlay(sb, 66, f4d.form(Math.round(sol.errInter.doubleValue()*100.)));
    if (sol.errSmall.isValidNumber()) 
      sb = overlay(sb, 76, f4d.form(Math.round(sol.errSmall.doubleValue()*100.)));
    //

      //mag = sol.getPrefMagOfType("d");
      //if (mag != null && ! mag.hasNullValue() ) {
          ////71 3 F3.2 Coda duration magnitude. *
          //sb = overlay(sb, 70, f3d.form(Math.round(mag.getMagValue()*100.)));
          ////118 1 A1 Primary coda duration magnitude type code
          //sb = overlay(sb, 118, mag.getTypeSubString().substring(0,1).toUpperCase());
      //}

    //74 3 A3 Event location remark (region), derived from location.
    //sb = overlay(sb, 73, x);
    //81 1 A1 Auxiliary remark from analyst (i.e. Q for quarry).
    sb = overlay(sb, 80, EventTypeMap3.getMap().toDbType(sol.eventType.toString()).substring(0,1).toUpperCase());

    //82 1 A1 Auxiliary remark from program (i.e. - for depth fixed, X for location fixed).
    str = (sol.depthFixed.booleanValue()) ? "-" : " ";  // fix depth only 
    str = (sol.locationFixed.booleanValue()) ? "X" : str; // fix depth and location
    str = (sol.timeFixed.booleanValue()) ? "O" : str; // letter "oh" fix origin time, location, and depth 
    sb = overlay(sb, 81, str);

    //83 3 I3 Number of S times with weights greater than 0.1.
    sb = overlay(sb, 82, f3d.form(sol.sReadings.longValue()));
    //86 4 F4.2 Horizontal error (km).
    sb = overlay(sb, 85, f4d.form(Math.round(sol.errorHoriz.doubleValue()*100.)));
    //90 4 F4.2 Vertical error (km).
    sb = overlay(sb, 89, f4d.form(Math.round(sol.errorVert.doubleValue()*100.)));
    //94 3 I3 Number of P first motions. *
    sb = overlay(sb, 93, f3d.form(sol.firstMotions.longValue()));

    //111 3 A3 3-letter code of crust and delay model.
    sb = overlay(sb,110, sol.crustModelName.toString().substring(0,3));

    //118 1 A1 Primary coda duration magnitude type code SEE ABOVE
    //sb = overlay(sb, 117, x);

    //119 3 I3 Number of valid P & S readings (assigned input weight > 0).
    sb = overlay(sb, 118, f3d.form(sol.totalReadings.longValue()));

    //122 1 A1 Primary amplitude magnitude type code SEE ABOVE
    //sb = overlay(sb, 121, x);

    Magnitude mag = sol.magnitude;
    if (mag != null && ! mag.hasNullValue()) {
        //123 1 A1 "External" magnitude label or type code. Typically "L" (=ML)
        sb = overlay(sb, 122 , mag.getTypeSubString().substring(0,1).toUpperCase());
        //124 3 F3.2 "External" magnitude.
        sb = overlay(sb, 123, f3d.form(Math.round(sol.magnitude.getMagValue()*100.)));
        //127 3 F3.1 Total of "external" magnitude weights (~ number of readings).
        sb = overlay(sb, 126, f4d.form(mag.getReadingsUsed()));
    }

    //130 1 A1 Alternate amplitude magnitude label or type code.
    //sb = overlay(sb, 129, x);
    //131 3 F3.2 Alternate amplitude magnitude.
    //sb = overlay(sb, 130, x);
    //134 3 F3.1 Total of the alternate amplitude mag weights ~no. of readings.
    //sb = overlay(sb, 133, x);

    //137 10 I10 Event identification number
    sb = overlay(sb, 136, f10d.form(sol.id.longValue())); 

    mag = sol.magnitude;
    if (mag != null && ! mag.hasNullValue()) {
        //147 1 A1 Preferred magnitude label code chosen from those available.
        sb = overlay(sb, 146, sol.magnitude.getTypeSubString().substring(0,1).toUpperCase());
        //148 3 F3.2 Preferred magnitude, chosen by the Hypoinverse PRE command.
        sb = overlay(sb, 147, f3d.form(Math.round(sol.magnitude.getMagValue()*100.)));
        //151 4 F4.1 Total of the preferred mag weights (~ number of readings). *
        sb = overlay(sb, 150, f4d.form(mag.getReadingsUsed()));
    }

    //155 1 A1 Alternate coda duration magnitude label or type code.
    //sb = overlay(sb, 154, x);
    //156 3 F3.2 Alternate coda duration magnitude.
    //sb = overlay(sb, 155, x);
    //159 4 F4.1 Total of the alternate coda duration magnitude weights. *
    //sb = overlay(sb, 158, x);

    //163 1 A1 Version of the information, i.e. the stage of processing.
    // ?? sb = overlay(sb, 162, sol.processingState.toString().substring(0,1).toUpperCase());
    // ?? sb = overlay(sb, 162, sol.srcVersion.toString().substring(0,1).toUpperCase());
    //164 1 A1 Version of last human review. Hypoinverse passes this through.
    //sb = overlay(sb, 163, parseLong(version);
    //165 2 A2 location domain authority
    if (! sol.crustModel.isNull()) sb = overlay(sb, 164, sol.crustModel.toString());
    //167 2 A2 modelid key from model table
    if (! sol.velModel.isNull()) sb = overlay(sb, 166, sol.velModel.toString());

    // BELOW CHANGE for origin table schema change 2015/03/16 -aww
    if (! sol.depthCrustType.isNull()) { //sb = overlay(sb, 168, "G");
        //169 1 A1 Depth type (M model depth, G geoid depth) set with GEO command.
        sb = overlay(sb, 168, sol.depthCrustType.toString().substring(0,1));
        //170 1 A1 Dominant crust model type: T=CRT, H=CRH, E=CRE, V=CRV, L=CRL
        if (! sol.crustModelType.isNull()) sb = overlay(sb, 169, sol.crustModelType.toString().substring(0,1));
        // NOTE: no member data field in Solution class for datum so we have to calculate it
        if (! sol.depth.isNull() )
          if ( ! sol.mdepth.isNull() ) {
            // elev meters
            int idatum = (int) Math.round( sol.mdepth.doubleValue() - sol.depth.doubleValue() )*1000;
            //171 4 I4 Earthquake depth datum in m above the geoid (CRT model surface).
            sb = overlay(sb, 170, f4d.form(idatum));
            // Temp if-else block until schema changed 2015/03/31 -aww
            //175 5 F5.2 Geoid depth in km below the geoid or sea level.
            if ( sol.depthCrustType.equalsValue("M") && (sol.crustModelType.equalsValue("H") ||  sol.crustModelType.equalsValue("T") )) {
                //175 5 F5.2 Geoid depth in km below the geoid or sea level.
                sb = overlay(sb, 174, f5d.form(Math.round((sol.mdepth.doubleValue()-(idatum/1000.))*100.)));
            }
            else {
                sb = overlay(sb, 174, f5d.form(Math.round(sol.depth.doubleValue()*100.)));
            }
            if (debug)
                System.out.println("DEBUG HypoFormat2 eDatumKm+gdepthKm=mdepthKm: "+idatum+"/1000+"+sol.depth.doubleValue() +"="+ sol.mdepth.doubleValue()); 

        }
    }

    return (sol == null) ? "    " : sb.toString(); // 168 is the last filled column.
  }

/**
 * Format the channel as a Hypoinverse #2 station data format string.
         |         |         |         |         |         |         |         |
12345678901234567890123456789012345678901234567890123456789012345678901234567890
                                      xxxx3.1
GLA   CI  HHZ  33  0.0520N114  0.8270E   00.0  P  0.00  0.00  0.00  0.00 0  0.00
BAR   CI  HLE  32 40.8000 116 40.3300  496.0 A 0.00 0.00 0.00 0.00 1 0.00
BAR   CI  HLN  32 40.8000 116 40.3300  496.0 A 0.00 0.00 0.00 0.00 1 0.00
BAR   CI  HLZ  32 40.8000 116 40.3300  496.0 A 0.00 0.00 0.00 0.00 1 0.00
BAR2  CI  EHZ  32 40.8000 116 40.3300  496.0 A 0.00 0.00 0.00 0.00 1 0.00
BAR2  CI  VHZ  32 40.8000 116 40.3300  496.0 A 0.00 0.00 0.00 0.00 1 0.00
 */
  public static String toH2StationString(Channel chan) {

    StringBuffer sb = new StringBuffer(82);

    Format d2 = new Format("%2d");
    Format d3 = new Format("%3d");
    Format f52 = new Format("%5.2f");
    Format f62 = new Format("%6.2f");
    Format f74 = new Format("%7.4f");

    ChannelName cn = chan.getChannelName();

    String str = cn.getSta();
    if (str.length() > 5) str = str.substring(0,5);
    sb = sb.append ( new Format("%-5s ").form(str) );

    str = cn.getNet();
    if (str.length() > 2) str = str.substring(0,2);
    sb = sb.append ( new Format("%-2s  ").form(str) );

    str = cn.getSeedchan();
    if (str.length() > 3) str = str.substring(0,3);
    sb = sb.append ( new Format("%-3s  ").form(str) );

/*
    try {
      sb = sb.append(cn.getSta().substring(0,5)+" ");
    }
    catch  (StringIndexOutOfBoundsException e) {}

    try {
      sb = sb.append(cn.getNet().substring(0,2)+" ");
    }
    catch  (StringIndexOutOfBoundsException e) {}
    sb = sb.append(' ');          // optional 1-letter comp code
    try {
      sb = sb.append(cn.getChannel().substring(0,3)+" ");
    }
    catch  (StringIndexOutOfBoundsException e) {}

    sb = sb.append(' ');          // optional sta weight code code
*/

// lat/lon
    LatLonZ latlonz = chan.getLatLonZ();
    sb = sb.append(d2.form(Math.abs(latlonz.getLatDeg()))).append(" ");
    sb = sb.append(f74.form(latlonz.getLatMin()));
    if (latlonz.getLat() > 0.) {
       sb = sb.append('N');          // North
    } else {
       sb = sb.append('S');          // South
    }

    sb = sb.append(d3.form(Math.abs(latlonz.getLonDeg()))).append(" ");
    sb = sb.append(f74.form(latlonz.getLonMin()));
    if (latlonz.getLon() > 0.) {
       sb = sb.append('E');
    } else {
       sb = sb.append('W');
    }

    // Note changed LatLonZ elevation sign convention, z => depth, depths are positive 6/15/2004 -aww
    sb = sb.append( new Format("%4d").form( (int)(-latlonz.getZ()*1000.) ) ); // depth(km) to elevation(m) (not used)

    sb = sb.append( new Format("%3.1f").form(0.0) );   // period

    sb = sb.append("  P ");   // space, primary model, remark

    sb = sb.append( f52.form(0.0) ).append(" ");   // P-delay #1
    sb = sb.append( f52.form(0.0) ).append(" ");   // P-delay #2

    sb = sb.append( f52.form(0.0) ).append(" ");   // Amp corr & weight

    sb = sb.append( f52.form(0.0) ).append(" ");   // mag corr. & wt

    sb = sb.append( "0" );   // inst type code

    sb = sb.append( f62.form(0.0) );   // Cal factor

    // location name extension, use location from ChannelName - aww 04/15/2005
    //str = cn.getLocation();
    str = (locationCodeSpace2Dash) ? cn.getLocationString() : cn.getLocation();  // -aww 2011/02/24 alternative
    //Kludge guess of code if unknown (database tables missing code) -aww
    if (str == null || str == "" || str.equalsIgnoreCase("NULL")) {
        //str = EnvironmentInfo.getLocationCode(chan.getNet());
        //str = EnvironmentInfo.getLocationCode(EnvironmentInfo.getNetworkCode()); // remove 2008/01/10 - aww
        str = "??";  // added 2008/01/10 -aww
    }
    if (str.length() > 2) str = str.substring(0,2);
    sb = sb.append (new Format("%-2s").form(str));

    return sb.toString();
  }

/**
 * Parse the  Hypoinverse2000 "summary header" line and and return a new Solution object <p>
 */
  public static Solution parseArcSummary(String str) {
    Solution sol = Solution.create();
    parseArcSummary(sol, str);
    return sol;
  }
/**
 * Parse the  Hypoinverse2000 "summary header" line into an existing Solution object. <p>
 * This will only replace those values in the Solution that are returned by Hypoinverse. In
 * particular the orid, evid, commid, Event and Magnitude objects of the Solution are left
 * unchanged. Other fields are not modified because that info is not in the Hypoinverse
 * output, e.g. authority, source, quality, type, etype, errorLat, errorLon, etc.
 */

  public static Solution parseArcSummary(Solution inSol, String str) {

  Solution sol = inSol; // local copy reference

   try { // catch parse exception

// Only use the evid from Hypoinverse if there isn't one already
    if (sol.id.longValue() <= 0l) sol.id.setValue( parseLong(str, 136, 146) );

// Origin time
    sol.datetime.setValue(parseArcSummaryTime(str));

// Latitude
    double deg = parseDouble(str, 16, 18);
    double min = (double) parseInt(str, 19, 23) / 100.; // f4.2 :. /100
    double xlat = deg + ( min / 60.);
    String hemi = str.substring(18,19);  // "S" for south, BLANK otherwise!
    if (hemi.equalsIgnoreCase("S")) xlat = -xlat;

// Longitude
    deg = parseDouble(str, 23, 26);
    min = (double) parseInt(str, 27, 31) / 100.;        // f4.2 :. /100
    // WARNING: Hypoinverse does NOT follow its own rules. The summary line returns
    // positive longitude and does NOT put "E" in the line. :. must kludge here.
    double xlon = -(deg + ( min / 60.));
    hemi = str.substring(26,27);         // "E" for south, BLANK otherwise!
    if (hemi.equalsIgnoreCase("E")) xlon = -xlon;

// Z - for H,T model types value depends on GEO, T its geoid, F the default its model depth (for other model types it's always geoid and datum=0)
    double xz = parseInt(str, 31, 36) / 100.;   // f5.2, in Schema positive depth is down.

    // AWW  03/16/2015 - origin table schema changes
    if (str.length() > 168) {
      // 111 3 A3 3-letter code of crust and delay model.
      sol.crustModelName.setValue(str.substring(110,113));

      //169 1 A1 Depth type (M model depth, G geoid depth) set with GEO command.
      sol.depthCrustType.setValue(str.substring(168,169));
      //170 1 A1 Dominant crust model type: T=CRT, H=CRH, E=CRE, V=CRV, L=CRL
      sol.crustModelType.setValue(str.substring(169,170));
      //171 4 I4 Earthquake depth datum in m above the geoid (CRT model surface).
      double eDatumKm = (double) parseInt(str, 170, 174)/1000.;
      //String depthType = str.substring(168,169); // G or M
      //if (depthType.equals("M")) { // reported depth is geoidal
      //    xz = (double) parseInt(str, 31, 36) / 100.;   // f5.2, in Schema positive depth is down.
      //}
      //else { // get geoidal depth from:
        xz = (double) parseInt(str, 174, 179)/100.;
      //}
      sol.mdepth.setValue(xz+eDatumKm);
      if (debug)
        System.out.println("DEBUG HypoFormat eDatumKm+gdepthKm=mdepthKm: " + eDatumKm + "+" + xz + "=" + sol.mdepth.doubleValue()); 
    }
    else { // punt, assign depth to mdepth when user's hypinverse version < 1.4
        System.out.println("ERROR HypoFormat REQUIRES hypoinverse 1.4+ version, defaulting model depth to parsed arc summary depth: " + xz ); 
        sol.mdepth.setValue(xz);
    }
    
    // Note do not set lat,lon,z using public access of Solution members
    // need to use setLatLonZ method since side-effect sets stale state of preferred magnitude
    sol.setLatLonZ(xlat, xlon, xz);
//
// Local Amp Mag : 36,39 ?  and 122 1 A1 Primary amplitude magnitude type code ?
//
// Location parameters
    sol.usedReadings.setValue(parseInt(str, 39, 42));   // NOTE: P & S w/ weights > 0.1
    //sol.totalReadings.setValue(sol.phaseList.size());   // removed total associated phases includes those with inWgt=0 -aww 2011/05/11
    // only those with inWgt> 0, instead do this in LocationEngine -aww 2011/05/11 
    //sol.totalReadings.setValue(sol.phaseList.getChannelWithInWgtCount());
    //System.out.println("DEBUG HypoFormat usedReadings : " + sol.usedReadings);
    //System.out.println("DEBUG HypoFormat totalReadings: " + sol.totalReadings);
    sol.gap.setValue(parseDouble(str, 42, 45));
    sol.distance.setValue(parseDouble(str, 45, 48));
    sol.rms.setValue(parseDouble(str, 48, 52) / 100.);

    //  OriginError attribute table - aww 07/12/2006
    sol.aziLarge.setValue(parseDouble(str,52,55));
    sol.dipLarge.setValue(parseDouble(str,55,57));
    sol.errLarge.setValue(parseDouble(str,57,61)/100.);
    sol.aziInter.setValue(parseDouble(str,61,64));
    sol.dipInter.setValue(parseDouble(str,64,66));
    sol.errInter.setValue(parseDouble(str,66,70)/100.);
    sol.errSmall.setValue(parseDouble(str,76,80)/100.);
    //DEBUG System.out.println("Solution error: " + sol.toErrorString());

    String sfix = str.substring(81,82);
    //System.out.println("HypoFormat sfix = " + sfix + " returned str:\n" + str);
    if (sfix.equals("-")) {
        sol.depthFixed.setValue(true);
        sol.locationFixed.setValue(false);
        sol.timeFixed.setValue(false);
    }
    else if (sfix.equals("X")) {
        sol.depthFixed.setValue(true);
        sol.locationFixed.setValue(true);
        sol.timeFixed.setValue(false);
    }
    else if (sfix.equals("O")) {
        sol.depthFixed.setValue(true);
        sol.locationFixed.setValue(true);
        sol.timeFixed.setValue(true);
    }
    else {
        sol.depthFixed.setValue(false);
        sol.locationFixed.setValue(false);
        sol.timeFixed.setValue(false);
    }

    sol.sReadings.setValue(parseInt(str, 82, 86));
    sol.errorHoriz.setValue(parseDouble(str, 85, 89) / 100.);
    sol.errorVert.setValue(parseDouble(str, 89, 93) / 100.);
    sol.firstMotions.setValue(parseInt(str, 93, 96));

    // EventQuality requires errorVert & errorHoriz
    sol.quality.setValue(EventQuality.getValue(sol));

    // String intern() keeps char array ref from propagating when input arg is large string.
    //sol.crustModel.setValue(str.substring(110, 113).intern());
    //sol.setSourceVersion.setValue(str.substring(162, 163).intern());
    //sol.setVersion(str.substring(163, 164).intern());
    if (str.length() >= 168) { // -aww bugfix 2015/11/16
        sol.crustModel.setValue(str.substring(164, 166).intern());
        sol.velModel.setValue(str.substring(166, 168).intern());
    }

    // parameters are not available from the Hypoinverse format: subsource
    // Other problems:  "last authority" is one char, we expect FDSN char[2]

    // magnitude info not read yet
    //  grab duration magnitude data and build new Md mag

/*
    Number num;
    if((num = parseNumber(str, 70, 73)) != null)
    {
       Magnitude mag = Magnitude.create();
       mag.subScript = new DataString("d");
       mag.value = new DataDouble(num.doubleValue()/100);

    // BOGUS DON'T DO BELOW !!!
       if((num = parseNumber(str, 100, 105)) != null)
       {
         mag.usedChnls = new DataLong((num.intValue()+9.9)/10);  // approx
       }

       // mad (Median Absolute Difference)  -- could be used for quality
       if((num = parseNumber(str, 107, 110)) != null)
       {
         mag.error = new DataDouble(num.doubleValue() / 100.);  // approx
       }
       mag.sol = sol;
       mag.codaList = sol.codaList;
       mag.source = sol.source;
       sol.magnitude = mag;
    }
*/
   }
   catch (StringIndexOutOfBoundsException ex) {
      sol = null;
      System.err.println(ex.getMessage());
   }
   catch (Exception ex) {
      sol = null;
      System.err.println(ex.getMessage());
   }

   //if (sol != null) System.out.println("Solution after parse\n: " + sol.toString());
   return sol;
  }

/**
 * Parse the P-wave time in the station Archive string
 */
  public static double parseArcPTime(String str) {
    double epochSecs = HypoFormat.parseArcYMDHM(str.substring(17,29));

    // BUG: when whole sec is just '-' parse of seconds wrong - p.lombard
    //int sec  = (int) parseDouble(str, 29, 32);      // whole seconds
    //int frac = (int) parseDouble(str, 32, 34);     // fractional 0.01 seconds
    //if (sec < 0) frac = -frac;
    //return epochSecs + (double) sec + (double)frac/100.;     // convert to epoch time

    // above logic replaced by below as per Lombard request - aww 2008/09/04
    return epochSecs + (double) ParseString.getIntToFloat(str, 29, 34, 2); // convert to epoch time
  }

/**
 * Parse the S-wave time in the station Archive string
 */
  public static double parseArcSTime(String str) {
    double epochSecs = HypoFormat.parseArcYMDHM(str.substring(17,29));

    // BUG: when whole sec is just '-' parse of seconds wrong - p.lombard
    //int sec  = (int) parseDouble(str, 41, 44);      // whole seconds
    //int frac = (int) parseDouble(str, 44, 46);     // fractional 0.01 seconds
    //if (sec < 0) frac = -frac;
    //return epochSecs + (double) sec + (double)frac/100.;     // convert to epoch time

    // above logic replaced by below as per Lombard request - aww 2008/09/04
    return epochSecs + (double) ParseString.getIntToFloat(str, 41, 46, 2); // convert to epoch time
  }

/**
 * Return epoch second of time string up to the minute. Does NOT include the seconds part
 * which varies with context. For station lines you need to pass <str>.substring(17,29).
 * This is the "base minute" of the "card".
 */
  public static double parseArcYMDHM(String str) {

/*
    int yr = parseInt(str,  0,  4);
    int mo = parseInt(str,  4,  6) - 1;  // in Java-land January = 0, etc.
    int dy = parseInt(str,  6,  8);
    int hr = parseInt(str,  8, 10);
    int mn = parseInt(str, 10, 12);
*/

// Y2K note: Date expects 'yr' to be "years since 1900", but Calendar expects 4 digit year.
// January is month '0'
// If you try to use GMT time zone here, Java will kindly "correct" to local time later when
// you use SimpleDateFormat and subtract 7 or 8 hours. THIS ISN'T WHAT WE WANT. So, just use
// default and it should be OK.
//    TimeZone gmt = TimeZone.getTimeZone("GMT");
/*
    TimeZone gmt = TimeZone.getDefault();
    GregorianCalendar cal = new GregorianCalendar(gmt);
    cal.set(yr, mo-1, dy, hr, mn, 0);                  // convert to Java time base
    long epochMillis = cal.getTime().getTime();
*/
    //DateTime dt = new DateTime(yr, mo, dy, hr, mn, 0.0); // replaced by below for leap secs - aww 2008/02/04
    //return dt.getEpochSeconds(); // replaced by below for leap secs - aww 2008/02/04
    
    //System.out.println("DEBUG str: " + str.substring(0,12));
    return LeapSeconds.stringToTrue(str.substring(0,12), "yyyyMMddHHmm");
  }

/**
 * Parse the Archive summary line format string. Note: its slightly different from the
 * time in the station lines.
 * <tt>
 * "199902101612100333 4015116 5965    3  0 31155  3  2316273 10134516  58  0     28 #  2  56  97 37   0   0  0  0HAD      39    0  0   0  0  13761428   0   0   0   0"
 </tt>
 *
 */
  public static double parseArcSummaryTime(String str) {
    double epochSecs = HypoFormat.parseArcYMDHM(str);
    // BUG: when whole sec is just '-' parse of seconds wrong
    //int sec  = parseInt(str, 12, 14);  // whole seconds (f4.2) Note:sta fmt is (f5.2)
    //int frac = parseInt(str, 14, 16);  // fractional seconds
    //if (sec < 0) frac = -frac;
    //return epochSecs + (double) sec + (double)frac/100.;     // convert to epoch time

    // above logic replaced by below as per Lombard request - aww 2008/09/04
    return epochSecs + (double) ParseString.getIntToFloat(str, 12, 16, 2); // convert to epoch time
  }

/**
 * Convert the specified field in the string to a Number object.
 * Returns 'null' upon exception or if input String is blank. */
  public static Number parseNumber(String str, int start, int stop) {
    if (str.length() < stop) return null;
    String substr = "";
    try
    {
      substr = str.substring(start, stop).trim();
      return (substr.length() == 0) ? null : Double.valueOf(substr);
    }
    catch (NumberFormatException exc)
    {
      System.out.println ("Number format error: /"+substr+"/");
    } catch  (StringIndexOutOfBoundsException e) {
      System.out.println ("String out of bounds error: /"+str+"/");
    }
    return null;
  }
/**
 * Convert the specified field in the string to a value. Returns "0.0" on error.
 * @return  0. if input is empty/blank String.
 * @return -1. input error condition.
 */
  public static double parseDouble(String str, int start, int stop) {
    if (str.length() < stop) return -1.;
    String substr = "";
    try
    {
      substr = str.substring(start, stop).trim();
      return (substr.length() == 0) ? 0. : Double.valueOf(substr).doubleValue();
    }
    catch (NumberFormatException exc)
    {
      System.out.println ("Double format error: /"+substr+"/");
    } catch (StringIndexOutOfBoundsException e) {
      System.out.println ("String out of bounds error: " + str);
    }
    return -1.;  // error state
  }

/**
 * Convert the specified field in the string to an int value.
 * Returns -1 input error condition, 0 if input is empty/blank String.
 */
  public static int parseInt(String str, int start, int stop) {
    if (str.length() < stop) return -1;
    String substr = "";
    try
    {
      substr = str.substring(start, stop).trim();
      return (substr.length() == 0) ? 0 : Integer.valueOf(substr).intValue();
    }
    catch (NumberFormatException exc)
    {
      System.out.println ("Integer format error: /"+substr+"/");
    } catch (StringIndexOutOfBoundsException e) {
      System.out.println ("String out of bounds error: " + str);
    }
    return -1;  // error state
  }

/**
 * Convert the specified field in the string to an int value.
 * Returns -1l input error condition, 0l if input is empty/blank String.
 */
  public static long parseLong(String str, int start, int stop) {
    if (str.length() <= stop) return -1l;
    String substr = "";
    try
    {
      substr = str.substring(start, stop).trim();
      return (substr.length() == 0) ? 0l : Long.valueOf(substr).longValue();
    }
    catch (NumberFormatException exc)
    {
      System.out.println ("Long format error: /"+substr+"/");
    } catch (StringIndexOutOfBoundsException e) {
      System.out.println ("String out of bounds error: " + str);
    }
    return -1l;  // error state
  }

/**
 * Parse a station list line in Hypoinverse format into a Channel object.
 * Returns null if there's a problem.
 */
/*
0         1         2         3         4         5         6         7         8
012345678901234567890123456789012345678901234567890123456789012345678901234567890
ABL  34 50.91N119 13.50W1975        0.0                            GSP VHZ 2252355    30  1700

 */

  public static Channel parseStationLine(String str) {
    // Method not called by Java code
    // What format is this? below may not be true - aww 04/15/2005

    double deg, min, lat, lon, z;

    Channel ch = Channel.create();

//TODO: make this general!!

    // ChannelName uses String intern() to keep char array from propagating when input arg is large string.
    ch.setNet(EnvironmentInfo.getNetworkCode());     // must assume
    ch.setSta(str.substring(0, 5).trim());  // trim blanks
    ch.setChannel(str.substring(71, 74).trim()); // Is this Right?  - aww 04/15/2005
    ch.setSeedchan(ch.getChannel());    // kludge

// cull out obvious crap
    if ( ch.getSta().equals("") ) return null;
    if ( ch.getChannel().equals("???") ) return null;

    deg = parseDouble(str, 5, 7);
    min = parseDouble(str, 8, 13);
    lat  = deg + (min/60.0);
    if (str.substring(13, 14).equalsIgnoreCase("S")) lat *= -1.0;    // change sign

    deg = parseDouble(str, 14, 17);
    min = parseDouble(str, 18, 23);
    lon = deg + (min/60.0);
    if (str.substring(23, 24).equalsIgnoreCase("W")) lon *= -1.0;    // change sign

    // guard against bogus lines
    if (lat == 0.0 && lon == 0.0) return null;

    z = parseDouble(str, 24, 28);
    z = -z/1000.;  // m -> km, change sign to depth convention - aww 06/11/2004

    ch.getLatLonZ().set(lat, lon, z);

    //if (str.length() > 73) ch.setLocation(str.substring(73,75)); // -aww (location can have blanks) not sure this is used

    return (ch);
  }


/**
 * Parse a station list line in Hypoinverse archive format into a Channel object.
 * Returns null if there's a problem. */
/*
0         1         2         3         4         5         6         7         8
012345678901234567890123456789012345678901234567890123456789012345678901234567890
ABL   CI  EHZ  34 50.9100 119 13.5000 1975.0 A 0.00 0.00 0.00 0.00 1 0.00

 */

  public static Channel parseStationArcLine(String str) {
    // Method not called by Java code
    double deg, min, lat, lon, z;

    Channel ch = Channel.create();

    // ChannelName uses String intern() to keep char array from propagating when input arg is large string.
    ch.setNet(str.substring(6, 8).trim());
    ch.setSta(str.substring(0, 5).trim());  // trim blanks
    ch.setChannel(str.substring(10, 13).trim());
    ch.setSeedchan(ch.getChannel());    // kludge

// cull out obvious crap
    if ( ch.getSta().equals("") ) return null;
    if ( ch.getChannel().equals("???") ) return null;

    deg = parseDouble(str, 15, 17);
    min = parseDouble(str, 18, 25);
    lat  = deg + (min/60.0);

    deg = parseDouble(str, 26, 29);
    min = parseDouble(str, 30, 37);
    lon = deg + (min/60.0);
// kludge !!!!! Longitudes in the file are positive and have no hemisphere letter.
    lon *= -1.0;    // change sign

    // guard against bogus lines
    if (lat == 0.0 && lon == 0.0) return null;

    z = parseDouble(str, 38, 44);
    z = -z/1000.;  // m -> km, change sign to depth convention - aww 06/11/2004

    ch.getLatLonZ().set(lat, lon, z);

    // get ChannelName extension location code from end of string -aww 04/15/2005
    // location can have blanks, removed substring trim() -aww 07/11/05
    if (str.length() > 80) ch.setLocation(str.substring(80,82));

    return (ch);

  }

/*
// Main for testing
  static public final class Tester {
    public static void main(String[] arg) {

  //DataSource ds = TestDataSource.create();
  DataSource ds = TestDataSource.create("serveri","databasei","calib","calib");

  //Solution sol1 = Solution.create().getById(9082441);
  Solution sol1 = Solution.create().getById(Long.parseLong(arg[0]));
  if (sol1 != null) {
    sol1.loadMagList(true);
    sol1.loadPhaseList();
    sol1.loadMagCodaList();
    sol1.loadMagAmpList();
    System.out.println (HypoFormat.toTerminatorString(sol1, true, true, false, true, true));
    System.out.println ("HYP2000 out: |"+toSummaryString(sol1));
    Solution sol = parseArcSummary(toSummaryString(sol1));
    System.out.println ("input id sol.toSummaryString(): " + sol.toSummaryString());
  }

//
//         1         2         3         4         5         6         7         8         9        10        11
//012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
//STATNnt cCMP pr  yearmodyhrmnseconPresPwtS-secSr sSrespek2pekmmSwtPdlySdlyDistAngADPerRCodaAzmDurMagPimpSimpSFX
//QAL  CI VVHZ     199811120254    0   0  0 5709ES 2 301      0 0  0   0   01359 7000  0    0174  0  0   0   0
//QAL  CI VVHZ     199811120254      301 50 5709ES 2                        1359              70
//
    //String testarr = "MCV  NC VVHZ  PU0199806262007 4310  -9110    0   0   0      0 0  0   0   0  1616400  0  159297404  0 502   0WD ";

      String testarr =
"QAL  CI VVHZ IPU2199811120254    0   0  0 5709ES 2 301      0 0  0   0   01359 7000  0    0174  0  0   0   0   ";

      String testorg =
"199902101612100333 4015116 5965 1819312005115012 002316271111222580123212---  28A#  2  56  97 37   0   0  0  0HAD      39    0  0   0  0  13761428   0   0   0   000CI01";

// parse the string
      Phase ph = parseArcToPhase(testarr);

// reformat
      System.out.println (" in: |"+testarr);
      System.out.println ("out: |"+toArchiveString (ph));
      System.out.println (ph.toString());
      System.out.println("");

// solution summary
      Solution sol = parseArcSummary(testorg);
      sol.id.setValue(13761428l);   
      //sol.setEventType(EventTypeMap.LOCAL);    //removed, bumps version -aww 2008/04/15
      sol.eventType.setValue( EventTypeMap3.getMap().toJasiType(EventTypeMap3.EARTHQUAKE) ); // to not bump version
      sol.gtype.setValue( GTypeMap.LOCAL );
      System.out.println(" in: |"+testorg);
      System.out.println("out: |"+toSummaryString(sol));
      System.out.println("testorg sol.summaryString()");
      System.out.println(sol.getNeatStringHeader());
      System.out.println(sol.toNeatString());
      System.out.println("");

// terminator variants
      System.out.println(HypoFormat.toTerminatorString(sol));
      System.out.println(HypoFormat.toTerminatorString(sol, true, true, true, false, true));
      System.out.println(HypoFormat.toTerminatorString(sol, true, false, false, false, false));
      System.out.println(HypoFormat.toTerminatorString(sol, false, true, false, false, false));
      System.out.println(HypoFormat.toTerminatorString(sol, false, false, false, true, false));
      System.out.println(HypoFormat.toTerminatorString(sol, false, false, true, true, true));
    }
  } // end of Tester
*/
} // end of class

