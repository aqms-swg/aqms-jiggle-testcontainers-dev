package org.trinet.jasi;

import java.util.*;

import org.trinet.util.*;
//import org.trinet.jiggle.JiggleProperties;  // for the main testing

/**
Model for determining the set of channels and time-windows likely to have
seismic energy above the noise level. <p>
How the technique works: first, calculate the expected <b>ground motion</b> for the
given solution's magnitude at the site using the Joyner-Boore relationship
that is used by ShakeMap for small events. This will be an acceleration or a
velocity depending on the channel type. Then that expected
ground motion is converted to counts using the gain of the channel.
Ideally the average background noise of each channel (in counnts) would be known,
but since it isn't we compare the expected signal amp to a default threshold
to see if the seismic energy would rise above the noise level. In other words,
<b>if the signal-to-noise ratio (SNR) is greater than one, the channel is saved.</b><p>

The default inclusion threshold is 200 counts.<br>
The default include all magnitude threshold is M >= 3.0<p>

Minimum window duration (secs) =  40.0<br>
Minimum window duration (secs)  = 600.0 (this will truncate codas > ~ML 5.0)<br>
Secs added to start of time window calculated by the model algorithm = 20.0<br>
Secs added to duration of time window calculated by the model algorithm  = 20.0 <br>
Maximum horizontal map distance allowed for an included Channel = 1000.0<br>

Properties specific to this class:<br>

org.trinet.jasi.JBChannelTimeWindowModel.noiseThreshold=25
org.trinet.jasi.JBChannelTimeWindowModel.thresholdFactor=1.0

*/
public class JBChannelTimeWindowModel extends AbstractSignalTimeWindowModel {

    public static final String defModelName = "Joyner/Boore";

    /** A string with an brief explanation of the model. For help and tooltips. */
    public static final String defExplanation =
      "channels with seismic energy predicted by Joyner/Boore model using channel's gain";

    /** Threshold above which theoretical ground motion must go to be included in the channel set.*/
    public static double defNoiseCounts = 200.0;
  
    /** Factor by which ground motion must exceed background noise level. Default = 1.0
     * For example, if this value is 2.0 and the background noise level at a particular 
     * site is 30 counts then the calculated ground motion must be 60 counts or more 
     * (2 * 30) to be included. */
    public static final double defThresholdFactor = 1.0;

    /** Minimum window duration (secs). Some algorithms may need a minimum amount of data.*/
    public static final double defMinWindow    =  40.0;
    /** Minimum window duration (secs).*/
    public static final double defMaxWindow    = 600.0;  // this will truncate codas > ~5.0
    /** Time added to start of time window calculated by the model algorithm (secs).*/
    public static final double defPreEvent     =  20.0;
    /** Time added to duration of time window calculated by the model algorithm (secs).*/
    public static final double defPostEvent    =  20.0;

    /** The maximum horizontal map distance allowed by the getDistanceCutoff() method.
     *  All channel beyond this distance will be excluded from the Channel set.*/
    public static final double defMaxDistance = 1000.0;

    /** Include all components (orientations) of a channel type if any is included. */
    public static final boolean defIncludeAllComponents = true;

    /** Include all channels if the magnitude >= this value.*/
    public static final double defIncludeAllMag = 3.0;

    protected static double thresholdFactor = defThresholdFactor;
    protected static double noiseCounts = defNoiseCounts;

    // set defaults for this model
    {
      setModelName(defModelName);
      setExplanation(defExplanation);
    }

    public JBChannelTimeWindowModel() {
        setMyDefaultProperties();
    }

    public JBChannelTimeWindowModel(GenericPropertyList gpl, Solution sol, ChannelableList candidateList) {
        super(gpl, sol, candidateList);
    }
    public JBChannelTimeWindowModel(Solution sol, ChannelableList candidateList) {
        super(sol, candidateList);
    }
    public JBChannelTimeWindowModel(ChannelableList candidateList) {
        super(candidateList);
    }
    public JBChannelTimeWindowModel(Solution sol) {
        this();
        super.setSolution(sol);
    }

    /** Set the factor (multiplier) by which the calculated value must
     * exceed the background noise level. */
    public void setThresholdFactor(double factor) {
      thresholdFactor  = factor;
    }
    /** Get the factor (multiplier) by which the calculated value must
     * exceed the background noise level. */
    public double getThresholdFactor() {
      return thresholdFactor;
    }
    
    /** Set the default noise floor in counts.
     *  This is used if the channel-specific noise floor is unknown. */
     public void setThresholdCounts(double noiseCountThreshold) {
       noiseCounts = noiseCountThreshold;
     }    
    /** Return the default noise floor in counts.
     *  This is used if the channel-specific noise floor is unknown.
     */
    public double getThresholdCounts() {
      return noiseCounts;
    }
    /** Return the noise floor for this channel in counts. */
    public double getThresholdCounts(Channel ch) {
      return getNoiseLevelInCounts(ch) ;
    }
    /** Guess the noise level (in counts) for a given channel.
    This should lookup a real emperical value from the dbase but no noise statistics are
    available, so this just has some rough values based on SeedChannel name. */
    public double getNoiseLevelInCounts(Channel ch) {

      String seedChan = ch.getSeedchan();

      double noiseFloor = getThresholdCounts();

      if (seedChan.startsWith("HH") ||
          seedChan.startsWith("BH") ||
          seedChan.startsWith("LH") )       { noiseFloor = 150;
      } else if (seedChan.startsWith("HL") ||
                 seedChan.startsWith("LL") ||
                 seedChan.startsWith("BL") ||
                 seedChan.startsWith("BN") ||
                 seedChan.startsWith("HN")) { noiseFloor =  20; // acceleration
      } else if (seedChan.startsWith("HG")) { noiseFloor = 150;
      } else if (seedChan.startsWith("EH")) { noiseFloor =  30;
      } else if (seedChan.startsWith("EL")) { noiseFloor =  40;
      } else if (seedChan.startsWith("SH")) { noiseFloor =  30;
      }
      return noiseFloor;
    }

    //Implementation of abstract method declared in superclass.
    /** Return true if seismic energy is likely for input channel. */
    public boolean shouldShowEnergy(Channel ch, double mag, double dist, double z) {
        return shouldShowEnergyByPowerLaw(ch, mag, dist, z);
    }

    /**
     * Return true if the channel is likely to have seismic energy above the background
     * noise level, that is, the calculated amplitude exceeds the nominal noise level. 
     * This a weak link because the ACTUAL noise level of the channel is unknown.
     * @param ch
     * @param mag
     * @param dist
     * @param z
     * @return
     */
    public boolean shouldShowEnergyByNoiseLevel(Channel ch, double mag, double dist, double z) {

      ChannelGain chanGain = ch.getGain(sol.getDateTime()); // should this do a db lookUp ?
      if (chanGain.isNull()) chanGain = getNominalGain(ch);   // no gain in dbase - use nominal

      double gain = chanGain.doubleValue();
      int   units = chanGain.getUnits();       // gain is in counts/(cm/sec) or counts/(cm/sec2)

      // special case dicking around with gain because some BK channels have NEG gain
      // (More efficient than Math.abs()?)
      if (gain < 0.0) gain = -gain;

      double pgm;

      // Calculate peak ground velocity or acceleration for this mag/distance
      // Use Gain.units just to tell if its vel or accel
      if ( Units.isVelocity(units) ) {
        pgm = JBModel.getPeakVelocity(mag, z, dist);   // velocity
      } else if ( Units.isAcceleration(units) ) {
        pgm = JBModel.getPeakAccel(mag, z, dist);      // acceleration
      } else {
        System.err.println("Invalid gain units: "+ch.getChannelId().toString()+" "+Units.getString(units));
        pgm = 0.0;  // will not include
      }
      if (units == Units.DuMS || units == Units.DuMSS) gain = gain/100.; // 01/23/2006 aww

      // convert amplitude to counts: amp [cm/sec] * gain [counts/(cm/sec)] = counts    
      long ampCounts = (long) (pgm * gain);   

      // does calculated amp exceed the noise level threshold * factor?
      // THIS IS A WEAK LINK - actual noise level is unknown and must be approximated.
      double threshold = getThresholdCounts(ch) * getThresholdFactor();

      // does it meet the trigger threshold?
      boolean status = ( ampCounts >= threshold );

      if (debug) {
          String x = "a";
          if ( Units.isVelocity(units) ) {x="v";}
         String str = ch.channelId.toDelimitedSeedNameString(".")+ 
            "  thresh= "+ threshold +
            "  gain= "+gain+" * cnts= "+ampCounts+
            "  =PG"+x+"= "+pgm + "  dist = "+ (int)dist;
         if (status){
             System.out.println("+ "+str);
         } else {
             System.err.println("- "+str);  // err to make it red in IDE
         }
      }
      return status;
    }
    /** 
     * This method calculates the expected velocity or acceleration at this station
     * and returns true if it exceeds a hardwired threshold value.
     * This is an experiment.
     * @param ch
     * @param mag
     * @param dist
     * @param z
     * @return
     */
    public boolean shouldShowEnergyByThreshold(Channel ch, double mag, double dist, double z) {

          ChannelGain chanGain = ch.getGain(sol.getDateTime()); // should this do a db lookUp ?
          if (chanGain.isNull()) chanGain = getNominalGain(ch); // no gain in dbase - use nominal

          double gain = chanGain.doubleValue();
          int   units = chanGain.getUnits();       // gain is in counts/(cm/sec) or counts/(cm/sec2)

          // Experimental: arbitrary threshold levels
          // These are too conservative 
//          double threshVel = 0.00012;  // cm/sec
//          double threshAcc = 0.005;    // cm/sec^2
//          double threshVel = 0.00024;  // cm/sec
//          double threshAcc = 0.010;    // cm/sec^2
          double threshVel = 0.0005;   // cm/sec
          double threshAcc = 0.020;    // cm/sec^2
          // special case dicking around with gain because some BK channels have NEG gain
          if (gain < 0.0) gain = -gain;

          double pgm;
          String str = ch.channelId.toDelimitedNameString(".")+"  gain= "+gain+ "  dist = "+
                       (int)ch.getHorizontalDistance();
          // Calculate peak ground velocity or acceleration for this mag at this distance
          //if (units == Units.CntCMS) { // 01/23/2006 aww
          if ( Units.isVelocity(units) ) {
            pgm = JBModel.getPeakVelocity(mag, z, dist);
            if (pgm > threshVel) {
              if (debug) System.out.println("+ "+str+ " peak vel = "+ pgm);
              return true;
            } else {
              if (debug) System.out.println("- "+str+ " peak vel = "+ pgm);
              return false;
            }
          //} else if (units == Units.CntCMSS) { // 01/23/2006 aww
          } else if ( Units.isAcceleration(units) ) {
            pgm = JBModel.getPeakAccel(mag, z, dist);
            if (pgm > threshAcc) {
              if (debug) System.out.println("+ "+str+ " peak acc = "+ pgm);
              return true;
            } else {
              if (debug) System.out.println("- "+str+ " peak acc = "+ pgm);
              return false;
            }
          } else {
            System.err.println("Invalid gain units: "+ch.getChannelId().toString()+" "+Units.getString(units));
            pgm = 0.0;  // will not include
          }

          return false;

    }    
    /** 
     * This method uses an empirically derived power law to calc a cutoff
     * distance
     * @param ch
     * @param mag
     * @param dist
     * @param z
     * @return
     */
    public boolean shouldShowEnergyByPowerLaw(Channel ch, double mag, double dist, double z) {

      double cutoff;
      
      // These values are from a powerlaw fit in Excel to observations of
      // how far out I could see energy on 6 events between 0.3 and 2.9
      // These values were further tweeked interactively in Excel.
      double C1_vel = 120.0;
      double C2_vel = 1.3;
      double C1_acc = 40.0;
      double C2_acc = 1.8;

      String str = ch.channelId.toDelimitedNameString(".")+ 
            "  dist = "+(int)ch.getHorizontalDistance();
 
      if ( ch.isVelocity() ) {
        cutoff = C1_vel * Math.pow(mag, C2_vel);
      } else if (ch.isAcceleration() ) {
        cutoff = C1_acc * Math.pow(mag, C2_acc);
      }  else {
         System.err.println(getModelName() + " Unknown inst type, not VEL or ACC: "+ch.getChannelId().toString());
         cutoff = 0.0; // don't include
      }
      
        if (cutoff > dist) {
          if (debug) System.out.println("+ "+str);
          return true;
        } else {
          if (debug) System.out.println("- "+str);
        }

      return false;

    }
    /*
This is a summary of the gains in the CI dbase on 7/18/03
CHL  MAX(GAIN)  MIN(GAIN)  AVG(GAIN)
--- ---------- ---------- ----------
EHE    2008452     142114   594499.5
EHN    2008452     142114   656688.4
EHZ  183000000     142114 16263318.2
ELE     142114      17891    67236.6
ELN     142114      17891 66951.6429
ELZ    1128852      17891 124606.765
HH1 83524621.1 83524621.1 83524621.1
HH2 81725873.6 81725873.6 81725873.6
HH3 77929841.4 77929841.4 77929841.4
HHE   10570000    3355400 6406805.22
HHN   10526320    3355400 6391397.89
HHZ   10779000    3355400 6438458.37
HL1   8569.612   2142.403 4780.17207
HL2   8569.612   2142.403 4749.21163
HL3   8569.612   2142.403 4749.21163
HL4   4284.805       4276 4280.26833
HL5   4284.805       4276 4280.26833
HL6   4284.805       4276 4280.26833
HLE   8569.612 -540.96902 3608.58303
HLN   8569.612 -539.61868 3608.43505
HLZ   8569.612 -543.23663 3608.45898

select seedchan, max(gain), min(gain), avg(gain) from simple_response
where offdate > SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) group by seedchan;
*/
    /**
     * Guess (make up) a gain for a channel with no gain. Uses "typical" value.
     * Values are based on the average gain for channels of each type in the
     * SCEDC dbase.
     * @param ch
     * @return
     */
    public ChannelGain getNominalGain(Channel ch) {

      ChannelGain chanGain = new ChannelGain();
      chanGain.setValue(0.0);

      chanGain.setUnits(Units.CntCMS);  // default units = counts/velocity

      String seedChan = ch.getSeedchan();

      // careful! Order matters for vertical/horizontal of EH_ & EL_
      if (seedChan.startsWith("HH"))        { chanGain.setValue(  6700000.0);
      } else if (seedChan.startsWith("BH")) { chanGain.setValue(  6700000.0);
      } else if (seedChan.startsWith("LH")) { chanGain.setValue(  6600000.0);
      } else if (seedChan.startsWith("HG")) { chanGain.setValue(     4100.0);

      } else if (seedChan.startsWith("SH")) { chanGain.setValue(  64271230.0);   // short period 50sps

      } else if (seedChan.startsWith("EHZ") )
                                            { chanGain.setValue( 18297642.0);   // short period
      } else if (seedChan.startsWith("EH")) { chanGain.setValue(   714695.0);   // " horizontals

      } else if (seedChan.startsWith("HL") ||
                 seedChan.startsWith("HN")) { chanGain.setValue(     4100.0);   // FBA's
                                              chanGain.setUnits(Units.CntCMSS); // acceleration

      } else if (seedChan.startsWith("ELZ")){ chanGain.setValue(   129000.0);   // short period
                                              chanGain.setUnits(Units.CntCMSS); // acceleration

      } else if (seedChan.startsWith("EL")) { chanGain.setValue(    68000.0);   // horizontals
                                              chanGain.setUnits(Units.CntCMSS); // acceleration
      }
      return chanGain;
    }

    /** Setup model behavior parameters using values in a property list.
     * Assumes each property has a prefix matching the string returned by getClassname().
     * Override this to set properties specific to a model.
     * In the new method call super.setProperties(props) first to get the standard stuff.<p>
     * <pre>
     * For example:
     * You have a class called org.trinet.jasi.JBChannelTimeWindowModel:
     *
     * Properties would define as follows:
     *
    <code>
    org.trinet.jasi.JBChannelTimeWindowModel.minDistance=20
    org.trinet.jasi.JBChannelTimeWindowModel.maxDistance=500
    org.trinet.jasi.JBChannelTimeWindowModel.minWindowSize=30
    org.trinet.jasi.JBChannelTimeWindowModel.maxWindowSize=300
    org.trinet.jasi.JBChannelTimeWindowModel.preEventSize=20
    org.trinet.jasi.JBChannelTimeWindowModel.postEventSize=30
    org.trinet.jasi.JBChannelTimeWindowModel.includeAllComponents=true
    org.trinet.jasi.JBChannelTimeWindowModel.includeAllMag=3.5
    org.trinet.jasi.JBChannelTimeWindowModel.maxChannels=9999
    org.trinet.jasi.JBChannelTimeWindowModel.noiseThreshold=25
    org.trinet.jasi.JBChannelTimeWindowModel.thresholdFactor=3.0
    </code>
    This method adds "noiseThreshold" to the property list.
     * */
    public void setProperties(GenericPropertyList props) {

      super.setProperties(props);   /// get all the standard stuff

      if (props != null) {
        String pre = getPropertyPrefix();
        if (props.isSpecified(pre+"noiseThreshold") )
          setThresholdCounts((int)props.getDouble(pre+"noiseThreshold"));

        if (props.isSpecified(pre+"thresholdFactor") )
          setThresholdFactor((int)props.getDouble(pre+"thresholdFactor"));

      }
      if (debug) System.out.println (getParameterDescriptionString());
    }

    /** Returns a human-readable list of the current parameter settings.
    Adds parameters specific to this model not found in the generic model.*/
    public String getParameterDescriptionString() {
        Format df81 = new Format("%8.1f");
        StringBuffer sb = new StringBuffer(512);
        
      sb.append( super.getParameterDescriptionString());
      
      String pre = getPropertyPrefix(); 
      sb.append(pre).append("noiseThreshold = ").append(getThresholdCounts()).append("\n");
      sb.append(pre).append("thresholdFactor = ").append(df81.form(getThresholdFactor())).append("\n");     

      return sb.toString();
    }

    public ArrayList getKnownPropertyKeys() {
      ArrayList list = super.getKnownPropertyKeys();
      String pre = getPropertyPrefix(); 
      list.add(pre + "noiseThreshold");
      list.add(pre + "thresholdFactor");
      return list;
    }

    public void setDefaultProperties() {
      super.setDefaultProperties();
      setMyDefaultProperties();
    }

    private void setMyDefaultProperties() {
      postEvent = defPostEvent;
      preEvent = defPreEvent;
      minWindow = defMinWindow;
      maxWindow = defMaxWindow;
      maxDistance = defMaxDistance;
      includeAllComponents = defIncludeAllComponents;
      includeAllMag = defIncludeAllMag;
      noiseCounts = defNoiseCounts;
      thresholdFactor = defThresholdFactor;
    }

/* ///////////////////////////////////////////////////////
//  public static final class Tester {
    public static void main(String args[]) {

      long evid;

//      if (args.length <= 0) {
//
//        System.out.println ("Usage: JBChannelTimeWindowModel [evid] ");
//        //System.exit(0);
//
//      } else {
//        Long val = Long.valueOf(args[0]);
//        evid = (long) val.longValue();
//      }
      // event ID

        //evid = 14119792  ;  // M =
//        evid = 10100053  ;  // 4.7 Parkfield -- should get ALL channels
        evid = 10100169; // 3.2
        System.out.println ("Test overriding command line EVID - evid = "+evid);

     // read props file to get waveserver filename
       //JiggleProperties props = new JiggleProperties("properties");
       JiggleProperties props = new JiggleProperties("JBmodel.props");

        //DataSource db = new DataSource();
        DataSource db = TestDataSource.create("serverma");

        System.out.println (db.toString());

        Solution sol    = Solution.create().getById(evid);

        if (sol == null) {
            System.out.println ("No such event: "+evid);
            System.exit(0);
        }

        System.out.println (sol.toString());

        // read channel list from cache
        String cacheFileName = props.getUserFileNameFromProperty("channelCacheFilename");
//        MasterChannelList.readOnlyChannelLatLonZ();
        MasterChannelList.set(ChannelList.readFromCache(cacheFileName));

        // Prevent loading of Mag infor until new tables get decided on - DDG 11/5/04
//        Channel.setLoadOnlyLatLonZ();
//        Channel.setLoadResponse(true);
//        Channel chnl = Channel.create();
//        // use the candidate list as the master list
//        ChannelableList candidateList = chnl.getCandidateChannelList();
//        MasterChannelList.set(candidateList);

        // Get a JB model setup with the args in the properties file
        ChannelTimeWindowModel model =
            props.getChannelTimeWindowModelList().getByClassname("org.trinet.jasi.JBChannelTimeWindowModel") ;

        if (model == null) {
          System.err.println ("Model could not be created.");
          System.exit(1);
        }
//        JBChannelTimeWindowModel model = new JBChannelTimeWindowModel();
        model.setProperties(props);
        model.setSolution(sol);

        //
        ArrayList ctwList = (ArrayList) model.getChannelTimeWindowList();

        //ctwList.toString();

//        if (ctwList != null ) {
//          for (int i = 0; i < ctwList.size(); i++) {
//            System.out.println( ((ChannelTimeWindow) ctwList.get(i)).toString() );
//          }
//        }
        System.out.println ("Channel count = "+ctwList.size());

    }
//  } // end of Tester
*/
}
