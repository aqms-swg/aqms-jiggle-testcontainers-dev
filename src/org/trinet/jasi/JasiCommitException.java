package org.trinet.jasi;
//public class JasiCommitException extends RuntimeException {

/** Exception thrown by a failed attempt to commit a JasiObject to a datasource.*/
public class JasiCommitException extends Exception {
    public JasiCommitException() {
        super();
    }
    public JasiCommitException(String message) {
        super(message);
    }
}       
