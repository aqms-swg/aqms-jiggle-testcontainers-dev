package org.trinet.jasi;
public interface JasiDataReaderIF {
    public void setFactoryClassName(String aClassName);
    public JasiObject parseData(Object data);
}

