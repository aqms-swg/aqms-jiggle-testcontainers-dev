package org.trinet.jasi;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.*;
import org.trinet.jdbc.datasources.*;
import org.trinet.jdbc.table.ExecuteSQL;
import org.trinet.jiggle.common.DbConstant;
import org.trinet.jiggle.common.LogUtil;

import oracle.jdbc.OraclePreparedStatement;

/**
 * Jasi API application properties including database (DataSource) connection properties.
 * @see: GenericPropertyList
 * @see: JasiPropertyList
*/

public class JasiDatabasePropertyList extends JasiPropertyList {
    /** Level for debug when debugSQL and debugTN are not set */
    private static final String DEBUG_LEVEL = "DEBUGDB";
    /** Level for debugSQL */
    private static final String DEBUGSQL_LEVEL = "DEBUGSQL";
    /** Level for debugCommit (debugTN) */
    private static final String DEBUGTN_LEVEL = "DEBUGCOMMIT";
    /** Null text */
    private static final String NULL_TEXT = "('null',";
    // global signal for db "reader" classes to print out SQL strings
    public static boolean debugSQL = false; // aww 02/24/2005
    // Implemented toggling of debugSQL/debugTN in TN subclasses dynamically from Jiggle preferences -aww 2008/11/13
    public static boolean debugTN = false; // aww 2008/11/13

    /**
     * Debug the statement if the debug SQL flag is set.
     * @param text the text or null if none.
     * @param sm the statement or null if none.
     */
    public static void debugSQL(String text, Statement sm) {
      log(text, DEBUGSQL_LEVEL, sm, debugSQL);
    }

    /**
     * Debug the statement if the debug SQL flag or the specified flag is set.
     * @param text the text or null if none.
     * @param sm the statement or null if none.
     * @param debug true to debug, false otherwise.
     */
    public static void debugSQL(String text, Statement sm, boolean debug) {
      if (debug && debugTN) {
        log(text, DEBUGTN_LEVEL, sm, true);
      } else if (debugSQL) {
        log(text, DEBUGSQL_LEVEL, sm, true);
      } else if (debug) {
        log(text, DEBUG_LEVEL, sm, true);
      }
    }

    /**
     * Debug the statement if the debug TN flag is set.
     * @param text the text or null if none.
     * @param sm the statement or null if none.
     */
    public static void debugTN(String text, Statement sm) {
      log(text, DEBUGTN_LEVEL, sm, debugTN);
    }
    
    /**
     * Log the statement if the specified flag is set.
     * @param text the text or null if none.
     * @param level the level.
     * @param sm the statement or null if none.
     * @param flag true to log, false otherwise.
     */
    public static void log(String text, String level, Statement sm, boolean flag) {
      if (!flag) {
        return;
      }
      String msg = text;
      String check = null;
      try {
        if (sm != null && DbConstant.isPostgreSQL(sm.getConnection())) {
          msg = sm.toString();
          check = NULL_TEXT;
        }
      } catch (Exception ex) {        
      }
      if (msg == null) { // exit if no message
        return;
      }
      // if this is a callable statement
      if (sm instanceof CallableStatement) {
        final StringBuilder sb = new StringBuilder();
        int start = -1;
        int end = -1;
        if (check != null) {
          // check for text
          start = msg.indexOf(check);
        }
        if (start != -1) { // if null text found
          // remove the null text
          end = start + check.length();
          sb.append(msg, 0, start + 1); // include the '('
          sb.append(msg, end, msg.length());
        } else {
          sb.append(msg);
        }
        try {
          Object result = ((CallableStatement) sm).getObject(1);
          if (result != null) { // if there is a result
            // add text for the result
            sb.append(" (");
            sb.append(result);
            sb.append(')');
          }
        } catch (Exception ex) {
        }
        msg = sb.toString();
      }
      LogUtil.log(level, msg);
    }

/**
 *  Constructor: no-op.
 *  @see: setRequiredProperties()
 */
    public JasiDatabasePropertyList() {
      // force programmer to setup defaults -aww?
    }

/**
 *  Constructor: Makes a COPY of a property list. Doesn't read the files.
 */
    //public JasiDatabasePropertyList(GenericPropertyList props) {
    public JasiDatabasePropertyList(JasiDatabasePropertyList props) {
        super(props);
        privateSetup();
    }

/**
 *  Construtor: reads this property file and System properties
 */
    public JasiDatabasePropertyList(String fileName)
    {
        super(fileName);
        privateSetup();
    }

/**
*  Constructor: input filenames are not appended to constructed paths, but rather are
*  interpreted as specified (either relative to current working directory or a complete path). 
*/
    public JasiDatabasePropertyList(String userPropFileName, String defaultPropFileName)
    {
        super(userPropFileName, defaultPropFileName);
        privateSetup();
    }

    public boolean setupDataSource() {
      // parse dbase properties and sets static DataSource
      DbaseConnectionDescription dcd = getDbaseDescription();
      if (! dcd.isValid() ) return false;

      if (DataSource.isNull()) {
          new DataSource().set(dcd); // was new DataSource(dcd) -aww 2008/11/19
      }
      else {
          DataSource.close(); // close connection and create new one. - aww 2007/01/11
          DataSource.set(dcd);
      }

      if (getProperty("dbWriteBackEnabled") != null) 
        DataSource.setWriteBackEnabled(getBoolean("dbWriteBackEnabled"));

      return true;

    }

    /** Returns true if the DataSource properties of the passed JasiDatabasePropertyList
     *  object are the same from this one. */
    public boolean dataSourceEquals(JasiDatabasePropertyList otherProps) {

        if (
            !getProperty("dbaseHost"  ,"").equals(otherProps.getProperty("dbaseHost","") ) ||
            !getProperty("dbaseDomain","").equals(otherProps.getProperty("dbaseDomain","") ) ||
            !getProperty("dbaseName"  ,"").equals(otherProps.getProperty("dbaseName","") ) ||
            !getProperty("dbasePort"  ,"").equals(otherProps.getProperty("dbasePort","") ) ||
            !getProperty("dbaseDriver","").equals(otherProps.getProperty("dbaseDriver","") ) ||
            !getProperty("dbaseUser"  ,"").equals(otherProps.getProperty("dbaseUser","") ) ||
            !getProperty("dbasePasswd","").equals(otherProps.getProperty("dbasePasswd","") )
          ) return false;
        return true;
   }

    /**
     * Setup class static, instance, and/or environment variables from current properties.
     * This method should be called after the pertinent properties are initially read
     * or changed through updates.
     */
    public boolean setup() {
      boolean status = super.setup();
      return (privateSetup() && status);
    }

    private boolean privateSetup() {
      if (isSpecified("debugSQL")) debugSQL = getBoolean("debugSQL"); // aww 02/24/2005
      ExecuteSQL.setPrintStringSQL(debugSQL); // SQL string doing commit update/insert to db -aww 2008/02/29

      if (isSpecified("debugCommit")) debugTN = getBoolean("debugCommit"); // aww 2008/11/13

      // Java code revised to use UTC true seconds internally for data time values.
      // Internally java code now using "true epoch" with leap seconds.
      // As of 2008/02/11 Time values are stored as "nominal" secs at SCEDC and "true" at NCEDC.
      // reads and commit data using TrueTime stored package to determine time conversion, thus 
      // removed block below -aww 2008/02/11
      /*
      String str = getProperty("dbTimeBase", "");
      if (str.equalsIgnoreCase(DataSource.LEAP)) {
         DataSource.setDbTimeBaseLeap();
      } else if (str.equalsIgnoreCase(DataSource.NOMINAL)) { 
         DataSource.setDbTimeBaseNominal();
      }
      */
      if (getBoolean("dbAttributionEnabled")) {
          JasiDbReader.dbAttributionEnabled = true;
      }
      //
      return true;
    }

    public List getKnownPropertyNames() {
        List aList = super.getKnownPropertyNames();
        List myList = classDeclaredPropertyNames();
        if (myList instanceof ArrayList)
            ((ArrayList)myList).ensureCapacity(aList.size() + myList.size());
        aList.addAll(myList);
        Collections.sort(aList);
        return aList;
    }

    public static List classDeclaredPropertyNames() {
        ArrayList myList = new ArrayList(16);

        myList.add("dbaseHost");
        myList.add("dbaseDomain");
        myList.add("dbaseName");
        myList.add("dbasePort");
        myList.add("dbaseDriver");
        myList.add("dbaseUser");
        myList.add("dbasePasswd");
        myList.add("dbaseSubprotocol");
        //myList.add("dbaseRoleName");
        myList.add("dbLastLoginURL");
        //myList.add("dbTimeBase"); // removed -aww 2008/02/11
        myList.add("dbAttributionEnabled");
        myList.add("dbWriteBackEnabled");
        myList.add("debugSQL");
        myList.add("debugCommit");

        Collections.sort(myList);

        return myList;
    }

    /*
    // removed block below -aww 2008/02/11
    public void setDbTimeBaseLeap() {
        setProperty("dbTimeBase", DataSource.LEAP);
    }
    public void setDbTimeBaseNominal() {
        setProperty("dbTimeBase", DataSource.NOMINAL);
    }
    public String getDbTimeBase() {
        return getProperty("dbTimeBase");
    }
    */

    public void setDbWriteBackEnabled(boolean tf) {
        setProperty("dbWriteBackEnabled", tf);
    }
    public String getDbWriteBackEnabled() {
        return getProperty("dbWriteBackEnabled");
    }

    public void setDbaseUser(String name) {
        setProperty("dbaseUser", name);
    }

    public void setDbasePasswd(String passwd) {
        setProperty("dbasePasswd", passwd);
    }

    public void setDbaseURL(String url) {
      setProperty("dbaseSubprotocol", DbaseConnectionDescription.parseSubprotocolFromURL(url));
      setProperty("dbaseHost", DbaseConnectionDescription.parseHostNameFromURL(url));
      setProperty("dbaseDomain", DbaseConnectionDescription.parseDomainNameFromURL(url));
      setProperty("dbasePort", DbaseConnectionDescription.parsePortFromURL(url));
      setProperty("dbaseName", DbaseConnectionDescription.parseDbaseNameFromURL(url));
    }

    public void setDbaseDriver(String driver) {
        setProperty("dbaseDriver", driver);
    }

    /*
    public void setDbaseHost(String host) {
        setProperty("dbaseHost", host);
    }
    public void setDbaseDomain(String domain) {
        setProperty("dbaseDomain", domain);
    }
    public void setDbaseName(String name) {
        setProperty("dbaseName", name);
    }
    public void setDbasePort(String port) {
        setProperty("dbasePort", port);
    }
    public void setDbaseDriver(String driver) {
        setProperty("dbaseDriver", driver);
    }
    */

  /*
    //For use in user/entity-creation tracking in ancillary db tables ?
    public String getDbaseRoleName() {
      String dbaseRoleName = 
          getProperty("dbaseRoleName", System.getProperty("DATABASE_USER_ROLE", ""));
      if ( dbaseRoleName.equals("") )
          dbaseRoleName = System.getProperty("dbaseUser", "");
      if ( ! dbaseRoleName.equals("") ) setProperty("dbaseRoleName", dbaseRoleName);
      return dbaseRoleName;
    }
  */

    /** Return the fully specified name of the JDBC URL for the dbase. It is composed
     * three properties: dbaseHost, dbasePort and dbaseName. It has a form like:
     * jdbc:oracle:thin:@serverq.gps.caltech.edu:1521:databaseq*/
    public String getDbaseURL() {
         return getDbaseDescription().getURL();
    }

    /** Property describes a previous user login to database. */
    public String getDbLastLoginURL() {
         return getProperty("dbLastLoginURL");
    }

    /** Set a URL describing user's login to database.*/
    public void setDbLastLoginURL(String url) {
         setProperty("dbLastLoginURL", url);
    }

    /** Return the DbaseConnectionDescription object described by this property list. */
    public DbaseConnectionDescription getDbaseDescription() {
        return new DbaseConnectionDescription(this);
    }

    /** Enable code to attribute table data to its creator's alias id. */
    public void setAttributionEnabled(boolean tf) {
        setProperty("dbAttributionEnabled", String.valueOf(tf));
    }

    /** Does code attribute table data to its creator's alias identifier. */
    public boolean isAttributionEnabled() {
        return getBoolean("dbAttributionEnabled");
    }

    /*
    // BEWARE:
    // when multiple property files are used, default required init's may cause
    // problems when these properties are missing from any one of the prop files.
    public void setRequiredProperties() {
        super.setRequiredProperties();
        if (! DataSource.isNull() ) {
          // use those values set for current source
          setProperty("dbWriteBackEnabled", DataSource.isWriteBackEnabled());
          setProperty("dbTimeBase", DataSource.getDbTimeBaseString());
        }
    }
    */
} // end of class
