package org.trinet.jasi;
/**
 * Interface that contains the names of all schema types supported by jasi.
 * All jasi objects (abstract JasiObject) implement this interface to control
 * their factory methods. These values are for convenience. It is possible to
 * instantiat other concrete jasi implementation classes by passing the correct
 * suffix to the JasiObject.create() method. <p>
 *
 * Currently defined concrete classes:<p>
<table BORDER COLS=1 WIDTH="100%" >
<tr>
  <td>typeString</td> <td>suffix</td> <td>Comment</td>
</tr>
<tr>
  <td>TRINET</td> <td>TN</td> <td>TriNet NCDC schema</td>
</tr>
<tr>
  <td>EARTHWORM</td> <td>EW</td> <td>Earthworm schema</td>
</tr>
  <td>BAP</td> <td>BP</td> <td>Bap USGS schema</td>
</tr>
</table>
 *
 * @see: org.trinet.jasi.JasiObject
 */
public interface JasiFactory  {

    // Supported schema types and aliases

    /** Code for the TriNet (NCEDC v1.5) schema support jasi classes */
    public static final int TRINET     = 0;

    /** Code for the Earthworm schema support jasi classes */
    public static final int EARTHWORM  = 1;

    /** Code for the BAP schema support jasi classes */
    public static final int BAP  = 2;

    // Additional schemas would be added here

    /** String describing TRINET schema type */
    public static final String TYPE_TRINET = "TRINET";

    /** String describing EARTHWORM schema type */
    public static final String TYPE_EARTHWORM = "EARTHWORM";

    /** String describing BAP schema type */
    public static final String TYPE_BAP = "BAP";

    /**
     * String describing PNSN schema type for backwards compatibility, replaced by
     * TRINET
     * 
     * @deprecated Use {@link #TYPE_TRINET} instead
     */
    public static final String TYPE_PNSN = "PNSN";

    /** Strings describing schema types. "TRINET", "EARTHWORM", "BAP" */
    public static final String[] typeString = {TYPE_TRINET, TYPE_EARTHWORM, TYPE_BAP};

    /** Strings used as suffixes for concrete class names. "TN", "EW" */
    public static final String[] suffix = {"TN", "EW", "BP"};

    /** Map string schema names to int descriptions */
    public static final int[] typeValue = {TRINET, EARTHWORM, BAP};

    /**
     * Get the data type, possibly replacing deprecated data types with the updated
     * data type.
     * 
     * @param dataType the data type.
     * @return the data type.
     */
    public static String getDataType(String dataType) {
      if (TYPE_PNSN.equalsIgnoreCase(dataType)) {
        dataType = TYPE_TRINET;
      }
      return dataType;
    }
    
    /**
     * Get the type value.
     * 
     * @param dataType the data type.
     * @return the type value or -1 if none.
     */
    public static int getTypeValue(String dataType) {
      dataType = getDataType(dataType);
      for (int i = 0; i < typeString.length; i++) {
        if (dataType.equalsIgnoreCase(typeString[i])) {
          return typeValue[i];
        }
      }
      return -1;
    }
}
