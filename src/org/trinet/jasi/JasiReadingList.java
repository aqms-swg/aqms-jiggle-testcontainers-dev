//TODO:
//  matching problem occurs when database rows with different row key ids
//  have duplicate channel, type, etc descriptions.
//  Possibly use static member in AbstractCommitableList, to override the
//  default addOrReplace allowing addOrReplaceById when matchMode =MATCH_ID ?
//
//  Doesn't effect addOrReplace BySameChan, ByLikeChan, with/without time.
//
//TODO: convert argument references from JasiReading to IF where appropiate
//
//TODO: Perhaps some of the methods implemented here should be migrated over to
//ChannelableList class shared with the JasiReadingListIF and ChannelableListIF
//if methods are static in other class, then delegate to those static equivalents in this class?
//
package org.trinet.jasi;
import javax.swing.event.*;
import java.util.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;

/**
 * This class extends AbstractSolutionAssocList in order to support the special
 * methods of JasiReadingIF objects.
 * <p>
 *
 * @See: AbstractSolutionAssocList
 * @See: ActiveArrayList
 */

public abstract class JasiReadingList extends AbstractSolutionAssocList
    implements ChannelableListIF, JasiReadingListIF {
    /** The master ChannelList. If set, any reading added to this list will be
    * matched to this list and its Channel object replaced by a reference to
    * the matching Channel object in this list. If no match is found, the
    * original Channel object is retained. This is done so that the readings
    * get the full channel description and the distance. */
    protected ChannelList chanList; // no public set method - aww

    public JasiReadingList() { }

    public JasiReadingList(int capacity) { super(capacity); }

    public JasiReadingList(Collection col) {
        addAll(col, false);  // since it's new no need to notify, no listener
    }

    /** Return a List of readings where isAuto() is true . */
    public JasiReadingListIF getAutomaticReadings() {
        int count = size();
        JasiReadingList jrl = (JasiReadingList) newInstance();
        if (count == 0) return jrl;
        jrl.ensureCapacity(count);
        JasiReading jr = null;
        for (int i = 0; i<count; i++) {
          jr = (JasiReading) get(i);
          if (jr.isAuto()) {
            jrl.add(jr);
          }
        }
        return jrl;
    }

    public int eraseAllAutomatic() {
        JasiReading jr = null;
        int count = 0;
        //Perhaps instead better to use the erase(jr) method to look up correct list idx, as list gets resized
        for (int idx=size()-1; idx>=0; idx--) {
            jr  = (JasiReading) get(idx);
            if (jr.isAuto()) {
                erase(idx);
                count++;
            }
        }

        return count;
    }

  /**
  * Reject all readings whose residual >= value.
  * Regardless of association.
  * Does not remove them from list.
  */
  public int rejectByResidual(double value) {
    return rejectByResidual(value, null);
  }

  /**
  * Reject all readings associated with input Solution
  * whose residual >= value. If input Solution is null, 
  * all elements satifying condition are deleted.
  * Does not remove them from list.
  */
  public int rejectByResidual(double value, Solution sol) {
    int mySize = size();
    int knt = 0;
    for (int i = 0; i < mySize; i++) {
      JasiReadingIF jc = (JasiReadingIF) get(i);
      if (sol == null || jc.isAssignedTo(sol)) {
        if ((Math.abs(jc.getResidual()) >= value)) {
          jc.setReject(true);
          knt++;
        }
      }
    }
    return knt;
  }

  /**
  * Reject all readings whose distance > value (km). 
  * Regardless of association.
  * Does not remove them from list.
  */
  public int rejectByDistance(double value) {
    return rejectByDistance(value, null);
  }

  /**
  * Reject all readings associated with input Solution
  * whose distance > value (km). If input Solution is null, 
  * all elements satifying condition are deleted.
  * Does not remove them from list.
  */
  public int rejectByDistance(double value, Solution sol) {
    int knt = 0;
    int mySize = size();
    for (int i = 0; i < mySize; i++) {
      JasiReadingIF jc = (JasiReadingIF) get(i);
      if (sol == null || jc.isAssignedTo(sol)) {
        if (jc.getHorizontalDistance() >= value) {
          jc.setReject(true);
          knt++;
        }
      }
    }
    return knt;
  }

  /**
  * Unreject all readings in list.  Does not remove them from list.
  */
  public int rejectAll() {
      return rejectAll(null);
  }

  /**
  * Unreject all readings in list associated with input Solution (all if null).  Does not remove them from list.
  */
  public int rejectAll(Solution sol) {
    int mySize = size();
    int knt = 0;
    for (int i = 0; i < mySize; i++) {
      JasiReadingIF jc = (JasiReadingIF) get(i);
      if (sol == null || jc.isAssignedTo(sol)) {
        if (!jc.isReject()) {
            jc.setReject(true);
            knt++;
        }
      }
    }
    return knt; // number rejected by method
  }

  /**
  * Unreject all readings in list.  Does not remove them from list.
  */
  public int unrejectAll() {
      return unrejectAll(null);
  }

  /**
  * Unreject all readings in list associated with input Solution (all if null).  Does not remove them from list.
  */
  public int unrejectAll(Solution sol) {
    int mySize = size();
    int knt = 0;
    for (int i = 0; i < mySize; i++) {
      JasiReadingIF jc = (JasiReadingIF) get(i);
      if (sol == null || jc.isAssignedTo(sol)) {
        if (jc.isReject()) {
            jc.setReject(false);
            knt++;
        }
      }
    }
    return knt; // number unrejected by method
  }

    /** Return true if List contains at least one contributing reading that matches
     * the net and station description of the input.
     * @see JasiReading.contributes()
     * */ 
    public boolean stationContributes(Channelable ch) {
        boolean status = false;
        int mySize = size();
        JasiReading jr = null;
        for (int i = 0; i<mySize; i++) {
           jr = (JasiReading) get(i); 
           if ( jr.sameStationAs(ch) &&
                   ( ! jr.isDeleted() && jr.contributes() ) ) {
               status = true;
               break;
           }
        }
        return status;
    }

    /** Return true if List contains at least one entry that matches net and station description of the input. */ 
    public boolean hasEntryForStation(ChannelIdentifiable ch) {
        boolean status = false;
        int mySize = size();
        for (int i = 0; i<mySize; i++) {
           if (((JasiReadingIF) get(i)).getChannelObj().sameStationAs(ch)) {
               status = true;
               break;
           }
        }
        return status;
    }

    /** Channels in list, interpretive wrapper for size().
     * @see #getStationCount()
     * */
    public int getChannelCount() {
//      return size();
      return ChannelableList.getSeedChanCount(this);
    }
    /** Return a count of how many readings in this list
     * were used in a summary result calculation (Solution, Magnitude).
     * Only counts readings not flagged as "deleted" whose weight > 0.
     * Since a "station" (a unique "net", "sta" signature) can
     * have multiple channels, the returned count could be greater
     * than that returned by getStationCount().
     * @see #getStationUsedCount()
     * */
    public int getChannelUsedCount() {
         if (isEmpty()) return 0;
         int count = 0;
         int mySize = size();
         JasiReadingIF jr = null;
         for (int i = 0; i<mySize; i++) {
           jr = (JasiReadingIF) get(i);
           if ( ! jr.isDeleted() && jr.contributes() ) count++;
         }
         return count;
    }


    /** Count of those contributing (weight &gt;0.) with input type, or for null type all contributing.*/
    public int getChannelUsedCount(String type) {
         if (isEmpty()) return 0;
         int count = 0;
         int mySize = size();
         JasiReadingIF jr = null;
         for (int i = 0; i<mySize; i++) {
           jr = (JasiReadingIF) get(i);
           if ( !jr.isDeleted() && jr.contributes() ) {
               if ((type == null) || type.equals(jr.getTypeQualifier())) count++;
           }
         }
         return count;
    }

    public int getChannelAutoUsedCount() {
         if (isEmpty()) return 0;
         int count = 0;
         int mySize = size();
         JasiReadingIF jr = null;
         for (int i = 0; i<mySize; i++) {
           jr = (JasiReadingIF) get(i);
           if ( !jr.isDeleted() && jr.contributes() && jr.isAuto() ) count++;
         }
         return count;
    }

    public int getChannelWithInWgtCount() {
         if (isEmpty()) return 0;
         int count = 0;
         int mySize = size();
         JasiReadingIF jr = null;
         for (int i = 0; i<mySize; i++) {
           jr = (JasiReadingIF) get(i);
           if ( !jr.isDeleted() && jr.getInWgt() > 0.) count++;
         }
         return count;
    }

    public static int getChannelWithInWgtCount(List aList) {
         if (aList.isEmpty()) return 0;
         int count = 0;
         int mySize = aList.size();
         JasiReadingIF jr = null;
         for (int i = 0; i<mySize; i++) {
           jr = (JasiReadingIF) aList.get(i);
           if ( !jr.isDeleted() && jr.getInWgt() > 0.) count++;
         }
         return count;
    }

    /** Returns count of unique network codes in the list.
    /** Returns count of unique network codes in the list.
     * Station uniqueness is qualified as a distinct "net" string.
     *  A network may have multiple stations & channels.
    * @see #getChannelCount()
    * @see #getChannelUsedCount()
    * */
    public int getNetworkCount() {
      return ChannelableList.getNetworkCount(this);
    }
    /** Returns count of unique stations in the list.
     * Station uniqueness is qualified as a distinct "net",
     * "sta" signature. A station may have multiple channels.
    * @see #getChannelCount()
    * @see #getChannelUsedCount()
    * */
    public int getStationCount() {
      return ChannelableList.getStationCount(this);
    }

    /** Number of 3-components stations in list (contributing or not). */
    public int getTriaxialCount() {
      return ChannelableList.getTriaxialCount(this);
    }

    /** Returns a count of unique "used" stations in the list.
     * Station uniqueness is qualified as a distinct "net",
     * "sta" signature.
     * A "used" station must have at least one "undeleted" channel
     * that has contributed to the summary result (weight > 0).
     * @see #getChannelUsedCount()
     * */
    // Does not require that the list be ordered in any particular way.
    public int getStationUsedCount() {
        if (isEmpty()) return 0;
        //int mySize = size(); // weird out-of-bounds from thread list size changes? -aww 06/08/07
        int count = size();
        // for each entry, scan forward in list & only count if no repeats ahead
        for (int i = 0; i < size(); i++) {
          JasiReadingIF jr = (JasiReadingIF) get(i);
          if (jr.isDeleted() || ! jr.contributes() ) {
              count--;
          } else {
            for (int k = i+1; k < size(); k++) {
              JasiReadingIF jr2 = (JasiReadingIF) get(k);
              if ( jr.sameStationAs(jr2) &&
                   ( ! jr2.isDeleted() && jr2.contributes()) ) {
                count--;
                break;
              }
            }
          }
        }
        return count;
    }

    public int getReadingCount(boolean deletedOK, boolean rejectedOK) {
      int mySize = size();
      int cnt = 0;
      JasiReading jr = null;
      for (int i = 0; i<mySize; i++) {
          jr = (JasiReading) get(i);
          if ( ! deletedOK && jr.isDeleted() ) continue;
          if ( ! rejectedOK && jr.isReject() ) continue;
          cnt++;
      }
      return cnt;
    }
 
    /**
    * Return a List of readings that have contributed to a summary result statistic.
    * Undeleted readings that are not zero-weighted. No association check is done.
    */
    public JasiReadingListIF getContributingReadings() { // getNonZeroWeightedReadings()  -aww
        int count = size();
        JasiReadingList jrl = (JasiReadingList) newInstance();
        if (count == 0) return jrl;
        jrl.ensureCapacity(count);
        JasiReading jr = null;
        for (int i = 0; i<count; i++) {
          jr = (JasiReading) get(i);
          if (jr.contributes() && ! jr.isDeleted() ) {
            jrl.add(jr);
          }
        }
        return jrl;
    }

    /** Returns a subset of this list containing the JasiReadings that are from the
     * given Channel, regardless of virtual deletion state.
     * */
    public ChannelableListIF getAssociatedByChannel(Channel chan) {
      JasiReadingList newList = (JasiReadingList) newInstance();
      if (newList == null) return null;
      int mySize = size();
      for (int i = 0; i<mySize; i++) {
        Channelable ch = (Channelable) get(i);
        if ( ch.equalsChannelId(chan) ) {
          newList.add(ch);
        }
      }
      return newList;
    }

    /** Uses MasterChannelList data (if protected member chanList,
     * is is null). Does a no-op if ChannelList is null or empty.
     * Replaces the Channel references of all readings in the list with
     * the equivalent Channel match from the master ChannelList list.
     * This is used to replace partial Channel descriptions with possibly
     * more complete descriptions such as lat/lon/z, response, corrections,
     * etc.
     * */
    public int matchChannelsWithList() {
     // need a public set method for chanList, else MasterList by default ? // aww
     if (chanList == null) chanList = MasterChannelList.get(); // do we want to do this aww ?
     return (chanList != null) ? matchChannelsWithList(chanList) : 0;
    }
    /**
     * Replaces the Channel references of all readings in the list with
     * the equivalent Channel match from the specified input ChannelList.
     * This is used to replace partial Channel descriptions with possibly
     * more complete descriptions such as lat/lon/z, response, corrections,
     * etc.
     */
    public int matchChannelsWithList(ChannelList channelList) {
        int mySize = size();
        int matchCount = 0;
        for (int i = 0; i<mySize; i++) {
          if (((JasiReadingIF) get(i)).setChannelObjFromList(channelList)) matchCount++;
        }
        return matchCount;
    }

// The 4 methods below added by aww - 01/07/2005
/**
 * Returns instance of this type containing all elements of this list
 * found to be equivalent to the input argument by JasiReading.isSameChanType(jc) test.
 * Match multiples occur when elements in list have different identifiers,
 * time, or other data, but their Channel and "type" are equivalent.
 * For example, sets of Amps or Coda derived from waveforms using the same
 * preferred origin for separate magnitude calculations.
 * @return null error instantiating new instance of this list type.
 * @return empty list if no equivalent element is found in this list.
 * @see JasiReading.isSameChanType(JasiReadingIF)
 */
  public JasiReadingListIF getAllOfSameChanType(JasiReadingIF jr) {
      JasiReadingListIF newList = (JasiReadingListIF) newInstance();
      if (newList == null) return null; // error abort
      if (jr == null) return newList; // no data, noop

      int mySize = size();
      JasiReadingIF myjr = null;
      // snapshot, scan this list adding matches to newList
      for (int idx=0; idx < mySize; idx++) {
        myjr = (JasiReadingIF) get(idx);
        if ( myjr.isSameChanType(jr) ) newList.add(myjr);
      }
      return newList; // normally newList size is 0 or 1.
  }

/**
 * Removes from this instance all elements found to match the Channel
 * and reading type of the input argument.
 * Match multiples occur when elements in list have different identifiers,
 * time, or other data, but their Channel and "type" are equivalent.
 * For example, sets of Amps or Coda derived from waveforms using the same
 * preferred origin for separate magnitude calculations.
 * @return count of elements removed, -1 if error.
 * @see #getAllOfSameChanType(JasiReadingIF)
 * @see JasiReading.isSameChanType(JasiReadingIF)
 */
  public int removeAllOfSameChanType(JasiReadingIF jr) {
      if (jr == null) return 0;
      JasiCommitableListIF aList = getAllOfSameChanType(jr);
      if (aList == null) return -1;
      if (aList.isEmpty()) return 0;
      return removeAll((List)aList);
  }

/**
 * Removes from this instance all elements found to match the Channel
 * and reading type of the input argument, then adds the input reading.
 * Does a noop if input is null.
 * @see #removeAllOfSameChanType(JasiReadingIF)
 */
  public void addOrReplaceAllOfSameChanType(JasiReadingIF jr) { // modified to invoke below 2010/09/27 -aww
      addOrReplaceAllOfSameChanType(jr, false);
  }
  public void addOrReplaceAllOfSameChanType(JasiReadingIF jr, boolean autoOnly) { // added 2010/09/27 -aww
      if (jr == null) return;
      // if input is AUTO and like exists that is NOT AUTO don't replace it, a no-op
      ArrayList aList = new ArrayList(1);
      aList.add(jr);
      addOrReplaceAllOfSameChanType(aList, autoOnly);
  }

/**
 * Adds or replaces all list elements found to match the Channel
 * and reading type of the input list elements.
 * If input list has multiples of the same channel and type matching
 * an element in this list, then matching input element with greatest
 * index should be the only one found in this list upon method return.
 * Does a noop if input is null or empty list. For example, if an input
 * List has Coda sorted by ascending coid, the Coda with the greatest
 * identifier id should be those remaing in the this list.
 * @see addOrReplaceAllOfSameChanType(JasiReadingIF)
 */
  public void addOrReplaceAllOfSameChanType(List aList) {   // changed to invoke below 2010/09/27 -aww
      addOrReplaceAllOfSameChanType(aList, false); 
  }
  public void addOrReplaceAllOfSameChanType(List aList, boolean onlyAuto) { // added 2010/09/27 -aww

      if (aList == null || aList.isEmpty()) return;

      int inSize = aList.size();
      JasiReadingIF jrNew = null;

      if (! onlyAuto) {
          for (int idx=0; idx < inSize; idx++) { //Loop over input list
              jrNew = (JasiReadingIF) aList.get(idx);
              removeAllOfSameChanType(jrNew); // remove all matching readings from this list
              add(jrNew); // append new reading to end of this list
          }
          return;
      }
      else { // check for any existing readings of same type that are NOT auto, if any are found, no add/replace, a no-op
          JasiReadingListIF oldList = null;
          for (int idx=0; idx < inSize; idx++) { //Loop over input list
              jrNew = (JasiReadingIF) aList.get(idx);
              oldList = (JasiReadingListIF) getAllOfSameStaType(jrNew);
              if (oldList.size() > 0) { // found a match 
                  JasiReadingIF jrOld = null;
                  for (int jdx=0; jdx < oldList.size(); jdx++) { // loop over matches
                    jrOld = (JasiReadingIF) oldList.get(jdx);
                    if (jrNew.isAuto() && onlyAuto && !jrOld.isAuto()) continue; // skip removal/add when existing reading is not auto
                    remove(jrOld);
                    oldList.remove(jrOld);
                  }
                  // oldList should be empty if all are AUTO or onlyAuto==false
              }
              if (oldList.size() == 0) add(jrNew); // add ok, no human/final
          }
      }
  }

// The 4 methods below added by aww - 06/22/2006
/**
 * Returns instance of this type containing all elements of this list
 * found to be equivalent to the input argument by JasiReading.isSameStaType(jc) test.
 * Match multiples occur when elements in list have different identifiers,
 * seedchan, time, or other data, but their Net, Station and "type" are equivalent.
 * For example, sets of Amps or Coda derived from waveforms using the same
 * preferred origin for separate magnitude calculations.
 * @return null error instantiating new instance of this list type.
 * @return empty list if no equivalent element is found in this list.
 * @see JasiReading.isSameStaType(JasiReadingIF)
 */
  public JasiReadingListIF getAllOfSameStaType(JasiReadingIF jr) {
      JasiReadingListIF newList = (JasiReadingListIF) newInstance();
      if (newList == null) return null; // error abort
      if (jr == null) return newList; // no data, noop

      int mySize = size();
      JasiReadingIF myjr = null;
      // snapshot, scan this list adding matches to newList
      for (int idx=0; idx < mySize; idx++) {
        myjr = (JasiReadingIF) get(idx);
        if ( myjr.isSameStaType(jr) ) newList.add(myjr);
      }
      return newList; // normally newList size is 0 or 1.
  }

/**
 * Removes from this instance all elements found to match the Net, Station
 * and reading type of the input argument.
 * Match multiples occur when elements in list have different identifiers,
 * seedchan, time, or other data, but their Net, Station and "type" are equivalent.
 * For example, sets of Amps or Coda derived from waveforms using the same
 * preferred origin for separate magnitude calculations.
 * @return count of elements removed, -1 if error.
 * @see #getAllOfSameStaType(JasiReadingIF)
 * @see JasiReading.isSameStaType(JasiReadingIF)
 */
  public int removeAllOfSameStaType(JasiReadingIF jr) {
      if (jr == null) return 0;
      JasiCommitableListIF aList = getAllOfSameStaType(jr);
      if (aList == null) return -1;
      if (aList.isEmpty()) return 0;
      return removeAll((List)aList);
  }

/**
 * Removes from this instance all elements found to match the Net, Station
 * and reading type of the input argument, then adds the input reading.
 * Does a noop if input is null.
 * @see #removeAllOfSameStaType(JasiReadingIF)
 */
  public void addOrReplaceAllOfSameStaType(JasiReadingIF jr) {
      addOrReplaceAllOfSameStaType(jr, false);  // changed to invoke below 2010/09/27 -aww
  }
  public void addOrReplaceAllOfSameStaType(JasiReadingIF jr, boolean autoOnly) { // added 2010/09/27 -aww
      if (jr == null) return;
      // if input is AUTO and like exists that is NOT AUTO don't replace it, a no-op
      ArrayList aList = new ArrayList(1);
      aList.add(jr);
      addOrReplaceAllOfSameStaType(aList, autoOnly);
  }

/**
 * Adds or replaces all list elements found to match the Net, Station
 * and reading type of the input list elements.
 * If input list has multiples of the same Net, Station and type matching
 * an element in this list, then matching input element with greatest
 * index should be the only one found in this list upon method return.
 * Does a noop if input is null or empty list. For example, if an input
 * List has Coda sorted by ascending coid, the Coda with the greatest
 * identifier id should be those remaing in the this list.
 * @see addOrReplaceAllOfSameStaType(JasiReadingIF)
 */
  public void addOrReplaceAllOfSameStaType(List aList) { 
      addOrReplaceAllOfSameStaType(aList, false);  // changed to below 2010/09/27 -aww
  }
  public void addOrReplaceAllOfSameStaType(List aList, boolean onlyAuto) { // added 2010/09/27 -aww

      if (aList == null || aList.isEmpty()) return;
      JasiReadingIF jrNew = null;

      int inSize = aList.size();
      if (! onlyAuto) {
          for (int idx=0; idx < inSize; idx++) {
              jrNew = (JasiReadingIF) aList.get(idx);
              removeAllOfSameStaType(jrNew); // remove all matching readings
              add(jrNew); // append new reading to end of list
          }
          return;
      }
      else {
          JasiReadingListIF oldList = null;
          //Loop over input list
          for (int idx=0; idx < inSize; idx++) {
              jrNew = (JasiReadingIF) aList.get(idx);
              oldList = (JasiReadingListIF) getAllOfSameStaType(jrNew);
              if (oldList.size() > 0) { // found a match 
                  JasiReadingIF jrOld = null;
                  for (int jdx=0; jdx < oldList.size(); jdx++) { // loop over matches
                    jrOld = (JasiReadingIF) oldList.get(jdx);
                    if (jrNew.isAuto() && onlyAuto && !jrOld.isAuto()) continue; // skip removal/add when existing reading is not auto
                    remove(jrOld);
                    oldList.remove(jrOld);
                  }
              }
              if (oldList.size() == 0) add(jrNew); // add ok, no human/final
          }
      }
  }

  /**
  * Replaces the first JasiReadingIF element of this list
  * found equivalent to each of the input list elements by
  * matching association, channel, and type. Otherwise, adds
  * unique elements of the input list to the end of this list.
  * No time match requirement.
  * Matching virtually deleted readings are also replaced.
  */
  public void addOrReplaceAll(List aList) {
    int inSize = aList.size();
    for (int i = 0; i<inSize; i++) {
      addOrReplace((JasiReadingIF)aList.get(i));
    }
  }
  /**
   * Replaces first JasiReadingIF found in collection matching
   * the associations, channel and type of input with the input,
   * if none found, adds the input to end of the collection.
   * Matching virtually deleted readings are replaced as well.
   * Returns the replaced reference, otherwise the added.
   * @see #addOrReplace(JasiReadingIF)
   * @throws ClassCastException if input is not instance of JasiReading
   */
  public JasiCommitableIF addOrReplace(JasiCommitableIF newReading) {
    return addOrReplace((JasiReadingIF) newReading); // force association match?
  }
  /**
   * Replaces first JasiReadingIF found in collection matching
   * the associations, channel and type of input with the input,
   * if none found, adds the input to end of the collection.
   * Matching virtually deleted readings are replaced as well.
   * Returns the replaced reference, otherwise the added.
   * Throws exception if input is not instance of JasiReading
   */
  public JasiReadingIF addOrReplace(JasiReadingIF newReading) {
    // Do we want a match mode switch here ?
    if (matchMode == MATCH_ID) return (JasiReadingIF) addOrReplaceById(newReading);
    return addOrReplace(getSameChanType(newReading, false), newReading);
  }
  /** Replace first JasiReadingIF found in list matching
   *  channel, type, time of input, with input, if any,
   *  otherwise add input to collection.
   *  Does not test for association equivalence.
   *  A matching virtually deleted reading is replaced.
   *  Returns replaced instance, otherwise the added.
   *  */
  public JasiReadingIF addOrReplaceLikeWithSameTime(JasiReadingIF newReading) {
    return addOrReplace(getLikeWithSameTime(newReading, false), newReading);
  }
  /** Replace first JasiReadingIF found in list matching
   *  associations, channel,  time of input, with input,
   *  if any, otherwise add input to collection.
   *  Tests for association equivalence.
   *  A matching virtually deleted reading is replaced.
   *  Returns replaced instance, otherwise the added.
   */
  public JasiReadingIF addOrReplaceWithSameTime(JasiReadingIF newReading) {
    double newTime = newReading.getTime();
    JasiReadingIF jr =
      getNearestToTime(newTime,
                      newReading.getChannelObj(),
                      newReading.getAssociatedSolution(), false);
    return (jr.getTime() == newTime) ?
      addOrReplace(jr, newReading): addOrReplace(null, newReading);
  }

  /*
  // Future  possible filtering implementation to reduce variant method code bloat:
  public JasiReadingIF addOrReplace(JasiReadingIF jr, int filterByMask);
    JasiReadingIF jr = getSame(jr, filterByMask, ALL);
    return (jr != null) ?
      addOrReplace(jr, newReading): addOrReplace(null, newReading);
  }
  public JasiReadingIF getSame(JasiReadingIF jr, int filterByMask, int delete_state);

  */

  //NOTE: Virtually delete non-null oldReading when replaced by newReading.
  private JasiReadingIF addOrReplace(JasiReadingIF oldReading, JasiReadingIF newReading) {
      if (oldReading == null) {  // no original, just add the new one
        add(newReading);         // this add will fire the change event
        return newReading;
      } else {
        if (oldReading != newReading) { // delete old replaced by new
          //delete below removed since its done by set() method invocation below
          //this.delete(oldReading); // toggle deleteFlag, does listener notification
          // for below to use override of set() now implemented in ActiveArrayList to notify
           this.set(indexOf(oldReading), newReading, true);  // does notify list listeners
        }
        return oldReading;
      }
  }
         //  Reflection could avoid repeating code sections, but is it efficient?
         //  repeated code could be replaced by delegating to private method with a try exception
         //  code block utilizing "reflection" calls like:
         //    Method m = getClass().getMethod(methodNameString, new Class[] { methodParam1.class,...});
         //    m.invoke(this, new Object[] {jasiReadingParam1, ...});
         //
         //  e.g.:
         //  private JasiReadingIF doGetJasiReadingMethod(String methodName, JasiReadingIF inputReading) {
         //    try {
         //      Method m = getClass().getMethod(aString, new Class[] {JasiReading.class});
         //      .. like list loop code goes here ...
         //         if (((Boolean) m.invoke( this, new Object[] {inputReading} )).booleanValue() )  {
         //      ... more like code ...
         //         }
         //    }
         //    catch (Exception ex) {}
         //    return jr;
         //   }

  /** Returns first "undeleted" reading found in collection
   * matching the channel and type of the input instance.
   * Returns null if none found.
   * */
    public JasiReadingIF getLikeChanType(JasiReadingIF reading ) {
      return getLikeChanType(reading, true);
    }
    public JasiReadingIF getLikeChanType(JasiReadingIF reading, boolean undeletedOnly) {
        if (isEmpty()) return null;
        int mySize = size();
        JasiReadingIF jr = null;
        for (int i = 0; i<mySize; i++) {
          JasiReadingIF loop_jr = (JasiReadingIF) get(i);
          if (undeletedOnly && loop_jr.isDeleted()) continue;
          if (reading.isLikeChanType(loop_jr)) {
            jr = loop_jr;
            break;
          }
        }
        return jr;
    }
    /** Returns first "undeleted" reading found in collection
     * matching the channel, type, and time of input.
     * Does not check for association match.
     * @see #getNearestToTime(JasiReadingIF)
     */
    public JasiReadingIF getLikeWithSameTime(JasiReadingIF reading) {
      return getLikeWithSameTime(reading, true);
    }
    public JasiReadingIF getLikeWithSameTime(JasiReadingIF reading, boolean undeletedOnly) {
        if (isEmpty()) return null;
        int mySize = size();
        JasiReadingIF jr = null;
        for (int i = 0; i<mySize; i++) {
          JasiReadingIF loop_jr = (JasiReadingIF) get(i);
          if (undeletedOnly && loop_jr.isDeleted()) continue;
          if (reading.isLikeChanTypeAndTime(loop_jr)) {
            jr = loop_jr;
            break;
          }
        }
        return jr;
    }

    /** Returns first "undeleted" reading found in collection
     * matching the associations, channel, type, and time of input.
     * Does an association match.
     * @see #getNearestToTime(JasiReadingIF)
     */
    public JasiReadingIF getSameChanType(JasiReadingIF reading ) {
      return getSameChanType(reading, true);
    }
    public JasiReadingIF getSameChanType(JasiReadingIF reading, boolean undeletedOnly) {
        if (isEmpty()) return null;
        int mySize = size();
        JasiReadingIF jr = null;
        for (int i = 0; i<mySize; i++) {
          JasiReadingIF loop_jr = (JasiReadingIF) get(i);
          if (undeletedOnly && loop_jr.isDeleted()) continue;
          if (reading.isSameChanType(loop_jr)) {
            jr = loop_jr;
            break;
          }
        }
        return jr;
    }
  // end of reflect reuse method types

  /** Returns the reading at the specified list index,
   * whether it is deleted or not.
   * */
  public JasiReadingIF getJasiReading(int idx) {
     return (JasiReadingIF) getJasiCommitable(idx);
  }

   /**
    * Returns an "undeleted" reading whose time is nearest to
    * the specified input time.
    * Returns null if there is none.
    */
    public JasiReadingIF getNearestToTime(double time) {
      return getNearestToTime(time, null, null);
    }
    /**
     * Returns an "undeleted" reading nearest to input time
     * whose channel matches that of the input channel,
     * Returns null if there is none.
     */
    public JasiReadingIF getNearestToTime(double time, Channel chan) {
        return getNearestToTime(time, chan, null);
    }

    /**
     * Returns an "undeleted" reading associated with input solution,
     * nearest in time to the specified input time.
     * Returns null if there is none.
     */
    public JasiReadingIF getNearestToTime(double time, Solution sol) {
        return getNearestToTime(time, null, sol);
    }

    /** Returns an "undeleted" reading, whose association and
     * channel match that of the input, nearest in time to the
     * time of the input reading.
     * Returns null if there is none.
     * @see #getLikeWithSameTime(JasiReadingIF)
     */
    public JasiReadingIF getNearestToTime(JasiReadingIF jr) {
      return getNearestToTime(jr.getTime(), jr.getChannelObj(), jr.getAssociatedSolution());
    }
    /** Returns an "undeleted" reading, whose association and
     * channel match that of the input, nearest in time to the
     * specified input time.
     * Returns null if there is none.
     */
    public JasiReadingIF getNearestToTime(double time, Channel chan, Solution sol) {
      return getNearestToTime(time, chan, sol, true);
    }
    public JasiReadingIF getNearestToTime(double time, Channel chan, Solution sol, boolean undeletedOnly) {
        if (isEmpty()) return null;
        JasiReadingIF nearest = null;
        double nearestDiff = Double.MAX_VALUE;
        double diff;
        int mySize = size();
        for (int i = 0; i<mySize; i++) {
          JasiReadingIF jr = (JasiReadingIF) get(i);
          if (undeletedOnly && jr.isDeleted()) continue;
          // which line order below is optimal for rejection test?
          if ((chan != null) && ! chan.equalsNameIgnoreCase(jr.getChannelObj()) ) continue;
          if ((sol != null)  && ! jr.isAssignedTo(sol) ) continue;
          diff = Math.abs(jr.getTime() - time);
          if (diff < nearestDiff) {
            nearestDiff = diff;
            nearest = jr;
          }
        }
        return nearest;
    }

  // NEED METHOD FOR RETURNING SUBLIST OF READINGS CONTAINED WITH INPUT TIMESPAN ?

  /** Returns shallow clone of list sorted as specified.
   * Cloned list contains same JasiReadingIF references as this list.
   * */
  public JasiReadingListIF getTimeSortedList(boolean ascends) {
     // could do clone, but list override does deep clone
     JasiReadingList jrl = (JasiReadingList) newInstance();
     jrl.fastAddAll(this);
     jrl.timeSort(ascends);
     return jrl;
  }
  /** Returns shallow clone of list sorted as specified.
   * Cloned list contains same JasiReadingIF references as this list.
   * */
  public JasiReadingListIF getDistanceSortedList(boolean ascends) {
     // could do clone, but list override does deep clone
     JasiReadingList jrl = (JasiReadingList) newInstance();
     jrl.fastAddAll(this);
     jrl.distanceSort();
     return jrl;
  }

   /**
    * Sort list elements alphabetically in the order specified, by site
    * (net,sta,seechan) with seedchan name in the default order defined
    * by input sort type.
    * An additional sort by datetime in case of multiple readings
    * of same type are present in the list.
    * @parameter type FULL_NAME_SORT, SITE_SEEDCHAN_TIME_SORT, SITE_TIME_SORT
    * @see JasiReadingListIF
   */
    public void channelSort(boolean ascends, int type) {
      switch (type) {
        case FULL_NAME_SORT:
          this.sort(AbstractReversibleSorter.createChannelSorter(ascends));
          break;
        case SITE_SEEDCHAN_TIME_SORT:
          this.sort( AbstractReversibleSorter.createCompositeComparator(
                       AbstractReversibleSorter.createSeedChannelSorter(ascends),
                       AbstractReversibleSorter.createTimeSorter(true)
                     )
                   );
          break;
        case SITE_TIME_SORT:
          this.sort( AbstractReversibleSorter.createCompositeComparator(
                       AbstractReversibleSorter.createSiteSorter(ascends),
                       AbstractReversibleSorter.createTimeSorter(true)
                     )
                   );
          break;
        default:
          throw new IllegalArgumentException("Error JasiReadingList unknown sort type input: " + type);
      }
    }
   /**
   * Sort list elements in alphabetically in ascending site name order with
   * a secondary sort of seedchan components in ascending time order.
   * @see #channelSort(boolean)
   */
    public void channelSort() {
      channelSort(true, SITE_TIME_SORT);
    }

    // Is method below just like the sort algorithm above ? Need to decide which methods to keep  - aww
    public void sortByStation() {
      // aww ActiveArrayList avoids sort error by removing Collection.sort below:
      this.sort(new ChannelNameSorter()); // ActiveArrayList has special sort don't use Collections.sort()
    }

   /**
    * Sort list elements by their datetime attributes in the direction specified,
    * for equivalent times, in ascending alphabetical order of the site name
    * with seed channel in the default order specified by ComponentSorter.
   */
    public void timeSort(boolean ascends) {
      //Do not use Collections.sort on ActiveArrayLists!
      //Collections.sort(this, AbstractReversibleSorter.createTimeSorter(ascends));
      //this.sort(AbstractReversibleSorter.createTimeSorter(ascends));
      /*this.sort( AbstractReversibleSorter.createCompositeComparator(
                   AbstractReversibleSorter.createTimeSorter(ascends),
                   AbstractReversibleSorter.createChannelSorter(true)
                 )
               );
      */
      this.sort( AbstractReversibleSorter.createCompositeComparator(
                   AbstractReversibleSorter.createTimeSorter(ascends),
                   AbstractReversibleSorter.createSeedChannelSorter(true)
                 )
               );
    }
   /**
    * Sort list elements in ascending reading datetime order. For
    * equivalent times, in ascending alphabetical order of the site name name
    * with seed channel name in the default order specified by ComponentSorter.
    * @see #timeSort(boolean)
   */
    public void timeSort() {
      timeSort(true);
    }
    /**
     * Sort list elements by their Channel distance from the event source
     * in the direction specified.
     * For equivalent distances, readings are sorted by site name in ascending
     * order but with seedchan in the order specified by ComponentSorter class,
     * ie. HH before HL and orientations the order Z,N,E.
     * If no distance is defined for any channel of the list elements the list
     * is sorted by channel with the same input sort type.
     * @see #channelSort(boolean, int)
    */
    public void distanceSort(boolean ascends, int type) {
      if (! haveKnownDistances()) {
        channelSort(true, type);
        return;
      }
      switch (type) {
          case FULL_NAME_SORT:
            this.sort( AbstractReversibleSorter.createCompositeComparator(
                       AbstractReversibleSorter.createDistanceSorter(ascends),
                       AbstractReversibleSorter.createChannelSorter(true)
                       )
                     );
            break;
          case SITE_SEEDCHAN_TIME_SORT:
            this.sort( AbstractReversibleSorter.createCompositeComparator
                       (
                         AbstractReversibleSorter.createDistanceSorter(ascends),
                         AbstractReversibleSorter.createCompositeComparator
                         (
                           AbstractReversibleSorter.createSeedChannelSorter(true),
                           AbstractReversibleSorter.createTimeSorter(true)
                         )
                       )
                     );
                       //AbstractReversibleSorter.createSeedChannelSorter(true)
            break;
          case SITE_TIME_SORT:
            this.sort( AbstractReversibleSorter.createCompositeComparator(
                       AbstractReversibleSorter.createDistanceSorter(ascends),
                       AbstractReversibleSorter.createSiteTimeSorter(true)
                       )
                     );
            break;
          default:
            throw new IllegalArgumentException("Error JasiReadingList unknown sort type input: " + type);
      }
    }

    /**
     * Sort list elements in ascending Channel distance order.
    * For equivalent times, in the ascending order of the site name with
    * seedchan components ordered by time.
    * @see #distanceSort(boolean)
    */
    public void distanceSort() {
      distanceSort(true, SITE_TIME_SORT);
    }

    private boolean haveKnownDistances() {
      int mySize = size();
      JasiReadingIF jr = null;
      boolean haveDist = false;
      for (int i=0; i < mySize; i++) {
          jr = (JasiReadingIF) get(i);
          double km = jr.getHorizontalDistance(); // ok to use slant here for test - aww 06/11/2004
      // distance remains unknown unless a ChannelList loaded with sta coord was matched with channels in list.
          if ( ( Double.isNaN(km) || (km == Channel.NULL_DIST) ) && jr.getChannelObj().hasLatLonZ()) {
             jr.calcDistance();
             haveDist = true;
          }
          else if (km >= 0.) haveDist = true; // any readings with distance
      }
      return haveDist;
    }

    /**
     * Calculate distances from the input LatLonZ location to each reading
     * Channel distance is set to Channel.NULL_DIST, for any reading element
     * in  this list whose site location attributes are not set. After calculating
     * distances, this list is sorted by is ascending distance, for equal distances
     * sorted by seedchan in the default order specified by ComponentSorter.
     * @see ComponentSorter
     * @see #distanceSort(boolean)
    */
    public void distanceSort(GeoidalLatLonZ latlonz) { // aww 06/11/2004 changed input arg type
       calcDistances(latlonz);
       // ActiveArrayList has special sort exception
       // by removing Collection.sort below:
       // Collections.sort(this, new ChannelSorter());
       // this.sort(new ChannelSorter()); // aww used to be this now:
       //this.sort(new ChannelDistanceSorter()); // test below
       this.distanceSort(true, SITE_SEEDCHAN_TIME_SORT); // test here 2008/02/07 -aww
    }

    /**
     * Sort the Channel list by distance from the input location.  <p>
     * Does not order components at same distance by name or type.<br>
    */
    public void distanceOnlySort(GeoidalLatLonZ latlonz) { // aww 06/11/2004
      calcDistances(latlonz);
      // aww ActiveArrayList avoids sort error by removing Collection.sort below:
      this.sort(new DistanceSorter()); // Comparator in gazetteer package
    }

  /**
   * Calculate and set both the horizontal distance and azimuth
   * from the input LatLonZ location to each reading Channel in list.
   * If a reading Channel has undefined latitude and longitude
   * its distance is set to Channel.NULL_DIST.
   * */
  public void calcDistances(GeoidalLatLonZ loc) { // aww 06/11/2004 changed input arg type
    int mySize = size();
    for (int i=0; i < mySize; i++) {
      ((Channelable)get(i)).calcDistance(loc); // does azimuth
    }
  }

  /** Find the maximum azmuthal gap for the set of "undeleted" readings
   * found in a scan of this list.
   * @return the maximum gap in degrees.
   * @see org.trinet.jasi.Channelable
   * */
  public double getMaximumAzimuthalGap() {
     double maxGap = 360.0;
     int mySize = size();
     if (mySize < 2) return maxGap;  // only one point

     // save azimuths fro undeleted readings
     ArrayList azList = new ArrayList(mySize);
     JasiReadingIF jr = null;
     for (int i = 0; i < mySize; i++) {
        jr = (JasiReadingIF) get(i);
        if (! jr.isDeleted() && jr.contributes()) azList.add(Double.valueOf(jr.getAzimuth()));
     }
     if (azList.size() < 2) return maxGap;  // no data, all are deleted

     // Have undeleted data, sort in ascending order, first is smallest
     Double [] az = (Double []) azList.toArray(new Double[azList.size()]);
     Arrays.sort(az, 0, az.length);
     // start with gap from last to first azimuth
     // this angle would span the 0 position in the circle
     double maxgap = Math.abs((az[0].doubleValue() + 360.0) - az[az.length-1].doubleValue());
     if (Double.isNaN(maxgap)) maxGap = 360.0;
     // go clockwise around the circle
     double diff = 0.;
     for (int i=0; i < az.length-1; i++) { // Kate found bug changed upper limit to length-1 not -2 08/29/2007 -aww
       diff = Math.abs(az[i+1].doubleValue() - az[i].doubleValue()); // abs extra
       if (diff > maxgap && ! Double.isNaN(diff)) maxgap = diff;
     }
     return maxgap;
  }

    /**
     * Trim (remove) any readings from this list whose
     * channel distance is greater than the specified input.
     * Returns true if objects were removed.
     */
    public boolean trim(double dist) {
      boolean trimmed = false;
      // Most efficient to do in reverse, eliminates repacking array
      for (int i = size()-1; i >= 0; i--) {
        //if ( ((Channelable)get(i)).getDistance() > dist ) { // no don't use slant awwDist 06/11/2004 removed
        if ( ((Channelable)get(i)).getHorizontalDistance() > dist ) { // aww 06/11/2004
          // Do we want remove(i), delete(i), or erase(i) below? -aww
          remove(i);  // each remove fires an event
          trimmed = true;
        }
      }
      return trimmed;
    }

    /**
     * Trim this List to 'count' elements by removing
     * the list elements whose indexes >= count.
     * Returns true if any element was removed.
     */
    public boolean trim(int count) {
      if (count < 0) return false;
      int mySize = size();
      if (count >= mySize ) return false; // nothing to trim
      // Removes from this List all of the elements between
      // fromIndex, inclusive and toIndex, exclusive, e.g.
      // 0,1,2 indices for size=3:
      // trim(0) removes all elements
      // trim(1) removes 2nd&3rd
      // trim(2) removes 3rd element
      removeRange(Math.min(mySize, count), mySize);
      return true;
    }

    /**
     * Return this list as an array of Channelable objects.
     * Returns a zero-length array if list is empty.
     */
    public Channelable[] getChannelableArray() {
      return (Channelable[]) this.toArray(new Channelable[0]);
    }

    /** Returns the first JasiReadingIF found in the list
     * whose channel data matches that of the input.
     * Returns null if no match.
     * */
    public Channelable getByChannel(Channelable chan) {
        return ChannelableList.getByChannel(this, chan);
    }
    /** Returns the index of the first JasiReadingIF found in the list
     * whose channel data matches that of the input.
     * Returns -1 if no match.
     * */
    public int getIndexOf(Channelable chan) {
        return ChannelableList.getIndexOf(this, chan);
    }

  abstract public String getNeatHeader();

  //Below redundant with superclass methods -aww 09/18/2007
  /*
  //Return a string with a neat listing of this reading list. 
  // does not include virtually deleted readings. 
  public String toNeatString() {
    return toNeatString(false);
  }
  public String toNeatString(boolean includeDeleted) {
    int mySize = size();
    if (mySize == 0) return "Empty list";
    StringBuffer sb = new StringBuffer(mySize*256);
    sb.append(getNeatHeader()).append("\n");
    for (int i=0; i<mySize; i++) {
      JasiCommitableIF jc = (JasiCommitableIF) get(i);
      if (!includeDeleted && jc.isDeleted()) continue;
      sb.append(jc.toNeatString()).append("\n");
    }
    return sb.toString();
  }
  */

    /**
    * Print dumpToString(), for debugging.
    */
    public void dump() {
      System.out.println(dumpToString());
    }
    /** Line-feed delimited toString(), deleted readings are included.*/
    public String dumpToString() {
        return toString(true);
    }
    /** Line-feed delimited toString() for all readings in list,
     * including those virtually deleted.
     * */
    public String toString() {
        return toString(true);
    }
    public String toString(boolean includeDeleted) {
      int mySize = size();
      StringBuffer sb = new StringBuffer(mySize*256);
      for (int i = 0; i<mySize; i++) {
        JasiReadingIF jr = (JasiReadingIF) get(i);
        if (! includeDeleted && jr.isDeleted()) continue;
        sb.append(jr.toString()).append("\n");
      }
      return sb.toString();
    }

// BELOW ADDED after examining Doug's version of code
// Figure out which "sort" methods to keep, like my implementations above.
// Could implement static equivalent of sort,cull methods in ChannelableList
    /**
    * Sort the JasiReading list by time, youngest first. If objects have the same time their order
    * is undefined. Input argument <i>sortOrder'</i> determines the sort direction.<p>
    * @see JasiReadingTimeSorter.RECENT_FIRST
    * @see JasiReadingTimeSorter.OLDEST_FIRST
    */
    public void sortByTime(int sortOrder) {
      if (sortOrder == JasiReadingTimeSorter.RECENT_FIRST ||  // check for valid sortOrder
          sortOrder == JasiReadingTimeSorter.OLDEST_FIRST) {
        sort(new JasiReadingTimeSorter(sortOrder));
      }
    }
    /**
    * Sort the JasiReading list by time, youngest first. If objects have the same time
    * they are sorted by component name but otherwise their order is undefined.
    */
    public void sortByTime() {
      sortByTime(JasiReadingTimeSorter.RECENT_FIRST);
    }
    /**
    * Sort the JasiReading list by time; oldest first. If objects have the same time their order
    * is undefined.
    */
    public void sortByReverseTime() {
      sortByTime(JasiReadingTimeSorter.OLDEST_FIRST);
    }
    /**
     * Returns a TimeSpan whose lower bound is the time of first list element and
     * whose upper bound is the time of last list element.
     * If list is time sorted, TimeSpan covers time range for all list elements.
     * If list is empty returns an empty "i.e. undefined" TimeSpan.
     */
    public TimeSpan getTimeSpan() {
      // should you force a time sort here? -aww
      if (isEmpty()) return new TimeSpan();
      double t0 = ((JasiReading)get(0)).getDateTime().getTrueSeconds(); // -aww 2008/02/10
      double t1 = ((JasiReading)get(size()-1)).getDateTime().getTrueSeconds(); // -aww 2008/02/10
      return new TimeSpan(t0, t1);
    }

    /**
     * Returns a TimeSpan whose lower bound is the mininum time and whose upper
     * bound is the maximum time found in the list elements regardless of order.
     * Ignores time values that are Double.NaN in determining min,max values.
     * If list is empty returns an empty "i.e. undefined" TimeSpan.
     */
    public TimeSpan getMinMaxTimeSpan() {

      int cnt = size();
      if (cnt == 0) return new TimeSpan();

      double t0 = Double.POSITIVE_INFINITY;
      double t1 = Double.NEGATIVE_INFINITY;
      double t = 0.;
      for (int idx = 0; idx < cnt; idx++) {
          t = ((JasiReading)get(idx)).getTime();
          if ( ! Double.isNaN(t) ) {
            if (t > t1) t1 = t;
            if (t < t0) t0 = t;
          }
      }
      return new TimeSpan(t0, t1);
    }

   /**
    * Make sure all element members have LatLonZ Channel info.
    * If missing tries to look up a info in the DataSource.
    * Returns 'true' if ALL the Channels have a LatLonZ.
    */
    public boolean insureLatLonZinfo(java.util.Date date) {
      return ChannelableList.insureLatLonZinfo(this, date);
    }
    public ChannelableListIF cullByChannelNameAttribute(String [] attributeValues,
                    int attributeId, int includeFlag) {
        return cullByChannelNameAttribute(new StringList(attributeValues), attributeId, includeFlag);
    }
    public ChannelableListIF cullByChannelNameAttribute(StringList attributeValueList,
                    int attributeId, int includeFlag) {
      return ChannelableList.cullByChannelNameAttribute(this, attributeValueList, attributeId, includeFlag);
    }
    /** Return a new ChannelableList that is a subset of this list and
     * contains only those Channelable objects that were ACTIVE on input true epoch second.
     */
    public ChannelableListIF cullByTime(double time) {
      return ChannelableList.cullByTime(this, time);
    }
    /** Return the first Channelable object in the list with a full name
     * <b>begining with</b> the given string. Search is NOT case sensitive.
     * Assumes string is a full Net-Sta-Chan-Loc description delimited by
     * space, \t,"."/", or ",". For example: the input string "CI.AB" would match
     * "CI.ABC.HHL.0". So, would "CI ABC", "ci.ab" or "CI/ABC/H".
     * Returns null if no match. */

    public Channelable getChannelableStartingWith(String str) {
      return ChannelableList.getChannelableStartingWith(this, str);
    }

    /** Match channels in this list with the MasterChannelList to lookup lat/lon, gain, etc.
     * If the MasterChannelList is empty (uninitialized) it will look up each individual
     * channel's info from the DataSource OR if it is more efficient it will read in the
     * whole channel list for this date.<p>
     * This method will not use */
    public boolean matchChannelsWithDataSource(java.util.Date date) {
      return MasterChannelList.matchChannelsWithDataSource(this, date);
    }
} // JasiReadingList

