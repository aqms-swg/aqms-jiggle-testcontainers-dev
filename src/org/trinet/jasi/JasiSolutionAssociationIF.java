package org.trinet.jasi;
import java.util.Collection;
public interface JasiSolutionAssociationIF extends JasiSourceAssociationIF, JasiCommitableIF {
  //For different subtypes of observational data need to
  //descriminate "channel" dependent vs. "summary" dependent results
  //see use of copySolutionDependentData() of JasiReadingIF for arrivals.
  public void initSolDependentData();
  //public copySolutionDependentData(JasiSolutionAssociationIF jc);

  public void assign(Solution sol); // assignment affiliation with input Solution, programmer convenience no side-effects
  public boolean isAssignedToSol(); // a non-null Solution affiliation. Simple assignment no check for list membership.
  public boolean isAssignedTo(Solution sol); // Simple assignment check, no check for list membership.
  public void associate(Solution sol); //assign to input Solution and adding instance reference to its collections.
  public Solution getAssociatedSolution(); // returns Solution reference assigned (No collection membership check).
  public boolean isAssociated(); // A non-null Solution affiliation and membership as element in its collections.
  public boolean isAssociatedWith(Solution sol); // as above for specified input Solution
  public boolean assocEquals(JasiSolutionAssociationIF obj);
  public void unassociate(); // Nulls Solution affiliation removing object reference from its collections.
  public boolean erase(); // Convenience method combines unassociate() and delete() behavior.
  public boolean containedBy(Solution sol); // check for membership as reference element in Solution collection.
  public boolean addTo(Solution sol);
  public boolean addOrReplaceIn(Solution sol);
  public boolean removeFrom(Solution sol);
  public boolean deleteFrom(Solution sol);
  public Collection getBySolution(Solution sol);
  public Object getOriginIdentifier();
  //public void removeFromAssocSolList(); //see removeFrom(Solution), remove from Solution collection affiliation not nulled
  //public void removeFromAssocLists(); //erase() ?, remove from all lists (e.g. mag as well)
}
