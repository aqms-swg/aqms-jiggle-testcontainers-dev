package org.trinet.jasi;
import java.util.*;
import org.trinet.util.*;
import org.trinet.jdbc.datatypes.*;
/**
* A list of Solution associated Magnitudes.
* WARNING! Beware of invoking list replacement and removal
* methods on directly on a Solution's own internal MagList, 
* Solution's preferred Magnitude may be removed from list.
* Overrides are not implemented here to prevent this.
* Rather your should use Solution class methods such as
* remove(Magnitude) or setPreferredMagnitude(Magnitude).<p>
* Methods of concern: <br>
* <pre>
       public void clear();
       public void clear(boolean notify);
       public boolean remove(Object obj);
       public Object set(int index, Object obj);
       public Object set(int index, Object newObj, boolean notify);
       public Object remove(int index, boolean notify);
       public boolean remove(Object obj, boolean notify);
       public void removeRange(int from, int to);
</pre>
*/
public class MagList extends AbstractSolutionAssocList {

  public MagList() {super();}
  public MagList(int capacity) {
      super(capacity);
  }
  public MagList(Collection c) {
      addAll(c, false);
  }

  public Magnitude getMagnitude(int idx) { return (Magnitude) get(idx); }
  public Magnitude[] getArray() {
    return (Magnitude[]) toArray(new Magnitude[this.size()]);
  }
  /**
  * Removes all mags of same type from list and adds the input mag to list.
  * If input Solution owns this MagList, keeps the preferred magnitude in the
  * list if type is equivalent. 
  */
  public void replaceAllOfTypeWith(Solution sol, Magnitude mag) {
    MagList solMagList = (MagList) getAssociatedWith(sol, false);
    MagList typeMagList = (MagList) solMagList.getListByType(sol,mag.getTypeQualifier());
    if (typeMagList.size() > 0 ) { remove(typeMagList); } // pre-existing
    if (this == sol.getMagList()) this.add(sol.getPreferredMagnitude());
    sol.associate(mag); // adds mag and its data to solution lists
  }
  /** Returns preferred magnitude in list for associated sol, or null if none. */ 
  public Magnitude getPreferredMagnitude(Solution sol) {
    if (isEmpty()) return null;
    int mySize = size();
    Magnitude mag = null;
    for (int i = 0; i < mySize; i++) {
      Magnitude loop_mag = (Magnitude) get(i);
      if (loop_mag.isAssignedTo(sol) && loop_mag.isPreferred()) {
        mag = loop_mag;
        break;
      }
    }
    return mag;
  }
  public MagList getAlternateMagList(Solution sol) {
    MagList magList = (MagList) getAssociatedWith(sol, false);
    magList.remove(sol.getPreferredMagnitude());
    return magList;
  }
  public String getNeatHeader() {
    return Magnitude.getNeatStringHeader();
  }

  public Magnitude getLastSolvedOfType(String magType) {
      String type =
          (magType.length() > 1 && magType.substring(0,1).equalsIgnoreCase("M")) ?
                magType.substring(1) : magType;

      JasiCommitableListIF aList = getListByType(type);

      int count = aList.size();
      if (count == 0) return null; // no such type

      DataTimestamp magSolveDate = ((Magnitude)aList.get(0)).getSolveDate();
      DataTimestamp maxSolveDate = magSolveDate; // get the 1st element date
      int indexOfMaxDate = 0;
      // Search for later date in list elements, if any
      for (int idx=1; idx < count; idx++) {
          magSolveDate = ((Magnitude)aList.get(idx)).getSolveDate();
          if (magSolveDate.compareTo(maxSolveDate) > 0) {
              maxSolveDate = magSolveDate;
              indexOfMaxDate = idx; // save the list index
          }
      }

      return (Magnitude) aList.get(indexOfMaxDate);
  }

  /* NOTE: If  MagList listener are not implemented by Solution
   * these overrides below are needed to allow Solution add/remove listener
   * to magnitudes added/removed from MagList no use of list.set overwrite
   * instead a remove and add.
  public JasiCommitableIF addOrReplace(JasiCommitableIF newData) {
    if (newData == null) return null; // no value to add;
    int idx = indexOfEquivalent(newData);
    if (idx < 0) {
      add(newData); // not already in list, fire
      return newData;
    }
    JasiCommitableIF oldData = (JasiCommitableIF) get(idx);
    if (oldData == newData) return oldData; // identical objects no action
    //assume only one instance first found in list
    //oldData.setDeleteFlag(true); // do we delete it by default or not before overwrite?
    remove(idx); // remove prior version, fire for MagList listener in Solution
    add(idx,newData); // add new version, fire for listener
    return oldData;
  }
  public JasiCommitableIF addOrReplaceById(JasiCommitableIF newData) {
    if (newData == null) return null; // no value to add;
    int mySize = size();
    if (mySize == 0) {
      add(newData); // fire
      return newData;
    }
    // look for an equivalent observation to replace
    for (int idx=0; idx < mySize; idx++) { 
      JasiCommitableIF oldData = (JasiCommitableIF) get(idx);
      if (oldData.idEquals(newData)) { // what about DataObject equalsValue()?
        //assume only one instance in list
        if (newData == oldData) return oldData; // no fire already in list
        //oldData.setDeleteFlag(true); // do we delete it by default or not?
        remove(idx); // remove prior version, fire for MagList listener in Solution
        add(idx,newData); // add new version, fire for listener
        return oldData;
      }
    }
    // not already in list, fire
    add(newData);
    return newData;
  }
*/
} // MagList
