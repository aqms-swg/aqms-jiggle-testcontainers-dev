// CHECK EFFECT OF "IF"-TYPE BROADENING CASTS IN IF HIERARCHY,
// IS CORRECT OVERLOADED METHOD INVOKED IN IF IMPLEMENTATIONS?
// COMPILE TIME ARG-TYPE METHOD SIGNATURE IS USED, NOT RUNTIME ARG TYPE.
//
//Maybe a boolean attribute/method for a "valid" state after calculation?
//
//TODO: avoid confusion with language syntax, change "method" member to
//      "algorithm" or "technique" likewise in Solution class
//
// TODO: Between the List types, Solution, Magnitude and their Reading types
// rework un/delete() such that the associated un/delete() delegates to the associator
// and the associators or the generic Lists methods use the setDeleteFlag flag
// rather than the un/delete() to toggle the flag, else feedback loop.
//
// TODO: subclass below here GenericCodaMag and GenericAmpMag
//       then have TN classes inhereit from those?
//
// TODO: Both Solution and Magnitude could have static boolean member
// "assumeStaleOnLoad" so that after preserving current states before
// states before load (needsCommit & isStale), they can be restored
// after DataSource load, loading from finalized archives should not
// necessarily force a state change if list is empty.
//
// TODO: Both Solution and Magnitude should put the String formating
// filters in separate xxxStringFormater class with static methods.
//
// TODO: Instead of multiple lists, perhaps a hash collection of data
// lists since a subtype might depend on more than one data list type.
// Instead of multiple reading lists instantiated as below, need
// single generic Collection of lists data member whose concrete
// jasi subtype is defined by a sub/inner class of Magnitude.
//
// TODO: Instead of global Magnitude object and a subscript data member,
// subtype magtype specifics... In Magnitude superclass abstract
// methods like getXXX, addXXX, deleteXXX to a generic accessor with
// appropiate JasiReadingList or JasiReading type arguments. Each
// Mag sub/inner class implementation would implement the specifics
// of the concrete reading types of interest. The Abstract Magnitude
// parent would delegate to the children subtypes Ml, Mc etc classes.
// IF subtype were an inner class member in abstract Magnitude it
// could contain the specifics necessary for that type, as well as
// the descriptor, data lists, etc. appropiate for the subtype.
// These method/field members would be accessed via method delegates
// in the containing Magnitude (Abstract) superclass. Casting would
// be done to access specific custom to the subtype, such as:
//  if (mag instanceof SuperDuperMag) { ... }
//
package org.trinet.jasi;

import java.util.*;
import javax.swing.event.*;
import org.trinet.jasi.coda.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;

/**
 * Description of a calculated magnitude associated with a Solution. <p> This is
 * a component of the Java Seismic Abstraction Interface (jasi) which allows
 * applications to interact with different schemas tranparently. <p> All data
 * members are DataObjects rather than primative types so they can handle the
 * concept of "nullness" because databases can contain null values for certain
 * fields. Also, applications may not want to write a "bogus" value like 0 into
 * the database when the real value is not known. Data primatives can't be null
 * and are initiallize to zero.<p>
 * Warning! do not inadvertently delete or unassociate the a Solution's
 * preferred Magnitude.
 *
 * @See: JasiObject */

public abstract class Magnitude extends SolutionAssocJasiObject
  implements JasiAssociationFitIF, JasiCommitableAssociatorIF, MagTypeIdIF
{
    protected boolean originDependent= true; 

    // End-user application program toggles  -aww
    public static boolean dbAttributionEnabled = false; //  don't attribute
    public static boolean staleCommitOk = true; // forces commit even if stale 
    public static boolean staleCommitNoop = false; // if true do nothing on commit, return true?
    //public static boolean commitLoadsReadings = true; // ? -aww

    /*
      Here's the list of public data members for a Magnitude. These need to be
      supported by any port of this abstract layer
     */

    /** True if amps list has changed which makes the magnitude invalid or stale. */
    protected boolean isStale = false;

    // Synchronize sol list additions/removal with mag list equivalents
    // Need an enabling method to turn on/off this behavior.
    // DEFAULT false = decouples any sol list changes from like mag lists.
    protected boolean listenToSolReadingListAdd    = false;
    protected boolean listenToSolReadingListRemove = false;
    protected boolean listenToSolReadingListState  = false;

    private long version = 0l; // counter for engine iterations?

    private DataTimestamp solveDate = new DataTimestamp(); // updated by engine ?

    /** DataString containing username of the analyst that created this solution. */
    //private DataString who = new DataString(EnvironmentInfo.getUsername());
    private DataString who = new DataString("---"); // unknown until synched with stored attribution

    /** A unique ID number for this magnitude. May be null. May be a dbase key. */
    public DataLong    magid           = new DataLong();

    /** The value of the magnitude */
    public DataDouble  value           = new DataDouble();
    public DataDouble  adjValue        = new DataDouble();
    /** The subScript of magnitude. Only the suffix part of the type without
     * the leading "M".  For example: for an "Ms" subScript="s".
     * A valid value should be non-blank, blank for null value
     * Values of "n" and "un" flag no magnitude value and unknown type for value.
     * @see MagnitudeMethodIF */
    public DataString  subScript       = new DataString();

    /** The method used to calculate the magnitude. May be a program name or
        some other string that is meaningful to the underlying DataSource. */
    public DataString  algorithm       = new DataString();

    /** Number of stations that contributed to this magnitude */
    /* Forced to do this rather than use getStationsUsed() because the RT system
    * does not give a weight to used amps. */
    public DataLong    usedStations    = new DataLong();
    public DataLong    usedChnls       = new DataLong();

    /** The standard deviation of the contributing channel magnitudes */
    public DataDouble  error           = new DataDouble();
    /** Largest azmuthal gap in stations contributing to the magnitude */
    public DataDouble  gap             = new DataDouble();
    /** Horizontal distance in km to the nearest station contributing to the magnitude */
    public DataDouble  distance        = new DataDouble();
    /** Estimate of magnitude quality 0.0<=quality<=1.0, 1.0 is best */
    public DataDouble  quality         = new DataDouble();

    /** List of Codas associated with this Magnitude */
    public CodaList codaList           = CodaList.create();
    /** List of Amplitudes associated with this Magnitude */
    public AmpList ampList             = AmpList.create();

    ListDataStateListener myReadingsDataStateListener =  null;
    ListDataStateListener solCodaDataStateListener = null;
    ListDataStateListener solAmpDataStateListener = null;


    public Object clone() {
        Magnitude mag       = (Magnitude)  super.clone();
        mag.magid           = (DataLong)   this.magid.clone();
        mag.magid.setNull(true); // ? force new assign
        mag.value           = (DataDouble) this.value.clone();
        mag.adjValue        = (DataDouble) this.adjValue.clone();
        mag.subScript       = (DataString) this.subScript.clone();
        mag.who             = (DataString) this.who.clone();
        mag.algorithm       = (DataString) this.algorithm.clone();
        mag.usedStations    = (DataLong)   this.usedStations.clone();
        mag.usedChnls       = (DataLong)   this.usedChnls.clone();
        mag.error           = (DataDouble) this.error.clone();
        mag.gap             = (DataDouble) this.gap.clone();
        mag.distance        = (DataDouble) this.distance.clone();
        mag.quality         = (DataDouble) this.quality.clone();
        mag.solveDate       = (DataTimestamp) this.solveDate.clone();
        mag.ampList         = (AmpList)    this.ampList.clone();
        mag.codaList        = (CodaList)   this.codaList.clone();
        mag.initListeners(); // reset data listeners of clone
        return mag;
    }

    /*
    public void copyMagSummaryData(Magnitude mag) {
        value           = mag.value;
        subScript       = mag.subScript;
        algorithm       = mag.algorithm;
        usedStations    = mag.usedStations;
        usedChnls        = mag.usedChnls;
        error           = mag.error;
        gap             = mag.gap;
        distance        = mag.distance;
        quality         = mag.quality;
    }
    */

// //////////////////////////////////////////////////////////////////////
    /**
     * Constructor is protected' so that you can only instantiate jasi objects
     * using the 'create()' factory method.
     */
    protected Magnitude() {
      initListeners();
    }

    /** Convenience wrapper to clear the loaded data lists of their contents,
     * doesn't notify list listeners.
     * Invoke this only when the list data is no longer used and should be garbage collected.
     * */
    public void clearDataLists(boolean verbose) {
      if (getNeedsCommit() && verbose) {
        System.err.println("WARNING - Magnitude clearDataLists(): Magnitude needed commit: " + magid.toString());
      }
      // It is better to clear existing, or create new lists with new listeners here,
      // what should be done - aww ??
      // Let's try clear WITHOUT notifying any listeners ...
      ampList.clear(false);
      codaList.clear(false);
      //System.out.println("DEBUG - Magnitude.clearDataLists() cleared: amp,coda");
    }

    protected void initListeners() {
        if (ampList != null)   ampList.clearListDataStateListeners();
        if (codaList != null)  codaList.clearListDataStateListeners();
        myReadingsDataStateListener = new ReadingListDataStateListener();
        solCodaDataStateListener    = new SolReadingsDataStateListener();
        solAmpDataStateListener     = new SolReadingsDataStateListener();
        addMagDataListeners();
    }

// -- Concrete FACTORY METHODS ---
    /**
     * Factory Method: This is how you instantiate a magnitude object. You do
     * NOT use a constructor. This is so that this "factory" method can create
     * the type of magnitude that is appropriate for your site's database.
     */
// ////////////////////////////////////////////////////////////////////////////
    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. Creates a Solution of the DEFAULT type.
     * @See: JasiObject
     */
     public static final Magnitude create() {
        return create(DEFAULT);
     }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is an integer implementation type.
     * @See: JasiObject
     */
     public static final Magnitude create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
     }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is as 2-char implementation suffix.
     * @See: JasiObject
     */
     public static final Magnitude create(String suffix) {
        return (Magnitude) JasiObject.newInstance("org.trinet.jasi.Magnitude", suffix);
     }
// ////////////////////////////////////////////////////////////////////////////

   /**
     * Returns from the default data source all Magnitudes associated
     * with the specified identifier.
     */

    abstract public JasiCommitableIF getById(Object obj);

   /**
     * Returns ALL the Magnitudes associate with the event with this ID number
     * from the DataSource.
     * Returns null if no mags are found.<p>
     */
    abstract public Collection getBySolution(long id);
    abstract public Collection getBySolution(Solution sol);
    abstract public Collection getBySolution(Solution sol, String [] typeList);
    abstract public Collection getBySolution(Solution sol, String [] typeList, boolean assoc);

    abstract public Collection getAllBySolution(Solution sol);
    abstract public Collection getAllBySolution(Solution sol, String [] typeList);
    abstract public Collection getPrefMagOfTypeBySolution(Solution sol);

    /**
    * Need unique identifiers for equivalent to work as wanted.
    * True => super.equivalent() plus associated solution and method
    * are equal.
    */
    public boolean equivalent(Object obj) {
      if (! super.equivalent(obj)) return false;
      Magnitude mag = (Magnitude) obj;
      //System.err.println("DEBUG: equivalent mag jasiobject algoEquals: " + algorithm.equals(obj));
      //System.err.println("DEBUG: equivalent mag jasiobject asocSolEqual: " + (this.sol = mag.sol));
      return ( (this.sol == mag.sol) && algorithm.equals(mag.algorithm));
    }

    public Object getIdentifier() {
      return magid;
    }
    public void setIdentifier(Object obj) {
      magid.setValue((DataLong) obj);
    }

    public abstract long setUniqueId();
    public abstract int getPriority();

    public Object getTypeQualifier() {
      return subScript.toString();
    }

    public boolean isTypeFor(JasiCommitableIF jc) {
       boolean status = false;
       if (jc instanceof Amplitude && isAmpMag()) {
           Amplitude amp = (Amplitude) jc;
           if ( AmpType.isWoodAnderson(amp.getType()) && (getTypeSubString().equals("l") || getTypeSubString().equals("lr")) ) return true;
           if ( AmpType.isEnergy(amp.getType()) && getTypeSubString().equals("e")) return true;
           if ( AmpType.isMoment(amp.getType()) && getTypeSubString().equals("w")) return true;
       }
       else if (jc instanceof Coda && isCodaMag()) {
           Coda coda = (Coda) jc;
           if ( coda.getDurationType() == CodaDurationType.D && getTypeSubString().equals("d")) return true;
           if ( coda.getDurationType() == CodaDurationType.A && getTypeSubString().equals("c")) return true;
       }
       return status;
    }

    /** Magnitude dependent on amplitude data. */
    public boolean isAmpMag() {
      return ( subScript.toString().equalsIgnoreCase(MagTypeIdIF.ME) ||
               subScript.toString().equalsIgnoreCase(MagTypeIdIF.ML) ||
               subScript.toString().equalsIgnoreCase(MagTypeIdIF.MLR) ||
              (subScript.toString().equalsIgnoreCase(MagTypeIdIF.MW) && algorithm.toString().equals("locmag")) );
      //return !isCodaMag();
      /* for future discrimination
      if (subScript.isNull()) return false;
      return subScript.toString().equalsIgnoreCase(MagTypeIdIF.ML);
      */
    }
    /** Magnitude dependent on coda data. */
    public boolean isCodaMag() {
      if (subScript.isNull()) return false;
      return (subScript.toString().equalsIgnoreCase(MagTypeIdIF.MC) ||
              subScript.toString().equalsIgnoreCase(MagTypeIdIF.MD) );

    }

    /** Return true if this is the associated Solution's preferred magnitude. */
    public boolean isPreferred()  {
       return (sol != null && sol.magnitude == this);
    }
    public boolean isPreferred(Solution aSol)  {
       // doesn't check (aSol.magnitude.sol == aSol) assumes assignment assoc correct
       // need to make sol.magnitude private or check assignment and throw exception
       return (aSol != null && aSol.magnitude == this);
    }
    public boolean isPreferredOfType()  {
       return (sol != null && (sol.getPrefMagOfType(this) == this));
    }

    /** Magnitude value is dependent on origin, location of event.
     * Returns true by default, concrete subclass implementations
     * should override this method if the Magnitude value is NOT
     * dependent on the Solution origin.*/
    public boolean dependsOnOrigin() { return originDependent;}
    public void setDependsOnOrigin(boolean tf) {
        originDependent = tf;
    }

    abstract public boolean existsInDataSource();

    public boolean hasNullType() {
        return subScript == null || subScript.isNull() || subScript.toString().equalsIgnoreCase(" ");
    }

    /** No actual Magnitude value by some standard scale of measurement.*/
    public boolean hasNullValue() { return ! value.isValidNumber(); } // trap NaN - aww 10/7/2005

    public double getMagValue() { return value.doubleValue(); }

    // Set by engine when instance is processed or created by engine
    public void setSolveDate() {
        solveDate.setValue(new java.util.Date());
        version++;
    }
    public long getVersion() {
        return version;
    }

    // Should be time of last engine run, isNull=true implies no run on instance since instantiation
    public DataTimestamp getSolveDate() {
        return solveDate;
    }

    /**
     * Return true if any Magnitude field is different from what's in the DataSource.
     * Either, 1) its been changed and not saved or 2) it is newly created and not saved.
     */
     abstract public boolean hasChanged() ;

    /** True if changes were made that require a summary value recalculation. */
    public boolean isStale() { return isStale; };

    /** Set value of isStale flag. */
    public void setStale(boolean tf) {
        isStale = tf;
        if (tf) {
            setNeedsCommit(true); // stale magnitude needs commit - aww
        }
    }

    public boolean getNeedsCommit() {
       return needsCommit || hasChanged();
    }

    /**
     * Override prevents virtual deletion of this Magnitude
     * if isPreferred() == true.
     */
    public boolean setDeleteFlag(boolean tf) {
            //System.err.println("setDeleteFlag I am preferred mag : " + isPreferred());
            //if (this.sol != null) System.err.println("the preferred : " + sol.magnitude.toNeatString());
            //else System.err.println("I am not associated with a sol");
        if (isPreferred()) return false;
        return super.setDeleteFlag(tf);
    }

    // Methods for other IF
    public double getDistance() {   // should equal horizontal
      return distance.doubleValue();
    }
    public double getHorizontalDistance() { // should be same as distance
      return distance.doubleValue();
    }
    /** Epicentral distance of closest station contributing to mangitude value.
     * @see #setDistance(double).
     * */
    public void setHorizontalDistance(double minStaDist) {
      setDistance(minStaDist);
    }
    public double getVerticalDistance() {
      return  0.; // since horizontalDistance = distance
    }
    /** Epicentral distance of closest station contributing to mangitude value. */
    public void setDistance(double minStaDist) {
      distance.setValue(minStaDist);
    }
    public double getAzimuth() {
      return gap.doubleValue();
    }
    public void setAzimuth(double staGap) {
      gap.setValue(staGap);
    }
    public double getResidual() {
      return error.doubleValue();
    }

    /** Return count of channels (not stations) used in summary. */
    public int getReadingsUsed() {
       //return getReadingList().getChannelUsedCount();
       // Is set by database read, magnitude engine, or by user hand-entries
       int used = (usedChnls.isNull()) ? 0 : usedChnls.intValue(); // channels if read from db
       int listUsed = getReadingList().getChannelUsedCount(); // only if assoc list data is loaded
       used = (listUsed == 0) ? used : listUsed; // if assoc list number exists use it
       usedChnls.setValue(used); // update number
       return used;
    }

    /** Return count of stations (not channels) used to calculate summary value. */
    public int getStationsUsed() {
       // Is set by database read, magnitude engine, or by user hand-entries
       int used = (usedStations.isNull()) ? 0 : usedStations.intValue(); // channels if read from db
       int listUsed = getReadingList().getStationUsedCount(); // only if assoc list data is loaded
       used = (listUsed == 0) ? used : listUsed; // if assoc list number exists use it
       usedStations.setValue(used); // update number
       return used;
    }

    /**
     * Return the number of readings in this magnitude's dependent list.
     */
    public int getReadingsCount() {
        return getReadingList().size();
    }
    /*
    /**
     * Return the number of Amplitudes associated with this magnitude
     * Should be 0 for an Coda magnitude subtype.
     */
    public int getAmpCount() {
        return ampList.size();
    }
    /*
     * Return the number of Codas associated with this magnitude
     * Should be 0 for an Amplitude magnitude subtype.
     */
    public int getCodaCount() {
        return codaList.size();
    }

    /** Returns data list based upon Magnitude subtype,
     * @see #isCodaMag()
     */
    public MagnitudeAssociatedListIF getReadingList() {
      return (isCodaMag()) ?
        (MagnitudeAssociatedListIF) codaList : (MagnitudeAssociatedListIF) ampList;
    }

    // changed public access, as test to force use of getReadingList()
    protected AmpList getAmpList() { return ampList;}
    protected CodaList getCodaList() { return codaList;}

    public MagnitudeAssocJasiReading getReadingTypeInstance() {
        if (isCodaMag()) return Coda.create();
        else if (isAmpMag()) return Amplitude.create();
        return null;
    }

    /**
     *  Adds any readings that are associated in the DataSource
     *  to this Magnitude's data dependent list.
     *  First clears the list if input argument is true.
     *  Note that references are used, the readings are not cloned.
     *  Returns a count of the number of amps in ampList.
     *  Sets staleMagnitude 'true' if any are added.
     */
    abstract public int loadReadingList(boolean clearList);

    public int loadReadingList() {
      return loadReadingList(true);
    }

/* Moved specific list methods implementations to subclasses
    // should use loadReadingList in most cases!
    abstract protected int loadAmpList(boolean clearList);
    protected int loadAmpList() {
      return loadAmpList(true);
    }
    abstract protected int loadCodaList(boolean clearList);
    protected int loadCodaList() {
      return loadCodaList(true);
    }
*/

    /* IF methods below may not be needed just to vacate list but not null assignment.
    public void removeFromAssocLists() {
      removeFromAssocSolList();
    }
    public void removeFromAssocSolList() {
      Solution sol = getAssociatedSolution();
      if (sol != null) sol.remove(this); // sol.getMagList().remove(this);
    }
    */

    /** Associates this Magnitude and its data with input Solution.
     *  Adds Magnitude to Solution's Magnitude collection.
     *  Updates associated Solution data collections with Magnitude data.
     */
    public void associate(Solution sol) {
      super.associate(sol);
      // Above invokes sol.associate(mag), which also associates data lists
      // if Solution implementation is missing mag.associateLists(this)
      // must add an override or here:
      //associateLists(sol);
    }
    /** Associates input Solution with the elements of Magnitude's data lists.
     *  Updates associated Solution data collections with Magnitude data.
     */
    public void associateLists(Solution sol) {
       /* 
       if (ampList != null && ampList.size() > 0) {
         ampList.associateAllWith(sol);
       }
       if (codaList != null && codaList.size() > 0) {
         codaList.associateAllWith(sol);
       }
       */
       MagnitudeAssociatedListIF myList = getReadingList();
       if (myList.size() > 0) {
         //Which to use below depends on whether sol.associate(data) uses add(data) or addOrReplace(data)
         //when dealing with "cloned" instances of readings being added to solution's equivalent list

         //Line below with "clones" creates multiple copies if associate does add(data)
         //myList.associateAllWith(sol); // removed 07/20/2004


         //Force a safety assertion check, should have same sol assignment so:
         myList.assignAllTo(sol); // throws exception if already assigned to different sol? 

         //now replace those matching by id, else add new elements to list. Causes corruption
         //if list replacement elements are toggled "deleted" by the AbstractCommitableList "set" method 
         //because the "preferred" mag listens to the sol reading lists. In that case, the list elements 
         //updated by the list listener would be those toggled deleted by the "set" method.
         sol.addOrReplaceAll(myList); // skip those not assigned to solution. 07/20/2004 -aww
       }
    }
    /*
     * // removed 07/19/2004 -aww
     * Clears Magnitude's reading list and replacing its elements with
     * those found in its associated Solution's corresponding list
     * also associated with this Magnitude.
     * Elements unassociated with any Magnitude are also included,
     * if input argument includeUnassociated==true.
    public void setReadingListFromAssocSolution(boolean includeUnassociated) {
      if (this.sol == null) return;
      MagnitudeAssociatedListIF myList = getReadingList();
      if (myList == null) return; // what action do we want here?
      // clear current contents or keep data if solList is empty?
      MagnitudeAssociatedListIF solList =
          (MagnitudeAssociatedListIF) sol.getListFor(myList);
      if (solList == null) return; // no data
      if (includeUnassociated) { // assign this Magnitude
        solList.getUnassociatedWithMag().assignAllTo(this);
      }
      myList.addOrReplaceAll(solList.getAssociatedWith(this));
      setStale(true); // not needed with listener
    }
    */
     /**
     * Clears Magnitude's reading list and replaces its elements with
     * those found in its associated Solution's corresponding list
     * The list elements are aliased. 
     */ 
    public int setReadingListFromAssocSolution() { // 07/19/2004 associates with this all Solution data of same list type 
        return setReadingListFromAssocSolution(false); 
    }
    public int setReadingListFromAssocSolution(boolean cloneList) { // 02/16/2005 added new signature -aww 
      MagnitudeAssociatedListIF myList = getReadingList();
      // do we want to clear current contents / keep data if solList is empty?
      myList.clear(false); // added no notify -aww 2007/12/13 

      if (this.sol == null) return 0;
      MagnitudeAssociatedListIF solList =
          (MagnitudeAssociatedListIF) sol.getListFor(myList);
      if (solList == null) return 0; // no data
      // do we want to alias or clone the list? What about later edits/commits of the solList?
      if (cloneList) solList =  (MagnitudeAssociatedListIF) solList.clone(); // test trial, clone -aww
      fastAssociateAll(solList);
      setStale(true); // not needed with listener

      return getReadingsCount();
    }

     /**
     * Clears Magnitude's reading list and loads it with elements 
     * associated with its Solution in the data source. 
     * Its new list elements are not aliased, added to the associated Solution's list. 
     */ 
    public int loadReadingListBySolution() { // new method 07/19/2004 -aww
      // clear current contents or keep data if List is not empty?
      getReadingList().clear(); // for now, empty current contents
      if (this.sol == null) return 0; // no associated solution

      // what action do we want here, uncoupled?
      // since Solution list may already be associated with similar id readings 
      // below invocation of method does not associate the passed input Solution (false)
      // Association call might try to "add" to the list a different object element with same id as another (equivalent).
      // The prefmag list Listener tries to "replace" its elements with equivalent reading.
      List solList =
          (List) getReadingTypeInstance().getBySolution(sol, null, false);
      if (solList == null) return 0; // no found in data source

      fastAssociateAll(solList); // add list data to Magnitude list

      // Next synch mag readingList with the associated Solution's like list:
      sol.addOrReplaceAll(getReadingList()); // try test trial 07/26/2004 -aww 

      setStale(true); // not needed with list listener

      return getReadingsCount();
    }

    /** Simply assigns this Magnitude and its data to input Solution.
     *  Does not add Magnitude or its data to input Solution collections.
     *  Throws exception if this Magnitude or any of its data are
     *  already associated with a different non-null Solution.
     *  @see #associate(Solution)
     */
    public void assign(Solution sol) {
      super.assign(sol);
      assignLists(sol); // do we want this?
    }
    private void assignLists(Solution sol) {
       if (ampList != null && ampList.size() > 0) ampList.assignAllTo(sol);
       if (codaList != null && codaList.size() > 0) codaList.assignAllTo(sol);
    }

  /* Removes this Magnitude from the MagList of a non-null
   * associated Solution, ONLY if it is not the preferred Magnitude.
   * The Solution assigment of this Magnitude is nulled.
   * The associated reading data is NOT removed from
   * the lists of this Magnitude or those of the Solution.
   * @see org.trinet.jasi.Solution.erase(Magnitude)
   * */
   public void unassociate() {
     /*
     if (this.sol != null) {
       // Does it matter if Solution's data readings reference the removed Magnitude?
       // Don't remove them from Solution's lists, another Magnitude may use them.
       // If shared instances of these data readings have internal pointers
       // to this magnitude in the other reading lists, then do we want to
       // null the reference to this magnitude?  Below code nulls reference,
       // also removing readings from unassociated magnitude's lists, a side-effect:
       ((MagnitudeAssociatedListIF)sol.getListFor(this.getReadingList())).unassociateAllFromMag(this);
     }
     */
     //this.delete(); // do we need extra insurance to avoid commit of stale data?
     super.unassociate();
   }

//JasiMagnitudeAssociationIF implements:
// abstract IF methods in MagnitudeAssocJasiReading class subtype
// addTo(Mag) removeFrom(Mag) deleteFrom(Mag) containedBy(Mag)
// using list listeners would remove NEED for addXXX removeXXX methods
// to set state of Magnitude. This would require all internal readinglist
// objects to be assigned only via methods that add/remove list listeners.
// Implement overridden:
// add(xxxObjectSubtype) remove(xxxObjectSubtype) methods
// instead of setting state in these methods, use list listeners to set
// based on mag subtypes listen to appropiate list and decide stale.
//
  /** Associates input reference with this Magnitude, adding it
   *  to the appropiate data collection, if any.
   *  Does not unassociate input's prior association
   *  before associating it with this Magnitude.
   *  Thus, any prior Magnitude association still references
   *  the input.
   *  Insures that the input is also associated with the
   *  same Solution as this Magnitude.
   *  Returns false if input is null, else returns true.
   */
  public boolean associate(JasiCommitableIF jc) {
    return associate(jc, false); // ? should default be true or false to unassociate prior?
  }

  /**
   * Like associate() with optional unassociation any prior Magnitude
   * of the input before associating it with this Magnitude.
   * Unassociation removes the input's reference from prior Magnitude.
   * Does not change in the virtual delete state of the input.
   * Returns false if input is null, or is already associated,
   * else returns true.
   * NOTE: Should implementation do add(jc) or addOrReplace(jc) ??
   * Programmer should resolve this.
   * @see #associate(JasiCommitableIF)
   */
  public boolean associate(JasiCommitableIF jc, boolean unassociatePriorAssoc) {
    if (jc == null) return false;
    // cast here until assign problem resolution
    MagnitudeAssocJasiReading obj = (MagnitudeAssocJasiReading) jc;
    if (! obj.isAssignedTo(this) && unassociatePriorAssoc) obj.unassociate();
    //obj.setDeleteFlag(false); // ? do we want to toggle state undeleted by default ?
    //anObj.assign(this)  => won't reset associated non-null prior unless implementation changed
    obj.magnitude = this; // else add doesn't work;
    boolean retVal = add(obj);  // do add(obj) or addOrReplace(obj) here?
    // data may already be in sol list, originally or by listener
    // but sol assignment may not by right so below will fix it:
    obj.associate(sol);
    return retVal;
  }
  /**
  * Removes input reference from this Magnitude and nulls matching Magnitude assignment.
  * Checks that input object is actually associated with this instance and not another
  * before nulling the Magnitude association reference.
  */
  public boolean unassociate(JasiCommitableIF jc) {
    if (jc == null) return false;
    JasiMagnitudeAssociationIF obj = (JasiMagnitudeAssociationIF) jc;
    remove(obj);
    boolean retVal = obj.isAssignedTo(this);
    if (retVal) obj.assign((Magnitude)null);
    return retVal;
  }
  /** Adds the input reference to this Magnitude
   *  ONLY if it is assigned to it and not already contained by it.
   *  Returns true if successful, else false.
   */
  public boolean add(JasiCommitableIF jc) {
    if (jc == null) return false;
    JasiMagnitudeAssociationIF obj = (JasiMagnitudeAssociationIF) jc;
    if (! obj.isAssignedTo(this)) return false;
    JasiCommitableListIF aList = getListFor(obj);
    if (aList == null) return false;
    boolean status = aList.add(obj); //only preferred listeners synch sol association
    // Note if multiple mags of share same readings as found in Solution's list, changing it for one will not update all 
    // because the reading lists are only synched by listeners for the preferred magnitude for the event.
    if (status && sol != null && !(sol.magnitude == this)) sol.add(jc); // force update of magnitude 11/28/30 -aww
    return status;
  }
  /** Adds, or replaces the equivalent to, the input reference
   *  in this Magnitude ONLY if it is assigned to it.
   *  Returns true if successful, else false.
   */
  public boolean addOrReplace(JasiCommitableIF jc) {
    if (jc == null) return false;
    JasiMagnitudeAssociationIF obj = (JasiMagnitudeAssociationIF) jc;
    if (! obj.isAssignedTo(this)) return false;
    //Screen for different mag types to allow respective list adds?
    JasiCommitableListIF aList = (JasiCommitableListIF) getListFor(obj);
    if (aList == null) return false;
    obj = (JasiMagnitudeAssociationIF) aList.addOrReplace(obj); //only preferred listeners synch sol association
    if (obj == null) {
        System.out.println("DEBUG Magnitude  addOrReplace   return null");
        return false;
    }
    // Note if multiple mags of share same readings as found in Solution's list, changing it for one will not update all 
    // because the reading lists are only synched by listeners for the preferred magnitude for the event.
    if (sol != null && !(sol.magnitude == this)) {
        //System.out.println("DEBUG Magnitude  addOrReplace   sol with jc");
        sol.addOrReplace(jc); // force update of magnitude 11/28/30 -aww
    }
    return true;
  }
  /** Returns true if the input reference is removed from this Magnitude,
   * else returns false.  Removes without association check.
   */
  public boolean remove(JasiCommitableIF jc) {
    JasiCommitableListIF aList = getListFor(jc);
    return (aList != null) ? aList.remove(jc) : false;
  }
  /** Virtually deletes the input object
   *  ONLY if it is assigned to this Magnitude.
   *  Returns true if successful, else returns false.
   *  If in list, notifies list listener of state change.
   *  @see JasiCommitableIF#delete()
  */
  public boolean delete(JasiCommitableIF jc) {
    if (jc == null) return false;
    JasiMagnitudeAssociationIF obj = (JasiMagnitudeAssociationIF) jc;
    // delete only if assigned but don't check if in list: true == contains(obj);
    if (! obj.isAssignedTo(this)) return false;
    JasiCommitableListIF aList = getListFor(obj);
    // if in list, and state changes, list will fire event to listeners
    boolean retVal = (aList == null) ? false : aList.delete(obj);
    if (! retVal) obj.setDeleteFlag(true); // not in list, just assigned, delete it anyway
    return true;
  }
  /** Undo a virtual delete of the input object
   *  ONLY if it is assigned to this Magnitude.
   *  Returns true if successful, else returns false.
   *  @see JasiCommitableIF.undelete()
  */
  public boolean undelete(JasiCommitableIF jc) {
    if (jc == null) return false;
    JasiMagnitudeAssociationIF obj = (JasiMagnitudeAssociationIF) jc;
    // undelete only if assigned but don't check if in list: true == contains(obj);
    if (! obj.isAssignedTo(this)) return false;
    JasiCommitableListIF aList = getListFor(obj);
    // if in list, and state changes, list will fire event to listeners
    boolean retVal = (aList == null) ? false : aList.undelete(obj);
    if (! retVal) obj.setDeleteFlag(false); // not in list, just assigned, delete it anyway
    return true;
  }
  /** Convenience wrapper for unassociate(jc) and delete().
   * Removes input reference from both this Magnitude instance
   * its associated Solution instance, if any.
   * Returns true if input was assigned to this instance,
   * else false.
   **/
  public boolean erase(JasiCommitableIF jc) {
    // If assigned to this, delete it, always remove it from list.
    // Do delete(jc) first for assoc check, in case unassociate nulls assoc
    //
    //slower way two trips thru list:
    delete(jc); // set flags deleted, list does delete state notify
    boolean retVal = unassociate(jc);
    if (this.sol != null) this.sol.unassociate(jc);
    return retVal;
    //
    //alternative one list trip, but jc.delete doesn't notify yet:
    //return (unassociate(jc)) ?  jc.setDeleteFlag(true) : false;
    //
    //With implementation of MagnitudeAssocJasiReading
    //below alternative does the Solution removal too,
    //but jc.delete doesn't notify listeners yet:
    //return jc.erase(); // doesn't notify ?
  }
  /** Returns true if input reference is found in this Magnitude's data.
   */
  public boolean contains(JasiCommitableIF jc) {
    if (jc == null) return false;
    JasiCommitableListIF aList = getListFor(jc);
    // Do indexOfEquivalent(jc) instead of instance identity below ?
    return (aList == null) ? false : aList.contains(jc);
  }
  /** Returns the member data collection, if any in this Magnitude,
   * to which input would belong to, else returns null.
   */
  public JasiCommitableListIF getListFor(JasiCommitableIF jc) {
    // HashMap container could use subclass type as lookup key to retrieve list
    // xxxList aList = (xxxList) hashSet.get(subclass); // return aList;
    if (jc instanceof Amplitude && isAmpMag()) return getAmpList();
    else if (jc instanceof Coda && isCodaMag()) return getCodaList();
    else return null;
  }
  /** Returns collection of same data type as input,
   * if any such type is a data member of Magnitude, else
   * returns null.
   */
  public JasiCommitableListIF getListFor(JasiCommitableListIF jc) {
    // HashMap container could use subclass type as lookup key to retrieve list
    // xxxList aList = (xxxList) hashSet.get(subclass) ; return aList;
    if (jc instanceof AmpList && isAmpMag()) return getAmpList();
    else if (jc instanceof CodaList && isCodaMag()) return getCodaList();
    else return null;
  }
  /** Returns List of pertaining to the class type as input,
   * if any such type is a data member of Solution, else
   * returns null.
   */
  public JasiCommitableListIF getListFor(Class aClass) {
    if (isAmpMag()) {
      if ( aClass == Amplitude.class || aClass == AmpList.class ) return getAmpList();
    }
    else if (isCodaMag()) {
      if ( aClass == Coda.class      || aClass == CodaList.class ) return getCodaList();
    }
    return null;
  }

  /** Assigns this Magnitude to all elements of the input list,
   *  adding them to this Magnitude's corresponding list, if any.
   *  Virtually deleted elements are included.
   *  Returns count of newly added elements to Magnitude's list.
   */
  public int associate(JasiCommitableListIF aList) {
    MagnitudeAssociatedListIF myList = (MagnitudeAssociatedListIF) getListFor(aList);
    if (myList == null) return 0;
    int oldSize = myList.size();
    ((MagnitudeAssociatedListIF)aList).associateAllWith(this);
    return (myList.size() - oldSize);
  }
  /**
   * Adds to this Magnitude's corresponding reading list those input list
   * elements associated with this Magnitude not already present in the list.
   * Returns count of elements added.<p>
   * Use getReadingList().addOrReplaceAll(aList), to replace like kind with
   * kind.
   * @see #getReadingList()
   * @see #addOrReplaceAll(JasiCommitableListIF)
   */
  public int addAssociatedData(JasiCommitableListIF aList) {
    MagnitudeAssociatedListIF myList =
       (MagnitudeAssociatedListIF) getListFor(aList);
    if (myList == null) return 0;
    MagnitudeAssociatedListIF addList =
      ((MagnitudeAssociatedListIF)aList).getAssociatedWith(this);
    int oldSize = myList.size();
    myList.addAll(addList); // addAll tests by equals() not equivalent().
    return (myList.size() - oldSize);
  }
  /** Assigns this Magnitude to each
   * JasiMagnitudeAssociationIF element
   * of the input list not associated with
   * a any Magnitude collection.
   * @see #associateAll(List)
   */
  public void assignToUnassociated(List aList) {
    if (aList == null || aList.isEmpty()) return;
    int count = aList.size();
    // do a isTypeFor(jc) in loop here ?
    for (int idx =0; idx < count; idx++) {
      try {
        ((JasiMagnitudeAssociationIF) aList.get(idx)).assign(this);
      }
      catch (IllegalArgumentException ex) {
        System.err.println(ex.getMessage());
      }
    }
  }
  /*
   * Associates each JasiMagnitudeAssociationIF element
   * of the input list with this Magnitude.
   * @see #assignToUnassociated(List)
   */
  public void associateAll(List aList) {
    if (aList == null || aList.isEmpty()) return;
    int count = aList.size();
    for (int idx =0; idx < count; idx++) {
    // do a isTypeFor(jc) in loop here ?
      ((JasiMagnitudeAssociationIF) aList.get(idx)).associate(this);
    }
  }

  // Programmer convenience for loading unique data
  // such as when corresponding data list of this Magnitude
  // is empty or known not to have data elements in input,
  // thus filtering of the input list would just waste time.
  // Does require the Magnitude reference of input elements
  // to be either this Magnitude or null (virgin).
  // Requires the associated Solution of the input list elements
  // to be null or to have been pre-associated with this Magnitude's Solution.
  // Could have assignment unassociate different prior referencees?
  public void fastAssociateAll(List aList) {
    if (aList == null || aList.isEmpty()) return;
    int count = aList.size();
    //assignToUnassociated(aList);
    JasiMagnitudeAssociationIF jc = null;
    for (int idx =0; idx < count; idx++) {
    // do a isTypeFor(jc) in loop here ?
      jc = ((JasiMagnitudeAssociationIF) aList.get(idx));
      jc.assign(this.sol); // exception, if prior assignment not this.sol or null
      jc.assign(this); // exception, if prior assignment not this Magnitude or null
    }
    ActiveArrayList myList = (ActiveArrayList) getListFor(jc);
    myList.fastAddAll(aList); // no filtering out of nulls or duplicates
  }

  public void associate(List aList) {
      if (aList == null) return; // could treat as error ?
      // do a isTypeFor(jc) in loop here ?
      JasiMagnitudeAssociationIF jc = (JasiMagnitudeAssociationIF) aList.get(0);
      if (jc instanceof Coda && ! isCodaMag()) {
        System.err.println(getClass().getName()+" WARNING associateList input mag is not Coda subtype!");
      } else if (jc instanceof Amplitude && ! isAmpMag()) {
        System.err.println(getClass().getName()+" WARNING associateList input mag is not Amp subtype!");
      }

      JasiCommitableListIF jcl = getListFor(jc);
      if (jcl == null) {
        System.err.println(getClass().getName()+".associate(List) no internal list for type: " + jc.getClass().getName());
        return; // could treat as error ?
      }

      if (jcl.isEmpty()) {
        fastAssociateAll(aList); // does not filter input elements
      }
      else {
        associateAll(aList); // filters input for dupes or nulls
      }
  }

  /**
   * Replaces in this Magnitude's corresponding reading list those input
   * list elements associated with this Magnitude that are equivalent,
   * otherwise adds those associated to the list.
   * @see #getReadingList()
   * @see #addAssociatedData(JasiCommitableListIF)
   */
  public void addOrReplaceAll(JasiCommitableListIF aList) {
    MagnitudeAssociatedListIF myList =
       (MagnitudeAssociatedListIF) getListFor(aList);
    if (myList == null) return;
    myList.addOrReplaceAll(
      ((MagnitudeAssociatedListIF) aList).getAssociatedWith(this)
    );
  }

  /** Adds ListDataState listeners to the data lists of the associated Solution.
   *  Listeners monitors changes in the Solution's lists and may
   *  update correponding data lists of this Magnitude.
   * */
  protected void addSolDataStateListeners(Solution sol) {
  // since mag subtype change would have to work with different list, have use all
    if (sol == null) return;
    ActiveArrayList aList = sol.getCodaList();
    if (aList != null) aList.addListDataStateListener(solCodaDataStateListener);
    aList = sol.getAmpList();
    if (aList != null)  aList.addListDataStateListener(solAmpDataStateListener);
  }
  /** Removes ListDataState listeners from the data lists of the associated Solution.
   *  Listeners monitored changes in the Solution's lists to enable
   *  updates of any correponding data lists of the this Magnitude.
   * */
  protected void removeSolDataStateListeners(Solution sol) {
  // since mag subtype change would have to work with different list, have use all
    if (sol == null) return;
    ActiveArrayList aList = sol.getCodaList();
    if (aList != null) aList.removeListDataStateListener(solCodaDataStateListener);
    aList = sol.getAmpList();
    if (aList != null) aList.removeListDataStateListener(solAmpDataStateListener);
  }
  /** Adds listeners to internal data lists to monitor changes which set state stale. */
  protected void addMagDataListeners() {
  // since mag subtype change would have to work with different list, have use all
    removeMagDataListeners();
    if (codaList != null) codaList.addListDataStateListener(myReadingsDataStateListener);
    if (ampList != null) ampList.addListDataStateListener(myReadingsDataStateListener);
  }
  /** Removes listeners from internal data lists monitoring the changes effecting stale state. */
  protected void removeMagDataListeners() {
  // since mag subtype change would have to work with different list, have use all
    if (codaList != null) codaList.removeListDataStateListener(myReadingsDataStateListener);
    if (ampList != null) ampList.removeListDataStateListener(myReadingsDataStateListener);
  }

// IF ListDataStateListener
  // TODO: different ListDataStateListener instances for different mag subtypes
  // Need subclasses since adding/removing coda should not effect amp mags and vice versa.
  // NOTE: loading unique object instances in Lists will not equate with those in solution.
  // only equivalent replacement works to avoid redundancy!

  protected class ReadingListDataStateListener implements ListDataStateListener {
    public void intervalAdded(ListDataStateEvent e) {
      //System.out.println("Mag - my list interval added in : " + e.getSource().getClass().getName());
      setStale(true); // global, no check if added reading's deletion state effects stale state. aww
    }
    public void intervalRemoved(ListDataStateEvent e) {
      //System.out.println("Mag - my list interval removed in : " + e.getSource().getClass().getName());
      setStale(true);  // no check for deletion state
    }
    public void contentsChanged(ListDataStateEvent e) { // entire collection added,removed or element replaced
      //System.out.println("Mag - my list contents changed in : " + e.getSource().getClass().getName());
      setStale(true);  // no check for deletion state
    }
    public void stateChanged(ListDataStateEvent e) { // entire collection added,removed or element replaced
      StateChange sc = e.getStateChange();
      if (sc.getStateName().equals("deleted")) {
        //System.out.println("Mag - my list delete state changed in : " + e.getSource().getClass().getName());
        setStale(true);  // no check for deletion state
      }
    }
    public void orderChanged(ListDataStateEvent e) {}
  }//end of ReadingListDataStateListener class

  protected void listenToSolReadingListAdd(boolean  tf) {
    listenToSolReadingListAdd    = tf;
  }
  protected void listenToSolReadingListRemove(boolean  tf) {
    listenToSolReadingListRemove = tf;
  }
  protected void listenToSolReadingListState(boolean  tf) {
    listenToSolReadingListState  = tf;
  }

  // Feedback of events mag-sol, sol-mag by add/replace, clear etc. on list
  // until list elements are (==) identical objects.
  // NOTE: Collections.sort(list) invokes list.set()
  // NOTE: Unique instances in data Lists will not equate with equals().
  protected class SolReadingsDataStateListener implements ListDataStateListener {
    // Should adding to sol list force stale state or add to the mag list ?
    // Solution additions only effects state if they are associatedWith this mag.
    // To add, check sol src list additions for their mag association and type.
    // Since a mag list add fires event to inform like sol list, if listenToSolReadingListxxx
    // is enabled here sol list fires event back same mag list to check for object match.
    // Thus there is alot of overhead in synching the lists.
    // If interval of rows added to end of sol list, list.addAll() invokes add() for each.
    public void intervalAdded(ListDataStateEvent e) {
      if (! listenToSolReadingListAdd) return;
      //System.out.println("Mag - interval added in sol list : " + e.getSource().getClass().getName());
      int first = e.getIndex0();
      int last = e.getIndex1();
      MagnitudeAssociatedListIF srcList = (MagnitudeAssociatedListIF) e.getSource();
      if (first > -1 && last >= first) {
        // if addOrReplace implementation requires association match then:
        for (int idx = 0; idx<=last; idx++) {
    // do a isTypeFor(jc) in loop here ?
          ((JasiCommitableListIF)getListFor(srcList)).addOrReplace((JasiCommitableIF)srcList.get(idx));
        }
        return;
      }
      Object[] theAdded = (Object []) e.getStateChange().getValue();
      for (int idx = 0; idx<=theAdded.length; idx++) {
        ((JasiCommitableListIF)getListFor(srcList)).addOrReplace((JasiCommitableIF)theAdded[idx]);
      }
    }
    // What does removing from sol list imply about mag data, does it means the data is destroyed, unwanted in db?
    public void intervalRemoved(ListDataStateEvent e) {
      if (! listenToSolReadingListRemove) return;
      //System.out.println("Mag - my codalist interval removed via sol : " + e.getSource().getClass().getName());
      // local list elements may not be at same indexes order for src sublist elements to use:
      // this.List.removeAll(((List) e.getSource()).subList(first,last+1));
      // NOTE: unique instances in mag,sol List will not equate, must getLike.
      // thus retainAll only works if contains() is not simply indexOf() object identity via equals():
      //this.codaList.retainAll((List)e.getSource()); // different list instances might not be "==".
      // event StateChange value holds references to removed elements
      // if those removed from sol list are associated with this mag
      int first = e.getIndex0();
      int last = e.getIndex1();
      MagnitudeAssociatedListIF srcList = (MagnitudeAssociatedListIF) e.getSource();
      //requires time,sol,mag scan of ENTIRE src list for EACH mag element, too much time?:
      //((JasiCommitableListIF)getListFor(srcList)).retainEquivalent(srcList);
      Object data = e.getStateChange().getValue();
      // have to use the "removed" element returned in the StateChange
      if (first > -1 && last == first) {
        ((JasiCommitableListIF)getListFor(srcList)).remove((JasiCommitableIF)data);
        return;
      }
      // assume array of removed elements scattered in list:
      Object [] theRemoved = (Object []) data;
      for (int idx = 0; idx<=theRemoved.length; idx++) {
        ((JasiCommitableListIF)getListFor(srcList)).remove((JasiCommitableIF)theRemoved[idx]);
      }
    }
    // Avoid editing the readings effecting mag in sol lists and contentsChanged case is not needed
    public void contentsChanged(ListDataStateEvent e) {
      if (! listenToSolReadingListAdd) return;
      //System.out.println("Mag - my codalist contents updated via sol : " + e.getSource().getClass().getName());
      int first = e.getIndex0();
      int last = e.getIndex1();
      if (first > -1 && last == first) { // probably set() fired event
    // do a isTypeFor(jc) in loop here ?
        addOrReplace((JasiCommitableIF)((MagnitudeAssociatedListIF) e.getSource()).get(first));
      }
      // really would need to compare both lists here to synchronize contents.
      else { // may or may not be true, list contents may be bogus.
        MagnitudeAssociatedListIF srcList = (MagnitudeAssociatedListIF) e.getSource();
        ((JasiCommitableListIF)getListFor(srcList)).retainEquivalent(srcList);
      }
    }
    // Avoid editing state of the readings effecting mag in sol lists
    // and stateChanged case is not needed here
    public void stateChanged(ListDataStateEvent e) { // collection element states modified
      if (! listenToSolReadingListState) return;
      //System.out.println("Mag - my list state changed via sol : " + e.getSource().getClass().getName());
      int first = e.getIndex0();
      int last = e.getIndex1();
      SolutionAssociatedListIF srcList = (SolutionAssociatedListIF) e.getSource();
      if (first > -1 && last >= first) {
        StateChange sc = e.getStateChange();
        if (sc.getStateName().equals("deleted")) {
          if (sc.getValue() == Boolean.TRUE)
            // matches time, sol, and mag for same err?
            ((JasiCommitableListIF)getListFor(srcList)).deleteAll(srcList.subList(first,last+1));
          else if (sc.getValue() == Boolean.FALSE)
            // match time, sol, and mag for same err?
            ((JasiCommitableListIF)getListFor(srcList)).undeleteAll(srcList.subList(first,last+1));
        }
      }
      else throw new IndexOutOfBoundsException("List StateChange undefined range:"+first+","+last);
      // else do what with sol srcList?  retainEquivalent? addOrReplaceAll?
    }
    public void orderChanged(ListDataStateEvent e) {}
  }//end of SolReadingsDataStateListener class

    /** Returns a string containing Magnitude value (to 3 decimal places) and its type (e.g. "4.333 Ml"). */
    public String toString() {
      StringBuffer sb = new StringBuffer(12);
      String str = (value.isNull()) ? "NaN" : new Format("%5.3f").form(value.doubleValue());
      return sb.append(str).append(" ").append(getTypeString()).toString();
    }
    /**
     * String representation of magnitude calculation relevant data members of this class.
    */
    public String toDumpString() {
      // aww 02/14/2004 shortened desc of longer field names.
      Format df2 = new Format("%5.2f");
      StringBuffer sb = new StringBuffer(180);
      sb.append("id=").append(magid.isNull() ? "-none-" : magid.toString()).append(" ");
      sb.append(getTypeString()).append("=");
      sb.append(df2.form(value.doubleValue())).append(" ");
      sb.append("auth=").append(authority.toString()).append(" ");
      sb.append("src=").append(source.toString()).append(" ");
      sb.append("algor=").append(algorithm.toString()).append(" ");
      // getReadingsXXX() require a "loaded" reading list else return 0
      sb.append("n=").append(getReadingsCount()).append(" ");
      sb.append("usedChn=").append(getReadingsUsed()).append(" ");
      // whereas getStationsUsed() value may have been read from NETMAG row 
      sb.append("usedSta=").append(getStationsUsed()).append(" ");
      sb.append("err=").append(df2.form(error.doubleValue())).append(" ");
      sb.append("gap=").append(df2.form(gap.doubleValue())).append(" ");
      sb.append("dist=").append(df2.form(distance.doubleValue())).append(" ");
      sb.append("qual=").append(quality.toString()).append(" ");
      sb.append("state=").append(processingState.toString()).append(" ");
      sb.append("who=").append(who.toString());
      return sb.toString();
    }
    /*
      Return a fixed format string of the form:
<tt>
  Event ID     mag     type   source       algorithm auth #sta #chl   error     gap    dist    qual  isPref     who
dddddddddd ffff.ff ssssssss ssssssss sssssssssssssss ss   dddd dddd ffff.ff ffff.ff ffff.ff ffff.ff  true/false nameId
</tt>
You must call getNeatStringHeader() to get the header line shown above.

@see getNeatStringHeader()
    */
    public String toNeatString() {
        Format df1 = new Format("%7.2f");
        Format sf2 = new Format("%8s");
        Format sf3 = new Format("%15s");
        Format sf4 = new Format("%6s");
        Format df4 = new Format("%4d");

        String idStr = null;
        if (sol == null || sol.id.isNull()) {
            idStr = "  -none-  ";
        } else {
            Format df0 = new Format("%10d");
            idStr = df0.form(sol.id.longValue());
        }

        StringBuffer sb = new StringBuffer(132);
        sb.append(idStr).append(" ");
        sb.append(df1.form(value.doubleValue())).append(" ");
        sb.append(sf2.form(getTypeString())).append(" ");
        sb.append(sf2.form(source.toString())).append(" ");
        String meth = algorithm.toString();
        sb.append(sf3.form(meth.substring(0,Math.min(meth.length(),15)))).append(" ");
        idStr = getAuthority();
        sb.append(idStr.substring(0, Math.min(3, idStr.length()))).append("   ");
        sb.append(df4.form(getStationsUsed())).append(" ");
        sb.append(df4.form(getReadingsUsed())).append(" ");
        sb.append(df1.form(error.doubleValue())).append(" ");
        sb.append(df1.form(gap.doubleValue())).append(" ");
        sb.append(df1.form(distance.doubleValue())).append(" ");
        sb.append(df1.form(quality.doubleValue())).append(" ");
        sb.append(sf4.form(String.valueOf(isPreferred()))).append(" ");
        sb.append(sf4.form(String.valueOf(isStale()))).append(" ");
        sb.append(sf4.form(who.toString())).append(" ");
        if (adjValue.isValidNumber()) sb.append(df1.form(adjValue.doubleValue())).append(" ").append(getTypeString()+"r");
        return sb.toString();

    }
    /*
      Return a fixed format header to match output from toNeatString(). Has the form:
<tt>
  Event ID     mag     type   source       algorithm #sta   error     gap    dist    qual  prefer  stale    who
dddddddddd ffff.ff ssssssss ssssssss sssssssssssssss dddd ffff.ff ffff.ff ffff.ff ffff.ff  ssssss ssssss ssssss
</tt>
@see: toNeatString()
    */
    public static String getNeatStringHeader() {
        StringBuffer sb = new StringBuffer(132);
        sb.append("  Event ID     mag     type   source       algorithm ");
        sb.append("auth #sta #chl   error     gap    dist    qual ");
        sb.append("prefer  stale    who");
        return sb.toString();
    }

    public String getNeatHeader() { return getNeatStringHeader(); }

    /**
     * Return 2 character Magnitude type descriptive string of the form: "Ml", "Mb", etc.
     * If the type is null returns "M "
     */
    public String getTypeString() {
        return "M"+ getTypeSubString();
    }

    /**
     * Set 1-character Magnitude type descriptive character: e.g. "l", "b", etc.
     */
    public void setType(String magType) {
        subScript.setValue(magType);
    }

    /**
     * Return 1 character Magnitude type string of the form: "l", "b", etc.
     * If the type is null returns " "
     */
    public String getTypeSubString() {
        String sub = subScript.toString();
        return (NullValueDb.isEmpty(sub)) ?  " " : sub;
    }

    /** Return a DataString containing the last analyst to locate this event.
     *  May be null. */
    public DataString getWho() {
       return who;
    }
    /** Set name of the last analyst to locate this event. */
    public void setWho(String name) {
      who.setValue(name);
    }
    /** Set name of the last analyst to locate this event. */
    public void setWho(DataString name) {
      who.setValue(name);
    }

    /** Return a String with the magnitude and the list of amplitudes or codas.*/
    public String neatDump() {
        StringBuffer sb = new StringBuffer(ampList.size()*132+codaList.size()*132+256);
        sb.append(getNeatStringHeader()).append("\n");
        sb.append(toNeatString());
        if (!ampList.isEmpty()) {
          ampList.distanceSort();
          sb.append("\n").append(ampList.toNeatString());
        }
        if (!codaList.isEmpty()) {
          codaList.distanceSort();
          sb.append("\n").append(codaList.toNeatString());
        }
        return sb.toString();
    }

} // end of Magnitude class
