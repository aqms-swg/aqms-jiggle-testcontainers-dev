package org.trinet.jasi;

import org.trinet.util.GenericPropertyList;

/**
 * ChannelTimeWindowModel that returns a list of ChannelTimeWindow that is
 * derived from the waveforms associated with this solution in the DataSource.
 */

public class MdDataSourceChannelTimeModel extends DataSourceChannelTimeModel {

    {
        setModelName( "Md DataSource");
        setExplanation("channels associated with both Md codas and database waveforms");
        chanIncludeList = new String [] {"HHZ","EHZ","SHZ"};
    }

    public MdDataSourceChannelTimeModel() {
        //setMyDefaultProperties();
        super();
    }

    public MdDataSourceChannelTimeModel(GenericPropertyList gpl, Solution sol, ChannelableList candidateList) {
        super(gpl, sol, candidateList);
    }

    public MdDataSourceChannelTimeModel(Solution sol, ChannelableList candidateList) {
        super(sol, candidateList);
    }

    public MdDataSourceChannelTimeModel(ChannelableList candidateList) {
        super(candidateList);
    }

    public MdDataSourceChannelTimeModel(Solution sol) {
        this();
        super.setSolution(sol);
    }

}
