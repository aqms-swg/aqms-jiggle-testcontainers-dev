package org.trinet.jasi;

import java.util.*;

import org.trinet.util.*;

/**
 * Abstract parent of ChannelTimeWindowModels that return a list of ChannelTimeWindow that 
 * are likely to show seismic energy above the noise level. 
*/
public class MissingPickPowerLawTimeWindowModel extends PowerLawTimeWindowModel {

    // Note: extended class must set new model name otherwise it's "equal()" to its parent in list lookup via contains()
    public static final String defModelName = "MissingPickPowerLaw";
    public static final String defExplanation =
        "channels w/o picks whose dist <= k*(magnitude**p), where k,p are set by model properties";

    {
      setModelName(defModelName);
      setExplanation(defExplanation);
    }

    public MissingPickPowerLawTimeWindowModel() { }

    public MissingPickPowerLawTimeWindowModel(GenericPropertyList gpl, Solution sol, ChannelableList candidateList) {
        super(gpl, sol, candidateList);
    }
    public MissingPickPowerLawTimeWindowModel(Solution sol, ChannelableList candidateList) {
        super(sol, candidateList);
    }
    public MissingPickPowerLawTimeWindowModel(ChannelableList candidateList) {
        super(candidateList);
    }
    public MissingPickPowerLawTimeWindowModel(Solution sol) {
        super(sol);
    }

    public ChannelableList getChannelTimeWindowList() {
        if ( ! (includePhases || includeMagAmps || includeCodas || includePeakAmps) ) {
          System.err.println(getModelName() + " : ERROR Required model include properties are not set, no-op");
        }
        return super.getChannelTimeWindowList();
    }

    // actually "removes" channels in requiredList from input aList
    protected void addRequiredChannelsToList(ChannelableList aList) {

        setRequiredChannelList(new ChannelableList()); // adds channels with picks etc. if include properties are set. 

        if (requiredList == null || requiredList.size() == 0 ) return;
        int count = requiredList.size();
        Channelable ch = null;
        int idx = -1;
        for (int ii = 0; ii < count; ii++) {
            ch = (Channelable) requiredList.get(ii);
            idx = aList.getIndexOf(ch);
            if (idx >= 0) aList.remove(idx); // removes only those with picks of desired type
        }
    }

    public void setDefaultProperties() {
        super.setDefaultProperties();
        setMyDefaultProperties();
    }

    private void setMyDefaultProperties() {
        includePhases = true;   // default is phases only
        //includeAllComponents = true; // done in super class init
        includeMagAmps = false;
        includePeakAmps = false;
        includeCodas = false;
    }

}
