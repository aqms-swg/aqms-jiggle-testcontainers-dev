package org.trinet.jasi;

import java.util.*;

import org.trinet.util.*;

/**
 * Model that returns a set of ChannelTimeWindows that represent where seismic
 * energy is most likely to be found found in model's candidate list channel set
 * but whose channels are missing derived parametric like picks (or amps etc).
 * Default is a uses a mag/distance relationship defined by H. Kanamori for Southern California:<br>
 * <b>cutoff km = 170.0 * magnitude - 205.0</b>.
 * There is no account taken of channel gain in this model.
 */

public class MissingPickSimpleChannelTimeModel extends SimpleChannelTimeModel {

  public static final String defModelName = "MissingPickSimpleMagCutoff";

  /** A string with an brief explanation of the model. For help and tooltips. */
  public static final String defExplanation =
      "channels w/o picks whose dist <= a * magnitude - b, where a,b are set by properties";

  {
    setModelName(defModelName);
    setExplanation(defExplanation);
  }

  public MissingPickSimpleChannelTimeModel() {
      setMyDefaultProperties();
  }

  public MissingPickSimpleChannelTimeModel(GenericPropertyList gpl, Solution sol, ChannelableList candidateList) {
      super(gpl, sol, candidateList);
  }

  public MissingPickSimpleChannelTimeModel(Solution sol, ChannelableList candidateList) {
      super(sol, candidateList);
  }
  public MissingPickSimpleChannelTimeModel(ChannelableList candidateList) {
      super(candidateList);
  }
  public MissingPickSimpleChannelTimeModel(Solution sol) {
      this();
      super.setSolution(sol);
  }

  public ChannelableList getChannelTimeWindowList() {
        if ( ! (includePhases || includeMagAmps || includeCodas || includePeakAmps) ) {
          System.err.println(getModelName() + " : ERROR Required model include properties are not set, no-op");
        }
        return super.getChannelTimeWindowList();
  }

  protected void addRequiredChannelsToList(ChannelableList aList) {
        setRequiredChannelList(new ChannelableList());  // loads and sorts include reading channels by distance
        if (requiredList == null || requiredList.size() == 0 ) return;
        int count = requiredList.size();
        Channelable ch = null;
        int idx = -1;
        for (int ii = 0; ii < count; ii++) {
            ch = (Channelable) requiredList.get(ii);
            idx = aList.getIndexOf(ch);
            if (idx >= 0) aList.remove(idx); // removes only those with picks of desired type
        }
  }

  public void setDefaultProperties() {
      super.setDefaultProperties();
      setMyDefaultProperties();
  }

  private void setMyDefaultProperties() {
      minDistance = defMinDist; // dist. calc could produce a negative number without this
      magDistIntercept = defMagDistIntercept;
      magDistSlope = defMagDistSlope;
      includePhases = true;
      includeMagAmps = false;
      includePeakAmps = false;
      includeCodas = false;
  }
}
