package org.trinet.jasi;

import java.util.*;

import org.trinet.util.*;

//import org.trinet.jiggle.JiggleProperties;  // for the main testing

/**
   Model for determining the set of channels and time-windows likely to have
   signal above the noise level. <p>


   The default inclusion threshold is 200 counts.<br>
   The default include all magnitude threshold is M >= 3.0<p>

   Minimum window duration (secs) =  40.0<br>
   Maximum window duration (secs)  = 600.0 (this will truncate codas > ~ML 5.0)<br>
   Secs added to start of time window calculated by the model algorithm = 20.0<br>
   Secs added to duration of time window calculated by the model algorithm  = 20.0 <br>
   Maximum horizontal map distance allowed for an included Channel = 1000.0<br>

*/
public class NCChannelTimeWindowModel extends AbstractSignalTimeWindowModel {

    public static final String defModelName = "NC Model";

    /** A string with an brief explanation of the model. For help and tooltips. */
    public static final String defExplanation =
        "channels with seismic energy predicted by the Northern California energy model";

    /** Minimum window duration (secs). Some algorithms may need a minimum amount of data.*/
    public static final double defMinWindow    =  40.0;

    /** Minimum window duration (secs).*/
    public static final double defMaxWindow    = 600.0;  // this will truncate codas > ~5.0

    /** Time added to start of time window calculated by the model algorithm (secs).*/
    public static final double defPreEvent     =  20.0;

    /** Time added to duration of time window calculated by the model algorithm (secs).*/
    public static final double defPostEvent    =  20.0;

    /** The maximum horizontal map distance allowed by the getDistanceCutoff() method.
     *  All channel beyond this distance will be excluded from the Channel set.*/
    public static final double defMaxDistance = 1000.0;

    /** Include all components (orientations) of a channel type if any is included. */
    public static final boolean defIncludeAllComponents = true;

    /** Include all channels if the magnitude >= this value.*/
    public static final double defIncludeAllMag = 3.0;

    /** Slope of log(mag) vs log(dist) for acceleration channels */
    public static final double defAccMagDecrement = 2.56;
    protected static double accMagDecrement = defAccMagDecrement;

    /** Slope of log(mag) vs log(dist) for velocity and other non-acceleration channels */
    public static final double defVelMagDecrement = 2.56;
    protected static double velMagDecrement = defVelMagDecrement;

    /** Threshold for reduced log(mag) for acceleration channels */
    public static final double defAccThreshold = 1.67;
    protected static double accThreshold = defAccThreshold;
    
    /** Threshold for reduced log(mag) for velocity and other non-acceleration channels */
    public static final double defVelThreshold = 1.67;
    protected static double velThreshold = defVelThreshold;
    
    // Array of time-code channel names
    //protected String timeChannelNames[] = null;
    
    // Array of accelerometer (low-gain) channels
    protected String accelerometerChannels[] = null;

    // set defaults for this model
    {
      setModelName(defModelName);
      setExplanation(defExplanation);
    }

    public NCChannelTimeWindowModel() {
        setMyDefaultProperties();
    }

    public NCChannelTimeWindowModel(GenericPropertyList gpl, Solution sol, ChannelableList candidateList) {
        super(gpl, sol, candidateList);
    }
    public NCChannelTimeWindowModel(Solution sol, ChannelableList candidateList) {
        super(sol, candidateList);
    }

    public NCChannelTimeWindowModel(ChannelableList candidateList) {
        super(candidateList);
    }

    public NCChannelTimeWindowModel(Solution sol) {
        this();
        super.setSolution(sol);
    }

    /** Set the reduced log(mag) threshold for acceleration channels */
    public void setAccThreshold(double threshold) {
        accThreshold = threshold;
    }

    /** Return the reduced log(mag) threshold for acceleration channels */
    public double getAccThreshold() {
        return accThreshold;
    }

    /** Set the reduced log(mag) threshold for velocity and other non- acceleration channels */
    public void setVelThreshold(double threshold) {
        velThreshold = threshold;
    }

    /** Return the reduced log(mag) threshold for velocity and other non-acceleration channels */
    public double getVelThreshold() {
        return velThreshold;
    }


    /** Set the slope of log(mag) vs log(dist) for acceleration channels */
    public void setAccMagDecrement(double value) {
        accMagDecrement = value;
    }

    /** Return the reduced log(mag) threshold for acceleration channels */
    public double getAccMagDecrement() {
        return accMagDecrement;
    }

    /** Set the slope of log(mag) vs log(dist) for velocity and other non-acceleration channels */
    public void setVelMagDecrement(double value) {
        velMagDecrement = value;
    }

    /** Return the reduced log(mag) threshold for velocity and other non-acceleration channels */
    public double getVelMagDecrement() {
        return velMagDecrement;
    }
/**
 * Set list of 3-character channel codes that will ALWAYS be included in the list.
 * 
 * @param string
    public void setTimeChannelNames(String string) {
        if (string == null) return;
        String delList = " ,:;\t\n" ;  // delimiter list - added whitespace -aww 2009/08/20
        StringTokenizer strTok = new StringTokenizer(string, delList);
        ActiveList list = new ActiveList(); // doesn't allow duplicate add
    
        try {
            while (strTok.hasMoreTokens()) {
                list.add(strTok.nextToken().trim());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.err.println("Bad syntax in timeChannelNames property string:" +string);
        }
        if (!list.isEmpty())
            timeChannelNames = (String []) list.toArray(new String[list.size()]);

    }
    
    public String[] getTimeChannelNames() {
        return timeChannelNames;
    }
 */
 
    /** 
     * Define a list of channel codes that will be treated as
     * accelerometers. Specify the first two characters only 
     * E.g. "BL, CL, EL, HL, BN, HN, HG, BV, HV".
     * Allows handeling of non-SEED compliant channel names.
     * @param string
     */
    public void setAccelerometerChannels(String string) {
        if (string == null) return;
        String delList = " ,:;\t\n" ;  // delimiter list
        StringTokenizer strTok = new StringTokenizer(string, delList);
        ActiveList list = new ActiveList();
    
        try {
            while (strTok.hasMoreTokens()) {
                list.add(strTok.nextToken().trim());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.err.println("Bad syntax in setAccelerometerChannels property string:" +string);
        }
        if (!list.isEmpty()) {
            accelerometerChannels = (String []) list.toArray(new String[list.size()]);
        }
    }
    
    public String[] getAccelerometerChannels() {
        return accelerometerChannels;
    }
    
/** Return a List of ChannelTimeWindow objects selected by the model.
     *  Override of superclass method adds time component channels to list 
     *  whenever a seismic channels exist and time components are defined by property.
    public ChannelableList getChannelTimeWindowList() {

        ChannelableList chList = super.getChannelTimeWindowList();
        if (chList == null) return null;

        if (chList.isEmpty()) return chList;
        
        // Collect the time channels
        if (timeChannelNames == null || timeChannelNames.length == 0) return chList;

        ChannelList timeChannels = ChannelList.getByComponent(timeChannelNames);
        if (timeChannels.isEmpty()) {
            System.out.println(getModelName() + " : no time channels configured.");
        }
        else {
            if (debug) System.out.println(getModelName() + " DEBUG adding "+timeChannels.size()+ " time channels");
            // Find the earliest start and latest end times in the windows
            int count = chList.size();
            ChannelTimeWindow ctw = null;
            TimeSpan maxSpan = new TimeSpan();
            for (int i = 0; i < count; i++) {
              ctw = (ChannelTimeWindow) chList.get(i);
              maxSpan.include(ctw.getTimeSpan());
            }

            count = timeChannels.size();
            for (int i = 0; i < count; i++) {
              chList.add(new ChannelTimeWindow(((Channelable)timeChannels.get(i)).getChannelObj(), maxSpan));
            }
        }
       
        // Not sure about filtering again after adding "time" seedchan CTW -aww 2008/09/26
        //filterListByChannelPropertyAttributes(chList);
        //if (filterWfListByChannelList) chList = filterListByChannelList(chList);

        return (chList.size() > getMaxChannels()) ? new ChannelableList(chList.subList(0,getMaxChannels())) : chList;
    }
*/

    /**
     * Implement the Northern California log-distance reduction function.
     * Return's true if the channel should be archived.
     * @param ch
     * @param mag
     * @param dist
     * @param z
     * @return
     */
    public boolean shouldShowEnergy(Channel ch, double mag, double dist, double z) {

        boolean isVelocity = true;
        double threshold = 0.;
        String seedChan = ch.getSeedchan();
        String str = ch.toDelimitedNameString(".")+"  dist = "+ dist;
        
        /*
         * NOTE: (DDG) 4/07 - should probably call Channel.isAcceleration() & isVelocity(), 
         * which uses the gain units value to determine instrument type,
         * and use the property only for non-SEED channel strings. 
         * As it stands you must define ALL possible accel channels in the property.
         * 
         * In reality, this only matters if vel and accel have different threshold and
         * decrement values.
         */
    
        /* Accelerometer channels: can't look for "_L" because "EL_ channels are velocity 
         * List defined with 'accelerometerChannels' property. */
        // This would not work if NC & SC exchanges "EL_" data; its acceleratation in SC 
        // and velocity in NC.
        if (accelerometerChannels != null) {
            for (int i = 0; i < accelerometerChannels.length; i++) {
                if (accelerometerChannels[i].length() > 0 && 
                        seedChan.startsWith(accelerometerChannels[i])) {
                    isVelocity = false;
                    break;
                }
            }
        }
        if (isVelocity) {
            threshold = mag - velMagDecrement * logten(dist) - velThreshold;
        } else {
            threshold = mag - accMagDecrement * logten(dist) - accThreshold;
        }
        if (threshold > 0.0) {
            if (debug) System.out.println(getModelName() + " DEBUG + "+str+ " threshold = "+ threshold);
            return true;
        } else {
            if (debug) System.out.println(getModelName() + " DEBUG - "+str+ " threshold = "+ threshold);
            return false;
        }
    }
    /**
     * Return the log, base10 of 'val'. Strangely, Java's Math package
     * does not include a base-10 log function.
     * @param val
     * @return
     */
    static private double logten( double val) {     
        final double logOf10 = Math.log(10.0);   // natural log of 10

        if (val > 0) {
            return Math.log(val) / logOf10;     //ln(val)/ln(10) = log10 of val
        } else {    
            return 0.0;
        }
    }


    /** Setup model behavior parameters using values in a property list.
     * Assumes each property has a prefix matching the string returned by getClassname().
     * Override this to set properties specific to a model.
     * In the new method call super.setProperties(props) first to get the standard stuff.<p>
     * <pre>
     * For example:
     * You have a class called org.trinet.jasi.NCChannelTimeWindowModel:
     *
     * Properties would define as follows:
     *
     org.trinet.jasi.NCChannelTimeWindowModel.minDistance=20
     org.trinet.jasi.NCChannelTimeWindowModel.maxDistance=500
     org.trinet.jasi.NCChannelTimeWindowModel.minWindowSize=30
     org.trinet.jasi.NCChannelTimeWindowModel.maxWindowSize=300
     org.trinet.jasi.NCChannelTimeWindowModel.preEventSize=20
     org.trinet.jasi.NCChannelTimeWindowModel.postEventSize=30
     org.trinet.jasi.NCChannelTimeWindowModel.includeAllComponents=true
     org.trinet.jasi.NCChannelTimeWindowModel.includeAllMag=3.5
     org.trinet.jasi.NCChannelTimeWindowModel.maxChannels=9999

     This method adds to the list, these properties :

     org.trinet.jasi.NCChannelTimeWindowModel.velThreshold
     org.trinet.jasi.NCChannelTimeWindowModel.accThreshold
     org.trinet.jasi.NCChannelTimeWindowModel.velMagDecrement
     org.trinet.jasi.NCChannelTimeWindowModel.accMagDecrement
     org.trinet.jasi.NCChannelTimeWindowModel.timeChannelNames
     org.trinet.jasi.NCChannelTimeWindowModel.accelerometerChannels

     * */
    public void setProperties(GenericPropertyList props) {

        super.setProperties(props);   /// get all the standard stuff
        if (props == null) return;

        String pre = getPropertyPrefix();
        if (props.isSpecified(pre+"velThreshold") )
            setVelThreshold((double)props.getDouble(pre+"velThreshold"));

        if (props.isSpecified(pre+"velMagDecrement") )
            setVelMagDecrement((double)props.getDouble(pre+"velMagDecrement"));

        if (props.isSpecified(pre+"accThreshold") )
            setAccThreshold((double)props.getDouble(pre+"accThreshold"));

        if (props.isSpecified(pre+"accMagDecrement") )
            setAccMagDecrement((double)props.getDouble(pre+"accMagDecrement"));
    
        //if (props.isSpecified(pre+"timeChannelNames") )
        //    setTimeChannelNames(props.getProperty(pre+"timeChannelNames"));
    
        if (props.isSpecified(pre+"accelerometerChannels"))
            setAccelerometerChannels(props.getProperty(pre+"accelerometerChannels"));

        if (debug) System.out.println (getParameterDescriptionString());
    }

    /** Returns a human-readable list of the current parameter settings.
    Adds parameters specific to this model not found in the generic model.*/
    public String getParameterDescriptionString() {
        StringBuffer sb = new StringBuffer(512);
        sb.append(super.getParameterDescriptionString());
        String pre = getPropertyPrefix(); 
        sb.append(pre).append("accMagDecrement  = ").append( getAccMagDecrement()).append("\n");
        sb.append(pre).append("accThreshold = ").append(getAccThreshold()).append("\n");
        sb.append(pre).append("velMagDecrement = ").append(getVelMagDecrement()).append("\n");
        sb.append(pre).append("velThreshold = ").append(getVelThreshold()).append("n");
        //sb.append(pre).append("timeChannelNames = " ).append(GenericPropertyList.toPropertyString(getTimeChannelNames())).append("\n");
        sb.append(pre).append("accelerometerChannels = " ).append(GenericPropertyList.toPropertyString(getAccelerometerChannels())).append("\n");
        return sb.toString();
    }

    public ArrayList getKnownPropertyKeys() {
      ArrayList list = super.getKnownPropertyKeys();
      String pre = getPropertyPrefix(); 
      list.add(pre + "accMagDecrement");
      list.add(pre + "accThreshold");
      list.add(pre + "velMagDecrement");
      list.add(pre + "velThreshold");
      //list.add(pre + "timeChannelNames");
      list.add(pre + "accelerometerChannels");
      return list;
    }

    public void setDefaultProperties() {
      super.setDefaultProperties();
      setMyDefaultProperties();
    }

    private void setMyDefaultProperties() {
      postEvent =  defPostEvent;
      preEvent = defPreEvent;
      minWindow = defMinWindow;
      maxWindow = defMaxWindow;
      maxDistance = defMaxDistance;
      includeAllComponents = defIncludeAllComponents;
      includeAllMag = defIncludeAllMag;

      accMagDecrement = defAccMagDecrement;
      velMagDecrement = defVelMagDecrement;
      accThreshold = defAccThreshold;
      velThreshold = defVelThreshold;
      //timeChannelNames = null;
      accelerometerChannels = null;
    }

    /* ///////////////////////////////////////////////////////
    public static void main(String args[]) {

    long evid;

    //      if (args.length <= 0) {
    //
    //        System.out.println ("Usage: NCChannelTimeWindowModel [evid] ");
    //        //System.exit(0);
    //
    //      } else {
    //        Long val = Long.valueOf(args[0]);
    //        evid = (long) val.longValue();
    //      }
    // event ID

        //evid = 14119792  ;  // M =
    //        evid = 10100053  ;  // 4.7 Parkfield -- should get ALL channels
        evid = 10100169; // 3.2
        System.out.println ("Test overriding command line EVID - evid = "+evid);

    // read props file to get waveserver filename
    JiggleProperties props = new JiggleProperties("NCmodel.props");

        DataSource db = TestDataSource.create("serverma");

        System.out.println (db.toString());

        Solution sol    = Solution.create().getById(evid);

        if (sol == null) {
            System.out.println ("No such event: "+evid);
            System.exit(0);
        }

        System.out.println (sol.toString());

        // read channel list from cache
        String cacheFileName = props.getUserFileNameFromProperty("channelCacheFilename");

    //        MasterChannelList.readOnlyChannelLatLonZ();
        MasterChannelList.set(ChannelList.readFromCache(cacheFileName));

        // Get a NC model setup with the args in the properties file
        ChannelTimeWindowModel model =
            props.getChannelTimeWindowModelList().getByClassname("org.trinet.jasi.NCChannelTimeWindowModel") ;

        if (model == null) {
        System.err.println ("Model could not be created.");
        System.exit(1);
        }
        model.setProperties(props);
        model.setSolution(sol);

        //
        ArrayList ctwList = (ArrayList) model.getChannelTimeWindowList();

        System.out.println ("Channel count = "+ctwList.size());

    }
    */
}
