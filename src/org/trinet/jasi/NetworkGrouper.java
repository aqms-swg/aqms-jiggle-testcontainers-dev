package org.trinet.jasi;

/**
 * Similar to a StringTokenizer for Channelable lists. Given a Channelable list,
 * it will return smaller lists that contain only Channelable objects that belong
 * to the same network. The input/output ChannelableList is sorted by NET.STA.COMP .
 * @see: ChannelNameSorter
 * <p>Company: USGS</p>
 * @author Doug Given
 * @version 1.0
 */

public class NetworkGrouper extends StationGrouper {

  public NetworkGrouper() {
  }

  /** Set the total groups present .*/
  protected void setGroupCount() {
    groupcount = list.getNetworkCount();
  }

  /** */
  public NetworkGrouper(ChannelableListIF list) {
    super(list);
  }

  /** Return true if the two channelabel object belong in the same group. */
  public boolean inSameGroup (Channelable ch1, Channelable ch2) {
    return ch1.getChannelObj().getNet().equalsIgnoreCase(ch2.getChannelObj().getNet());
  }

/*
//  public static class Tester {
      public static void main(String[] args) {
        System.out.println ("Making connection...");
        DataSource ds = TestDataSource.create();

        // read in the current station list - use cached version if available
        long evid = 13950596;
        AmpList amplist = new AmpList(Amplitude.create().getBySolution(evid));
        NetworkGrouper grouper = new NetworkGrouper(amplist);

        System.out.println("amps = "+ amplist.size());
        System.out.println("grps = "+grouper.countGroups());

        amplist.dump();
        int knt = 0;
        AmpList sublist;
        while (grouper.hasMoreGroups()) {
            sublist = (AmpList) grouper.getNext();
            System.out.println("grp #"+ knt++);
            System.out.println(sublist.toNeatString());
        }
      }
//  }
*/
}


