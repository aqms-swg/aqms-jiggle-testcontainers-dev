package org.trinet.jasi;
import javax.swing.event.*;
public interface ParseableJasiObjectIF {
  public JasiObject parseData(Object data); // could be resultset etc.
  public String toNeatString();
  // Note: since getStringHeader implemented static in classes for now define:
  public String getNeatHeader();
}
