package org.trinet.jasi;

import org.trinet.util.Format;
import org.trinet.util.LeapSeconds;

  public class Peak2PeakSample extends SampleWithPeriod {

      public double value2 = 0.;
      public double datetime2 = 0.;

      public Peak2PeakSample(double peakValue, double peakTime, double value2, double time2, float per) {
          super(peakValue, peakTime, per);
          this.value2    = value2;
          this.datetime2 = time2;
      }

      public Peak2PeakSample(SampleWithPeriod s) {
          super(s);
      }

      public Peak2PeakSample(Sample s) {
          super(s);
      }

      public void setValue2(double value) {
          value2 = value;
      }
      public void setDatetime2(double time2) {
          datetime2 = time2;
      }

      public Sample copy() {
          return (Peak2PeakSample) clone();
      }

      public String toString() {
          return LeapSeconds.trueToString(datetime) + " " + fe.form(value) + " " + ff.form(period) + " " + 
                 LeapSeconds.trueToString(datetime2) + " " + fe.form(value2); 
      }

      public String toUnitsString(String units) {
        //return String.format("%5.2f %s at %s", value, units, LeapSeconds.trueToString(datetime));
        Format f = (units.equalsIgnoreCase("counts")) ? fd : fe; 
        return LeapSeconds.trueToString(datetime) + " " + units + " " + f.form(value) + " " + " " + ff.form(period) +
            " " + LeapSeconds.trueToString(datetime2) + " " + f.form(value2); 
      }

      public Object clone() {
        return (Peak2PeakSample) super.clone();
      }

  }

