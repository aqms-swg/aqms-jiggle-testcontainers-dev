package org.trinet.jasi;

import java.util.*;

import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;

/**
 * A generic phase arrival object.
 */
public abstract class Phase extends JasiReading {
/*
Regarding IMPORTANCE in AssocArO table 
The station importance is a product of the generalized inverse approach and
usually is not computed by other standard location programs.
It is a quantitative measure of the contribution a particular arrival makes
to the hypocenter solution, and includes the effect of the weights applied
to the arrival data.
*/
    public DataDouble importance = new DataDouble();

    /** Description of the phase, type, first motion, quality.
     * @See org.trinet.jasi.PhaseDescription */
    public PhaseDescription description = new PhaseDescription();

    // Things relating to the associated Solution
    /** Emergence (takeoff) angle from source for calculated ray path to site
     *  measured relative to nadir (i.e. 0 degrees is down). */
    public DataDouble  emergenceAngle = new DataDouble();

    /** Weight assigned by the operator or picking program. This is usually a quality
     * estimate that is used to assign some phase picks more weight than others
     * in hypocentral calculations. It is a value between 0.0 and 1.0. */
    public DataDouble  weightIn = new DataDouble();

    /** Weight assigned by the location program. This usually is a measure of how
     * much a phase contributed to the solution relative to other phases. The
     * range of possible values will depend on the location algorithm used.*/
    public DataDouble  weightOut = new DataDouble();

    /* Model traveltime delay (geology, elevation, velocity model correction?) */
    public DataDouble  delay = new DataDouble(); // aww added

    // Uncertainty in pick time
    public DataDouble deltaTime = new DataDouble();

    public Object clone() {
        Phase ph          = (Phase) super.clone();
        ph.description    = (PhaseDescription) this.description.clone();
        ph.emergenceAngle = (DataDouble) this.emergenceAngle.clone();
        ph.weightIn       = (DataDouble) this.weightIn.clone();
        ph.weightOut      = (DataDouble) this.weightOut.clone();
        ph.delay          = (DataDouble) this.delay.clone(); // aww added
        ph.importance     = (DataDouble) this.importance.clone(); // aww added 2008/07/14
        ph.deltaTime     = (DataDouble) this.deltaTime.clone(); // aww added 2010/01/24
        return ph;
    }

    public static final double [] DEFAULT_DELTATIMES = new double [] {0.01,0.02,0.05,0.1,0.2};
    public static double [] DeltaTimes = DEFAULT_DELTATIMES;

    /**
     * Constructor is protected so you must instatiate with create().
     */
    protected Phase() { }

// -- Concrete FACTORY METHODS ---
    /**
     * Factory Method: This is how you instantiate a SeismicEvent object. You do
     * NOT use a constructor. This is so that this "factory" method can create
     * the type of SeismicEvent that is appropriate for your site's database.
     */
// ////////////////////////////////////////////////////////////////////////////
    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. Creates a Solution of the DEFAULT type.
     * @See: JasiObject
     */
     public static final Phase create() {
        return create(DEFAULT);
     }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is an integer implementation type.
     * @See: JasiObject
     */
     public static final Phase create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
     }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is as 2-char implementation suffix.
     * @See: JasiObject
     */
     public static final Phase create(String suffix) {
        return (Phase) JasiObject.newInstance("org.trinet.jasi.Phase", suffix);
     }

// ////////////////////////////////////////////////////////////////////////////

    /** Copy DataObjects from a phase passed as the arg to this phase that are
     * dependent on a solution. These are: distance, azimuth, emergenceAngle,
     * weightIn, weightOut, & residual. Checks that channel and phase type
     * of the two phases match. Returns true if they do and DataObjects were
     * copied. */
    abstract public boolean copySolutionDependentData (Phase newPhase) ;
    public boolean copySolutionDependentData (JasiReadingIF jr) {
      return copySolutionDependentData((Phase)jr);
    }
    /** Copy the phase in the arg's info into this phase. This preserves the
     * reference and datasource hooks of the original so that its dbase index
     * will be reused. */
    abstract public boolean replace(Phase ph);

    public boolean replace(JasiReadingIF jr) {
       return replace((Phase) jr);
    }

    /** Resets data derived from the Solution calculation
     * to an initial value state.
     *  Override in subclasses to change behavior. */
    public void initSolDependentData() {
      //weightIn.setNull(true);  // No reset picking observer sets this
      residual.setNull(true);  // jr
      delay.setNull(true);  // added model tt delay - aww 
      importance.setNull(true);  // added importance - aww  2008/07/14
      weightOut.setNull(true); // ph
      emergenceAngle.setNull(true); // ph
      getChannelObj().setDistanceBearingNull();
      //getChannelObj().azimuth.setNull(true); //Channel.NULL_DIST ?
      //getChannelObj().dist.setNull(true);
    }

    /**
     * Set phase description.
     */
    public void changeDescription(PhaseDescription pd)
    {
        description = new PhaseDescription(pd);
    }

    /** Set phase description given a */
    public void changeDescription (String iphase, String quality, String fm, String qual)
    {
        int wt = Integer.valueOf(qual).intValue();        // string -> int

        description = new PhaseDescription(iphase, quality, fm, wt);
    }


    /** Return true if input is a Phase and its descriptor type matches this instance.
     * @See: #isLikeChanType(JasiReading) */
    public boolean isSameType(JasiReadingIF jr) {
      return (jr instanceof Phase) ?
        this.description.isSameType((Phase) jr) : false;
    }

    public boolean isP() {
        return this.description.iphase.equalsIgnoreCase("P");
    }

    public boolean isS() {
        return this.description.iphase.equalsIgnoreCase("S");
    }

    // method for JasiCommitableIF 
    public Object getTypeQualifier() { 
      return description.iphase;
    }

    public double getWeightUsed() {
      return (weightOut == null || weightOut.isNull()) ? 
                 0.0 : weightOut.doubleValue();
    }
    public void setWeightUsed(double value) {
      //if (weightOut.isNull())  weightOut.setValue(value); // aww removed test condition
      weightOut.setValue(value); // aww
    }

    /** Return the reading quality, the value can be input to
     * algorithm calculations.
     *  Range is 0.0 to 1.0.
     */
    public double getQuality() { 
      return description.getQuality();
    }
    public void setQuality(double value) { // resets deltaTime value 
      description.setQuality(value);
      double val = qualityToDeltaTime( value );
      deltaTime.setValue( (val <= 0.) ? 0.: val );
    }

    /** Return the reading weight value input to algorithm calculations.
     *  Allowed range is 0.0 to 1.0.
    */
    public double getInWgt() { 
      if (weightIn == null || weightIn.isNull()) return 0.0;
      return weightIn.doubleValue();
    }
    /** Set the reading weight value for input into algorithm calculations.
     *  Allowed range is 0.0 to 1.0.
    */
    public void setInWgt(double wt) { 
      // Should we test for reject flag to assign 0 wt - aww 06/22/2006 ?
      weightIn.setValue(wt);
      reject = ( wt == 0.); // zero wt forces reject 01/23/2007 -aww
    }

    // override of parent method maps true to inWeight=0 and false to inWeight=1 
    public void setReject(boolean tf) {
        if (reject != tf && sol != null && ! sol.isStale() && sol.contains(this)) sol.setStale(true);
        reject = tf;
        weightIn.setValue((reject) ? 0. : 1.); // forces zero wt on reject 01/23/2007 -aww
    }

    // added methods below - aww
    public double getDelayValue() {
        return (delay.isValidNumber()) ? delay.doubleValue() : 0.;
    }
    public void setDelayValue(double value) {
        delay.setValue(value);
    }

    /* 
    public double getImportance() {
        return (importance.isValidNumber()) ? importance.doubleValue() : 0.;
    }
    public void setImportance(double value) {
        importance.setValue(value);
    }
    */

    /*
    public double getTTCorrection() {
        String type = description.iphase;
        if (type == null || type.length() < 1) return 0.;
        // from Channel subclass map load, the TTCorrection class D.N.E yet
        TTCorrection ttCorr = (TTCorrection) getCorrectionAtReadingTime("tt");
        // generally: sCorrValue = pCorrValue * Vp/Vs 
        return (ttCorr == null) ? 0. : ttCorr.getCorrValue(type);
    }
    */

    public double getDeltaTime() {
        return (deltaTime.isValidNumber()) ? deltaTime.doubleValue() : 0.;
    }

    public void setDeltaTime(double value) { // resets quality
        deltaTime.setValue(value);
        // Sets quality using mapping 
        if (value >= 0.) deltaTimeToQuality(); // note this uses static method below to map deltim secs to quality
    }

    public void deltaTimeToQuality() {  // sets this instance quality to its deltatime mapping
        double val = deltaTimeToQuality( getDeltaTime() );
        description.setQuality((val <= 0.) ? 0.: val ); // Note this does not set deltaTime, nor reset fm
    }

    public void qualityToDeltaTime() { // sets this instance deltatime to its quality mapping
        double val = qualityToDeltaTime( getQuality() );
        deltaTime.setValue( (val <= 0.) ? 0.: val );
    }

    // Just converts values
    public static double deltaTimeToQuality(double value) { 
        //double val = 1. - value*5.;
        //System.out.println("Phase deltaTimeToQuality " + value + " --> " + val);
        //return val;
        //
        //
        if (Double.isNaN(value)) return value;

        int iwgt = 0;
        while (iwgt<DeltaTimes.length) {
            if (value <= DeltaTimes[iwgt]) break;
            iwgt++;
        }
        if (iwgt > 4) iwgt = 4;
        return PhaseDescription.toQuality(iwgt);
    }

    // Just converts values
    public static double qualityToDeltaTime(double value) { 
        //double val = (1. - value)/5.;
        //System.out.println("Phase qualityToDeltaTime " + value + " --> " + val);
        //return val;
        //System.out.println(" qual: "+value+" iwgt: "+PhaseDescription.toWeight(value)+" delTime:" +DeltaTimes[PhaseDescription.toWeight(value)]);
        if (Double.isNaN(value)) return value;
        return DeltaTimes[ PhaseDescription.toWeight(value) ];
    }

    /**
     *  Return a string with the most of the phase field info.
     */
    public String toString()
    {
        StringBuffer sb = new StringBuffer(256);
        sb.append(toAssocIdString()).append(" ");
        //sb.append(getChannelObj().toDelimitedNameString(' ')).append(" ");
        sb.append(description.toString(isReject()).replaceAll(" ",".")).append(" ");
        //sb.append(getDateTime().toString()).append(" ");
        sb.append(getDateTime().toDateString("yyyy-MM-dd HH:mm:ss.fff")).append(" "); // instead aww 2015/11/20
        sb.append(getHorizontalDistance()).append(" "); // aww 06/11/2004
        sb.append(residual.toString()).append(" ");
        sb.append(delay.toString());  // aww added
        sb.append(weightOut.toString()).append(" ");
        sb.append(getAzimuth()).append(" ");
        sb.append(emergenceAngle.toString()).append(" ");
        sb.append(weightIn.toString()).append(" ");

        // beware of null solution
        //sb.append(" id: ").append(getIdentifier().toString());
        //if (sol != null) sb.append(" | evid = ").append(sol.id.toString());
        //if (isDeleted()) sb.append(" *DEL*");
        //if (isReject()) sb.append(" *REJ*");

        return sb.toString();
    }

    /**
      Return a fixed format string of the form:
<tt>

Nt Sta  Chn Desc      Date             Time        Dist     Res      Wt      Az    Inci
AZ CRY  HHZ  Pd0 February 16, 2001 11:20:05.840    0.00    0.00    0.00    0.00    0.02
xxxxxxxxxx xxxx xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx xxxx.xx xxxx.xx xxxx.xx xxxx.xx xxxx.xx
</tt>
        You must call getNeatStringHeader() to get the header line shown above.

        @see getNeatStringHeader()
    */
    public String toNeatString() {

        Format df = new Format("%8.2f");
        Format df0 = new Format("%3d");
        Format df1 = new Format("%7.2f");
        Format df3 = new Format("%4.3f");

        StringBuffer sb = new StringBuffer(132);
        sb.append(getChannelObj().getChannelName().toNeatString()).append(" ");
        sb.append(description.toString(isReject())).append(" ");
        // DateTime format for seconds is ALWAYS "xx.xxx"
        //sb.append(getDateTime().toString()).append(" ");         // removed aww 2015/11/20
        sb.append(getDateTime().toDateString("yyyy-MM-dd HH:mm:ss.fff")).append(" "); // instead aww 2015/11/20
        sb.append(df.form(getHorizontalDistance())).append(" "); // aww 06/11/2004 02/14/2005 HorizDistance bug fix
        sb.append(df1.form(residual.doubleValue())).append(" ");
        sb.append(df1.form(delay.doubleValue())).append(" "); // aww added
        sb.append(df1.form(deltaTime.doubleValue())).append(" "); // aww added
        sb.append(df1.form(weightOut.doubleValue())).append(" ");
        sb.append(df0.form(Math.round(getAzimuth()))).append(" ");
        sb.append(df1.form(emergenceAngle.doubleValue())).append(" ");
        sb.append(df3.form(importance.doubleValue())).append(" ");
        Concatenate.rightJustify(sb, getSource(), 8).append(" ");
        sb.append(getProcessingStateString());
        if (isDeleted()) sb.append(" DELETED") ;
        if (isReject()) sb.append(" REJECT");
        return sb.toString();
    }

    /*
     * Return a fixed format header to match output from toNeatString().
     * @see: toNeatString()
    */
    public static String getNeatStringHeader() {
        return ChannelName.getNeatStringHeader() +
             " Desc     Date      Time          Dist     Res   Delay  DelTim      Wt  Az     EmA  Impt   Source F";
    }
    public String getNeatHeader() { return getNeatStringHeader(); }

    /** Override to set assigned Solution state stale, if reading is
     * contained by it and it's not stale already.
    //Not needed if listeners listen to delete state change to toggle stale.
    public void setDeleteFlag(boolean tf) {
      if (deleteFlag == tf) return;
      deleteFlag = tf;
      // insurance with overhead?
      if (sol != null && ! sol.isStale() 
          && sol.contains(this)) sol.setStale(true);
    }
    * */

    // IF methods below may not be needed to vacate list but not null association.
    /* instead cf. super.removeFrom(Sol)
    public void removeFromAssocLists() { removeFromAssocSolList(); }
    public void removeFromAssocSolList() {
      Solution sol = getAssociatedSolution();
      if (sol != null) sol.remove(this); // sol.getPhaseList().remove(this);
    }
    */

} // end of Phase class
