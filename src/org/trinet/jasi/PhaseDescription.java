package org.trinet.jasi;

import java.io.*;
import java.util.*;

/**
 * A phase description pick object. <p>
 * This will automatically set "S" pick first motions to blank.
 */
public class PhaseDescription extends Object implements Cloneable, Serializable {
// data members
    public final static String  DEFAULT_TYPE = " ";
    public final static String  DEFAULT_FM = ".." ; // used to be "  " blank-blank -aww 2011/04/12

    public String iphase = DEFAULT_TYPE;    // phase type "p", "s", etc.
    public String ei     = DEFAULT_TYPE;    // "i", "e", "w"
    public String fm     = DEFAULT_FM;      // "d.", "c.", etc
//    protected int weight    = 4;          // 0, 1, 2, 3, 4
    protected double quality = 0.0;         // 0.0 -> 1.0

    /** Keep a copy of original so we can tell if any of the fields has been
     * changed from the values set by the original constructor.  */
    protected String iphase0 = DEFAULT_TYPE;  // phase type "p", "s", etc.
    protected String ei0     = DEFAULT_TYPE;  // "i", "e", "w"
    protected String fm0     = DEFAULT_FM;    // "d.", "c.", etc
//    protected int weight0    = 4;             // 0, 1, 2, 3, 4  wt default 4 maps to quality default 0. -aww
    protected double quality0 = (float) 0.0;  // 0.0 -> 1.0

    /** If true, allow parseShortString() to put first motions on S waves. Default = false. */
    static boolean fmOnS = false;

    /** If true, allow parseShortString() to put first motions on horizontal components. Default = false. */
    static boolean fmOnHoriz = false;

    /** Don't allow parseShortString() to put first motions on picks with less then this quality (scale 0.0 -> 1.0)
    * Default = 0.0 which allows first motions on picks of all qualities. */
    static double fmQualCut = 0.0;

    // insert cloning for any object attributes added that are not final
    public Object clone() {
      PhaseDescription pd = null;
      try {
        pd = (PhaseDescription) super.clone();
      }
      catch (CloneNotSupportedException ex) {
        ex.printStackTrace();
      }
      return pd;
    }

/**
 * Constructor
 */
    public PhaseDescription() { }

    public PhaseDescription(String iphase, String ei, String fm, int weight) {
        set(iphase, ei, fm, weight);
        remember(this);
    }

    public PhaseDescription(String iphase, String ei, String fm, double quality) {
        set(iphase, ei, fm, quality);
        remember(this);
    }
/**
 * Constructor
 */
    public PhaseDescription(PhaseDescription pd) {
        set(pd.iphase, pd.ei, pd.fm, pd.getQuality());
        remember(this);
    }

/**
 * Constructor from "short" phase description string of form "IP2"
 */
    public PhaseDescription(String str) {
        parseShortString(str);
        remember(this);
    }

/**
 * Return a copy of the given phase description.
 */
    public static PhaseDescription copy(PhaseDescription pd) {

        PhaseDescription pd0 = new PhaseDescription();
        pd0.iphase = pd.iphase.intern(); // aww try this to avoid duplicates in heap
        pd0.ei     = pd.ei.intern(); // aww try this like above
        pd0.fm     = pd.fm.intern(); // aww this like above
        pd0.setQuality(pd.getQuality());
//      pd0.quality = pd.quality;

        return pd0;
    }

    /** If set true allow first motions on S waves. */
    public static void setFmOnS(boolean tf) {
      fmOnS = tf;
    }
    public static boolean getFmOnS() {
      return fmOnS;
    }
    /** If set true do not allow first motions on horizontals. */
    public static void setFmOnHoriz(boolean tf) {
      fmOnHoriz = tf;
    }
    public static boolean getFmOnHoriz() {
      return fmOnHoriz;
    }
    /** Don't allow first motions on picks with less then this quality (scale 0.0 -> 1.0) */
    public static void setFmQualCut(double quality) {
      fmQualCut = quality;
    }
    public static double getFmQualCut( ) {
      return fmQualCut;
    }

/**
 * Remember original phase description values.
 */
    public void remember(PhaseDescription pd) {
      if (pd == null) return;
      iphase0  = pd.iphase.intern(); // avoids duplicates, should be speedier equals - aww
      ei0      = pd.ei.intern();
      fm0      = pd.fm.intern();
//    weight0  = pd.weight;
      quality0 = pd.getQuality();
    }
/**
 * Set phase description given component parts. Cannot enforce fmQualCut here
 * because quality is not set.
 */
    public void set( String phaseType, String onset, String fstmo) {
        if (phaseType != null) this.iphase  = phaseType;
        // in TriNet schema must be "e", "i", "w" (lower-case)
        if (onset != null) this.ei     = onset.toLowerCase();
        if (fstmo != null) this.fm  = fstmo;

        checkFmOnS();

    }

    public void set(String phaseType, String onset, String fstmo, int weight) {
        set(phaseType, onset, fstmo, weight, false);
    }

    public void set(String phaseType, String onset, String fstmo, int weight, boolean testFmQual) {
        set(phaseType, onset, fstmo);
        setWeight(weight);
         // don't put 1st mo on low weight picks
         if (testFmQual && (getQuality() < fmQualCut)) fm = DEFAULT_FM;
    }
/**
 * Set phase description given component parts.
 */
    public void set(String phaseType, String onset, String fstmo, double quality) {
        set(phaseType, onset, fstmo, quality, false);
    }
    public void set(String phaseType, String onset, String fstmo, double quality, boolean testFmQual) {
        set(phaseType, onset, fstmo);
//      this.weight  = toWeight(quality);
        setQuality(quality, testFmQual);
    }

    public void setQuality(double quality) {
        setQuality(quality, false);
    }

    public void setQuality(double quality, boolean testFmQual) {
        this.quality = quality;
        // remove 1st mo from low weight picks
        if (testFmQual && (quality < fmQualCut)) fm = DEFAULT_FM;
    }

     public void setWeight(int weight) {
        if (weight > 4) weight -= 5; // 06/22/2006 -aww
        setQuality(toQuality(weight));
     }

 /**
 * convert "quality" to old style weight.
 *  wt  quality
 *  0   1.00
 *  1   0.75
 *  2   0.50
 *  3   0.25
 *  4   0.00
 */
    public int getWeight() {
      return toWeight(quality);
    }

    public int getWeight(boolean addFive) {
      int wt = toWeight(quality);
      return (addFive) ?  wt+5 : wt;
    }

/**
 * convert old style weight to "quality"
 */
    public double getQuality() {
      return quality;
    }

    /**
     * Return true if all fields of the two PhaseDescriptions are the same.
     */
    public boolean equals(PhaseDescription ph2) {
        return (
            iphase.equals(ph2.iphase) &&
            ei.equals(ph2.ei) &&
            fm.equals(ph2.fm) &&
            getQuality() == ph2.getQuality()
        );
    }

/**
 * Returns true if the any of the fields has been changed from the values set by
 * the original constructor.  */
    public boolean hasChanged() {
        return ! (
            iphase.equals(iphase0) &&
            ei.equals(ei0) &&
            fm.equals(fm0) &&
//          weight  == weight0 &&
            quality == quality0
        );
    }

/**
 * Return true if phase in arg is of same type. E.g. "P"="P", "Sg"="Sg".
 */
    public boolean isSameType(Phase ph) {
        return isSameType(ph.description);
    }

/**
 * Return true if PhaseDescription in arg is of same type. E.g. "P"="P",
 * "Sg"="Sg".  This is case sensitive. */
    public boolean isSameType(PhaseDescription phdesc) {
        return iphase.equals(phdesc.iphase);
    }

/**
 * Set phase description given a "short" string. Example: "iS3", "EP2".
 * There is no first motion description.  Pass the Channel so we can test
 * if it's horizontal if fmOnHoriz is false.
 */
    public boolean parseShortString(String desc, Channel chan) {
       boolean addFive = parseShortString(desc);
       if (!fmOnHoriz && !chan.isVertical() ) fm = DEFAULT_FM;
       return addFive;
    }
/**
 * Set phase description given a "short" string. Example: "iS3", "EP2".
 * There is no first motion description.
 */
    public boolean parseShortString(String desc){
      boolean addFive = false;
      try {
          ei     = desc.substring(0,1);
          iphase = desc.substring(1,2);
          //fm is undefined for short strings
          checkFmOnS();
          // can't check fmOnHoriz here because we don't know the Channel info!
          int iwt = Integer.parseInt(desc.substring(2,3));
          setWeight(iwt);
          addFive = (iwt > 4);
      } catch (Exception ex) {
        System.err.println ("Can't parse bad phase description string: /"+desc+"/");
      }
      return addFive;
    }
/**
 * Set phase description given a Hypoinverse description string.
 * Example: "IS 3", "EPU2".
 */
    public boolean parseHypoinverseString(String desc) {
      boolean addFive = false;
      try {
        ei     = desc.substring(0,1);
        iphase = desc.substring(1,2);

        fm = DEFAULT_FM;
        String ud = desc.substring(2,3) ;
        // translate first-motion to NCDC conventions
        if (ud.equalsIgnoreCase("D")) fm="d.";
        // added default "c" override case, in case of string format change // aww
        else if (ud.equalsIgnoreCase("U") || ud.equalsIgnoreCase("c")) fm="c.";
        else if (ud.equals("+")) fm="+.";
        else if (ud.equals("-")) fm="-.";

        checkFmOnS();

        int wt = Integer.parseInt(desc.substring(3,4));
        setWeight(wt);
        addFive = (wt > 4); 

      } catch (Exception ex) {
        System.err.println ("Can't parse bad phase description string: /"+desc+"/");
      }
      return addFive;
    }

    public void flipPolarity() {
        if (fm.indexOf('d') >= 0) fm = fm.replace('d','c');
        else if (fm.indexOf('c') >= 0) fm = fm.replace('c','d');
        else if (fm.indexOf('-') >= 0) fm = fm.replace('-','+');
        else if (fm.indexOf('+') >= 0) fm = fm.replace('+','-');
    }

    public boolean parseHypoinverseString(String desc, Channel chan) {
        boolean addFive = parseHypoinverseString(desc) ;
        if (!fmOnHoriz && !chan.isVertical() ) fm = DEFAULT_FM;
        return addFive;
    }

    public void checkFmOnS() {
        if (!fmOnS && isS()) {
            fm = DEFAULT_FM; // "S" first motion is usually unknown 
        }
    }

    public boolean isP() {
        return iphase.substring(0,1).equalsIgnoreCase("P");
    }

    public boolean isS() {
        return iphase.substring(0,1).equalsIgnoreCase("S");
    }

/**
 * Return string with phase description. E.g. "iP3".
 * The "quality" is mapped to a 0-4 weight.
 * This is the traditional format used by Hypoinverse, etal.
 */
    public String toShortString() {
       return toHypoinverseString(false, false);
    }
/**
 * Return string with phase description. E.g. "IPU3".
 * The "quality" is mapped to a 0-4 weight.
 * This is the traditional format used by Hypoinverse, etal.
 */
    public String toHypoinverseString() {
       return toHypoinverseString(true, false);
    }

    public String toHypoinverseString(boolean addFive) {
       return toHypoinverseString(true, addFive);
    }

    private String toHypoinverseString(boolean addFM, boolean addFive) {

      // beware of null values which are legal in the schema
      String onset = (ei == null || ei.equals("")) ? DEFAULT_TYPE : ei;
      String fmStr = "";
      if (addFM) {
        fmStr = " "; // note value here is not "." , but 1-blank
        //System.out.println("toHypoinverseString fm: " + fm);
        if (fm != null) {
          if (fm.startsWith("c")) fmStr = "U";
          else if (fm.startsWith("d")) fmStr = "D";
          else if (fm.startsWith("+")) fmStr = "+";
          else if (fm.startsWith("-")) fmStr = "-";
          else if (fm.endsWith("u")) fmStr = "U";
          else if (fm.endsWith("r")) fmStr = "D";
        }
      }

      return ( onset + iphase + fmStr + getWeight(addFive) );

    }
/**
 * Return string with phase description. E.g. "iPc3".
 * This Includes only the 1st character of the first motion String.
 * The "quality" is mapped to a 0-4 weight.
 */
    public String toString() {
        return toString(false);
    }

    public String toString(boolean addFive) {
      // beware of null values which are legal in the schema
      String onset = (ei == null || ei.equals("")) ? DEFAULT_TYPE : ei;

      // no value described as "." in the schema (?!)
      String fmStr = (fm == null || fm.equals("") || fm.equals(DEFAULT_FM)) ? " " : fm;
      fmStr = (fmStr.substring(0,1).equals(".") && fmStr.endsWith("u") || fmStr.endsWith("r")) ? fmStr.substring(1,2) : fmStr.substring(0,1);

      // only use 1 char in fm
      return  ( onset + iphase + fmStr + getWeight(addFive) );
    }

/**
 * convert "quality" to old style weight.
 *  wt  quality
 *  0   1.00
 *  1   0.75
 *  2   0.50
 *  3   0.25
 *  4   0.00
 */
    public static int toWeight(double quality) {
      if (Double.isNaN(quality) || quality > 1. || quality < 0.)
          throw new IllegalArgumentException("Input quality out of bounds quality=" + quality);
      return (int) Math.round((1.0 - quality) * 4.0);   // floor of (X+.5)
    }
/**
 * convert old style weight to "quality"
 */
    public static double toQuality(int wt) {
      if (wt > 4) wt -= 5; // 06/22/2006 -aww
      if (wt > 4 || wt < 0)
          throw new IllegalArgumentException("Input weight out of bounds, wt=");
      return (double) (1.0 - ((double) wt / 4.0 ));
    }

    /** Returns fm from 4-char Hypoinverse format, else " ". */
    public static String getFmFrom(String desc) {
        if (desc.length() != 4) return " ";
        return String.valueOf(desc.charAt(2));
    }

    /** Returns wgt from Hypoinverse short (3-char) or long (4-char) format, else -1. */
    public static int getWtFrom(String desc) {
        if (desc.length() == 4) return Character.digit(desc.charAt(3), 10);
        else if (desc.length() == 3) return Character.digit(desc.charAt(2), 10);
        return -1; // default full-weight?
    }

} // end of class

