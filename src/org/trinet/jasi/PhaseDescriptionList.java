package org.trinet.jasi;

import java.util.ArrayList;

/**
 * Collection of PhaseDescriptions. Defaults to
 * Copyright:    Copyright (c) 2001
 * Company:      USGS
 * @author Doug Given
 *
 * * WORK IN PROGRESS.. no yet functional
 * Idea is to allow a list of phase descriptors that can be specified in
 * a properties file for build in the PhasePopup and PhaseDialog.
 * Complication is specifying that there are "groups" that you may need
 * to put seperators or figure rows in dialogs.

 */

public class PhaseDescriptionList extends ArrayList {

  String defP[] = {"iP0", "iP1", "eP2", "eP3", "eP4"};
  String defS[] = {"iS0", "iS1", "eS2", "eS3", "eS4"};

// add the S choices


  public PhaseDescriptionList() {

  }

//  public addGroup (String[] group) {
//  }

  public PhaseDescription[] getArray() {
     return (PhaseDescription[]) toArray(new PhaseDescription[size()]);
  }

  public PhaseDescription getPhaseDescription(int index) {
     return (PhaseDescription) get(index);
  }
}