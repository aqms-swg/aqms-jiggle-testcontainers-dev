package org.trinet.jasi;

import java.util.*;
import org.trinet.util.*;
/**
 * ChannelTimeModel that selects all channels with picks for the given
 * solution and constructs time windows that should include the P and S waves.
 * */
public class PicksOnlyChannelTimeModel extends ChannelTimeWindowModel {

    public final static String defModelName = "Picked"; 

    /** A string with an brief explanation of the model. For help and tooltips. */
    public final static String defExplanation = "channels associated with specified parametric data (e.g. arrivals, amps, codas)";
    
    {
        setModelName(defModelName);
        setExplanation(defExplanation);
    }

    public PicksOnlyChannelTimeModel() {
        setMyDefaultProperties();
    }
    public PicksOnlyChannelTimeModel(GenericPropertyList gpl, Solution sol, ChannelableList candidateList) {
        super(gpl, sol, candidateList);
    }
    public PicksOnlyChannelTimeModel(Solution sol, ChannelableList candidateList) {
        super(sol, candidateList);
    }
    public PicksOnlyChannelTimeModel(ChannelableList candidateList) {
        super(candidateList);
    }
    public PicksOnlyChannelTimeModel(Solution sol) {
        this();
        super.setSolution(sol);
    }

    /** Return a ChannelableList of ChannelTimeWindow objects selected by the model.
    * The will not contain time-series yet. To load time-series you must set
    * the loader mode with a call to AbstractWaveform.setWaveDataSource(Object)
    * and then invoke the loadTimeSeries() method of each Waveform instance.<p>
    *
    * This model needs a solution with included readings to determine what channels
    * to select. Returns null if Solution is not set or if Solution has no picks.<p>
    *
    * To be selected a channel must be in the channel list.
    * @see ChannelTimeWindowModel.setCandidateList()
    * @see MasterView.defineByChannelTimeWindowModel(ChannelTimeWindowModel)
    */
    public ChannelableList getChannelTimeWindowList() {

        if (sol == null) {
          System.err.println(getModelName() + " : ERROR Required solution reference is null, no-op");
          return null;
        }

        if ( ! (includePhases || includeMagAmps || includeCodas || includePeakAmps) ) {
          System.err.println(getModelName() + " : ERROR Required model include properties are not set, no-op");
        }

        // If including all, if any pick is added, others orientations are added
        // either as new Trixials or from those found in a defined group in candidateList. 
        // SIDE EFFECT:
        //   if includeAll and a candidateList is specified only components in the list are added
        // Returns the original required list possibly with other component orientations
        // added (which may or may not exist), list doesn't allow duplicates
        // loads and sorts include reading channels by distance
        setRequiredChannelList(getIncludeAllComponents() ? new ChannelableSetList() : new ChannelableList());
        //requiredList.distanceSort(sol.getLatLonZ()); // done by above -aww
        
        int count = requiredList.size();
        if (count <= 0) return new ChannelableList(0); // no picks

        // if it's name is set and list null, this creates a new candidate list by default for comparison
        getCandidateList(); // may be null, ok

        Channelable ch = null;
        ChannelableList newList = new ChannelableList(count);
        double dist = 0.;
        for (int i=0; i< count; i++) {
            ch = ((Channelable) requiredList.get(i)).getChannelObj();
            if (include(ch)) {
              // required list already in sorted distance order, so add below maintains distance order
              dist = ch.getHorizontalDistance();
              newList.add( new ChannelTimeWindow(ch.getChannelObj(), getTimeWindow(dist)));
            }
        }
        if (debug) System.out.println("DEBUG " + getModelName() + " after candidateList include, newList.size: " + newList.size());

        newList = filterListByChannelPropertyAttributes(newList);
        if (filterWfListByChannelList) newList = filterListByChannelList(newList);
        return (newList.size() > getMaxChannels()) ? new ChannelableList(newList.subList(0,getMaxChannels())) : newList;
    }

    public void setDefaultProperties() {
        super.setDefaultProperties();
        setMyDefaultProperties();
    }

    private void setMyDefaultProperties() {
        // force user to set candidateListName, else db picks might be unknowingly hidden by default 
        candidateListName = null;
        oldCandidateListName = null;
        candidateList = null;

        includeAllComponents = false;
        includePhases = true;   // default is phases, only
        includeMagAmps = false;
        includePeakAmps = false;
        includeCodas = false;
    }

}
