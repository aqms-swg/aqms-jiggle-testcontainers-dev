package org.trinet.jasi;

import java.util.*;

import org.trinet.util.*;

//import org.trinet.jiggle.JiggleProperties;  // for the main testing

/**
Model for determining the set of channels and time-windows likely to have
seismic energy above the noise level. <p>
How the technique works: first, calculate the expected <b>ground motion</b> for the
given solution's magnitude at the site using the Joyner-Boore relationship
that is used by ShakeMap for small events. This will be an acceleration or a
velocity depending on the channel type. Then that expected
ground motion is converted to counts using the gain of the channel.
Ideally the average background noise of each channel (in counnts) would be known,
but since it isn't we compare the expected signal amp to a default threshold
to see if the seismic energy would rise above the noise level. In other words,
<b>if the signal-to-noise ratio (SNR) is greater than one, the channel is saved.</b><p>

The default inclusion threshold is 200 counts.<br>
The default include all magnitude threshold is M >= 3.0<p>

Minimum window duration (secs) =  40.0<br>
Minimum window duration (secs)  = 600.0 (this will truncate codas > ~ML 5.0)<br>
Secs added to start of time window calculated by the model algorithm = 20.0<br>
Secs added to duration of time window calculated by the model algorithm  = 20.0 <br>
Maximum horizontal map distance allowed for an included Channel = 1000.0<br>

Properties specific to this class:<br>
<pre>
    org.trinet.jasi.PowerLawTimeWindowModel.velConstant
    org.trinet.jasi.PowerLawTimeWindowModel.velExponent
    org.trinet.jasi.PowerLawTimeWindowModel.accConstant
    org.trinet.jasi.PowerLawTimeWindowModel.accExponent
</pre>


*/
public class PowerLawTimeWindowModel extends AbstractSignalTimeWindowModel {

    public static final String defModelName = "Power Law";

    /** A string with an brief explanation of the model. For help and tooltips. */
    public static final String defExplanation =
        "channels whose dist <= k*(magnitude**p), where k,p are set by model properties";

    /** Include all components (orientations) of a channel type if any is included. */
    public static final boolean defIncludeAllComponents = true;

    public static final double defConstVel = 120.0;
    public static double defExpVel   = 1.3;
    public static double defConstAcc = 40.0;
    public static double defExpAcc   = 1.8;

    /** Factor by which ground motion must exceed background noise level. Default = 1.0
     * For example, if this value is 2.0 and the background noise level at a particular 
     * site is 30 counts then the calculated ground motion must be 60 counts or more 
     * (2 * 30) to be included. */
    protected double constVel = defConstVel;
    protected double expVel   = defExpVel;
    protected double constAcc = defConstAcc;
    protected double expAcc   = defExpAcc;

    // set defaults for this model
    {
      setModelName(defModelName);
      setExplanation(defExplanation);
    }

    public PowerLawTimeWindowModel() {
        setMyDefaultProperties();
    }

    public PowerLawTimeWindowModel(GenericPropertyList gpl, Solution sol, ChannelableList candidateList) {
        super(gpl, sol, candidateList);
    }
    public PowerLawTimeWindowModel(Solution sol, ChannelableList candidateList) {
        super(sol, candidateList);
    }
    public PowerLawTimeWindowModel(ChannelableList candidateList) {
        super(candidateList);
    }
    public PowerLawTimeWindowModel(Solution sol) {
        this();
        super.setSolution(sol);
    }

    public void setVelocityConstant(double value) {
        constVel = value; 
    }
    public double getVelocityConstant() {
        return constVel;
    }
    public void setVelocityExponent(double value) {
        expVel = value; 
    }
    public double getVelocityExponent() {
        return expVel;
    }
      
    public void setAccelerationConstant(double value) {
        constAcc = value; 
    }
    public double getAccelerationConstant() {
        return constAcc;
    }
    public void setAccelerationExponent(double value) {
        expAcc = value; 
    }
    public double getAccelerationExponent() {
        return expAcc;
    }
      
    /** 
     * This method uses an empirically derived power law to calc a cutoff
     * distance
     * @param ch
     * @param mag
     * @param dist
     * @param z
     * @return
     */
    public boolean shouldShowEnergy(Channel ch, double mag, double dist, double z) {

      // These values are from a powerlaw fit in Excel to observations of
      // how far out I could see energy on 6 events between 0.3 and 2.9
      // These values were further tweeked interactively in Excel.
      double con = 0.;
      double exp = 0.;
      if ( ch.isVelocity() ) {
          con = getVelocityConstant();
          exp = getVelocityExponent();
      } else if (ch.isAcceleration() ) {
          con = getAccelerationConstant();
          exp = getAccelerationExponent();
      }  else {
         System.err.println(getModelName() + ": Unknown seedchan type, not VEL or ACC: "+
        		 ch.getChannelId().toDelimitedSeedNameString(".")+
        		 "  gain units = "+ch.getCurrentGain().getUnitsString() + " defaulting to VEL distance scaling");
         //return false;
         //default to velocity settings
         con = getVelocityConstant();
         exp = getVelocityExponent();
      }

      double cutoff = con * Math.pow(mag, exp);
      boolean tf =  (dist <= cutoff);
      if (debug) {
          bm.printTimeStamp(getModelName() + " DEBUG " + (tf ? "+" : "-") + ch.toDelimitedNameString(".") +
                  " dist= "+ Math.round(dist) + " cutoff= " + cutoff);
      }

      return tf;

    }

    /** Setup model behavior parameters using values in a property list.
     * Assumes each property has a prefix matching the string returned by getClassname().
     * Override this to set properties specific to a model.
     * In the new method call super.setProperties(props) first to get the standard stuff.<p>
     * <pre>
     * For example:
     * You have a class called org.trinet.stuff.MyCoolModel:
     *
     * Properties would define as follows:
     *
    <code>
    org.trinet.jasi.PowerLawTimeWindowModel.velConstant=120.0
    org.trinet.jasi.PowerLawTimeWindowModel.velExponent=1.3
    org.trinet.jasi.PowerLawTimeWindowModel.accConstant=40.0
    org.trinet.jasi.PowerLawTimeWindowModel.accExponent=1.8
    </code>
    This method adds above properities to the property list.
     * */
    public void setProperties(GenericPropertyList props) {

      super.setProperties(props);   /// get all the standard stuff

      /*
       * Notes on tuning:
       * Tuning is best done interactively in Excel where you can adjust the curve
       * to a summary data set or some other view of you desired behavior.
       * 
       * The xxxConstant tends to affect the position of the curve. Larger
       * values move it upward, lower values downward. 
       * 
       * The xxxExponent tends to change the slope of the curve. Smaller
       * values flatten it out (rotate it clockwise) while larger values make
       * it steeper (rotate conterclockwise) yielding more close-in chanels and 
       * fewer distant channels if you don't adjust xxxConstant.
       */
      if (props != null) {
        String pre = getPropertyPrefix();
        if (props.isSpecified(pre+"velConstant") )
          setVelocityConstant(props.getDouble(pre+"velConstant"));

        if (props.isSpecified(pre+"velExponent") )
          setVelocityExponent(props.getDouble(pre+"velExponent"));

        if (props.isSpecified(pre+"accConstant") )
          setAccelerationConstant(props.getDouble(pre+"accConstant"));

        if (props.isSpecified(pre+"accExponent") )
          setAccelerationExponent(props.getDouble(pre+"accExponent"));
      }
      
      if (debug) System.out.println (getParameterDescriptionString());
    }

    /** Returns a human-readable list of the current parameter settings.
    Adds parameters specific to this model not found in the generic model.*/
    public String getParameterDescriptionString() {
        Format df84 = new Format("%8.4f");
        StringBuffer sb = new StringBuffer(512);
        
      sb.append( super.getParameterDescriptionString());
      String pre = getPropertyPrefix(); 
      sb.append(pre).append("velConstant = ").append(df84.form(getVelocityConstant())).append("\n"); 
      sb.append(pre).append("velExponent = ").append(df84.form(getVelocityExponent())).append("\n");     
      sb.append(pre).append("accConstant = ").append(df84.form(getAccelerationConstant())).append("\n"); 
      sb.append(pre).append("accExponent = ").append(df84.form(getAccelerationExponent())).append("\n");   
      
      return sb.toString();
    }

    public ArrayList getKnownPropertyKeys() {
      ArrayList list = super.getKnownPropertyKeys();
      String pre = getPropertyPrefix(); 
      list.add(pre + "velConstant");
      list.add(pre + "velExponent");
      list.add(pre + "accConstant");
      list.add(pre + "accExponent");
      return list;
    }

    public void setDefaultProperties() {
      super.setDefaultProperties();
      setMyDefaultProperties();
    }

    private void setMyDefaultProperties() {
      includeAllComponents = defIncludeAllComponents;
      constVel = defConstVel;
      expVel = defExpVel;
      constAcc = defConstAcc;
      expAcc = defExpAcc;
    }

/* ///////////////////////////////////////////////////////
//  public static final class Tester {
    public static void main(String args[]) {

      long evid;

//      if (args.length <= 0) {
//
//        System.out.println("Usage: PowerLawTimeWindowModel [evid] ");
//        //System.exit(0);
//
//      } else {
//        Long val = Long.valueOf(args[0]);
//        evid = (long) val.longValue();
//      }
      // event ID

        //evid = 14119792  ;  // M =
//        evid = 10100053  ;  // 4.7 Parkfield -- should get ALL channels
        evid = 10100169; // 3.2
        System.out.println("Test overriding command line EVID - evid = "+evid);

     // read props file to get waveserver filename
       //JiggleProperties props = new JiggleProperties("properties");
       JiggleProperties props = new JiggleProperties("JBmodel.props");

        //DataSource db = new DataSource();
        DataSource db = TestDataSource.create("serverma");

        System.out.println(db.toString());

        Solution sol    = Solution.create().getById(evid);

        if (sol == null) {
            System.out.println("No such event: "+evid);
            System.exit(0);
        }

        System.out.println(sol.toString());

        // read channel list from cache
        String cacheFileName = props.getUserFileNameFromProperty("channelCacheFilename");
//        MasterChannelList.readOnlyChannelLatLonZ();
        MasterChannelList.set(ChannelList.readFromCache(cacheFileName));

        // Prevent loading of Mag infor until new tables get decided on - DDG 11/5/04
//        Channel.setLoadOnlyLatLonZ();
//        Channel.setLoadResponse(true);
//        Channel chnl = Channel.create();
//        // use the candidate list as the master list
//        ChannelableList candidateList = chnl.getCandidateChannelList();
//        MasterChannelList.set(candidateList);

        // Get a model setup with the args in the properties file
        ChannelTimeWindowModel model =
            props.getChannelTimeWindowModelList().getByClassname("org.trinet.jasi.PowerLawTimeWindowModel") ;

        if (model == null) {
          System.err.println("Model could not be created.");
          System.exit(1);
        }
//        PowerLawTimeWindowModel model = new PowerLawTimeWindowModel();
        model.setProperties(props);
        model.setSolution(sol);

        //
        ArrayList ctwList = (ArrayList) model.getChannelTimeWindowList();

        //ctwList.toString();

//        if (ctwList != null ) {
//          for (int i = 0; i < ctwList.size(); i++) {
//            System.out.println( ((ChannelTimeWindow) ctwList.get(i)).toString() );
//          }
//        }
        System.out.println ("Channel count = "+ctwList.size());

    }
//  } // end of Tester
*/
}
