package org.trinet.jasi;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;

import org.trinet.util.DateTime;
import org.trinet.util.LeapSeconds;

/**
 * Provides locking services for some shared resource, a JasiObject. The concrete
 * class must determine how this is implements. However, it must be done in such
 * a way that all sessions sharing a datasource will also share locks. This can
 * be done by using the source database or with files on the data host.
 *
 * @author Doug Given
 */

public abstract class ResourceLock extends JasiObject {

    /** IP address of lock holder. */
    public String host = null;

    /** Username of lock holder. */
    protected String username = null;

    /** Name of application holding the lock. */
    public String application = null;

    /** Time of lock. Seconds since epoch Jan. 1, 1970. */
    public double datetime = 0.;

    /** The locked object. */
    protected JasiObject object;

    /** Lock info for the requestor */
    protected double requestorDateTime    = new DateTime().getTrueSeconds(); // now in UTC true seconds -aww 2008/02/11
    protected String requestorApplication = EnvironmentInfo.getApplicationName();
    protected String requestorHost        = EnvironmentInfo.getLocalHostName();
    protected String requestorUsername    = EnvironmentInfo.getUsername();

    { // override uses dashes instead of empty string, so it's distinguishable from null when table is queried
        if (requestorUsername == null || requestorUsername.equals("")) requestorUsername = "---";
    }

    /** Lock the object, returns true on success, false on failure.
    * If locked, infromation on the current lock holder is in the data members. */
    public abstract boolean lock();

    /** Release the lock on this object. Returns true even if the lock was not held
    * in the first place. */
    public abstract boolean unlock();

//    public abstract boolean stealLock (JasiObject obj) ;

    /** Returns true if the object is locked by anyone, the caller included. */
    public abstract boolean isLocked();

    /** Returns true if the object is locked by the caller. */
    public abstract boolean lockIsMine();

    public String toString() {
        return " Host: "+host+" Username: "+username+
            " Application: "+application+" Time: "+LeapSeconds.trueToString(datetime)+ // assumes UTC true seconds -aww 2008/02/11
            " Object: "+object.toString();
    }

    public String getUsername() {
      return username;
    }

     public String requestorToString() {
        return " Host: "+requestorHost+
            " Username: "+requestorUsername+
            " Application: "+requestorApplication+
            " Time: "+LeapSeconds.trueToString(requestorDateTime)+
            " Object: "+object.toString();
    }

    /** Returns true only if host, username, and application name of this lock matches that of
     * the caller. Comparison is NOT case sensitive. */
    public boolean matches() {
        if (host.equalsIgnoreCase(requestorHost) &&
            username.equalsIgnoreCase(requestorUsername) &&
            application.equalsIgnoreCase(requestorApplication) ) return true;

        return false;
    }

} // ResourceLock
