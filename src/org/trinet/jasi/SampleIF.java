package org.trinet.jasi;

public interface SampleIF extends Cloneable, java.io.Serializable {

    public void set(double time, double val);

    public double getDatetime();
    public void setDatetime(double time );

    public double getValue();
    public void setValue(double val );

    public boolean isNull();

    public String toString();
    public String toUnitsString(String units);

    public double timeDiff(Sample samp);
    public Sample copy();
    public Object clone();

}
