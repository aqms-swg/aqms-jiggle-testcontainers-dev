package org.trinet.jasi;

import org.trinet.util.Format;
import org.trinet.util.LeapSeconds;

  public class SampleWithPeriod extends Sample {

      public float period = Float.NaN;

      public SampleWithPeriod() { }

      public SampleWithPeriod(double time, double val, float per) {
          super(time, val);
          period = per;
      }

      public SampleWithPeriod(SampleWithPeriod s) {
          super(s);
          this.period = s.period;
      }

      public SampleWithPeriod(Sample s) {
          super(s);
      }

      public SampleWithPeriod(Sample s, float per) {
          super(s);
          period = per;
      }

      public Sample copy() {
          return (SampleWithPeriod) clone();
      }

      public float getPeriod() {
          return period;
      }

      public void setPeriod(float per) {
          period = per;
          //setNotNull();
      }

      public String toString() {
          return LeapSeconds.trueToString(datetime) + " " + fe.form(value) + " " + ff.form(period);
      }

      public String toUnitsString(String units) {
        //return String.format("%5.2f %s at %s", value, units, LeapSeconds.trueToString(datetime));
        Format f = (units.equalsIgnoreCase("counts")) ? fd : fe; 
        return LeapSeconds.trueToString(datetime) + " " + units + " "  + f.form(value) + " " + ff.form(period); 
      }

      public Object clone() {
        return (SampleWithPeriod) super.clone();
      }

  }

