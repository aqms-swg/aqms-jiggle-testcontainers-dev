package org.trinet.jasi;

import java.io.*;
import java.util.*;
import java.text.*;
import java.lang.*;

import org.trinet.jdbc.datatypes.*;
import org.trinet.util.Format;                // CoreJava printf-like Format class
import org.trinet.util.LeapSeconds;
import org.trinet.util.DateTime;
/**
 * Support for various event summary format tasks.
 *
 * @author Doug Given
 */

/*
  Tried using DecimalFormat but it is incapable of producing a truely fixed
  output. Can't control leading blanks.
*/
public class SeisFormat  {

    public SeisFormat() { }

    /*
          1         2         3         4         5         6         7         8         9
0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456
 9134001 CI 2000/01/13 16:54:37  33.17N 116.11W   6.0 1.6MC  C    2 mi. NNE of Ocotillo Wells, CA
 9134011 CI 2000/01/13 18:13:38  34.43N 116.19W   6.3 1.6MGN A*  21 mi. NNE of Joshua Tree, CA
 9134012 CI 2000/01/13 18:18:07  34.32N 116.25W   2.8 1.6MGN A*  13 mi. NNE of Joshua Tree, CA
 9528729 CI 2000/01/14 23:25:36  34.76N 116.21W   0.0 1.2 Ml
    */

    public static String toCNSS3 (Solution sol) {

        Format df0 = new Format("%8d");            // CORE Java Format class
        Format df1 = new Format("%5.2f");
        Format df2 = new Format("%6.2f");
        Format df3 = new Format("%5.1f");
        Format df4 = new Format("%3.1f");

        String fmtStr = "yyyy/MM/dd HH:mm:ss";
        String dtStr = LeapSeconds.trueToString( sol.getTime(), fmtStr); // note no frac sec

        String ns = "N";
        String ew = "E";
        float latf = sol.lat.floatValue();
        float lonf = sol.lon.floatValue();

        // handle hemipsphere issues
        if (latf < 0) {
            latf = Math.abs(latf);
            ns = "S";
        }
        if (lonf < 0) {
            lonf = Math.abs(lonf);
            ew = "W";
        }

        String qual = letterQuality(sol);

        String checked = "*";
        if (sol.processingState.equalsValue(JasiProcessingConstants.STATE_HUMAN_TAG)) checked = " ";
        else if (sol.processingState.equalsValue(JasiProcessingConstants.STATE_FINAL_TAG)) checked = " ";
        //else if (sol.processingState.equalsValue(JasiProcessingConstants.STATE_INTERMEDIATE_TAG)) checked = " "; // maybe screwy so don't report?

        String whereLine = "";

        return ( df0.form(sol.id.longValue()) + " " +
                sol.authority.toString().substring(0, 2) +" "+        // 1st 2 chars
                dtStr + "  " +
                df1.form(latf) + ns + " "+
                df2.form(lonf) + ew +" " +
                df3.form(sol.depth.doubleValue()) + " "+
                df4.form(sol.magnitude.value.doubleValue()) +
                sol.magnitude.getTypeString() +" "+
                qual + checked + whereLine
                );
   
    }

    /** Historical Caltech quality criteria; A, B, C, D*/

    /*

c ..These are the CUSP catalog quality definitions
c         ERH     ERZ    QUALITY
c        < 1.0   < 2.0      A
c        < 5.0   < 2.5      B
c        < 5.0   > 2.5      C
c        > 5.0              D
        CAT.CQ = 'D'
        IF(CAT.ERH .LT. 5.0) CAT.CQ = 'C'
        IF(CAT.ERH .LT. 5.0 .AND. CAT.ERZ .LT. 2.5) CAT.CQ = 'B'
        IF(CAT.ERH .LT. 1.0 .AND. CAT.ERZ .LT. 2.0) CAT.CQ = 'A'
    */

    public static String letterQuality (Solution sol) {

        double erh = 999.99;
        double erz = 999.99;
        if (!sol.errorHoriz.isNull()) erh = sol.errorHoriz.doubleValue();
        if (!sol.errorVert.isNull())  erz = sol.errorVert.doubleValue();

        //        System.out.println ("erh erz "+ erh +" "+erz);
        String q = "D";
        if (erh < 5.0) q = "C";
        if (erh < 5.0 && erz < 2.5) q = "B";
        if (erh < 1.0 && erz < 2.0) q = "A";

        return q;
    }

    /** Set quality word:  'POOR',  'Fair,  'Good', 'Excellent'
/*
        cqual = 'POOR'
        if (ktup.q(1:1) .eq. 'C') cqual = 'Fair'
        if (ktup.q(1:1) .eq. 'B') cqual = 'Good'
        if (ktup.q(1:1) .eq. 'A') cqual = 'Excellent'
                      */
    public static String wordQuality (Solution sol) {

        String q = letterQuality(sol);

        if (q.equals("A")) return "Excellent";
        if (q.equals("B")) return "Good";
        if (q.equals("C")) return "Fair";

        return "Poor";
    }

    /**
    Return the string (word) that describes an event of this magnitude.
    See: http://earthquake.usgs.gov/faq/meas.html
    <par>
    great; M > =8
    major; 7 < =M < 7.9
    strong; 6 < = M < 6.9
    moderate: 5 < =M < 5.9
    light: 4 < =M < 4.9
    minor: 3 < =M < 3.9
    micro: M < 3
    </par>
     */
    public static String magClassString (double mag) {
        int imag = (int) Math.floor(mag+0.05);     // handle rounding issues
        switch (imag) {
            case 3:
                return "minor";
            case 4:
                return "light";
            case 5:
                return "moderate";
            case 6:
                return "strong";
            case 7:
                return "major";
            case 8:
            case 9:
            case 10:                // well, you can't be too safe
                return "great";
            default:
                return "micro";
        }
    }

//  public static void main(String[] args) {
//      for (double mag = -1.0;mag<10.0;mag=mag+0.05) {
//          System.out.println(mag+"  "+magClassString(mag));
//      }
//  }

    /**
     * Return formatted solution summary string.
     */
    public static String toSummaryString (Solution sol) {
        return htmlString (sol, null, false);
    }

    /**
     * This is the format used for the TriNet review page. If 'tags' is true,
     embedded HTML tags for use in the review web page will be encluded.
     If 'tags' is false, it will be plain text.  */
/*
    public static String htmlReview (Solution sol, boolean tags) {
    
    // ID# mag year/mo/dy hr:mn:sc latitude longitude dep
    // 95142852.2 Ml 1999/11/22 01:11:22 36.0736 -117.8523 1.8
    // 9520825 0.8 Ml 1999/12/0121:12:12 33.9418 -116.4998 7.0 6 5.81
    // ID# MAG DATE TIME(UTC) LAT LON Z # RMS
    // year/mo/dy hr:mn:sc deg deg km PHS
    // 9520825  0.8 Ml 1999/12/01 21:12:12  33.9418 -116.4998  7.0   6 5.81 360 le H SRC
    //    ID#    MAG      DATE    TIME(UTC)   LAT       LON     Z    #  RMS GAP TP C
    //                 year/mo/dy hr:mn:sc    deg       deg    km  PHS
        String tag0 = "";
        String tag1 = "";
        long evid = sol.id.longValue();                // the event #
        if (tags) {
            String cgi = "cgi-bin/makeView.cgi?EVID=";        // the cgi script name
    
            String target = "zoom";                // target frame
    
            tag0 = "<a href=\""+cgi+evid+"\" target=\""+target+"\">";
            tag1 = "</a>";
         }
         Format df0 = new Format("%8d");            // CORE Java Format class
         Format df1 = new Format("%5.4f");
         Format df2 = new Format("%7.4f");
         Format df3 = new Format("%4.1f");
         Format df4 = new Format("%5.2f");
         Format df5 = new Format("%3d");
         String fmtStr = "yyyy/MM/dd HH:mm:ss";
         String dtStr = LeapSeconds.trueToString( sol.getTime(), fmtStr );
         String str = tag0 + df0.form(evid) + tag1+ " " +
                 df3.form(sol.magnitude.value.doubleValue()) + " " +
                 sol.magnitude.getTypeString() +" "+
                 dtStr + "  " +
                 df1.form(sol.lat.doubleValue()) + " " +
                 df2.form(sol.lon.doubleValue()) + " " +
                 df3.form(sol.depth.doubleValue()) + " "+
                 df5.form(sol.usedReadings.intValue()) +" "+
                 df4.form(sol.rms.doubleValue())+" "+
                 df5.form(sol.gap.intValue())+" "+
              // convert event type to 2char TriNet style code
                 EventTypeMap.create().toLocalCode(sol.eventType.toString()) +" "+
                 sol.processingState.toString()+ " "+
                 sol.source.toString().concat("     ").substring(0, 4);   // truncate to 3 chars
        return str;
    }
*/

    /**
     * This is the format used for the TriNet review page. If 'tags' is true,
     embedded HTML tags for use in the review web page will be encluded.
     If 'tags' is false, it will be plain text.  */
    public static String htmlString(Solution sol, String host, boolean tags) {
    //    ID#    mag  year/mo/dy hr:mn:sc latitude longitude  dep
    // 9514285  2.2 Ml 1999/11/22 01:11:22  36.0736 -117.8523  1.8
    // 9520825  0.8 Ml 1999/12/01 21:12:12  33.9418 -116.4998  7.0   6 5.81
    //    ID#    MAG      DATE    TIME(UTC)   LAT       LON     Z    #  RMS
    //                 year/mo/dy hr:mn:sc    deg       deg    km  PHS

    // 9520825  0.8 Ml 1999/12/01 21:12:12  33.9418 -116.4998  7.0   6 5.81 360 le H SRC
    //    ID#    MAG      DATE    TIME(UTC)   LAT       LON     Z    #  RMS GAP TP C
    //                 year/mo/dy hr:mn:sc    deg       deg    km  PHS

        //    String cgi = "cgi-bin/makeView.cgi?EVID=";        // the cgi script name
        long evid = sol.id.longValue();              // the event #

        String tag0 = "";
        String tag1 = "";

        if (tags) {
            String cgi = "cgi-bin/makeView.cgi?EVID="; // the cgi script name
            String target = "zoom";                    // target frame
            tag0 = "<a href=\""+cgi+evid+"&HOST="+host+"\" target=\""+target+"\">";
            tag1 = "</a>";
        }

         Format df0 = new Format("%8d");            // CORE Java Format class
         Format df1 = new Format("%7.4f");
         Format df2 = new Format("%9.4f");
         Format df3 = new Format("%4.1f");
         Format df4 = new Format("%5.2f");
         Format df5 = new Format("%3d");

         String timeFmt = "yyyy/MM/dd HH:mm:ss";

         String dtStr = new DateTime(sol.getTime(), true).toDateString(timeFmt);

         String str = tag0 + df0.form(evid) + tag1+ " " +
                     df3.form(safeValue(sol.magnitude.value)) + " " +
                     sol.magnitude.getTypeString() +" "+
                     dtStr + "  " +
                    df1.form(safeValue(sol.lat)) + " " +
                     df2.form(safeValue(sol.lon)) + " " +
                     df3.form(safeValue(sol.depth)) + " "+
                     df5.form((int)safeValue(sol.usedReadings)) +" "+
                     df4.form(safeValue(sol.rms))+" "+
                     df5.form((int)safeValue(sol.gap))+" "+
                     sol.eventType.toString().concat("     ").substring(0, 3) +" "+
                     sol.processingState.toString()+ " "+
                     sol.source.toString().concat("     ").substring(0, 4);   // truncate to 3 chars


         return str;
    }

    static double safeValue(DataObject val) {
        if (val.isNull()) return 0.0;
        return val.doubleValue();
    }


} // SeisFormat
