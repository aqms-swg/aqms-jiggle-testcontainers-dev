package org.trinet.jasi;
import java.util.*;
import org.trinet.util.*;

/**
 * Model that returns a set of ChannelTimeWindows that represent where seismic
 * energy is most likely to be found for channels those found in model's candidate
 * list channel set. 
 * Default is a uses a mag/distance relationship defined by H. Kanamori for Southern California:<br>
 * <b>cutoff km = 170.0 * magnitude - 205.0</b>.
 * There is no account taken of channel gain in this model.
 * <img src=./docs/SimpleChannelTimeModel.jpg>
 *
 */
public class SimpleChannelTimeModel extends ChannelTimeWindowModel {

  public static final String defModelName = "SimpleMagCutoff";

  /** A string with an brief explanation of the model. For help and tooltips. */
  public static final String defExplanation =
      "channels whose distance km <= a * magnitude - b, where a,b are set by properties";

  public static final double defMinDist = 20.;
  public static final double defMagDistIntercept = -205.;
  public static final double defMagDistSlope = 170.0;

  protected double magDistSlope = defMagDistSlope;
  protected double magDistIntercept = defMagDistIntercept;

  {
    setModelName(defModelName);
    setExplanation(defExplanation);
  }

  public SimpleChannelTimeModel() {
      setMyDefaultProperties();
  }

  public SimpleChannelTimeModel(GenericPropertyList gpl, Solution sol, ChannelableList candidateList) {
      super(gpl, sol, candidateList);
  }

  public SimpleChannelTimeModel(Solution sol, ChannelableList candidateList) {
      super(sol, candidateList);
  }
  public SimpleChannelTimeModel(ChannelableList candidateList) {
      super(candidateList);
  }
  public SimpleChannelTimeModel(Solution sol) {
      this();
      super.setSolution(sol);
  }

  public void setMagDistSlope(double slope) {
      magDistSlope = slope;
  }

  public void setMagDistIntercept(double intercept) {
      magDistIntercept = intercept;
  }

  public double getMagDistSlope() {
      return magDistSlope;
  }

  public double getMagDistIntercept() {
      return magDistIntercept;
  }

  public ChannelableList getChannelTimeWindowList() {
      if (sol == null) {
        System.err.println(getModelName() + " : ERROR Required solution reference is null, no-op");
        return null;
      }
      return super.getChannelTimeWindowList();
  }

  /**
   * Return the value of the cutoff distance for this magnitude. The default
   * distance cutoff is an empirical relationship derived by Hiroo Kanamori
   * for southern California. It is given by:<p>
   *<tt>
   * cutoff km = 170.0 * mag - 205.0
   *</tt>
   * when mag is >= getIncludeAllMag(), cutoff = super.getMaxDistance() is returned.
   * */
  public double getMaxDistance() {

      if (sol == null || sol.magnitude == null) return getMinDistance();

      double mag = sol.magnitude.value.doubleValue();

      if (mag >= includeAllMag) return super.getMaxDistance();

      // at mags < 1.3 the raw formula yields a NEGATIVE number!
      // Use max/min to protect against stupid answers
      double dist = Math.max( getMinDistance(), (magDistSlope * mag) + magDistIntercept) ;
      return Math.min(dist, super.getMaxDistance() );
  }

  public void setDefaultProperties() {
      super.setDefaultProperties();
      setMyDefaultProperties();
  }

  private void setMyDefaultProperties() {
      minDistance = defMinDist; // dist. calc could produce a negative number without this
      magDistIntercept = defMagDistIntercept;
      magDistSlope = defMagDistSlope;
      includePhases = false;
      includeMagAmps = false;
      includePeakAmps = false;
      includeCodas = false;
  }

  public void setProperties(GenericPropertyList props) {

      super.setProperties(props);   /// get all the standard stuff

      if (props != null) {
        String pre = getPropertyPrefix();
        if (props.isSpecified(pre+"magDistSlope") )
          setMagDistSlope(props.getDouble(pre+"magDistSlope"));

        if (props.isSpecified(pre+"magDistIntercept") )
          setMagDistIntercept(props.getDouble(pre+"magDistIntercept"));

      }

      if (debug) System.out.println (getParameterDescriptionString());
  }

  /** Returns a human-readable list of the current parameter settings.
  Adds parameters specific to this model not found in the generic model.*/
  public String getParameterDescriptionString () {
      Format df84 = new Format("%8.4f");
      StringBuffer sb = new StringBuffer(512);
        
      sb.append( super.getParameterDescriptionString());
      String pre = getPropertyPrefix(); 
      sb.append(pre).append("magDistSlope = ").append(df84.form(getMagDistSlope())).append("\n"); 
      sb.append(pre).append("magDistIntercept = ").append(df84.form(getMagDistIntercept())).append("\n");     
      
      return sb.toString();
  }

  public ArrayList getKnownPropertyKeys() {
      ArrayList list = super.getKnownPropertyKeys();
      String pre = getPropertyPrefix(); 
      list.add(pre + "magDistSlope");
      list.add(pre + "magDistIntercept");
      return list;
  }

} // ChannelTimeSetModel
