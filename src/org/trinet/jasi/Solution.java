// OVERLOADING VS. OVERIDDEN METHODS !
// CHECK EFFECT OF "IF"-TYPE BROADENING CASTS IN CLASS HIERARCHY,
// IS CORRECT OVERLOADED METHOD INVOKED IN SUBCLASSES?
// COMPILE TIME ARG-TYPE METHOD SIGNATURE USED, NOT RUNTIME ARG TYPE.
//
// TODO: Between the List types, Solution, Magnitude and their Reading types
// rework delete() such that the associated delete() delegates to the associator
// and the associators or the generic Lists methods use the setDeleteFlag flag
// rather than the delete() to toggle the flag, else feedback loop.
//
// TODO: Subclass from abstract solution without waveforms?
// TODO: Implement interface for Abstract Solution, Magnitude ?
// TODO: Create Factory classes for different Solution, Magnitude, etc. subtypes of JasiObjects
// TODO: reduce size of codebase for this class:
// Split out the String formater filters into utility class delegate, same for Magnitude etc.
// Split out the loading, getBy... Database accessors into factory class delegate same for Magnitude etc.
// 
//
// TODO: Put the event origin ids and derived summary results in a
// separate container data object that can be easily cloned and used
// to associate.
//
// TODO: Both Solution and Magnitude could have static boolean member
// "assumeStaleOnLoad" so that after preserving current states before
// states before load (needsCommit & isStale), they can be restored
// after DataSource load, loading from finalized archives should not
// necessarily force a state change if list is empty.
//
// TODO: Implication of adding an amp or coda to Solution lists 
// as to the effecting state of magnitude subtype. 
// Some amplitudes/coda not relevant magnitude calculations. -AWW
//
package org.trinet.jasi;

/**
 * Solution: Container of data descriptive of a seismic event.
 */

import java.sql.Connection;
import java.util.*;
import javax.swing.event.*;

import org.trinet.jasi.coda.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;

/**
 * Defines data members and methods that are exposed by all jasi Solution
 * objects.  All data members are DataObjects rather than primative types so
 * they can handle the concepts of "nullness", "mutability", and
 * "change-state".<p>
 *
 * NULLNESS = a field can be null, i.e. unknown or no value. The traditional
 *  approach has been to insert zero or some bogus value. In ASCII
 *  representations sometimes a field was just left blank. For example: RMS
 *  'distance' might*simply be unknown. To give it a value of 0.0km could foul
 *  up other calculations. When you write data out to a database you want to
 *  leave * null fields null and not write a bunch of zeros that were used as
 *  an expedient *inside of an application.<p>
 *
 * MUTABLITY = some fields should NEVER be explicity manipulated by an
 * application.  These are usually "key" fields that would break the database or
 * cause constraint errors. They would be set immutable. Think of these as
 * "read-only".<p>
 *
 * CHANGE-STATE = this is primarily an efficiency issue. If you read in 1000
 * records, change 10 of them, then save your work, you only want to write
 * ('update') the changed records to the database.<p>
 *
 * The database can contain null values for
 * certain fields. Also applications may not want to write a "bogus" value like 0
 * into the database when when the real value is not known. Data primatives can't
 * be null and are initiallize to zero. */

//public abstract class Solution extends SourceAssocJasiObject // changed descent hierarchy 10/03 aww
public abstract class Solution extends CommitableJasiObject    // which extends SourceAssocJasiObject
  implements Lockable, TimeDataIF, ErrorResidualIF,
             JasiCommitableAssociatorIF, GeoidalLatLonZ {

    /*
      Here's the list of public data members for a solution. These need to be
      supported by any port of this abstract layer
     */

    // End-user application program toggle  -aww
    public static boolean waveformCommitOk = true;
    public static boolean staleCommitOk = true;
    public static boolean commitResetsRflag = true;
    public static boolean dbAttributionEnabled = false; // don't attribute 
    public static boolean commitResetsAttrib = true;
    public static boolean commitOriginError = false; // don't save 
    public static boolean loadCatalogPrefMags = false; // don't 
    public static boolean authRegionEnabled = false; // don't 
    public static boolean resetAllEtypeAuth = false; // don't 
    public static boolean postInterimCommit = false; // don't 
    public static boolean calcOriginDatum = false; // when set false calcOriginDatumKm method returns 0. km; otherwise true, the avg iof phase sta elev km;

    protected static boolean autoSetPrefMag = false; // event preferred magnitude set by db stored procedure priority rules
    protected static boolean autoSetPrefOr = false; // event preferred origin set by db stored procedure priority rules
    protected static boolean useEventPreforTable = false; // event preferred origin set by db stored procedure priority rules

    protected boolean useTrialLocation = false;
    protected boolean useTrialTime = false;

    protected static String[] alarmDbLinks =  new String[0]; // event preferred origin set by db stored procedure priority rules

    /** Unique integer ID number of the Solution */
    public DataLong id            = new DataLong();
    public DataLong prefor        = new DataLong();
    public DataLong prefmec       = new DataLong();
    /** Unique integer ID number of the Solution that was assigned by an
     * external source. Imported data must be given a "safe" (guaranteed unique)
     * local ID when it is imported. This data member remembers the original,
     * external ID. May be null. THis is a string because some networks (e.g. NEIC)
     * use letters and numbers in their ID codes. */
    public DataString externalId  = new DataString();

    /** In some cases a Solution is "descended" or "cloned" from an original Solution.
    * This is the number of the "parent" Solution. May be null.*/
    public DataLong parentId      = new DataLong();

    /** Origin time in true UTC seconds since (00:00 Jan. 1, 1970), includes leap seconds. */
    public DataDouble datetime    = new DataDouble();

    /** Latitude of the hypocenter, decimal degrees, N positive */
    // use setLatLonZ(...) to set, effect magnitude state.
    public DataDouble lat         = new DataDouble();
    /** Longitude of the hypocenter, decimal degrees, E positive */
    // use setLatLonZ(...) to set, effect magnitude state.
    public DataDouble lon         = new DataDouble();
    /** Depth of hypocenter in km below vertDatum. Negative is above datum */
    // use setLatLonZ(...) to set, effect magnitude state.
    public DataDouble depth       = new DataDouble();

    /** Datum for lat and long */
    public DataString horizDatum  = new DataString();
    /** World Reference Datum for depth */
    public DataString vertDatum   = new DataString();

    /** A string describing the solution type:
        "H"=hypocenter, "C"=centroid, "A"=amplitude */
    public static final String HYPOCENTER = "H";
    public static final String CENTROID   = "C";
    public static final String AMPLITUDE  = "A";
    public static final String DOUBLEDIFF = "D";
    public static final String UNKNOWN_TYPE = "U"; // -aww added 2014/03/30

    public DataString type        = new DataString(HYPOCENTER);
    public DataString gtype       = new DataString(); // -aww added 2015/03/19

    /** String describing the location method (algorithm) */
    public DataString algorithm   = new DataString();
    /** String describing the crustal domain used. */
    public DataString crustModel  = new DataString(); // domain, like the auth 2-char
    /** String describing the velocity model used */
    public DataString velModel    = new DataString(); // 2-char modelid

    // new data field members Added 2015/03/16 -aww
    // see hyp1.4 doc p.112
    //169 1 A1 Depth type (M model depth, G geoid depth) set with GEO command.
    //170 1 A1 Dominant crust model type: T=CRT, H=CRH, E=CRE, V=CRV, L=CRL
    //171 4 I4 Earthquake depth datum in m above the geoid (CRT model surface).
    //175 5 F5.2 Geoid depth in km below the geoid or sea level.
    public DataString depthCrustType = new DataString();
    public DataString crustModelType = new DataString();
    public DataString crustModelName = new DataString();
    public DataDouble mdepth = new DataDouble();
    // end of new data field members Added 2015/03/16 -aww

    /** Authority of the initial declaration of the event. An event declared or created
    * by one authority may be relocated by another. Thus, the 'authority' and 'eventauthority'
    * would be different.*/
    public DataString eventAuthority  = new DataString(); // now NULL, was EnvironmentInfo.getNetworkCode(); -aww 2011/08/17

    /** Source of the initial declaration of the event. An event declared or created
    * by one application may be relocated by another. Thus, the 'source' and 'eventsource'
    * would be different. */
    public DataString eventSource  = new DataString(EnvironmentInfo.getApplicationName());

    /** DataString containing username of the analyst that created this solution. */
    //private DataString who         = new DataString(EnvironmentInfo.getUsername());
    private DataString who         = new DataString("---"); // unknown until synched with stored attribution

    protected DataTimestamp solveDate  = new DataTimestamp(); // updated by engine ?
     
    // Are versions char values like alphabet or numeric? -aww
    protected DataLong  srcVersion   = new DataLong(); // data source (db) official version if any? -aww
    private DataLong  version        = new DataLong(0l); // counter for engine solve iterations ? -aww

    /** Largest azimuthal gap of solution in degrees. */
    public DataDouble gap          = new DataDouble();

    /** Distance in km to the nearest station that contributed to the solution */
    public DataDouble distance     = new DataDouble();
    /** RMS error of solution */
    public DataDouble rms          = new DataDouble();
    /** Error in origin time in seconds. */
    public DataDouble errorTime    = new DataDouble();
    /** Horizontal error in km. */
    public DataDouble errorHoriz   = new DataDouble();
    /** Vertical error in km. */
    public DataDouble errorVert    = new DataDouble();
    /** Latitude error in degrees. */
    public DataDouble errorLat     = new DataDouble();
    /** Longitude error in degress. */
    public DataDouble errorLon     = new DataDouble();
    /** Total phase readings, of all types associated with the solutions. */
    public DataLong totalReadings  = new DataLong();
    /**  Total phase readings, of all types used in the solution. */
    public DataLong usedReadings   = new DataLong();
    /**  Total automatic picked phase readings, of all types used in the solution. */
    public DataLong autoUsedReadings  = new DataLong();
    /**  Total S-phase readings, of all types used in the solution. */
    public DataLong sReadings      = new DataLong();
    /** Total first motion reading associated with this solution */
    public DataLong firstMotions   = new DataLong();

    /** Solution quality expressed as a number between 0.0 and 1.0; 0.0 is the worst*/
    public DataDouble quality      = new DataDouble();

    /** Solution priority, this is a site defined value that can be used to
    * determine the priority of events for processing tasks. */
    public DataDouble priority     = new DataDouble();

    /** A free-format comment text string. May not be supported by
    * all data sources or may be limited in length.*/
    public DataString comment      = new DataString();

    /** Equals 0 if the solution should not be considered valid. */
    final long DefaultValidFlag    = 1;
    public DataLong validFlag      = new DataLong(DefaultValidFlag);

    /** Equals 1 if this is a dummy solution. A dummy solution is often needed
    * to hold incomplete Solution information. For example a Solution for which
    * no viable location was possible. Events with type 'trigger' will usually
    * have dummyFlag = 1. */
    final long DefaultDummyFlag    = 0;
    public DataLong dummyFlag      = new DataLong(DefaultDummyFlag);

    // Event associations
    public DataString association = new DataString();
    
    /** A string describing the event type. Possible types are: <p>
        <ul>
        <li> local </li>
        <li> quarry </li>
        <li> regional </li>
        <li> teleseism </li>
        <li> sonic </li>
        <li> nuclear </li>
        <li> trigger </li>
        <li> unknown </li>
        </ul>
     @see: EventTypeMap
     */
    public DataString eventType = new DataString(EventTypeMap3.getDefaultJasiType());   //"unknown"


    /** True if the solution's depth is fixed. */
    // Defaults to "false". If you use "new Databoolean()" it sets isUpdate = true!
    public DataBoolean depthFixed    = new DataBoolean();
    /** True if the solution's location is fixed. */
    public DataBoolean locationFixed = new DataBoolean();
    /** True if the solution's origin time is fixed. */
    public DataBoolean timeFixed     = new DataBoolean();

    // OriginError attributes - aww 07/12/2006
    protected DataDouble aziLarge = new DataDouble();
    protected DataDouble dipLarge = new DataDouble();
    protected DataDouble errLarge = new DataDouble();
    protected DataDouble aziInter = new DataDouble();
    protected DataDouble dipInter = new DataDouble();
    protected DataDouble errInter = new DataDouble();
    protected DataDouble errSmall = new DataDouble();
    //

    /** Number of waveforms associated with this solution */
    public DataLong waveRecords      = new DataLong();

    /** List of alternate solutions for this solution. This list does not contain
     * the preferred (this) solution. */
    public GenericSolutionList altSolList = new GenericSolutionList(); // instead of SolutionList -aww?

    /** The preferred Magnitude associated with this Solution.
     * LISTENER MODEL ONLY WORKS IF MAGNITUDE IS ONLY SET VIA METHODS
     * AND NOT BY SIMPLE ASSIGNMENT!
     */
    // Do we need a preferred magnitude list for each known type? aww
    // to enable add/remove reading ListDataStateListeners for magnitude
    // TODO: make prefmag access private to force use of get/set preferred methods
    public Magnitude magnitude = null;
    //private static final Magnitude NULL_MAGNITUDE = Magnitude.create();

    static protected boolean listenToMagReadingListAdd    = true;
    static protected boolean listenToMagReadingListRemove = false;
    static protected boolean listenToMagReadingListState  = false;

    //Note: phase list size change will make Solution stale by default
    protected boolean listenToPhaseListChange  = true;

    protected HashMap prefMagMap = new HashMap(11);

    /** List of magnitudes associated with the solution.
     * This list contains both the preferred magnitude and all non-preferred mags.
     * Magnitudes in list listen to the solution lists of interest.
     * */
    public MagList altMagList = new MagList();
    // we need to monitor the MagList for changes:
    { altMagList.addListDataStateListener(new MagListDataStateListener()); }

    /** List of phases associated with this Solution */
    public PhaseList phaseList = PhaseList.create();

    // to enable setStale(true) of the location data of this Solution 
    // we need to monitor the phaseList for changes:
    { phaseList.addListDataStateListener(new PhaseDataStateListener()); }

    /**
     * List of amplitude reading associated with this solution.
     */
    public AmpList ampList = AmpList.create();

    protected ListDataStateListener magAmpDataStateListener =
                 new MagReadingDataStateListener();
    /**
     * List of codas observations associated with this solution.
     */
    public CodaList codaList = CodaList.create();

    protected ListDataStateListener magCodaDataStateListener =
                 new MagReadingDataStateListener();

    /** List of Waveform object associated with this Solution.  Note that not
     * all schemas make a connection between solutions and waveforms.  This is
     * provided to accommodate those that do.*/
    public ChannelableList waveformList = new ChannelableList();

  /*

  // Listeners could be added to Solution's ampList and codaList in order
  // to effect state needsCommit == true transition for list additions
  // independent of Magnitude such as when Amps or Coda are generated 
  // without an association to the preferred Magnitude calculation.
  //
  // Add list listener data lists above in order to setNeedsCommit(true) if changed?
  {
     ampList.addListDataStateListener(new OtherDataStateListener());
     codaList.addListDataStateListener(new OtherDataStateListener());
  }

  protected class OtherDataStateListener implements ListDataStateListener {
    public void intervalAdded(ListDataStateEvent e) {
      System.out.println("Sol - my interval added in :"+e.getSource().getClass().getName());
      setNeedsCommit(true); // have more, need to save assoc
    }
    public void intervalRemoved(ListDataStateEvent e) {
      System.out.println("Sol - my interval removed in :"+e.getSource().getClass().getName());
      //setNeedsCommit(true); // have fewer, unless reloc sol, not relevant
    }
    public void contentsChanged(ListDataStateEvent e) {
      System.out.println("Sol - my contents changed in :"+e.getSource().getClass().getName());
      setNeedsCommit(true); // may have more to save assoc
    }
    public void stateChanged(ListDataStateEvent e) {
      System.out.println("Sol - my state changed in :"+e.getSource().getClass().getName());
      //setNeedsCommit(true); // what state would be relevant to commit?
    }
    public void orderChanged(ListDataStateEvent e) {}
  }//end of OtherDataListener class
*/
  
    /** A solutionLock instance to support the Lockable interface. Will be null
    * if locking is not supported by the datasource */

    protected SolutionLock solLock = SolutionLock.create();

    /** Set true to lookup comments for each solution in the data source.
    * Default is 'true'. */
    protected boolean includeComment = true;

    /**
     * NOTE ON COLLECTIONS Java (SWING) Collections are abstract. The caller
     * must use a concrete subclass where ever "Collection" is used as an
     * argument or return type here. Use of the abstract type "Collection" here
     * allows the user to choose ANY subclass they choose with on modification to
     * the jasi classes. */

    /** True if changes have been made to the phase list and a relocation is
     needed.  */
    // do not to toggle isStale external to method setStale(), side-effect on magnitude state
    private boolean isStale = false;

    /** Summary string describing status of the last commit operation. */
    protected StringBuffer commitStatusMessage = null;

    // true=>commit() all undeleted, non-stale alternate mags assigned to this sol 
    public static boolean commitAlternateMagnitudes = false; // static needs testing 06/29/2004 -aww

// //////////////////////////////////////////////////////////////////////
    /**
     * constructor is 'friendly' so that you can only instantiate jasi objects
     * using the 'create()' factory method.
     */
    protected Solution() { }


    public Object clone() {
        System.out.println("Solution cloning not implemented!");
        return null;
    }

// -- Concrete FACTORY METHODS ---
    /*
     o Must be static so we can call with Solution.create()
     o Can't be abstract in JasiObject because this must return a Solution object
       and JasiObject can only return Object.
     o Its final so concrete children can't override.
    */


    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. Creates a Solution of the DEFAULT type.
     * @See: JasiObject
     */
     public static final Solution create() {
        return create(DEFAULT);
    }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is an integer implementation type.
     * @See: JasiObject
     */
    public static final Solution create(int schemaType) {
      return create(JasiFactory.suffix[schemaType]);
    }

    /**
     * Instantiate an object of this type. You do
     * NOT use a constructor. This "factory" method creates various
     * concrete implementations. The argument is as 2-char implementation suffix.
     * @See: JasiObject
     */
    public static final Solution create(String suffix) {
        Solution sol = (Solution) JasiObject.newInstance("org.trinet.jasi.Solution", suffix);
        sol.setDefaultValues();
        return sol;
    }

    public abstract String getEventAuthority();
    public abstract String getRegionAuthority();

// ////////////////////////////////////////////////////////////////////////////
    /**
     * Returns the Solution with this ID number from the data source.
     * No other criteria or flags are checked.
     */
    abstract public Collection getByState(String state);
    abstract public Solution getById(Connection conn, long evid);
    abstract public Solution getById(long evid);
    //abstract public Collection getAllById(long evid);  // includes event.prefor
    //abstract public Collection getAllById(Connection conn, long evid);  // includes event.prefor
    abstract public Collection getAlternatesById(long evid);
    abstract public Collection getAlternatesById(Connection conn, long evid);
    abstract public Solution getByPreferredOrid(long orid);

    /**
     * Returns the Solution with this ID number from the default data source.
     * No other criteria or flags are checked.
     */
    public JasiCommitableIF getById(Object obj) {
      return getById(((DataLong)obj).longValue());
    }

    /**
     * Returns array of Solutions within this time window. Times are true UTC seconds.
     * Returns null if no event is found. No other criteria or flags are checked.
     */
    abstract public Collection getByTime(double start, double stop);

    /**
     * Returns array of Solutions within this time window. Times are true UTC seconds.
     * Returns null if no event is found.  No other criteria or flags are checked.
     */
    abstract public Collection getByTime(Connection conn, double start, double stop);

    /**
     * Returns array of "valid" Solutions within this time window.
     * Some data sets contain solutions that have duplicates, interim, or bogus
     * entries. This method filters those out and returns only VALID solutions.
     * This means the 'validFlag' is true and the 'dummyFlag' is either false or
     * 'dummyFlag is true but the processing state is not 'automatic'.
     *
     * @see: EventSelectionProperties
     */
    abstract public Collection getValidByTime(double start, double stop);
    /**
     * Returns array of "valid" Solutions within this time window.
     * Some data sets contain solutions that have duplicates, interim, or bogus
     * entries. This method filters those out and returns only VALID solutions.
     * This means the 'validFlag' is true and the 'dummyFlag' is either false or
     * 'dummyFlag is true but the processing state is not 'automatic'.
     *
     * @see: EventSelectionProperties
     */
    abstract public Collection getValidByTime(Connection conn,
                                               double start, double stop);

     /**
     * Returns array of Solutions that meet the properties defined in
     * EventSelectionProperties.
     */
    abstract public Collection getByProperties(EventSelectionProperties properties);

     /**
     * If this Solution instance has a valid DataSource evid and the input properties are not null,
     * returns a new Solution instance with the matching id if its attributes 
     * meet the input properties, otherwise returns null.
     * However, if the input properties are null, returns same result as getById(evid).
     * @see #getById(long)
     */
    abstract public Solution refresh(EventSelectionProperties properties);

    /**
     * Returns array of Solutions that meet the properties defined in
     * EventSelectionProperties.
     */
    abstract public Collection getByProperties(Connection conn,
                                               EventSelectionProperties properties);
    /**
    * Sets update state of all dependent DataObject members to the input boolean value.
    */
    abstract public void setUpdate(boolean tf);

    /** Set the default values of some fields. Concrete classes can override this
    method if a different set of defaults is desired. */
    public void setDefaultValues() {
        // make new instances here rather then setValue() because that would
        // set the modified flag true.
        type                = new DataString(HYPOCENTER);
        validFlag           = new DataLong(DefaultValidFlag);
        eventType           = new DataString(EventTypeMap3.getDefaultJasiType());
        processingState     = new DataString(EnvironmentInfo.getAutoString());
        gtype               = new DataString(GTypeMap.LOCAL);
        // used to be processingState     = new DataString(DefaultProcessingState);
//        depthFixed        = new DataBoolean(false);
//        locationFixed     = new DataBoolean(false);
//        timeFixed         = new DataBoolean(false);
    }

    public boolean equivalent(Object obj) {
      if (! super.equivalent(obj)) return false;
      Solution sol = (Solution) obj;
      return ( eventSource.equals(sol.eventSource) && algorithm.equals(sol.algorithm));
    }

    /**
     * Return true if any Solution field is different from what's in the DataSource.
     * Either, 1) its been changed and not saved or 2) it is newly created and not saved.
     */
     abstract public boolean hasChanged() ;

     abstract public boolean existsInDataSource() ;

    /** If true, solution is from the preferred selection from the data source and
     * its attributes have been derived from some solving algorithm.
     * */
    public boolean isValid() { return (validFlag.longValue()==1l);}

    /** If true, solution is incomplete or placeholder. */
    public boolean isDummy() { return (dummyFlag.longValue()==1l);}

    /** If true, event preferred magnitude is set by priority rules in database upon commit. Default is false. */
    public static void setPreforByRulesOnCommit(boolean tf) {
        autoSetPrefOr = tf;
    }
    /** If true, event preferred magnitude is set by priority rules in database upon commit. Default is false. */
    public static void setPrefMagByRulesOnCommit(boolean tf) {
        autoSetPrefMag = tf;
    }

    /** Sets "isStale" and "needsCommit" flags of this Solution and all of its magnitudes to false. */
    public void resetStatusFlags() {
        isStale = false;
        setNeedsCommit(false);
        if (magnitude != null) {
          magnitude.setStale(false);
          magnitude.setNeedsCommit(false);
        }
        Magnitude[] magArray = getMagList().getArray();
        for (int idx=0;idx<magArray.length; idx++) {
            if ( magArray[idx] != magnitude) {
              magArray[idx].setStale(false);
              magArray[idx].setNeedsCommit(false);
            }
        } 
    }

    public void setMagnitudeNeedsCommit() {
        if ( magnitude != null && magnitude.dependsOnOrigin() ) magnitude.setNeedsCommit(true);
        Magnitude[] magArray = getMagList().getArray();
        for (int idx=0;idx<magArray.length; idx++) {
            if ( magArray[idx] != magnitude && magArray[idx].dependsOnOrigin() ) magArray[idx].setNeedsCommit(true);
        } 
    }

    public void setMagnitudeStatusStale() {
        if ( magnitude != null && magnitude.dependsOnOrigin() ) magnitude.setStale(true);
        Magnitude[] magArray = getMagList().getArray();
        for (int idx=0;idx<magArray.length; idx++) {
            if ( magArray[idx] != magnitude && magArray[idx].dependsOnOrigin() ) magArray[idx].setStale(true);
        } 
    }

    /** Set value of isStale flag. If true, solution should be relocated. */
    public void setStale(boolean tf) {
        isStale = tf;
        // setStale=true forces need for commit but setStale=false does NOT
        // That must be done only after a commit();
        if (isStale) {
            setNeedsCommit(true); // stale event need commit

            // If location (lat,lon,z) changes the magnitudes need recalculation.
            // Below line sets all origin all dependent Magnitudes stale. -aww
            // Note that LocationEngine must also do so if location changes
            // upon relocation success because its input origin may be not stale. -aww
            setMagnitudeStatusStale();
        }
    }

    /** True if changes have been made to the phase list and a relocation is
    needed.  */
    public boolean isStale() { return isStale; };
    // Need to handle case where the prior solved LatLonZ is user app changed like
    // when setting a forced trial location different from the solved LatLonZ,
    // so setLatLonZ method was changed to toggle stale if location is changed
    // the locationEngine should always set the new latLonZ before the stale is reset -aww
    public boolean hasStaleLocation() { return isStale; };

    public boolean useTrialLocation() {
        return useTrialLocation;
    }
    public void setUseTrialLocation(boolean tf) {
        useTrialLocation = tf;
    }

    public boolean useTrialTime() {
        return useTrialTime;
    }
    public void setUseTrialTime(boolean tf) {
        useTrialTime = tf;
    }

    /** True if changes have been made to the coda or amp list
     * and a magnitude should be recalculated.
     * */
    public boolean hasStaleMagnitude() {
        //Hey, if location isStale, doesn't the preferred magnitude need recalculation? -aww
        return magnitude.isStale();
    };

    public boolean hasStalePrefMag() {
        Iterator iter = getPrefMags().iterator();
        boolean stale = false;
        while (iter.hasNext() && ! stale) {
            stale &= ((Magnitude) iter.next()).isStale();
        }
        return stale;
    }

    public boolean prefMagNeedsCommit() {
        Iterator iter = getPrefMags().iterator();
        boolean needsCommit = false;
        while (iter.hasNext() && ! needsCommit) {
            needsCommit &= ((Magnitude) iter.next()).getNeedsCommit();
        }
        return needsCommit;
    }

    public void setAuthRegion(String auth) {
        // Change only if current value does not equal the input
        if (!eventAuthority.toString().equals(auth)) {
            eventAuthority.setValue(auth); // reverted to event row only -aww  2011/09/21
        }

        /*
        // Do origin row reset to eventAuthority (2011/08/12)
        if (!authority.toString().equals(auth)) authority.setValue(auth);
        // And reset prefmag rows (2011/08/12)
        Iterator iter = getPrefMags().iterator();
        Magnitude mag = null;
        while (iter.hasNext()) {
            mag =(Magnitude) iter.next();
            // Ooops! What magtypes, subsource?
            if ( "ldwhe".indexOf(mag.getTypeSubString()) >= 0 || mag.getSource().equals(EnvironmentInfo.getApplicationName()) ) {
                if (!mag.authority.toString().equals(auth)) mag.setAuthority(auth);
            }
        }
        // Commit: updates event row auth etc. and writes new origin/netmag rows and new reading associations use origin/netmag auth
        // Alternate is to UPDATE AUTH in phaseList,ampList,codaList contents for all prefmag and prefor here
        // or after event commit call stored epref package function
        */
    }

    /** Returns true if the solution or magnitude needs to be committed to the
     *  data source. That is, some part of solution or its magnitude has changed
     * since it was read from the data source or the last commit. */
    public boolean getNeedsCommit() {
       return needsCommit || hasChanged()
           || (magnitude != null && magnitude.getNeedsCommit())
           || prefMagNeedsCommit();
    }

    /** Return a string describing the results of the commit() operation.
    This is the same message that would be returned by
    JasiCommitException.getMessage() if an exception is thrown. */
    public String getCommitStatus() {
      return (commitStatusMessage == null) ? "" : commitStatusMessage.toString();
    }

    /** Convenience method to save event, origin, waveform and reading data
     * back to data source before doing any processing that depends
     * on these data (e.g. magnitude calculations).<br>
     * Does not commit associated Magnitudes.
     * @return  <i>false </i> data processing error.
     */
    abstract public boolean commitReadings() throws JasiCommitException;

    /** Commit any additions, changes or deletions to the data source plus
    * take any site-specific actions for a final solution. */
    abstract public boolean finalCommit() throws JasiCommitException;

    /** Set DataObject null only if its not already. Otherwise the 'Update' flag would get
    set even if there was no real change. This should be faster then creating
    new instances of the data objects. */
    public void resetValue(DataObject dataObject) {
      if (!dataObject.isNull()) dataObject.setNull(true);
    }

    /**
     * Set all location dependent fields to default values.
     * This should be done before parsing a relocation
     * to insure old values from an earlier location are not left by an imperfect parse
     * of a subsequent location result.
     */
    public void clearLocationAttributes() {

        //resetValue(eventType) ;  // Event table

        resetValue(datetime);

        nullLatLonZ();

        resetValue(authority) ; // ?? lookup region, or set to EnvironmentInfo network code ??
        resetValue(source) ; // ?? set to EnvironmentInfo application name ??

        clearOriginParameters();
    }


    public void nullLatLonZ() {
        resetValue(lat);
        resetValue(lon);
        resetValue(depth);
        resetValue(mdepth);
    }

    public void clearOriginParameters() {
        resetValue(horizDatum) ;
        resetValue(vertDatum) ;
        resetValue(algorithm) ;
        resetValue(crustModel) ;
        resetValue(gtype) ;
        resetValue(velModel) ;
        resetValue(gap) ;
        resetValue(distance) ;
        resetValue(rms) ;
        resetValue(errorTime) ;
        resetValue(errorHoriz) ;
        resetValue(errorVert) ;
        resetValue(errorLat) ;
        resetValue(errorLon) ;

        resetValue(totalReadings) ;
        resetValue(usedReadings) ;
        resetValue(sReadings) ;
        resetValue(firstMotions) ;
        resetValue(externalId) ;
        resetValue(quality) ;

//        resetValue(type) ;
//        resetValue(processingState) ;
//        resetValue(depthFixed) ;
//        resetValue(locationFixed) ;
//        resetValue(timeFixed) ;

        //setDefaultValues(); // removed, it reset update state of Event table values, location method may also update processingState -aww 2010/08/24

        // added test to not reset origin rflag 2015/04/03 -aww
        if ( commitResetsRflag && !processingState.equalsValue(EnvironmentInfo.getAutoString()) )
          processingState.setValue(EnvironmentInfo.getAutoString()); //automatic or human?


    }

    /**
     * Set the processing state of the solution. The argument is one of the following:<p>
     * JasiProcessingConstants.STATE_UNKNOWN <br>
     * JasiProcessingConstants.STATE_AUTO <br>
     * JasiProcessingConstants.STATE_HUMAN <br>
     * JasiProcessingConstants.STATE_INTERMEDIATE <br>
     * JasiProcessingConstants.STATE_FINAL <br>
     * JasiProcessingConstants.STATE_CANCELLED <br>
     */
    public void setProcessingState(int newState) {
        if (getProcessingStateInt() == newState) {
            return; // no change, no-op
        } else if (newState == JasiProcessingConstants.STATE_UNKNOWN) {
            processingState.setNull(true);
        } else {
            processingState.setValue(ProcessingState.getTag(newState));
        }
    }

    /**
     * Returned value indicates the current processing state of the solution.
     * @return one of the following:<p>
     * JasiProcessingConstants.STATE_UNKNOWN <br>
     * JasiProcessingConstants.STATE_AUTO <br>
     * JasiProcessingConstants.STATE_HUMAN <br>
     * JasiProcessingConstants.STATE_INTERMEDIATE <br>
     * JasiProcessingConstants.STATE_FINAL <br>
     * JasiProcessingConstants.STATE_CANCELLED <br>
     */
    public int getProcessingStateInt() {
        //if (this.processingState.isNull()) return JasiProcessingConstants.STATE_UNKNOWN;
        return ProcessingState.tagToId(processingState.toString());
   }

   public boolean isCancelled() {
       return JasiProcessingConstants.STATE_CANCELLED_TAG.equals(processingState.toString());
   }

   abstract public int postId(String group, String source, String state, int rank, int result);

   public boolean isEventDbType(String dbType) {
     if (dbType == null) return false;
     return getEventTypeString().equalsIgnoreCase(EventTypeMap3.getMap().toJasiType(dbType));
   }

   public boolean isEventJasiType(String jasiType) {
     if (jasiType == null) return false;
     return getEventTypeString().equalsIgnoreCase(jasiType);
   }
 
    /** Returns the distance in kilometers from the hypocenter of this Solution
     * to the location defined by the input reading's Channel latitude and longitude 
     * and elevation.
     * Returns Channel.NULL_DIST if the LatLonZ coordinates of the input or Solution
     * are undefined.
    */ 
    public double calcSlantDistance(JasiReading jr) {
        LatLonZ myllz = getLatLonZ();
        if ( myllz.isNull() ) return Channel.NULL_DIST;

        LatLonZ llz = (LatLonZ) jr.getChannelObj().getLatLonZ().clone();
        if (llz.isNull())  return Channel.NULL_DIST;
        //llz.setZ(0.); // else remove channel elevation term  - commented out on 2015/06/11 -aww

        return GeoidalConvert.distanceKmBetween(myllz, llz); // hDist,solDepth
    }

    //
    public double calcHorizDistance(LatLonZ llz) {
        if (llz.isNull())  return Channel.NULL_DIST;
        LatLonZ myllz = getLatLonZ();
        if (myllz.isNull())  return Channel.NULL_DIST;
        return GeoidalConvert.horizontalDistanceKmBetween(myllz, llz);
    }
    //

/**
 * Return a LatLonZ object for this Solution. Returns an empty LatLonZ() object
 *  if either lat, lon, or depth are null.
 */
    public LatLonZ getLatLonZ() {
        if (lat.isNull() || lon.isNull() || depth.isNull()) {
                //System.out.println("DEBUG Solution null lat,lon, or depth");
                return new LatLonZ();
        }
        return new LatLonZ(lat.doubleValue(), lon.doubleValue(), depth.doubleValue());
    }

    public boolean hasLatLonZ() {
        //return (lat.isNull() || lon.isNull() || depth.isNull());
        return !getLatLonZ().isNull();
    }

    /** Geoid depth.**/
    public double getDepth() {
        return depth.doubleValue();
    }

    /** Location model depth. */
    public double getModelDepth() {
        return mdepth.doubleValue();
    }

    /** Km above geoid (CRH model surface). **/
    public double getDepthDatum() {
        return mdepth.doubleValue() - depth.doubleValue();
    }

    /** Geographic type, returns empty string when undefined. **/
    public String getOriginGTypeString() {
        return (gtype.isBlankOrNull()) ? "" : gtype.toString(); // jasi type not db type
    }

    /** Geographic type, returns true when input string matches instance value. **/
    public boolean isOriginGType(String gType) {
        return gtype.equalsValue(gType); // jasi type not db type
    }

/**
 * Set the LatLonZ of this Solution.
 * Sets stale state true if location changes from previous values.
 */
    public void setLatLonZ(double latitude, double longitude, double zDepth) {
       LatLonZ oldLatLonZ = getLatLonZ();
       lat.setValue(latitude);
       lon.setValue(longitude);
       depth.setValue(zDepth);
       // short-circuits if already stale, setStale(true) already invoked.
       // thus be careful not to toggle isStale external to method setStale() !
       // Note if old LatLonZ value is not defined don't set stale,
       // to allow case of hand entered without phase data -aww 03/24/2005
       if (! (isStale || oldLatLonZ.isNull() || oldLatLonZ.equals(getLatLonZ())) ) setStale(true);

       sortReadingLists();
    }
/**
 * Set the LatLonZ of this Solution.
 * Sets stale state true if location changes from previous values.
 */
    public void setLatLonZ(LatLonZ latlonz) {
       setLatLonZ(latlonz.getLat(), latlonz.getLon(), latlonz.getZ());
    }
    /** Return a DataString containing the last analyst to locate this event.
     *  May be null. */
    public DataString getWho() {
       return who;
    }
    /** Set name of the last analyst to locate this event. */
    public void setWho(String name) {
      who.setValue(name);
    }
    /** Set name of the last analyst to locate this event. */
    public void setWho(DataString name) {
      who.setValue(name);
    }

    /** Set the event type. Argument is a jasi-style event type description string.
     * Checks validity of the string against list of types in EventTypeMap class.
     * Returns 'true' if type is OK. If not returns 'false' and sets type to the
     * default type.
     * Concrete classes will need to translate to the event type identification scheme
     * of the underlying data.
     * @see org.trinet.jasi.EventTypeMap */
    abstract public boolean setEventType(String type);
    abstract public boolean setOriginGType(String type);

    /** Implement algorithm to use rules applying quarry metadata in datasource,
     * by default returns true if declared event type is quarry. */
    abstract public boolean isQuarry();

    public boolean isInsideNetwork() {
        // old way
        if (! EnvironmentInfo.hasAuthNetCodes()) {
            return isInsideRegion(EnvironmentInfo.getNetworkCode());
        }
        // for operation networks like Utah with more than 1-code (UU,WY)
        // -aww 2008/12/10
        String[] authCodes = EnvironmentInfo.getAuthNetCodes();
        boolean status = false;
        for (int idx = 0; idx < authCodes.length; idx++) {
            status |= isInsideRegion(authCodes[idx]);
            if (status) break;
        }
        return status;
    }

    abstract public boolean isInsideRegion(String regionName);

    abstract public long getAnyDuplicate();

    /** Return the local implentation's event type string.
     * @see org.trinet.jasi.EventTypeMap */
    abstract public String getEventTypeString();

    public Object getTypeQualifier() { return getEventTypeString();}

    public String getOriginType() { return ( type.isNull() ? UNKNOWN_TYPE : type.toString() );} // -aww added 2014/03/30

    public double getTime() {
       return datetime.doubleValue();
    }

    public double getAge() {
        return (hasValidDateTime()) ?
            (((double) System.currentTimeMillis())/1000.0 - LeapSeconds.trueToNominal(datetime.doubleValue())) : Double.NaN;
    }

    public DateTime getDateTime() {
       return new DateTime(datetime.doubleValue(), true); // for UTC seconds -aww 2008/02/11
    }
    public boolean hasValidDateTime() {
       return datetime.isValidNumber();
    }

    // Should be time of last engine run, isNull=true implies no run on instance since instantiation
    public DataTimestamp getSolveDate() {
        return solveDate;
    }
    public void setSolveDate(java.util.Date d) {
        solveDate.setValue(d);
    }
    // Set by engine when this instance is processed location engine
    public void setSolveDate() {
        solveDate.setValue(new java.util.Date());
        version.setValue(getVersion()+1);
    }
    // Relative iteration set by engine when this instance is processed by location engine, starts with zero
    public long getVersion() {
       // iteration of solve e.g. origin location
        return (version.isValidNumber()) ? version.longValue() : 0l;
    }
    public void setVersion(long val) {
        version.setValue(val);
    }
    // Database event table version (value bumped when prefor,prefmag,prefmec changes)
    public long getSourceVersion() {
        return (srcVersion.isValidNumber()) ? srcVersion.longValue() : -1l;
    }
    // Database event table version (value bumped when prefor,prefmag,prefmec changes)
    public void setSourceVersion(long val) {
        srcVersion.setValue(val);
    }

    public void setTime(double seconds) { datetime.setValue(seconds); }

    public void setTime(java.util.Date dt) { // -aww 2008/02/10
        if (dt instanceof DateTime) {
            setTime(((DateTime)dt).getTrueSeconds());
        }
        else {
            setTime(LeapSeconds.dateToTrue(dt));
        }
    }

    // method for other IF:
    public double getResidual() {
      return (rms == null) ?  Double.NaN : rms.doubleValue();
    }

   /** Set the comment string. Override this method is the concrete
   data source must limit the length or format of the string. */
    public void setComment(String str) {
      if (! comment.equalsValue(str)) comment.setValue(str);
    }
/** Get the comment string. Returns null if there is no comment. */
    public String getComment() {
      return comment.toString();
    }
/** Returns 'true' if there is a comment. */
    public boolean hasComment() {
      return !comment.isBlankOrNull();
    }
    /**  Set true to lookup comments for each solution in the data source.*/
    public void setIncludeComment(boolean tf) {
      includeComment = tf;
    }
    /**  If true comments for each solution will be extracted from the data source.*/
    public boolean getIncludeComment(){
      return includeComment;
    }

    public Object getIdentifier() { return id;}
    public void setIdentifier(Object obj) { id.setValue((DataLong) obj);}
    public boolean idEquals(long id) { return super.idEquals(Long.valueOf(id)); }

    /**
    * Set the ID of the Solution. The ID must be a unique long integer.
    */
    public void setId(long newId) {
      // toggles id update flag only if necessary
      if (id.longValue() != newId) id.setValue(newId); 
    }
    /**
    * Return the ID of the Solution.
    */
    public DataLong getId() { return (DataLong) getIdentifier();}
  /* 
   * Used to retrieve the next valid solution ID from
   * the JASI data source.
   * Because this is an abstract function, it cannot be static;
   * however, it is logically static because it is not related to the current
   * solution object, it only uses that object to access the appropriate
   * JASI concrete class.
   */
    abstract public long getNextId();

    /** Set the ID of the Solution this Solution is "descended" or "cloned" from.
    */
    public void setParentId( long parentId) {
      this.parentId.setValue(parentId);
    }
    /** Return the ID of the Solution this Solution is "descended" or "cloned" from.
    */
    public DataLong getParentId() {
       return parentId;
    }

    /** Returns true if this is a clone event. */
    public boolean isClone() { // added 05/25/2007 -aww
       return (id.longValue() != parentId.longValue()) ;
    }

    /**
     * Set this solution's ID to a unique number that is valid in the local
     * context.  Returns the unique value. Returns 0 if there is an error.
     * */
    abstract public long setUniqueId();

    /** Resets the prefor and prefmag in the database to the values,
     * if any, that  were read originally from database when Solution is
     * first created from database query.
     */
    abstract public boolean rollbackPrefs();

    /**
     * Returns an identifier uniquely indentifying the preferred magnitude
     * of this Solution. Returns null if not defined.
     * */
    public Object getPreferredMagId() {
        return (magnitude == null) ? null : magnitude.getIdentifier();
    }

    /**
     * Returns an identifier uniquely indentifying the preferred origin
     * of this Solution. Returns null if not defined.
     * */
    public Object getPreferredOriginId() {
        return prefor;
    }

// Lockable interface
/** Lock this solution, returns true on success, false on failure.
 * If locked, information on the current lock holder is in the data members. */
  public boolean lock() {
    if (solLock == null) return false;    // locking not supported
    solLock.setSolution(this);
    return solLock.lock();
  }

/** Release the lock on this solution. Returns true even if the lock was not held
 * in the first place. */
  public boolean unlock() {
    if (solLock == null) return false;    // locking not supported
    solLock.setSolution(this);
    return solLock.unlock();
  }

/** Returns true if this solution is locked by anyone, the caller included. */
  public boolean isLocked() {
    if (solLock == null) return false;    // locking not supported
    solLock.setSolution(this);
    return solLock.isLocked();
  }

/** Returns true if this solution is locked by the caller. */
  public boolean lockIsMine() {
    if (solLock == null) return false;    // locking not supported
    solLock.setSolution(this);
    return solLock.lockIsMine();
  }
  public String getLockString() {
    return (solLock == null) ? "" : solLock.toString();
  }
// end of Lockable interface

    /** Convenience wrapper to clear the loaded data lists of their contents,
     * doesn't notify list listeners. Invoke this only when the list data is
     * no longer used and should be garbage collected.
     * Clears the amp, coda, magnitude, phase, and waveform lists.
     * Invokes clearDataLists on the preferred magnitude as well.
     * @see Magnitude#clearDataLists(boolean)
     * */
    public void clearDataLists(boolean verbose) {
      if (getNeedsCommit() && verbose) {
        System.err.println("WARNING - Solution clearDataLists() Solution needed commit: " + id.toString());
      }
      // It is better to clear existing, or create new lists with listeners here,
      // what should be done - aww ??
      // Let's try clear WITHOUT notifying listeners ...
      magnitude.clearDataLists(verbose);
      // if other mags in list, do we want to clear their data lists too?
      // just clear the mag list for now:
      altMagList.clear(false);
      prefMagMap.clear(); // clears out map of preferred magnitude types 10/21/2004 -aww
      phaseList.clear(false);  
      ampList.clear(false);  
      codaList.clear(false);  
      waveformList.clear(false);
      //System.out.println("DEBUG: Solution.clearDataLists() cleared: mag,phase,amp,coda,waveform");
    }

/** Sort/resort all the JasiReadingLists (phase, amp, coda) by distance from the
* location of the solution. Called automatically by setLatLonZ(). */
    public void sortReadingLists(GeoidalLatLonZ latlonz) { // changed input arg type -aww 06/11/2004
       phaseList.distanceSort(latlonz);
       ampList.distanceSort(latlonz);
       codaList.distanceSort(latlonz);
    }
/** Sort/resort all the JasiReadingLists (phase, amp, coda) by distance from the
* location of the solution. Called automatically by setLatLonZ(). */
    public void sortReadingLists() {
       sortReadingLists(this); // changed input arg type -aww 06/11/2004
    }

/** Returns this Solution's PhaseList. */
    public PhaseList getPhaseList() {
      return phaseList;
    }
/** Returns this Solution's CodaList. */
    public CodaList getCodaList() {
      return codaList;
    }
/** Returns this Solution's AmpList. */
    public AmpList getAmpList() {
      return ampList;
    }
    // Use new jasi object MagList for containing all mags
    // preferred mag reference is added to the list as well
    // but the external magnitude member remains as the preferred mag.
    /* replace prior list as thus:
      public ArrayList getMagList() {
        ArrayList magList = new ArrayList();
        if (sol.magnitude != null) magList.add(mag);
        Collection altMagList = sol.getAlternateMagnitudes(this);
        if (altMagList.size() > 0) magList.add(altMagList);
        return magList;
      }
    */
/** Returns this Solution's MagList. */
    public MagList getMagList() {
      return altMagList;
    }

 /**
 *  Retrieves from the default DataSource all waveforms that are
 *  associated with this Solution, adding them to this Solution's
 *  waveform list.
 *  Setting <i>clearList=true</i> first clears existing all existing
 *  list elements.
 *  */
    abstract public int loadWaveformList(boolean clearList);

 /** 
 *  First clears this Solution's waveform list, then retrieves
 *  from the default DataSource all waveforms that are
 *  associated with this Solution and adds them to the list.
 *  */
    public int loadWaveformList() {
      return loadWaveformList(true);
    }

/**
 *  Retrieves from the default DataSource all phases that are
 *  associated with this Solution, adding them to this Solution's
 *  phase list.
 *  Note that references are used, the phases are not copied.
 *  Sets staleLocation 'true' if phases are added.
 *  */
    abstract public int loadPhaseList(boolean clearList);

    public int loadPhaseList() {
      return loadPhaseList(true);
    }

    public int getPhasesUsed() {
        if (phaseList.size() == 0) return (usedReadings.isNull()) ?  0 : usedReadings.intValue(); 
        return phaseList.getChannelUsedCount();
    }

    public int getAutoPhasesUsed() {
        if (phaseList.size() == 0) return (autoUsedReadings.isNull()) ?  0 : autoUsedReadings.intValue(); 
        return phaseList.getChannelAutoUsedCount();
    }

    public int getPhasesWithInWgt() {
        if (phaseList.size() == 0) return (totalReadings.isNull()) ?  0 : totalReadings.intValue(); // added 2011/07/21 -aww
        return phaseList.getChannelWithInWgtCount();
    }

/**
*  Retrieves from the default DataSource all codas that are
*  associated with this Solution, adding them to this Solution's
*  coda list.
*  Note that references are used, the codas are not copied.
*  Sets preferred Magnitude stale if any are added that are associated with it.
*  Codas may be associated with this Solution
*  that are not associated with its preferred Magnitude.
*/
    abstract public int loadCodaList(boolean clearList);

    public int loadCodaList() {
       return loadCodaList(true);
    }

    abstract public int loadPrefMagCodaList(boolean clearList);

/**
 *  Retrieves from the default DataSource all Coda that are
 *  associated with the preferred Magnitude of this Solution,
 *  if an Coda dependent Magnitude, adds them to both this
 *  Solution's list and the preferred magnitude's list.
 *  Note that references are used, the coda are not copied.
 *  */
    abstract public int loadMagCodaList(boolean clearList);

    public int loadMagCodaList() {
       return loadMagCodaList(true);
    }
/**
 *  Retrieves from the default DataSource all Amplitudes that are
 *  associated with this Solution, adding them to this Solution's
 *  amplitude list.
 *  Note that references are used, the Amplitudes are not copied.
 *  Amplitudes may be associated with this Solution
 *  that are not associated with its preferred Magnitude.
 *  */
    abstract public int loadAmpList(boolean clearList);

    public int loadAmpList()  {
      return loadAmpList(true);
    }

    abstract public int loadPrefMagAmpList(boolean clearList);
/**
 *  Retrieves from the default DataSource all Amplitudes that are
 *  associated with the preferred Magnitude of this Solution,
 *  if an Amplitude dependent Magnitude, adding them to this
 *  Solution's amplitude list.
 *  Note that references are used, the amps are not copied.
 *  Sets the preferred Magnitude stale if any are added.
 *  */
    abstract public int loadMagAmpList(boolean clearList);

    public int loadMagAmpList() {
      return loadMagAmpList(true);
    }

    /** 
     * Retrieves all Magnitudes from the default DataSource that are associated
     * with the preferred origin of this Solution, adding them to this Solution's
     * magnitude list.
     * This Solution's preferred Magnitude is reset to is the last primary or
     * "preferred" magnitude stored in the DataSource, which is added to the list.
     * Returns  the current size of the magnitude list.
     * */
    abstract public int loadMagList(boolean clearList) ;

    public int loadMagList() {
      return loadMagList(true);
    }
    /*
     * Retrieves all Magnitudes from the default DataSource that are associated
     * with the preferred origin of this Solution, adding them to this Solution's
     * magnitude list. Does not clear list before adding the new elements.
     * Unlike loadMagList(), does NOT reset the preferred Magnitude reference.
     * Returns the count of the alternate magnitudes.
     * @see #loadMagList()
     */
    abstract public int loadAlternateMagnitudes();

    /*
     * Retrieves only the Magnitudes from the default DataSource that are associated
     * with the id of this Solution, adding them to this Solution's
     * magnitude list and preferred magnitude by type map.
     * Does not clear list before adding the new elements.
     * Unlike loadMagList(), does NOT reset the preferred Magnitude reference.
     * Returns the count of the preferred magnitudes.
     * @see #loadMagList()
     */
    abstract public int loadPrefMags();

    /** Returns true if there are alternate magnitudes for this solution. In some
     * databases an "event" can have multiple magnitudes.
     * For example, Ml, Mb, Ms, Me, etc. or magnitudes from different
     * institutions might be stored and associated with this solution.
     */
    public boolean hasAlternateMagnitudes() {
        if (altMagList.size() == 0) return false;
        else if (altMagList.size() > 1) return true; // == 1 ? maybe preferred
        else return ! (altMagList.get(0).equals(this.magnitude)); // or "=="
    }

    /** Return number of alternate magnitudes */
    public int alternateMagnitudeCount() {
        //return altMagList.size(); // no list also may contained the preferred so:
        return altMagList.getAlternateMagList(this).size();
    }

    /** Returns a copy of this Solution's magnitude list minus the preferred
     *  magnitude element.
     * */
    public List getAlternateMagnitudes() {
        return altMagList.getAlternateMagList(this);
    }
    public List getMagnitudeByType(String magtype) {
        String type =
            (magtype.length() > 1 && magtype.substring(0,1).equalsIgnoreCase("M")) ?
                magtype.substring(1) : magtype; // skip over leading "M"
        return altMagList.getListByType(type.toLowerCase());
    }
    /** Returns an array of the alternate Magnitudes.
     * The primary or "prefered" magnitude is NOT included in this array.
     * */
    public Magnitude[] getAlternateMagnitudeArray() {
        return altMagList.getAlternateMagList(this).getArray();
    }

    public String getPrefMagTypeByPriority() {
        Magnitude mag = getPrefMagByPriority();
        return (mag == null) ? "" : mag.getTypeSubString();
    }

    public Magnitude getPrefMagByPriority() {
      Collection prefMags = getPrefMags();
      if (prefMags.size() == 0) return null;

      int maxpriority = 0;
      int priority = 0;

      Magnitude mag = null;
      Magnitude priorityMag = null;
      Iterator iter = prefMags.iterator();

      while ( iter.hasNext() ) {
          mag = (Magnitude) iter.next();
          if (mag.isDeleted()) continue; // skip deleted magnitudes - aww 2009/05/14
          priority = mag.getPriority();
          //System.out.println("Prefmag priority: " + priority + " type: " + mag.getTypeSubString());
          if (priority > maxpriority) {
              maxpriority = priority;
              priorityMag = mag; 
          }
      }
      return priorityMag;
    }

    public boolean setPrefMagOfType(Magnitude mag) {
        if (mag == null || mag.hasNullType() || mag.hasNullValue()) return false;  // added value test - aww 10/24/2005
        // magid could be null still if new magseq id not assigned 
        if (! altMagList.contains(mag)) addOrReplaceMagnitudeByType(mag); // altMagList.addOrReplaceById(mag);
        // if map instance changes to new preference we need to flag commit ?
        if (prefMagMap.put(mag.getTypeQualifier(), mag) != mag) setNeedsCommit(true);
        return true;
    }
    public boolean removePrefMagOfType(String subType) {
        if (subType == null) return false;
        boolean status = (prefMagMap.remove(subType) != null);
        // Should we also look for mag instance in altMagList and delete it?
        // Note if magid already in db table it's not deleted there since
        // no easy way to effect db table delete by invoking sol.commit
        //if (status) setNeedsCommit(true);
        return status;
    }
    public boolean removePrefMagOfType(Magnitude mag) {
        return removePrefMagOfType(mag.getTypeSubString());
    }
    public Collection getPrefMags() {
        return prefMagMap.values();
    }
    public boolean hasPrefMagOfType(String subType) {
        return prefMagMap.containsKey(subType.substring( (subType.startsWith("M") || subType.startsWith("m")) ? 1 : 0 ));
    }
    public boolean isPrefMagOfType(Magnitude mag) {
        return (mag == null) ? false : (mag == getPrefMagOfType(mag));
    }
    public Magnitude getPrefMagOfType(Magnitude mag) {
        if (mag == null) return null;
        return getPrefMagOfType(mag.getTypeSubString());
    }
    public Magnitude getPrefMagOfType(String subType) {
        if (subType == null) return null;
        return (Magnitude) prefMagMap.get(subType.substring((subType.startsWith("M") || subType.startsWith("m")) ? 1 : 0 ));
    }
    public String prefMagMapString() {
        return prefMagMap.toString();
    }
    // end of extra preferred by type

    /** Returns the the primary/preferred Magnitude instance of this Solution.
     * Not allowed to be null.
     * */
    public Magnitude getPreferredMagnitude() {
        //return altMagList.getPreferredMagnitude(); // enable
        return magnitude;
    }
    public boolean isPreferred(Magnitude mag) {
        // doesn't check (mag.sol == this) assumes assignment assoc correct
        // need to make sol.magnitude private or check assignment and throw exception
        return (mag == null) ? false : (mag == magnitude);
    }
    /** Set the preferred Magnitude assignment of this Solution.
     *  Not allowed to be null.
     * */
    public void setPreferredMagnitude(Magnitude newMag) {
        //System.out.println("DEBUG Solution.setPreferredMagnitude current preferred at START = "
        // + ((magnitude == null) ? "NULL" : magnitude.toNeatString()));
        if (newMag == magnitude) { // no change in preferred instance, but its subscript 
          setPrefMagOfType(newMag);   // type may have changed, it's the key for the map
          return;                  // of eventprefmag in the Solution class - aww 02/10/2005
        }
        if (newMag != null) newMag.assign(this); // affiliation needed to enable adding to list
        else { // do we want the ability to null the prefmag?
          /* to KILL PREFMAG:
          removeDataStateListeners(magnitude); // better if automatic via MagList listener on remove
          altMagList.remove(magnitude);
          magnitude = null; // magnitude = NULL_MAGNITUDE; // use "special" null case Magnitude object?
          */
          return;
        }
        //Make assignments before list adds else newMag flag isDeleted() by List.set()
        Magnitude oldMag = magnitude;
        magnitude = newMag;
        if (magnitude != null) {
          // Do we want to instead use below addOrReplaceMagnitudeByType -aww?
          // addOrReplace(oldMag); // adds to list if not already there
          if (! altMagList.contains(oldMag)) addOrReplaceMagnitudeByType(oldMag);
          else removeDataStateListeners(oldMag); // also done by addOrReplace... methods 
        }
        if (newMag != null) {
          // Do we want to instead use below addOrReplaceMagnitudeByType -aww?
          //addOrReplace(newMag); // replace if ==, or identifiers, sol, type, and algorithm match, else add
          addOrReplaceMagnitudeByType(newMag);
          // only prefmag listens, not the alternates, see listener notes
          // method addOrReplace ... above for same magtype adds listener else
          // below is needed for a different magtype - aww 10/21/2004
          if (! newMag.isSameType(oldMag) ) addDataStateListeners(newMag);
          if (newMag != getPrefMagOfType(newMag)) setPrefMagOfType(newMag); // forced insurance

          //System.out.println("DEBUG Solution new preferred at END  = " + magnitude.toDumpString());
          //System.out.println("DEBUG Solution altMagList at END = " + altMagList.toNeatString());
          //System.out.println("DEBUG Solution prefMagMap at END = " + prefMagMapString());
          //System.out.println("DEBUG Solution DONE setPreferredMagnitude()");
        }
    }
    /**
     * Sets the event preferred Magnitude to the Magnitude in this Solution's
     * magnitude list with an identifier matching the input argument.
     * Does a no-op if no match is found.
     */
    public void setPreferredMagFromIdInList(Object magid) {
        Iterator iter = altMagList.iterator();
        Magnitude prefmag = null;
        while (iter.hasNext()) {
          Magnitude mag = (Magnitude) iter.next();
          if (mag.idEquals(magid)) {
            prefmag = mag;
            break;
          }
        }
        if (prefmag != null) {
          setPreferredMagnitude(prefmag);
        }
    }

    /**
     * Given a Collection of Waveforms, add any that are associated with this
     *  Solution to its waveformList.  Note that references are used, the waveforms are
     *  not copied.  Returns a count of the number of waveforms that were added.
     * Note that not all schemas make a connection between solutions and waveforms.
     * This method is provided to accommodate those that do.
     * */
    public int addWaveforms(Collection newWfList) {
        if (newWfList == null) return 0;
        Waveform wf[] = new Waveform[newWfList.size()];
        newWfList.toArray(wf);
        int knt = 0;
        for (int i = 0; i<wf.length; i++) {
          if ( addWaveform(wf[i]) ) knt++;
        }
        return knt;
    }

    /**
     * Add one Waveform to the waveformList.  Note that references are used, the
     *  waveforms are not copied.  Returns true it the waveform is added.  * Note
     *  that not all schemas make a connection between solutions and waveforms.  *
     *  This method is provided to accommodate those that do.
     *  */
    public boolean addWaveform(Waveform wf) {
        return waveformList.add(wf);
    }

    /**  Set the value of 'waveRecords' for this Solution if possible. This is
     * done in a separate method because some data sources may not supply this
     * information without doing an expensive search of a database.
     * */
    public int countWaveforms() {
        return (waveRecords.isNull()) ? 0 : waveRecords.intValue();
    }

    public ChannelableList getWaveformList() {
       return waveformList;
    }

    /** Returns true if there are alternate solutions for this solution. In some
     * databases an "event" can have multiple solutions. For example, different
     * programs, or crustal models might be used or solutions from different
     * institutions might be stored and associated with this solution.
     * */
    public boolean hasAlternateSolutions() {
      if (alternateSolutionCount() > 0) return true;
      return false;
    }

    /** Return number of alternate solutions. */
    public int alternateSolutionCount() {
      return altSolList.size();
    }

    /** Returns List of Solution objects each containing an alternate solution.
     * The primary or "prefered" solution is NOT included in this set */
    public JasiCommitableListIF getAlternateSolutions() {
      return altSolList;
    }

    /** Returns array of Solution objects each containing an alternate solution.
     * The primary or "prefered" solution is NOT included in this set */
    public Solution[] getAlternateSolutionArray() {
      return altSolList.getArray();
    }

    /** Add a solution to the list of alternate solutions. */
    public void addAlternateSolution(Solution sol) {
      altSolList.add(sol);
    }

    /*
     * Retrieves all Solutions from the default DataSource that are associated
     * with the event id of this Solution but having different origins, 
     * Does not clear list before adding the new elements.
     * Returns the count of the alternate solutions.
     */
    abstract public int loadAltSolList(boolean clearList);

    public int loadAltSolList() {
      return loadAltSolList(true);
    }

    /** Set the primary or "preferred" solution to the solution passed in the
     * arg.  If there is a current preferred solution it is put into the
     * altSolList.
     * */
    public void setPreferredSolution(Solution sol) {
    // TODO: how do you do this??
    // Could expand the class heirarchy, have in this super class called
    // "Event" the altSolList and a prefSol data members, analogous to the
    // altMagList and magnitude (prefMag) data members of Solution class?
    }

//JasiSolutionAssociationIF implements:
//  addTo(Sol) removeFrom(Sol) addOrReplaceIn(Sol) containedBy(Sol)
//abstract IF methods in SolutionAssocJasiObject class subtype
//using list listeners would remove NEED for addXXX removeXXX methods
//to set state of Solution. This would require all internal readinglist
//objects to be assigned only via methods that add/remove list listeners.
//
// Instead of below method just implement overridden:
// add(xxxSubtype) remove(xxxSubtype) or delete(xxxSubType) methods
// but these methods may not even be needed if list listeners are used.
// sol should listen to phaseList, but magnitude should listen to codaList
// ampList and based on its subtype decide stale state.

  /** Associate input with this Solution, adding it to the
   *  appropiate data collection, if any.
   *  Does not unassociate input's prior association before
   *  associating. 
   *  Returns false if input is null, else returns true.
   */
  public boolean associate(JasiCommitableIF jc) {
    return associate(jc, false); // ? should default behavior be true or false?
  }
  /** Like associate() but optionally unassociates input's prior
   *  Solution association before associating it with this Solution.
   *  Does not change the virtual delete state of the input.
   *  Returns false if input is null, or already associated,
   *  else returns true.
   *  NOTE: does it add(jc) OR addOrReplace(jc), equivalent or equals???
   *  Programmer must resolve this.
   */
  public boolean associate(JasiCommitableIF jc, boolean unassociatePriorAssoc) {
    // what is desired behavior for null input? return false?, throw an exception?
    if (jc == null) return false;
    SolutionAssocJasiObject obj = (SolutionAssocJasiObject) jc;
    if (! obj.isAssignedTo(this) && unassociatePriorAssoc) obj.unassociate();
    //obj.setDeleteFlag(false); // ? do we want to toggle state undeleted by default ? 
    obj.sol = this; // else add doesn't work
    // could create an input arg mask to decide which data filter method to invoke:
    // DDG added code to JasiReading associate to force auto distance calc
    // DO A TEST OF LIKE LOGIC HERE: 09/09/2004
    if (obj instanceof Channelable && hasLatLonZ()) {
      ((Channelable) obj).calcDistance(getLatLonZ());
    }
    //

    return add(obj); // ? do add(obj) or addOrReplace(obj) here ?
  }
  /**
  * Removes input reference from Solution and nulls matching Solution assignment.
  * Does not unassociate the preferred Magnitude, a special case.
  * Checks that input object is actually associated with this instance and 
  * not another Solution before nulling the association reference.
  */
  public boolean unassociate(JasiCommitableIF jc) {
    if (jc == null) return false;
    JasiSolutionAssociationIF obj = (JasiSolutionAssociationIF) jc;
    remove(obj);
    boolean retVal = obj.isAssignedTo(this);
    if (retVal) obj.assign((Solution)null);
    return retVal;
  }
  /** Adds the input reference to this Solution
   *  ONLY if it is assigned to it and not already contained by it.
   *  Returns true if added, else false.
   */
  public boolean add(JasiCommitableIF jc) {
    if (jc == null) return false;
    JasiSolutionAssociationIF obj = (JasiSolutionAssociationIF) jc;
    if (! obj.isAssignedTo(this)) return false;
    JasiCommitableListIF aList = getListFor(obj);
    return (aList != null) ? aList.add(obj) : false;
  }
  /** Adds, or replaces the equivalent to, the input reference
   *  in this Solution ONLY if it is assigned to it.
   *  Returns true if successful, else false.
   */
  public boolean addOrReplace(JasiCommitableIF jc) {
    if (jc == null) return false;
    JasiSolutionAssociationIF obj = (JasiSolutionAssociationIF) jc;
    if (! obj.isAssignedTo(this)) return false;
    JasiCommitableListIF aList = getListFor(obj);
    return (aList != null) ? (aList.addOrReplace(obj) != null) : false;
  }
  /** Returns true if the input reference is removed from this Solution,
   * else returns false.
   */
  public boolean remove(JasiCommitableIF jc) {
    JasiCommitableListIF aList = getListFor(jc);
    return (aList != null) ? aList.remove(jc) : false;
  }
  /*
    * If Solution's containing list had no object state listener,
    * commitable's subclass override of delete method implementation 
    * could toggle associated summary object stale, if appropiate.
    * That way if it's deleted out of context, summary object
    * state is still correctly flagged stale. e.g. see Phase, Amplitude
  */ 
  /** Virtually deletes the input object
   *  ONLY if it is assigned to this Solution.
   *  Returns true if successful, else returns false.
   *  Notifies list listener of change.
  */
  public boolean delete(JasiCommitableIF jc) {
    if (jc == null) return false;
    JasiSolutionAssociationIF obj = (JasiSolutionAssociationIF) jc;
    // delete only if assigned but don't check if in list: true == contains(obj);
    if (! obj.isAssignedTo(this)) return false;
    // if in list, and state changes, list will fire event to listeners
    JasiCommitableListIF aList = getListFor(obj);
    boolean retVal = (aList == null) ? false : aList.delete(obj);
    if (! retVal) obj.setDeleteFlag(true); // not in list, just assigned, delete it anyway
    return true;
  }
  /** Undo a virtual delete of the input object
   *  ONLY if it is assigned to this Solution.
   *  Returns true if successful, else returns false.
  */
  public boolean undelete(JasiCommitableIF jc) {
    if (jc == null) return false;
    JasiSolutionAssociationIF obj = (JasiSolutionAssociationIF) jc;
    // undelete only if assigned, no check if in list: true == contains(obj);
    if (! obj.isAssignedTo(this)) return false;
    JasiCommitableListIF aList = getListFor(obj);
    // if in list, and state changes, list will fire event to listeners
    boolean retVal = (aList == null) ? false : aList.undelete(obj);
    if (! retVal) obj.setDeleteFlag(false); // not in list, just assigned, delete it anyway
    return true;
  }
  /** Convenience wrapper for unassociate(jc) and jc.delete().
   * Returns true if input is removed from or is assigned to
   * this instance and deleted, else false.
   * Does not erase Magnitude instances.
   * @see #erase(Magnitude)
   */
  public boolean erase(JasiCommitableIF jc) {
    // If assigned to this, delete it, unassociate always removes it from list.
    // Do delete(jc) first for assoc check, in case unassociate nulls assoc 
    //
    //slower way two trips thru list:
    delete(jc); // set flags deleted, list does delete state notify 
    return unassociate(jc);
    //
    //alternative one list trip, but jc.delete doesn't notify yet:
    //return (unassociate(jc)) ?  jc.setDeleteFlag(true) : false;
    //
    //With delegation implementation of MagnitudeAssocJasiReading
    //the below alternative does Magnitude list removal too,
    //but jc.delete doesn't notify listeners yet:
    //return jc.erase(); // doesn't notify?
  }
  /** Returns true if input reference is found in this Solution's data.
   */
  public boolean contains(JasiCommitableIF jc) {
    //no checks as of yet for non-list items
    JasiCommitableListIF aList = getListFor(jc);
    // Do indexOfEquivalent(jc) instead of instance identity below ?
    return (aList == null) ? false : aList.contains(jc);
  }
  /** Returns the member data collection, if any in this Solution,
   * to which input would belong to, else returns null.
   */
  public JasiCommitableListIF getListFor(JasiCommitableIF jc) {
    // hash container could use subclass as lookup key to retrieve list
    // xxxList aList = (xxxList) hashSet.get(subclass) instead of this:
    if (jc instanceof Amplitude) return getAmpList();
    else if (jc instanceof Coda) return getCodaList();
    else if (jc instanceof Phase) return getPhaseList();
    else if (jc instanceof Magnitude) return getMagList();
    else if (jc instanceof Solution) return getAlternateSolutions();
    else return null; 
  }

  /** Returns List of same data type as input,
   * if any such type is a data member of Solution, else
   * returns null.
   */
  public JasiCommitableListIF getListFor(JasiCommitableListIF aList) {
    if (aList instanceof AmpList) return getAmpList();
    else if (aList instanceof CodaList) return getCodaList();
    else if (aList instanceof PhaseList) return getPhaseList();
    else if (aList instanceof MagList) return getMagList();
    else if (aList instanceof GenericSolutionList) return getAlternateSolutions();
    else return null;
  }

  /** Returns List of pertaining to the class type as input,
   * if any such type is a data member of Solution, else
   * returns null.
   */
  public JasiCommitableListIF getListFor(Class aClass) {
    if ( aClass == Amplitude.class || aClass == AmpList.class ) return getAmpList();
    else if ( aClass == Coda.class      || aClass == CodaList.class ) return getCodaList();
    else if ( aClass == Phase.class     || aClass == PhaseList.class ) return getPhaseList();
    else if ( aClass == Magnitude.class || aClass == MagList.class ) return getMagList();
    else if ( aClass == Solution.class  || aClass == SolutionList.class)  return getAlternateSolutions();
    else return null;
  }

  public int loadListFor(Class aClass) {
         if ( aClass == Amplitude.class || aClass == AmpList.class ) return loadAmpList();
    else if ( aClass == Coda.class      || aClass == CodaList.class ) return loadCodaList();
    else if ( aClass == Phase.class     || aClass == PhaseList.class ) return loadPhaseList();
    else if ( aClass == Magnitude.class || aClass == MagList.class ) return loadMagList();
    else if ( aClass == Solution.class  || aClass == SolutionList.class)  return loadAltSolList();
    else return 0;
  }

  /* not very efficient
  public int addAssociatedData(List aList) {
    int size = aList.size();
    if (size == 0) return 0;
    JasiCommitableIF jc = (JasiCommitableIF) aList.get(0);
    JasiCommitableListIF aList = getListFor(jc);
    if (aList == null) return 0;
    int count = 0;
    for (int idx =0; idx <size; idx++) {
      jc = (JasiCommitableIF) aList.get(idx);
      if (jc.isAssignedTo(this) && aList.add(jc)) count++;
    }
    return count;
  }
  */
  // more efficient
  /** Adds to this Solution's corresponding list type all elements of
   *  the input list assigned to this Solution that are not flagged
   *  as virtually deleted and not already present in the list.
   *  Returns count of newly added elements to Solution's list.
   */
  public int addAssociatedData(JasiCommitableListIF aList) {
    JasiCommitableListIF myList = getListFor(aList);
    if (myList == null) return 0; 
    SolutionAssociatedListIF addList =
      ((SolutionAssociatedListIF)aList).getAssociatedWith(this);
    int oldSize = myList.size();
    myList.addAll(addList); // addAll tests by equals() not equivalent()
    //How do we matchChannelsWithList/DataSource after adding to lists?
    //perhaps implement a convenience method to do it for the known
    //ChannelableList (time dependent?) members of Solution, it would use
    //as an input arg a reference ChannelList, e.g. MasterChannelList ? - aww
    return (myList.size() - oldSize);
  }

  public void matchChannelsWithList() {
      matchChannelsWithList(MasterChannelList.get());
  }

  public void matchChannelsWithList(ChannelList chanList) {

      if (chanList == null) return;

      if (phaseList != null) phaseList.matchChannelsWithList(chanList);
      if (ampList != null) ampList.matchChannelsWithList(chanList);
      if (codaList != null) codaList.matchChannelsWithList(chanList);

      if (waveformList != null) waveformList.matchChannelsWithList(chanList);

  }

  /** Assigns this Solution to all JasiSolutionAssociationIF elements of the input list.
   * Does not add input elements to the corresponding list of this Solution.
   */
  public void assignToUnassociated(List aList) {
    if (aList == null || aList.isEmpty()) return;
    int count = aList.size();
    for (int idx =0; idx < count; idx++) {
      try {
        ((JasiSolutionAssociationIF) aList.get(idx)).assign(this);
      }
      catch (IllegalArgumentException ex) {
        System.err.println(ex.getMessage());
      }
    }
  }
  /*
   * Associates each JasiSolutionAssociationIF element
   * of the input list with this Solution.
   * Invokes associate() for each list element, thus
   * the result depends whether the associate() implementation
   * of does an update by add() or addOrReplace().
   * @see #associate(JasiCommitableIF)
   */
  public void associateAll(List aList) {
    if (aList == null || aList.isEmpty()) return;
    int count = aList.size();
    for (int idx =0; idx < count; idx++) {
      ((JasiSolutionAssociationIF) aList.get(idx)).associate(this);
    }
  }

  /** Programmer convenience for loading unique data 
  * such as when corresponding data list of this Solution
  * is empty or known not to have any data elements of input,
  * i.e. filtering of the input list would just waste time.
  * Does REQUIRE the Solution association reference of input 
  * elements to be either this Solution or null (unassociated).
  * Could change implementation to unassociate any prior reference?
  */
  public void fastAssociateAll(List aList) {
    if (aList == null || aList.isEmpty()) return;
    int count = aList.size();
    //assignToUnassociated(aList);
    JasiSolutionAssociationIF jc = null;
    for (int idx =0; idx < count; idx++) {
      jc = ((JasiSolutionAssociationIF) aList.get(idx));
      jc.assign(this); // exception, if prior assignment not this solution or null
    }
    ActiveArrayList myList = (ActiveArrayList) getListFor(jc); 
    myList.fastAddAll(aList); // no filtering out of nulls or duplicates
  }

  public void associate(List aList) {
      if (aList == null || aList.isEmpty()) return; // error? needed isEmpty 4 bug fix 06/06/05  -aww
      JasiSolutionAssociationIF jc = (JasiSolutionAssociationIF) aList.get(0);

      JasiCommitableListIF jcl = getListFor(jc);
      if (jcl == null) {
        System.err.println(getClass().getName()+".associate(List) no internal list for type: " + jc.getClass().getName());
        return; // could treat as error ? 
      }

      if (jcl.isEmpty()) {
        fastAssociateAll(aList); // does not filter input elements
      }
      else {
        associateAll(aList); // filters input for dupes or nulls
      }
  }

  /**
   * Replaces in this Solution's corresponding data list those input
   * list elements associated with this Solution that are equivalent,
   * otherwise adds those associated to the list.
   * @see #getReadingList()
   * @see #addAssociatedData(JasiCommitableListIF)
   */
  public void addOrReplaceAll(JasiCommitableListIF aList) {
    SolutionAssociatedListIF myList =
       (SolutionAssociatedListIF) getListFor(aList);
    if (myList == null) return;
    myList.addOrReplaceAll(
      ((SolutionAssociatedListIF)aList).getAssociatedWith(this)
    );
  }

 //  LISTENERS REMAIN ON ALL MAGS IN LIST SO DELETING 
 //  SOLUTION READINGS DELETES FROM MAGNITUDES IN LIST
 //  AND VICE VERSA ADDS TO SOLUTION LISTS FOR ANY MAG
 //  LIST ADDITIONS, ANY CHANGE IN MAG LIST RESULTS IN
 //  IN STALE MAG STATE 
    /** Adds input Magnitude instance reference to MagList if list contains(newMag) == false.
     * No check for "equivalent data" in different Magnitude object instances.
     * Returns false if input is null, not affiliated with this Solution instance, 
     * is already on the list
     * @see #addOrReplace(Magnitude)
     * */
    public boolean add(Magnitude newMag) {
        if (newMag == null || newMag.sol != this) return false;
        if (newMag.hasNullType() || newMag.hasNullValue()) return false; // test here aww 10/24/2005
        // note does no check for preferred mag id duplication
        // should we report another mag with same identifier as the preferred?
        boolean retVal = altMagList.add(newMag);
        if (retVal) { // added the new one
          //don't attach listeners if done automatically by MagList listener:
          //addDataStateListeners(newMag);
          newMag.associateLists(this); // make sure all the mag readings are in solutions lists
        }
        return retVal;
    }

    /** Adds input Magnitude instance reference to Maglist if list indexOfEquivalent(newMag) == -1.
     *  otherwise replaces equivalent Magnitude in list where indexOfEquivalent(newMag) >= 0.
     *  If equivalent reference is preferred input newMag becomes the preferred magnitude.
     *  Updates ListDataStateListener listener lists.
     *  */
    public boolean addOrReplace(Magnitude newMag) {
        if (newMag == null || newMag.sol != this) return false;
        if (newMag.hasNullType() || newMag.hasNullValue()) return false; // test here aww 10/24/2005
        // amp,coda,phase lists' elements aren't coupled to listeners like those of altMagList 
        // override addOrReplace in MagList to use remove,add combo instead of set() to utilize
        // ListDataStateListener methods to remove, add Listener when replacing equivalent mag
        // at some index in maglist
        Magnitude aMag = (Magnitude) altMagList.addOrReplace(newMag); // add or replace equivalent with the new one

        // LOGIC BELOW TO HANDLE ANY PREFMAG ISSUES
        // if input replaced prefMag of any type, update the map 
        // if (aMag == getPrefMagOfType(newMag) && aMag != newMag) { // "no prefmag", null case fails here aww 04/28/2005
        if ( getPrefMagOfType(newMag) != newMag) {  // instead set if not identical - aww 04/28/2005
            setPrefMagOfType(newMag); // aww 10/21/2004
        }
        // if object replaced event preferred magnitude, update preferred reference
        if ( aMag == this.magnitude ||
             (! altMagList.contains(this.magnitude) && aMag.isSameType(this.magnitude) ) // no allowance for algo difference
           ) { // preferred may have been removed from list by a addOrReplaceByType - aww 06/15/2006
          magnitude = newMag; // set the event preferred to the newmag ?
          // Even if aMag==newMag make sure any listeners removed in
          // setPreferredMag(Mag) or elsewhere are "added"
          // add.. below first does a remove to avoid "dupes"
          addDataStateListeners(magnitude); // only if a altMaglist listener isn't enabled to do it.
        }
        // if replaced old with new, remove listener if altMagList listener doesn't do it. 
        if (aMag != newMag) removeDataStateListeners(aMag); // remove old data listeners

        // since above doesn't guarantee updated sol reading lists
        newMag.associateLists(this); // make sure all mag readings are in this sol lists
        // NOTE: "readings" of like type from alternate Magnitudes 
        // would be replaced equivalent if the same instance aliased, 
        // "AssocXxM" info would be that of the last updated Magnitude.
        // If they are distinct instances with different associated
        // magnitudes then the method should add, not replace, them
        // in the Solution's reading list. But we need to confirm this
        // behavior through testing. -aww

        return true;
    }

     /**
     *  Wrapper around addOrReplace(Magnitude).
     *  First removes all existing Magnitude references in MagList where Magnitude.getTypeQualifier()
     *  matches that of the input <i>newMag </i>, then it then invokes addOrReplace(newMag).
     *  */
    public boolean addOrReplaceMagnitudeByType(Magnitude newMag) {
        if (newMag == null || newMag.sol != this) return false;
        if (newMag.hasNullType() || newMag.hasNullValue()) return false; // test here aww 10/24/2005
        MagList newList = (MagList) altMagList.getListByType(newMag.getTypeQualifier());

        /*
        System.out.println(" DEBUG SOLUTION addOrReplaceMagnitudeByType newMag.isSameType(magnitude):"
                           +newMag.isSameType(magnitude)+
                           " likeMagType count:"+newList.size()+
                           " magType: "+newMag.getTypeQualifier()
                          );
        */

        //Removed next line below, so preferred magnitude of same type can be purged
        //code further down below resets prefmag when input newMag is of same type.
        //newList.remove(magnitude); // removed 07/15/2004 -aww

        //Remove newMag input from purge list, this removal is needed to a control side-effect
        //of list update behavior when newMag is already in the list and it's the preferred
        //(e.g. when solution's maglist changes are listened to by a MagListTableModel). 
        //The addOrReplace below handles the newMag update to the list.
        newList.remove(newMag); // added 07/15/2004 -aww

        /* Below loop not needed if ListDataStateListener is on Solutions's own MagList 
        int count = newList.size();
        for (int idx=0; idx<count; idx++) {
          Magnitude oldMag = (Magnitude) newList.get(idx);
          // better if MagList Listener did remove DataListListener automatically
          if (oldMag != this.magnitude) removeDataStateListeners(oldMag);
        }
        */
        //Remove remaining elements of same mag subtype from the mag List
        //Note: any mag List listeners are updated by the removeAll.
        altMagList.removeAll(newList); // the "preferred" magnitude is removed if same magtype

        return addOrReplace(newMag); // updates preferred if replaced

    }

    /** Virtually deletes input Magnitude in Solution list only
     * if it is not the preferred magnitude, if sucessful
     * flags input Magnitude as deleted, else does a no-op.
     * Deleting magnitude implies it is legitimately associated
     * with this Solution but it is not to be commited, 
     * probably having been superceded by another Magnitude
     * of the same type. Removes the Magnitude from the Solution's
     * list data listeners.
     * @see #remove(Magnitude)
     * */
    public boolean delete(Magnitude mag) {
      if (mag == null || mag.isPreferred()) return false;
      removePrefMagOfType(mag); // test remove from prefMagMap too ?? -aww 10/21/2004
      return altMagList.delete(mag);
    }
    /** Removes equivalent of input Magnitude from magnitude list only
     * if it is not the preferred magnitude.
     * Does not null reference to this Solution 
     * in the input nor does it virtually delete it.
     * Removes input Magnitude from Solution's list data listeners,
     * thus changes to Solution lists no longer are reflected in
     * the state of the removed Magnitude.
     * */
    public boolean remove(Magnitude mag) {
        if (mag == null || mag.isPreferred()) return false;
        // equivalent only as equals() object but could implement an
        // below stops monitoring of list changes by removed mag:
        //removeDataStateListeners(mag); // MagList Listener does this automatically.
        // use of remove(...) in MagList to test for contents identity equals.
        removePrefMagOfType(mag); // remove it from prefMagMap -aww 10/21/2004
        return altMagList.removeEquivalent(mag);
    }
    /** Combines unassociate and delete of input Magnitude and 
     *  all of its readings from corresponding Solution's lists
     *  Returns false if lists change failed or the input
     *  input is preferred magnitude or null, else returns true.
     */
    public boolean erase(Magnitude mag) {
        if (mag == null || mag.isPreferred()) return false;
        this.unassociate(mag); // remove mag reference
        JasiCommitableListIF magList = mag.getReadingList();
        JasiCommitableListIF solList = this.getListFor(magList);
        // just in case input mag has no known data list type
        if (solList == null) return false;
        return (solList.eraseAll(magList) >= 0) ; // remove & delete its readings
    }

    /**
    * Global setting effects enabling/disabling synchronized data
    * list add/replace/delete with the lists of the preferred Magnitude 
    * for this Solution and all subsequently created Solutions.
    * Removing elements from the Magnitude doesn't effect the Solution
    * lists.
    */
    public static void listenToMagReadingListAdd(boolean tf) {
      listenToMagReadingListAdd = tf;
    }

    public static void listenToMagReadingListRemove(boolean tf) {
      listenToMagReadingListRemove = tf;
    }

    public static void listenToMagReadingListState(boolean tf) {
      listenToMagReadingListState = tf;
    }

    /** Default setting is true, thus any change in the total number
     * of phases in the phase list sets the Solution state "stale",
     * implying a relocation is needed. User can toggle setting
     * <i>false</i> to disable this action for case where phase data 
     * need to be loaded but the the Solution origin has not changed.
     */
    public void listenToPhaseListChange(boolean tf) {
      listenToPhaseListChange = tf;
    }

  /** Removes ListDataState listeners from this Solution's data lists.
   *  Listeners monitored changes in the Solution's lists to enable
   *  updates of any correponding data lists of the input Magnitude.
   * */
  protected void removeDataStateListeners(Magnitude mag) {
    if (mag == null) return;
    // remove input Magnitude as listener to Solution's lists.
    mag.removeSolDataStateListeners(this);
    // remove this Solution as listener to input Magnitude's lists.
    removeMagDataStateListeners(mag);
  }
  /** Adds ListDataState listeners to this Solution's data lists.
   *  Listeners monitors changes in the Solution's lists and may 
   *  update correponding data lists of the input Magnitude.
   * */
  protected void addDataStateListeners(Magnitude mag) {
    removeDataStateListeners(mag); // don't duplicate listeners
    if (mag == null) return;
    // add input Magnitude as listener to Solution's lists (removal => gone for good?).
    // thus removal in Solution lists purges like readings from Magnitude's matching list.
    mag.addSolDataStateListeners(this);    
    // add this Solution as listener to input Magnitude's lists.
    // thus adding to Magnitude list adds like readings to Solution's matching list.
    //if(mag.getReadingList() instanceof CodaList)mag.getReadingList().addListDataStateListener(codaDataListener); 
    addMagDataStateListeners(mag);
  }
  /** Adds ListDataState listeners to the input Magnitude's data lists.
   *  Listeners monitors changes in the Magnitude's list any may update
   *  any correponding data lists of this Solution.
   * */
  protected void addMagDataStateListeners(Magnitude mag) {
    if (mag == null) return;
    ActiveArrayList aList = mag.getCodaList();
    if (aList != null) aList.addListDataStateListener(magCodaDataStateListener); // this
    aList = mag.getAmpList();
    if (aList != null) aList.addListDataStateListener(magAmpDataStateListener); // this
  }
  /** Removes ListDataState listeners from  the input Magnitude's data lists.
   *  Listeners monitored changes in the Magnitude's list to enable
   *  updates of any correponding data lists of this Solution.
   * */
  protected void removeMagDataStateListeners(Magnitude mag) {
  // since mag subtype change would have to work with different list, have use all
    if (mag == null) return;
    ActiveArrayList aList = mag.getCodaList();
    if (aList != null) aList.removeListDataStateListener(magCodaDataStateListener);  
    aList = mag.getAmpList();
    if (aList != null) aList.removeListDataStateListener(magAmpDataStateListener);  
  }

    // IF THERE ARE MULTIPLE MAGLIST CHANGES ASSUME THIS SOL
    // AND ITS MAGS IN MAGLIST ARE THE ONLY LIST LISTENERS
    // CLEAR THESE LIST LISTENERS, THEN RE-ADD TO SOL AND
    // THE CURRENT MAGLIST 
    private void resetMagDataListListeners() {
      ampList.clearListDataStateListeners(); // both sol and mag
      codaList.clearListDataStateListeners(); // both sol and mag
      int count = altMagList.size();
      for (int idx = 0; idx < count; idx++) {
        addDataStateListeners((Magnitude) altMagList.get(idx));
      }
      // make sure the solution's preferred mag is on the list
      if (this.magnitude != null) addDataStateListeners(this.magnitude);
    }

  //
  // ListDataStateListenerIF
  //
  // Works only if magnitude set only through preferred method in order to add/remove listeners
  // Since Magnitude subtype is not a subclassed, we have use all lists here.
  // NOTE: An alternative to code below is to delegate to mag configuration of list listeners,
  // its implementation decides which lists to add/remove listeners for:
  //    mag.addAssocSolutionDataListListeners();
  //    mag.removeAssocSolutionDataListListeners();
  // NOTE: loading unique object instances in mag Lists will not equate with those in solution.
  // only "equivalent" replacement works to avoid redundancy!!!
  //
  /* Readings list listeners not added to alternates, because changes in the dynamic lists of Solution
   * or the prefmag will be propagated to alternates lists and since their dependent data isn't a cloned
   * snapshot these changes in the objects they reference invalidates the summary of these alternate
   * Magnitudes anyway. Their lists may have their elements "assigned" to subsequent summary results.
   */
  /** Listener added to Solution's MagList in order to add/remove magnitude listener to Solution's reading lists. */
  protected class MagListDataStateListener implements ListDataStateListener {
    public void intervalAdded(ListDataStateEvent e) {
      //System.out.println("Sol - my interval added in :"+e.getSource().getClass().getName());
      /*
      //StateChange sc = e.getStateChange();
      //if (! sc.getStateName().equals("added")) return;
      int first = e.getIndex0();
      int last = e.getIndex1(); 
      MagList magList = (MagList) e.getSource(); // better be same as getMagList()
      for (int idx=first ; idx <= last; idx++) {
        addDataStateListeners((Magnitude)magList.get(idx)); // add listeners to new entries
      }
      */
      setNeedsCommit(true);
    }
    public void intervalRemoved(ListDataStateEvent e) {
      //System.out.println("Sol - my interval removed in :"+e.getSource().getClass().getName());
      /*
      StateChange sc = e.getStateChange();
      //if (! sc.getStateName().equals("removed")) return;
      int first = e.getIndex0();
      int last = e.getIndex1(); 
      //setNeedsCommit(true); // this may not effect commit
      if (first > -1 && last == first) { // only one entry
        // since data already gone from list, need info passed via StateChange
        // remove listener from single removal
        removeDataStateListeners((Magnitude)sc.getValue());
        return;
      }
      // many elements were removed (first0 !=last0) or (index < 0)
      // shouldn't be too many, so just do them all 
      resetMagDataListListeners();
      //setNeedsCommit(true); // can't commit removed data, only if it effect other saved attributes
      */
    }
    public void contentsChanged(ListDataStateEvent e) {
      //System.out.println("Sol - my contents changed in :"+e.getSource().getClass().getName());
      /*
      //StateChange sc = e.getStateChange();
      //if (! sc.getStateName().equals("replaced")) return;
      int first = e.getIndex0();
      int last = e.getIndex1(); 
      MagList magList = (MagList) e.getSource(); // assume event src is local altMagList
      setNeedsCommit(true);
      if (first > -1 && last == first) { // only one
        // since data already gone from list, need info passed via StateChange
        // remove listener from single removal
        removeDataStateListeners((Magnitude)e.getStateChange().getValue());
        addDataStateListeners((Magnitude)magList.get(first)); // add listeners to replacement
        return;
      }
      // many elements were changed (first0 !=last0) or (index < 0)
      // shouldn't be too many, so just do them all
      resetMagDataListListeners();
      */
      setNeedsCommit(true); // replaced an element, for example preferred magnitude
    }
    public void stateChanged(ListDataStateEvent e) {
      //System.out.println("Sol - no-op state changed in :"+e.getSource().getClass().getName());
      //StateChange sc = e.getStateChange();
      //if (sc.getStateName().equals("???")) {};
      // could test for deleted state of prefmag and unset deleted state?
      //setNeedsCommit(true); // change may not be relevant to effect commit
    }
    public void orderChanged(ListDataStateEvent e) {}
  }//end of MagListDataStateListener class

  // Listener added to Solution's phaseList in order to effect state transition (stale)
  protected class PhaseDataStateListener implements ListDataStateListener {
    // Below there is no iteration through list to check added/removed phases deletion state. 
    // If a reading is "deleted" adding or removing it should not effect stale state.
    // Resolving this would require some overhead and implementing a scan of the delete state
    // of the elements returned in the event's StateChange object. 

    public void intervalAdded(ListDataStateEvent e) {
      //System.out.println("Sol - my interval added in :"+e.getSource().getClass().getName());
      if (! listenToPhaseListChange) return; 
      setStale(true);
    }
    public void intervalRemoved(ListDataStateEvent e) {
      //System.out.println("Sol - my interval removed in :"+e.getSource().getClass().getName());
      if (! listenToPhaseListChange) return; 
      setStale(true);
    }
    public void contentsChanged(ListDataStateEvent e) {
      //System.out.println("Sol - my contents changed in :"+e.getSource().getClass().getName());
      if (! listenToPhaseListChange) return; 
      setStale(true);
    }
    public void stateChanged(ListDataStateEvent e) {
      //System.out.println("Sol - my state changed in :"+e.getSource().getClass().getName());
      //should check to see if object on list is already in the changed state, if so no-op here
      if (! listenToPhaseListChange) return; 
      setStale(true);
    }
    public void orderChanged(ListDataStateEvent e) {}
  } //end of PhaseDataListener class
  
  protected class MagReadingDataStateListener implements ListDataStateListener {
    //
    // Can use a method for turning on/off add, removal, replacement synch with mag list.
    //
    // To change local solution list data we must use info passed in event StateChange(...)
    // must retain other object's readings besides those ony found in event src (mag) list.
    // thus the following won't work:
    // List srcList=e.getSource();((JasiCommitableListIF)getListFor(srcList)).retainAll(srcList)
    //
    // NOTE: loading unique object instances to mag,sol List will not equate.
    //       only equivalent replacement test would work to avoid redundancy!!!
    //
    // NOTE: Collections.sort(list) invokes the List.set() method
    //
    public void intervalAdded(ListDataStateEvent e) {
      if (! listenToMagReadingListAdd) return; 
      StateChange sc = e.getStateChange();
      if (! sc.getStateName().equals("added")) return;
      int first = e.getIndex0();
      int last = e.getIndex1(); 
      SolutionAssociatedListIF srcList = (SolutionAssociatedListIF) e.getSource();
      if (first > -1 && last > -1) {
        // match time, sol, and mag assoc for same err
        //System.out.println("Sol - my list interval:"+first+","+last+" added via mag: "+e.getSource().getClass().getName());
        getListFor(srcList).addOrReplaceAll(srcList.subList(first,last+1));
        return;
      }
      //else undefined for now, do what?  associateAll or addOrReplaceAll?
      else System.err.println(getClass().getName()+
                 " Undefined action for intervalAdded first,last: " +first+","+last);
      //Object data = sc.getValue();
    }
    public void intervalRemoved(ListDataStateEvent e) {
      if (! listenToMagReadingListRemove) return;
      StateChange sc = e.getStateChange();
      if (! sc.getStateName().equals("removed")) return;
      int first = e.getIndex0();
      int last = e.getIndex1(); 
      // SolutionAssociatedListIF srcList = (SolutionAssociatedListIF) e.getSource();
      Object data = sc.getValue();
      if (data != null) {
        //System.out.println("Sol - my list interval:"+first+","+last+" removed via mag: "+e.getSource().getClass().getName());
        if (first > -1 && first == last) {
          remove((JasiCommitableIF) data);
          return;
        }
        JasiCommitableIF [] jcArray = (JasiCommitableIF []) data;
        for (int idx=0; idx<jcArray.length; idx++) {
          remove((JasiCommitableIF) jcArray[idx]);
        }
      }
    }
    public void contentsChanged(ListDataStateEvent e) {
      if (! listenToMagReadingListAdd) return;
      //System.out.println("Sol - my list contents updated via mag :"+e.getSource().getClass().getName());
      // do we want "set" addOrReplace in magLists to update sol Lists?
      //StateChange sc = e.getStateChange();
      //if (! sc.getStateName().equals("replaced")) return;
      int first = e.getIndex0();
      int last = e.getIndex1(); 
      SolutionAssociatedListIF srcList = (SolutionAssociatedListIF) e.getSource();
      if (first > -1 && last == first) {
          // try for an equivalent match with new data
          addOrReplace((JasiCommitableIF) srcList.get(first));
          return;
      }
      // don't know what,where so scan myList for replaced contents of event srclist
      getListFor(srcList).addOrReplaceAll(srcList);
    }
    // match delete/undelete, or other state change, perhaps someone want to share 
    // cloned observations with multiple mags, delete from one but not the other
    // only possible if listenToMagReadingListState = false;
    public void stateChanged(ListDataStateEvent e) {
      if (! listenToMagReadingListState) return;
      //System.out.println("Sol - my list state updated via mag :"+e.getSource().getClass().getName());
      int first = e.getIndex0();
      int last = e.getIndex1(); 
      SolutionAssociatedListIF srcList = (SolutionAssociatedListIF) e.getSource();
      if (first > -1 && last > -1) {
        StateChange sc = e.getStateChange();
        if (sc.getStateName().equals("deleted")) {
          // match time, sol, and mag for same err
          // to be robust scan sublist to see if state change differs current value
          // before invoking action, hopefully delete method doesn't cause feedback
          // event storm between mag and sol lists, only firing when state changes
          // Shortcoming if aliased element in both lists then first one that modifies
          // the elements notifies listeners but the other list doesn't since that
          // state of the aliased element remains the same. Perhaps a new event
          // named state "unchanged" should to be fired to notify listeners.
          if (sc.getValue() == Boolean.TRUE)
            getListFor(srcList).deleteAll(srcList.subList(first,last+1));
          else if (sc.getValue() == Boolean.FALSE)
            getListFor(srcList).undeleteAll(srcList.subList(first,last+1));
        }
      }
      // else undefined, do what?
      else System.err.println(getClass().getName()+
                 " Undefined stateChange interval first,last: " +first+","+last);
    }
    public void orderChanged(ListDataStateEvent e) {}
  }//end of MagReadingDataListener class

/**
 * Return a brief string of the form:
 */
    public String toString() {
      StringBuffer sb = new StringBuffer(256);
      // accomodate null magnitude
      Format df5 = new Format("%5.2f");
      String magType = "**";
      String magVal  = df5.form(0.0);
      if (magnitude != null) {
        magType = magnitude.getTypeString();
        magVal  = df5.form(magnitude.value.doubleValue());
      }
      sb.append(id).append(" ");
      //sb.append( (datetime.isNull()) ? "Null DateTime" : LeapSeconds.trueToString(datetime.doubleValue()) ).append(" "); // UTC datetime -aww 2008/02/10
      sb.append( (datetime.isNull()) ?
              "Null DateTime" : LeapSeconds.trueToString(datetime.doubleValue(),"yyyy-MM-dd HH:mm:ss.ff") ).append(" "); // -aww 2015/11/20
      sb.append(lat).append(" ");
      sb.append(lon).append(" ");
      sb.append(depth).append(" ");
      sb.append(magType).append(" ");
      sb.append(magVal).append(" ");
      sb.append(rms).append(" ");
      sb.append(totalReadings).append(" ");
      sb.append(distance).append(" ");
      sb.append(gap).append(" ");
      sb.append(authority).append(" ");
      sb.append(source).append(" ");
      sb.append(getEventTypeString()).append(" ");
      sb.append(gtype).append(" ");
      sb.append(processingState).append(" ");
      sb.append(timeFixed).append(" ");
      sb.append(locationFixed).append(" ");
      sb.append(depthFixed).append(" ");
      sb.append(who).append(" ");
      sb.append(prefMagMap.toString()).append(" ");
      // below added for debugging extra columns added to the origin table by schema change 03/16/2015 -aww
      sb.append(mdepth).append(" ");
      sb.append(depthCrustType).append(" ");
      sb.append(crustModelType).append(" ");
      sb.append(crustModelName).append(" ");
      return sb.toString();
    }
/**
 * Make a summary text string. <p>
 * Example: <p>
   9619508 October 21, 1999 22:18:12.87  34.1417 -116.5068   5.00 1.90 Ml local
*/
  public String toSummaryString() {
     Format df0 = new Format("%10d");    // CORE Java Format class
     Format df1 = new Format("%8.4f");   // CORE Java Format class
     Format df2 = new Format("%9.4f");   // CORE Java Format class
     Format df5 = new Format("%5.2f");
     Format df6 = new Format("%6.2f");
     Format ds2 = new Format("%2s");
     Format ds3 = new Format("%-3s");
     // WARNING "ff" in string below is format code used in EpochTime for fractional seconds parsing
     //String dtStr = getDateTime().toDateString("MMM dd, yyyy HH:mm:") + dt.getSecondsStringToPrecisionOf(2); // -aww 2008/02/10
     //String dtStr = getDateTime().toDateString("yyyy-MM-dd HH:mm:") + dt.getSecondsStringToPrecisionOf(2); // -aww 2015/11/15
     String dtStr = getDateTime().toDateString("yyyy-MM-dd HH:mm:ss.ff"); // instead aww 2015/11/20

     StringBuffer sb = new StringBuffer(132);
     sb.append(df0.form(id.longValue())).append(" ");
     sb.append(ds2.form((eventAuthority.isNull()) ? "??" : eventAuthority.toString().substring(0,2))).append(" ");
     sb.append(dtStr).append("  ");
     sb.append(df1.form(lat.doubleValue())).append(" ");
     sb.append(df2.form(lon.doubleValue())).append(" ");
     //sb.append(df6.form(depth.doubleValue()));
     sb.append(mdepth.isValid() ? df6.form(mdepth.doubleValue()) : "------");

    // only append mag if not null
    if (magnitude != null) {
       sb.append(" ").append(df5.form(magnitude.value.doubleValue()));
       sb.append(" ").append(ds3.form(magnitude.getTypeString()));
    } else {
       sb.append("   0.0 Mun");
    }
    sb.append(" ").append(EventTypeMap3.toDbType(getEventTypeString()));
    String str = getOriginGTypeString();
    sb.append(" ").append(ds2.form((str.equals("")) ? "-" : GTypeMap.toDbType(str).toUpperCase()));
    str = who.toString();
    sb.append(" ").append(new Format("%-8s").form(str.substring(0,Math.min(8,str.length()))));
    sb.append(" ").append(ds2.form((crustModel.isNull() ? "--" : crustModel.toString())));
    sb.append(" ").append(ds2.form((velModel.isNull() ? "--" : velModel.toString())));
    sb.append(" ").append(ds2.form(type.isNull() ? "-" : type.toString()));
    sb.append(" ").append(new Format("%3d").form((srcVersion.isNull() ? -1l : srcVersion.longValue())));
    sb.append(" ").append(ds2.form(depthCrustType.isNull() ? "-" : depthCrustType.toString()));
    sb.append(" ").append(ds2.form(crustModelType.isNull() ? "-" : crustModelType.toString()));
    sb.append(" ").append(ds3.form(crustModelName.isNull() ? "---" : crustModelName.toString()));
    //sb.append(" ").append(mdepth.isValid() ? df6.form(mdepth.doubleValue()) : "-----");
    sb.append(" ").append(depth.isValid() ? df6.form(depth.doubleValue()) : "------");
    sb.append(" ").append(mdepth.isValid() ? df5.form(mdepth.doubleValue()-depth.doubleValue()) : "-----");

    return sb.toString();
   }

   public String toNeatString() {
     return toSummaryString();
   }
/*
      Return a fixed format header to match output from toNeatString(). Has the form:
@see: toNeatString()
    */
    public static String getNeatStringHeader() {
//              9619508 October 21, 1999 22:18:12.8700  34.1417 -116.5068   5.00 1.90 Ml local
     return "  evid     nt date       time          lat      lon          mz   mag mt  et gt who      cm vm ty ver gm dm mod     gz  datm";
    }

    public String getNeatHeader() { return getNeatStringHeader(); }

    public String toErrorString() {
        StringBuffer sb = new StringBuffer(132);
        sb.append(toShortErrorString());
        Format df52 = new Format("%5.2f");
        Format df40 = new Format("%4.0f");
        //OriginError attributes
        sb.append(" ").append(df40.form(aziLarge.doubleValue()));
        sb.append(" ").append(df40.form(dipLarge.doubleValue()));
        sb.append(" ").append(df52.form(errLarge.doubleValue()));
        sb.append(" ").append(df40.form(aziInter.doubleValue()));
        sb.append(" ").append(df40.form(dipInter.doubleValue()));
        sb.append(" ").append(df52.form(errInter.doubleValue()));
        sb.append(" ").append(df52.form(errSmall.doubleValue()));
        return sb.toString();
    }
/**
 * Make a summary text string of the error information. <p>
 */
    public String toShortErrorString() {
      Format df1 = new Format("%5.2f");
      Format sl4 = new Format("%-4.4s");
      Format sr4 = new Format("%4.4s");
      Format df2 = new Format("%4d");
      Format sl8 = new Format("%-8.8s");

      int Scount =  sReadings.intValue();
      if (Scount < 0) Scount = 0;       // no nulls

      int Pcount =  usedReadings.intValue() - Scount;
      if (Pcount < 0) Pcount = 0;

      int fm = firstMotions.intValue();
      if (fm < 0) fm = 0;

      StringBuffer sb = new StringBuffer(80);
      sb.append(sr4.form(String.valueOf(Pcount))).append("/").append(sl4.form(String.valueOf(Scount)));
      sb.append(" ").append(new Format("%3d").form(fm));
      sb.append(" ").append(df1.form(rms.doubleValue()));
      sb.append(" ").append(df1.form(errorHoriz.doubleValue()));
      sb.append(" ").append(df1.form(errorVert.doubleValue()));
      sb.append(" ").append(df2.form(distance.intValue()));
      sb.append(" ").append(df2.form(gap.intValue()));
      sb.append(" ").append(df1.form(quality.doubleValue()));
      sb.append(" ").append(sl4.form(authority.toString()));
      sb.append(" ").append(sl8.form(source.toString()));
      sb.append(" ").append(sl8.form(algorithm.toString()));
      return sb.toString();
    }

   public static String getShortErrorStringHeader() {
//              dddd/dddd ddd xx.xx xx.xx xx.xx dddd dddd xx.xx ssss ssssssss ssssssss
       return  "#  P/S     Fm   RMS  errH  errZ dmin  gap  qual auth src      meth    ";
   }
   public static String getErrorStringHeader() {
//              dddd/dddd ddd xx.xx xx.xx xx.xx dddd dddd xx.xx ssss ssssssss ssssssss xxxx xxxx xx.xx xxxx xxxx xx.xx xx.xx
       return  "#  P/S     Fm   RMS  errH  errZ dmin  gap  qual auth src      meth     aziL dipL  errL aziI dipI  errI  errS";
   }

/*

Finger 1
 00/01/12 20:19:01  35.05N 117.65W   8.4 1.5MGN C*   4 mi. N   of Boron, CA

Finger 2
9133867 CI 00/01/12 20:19:01  35.05N 117.65W   8.4 1.5MGN C*   4 mi. N   of Boron, CA"

Finger 3
                               ffff.ff ffff.ff  fff.f f.f
 9133867 CI 2000/01/12 20:19:01  35.05N 117.65W   8.4 1.5MGN C*   4 mi. N   of Boron, CA

   9619508 le October 21, 1999 22:18:12.870  34.1417 -116.5068   5.00 1.90 Ml

*/

  public String toFingerFormat() {
    Format df0 = new Format("%10d");    // CORE Java Format class
    Format df1 = new Format("%5.2f");
    Format df2 = new Format("%6.2f");
    Format df3 = new Format("%5.1f");
    Format df4 = new Format("%3.1f");

    //
    String dtStr = LeapSeconds.trueToString(datetime.doubleValue(),"yyyy/MM/dd HH:mm:ss"); // for UTC datetime -aww 2008/02/10

    // handle hemipsphere issues
    float latf = lat.floatValue();
    String ns = "N";
    if (latf < 0) {
      latf = Math.abs(latf);
      ns = "S";
    }
    float lonf = lon.floatValue();
    String ew = "E";
    if (lonf < 0) {
      lonf = Math.abs(lonf);
      ew = "W";
    }

    // guard against null or short authority
    String auth = authority.toString();
    if (auth == null) auth = "  ";
    if (auth.length() < 2) {
      auth = auth+"  ";
    } else { // truncate to 2 chars
      auth = auth.substring(0, 2);
    }
    StringBuffer sb = new StringBuffer(132);
    sb.append(df0.form(id.longValue())).append(" ");
             sb.append(auth).append(" ");
             sb.append(dtStr).append(" ");
             sb.append(df1.form(latf)).append(ns).append(" ");
             sb.append(df2.form(lonf)).append(ew).append(" ");
             sb.append(df3.form(depth.doubleValue())).append(" ");
             sb.append(df4.form(magnitude.value.doubleValue())).append(" ");
             sb.append(magnitude.getTypeString());
    return sb.toString();
  }

  public String fullDump() {
    int wfCount = waveformList.size();
    StringBuffer sb = new StringBuffer(wfCount*132+phaseList.size()*132+256);
    sb.append(toSummaryString()).append("\n");
    sb.append(toErrorString()).append("\n");
    sb.append(phaseList.dumpToArcString());
    if (wfCount > 0) {
      for (int i = 0; i < wfCount; i++) {
        sb.append(waveformList.get(i)).append("\n");
      }
    } else {
        sb.append(" * No waveforms for this solution. \n");
    }
    return sb.toString();
  }
    // debug supporting utility method
    static public final String getSummaryStateString(Solution sol, Magnitude mag) {
      StringBuffer sb = new StringBuffer(180);
      if ( sol != null ) {
          sb.append("  Sol  stale,needsCommit,hasChanged,valid,dummy: ");
          sb.append(sol.isStale());
          sb.append(" ");
          sb.append(sol.getNeedsCommit());
          sb.append(" ");
          sb.append(sol.hasChanged());
          sb.append(" ");
          sb.append((sol.validFlag.longValue()==1l));
          sb.append(" ");
          sb.append((sol.dummyFlag.longValue()==1l));
      }
      else sb.append("  Input Sol null.");
      sb.append("\n");

      if ( mag != null) {
          sb.append("  Mag  stale,needsCommit,hasChanged: "); 
          sb.append(mag.isStale());
          sb.append(" ");
          sb.append(mag.getNeedsCommit());
          sb.append(" ");
          sb.append(mag.hasChanged());
      }
      else
          sb.append("  Input Mag null.");
      sb.append("\n");
      return sb.toString();
    }

//
  private class DistElev {
     Channel ch = null; // after debugging can remove this data member from class
     double dist = 0.; 
     double elev = 0.;

     DistElev(Channel chan, double distance, double elevation) {
         ch = chan;
         dist = distance;
         elev = elevation;
     }

     public String toString() {
         return ((ch == null) ? "NULL" : ch.toDelimitedSeedNameString()) + " " + dist + " " +  elev;
     }
  }
  public double calcOriginDatumKm() {
      // average 5 closest sta elev in phase list
      return calcOriginDatumKm(5);
  }
  // average the input number of distinct sta elevs in phaselist with known distances and non-zero elev > -.12 km 
  // returns Double.NaN when no phases or no solution has null LLZ
  public double calcOriginDatumKm(int nsta) {

      // NOTE: static property should be enabled only to correct depth for datum when setting trial depth for CRH CRT models (non-geoidal type)
      if (!calcOriginDatum) {
          //System.out.println("DEBUG: calcOriginDatum disabled, returning 0.");
          return 0.;
      }

      LatLonZ myllz = getLatLonZ();
      int nphs = phaseList.size();

      // bad or no data, undefined datum NaN
      if (nsta == 0 || nphs == 0 || myllz.isNull()) {
          //System.out.println("DEBUG: error, calcOriginDatum missing sta phases or lat,lon returning NaN.");
          return Double.NaN;
      }

      // sort instance phase list (its usually already sorted)
      phaseList.distanceSort(myllz);

      // remove all "zero" elevation and unknown distance channels and save non-duplicated sta data
      Phase [] parray = phaseList.getArray();
      DistElev [] data = new DistElev[nphs];
      Channel oldChan = null;
      Channel nextChan = null;
      double nextDist = 0;
      double nextElev = 0.;
      int kphs = 0;  
      for ( int i=0; i<nphs; i++) {
         nextChan = parray[i].getChannelObj();
         if (oldChan != null && nextChan.sameStationAs(oldChan.getChannelId()) ) continue; // skip to next
         oldChan = nextChan;
         nextDist = nextChan.getHorizontalDistance();
         nextElev = -nextChan.getLatLonZ().getZ(); // note z is stored as "depth" not "elev"
         // Must have non-zero elev >= -120 m and valid distance km
         if ( nextElev != 0.  && nextElev >= -.12 && nextDist != Channel.NULL_DIST )  {
             data[kphs] = new DistElev(nextChan, nextDist, nextElev);
             kphs++;
         }
      }
      // no eligible phases
      if (kphs == 0) return Double.NaN;

      //System.out.println("DEBUG calcOriginDatumKm valid input phases kphs: " + kphs);

      Comparator mycmp = new Comparator() {
          public int compare(Object o1, Object o2) {
              DistElev de1 = (DistElev) o1;
              DistElev de2 = (DistElev) o2;
              if ( de1 == de2 ) return 0;
              if ( de1 == null) return 1;
              if ( de2 == null) return -1;
              if (de1.dist == de2.dist) {
                  if (de1.elev == de2.elev) return 0;
                  return (de1.elev < de2.elev) ?  -1 : 1;
              }
              return ( de1.dist < de2.dist ) ? -1 : 1;
          }
          public boolean equals(Object o) {
              return ( o == this );
          }
      };
      // data array should have only 1 entry per station, sort list by distance, elev
      if (kphs > 1) {
          Arrays.sort(data,mycmp);
          //for (int i=0; i<kphs; i++) {
          //    System.out.println("DEBUG calcOriginDatumKm: " + data[i].toString());
          //}
      }

      // Save elev of closest sta
      double datum = data[0].elev;

      // If closest > 100 km return elev of closest, or if 2 at same distance, the first it the one with lowest elevation
      if ( data[0].dist > 100.) return datum;

      // For distances <= 100 km, average the nsta closest elevations
      nphs = ( kphs < nsta) ? kphs : nsta; // min of sta <= 100 or input#
      for ( int i=1; i<nphs; i++ ) {
          datum += data[i].elev;
      }   
      datum = datum/nphs;
      //System.out.println("DEBUG calcOriginDatumKm: nphs:" + nphs + " datumKm: " + datum);
      return datum;
  }
//

    public void setAssociation(String str) {
        if (! association.equalsValue(str)) {
            association.setValue(str);
        }
    }

    public String getAssociation() {
        return association.toString();
    }  
} // end of Solution class
