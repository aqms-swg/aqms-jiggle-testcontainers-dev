// OVERLOADING VS. OVERIDDEN METHODS !
// CHECK EFFECT OF "IF"-TYPE BROADENING CASTS CASTS IN CLASS HIERARCHY,
// IS CORRECT OVERLOADED METHOD INVOKED IN SUBCLASSES?
// COMPILE TIME ARG-TYPE METHOD SIGNATURE USED, NOT RUNTIME ARG TYPE.
//
// TODO: implement copy/init mag,sol data IF logic in super,sub assoc classes - aww
package org.trinet.jasi;
//public abstract class SolutionAssocJasiObject extends SourceAssocJasiObject implements JasiSolutionAssociationIF {
public abstract class SolutionAssocJasiObject extends CommitableJasiObject implements JasiSolutionAssociationIF {

    /** Solution with which this object instance is affiliated. */
    public Solution sol = null;

    public Object clone() {
      SolutionAssocJasiObject sjr = (SolutionAssocJasiObject) super.clone();
      sjr.sol = this.sol; // leave the same?
      return super.clone();
    }

    /** Override flags this object as well as a its associated Solution
     * as needing a commit to update the DataSource archive.
     * Set true after a change that requires a DataSource update of this object and
     * possibly its associated solution.
     * Set false after DataSource commit update.
     * */
    public void setNeedsCommit(boolean tf) {
      if (tf && sol != null) sol.setNeedsCommit(tf);
      super.setNeedsCommit(tf);
    }

    /** Simple assignment affiliation, instance not added to input Solution.
     *  Otherwise assigns input Solution if previous assignment if not same.
     *  No state change.
     *  @throws IllegalArgumentException if this instance is associated with
     *  another Solution and contained in its collection.
     *@see #associate(Solution)
     */
    public void assign(Solution sol) {
        if (this.sol != null && this.sol != sol) {
          try {
            if (this.sol.contains(this)) { // isAssociatedWith(); 
              throw new IllegalArgumentException("Object already associated with solution:" +
                              this.sol.getIdentifier().toString() +
                            " use associate(Solution)");
            }
          }
          catch (IllegalArgumentException ex) {
            ex.printStackTrace();
            return;
          }
        }
        this.sol = sol;
    }
    /**
     * Associate this instance with input Solution such that containedBy(Sol) == true.
     * Implies object becomes a member of Solution collection except that an
     * input of null unassociates this object from any Solution.
     * Implementations may reset virtual delete state to undeleted.
     * @see #unassociate()
     */
    public void associate(Solution newSol) {
        // delegate unassociate old /associate new actions to non-null Solution assignments
        if (newSol !=  null) {
          newSol.associate(this); // undelete() done by sol or by subclass override?
        }
        this.sol = newSol;
    }
    /**
     * Remove equivalent object reference from associated solution
     * and null instance solution assignment.
     * Default is no change instance virtual deletion state.
     */
    public void unassociate() {
        if (sol != null) sol.unassociate(this);
        sol = null;
    }
    /**
     * Convenience wrapper combines unassociate() and delete().
     */
    public boolean erase() {
      unassociate();
      return delete();
    }
    /** Return the solution with which this instance is associated.
     * Returns null if unassociated.
     * */
    public Solution getAssociatedSolution() {
        return sol;
    }

    /** Object which identifies the origin with which the object is associated.
     * Returns null if not defined, usually the Object has an an equivalent
     * integer value.
     * */
    public abstract Object getOriginIdentifier();

    /** Returns 'true' if this instance contains a non-null Solution reference.
     * Does not check if assigned Solution contains a reference equivalent to this instance.
     */
    public boolean isAssignedToSol() { return (sol != null) ; }

    /** Returns 'true' if this instance affiliation is that of the non-null input Solution.
     * Does not check if assigned Solution contains a reference equivalent to this instance.
     * */
    public boolean isAssignedTo(Solution sol) { return (this.sol == sol) ; }

    /** Returns 'true' if this instance is affiliated with a non-null Solution.
     * Default implementation: analog for isAssignedToSol().
     * Assigned Solution is NOT checked to contain a reference equivalent to this instance.
     */
    public boolean isAssociated() {
        return (sol != null); // && sol.contains(this));
    }
    /** Returns 'true' if this instance is associated with non-null input Solution.
     * Default implementation: analog for isAssignedTo(Solution).
     * Assigned Solution is NOT checked to contain a reference equivalent of this instance.
     * */
    public boolean isAssociatedWith(Solution sol) {
        return (this.sol == sol); // && sol != null && sol.contains(this));
    }
    /** Returns 'true' if this instance and the input have
     * equivalent Solution assignments.
     * */
    public boolean assocEquals(JasiSolutionAssociationIF obj) {
        if (obj == null) return false;
        return ( obj.isAssignedTo(this.sol) ||
                 this.sol != null && this.sol.equivalent(obj.getAssociatedSolution()) );
    }
    /** Returns true if input Solution contains a reference to an equivalent object. */
    public boolean containedBy(Solution sol) {
      return (sol == null) ? false : sol.contains(this);
    }
    /** Add to the input Solution, if this instance is affiliated with it.
     * Instance is not added if Solution already contains equivalent object.
     * Returns false if not added, or null Solution input argument.
     * */
    // Default no change in deletion state.
    public boolean addTo(Solution sol) {
      return (sol == null || this.sol != sol) ? false : sol.add(this);
    }
    /** Add or replace equivalent object reference in the input Solution,
     * if this instance is affiliated with it.
     */
    public boolean addOrReplaceIn(Solution sol) {
      return (sol == null || this.sol != sol) ? false : sol.addOrReplace(this);
    }
    /** Remove equivalent object reference from the input Solution.  */
    // Default no change in instance Solution affiliation or deletion state.
    public boolean removeFrom(Solution sol) {
      //sol.unassociate(this) // not this, it nulls assignment
      return (sol == null) ? false : sol.remove(this);
    }
    /** Virtually delete this instance if associated with input Solution 
     * reference. Solution notifies listeners of delete state change.
     */
    //Default no change in instance Solution affiliation.
    public boolean deleteFrom(Solution sol) {
      return (sol == null) ? false : sol.delete(this);
    }
    /* aww need to test this implementation below after 03/03:
    public boolean delete() {
      return (this.sol == null) ? super.delete() : this.sol.delete(this);
    }
    public boolean undelete() {
      return (this.sol == null) ? super.undelete() : this.sol.undelete(this);
    }
    */
    
// abstract JasiSolutionAssociationIF methods
// abstract public void initSolDependentData(); // b4 calculation, or when "recalculated" reset values
// abstract public void copySolutionDependentData(JasiReading jr); // must be this.isSameChanTypeAndTime(jr)?
//
}
