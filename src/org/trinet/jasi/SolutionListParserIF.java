package org.trinet.jasi;
import org.trinet.util.*;

public interface SolutionListParserIF {
  public static final int SOL_TYPE = 0;
  public static final int MAG_TYPE = 1;
  public static final int AMP_TYPE = 2;
  public static final int PHASE_TYPE = 3;
  public static final int CODA_TYPE = 4;
  public static final int UNKNOWN_TYPE = 99;

  public Solution parseSolution();
  public GenericSolutionList parseSolutions();
  public void setFileName(String filePathName);
  public void setGraphicsOwner(java.awt.Component owner);
  public void setProperties(GenericPropertyList gpl);
}
