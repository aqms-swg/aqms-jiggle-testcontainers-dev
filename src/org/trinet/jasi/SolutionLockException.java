package org.trinet.jasi;

/**
 * Exception class that gets thrown when there's a problem in the SolutionLockClass.
 *
 * @author Doug Given
 */

public class SolutionLockException extends Exception {
    
    public SolutionLockException (String msg) {
	super (msg);
    }    
} // SolutionLockException
