//
//TODO: Migrate common "writing" methods to a JasiReadingDbWriter class that
//can be used by PhaseTN, AmplitudeTN and CodaTN
//e.g. commit(), dbInsert(), dbAssociateXXX() etc.
//
package org.trinet.jasi.TN;

import java.util.*;
import java.sql.*;

import org.trinet.jasi.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

/**
 * A concrete Amplitude object represents an peak amplitude reading made by any
 * number of techniques.
 */
public class AmplitudeTN extends Amplitude implements DbMagnitudeAssocIF, DbReadableJasiObjectIF {

    // if static call getBy(...) methods with this instance so parsing resultset preserves correct joinType state
    private static JasiDbReader jasiDataReader =  null; // new JasiDbReader(this); // if not static
    //private static JasiDbReader jasiDataReader = new JasiDbReader("org.trinet.jasi.TN.AmplitudeTN");
    //protected static JasiDbReader jasiDataReader = new JasiDbReader();
    /*
    {
      //jasiDataReader.setFactoryClassName(getClass().getName());
      if (JasiDatabasePropertyList.debugTN) setDebug(true);
      else if (JasiDatabasePropertyList.debugSQL) jasiDataReader.setDebug(true);
    }
    */

    /* below replace static block, do it for each instance -aww 2008/11/13
    {
        jasiDataReader.setDebug(JasiDatabasePropertyList.debugSQL);
        setDebug(JasiDatabasePropertyList.debugTN);
    }
    */


    // database table structure to use
    private static final Amp tableObj = new Amp();  // default
    //private static String tname = tableObj.getTableName();

    /** database read id of associations */
    //protected DataLong orid = new DataLong();
    //protected DataLong magid = new DataLong();

    /** the ampid */
    protected DataLong ampid = new DataLong();

    /** True if data were orginally read from or has been written to the dbase.
     Used to know if a commit requires an 'insert' or an 'update' */
    protected boolean fromDbase = false;

    /** True if this Amp's association was orginally read from the dbase. Used to know if
        a commit requires an 'insert' or an 'update' */
    protected boolean assocFromDbase = false;

    protected static final int Type_AssocNone= 0;
    protected static final int Type_AssocAmO = 1;
    protected static final int Type_AssocAmM = 2;

    /** Holds value describing if this is amp lookup is done with a join to the
     * AssocAm or AssocAmM table */
    private int joinType = Type_AssocNone;

    /** The standard start to most SQL queries, to get amps associated with an
        event, joins Amp, AssocAmO tables */
    //private static String sqlPrefixByEvent = null;

    /** The standard start to most SQL queries, to get amps associated with a
        magnitude, joins Amp, AssocAmM tables */
    //private static String sqlPrefixByMag = null;

    /** The standard start to most generic SQL queries, to get amps. */
    //private static String sqlPrefixGeneric = null;

    /** order so channels appear together in the list. */
    //protected static String sqlOrderSuffix = " order by net, sta "; // removed, replaced with below -aww 01/06/2005
    //protected static String sqlOrderSuffix = " order by net, sta, seedchan, "+tname+".lddate, "+tname+".ampid ";
    //protected static String sqlOrderSuffix = null;
    

    // Replace Strings above with:
    private static final String psSqlPrefix=
      //  "SELECT amp.AMPID,amp.DATETIME,amp.STA,amp.NET,amp.AUTH,amp.SUBSOURCE,amp.CHANNEL,amp.CHANNELSRC,amp.SEEDCHAN,amp.LOCATION," +
      //  "amp.AMPLITUDE,amp.AMPTYPE,amp.UNITS,amp.AMPMEAS,amp.PER,amp.SNR,amp.QUALITY,amp.RFLAG,amp.CFLAG,amp.WSTART,amp.DURATION";
      "SELECT amp.AMPID,TRUETIME.getEpoch(amp.DATETIME,'UTC'),amp.STA,amp.NET,amp.AUTH,amp.SUBSOURCE,amp.CHANNEL,amp.CHANNELSRC,"+
      "amp.SEEDCHAN,amp.LOCATION,amp.AMPLITUDE,amp.AMPTYPE,amp.UNITS,amp.AMPMEAS,amp.PER,amp.SNR,amp.QUALITY,amp.RFLAG,amp.CFLAG,"+
      "TRUETIME.getEpoch(amp.WSTART,'UTC'),amp.DURATION";

    private static final String psSqlPrefixByAmpid = psSqlPrefix + " FROM AMP amp WHERE (amp.ampid=?)";

    private static final String psSqlPrefixByMagid =  psSqlPrefix + 
        ",m.WEIGHT,m.IN_WGT,m.MAG,m.MAGRES,m.MAGCORR FROM AMP amp, ASSOCAMM m WHERE (amp.ampid=m.ampid) AND (m.magid=?)";

    private static final String psSqlPrefixByOrid = psSqlPrefix + 
        ",o.DELTA,o.SEAZ FROM AMP amp, ASSOCAMO o WHERE (amp.ampid=o.ampid) AND (o.orid=?)";

    private static final String psSqlPrefixByEvid = psSqlPrefix + 
        ",o.DELTA,o.SEAZ FROM AMP amp, ASSOCAMO o WHERE amp.ampid=o.ampid and o.orid=(SELECT prefor FROM EVENT WHERE evid=?)";

    private static final String psSqlPrefixByTime = psSqlPrefix +
        " FROM AMP amp WHERE (amp.DATETIME between TRUETIME.putEpoch(?,'UTC') AND TRUETIME.putEpoch(?,'UTC')) ORDER BY amp.DATETIME";

    private static final String psSqlOrderSuffix = " ORDER BY amp.NET,amp.STA,amp.SEEDCHAN,amp.LOCATION,amp.LDDATE DESC,amp.AMPID DESC";

    public AmplitudeTN() {
      //setStrings();
    }

    /*
    public AmplitudeTN(DataTableRow tableRow) {
      setTableObj(tableObj);
    }
    */

    /*
    public AmplitudeTN(AmplitudeTN aAmp) {
      replace(aAmp);
    }
    */

    public void setDebug(boolean tf) {
      debug = tf;
      if (jasiDataReader != null) jasiDataReader.setDebug(debug);
    }

    protected int getJoinType() {
      return joinType;
    }

    protected void setJoinType(int type) {
      joinType = type;
    }

    protected DataTableRow getTableObj()  {
      return (AmplitudeTN.tableObj == null) ?  new Amp() : AmplitudeTN.tableObj;
    }

    /*
    protected void setTableObj(DataTableRow tableRow)  {
      tableObj = tableRow;
      tname = tableObj.getTableName();
      setStrings();
    }
    */
/*
    protected String getTableName() {
      return tname;
    }

    // Create the SQL prefix strings using the table name derived from the tableType passed to the constructor. 
    protected void setStrings() {

      sqlOrderSuffix = " order by net, sta, seedchan, "+tname+".lddate, "+tname+".ampid";

      StringBuffer sb = new StringBuffer(512);
      // The standard start to most SQL queries, to get amps associated with an event, joins Amp & AssocAmO tables 
      //
      sb.append("Select "); 
      //sb.append(getTableObj().qualifiedColumnNames());  // just returns raw column names -aww
      sb.append(((Amp) tableObj).QUALIFIED_COLUMN_NAMES); // to get truetime.getEpoch(datetime,'UTC') -aww 2008/02/14
      //sb.replace("GENERIC","AMP"); // kludge for weird AMP tablerow IF init -aww
      sb.append(",");
      sb.append(AssocAmO.QUALIFIED_COLUMN_NAMES);
      sb.append(" from ").append(tname);
      sb.append(", AssocAmO where (");
      sb.append(tname).append(".ampid = AssocAmO.ampid) ");
      sqlPrefixByEvent = sb.toString();
      //

      // The standard start to most SQL queries, to get amps associated with a magnitude, joins Amp & AssocAmM tables 
      sb = new StringBuffer(512);
      sb.append("Select ");
      //sb.append(getTableObj().qualifiedColumnNames());
      sb.append(((Amp) tableObj).QUALIFIED_COLUMN_NAMES); // to get truetime.getEpoch(datetime,'UTC') -aww 2008/02/14
      //sb.replace("GENERIC","AMP"); // kludge for weird AMP tablerow IF init -aww
      sb.append( ",");
      sb.append(AssocAmM.QUALIFIED_COLUMN_NAMES);
      sb.append(" from ").append(tname).append(", AssocAmM where (");
      sb.append(tname).append(".ampid = AssocAmM.ampid) ");
      sqlPrefixByMag = sb.toString();

      // The standard start to most SQL queries, to get amps in a time window 
      sb = new StringBuffer(512);
      sqlPrefixGeneric = sb.toString();
      sb.append("Select ");
      //sb.append(getTableObj().qualifiedColumnNames());
      sb.append(((Amp) tableObj).QUALIFIED_COLUMN_NAMES); // to get truetime.getEpoch(datetime,'UTC') -aww 2008/02/14
      //sb.replace("GENERIC","AMP"); // kludge for weird AMP tablerow IF init -aww
      sb.append(" from ").append(tname).append(" ");
      sqlPrefixGeneric = sb.toString();

    }
    protected String getSqlPrefixByEvent() {
      return sqlPrefixByEvent;
    }
    protected String getSqlPrefixByMag() {
      return sqlPrefixByMag;
    }
    protected String getSqlPrefixGeneric() {
      return sqlPrefixGeneric;
    }
    protected String getSqlPrefixByEvent(long evid) {
      StringBuffer sb = new StringBuffer(1024);
      sb.append(sqlPrefixByEvent);
      sb.append(" and (AssocAmO.orid = (Select prefor from Event where evid = ").append(evid).append("))");
      return sb.toString();
    }
*/

//
// End of modifications for Unassoc Amp implementation
//
    public String toAssocIdString() {
        StringBuffer sb = new StringBuffer(super.toAssocIdString());
        sb.append(" orid: ").append(getOridValue()); // orid.toStringSQL());
        return sb.toString();
    }

    // accessors for data members
    public Object getIdentifier() {
        return ampid;
    }
    public void setIdentifier(Object obj) {
        ampid.setValue((DataLong) obj);
    }
    public long getAmpid() {
        return (ampid.isValidNumber()) ? ampid.longValue() : 0l;
    }
    public Object getOriginIdentifier() {
        return (sol == null) ? null : sol.getPreferredOriginId();
    }
    public long getOridValue() {
        //return (orid.isValidNumber()) ? orid.longValue() : 0l;
        if (sol == null) return 0l;
        DataLong orid = (DataLong) sol.getPreferredOriginId();
        return  (orid.isValidNumber()) ? orid.longValue() : 0l;
    }
    public long getMagidValue() {
        DataLong dl = (DataLong)getAssociatedMag().getIdentifier();
        return  (dl.isValidNumber()) ? dl.longValue() : 0l;
    }
    //public void setOrid(long id) {orid.setValue(id);}
    public void setAmpid(long id) {ampid.setValue(id);}

    /*
    protected static void associateList(Solution sol,  List amps) {
        if (sol.getAmpList().isEmpty()) {
            sol.fastAssociateAll(amps); // does not filter input elements
        }
        else {
            sol.associateAll(amps); // filters input for dupes or nulls
        }
    }
    protected static void associateList(Magnitude mag, List amps) {
        if (!mag.isAmpMag()) {
           System.err.println("WARNING - AmplitudeTN associateList input mag is not amp mag subtype!");
        }
        if (mag.getReadingList().isEmpty()) {
            mag.fastAssociateAll(amps); // does not filter input elements
        }
        else {
            mag.associateAll(amps); // filters input for dupes or nulls
        }
    }
    */


    /** Return the SQL clause to get Amplitudes only of the types in the list.
    * For example given String list[] = {"PGA", "WAC", "HEL" }  this will return
    * " and ((amptype like 'PGA') or (amptype like 'WAC') or amptyp like 'HEL')
    * */
    protected String makeTypeClause(String[] typeList) {
        if (typeList == null || typeList.length == 0) return "";
        StringBuffer sb = new StringBuffer(256);
        sb.append(" and (");
        for (int i = 0; i < typeList.length; i++) {
          if (i > 0) sb.append(" or ");
          sb.append("(");
          //sb.append(getTableName()).append(".amptype like '");
          sb.append("amp.amptype like '"); // generic alias "amp"
          sb.append(typeList[i]);
          sb.append("')");
        }
        sb.append(")");
 //     System.out.println( "makeTypeQuery /"+sb.toString()+"/");
        return sb.toString();
    }
    /** Return the SQL clause to get amps only the nets in the list.
    * For example given String list[] = {"CI", "AZ" }  this will return
    * " and ((net like 'CI') or (net like 'AZ') ). This is not impemented using the
    * the SQL construct "IN ('CI', 'AZ')" because this would not allow the use of
    * wildcards. */
    protected String makeNetClause(String[] netList) {
           if (netList == null || netList.length == 0) return "";
           StringBuffer sb = new StringBuffer(128);
           sb.append(" and (");
           for (int i = 0; i < netList.length; i++) {
             if (i > 0) sb.append(" or ");
             sb.append("(");
             //sb.append(getTableName()).append(".net like '");
             sb.append("amp.net like '"); // generic alias "amp"
             sb.append(netList[i]);
             sb.append("')");
           }
           sb.append(")");
           return sb.toString();
    }

    /**
     * Returns a list of Amplitudes associated in the default DataSource
     * with the magid identifier of the input Magnitude.
     * Returned Amplitudes are associated with the input Magnitude
     * Does eqijoin of Amp and AssocAmM tables using input magid.
     * */
    public Collection getByMagnitude(Magnitude mag) {
           return getByMagnitude(mag, null, true); // all types, associate
    }
    public Collection getByMagnitude(Magnitude mag, String[] typeList) {
           return getByMagnitude(mag, typeList, true); // input types, associate
    }
    /**
     * Returns a list of Amplitudes associated in the default DataSource with a
     * magnitude whose magid matches the input and whose
     * type qualifiers match those specified in the input.
     * If assoc=true Amplitudes are associated with input Magnitude.
     * Returns null if there is no Magnitude in database matching input's magid.
     * Returns all types if the input typeList is null or empty.
     * Does an eqijoin of Amp and AssocAmM tables using input's magid.
     * */
    public Collection getByMagnitude(Magnitude mag, String[] typeList, boolean assoc) {
        // Do we want to check mag type?
        if (! mag.isAmpMag()) {
          System.err.println("AmplitudeTN getByMagnitude input mag not amp type");
        }
        setJoinType(Type_AssocAmM); // save it to flag resultSet parse

        long inMagid = ((MagnitudeTN)mag).getMagidValue(); // input arg id
        if (inMagid <= 0l) return null; // return is 0 if not valid (null)

        StringBuffer sb = new StringBuffer(1024);
        //sb.append(getSqlPrefixByMag());
        //sb.append(" and (AssocAmM.magid = ").append(inMagid).append(")");
        //sb.append(sqlOrderSuffix);
        sb.append(AmplitudeTN.psSqlPrefixByMagid);
        sb.append(makeTypeClause(typeList));
        sb.append(AmplitudeTN.psSqlOrderSuffix);

        //ArrayList aList = (ArrayList) ((JasiDbReader)getDataReader()).getBySQL(sb.toString());
        //ArrayList aList = (ArrayList) ((JasiDbReader)getDataReader()).getBySQL(DataSource.getConnection(), sb.toString(), this);

        ArrayList aList = null;
        PreparedStatement psByMagid = null;
        try {
            Connection conn = DataSource.getConnection();
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(sb.toString());
            psByMagid = conn.prepareStatement(sb.toString());
            psByMagid.setLong(1, inMagid);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByMagid, this, true);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }

        if (aList == null) return aList;
        int dbCount  = aList.size();
        if (dbCount < 1) return aList; // no data

        // option to associate elements of list with the input argument
        // otherwise returned list elements are orphaned from context
        //if (assoc) associateList(mag, aList);
        if (assoc) mag.associate(aList);

        return aList;
    }

        /* Query logic to remove duplicate channel data with different ampids, get most recent lddate:
        sb.append("Select * from (Select ");
        //sb.append(getTableObj().qualifiedColumnNames()).append(",");  // just returns raw column names
        sb.append(((Amp) tableObj).QUALIFIED_COLUMN_NAMES).append(","); // to get truetime.getEpoch(datetime,'UTC') -aww 2008/02/14
        //sb.replace("GENERIC","AMP"); // kludge for weird AMP tablerow IF init -aww
        sb.append(AssocAmO.QUALIFIED_COLUMN_NAMES);
        // NOTE: should partition by location, but data in db has mixed null and empty strings, needs fixing!!!! -aww
        sb.append(", dense_rank() over (partition by net,sta,seedchan,location order by");
        sb.append(tname).append(".lddate desc,").append(tname).append(".ampid desc) drank");
        sb.append(" from ").append(tname).append(", AssocAmO");
        sb.append(" where (").append(tname).append(".ampid = AssocAmO.ampid)");
        sb.append(" and (AssocAmO.orid = ").append(((SolutionTN)aSol).getOridValue()).append(")");
        sb.append(makeTypeClause(typeList));
        sb.append(") t where t.drank=1")
        */
    /**
     * Returns a list of all Amplitudes associated in the default DataSource
     * with the identifier of the input Solution.
     * Returned Amplitudes are affiliated with the input Solution,
     * but they are not added to its internal lists.
     * Returned List is a superset of that containing the
     * Amplitudes of any magnitude associated with the Solution.
     * This does a join using the AssocAmO link table.
     * */
    public Collection getBySolution(Solution aSol) {
        return getBySolution(aSol, null, true); // all types, associate
    }
    /**
     * Returns a list of all Amplitudes associated in the default DataSource
     * with the identifier of the input Solution and whose type qualifiers
     * match those of the inputType list.
     * Returns all types if input typeList is null or empty.
     * @see #getBySolution(Solution)
     * */
    public Collection getBySolution(Solution aSol, String[] typeList) {
      return getBySolution(aSol, typeList, true); // specified types associate
    }

    public Collection getBySolution(Solution aSol, String[] typeList, boolean assoc) {

        setJoinType(Type_AssocAmO);

        long inOrid = ((SolutionTN)aSol).getOridValue(); // input arg value
        if (inOrid <= 0) return null; // return is 0 if not valid (null), so don't query - aww 02/07/2005

        StringBuffer sb = new StringBuffer(1024);
        //sb.append(getSqlPrefixByEvent());
        //sb.append(" and (AssocAmO.orid = ").append(inOrid).append(")");
        //sb.append(makeTypeClause(typeList));
        //sb.append(sqlOrderSuffix);
        sb.append(AmplitudeTN.psSqlPrefixByOrid);
        sb.append(makeTypeClause(typeList));
        sb.append(AmplitudeTN.psSqlOrderSuffix);

        //ArrayList aList = (ArrayList) ((JasiDbReader)getDataReader()).getBySQL(sb.toString());
        //ArrayList aList = (ArrayList) ((JasiDbReader)getDataReader()).getBySQL(DataSource.getConnection(), sb.toString(), this);

        ArrayList aList = null;
        PreparedStatement psByOrid = null;
        try {
            Connection conn = DataSource.getConnection();
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(sb.toString());
            psByOrid = conn.prepareStatement(sb.toString());
            psByOrid.setLong(1, inOrid);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByOrid, this, true);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }

        if (aList == null) return aList;
        int dbCount  = aList.size();
        //System.out.println("getBySolution: amps found ="+dbCount);
        if (dbCount < 1) return aList; // no data

        // option to associate elements of list with the input argument
        // otherwise returned list elements are orphaned from context
        //if (assoc) associateList(aSol, aList);
        if (assoc) aSol.associate(aList);

        return aList;
    }
    /** Returns a list of Amplitudes associated in the default DataSource with a
     * Solution whose identifier id matches the input id number that have
     * a Net attribute that matches any in the input String [] netList.
     * Returns null if there is no Solution with such identifier.
     * Returns all networks if netList is null or empty.
     */
    // Do we want this implemented for all JasiReading types? aww
    public Collection getBySolutionNets(long id, String[] netList) {
        setJoinType(Type_AssocAmO);
        Solution aSol = Solution.create().getById(id);
        if (aSol == null) return null;
        // must lookup prefor to use AssocAmO
        long orid = ((SolutionTN)aSol).getOridValue();
        StringBuffer sb = new StringBuffer(1024);
        //sb.append(getSqlPrefixByEvent());
        //sb.append(" and (AssocAmO.orid = ").append(orid).append(")");
        //sb.append(makeNetClause(netList));
        //sb.append(sqlOrderSuffix);
        sb.append(AmplitudeTN.psSqlPrefixByOrid);
        sb.append(makeNetClause(netList));
        sb.append(AmplitudeTN.psSqlOrderSuffix);

        //ArrayList aList = (ArrayList)((JasiDbReader)getDataReader()).getBySQL(sb.toString());
        //ArrayList aList = (ArrayList) ((JasiDbReader)getDataReader()).getBySQL(DataSource.getConnection(), sb.toString(), this);

        ArrayList aList = null;
        PreparedStatement psByOrid = null;
        try {
            Connection conn = DataSource.getConnection();
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(sb.toString());
            psByOrid = conn.prepareStatement(sb.toString());
            psByOrid.setLong(1, orid);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByOrid, this, true);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }

        if (aList == null) return aList;
        int dbCount  = aList.size();
        //System.out.println("getBySolution: amps found ="+dbCount);
        if (dbCount < 1) return aList; // no data
        // option to associate elements of list with the input argument
        // otherwise returned list elements are orphaned from context
        //associateList(aSol, aList);
        aSol.associate(aList);

        return aList;
    }
    /**
     * Returns a list of all Amplitudes associated in the default DataSource
     * with a Solution whose identifier matches the input id
     * Return null if no Solution is found matching the input id.
     * Amplitudes are associated with non-null Solution corresponding to input.
     * @see #getBySolution(long, String[])
     *
     */
    public Collection getBySolution(long id) {
        return getBySolution(id, null);
    }
    /**
     * Returns a list of all Amplitudes associated in the default DataSource
     * with a Solution whose identifier matches the input id
     * that have ampType qualifiers matching any of those of the inputType list.
     * Return null if no Solution is found matching the input id.
     * Returns all types if input typeList is null or empty.
    */
    public Collection getBySolution(long id, String[] typeList) {
        // must first  retreive the Solution, then look up prefor for this id
        Solution aSol = (Solution) Solution.create().getById(id);
        // check for no such solution in DataSource
        return (aSol == null) ? null : getBySolution(aSol, typeList, true); // specified types, associate
    }

    /**
     * Returns list of Amplitudes from the default DataSource whose times fall
     * within specified input time window.
     * Times are seconds in UNIX epoch time.
     * Returns null if no amplitudes are found. <br>
     * (NOTE: in TriNet this gets amps from ALL sources (RT1, RT2)
     * so there will be duplicate readings.)
     * @see #getByTime(Connection, double, double)
     * */
     // This is quite SLOW, need dbase indexing?
     public Collection getByTime(double start, double end) {
         return getByTime(DataSource.getConnection(), start, end);
     }

    /**
     * Returns list of Amplitudes from the specified data connection
     * whose times fall within specified input time window.
     * @see #getByTime(double, double)
     */
    public Collection getByTime(Connection conn, double start, double end) {
        //Kludge for NCEDC request with leap seconds added // removed 2008/01/24 for leap time -aww
        //start = DataSource.nominalToDbTimeBase(start); //end  = DataSource.nominalToDbTimeBase(end); //end of patch - aww
        setJoinType(Type_AssocNone);
        //StringBuffer sb = new StringBuffer(1024);
        //sb.append(getSqlPrefixGeneric());
        //sb.append(" where ");
        //sb.append(getTableName());
        //sb.append(".DATETIME between ");
        //sb.append("TRUETIME.putEpoch(").append(start).append(", 'UTC')");
        //sb.append(" AND TRUETIME.putEpoch(").append(end).append(", 'UTC')");
        //sb.append(" order by Datetime");
        //return ((JasiDbReader)getDataReader()).getBySQL(conn, sb.toString()); // replaced by below:
        //return ((JasiDbReader)getDataReader()).getBySQL(conn, sb.toString(), this);
        
        ArrayList aList = null;
        PreparedStatement psByTime = null;
        try {
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(AmplitudeTN.psSqlPrefixByTime);
            psByTime = conn.prepareStatement(AmplitudeTN.psSqlPrefixByTime);
            psByTime.setDouble(1, start);
            psByTime.setDouble(2, end);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByTime, this, true);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }

        return aList;
    }

    protected Amplitude getByAmpid(long id) {
        setJoinType(Type_AssocNone);
        //StringBuffer sb = new StringBuffer(1024);
        //sb.append(getSqlPrefixGeneric()).append(" where ");
        //sb.append(getTableName()).append(".ampid = ").append(id);
        //ArrayList aList = (ArrayList)((JasiDbReader)getDataReader()).getBySQL(sb.toString()); // replaced by below:
        //ArrayList aList = (ArrayList)((JasiDbReader)getDataReader()).getBySQL(DataSource.getConnection(), sb.toString(), this);
        ArrayList aList = null;
        PreparedStatement psByAmpid = null;
        try {
            Connection conn = DataSource.getConnection();
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(AmplitudeTN.psSqlPrefixByAmpid);
            psByAmpid = conn.prepareStatement(AmplitudeTN.psSqlPrefixByAmpid);
            psByAmpid.setLong(1, id);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByAmpid, this, true);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        return (aList == null || aList.isEmpty()) ? (Amplitude) null : (Amplitude) aList.get(0);
    }

    /**
     * Return an Amplitude from the default DataSource whose identifier matches
     * the input id number.
     * Returned Amplitude has null Solution and Magnitude association assigments.
     * Returns null if there is no match.
     * */
    public JasiCommitableIF getById(long id) {
        return getByAmpid(id);
    }
    public JasiCommitableIF getById(Object id) {
        if (id instanceof Number)
            return  getById(((Number)id).longValue());
        else if (id instanceof DataNumber)
            return getById(((DataNumber)id).longValue());
        else return null;
    }

    public Collection getByAmpSet(long evid, long ampsetid) {
        // Only enable if both RT and PostProc code all use the assocevampset
        setJoinType(Type_AssocNone);
        StringBuffer sb = new StringBuffer(1024);
        sb.append("SELECT t.* FROM ( SELECT a.AMPID,TRUETIME.getEpoch(a.DATETIME,'UTC'),a.STA,a.NET,a.AUTH,a.SUBSOURCE,a.CHANNEL,a.CHANNELSRC,"); 
        sb.append("a.SEEDCHAN,a.LOCATION,a.AMPLITUDE,a.AMPTYPE,a.UNITS,a.AMPMEAS,a.PER,a.SNR,a.QUALITY,a.RFLAG,a.CFLAG,");
        sb.append("TRUETIME.getEpoch(a.WSTART,'UTC'),a.DURATION,");
        sb.append("ROW_NUMBER() OVER (PARTITION BY a.net,a.sta,a.seedchan,a.location,a.amptype ORDER BY a.lddate DESC) rnum");
        sb.append(" FROM amp a, ampset s, assocevampset ss WHERE");
        sb.append(" (a.ampid=s.ampid) AND (s.ampsetid=ss.ampsetid)");
        sb.append(" AND (ss.ampsetid=?) AND (ss.evid=?)"); 
        sb.append(") t WHERE t.rnum=1");
        sb.append(" ORDER BY t.net,t.sta,t.seedchan,t.location,t.amptype");;

        ArrayList aList = null;
        PreparedStatement psByAmpSet = null;
        try {
            Connection conn = DataSource.getConnection();
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(sb.toString());
            psByAmpSet = conn.prepareStatement(sb.toString());
            psByAmpSet.setLong(1, ampsetid);
            psByAmpSet.setLong(2, evid);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByAmpSet, this, true);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        return aList;
    }
    public Collection getByValidAmpSet(long evid, String ampsettype, String subsrcExpr) {
        // Only enable if both RT and PostProc code all use the assocevampset
        setJoinType(Type_AssocNone);
        StringBuffer sb = new StringBuffer(1024);
        sb.append("SELECT t.* FROM ( SELECT a.AMPID,TRUETIME.getEpoch(a.DATETIME,'UTC'),a.STA,a.NET,a.AUTH,a.SUBSOURCE,a.CHANNEL,a.CHANNELSRC,"); 
        sb.append("a.SEEDCHAN,a.LOCATION,a.AMPLITUDE,a.AMPTYPE,a.UNITS,a.AMPMEAS,a.PER,a.SNR,a.QUALITY,a.RFLAG,a.CFLAG,");
        sb.append("TRUETIME.getEpoch(a.WSTART,'UTC'),a.DURATION,");
        sb.append("ROW_NUMBER() OVER (PARTITION BY a.net,a.sta,a.seedchan,a.location,a.amptype ORDER BY a.lddate DESC) rnum");
        sb.append(" FROM amp a, ampset s, assocevampset ss WHERE");
        sb.append(" (a.ampid=s.ampid) AND (s.ampsetid=ss.ampsetid)");
        sb.append(" AND (ss.ampsettype=?) AND (ss.isvalid=?) AND (ss.evid=?)");
        if (subsrcExpr != null && subsrcExpr.length() > 0) sb.append(" AND (ss.subsource LIKE ?)");
        sb.append(") t WHERE t.rnum=1");
        sb.append(" ORDER BY t.net,t.sta,t.seedchan,t.location,t.amptype");;

        ArrayList aList = null;
        PreparedStatement psByAmpSet = null;
        try {
            Connection conn = DataSource.getConnection();
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(sb.toString());
            psByAmpSet = conn.prepareStatement(sb.toString());
            psByAmpSet.setString(1, ampsettype);
            psByAmpSet.setInt(2, 1);
            psByAmpSet.setLong(3, evid);
            if (subsrcExpr != null && subsrcExpr.length() > 0) psByAmpSet.setString(4, subsrcExpr);

            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByAmpSet, this, true);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        return aList;
    }

    /** Extract all Amplitudes for these Nets in the list associated with
    * the original (RT) orid for input Solution <i>evid</i>. <p>
    * AssocAmO rows are written when an new origin is inserted.
    * If the new origin is associated with newly generated Waveform amps,
    * the replaced RT system amps are not associated with this new origin.
    */ 
    public Collection getByEarliestLoadedOridForSolutionNets(long evid, String[] netList) {
      setJoinType(Type_AssocAmO);
      StringBuffer sb = new StringBuffer(1024);
      //sb.append(getSqlPrefixByEvent());
      //sb.append(" and (AssocAmO.orid = ");
      //sb.append(" (Select orid from Origin where evid = ").append(evid);
      //sb.append(" and lddate = (select min(lddate) from origin where evid = ").append(evid);
      //sb.append(")))");
      //sb.append(makeNetClause(netList));
      //sb.append(" order by net, sta ");
      //return ((JasiDbReader)getDataReader()).getBySQL(sb.toString()); // replaced by below:
      //return ((JasiDbReader)getDataReader()).getBySQL(DataSource.getConnection(), sb.toString(), this);
      sb.append(AmplitudeTN.psSqlPrefix); 
      sb.append(",o.DELTA,o.SEAZ FROM AMP amp, ASSOCAMO o WHERE (amp.ampid=o.ampid) AND");
      sb.append(" (o.orid=(SELECT ORID FROM ORIGIN WHERE EVID=? AND LDDATE=(SELECT MIN(LDDATE) FROM ORIGIN WHERE EVID=?)))");
      sb.append(makeNetClause(netList));
      sb.append(" ORDER BY NET,STA");
      ArrayList aList = null;
      PreparedStatement psByEvid = null;
      try {
          Connection conn = DataSource.getConnection();
          if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(sb.toString());
          psByEvid = conn.prepareStatement(sb.toString());
          psByEvid.setLong(1, evid);
          psByEvid.setLong(2, evid);
          aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByEvid, this, true);
      }
      catch (SQLException ex) {
          ex.printStackTrace();
      }
      return aList;
    }

    public AmpList getDuplicate(Amplitude amp) {
        return getDuplicate(DataSource.getConnection(), amp);
    }

    /**
     * Returns Amplitudes from the data source that are likely to be duplicates
     * of the given amp. That is, those that are associated with the same evid,
     * are from the same channel and have the same amp type.
     * Returns null if no amplitudes are found.
     */
     public AmpList getDuplicate(Connection conn, Amplitude amp) {
       if (!amp.isAssociated()) return null;
       return getDuplicate(conn, amp.getAssociatedSolution().getId().longValue(),
                           amp.getChannelObj(), amp.type);
     }
     /**
      * Returns Amplitudes from the data source that are likely to be duplicates
      * of an amp with these attributes: associated with the same evid,
      * have the same channel name and are the same amp type.
      * Returns null if no amplitudes are found. Input ampType must be a valid value.
      * @see AmpType
      */
    public AmpList getDuplicate(Connection conn, long evid, Channelable chan, int ampType) {

        if ( !AmpType.isLegal(ampType)) return null;

        setJoinType(Type_AssocNone); // Why Type_AssocNone and not parse the Type_AssocAmO? -aww

        StringBuffer sb = new StringBuffer(1024);
        //sb.append(getSqlPrefixByEvent(evid)); 
        //sb.append(" and net like ").append(StringSQL.valueOf(chan.getChannelObj().getNet()));
        //sb.append(" and sta like ").append(StringSQL.valueOf(chan.getChannelObj().getSta()));
        //sb.append(" and seedchan like ").append(StringSQL.valueOf(chan.getChannelObj().getSeedchan()));
        //sb.append(" and amptype like ").append(StringSQL.valueOf(AmpType.getString(ampType)));
        //sb.append(" order by Datetime");
        //ArrayList aList = (ArrayList)((JasiDbReader)getDataReader()).getBySQL(sb.toString()); // replaced by below:
        //ArrayList aList = (ArrayList)((JasiDbReader)getDataReader()).getBySQL(conn, sb.toString(), this);
        sb.append(AmplitudeTN.psSqlPrefixByEvid); 
        sb.append(" AND amp.NET=? AND amp.STA=? AND amp.SEEDCHAN=? AND amp.LOCATION=? AND amp.AMPTYPE=?");
        sb.append(" ORDER BY amp.DATETIME, amp.LDDATE DESC");

        ArrayList aList = null;
        PreparedStatement psByEvid = null;
        Channel chn = chan.getChannelObj();
        try {
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(sb.toString());
            psByEvid = conn.prepareStatement(sb.toString());
            psByEvid.setLong(1, evid);
            psByEvid.setString(2, chn.getNet());
            psByEvid.setString(3, chn.getSta());
            psByEvid.setString(4, chn.getSeedchan());
            psByEvid.setString(5, chn.getLocation());
            psByEvid.setString(6, AmpType.getString(ampType));
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByEvid, this, true);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }

        if (aList == null) return null;

        int dbCount  = aList.size();
        //System.out.println("getDuplicate: amps found ="+dbCount);
        if (dbCount < 1) return null; // no data

        AmpList ampList = AmpList.create();
        ampList.fastAddAll(aList);
        return ampList;
    }

    public JasiObject parseData(Object rsdb) {
        return parseResultSetDb((ResultSetDb)rsdb);
    }
    public JasiObject parseResultSetDb(ResultSetDb rsdb) {
        return parseResultSet(rsdb);
    }
    /**
     * Parse a resultset row that contains a Amplitude with AssocAmO or AssocAmM join
     */
    protected Amplitude parseResultSet(ResultSetDb rsdb) {
        //return parseResultSet(rsdb, AmplitudeTN.tableObj, getJoinType(), getClass());
        return parseResultSet(rsdb, getJoinType(), getClass());
    }

    /*
    protected static Amplitude parseResultSet(ResultSetDb rsdb, DataTableRow aRow, int joinType, Class myClass) {

        int offset = 0;

        // get jdbc objects from the result set
        DataTableRow tableObj = (DataTableRow) aRow.parseOneRow(rsdb, offset);


        // build up the Amplitude from the jdbc objects
        AmplitudeTN ampNew = null;
        try {
             ampNew = (AmplitudeTN) myClass.newInstance();
        }
        catch (Exception ex) {
          ex.printStackTrace();
          return null;
        }
        // allow changes to this DataTableRow. Need to set this because we are
        // not using our own rather than org.trinet.jdbc parsing methods
        if (DataSource.isWriteBackEnabled()) tableObj.setMutable(true).setMutableAllValues(true); // 07/07/2004 -aww
        ampNew.parseTableRow(tableObj); // parse from generic type

        offset += TableRowGenericAmp.MAX_FIELDS;

        // Parse depends on whether you did joint with AssocAmO or AssocAmM table.
        DataTableRow assoc = null;
        if (joinType == Type_AssocAmO) {
            assoc = new AssocAmO().parseOneRow(rsdb, offset);
            if (DataSource.isWriteBackEnabled()) assoc.setMutable(true).setMutableAllValues(true); // 07/07/2004 -aww
            ampNew.parseAssocAmO((AssocAmO) assoc);

        } else if (joinType == Type_AssocAmM) {
            assoc = new AssocAmM().parseOneRow(rsdb, offset);
            if (DataSource.isWriteBackEnabled()) assoc.setMutable(true).setMutableAllValues(true); // 07/07/2004 -aww
            ampNew.parseAssocAmM((AssocAmM) assoc);
        }

        // remember that we got this from the dbase
        ampNew.setUpdate(false);
        return (Amplitude) ampNew;
    }
    */

    //
    protected static Amplitude parseResultSet(ResultSetDb rsdb, int joinType, Class myClass) {

        // build up the Amplitude from the jdbc objects
        AmplitudeTN ampNew =  null;

        try {
             ampNew = (AmplitudeTN) myClass.newInstance();
        }
        catch (Exception ex) {
          ex.printStackTrace();
          return null;
        }

        Channel chan = ampNew.getChannelObj();
        ResultSet rs = rsdb.getResultSet();

        //
        // Parse query resultset columns
        //
        try {
            int offset = 1;
            //ampid
            ampNew.ampid.setValue(rs.getLong(offset++));

            //datetime
            double dd = rs.getDouble(offset++);
            if (!rs.wasNull()) ampNew.datetime.setValue(dd);

            //sta
            chan.setSta(rs.getString(offset++));
            //net
            chan.setNet(rs.getString(offset++));

            //auth
            ampNew.authority.setValue(rs.getString(offset++));
            //subsource
            ampNew.source.setValue(rs.getString(offset++));

            //channel
            chan.setChannel(rs.getString(offset++));
            //channelsrc
            chan.setChannelsrc(rs.getString(offset++));
            //seedchan
            chan.setSeedchan(rs.getString(offset++));
            //location
            chan.setLocation(rs.getString(offset++));

            //amplitude
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) ampNew.value.setValue(dd);

            //amptype
            String ampType = rs.getString(offset++);
            if (ampType != null) ampNew.type = AmpType.getInt(ampType);

            //units
            String unitstr = rs.getString(offset++);
            ampNew.units = Units.getInt(unitstr);

            //ampmeas
            String half = rs.getString(offset++);
            if (half != null && half.equals("0")) ampNew.halfAmp=false;

            //per
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) ampNew.period.setValue(dd);

            //snr
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) ampNew.snr.setValue(dd);

            //quality
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) ampNew.quality.setValue(dd);
            //else ampNew.quality.setNull(true);

            //rflag
            ampNew.processingState.setValue(rs.getString(offset++));

            //cflag
            String clip = rs.getString(offset++);
            if (clip == null) {
                System.err.println("AmplitudeTN WARNING DB row CFLAG value NULL, defaulting to ON_SCALE: " + chan.toDelimitedSeedNameString("."));
                ampNew.clipped = ON_SCALE;
            }
            else if (clip.equalsIgnoreCase("OS")) {
                ampNew.clipped = ON_SCALE;
            }
            else if (clip.equalsIgnoreCase("BN")) {
                ampNew.clipped = BELOW_NOISE;
            }
            else if (clip.equalsIgnoreCase("CL")) {
                ampNew.clipped = CLIPPED;
            }

            //windowstart
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) ampNew.windowStart.setValue(dd);

            //duration
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) ampNew.windowDuration.setValue(dd);

            //ampNew.phaseType = (DataString) rsdb.getDataObject(offset++);
            //ampNew.uncertainty = (DataDouble) rsdb.getDataObject(offset++);
            //this.timeStamp = (DataDate) rsdb.getDataObject(offset++);
  
            // Parse depends on whether you did joint with AssocAmO or AssocAmM table.
            if (joinType == Type_AssocAmO) {
              //o.ORID,o.DELTA,o.SEAZ 
              //orid.setValue(rs.getLong(offset++)); // removed from query, instance orid data member removed
              dd = rs.getDouble(offset++);
              if (!rs.wasNull()) ampNew.setHorizontalDistance(dd);
              dd = rs.getDouble(offset++);
              if (!rs.wasNull()) ampNew.setAzimuth(dd);
            } else if (joinType == Type_AssocAmM) {
              //m.magid
              //ampNew.magid.setValue(rs.getLong(offset++)); // magid not used, instead it refs the id of associated Magnitude
              //

              //m.WEIGHT
              dd = rs.getDouble(offset++);
              if (!rs.wasNull()) ampNew.channelMag.weight.setValue(dd);

              //m.IN_WGT
              dd = rs.getDouble(offset++);
              if (!rs.wasNull()) {
                  ampNew.channelMag.inWgt.setValue(dd);
                  if (ampNew.channelMag.inWgt.isValid()) { // handle "rejected" amp read from db 
                      ampNew.setReject(ampNew.channelMag.inWgt.doubleValue() == 0.);
                  }
              }
              // Some RT Me mags have null in_wgt and weight>0.
              else if (ampNew.channelMag.weight.doubleValue() > 0.) {
                  ampNew.channelMag.inWgt.setValue(1.);
              }

              //m.MAG
              dd = rs.getDouble(offset++);
              if (!rs.wasNull()) ampNew.channelMag.value.setValue(dd);

              //m.MAGRES
              dd = rs.getDouble(offset++);
              if (!rs.wasNull()) ampNew.channelMag.residual.setValue(dd);

              //m.MAGCORR
              dd = rs.getDouble(offset++);
              if (!rs.wasNull()) ampNew.channelMag.correction.setValue(dd);

            }
            ampNew.setUpdate(false);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }

        // remember that we got this from the dbase
        return (Amplitude) ampNew;
    }
    //

    /**
     * Copy DataObjects out of a jdbc.Amp object into this Amplitude.
     */

    protected void parseTableRow(DataTableRow row) {
      Channel cha = ChannelTN.parseChannelFromDataTableRow(row, DataTableRowUtil.AUTHSUBSRC);
      // call setChannel to synch with master channel list
      //setChannelObj(cha); // aww 04/05/2005
      this.chan = cha; // aww 04/05/2005
      authority = (DataString) row.getDataObject(TableRowGenericAmp.AUTH);
      source = (DataString) row.getDataObject(TableRowGenericAmp.SUBSOURCE);
      ampid = (DataLong) row.getDataObject(TableRowGenericAmp.AMPID);
      datetime = (DataDouble) row.getDataObject(TableRowGenericAmp.DATETIME);
      //Kludge for NCEDC leap corrected data, to convert to nominal time -aww
      //DataSource.dbTimeBaseToNominal(datetime); // removed for leap 2008/01/24 -aww

      processingState = (DataString) row.getDataObject(TableRowGenericAmp.RFLAG);
      phaseType = (DataString) row.getDataObject(TableRowGenericAmp.IPHASE);
      value = (DataDouble) row.getDataObject(TableRowGenericAmp.AMPLITUDE);
      // Set AmpType value
      String ampType = row.getDataObject(TableRowGenericAmp.AMPTYPE).toString();
      type = AmpType.getInt(ampType);
      // if zero-to-peak = 1 , if peak-to-peak = 0
      String half = row.getDataObject(TableRowGenericAmp.AMPMEAS).toString();
      if (half.equals("0")) halfAmp=false;
      // Set units
      String unitstr = row.getDataObject(TableRowGenericAmp.UNITS).toString();
      units = Units.getInt(unitstr);
      uncertainty = (DataDouble) row.getDataObject(TableRowGenericAmp.ERAMP);
      // IGNORING 'FLAGAMP', 'TAU'
      period = (DataDouble) row.getDataObject(TableRowGenericAmp.PER);
      snr = (DataDouble) row.getDataObject(TableRowGenericAmp.SNR);
      quality = (DataDouble) row.getDataObject(TableRowGenericAmp.QUALITY);
      String clip = row.getDataObject(TableRowGenericAmp.CFLAG).toString();
      if (clip.equalsIgnoreCase("OS")) { clipped =  ON_SCALE; }   //on scale
      else if (clip.equalsIgnoreCase("BN")) { clipped =  BELOW_NOISE; } //below noise
      else if (clip.equalsIgnoreCase("CL")) { clipped =  CLIPPED; } //clipped
      windowStart = (DataDouble) row.getDataObject(TableRowGenericAmp.WSTART); // TableRowAmp... fix, use getEpoch(WSTART, 'UTC') -aww 2010/02/25
      windowDuration = (DataDouble) row.getDataObject(TableRowGenericAmp.DURATION);
      // save TableRowGenericAmp.LDDATE in timeStamp? -aww
      this.timeStamp = (DataDate) row.getDataObject(TableRowGenericAmp.LDDATE); // date of first commit?
      return;
    }

    /**
     * Copy DataObjects out of a jdbc.AssocAmO object into a Amplitude.
     */
    protected void parseAssocAmO(AssocAmO assoc) {
      //orid = (DataLong) assoc.getDataObject(AssocAmO.ORID);
      DataDouble dd = (DataDouble) assoc.getDataObject(AssocAmO.DELTA);
      //if (dd.isValid()) setDistance(dd.doubleValue()); // not slant, should be horizontal - aww 06/11/2004
      if (dd.isValid()) setHorizontalDistance(dd.doubleValue()); // aww 06/11/2004
      dd = (DataDouble) assoc.getDataObject(AssocAmO.SEAZ);
      if (dd.isValid()) setAzimuth(dd.doubleValue());
      return;
    }

    /**
     * Copy DataObjects out of a jdbc.AssocAmO object into a Amplitude.
     */
    protected void parseAssocAmM(AssocAmM assoc) {
      channelMag.value      = (DataDouble) assoc.getDataObject(AssocAmM.MAG);
      channelMag.residual   = (DataDouble) assoc.getDataObject(AssocAmM.MAGRES);
      //channelMag.importance = (DataDouble) assoc.getDataObject(AssocAmM.IMPORTANCE);
      channelMag.correction = (DataDouble) assoc.getDataObject(AssocAmM.MAGCORR);
      channelMag.weight     = (DataDouble) assoc.getDataObject(AssocAmM.WEIGHT);
      channelMag.inWgt      = (DataDouble) assoc.getDataObject(AssocAmM.IN_WGT); //aww 6/04

      // Below handles "rejected" coda read from db -aww 2009/06/12
      if (channelMag.inWgt.isValid()) {
          setReject(channelMag.inWgt.doubleValue() == 0.);
      }
      // DataBoolean db = (DataBoolean) assoc.getDataObject(AssocAmM.IN_WGT_USE);
      // if (db.isValid()) reject = ! db.booleanValue(); // aww 06/22/2006 not implemented yet

      return;
    }

    /** Create a Amp table row of the proper type. */
    protected StnChlTableRow createTableRow(long seq) {
        Amp ampRow = null;
        try {
            ampRow = (Amp) getTableObj().getClass().newInstance();
            ampRow.setValue(TableRowGenericAmp.AMPID, seq);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return (StnChlTableRow) ampRow;
    }

    /*
    // If your allowing setTableObj then how can you assume its Amp instance
    // it might be a subclass like UnassocAmp? aww
    protected StnChlTableRow createTableRow(long seq) {
        return (StnChlTableRow) new Amp(seq);
    }
    */

    /**
     * Copy contents of this amplitude into a jdbc.Amp row object
     */
    protected StnChlTableRow toTableRow() {
        // OVERRIDE IN SUBCLASS IF UnassocAmp TABLE STRUCTURE CHANGES!
        // Always make a new row, get unique key #
//        long newId = SeqIds.getNextSeq(DataSource.getConnection(), "AMPSEQ");
        long newId = SeqIds.getNextSeq(DataSource.getConnection(),
                        TableRowGenericAmp.SEQUENCE_NAME);
        ampid.setValue(newId);

        StnChlTableRow aRow = createTableRow(newId);
        aRow.setUpdate(true); // set flag to enable processing

        setRowChannelId(aRow, getChannelObj());

        //Kludge for NCEDC leap corrected data, to convert to leap time -aww
        //DataDouble dt = (DataDouble) datetime.clone();
        //DataSource.nominalToDbTimeBase(dt);
        //aRow.setValue(TableRowGenericAmp.DATETIME, dt); // removed for leap 2008/01/24 -aww
        // end of leap patch aww
        if (datetime.isValid()) aRow.setValue(TableRowGenericAmp.DATETIME, datetime);

        // "rectify" - peak amp must be > 0.0
        aRow.setValue(TableRowGenericAmp.AMPLITUDE, Math.abs(value.doubleValue()));
        aRow.setValue(TableRowGenericAmp.AMPTYPE, AmpType.getString(type));

        // if zero-to-peak = 1 , if peak-to-peak = 0
        if (halfAmp) {
            aRow.setValue(TableRowGenericAmp.AMPMEAS, ZERO_TO_PEAK);
        } else {
            aRow.setValue(TableRowGenericAmp.AMPMEAS, PEAK_TO_PEAK);
        }

        aRow.setValue(TableRowGenericAmp.UNITS, Units.getString(units));

        //if (authority.isValid())       aRow.setValue(TableRowGenericAmp.AUTH, authority); // aww 01/11/2005
        aRow.setValue(TableRowGenericAmp.AUTH, getClosestAuthorityString()); // aww 01/11/2005

        // the following can be null
        if (processingState.isValid()) aRow.setValue(TableRowGenericAmp.RFLAG, processingState);
        if (source.isValid())          aRow.setValue(TableRowGenericAmp.SUBSOURCE, source);
        if (phaseType.isValid())       aRow.setValue(TableRowGenericAmp.IPHASE, phaseType);
        if (uncertainty.isValid())     aRow.setValue(TableRowGenericAmp.ERAMP, uncertainty);
        if (period.isValid())          aRow.setValue(TableRowGenericAmp.PER, period);
        if (snr.isValid())             aRow.setValue(TableRowGenericAmp.SNR, snr);
        if (quality.isValid())         aRow.setValue(TableRowGenericAmp.QUALITY, quality);

        // IGNORING 'FLAGAMP', 'TAU'
        if (clipped == ON_SCALE)         aRow.setValue(TableRowGenericAmp.CFLAG, "OS");
        else if (clipped == BELOW_NOISE) aRow.setValue(TableRowGenericAmp.CFLAG, "BN");
        else if (clipped == CLIPPED)     aRow.setValue(TableRowGenericAmp.CFLAG, "CL");

        if (windowStart.isValid()) {
            // WSTART not a DATETIME column, Amp class overrides DataTableRow isColumnEpochSecsUTC(columnName) -aww 2010/02/26
            aRow.setValue(TableRowGenericAmp.WSTART, windowStart);
        }
        if (windowDuration.isValid()) aRow.setValue(TableRowGenericAmp.DURATION, windowDuration);

        if (debug) {
            System.out.println("DEBUG AmplitudeTN toTableRow(): "+aRow.toString());
        }

        return aRow;
    }

    /** Put the channel name attributes in the Amp DataTableRow */
    protected void setRowChannelId(StnChlTableRow aRow, Channel chan) {
        //
        //DataTableRowUtil.setRowAuthChannelId(aRow, chan); // works but below is quicker
        //

        ChannelName cn = chan.getChannelName();
        // by default all fields are "" rather then null strings: see ChannelName
        aRow.setValue(TableRowGenericAmp.STA, cn.getSta()); // this attribute can't be null
        String str = cn.getNet();
        if (str.length() > 0) aRow.setValue(TableRowGenericAmp.NET, str);
        str = cn.getAuth();
        if (str.length() > 0) aRow.setValue(TableRowGenericAmp.AUTH, str);
        str = cn.getSubsource();
        if (str.length() > 0) aRow.setValue(TableRowGenericAmp.SUBSOURCE, str);
        str = cn.getChannel();
        if (str.length() > 0) aRow.setValue(TableRowGenericAmp.CHANNEL, str);
        str = cn.getChannelsrc();
        if (str.length() > 0) aRow.setValue(TableRowGenericAmp.CHANNELSRC, str);
        str = cn.getSeedchan();
        if (str.length() > 0) aRow.setValue(TableRowGenericAmp.SEEDCHAN, str);
        str = cn.getLocation();
        if (str.length() > 0) aRow.setValue(TableRowGenericAmp.LOCATION, str);
    }

    /**
     * Create AsocAmM using current attribute values of this Amplitude.
     */
    private AssocAmM toAssocAmMRow() {
        // keys are ampid and magid
        AssocAmM assocRow = new AssocAmM();
        assocRow.setUpdate(true); // set flag to enable processing

        // For assoc row using auth and subsource from reading is confusing
        // if the assoc row is supposed to refer to its "creator" then should 
        // use auth and subsource of the associated magnitude - aww 01/11/2005
        MagnitudeTN magTN = (MagnitudeTN) magnitude;
        // Constrained "NOT NULL" in the schema, hypothetically "" or NaN could be a legit value string 
        assocRow.setValue(AssocAmM.MAGID, magTN.magid);
        assocRow.setValue(AssocAmM.AMPID, ampid);
        //String str = magTN.getAuthority(); // removed -aww 2011/08/17
        String str = getClosestAuthorityString(); // added -aww 2011/08/17
        // force null check here
        if (!NullValueDb.isNull(str)) assocRow.setValue(AssocAmM.AUTH, str);

        // ALLOWED TO BE NULL
        //if (source.isValid()) assocRow.setValue(AssocAmM.SUBSOURCE, source); // removed 01/11/2005 aww
        str = magTN.getSource();
        if (!NullValueDb.isNull(str)) assocRow.setValue(AssocAmM.SUBSOURCE, str);

        //if (quality.isValid())             assocRow.setValue(AssocAmM.IN_WGT, quality); // aww 06/04
        if (channelMag.inWgt.isValid())      assocRow.setValue(AssocAmM.IN_WGT, channelMag.inWgt); // aww 06/04
        if (channelMag.value.isValid())      assocRow.setValue(AssocAmM.MAG, channelMag.value);
        if (channelMag.residual.isValid())   assocRow.setValue(AssocAmM.MAGRES, channelMag.residual);
//      if (channelMag.importance.isValid()) assocRow.setValue(AssocAmM.IMPORTANCE, channelMag.importance);
        if (channelMag.correction.isValid()) assocRow.setValue(AssocAmM.MAGCORR, channelMag.correction);
        if (channelMag.weight.isValid())     assocRow.setValue(AssocAmM.WEIGHT, channelMag.weight);
        if (processingState.isValid())       assocRow.setValue(AssocAmM.RFLAG, processingState);

        //assocRow.setValue(AssocAmM.IN_WGT_USE,  reject); // aww 06/22/2006 not implemented yet

        if (debug) {
          System.out.println("DEBUG AmplitudeTN toAssocAmMRow(): "+assocRow.toString());
        }

        return assocRow;
    }

    /**
     * Create AssocAmO object using current attribute values of this Amplitude.
     */
    private AssocAmO toAssocAmORow() {

        AssocAmO assocRow = new AssocAmO(); // keys are ampid and magid
        assocRow.setUpdate(true); // set flag to enable processing

        // For assoc row using auth and subsource from reading is confusing
        // if the assoc row is supposed to refer to its "creator" then should 
        // use auth and subsource of the associated origin - aww 01/11/2005
        SolutionTN solTN = (SolutionTN) sol;
        long assocOrid = solTN.getOridValue();
        //setOrid(assocOrid);

        // Constrained "NOT NULL" in the schema, hypothetically "" or NaN could be a legit value string 
        assocRow.setValue(AssocAmO.ORID,  assocOrid);
        assocRow.setValue(AssocAmO.AMPID, ampid);
        //String str = solTN.getAuthority(); // removed -aww 2011/08/17
        String str = getClosestAuthorityString(); // added  -aww 2011/08/17
        // force null check here
        if (!NullValueDb.isNull(str)) assocRow.setValue(AssocAmO.AUTH, str);

        // ALLOWED TO BE NULL
        //if (source.isValid()) assocRow.setValue(AssocAmO.SUBSOURCE, source); // aww 01/11/2005 removed
        str = solTN.getSource();
        if (!NullValueDb.isNull(str)) assocRow.setValue(AssocAmO.SUBSOURCE, str);

        //double x = getDistance(); // don't save slant, use horizontal - aww 06/11/2004
        double x = getHorizontalDistance(); // aww 06/11/2004
        if ( ! (x == Channel.NULL_DIST || Double.isNaN(x)) )
            assocRow.setValue(AssocAmO.DELTA, x);
        x = getAzimuth();
        if ( ! (x == Channel.NULL_AZIMUTH || Double.isNaN(x)) )
            assocRow.setValue(AssocAmO.SEAZ, x);
        if (processingState.isValid()) assocRow.setValue(AssocAmO.RFLAG, processingState);

        if (debug) {
          System.out.println("DEBUG AmplitudeTN toAssocAmORow(): "+assocRow.toString());
        }

        return assocRow;
    }

    /**
     * Commit changes or delete to underlying DataSource. The action taken will
     * depend on the state of the amplitude. It may or may not be originally
     * from the data source, it may be deleted, unassociated, modified, or
     * unchanged.  To write to the DataSource, the flag
     * DataSource.isWriteBackEnabled() must be true. This must be set with
     * DataSource.setWriteBackEnabled(true) BEFORE data is read.
     * */

    /*
    We will never really delete an amp row in the dbase because this would alter
    a mag/amp data-set created by someone else. That would be rude. We only write
    an NEW NetMag, AssocAmM rows connecting existing or new Amps to the NetMag.
    */
    public boolean commit() {
        if (!DataSource.isWriteBackEnabled()) return false;
        setDebug(JasiDatabasePropertyList.debugTN); 
        if (debug) {
            System.out.println("DEBUG AmplitudeTN commit() fromDB:"+fromDbase+" "+toNeatString());
            System.out.println(toString());
        }

        // Reading is deleted or not associated! So don't write new rows.
        // Note we NEVER actually delete a row, just don't write it out
        // nor make an association if it came from the database.
        if (isDeleted() || ! isAssignedToSol()) return true;

        boolean status = false; // commit status
        if (fromDbase) {  // "existing" (came from the dbase)
          if (getNeedsCommit() || hasChanged()) {   // force new row (sequence number) and association
            if (debug) System.out.println("(changed) INSERT and assoc");
            status = dbaseInsert() && dbaseAssociate(false); // insert && associate
          } else { // if no changes, just make new association
            if (debug) System.out.println("(unchanged) assoc only");
            status = dbaseAssociate(true);
            if (status) setUpdate(false); // mark as up-to-date
          }
        // virgin (not from dbase) <INSERT>
        } else {
          if (debug) System.out.println("(new) INSERT and assoc");
          status = dbaseInsert() && dbaseAssociate(false); // insert && associate
        }
        setNeedsCommit(! status); // hopefully false?
        return status;
    }

    // since protected can access across package boundaries by Solution and Magnitude
    public boolean isFromDataSource() {
       return fromDbase;
    }

    /**
     * NEVER delete an amp. Never remove association with another NetMag.  If
     * you don't like it just don't assoc it with your new NetMag.
    */
    protected boolean dbaseDelete() {
        return true;
    }

    protected boolean dbaseAssociate(boolean updateExisting) {
      // must have a valid origin association
      if (! dbaseAssociateWithOrigin(updateExisting) ) return false;  // abort here?
      return (isAssignedToMag()) ? dbaseAssociateWithMag(updateExisting) : true; // return true, associated mag not required
      // prior method implementation below, behavior was different 09/30/30
      // Mag association was required, but why do amps always have to associated with a Mag ? - aww:
      //    return dbaseAssociateWithOrigin() && dbaseAssociateWithMag();
    }

    /**
     * Make AssocAmO row for this Amp
     */
    protected boolean dbaseAssociateWithOrigin(boolean updateExisting) {
        if (! isAssignedToSol() ) return false; // unassociated amp
        // Check that 'orid' of associated event has been set
        if ( ((SolutionTN) sol).getOridValue() == 0l) {
          if (debug) System.out.println("DEBUG AmplitudeTN Error associated Solution has 0 orid " +
                 getChannelObj().getChannelId().toString()
              );
          return false; // added, don't allow "zero" orid in table, an UnAssocAmp type? -aww 11/09/2004
        }
        if (getAmpid() == 0l) {
          if (debug) System.out.println("DEBUG AmplitudeTN Error 0 ampid "+
                          getChannelObj().getChannelId().toString());
          return false;
        }

        // does row update instead of row insert if row checked and it exists
        return DataTableRowUtil.rowToDb(DataSource.getConnection(), toAssocAmORow(), updateExisting);
    }

    /**
     * Make AssocAmM row for this Amp
     */
    protected boolean dbaseAssociateWithMag(boolean updateExisting) {
        if (! isAssignedToMag() ) return false;

        // Should we check the mag subscript?
        // Magnitude mag = getAssociatedMag();
        // if (! mag.isAmpMag()) {
        //   throw new WrongMagTypeException("associated Magnitude not an Amp Mag "+mag.getTypeString());
        // }

        // check that 'magid' or associated magnitude has been set
        if (getMagidValue() == 0l) {
          if (debug) System.out.println("DEBUG AmplitudeTN Error associated Magnitude has 0 magid "+
                          getChannelObj().getChannelId().toString());
          return false; // added, don't allow "zero" magid in assoc table, an UnAssocAmp type? -aww 11/09/2004
        }
        if (getAmpid() == 0l) {
          if (debug) System.out.println("DEBUG AmplitudeTN Error 0 coid "+
                          getChannelObj().getChannelId().toString());
          return false;
        }

        // does row update instead of row insert if row checked and it exists
        return DataTableRowUtil.rowToDb(DataSource.getConnection(), toAssocAmMRow(), updateExisting);
    }

    /**
     * Insert a new row in the dbase.
     * This DOES NOT write the association rows for the origin and the netmag.
     * The Solution and Magnitude object's when they are committed create these
     * association rows.
     */
    protected boolean dbaseInsert() {
        // if (fromDbase) return false;  // but isn't a change worthy of update?
        boolean status = true;
        StnChlTableRow aRow = toTableRow();
        aRow.setProcessing(DataTableRowStates.INSERT);

        // write arrival row
        status = (aRow.insertRow(DataSource.getConnection()) > 0);
        if (status) {                    // successful insert
          //Note: Solution, Magnitude commit() invoke reading commit() which write new assoc - aww 09/03
          //status = dbaseAssociate(); // (removed see above note) commit() invokes it.
          // now its "from" the dbase
          setUpdate(false); // mark as up-to-date (fromDbase=true)
        }
        return status;
    }

    /**
    * Returns true if this instance was not initialized from database data.
    * or hasChangedAttributes() == true.
    */
    public boolean hasChanged() {
        return ( !fromDbase || hasChangedAttributes());
    }
    /** Returns true if certain data member values were modified after initialization.
     * (value, datetime)
     * */
    public boolean hasChangedAttributes() {
        return (
            value.isUpdate() ||
            datetime.isUpdate()
            //|| quality.isUpdate() // aww, this may effect results someday, weights?
        );
    }

    /**
     * Set the isUpdate() flag for all data dbase members the given boolean value.
     * */
    protected void setUpdate(boolean tf) {
        fromDbase = !tf;
        value.setUpdate(tf);
        datetime.setUpdate(tf);
        //quality.setUpdate(tf); // need this if used by summary calc method, like weight - aww
    }

    public boolean replace(Amplitude aAmp) {

        boolean status = super.replace(aAmp);


        if (aAmp instanceof AmplitudeTN) {
            AmplitudeTN uAmp = (AmplitudeTN) aAmp; 
            ampid.setValue(uAmp.ampid);
            fromDbase = uAmp.fromDbase;
            assocFromDbase = uAmp.assocFromDbase;
            joinType = uAmp.joinType;
        }
        else if (aAmp instanceof UnassocAmplitudeTN) {
            UnassocAmplitudeTN uAmp = (UnassocAmplitudeTN) aAmp; 
            ampid.setValue(uAmp.ampid);
            fromDbase = false;
            assocFromDbase = false;
            joinType = Type_AssocNone;
        }
        else {
            ampid.setNull(true);
            fromDbase = false;
            assocFromDbase = false;
            joinType = Type_AssocNone;
        }

        return status;
    }

    /** Does a no-op, returns false. Map input Amp data to this Amp. */
    public boolean copySolutionDependentData(Amplitude aAmp) {
        return false;
        /*
        if (isLikeChanType(aAmp)) { // no association check
          setChannelObj(aAmp.getChannelObj());
          return true;
        }
        else {
          return false;
        }
        */
    }

    /**Access to the JasiDbReader to allow generic SQL queries. */
    public JasiDataReaderIF getDataReader() {
        if (jasiDataReader == null) {
           //jasiDataReader = new JasiDbReader(this);
           jasiDataReader = new JasiDbReader();
           setDebug(JasiDatabasePropertyList.debugTN); 
        }
        return jasiDataReader;
    }

    // A matching row key exists in datasource but its actually data may differ from instance
    public boolean existsInDataSource() {
        JasiDbReader jdr = (JasiDbReader) getDataReader();
        int count = jdr.getCountBySQL(DataSource.getConnection(), "AMP", "AMPID", ampid.toString()); 
        return (count > 0);
    }

    // Clone the specific amplitude dependent data of this subtype.
    public Object clone() {
        AmplitudeTN ampTN = (AmplitudeTN) super.clone();
        //ampTN.orid        = (DataLong) this.orid.clone();
        //ampTN.magid       = (DataLong) this.magid.clone();
        ampTN.ampid       = (DataLong) this.ampid.clone();
        return ampTN;
    }
} // AmplitudeTN
