package org.trinet.jasi.TN;

import java.sql.*;
import java.util.*;
import java.io.*;

import org.trinet.jasi.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;

import org.trinet.util.*;
import org.trinet.util.gazetteer.*;

/**
 * This is a schema specific implementation of ChannelLocation that acts as the
 * of the site (latlonz) . <p>
 *
 * @See: org.trinet.jasi.ChannelName
 */

public class ChannelLocationTN extends ChannelLocation implements DbReadableJasiChannelObjectIF {

    private static org.trinet.jdbc.table.ChannelData chanDataRow = new org.trinet.jdbc.table.ChannelData();
    private final static String TN_TABLE_NAME = ChannelData.DB_TABLE_NAME;

    static protected JasiChannelDbReader jasiDataReader =  new JasiChannelDbReader();
    { 
      jasiDataReader.setFactoryClassName(getClass().getName());
      jasiDataReader.setDebug(JasiDatabasePropertyList.debugSQL);
    }

    public ChannelLocationTN() { }

    /**
     * Returns a Collection of objects from the default DataSource active at the current time
    */
    public Collection loadAll() {
        return loadAll(DataSource.getConnection(), (java.util.Date) null);
    }

    /**
     * Return Collection of objects that were valid on the input date.
     * Data is retrieved from the default DataSource Connection.
    */
    public Collection loadAll(java.util.Date date) {
        return loadAll(DataSource.getConnection(), date);
    }

    public Collection loadAll(java.util.Date date, String[] prefSeedchan) {
        return loadAll(date, null, prefSeedchan, null);
    }

    public Collection loadAll(java.util.Date date, String [] prefNet,
                    String [] prefSeedchan, String [] prefChannel) {
        return loadAll(date, prefNet, prefSeedchan, prefChannel, null);
    }
    public Collection loadAll(java.util.Date date, String [] prefNet,
                    String [] prefSeedchan, String [] prefChannel,
                    String [] prefLocations) {
        return jasiDataReader.loadAll(TN_TABLE_NAME, DataSource.getConnection(), date,
                        prefNet, prefSeedchan, prefChannel, prefLocations);
    }

    /**
    * Returns a Collection of objects created from data obtained from the specified connection
    * that are valid for the specified input date.
    * Note - individual lookups may be more time efficient, if one shot less then several hundred channels.
    */
    static public Collection loadAll(Connection conn, java.util.Date date) {
        return jasiDataReader.loadAll(conn, date);
    }

    /*
     * Finds the data in the DataSource matching the input's id 
     * and valid invocation (SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)). If data is found, a new ChannelLocation with 
     * that data is is returned, else the original input reference is returned.
     * Does require that the channel be currently "active", that is
     * ONDATE <= SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) <=OFFDATE.
    */
    public ChannelLocation lookUp(ChannelLocation chan) {
      return lookUp(chan, (java.util.Date) null) ;
    }
    public ChannelLocation lookUp(ChannelLocation chan, java.util.Date date) {
       ChannelLocation ch = (ChannelLocation) getByChannelId(chan.getChannelId(), date);
       return (ch == null) ? chan : ch;
    }

    /*
     * Finds the LATEST data (greatest ONDATE) in the DataSource matching
     * the input's id. If data is found, a new ChannelLocation populated with 
     * that data is is returned, else the original input reference is returned.
     * Does NOT require that the channel be currently "active", that is
     * ONDATE <= SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) <= OFFDATE.
     */
    public ChannelLocation getLatestByChannelId(ChannelIdIF id) {
        if (id == null || NullValueDb.isBlank(id.getSta()))
           throw new IllegalArgumentException("ChannelId sta name must be defined.");
        StringBuffer sb = new StringBuffer(128);
        sb.append(SQL_SELECT_PREFIX).append(" WHERE ");
        // note below doesn't discriminate differing channel,channelsrc
        //sb.append(DataTableRowUtil.toChannelLocationSQLWhereClause(TN_TABLE_NAME, id));
        // to discriminate differing channel,channelsrc:
        sb.append(DataTableRowUtil.toChannelSrcSQLWhereClause(TN_TABLE_NAME, id));
        sb.append(" ORDER BY "+TN_TABLE_NAME+".ondate DESC"); // most recent first
        Collection col = jasiDataReader.getBySQL(sb.toString());
        if (col == null || col.size() == 0) return null;
        Object [] obj = col.toArray();
        return (ChannelLocation) obj[0];
    }

    public ChannelDataIF getByChannelId(ChannelIdIF id, java.util.Date date) {
        return (ChannelDataIF) jasiDataReader.getByChannelId(TN_TABLE_NAME, id, date);
    }
    public Collection getByChannelId(ChannelIdIF id, DateRange dr) {
        return jasiDataReader.getByChannelId(TN_TABLE_NAME, id, dr);
    }


    /** Return a list of currently active channels that match the list of
    * component types given in the array of strings.
    * The component name is compared to the SEEDCHAN field in the NCDC
    * schema.<p>
    * SQL wildcards are allowed as follows:<br>
    * "%" match any number of characters<br>
    * "_" match one character.<p>
    * For example "H%" would match HHZ, HHN, HHE, HLZ, HLN & HLE. "H_" wouldn't
    * match any of these but "H__" would match them all. "_L_" would match
    * all components with "L" in the middle of three charcters (low gains).
    * The ANSI SQL wildcards [] and ^ are not supported by Oracle.
    */
    public ChannelDataMap getByComponent(String[] compTypes) {
        return getByComponent(compTypes, null);
    }

    /** Return a list of channels that were active on the given date and
    *  that match the list of component types given in the array of strings.
    * The component type is compared to the SEEDCHAN field in the NCDC
    * schema.<p>
    * SQL wildcards are allowed as follows:<br>
    * "%" match any number of characters<br>
    * "_" match one character.<p>
    *
    * For example "H%" would match HHZ, HHN, HHE, HLZ, HLN & HLE. "H_" wouldn't
    * match any of these but "H__" would match them all. "_L_" would match
    * all components with "L" in the middle of three charcters (low gains).
    * The ANSI SQL wildcards [] and ^ are not supported by Oracle.
    * Defaults to "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) = currently active" if input Data is null.
    * */
    public ChannelDataMap getByComponent(String[] compTypes, java.util.Date date) {
        Collection col = loadAll(date, compTypes);
        ChannelDataMap cdl = (col == null || col.size() == 0) ? new ChannelDataMap() : new ChannelDataMap(col);
        cdl.setDefaultDataFactory(new ChannelLocationTN());
        return cdl;
    }

    public ChannelDataMap readList(java.util.Date date) {
        Collection col = loadAll(date);
        ChannelDataMap cdl = (col == null || col.size() == 0) ? new ChannelDataMap() : new ChannelDataMap(col);
        cdl.setDefaultDataFactory(new ChannelLocationTN());
        return cdl;
    }

    /** Return count of all channels in data source at the given time.
     * Defaults to SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) (current time) if input is null.
     * */
    public int getCount(java.util.Date date) {
      return jasiDataReader.getCountBySQL(DataSource.getConnection(), toChannelSQLSelectPrefix(date));
    }
 
    private static ChannelLocationTN parseResultSet(ResultSetDb rsdb){
        int offset = 0;
        // parse the first part of the return that is just a Waveform table row
        ChannelLocationTN ch = parseChannelDataRow((ChannelData) chanDataRow.parseOneRow(rsdb, offset));

        // Now, pick up the "extra" columns specified by the SQL query.
        // See SelectJoinString for a list of fields
        offset += chanDataRow.MAX_FIELDS;
        return ch;
    }

    protected final static String SQL_SELECT_PREFIX =
      "SELECT " +ChannelData.QUALIFIED_COLUMN_NAMES+ " FROM " +TN_TABLE_NAME;

    // For all history
    public String toChannelSQLSelectPrefix() {
       return SQL_SELECT_PREFIX;
       //return toChannelSQLSelectPrefix((java.util.Date)null); // currently active
    }
    /** Return the SQL join string for channel data at the time described by
    * this SQL formatted date string.
    */
    public String toChannelSQLSelectPrefix(java.util.Date date) {
      return SQL_SELECT_PREFIX  + " WHERE " +
                 DataTableRowUtil.toDateConstraintSQLWhereClause(TN_TABLE_NAME, date) ;
    }

    private static ChannelLocationTN parseChannelDataRow(ChannelData cdRow) {
        ChannelLocationTN chan = new ChannelLocationTN();
        if (chan.channelId == null) chan.channelId = new ChannelName();
        chan.channelId =
            DataTableRowUtil.parseChannelSrcFromRow(cdRow, chan.channelId);

        DataDouble lat = (DataDouble) cdRow.getDataObject(ChannelData.LAT);
        DataDouble lon = (DataDouble) cdRow.getDataObject(ChannelData.LON);
        if (lat != null && lon != null) {
            chan.latlonz.setLat(lat.doubleValue());
            chan.latlonz.setLon(lon.doubleValue());
        }
        DataDouble z   = (DataDouble) cdRow.getDataObject(ChannelData.ELEV);        //km
        if (z != null) {
          // convert elevation to km, which is what LatLonZ uses.
          double km = z.doubleValue()/1000.0;
          //chan.latlonz.setZ(km); // aww 06/11/2004
          chan.latlonz.setZ(-km); // change sign, to depth-axis convention aww 06/11/2004
          chan.latlonz.setZUnits(GeoidalUnits.KILOMETERS);
        }

        DataDate on = (DataDate) cdRow.getDataObject(ChannelData.ONDATE);
        java.util.Date onDate =  ( on.isNull()) ? null : on.dateValue();
        DataDate off = (DataDate) cdRow.getDataObject(ChannelData.OFFDATE);
        java.util.Date offDate = (off.isNull()) ? null : off.dateValue();
        if (chan.dateRange == null) {
          chan.setDateRange(new DateRange(onDate, offDate));
        }
        else {
          chan.dateRange.setMin(onDate);
          chan.dateRange.setMax(offDate);
        }

        return chan;
    }

    public JasiObject parseData(Object rsdb) {
      return parseResultSetDb((ResultSetDb) rsdb);
    }
    public JasiObject parseResultSetDb(ResultSetDb rsdb) {
      return parseResultSet((ResultSetDb) rsdb);
    }
    /**Access to the JasiDbReader to allow generic SQL queries. */
    public JasiDataReaderIF getDataReader() {
        return jasiDataReader;
    }

} // end of ChannelLocationTN
