//
// This code looks like it could be melded into JasiChannelDbReader -aww
// JasiReading TN classes should all implement  tableObj like concept
//
package org.trinet.jasi.TN;
/*
   THIS IS NOT COMPLETE -- BEGUN 4/3/03
*/
import java.util.*;
import java.sql.*;

import org.trinet.jasi.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

/**
 * Fetch a channelable list from the DataSource.
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: USGS</p>
 * @author Doug Given
 * @version 1.0
 *
   *************************************
   THIS IS NOT COMPLETE -- BEGUN 4/3/03
   *************************************

   Implementation entanglements with different Channelable types...
 */

public class ChannelableListFetcherTN extends ChannelableListFetcher {


  protected StringBuffer whereClause = new StringBuffer();

  protected StringBuffer netClause  = new StringBuffer();
  protected StringBuffer staClause  = new StringBuffer();
  protected StringBuffer chanClause = new StringBuffer();
  protected StringBuffer locClause  = new StringBuffer();

  protected StringBuffer timeClause = new StringBuffer();

// What dbase table will we use
  static DataTableRow tableObj = null;  // default
  static String tname = null;

  public ChannelableListFetcherTN() {
  }
  public void setClass (JasiObject obj) {

    // the channelable objects
    // map object type to underlying schema table so we can use its name in
    // building where clauses
    if (obj instanceof AmplitudeTN) {
      tableObj = new Amp();
    } else if (obj instanceof UnassocAmplitudeTN) {
      tableObj = new UnassocAmp();
    } else if (obj instanceof PhaseTN) {
      tableObj = new Arrival();
    } else if (obj instanceof CodaTN) {
      tableObj = new org.trinet.jdbc.table.Coda();
    } else if (obj instanceof WaveletTN) {
      tableObj = new org.trinet.jdbc.table.Waveform();
    }
    tname = tableObj.getTableName();  // set the table name for later queries
    System.err.println("Table = "+tname);
  }
  /**
   *  Set the list of net codes to either require or exclude.
   If inexclude = INCLUDE
   only Objects with a net type found in the list will be returned.
   If inexclude = EXCLUDE
   only Objects with a net type NOT found in the list will be returned. */
   /*
   Sets the the SQL "where" clause to get amps only of the types in the list.
   * Make an Inclusive clause
   (table.col like 'lst1' or table.col like 'lst2' ...)
   * Make an Exclusive clause
  (table.col not like 'lst1' and table.col not like 'lst2' ...)
   */
   public void setNetList (String[] list, int inexclude) {
     netClause = composeListClause(list, inexclude, netClause, tname, "net");
  }
  /** Set the list of station codes to either require or exclude. */
  public void setStationList (String[] list, int inexclude) {
    staClause = composeListClause(list, inexclude, staClause, tname, "sta");
  }
  /** Set the list of channel codes to either require or exclude. */
  public void setChannelList (String[] list, int inexclude) {
    chanClause = composeListClause(list, inexclude, chanClause, tname, "seedChan");
  }
  /** Set the list of location codes to either require or exclude. */
  public void setLocationList (String[] list, int inexclude) {
    locClause = composeListClause(list, inexclude, locClause, tname, "location");
  }
  /** Make a clause <p>
   */
   StringBuffer composeListClause (String[] list, int inexclude, StringBuffer sb,
                           String tablename, String colname) {

   if (inexclude == INCLUDE) {
     return composeListClauseInc(list, sb, tablename, colname);
   } else if (inexclude == EXCLUDE){
     return composeListClauseExc(list, sb, tablename, colname);
   } else {
     System.err.println("ChannelableListFetcher: bad include/exclude value.");
     return new StringBuffer();
   }
  }
  /**
   * Make an Inclusive clause
   (table.col like 'lst1' or table.col like 'lst2' ...)
   */
  StringBuffer composeListClauseInc (String[] list, StringBuffer sb,
                         String tablename, String colname) {
    sb = new StringBuffer();     // start from scratch

    sb.append("(");
    for (int i = 0; i < list.length; i++) {
      if (i > 0) sb.append(" or ");
      sb.append(tablename).append(".").append(colname);
      sb.append(" like '").append(list[i]).append("' ");
    }
    sb.append(")");
    return sb;
  }
  /**
  * Make an Exclusive clause
  (table.col not like 'lst1' and table.col not like 'lst2' ...)
  */
 StringBuffer composeListClauseExc (String[] list, StringBuffer sb,
                        String tablename, String colname) {
   sb = new StringBuffer();     // start from scratch

   sb.append("(");
   for (int i = 0; i < list.length; i++) {
     if (i > 0) sb.append(" and ");
     sb.append(tablename).append(".").append(colname);
     sb.append(" not like '").append(list[i]).append("' ");
   }
   sb.append(")");
   return sb;
  }

  /** Returns an empty StringBuffer if no sub-clauses were created. */
  public String getWhereClause () {

    StringBuffer sb = new StringBuffer();

     addClause(sb, netClause);
     addClause(sb, staClause);
     addClause(sb, chanClause);
     addClause(sb, locClause);
     addClause(sb, timeClause);

     if (sb.length() > 0) {
       whereClause = new StringBuffer();
       whereClause.append("Where ");
       whereClause.append(sb.toString());
     }
     return whereClause.toString();
  }
/** Append a subclause, correctly handling blank clauses and "and"s between clauses. */
  StringBuffer addClause (StringBuffer mainClause, StringBuffer subClause) {
    if (subClause.length() != 0) {
      if (mainClause.length() != 0) mainClause.append(" and ");
      mainClause.append("(");
      mainClause.append(subClause.toString());
      mainClause.append(")");
    }
    return mainClause;
  }

// TODO
public ChannelableList fetch (){
  return null;
}

/*
  public static void main(String[] args) {

    System.out.println ("Making connection...");
    DataSource ds = TestDataSource.create();

    ChannelableListFetcher clf = ChannelableListFetcher.create();

    clf.setClass(Amplitude.create());

    String netList[] = {"CI", "CE"};
    clf.setNetList(netList, ChannelableListFetcher.INCLUDE);

    String staList[] = {"ABC", "P%"};
    clf.setStationList(staList, ChannelableListFetcher.EXCLUDE);

    String where = ((ChannelableListFetcherTN) clf).getWhereClause();
    System.out.println(where) ;

//    AmpList amplist = clf.fetch();

  }
*/
}
