package org.trinet.jasi.TN;
import java.sql.*;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;
import org.trinet.jdbc.NullValueDb;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.TableRowCoda;

public class CodaListTN extends CodaList {

    private PreparedStatement psInsert = null;

    private static final String psInsertStr =
        "INSERT INTO CODA (COID,STA,NET,AUTH,SUBSOURCE,CHANNEL,CHANNELSRC,SEEDCHAN," + // 1-8
        "LOCATION,CODATYPE,AFIX,AFREE,QFIX,QFREE,TAU,NSAMPLE,RMS,DURTYPE,UNITS," + // 9-19
        "TIME1,AMP1,TIME2,AMP2,QUALITY,RFLAG,DATETIME,ALGORITHM,WINSIZE) VALUES" +  // 20-28
        " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,TRUETIME.putEpoch(?,'UTC'),?,?)"; // 28 columns
      //" (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"; // 28 columns

    private PreparedStatement psInsertAssocO = null;
    private static final String psInsertAssocOStr =
        "INSERT INTO ASSOCCOO (ORID,COID,AUTH,SUBSOURCE,DELTA,SEAZ,RFLAG) VALUES (?,?,?,?,?,?,?)"; // 7 columns

    private CallableStatement csUpsertAssocO = null;
    private static final String csUpsertAssocOStr = "{ call EPREF.upsertAssocCoO(?,?,?,?,?,?,?) }"; // 7 columns

    private PreparedStatement psInsertAssocM = null;
    private static final String psInsertAssocMStr = 
        "INSERT INTO ASSOCCOM (MAGID,COID,AUTH,SUBSOURCE,WEIGHT,IN_WGT,MAG,MAGRES,MAGCORR,RFLAG) VALUES (?,?,?,?,?,?,?,?,?,?)"; // 10 columns

    private CallableStatement csUpsertAssocM = null;
    private static final String csUpsertAssocMStr = "{ call EPREF.upsertAssocCoM(?,?,?,?,?,?,?,?,?,?) }"; // 10 columns

    public static boolean debug = false;

    static {
        debug = JasiDatabasePropertyList.debugTN;
    }

    /** No-op default constructor, empty list. */
    public CodaListTN() {}

    /** Create empty list with specified input capacity.*/
    public CodaListTN(int capacity) {
        super(capacity);
    }

    /** Create list and add input collection elements to list.*/
    public CodaListTN(Collection col) {
        super(col);
    }
    /*
    private static final String csSeqStr = "{ ? = call SEQUENCE.getSeqTable('ARSEQ', ?) }";
        CallableStatement csSeqIdStmt = DataSource.getConnection().prepareCall(csSeqStr); 
        csSeqIdStmt.registerOutParameter(1, java.sql.Types.ARRAY, "SEQ_TABLE");
        csSeqIdStmt.setInt(2, newList.size());
        int result = csSeqIdStmt.executeUpdate();
        java.sql.Array intArray = cs.SeqIdStmt.getArray(1);
        BigDecimal[] bigd = (BigDecimal[]) intArray.getArray();  // need to use bigd.longValue() for the ampid 
    */

    // private static final String psSeqStr = "Select * from sequence.getSeqTable('COSEQ',?)";
    private PreparedStatement psSeqIdStmt = null;

    public boolean commit() {

        debug = JasiDatabasePropertyList.debugTN;

        if (!DataSource.isWriteBackEnabled()) return false;
        boolean status = true;

        int mySize = this.size();
        if (mySize == 0)  return true; // nothing to commit, a no-op

        // Find new readings to be saved, but without a valid id
        CodaTN codaTN = null;
        ArrayList newList = new ArrayList(mySize);
        for (int i = 0; i < mySize; i++) {
            codaTN = (CodaTN) get(i);
            //System.out.println(codaTN); // dump for debug
            //if (codaTN.isDeleted() || ! (codaTN.isAssignedToSol() || codaTN.isAssignedToMag())
            //      || codaTN.getCoid() != 0) continue; // -aww 2011/05/12 replaced by below
            if (codaTN.isDeleted() || ! (codaTN.isAssignedToSol() || codaTN.isAssignedToMag()) ||
                    (codaTN.getCoid() !=0 && !codaTN.hasChanged()) ) continue; // skip, old id and a change=>new coid - aww 2011/05/12
            newList.add(codaTN);
        }

        // Set the ids of new readings to be committed
        PreparedStatement psSeqIdStmt = null;
        if (newList.size() > 0) {
          ResultSet rs = null;
          try {
            if (psSeqIdStmt == null) {
                psSeqIdStmt = DataSource.getConnection().prepareStatement(DataSource.getJiggleConnection().getSequenceQuery(TableRowCoda.SEQUENCE_NAME));
            }
            psSeqIdStmt.setInt(1, newList.size());
            rs = psSeqIdStmt.executeQuery();
            int ii = 0;
            while (rs.next()) {
                codaTN = (CodaTN)newList.get(ii++);
                codaTN.setCoid( rs.getLong(1) );
            }
          }
          catch (SQLException ex) {
              status = false;
              ex.printStackTrace();
          }
          finally {
            try {
              if (rs != null) rs.close();
              if (psSeqIdStmt != null) psSeqIdStmt.close();
              psSeqIdStmt = null;
            }
            catch (SQLException ex) {
            }
          }
        }
        if (!status) return false; // error setting new ids

        // Batch queue insert or update
        try {
          for (int i = 0; i<mySize; i++) {
            codaTN = (CodaTN) get(i);
            if (debug) System.out.println ("DEBUG CodaListTN commit() fromDB:"+codaTN.isFromDataSource()+" "+codaTN.toNeatString());
            // Reading is deleted or not associated so skip
            if (codaTN.isDeleted() || ! codaTN.isAssignedToSol()) continue;
              if (codaTN.isFromDataSource()) {  // "existing" (came from the dbase)
                if (codaTN.getNeedsCommit() || codaTN.hasChanged()) { // insert new row and association
                    if (debug) System.out.println ("Coda (changed) INSERT and assoc");
                    status = dbaseInsert(codaTN) && dbaseAssociate(codaTN, false);
                } else { // if no changes, just make new association
                    if (debug) System.out.println ("Coda (unchanged) assoc only");
                    status = dbaseAssociate(codaTN, true);
                    if (status) codaTN.setUpdate(false); // mark as up-to-date
                }
              } else {        // not from dbase <INSERT>
                  if (debug) System.out.println ("Coda (new) INSERT and assoc");
                  status = dbaseInsert(codaTN) && dbaseAssociate(codaTN, false);
              }
              //codaTN.setNeedsCommit(! status); // hopefully false?
              if (!status) {
                System.out.println("CodaListTN commit() batched:" + status + " " + codaTN.toString());
              }
          }

          // Done with queuing up, now execute DML
          status = batchToDb(true); // close and null statements
          if (debug) System.out.println("DEBUG CodaListTN batchToDb success " + status);

          if (status) { // assume success, reset commit status
            for (int i = 0; i<mySize; i++) {
              ((CodaTN) get(i)).setNeedsCommit(false); // hopefully ok?
            }
          }
        }
        catch (Exception ex) {
          ex.printStackTrace();
          return false;
        }
        return status;  // relies on commit throwing exception if unsuccessful 
    }

    private boolean dbaseAssociate(CodaTN codaTN, boolean update) {
        return (update) ? upsertAssoc(codaTN) : insertAssoc(codaTN);
    }

    private boolean upsertAssoc(CodaTN codaTN) {
        boolean status = upsertOridAssoc(codaTN);
        status |= upsertMagidAssoc(codaTN);
        return status;
    }

    private boolean upsertOridAssoc(CodaTN codaTN) {
        boolean status = true;
        try {
            // Have valid orid ?
            SolutionTN solTN = (SolutionTN) codaTN.sol;
            if (solTN == null) return false;

            long assocOrid = solTN.getOridValue();
            if (assocOrid <= 0l) return false;

            if (csUpsertAssocO == null) {
                if (debug || JasiDatabasePropertyList.debugSQL) {
                    System.out.println("DEBUG CodaListTN upsertOridAssoc for orid:" + assocOrid);
                }
                csUpsertAssocO = DataSource.getConnection().prepareCall(csUpsertAssocOStr);
                //csUpsertAssocO.registerOutParameter(1, Types.INTEGER);
            }

            int icol = 1;

            // Key can't be null value
            //orid
            csUpsertAssocO.setLong(icol++, assocOrid);
            //coid
            csUpsertAssocO.setLong(icol++, codaTN.getCoid());

            //auth
            //String str = solTN.getAuthority(); // changed to below -aww 2011/09/21
            String str = codaTN.getClosestAuthorityString();  // uses local net code if undefined 
            if (NullValueDb.isNull(str)) str = null;
            csUpsertAssocO.setString(icol++, str);

            //subsource
            str = solTN.getSource();
            if (NullValueDb.isNull(str)) str = null;
            csUpsertAssocO.setString(icol++, str);

            //delta
            double x = codaTN.getHorizontalDistance();
            if ( ! (x == Channel.NULL_DIST || Double.isNaN(x)) ) csUpsertAssocO.setDouble(icol++, x);
            else csUpsertAssocO.setNull(icol++, Types.DOUBLE);

            //seaz
            x = codaTN.getAzimuth();
            if ( ! (x == Channel.NULL_AZIMUTH || Double.isNaN(x)) ) csUpsertAssocO.setDouble(icol++, x);
            else csUpsertAssocO.setNull(icol++, Types.DOUBLE);

            // rflag
            if (codaTN.processingState.isValid()) csUpsertAssocO.setString(icol++, codaTN.processingState.toString());
            else csUpsertAssocO.setNull(icol++, Types.VARCHAR);

            csUpsertAssocO.addBatch();

        }
        catch (SQLException ex) {
            System.err.println(codaTN);  // in case we need to know more on error 
            status = false;
            ex.printStackTrace();
        } finally {
            JasiDatabasePropertyList.debugSQL(csUpsertAssocOStr, csUpsertAssocO, debug);
        }
        return status;
    }

    private boolean upsertMagidAssoc(CodaTN codaTN) {
        boolean status = true;
        try {
            // Have valid magid ?
            MagnitudeTN magTN = (MagnitudeTN) codaTN.magnitude;
            if (magTN == null) return false; 

            long assocMagid =  magTN.getMagidValue();
            if (assocMagid <= 0l) return false;


            if (csUpsertAssocM == null) {
                if (debug || JasiDatabasePropertyList.debugSQL) {
                    System.out.println("DEBUG CodaListTN upsertMagidAssoc for magid:" + assocMagid);
                }
                csUpsertAssocM = DataSource.getConnection().prepareCall(csUpsertAssocMStr);
                //csUpsertAssocM.registerOutParameter(1, Types.INTEGER);
            }

            int icol = 1;

            // Key can't be null value
            //magid
            csUpsertAssocM.setLong(icol++, assocMagid);

            //coid
            csUpsertAssocM.setLong(icol++, codaTN.getCoid());

            //auth
            //String str = magTN.getAuthority(); // changed to below -aww 2011/09/21
            String str = codaTN.getClosestAuthorityString();  // uses local net code if undefined 
            if (NullValueDb.isNull(str)) str = null;
            csUpsertAssocM.setString(icol++, str);

            //subsource
            str = magTN.getSource();
            if (NullValueDb.isNull(str)) str = null;
            csUpsertAssocM.setString(icol++, str);

            //wgt
            if (codaTN.channelMag.weight.isValid()) csUpsertAssocM.setDouble(icol++, codaTN.channelMag.weight.doubleValue());
            else csUpsertAssocM.setNull(icol++, Types.DOUBLE);

            //in_wgt
            if (codaTN.channelMag.inWgt.isValid()) csUpsertAssocM.setDouble(icol++, codaTN.channelMag.inWgt.doubleValue());
            else csUpsertAssocM.setNull(icol++, Types.DOUBLE);

            //mag
            if (codaTN.channelMag.value.isValid()) csUpsertAssocM.setDouble(icol++, codaTN.channelMag.value.doubleValue());
            else csUpsertAssocM.setNull(icol++, Types.DOUBLE);

            //magres
            if (codaTN.channelMag.residual.isValid()) csUpsertAssocM.setDouble(icol++, codaTN.channelMag.residual.doubleValue());
            else csUpsertAssocM.setNull(icol++, Types.DOUBLE);

            //magcorr
            if (codaTN.channelMag.correction.isValid()) csUpsertAssocM.setDouble(icol++, codaTN.channelMag.correction.doubleValue());
            else csUpsertAssocM.setNull(icol++, Types.DOUBLE);

            // rflag
            if (codaTN.processingState.isValid()) csUpsertAssocM.setString(icol++, codaTN.processingState.toString());
            else csUpsertAssocM.setNull(icol++, Types.VARCHAR);

            csUpsertAssocM.addBatch();
        }
        catch (SQLException ex) {
            System.err.println(codaTN);  // in case we need to know more on error 
            status = false;
            ex.printStackTrace();
        } finally {
            JasiDatabasePropertyList.debugSQL(csUpsertAssocMStr, csUpsertAssocM, debug);
        }
        return status;
    }

    private boolean insertAssoc(CodaTN codaTN) {
        boolean status = insertOridAssoc(codaTN);
        status |= insertMagidAssoc(codaTN);
        return status;
    }

    private boolean insertOridAssoc(CodaTN codaTN) {
        boolean status = true;
        try {
            // Have valid orid ?
            SolutionTN solTN = (SolutionTN) codaTN.sol;
            if (solTN == null) return false;

            long assocOrid = solTN.getOridValue();
            if (assocOrid <= 0l) return false;


            if (debug || JasiDatabasePropertyList.debugSQL) 
                System.out.println("DEBUG CodaListTN insertOridAssoc orid:" + assocOrid + " coid:" + codaTN.getCoid());
            if (psInsertAssocO == null) {
                psInsertAssocO = DataSource.getConnection().prepareStatement(psInsertAssocOStr);
            }

            int icol = 1;

            // Key can't be null 
            //orid
            psInsertAssocO.setLong(icol++, assocOrid);

            //coid
            psInsertAssocO.setLong(icol++, codaTN.getCoid());

            //auth
            //String str = solTN.getAuthority(); // changed to below -aww 2011/09/21
            String str = codaTN.getClosestAuthorityString();  // uses local net code if undefined 
            if (NullValueDb.isNull(str)) str = null;
            psInsertAssocO.setString(icol++, str);

            //subsource
            str = solTN.getSource();
            if (NullValueDb.isNull(str)) str = null;
            psInsertAssocO.setString(icol++, str);

            //delta
            double x = codaTN.getHorizontalDistance();
            if ( ! (x == Channel.NULL_DIST || Double.isNaN(x)) ) psInsertAssocO.setDouble(icol++, x);
            else psInsertAssocO.setNull(icol++, Types.DOUBLE);

            //seaz
            x = codaTN.getAzimuth();
            if ( ! (x == Channel.NULL_AZIMUTH || Double.isNaN(x)) ) psInsertAssocO.setDouble(icol++, x);
            else psInsertAssocO.setNull(icol++, Types.DOUBLE);

            // rflag
            if (codaTN.processingState.isValid()) psInsertAssocO.setString(icol++, codaTN.processingState.toString());
            else psInsertAssocO.setNull(icol++, Types.VARCHAR);

            psInsertAssocO.addBatch();

        }
        catch (SQLException ex) {
            System.err.println(codaTN);  // in case we need to know more on error 
            status = false;
            ex.printStackTrace();
        } finally {
            JasiDatabasePropertyList.debugSQL(psInsertAssocOStr, psInsertAssocO, debug);
        }
        return status;
    }

    private boolean insertMagidAssoc(CodaTN codaTN) {
        boolean status = true;
        try {
            // Have valid magid ?
            MagnitudeTN magTN = (MagnitudeTN) codaTN.magnitude;
            if (magTN == null) return false; 

            long assocMagid =  magTN.getMagidValue();
            if (assocMagid <= 0l) return false;


            if (debug || JasiDatabasePropertyList.debugSQL)
                System.out.println("DEBUG CodaListTN insertMagidAssoc magid:" + assocMagid + " coid:" + codaTN.getCoid());
            if (psInsertAssocM == null) {
                psInsertAssocM = DataSource.getConnection().prepareStatement(psInsertAssocMStr);
            }

            int icol = 1;

            // Key can't be null
            //magid
            psInsertAssocM.setLong(icol++, assocMagid);

            //coid
            psInsertAssocM.setLong(icol++, codaTN.getCoid());

            //auth
            //String str = magTN.getAuthority(); // changed to below -aww 2011/09/21
            String str = codaTN.getClosestAuthorityString();  // uses local net code if undefined 
            if (NullValueDb.isNull(str)) str = null;
            psInsertAssocM.setString(icol++, str);

            //subsource
            str = magTN.getSource();
            if (NullValueDb.isNull(str)) str = null;
            psInsertAssocM.setString(icol++, str);

            //wgt
            if (codaTN.channelMag.weight.isValid()) psInsertAssocM.setDouble(icol++, codaTN.channelMag.weight.doubleValue());
            else psInsertAssocM.setNull(icol++, Types.DOUBLE);

            //in_wgt
            if (codaTN.channelMag.inWgt.isValid()) psInsertAssocM.setDouble(icol++, codaTN.channelMag.inWgt.doubleValue());
            else psInsertAssocM.setNull(icol++, Types.DOUBLE);

            //mag
            if (codaTN.channelMag.value.isValid()) psInsertAssocM.setDouble(icol++, codaTN.channelMag.value.doubleValue());
            else psInsertAssocM.setNull(icol++, Types.DOUBLE);

            //magres
            if (codaTN.channelMag.residual.isValid()) psInsertAssocM.setDouble(icol++, codaTN.channelMag.residual.doubleValue());
            else psInsertAssocM.setNull(icol++, Types.DOUBLE);

            //magcorr
            if (codaTN.channelMag.correction.isValid()) psInsertAssocM.setDouble(icol++, codaTN.channelMag.correction.doubleValue());
            else psInsertAssocM.setNull(icol++, Types.DOUBLE);

            // rflag
            if (codaTN.processingState.isValid()) psInsertAssocM.setString(icol++, codaTN.processingState.toString());
            else psInsertAssocM.setNull(icol++, Types.VARCHAR);

            psInsertAssocM.addBatch();
        }
        catch (SQLException ex) {
            System.err.println(codaTN);  // in case we need to know more on error 
            status = false;
            ex.printStackTrace();
        } finally {
            JasiDatabasePropertyList.debugSQL(psInsertAssocMStr, psInsertAssocM, debug);
        }
        return status;
    }

    private boolean dbaseInsert(CodaTN codaTN) {
        boolean status = true;
        try {
            if (debug || JasiDatabasePropertyList.debugSQL)
                System.out.println("DEBUG CodaListTN dbaseInsert coid:" + codaTN.getCoid());
            // NOTE: 2nd argument below to return generatedKeys not meaningful for batch !
            //if (psInsert == null) psInsert = DataSource.getConnection().prepareStatement(psInsertStr, new String[] {"COID"});
            if (psInsert == null) {
                psInsert = DataSource.getConnection().prepareStatement(psInsertStr);
            }

            int icol = 1;

            ChannelName cn = codaTN.getChannelObj().getChannelName();
            // id was assumed to be set from sequence
            psInsert.setDouble(icol++, codaTN.getCoid());

            psInsert.setString(icol++, cn.getSta());
            psInsert.setString(icol++, cn.getNet());
            psInsert.setString(icol++, codaTN.getClosestAuthorityString());  // uses local net code if undefined 
            psInsert.setString(icol++, codaTN.source.toString());
            psInsert.setString(icol++, cn.getChannel());
            psInsert.setString(icol++, cn.getChannelsrc());
            psInsert.setString(icol++, cn.getSeedchan());
            psInsert.setString(icol++, cn.getLocation());

            //CODATYPE
            CodaType codaType = codaTN.descriptor.getCodaType();
            if (!(codaType == CodaType.UNKNOWN)) psInsert.setString(icol++, codaTN.descriptor.getCodaTypeName());
            else psInsert.setNull(icol++, Types.VARCHAR);

            //AFIX
            if (codaTN.aFix.isValid()) psInsert.setDouble(icol++, codaTN.aFix.doubleValue());
            else psInsert.setNull(icol++, Types.DOUBLE);
            //AFREE
            if (codaTN.aFree.isValid()) psInsert.setDouble(icol++, codaTN.aFree.doubleValue());
            else psInsert.setNull(icol++, Types.DOUBLE);
            //QFIX
            if (codaTN.qFix.isValid()) psInsert.setDouble(icol++, codaTN.qFix.doubleValue());
            else psInsert.setNull(icol++, Types.DOUBLE);
            //QFREE
            if (codaTN.qFree.isValid()) psInsert.setDouble(icol++, codaTN.qFree.doubleValue());
            else psInsert.setNull(icol++, Types.DOUBLE);
            //TAU
            if (codaTN.tau.isValid())   psInsert.setDouble(icol++, codaTN.tau.doubleValue());
            else psInsert.setNull(icol++, Types.DOUBLE);
            //NSAMPLE
            if (codaTN.windowCount.isValid()) psInsert.setDouble(icol++, codaTN.windowCount.doubleValue());
            else psInsert.setNull(icol++, Types.DOUBLE);
            //RMS
            if (codaTN.residual.isValid()) psInsert.setDouble(icol++, codaTN.residual.doubleValue());
            else psInsert.setNull(icol++, Types.DOUBLE);
            //DURTYPE
            CodaDurationType durType = codaTN.descriptor.getDurationType();
            if (!(durType == CodaDurationType.UNKNOWN)) psInsert.setString(icol++, codaTN.descriptor.getDurationTypeName());
            else psInsert.setNull(icol++, Types.VARCHAR);

            //IPHASE 
            //String desc = codaTN.descriptor.getDescription();
            //if (! NullValueDb.isBlank(desc)) psInsert.setString(icol++, desc);

            //UNITS
            if (codaTN.ampUnits.isValid()) psInsert.setString(icol++, codaTN.ampUnits.toString());
            else psInsert.setNull(icol++, Types.VARCHAR);

            ArrayList timeAmps = codaTN.getWindowTimeAmpPairs();
            if (timeAmps != null && timeAmps.size() > 0) {

              int size = timeAmps.size();
              TimeAmp ta =  (TimeAmp) timeAmps.get(0);
              DataDouble dd = ta.getTime();

              //TIME1
              if (dd.isValidNumber()) psInsert.setDouble(icol++, dd.doubleValue());
              else psInsert.setNull(icol++, Types.DOUBLE);
              dd = ta.getAmp();

              //AMP1
              if (dd.isValidNumber()) psInsert.setDouble(icol++, dd.doubleValue());
              else psInsert.setNull(icol++, Types.DOUBLE);
              if (size == 2) {

                 ta =  (TimeAmp) timeAmps.get(1);
                 dd = ta.getTime();
                 //TIME2
                 if (dd.isValidNumber()) psInsert.setDouble(icol++, dd.doubleValue()); 
                 else psInsert.setNull(icol++, Types.DOUBLE);

                 dd = ta.getAmp();
                 //AMP2
                 if (dd.isValidNumber()) psInsert.setDouble(icol++, dd.doubleValue()); 
                 else psInsert.setNull(icol++, Types.DOUBLE);

              }
              else { // for case of a single amp time window, short coda
                 psInsert.setNull(icol++, Types.DOUBLE);
                 psInsert.setNull(icol++, Types.DOUBLE);
              }
            }
            else {
                 psInsert.setNull(icol++, Types.DOUBLE);
                 psInsert.setNull(icol++, Types.DOUBLE);
                 psInsert.setNull(icol++, Types.DOUBLE);
                 psInsert.setNull(icol++, Types.DOUBLE);
            }

            //QUALITY
            if (codaTN.quality.isValid()) psInsert.setDouble(icol++, codaTN.quality.doubleValue());
            else psInsert.setNull(icol++, Types.DOUBLE);

            //RFLAG
            if (codaTN.processingState.isValid()) psInsert.setString(icol++, codaTN.processingState.toString());
            else psInsert.setNull(icol++, Types.VARCHAR);

            //DATETIME
            if (codaTN.datetime.isValid()) psInsert.setDouble(icol++, codaTN.datetime.doubleValue());
            else psInsert.setNull(icol++, Types.NUMERIC);

            //ALGORITHM
            if (codaTN.algorithm.isValid()) psInsert.setString(icol++, codaTN.algorithm.toString());
            else psInsert.setNull(icol++, Types.VARCHAR);

            //WINSIZE
            if (codaTN.windowSize.isValid()) psInsert.setDouble(icol++, codaTN.windowSize.doubleValue());
            else psInsert.setNull(icol++, Types.DOUBLE);

            psInsert.addBatch();
        }

        catch (SQLException ex) {
            System.err.println(codaTN);  // in case we need to know more on error 
            status = false;
            ex.printStackTrace();
        } finally {
            JasiDatabasePropertyList.debugSQL(psInsertStr, psInsert, debug);
        }
        if (status) codaTN.setUpdate(false); // mark as up-to-date -aww added 2010/09/13
        return status;
    }

    private boolean batchToDb(boolean close) {

        int cnt = batchToDb(psInsert, close);
        if (debug || JasiDatabasePropertyList.debugSQL) System.out.println("DEBUG CodaListTN batchToDb insertCoda status: " + cnt);
        boolean status = (cnt >= 0); 

        if (status) {

            cnt = batchToDb(csUpsertAssocO, close);
            status &= (cnt >= 0); 
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println("DEBUG CodaListTN batchToDb upsertAssocO status: " + cnt);

            cnt = batchToDb(psInsertAssocO, close);
            status &= (cnt >= 0); 
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println("DEBUG CodaListTN batchToDb insertAssocO status: " + cnt);

            cnt = batchToDb(csUpsertAssocM, close);
            status &= (cnt >= 0); 
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println("DEBUG CodaListTN batchToDb upsertAssocM status: " + cnt);

            cnt = batchToDb(psInsertAssocM, close);
            status &= (cnt >= 0); 
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println("DEBUG CodaListTN batchToDb insertAssocM status: " + cnt);
        }

        if (close) {
            psInsert = null;
            psInsertAssocO = null;
            csUpsertAssocO = null;
            psInsertAssocM = null;
            csUpsertAssocM = null;
        }

        return status;
    }

    // updateCount value: Statement.SUCCESS_NO_INFO
    // updateCount value: Statement.EXECUTE_FAILED
    private int batchToDb(Statement stmt, boolean close) {
        if (stmt == null) return 0;  // no-op, assume none

        int [] updateCounts = null;
        int status = -1;
        try {
            updateCounts = stmt.executeBatch();
            status = updateCounts.length;
            for (int idx = 0; idx<updateCounts.length; idx++) {
                if (updateCounts[idx] == Statement.EXECUTE_FAILED) {
                    status = -idx; // return negative index, flagging error -aww 2010/09/15
                    System.err.println("ERROR CodaTN executeBatchInsert failed at idx: " + idx);
                    break;
                }
            }
        }
/*
ORACLE BATCH IMPLEMENTATION
For a prepared statement batch, it is not possible to know which operation failed.
The array has one element for each operation in the batch, and each element has a value of -3.
According to the JDBC 2.0 specification, a value of -3 indicates that an operation did not complete successfully.
In this case, it was presumably just one operation that actually failed, but because the JDBC driver does not 
know which operation that was, it labels all the batched operations as failures. 
You should always perform a ROLLBACK operation in this situation.

For a generic statement batch or callable statement batch, the update counts array is only a partial array
containing the actual update counts up to the point of the error. The actual update counts can be provided
because Oracle JDBC cannot use true batching for generic and callable statements in the Oracle implementation
of standard update batching. 

For example, if there were 20 operations in the batch, the first 13 succeeded, and the 14th generated an exception,
then the update counts array will have 13 elements, containing actual update counts of the successful operations.
You can either commit or roll back the successful operations in this situation, as you prefer.
In your code, upon failed execution of a batch, you should be prepared to handle either -3's
or true update counts in the array elements when an exception occurs.
For a failed batch execution, you will have either a full array of -3's or a partial array of positive integers.
*/
        catch(BatchUpdateException b) {
            System.err.println("-----BatchUpdateException-----");
            System.err.println("SQLState:  " + b.getSQLState());
            System.err.println("Message:  " + b.getMessage());
            System.err.println("Vendor:  " + b.getErrorCode());
            System.err.print("Update counts:  ");
            updateCounts = b.getUpdateCounts();
            for (int i = 0; i < updateCounts.length; i++) {
                System.err.print(updateCounts[i] + "   ");
            }
            System.err.println("");
        }
        catch(SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            try {
                if (close) stmt.close();
            }
            catch (SQLException ex2) {
                ex2.printStackTrace();
            }
        }
        return status;
    }

} // CodaListTN
