// PROBLEM if origin doesn't change and phases/amps don't change commit of assoc
// relation violates prior key id constraint, orid,arid no change from db but other
// data change (not key) may require update after edits. Only bypass now is to force
// a relocation to get valid key commit of association with new orid
// TODO: database SQL stuff here similar to the other TN classes needs to be
// refactored into common methods and perhaps put into a DbWriterTN delegate class.
//
// Decide when to force a rollback; do we want to alway save partially written associations etc.?
//
package org.trinet.jasi.TN;

import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.logging.Level;

import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;
import org.trinet.jasi.engines.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.jasi.magmethods.TN.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.table.*;
import org.trinet.jdbc.datatypes.*;
//import org.trinet.jdbc.datasources.AbstractSQLDataSource;
import org.trinet.jiggle.common.JiggleExceptionHandler;
import org.trinet.jiggle.common.LogUtil;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;
import org.trinet.util.gazetteer.TN.OracleLatLon;
//
import org.trinet.jiggle.common.type.AssocEvent;
import org.trinet.jiggle.database.event.EventAssocManager;

/**
 * Component of the Seismic Abstraction Layer. This implementation interfaces
 * to the Berkeley/Caltech/USGS schema in Oracle. <p>
 *
 * All communication with the data source is via the mechanism specified in
 * DataSource. Therefore, DataSource must be used before any JASI components
 * can read/write to data sources. <p>
 *
 * All data members are DataObjects rather than primative types so they can
 * handle the concepts of "nullness", "mutability", and "change-state".<p>
 *
 * NULLNESS = a field can be null, i.e. unknown or no value. The traditional
 * approach has been to insert zero or some bogus value. In ASCII
 * representations sometimes a field was just left blank. For example: RMS
 * 'distance' might simply be unknown. To give it a value of 0.0km could foul up
 * other calculations. When you write data out to a database you want to leave
 * null fields null and not write a bunch of zeros that were used as an
 * expedient inside of an application.<p>
 *
 * MUTABLITY = some fields should NEVER be explicity manipulated by an
 * application. These are usually "key" fields that would break the database or
 * cause constraint errors. They would be set immutable. Think of these as
 * "read-only".<p>
 *
 * CHANGE-STATE = this is primarily an efficiency issue. If you read in 1000
 * records, change 10 of them, then save your work, you only want to write
 * ('update') the changed records to the database.<p>
 *
 * The database can contain null values for
 * certain fields. Also applications may not want to write a "bogus" value like 0
 * into the database when when the real value is not known. Data primatives can't
 * be null and are initiallize to zero. */

/* NCDC Schema map

  [EVENT]--[Origin]-+------------[[[AssocAmO]]]--+
                    |                            +--[[[Amp]]
                    +--[NetMag]--[[[AssocAmM]]]--+
                    |
                    +------------[[[AssocArO]]]-----[[[Arrival]]]
 */

public class SolutionTN extends Solution implements DbMagnitudeAssocIF, DbReadableJasiObjectIF {
    private static final String POLYGON_TEXT = "POLYGON((";

    // verbose output debug control flag
    protected static boolean debug = false;

    //private BenchMark bm = null;

    // Level of commit, allows partial save back to database
    private static final int COMMIT_ORIGIN        = 0; // event,origin
    private static final int COMMIT_WAVEFORMS    = 1; // above plus assocwae
    private static final int COMMIT_READINGS     = 2; // above plus arrival,amp,coda & assoc
    private static final int COMMIT_MAGNITUDES   = 3; // above plus magnitudes and prefmags
    private static final int COMMIT_ALL          = 9; // everything

    /*
      Here are protected data members used to support this particular
      implementation of the abstract layer */

    /** Schema constraint for size of AUTH attribute */
    public static final int MAX_AUTH_SIZE = 15;
    /** Schema constraint for size of SUBSOURCE attribute */
    public static final int MAX_SUBSOURCE_SIZE = 8;

    // Schema classes that map to tables. Keep these for update, delete, etc.
    // note the getDataObject returns alias to row's field object, not a clone
    // so either clone the object or use setValue(obj) to set the data members.
    protected Event  eventRow = null;
    protected Origin originRow = null;

    // foreign keys
    /*
      The fields prefmag, prefmec & commid exist in BOTH the Event and Origin tables.
      Which to use??  Made decision to use prefmag, prefmec of Origin, and commid of Event.
    */

    // SolutionTN as implemented more like an Origin row than an Event/Trigger entity
    // Most common usage forces prefor, orid values to be synched (see commit(), parseRow()) 
    // The "prefor" would differ from "orid" when Solution is an alternate loaded via 
    // the methods getAllById() or getAlternatesById() or loadAltSolList().
    // Perhaps we need to create an "Event" super class for the altSolutionList data.
    // This class would have a Solution ref data member (prefSol) assigned using logic
    // similar to that used to assign the preferred magnitude (prefMag) to a Magnitude
    // ref data member in the Solution class. -aww
    //
    // Solution.prefor value is only set by 2 methods: dbaseSetPrefOr and toNewOriginRow
    //protected DataLong prefor    = new DataLong(); // moved to parent super class

    public DataLong orid         = new DataLong();
    public DataLong prefmag      = new DataLong();
    //public DataLong prefmec      = new DataLong(); // moved to parent
    public DataLong commid       = new DataLong();


    private long lastPreforId   = NullValueDb.NULL_LONG; // to store last set value of prefor

    protected long lastEvtPrefmag = NullValueDb.NULL_LONG; // to store last set value of event prefmag
    protected long lastOrgPrefmag = NullValueDb.NULL_LONG; // to store last set value of origin prefmag

    // flags not used
    //protected DataLong bogusFlag  = new DataLong(); // mapped to Solution.dummyFlag (ORIGIN table)
    //protected DataLong selectFlag = new DataLong(); // mapped to Solution.validFlag (EVENT table)
    /** boolean to tell us whether this is ANSI SQL or Oracle 10+ SQL **/
    protected boolean useANSISQL = false;
    /** True if this Solution was orginally read from the dbase. Used to know if
    *   a commit requires an 'insert' or an 'update'
    * */
    protected boolean fromDbase = false;
    protected boolean bumpSrcVersion = false; // -aww 2008/04/15
    private boolean commitOriginChanged = false; // -aww 2008/02/29

    private boolean newOrigin2Assoc = false; // -aww 2010/06/30
    private boolean isSubnetTrigger = false; // -aww 2011/08/16

    /** Flag true if AssocWaE rows for cloned event have been copied. */
    protected boolean wfsCloned = false;

    //private JasiDbReader jasiDataReader = new JasiDbReader(this); // if not static 
    //Can be static so long as parseResultSet() does not depend on state (like joinType in AmplitudeTN)
    //private static JasiDbReader jasiDataReader = new JasiDbReader();
    private static JasiDbReader jasiDataReader = new JasiDbReader("org.trinet.jasi.TN.SolutionTN");

    /*
    static {
      //jasiDataReader.setFactoryClassName(getClass().getName());
      setDebug(JasiDatabasePropertyList.debugTN);
      if (JasiDatabasePropertyList.debugSQL) jasiDataReader.setDebug(true);
    }
    */

    // The basic event/origin/netmag join
    // protected static final String sqlPrefix =
    //     "Select event.*, origin.*, netmag.* from event, origin, netmag ";

    protected static final String sqlPrefix =
        "Select "+                              // RH: removed Oracle query hint FIRST_ROWS
        Event.QUALIFIED_COLUMN_NAMES+","+
        Origin.QUALIFIED_COLUMN_NAMES+","+
        //OriginError.QUALIFIED_COLUMN_NAMES+","+
        "ORIGIN_ERROR.MAGSMALL,ORIGIN_ERROR.AZIINTER,ORIGIN_ERROR.DIPINTER,ORIGIN_ERROR.MAGINTER," +
        "ORIGIN_ERROR.AZILARGE,ORIGIN_ERROR.DIPLARGE,ORIGIN_ERROR.MAGLARGE," +
        NetMag.QUALIFIED_COLUMN_NAMES+
        ",UTIL.waveformChannelCount(Event.evid)" +
        ",UTIL.getWhoOrigin(Origin.orid)"+
        ",UTIL.getWhoNetMag(Netmag.magid)";
        //",UTIL.getNetworkRegion(Origin.lat,Origin.lon)" + // this function DOES NOT EXIST would requir schema change to gazetteer_region table
        //",EPREF.getComment(Event.commid)"+  // most are null, takes longer than doing secondary query lookup for only those commid not null 
        //" FROM event,origin,origin_error,netmag ";

    /*
     Join: note that we must do an "outer" join by using (+). Otherwise, an
     event that lacks an 'prefor' or 'prefmag' will NOT be seen. (see: Oracel8
     Bible, McCullough-Dieter, pg. 217).
    */
    protected static final String sqlJoin = sqlPrefix +
      //RH " FROM event,origin,origin_error,netmag " +
      " FROM event " + 
      " LEFT OUTER JOIN origin ON event.prefor=origin.orid" +
      " LEFT OUTER JOIN netmag ON event.prefmag=netmag.magid" +
      " LEFT OUTER JOIN origin_error ON origin.orid=origin_error.orid ";
      //" WHERE (event.prefor = origin.orid(+)) and (event.prefmag = netmag.magid(+)) ";
      //RH "WHERE (event.prefor=origin.orid(+)) AND (event.prefmag=netmag.magid(+)) AND (origin_error.orid(+)=origin.orid) ";

    protected static final String sqlAltJoin = sqlPrefix +
      //RH " FROM event,origin,origin_error,netmag " +
      " FROM event" +
      " JOIN origin ON event.evid=origin.evid" +
      " LEFT OUTER JOIN netmag ON origin.prefmag=netmag.magid" +
      " LEFT OUTER JOIN origin_error ON origin.orid=origin_error.orid ";
      //" WHERE (event.evid=origin.evid) and (origin.prefmag=netmag.magid(+)) ";
      //RH "WHERE (event.evid=origin.evid) AND (origin.prefmag=netmag.magid(+)) AND (origin_error.orid(+)=origin.orid) ";

    // NOTE: For "preferred origin" map we need to use the new eventprefor table here:
    protected static final String sqlAltPrefOrJoin = sqlPrefix +
      //RH " FROM event,origin,eventprefor,origin_error,netmag " +
      " FROM event" +
      " JOIN origin ON event.evid=origin.evid" +
      " JOIN eventprefor ON origin.orid=eventprefor.orid" +
      " LEFT OUTER JOIN netmag ON origin.prefmag = netmag.magid" +
      " LEFT OUTER JOIN origin_error ON origin.orid=origin_error.orid" +
      " WHERE eventprefor.evid=? ";
      //RH "WHERE (event.evid=origin.evid) AND (origin.orid=eventprefor.orid) AND (origin.prefmag=netmag.magid(+))" +
      //RH " AND (origin_error.orid(+)=origin.orid) AND (eventprefor.evid=?) ";

    /**
     *  Create a Solution object.
     * A new solution must have a unique id at the time it is created.
     */
    public SolutionTN() {
        //authority.setNull(true); // added, to force it to use the eventAuthority by default -aww 2011/08/17, removed 2011/09/21
        if (jasiDataReader != null) jasiDataReader.setDebug(JasiDatabasePropertyList.debugSQL);
        setDebug(JasiDatabasePropertyList.debugTN);
    }

    public static void setDebug(boolean tf) {
        debug = tf;
        if (jasiDataReader != null) jasiDataReader.setDebug(debug);
        PhaseListTN.debug = debug;
        AmpListTN.debug = debug;
        CodaListTN.debug = debug;
    }

    public int loadPrefMags() {
        return loadPrefMags(DataSource.getConnection());
    }

    public int loadPrefMags(Connection conn) {
        prefMagMap.clear(); // clear any prior mapping

        ArrayList aList = new ArrayList();
        PreparedStatement psPrefMag = null;
        ResultSet rs = null;
        final String sql = "SELECT MAGID FROM EVENTPREFMAG WHERE (EVID=?)";
        try {
            conn = DataSource.getConnection();
            psPrefMag = conn.prepareStatement(sql);
            psPrefMag.setLong(1, id.longValue());
            rs = psPrefMag.executeQuery();
            while (rs.next()) {
              aList.add(Long.valueOf(rs.getLong(1)) );
            }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, psPrefMag, debug);
            Utils.closeQuietly(rs, psPrefMag);
        }
        int count = aList.size();
        if (debug || JasiDatabasePropertyList.debugSQL) 
            System.out.println("DEBUG SolutionTN loadPrefMags count: " + count);
        if (count == 0) return 0;

        Magnitude magFactory = null; // holder for factory object which may be needed, or not.
        Magnitude mag = null; // holder for factory object which may be needed, or not.

        for (int idx=0; idx < count; idx++) {
            //System.out.println("DEBUG SolutionTN loadPrefMags altmagList start...");
            // try to find existing magitude instance in magList
            DataLong magid = new DataLong( ((Long)aList.get(idx)).longValue() );
            mag = (Magnitude) altMagList.getByIdentifier(magid); 
            if (mag == null) { // magnitude instance not already in list
              //System.out.println("DEBUG SolutionTN loadPrefMags make new mag ..."+id.toString());
              if (magFactory == null) magFactory = Magnitude.create(); // make a Magnitude factory
              mag = (Magnitude) magFactory.getById(magid); // create instance from dbase data, null return implies no data
              if (mag == null) continue; // not in database, skip - aww 02/08/2005
              //NOTE: not using mag.assign(this) below since it forces orid agreement overriding db value
              mag.sol = this; // associate mag with this Solution (but don't synch orid, keep the dbase orid)
            }
            //System.out.println("DEBUG SolutionTN loadPrefMags set prefmag of type...");
            setPrefMagOfType(mag); // null input is no-op, else adds input to map and to magList (if not in list)
        }
        // If RT code is not populating eventprefmag table (no eventprefmag entries) try
        // forcing instance event preferred magnitude into the prefMagMap - aww 06/12/2006
        if (count == 0) setPrefMagOfType(this.magnitude);
        //

        return prefMagMap.size(); // zero size implies no preferred magnitudes exist!
    }
/*
    public int loadPrefMags(Connection conn) {
        //
        prefMagMap.clear(); // clear any prior mapping

        //System.out.println("DEBUG SolutionTN loadPrefMags dbaseLookup start...");
        EventPrefMag [] dbasePrefMags = dbaseLookupEventPrefMags(conn); // get the prefmag data from the dbase
        //PrefMag [] dbasePrefMags = dbaseLookupOriginPrefMags(conn); // get the prefmag data from the dbase
        //System.out.println("DEBUG SolutionTN loadPrefMags dbaseLookup done...");

        Magnitude magFactory = null; // holder for factory object which may be needed, or not.
        Magnitude mag = null; // holder for factory object which may be needed, or not.

        int offset = EventPrefMag.MAGID;
        //int offset = PrefMag.MAGID;
        for (int idx=0; idx < dbasePrefMags.length; idx++) {
            Object id = dbasePrefMags[idx].getDataObject(offset);
            //System.out.println("DEBUG SolutionTN loadPrefMags altmagList start...");
            mag = (Magnitude) altMagList.getByIdentifier(id); // try to find existing magitude instance in magList
            if (mag == null) { // magnitude instance not already in list
              //System.out.println("DEBUG SolutionTN loadPrefMags make new mag ..."+id.toString());
              if (magFactory == null) magFactory = Magnitude.create(); // make a Magnitude factory
              mag = (Magnitude) magFactory.getById(id); // create instance from dbase data, null return implies no data
              if (mag == null) continue; // not in database, skip - aww 02/08/2005
              //NOTE: not using mag.assign(this) below since it forces orid agreement overriding db value
              mag.sol = this; // associate mag with this Solution (but don't synch orid, keep the dbase orid)
            }
            //System.out.println("DEBUG SolutionTN loadPrefMags set prefmag of type...");
            setPrefMagOfType(mag); // null input is no-op, else adds input to map and to magList (if not in list)
        }
        // If RT code is not populating eventprefmag table (no eventprefmag entries) try
        // forcing instance event preferred magnitude into the prefMagMap - aww 06/12/2006
        if (dbasePrefMags.length == 0) setPrefMagOfType(this.magnitude);
        //

        return prefMagMap.size(); // zero size implies no preferred magnitudes exist!
    }
*/

/*
    private PrefMag [] dbaseLookupOriginPrefMags(Connection conn) {
        PrefMag aPrefMag = new PrefMag(conn);
        PrefMag [] prefMagArray = aPrefMag.getRowsByOriginId(prefor.longValue());
        return (prefMagArray == null) ? new PrefMag[0] : prefMagArray;
    }

    private EventPrefMag [] dbaseLookupEventPrefMags(Connection conn) {
        EventPrefMag aPrefMag = new EventPrefMag(conn);
        //System.out.println("DEBUG SolutionTN dbaseLookupEventPrefMags : " + id.longValue());
        EventPrefMag [] prefMagArray = aPrefMag.getRowsByEventId(id.longValue());
        //if (prefMagArray == null) System.out.println("DEBUG SolutionTN dbaseLookupEventPrefMags getRow is null!");
        //else System.out.println("DEBUG SolutionTN dbaseLookupEventPrefMags getRow size: " + prefMagArray.length);
        return (prefMagArray == null) ? new EventPrefMag[0] : prefMagArray;
    }
*/

    /**
     * Set value of 'waveRecords' for this Solution by examining the datasource, if available.
     */
    public int countWaveforms() {
        if (waveRecords.isNull()) {
            waveRecords.setValue( countWaveforms(id.longValue()) );
        }
        return waveRecords.intValue();
    }

    /**
     * Number of AssocWaE rows found for input evid by using datasource connection, if none or no connection, returns 0.
     */
    public static int countWaveforms(long id) { // made static method -aww 2009/12/17
        // We can't do this at instatiation time because the Connection is not created yet.
        Connection conn = DataSource.getConnection();
        if (conn == null) return 0;

        PreparedStatement sm = null;

        // make the query
        int count = 0;
        ResultSet rs = null;
        //String sql = "Select COUNT(wfid) from AssocWaE where evid=?"; // replaced by below for case of multiple wfid per channel -aww 2009/04/15
        final String sql =
            "SELECT COUNT(*) FROM (SELECT DISTINCT w.sta,w.net,w.seedchan,w.location FROM Waveform w,AssocWaE a WHERE (w.wfid=a.wfid) AND (a.evid=?))";
        try {
            sm = conn.prepareStatement(sql);
            sm.setLong(1, id);
            rs = sm.executeQuery();
            if (rs.next()) {
              count = rs.getInt(1);
            }
        }
        catch (SQLException ex) {
            System.err.println("SolutionTN countWaveforms() SQLException: " + ex.getMessage());
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, sm, debug);
          Utils.closeQuietly(rs, sm);
        }
        return count;
    }
    
    /**
     * Get the next valid solution (event) sequence number (evseq).
     * Returns the unique value. Returns 0 if there is an error.
     */
    public long setUniqueId() {
        long newId = getNextId();
        //id.setMutable(true);
        id.setValue(newId);
        return newId;
    }

    private long assignNewOrid() {
        long anIdValue =
            SeqIds.getNextSeq(DataSource.getConnection(),
                    org.trinet.jdbc.table.Origin.SEQUENCE_NAME);
        //orid.setMutable(true);
        orid.setValue(anIdValue);
        return getOridValue();
    }
    private long assignNewMagid() { // NOT USED: BEWARE conflict with magnitude obj id 
        long anIdValue =
            SeqIds.getNextSeq(DataSource.getConnection(),
                    org.trinet.jdbc.table.NetMag.SEQUENCE_NAME);
        //prefmag.setMutable(true);
        prefmag.setValue(anIdValue);
        return getMagidValue();
    }

    /**
     * Return the evid set value of the eventrow. Return 0 if null or not valid.
     */
    public long getEvidValue() {
      return (id.isValidNumber()) ? id.longValue() : 0l;
    }
    /**
     * Return the orid set in orid in the eventrow or originrow.
     */
    public long getOridValue() {
      return (orid.isValidNumber()) ? orid.longValue() : 0l;
    }
    public Object getOriginIdentifier() {return orid;}

    public DataLong getPrefor() {
        return prefor;
    }
    /**
     * Return the preferred origin id of event.
     */
    public long getPreforValue() {
        return (prefor.isValidNumber()) ? prefor.longValue() : 0l;
    }
    public DataLong getPrefmag() {
        return prefmag;
    }
    public long getPrefmagValue() {
        return (prefmag.isValidNumber()) ? prefmag.longValue() : 0l;
    }
    public long getMagidValue() {
      return (prefmag.isValidNumber()) ? prefmag.longValue() : 0l;
    }
    public DataLong getPrefmec() {
        return prefmec;
    }
    public long getPrefmecValue() {
        return (prefmec.isValidNumber()) ? prefmec.longValue() : 0l;
    }
    public DataLong getCommid() {
        return commid;
    }
    public long getCommidValue() {
        return (commid.isValidNumber()) ? commid.longValue() : 0l;
    }

    /*
     * Returns the value of prefmag, if a valid value is not already defined
     * a new id is assigned from a database sequence and returned.
     * @deprecated NOT USED
     * @see #getMagidValue()
     * @see #assignNewMagid()
    protected long getMagid() {
      long value = getMagidValue();
      return (value > 0l) ? value : assignNewMagid();
    }
    */

    /**
     * Returns the current value of orid, if a valid value is not already defined
     * a new id is assigned from a database sequence and returned.
     * @deprecated NOT USED
     * @see #getOridValue()
     * @see #assignNewOrid()
    */
    protected long getOrid() {
      long value = getOridValue();
      return (value > 0l) ? value : assignNewOrid();
    }

    /** Set the Magnitude object of this Solution. Overrides
    * Solution.setPreferredMagnitude in order to update the value of prefmag. */
    public void setPreferredMagnitude(Magnitude mg) {
        super.setPreferredMagnitude(mg);
        // prefmag aliased to mag, so if mag changes value internally it is available here
        //lastOrgPrefmag = prefmag.longValue(); // removed reset last value of id here - aww 2008/04/02
        prefmag = mg.magid; // aliased
        if ( lastOrgPrefmag != mg.magid.longValue() || lastEvtPrefmag != mg.magid.longValue() ) setNeedsCommit(true); // added -aww 2008/04/02
        //System.out.println("setpreferredmagnitude prefmag.isUpdate: " + prefmag.isUpdate() + " hasChanged: " + eventHasChanged());
        //System.out.println("DEBUG SolutionTN after setPreferredMagnitude lastEvtPrefmag: " + lastEvtPrefmag +
        //" lastOrgPrefmag: " + lastOrgPrefmag + " prefmagId: " + prefmag.longValue());
    }

    /**
     * Set the internal copy of the Event table object for later use.
     */
    protected void setEventRow(Event evt) {
        eventRow = evt;
    }

    /**
     * Set the internal copy of the Origin table object for later use.
     */
    protected void setOriginRow(Origin org)  {
        originRow = org;
    }


    /** Set the event type to a valid jasi eventType. If the type passed in 'type'
     * is not valid 'false' is returned and the eventType is not changed.
     * @see org.trinet.jasi.EventTypeMap */
    public boolean setEventType(String type) {
        if (! EventTypeMap3.isValidJasiType(type)) return false;
        // set only if it's a change because setValue() is an update
        // which can cause a dbase update upon commit
        if (! eventType.equalsValue(type)) {
          eventType.setValue(type);
          bumpSrcVersion = true; // version bumped with type change -aww 2008/04/15
        }
        return true;
    }

    public boolean setOriginGType(String type) {
        if (! GTypeMap.isValidJasiType(type)) return false;
        // set only if it's a change because setValue() is an update
        // which can cause a dbase update upon commit
        if (! gtype.equalsValue(type)) {
          gtype.setValue(type);
          bumpSrcVersion = true; // version bumped with type change ???
        }
        return true;
    }

    /** Return the local implentation's event type string.
     * @see org.trinet.jasi.EventTypeMap
     * */
    public String getEventTypeString() {
        return eventType.toString();
    }

    public int loadWaveformList(boolean clearList) {
        if (clearList && waveformList.size() > 0) waveformList.clear(); // fires listeners notify
        List aList = (List) AbstractWaveform.createDefaultWaveform().getBySolution(this);
        // below assumes above getBySolution() does NOT add list elements.
        addWaveforms(aList);
        return waveformList.size();
    }

    public int loadPhaseList(boolean clearList) {
        if (clearList && phaseList.size() > 0) phaseList.clear(); // fires listeners notify
        // assumes getBySolution() does NOT associate list elements.
        List aList = (List) Phase.create().getBySolution(this, null, false);
        if (aList == null) return 0; // not in database - aww 02/08/2005
        //associate(aList); // 10/03 could try this instead aww
        // below equivalent to this.fastAssociateAll(aList)
        // doesn't check for duplicates (addOrReplace vs. add):
        phaseList.assignListTo(aList, this);
        phaseList.fastAddAll(aList); // no check for nulls or dupes
        //phaseList.addAll(aList);   // slower, does checks
        //phaseList.setStale(true);  // not needed with list listeners
        return phaseList.size();
    }
    public int loadPrefMagAmpList(boolean clearList) {
        if (clearList && ampList.size() > 0) ampList.clear(); // fires listeners notify
        if ( prefMagMap.size() == 0) loadPrefMags();
        Iterator iter = prefMagMap.values().iterator();
        Amplitude amp = Amplitude.create();
        ArrayList aList = null;
        Magnitude mag = null;
        while (iter.hasNext()) {
            mag = (Magnitude) iter.next();
            if (mag.isAmpMag()) {
                // getByMagnitude() below does NOT associate list elements with magnitude (false)
                // else event preferred magnitude replaces any existing Solution reading list elements
                // by equivalence not id!!! 
                aList = (ArrayList) amp.getByMagnitude(mag,null,false);
                if (aList != null && aList.size() > 0) {
                    ampList.assignListTo(aList, this);
                    ampList.ensureCapacity(ampList.size() + aList.size());
                    ampList.addOrReplaceAllById(aList);
                }
            }
        }
        return ampList.size();
    }
    public int loadAmpList(boolean clearList) {
        // NOTE: With coupled list listeners clearing solution list here might also CLEAR magnitude list!
        if (clearList && ampList.size() > 0) ampList.clear(); // fires listeners notify
        // assumes getBySolution() does NOT associate list elements.
        List aList = (List) Amplitude.create().getBySolution(this, null, false);
        if (aList == null) return 0; // not in database - aww 02/08/2005
        // associate(aList); // 10/03 could try this instead aww
        // below equivalent to this.fastAssociateAll(aList)
        // doesn't check for duplicates (addOrReplace vs. add):
        ampList.assignListTo(aList, this);
        ampList.fastAddAll(aList); // no check for nulls or dupes
        //ampList.addAll(aList);   // slower, does checks
        return ampList.size();
    }
    public int loadPrefMagCodaList(boolean clearList) {
        if (clearList && codaList.size() > 0) codaList.clear(); // fires listeners notify
        if ( prefMagMap.size() == 0) loadPrefMags();
        Iterator iter = prefMagMap.values().iterator();
        org.trinet.jasi.Coda coda = org.trinet.jasi.Coda.create();
        ArrayList aList = null;
        Magnitude mag = null;
        while (iter.hasNext()) {
            mag = (Magnitude) iter.next();
            if (mag.isCodaMag()) {
                // getByMagnitude() below does NOT associate list elements with magnitude (false)
                // else event preferred magnitude replaces any existing Solution reading list elements
                // by equivalence not id!!! 
                aList = (ArrayList) coda.getByMagnitude(mag,null,false);
                if (aList != null && aList.size() > 0) {
                    codaList.assignListTo(aList, this);
                    codaList.ensureCapacity(codaList.size() + aList.size());
                    codaList.addOrReplaceAllById(aList);
                }
            }
        }
        return codaList.size();
    }
    public int loadCodaList(boolean clearList) {
        // NOTE: With coupled list listeners clearing solution list here might also CLEAR magnitude list!
        if (clearList && codaList.size() > 0) codaList.clear(); // fires listeners notify
        // assumes getBySolution() does NOT associate list elements.
        List aList = (List) org.trinet.jasi.Coda.create().getBySolution(this, null, false);
        if (aList == null) return 0; // not in database - aww 02/08/2005
        //associate(aList); // 10/03 could try this instead aww
        // below equivalent to this.fastAssociateAll(aList)
        // doesn't check for duplicates (addOrReplace vs. add):
        codaList.assignListTo(aList, this);
        codaList.fastAddAll(aList); // no check for nulls or dupes
        //codaList.addAll(aList); // slower, does checks
        return codaList.size();
    }
    public int loadMagAmpList(boolean clearList) {
        // NOTE: With coupled list listeners clearing solution list here might also CLEAR magnitude list!
        if (clearList && ampList.size() > 0) ampList.clear(); // fires listeners notify
        if (magnitude == null || ! magnitude.isAmpMag()) return 0;
        //delegate to magnitude to load also loads associated solution's ampList:
        return magnitude.loadReadingList(clearList);
    }
    public int loadMagCodaList(boolean clearList) {
        // NOTE: With coupled list listeners clearing solution list here might also CLEAR magnitude list!
        if (clearList && codaList.size() > 0) codaList.clear(); // fires listeners notify
        if (magnitude == null || ! magnitude.isCodaMag()) return 0;
        return magnitude.loadReadingList(clearList);
    }
    /** Clear the MagList if input argument is true.
     *  Adds all Magnitudes retrieved from the DataSource which are affiliated with
     *  this Solution including the preferred magnitude.
     *  @see #loadAlternateMagnitudes()
     *  */
    public int loadMagList(boolean clearList) {
        DataLong prefMagId = (DataLong) getPreferredMagId();
        if (clearList && altMagList.size() > 0) altMagList.clear(); // remove contents;
        // assumes getBySolution() does NOT associate list elements.
        List aList = (List) Magnitude.create().getBySolution(this, null, false);
        if (aList != null && aList.size() > 0) { // mags for prefor in db - aww 04/26/2005
          // equivalent to this.fastAssociateAll(aList) :
          altMagList.assignListTo(aList, this);
          altMagList.fastAddAll(aList); // no check for nulls or dupes
          //altMagList.addAll(aList); // slower, does checks
          // Force a reset of preferred magnitude:
        }
        // Must add known prefmags to maglist, since they may not have event.prefor association
        // and be absent from list created above -aww
        loadPrefMags(); // load added 07/28/2004 -aww

        magnitude = null; // null current prefMag reference 06/06/04 aww
        setPreferredMagFromIdInList(prefMagId); // reset to new list instance

        return altMagList.size();
    }
    /** Does NOT clear the MagList since the preferred magnitude may be on the list,
     *  only adds Magnitudes retrieved from the DataSource which are affiliated with
     *  this Solution and whose identifiers do not match that of the preferred magnitude.
     *  @see #loadMagList()
     *  */
    public int loadAlternateMagnitudes() {
        // assumes getBySolution() does NOT associate list elements.
        List aList = (List) Magnitude.create().getBySolution(this, null, false);
        if (aList == null) return 0; // not in database - aww 02/08/2005
        Magnitude aMag = null;
        if (magnitude != null) {
          for (int idx = 0; idx < aList.size(); idx++) {
             aMag = (Magnitude) aList.get(idx);
             if (aMag.idEquals(magnitude)) {
               aList.remove(idx); // ids, not objects matter here
               break;
             }
          }
        }
        //check for no alternates
        if (aList.isEmpty()) return 0;
        // equivalent to this.fastAssociateAll(aList) :
        altMagList.assignListTo(aList, this);
        altMagList.fastAddAll(aList); // no check for nulls or dupes
        //altMagList.addAll(aList); // slower, does checks
        return aList.size();
    }
    public int loadAltSolList(boolean clearList) {
        if (clearList && altSolList.size() > 0) altSolList.clear();  // remove contents.
        // No listeners or associations forced here, yet:
        altSolList.addAll(Solution.create().getAlternatesById(getId().longValue()));
        return altSolList.size();
    }

    public Collection getByState(String state) {
        StringBuffer sb = new StringBuffer(1024);
        sb.append(sqlJoin); // must do a join of Event/Origin/NetMag to get all the info we need
        //sb.append(" and event.evid in (select id from pcs_state where state='").append(state).append("')");
        sb.append("WHERE event.evid IN (SELECT id FROM pcs_state WHERE state=?)");
        //return ((JasiDbReader) getDataReader()).getBySQL(DataSource.getConnection(), sb.toString());
        ArrayList aList = null;
        PreparedStatement psByState = null;
        final String sql = sb.toString();
        try {
            Connection conn = DataSource.getConnection();
            psByState = conn.prepareStatement(sql);
            psByState.setString(1, state);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByState);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, psByState, debug);
            Utils.closeQuietly(psByState);
        }
        return aList;
    }

    /**
     * Returns the Solution with this ID number from the data source.
     * Uses default DataSource.
     * Returns null if no event is found.
     * For the NCDC v1.5 schema ID is the 'evid' not the 'orid'.
     */
    public Solution getById(long evid) {
        return getById(DataSource.getConnection(), evid);
    }
    /**
     * Returns the Solution with this ID number from the data source.
     * Returns null if no event is found.
     * For the NCDC v1.5 schema ID is the 'evid' not the 'orid'.
     */
    public Solution getById(Connection conn, long evid) {
        // must do a join of Event/Origin/NetMag to get all the info we need
        StringBuffer sb = new StringBuffer(1024);
        //sb.append(sqlJoin);
        //sb.append(" and (event.evid = ").append(evid).append(")");
        //ArrayList aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, sb.toString());
        sb.append(sqlJoin).append("WHERE (event.evid=?)");
        ArrayList aList = null;
        PreparedStatement psByEvid = null;
        final String sql = sb.toString();
        try {
            psByEvid = conn.prepareStatement(sql);
            psByEvid.setLong(1, evid);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByEvid);
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(conn, ex, "Database Error", "Error retrieving an event by event id.\n");
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, psByEvid, debug);
            try {
                if (psByEvid != null) {
                    psByEvid.close();
                }
            } catch (SQLException ex2) {
                JiggleExceptionHandler.handleDbException(conn, ex2);
            }
        }

        return (aList == null || aList.isEmpty()) ? null : (Solution) aList.get(0);
    }

    public Collection getPreforOfTypeById(long evid) {
        return getPreforOfTypeById(DataSource.getConnection(), evid);
    }
    public Collection getPreforOfTypeById(Connection conn, long evid) {
        StringBuffer sb = new StringBuffer(1024);
        sb.append(sqlAltPrefOrJoin);
        sb.append("ORDER BY origin.lddate");
        ArrayList aList = null;
        PreparedStatement psByEvid = null;
        final String sql = sb.toString();
        try {
            psByEvid = conn.prepareStatement(sql);
            psByEvid.setLong(1, evid);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByEvid);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, psByEvid, debug);
            Utils.closeQuietly(psByEvid);
        }

        return aList;
    }

    public Collection getAllById(long evid) {
        return getAlternatesById(DataSource.getConnection(), evid, true);
    }
    public Collection getAllById(Connection conn, long evid) {
        return getAlternatesById(conn, evid, true);
    }

    public Collection getAlternatesById(long evid) {
        return getAlternatesById(DataSource.getConnection(), evid, false);
    }
    public Collection getAlternatesById(Connection conn, long evid) {
        return getAlternatesById(conn, evid, false);
    }

    private Collection getAlternatesById(Connection conn, long evid, boolean includePrefor) {
        // must do a join of Event/Origin/NetMag to get all the info we need
        StringBuffer sb = new StringBuffer(1024);
        //sb.append(sqlAltJoin);
        //sb.append(" and (origin.orid <> event.prefor) and (event.evid = ");
        //sb.append(evid).append(")");
        //return ((JasiDbReader) getDataReader()).getBySQL(conn, sb.toString());
        sb.append(sqlAltJoin);
        sb.append("WHERE (event.evid=?)");
        if (! includePrefor) sb.append(" AND (origin.orid<>event.prefor)");
        sb.append(" ORDER BY origin.lddate desc, origin.orid desc");
        ArrayList aList = null;
        PreparedStatement psByEvid = null;
        final String sql = sb.toString();
        try {
            psByEvid = conn.prepareStatement(sql);
            psByEvid.setLong(1, evid);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByEvid);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, psByEvid, debug);
            Utils.closeQuietly(psByEvid);
        }

        return aList;
    }

    public Solution getByPreferredOrid(long orid) {
        return getByPreferredOrid(DataSource.getConnection(), orid);
    }
    public Solution getByPreferredOrid(Connection conn, long orid) {
        // must do a join of Event/Origin/NetMag to get all the info we need
        StringBuffer sb = new StringBuffer(1024);
        //sb.append(sqlJoin);
        //sb.append(" and (event.prefor = ").append(orid).append(")");
        sb.append(sqlJoin).append("WHERE (event.prefor=?)");

        //ArrayList aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, sb.toString());
        ArrayList aList = null;
        PreparedStatement psByOrid = null;
        final String sql = sb.toString();
        try {
            psByOrid = conn.prepareStatement(sql);
            psByOrid.setLong(1, orid);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByOrid);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, psByOrid, debug);
            Utils.closeQuietly(psByOrid);
        }
        return (aList == null || aList.isEmpty()) ? null : (Solution) aList.get(0);
    }

    /**
     * Returns array of Solutions within this time window.
     * Returns null if no event is found.
     */
    public Collection getByTime(double start, double stop) {
        return getByTime(DataSource.getConnection(), start, stop);
    }

    /**
     * Returns array of Solutions within this time window.
     * Returns null if no event is found.
     */
    public Collection getByTime(Connection conn, double start, double stop) {
        // must do a join of Event/Origin/NetMag to get all the info we need
        //   String sql = sqlJoin + "and Origin.DATETIME BETWEEN " +
        //   StringSQL.valueOf(start) + " AND " + StringSQL.valueOf(stop) +
        StringBuffer sb = new StringBuffer(1024);
        //sb.append(sqlJoin).append(" and ").append(getTimeSpanSQL(start, stop));
        //sb.append(" order by Origin.Datetime");
        //ArrayList aList = (ArrayList)((JasiDbReader) getDataReader()).getBySQL(conn, sb.toString());
        sb.append(sqlJoin);
        sb.append("WHERE (Origin.DATETIME BETWEEN TRUETIME.putEpoch(?,'UTC') AND TRUETIME.putEpoch(?,'UTC'))");
        sb.append(" ORDER BY Origin.Datetime");
        ArrayList aList = null;
        PreparedStatement psByTime = null;
        final String sql = sb.toString();
        try {
            psByTime = conn.prepareStatement(sql);
            psByTime.setDouble(1, start);
            psByTime.setDouble(2, stop);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByTime);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, psByTime, debug);
            Utils.closeQuietly(psByTime);
        }

        return aList;
    }

    /**
     * Returns array of"valid" Solutions within this time window.
     * Some data sets contain solutions that have duplicates, interim, or bogus entries.
     * This method filters those out and returns only VALID solutions.
     */
    public Collection getValidByTime(double start, double stop) {
        return getValidByTime(DataSource.getConnection(), start, stop);
    }
    /**
    * Returns array of"valid" Solutions within this time window.
    * Some data sets contain solutions that have duplicates, interim, or bogus entries.
    * This method filters those out and returns only VALID solutions.
    * */
    public Collection getValidByTime(Connection conn, double start, double stop) {
        StringBuffer sb = new StringBuffer(1024);
        //sb.append(sqlJoin).append(" and ").append(getTimeSpanSQL(start, stop));
        //sb.append(" and (event.selectflag = 1) ");
        //sb.append(" and not (origin.bogusflag=1 and origin.rflag='A')");
        //sb.append(" order by Origin.Datetime");
        //return ((JasiDbReader) getDataReader()).getBySQL(conn, sb.toString());
        sb.append(sqlJoin);
        sb.append("WHERE (event.selectflag=1) AND NOT (origin.bogusflag=1 AND origin.rflag='A')");
        sb.append(" AND (Origin.DATETIME BETWEEN TRUETIME.putEpoch(?,'UTC') AND TRUETIME.putEpoch(?,'UTC'))");
        sb.append(" ORDER BY Origin.Datetime");
        ArrayList aList = null;
        final String sql = sb.toString();
        PreparedStatement psByTime = null; 
        try {
            psByTime = conn.prepareStatement(sql);
            psByTime.setDouble(1, start);
            psByTime.setDouble(2, stop);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByTime);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, psByTime, debug);
            Utils.closeQuietly(psByTime);
        }

        return aList;
    }

    /** Return the part of an sql query that specifies a time window. */
    private static String getTimeSpanSQL(double start, double stop) {
        //Kludge for NCEDC request with leap seconds added
        //start = DataSource.nominalToDbTimeBase(start);
        //stop  = DataSource.nominalToDbTimeBase(stop);
        //end of patch - aww removed 01/24/2008 for leap secs
        StringBuffer sb = new StringBuffer(80);
        /*
        sb.append(" (Origin.DATETIME BETWEEN ");
        sb.append(StringSQL.valueOf(start));
        sb.append(" and ");
        sb.append(StringSQL.valueOf(stop));
        sb.append(")");
        */
        sb.append(" (Origin.DATETIME BETWEEN TRUETIME.putEpoch(");
        sb.append(StringSQL.valueOf(start));
        sb.append(",'UTC') AND TRUETIME.putEpoch(");
        sb.append(StringSQL.valueOf(stop));
        sb.append(",'UTC') )");
        return sb.toString();
    }

    /**
     * Return array of solutions that match the properties defined in the
     * EventSelectionProperties object.
     * */
    public Collection getByProperties(EventSelectionProperties props) {
        return getByProperties(DataSource.getConnection(), props);
    }

    public Solution refresh(EventSelectionProperties props) {
        long evid = getEvidValue();
        if (evid < 1l) return null; // or this?
        if (props == null) return getById(evid);

        props.setProperty("eventId", String.valueOf(evid));
        Collection c = getByProperties(DataSource.getConnection(), props);
        props.remove("eventId");
        return (c != null && c.size() > 0) ?  (Solution) c.iterator().next() : null;
    }

    /**
     * Return array of solutions that match the properties defined in the
     * EventSelectionProperties object.
     * */
    public Collection getByProperties(Connection conn,
                                       EventSelectionProperties props) {
        if (EventSelectionProperties.REGION_POLYLIST.equals(props.getRegionType())) {
          return getByPreparedStatementRegionProps(conn, props);
        }
        else if (EventSelectionProperties.REGION_POLYGON.equals(props.getRegionType())) {
          return getByPreparedStatementRegionProps(conn, props);
        }
        return getByStatementProps(conn, props);
    }

    private String getRowLimitClause(EventSelectionProperties props, boolean useANSISQL) {
        StringBuffer sb = new StringBuffer(64);
        int maxRows = props.getMaxCatalogRows();
        if (maxRows >= 0) {
            // Limit the returned rows from the earliest to maxCatalogRows
            if (debug) System.out.println("DEBUG: SolutionTN getByProperties row count limit set to maxCatalogRows = " + maxRows);
            if (useANSISQL) {
                sb.append(" OFFSET 0 ROWS FETCH NEXT ").append(maxRows).append(" ROWS ONLY");
            }
            else {
                // Oracle before version 12c
                sb.append(" AND (ROWNUM<=").append(maxRows).append(")");
            }
        }
        return sb.toString();
    }
    private Collection getByStatementProps(Connection conn,
                                       EventSelectionProperties props) {
        try {
            DatabaseMetaData db_info = conn.getMetaData();
            useANSISQL = (db_info.getDatabaseProductName().toString().equalsIgnoreCase("postgresql"));
            System.out.println("DEBUG in SolutionTN: " + db_info.getDatabaseProductName() +
                               " version: " + db_info.getDatabaseProductVersion() + " standard SQL is " + useANSISQL);
        } catch (SQLException ex) {
           ex.printStackTrace();
        }
        StringBuffer sqlWhereConditions = new StringBuffer(1024);
        if (! useANSISQL ) {
            sqlWhereConditions.append(getRowLimitClause(props, useANSISQL));
        }
        sqlWhereConditions.append(getEventTableWhereCondition(props));  // leading " and " if any
        sqlWhereConditions.append(getOriginTableWhereCondition(props)); // leading " and " if any
        sqlWhereConditions.append(getNetmagTableWhereCondition(props)); // leading " and " if any
        sqlWhereConditions.append(" ORDER BY Origin.Datetime ASC");
        if ( useANSISQL ) {
            sqlWhereConditions.append(getRowLimitClause(props, useANSISQL));
        }
        return getWherePrepared(sqlWhereConditions.toString(), props);
    }

    // Need to prepare sql statement context for reference to an ARRAY of latlon objects
    // so have to construct sql string with prepared statement syntax.
    public static int createRegionByCallableStatement(Connection conn, String name, LatLonZ [] llz) {
        //OracleCallableStatement ps = null;
        CallableStatement ps = null;
        int status = 0;
        final String sql = "{ ?=call geo_region.create_region(?,?) }";
        try {
          //oracle.sql.ARRAY myARRAY = (oracle.sql.ARRAY) getPolygonARRAY(llz, conn); 
          java.sql.Array myARRAY = (java.sql.Array) getPolygonARRAY(llz, conn); 
          // Begin coding SQL text for selection by "polygon" using a stored function
          ps = (CallableStatement) conn.prepareCall(sql);
          ps.registerOutParameter(1, java.sql.Types.INTEGER);
          ps.setString(2, name);
          //ps.setARRAY(3, myARRAY);
          ps.setArray(3, myARRAY);
          ps.executeUpdate();
          status = ps.getInt(1);
        }  
        catch (SQLException ex) {
          ex.printStackTrace();
        }
        catch (MissingPropertyException ex) {
          ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, ps, debug);
          Utils.closeQuietly(ps);
        }
        return status;
    }

    private static double[] getPoints(final String s) {
        double x, y;
        // s = POLYGON((x1 y1, xN yN))
        // ll = { lat1=y1, lon1=x1, latN=yN, lonN=xN }
        double[] ll = null;
        try {
            int endIndex, index;
            int beginIndex = s.indexOf(POLYGON_TEXT);
            if (beginIndex != -1) {
                int size = 1;
                beginIndex += POLYGON_TEXT.length();
                // determine the number of latitude and longitude values
                for (index = beginIndex; index < s.length(); index++) {
                    switch (s.charAt(index)) {
                    case ' ':
                    case ',':
                        size++;
                        break;
                    }
                }
                ll = new double[size];
                int lli = 0;
                while ((endIndex = s.indexOf(',', beginIndex)) != -1) {
                    // separate latitude and longitude
                    index = s.indexOf(' ', beginIndex);
                    // x
                    x = Double.parseDouble(s.substring(beginIndex, index));
                    // y
                    y = Double.parseDouble(s.substring(index, endIndex));
                    ll[lli++] = y;
                    ll[lli++] = x;
                    beginIndex = endIndex + 1;
                }
                endIndex = s.length() - 2;
                // separate latitude and longitude
                index = s.indexOf(' ', beginIndex);
                // x
                x = Double.parseDouble(s.substring(beginIndex, index));
                // y
                y = Double.parseDouble(s.substring(index, endIndex));
                ll[lli++] = y;
                ll[lli++] = x;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            ll = null;
        }
        return ll;
    }

    // Need to prepare sql statement context for reference to an ARRAY of latlon objects
    // so have to construct sql string with prepared statement syntax.
    public static double [] getRegionByName(Connection conn, String name) {
        double[] ll = null;
        final String sql = "select ST_AsText(border) from gazetteer_region WHERE name=? LIMIT 1";
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(sql);
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                ll = getPoints(rs.getString(1));
            } else {
                System.err.printf("SolutionTN: gazetteer_region not found (name='%s')\n", name);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            JasiDatabasePropertyList.debugSQL(sql, ps, debug);
            Utils.closeQuietly(ps);
        }
        return ll;
    }

    public static LatLonZ getLocationOfPlace(Connection conn, String name) {
        //OracleCallableStatement ps = null;
        CallableStatement ps = null;
        LatLonZ llz = null;
        final String sql = "{ ?=call geo_region.get_location_of(?) }";
        try {
          // Begin coding SQL text for selection by "point" using a stored function
          ps = (CallableStatement) conn.prepareCall(sql);
          ps.registerOutParameter(1, oracle.jdbc.OracleTypes.STRUCT,"PUBLIC.LATLON"); // used to be CODE. -aww 2008/04/22
          ps.setString(2, name);
          ps.executeUpdate();
          Map map = conn.getTypeMap();
          map.put("PUBLIC.LATLON", OracleLatLon.class); // used to be CODE. -aww 2008/04/22
          OracleLatLon oll = (OracleLatLon) ps.getObject(1);
          if (oll == null) return null;

          llz = new LatLonZ( oll.getLat().doubleValue(), oll.getLon().doubleValue(), 0. );
        }  
        catch (SQLException ex) {
          ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, ps, debug);
          Utils.closeQuietly(ps);
        }
        return llz;
    }

    private void loadAttribution() {
        Connection conn = DataSource.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        final String sql = "SELECT refer FROM credit WHERE (id=?) AND tname='ORIGIN'";
        try {
          // Begin coding SQL text for selection by "point" using a stored function
          stmt = conn.prepareStatement(sql);
          stmt.setLong(1, prefor.longValue());
          stmt.executeQuery();
          rs = stmt.getResultSet();
          String attrib = null;
          if (rs.next()) attrib = rs.getString(1);
          if (attrib != null) setWho(attrib);
        }  
        catch (SQLException ex) {
          ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, stmt, debug);
          Utils.closeQuietly(rs, stmt);
        }
    }


    // Need to prepare sql statement context for reference to an ARRAY of latlon objects
    // so have to construct sql string with prepared statement syntax.
    private Collection getByPreparedStatementRegionProps(Connection conn, EventSelectionProperties props) {
        PreparedStatement ps = null;
        Collection coll = null;
        StringBuffer joinSql = new StringBuffer(1024);
        StringBuffer whereSql = new StringBuffer(1024);
        String sql = null;
        try {
            joinSql.append(sqlJoin);
            whereSql.append(getEventTableWhereCondition(props));  // leading " and " if any
            whereSql.append(getOriginTableWhereCondition(props)); // leading " and " if any
            whereSql.append(getNetmagTableWhereCondition(props)); // leading " and " if any

            TimeSpan span = props.getTimeSpan();
            // Single polygon ?
            if (EventSelectionProperties.REGION_POLYGON.equals(props.getRegionType())) { // old-style single region
                LatLonZ[] llz = getPolygonArray(props);
                // Begin coding SQL text for selection by "polygon" using a stored function
                whereSql.append(" AND (geo_region.inside_border(?,Origin.lat,Origin.lon)");
                boolean reject = props.getBoolean("regionExcluded");
                whereSql.append((reject ? "=0 )" : "<>0 )"));  // returns +/- values
                whereSql.append(" ORDER BY Origin.Datetime ASC");

                joinSql = this.setEventSql(joinSql, whereSql, props.getMaxCatalogRows());
                ps = conn.prepareStatement(joinSql.toString());
                ps.setObject(1, span.getStart(), Types.NUMERIC);
                ps.setObject(2, span.getEnd(), Types.NUMERIC);
                ps.setObject(3, getPolygonARRAY(llz, conn), Types.ARRAY);
            } else { // assume a polylist of multiple regions by mag range

                EventSelectionProperties.EventRegion[] eRegions = props.getEventRegions();

                if (eRegions != null && eRegions.length > 0) {

                    DoubleRange defaultMagRange = props.getDoubleRange("region.default.magValueRange");
                    if (defaultMagRange == null) defaultMagRange = new DoubleRange(-9., 9.);

                    DoubleRange defaultDepthRange = props.getDoubleRange("region.default.orgDepthRange");
                    if (defaultDepthRange == null) defaultDepthRange = new DoubleRange(-9., 999.);

                    boolean defaultIncludeNullMag = props.getBoolean("region.default.includeNullMag", false);
                    boolean defaultInclude = props.getBoolean("region.default.include", true);
                    boolean includeNullMag = false;

                    // As implemented, null origin depth and/or null mag value default to -9. value, i.e. reject
                    whereSql.append(" AND (1=(CASE");
                    for (int idx = 0; idx < eRegions.length; idx++) {

                        includeNullMag = eRegions[idx].includeNullMag;

                        //sb.append(" WHEN ( (geo_region.inside_border(?,Origin.lat,Origin.lon)<>0) ");
                        whereSql.append(" WHEN ( (geo_region.inside_border(?,Origin.lat,Origin.lon)<>0) ");
                        whereSql.append(" AND (COALESCE(Origin.mdepth,-9.) BETWEEN "); // used to be Origin.depth -aww
                        whereSql.append(eRegions[idx].depthRange.getMinValue()).append(" AND ").append(eRegions[idx].depthRange.getMaxValue());
                        whereSql.append(") ) THEN");
                        whereSql.append(" CASE WHEN (");

                        if (includeNullMag)
                            whereSql.append("Netmag.magnitude IS NULL OR COALESCE(Netmag.magnitude,-9.) BETWEEN ");
                        else whereSql.append("COALESCE(Netmag.magnitude,-9.) BETWEEN ");

                        whereSql.append(eRegions[idx].magRange.getMinValue()).append(" AND ").append(eRegions[idx].magRange.getMaxValue());
                        if (eRegions[idx].include) {
                            whereSql.append(" ) THEN 1 ELSE 0 END");
                        } else {
                            whereSql.append(" ) THEN 0 ELSE 1 END");
                        }
                    }
                    whereSql.append(" ELSE");
                    whereSql.append(" CASE WHEN (");

                    if (defaultIncludeNullMag)
                        whereSql.append("(Netmag.magnitude IS NULL OR COALESCE(Netmag.magnitude,-9.) BETWEEN ");
                    else whereSql.append("(COALESCE(Netmag.magnitude,-9.) BETWEEN ");

                    whereSql.append(defaultMagRange.getMinValue()).append(" AND ").append(defaultMagRange.getMaxValue()).append(") AND (");
                    whereSql.append("COALESCE(Origin.mdepth,-9.) BETWEEN ");
                    whereSql.append(defaultDepthRange.getMinValue()).append(" AND ").append(defaultDepthRange.getMaxValue());
                    if (defaultInclude) {
                        whereSql.append(") ) THEN 1 ELSE 0 END");
                    } else {
                        whereSql.append(") ) THEN 0 ELSE 1 END");
                    }
                    //sb.append( ") ) THEN 1 ELSE 0 END");
                    whereSql.append(" END) )");
                    whereSql.append(" ORDER BY Origin.Datetime"); // added 2009/03/26 -aww
                    joinSql = this.setEventSql(joinSql, whereSql, props.getMaxCatalogRows());
                    sql = joinSql.toString();
                    ps = conn.prepareStatement(sql);
                    ps.setObject(1, span.getStart(), Types.NUMERIC);
                    ps.setObject(2, span.getEnd(), Types.NUMERIC);
                    for (int idx = 0; idx < eRegions.length; idx++) {
                        ps.setObject(idx + 3, getPolygonARRAY(eRegions[idx].polygon, conn), Types.ARRAY);
                    }
                }
            }

            coll = ((JasiDbReader) getDataReader()).getBySQL(conn, ps);
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(conn, ex, "Error Retrieving Events", "SQLException in getByPreparedStatementRegionProps sql error: ");
        } catch (MissingPropertyException ex) {
            JiggleExceptionHandler.handleException(ex, "Error Retrieving Events", "MissingPropertyException in getByPreparedStatementRegionProps: ");
        } finally {
            JasiDatabasePropertyList.debugSQL(sql, ps, debug);
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException e) {
                JiggleExceptionHandler.handleDbException(conn, e);
            }
        }
        return coll;
    }

    /**
     * Set row limit clause and the word "where" between join and where condition
     */
    private StringBuffer setEventSql(StringBuffer joinSql, StringBuffer whereSql, int maxRow) {
        // Add row limit condition per the DB
        if (maxRow > 0) {
            LogUtil.debug(debug, "SolutionTN.setEventSql row count limit set to maxCatalogRows = " + maxRow);
            whereSql = DataSource.getJiggleConnection().setRowLimitCondition(whereSql, maxRow);
        }

        // Remove leading "and" in where statement
        String whereString = whereSql.toString().trim();
        if (whereString.toLowerCase().startsWith("and")) {
            whereString = whereString.substring(3);
        }

        // if there is no leading "where", add it
        if (! (whereString.toLowerCase().startsWith("where"))) {
            joinSql.append(" WHERE ");
        }

        return joinSql.append(whereString);
    }

    private static LatLonZ [] getPolygonArray(EventSelectionProperties props) {
        LatLonZ [] llz = props.getRegionPolygon();
        if (llz == null || llz.length == 0) {
            throw new MissingPropertyException("getByProperties bad/missing regionPolygon property");
        }
        return llz;
    }

    private static java.sql.Array getPolygonARRAY(LatLonZ[] llz, Connection conn) {
        //convert from jasi llz to ORACLE type LatLon here...
        //oracle.sql.ARRAY myArray = null;
        java.sql.Array myArray = null;
        try {
            OracleLatLon[] ll = new OracleLatLon[llz.length];
            org.trinet.util.Format df = new org.trinet.util.Format("%9.5f");
            for (int ii = 0; ii < llz.length; ii++) {
                ll[ii] = new OracleLatLon(llz[ii].getLat(), llz[ii].getLon());

                LogUtil.debug(debug || JasiDatabasePropertyList.debugSQL,
                    "LatLon(" + df.form(ll[ii].getLat().doubleValue()) + "," + df.form(ll[ii].getLon().doubleValue()) + ")");
            }

            // Oracle jdbc qualifies type with user's schema if not fully qualified -aww
//          oracle.sql.ArrayDescriptor aDesc = new oracle.sql.ArrayDescriptor("PUBLIC.COORDINATES", conn); // used to be CODE. -aww 2008/04/22
//          myArray = new oracle.sql.ARRAY(aDesc, conn, ll);
            // Requires db version 11.2.0.5.0 or higher
            //myArray = ((OracleConnection) conn).createOracleArray("PUBLIC.COORDINATES", ll);

            myArray = DataSource.getJiggleConnection().getCoordinate(ll, conn);
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(conn, ex, "Error Retrieving Polygon", "SQLException in getPolygonARRAY: ");
        }
        return myArray;
    }

// EVENT
    private String getEventTableWhereCondition(EventSelectionProperties props) {

        String str = null;
        Boolean bTF =  null;

        StringBuffer sb = new StringBuffer(1024);

        long id = props.getLong("eventId");
        if (id > 0l) sb.append(" AND (Event.evid=").append(id).append(") ");

        // Event.selectFlag
        bTF = props.getEventValidFlag();
        if (bTF != null)  {
            sb.append(" AND (Event.selectFlag=" + (bTF.booleanValue() ? "1" : "0") +") ");
        }

        // Event.auth
        str = props.getEventAuth();
        if (str != null && str.length() > 0) sb.append(" AND (Event.auth='" + str + "') ");

        // Event.subsource
        str = props.getEventSubsource();
        if (str != null && str.length() > 0) sb.append(" AND (Event.subsource='" + str + "') ");

        sb.append(getEventTypesWhereCondition(props));
        sb.append(getGTypesWhereCondition(props));
        sb.append(getEventStatesWhereCondition(props));

        return sb.toString();
    }

/// ORIGIN
    private String getOriginTableWhereCondition(EventSelectionProperties props) {

        String where = "";
        String str = null;
        Boolean bTF =  null;
        DoubleRange dr = null;
        IntegerRange ir = null;

        // Origin.bogusFlag
        // don't specify the property if both bogus and non-bogus types are wanted
        bTF = props.getOriginDummyFlag();
        if (bTF != null) // don't specify property if user want both bogus and non-bogus types
            where += " AND (Origin.bogusFlag=" + (bTF.booleanValue() ? "1" : "0") +") ";

        // Origin.datetime
        // This will handle both "absolute" and "relative" time definitions
        // TimeSpan span = props.getTimeSpan();
        // time bounds
        //where += " AND " + getTimeSpanSQL(span.getStart(), span.getEnd());
        where += " AND (Origin.DATETIME BETWEEN TRUETIME.putEpoch(?,'UTC') AND TRUETIME.putEpoch(?,'UTC'))";

        // Origin.auth
        str = props.getOriginAuth();
        if (str != null && str.length() > 0) where += " AND (Origin.auth='" + str + "') ";

        // Origin.subsource
        str = props.getOriginSubsource();
        if (str != null && str.length() > 0) where += " AND (Origin.subsource='" + str + "') ";

        // Origin.type
        str = props.getOriginType();
        if (str != null && str.length() > 0) where += " AND (Origin.type='" + str + "') ";

        //Origin.mdepth -  used to be Origin.depth - aww 2015/10/09
        dr = props.getOriginDepthRange();
        if (dr != null && !dr.isNull())
          where += " AND (Origin.mdepth>=" + dr.getMinValue() + " AND Origin.mdepth<=" + dr.getMaxValue() + ") "; // -aww 2015/10/09

        //Origin.wrms
        dr = props.getOriginRmsRange();
        if (dr != null && !dr.isNull())
          where += " AND (Origin.wrms>=" + dr.getMinValue() + " AND Origin.wrms<=" + dr.getMaxValue() + ") ";

        //Origin.erhor
        dr = props.getOriginErrHorizRange();
        if (dr != null && !dr.isNull())
          where += " AND (Origin.erhor>=" + dr.getMinValue() + " AND Origin.erhor<=" + dr.getMaxValue() + ") ";

        //Origin.sdep
        dr = props.getOriginErrDepthRange();
        if (dr != null && !dr.isNull())
          where += " AND (Origin.sdep>=" + dr.getMinValue() + " AND Origin.sdep<=" + dr.getMaxValue() + ") ";

        // Origin.ndef
        ir = props.getOriginPhaseCountUsedRange();
        if (ir != null && !ir.isNull())
          where += " AND (Origin.ndef>=" + ir.getMinValue() + " AND Origin.ndef<=" + ir.getMaxValue() + ") ";

        //Origin.totalarr
        ir = props.getOriginPhaseCountRange();
        if (ir != null && !ir.isNull())
          where += " AND (Origin.totalarr>=" + ir.getMinValue() + " AND Origin.totalarr<=" + ir.getMaxValue() + ") ";

        str = props.getOriginAlgo();
        if (str != null && str.length() > 0)
          where += " AND (Origin.algorithm='" + str + "') ";

        ir = props.getOriginGapRange();
        if (ir != null && !ir.isNull())
          where += " AND (Origin.gap>=" + ir.getMinValue() + " AND Origin.gap<=" + ir.getMaxValue() + ") ";

        dr = props.getOriginDistRange();
        if (dr != null && !dr.isNull())
          where += " AND (Origin.distance>=" + dr.getMinValue() + " AND Origin.distance<=" + dr.getMaxValue() + ") ";

        ir = props.getOriginSPhaseRange();
        if (ir != null && !ir.isNull())
          where += " AND (Origin.nbs>=" + ir.getMinValue() + " AND Origin.nbs<=" + ir.getMaxValue() + ") ";

        ir = props.getOriginFirstMoRange();
        if (ir != null && !ir.isNull())
          where += " AND (Origin.nbfm>=" + ir.getMinValue() + " AND Origin.nbfm<=" + ir.getMaxValue() + ") ";

        dr = props.getOriginQualityRange();
        if (dr != null && !dr.isNull())
          where += " AND (Origin.quality>=" + dr.getMinValue() + " AND Origin.quality<=" + dr.getMaxValue() + ") ";

        bTF = props.getOriginFixDepthFlag();
        if (bTF != null)
          where += " AND (Origin.fdepth='" + (bTF.booleanValue() ? "y" : "n") + "') ";

        bTF = props.getOriginFixEpiFlag();
        if (bTF != null)
          where += " AND (Origin.fepi='" + (bTF.booleanValue() ? "y" : "n") + "') ";
        
        StringBuffer sb = new StringBuffer(1024);
        sb.append(where);
        sb.append(getOriginRegionWhereCondition(props));

        return sb.toString();
    }

    // NETMAG
    private String getNetmagTableWhereCondition(EventSelectionProperties props) {

        if ( props.getMagPrefNull() ) {
            return " AND (Netmag.magid IS NULL) ";
        }

        final StringBuilder where = new StringBuilder(34);
        String str = null;
        DoubleRange dr = null;
        IntegerRange ir = null;

        // Netmag.auth
        str = props.getMagAuth();
        if (str != null && str.length() > 0) {
            where.append(" AND (Netmag.auth='");
            where.append(str);
            where.append("') ");
        }

        // Netmag.subsource
        str = props.getMagSubsource();
        if (str != null && str.length() > 0) {
            where.append(" AND (Netmag.subsource='");
            where.append(str);
            where.append("') ");
        }

        dr = props.getMagValueRange();
        if (dr != null && !dr.isNull()) {
          where.append(" AND (COALESCE(Netmag.magnitude,0.)>=");
          where.append(dr.getMinValue());
          where.append(" AND COALESCE(Netmag.magnitude,0.)<=");
          where.append(dr.getMaxValue());
          where.append(") ");
        }

        ir = props.getMagStaRange();
        if (ir != null && !ir.isNull()) {
          where.append(" AND (Netmag.nsta>=");
          where.append(ir.getMinValue());
          where.append(" AND Netmag.nsta<=");
          where.append(ir.getMaxValue());
          where.append(") ");
        }

        ir = props.getMagObsRange();
        if (ir != null && !ir.isNull()) {
          where.append(" AND (Netmag.nobs>=");
          where.append(ir.getMinValue());
          where.append(" AND Netmag.obs<=");
          where.append(ir.getMaxValue());
          where.append(") ");
        }

        str = props.getMagPrefType();
        if (str != null && str.length() > 0) {
            where.append(" AND (Netmag.magtype");
            int beginIndex = 0;
            int endIndex = str.indexOf(' ', beginIndex);
            if (endIndex != -1) {
                where.append(" in (");
                do {
                    where.append("'");
                    where.append(str.substring(beginIndex, endIndex));
                    where.append("',");
                    beginIndex = endIndex + 1;
                    endIndex = str.indexOf(' ', beginIndex);
                } while (endIndex != -1);
                endIndex = str.length();
                where.append("'");
                where.append(str.substring(beginIndex, endIndex));
                where.append("')");
            } else {
                where.append("='");
                where.append(str);
                where.append("'");
            }
            where.append(") ");
        }

        dr = props.getMagErrRange();
        if (dr != null && !dr.isNull()) {
          where.append(" AND (Netmag.uncertainty>=");
          where.append(dr.getMinValue());
          where.append(" AND Netmag.uncertainty<=");
          where.append(dr.getMaxValue());
          where.append(") ");
        }

        str = props.getMagAlgo();
        if (str != null && str.length() > 0) {
          where.append(" AND (Netmag.magalgo='");
          where.append(str);
          where.append("') ");
        }

        ir = props.getMagGapRange();
        if (ir != null && !ir.isNull()) {
          where.append(" AND (Netmag.gap>=");
          where.append(ir.getMinValue());
          where.append(" AND Netmag.gap<=");
          where.append(ir.getMaxValue());
          where.append(") ");
        }

        dr = props.getMagDistRange();
        if (dr != null && !dr.isNull()) {
          where.append(" AND (Netmag.distance>=");
          where.append(dr.getMinValue());
          where.append(" AND Netmag.distance<=");
          where.append(dr.getMaxValue());
          where.append(") ");
        }

        dr = props.getMagQualityRange();
        if (dr != null && !dr.isNull()) {
          where.append(" AND (Netmag.quality>=");
          where.append(dr.getMinValue());
          where.append(" AND Netmag.quality<=");
          where.append(dr.getMaxValue());
          where.append(") ");
        }

        return where.toString();
    }

    // ORIGIN lat,lon region
    private String getOriginRegionWhereCondition(EventSelectionProperties props) {
        String where = "";
        String regionType = props.getRegionType();

        // check for no type
        if (regionType == null ||
            regionType.equals(EventSelectionProperties.REGION_ANYWHERE) ) return where;

        boolean reject = props.getBoolean("regionExcluded");

        // defined area types
        if (regionType.equalsIgnoreCase(EventSelectionProperties.REGION_POINT)) {
            LatLonZ radPoint = props.getRegionRadiusPoint();
            if ( radPoint != null && ! radPoint.isNull() ) {
              double radius = props.getRegionRadiusValue();
              if (radius > 0.) {
                where += " AND (Wheres.separation_km(" + radPoint.getLat() + "," +
                  radPoint.getLon() + ",Origin.lat,Origin.lon)" +
                 (reject ? ">" : "<=") + radius + ")";
              }
            }
        }
        else if (regionType.equalsIgnoreCase(EventSelectionProperties.REGION_BOX) ) {
            DoubleRange boxLat = props.getRegionBoxLatRange();
            DoubleRange boxLon = props.getRegionBoxLonRange();
            if (boxLat != null && boxLon != null) {
              if ( ! (boxLat.isNull() || boxLon.isNull()) ) {
                  if (reject) {
                    where +=
                      " AND (Origin.lat>" + boxLat.getMaxValue() +
                      " OR Origin.lat<"   + boxLat.getMinValue() +
                      " ) AND ( " +
                      " Origin.lon>"   + boxLon.getMaxValue() +
                      " OR Origin.lon<"   + boxLon.getMinValue() +
                      "))";
                  }
                  else {
                    where +=
                      " AND (Origin.lat<=" + boxLat.getMaxValue() +
                      " AND Origin.lat>="   + boxLat.getMinValue() +
                      " AND Origin.lon<="   + boxLon.getMaxValue() +
                      " AND Origin.lon>="   + boxLon.getMinValue() + ")";
                  }
              }
            }
        }
        else if (regionType.equalsIgnoreCase(EventSelectionProperties.REGION_BORDER) ) {
            String bpName = props.getRegionBorderPlaceName();
            if (bpName != null) {
              where += " AND (geo_region.inside_border('" + bpName +
                      "',Origin.lat,Origin.lon) " + (reject ? "<=0)" : ">0)");
            }
        }
        else if (regionType.equalsIgnoreCase(EventSelectionProperties.REGION_PLACE) ) {
            String rpName = props.getRegionRadiusPlaceName();
            if (rpName != null) {
                double radius = props.getRegionRadiusValue();
                if (radius > 0.) {
                  where += " AND (wheres.within_radius_of(Origin.lat,Origin.lon,'" +
                      rpName + "'," + radius + ") " + (reject ? "<=0)" : ">0)");
                }
            }
        }

        return where;
    }

    private String getEventTypesWhereCondition(EventSelectionProperties props) {

        String where = "";
        String str = "";

        // Event types (e.g. "eq", "sonic", etc.) - these must be OR'ed
        // These properties USED to have the form like:  SelectAttribute_local = true
        // There may not be a property for each type, thus the check for != null.
        // include etypes
        String [] types = props.getEventTypesIncluded();
        if (types != null && types.length > 0) {
          str = "";
          for (int i = 0; i< types.length; i++) {
              if ( EventTypeMap3.getMap().isValidJasiType(types[i]) ) {
                  if (str != "") str += " OR ";    // need this if more then one
                  str += " Event.ETYPE='"+EventTypeMap3.toDbType(types[i])+"'";
              }
              else {
                  System.out.println("SolutionTN invalid event included type for query : " + types[i]);
              }
          }
          if (str != "") where += " AND (" + str + ") ";
        }

        // exclude etypes
        types = props.getEventTypesExcluded();
        if (types != null && types.length > 0) {
          str = "";
          for (int i = 0; i< types.length; i++) {
              if ( EventTypeMap3.getMap().isValidJasiType(types[i]) ) {
                  if (str != "") str += " AND ";    // need this if more then one
                  str += " Event.ETYPE<>'"+EventTypeMap3.toDbType(types[i])+"'";
              }
              else {
                  System.out.println("SolutionTN invalid event excluded type for query : " + types[i]);
              }
          }
          if (str != "") where += " AND (" + str + ") ";
        }

        return where;
    }

    private String getGTypesWhereCondition(EventSelectionProperties props) {

        String where = "";
        String str = "";

        // Origin gtypes (e.g. "local", "regional", "teleseism") - these must be OR'ed
        // There may not be a property for each type, thus the check for != null.
        // include gtypes
        String [] types = props.getOriginGTypesIncluded();
        if (types != null && types.length > 0) {
          str = "";
          for (int i = 0; i< types.length; i++) {
              if ( GTypeMap.isValidJasiType(types[i]) ) {
                  if (str != "") str += " OR ";    // need this if more then one
                  str += " Origin.GTYPE='"+GTypeMap.toDbType(types[i])+"'";
              }
              else {
                  System.out.println("SolutionTN invalid origin included gtype for query : " + types[i]);
              }
          }
          if (str != "") where += " AND (" + str + ") ";
        }

        // exclude gtypes
        types = props.getOriginGTypesExcluded();
        if (types != null && types.length > 0) {
          str = "";
          for (int i = 0; i< types.length; i++) {
              if ( GTypeMap.isValidJasiType(types[i]) ) {
                  if (str != "") str += " AND ";    // need this if more then one
                  str += " Origin.GTYPE<>'"+GTypeMap.toDbType(types[i])+"'";
              }
              else {
                  System.out.println("SolutionTN invalid origin excluded gtype for query : " + types[i]);
              }
          }
          if (str != "") where += " AND (" + str + ") ";
        }

        return where;
    }

    private String getEventStatesWhereCondition(EventSelectionProperties props) {

        String where = "";
        String str = "";

        // include processing states
        String [] types = props.getOriginProcTypesIncluded();
        if (types != null && types.length > 0) {
          str = "";
          for (int i = 0; i< types.length; i++) {
            if (str != "") str += " OR ";    // need this if more then one
            if ( ProcessingState.isKnownLabel(types[i]) )
                str += " Origin.rflag='"+ProcessingState.labelToTag(types[i])+"'";
            else if ( ProcessingState.isKnownTag(types[i]) )
                str += " Origin.rflag='"+types[i]+"'";
          }
          if (str != "") where += " AND (" + str + ")";
        }
        // exclude processing states
        types = props.getOriginProcTypesExcluded();
        if (types != null && types.length > 0) {
          str = "";
          for (int i = 0; i< types.length; i++) {
            if (str != "") str += " OR ";    // need this if more then one
            if ( ProcessingState.isKnownLabel(types[i]) )
                str += " Origin.rflag<>'"+ProcessingState.labelToTag(types[i])+"'";
            else if ( ProcessingState.isKnownTag(types[i]) )
                str += " Origin.rflag<>'"+types[i]+"'";
          }
          if (str != "") where += " AND (" + str + ")";
        }

        return where;
    }

    /** Get using this SQL WHERE clause, appends " WHERE " if missing from start of input clause. */
    private Collection getWherePrepared(String whereClause, EventSelectionProperties props) {
        StringBuffer sb = new StringBuffer(1024);
        sb.append(sqlJoin);
        // strip off any leading " and " from the whereClause
        String aStr = whereClause.trim();
        if ( (aStr.startsWith("AND") || aStr.startsWith("and")) ) whereClause = aStr.substring(3);
        // if there is no leading where, add it
        if (! (whereClause.startsWith("WHERE") || whereClause.startsWith("where")) ) sb.append(" WHERE ");
        sb.append(whereClause);
        PreparedStatement psWhere = null;
        TimeSpan span = props.getTimeSpan();
        ArrayList aList = null;
        final String sql = sb.toString();
        try {
            Connection conn = DataSource.getConnection();
            psWhere = conn.prepareStatement(sql);
            //RH psWhere.setDouble(1, span.getStart());
            //RH psWhere.setDouble(2, span.getEnd());
            psWhere.setObject(1, span.getStart(), java.sql.Types.NUMERIC);
            psWhere.setObject(2, span.getEnd(), java.sql.Types.NUMERIC);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psWhere);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, psWhere, debug);
            Utils.closeQuietly(psWhere);
        }
        return aList;
    }

    public Collection getWhere(String whereClause) {
        StringBuffer sb = new StringBuffer(1024);
        sb.append(sqlJoin);
        // if no leading " and " add it
        String aStr = whereClause.trim();
        if (! (aStr.startsWith("AND") || aStr.startsWith("and")) ) whereClause = aStr.substring(3);
        if (! (whereClause.startsWith("WHERE") || whereClause.startsWith("where")) ) sb.append(" WHERE ");
        sb.append(whereClause);
        //if (debug || JasiDatabasePropertyList.debugSQL) System.out.println("DEBUG SolutionTN getWhere SQL :\n"+sb.toString()); // TEST
        return ((JasiDbReader) getDataReader()).getBySQL(sb.toString());
        //if (aList == null) return null;
        //return (Solution []) aList.toArray(new Solution[aList.size()]);
    }

    /** Get the comment from the dbase if 'includeComment' flag is set and
    * the Event has one.
    * */
    protected static void dbFetchComment(SolutionTN sol) {
        // Since most are null, this is faster than calling stored EPREF.getComment() function in query
        if (!sol.includeComment || !sol.commid.isValidNumber()) return;
        /*
          Remark rmk = new Remark(DataSource.getConnection());
          Remark rmks[] = rmk.getRowsByCommId(sol.commid.longValue());
          if (rmks != null && rmks.length > 0) {
              StringBuffer newStr = new StringBuffer(rmks.length*80);
              //String newStr = "";
              for (int i = 0; i < rmks.length; i++) {
                //newStr += rmks[i].toStringRemark();
                newStr.append(rmks[i].toStringRemark());
                if (i > 0 && i<rmks.length-1) newStr.append("\n");
              }
              sol.comment.setValue(newStr.toString());  // this sets isUpdate(true)
              sol.comment.setUpdate(false);  // it shouldn't be if its from the dbase
          }
          else {
              System.err.println("SolutionTN: Database Error? no Remark table rows found for event commid: " +
                      sol.commid.longValue());
          }
        */

        PreparedStatement ps = null;
        ResultSet rs = null;
        /* System.out.println("RH DEBUG: dbFetchComment " + sol.toSummaryString()); */
        String sql = null;
        try {
            Connection conn = DataSource.getConnection();
            DatabaseMetaData db_info = conn.getMetaData();
            if ("postgresql".equalsIgnoreCase(db_info.getDatabaseProductName().toString())) {
                sql = "SELECT REMARK FROM (SELECT LINENO, REMARK FROM REMARK WHERE COMMID=? ORDER BY LINENO) AS foo";
            } else {
                sql = "SELECT DISTINCT REMARK FROM (SELECT LINENO, REMARK FROM REMARK WHERE COMMID=? ORDER BY LINENO)";
            }
            ps= conn.prepareStatement(sql);
            ps.setLong(1, sol.commid.longValue());
            rs = ps.executeQuery();
            StringBuffer newStr = new StringBuffer(512);
            boolean status = rs.next();
            while (status) {
                newStr.append(rs.getString(1));
                if (rs.next()) newStr.append("\n");
                else status = false;
            }
            sol.comment.setValue(newStr.toString()); // this sets isUpdate(true)
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, ps, debug);
            Utils.closeQuietly(rs, ps);
        }
    }

    /** Get the comment from the dbase if 'includeComment' flag is set and
    * the Event has one.
    * */
    protected static void dbFetchComments(SolutionTN[] sol) {
        for (int i = 0; i < sol.length; i++) {
          dbFetchComment(sol[i]);
        }
    }
    public JasiObject parseData(Object rsdb) {
      return parseResultSetDb((ResultSetDb) rsdb);
    }
    public JasiObject parseResultSetDb(ResultSetDb rsdb) {
      return parseResultSet((ResultSetDb) rsdb);
    }
    /**Access to the JasiDbReader to allow generic SQL queries. */
    public JasiDataReaderIF getDataReader() {
        return jasiDataReader;
    }

    // A matching row key exists in datasource but its actually data may differ from instance
    public boolean existsInDataSource() {
        JasiDbReader jdr = (JasiDbReader) getDataReader();
        //int count = jdr.getCountBySQL(DataSource.getConnection(), "SELECT 1 FROM event WHERE evid=" + id.longValue());
        return ( jdr.getCountBySQL(DataSource.getConnection(), "EVENT", "EVID", id.toString()) > 0 );
    }
    public boolean originExistsInDataSource() {
        JasiDbReader jdr = (JasiDbReader) getDataReader();
        //int count = jdr.getCountBySQL(DataSource.getConnection(), "SELECT 1 FROM origin WHERE orid=" + orid.longValue());
        return ( jdr.getCountBySQL(DataSource.getConnection(), "ORIGIN", "ORID",  orid.toString()) > 0);
    }

    /**
     * Parse a resultset row that contains a concatinated Event/Origin/NetMag
     */
    protected static SolutionTN parseResultSet(ResultSetDb rsdb)  {
        SolutionTN sol = new SolutionTN();
        // note: these are local instances
        Event  evtRow  = new Event();
        Origin orgRow = new Origin();
        NetMag magRow = new NetMag();
        //OriginError oeRow = new OriginError();

        int offset = 0;
        // note we are using the class instances of event/origin/netmag
        //        System.out.println("parsing event...");
        // copy data from resultset to DataTableRow object
        evtRow = (Event) evtRow.parseOneRow(rsdb, offset);
        evtRow.setProcessing(Event.NONE);
        offset += Event.MAX_FIELDS;

        //         System.out.println("parsing Origin...");
        orgRow = (Origin) orgRow.parseOneRow(rsdb, offset);
        orgRow.setProcessing(Origin.NONE);
        offset += Origin.MAX_FIELDS;

        //        System.out.println("parsing OriginError...");
        //oeRow = (OriginError) oeRow.parseOneRow(rsdb, offset);
        //oeRow.setProcessing(OriginError.NONE);
        //offset += OriginError.MAX_FIELDS;
        sol.parseOriginError(rsdb, offset+1);
        offset += 7;

        //        System.out.println("parsing Netmag...");
        magRow = (NetMag) magRow.parseOneRow(rsdb, offset);
        magRow.setProcessing(NetMag.NONE);
        offset += NetMag.MAX_FIELDS+1; // rs column counting starts at column 1, not 0

        String dataStr = null;
        try {
            // Get the associated waveform count 
            sol.waveRecords.setValue(rsdb.getResultSet().getLong(offset++));
            // Get the origin attribution
            dataStr = rsdb.getResultSet().getString(offset++);
            if (dataStr != null) sol.getWho().setValue(dataStr); 
        } catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }

        // allow changes to this DataTableRow. Need to set this because we are
        // not using our own rather than org.trinet.jdbc parsing methods
        if (DataSource.isWriteBackEnabled()) {
            evtRow.setMutable(true).setMutableAllValues(true); // 07/07/04 -aww
            orgRow.setMutable(true).setMutableAllValues(true); // 07/07/04 -aww
            magRow.setMutable(true).setMutableAllValues(true); // 07/07/04 -aww
            //oeRow.setMutable(true).setMutableAllValues(true); // 07/12/06 -aww
            // below line locks selected rows if table is queried using this row object 
            //evtRow.setSelectForUpdate(true); // not needed, no row query so removed 7/20/2005 -aww
        }

        // row fields mutable so parse data into instance members
        sol.fromDbase = true;
        sol.parseEventRow(evtRow);
        sol.parseOriginRow(orgRow); // Do we want to save Origin.LDDATE vs. Event.LDDATE in timeStamp?
        //sol.parseOriginError(oeRow); // aww - 07/12/2006

        // remember DataTableRows for later writeback
        sol.eventRow  = evtRow;
        sol.originRow = orgRow;


        // Below create a new magnitude object, assign it, and add it to list
        // Do NOT invoke overridden setPreferredMagnitude,
        // since it changes the parsed db prefmag
        //if (sol.getPrefmagValue() > 0l) {
        sol.magnitude = Magnitude.create();
        //sol.magnitude.setDependsOnOrigin(false); // dummy magnitude, to avoid commit error from altMagList -aww 03/6/2007
        sol.magnitude.assign(sol);
        //sol.altMagList.add(sol.magnitude);
        sol.magnitude.setType(" ");
        sol.addDataStateListeners(sol.magnitude); // only if magList listener add below doesn't do it.

        MagnitudeTN mag = (MagnitudeTN) sol.magnitude;
        mag.parseNetMagRow(magRow);

        // Get the magnitude attribution
        try {
            dataStr = rsdb.getResultSet().getString(offset++);
            if (dataStr != null) mag.getWho().setValue(dataStr); 
        } catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }

        // allow changes to this DataTableRow. Need to set this because we are
        // not using our own rather than org.trinet.jdbc parsing methods
        // Kludge to get user id if any (uses default connection)
        //DataDouble dd = rsdb.getDataDouble(offset++); // enable only if PVR queried above aww 06/28/2006
        // Added mag subScipt type and value default settings below - aww 10/26/2005
        boolean isDummyMag = false;
        if ( mag.hasNullType() ) {
            mag.subScript.setValue(" "); // force to undefined type
            isDummyMag = true;
        }
        if ( mag.hasNullValue() ) {
            //mag.value.setValue(0.); // force to a 0. value
            isDummyMag = true;
        }
        if (isDummyMag || mag.algorithm.toString().equalsIgnoreCase("HAND")) {
            mag.setDependsOnOrigin(false); // to avoid commit error from altMagList -aww 07/05/2007
        }

        mag.setUpdate(false); // aww insurance to flag state virgin

        // now put parsed preferred magnitude type in prefMagMap -aww 10/19/2004
        sol.setPrefMagOfType(sol.magnitude); // does nothing and returns false for hasNullType
          
        setEventAssociation(sol);
        
        // calculate the priority -  Do we really need this, who uses this?
        SolutionTN.calcPriority(sol);
        //}

        // since most commid are null, doing a secondary query here is faster than calling EPREF.getComment() in main query for each evid
        dbFetchComment(sol);

        if (loadCatalogPrefMags) {
            sol.loadPrefMags();
        }

        /*
        try {
          dataStr = rsdb.getResultSet().getString(offset++);
          if (dataStr != null) {
           sol.comment.setValue(dataStr);  // this sets isUpdate(true)
           sol.comment.setUpdate(false);  // it shouldn't be if its from the dbase
          }
        } catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }
        */

        // Kludge to get user id, if any, uses DataSource default connection
        //if (dbAttributionEnabled) sol.loadAttribution();

        sol.setUpdate(false); //added to reset fromDb and changes parsed into fields
        sol.setNeedsCommit(false); //added since setPreferredMagnitude add listener sets it.
        //System.out.println("DEBUG id: "+sol.id.longValue()+" needsCommit: "+sol.needsCommit+" changed: "+sol.hasChanged()+
        //" evtPrefmag: "+sol.lastEvtPrefmag+" orgPrefmag: "+sol.lastOrgPrefmag+" prefmagId: "+sol.prefmag.longValue());

        return sol;
    }

    /**
     * Set associated events for this event id as comma separated string in "association" field
     * @param solution 
     */
    private static void setEventAssociation(Solution solution) {
        List<AssocEvent> assocEvents = EventAssocManager.getEventAssoc()
                .getAssociatedEvents(solution.getId().longValue());
        String assocString = AssocEvent.getEventAssociationList(assocEvents);
        solution.setAssociation(assocString);
    }
    
    /** Calculate the priority of this solution. */
    protected static void calcPriority(Solution sol) {
        sol.priority.setValue(EventPriorityModelTN.getPriority(sol));
    }

    /**
     * Suck contents out of an Event TableRow to populate this Solution.
     */
    // Note that this copies DataObjects not references.
    protected SolutionTN parseEventRow(Event evt) {
        this.id.setValue(evt.getDataObject(Event.EVID));
        id.setUpdate(false);        // DataTableRow weirdness: sets key field isUpdate = true
        // parent id is not currently stored in the dbase, closest would be the origin locevid ?
        setParentId(id.longValue());
        parentId.setUpdate(false);
        // foreign keys (PROTECTED)
        this.prefor.setValue(evt.getDataObject(Event.PREFOR));
        lastPreforId = prefor.longValue(); // save last db set value of prefor - aww

        /*
          The fields prefmag, prefmec & commid exist in BOTH the Event and Origin tables.
          Which to use??  Made decision to use prefmag, prefmec of Origin, and commid of Event.
          Changed it 5/13/03
        */
        //        this.prefmag.setValue(evt.getDataObject(Event.PREFMAG));
        //        this.prefmec.setValue(evt.getDataObject(Event.PREFMEC)); // -aww 2013/01/17 commented out:
                  this.prefmec.setValue(evt.getDataObject(Event.PREFMEC)); // changed back to using event since new origin prefmec is not updated

        // Save db value to compare later for change
        lastEvtPrefmag = evt.getDataObject(Event.PREFMAG).longValue(); // added - aww 2008/04/02

        // added 5/13/03 - other programs assume comments in EVENT table, not Origin.
        this.commid.setValue(evt.getDataObject(Event.COMMID));

        // This gets a NATIVE event type, must translate to generic (jasi) type
        String estr = evt.getDataObject(Event.ETYPE).toString();
        if (EventTypeMap3.TRIGGER.equals(estr)) isSubnetTrigger = true; // -aww 2011/08/16
        //this.setEventType(EventTypeMap.MAP.fromLocalCode(estr)); // don't do this it bumps the version -aww 2008/04/15
        this.eventType.setValue(EventTypeMap3.getMap().toJasiType(estr)); //instead do this  -aww 2008/04/15
        this.eventType.setUpdate(false);     // don't mark as changed by program

        // Event auth and source are distinct from Origin's
        this.eventAuthority.setValue(evt.getDataObject(Event.AUTH));
        this.eventSource.setValue(evt.getDataObject(Event.SUBSOURCE));
        this.validFlag.setValue(evt.getDataObject(Event.SELECTFLAG));
        this.srcVersion.setValue(evt.getDataObject(Event.VERSION));

        return this;
    }

    /**
     * Put contents of this Solution back into the original Event TableRow object.
     * If there was none, make one.
     * This is the reverse of parseEvent().
     */
    protected void toEventRow() {
         // Note that this copies DataObjects not references.
        // new evid could have been assigned already
        if (! id.isValidNumber()) id.setValue(setUniqueId()); // set if not assigned

        // create an Event object if there wasn't one we read from originally
        if (eventRow == null) {
          eventRow = new Event(id.longValue());
          eventRow.setValue(Event.AUTH, getEventAuthority());
          eventRow.setValue(Event.SUBSOURCE, getEventSource());
        }
        // allow updating the row
        eventRow.setUpdate(true);


        // MAKE SURE "NOT NULL" COLUMNS HAVE VALUES
        if (id.isUpdate())  eventRow.setValue(Event.EVID, id);

       /* Do NOT change Event.subsource and Event.AUTH if it was originally from
         the dbase, else you'll overwrite the original source of a relocated event.
         See: toOriginRow().
         However, a brand new event must populate these attributes.
       */
//      if (!isFromDataSource()) {
//          eventRow.setValue(Event.AUTH, getEventAuthority());
//          eventRow.setValue(Event.SUBSOURCE, getEventSource());
//      }

        // foreign keys (PROTECTED)
        if (commid.isUpdate()) {
            if (commid.longValue() == 0l) eventRow.setValue(Event.COMMID, null);
            else eventRow.setValue(Event.COMMID, commid);
        }

        /*
          The fields prefmag & prefmec exist in BOTH the Event & Origin tables
          Use same values for both the prefor Origin row in the Event row.
        */
        if (prefmag.isUpdate()) {
          eventRow.setValue(Event.PREFMAG, prefmag);
        }
        if (prefmec.isUpdate()) {
          eventRow.setValue(Event.PREFMEC, prefmec);
        }
        if (prefor.isUpdate()) {
          eventRow.setValue(Event.PREFOR, prefor);
        }

        if (eventType.isUpdate()) {
          // must convert to TriNet style 2-char event type
          eventRow.setValue(Event.ETYPE, EventTypeMap3.getMap().toDbType(eventType.toString()));
        }

        if (eventAuthority.isUpdate()) {
            eventRow.setValue(Event.AUTH, getEventAuthority());
        }

        if (eventSource.isUpdate()) {
            eventRow.setValue(Event.SUBSOURCE, getEventSource());
        }

        // note: validFlag is written to Event.selectflag, dummyFlag is written to Origin.bogusflag
        if (validFlag.isUpdate()) {
            eventRow.setValue(Event.SELECTFLAG, validFlag);
        }
    }

    /**
     * Suck contents out of an Origin (TableRow) object to populate
     * DataObjects of this Solution.
     */
    // Note that this gets COPIES of DataObjects not references.
    protected SolutionTN parseOriginRow(Origin org) {
        // use EVID we got from EVENT table if avail
        if (! id.isValidNumber()) {
            this.id.setValue(org.getDataObject(Origin.EVID));
            id.setUpdate(false);        // DataTableRow weirdness: sets key field isUpdate = true
        }

        // foreign keys (PROTECTED)
        this.orid.setValue(org.getDataObject(Origin.ORID));
        this.prefmag.setValue(org.getDataObject(Origin.PREFMAG));
        // save last db value of prefmag, virgin may be NullValueDb.NULL_LONG
        lastOrgPrefmag      = prefmag.longValue();
        //this.prefmec.setValue(org.getDataObject(Origin.PREFMEC)); // revert to loading event.prefmec -aww 2013/01/17
        // Commented 5/13/03 -- comments in Event only
        //this.commid.setValue(org.getDataObject(Origin.COMMID));
        //this.solveDate.setValue(org.getDataObject(Origin.LOC_MODEL_DTM));
        //this.solveDate.setValue(org.getDataObject(Origin.LDDATE)); // date of first commit? 01/08/2008 - aww
        this.datetime.setValue(org.getDataObject(Origin.DATETIME)); // assumes truetime.getEpoch(DATETIME,'UTC') in query string -aww 2008/01/24
        // Kludge for NCEDC leap corrected data, to convert to nominal time -aww
        //DataSource.dbTimeBaseToNominal(datetime); // removed 2008/01/24 -aww for leap secs

        this.lat.setValue(org.getDataObject(Origin.LAT));
        this.lon.setValue(org.getDataObject(Origin.LON));
        this.depth.setValue(org.getDataObject(Origin.DEPTH));

        this.horizDatum.setValue(org.getDataObject(Origin.DATUMHOR));
        this.vertDatum.setValue(org.getDataObject(Origin.DATUMVER));

        this.type.setValue(org.getDataObject(Origin.TYPE));
        if ( this.type.isBlankOrNull() ) this.type.setValue(Solution.UNKNOWN_TYPE);  // -aww added 2014/04/30 

        this.algorithm.setValue(org.getDataObject(Origin.ALGORITHM));
        // NCDC schema cmodel & vmodel were number but now are varchar2
        this.crustModel.setValue(org.getDataObject(Origin.CMODELID));
        this.velModel.setValue(org.getDataObject(Origin.VMODELID));

        this.authority.setValue(org.getDataObject(Origin.AUTH));
        this.source.setValue(org.getDataObject(Origin.SUBSOURCE));

        this.gap.setValue(org.getDataObject(Origin.GAP));
        this.distance.setValue(org.getDataObject(Origin.DISTANCE));
        this.rms.setValue(org.getDataObject(Origin.WRMS));
        this.errorTime.setValue(org.getDataObject(Origin.STIME));
        this.errorHoriz.setValue(org.getDataObject(Origin.ERHOR));
        this.errorVert.setValue(org.getDataObject(Origin.SDEP));
        this.errorLat.setValue(org.getDataObject(Origin.ERLAT));
        this.errorLon.setValue(org.getDataObject(Origin.ERLON));

        this.totalReadings.setValue(org.getDataObject(Origin.TOTALARR));
        // TOTALAMP is ignored
        this.usedReadings.setValue(org.getDataObject(Origin.NDEF));
        this.sReadings.setValue(org.getDataObject(Origin.NBS));
        this.firstMotions.setValue(org.getDataObject(Origin.NBFM));
        this.externalId.setValue(org.getDataObject(Origin.LOCEVID));
        this.quality.setValue(org.getDataObject(Origin.QUALITY));

        this.dummyFlag.setValue(org.getDataObject(Origin.BOGUSFLAG));
        this.processingState.setValue(org.getDataObject(Origin.RFLAG));

        this.crustModelType.setValue(org.getDataObject(Origin.CRUST_TYPE));
        this.crustModelName.setValue(org.getDataObject(Origin.CRUST_MODEL));
        this.mdepth.setValue(org.getDataObject(Origin.MDEPTH));
        this.gtype.setValue(GTypeMap.fromDbType(org.getDataObject(Origin.GTYPE).toString()));

        // save Origin.LDDATE in timeStamp? -aww
        this.timeStamp.setValue(org.getDataObject(Origin.LDDATE)); // date of last revision?
        //System.out.println("DEBUG solution timeStamp: " + this.timeStamp.toDateString());
        //

        // fixed flags: default is "false" and "null" so if null or NOT="Y" they're left false.
//
//WARNING: just using depthFixed.setValue(xxx) will also set isUpdate() true!!
//
        //String fix = ((DataString) org.getDataObject(Origin.FDEPTH)).toString();
        //if (fix.equalsIgnoreCase("y")) depthFixed = new DataBoolean(true).setUpdate(false);
        //fix = ((DataString) org.getDataObject(Origin.FEPI)).toString();
        //if (fix.equalsIgnoreCase("y")) locationFixed = new DataBoolean(true).setUpdate(false);
        //fix = ((DataString) org.getDataObject(Origin.FTIME)).toString();
        //if (fix.equalsIgnoreCase("y")) timeFixed = new DataBoolean(true).setUpdate(false);

        String fix = ((DataString) org.getDataObject(Origin.FDEPTH)).toString();
        setDataBoolean (depthFixed, fix);

        fix = ((DataString) org.getDataObject(Origin.FEPI)).toString();
        setDataBoolean (locationFixed, fix);

        fix = ((DataString) org.getDataObject(Origin.FTIME)).toString();
        setDataBoolean (timeFixed, fix);

        return this;
    }

    /**
    * Set a DataBoolean true if str  is "Y", "y", "t", or "T".
    * Sets the DataBoolean as isUpdate() = false.
    */
    private static final String[] TRUTH_STRINGS = {"Y", "y", "t", "T"};
    private void setDataBoolean (DataBoolean bool, String str) {
      org.trinet.util.StringList strList = new org.trinet.util.StringList(TRUTH_STRINGS);
      bool.setValue(strList.contains(str));
      bool.setUpdate(false);  // original population, don't set as updated
    }

    public boolean isFromDataSource() { return fromDbase;} // aww temp fix

    /**
     * Return true if any Event or Origin field is different from what's in the dbase.
     * Either, 1) its been changed and not saved or 2) it is newly created and not saved.
     * Does NOT check the magnitude. If you want the status of the magnitude you
     * need to check that with magnitude.hasChanged().
     * */
    public boolean hasChanged() {
        //System.out.println("Event changed : " + eventHasChanged() + " Origin changed: " +  originHasChanged());
        return eventHasChanged() || originHasChanged();
    }

    /**
     * Set the isUpdate() flag for all data dbase members the given boolean value.
     * */
    public void setUpdate(boolean tf) {
       fromDbase = !tf; // called after commit, should now match dbase values
       eventSetUpdate(tf);
       originSetUpdate(tf);
    }

    /**
     * Return true if any Event field has changed from what was read in from the
     * dbase. Returns true if data were not originally from the dbase.
     * */
    public boolean eventHasChanged() {
        if (!fromDbase )                return true; // short-circuit
        if (id.isUpdate())              return true;
        if (eventAuthority.isUpdate())  return true;
        if (eventSource.isUpdate())     return true;
        if (commid.isUpdate())          return true;
        if (comment.isUpdate())         return true;
        // The initial bogus "null" magnitude set upon create() has NullValueDb.NULL_LONG value.
        // Though prefmag update state is reset here after parse, the Magnitude magid appears not to have
        // its update state reset after Magnitude row parse. After prefmag=Magnitude.magid in setPreferredMagnitude(mag) 
        // prefmag will show true for its update state here.
        // Note a dbUpdate() resets prefmag state, but update may have been a no-op: eventRow was not updated in db 
        // NOTE: as read from db it's possible that the lastEvtPrefmag != lastOrgPrefmag
        if ( prefmag.isUpdate()) { // changed
           //(lastOrgPrefmag != NullValueDb.NULL_LONG) &&  // ? this condition needs an explanation of what code needs it -aww 2008/04/02
           if (prefmag.longValue() != lastOrgPrefmag) return true; // db origin row value differs
           if (prefmag.longValue() != lastEvtPrefmag) return true; // db event row value differs -added 2008/04/02
        }
        if (prefmec.isUpdate())         return true;
        if (prefor.isUpdate())          return true; // may not be same as instance orid
        if (orid.isUpdate())            return true;
        if (eventType.isUpdate())       return true;
        if (validFlag.isUpdate())       return true;
        return false;
    }

    /**
     * Set the setUpdate() flag for all data dbase members to the given boolean value.
     * */
    protected void eventSetUpdate(boolean tf) {
        id.setUpdate(tf);
        eventAuthority.setUpdate(tf);
        eventSource.setUpdate(tf);

        commid.setUpdate(tf);
        comment.setUpdate(tf);
        prefmag.setUpdate(tf);
        prefmec.setUpdate(tf);
        prefor.setUpdate(tf);

        eventType.setUpdate(tf);
        validFlag.setUpdate(tf);
    }

    /**
     * Return true if any field has changed from what was read in from the
     * dbase. Returns true if phase was not originally from the dbase.
     * */
    public boolean originHasChanged() {
        return originLocationHasChanged() || originDescriptionHasChanged();
    }

    /** Return true if an elements haves changed effecting the Origin location. */
    protected boolean originLocationHasChanged() {
        if (!fromDbase)               return true;   // short-circuit
        if (datetime.isUpdate())      return true;   // do insert, location result
        if (lat.isUpdate())           return true;   // do insert, location result
        if (lon.isUpdate())           return true;   // do insert, location result
        if (depth.isUpdate())         return true;   // do insert, location result
        // below should be updated only by algorithm that did location and not hand-edited
        if (gap.isUpdate())           return true;   // desc coupled to location results
        if (distance.isUpdate())      return true;   // desc coupled to location results
        if (rms.isUpdate())           return true;   // desc coupled to location results
        if (errorTime.isUpdate())     return true;   // desc coupled to location results
        if (errorHoriz.isUpdate())    return true;   // desc coupled to location results
        if (errorVert.isUpdate())     return true;   // desc coupled to location results
        if (errorLat.isUpdate())      return true;   // desc coupled to location results
        if (errorLon.isUpdate())      return true;   // desc coupled to location results
        if (totalReadings.isUpdate()) return true;   // desc coupled to location results
        if (usedReadings.isUpdate())  return true;   // desc coupled to location results
        if (sReadings.isUpdate())     return true;   // desc coupled to location results
        if (firstMotions.isUpdate())  return true;   // desc coupled to location results
        if (depthFixed.isUpdate())    return true;   // desc coupled to location results
        if (locationFixed.isUpdate()) return true;   // desc coupled to location results
        if (timeFixed.isUpdate())     return true;   // desc coupled to location results
        if (type.isUpdate())          return true;   // desc kind of hyp
        if (algorithm.isUpdate())     return true;   // ? desc could be new ?
        return false;
    }

    /** Return true if an element needing update, not effecting the location has changed. */
    protected boolean originDescriptionHasChanged() {
        if (!fromDbase)                 return true; // short-circuit
        if (processingState.isUpdate()) return true; // desc flag doesn't change results
        if (quality.isUpdate())         return true; // desc evaluation of location results
        if (dummyFlag.isUpdate())       return true; // desc Origin state flag
        if (authority.isUpdate())       return true; // desc responsible network
        if (source.isUpdate())          return true; // desc program/machine
        if (crustModel.isUpdate())      return true; // desc string
        if (velModel.isUpdate())        return true; // desc string
        if (horizDatum.isUpdate())      return true; // desc updated, derived data the same
        if (vertDatum.isUpdate())       return true; // desc updated, derived data the same
        if (externalId.isUpdate())      return true; // desc of original source of data 
        if (gtype.isUpdate())           return true;
        return false;
    }

    /**
     * Set the setUpdate() flag for all data dbase members the given boolean value.
     * */
    protected void originSetUpdate(boolean tf) {
        orid.setUpdate(tf) ;
        datetime.setUpdate(tf) ;
        lat.setUpdate(tf) ;
        lon.setUpdate(tf) ;
        depth.setUpdate(tf) ;
        horizDatum.setUpdate(tf) ;
        vertDatum.setUpdate(tf) ;

        type.setUpdate(tf) ;
        algorithm.setUpdate(tf) ;
        crustModel.setUpdate(tf) ;
        velModel.setUpdate(tf) ;
        authority.setUpdate(tf) ;
        source.setUpdate(tf) ;

        gap.setUpdate(tf) ;
        distance.setUpdate(tf) ;
        rms.setUpdate(tf) ;

        aziLarge.setUpdate(tf) ;
        dipLarge.setUpdate(tf) ;
        errLarge.setUpdate(tf) ;
        aziInter.setUpdate(tf) ;
        dipInter.setUpdate(tf) ;
        errInter.setUpdate(tf) ;
        errSmall.setUpdate(tf) ;

        errorTime.setUpdate(tf) ;
        errorHoriz.setUpdate(tf) ;
        errorVert.setUpdate(tf) ;
        errorLat.setUpdate(tf) ;
        errorLon.setUpdate(tf) ;

        totalReadings.setUpdate(tf) ;
        usedReadings.setUpdate(tf) ;
        sReadings.setUpdate(tf) ;
        firstMotions.setUpdate(tf) ;
        externalId.setUpdate(tf) ;
        quality.setUpdate(tf) ;

        processingState.setUpdate(tf) ;
        //validFlag.setUpdate(tf) ; // bug? should be done for event only  -aww 2009/11/21
        dummyFlag.setUpdate(tf) ;

        depthFixed.setUpdate(tf) ;
        locationFixed.setUpdate(tf) ;
        timeFixed.setUpdate(tf)  ;
        timeStamp.setUpdate(tf);  // would need to update with SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) after commit() 

        // 2015/03/16 : origin schema change for geoid/model depths
        mdepth.setUpdate(tf);
        crustModelType.setUpdate(tf);
        crustModelName.setUpdate(tf);
        depthCrustType.setUpdate(tf);
        gtype.setUpdate(tf); //2015/03/19 -aww
    }

    /**
     * Put contents of this Solution into a NEW instance of jdbc package Origin (TableRow) object.
     */
    protected void toNewOriginRow() {

        lastPreforId  = orid.longValue(); // save last value of prefor -  aww

        long newOrid = assignNewOrid();
        //System.err.println("DEBUG SolutionTN toNewOriginRow new orid: " + newOrid);

        // make a NEW Origin table row object
        originRow = new Origin(newOrid, id.longValue(), getAuthority());
        this.prefor.setValue(newOrid); // prefor reset to new orid

        // version bumped with new orid 
        //bumpSrcVersion = true; // removed, use writePrefor to set flag -aww 2014/05/15

        toOriginRow();
    }
/*
ORID
EVID
PREFMAG
PREFMEC
//COMMID - currently null
BOGUSFLAG
DATETIME
LAT
LON
DEPTH
TYPE
ALGORITHM
//ALGO_ASSOC
AUTH
SUBSOURCE
DATUMHOR
DATUMVER
GAP
DISTANCE
WRMS
//STIME - currently null
ERHOR
SDEP
//ERLAT - currently null
//ERLON - currently null
TOTALARR
//TOTALAMP - currently null
NDEF
NBS
NBFM
LOCEVID
QUALITY
FDEPTH
FEPI
FTIME
VMODELID
CMODELID
LDDATE
RFLAG
MDEPTH
CRUST_TYPE
CRUST_MODEL
GTYPE
*/
    /** Put contents of this Solution into current instance of jdbc package Origin (TableRow) object. */
    protected void toOriginRow() {
        // allow updating the row
        originRow.setUpdate(true);

        // set NOT NULL values
        //(already done above)// originRow.setValue(Origin.AUTH, getAuthority());

        // set NOT NULL values (these were NOT set in constructor)
        if (datetime.isValidNumber()) {
            //Kludge for NCEDC leap corrected data, to convert to leap time -aww
            //originRow.setValue(Origin.DATETIME, DataSource.nominalToDbTimeBase(datetime.doubleValue())); // removed 2008/01/24 for leap secs -aww
            originRow.setValue(Origin.DATETIME, datetime);
        } else {
            originRow.setValue(Origin.DATETIME, 0);
        }
        if (lat.isValid()) {
            originRow.setValue(Origin.LAT, lat);
        } else {
            originRow.setValue(Origin.LAT, 0.0);
        }
        if (lon.isValid()) {
            originRow.setValue(Origin.LON, lon);
        } else {
            originRow.setValue(Origin.LON, 0.0);
        }

        // foreign keys (PROTECTED)
        //if (commid.isValid()) originRow.setValue(Origin.COMMID, commid);  // goes with event, not origin removed 5/13/03
        if (prefmag.isValid()) originRow.setValue(Origin.PREFMAG, prefmag);
        //if (prefmec.isValid()) originRow.setValue(Origin.PREFMEC, prefmec); // Event.prefmec may be derived using an older FP/MT origin -aww 2013/01/19

        if (depth.isValid())      originRow.setValue(Origin.DEPTH, depth);
        if (mdepth.isValid())     originRow.setValue(Origin.MDEPTH, mdepth);
        if (crustModelType.isValid())     originRow.setValue(Origin.CRUST_TYPE, crustModelType);
        if (crustModelName.isValid())     originRow.setValue(Origin.CRUST_MODEL, crustModelName);

        if (gtype.isValid()) {
            if (gtype.isBlankOrNull()) {
               originRow.setNullValue(Origin.GTYPE, true);
            }
            else {
               originRow.setValue(Origin.GTYPE, GTypeMap.toDbType(gtype.toString()));
            }
        }

        if (horizDatum.isValid()) originRow.setValue(Origin.DATUMHOR, horizDatum);
        if (vertDatum.isValid())  originRow.setValue(Origin.DATUMVER, vertDatum);
        if (type.isValid())       originRow.setValue(Origin.TYPE, type);
        if (algorithm.isValid())  originRow.setValue(Origin.ALGORITHM, algorithm);
        if (crustModel.isValid()) originRow.setValue(Origin.CMODELID, crustModel);
        if (velModel.isValid())   originRow.setValue(Origin.VMODELID, velModel);
        //if (source.isValid())     originRow.setValue(Origin.SUBSOURCE, getSource()); // null if undefined -aww 2005-06-21
        if (getSource().length() > 0) originRow.setValue(Origin.SUBSOURCE, getSource()); // defaults to app name -aww
        if (gap.isValid())        originRow.setValue(Origin.GAP, gap);
        if (distance.isValid())   originRow.setValue(Origin.DISTANCE, distance);
        if (rms.isValid())        originRow.setValue(Origin.WRMS, rms);
        if (errorTime.isValid())  originRow.setValue(Origin.STIME, errorTime);
        if (errorHoriz.isValid()) originRow.setValue(Origin.ERHOR, errorHoriz);
        if (errorVert.isValid())  originRow.setValue(Origin.SDEP, errorVert);
        if (errorLat.isValid())   originRow.setValue(Origin.ERLAT, errorLat);
        if (errorLon.isValid())   originRow.setValue(Origin.ERLON, errorLon);

        // force update on commit
        //totalReadings = new DataLong(phaseList.size());  // added to force total update on commit 12/08/2005 aww
        // may be less the phaseList.size() -aww 2011/05/11
        //totalReadings.setValue(phaseList.getChannelWithInWgtCount());
        if (totalReadings.isValid()) originRow.setValue(Origin.TOTALARR, totalReadings);
        
        //usedReadings.setValue(phaseList.getChannelUsedCount());  // force update on commit, may be less the phaseList.size() 2011/05/11 -aww
        if (usedReadings.isValid()) originRow.setValue(Origin.NDEF, usedReadings);

        //sReadings.setValue(phaseList.getChannelUsedCount("S"));  // force update on commit 2011/05/11 -aww
        if (sReadings.isValid())  originRow.setValue(Origin.NBS,sReadings );

        if (firstMotions.isValid()) originRow.setValue(Origin.NBFM,firstMotions );

        if (externalId.isValid()) originRow.setValue(Origin.LOCEVID, externalId);
        if (quality.isValid())    originRow.setValue(Origin.QUALITY, quality);

        if (dummyFlag.isValid())  originRow.setValue(Origin.BOGUSFLAG, dummyFlag);

        //if (debug) System.out.println(originRow.toString());
        //System.out.println("DEBUG toOriginRow() setting originRow processingState to: " + processingState);

        if (processingState.isValid()) originRow.setValue(Origin.RFLAG, processingState);

        // translate fixed flags
        if (depthFixed.isValid()) {
          if (depthFixed.booleanValue()) {
            originRow.setValue(Origin.FDEPTH, "y");
          } else {
            originRow.setValue(Origin.FDEPTH, "n");
          }
        }
        else originRow.setValue(Origin.FDEPTH, "n"); // force null to n - aww

        if (locationFixed.isValid()) {
          if (locationFixed.booleanValue()) {
            originRow.setValue(Origin.FEPI, "y");
          } else {
            originRow.setValue(Origin.FEPI, "n");
          }
        }
        else originRow.setValue(Origin.FEPI, "n"); // force null to n - aww

        if (timeFixed.isValid()) {
          if (timeFixed.booleanValue()) {
            originRow.setValue(Origin.FTIME, "y");
          } else {
            originRow.setValue(Origin.FTIME, "n");
          }
        }
        else originRow.setValue(Origin.FTIME, "n"); // force null to n - aww

        //if (solveDate.isValidNumber()) originRow.setValue(Origin.LOC_MODEL_DTM, solveDate.dateValue());
       
    }


    /** get ORIGIN.auth
    * Enforce schema length and notnull constraints. Truncate if necessary.
    * AUTH is a 'NOT NULL' attribute so if the internal value is null
    * default to "??".
    * */
    public String getAuthority() {
        //String str = (authority.isValid()) ?  authority.toString() : getEventAuthority(); // invalid was "??" -aww 2011/08/17, removed 2011/09/21
        String myauth = authority.toString();
        String str = null;
        if (authority.isValid()) {
           str = myauth;
        }
        else {
            str = EnvironmentInfo.getNetworkCode(); // invalid used to be ?? -aww 2011/08/17
            // ? Should we reset authority here to local net code value if it's invalid ? 
            if (!str.equals(EnvironmentInfo.DEFAULT_NETCODE) && !str.equals(myauth)) authority.setValue(str);
        }
        return str.substring(0, Math.min(str.length(), MAX_AUTH_SIZE));
    }

    /** Get ORIGIN.subsource.
    * Enforce schema length and notnull constraints. Truncate if necessary. If it is null
    * will default to EnvironmentInfo.getApplicationName()*/
    public String getSource() {
        String str = (source.isValid()) ?
            source.toString() : EnvironmentInfo.getApplicationName();
        return str.substring(0, Math.min(str.length(), MAX_SUBSOURCE_SIZE));
    }

    /** Get EVENT.auth.
    * Enforce schema length and notnull constraints. Truncate if necessary.
    * AUTH is a 'NOT NULL' attribute so if the internal value is null
    * default to the localNetCode. Solution constructor sets it to null DataString
    * (thus invoking method will reset it to local net code unless it's a trigger
    * or new event clone, in which case it uses the region authority.
    */
    public String getEventAuthority() {

        String myauth = eventAuthority.toString();
        String str = null;
        if (eventAuthority.isValid()) {
           str = myauth;
        }
        else { // shouldn't be null since db constraint is not null
            str = EnvironmentInfo.getNetworkCode(); // invalid used to be ?? -aww 2011/08/17
            // ? Should we reset eventAuthority here to local net code value if it's invalid ? 
            if (!str.equals(EnvironmentInfo.DEFAULT_NETCODE) && !str.equals(myauth)) eventAuthority.setValue(str);
        }

        // Reset to authority only if it's a clone or a trigger to regional eventAuthority  -aww 2011/08/16
        if (!fromDbase || isSubnetTrigger || resetAllEtypeAuth) { // added optional override property -aww 2011/08/18
           // Note the AUTH region for net name "XX" could be larger the the NET reporting region for net name  "XX"
           // Do we need to add "auth" column in assoc_region_group table for recovery from a join query ?
           // or instead have "special" names like CI-AUTH in gazetteer_region table from which the auth is parsed ?
           String eauth = getRegionAuthority();
           if (eauth != null && (eauth.length() > 0)) {
               str = eauth.substring(0, Math.min(eauth.length(), MAX_AUTH_SIZE));
           }
           setAuthRegion(str); // this sets only if they differ the eventAuth, not the "authority" for origin,magnitudes too, should it ?
        }

        return str.substring(0, Math.min(str.length(), MAX_AUTH_SIZE));
    }

    /** Get EVENT.subsource.
    * Enforce schema length and notnull constraints. Truncate if necessary. If it is null
    * will default to EnvironmentInfo.getApplicationName()*/
    String getEventSource() {
        String str = (eventSource.isValid()) ?
            eventSource.toString() : EnvironmentInfo.getApplicationName();
        return str.substring(0, Math.min(str.length(), MAX_SUBSOURCE_SIZE));
    }

    private void reportState() {
        System.out.println("SolutionTN.commit() : fromDbase= "+ fromDbase);
        System.out.println(" isDeleted() = "+ isDeleted());
        System.out.println(" hasChanged()= "+ hasChanged());
        System.out.println(" isStale()   = "+ isStale());
    }

    /*
      STATES:
      o  The class has a boolean deleteFlag.
      o  The fromDbase boolean tells us if we will 'insert' or 'update' on commit.
      o  If the individual DataObject.isUpdate() = true,  an update is necessary.
      Commit action matrix:
      fromDbase                =     true               false
      deleteFlag=true                dbaseDelete        noop
      isUpdate()=true                update             insert
      isUpdate()=false               noop               insert
     */

    /** Commit changes or delete to the underlying DataSource. To write to the
     *  DataSource, the flag DataSource.isWriteBackEnabled() must be true. This
     *  must be set with DataSource.setWriteBackEnabled(true) BEFORE data is
     *  read. <p>
     *  Behavior:<p>
     *  1) If event was not read from the database, new EVENT, ORIGIN and NETMAG rows are inserted.<p>
     *  2) If the event was read from the database and:<br>
     *     a) no changes were made, nothing is done to the database.<br>
     *     b) changes were made,  new ORIGIN and NETMAG rows are inserted, and
     *        the original EVENT row's <it>prefor</it> and <it> prefmag </it>
     *        entries are pointed at the new rows.<br> (
     *    Note that a new NETMAG row is written event if it has not changed.
     *    If the ORIGIN changes the NETMAG row <b>should</b> change to reflect new orid.
     *    This rule would need to be enforced by the application.
     *  If there is a problem with this commit it will do a rollback. This means that any
     *  transactions done before this one can be lost if DataSource.commit() is not called
     *  first.
    */
    public boolean commit() throws JasiCommitException {
        return doCommit(COMMIT_ALL);
    }

    private boolean doCommit(int commitLevel) throws JasiCommitException {
        //System.out.println("DEBUG SolutionTN doCommit getNeedsCommit() : " + getNeedsCommit() + " hasChanged() : " + hasChanged());
        // Note "commit" as much as possible to database, upon failure
        // rollback and continue or throw a fatal JasiCommitException
        // so as to trap the error in an outer block, perhaps rethrow
        // it back to caller method using commitStatusMessage string for info.
        boolean status = false;
        if (debug) reportState();

        // reset to new instance
        commitStatusMessage = new StringBuffer(1024);

        //if (bm == null) bm = new BenchMark();
        //else bm.reset();
        //bm.printTimeStamp("doCommit() STARTING");

        try {
          if (!DataSource.isWriteBackEnabled()) {
            commitStatusMessage.append("Database is NOT write enabled.");
            throw new JasiCommitException(commitStatusMessage.toString());
          }

          if (isDeleted()) {
            if (commitLevel == COMMIT_ALL) {
                status = doDeleteCommit(commitStatusMessage);
            } else {
              commitStatusMessage.append("Cannot commit the data to database. Solution is flagged as DELETED!");
              throw new JasiCommitException(commitStatusMessage.toString());
            }
          }
          else { // normal commit
            if (isStale()) {
              if (staleCommitOk) { 
                System.out.println("WARNING: SolutionTN.commit(), STALE ORIGIN RECALC NEEDED, OLD EVID: "+id.longValue());
              }
              else {
                System.out.println("ERROR: SolutionTN.commit(), STALE ORIGIN RECALC NEEDED, COMMIT ABORTED, OLD EVID: "+id.longValue());
                return false;
              }
            }


            commitOriginChanged = originHasChanged(); // save status at start - aww 2008/02/29

            // Line below throws JasiCommitException upon write failure
            status = commitEventOrigin(commitStatusMessage);      // true ==> this.fromDb == true
            if (! status) throw new JasiCommitException(commitStatusMessage.toString());

            for (;;) { // uses input level to determine break exit
                // Note the call order of data commits => waveforms, readings, then magnitudes.
                // Saves as much of the user data as possible before any error by using intermediate commits (dbCommit)
                // NOTE: should any of the commits below not count towards the boolean status value?

                if (commitLevel < COMMIT_WAVEFORMS) break;
                status &= commitEventAssocWaveforms(commitStatusMessage); // parentId!=id clone update of assocWaE (does dbCommit)

                if (commitLevel < COMMIT_READINGS) break;
                status &= commitOriginAssocLists(commitStatusMessage); // if no readings returns true (does dbCommit)

                if (commitLevel < COMMIT_MAGNITUDES) break;
                status &= commitMagnitudes(commitStatusMessage); // JasiCommitException on commit failure (does dbCommit)

                if (commitLevel < COMMIT_ALL) break;

                //
                // would do prefmec, and commid in here ... ?
                //

                break; // force exit at end of infinite loop
            } // end of infinite loop

            if (bumpSrcVersion) { // after all other updates call EPREF.bump_version once  // -aww 2008/04/15
                status &= dbaseBumpVersion(commitStatusMessage);
            }

            // after all done force state reset // status block moved from "for" loop to here -aww 2008/04/15
            if (status) {
              setNeedsCommit(false);  // all should match database data now
              setUpdate(false);       // redundant done by other commit methods ?
            }

          } // end of "undeleted" Solution commit block
        } // end of try block
        catch (JasiCommitException ex) {
          ex.printStackTrace();
          status = false; // force false since it could have been set true by a method called before the exception
        }
        finally {
          commitStatusMessage.append("\nSolutionTN commit status value: ").append(status);
          if (debug || JasiDatabasePropertyList.debugSQL)
              System.out.println("DEBUG SolutionTN commit status message is:\n"+commitStatusMessage);
        }
        // Do we want to throw JasiCommitException if !status?
        if (! status) throw new JasiCommitException("SolutionTN commit error!");

        //bm.printTimeStamp("doCommit COMPLETED");
            // Don't commit other data if event/origin commit status false, might have bad evid/orid mappings
        commitOriginChanged = originHasChanged(); // save status at end - aww 2008/02/29

        //System.out.println("DEBUG id: "+id.longValue()+" needsCommit: "+needsCommit+" changed: "+hasChanged());
        return status; // false here implies commit to dbase of some data botched
    }

    private boolean dbaseBumpVersion(StringBuffer statusMessage) { // -aww 2008/04/15
        final String sql = "{ ?=call EPREF.bump_version(?) }";  // does db commit
        long retVal = -1l;
        CallableStatement cs = null;
        try {
            Connection conn = DataSource.getConnection();
            cs = conn.prepareCall(sql);
            cs.registerOutParameter (1, java.sql.Types.INTEGER);
            cs.setLong(2, id.longValue());
            cs.execute();
            retVal = cs.getInt(1); // version
        } catch (SQLException ex) {
            System.out.println("SQL=\n"+sql);
            ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, cs, debug);
          try {
            if (cs != null) cs.close();
          }
          catch(SQLException ex) {
            System.err.println("SolutionTN.dbaseBumpVersion unable to close statement.");
          }
        }

        if (retVal > 0l) {
          statusMessage.append("\nbumpVersion for id: " +StringSQL.valueOf(id) + " succeeded : " + retVal);
          this.srcVersion.setValue(retVal); // reset to the value returned -aww 2008/04/16
          bumpSrcVersion = false; // reset upon successful bump
        }
        else {
          statusMessage.append("\nbumpVersion for id: " +StringSQL.valueOf(id) + " failed! : " + retVal);
        }
        return (retVal > 0l);
    }

    public String getRegionAuthority() {

       LatLonZ llz = this.getLatLonZ();
       // must be trigger, no location so region unknown 
       if (llz.isNull()) return null;

       return getRegionAuthority(llz.getLat(), llz.getLon());
    }

    public static String getRegionAuthority(double lat, double lon) {

        if (!authRegionEnabled) return null; // short-circuit until stored procs are updated in AQMS db -aww 2011/08/16

        if (lat == 0. && lon == 0.) return null; // must be trigger, no location so region unknown 

        final String sql = "{ ?=call GEO_REGION.authOfRegionInGroup(?,?,?) }";  // does db query
        String retVal = null;
        CallableStatement cs = null;
        try {
            Connection conn = DataSource.getConnection();
            cs = conn.prepareCall(sql);
            cs.registerOutParameter (1, java.sql.Types.VARCHAR);
            cs.setDouble(2, lat);
            cs.setDouble(3, lon);
            cs.setString(4, "AUTH");
            cs.execute();
            retVal = cs.getString(1); // region name
        } catch (SQLException ex) {
            System.out.println("SQL=\n"+sql);
            ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, cs, debug);
          try {
            if (cs != null) cs.close();
          }
          catch(SQLException ex) {
            System.err.println("SolutionTN.getRegionAuthority unable to close statement.");
          }
        }

        if (debug) System.out.println("SolutionTN.getRegionAuthority  return =" + retVal);

        // Do we need to truncate string to first 2-chars or out to a "delimiter" like - . _ ?
        return retVal;
    }

    // Event/Origin UPDATE/INSERT
    private boolean commitEventOrigin(StringBuffer statusMessage) throws JasiCommitException {

        newOrigin2Assoc = false; // reset flag, associations are needed -aww 2010/06/30

        if (debug) System.out.println("commitEventOrigin");
        if (! getNeedsCommit() && ! hasChanged()) {
          statusMessage.append("\nNo change to Event/Origin.");
          return true; // make no-op condition "true" - aww 07/28/2004
        }
        boolean status = false;
        try {
          // comment used to inserted here - a bug -aww 02/17/2006
          if (fromDbase) { // update if originally from the dbase
            if (debug) System.out.println("Update existing event...");
            status = dbaseUpdate();
          } else { // insert if a new Solution
            if (debug) System.out.println("Insert new event...");
            status = dbaseInsert();
          }
        } catch (Exception ex)  {   // SQL or JasiCommitException
          status = false;
          statusMessage.append("\nEvent/Origin insert/update exception for event: "+id.longValue());
          System.err.println(ex);
          ex.printStackTrace();
        }
        if (status) {
          status = doDbCommit();
          if (status) {
            setTimeStamp(new java.util.Date());  // approximate, would need to update with SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) after commit() 
            statusMessage.append("\nEvent/Origin commit succeeded for event: " +id.longValue());
          }
          else {
            statusMessage.append("\nEvent/Origin commit failed for event: " + id.longValue());
          }
        }
        else {
          statusMessage.append("\nEvent/Origin insert/update write failed for event: " + id.longValue() +
                  " lastPrefor: " + lastPreforId); 
          if (! rollback()) statusMessage.append("\nRollBack failed.");
        }

        if (status) setUpdate(false); // now there's a dbase row fromDb = true;
        else throw new JasiCommitException(statusMessage.toString());

        return status;
    }

    /** Insert the event comment if it is new. Do this BEFORE committing the Event row
    * so the 'commid' will get written.
    * Returns true if comment field unchanged (a noop) or row successfully inserted.
    * */
    public boolean dbaseInsertComment() {
        if (! (comment.isUpdate() && comment.isValid()) ) return true;
        if (debug) System.out.println("dbaseInsertComment");

        boolean status = false;

        String rmk = comment.toString();

        long myCommid = 0l;

        if ( rmk.length() > 0) {
            myCommid = SeqIds.getNextSeq(DataSource.getConnection(), org.trinet.jdbc.table.Remark.SEQUENCE_NAME);
            status = insertComment(myCommid, rmk);
        }
        else status = true;

        /*
        long myCommid = getCommidValue();
        if (myCommid <= 0l) { // get new comment ID key sequence #
            myCommid = SeqIds.getNextSeq(DataSource.getConnection(), org.trinet.jdbc.table.Remark.SEQUENCE_NAME);
        }
        //
        // Problem with inserting comment lines as multiple rowNum rows:
        //final long rowNum = 1l;
        // Make a dbase table object
        // Update an existing comment for the event rather than make new row -aww 04/28/2005
        //boolean updateExisting = true;
        //   Do you set blank or delete a row when the comment is shortened to fewer rows?
        //boolean status = DataTableRowUtil.rowToDb( DataSource.getConnection(), new Remark(myCommid, rowNum, comment.toString()), updateExisting);
        //
        //

        // PreparedStatement insert version with bind variables
        PreparedStatement ps = null;
        try {
            Connection conn = DataSource.getConnection();
            //
            //String sql = "{ ?=call EPREF.insertComment(?,?,?) )";
            //if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(sql);
            //ps= conn.prepareCall(sql);
            //ps.registerOutParameter(1, java.sql.Types.INTEGER);
            //ps.setLong(2, myCommid);
            //ps.setInt(3, 1);
            //ps.setString(4, comment.toString());
            //status = (ps.executeUpdate() > 0);
            //if (status) status = (ps.getInt(1) >= 0);
            //
            String sql = 
              "MERGE INTO REMARK r USING (SELECT ? id, ? line, ? rmk FROM dual) d " +
              "ON (r.commid=d.id AND r.lineno=d.line) " +
              "WHEN MATCHED THEN UPDATE SET r.remark=d.rmk " +
              "WHEN NOT MATCHED THEN INSERT (r.commid,r.lineno,r.remark) VALUES (d.id,d.line,d.rmk)";
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(sql);
            ps= conn.prepareStatement(sql);
            ps.setLong(1, myCommid);
            ps.setInt(2, 1);
            ps.setString(3, comment.toString());
            status = (ps.executeUpdate() > 0);
        }
        catch (SQLException ex) {
            status = false;
            ex.printStackTrace();
        }
        finally {
            try {
                if (ps!= null) ps.close();
            }
            catch (SQLException ex2) {}
        }
        */
        // Update event row in database with a committed commid -aww 02/17/2006
        if (status) status &= dbaseSetCommidForEvent(myCommid);
        return status;
    }

    private PreparedStatement psRmkSetInsertStmt = null;
    private static final String rmkSetInsertStr = "INSERT INTO REMARK (commid,lineno,remark) VALUES (?, ?, ?)";
    private boolean insertComment(long id, String rmk) {
        int lineno = 0;
        int status = 0;
        try {
            if (psRmkSetInsertStmt == null) {
                if (debug || JasiDatabasePropertyList.debugSQL) {
                    System.out.println(rmkSetInsertStr);
                }
                psRmkSetInsertStmt = DataSource.getConnection().prepareStatement(rmkSetInsertStr); 
            }

            StringTokenizer stoke = new StringTokenizer(rmk, "\n");
            while(stoke.hasMoreTokens()) {
                rmk = stoke.nextToken();
                lineno++;
                psRmkSetInsertStmt.setLong(1, id);
                psRmkSetInsertStmt.setLong(2, lineno);
                psRmkSetInsertStmt.setString(3, rmk);
                psRmkSetInsertStmt.addBatch();
            }

            boolean close = true;
            status = batchToDb(psRmkSetInsertStmt, close); // close statement after commit
            if (debug || JasiDatabasePropertyList.debugSQL) {
                System.out.println("DEBUG SolutionTN insertComment batchToDb status: " + status);
            }
        }
        catch (SQLException ex) {
            System.err.println("ERROR: SolutionTN insertComment evid:" + getId().longValue() + " commid,lineno: " +id+ "," + lineno); 
            ex.printStackTrace();
        }
        finally {
            try {
              if (psRmkSetInsertStmt != null) {
                  psRmkSetInsertStmt.close();
                  psRmkSetInsertStmt = null;
              }
            }
            catch (SQLException ex2) {
                ex2.printStackTrace();
            }
        }
        return (status > 0);
    }
    private int batchToDb(Statement stmt, boolean close) {
        if (stmt == null) return 0;  // no-op, assume none

        int [] updateCounts = null;
        int status = -1;

        try {
            updateCounts = stmt.executeBatch();
            status = updateCounts.length;
            for (int idx = 0; idx<updateCounts.length; idx++) {
                if (updateCounts[idx] == Statement.EXECUTE_FAILED) {
                    status = -idx; // return negative index, flagging error -aww 2010/09/15
                    System.err.println("ERROR SolutionTN executeBatch insert comment failed line: " + idx);
                    break;
                }
            }
       }
       catch(BatchUpdateException b) {
                System.err.println("-----BatchUpdateException-----");
                System.err.println("SQLState:  " + b.getSQLState());
                System.err.println("Message:  " + b.getMessage());
                System.err.println("Vendor:  " + b.getErrorCode());
                System.err.print("Update counts:  ");
                updateCounts = b.getUpdateCounts();
                b.getNextException();
                System.err.println("SQLState:  " + b.getSQLState());
                System.err.println("Message:  " + b.getMessage());
                for (int i = 0; i < updateCounts.length; i++) {
                    System.err.print(updateCounts[i] + "   ");
                }
                System.err.println("");
        }
        catch(SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            try {
                if (close) stmt.close();
            }
            catch (SQLException ex2) {
                ex2.printStackTrace();
            }
        }
        return status;
    }

    private boolean dbaseSetCommidForEvent(long myCommid) {
        DataObject obj = eventRow.getDataObject(Event.COMMID);
        if (debug) System.out.println(" dbaseSetCommidForEvent(" + myCommid + ")" + " commid " + commid );
        if (obj != null && obj.equalsValue(Long.valueOf(myCommid))) return true; // no-op if same value -aww 2008/02/20

        eventRow.setUpdateValues(false); // aww 2009/05/13

        boolean status = true;
        // Set event foreign key to point at input commid.
        eventRow.setValue(Event.COMMID, myCommid);

        if (myCommid == 0l) eventRow.setNullValue(Event.COMMID, true); // reset

        // Need setUpdate(true) to use key value in where condition
        eventRow.setProcessing(DataTableRowStates.UPDATE);
        /*
        eventRow.getDataObject(Event.EVID).setUpdate(true);
        if (debug) System.out.println("dbaseSetCommidForEvent() eventRow.updateRow(conn)");
        status = (eventRow.updateRow(DataSource.getConnection()) > 0);
        eventRow.getDataObject(Event.EVID).setUpdate(false);
        if (debug) System.out.println("dbaseSetCommidForEvent() eventRow update return status:" + status);
        */
        // PreparedStatement update version with bind variables
        PreparedStatement ps = null;
        final String sql = "UPDATE EVENT SET COMMID=? WHERE (EVID=?)";
        try {
            Connection conn = DataSource.getConnection();
            ps= conn.prepareStatement(sql);
            if (myCommid != 0l) ps.setLong(1, myCommid);
            else ps.setNull(1, java.sql.Types.BIGINT);
            ps.setLong(2, id.longValue());
            status = (ps.executeUpdate() > 0);
        }
        catch (SQLException ex) {
            status = false;
            ex.printStackTrace();
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, ps, debug);
            Utils.closeQuietly(ps);
        }
        if (status) commid.setValue(myCommid);
        return status;
    }

    /**
     * Update Event and Origin table rows in the dbase.
     * Returns true if there was any change to the dbase.
     * Note: writes everything from Origin anew if location has changed
     * else only 'updates' Origin and Event row data.
     * */
    private boolean dbaseUpdate() {
        // Update database EVENT and ORIGIN row (set EVENT.prefor, ORIGIN.evid foreign keys)
        // will inserts a NEW Origin row in database, if it location has changed 
        boolean status = dbaseUpdateEvent();
        if (status) setUpdate(false); // sets this.fromDb to true
        return status;
    }

    private boolean dbaseUpdateEvent() {

       if (debug) System.out.println("dbaseUpdateEvent toEventRow()");

       toEventRow(); // updates eventRow with new or changed values: id, etype, select, etc.

       // insert comment, and update commid in the Event row - aww 02/17/2006
       boolean status = dbaseInsertComment(); // eventSetUpdate(false) below to clears commid field update
       if (debug) System.out.println("dbaseUpdateEvent dbaseInsertComment() status: " + status);

       status = dbaseUpdateOrigin();
       if (debug) System.out.println("dbaseUpdateEvent dbaseUpdateOrigin() status: " + status);
       if (!status) return false;

       // UPDATE: database table EVENT.prefor
       // setting of EVENT.prefor or not is done by the call to dbaseUpdateOrigin above
       //status &= dbaseSetPreforForEvent(); // NO, we have to check rules when autoSetPrefOr mode is true!
       //status &= writePrefor((SolutionTN)this, commitStatusMessage); // this is done in above call to dbaseUpdateOrigin
       //if (debug) System.out.println("dbaseUpdateEvent dbaseSetPreforForEvent() status: " + status);
       //if (!status) return false;

       // update event row when one the these fields has been updated - logic below added aww 2009/11/21 
        boolean updateEventRow = false;
        if (eventType.isUpdate() || eventAuthority.isUpdate() || eventSource.isUpdate() || validFlag.isUpdate()) {
            if (debug) {
                System.out.println("updateEventRow: event type, auth, subsource or selectFlag changed");
                System.out.println(eventType.isUpdate() +" "+ eventAuthority.isUpdate() +" "+ eventSource.isUpdate() +" "+ validFlag.isUpdate());
            }
            updateEventRow = true;
        }
        if (updateEventRow) {
          // reset filed status to not updatable 
          eventRow.setUpdateValues(false); // aww 2009/05/13
          eventRow.setProcessing(DataTableRowStates.UPDATE);

          // reset those fields that changed
          // Did event authority change?
          if (eventAuthority.isUpdate()) {
            eventRow.setValue(Event.AUTH, getEventAuthority());
          }
          // Did event source change?
          if (eventSource.isUpdate()) {
            eventRow.setValue(Event.SUBSOURCE, getEventSource());
          }
          // Did user toggle event delete/undelete ?
          // note: validFlag is written to Event.selectflag, dummyFlag is written to Origin.bogusflag
          if (validFlag.isUpdate()) {
            eventRow.setValue(Event.SELECTFLAG, validFlag);
          }
          // Did user change event type?
          if (eventType.isUpdate()) {
            eventRow.setValue(Event.ETYPE, EventTypeMap3.getMap().toDbType(eventType.toString()));
          }

          // now update the Event table row in database with the changes
          eventRow.getDataObject(Event.EVID).setUpdate(true);
          status = (eventRow.updateRow(DataSource.getConnection()) > 0);
          eventRow.getDataObject(Event.EVID).setUpdate(false);
          if (debug) System.out.println("dbaseUpdateEvent event type updateRow status: " + status);
       }

       if (status) eventSetUpdate(false);

       return status;
    }

    private boolean dbaseUpdateOrigin() {

        if ( ! originHasChanged() ) { // nothing to update
            if (debug) System.out.println("DEBUG SolutionTN.dbaseUpdateOrigin !originHasChanged a no-op!");

            return true;
        }
        else if (originLocationHasChanged()) { // revert to insert mode
            return dbaseInsertOrigin();
        }

        // Only descriptive info should be updated 
        // (state or other descriptive data of Origin)
        if (debug) System.out.println("dbaseUpdateOrigin toOriginRow()");
        toOriginRow(); // aww 02/11/2005
        originRow.setProcessing(DataTableRowStates.UPDATE);
        //if (debug) System.out.println(originRow.toString());

        // Need setUpdate(true) to use key value in where condition
        DataLong rowOrid = (DataLong) originRow.getDataObject(Origin.ORID);
        rowOrid.setUpdate(true);
        boolean status = (originRow.updateRow(DataSource.getConnection()) > 0);
        rowOrid.setUpdate(false);
        // don't continue if Origin row didn't go
        if (debug) System.out.println("dbaseUpdateOrigin updateRow status: " + status);
        if (! status) return false;
        else {
            if (dbAttributionEnabled) {
                if (debug) System.out.println("dbaseUpdateOrigin setting ORIGIN attribution");
                // revised to return table value set -aww 2010/08/25
                // Option added to preserve previous origin attribution, of who did original when you relocate catalog e.g. batch hypomag -aww 2015/04/03
                //System.out.println("DEBUG ORIGIN commit resets attrib: " + commitResetsAttrib + " getWho:\"" + getWho() + "\"" +
                //                                        " ---: " + getWho().equalsValue("---") + " isNullblank:" + getWho().isBlankOrNull());
                String attrib = ((JasiDbReader) getDataReader()).setAttribution(DataSource.getConnection(),
                                                        rowOrid.longValue(),
                                                        "ORIGIN",
                                                        (!commitResetsAttrib && !(getWho().equalsValue("---") || getWho().isBlankOrNull()) ) ?
                                                                getWho().toString() : EnvironmentInfo.getUsername()
                                                        ); 
                //setWho(EnvironmentInfo.getUsername()); // no, its an alias here
                //loadAttribution(); // key mapping of the PREFOR, hopefully the same as orid just committed -aww removed 2010/08/25 
                if (attrib != null) setWho(attrib); // new way -aww 2010/08/25
            }
        }

        //NOTE: no fatal status error here -aww 07/12/2006
        // Below removed 2010/08/23 - if origin hasn't changed, its error should be the same
        //status = dbaseAssociateWithOriginError(true);
        //if (debug) System.out.println("dbaseUpdateOrigin dbaseAssociateWithOriginError status: " + status);

        // UPDATE: database table ORIGIN.evid, ORIGIN.RFLAG (ProcessingState)
        status = dbaseSetEvidForOrigin();
        if (debug) System.out.println("dbaseUpdateOrigin dbaseSetEvidForOrigin status: " + status);

        // XXX-updateEventPreforOfType
        SolutionTN solTN = (SolutionTN) this;
        // Bugfix 2014/10/12: needed to update event.prefor when property (useEventPreforTable == false) -aww
        status &= (useEventPreforTable) ?  writePrefor(solTN, commitStatusMessage) : dbaseSetPreforForEvent();

        //System.out.println("DEBUG dbaseUpdateOrigin writePrefor return status: " + status);

        if (status) { // ?? do we need to check status, or just do it??
            originSetUpdate(false);
        }

        return status;
    }

    /**
     * Insert new Event and Origin table rows in the dbase
     */
    // To avoid key constraint errors you must:
    // 1) First set Event row's foreign keys = null
    // 2) Then insert the Event row
    // 3) Then insert the Origin, Mec, NetMag, Comment rows
    // 4) Then set key fields & update the Event and Origin table rows

    private boolean dbaseInsert() {
        boolean status = true;

        if (debug) System.out.println("dbaseInsert toEventRow()");
        toEventRow(); // sets new 'evid' if none (eventRow.id)
        eventRow.setProcessing(DataTableRowStates.INSERT);

        if (debug) System.out.println("nulling keys...");
        // set Event row's foreign keys = null
        nullEventForeignKeys();

        // insert the Event row (w/ null foreign keys)
        //status  = (eventRow.insertRow(DataSource.getConnection()) > 0);

        // PreparedStatement update version with bind variables
        PreparedStatement ps = null;
        final String sql = "INSERT INTO EVENT (EVID,AUTH,SUBSOURCE,ETYPE,SELECTFLAG) VALUES (?,?,?,?,?)";
        try {
            Connection conn = DataSource.getConnection();
            ps= conn.prepareStatement(sql);

            ps.setLong(1, id.longValue());
            ps.setString(2, getEventAuthority());
            ps.setString(3, getEventSource());
            ps.setString(4, EventTypeMap3.getMap().toDbType(eventType.toString()));
            ps.setInt(5, validFlag.intValue());

            status = (ps.executeUpdate() > 0);
        }
        catch (SQLException ex) {
            status = false;
            ex.printStackTrace();
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, ps, debug);
            Utils.closeQuietly(ps);
        }

        if (! status) return false;
        /*
        else // set attribution here
            if (dbAttributionEnabled) {
                if (debug) System.out.println("dbaseInsert setting EVENT attribution");
                ((JasiDbReader) getDataReader()).setAttribution(DataSource.getConnection(),
                                                        id.longValue(),
                                                        "EVENT",
                                                        EnvironmentInfo.getUsername()
                                                        ); 
            }
        */
        // insert comment, and update commid in the Event row - aww 02/17/2006
        status = dbaseInsertComment(); // eventSetUpdate(false) below to clears commid field update
        if (debug) System.out.println("dbaseInsert dbaseInsertComment() status: " + status);

        eventSetUpdate(false);

        if (debug) System.out.println("insert origin");
        // insert the Origin row
        status = dbaseInsertOrigin();

        // after insert the Origin row, then set origin foreign keys
        status &= dbaseSetPreforForEvent(); // see change at end of dbaseInsertOrigin dated - aww 2010/08/23

        return status;
    }

    private void nullEventForeignKeys() {
        eventRow.setValue(Event.PREFOR,  null);
        eventRow.setValue(Event.PREFMAG, null);
        eventRow.setValue(Event.PREFMEC, null);
        eventRow.setValue(Event.COMMID,  null);
    }

    /**
     * Insert the new Origin w/null keys (e.g. Netmag,
     * Arrivals, Amps) if successful resets foreign keys to new orid.
     */
    private boolean dbaseInsertOrigin() {
        if ( ! originHasChanged() ) return true;
        // <ORIGIN> insert the Origin row (w/ null foreign keys)
        if (debug) System.out.println("dbaseInsertOrigin toNewOriginRow()");
        toNewOriginRow();          // assigns new 'orid'
        originRow.setProcessing(DataTableRowStates.INSERT);
        if (debug) System.out.println("dbaseInsertOrigin nullOriginForeignKeys()");
        nullOriginForeignKeys();

        // Added set logic here (since call to dbaseSetEvidForOrigin removed near end below) -aww 2010/08/23
        // Set origin row foreign key evid
        originRow.setValue(Origin.EVID, id);
        // Set origin row processing state from current value: A, H, F
        
        if (processingState.isValid() && commitResetsRflag && !EnvironmentInfo.isAutomatic() &&
                !processingState.toString().equals(JasiProcessingConstants.STATE_FINAL_TAG )) {
           // states not "F" are forced "I" when not in automatic mode and commitResetsRflag -aww 2014/04/09
           processingState.setValue(JasiProcessingConstants.STATE_INTERMEDIATE_TAG); // states not "F" are forced "I" -aww 2009/05/08
           //System.out.println("DEBUG dbaseInsertOrigin() setting to I processingState: " + processingState);
           originRow.setValue(Origin.RFLAG, processingState); // 02/10/2005
        }

        //if (debug) System.out.println(originRow.toString());
        // don't continue if Origin row didn't go
        //boolean status = (originRow.insertRow(DataSource.getConnection()) > 0);

        boolean status = false;

        PreparedStatement ps = null;
        Connection conn = null;
        final String sql =
            "INSERT INTO ORIGIN (ORID,EVID,BOGUSFLAG,DATETIME,LAT,LON,DEPTH,TYPE,"+ // 8
            "ALGORITHM,AUTH,SUBSOURCE,DATUMHOR,DATUMVER,GAP,DISTANCE,WRMS,ERHOR,SDEP,TOTALARR,"+ // 19
            "NDEF,NBS,NBFM,QUALITY,FDEPTH,FEPI,FTIME,VMODELID,CMODELID,RFLAG,MDEPTH,CRUST_TYPE,CRUST_MODEL, GTYPE)" + // 33
            "VALUES (?,?,?,TRUETIME.putEpoch(?,'UTC'),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            conn = DataSource.getConnection();
            ps= conn.prepareStatement(sql);

            int icol = 1;

            //ORID
            ps.setLong(icol++, prefor.longValue());

            //EVID
            ps.setLong(icol++, id.longValue());

            //BOGUSFLAG
            if (dummyFlag.isValid()) ps.setObject(icol++, dummyFlag.toString(),Types.SMALLINT);
            else ps.setObject(icol++, null, Types.SMALLINT);

            //DATETIME
            ps.setDouble(icol++, (datetime.isValidNumber() ? datetime.doubleValue() : 0.));

            //LAT
            ps.setDouble(icol++, (lat.isValidNumber() ? lat.doubleValue() : 0.));

            //LON
            ps.setDouble(icol++, (lon.isValidNumber() ? lon.doubleValue() : 0.));

            //DEPTH
            ps.setDouble(icol++, (depth.isValidNumber() ? depth.doubleValue() : 0.));

            //TYPE
            if (type.isValid()) ps.setString(icol++, type.toString());
            else ps.setString(icol++, null);

            //ALGORITHM
            if (algorithm.isValid())  ps.setString(icol++, algorithm.toString());
            else ps.setString(icol++, null);

            //AUTH
            ps.setString(icol++, getAuthority());

            //SUBSOURCE
            String str = getSource().trim();
            ps.setString(icol++, ((str.length() > 0) ? str : null));

            //DATUMHOR
            if (horizDatum.isValid()) ps.setString(icol++, horizDatum.toString());
            else ps.setString(icol++, null);

            //DATUMVER
            if (vertDatum.isValid())  ps.setString(icol++, vertDatum.toString());
            else ps.setString(icol++, null);

            //GAP
            if (gap.isValid()) ps.setDouble(icol++, gap.doubleValue());
            else ps.setNull(icol++, Types.DOUBLE);

            //DISTANCE
            if (distance.isValid()) ps.setDouble(icol++, distance.doubleValue());
            else ps.setNull(icol++, Types.DOUBLE);

            //WRMS
            if (rms.isValid()) ps.setDouble(icol++, rms.doubleValue());
            else ps.setNull(icol++, Types.DOUBLE);

            //ERHOR
            if (errorHoriz.isValid()) ps.setDouble(icol++, errorHoriz.doubleValue());
            else ps.setNull(icol++, Types.DOUBLE);

            //SDEP
            if (errorVert.isValid()) ps.setDouble(icol++, errorVert.doubleValue());
            else ps.setNull(icol++, Types.DOUBLE);

            //TOTALARR
            // force update on commit
            //totalReadings = new DataLong(phaseList.size()); // removed -aww 2011/05/11
            // may be less the phaseList.size(), this is done in toOriginRow() method -aww 2011/05/11
            //totalReadings.setValue(phaseList.getChannelWithInWgtCount());
            if (totalReadings.isValid()) ps.setInt(icol++, totalReadings.intValue());
            else ps.setNull(icol++, Types.INTEGER);

            //NDEF
            if (usedReadings.isValid()) ps.setInt(icol++, usedReadings.intValue());
            else ps.setNull(icol++, Types.INTEGER);

            //NBS
            if (sReadings.isValid())  ps.setInt(icol++, sReadings.intValue());
            else ps.setNull(icol++, Types.INTEGER);

            //NBFM
            if (firstMotions.isValid()) ps.setInt(icol++, firstMotions.intValue());
            else ps.setNull(icol++, Types.INTEGER);

            //QUALITY
            if (quality.isValid()) ps.setDouble(icol++, quality.doubleValue());
            else ps.setNull(icol++, Types.DOUBLE);

            //FDEPTH
            if (depthFixed.isValid()) {
              if (depthFixed.booleanValue()) {
                ps.setString(icol++, "y");
              } else {
                ps.setString(icol++, "n");
              }
            }
            else ps.setString(icol++, "n"); // force null to n - aww

            //FEPI
            if (locationFixed.isValid()) {
              if (locationFixed.booleanValue()) {
                ps.setString(icol++, "y");
              } else {
                ps.setString(icol++, "n");
              }
            }
            else ps.setString(icol++, "n"); // force null to n - aww

            //FTIME
            if (timeFixed.isValid()) {
              if (timeFixed.booleanValue()) {
                ps.setString(icol++, "y");
              } else {
                ps.setString(icol++, "n");
              }
            }
            else ps.setString(icol++, "n"); // force null to n - aww

            //VMODELID
            if (velModel.isValid()) ps.setString(icol++, velModel.toString());
            else ps.setString(icol++, null);

            //CMODELID
            if (crustModel.isValid()) ps.setString(icol++, crustModel.toString());
            else ps.setString(icol++, null);

            //RFLAG
            if (processingState.isValid()) ps.setString(icol++, processingState.toString());
            else ps.setString(icol++, null);

            //MDEPTH
            if (mdepth.isValid()) ps.setDouble(icol++, mdepth.doubleValue());
            else ps.setNull(icol++, Types.DOUBLE);

            //CRUST_TYPE
            if (crustModelType.isValid()) ps.setString(icol++, crustModelType.toString());
            else ps.setString(icol++, null);

            //CRUST_MODEL
            if (crustModelName.isValid()) ps.setString(icol++, crustModelName.toString());
            else ps.setString(icol++, null);

            //GTYPE
            //if (gtype.isValid()) ps.setString(icol++, gtype.toString());
            if (gtype.isValid() && !gtype.isBlankOrNull()) ps.setString(icol++, GTypeMap.toDbType(gtype.toString()));
            else ps.setString(icol++, null);

            status = (ps.executeUpdate() > 0);
        }
        catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(conn, ex, "Error insert to Origin table", "dbaseInsertOrigin sql error: ");
            status = false;
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, ps, debug);
            try {
                if (ps!= null) {
                    ps.close();
                }
            } catch (SQLException e) {
                JiggleExceptionHandler.handleDbException(conn, e);
            }
        }

        if (! status) return false;
        else { // set attribution here
            if (dbAttributionEnabled) {
                if (debug) System.out.println("dbaseInsert setting ORIGIN attribution");
                // revised to return table value set -aww 2010/08/25
                // Option added to preserve previous origin attribution, of who did original when you relocate catalog e.g. batch hypomag -aww 2015/04/03
                //System.out.println("DEBUG ORIGIN commit2 resets attrib: " + commitResetsAttrib + " getWho:\"" + getWho() + "\"" +
                //                                        " ---: " + getWho().equalsValue("---") + " isNullblank:" + getWho().isBlankOrNull());
                String attrib = ((JasiDbReader) getDataReader()).setAttribution(DataSource.getConnection(),
                                                        prefor.longValue(),
                                                        "ORIGIN",
                                                        (!commitResetsAttrib && !(getWho().equalsValue("---") || getWho().isBlankOrNull()) ) ?
                                                                getWho().toString() : EnvironmentInfo.getUsername()
                                                        ); 
                //setWho(EnvironmentInfo.getUsername()); // no, its an alias here
                //loadAttribution(); // read back the key mapping -aww removed 2010/08/25 
                if (attrib != null) setWho(attrib); // new way -aww 2010/08/25
            }
            //this.solveDate.setValue((new java.util.Date()); // approx. time of commit 01/08/2008 - aww
        }

        newOrigin2Assoc = true; // added to flag associations are needed -aww 2010/06/30

        status = dbaseAssociateWithOriginError(false); // should be a new row
        if (debug) System.out.println("dbaseInsertOrigin dbaseAssociateWithOriginError status: " + status);

        //status &= dbaseSetPrefOr(); // replaced, don't set event.prefor here done inside insert/update event row methods -aww 2010/08/23
        //status &= dbaseSetEvidForOrigin(); // done by insert statement above -aww 2010/08/23
        //if (!status) return false;


        // XXX-updateEventPreforOfType
        SolutionTN solTN = (SolutionTN) this;
        // Bugfix 2014/10/12: needed to update event.prefor when property (useEventPreforTable == false) -aww
        //status &= writePrefor(solTN, commitStatusMessage);
        status &= (useEventPreforTable) ?  writePrefor(solTN, commitStatusMessage) : dbaseSetPreforForEvent();
        //System.out.println("DEBUG dbaseInsertOrigin writePrefor return status: " + status);
        //

        if (status) { // ?? do we need to check this, or just do it??
            originSetUpdate(false);
        }

        /* TEST REMOVAL HERE, moved to Magnitude.commit() - aww 01/13/2005
        // make sure preferred Magnitude has the new Orid
        MagnitudeTN magTN = (MagnitudeTN) magnitude;
        if ( magTN != null &&
             !magTN.hasNullValue() &&
             magTN.getOridValue() != getOridValue() // don't set if already equal
        ) magTN.setOrid(getOridValue());       // because it causes hasChanged() true
        */

        return status;
    }

    private boolean writePrefor(SolutionTN sol, StringBuffer statusMessage) {

        // Here disable update using hidden user set property (remove this code when db schema is updated) -aww 2013/01/15
        if (! useEventPreforTable) return true;

        // input value must be not "NULL"
        if (! sol.orid.isValidNumber()) { // could be error, so warn and do nothing
           statusMessage.append("\nWARNING writePrefor input solution orid is NOT valid (NULL), no database write ");
           return true; // a no-op
        }
        
        if (sol.isDeleted()) { // a deletion could be error, so warn 
           statusMessage.append("\nWARNING writePrefor input solution is flagged DELETED, no EventPrefor update for orid: ");
           statusMessage.append(sol.orid.longValue());
           statusMessage.append(" type: ");
           statusMessage.append(sol.type);
           return true;
        }


        int checkPriority = (autoSetPrefOr) ? 1 : 0;
        final String sql = "{ ?=call EPREF.setprefor_type(?,?,?,?,?,?,?) }";
        CallableStatement cs = null;
        boolean status = false;
        try {
            Connection conn = DataSource.getConnection();
            cs = conn.prepareCall(sql);
            cs.registerOutParameter (1, java.sql.Types.INTEGER);
            // EPREF.setprefor_type(p_evid, p_orid, p_otype, p_evtpref, p_bump, p_commit, p_check_priority)
            cs.setLong(2, this.id.longValue());
            cs.setLong(3, sol.orid.longValue());
            String stype = ( sol.type.isBlankOrNull() ) ? Solution.UNKNOWN_TYPE : sol.type.toString();   // -aww added 2014/04/30 
            cs.setString(4, stype);
            cs.setInt(5,(sol.orid.longValue() == this.prefor.longValue()) ? 1 : 0);  // true sets event preferred event.prefor
            cs.setInt(6, 0);
            cs.setInt(7, 0);
            cs.setInt(8, checkPriority);
            cs.execute();
            long retVal = cs.getInt(1); // returns 0 value if not highest priority of type in EventPrefor table 
            if (retVal == 0) {
              statusMessage.append("\nWrite of prefor id: " +StringSQL.valueOf(sol.orid) + " Check for a OrgPrefPriority rules failure.");
            }
            bumpSrcVersion = ( retVal > 1 ); // aww 2014/05/15
            status = ( retVal >= 0 );

        } catch (SQLException ex) {
            System.out.println("SQL=\n"+sql);
            ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, cs, debug);
          try {
            if (cs != null) cs.close();
          }
          catch(SQLException ex) {
            System.err.println("SolutionTN.writePrefor unable to close statement.");
          }
        }

        if (status) {
          statusMessage.append("\nWrite of eventprefor orid: " +StringSQL.valueOf(sol.orid) + " type: " + sol.type + " succeeded.");
        }
        else {
          statusMessage.append("\nWrite of eventprefor orid: " +StringSQL.valueOf(sol.orid) + " type: " + sol.type + " failed!.");
        }
        return status;
    }

    private void nullOriginForeignKeys() {
        ////// make sure foreign keys are null, else constraint error
        //originRow.setValue(Origin.EVID,  null); // constraint not null
        originRow.setValue(Origin.PREFMAG, null); // reset later
        originRow.setValue(Origin.PREFMEC, null); // no reset
        originRow.setValue(Origin.COMMID,  null); // no reset
    }

    // for now HYP2000 column values:
    private boolean dbaseAssociateWithOriginError(boolean updateExisting) {
        if (! commitOriginError) return true; // flag to bypass saving origin_error table data
        // Check that 'orid' of event has been set
        if ( getOridValue() == 0l ) {
          if (debug) System.out.println("DEBUG Error Solution.dbaseAssociateWithOriginError has 0l orid");
          return false;
        }
        if (!usedReadings.isValid() || usedReadings.intValue() == 0) return true;  // assume no errors known

        //return DataTableRowUtil.rowToDb(DataSource.getConnection(), toOriginError(), updateExisting);
        boolean status = false;
        PreparedStatement psOrgErr = null;
        final String sql  =
            "INSERT INTO ORIGIN_ERROR (ORID,MAGSMALL,AZIINTER,DIPINTER,MAGINTER,AZILARGE,DIPLARGE,MAGLARGE) VALUES (?,?,?,?,?,?,?,?)";
        try {
            Connection conn = DataSource.getConnection();
            psOrgErr = conn.prepareStatement(sql);
            psOrgErr.setLong(1, orid.longValue());

            if (errSmall.isValid()) psOrgErr.setDouble(2, errSmall.doubleValue());
            else psOrgErr.setNull(2, java.sql.Types.DOUBLE);

            if (aziInter.isValid()) psOrgErr.setDouble(3, aziInter.doubleValue());
            else psOrgErr.setNull(3, java.sql.Types.DOUBLE);

            if (dipInter.isValid()) psOrgErr.setDouble(4, dipInter.doubleValue());
            else psOrgErr.setNull(4, java.sql.Types.DOUBLE);

            if (errInter.isValid()) psOrgErr.setDouble(5, errInter.doubleValue());
            else psOrgErr.setNull(5, java.sql.Types.DOUBLE);

            if (aziLarge.isValid()) psOrgErr.setDouble(6, aziLarge.doubleValue());
            else psOrgErr.setNull(6, java.sql.Types.DOUBLE);

            if (dipLarge.isValid()) psOrgErr.setDouble(7, dipLarge.doubleValue());
            else psOrgErr.setNull(7, java.sql.Types.DOUBLE);

            if (errLarge.isValid()) psOrgErr.setDouble(8, errLarge.doubleValue());
            else psOrgErr.setNull(8, java.sql.Types.DOUBLE);

            status = (psOrgErr.executeUpdate() > 0);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, psOrgErr, debug);
            Utils.closeQuietly(psOrgErr);
        }
        return status;
    }
/*
ORID
//SXX -- currently null for HYP2000
//SYY
//SZZ
//STT
//SXY
//SXZ
//SYZ
//STX
//STY
//STZ
//AZISMALL -- not used
//DIPSMALL
MAGSMALL
AZIINTER
DIPINTER
MAGINTER
AZILARGE
DIPLARGE
MAGLARGE
LDDATE
*/
/*
    private OriginError toOriginError() {
        OriginError oeRow = new OriginError(); // key is orid 
        oeRow.setValue(OriginError.ORID, orid); // sets oeRow orid.isUpdate() true
        if (errSmall.isValid()) oeRow.setValue(OriginError.MAGSMALL, errSmall);
        if (aziInter.isValid()) oeRow.setValue(OriginError.AZIINTER, aziInter);
        if (dipInter.isValid()) oeRow.setValue(OriginError.DIPINTER, dipInter);
        if (errInter.isValid()) oeRow.setValue(OriginError.MAGINTER, errInter);
        if (aziLarge.isValid()) oeRow.setValue(OriginError.AZILARGE, aziLarge);
        if (dipLarge.isValid()) oeRow.setValue(OriginError.DIPLARGE, dipLarge);
        if (errLarge.isValid()) oeRow.setValue(OriginError.MAGLARGE, errLarge);
        if (debug) System.out.println("DEBUG OriginError Row: "+oeRow.toString());
        oeRow.setUpdate(true); // set flag to enable processing
        return oeRow;
    }

    private void parseOriginError(OriginError oe) {
        errSmall.setValue((DataDouble) oe.getDataObject(OriginError.MAGSMALL));
        aziInter.setValue((DataDouble) oe.getDataObject(OriginError.AZIINTER));
        dipInter.setValue((DataDouble) oe.getDataObject(OriginError.DIPINTER));
        errInter.setValue((DataDouble) oe.getDataObject(OriginError.MAGINTER));
        aziLarge.setValue((DataDouble) oe.getDataObject(OriginError.AZILARGE));
        dipLarge.setValue((DataDouble) oe.getDataObject(OriginError.DIPLARGE));
        errLarge.setValue((DataDouble) oe.getDataObject(OriginError.MAGLARGE));
    }
*/
    private boolean parseOriginError(ResultSetDb rsdb, int offset) {
        int icol = offset;
        boolean status = true;
        try {
            ResultSet rs = rsdb.getResultSet();
            double dd = rs.getDouble(icol++);
            if (!rs.wasNull()) errSmall.setValue(dd);
            dd = rs.getDouble(icol++);
            if (!rs.wasNull()) aziInter.setValue(dd);
            dd = rs.getDouble(icol++);
            if (!rs.wasNull()) dipInter.setValue(dd);
            dd = rs.getDouble(icol++);
            if (!rs.wasNull()) errInter.setValue(dd);
            dd = rs.getDouble(icol++);
            if (!rs.wasNull()) aziLarge.setValue(dd);
            dd = rs.getDouble(icol++);
            if (!rs.wasNull()) dipLarge.setValue(dd);
            dd = rs.getDouble(icol++);
            if (!rs.wasNull()) errLarge.setValue(dd);
            /*
            errSmall.setValue(rsdb.getDataDouble(icol++));
            aziInter.setValue(rsdb.getDataDouble(icol++));
            dipInter.setValue(rsdb.getDataDouble(icol++));
            errInter.setValue(rsdb.getDataDouble(icol++));
            aziLarge.setValue(rsdb.getDataDouble(icol++));
            dipLarge.setValue(rsdb.getDataDouble(icol++));
            errLarge.setValue(rsdb.getDataDouble(icol++));
            */
        }
        catch (SQLException ex) {
            status = false;
            ex.printStackTrace();
        }
        return status;
    }

    /** Convenience method to save event, origin, waveform and reading data
     * back to database before doing further processing that depends
     * on these data (e.g. magnitude calculations).<br>
     * Does not commit waveform associations or associated Magnitudes.
     * @return  <i>false </i> data transaction error.
     */
    public boolean commitReadings() throws JasiCommitException {
        return doCommit(COMMIT_READINGS);
    }

    private boolean commitOriginAssocLists(StringBuffer statusMessage) throws JasiCommitException {

        if (debug) System.out.println("commitOriginPhaseList");
        boolean status = commitOriginPhaseList(statusMessage);

        // Instead handle the AmpList, CodaList origin association via the associated Magnitude commits -aww 2010/06/30
        /*
        //bm.printTimeStamp("commitOriginPhaseList");
        if (debug) System.out.println("commitOriginAmpList");
        status &= commitOriginAmpList(statusMessage);
        //bm.printTimeStamp("commitOriginAmpList");
        if (debug) System.out.println("commitOriginCodaList");
        status &= commitOriginCodaList(statusMessage); // do we need this analog? aww
        //bm.printTimeStamp("commitOriginCodaList");
        */

        return status;
    }

    private boolean commitMagnitudes(StringBuffer statusMessage) throws JasiCommitException {
        boolean status = commitPreferredMagnitude(statusMessage); // exception upon commit failure (does dbCommit)
        //bm.printTimeStamp("commitPreferredMagnitude");
        if (!status) return status; // don't do alternates
        status &= commitAlternateMagnitudes(statusMessage);    // does a no-op if commitAlternateMagnitudes == false
        //bm.printTimeStamp("commitAlternateMagnitudes");
                                                          // on per mag basis either does dbCommit, rollback exit on error)
        //Now that all are saved, let the internal dbase logic decide if preferred needs changing, may do dbCommit!
        if (status && autoSetPrefMag) {
            status = dbaseAutoSetPrefMag();
            if (!status) {
                String str = "ERROR! Set EVENT.PREFMAG by priority rules FAILED for event " +
                   getId().longValue() + " prefmag: " + magnitude.magid.longValue();
                statusMessage.append("\n"+str);
                System.err.println(str + "\nCheck the MagPrefPriority table priority criteria for magnitude:\n"+
                        magnitude.toNeatString());
            }
        }
        return status;
    }
    private boolean commitPreferredMagnitude(StringBuffer statusMessage) throws JasiCommitException {
        if (debug) System.out.println("commitPreferredMagnitude");
        // MAGNITUDE:  It may have changed even if Solution didn't.
        // Magnitude.commit() will do more checks and do the right thing
        if (magnitude == null) return true; // ? undefined preferred magnitude, punt

        boolean status = false;
        try {
          if (magnitude.isDeleted()) {
            statusMessage.append("\nCommit ERROR! : Event preferred magnitude is flagged DELETED, not allowed!");
          }
          else {
            // check for changed value b4 commit, assumes new insert only if changed -aww 2010/09/01
            boolean doPref = ((MagnitudeTN)magnitude).hasChangedValue()
                || (lastOrgPrefmag != magnitude.magid.longValue() || lastEvtPrefmag != magnitude.magid.longValue());
            status = magnitude.commit(); // deleted=> no-op so must not be deleted!
            if (debug)System.out.println("DEBUG SolutionTN commitPreferredMagnitude b4 writePrefMag doPref: " + doPref + " commit status: " + status); 
            if (status) {
              // What db deadlock/corruption causes below method call to hang process?
              //status &= dbaseSetPrefMag(); // if null magid, returns true, is that ok? - TEST REMOVAL 2010/08/25 since commit insert of a magnitude does this call 
              // the "preferred", update any existing row, else insert new row
              if (status && doPref) { // update eventprefmag table only if changed b4 commit -aww 2010/09/01
                // valid event.prefmag or a NULL magid - aww 02/09/2005
                // NULL magid no-op returns true, deleted mag returns false
                // false return causes rollback
                status &= writePrefMag(magnitude, statusMessage); // NOTE: EPREF.setprefmag_magtype sets event.prefmag of same subtype if magids differ
              }
            }
          }
        } catch (Exception ex)  {
          status = false;
          System.err.println(ex);
          ex.printStackTrace();
        }
        if (status) { // Do intermediate commit of mag info
          statusMessage.append("\nWrite of preferred magnitude succeeded.");
          status = doDbCommit();
          if (status) {
             magnitude.setTimeStamp(new java.util.Date());  // approximate, would need to update with SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) after commit() 
             statusMessage.append("\nCommit of preferred magnitude succeeded.");
          }
          else {
             statusMessage.append("\nCommit of preferred magnitude failed for event: " + id.longValue());
          }
        } else {
          statusMessage.append("\nWrite of preferred magnitude failed for event: " + id.longValue() +
          " lastPrefor: " + lastPreforId + " lastEvtPrefmag: " +  lastEvtPrefmag + " lastOrgPrefmag: " + lastOrgPrefmag);
        }
        // send error resolution up the stack
        if (!status) throw new JasiCommitException(statusMessage.toString()); // added 07/30/2004 aww
        return status;
    }

    private boolean writePrefMag(Magnitude mag, StringBuffer statusMessage) {


        DataLong magid =  ((MagnitudeTN)mag).getMagid(); // must be not NULL
        if (! magid.isValidNumber()) { // could be error, so warn and do nothing
           statusMessage.append("\nWARNING writePrefMag input mag.magid is NOT valid (NULL), no database write ");
           return true; // a no-op - aww 02/09/2005
        }
        
        if (mag.isDeleted()) { // a prefmag deletion could be error, so warn 
           statusMessage.append("\nWARNING writePrefMag input is flagged DELETED, no EVENTPREFMAG update for magid: ");
           statusMessage.append(magid.longValue());
           statusMessage.append(" magtype: ");
           statusMessage.append(mag.subScript);
           return true; // note a deleted mag.commit() may be no-op Jiggle user override 12/04/2006
        }

        // true arg => update any existing row, else insert new row
        //boolean status = DataTableRowUtil.rowToDb(DataSource.getConnection(), toOriginPrefMagRow(mag), true );
        //boolean status = DataTableRowUtil.rowToDb(DataSource.getConnection(), toEventPrefMagRow(mag), true ); //removed 2008/02/29
        //
        //EPREF HERE INSTEAD OF ABOVE ON 2008/02/29
        //NOTE FUNCTION CALL CHECKS EXISTING DB VALUES FOR CHANGE, IF NO CHANGE, A NOOP, NO UPDATE
        int checkPriority = (autoSetPrefMag) ? 1 : 0; // -aww 2008/05/16
        /*
        StringBuffer sb = new StringBuffer(80); 
        if (mag == this.magnitude) {
          sb.append("{ ?=call EPREF.setprefmag_magtype(");
          sb.append(id.longValue());
          sb.append(",");
          sb.append(magid.longValue());
          sb.append(",'");
          sb.append(mag.getTypeSubString());
          sb.append("',1,0,0"); // set as event preferred but don't bump version or commit -aww 2008/04/15
          sb.append(",").append(checkPriority); // should be "0", don't check -aww 2008/05/16
          sb.append(") }");
        }
        else {
          sb.append("{ ?=call EPREF.setprefmag_magtype(");
          sb.append(id.longValue());
          sb.append(",");
          sb.append(magid.longValue());
          sb.append(",'");
          sb.append(mag.getTypeSubString());
          sb.append("',0,0,0"); // don't set event preferred, bump version or commit -aww 2008/04/15
          sb.append(",").append(checkPriority); // should be "0", don't check -aww 2008/05/16
          sb.append(") }");
        }
        */
        final String sql = "{ ?=call EPREF.setprefmag_magtype(?,?,?,?,?,?,?) }";
        CallableStatement cs = null;
        boolean status = false;
        try {
            Connection conn = DataSource.getConnection();
            cs = conn.prepareCall(sql);
            cs.registerOutParameter (1, java.sql.Types.INTEGER);
            cs.setLong(2, id.longValue());
            cs.setLong(3, magid.longValue());
            cs.setString(4, mag.getTypeSubString());
            cs.setInt(5,(mag == this.magnitude) ? 1 : 0);  // set preferred if so
            cs.setInt(6, 0);
            cs.setInt(7, 0);
            cs.setInt(8, checkPriority);
            cs.execute();
            long retVal = cs.getInt(1); // returns 0 value if not highest priority of type in EventPrefMag table
            if (retVal == 0) {
              statusMessage.append("\nWrite of prefmag id: " +StringSQL.valueOf(magid) + " Check for a MagPrefPriority rules failure."); // -aww 2008/05/16
            }
            bumpSrcVersion = ( retVal > 1 ); // aww 2014/05/15
            status = ( retVal >= 0 ); // use when new EPREF implemented above - 2008/05/16 aww 

        } catch (SQLException ex) {
            System.out.println("SQL=\n"+sql);
            ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, cs, debug);
          try {
            if (cs != null) cs.close();
          }
          catch(SQLException ex) {
            System.err.println("SolutionTN.writePrefMag unable to close statement.");
          }
        }

        if (status) {
          statusMessage.append("\nWrite of prefmag id: " +StringSQL.valueOf(magid) + " succeeded.");
        }
        else {
          statusMessage.append("\nWrite of prefmag id: " +StringSQL.valueOf(magid) + " failed!.");
        }
        return status;
    }
    /*
     //NOTE: Bulk PrefMag processing in method below not USED - instead all
     //PrefMag updates are done after sol.magnitude and sol.altMagnitudeList commit
     //because the preferred magid will be a "new" number after mag.commit()
    private boolean commitPrefMags(StringBuffer statusMessage ) throws JasiCommitException {
        if (debug) System.out.println("commitPrefMags");
        boolean success = true;
        boolean status = false;
        int count = 0;
        MagnitudeTN mag = null;
        Iterator iter = prefMagMap.values().iterator();
        while (iter.hasNext()) {
          mag = (MagnitudeTN) iter.next();
          status = writePrefMag(mag, statusMessage); // no-op when virtually "deleted" ?
          if (status) count++;
          success &= status;
        }
        if (! success) {
            statusMessage.append("\nWrite of all Prefmags failed.");
            if (debug) System.out.println("Write of all Prefmags failed.");
            //rollback(); // don't throw away any successful writes?
        }
        if (count > 0) { // dbase commit any successful update of prefmag info?
          success = doDbCommit();
          if (success) {
              statusMessage.append("\nCommit of Prefmags succeeded.");
          }
          else {
              statusMessage.append("\nCommit of Prefmags failed.");
              //throw new JasiCommitException(statusMessage.toString()); // added 07/30/2004 aww
          }
        }
        return success;
    }
    */

    /*
    private EventPrefMag toEventPrefMagRow(Magnitude mag) {
        MagnitudeTN magTN = (MagnitudeTN) mag;
        EventPrefMag dbasePrefMag = new EventPrefMag();
        if (id.isValidNumber()) dbasePrefMag.setValue(EventPrefMag.EVID, id);
        if (magTN.subScript.isValid()) dbasePrefMag.setValue(EventPrefMag.MAGTYPE, magTN.subScript);
        if (magTN.magid.isValidNumber()) dbasePrefMag.setValue(EventPrefMag.MAGID, magTN.magid);
        //dbasePrefMag.setValue(EventPrefMag.LDDATE, new DataDate()); // updates on commit 06/22/2006 <- disabled 02/22/2007 -aww
        dbasePrefMag.setUpdate(true); // set flag to enable processing
        //if (debug) System.out.println("toEventPrefMagRow: " + dbasePrefMag.toString());
        if (debug) System.out.println("toEventPrefMagRow");
        return dbasePrefMag;
    }
    */

    /*
    private PrefMag toOriginPrefMagRow(Magnitude mag) {
        MagnitudeTN magTN = (MagnitudeTN) mag;
        PrefMag dbasePrefMag = new PrefMag();
        //Force orid,evid assoc values to be those of this event not those stored in the input mag
        if (prefor.isValidNumber()) dbasePrefMag.setValue(PrefMag.ORID, prefor);
        if (magTN.subScript.isValid()) dbasePrefMag.setValue(PrefMag.MAGTYPE, magTN.subScript);
        if (magTN.magid.isValidNumber()) dbasePrefMag.setValue(PrefMag.MAGID, magTN.magid);
        if (id.isValidNumber()) dbasePrefMag.setValue(PrefMag.EVID, id);
        dbasePrefMag.setUpdate(true); // set flag to enable processing
        if (debug) System.out.println("toOriginPrefMagRow: " + dbasePrefMag.toString());
        return dbasePrefMag;
    }
    */

    // Returns true if this is a clone event.  - moved method to super class -aww 05/25/2007
    //public boolean isClone() { // choice of name is easy to confuse with the Java clone() concept -aww
    //   return (id.longValue() != parentId.longValue()) ;
    //}

    // WAVEFORMS <AssocWaE> write waveform associations
    /**
     * Copy all the AssocWaE rows for the oldEvid to this evid. This is much
     * faster then actually writing them one-by-one but only works if this is a
     * clone and you want ALL waveforms associated. Returns true on success,
     * false on failure or noop. Uses Oracle store procedure.
     */
    private boolean commitEventAssocWaveforms(StringBuffer statusMessage) throws JasiCommitException {
        if (debug) System.out.println("commitEventAssocWaveforms");
        // No-op if not a clone or the wf associations were already copied
        if (! isClone()) {
          statusMessage.append("\nEvent not a clone thus NO new event waveform associations to commit.");
          return true; //  used to be false; made return status true, for no-op, aww
        }
        if (wfsCloned) {
          statusMessage.append("\nEvent is a clone its waveform associations ALREADY committed.");
          return true; //  used to be false; made return status true, for no-op, aww
        }
        else if (! waveformCommitOk) {
          statusMessage.append("\nEvent waveform association commit is disabled by input property.");
          return true; //  used to be false; made return status true, for no-op, aww
        }

        /* Shortcut return when parent waveform count is 0 (via db connection), NOT NEEDED JUST ADDS OVERHEAD
        int count = countWaveforms(parentId.longValue());   // added 2009/11/29, fixed bug query using clone's id, thus returned 0 - aww 2009/12/17
        if (count == 0) {
            statusMessage.append("\nParent event has no waveform rows to (clone) in commit.");
            return true;
        }
        */

        final String sql = "{ ?=call EPREF.CloneAssocWaE(?,?,?) }";
        long retVal = -1l;
        CallableStatement cs = null;
        try {
            Connection conn = DataSource.getConnection();
            if (conn == null) {
              throw new JasiCommitException("No connection to data source.");
            }
            cs = conn.prepareCall(sql);
            cs.registerOutParameter (1, java.sql.Types.BIGINT);
            cs.setLong(2, parentId.longValue());
            cs.setLong(3, id.longValue());
            cs.setInt(4, 0); // don't commit
            cs.execute();
            retVal = cs.getLong(1); // version
        } catch (SQLException ex) {
            System.out.println("SQL=\n"+sql);
            ex.printStackTrace();
        }
        finally {
          try {
            JasiDatabasePropertyList.debugSQL(sql, cs, debug);
            if (cs != null) {
              cs.close();
            }
          }
          catch(SQLException ex) {
            System.err.println("SolutionTN.commitEventAssocWaveforms unable to close statement.");
          }
        }
        boolean status = (retVal > 0);

        if (status) {
          statusMessage.append("\nWaveform/Event association write succeeded.");
          status = doDbCommit();
          if (status) {
            wfsCloned = true; // do we have dbase rows for sure ?
            statusMessage.append("\nWaveform/Event association commit succeeded.");
          }
          else {
            statusMessage.append("\nWaveform/Event association commit failed for (clone) event: " + id.longValue());
            throw new JasiCommitException(statusMessage.toString());
          }
        }
        else {
          statusMessage.append("\nWaveform/Event association write failed for (clone) event: " + id.longValue());
          // rollback on failure so we throw no exception here to be able to continue processing other data lists
          status = true; // allow commit to succeed
        }

        //bm.printTimeStamp("commitEventAssocWaveforms");
        return status;
    }

    /** Do everything necessary to delete event from dbase. */
    protected boolean doDeleteCommit(StringBuffer statusMessage) throws JasiCommitException {
        long evid = getId().longValue();
        boolean status = false;
        if (debug) System.out.println("Deleting "+evid );
        // No reason to continue with mag, etc. if its a delete
        // Note: commit is done in dbaseDelete()
        try {
            status = dbaseDelete();
            if (status) statusMessage.append("\nEvent/Origin delete succeeded for event: "+evid);
        } catch (Exception ex)  {   // SQL or JasiCommitException
            status = false;
            statusMessage.append("\nEvent/Origin delete failed for event: " + evid);
            System.err.println(ex);
            ex.printStackTrace();
            if (! rollback()) statusMessage.append("\nRollback failed!");
            throw new JasiCommitException(statusMessage.toString());
        }

        //setNeedsCommit(!status);  // 04/24/2007 -aww
        if (status) {  // 04/24/2007 -aww
            // reset commit states in case any changed before delete, hasChanged() checks premag commit states
            resetStatusFlags(); 
            setUpdate(false); // validFlag may have been set (as well as other fields changed by e.g. by mung). 
        }
        //System.out.println("doDeleteCommit return status : " + status + getSummaryStateString(this,this.magnitude));
        return status;
    }

    /** Commit any additions, changes or deletions to the data source plus
    * take any site-specific actions for a final solution.
    * */
    public boolean finalCommit() throws JasiCommitException {
        boolean status = true;
        //boolean inDebug = debug;  // aww temporary debug kludge
        //debug = true;
        commitStatusMessage = null;
       
        if (debug) System.out.println("finalCommit getNeedsCommit() : " + getNeedsCommit());   
        boolean originNeededCommit = originHasChanged(); // if it's changed, then the attribution will be updated by dbaseUpdateOrigin
        if (getNeedsCommit()) {
          if (debug) System.out.println("finalCommit commit() starting...");
          status = commit();
          if (debug) System.out.println("finalCommit commit() return status: " + status);
        }
        // need to initialize status message if commit does not - aww 12/01/2006 
        if (commitStatusMessage == null)  commitStatusMessage = new StringBuffer(1024);

        StringBuffer statusMessage = new StringBuffer(256);
        // invoke finalize here, no matter if dbase committed or not - aww
        if (status) {
           if (debug) System.out.println("finalCommit dbaseFinalize() starting...");
           status  = dbaseFinalize();
           if (debug) System.out.println("finalCommit dbaseFinalize() return:" + status);

           // attribution block below added for case of nothing else in origin changed, but finalize did rflag posting -aww 2010/10/18
           // assume if there's is a "who" already we don't want to change it here?
           if (dbAttributionEnabled && status && !originNeededCommit && getWho().isNull()) {
             if (debug) System.out.println("dbaseUpdateOrigin setting ORIGIN attribution");
             // revised to return table value that was set
             String attrib = ((JasiDbReader) getDataReader()).setAttribution(DataSource.getConnection(),
                                                        prefor.longValue(),
                                                        "ORIGIN",
                                                        EnvironmentInfo.getUsername()
                                                        ); 
             if (attrib != null) setWho(attrib);
           }
           //
                   

        }
        //bm.printTimeStamp("dbaseFinalize");
        if (status) { // success, so set current state as "finalized"
          statusMessage.append("\nSolution finalCommit dbaseFinalize() TRUE  for event: " + id.longValue());
          commitStatusMessage.append(statusMessage);
          setProcessingState(JasiProcessingConstants.STATE_FINAL_TAG);
          processingState.setUpdate(false);   // flip internal state w/o marking as changed (needs save)
        }
        else {
          statusMessage.append("\nSolution finalCommit dbaseFinalize() FALSE for event: " + id.longValue());
          commitStatusMessage.append(statusMessage);
        }

        if (debug) System.out.println(statusMessage.toString());
        //debug = inDebug; // aww temporary debug kludge
        return status;
    }

    /**
     * Delete event from the database. This calls a stored procedure, so the
     * actual meaning of "delete" is defined there. This is a virtual delete that sets
     * selectflag = 0.  The change is commited in this method.
     */
    protected boolean dbaseDelete() throws JasiCommitException {
        if (! fromDbase) return true; // not from dbase nothing to delete
        // Call stored procedure that knows rules for deleting an event
        // the procedure does a commit so you don't have to.

        final String sql = "{ ?=call EPREF.Delete_Event(?) }";
        long retVal = -1l;
        CallableStatement cs = null;
        try {
            Connection conn = DataSource.getConnection();
            if (conn == null) {
              throw new JasiCommitException("No connection to data source.");
            }
            cs = conn.prepareCall(sql);
            cs.registerOutParameter (1, java.sql.Types.BIGINT);
            cs.setLong(2, id.longValue());
            cs.execute();
            retVal = cs.getLong(1); // status

            if (retVal > 0) {
                conn.commit();
            }
        } catch (SQLException ex) {
            System.out.println("SQL=\n"+sql);
            ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, cs, debug);
          try {
            if (cs != null) cs.close();
          }
          catch(SQLException ex) {
            System.err.println("SolutionTN.dbaseDelete unable to close statement.");
          }
        }
        return (retVal > 0);
    }

    public boolean rollbackPrefs() {
        try {
            return dbaseRollbackPrefs();
        }
        catch (JasiCommitException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    private boolean dbaseRollbackPrefs() throws JasiCommitException {
        if (!fromDbase) {
            //if (debug) System.out.println("DEBUG dbaseRollbackPrefs: Nothing to rollback");
            return true;
        }

        // Check for valid previous pref ids, no-op if all are undefined
        if ( lastPreforId == NullValueDb.NULL_LONG && lastOrgPrefmag == NullValueDb.NULL_LONG) return true;

        Connection conn = DataSource.getConnection();
        if (conn == null) throw new JasiCommitException("No connection to data source.");

        // Call stored procedure that sets the event prefor and prefmag to their previous values 
        // The setpref procedure does a commit so you don't have to here.
        long retVal = -1l;
        CallableStatement sm = null;
        // Jiggle does not change lastMecId 
        final String sql = "{ ?=call EPREF.setpref(?, ?, ?) }";
        if (debug || JasiDatabasePropertyList.debugSQL) System.out.println("SolutionTN dbaseRollbackPrefs:\n"+sql);
        try {
            sm = conn.prepareCall(sql);
            sm.registerOutParameter(1, java.sql.Types.INTEGER);
            sm.setLong(2, id.longValue());
            sm.setLong(3, (lastPreforId == NullValueDb.NULL_LONG) ? -1l : lastPreforId);
            sm.setLong(4, (lastOrgPrefmag == NullValueDb.NULL_LONG) ? -1l : lastOrgPrefmag);
            sm.executeUpdate();
            retVal = sm.getInt(1);
            
        }
        catch (SQLException ex) {
            System.err.println("SolutionTN doCallableSQL:\n"+sql);
            System.err.println(ex);
            ex.printStackTrace();
            try { // rollback on error
                conn.rollback();
            } catch (SQLException ex2)  {
                System.err.println("Rollback failed:\n"+ex2);
                ex2.printStackTrace();
                //throw new JasiCommitException("SolutionTN doCallableSQL: Rollback failed!"); // in lieu of return
            }
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, sm, debug);
            Utils.closeQuietly(sm);
        }
        return (retVal >= 0l);
    }

    /**
     * Finalize event in the database. This calls a stored procedure, so the
     * actual meaning of "finalize" is defined there.
     */
/* See most current PL/SQL implementation in server, code should be in function to:
  PROCEDURE Finalize_Event (p_evid IN Event.evid%TYPE) AS
  BEGIN
-- Set 'rflag' = "F"
     SetRflag (p_evid, 'F');
-- POST event to 'FINALIZE' for alarm processing, etc.
     PCS.putState ('TPP', 'TPP', p_evid, 'FINALIZE', 100);
-- DEPRECATED FUNCTION: DO NOT CLONE EARLIEST
-- Copy AssocAmO associations from original event
--     CloneAssocAmO (p_evid);
   END;
*/
    protected boolean dbaseFinalize() throws JasiCommitException {
        // Call stored procedure that knows rules for finalizing an event
        if (debug) System.out.println("DEBUG SolutionTN calling EPREF.Finalize_Event for evid: " + id.longValue());

        final String sql = "{ ?=call EPREF.Finalize_Event(?) }";
        long retVal = -1l;
        CallableStatement cs = null;
        Connection conn = null;
        try {
            conn = DataSource.getConnection();
            if (conn == null) {
              throw new JasiCommitException("No connection to data source.");
            }
            if (debug) System.out.printf("DEBUG autocommit on = " + conn.getAutoCommit());
            cs = conn.prepareCall(sql);
            cs.registerOutParameter (1, java.sql.Types.BIGINT);
            cs.setLong(2, id.longValue());
            cs.execute();
            retVal = cs.getLong(1); // status
        } catch (SQLException ex) {
            System.out.println("SQL=\n"+sql);
            ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, cs, debug);
          try {
              // PostgreSQL stored function cannot commit, commit here
            if (conn != null && (conn.getMetaData().getDatabaseProductName().toString().equalsIgnoreCase("postgresql"))) conn.commit();
            if (cs != null) cs.close();
          }
          catch(SQLException ex) {
            System.err.println("SolutionTN.dbaseFinalize unable to close statement.");
          }
        }
        return (retVal > 0);
    }

    /** Set the prefor of this solution to the input id, and update the database. */
    private boolean dbaseSetPrefOr(DataObject prefOrid) {
        if (! prefOrid.isValidNumber()) return false;
        this.prefor.setValue(prefOrid.longValue());
        return dbaseSetPrefOr();
    }

    private boolean dbaseSetPrefOr() {
        return dbaseSetPreforForEvent() && dbaseSetEvidForOrigin();
    }

    private boolean dbaseSetPreforForEvent() {

        DataObject obj = this.prefor; //  NO longer aliased, needed to use the updated instance value not eventRow.getDataObject(Event.PREFOR);
        // bugfix: 2008/03/10 use lastPreforId in test below not prefor -aww
        if (debug) {
            System.out.println("DEBUG SolutionTN calling dbaseSetPreforForEvent for evid: " + id.longValue() + " lastPreforid: " + lastPreforId 
                    + " this.prefor: " + prefor);
        }
        if (lastPreforId != NullValueDb.NULL_LONG && obj != null && obj.equalsValue(Long.valueOf(lastPreforId))) return true; // no-op if same value orid -aww 2008/02/20
        // version bumped prefor differs from db value
        boolean status = false;
        eventRow.setUpdateValues(false); // aww 2009/05/13
        // Set foreign keys to point at preferred origin.
        eventRow.setValue(Event.PREFOR, prefor);
        eventRow.setProcessing(DataTableRowStates.UPDATE);
        /*
        // Need setUpdate(true) to use key value in where condition
        eventRow.getDataObject(Event.EVID).setUpdate(true);
        if (debug) System.out.println("dbaseSetPreforForEvent() eventRow.updateRow(conn)");
        status = (eventRow.updateRow(DataSource.getConnection()) > 0);
        if (status) lastPreforId = prefor.longValue(); // save last db set value, in case it's relocated  - aww 2008/04/15
        eventRow.getDataObject(Event.EVID).setUpdate(false);
        if (debug) System.out.println("dbaseSetPreforForEvent eventRow update return status:" + status);
        */

        // PreparedStatement update version with bind variables
        PreparedStatement ps = null;
        final String sql = "UPDATE EVENT SET PREFOR=? WHERE (EVID=?)";
        try {
            Connection conn = DataSource.getConnection();
            ps= conn.prepareStatement(sql);
            ps.setLong(1, prefor.longValue());
            ps.setLong(2, id.longValue());
            status = (ps.executeUpdate() > 0);
            if (status) bumpSrcVersion = true; // bump version after prefor change
        }
        catch (SQLException ex) {
            status = false;
            ex.printStackTrace();
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, ps, debug);
            Utils.closeQuietly(ps);
        }

        if (status) lastPreforId = prefor.longValue(); // save last db set value, in case it's relocated  - aww 2008/04/15

        return status;
    }

    private boolean dbaseSetEvidForOrigin() {
        boolean status = false;
        originRow.setUpdateValues(false); // aww 2009/05/13

        // Origin points back to the event now the origin to evid,
        DataObject obj = originRow.getDataObject(Origin.EVID);
        if (obj == null || ! obj.equalsValue(id)) { // no-op if same value evid -aww 2008/02/20
            originRow.setValue(Origin.EVID, id);
            status = true;
        }
        // Set processing state to current value: A, H, F
        if (commitResetsRflag) {
            if (processingState.isValid() && !EnvironmentInfo.isAutomatic() &&
                    !processingState.toString().equals(JasiProcessingConstants.STATE_FINAL_TAG )) {
                // states not "F" are forced "I" when not in automatic mode and commitResetsRflag -aww 2014/04/09
                processingState.setValue(JasiProcessingConstants.STATE_INTERMEDIATE_TAG);
            }
            obj = originRow.getDataObject(Origin.RFLAG);
            //System.out.println("DEBUG dbaseSetEvidForOrigin() current sol processingState: " + processingState + " current origin row rflag: " + obj);
            if (obj == null || ! obj.equalsValue(processingState)) { // no-op if same value processing state -aww 2008/02/20
                originRow.setValue(Origin.RFLAG, processingState); // 02/10/2005
                status = true;
            }
        }
        if (! status) return true; // no-op if same values -aww 2008/02/20


        // Need setUpdate(true) to use key value in where condition
        originRow.setProcessing(DataTableRowStates.UPDATE);
        /*
        originRow.getDataObject(Origin.ORID).setUpdate(true); // aww 03/03
        if (debug) System.out.println("dbaseSetEvidForOrigin() originRow.updateRow(conn)");
        status = (originRow.updateRow(DataSource.getConnection()) > 0);
        originRow.getDataObject(Origin.ORID).setUpdate(false); // aww 03/03
        if (status) prefor.setUpdate(false);
        if (debug) System.out.println("dbaseSetEvidForOrigin originRow update return status:" + status);
        */

        // PreparedStatement update version with bind variables
        PreparedStatement ps = null;
        final String sql = "UPDATE ORIGIN SET EVID=?, RFLAG=? WHERE (ORID=?)";
        try {
            Connection conn = DataSource.getConnection();
            ps= conn.prepareStatement(sql);
            ps.setLong(1, id.longValue());
            ps.setString(2, processingState.toString()); // set the updated rflag state too, bug fix - aww 2010/08/23
            ps.setLong(3, prefor.longValue());
            status = (ps.executeUpdate() > 0);
            if (status) bumpSrcVersion = true; // bump version after rflag change
        }
        catch (SQLException ex) {
            status = false;
            ex.printStackTrace();
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, ps, debug);
            Utils.closeQuietly(ps);
        }
        return status;
    }

    /**
     * Use the logic in the dbase to set the preferred magnitude from among all those
     * in the dbase associated with input <i>evid</i>.
    * Note that this method causes a datasource commit.
    * */
    protected boolean dbaseAutoSetPrefMag() { // throws JasiCommitException { // reworked  -aww 2008/04/15

        Connection conn = DataSource.getConnection();
        if (conn == null) {
          //throw new JasiCommitException("SolutionTN.dbaseAutoSetPrefMag error: No connection to data source.");
          System.err.println("SolutionTN.dbaseAutoSetPrefMag error: No connection to data source.");
          return false;
        }

        final String sql = "{ ?=call MagPref.bestMagidOfEventPrefMag(?) }";
        CallableStatement cs = null;
        boolean status = false;
        try {
            cs = conn.prepareCall(sql);
            cs.registerOutParameter (1, java.sql.Types.BIGINT);
            cs.setLong(2, id.longValue());
            cs.execute();
            long id = Math.abs(cs.getLong(1)); // returns 0 value if evid not in EventPrefMag table 
            if (id > 0) {
                DataLong magId = new DataLong(id); // highest priority magid
                // try to find matching magid instance in magList
                MagnitudeTN mag = (MagnitudeTN) altMagList.getByIdentifier(magId);
                if (mag == null) { // not in list
                    mag = (MagnitudeTN) magnitude.getById(magId); // read new magnitude from db
                    if (mag != null) setPreferredMagnitude(mag); // set the new, event preferred
                }
                else if (mag != this.magnitude) { // in list but not event preferred
                    setPreferredMagFromIdInList(magId); // set event preferred to list instance
                }
                status = dbaseSetPrefMag(); // update db rows (no commit or version bump here) // -aww 2008/04/15
            }
        } catch (SQLException ex) {
            System.out.println("SQL=\n"+sql);
            System.err.println(ex);
            ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, cs, debug);
          try {
            if (cs != null) cs.close();
          }
          catch(SQLException ex) {
            System.err.println("SolutionTN.dbaseAutoSetPrefMag unable to close statement.");
          }
        }

        if (debug)
            System.out.println("DEBUG SolutionTN.dbaseAutoSetPrefMag return status: " + status);

        return status;
    }

    /** Set the prefmag of this solution to its Magnitude object.
    * Does not do a datasource.commit.
    * */
    protected boolean dbaseSetPrefMag() {
        return dbaseSetPrefMag(magnitude);
    }

    /** Set the prefmag of this solution to the given Magnitude.
    * Does not do a datasource.commit.
    * */
    protected boolean dbaseSetPrefMag(Magnitude prefMag) {
        DataObject prefMagid = ((MagnitudeTN)prefMag).getMagid();

        // What should it do with undefined magid (NULL) ?
        // if (! prefMagid.isValidNumber())  return false; // Propagate?  null is possible

        boolean status = true;
        DataObject obj = eventRow.getDataObject(Event.PREFMAG);
        // no-op if same as last magid and current magid -aww 2008/04/01 (used to be only prefMagid check) because
        // update calls toEventRow() which sets the Event.PREFMAG to the value of prefmag which typically == prefMagid
        if (obj == null || !obj.equalsValue(new DataLong(lastEvtPrefmag)) || !obj.equalsValue(prefMagid) ) { // added lastEvtPrefmag -aww 2008/04/02
          eventRow.setUpdateValues(false); // aww 2009/05/13
          // Assumes eventRow is already setup.
          // Set foreign keys to point at preferred mag.
          eventRow.setValue(Event.PREFMAG, prefMagid);

          eventRow.setProcessing(DataTableRowStates.UPDATE);
          /*
          // Need setUpdate(true) to use key value in where condition
          eventRow.getDataObject(Event.EVID).setUpdate(true);
          // What db state causes this call to hang the process, a deadlock with NETMAG? -aww
          status = (eventRow.updateRow(DataSource.getConnection()) > 0);
          eventRow.getDataObject(Event.EVID).setUpdate(false); // aww 03/03 ?
          if (debug) System.out.println("dbaseSetPrefMag eventRow update return status: "+ status);
          */
          // PreparedStatement update version with bind variables
          PreparedStatement ps = null;
          final String sql = "UPDATE EVENT SET PREFMAG=? WHERE (EVID=?)";
          try {
            Connection conn = DataSource.getConnection();
            ps= conn.prepareStatement(sql);
            ps.setLong(1, prefMagid.longValue());
            ps.setLong(2, id.longValue());
            status = (ps.executeUpdate() > 0);
            if (status) bumpSrcVersion = true; // bump version after prefmag change
          }
          catch (SQLException ex) {
            status = false;
            ex.printStackTrace();
          }
          finally {
            JasiDatabasePropertyList.debugSQL(sql, ps, debug);
            Utils.closeQuietly(ps);
          }
          if (!status) return false;
          lastEvtPrefmag  = prefMagid.longValue(); // save last db set value of event prefmag -aww 2008/04/02
        }

        obj = originRow.getDataObject(Origin.PREFMAG);
        // no-op if same as last magid and current magid -aww 2008/04/01 (used to be only prefMagid check) because
        // update calls toOriginRow() which sets the Origin.PREFMAG to the value of prefmag which typically == prefMagid
        if (obj == null || ! obj.equalsValue(new DataLong(lastOrgPrefmag)) || !obj.equalsValue(prefMagid) ) {
          originRow.setUpdateValues(false); // aww 2009/05/13
          /*
          // Need setUpdate(true) to use key value in where condition
          originRow.getDataObject(Origin.ORID).setUpdate(true); // aww 03/03
          originRow.setValue(Origin.PREFMAG, prefMagid);
          originRow.setProcessing(DataTableRowStates.UPDATE);
          status = (originRow.updateRow(DataSource.getConnection()) > 0);
          originRow.getDataObject(Origin.ORID).setUpdate(false); // aww 03/03
          */
          // PreparedStatement update version with bind variables
          PreparedStatement ps = null;
          final String sql = "UPDATE ORIGIN SET PREFMAG=? WHERE (ORID=?)";
          try {
            Connection conn = DataSource.getConnection();
            ps= conn.prepareStatement(sql);
            ps.setLong(1, prefMagid.longValue());
            ps.setLong(2, prefor.longValue());
            status = (ps.executeUpdate() > 0);
          }
          catch (SQLException ex) {
            status = false;
            ex.printStackTrace();
          }
          finally {
            JasiDatabasePropertyList.debugSQL(sql, ps, debug);
            Utils.closeQuietly(ps);
          }
          if (status) lastOrgPrefmag = prefMagid.longValue(); // save last db set value of origin prefmag
        }

        if (debug) System.out.println("dbaseSetPrefMag update return status: "+ status);

        return status;

    }

    /**
    * Commits the CodaList elements associated with a new Origin.
    * Only call this for once per new origin, after it's committed.
    * If a coda element is new, an Coda row will be written.
    * An AssocCoO row will also be written.
    * If the origin is unchanged, we don't write the AssocCoM rows
    * because some may be affiliated with stale secondary Magnitudes.
    * Only Magnitude commit() should write the AssocCoM rows.
    */
    protected boolean commitOriginCodaList(StringBuffer statusMessage) throws JasiCommitException {

        /*
        int count = codaList.size();
        if (count == 0)  return true; // nothing to commit, no-op

        boolean status = false;

        org.trinet.jasi.TN.CodaTN codaTN = null;

        for (int i = 0; i < count; i++) {

            codaTN = (org.trinet.jasi.TN.CodaTN) codaList.get(i);

            // Don't write deleted data association to db
            // Some in list may not be associated with Solution?
            if (codaTN.isDeleted() || ! codaTN.isAssignedTo(this)) continue;
            //
            // Can't use list commit() because it trys to write
            // AssocCoM rows and NetMag row may not be written yet
            //
            // See if new row is needed
            if ( codaTN.isFromDataSource() && ! codaTN.getNeedsCommit()) { // old row ok
              // if reading hasChanged() need also an insert logic here!!!!!!!!!!!
              // REALLY need an update not insert for non-solution dependent values
              // WARNING: if magnitude and solution readings are not list aliased,
              // and both are need insert due to changes we get 2 new readings, instead of 1
              // changed for mung 04/17/2007 -aww
              status = (commitOriginChanged) ? codaTN.dbaseAssociateWithOrigin(true) : true; // check for assoc existance
            } else { // new row needed
              status = codaTN.dbaseInsert();
              if (status) status = codaTN.dbaseAssociateWithOrigin(false); // new so don't check for assoc existance
              codaTN.setNeedsCommit(!status);
            }
        }
        */

        boolean status = codaList.commit();

        if (status) {
            status = doDbCommit(); // make db write permanent
            if (status) {
              statusMessage.append("\nCoda/Origin commit succeeded.");
            }
            else { // 11/12/03 aww
              statusMessage.append("\nCoda/Origin commit failed.");
              throw new JasiCommitException(statusMessage.toString());
            }
        }
        // throw away any writes -aww
        else if (! rollback()) {
          statusMessage.append("\nRollBack failed.");
          throw new JasiCommitException(statusMessage.toString());
        }

        return status; // false implies no data or failure
    }

    /**
    * Commits the amp list elements associtated with a new Origin.
    * Only call this for once per new origin, after it's committed.
    * If Amp is new (not from dbase) an Amp row will be written.
    * An AssocAmO row will also be written.
    * If the origin is unchanged, we don't write the AssocAmM rows
    * because some may be affiliated with stale secondary Magnitudes.
    * Only Magnitude commit() should write the AssocAmM rows.
    */
    protected boolean commitOriginAmpList(StringBuffer statusMessage) throws JasiCommitException {

        /*
        int count = ampList.size();
        if (count == 0)  return true; // nothing to commit, no-op

        boolean status = false;

        AmplitudeTN ampTN = null;
        for (int i = 0; i < count; i++) {

            ampTN = (AmplitudeTN) ampList.get(i);

            // Don't write deleted data association to db
            // Some amplitudes in list may not be associated with Solution?
            if (ampTN.isDeleted() || ! ampTN.isAssignedTo(this)) continue;

            // Can't use ampList.commit() because it trys to write
            // AssocAmM rows and NetMag row may not be written yet
            //
            // See if new row is needed
            if ( ampTN.isFromDataSource() && ! ampTN.getNeedsCommit()) { // old row ok
              // if reading hasChanged() need also an insert logic here!!!!!!!!!!!
              // REALLY need an update not insert for non-solution dependent values
              // WARNING: if magnitude and solution readings are not list aliased,
              // and both are need insert due to changes we get 2 new readings, instead of 1
              // changed for mung 04/17/2007 -aww
              status = (commitOriginChanged) ? ampTN.dbaseAssociateWithOrigin(true) : true; // check for assoc existance
            } else { // new row needed
              status = ampTN.dbaseInsert();
              if (status) status = ampTN.dbaseAssociateWithOrigin(false); // new so don't check for assoc existance
              ampTN.setNeedsCommit(!status);
            }
        }
        */

        boolean status = ampList.commit();
        if (status) {
            status = doDbCommit(); // make db write permanent
            if (status) {
              statusMessage.append("\nAmp/Origin commit succeeded.");
            }
            else { // 11/12/03 aww
              statusMessage.append("\nAmp/Origin commit failed for event: " + id.longValue());
              throw new JasiCommitException(statusMessage.toString());
            }
        }
        // throw away any writes -aww
        else if (! rollback()) {
          statusMessage.append("\nRollBack failed.");
          throw new JasiCommitException(statusMessage.toString());
        }

        return status; // false implies no data or failure
    }

    protected boolean commitOriginPhaseList(StringBuffer statusMessage) throws JasiCommitException {

        if (! newOrigin2Assoc) return true; // old origin no new associations needed -aww 2010/06/30

        /*
        int count = phaseList.size();
        if (count == 0)  return true; // nothing to commit, a no-op

        boolean status = true;

        // Batch update database arrivals in phaseList 
        PhaseTN phaseTN = null;
        for (int i = 0; i < count; i++) {

            phaseTN = (PhaseTN) phaseList.get(i);

            // Don't write deleted data association to db
            // Some phases in list may not be associated with Solution?
            if (phaseTN.isDeleted() || ! phaseTN.isAssignedTo(this)) continue;

            // See if new Arrival row is needed, or just an association with origin
            if ( phaseTN.isFromDataSource() && ! phaseTN.getNeedsCommit()) { // old row ok
              // if reading hasChanged() need also an insert logic here!!!!!!!!!!!
              // REALLY need an update not insert for non-solution dependent values
              // WARNING: if magnitude and solution readings are NOT list aliased, and both
              // need commit due to changes, we get 2 new rows, instead of 1, per reading
              //status = (commitOriginChanged) ? phaseTN.dbaseAssociateWithOrigin(true) : true; // check for assoc existance
              status = (commitOriginChanged) ? phaseTN.upsertAssoc(true, close) : true; // check for assoc existance
            } else { // new Arrival row is needed
              status = phaseTN.dbaseInsert();
              if (status) status = phaseTN.dbaseAssociateWithOrigin(false); // new so don't check for assoc existance
              phaseTN.setNeedsCommit(!status);
            }
        }
        */

        boolean status = phaseList.commit();

        if (status) { // save
            status = doDbCommit(); // make db write permanent
            if (status) {
              statusMessage.append("\nPhase/Origin commit succeeded.");
            }
            else { // 11/12/03 aww
              statusMessage.append("\nPhase/Origin commit failed for event: " + id.longValue());
              throw new JasiCommitException(statusMessage.toString());
            }
        }
        else { // revert changes
          if ( !rollback() ) {
              statusMessage.append("\nRollBack failed.");
              throw new JasiCommitException(statusMessage.toString());
          }
          else statusMessage.append("\nRollback of Phase/Origin transaction.");
        }

        return status; // false implies no data or failure
    }


    /*
    * Commits the alternate magnitude list elements, creating the database association rows.
    * for alternate Magnitudes that are not "stale" with a non-null value.
    * Invoked after commit of Solution origin sets associated Magnitude origin id to that of
    * this Solution . Otherwise it may be bogus and need recalculation.
    */
    private boolean commitAlternateMagnitudes(StringBuffer statusMessage) throws JasiCommitException {
        if (debug) System.out.println("commitAlternateMagnitudes");
        if (! commitAlternateMagnitudes) return true; // by default, disabled

        MagList magList = (MagList) getAlternateMagnitudes();
        int count = magList.size();
        if (debug) {
          System.out.println("commitAlternateMagnitudes of count:"+count+" values:");
          System.out.println(magList.toNeatString(true));
        }
        // return true if empty list
        if (count == 0) return true; // nothing to commit, a no-op, aww 11/13

        boolean status = true;
        boolean success = true;

        MagnitudeTN mag = null;
        boolean dbCommitFailure = false;
        for (int idx = 0; idx < count; idx++) {
            mag = (MagnitudeTN) magList.get(idx);
            if (debug) System.out.println("Processing Alternate Magnitude magid: "+mag.getMagidValue());
            // only do those with not null values and associated with this solution
            if ( ! mag.isAssignedTo(this) || mag.hasNullValue() ) {
                statusMessage.append("\nWARNING! Alternate magnitude unassociated with solution or null value, skipping commit for magid: "+mag.getMagidValue());
                continue;
            }

            // Check for "stale" magnitudes, list data of summary mag may be from prior
            // solution calculations - hopefully user does not want to save stale mags.
            if (! mag.isDeleted() && mag.isStale()) { // added isDeleted check 12/04/2006 -aww
                statusMessage.append(
                  "\nAlternate Magnitude magid: "+mag.getMagidValue()+" is STALE magnitude!"
                );
                //throw new JasiCommitException(statusMessage.toString()); // NO! don't abort
                //continue; // loop skip, though this mag may be a type we can't recalculate,  // removed skip 02/17/2007
                // note code below forces mag orid to agree its associated solution's orid
                // if orid changes, doing a mag.commit() creates a completely new magid row.
                // Should end application set alternates to to saved to an "unstale" state
                // before commiting them?
            }

            // make sure mag origin id matches solution, since solution id may have changed
      /* TEST REMOVAL HERE, moved to Magnitude.commit() -aww 01/13/2005
            if ( ! (mag.getOridValue() == getOridValue()) ) { // get the orid after origin commit
                mag.setOrid(getOridValue());  // causes mag.hasChanged() == true, new magid insert by mag.commit
            }
      */
            // Should alternate mag readings be addOrReplaced in solution's lists just before commit,
            // a problem if many magnitudes of same magType in list. - aww -06/29/2004
            //   JasiCommitableListIF aList = mag.getReadingList()
            //   sol.addOrReplaceAll(aList); // doesn't allow duplicate table row ids for instances
            //   sol.associateAll(aList); // may have id duplicates, same id but different instances
            // after above list additions user should see additions in GUI for solution lists but
            // subsequent magnitude processing may clear this data out, particularly since the
            // sol lists are coupled with preferred magnitude lists through list listeners -aww
            //
            try {
                boolean fdb = !mag.hasChangedValue(); // check changed value b4 commit, assumes new insert only if changed -aww 2010/09/01 
                //System.out.println("DEBUG SolutionTN commitAlternateMagnitudes b4 writePrefMag test fromDbase FDB value is : " + fdb);
                status = mag.commit(); // should skip if deleted, but it might commit if stale
                if (!mag.isDeleted() && prefMagMap.containsValue(mag) && !fdb) {// update eventprefmag table only if changed b4 commit -aww 2010/09/01
                    status &= writePrefMag(mag, statusMessage); // treats deleted state as special
                }

                if (status) {
                    statusMessage.append("\nAlternate Magnitude write succeeded for magid: "+mag.getMagidValue());
                    status = doDbCommit(); // after each successful magnitude commit()
                    if (status) {
                      mag.setTimeStamp(new java.util.Date());  // approximate, would need to update with SYS_EXTRACT_UTC(CURRENT_TIMESTAMP) after commit() 
                      statusMessage.append("\nAlternate Magnitude commit succeeded for magid: "+mag.getMagidValue());
                    }
                    else {
                      statusMessage.append("\nAlternate Magnitude commit failed for magid: "+mag.getMagidValue());
                      dbCommitFailure = true;
                      break; // don't do loop processing if db commit fails, db connection is bad?
                    }
                }
                else {
                    statusMessage.append("\nAlternate Magnitude write failed for magid: "+mag.getMagidValue());
                }

                // process an exception when status == false. - 11/12/03 aww
                if (!status) throw new JasiCommitException("\nAlternate Magnitude write failed for magid: "+mag.getMagidValue());
            }
            catch (JasiCommitException ex) {
              status = false;
              System.err.println(ex);
              if (! rollback()) {
                statusMessage.append("\nRollBack failed.");
                throw new JasiCommitException(statusMessage.toString());
              }
              // otherwise keep going
            }
            success &= status;
        } // end of loop
        if (dbCommitFailure) throw new JasiCommitException(statusMessage.toString());
        return success;
    }


    private static boolean doDbCommit() {
        boolean retVal = true;
        try {
            if (debug || JasiDatabasePropertyList.debugSQL)
                System.out.println("DataSource.commit SolutionTN >>> ");
            DataSource.getConnection().commit();
            if (debug || JasiDatabasePropertyList.debugSQL)
                System.out.println("DataSource.commit complete.");
        }
        catch (SQLException ex)  {
            retVal = false;
            System.err.println(ex);
            ex.printStackTrace();
        }
        return retVal;
    }

    /** Rollback the last set of dbase transactions. Catch exceptions and print them.
    * Returns false on exception.
    * */
    protected static boolean rollback() {
        try {
            if (debug || JasiDatabasePropertyList.debugSQL)
                System.out.println("DataSource.rollback SolutionTN >>> ");
            DataSource.getConnection().rollback();
            if (debug || JasiDatabasePropertyList.debugSQL)
                System.out.println("DataSource.rollback complete.");
        } catch (SQLException ex)  {
            System.err.println(ex);
            ex.printStackTrace();
            return false;
        }
        return true;
    }
/** Returns the next identifying event sequence number from the datasource. */
    public long getNextId() {
        return SeqIds.getNextSeq(DataSource.getConnection(),
                   org.trinet.jdbc.table.Event.SEQUENCE_NAME);
    }

    public boolean isInsideRegion(String regionName) {
        CallableStatement csN = null;
        int status = 0;
        if (regionName == null || lat.isNull() || lon.isNull()) return false;
        final String sql = "{ ?=call geo_region.inside_border(?,?,?) }";
        try {
            if (csN == null) {
              csN = DataSource.getConnection().prepareCall(sql);
              csN.registerOutParameter(1, java.sql.Types.INTEGER);
            }
            csN.setString(2, regionName);
            csN.setObject(3, lat.doubleValue(), java.sql.Types.NUMERIC);
            csN.setObject(4, lon.doubleValue(), java.sql.Types.NUMERIC);
            csN.execute();
            status = csN.getInt(1); 
        }
        catch (SQLException ex) {
                ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, csN, debug);
          Utils.closeQuietly(csN);
        }
        return (status > 0);
    }

    public long getAnyDuplicate() {
        //return 0l;
        //
        CallableStatement csN = null;
        final String sql = "{ ?=call match.getMatch(?, ?, ?, ?, ?, ?, ?) }";
        long dupId = 0l;
        try {
            if (csN == null) {
              csN = DataSource.getConnection().prepareCall(sql);
              csN.registerOutParameter(1, java.sql.Types.BIGINT);
            }
            // use current values 
            csN.setLong(2, getEvidValue());
            csN.setLong(3, getPreforValue());
            csN.setString(4, getAuthority()); // origin auth
            csN.setDouble(5, lat.doubleValue());
            csN.setDouble(6, lon.doubleValue());
            csN.setDouble(7, (datetime.isValidNumber() ? datetime.doubleValue() : 0.));
            csN.setString(8, null); // null subsource
            csN.execute();
            dupId = csN.getLong(1); 
            //
        }
        catch (SQLException ex) {
                ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, csN, debug);
          if (debug || JasiDatabasePropertyList.debugSQL) 
            System.out.println("DEBUG SolutionTN getAnyDuplicateEvid call match.getMatch(" + id.longValue() + ") returned dupid: " + dupId);
          Utils.closeQuietly(csN);
        }
        return dupId;
        //
    }

    public boolean isQuarry() {

        if ( isEventDbType(EventTypeMap3.QUARRY) ) return true;

        CallableStatement csQ = null;
        long gazid = 0l;
        // Do gazetteerquarry rows need to have the max event depth allowed adjusted for geiod ? 2015/03/16 ?
        final String sql = "{ ?=call quarry.isquarry(?,?,?,?,?) }";
        try {
            if (csQ == null) {
              // inputs now (lat,lon,time,depth,mag) aww -07/14/2006
              csQ = DataSource.getConnection().prepareCall(sql);
              csQ.registerOutParameter(1, Types.BIGINT);
            }
            csQ.setDouble(2, lat.doubleValue());
            csQ.setDouble(3, lon.doubleValue());
            csQ.setDouble(4, (datetime.isValidNumber() ? datetime.doubleValue() : 0.));
            csQ.setDouble(5, (depth.isValidNumber() ? depth.doubleValue() : 0.));
            double mag = (magnitude != null && ! magnitude.hasNullValue()) ? magnitude.getMagValue() : 0.;
            csQ.setDouble(6, mag);
            csQ.execute();
            gazid = csQ.getLong(1); 
            //
        }
        catch (SQLException ex) {
                ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, csQ, debug);
          Utils.closeQuietly(csQ);
        }
        return (gazid > 0l);
    }

    public int postId(String group, String source, String state, int rank, int result) {

        if (group == null || source == null || state == null || rank < 0) {
            System.err.println("ERROR: SolutionTN postId found bad parameter for input group: "+
                    group+" source: "+source+" state: "+state+ " rank: "+rank);
            //throw new IllegalArgumentException(); 
            return -1;
        }

        CallableStatement cs = null;
        int status = 0;
        Connection conn = DataSource.getConnection();
        final String sql = "{ ?=call PCS.putState(?,?,?,?,?,?) }";
        try {
            if ( conn.isClosed() ) {
                System.err.println("ERROR: SolutionTN postId db connection is closed!");
                return -1;
            }

            // posted id, commits, does transitions, if any exist for state description
            if (JasiDatabasePropertyList.debugSQL) {
                System.out.println("DEBUG SolutionTN postId sql: " + sql);
            }

            cs = conn.prepareCall(sql);
            cs.registerOutParameter(1, java.sql.Types.INTEGER);
            cs.setString(2, group);
            cs.setString(3, source);
            cs.setLong(4, id.longValue());
            cs.setString(5, state);
            cs.setInt(6, rank);
            cs.setInt(7, result);
            cs.execute();
            status = cs.getInt(1);
            conn.commit();
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(conn, ex, "Error in postId", "SQLException in postId sql error: ");
        } finally {
            JasiDatabasePropertyList.debugSQL(sql, cs, debug);
            try {
                if (cs != null) {
                    cs.close();
                }
            } catch (SQLException ex) {
              JiggleExceptionHandler.handleDbException(conn, ex);
            }
        }
        return status;
    }

    public static String getAlarmHistory(long evid) {

        StringBuffer sb = new StringBuffer(2048); 

        if (alarmDbLinks == null || alarmDbLinks.length == 0) {
            sb.append( getAlarmHistory(evid, "") );
            return sb.toString();
        }

        for (int jdx = 0; jdx<alarmDbLinks.length; jdx++) {
            sb.append( SolutionTN.getAlarmHistory( evid, alarmDbLinks[jdx]) );
            sb.append("----------------------------------------------------------------------------------------------------\n");
        }

        return sb.toString();

    }

    private static String getAlarmHistory(long evid, String dbLink) {

        Connection conn = DataSource.getConnection();

        PreparedStatement sm = null;
        ResultSet rs = null;

        //String sql = "SELECT event_id, alarm_action, action_state, modcount, mod_time FROM alarm_action WHERE event_id=? ORDER BY mod_time DESC";
        final String sql = "SELECT event_id, alarm_action, action_state, modcount, mod_time FROM alarm_action"+
                         ((dbLink == null) ? "" : dbLink) + " WHERE event_id=? ORDER BY mod_time DESC";
        
        StringBuffer sb = new StringBuffer(1024); 
        sb.append("Event_Id        Action          State           ModCnt          ModDate             ").append(dbLink).append("\n");

        try {
            sm = conn.prepareStatement(sql);
            sm.setLong(1, evid); 
            rs = sm.executeQuery();

            long eventid = 0l;
            String action = "";
            String state = "";
            int modcount = 0;
            String modtime = "";

            org.trinet.util.Format fs15 = new org.trinet.util.Format("%-15.15s");

            while (rs.next()) {

              eventid  = rs.getLong(1);
              action   = rs.getString(2);
              state    = rs.getString(3);
              modcount = rs.getInt(4);
              modtime  = rs.getString(5);

              sb.append( fs15.form(String.valueOf(eventid))).append(" ");
              sb.append( fs15.form(action)).append(" ");
              sb.append( fs15.form(state)).append(" ");
              sb.append( fs15.form(String.valueOf(modcount))).append(" ");
              sb.append( modtime.substring(0,19) );
              sb.append("\n");
            }
            if (eventid == 0l ) sb.append(evid).append(" has no alarm actions in database\n");
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, sm, debug);
          Utils.closeQuietly(rs, sm);
        }
        return sb.toString();
    }

    public static String getPrefMecDesc(long evid, long prefmec, boolean doHistory, boolean appendKey) {

        Connection conn = DataSource.getConnection();

        PreparedStatement sm = null;
        ResultSet rs = null;

        //String sql = "select mecid,mechtype,mecalgo,subsource,auth,nsta,quality,strike1,dip1,rake1," +
        //   " strike2,dip2,rake2,pvr,lddate,nvl(oridin,0),nvl(magid,0)" +
        //   " from mec m where m.mecid in (select p.mecid from eventprefmec p where p.evid=? union " +
        //   " select m2.mecid from mec m2, event e where e.evid=? and m2.oridin=e.prefor) order by m.lddate desc";
        
        final String sql = "select m.mecid,m.mechtype,m.mecalgo,m.subsource,m.auth,m.nsta,m.quality,m.strike1,m.dip1,m.rake1,"+
            "m.strike2,m.dip2,m.rake2,m.pvr,m.lddate,coalesce(m.oridin,0),coalesce(m.magid,0),coalesce(o.prefmec,0)" +
            (doHistory ?
            " from mec m, origin o where o.evid=? and m.oridin=o.orid" :
            " from mec m, eventprefmec p, origin o where p.evid=? and m.mecid=p.mecid and o.orid=m.oridin")
            + " order by m.lddate desc, m.mechtype desc, m.mecid desc";
        
        StringBuffer sb = new StringBuffer(1024); 
        sb.append("Mecid           Type     Algo     Source   Auth       Nsta  Qty Strik1   Dip1  Rake1 Strik2   Dip2  Rake2 PvR LdDate              OE OridIn/Magid\n");

        try {
            sm = conn.prepareStatement(sql);
            sm.setLong(1, evid); 
            //sm.setLong(2, evid); 
            rs = sm.executeQuery();

            org.trinet.util.Format fs15 = new org.trinet.util.Format("%-15.15s");
            org.trinet.util.Format fs8 = new org.trinet.util.Format("%-8.8s");
            org.trinet.util.Format fi6 = new org.trinet.util.Format("%6d");
            org.trinet.util.Format fi3 = new org.trinet.util.Format("%3d");
            org.trinet.util.Format fd42 = new org.trinet.util.Format("%4.2f");

            long  mecid  = 0l;
            String  type  = "";
            String  algo = "";
            String  src  = "";
            String auth = "";
            int  nsta  = 0;
            double qual  = 0.;
            int  strike = 0;
            int  dip = 0;
            int  rake  = 0;
            int  strike2 = 0;
            int  dip2= 0;
            int  rake2  = 0;
            int  pvr = 0;
            long oridin = 0l;
            long magid = 0l;
            long oprefmec = 0l;
            String lddate = "";

            String oldType = null;

            while (rs.next()) {

              mecid   = rs.getLong(1);

              type   = rs.getString(2);
              if ( oldType != null && !oldType.equals(type) ) {
                  sb.append("\n");
              }
              oldType = type;
              algo = rs.getString(3);
              src  = rs.getString(4);
              auth = rs.getString(5);
              nsta  = rs.getInt(6);
              qual  = rs.getDouble(7);

              strike  = rs.getInt(8);
              dip  = rs.getInt(9);
              rake  = rs.getInt(10);

              strike2  = rs.getInt(11);
              dip2  = rs.getInt(12);
              rake2  = rs.getInt(13);
              pvr = rs.getInt(14);

              lddate = rs.getString(15);
              oridin = rs.getLong(16);
              magid = rs.getLong(17);
              oprefmec = rs.getLong(18);

              sb.append( fs15.form(String.valueOf(mecid))).append(" ");
              sb.append( fs8.form(type) ).append(" ");
              sb.append( fs8.form(algo) ).append(" ");
              sb.append( fs8.form(src) ).append(" ");
              sb.append( fs8.form(auth) ).append(" ");
              sb.append( fi6.form(nsta) ).append(" ");
              sb.append( fd42.form(qual) ).append(" ");
              sb.append( fi6.form(strike) ).append(" ");
              sb.append( fi6.form(dip) ).append(" ");
              sb.append( fi6.form(rake) ).append(" ");
              sb.append( fi6.form(strike2) ).append(" ");
              sb.append( fi6.form(dip2) ).append(" ");
              sb.append( fi6.form(rake2) ).append(" ");
              //sb.append( String.format("%3d", pvr) ).append(" ");
              sb.append( fi3.form(pvr) ).append(" ");
              sb.append( lddate.substring(0,19) ).append(" ");
              sb.append((oprefmec != 0l && oprefmec == mecid) ? "+" : "-");
              sb.append((prefmec != 0l && prefmec == mecid) ? "+" : "-");
              sb.append(" ");
              sb.append(oridin).append("/").append(magid);

              sb.append("\n");
            }
            if (mecid == 0l ) {
                sb.append(evid).append(" has no mec in database\n");
            }
            else if (appendKey) {
                sb.append("\nOE column key:\nO '+' = prefmec of origin\nE '+' = prefmec of event\n");
            }
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, sm, debug);
          Utils.closeQuietly(rs, sm);
        }
        return sb.toString();
    }

/*
  public static final class Tester {
    public static final void main(String args[]) {

      //60739309 60385902
      if ( args.length < 4 ) {
          System.out.println("Syntax input args: [user] [pwd] [dbhost, domain qualified] [dbname] ");
          System.exit(0);
      }
      String user   = args[0];
      String passwd = args[1];
      String host   = args[2]; 
      String dbname = args[3];

      String url= "jdbc:" + AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL +":@"+ host +":"+ AbstractSQLDataSource.DEFAULT_DS_PORT +":"+ dbname;
      System.out.println("SolutionTN: making connection... ");
      System.out.println("url: " + url);
      DataSource init = new DataSource (url, AbstractSQLDataSource.DEFAULT_DS_DRIVER, user, passwd); 
      init.setWriteBackEnabled(true);
      // get one by evid
      //System.out.println("Dup of 71070629 is " + Solution.create().getById(71070629).getAnyDuplicate() + " == 71965090");
      System.out.println("Dup of is 60385902 is " + Solution.create().getById(60385902).getAnyDuplicate() + " == 60739309");
      init.close();
      System.exit(0);
      if (false) {
        // Test commit with NO CHANGES
        System.out.println("========== Testing delete ... ");
        // MUST PUT A DELETEABLE EVID HERE TO TEST!
        Solution sol2 = Solution.create().getById(11558792);
        if (sol2 != null) {
          sol2.delete();
          try {
            sol2.commit();                //CodaTN
          } catch (JasiCommitException ex) {}
          System.out.println(sol2.getCommitStatus());
        }
      }

      //Solution sol1 = Solution.create("TN").getById(evid);
      Solution sol1 = Solution.create().getById(evid);

      // //////////////////
      try {
        LatLonZ llz = sol1.getLatLonZ();
        llz.setLat(llz.getLat()+0.001);  // change something to force insert
        sol1.setLatLonZ(llz);
        sol1.commit();
      }
      catch (JasiCommitException ex) {
      }

      //Solution sol1 = Solution.create(JasiObject.TRINET).getById(evid);
      //sol1.loadAmpList();         // origin assoc amps
      sol1.loadMagAmpList();      // mag assoc amps
      sol1.loadPhaseList();
      sol1.loadCodaList();

//    System.out.println(sol1.toSummaryString()+" source="+sol1.source.toString());
      System.out.println(sol1.toNeatString());

      System.out.println(sol1.getPhaseList().toNeatString());
      System.out.println(sol1.getAmpList().toNeatString());
      System.out.println(sol1.magnitude.ampList.toNeatString());
      System.out.println(sol1.getCodaList().toNeatString());
      System.out.println("Cloning phaselist, dumping it ...");
      System.out.println( ((PhaseList)sol1.getPhaseList().clone()).toNeatString());
      System.out.println("End of clone dump.");
      System.out.println("Cloning mag amplist, toNeatString ...");
      System.out.println(((AmpList)((AmpList)sol1.magnitude.getReadingList()).clone()).toNeatString());
      System.out.println("Cloning mag amplist, dumpToString ...");
      System.out.println(((AmpList)((AmpList)sol1.magnitude.getReadingList()).clone()).dumpToString());
      System.out.println("End of clone dump.");

      if (false) {
        // Test commit with NO CHANGES
        System.out.println("========== Testing commit with NO CHANGES... ");
        try {
          sol1.commit();                //CodaTN
        } catch (JasiCommitException ex) {}
        System.out.println(sol1.getCommitStatus());
      }

      if (false) {
        // Test changing an Event attribute
        System.out.println("========== Change Event value (eventType)");
        //sol1.setEventType(EventTypeMap.UNKNOWN);
        sol1.eventType.setValue( EventTypeMap3.getMap().toJasiType(EventTypeMap3.UNKNOWN) ); // to not bump version -aww 2008/04/15
        System.out.println(sol1.toSummaryString());
        try {
          sol1.commit();
        } catch (JasiCommitException ex) {}
        System.out.println(sol1.getCommitStatus());
      }

      if (false) {
        // Test changing an Origin attribute
        System.out.println("========== Change Origin value (lat)");
        sol1.lat.setValue(37.89);
        System.out.println(sol1.toSummaryString());
        try {
          sol1.commit();
        } catch (JasiCommitException ex) {}
        System.out.println(sol1.getCommitStatus());
      }

      if (false) {
        // Test setting new Mag
        System.out.println("========== Recalc. Mag ");

        EnvironmentInfo.setNetworkCode("CI");
        EnvironmentInfo.setApplicationName("Test");
        EnvironmentInfo.setAutomatic(false);

        System.out.println("Old Mag = "+ sol1.magnitude.toNeatString());
        System.out.println(" Calculating mag (SoCalML) for: "+evid);
        MagnitudeEngineIF magEng = AbstractMagnitudeEngine.create();
        magEng.setMagMethod(new SoCalMlMagMethod());

//      magEng.solve(sol.magnitude);
        magEng.solve(sol1);  // returns boolean value true upon success

        Magnitude newMag = magEng.getSummaryMagnitude();
//        if (newMag != null) {
//          newMag.value.setValue(1.11);
//          newMag.subScript.setValue("n");
//          newMag.authority.setValue("CI");
//        }

        boolean match = (newMag == sol1.magnitude);
        System.out.println("newMag==preferred? "+ match);
        if (match) {
          System.out.println("NewMag    = "+ newMag.toNeatString());
        }
        else {
          System.out.println("NewMag    = "+ newMag.toNeatString());
          System.out.println("Preferred = "+ sol1.magnitude.toNeatString());
        }
        //try {
            //sol1.commit();
        //} catch (JasiCommitException ex) {
        //}
        //System.out.println(sol1.getCommitStatus());
      }
    }  // end of Main
  } // end of Tester
*/
} // end of SolutionTN class
