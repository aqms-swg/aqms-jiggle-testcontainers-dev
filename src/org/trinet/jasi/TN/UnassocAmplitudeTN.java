package org.trinet.jasi.TN;

import java.util.*;
import java.sql.*;

import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.jdbc.table.UnassocAmp;
import org.trinet.jasi.*;
import org.trinet.util.*;

/** * Object maps to amplitude data that come from the UnassocAmp table.  */
public class UnassocAmplitudeTN extends UnassocAmplitude implements DbMagnitudeAssocIF, DbReadableJasiObjectIF {

    // Static non-private indentifiers HIDE the those in super class.
    // Compiler uses the value of the static member reference in class
    // it's compiling so you must override any methods where you want
    // the compiler use the static member value of the subclass, and not
    // its parent value. One way to avoid this is to replace the reference
    // the static data member with an instance method accessor which can be
    // overridden in the subclass to return the correct static data member.
    // e.g. DataTableRow getTableObj() - aww

    // make access private to avoid ambiguity for now, make protected if necessary
    private static final UnassocAmp tableObj = null;

    // Implementation mirrors AmplitudeTN superclass - aww
    private static JasiDbReader jasiDataReader =  null;  // lazy instantiate in getDateReader, make new JasiDbReader(this); // if not static
    /*
    //private static  JasiDbReader jasiDataReader = new JasiDbReader("org.trinet.jasi.TN.UnassocAmplitudeTN");
    //private static JasiDbReader jasiDataReader = new JasiDbReader();
    {
      //jasiDataReader.setFactoryClassName(getClass().getName());
      jasiDataReader.setDebug(JasiDatabasePropertyList.debugSQL);
    }
    */

    // override here only if UnAssoc table columns need differ from Amp
    private static final String psSqlPrefix=
        //"SELECT amp.AMPID,amp.DATETIME,amp.STA,amp.NET,amp.AUTH,amp.SUBSOURCE,amp.CHANNEL,amp.CHANNELSRC,amp.SEEDCHAN,amp.LOCATION," +
        //"amp.AMPLITUDE,amp.AMPTYPE,amp.UNITS,amp.AMPMEAS,amp.PER,amp.SNR,amp.QUALITY,amp.RFLAG,amp.CFLAG,amp.WSTART,amp.DURATION";
        "SELECT amp.AMPID,TRUETIME.getEpoch(amp.DATETIME,'UTC'),amp.STA,amp.NET,amp.AUTH,amp.SUBSOURCE,amp.CHANNEL,amp.CHANNELSRC,"+
        "amp.SEEDCHAN,amp.LOCATION,amp.AMPLITUDE,amp.AMPTYPE,amp.UNITS,amp.AMPMEAS,amp.PER,amp.SNR,amp.QUALITY,amp.RFLAG,amp.CFLAG,"+
        //"TRUETIME.getEpoch(amp.WSTART,'UTC'),amp.DURATION";
        "TRUETIME.getEpoch(amp.WSTART,'UTC'),amp.DURATION,amp.fileid";
    //

    private static final String psSqlPrefixByAmpid = UnassocAmplitudeTN.psSqlPrefix + " FROM UNASSOCAMP amp WHERE (amp.ampid=?)";
    private static final String psSqlPrefixAll = UnassocAmplitudeTN.psSqlPrefix + " FROM UNASSOCAMP amp ORDER BY amp.DATETIME DESC,amp.LDDATE DESC";
    private static final String psSqlPrefixByTime = UnassocAmplitudeTN.psSqlPrefix +
        " FROM UNASSOCAMP amp WHERE (amp.DATETIME between TRUETIME.putEpoch(?,'UTC') AND TRUETIME.putEpoch(?,'UTC'))" +
        " ORDER BY amp.DATETIME DESC, amp.LDDATE DESC";

    private static final String psSqlPrefixByTimeNet = UnassocAmplitudeTN.psSqlPrefix +
        " FROM UNASSOCAMP amp WHERE (amp.DATETIME between TRUETIME.putEpoch(?,'UTC') AND TRUETIME.putEpoch(?,'UTC'))" +
        " AND amp.NET=? ORDER BY amp.NET,amp.STA,amp.SEEDCHAN,amp.LOCATION,amp.DATETIME DESC,amp.LDDATE DESC";

    private static final String psSqlPrefixByTimeSubsource = UnassocAmplitudeTN.psSqlPrefix +
        " FROM UNASSOCAMP amp WHERE (amp.DATETIME between TRUETIME.putEpoch(?,'UTC') AND TRUETIME.putEpoch(?,'UTC'))" +
        " AND amp.subsource=? ORDER BY amp.NET,amp.STA,amp.SEEDCHAN,amp.LOCATION,amp.DATETIME DESC,amp.LDDATE DESC";

    private static final String psSqlOrderSuffix = " ORDER BY amp.NET,amp.STA,amp.SEEDCHAN,amp.LOCATION,amp.LDDATE DESC,amp.AMPID";

    /** The ampid */
    protected DataLong ampid = new DataLong();

    /** True if data were orginally read from or has been written to the dbase.  Used to know if a commit requires an 'insert' or an 'update' */
    protected boolean fromDbase = false;

    /** Create a Unassoc table row of the proper type.*/
    public UnassocAmplitudeTN() { }


    public void setDebug(boolean tf) {
      debug = tf;
      if (jasiDataReader != null) jasiDataReader.setDebug(debug);
    }


    // below method overrides super class to access to class static data
    protected DataTableRow getTableObj()  {
      return (UnassocAmplitudeTN.tableObj == null) ?  new UnassocAmp() : UnassocAmplitudeTN.tableObj;
    }

    /** Return Collection of all amps from the UnassocAmp table.
     * They are sorted, recent to old, in order of both time of the amp and the lddate descending * into the database.
     * @See UnassocAmp
     * */
    public Collection getAll() {
        ArrayList aList = null;
        PreparedStatement psAll = null;
        try {
            Connection conn = DataSource.getConnection();
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(UnassocAmplitudeTN.psSqlPrefixAll);
            psAll = conn.prepareStatement(UnassocAmplitudeTN.psSqlPrefixAll);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psAll, this, true);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        return aList;
    }

  /**
   * Return Collection of all amps from the table whose channel names also exist in
   * the Channel_Data table. This insures a LatLonZ lookup will succeed.
   * Collection is sorted, recent to old, in order of both time of the amp and lddate descending.
   */
    public Collection getAllViable() {
      StringBuffer sb = new StringBuffer(1024); 
      sb.append(UnassocAmplitudeTN.psSqlPrefix);
      sb.append(" FROM UNASSOCAMP amp, JASI_CHANNEL_VIEW c ");
      sb.append("WHERE c.STA=amp.STA AND c.SEEDCHAN=amp.SEEDCHAN AND c.NET=amp.NET AND c.LOCATION=amp.LOCATION ");
      sb.append("ORDER BY amp.DATETIME DESC, amp.LDDATE DESC");
      return ((JasiDbReader)getDataReader()).getBySQL(DataSource.getConnection(), sb.toString(), this);
    }

    /**
     * Override like parent inserts new row in database if new or hasChanged(),
     * but does not write any association table data.
    */
    // An alternative, override dbaseAssociate to return true and use super.commit()
    public boolean commit() {
        if (!DataSource.isWriteBackEnabled()) return false;
        setDebug(JasiDatabasePropertyList.debugTN); 
        if (debug) System.out.println ("DEBUG "+getClass().getName()+" commit() fromDB:"+fromDbase+" "+toNeatString());

        // Reading is deleted or not associated! So don't write new rows.
        if (isDeleted()) return true;

        boolean status = false; // commit status
        if (fromDbase) {  // "existing" (came from the dbase)
          if (getNeedsCommit() || hasChanged()) { // force new row (sequence number)
            if (debug) System.out.println ("(changed) INSERT ");
            status = dbaseInsert(); // insert
          } else { // if no changes, just make new association
            if (debug) System.out.println ("(unchanged) no-op");
          }
        // virgin (not from dbase) <INSERT>
        } else {
          if (debug) System.out.println ("(new) INSERT");
          status = dbaseInsert(); // insert
        }
        setNeedsCommit(! status); // hopefully false?
        return status;
    }


    public Collection getByEarliestLoadedOridForSolutionNets(long evid, String[] netList) {
        return null;
    }

    public Collection getByAmpSet(long evid, long ampsetid) {
        return null;
    }
    public Collection getByValidAmpSet(long evid, String ampsettype, String subsrcExpr) {
        return null;
    }

    public Collection getByMagnitude(Magnitude mag) {
        return null;
    }
    public Collection getByMagnitude(Magnitude mag, String[] typeList) {
        return null;
    }
    public Collection getByMagnitude(Magnitude mag, String[] typeList, boolean assoc) {
        return null;
    }
    public Collection getBySolution(Solution aSol) {
        return null;
    }
    public Collection getBySolution(Solution aSol, String[] typeList) {
        return null;
    }
    public Collection getBySolution(Solution aSol, String[] typeList, boolean assoc) {
        return null;
    }
    public Collection getBySolutionNets(long id, String[] netList) {
        return null;
    }
    public Collection getBySolution(long id) {
        return null;
    }
    public Collection getBySolution(long id, String[] typeList) {
        return null;
    }

    /**
     * Returns list of Amplitudes from the default DataSource whose times fall
     * within specified input time window.
     * Times are seconds in UNIX epoch time.
     * Returns null if no amplitudes are found. <br>
     * (NOTE: in TriNet this gets amps from ALL sources (RT1, RT2)
     * so there will be duplicate readings.)
     * @see #getByTime(Connection, double, double)
     * */
     // This is quite SLOW, need dbase indexing?
    public Collection getByTime(double start, double end) {
        return getByTime(DataSource.getConnection(), start, end);
    }

    /**
     * Returns list of Amplitudes from the specified data connection
     * whose times fall within specified input time window.
     * @see #getByTime(double, double)
     */
    public Collection getByTime(Connection conn, double start, double end) {
        
        ArrayList aList = null;
        PreparedStatement psByTime = null;
        try {
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(UnassocAmplitudeTN.psSqlPrefixByTime);
            psByTime = conn.prepareStatement(UnassocAmplitudeTN.psSqlPrefixByTime);
            psByTime.setDouble(1, start);
            psByTime.setDouble(2, end);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByTime, this, true);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }

        return aList;
    }

    public Collection getByTimeSubsource(double start, double end, String subsource) {
        return getByTimeSubsource(DataSource.getConnection(), start, end, subsource);
    }

    public Collection getByTimeSubsource(Connection conn, double start, double end, String subsource) {
        ArrayList aList = null;
        PreparedStatement psByTime = null;
        try {
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(UnassocAmplitudeTN.psSqlPrefixByTimeSubsource);
            psByTime = conn.prepareStatement(psSqlPrefixByTimeSubsource);
            psByTime.setDouble(1, start);
            psByTime.setDouble(2, end);
            psByTime.setString(3, subsource);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByTime, this, true);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        return aList;
    }

    public Collection getByTimeNet(double start, double end, String net) {
        return getByTimeNet(DataSource.getConnection(), start, end, net);
    }

    public Collection getByTimeNet(Connection conn, double start, double end, String net) {
        ArrayList aList = null;
        PreparedStatement psByTime = null;
        try {
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(UnassocAmplitudeTN.psSqlPrefixByTimeNet);
            psByTime = conn.prepareStatement(psSqlPrefixByTimeNet);
            psByTime.setDouble(1, start);
            psByTime.setDouble(2, end);
            psByTime.setString(3, net);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByTime, this, true);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        return aList;
    }

    protected Amplitude getByAmpid(long id) {
        ArrayList aList = null;
        PreparedStatement psByAmpid = null;
        try {
            Connection conn = DataSource.getConnection();
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(UnassocAmplitudeTN.psSqlPrefixByAmpid);
            psByAmpid = conn.prepareStatement(UnassocAmplitudeTN.psSqlPrefixByAmpid);
            psByAmpid.setLong(1, id);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByAmpid, this, true);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        return (aList == null || aList.isEmpty()) ? null : (Amplitude) aList.get(0);
    }

    /** Create a UnassocAmp table row of the proper type. */
    protected StnChlTableRow createTableRow(long seq) {
        UnassocAmp ampRow = null;
        try {
            ampRow = (UnassocAmp) getTableObj().getClass().newInstance();
            ampRow.setValue(TableRowUnassocAmp.AMPID, seq);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return (StnChlTableRow) ampRow;
    }

    // A matching row key exists in datasource but its actually data may differ from instance
    public boolean existsInDataSource() {
        JasiDbReader jdr = (JasiDbReader) getDataReader();
        int count = jdr.getCountBySQL(DataSource.getConnection(), "UNASSOCAMP", "AMPID", ampid.toString()); 
        return (count > 0);
    }

//
// Code below copied from AmplitudeTN for UnassocAmplitude implementation
//
    public String toAssocIdString() {
        StringBuffer sb = new StringBuffer(super.toAssocIdString());
        sb.append(" orid: ").append(getOridValue()); // orid.toStringSQL());
        sb.append(" fileid: ").append(getFileidValue());
        return sb.toString();
    }

    // accessors for data members
    public Object getIdentifier() {
        return ampid;
    }
    public void setIdentifier(Object obj) {
        ampid.setValue((DataLong) obj);
    }
    public long getAmpid() {
        return (ampid.isValidNumber()) ? ampid.longValue() : 0l;
    }
    public Object getOriginIdentifier() {
        return (sol == null) ? null : sol.getPreferredOriginId();
    }

    public long getOridValue() {
        //return (orid.isValidNumber()) ? orid.longValue() : 0l;
        if (sol == null) return 0l;
        DataLong orid = (DataLong) sol.getPreferredOriginId();
        return  (orid.isValidNumber()) ? orid.longValue() : 0l;
    }
    public long getMagidValue() {
        DataLong dl = (DataLong)getAssociatedMag().getIdentifier();
        return  (dl.isValidNumber()) ? dl.longValue() : 0l;
    }
    //public void setOrid(long id) {orid.setValue(id);}
    public void setAmpid(long id) {ampid.setValue(id);}

    /** Return the SQL clause to get Amplitudes only of the types in the list.
    * For example given String list[] = {"PGA", "WAC", "HEL" }  this will return
    * " and ((amptype like 'PGA') or (amptype like 'WAC') or amptyp like 'HEL')
    * */
    protected String makeTypeClause(String[] typeList) {
        if (typeList == null || typeList.length == 0) return "";
        StringBuffer sb = new StringBuffer(256);
        sb.append(" and (");
        for (int i = 0; i < typeList.length; i++) {
          if (i > 0) sb.append(" or ");
          sb.append("(");
          //sb.append(getTableName()).append(".amptype like '");
          sb.append("amp.amptype like '"); // generic alias "amp"
          sb.append(typeList[i]);
          sb.append("')");
        }
        sb.append(")");
 //     System.out.println( "makeTypeQuery /"+sb.toString()+"/");
        return sb.toString();
    }
    /** Return the SQL clause to get amps only the nets in the list.
    * For example given String list[] = {"CI", "AZ" }  this will return
    * " and ((net like 'CI') or (net like 'AZ') ). This is not impemented using the
    * the SQL construct "IN ('CI', 'AZ')" because this would not allow the use of
    * wildcards. */
    protected String makeNetClause(String[] netList) {
           if (netList == null || netList.length == 0) return "";
           StringBuffer sb = new StringBuffer(128);
           sb.append(" and (");
           for (int i = 0; i < netList.length; i++) {
             if (i > 0) sb.append(" or ");
             sb.append("(");
             //sb.append(getTableName()).append(".net like '");
             sb.append("amp.net like '"); // generic alias "amp"
             sb.append(netList[i]);
             sb.append("')");
           }
           sb.append(")");
           return sb.toString();
    }


    /**
     * Return an Amplitude from the default DataSource whose identifier matches
     * the input id number.
     * Returned Amplitude has null Solution and Magnitude association assigments.
     * Returns null if there is no match.
     * */
    public JasiCommitableIF getById(long id) {
        return getByAmpid(id);
    }
    public JasiCommitableIF getById(Object id) {
        if (id instanceof Number)
            return  getById(((Number)id).longValue());
        else if (id instanceof DataNumber)
            return getById(((DataNumber)id).longValue());
        else return null;
    }

    public AmpList getDuplicate(Amplitude amp) {
        return getDuplicate(DataSource.getConnection(), amp);
    }

    /**
     * Returns Amplitudes from the data source that are likely to be duplicates
     * of the given amp. That is, those that are from the same channel and have the same amp type.
     * Returns null if no amplitudes are found.
     */
     public AmpList getDuplicate(Connection conn, Amplitude amp) {
       return getDuplicate(conn, amp.getChannelObj(), amp.type, amp.getTime(), amp.getValueDoubleValue());
     }
     /**
      * Returns Amplitudes from the data source that are likely to be duplicates
      * of an amp with these attributes:
      * have the same channel name and have the same amptype, value, and datetime.
      * Returns null if no amplitudes are found. Input ampType, time, and value must be valid.
      * @see AmpType
      */
    public AmpList getDuplicate(Connection conn, Channelable chan, int ampType, double time, double value) {

        if ( !AmpType.isLegal(ampType)) return null;

        StringBuffer sb = new StringBuffer(1024);
        sb.append(UnassocAmplitudeTN.psSqlPrefix);
        sb.append(" FROM UNASSOCAMP amp ");
        sb.append(" AND amp.NET=? AND amp.STA=? AND amp.SEEDCHAN=? AND amp.LOCATION=? AND amp.AMPTYPE=?");
        sb.append(" AND amp.DATETIME=? AND amp.VALUE=?"); 
        sb.append(" ORDER BY amp.LDDATE desc");

        ArrayList aList = null;
        PreparedStatement psByChanType = null;
        Channel chn = chan.getChannelObj();
        try {
            if (debug || JasiDatabasePropertyList.debugSQL) System.out.println(sb.toString());
            psByChanType = conn.prepareStatement(sb.toString());
            psByChanType.setString(1, chn.getNet());
            psByChanType.setString(2, chn.getSta());
            psByChanType.setString(3, chn.getSeedchan());
            psByChanType.setString(4, chn.getLocation());
            psByChanType.setString(5, AmpType.getString(ampType));
            psByChanType.setDouble(6, time);
            psByChanType.setDouble(7, value);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByChanType, this, true);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }

        if (aList == null) return null;

        int dbCount  = aList.size();
        //System.out.println("getDuplicate: amps found ="+dbCount);
        if (dbCount < 1) return null; // no data

        UnassocAmpListTN ampList = new UnassocAmpListTN(dbCount);
        ampList.fastAddAll(aList);
        return ampList;
    }

    public JasiObject parseData(Object rsdb) {
        return parseResultSetDb((ResultSetDb)rsdb);
    }
    public JasiObject parseResultSetDb(ResultSetDb rsdb) {
        return parseResultSet(rsdb);
    }

    /**
     * Parse a resultset row that contains a UnassocAmplitude 
     */
    protected Amplitude parseResultSet(ResultSetDb rsdb) {
        return parseResultSet(rsdb, getClass());
    }

    protected static Amplitude parseResultSet(ResultSetDb rsdb, Class myClass) {

        // build up the Amplitude from the jdbc objects
        UnassocAmplitudeTN ampNew =  null;

        try {
             ampNew = (UnassocAmplitudeTN) myClass.newInstance();
        }
        catch (Exception ex) {
          ex.printStackTrace();
          return null;
        }

        Channel chan = ampNew.getChannelObj();
        ResultSet rs = rsdb.getResultSet();

        //
        // Parse query resultset columns
        //
        try {
            int offset = 1;
            //ampid
            ampNew.ampid.setValue(rs.getLong(offset++));

            //datetime
            double dd = rs.getDouble(offset++);
            if (!rs.wasNull()) ampNew.datetime.setValue(dd);

            //sta
            chan.setSta(rs.getString(offset++));
            //net
            chan.setNet(rs.getString(offset++));

            //auth
            ampNew.authority.setValue(rs.getString(offset++));
            //subsource
            ampNew.source.setValue(rs.getString(offset++));

            //channel
            chan.setChannel(rs.getString(offset++));
            //channelsrc
            chan.setChannelsrc(rs.getString(offset++));
            //seedchan
            chan.setSeedchan(rs.getString(offset++));
            //location
            chan.setLocation(rs.getString(offset++));

            //amplitude
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) ampNew.value.setValue(dd);

            //amptype
            String ampType = rs.getString(offset++);
            ampNew.type = AmpType.getInt(ampType);

            //units
            String unitstr = rs.getString(offset++);
            ampNew.units = Units.getInt(unitstr);

            //ampmeas
            String half = rs.getString(offset++);
            if (half != null && half.equals("0")) ampNew.halfAmp=false;

            //per
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) ampNew.period.setValue(dd);

            //snr
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) ampNew.snr.setValue(dd);

            //quality
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) ampNew.quality.setValue(dd);
            //else ampNew.quality.setNull(true);

            //rflag
            ampNew.processingState.setValue(rs.getString(offset++));

            //cflag
            String clip = rs.getString(offset++);
            if (clip.equalsIgnoreCase("OS")) {
                ampNew.clipped = ON_SCALE;
            }
            else if (clip.equalsIgnoreCase("BN")) {
                ampNew.clipped = BELOW_NOISE;
            }
            else if (clip.equalsIgnoreCase("CL")) {
                ampNew.clipped = CLIPPED;
            }

            //windowstart
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) ampNew.windowStart.setValue(dd);

            //duration
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) ampNew.windowDuration.setValue(dd);

            //fileid
            dd = rs.getLong(offset++);
            if (!rs.wasNull()) ampNew.fileid.setValue(dd);

            ampNew.setUpdate(false);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }

        // remember that we got this from the dbase
        return (Amplitude) ampNew;
    }

    /**
     * Copy DataObjects out of a jdbc.Amp object into this Amplitude.
     */
    protected void parseTableRow(DataTableRow row) {
      Channel cha = ChannelTN.parseChannelFromDataTableRow(row, DataTableRowUtil.AUTHSUBSRC);
      // call setChannel to synch with master channel list
      //setChannelObj(cha); // aww 04/05/2005
      this.chan = cha; // aww 04/05/2005
      authority = (DataString) row.getDataObject(TableRowUnassocAmp.AUTH);
      source = (DataString) row.getDataObject(TableRowUnassocAmp.SUBSOURCE);
      ampid = (DataLong) row.getDataObject(TableRowUnassocAmp.AMPID);
      datetime = (DataDouble) row.getDataObject(TableRowUnassocAmp.DATETIME);
      //Kludge for NCEDC leap corrected data, to convert to nominal time -aww
      //DataSource.dbTimeBaseToNominal(datetime); // removed for leap 2008/01/24 -aww

      processingState = (DataString) row.getDataObject(TableRowUnassocAmp.RFLAG);
      phaseType = (DataString) row.getDataObject(TableRowUnassocAmp.IPHASE);
      value = (DataDouble) row.getDataObject(TableRowUnassocAmp.AMPLITUDE);
      // Set AmpType value
      String ampType = row.getDataObject(TableRowUnassocAmp.AMPTYPE).toString();
      type = AmpType.getInt(ampType);
      // if zero-to-peak = 1 , if peak-to-peak = 0
      String half = row.getDataObject(TableRowUnassocAmp.AMPMEAS).toString();
      if (half.equals("0")) halfAmp=false;
      // Set units
      String unitstr = row.getDataObject(TableRowUnassocAmp.UNITS).toString();
      units = Units.getInt(unitstr);
      uncertainty = (DataDouble) row.getDataObject(TableRowUnassocAmp.ERAMP);
      // IGNORING 'FLAGAMP', 'TAU'
      period = (DataDouble) row.getDataObject(TableRowUnassocAmp.PER);
      snr = (DataDouble) row.getDataObject(TableRowUnassocAmp.SNR);
      quality = (DataDouble) row.getDataObject(TableRowUnassocAmp.QUALITY);
      String clip = row.getDataObject(TableRowUnassocAmp.CFLAG).toString();
      if (clip.equalsIgnoreCase("OS")) { clipped =  ON_SCALE; }   //on scale
      else if (clip.equalsIgnoreCase("BN")) { clipped =  BELOW_NOISE; } //below noise
      else if (clip.equalsIgnoreCase("CL")) { clipped =  CLIPPED; } //clipped
      windowStart = (DataDouble) row.getDataObject(TableRowUnassocAmp.WSTART); // TableRowAmp... fix, use getEpoch(WSTART, 'UTC') -aww 2010/02/25
      windowDuration = (DataDouble) row.getDataObject(TableRowUnassocAmp.DURATION);
      // save TableRowUnassocAmp.LDDATE in timeStamp? -aww
      this.timeStamp = (DataDate) row.getDataObject(TableRowUnassocAmp.LDDATE); // date of first commit?
      fileid = (DataLong) row.getDataObject(TableRowUnassocAmp.FILEID);
      return;
    }

    /**
     * Copy contents of this amplitude into a jdbc.Amp row object
     */
    protected StnChlTableRow toTableRow() {
        // OVERRIDE IN SUBCLASS IF UnassocAmp TABLE STRUCTURE CHANGES!
        // Always make a new row, get unique key #
//        long newId = SeqIds.getNextSeq(DataSource.getConnection(), "AMPSEQ");
        long newId = SeqIds.getNextSeq(DataSource.getConnection(),
                        TableRowUnassocAmp.SEQUENCE_NAME);
        ampid.setValue(newId);

        StnChlTableRow aRow = createTableRow(newId);
        aRow.setUpdate(true); // set flag to enable processing

        setRowChannelId(aRow, getChannelObj());

        //Kludge for NCEDC leap corrected data, to convert to leap time -aww
        //DataDouble dt = (DataDouble) datetime.clone();
        //DataSource.nominalToDbTimeBase(dt);
        //aRow.setValue(TableRowUnassocAmp.DATETIME, dt); // removed for leap 2008/01/24 -aww
        // end of leap patch aww
        if (datetime.isValid()) aRow.setValue(TableRowUnassocAmp.DATETIME, datetime);

        // "rectify" - peak amp must be > 0.0
        aRow.setValue(TableRowUnassocAmp.AMPLITUDE, Math.abs(value.doubleValue()));
        aRow.setValue(TableRowUnassocAmp.AMPTYPE, AmpType.getString(type));

        // if zero-to-peak = 1 , if peak-to-peak = 0
        if (halfAmp) {
            aRow.setValue(TableRowUnassocAmp.AMPMEAS, ZERO_TO_PEAK);
        } else {
            aRow.setValue(TableRowUnassocAmp.AMPMEAS, PEAK_TO_PEAK);
        }

        aRow.setValue(TableRowUnassocAmp.UNITS, Units.getString(units));

        //if (authority.isValid())       aRow.setValue(TableRowUnassocAmp.AUTH, authority); // aww 01/11/2005
        aRow.setValue(TableRowUnassocAmp.AUTH, getClosestAuthorityString()); // aww 01/11/2005

        // the following can be null
        if (processingState.isValid()) aRow.setValue(TableRowUnassocAmp.RFLAG, processingState);
        if (source.isValid())          aRow.setValue(TableRowUnassocAmp.SUBSOURCE, source);
        if (phaseType.isValid())       aRow.setValue(TableRowUnassocAmp.IPHASE, phaseType);
        if (uncertainty.isValid())     aRow.setValue(TableRowUnassocAmp.ERAMP, uncertainty);
        if (period.isValid())          aRow.setValue(TableRowUnassocAmp.PER, period);
        if (snr.isValid())             aRow.setValue(TableRowUnassocAmp.SNR, snr);
        if (quality.isValid())         aRow.setValue(TableRowUnassocAmp.QUALITY, quality);

        // IGNORING 'FLAGAMP', 'TAU'
        if (clipped == ON_SCALE)         aRow.setValue(TableRowUnassocAmp.CFLAG, "OS");
        else if (clipped == BELOW_NOISE) aRow.setValue(TableRowUnassocAmp.CFLAG, "BN");
        else if (clipped == CLIPPED)     aRow.setValue(TableRowUnassocAmp.CFLAG, "CL");

        if (windowStart.isValid()) {
            // WSTART not a DATETIME column, Amp class overrides DataTableRow isColumnEpochSecsUTC(columnName) -aww 2010/02/26
            aRow.setValue(TableRowUnassocAmp.WSTART, windowStart);
        }
        if (windowDuration.isValid()) aRow.setValue(TableRowUnassocAmp.DURATION, windowDuration);

        if (debug) {
            System.out.println("DEBUG UnassocAmplitudeTN toTableRow(): "+aRow.toString());
        }

        return aRow;
    }

    /** Put the channel name attributes in the UnassocAmp DataTableRow */
    protected void setRowChannelId(StnChlTableRow aRow, Channel chan) {
        ChannelName cn = chan.getChannelName();
        // by default all fields are "" rather then null strings: see ChannelName
        aRow.setValue(TableRowUnassocAmp.STA, cn.getSta()); // this attribute can't be null
        String str = cn.getNet();
        if (str.length() > 0) aRow.setValue(TableRowUnassocAmp.NET, str);
        str = cn.getAuth();
        if (str.length() > 0) aRow.setValue(TableRowUnassocAmp.AUTH, str);
        str = cn.getSubsource();
        if (str.length() > 0) aRow.setValue(TableRowUnassocAmp.SUBSOURCE, str);
        str = cn.getChannel();
        if (str.length() > 0) aRow.setValue(TableRowUnassocAmp.CHANNEL, str);
        str = cn.getChannelsrc();
        if (str.length() > 0) aRow.setValue(TableRowUnassocAmp.CHANNELSRC, str);
        str = cn.getSeedchan();
        if (str.length() > 0) aRow.setValue(TableRowUnassocAmp.SEEDCHAN, str);
        str = cn.getLocation();
        if (str.length() > 0) aRow.setValue(TableRowUnassocAmp.LOCATION, str);
    }


    // since protected can access across package boundaries by Solution and Magnitude
    public boolean isFromDataSource() {
       return fromDbase;
    }

    /**
     * NEVER delete an amp. Never remove association with another NetMag.  If
     * you don't like it just don't assoc it with your new NetMag.
    */
    protected boolean dbaseDelete() {
        return true;
    }

    /**
     * Insert a new row in the dbase.
     * This DOES NOT write the association rows for the origin and the netmag.
     * The Solution and Magnitude object's when they are committed create these
     * association rows.
     */
    protected boolean dbaseInsert() {
        // if (fromDbase) return false;  // but isn't a change worthy of update?
        boolean status = true;
        StnChlTableRow aRow = toTableRow();
        aRow.setProcessing(DataTableRowStates.INSERT);

        // write arrival row
        status = (aRow.insertRow(DataSource.getConnection()) > 0);
        if (status) {                    // successful insert
          //Note: Solution, Magnitude commit() invoke reading commit() which write new assoc - aww 09/03
          //status = dbaseAssociate(); // (removed see above note) commit() invokes it.
          // now its "from" the dbase
          setUpdate(false); // mark as up-to-date (fromDbase=true)
        }
        return status;
    }

    /**
    * Returns true if this instance was not initialized from database data.
    * or hasChangedAttributes() == true.
    */
    public boolean hasChanged() {
        return ( !fromDbase || hasChangedAttributes());
    }
    /** Returns true if certain data member values were modified after initialization.
     * (value, datetime)
     * */
    public boolean hasChangedAttributes() {
        return (
            value.isUpdate() ||
            datetime.isUpdate()
            //|| quality.isUpdate() // aww, this may effect results someday, weights?
        );
    }

    /**
     * Set the isUpdate() flag for all data dbase members the given boolean value.
     * */
    protected void setUpdate(boolean tf) {
        fromDbase = !tf;
        value.setUpdate(tf);
        datetime.setUpdate(tf);
        //quality.setUpdate(tf); // need this if used by summary calc method, like weight - aww
    }

    /** Sets data member values to those of the input object */
    public boolean replace(Amplitude aAmp) {

        boolean status = super.replace(aAmp);
        if (aAmp instanceof UnassocAmplitudeTN) {
            UnassocAmplitudeTN uAmp = (UnassocAmplitudeTN) aAmp; 
            fromDbase = uAmp.fromDbase;
            ampid.setValue(uAmp.ampid);
            fileid.setValue(uAmp.fileid);
        }
        else if (aAmp instanceof AmplitudeTN) {
            AmplitudeTN uAmp = (AmplitudeTN) aAmp; 
            ampid.setValue(uAmp.ampid);
            fileid.setNull(true);
            fromDbase = false;
        }
        else {
            ampid.setNull(true);
            fileid.setNull(true);
            fromDbase = false;
        }

        return status;
    }

    /** Does a no-op, returns false. Map input Amplitude data to this Amplitude. */
    public boolean copySolutionDependentData(Amplitude aAmp) {
        return false;
    }

    /**Access to the JasiDbReader to allow generic SQL queries. */
    public JasiDataReaderIF getDataReader() {
        if (jasiDataReader == null) {
           //jasiDataReader = new JasiDbReader(this);
           jasiDataReader = new JasiDbReader();
           setDebug(JasiDatabasePropertyList.debugTN); 
        }
        return jasiDataReader;
    }

    // Clone the specific amplitude dependent data of this subtype.
    public Object clone() {
        UnassocAmplitudeTN ampTN = (UnassocAmplitudeTN) super.clone();
        //ampTN.orid        = (DataLong) this.orid.clone();
        //ampTN.magid       = (DataLong) this.magid.clone();
        ampTN.ampid       = (DataLong) this.ampid.clone();
        return ampTN;
    }

} // UnassocAmplitudeTN
