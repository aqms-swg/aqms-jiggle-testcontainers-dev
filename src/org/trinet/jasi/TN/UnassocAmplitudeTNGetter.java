package org.trinet.jasi.TN;

import org.trinet.jdbc.table.UnassocAmp;

import org.trinet.jasi.*;

import java.util.*;
import java.sql.*;

import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

/**
 * Return time delimited clumps of amps directly from a result set.
 */
public class UnassocAmplitudeTNGetter implements AutoCloseable {

  public static boolean debug = false;

  public static final int DefaultMaximumAmps = 4000;
  public static final double DefaultGapDelimiterSecs = 150;

  /** Size of gap (seconds) to delimit natural amp groups. */
  private double gapDelimiterSecs = DefaultGapDelimiterSecs;

  /** Maximun number of amps to allow in a group. <p>
   * Why this is here: theoretically the scheme of defining
   * amp groups with time gaps could be thwarted if something is producing amps at
   * a regular interval less than 'gapDelimiterSecs'. In this case a delimiting gap would never
   * exist and all amps, potentially enough to run you out of memory, would form a single group.
   */
  private int maximumAmps = DefaultMaximumAmps;

  private UnassocAmplitudeTN lastAmp = null;

  private int totalResultSetRows = 0;
  private ResultSetDb rsdb;
  private Statement sm = null;
  private Connection connection;
  private String ampSubsource = null;

  /** The SQL statement used to get amps from UnassocAmp. */
  // TIME ORDER SORT NEEDED BECAUSE FIRST AND LAST ROW IN GROUP DEFINE THE SPAN FOR CANDIDATE SOLUTIONLIST!
  // Note datetime can be null, but wstart column is NOT null - aww 2010/12/07
  private static String SQL = " Select "+ org.trinet.jdbc.table.UnassocAmp.QUALIFIED_COLUMN_NAMES + " from UnassocAmp";
  private static String SQL_ORDER =  " order by datetime desc, wstart desc, lddate desc";
  
  /** Will always default to DataSource.getNewConnection() */
  public UnassocAmplitudeTNGetter() {
    this(DefaultGapDelimiterSecs, DefaultMaximumAmps);
  }

  public UnassocAmplitudeTNGetter(int maxAmpsPerGroup) {
    this(DefaultGapDelimiterSecs, maxAmpsPerGroup);
  }

  public UnassocAmplitudeTNGetter(double gapDelimiterSeconds) {
    this(gapDelimiterSeconds, DefaultMaximumAmps);
  }

  /** If no connection specified will default to DataSource.getNewConnection().
   * Set delimiter gap window (seconds).  */
  public UnassocAmplitudeTNGetter(double seconds, int maxAmpsPerGroup) {
      setDelimiterGapWindow(seconds);
      setMaximumAmpsPerGroup(maxAmpsPerGroup);
      //initQuery(); // -aww removed 2009/08/31
  }

  /** Execute the query and return the result set which will be returned incrementally
   * by calls to getNextGroup(). */
  public boolean initialize() {

    sm = null;

    totalResultSetRows = 0;

    lastAmp = null;

    Connection conn = getConnection();
    if (conn == null) {
      System.err.println("ERROR UnassocAmplitudeTNGetter.initQuery(): getConnection() call returns null.");
      return false;
    }

    boolean status = true;
    String querySql = SQL;
    try {
      sm = conn.createStatement();
      if (ampSubsource != null) {
         querySql += " where subsource='" + ampSubsource + "'";
      }
      querySql += SQL_ORDER;
      if (debug) System.out.println("DEBUG UnassocAmplitudeTNGetter sql:\n" + querySql); // DEBUG
      rsdb = new ResultSetDb(ExecuteSQL.rowQuery(sm, querySql));

    } catch (SQLException ex) {
      status = false;
      System.err.println(" : rowQuery() SQLException");
      SQLExceptionHandler.prtSQLException(ex);
      try {
          closeConnection(); // new method added -aww 2010/04/23 
      }
      catch(Throwable ex2) { }
    }
    return status;
  }

  /** Connection must be one that will not be used for other things while this
   * class in in use. Therefore, we create a new one.*/
  private Connection getConnection() {
      try {  // close existing connection entities, trap exception -aww 2009/08/31
        if (connection != null) {
            connection.close();
            connection = null;
        }
        connection = DataSource.getNewConnect();
      }
      catch (Exception ex) {
        System.err.println(ex.getMessage());
      }
      return connection;
  }

  public void setAmpSubsource(String source) {
    ampSubsource = source;
  }

  public String getAmpSubsource() {
    return ampSubsource;
  }

  public void setDelimiterGapWindow(double seconds) {
    gapDelimiterSecs = seconds;
  }

  public double getDelimiterGapWindow() {
    return gapDelimiterSecs;
  }

  public void setMaximumAmpsPerGroup(int maximumAmpsPerGroup) {
    maximumAmps = maximumAmpsPerGroup;
  }

  public int getMaximumAmpsPerGroup() {
    return maximumAmps;
  }

  /** Return the next group of UnassocAmplitude objects as a AmpList.
   * The groups are seperated by a time gas set by setDelimiterGapWindow(secs).
   * Returns null if no groups remain.*/
  public AmpList getNextGroup()  {

    if (rsdb == null) return null;  // no result set

    DataTableRow tableRow = new UnassocAmp();
    double lastTime = Double.MIN_VALUE;
    int knt = 0;

    AmpList list = AmpList.create();

    if (lastAmp != null) {
        list.add(lastAmp, false);  // add last amp from previous call that signaled gap
        if (debug) System.out.println("DEBUG UnassocAmpTNGetter added last amp: " + lastAmp.toString());
        lastAmp = null; // reset here, else this stale leftover from gap break would get added to a subsequent group 
    }

    boolean status = true;
    ResultSet rs = rsdb.getResultSet();
    try {

      // suck'em in
      while ( ++knt <= getMaximumAmpsPerGroup())  {

        if (! rs.next() ) break; // no more data

        // get one table row
        tableRow = (DataTableRow) tableRow.parseOneRow(rsdb, 0);
        totalResultSetRows++;

        // Parse the Amp columns
        UnassocAmplitudeTN newAmp = new UnassocAmplitudeTN();
        newAmp.parseTableRow(tableRow);

        // test for gap
        if (lastTime == Double.MIN_VALUE) lastTime = newAmp.getTime(); // first time thru

        // assumes descending time ordered
        if (Math.abs(lastTime - newAmp.getTime()) > this.getDelimiterGapWindow()) {
          // GAP -- don't add it to the list but remember it for lasttime.
          lastTime = newAmp.getTime();
          lastAmp = newAmp; // save for next group
          if (debug) System.out.println("DEBUG UnassocAmpTNGetter, group break, found time gap at last amp read");
          break;            // jump out of the loop
        } else {
          list.add(newAmp, false); // add to current group
          if (debug) System.out.println("DEBUG UnassocAmpTNGetter added new amp: " + newAmp.toString());
        }
      }
    } catch (SQLException ex) {
      System.err.println("UnassocAmp table read SQLException");
      SQLExceptionHandler.prtSQLException(ex);
      status = false;
    }
    // DON'T close the result set here - there may be more groups DDG
    return (status) ? list : null;
  }

  public void closeConnection() { // new method added -aww 2010/04/23 
      try { // close jdbc resources
        if (sm != null) {
            if (debug) System.out.println("DEBUG UnassocAmpGetterTN closeConnection totalResultSetRows on close: " + totalResultSetRows);
            sm.close();
            sm = null;
        }
        if (connection != null) {
            connection.close();
            connection = null;
        }
      }
      catch (SQLException ex) {
      }
  }

  // Force closure of sql resources
  @Override
  public void close() {
      closeConnection(); // new method added -aww 2010/04/23 
  }

/*
  public static void main(String[] args) {
    TestDataSource.create("serverma");
    UnassocAmplitudeTNGetter ampGetter = new UnassocAmplitudeTNGetter(150, 300);
    AmpList list;
    while (true) {
      list = ampGetter.getNextGroup();
      if (list == null || list.isEmpty()) break;
      System.out.println(list.size() + " -------------");
//      list.dump();
    }
  }
*/
}
