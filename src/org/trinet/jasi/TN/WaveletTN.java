package org.trinet.jasi.TN;

import java.io.*;
import java.sql.*;
import java.util.*;

import org.trinet.jasi.*;
import org.trinet.jasi.seed.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.DataTableRowUtil;
import org.trinet.util.*;

/**
 * Represents the waveform time series for one channel for a time window (TimeSpan).
 * Because a time window may contain gaps or no data, the TimeSeries may contain
 * from zero to 'n' WFSegments. Each WFSegment is a CONTIGUOUS time series.<p>
 *
 * Fires ChangeEvents when the time-series is loaded so that time-series can be
 * loaded asynchronously (in background thread) then notify a graphics object when
 * loading is complete.<p>
 *
 * Note that a waveform may be "empty", that is it contain no time series. This may be
 * true before the loader thread has retreived the time-series. Check this
 * condition using the boolean method hasTimeSeries().<p>
 *
 * This class also knows how to read the waveforms from various data sources.
 * @see: loadTimeSeries().
 * <p>
 */
/*
1/15/02 Integrated AWW's DbBlobReader
*/
public class WaveletTN extends Wavelet {
    //
    // public static int MAXROWS = 200;
    //
    // WAVEFILE is a database stored package of user defined routines

    private static final String SelectWaveformColumns =
      //"SELECT w.WFID,w.NET,w.STA,w.CHANNEL,w.SEEDCHAN,w.LOCATION,w.ARCHIVE,w.DATETIME_ON,w.DATETIME_OFF," +
      "SELECT w.WFID,w.NET,w.STA,w.SEEDCHAN,w.LOCATION,w.ARCHIVE,"+
      "TRUETIME.getEpoch(w.DATETIME_ON,'UTC'),TRUETIME.getEpoch(w.DATETIME_OFF,'UTC')," +
      "w.SAMPRATE,w.WAVETYPE,w.NBYTES,w.TRACEOFF,w.STATUS,w.WAVE_FMT,w.FORMAT_ID"; 
      //"w.SAMPRATE,w.WAVETYPE,w.NBYTES,w.TRACEOFF,w.STATUS,w.WAVE_FMT,w.FORMAT_ID,w.WORDORDER"; // option to add wordorder?

    // use outer join to return Waveform rows that have no AssocWaE row
    /* Jiggle property "waverootsCopy" sets static int Wavelet.wcopy  which is used as arg in query function call in methods below -aww 2010/02/03 
      //"SELECT "+ org.trinet.jdbc.table.Waveform.QUALIFIED_COLUMN_NAMES+
    private static final String SelectJoinString = SelectWaveformColumns +
                        ",AssocWaE.evid,WAVEFILE.path(Waveform.wfid),WAVEFILE.dfile(Waveform.wfid)"+
                        " FROM Waveform, AssocWaE WHERE Waveform.wfid=AssocWaE.wfid(+)";
    */


    public WaveletTN() {}

    /*
    public boolean makeRequest(long evid, String auth, String staAuth, String archiveSrc, double start, double end) {

        if (evid <= 0l || getChannelObj() == null) {
            System.err.println("ERROR: WaveletTN makeRequest invalid evid: " + evid + " channel: " + getChannelObj());
            return false; 
        }

        CallableStatement cs = null;
        int result = 0;
        try {
            Connection conn = DataSource.getConnection();
            if ( conn.isClosed() ) {  // check that valid connection exists
                System.err.println("ERROR: WaveletTN makeRequest connection is closed");
                return false;
            }

            ChannelName cn = getChannelObj().getChannelName();

            //
            String sql = "{ ?=call RCG.make_t_request(?,?,?,?,?,?,?,?,?,?,?,?) }";
            cs = conn.prepareCall(sql);
            cs.registerOutParameter(1, java.sql.Types.INTEGER);
            cs.setLong(2, evid);
            cs.setString(3, auth);
            cs.setString(4, archiveSrc)
            cs.setString(5, staAuth)
            cs.setString(6, cn.getNet());
            cs.setString(7, cn.getSta());
            cs.setString(8, cn.getSeedchan());
            cs.setString(9, cn.getLocation());
            cs.setString(10, cn.getChannel());
            cs.setDouble(11, start);
            cs.setDouble(12, end);
            cs.setInt(13, 0); // 0 = no commit; 1= commit
            //
            // NOTE: RCID=REQSEQ.NEXTVAL, RequestType='T', PRIORITY=1 would be set inside the called stored function 
            // property for value STAAUTH passed into DB FUNCTION like "SCEDC"
            //
            cs.execute();
            result = cs.getInt(1);
            if (JasiDatabasePropertyList.debugSQL) 
                System.out.println("WaveletTN  " + "{"+result+"= call requestcard.make_t_request("+evid+",'"+
                                auth+"','"+archiveSrc+"','"+staAuth+"','"+
                                cn.getNet()+"','"+cn.getSta()+"','"+cn.getSeedchan()+"','"+cn.getLocation()+"','"+cn.getChannel()+"','"+
                                TimeSpan.timeToString(start)+"','"+
                                TimeSpan.timeToString(end)+"',0)}");
        }
        catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }
        finally {
          try {
            if (cs != null) cs.close();
          }
          catch (SQLException ex) {}
        }
        return (result>=0);
    }
    */


/**
 * Get an array of waveform headers given an SQL query.
 */
    private static Collection getBySQL(String sql) {
        return getBySQL(DataSource.getConnection(), sql);
    }

/**
 * Get an array of waveform headers given an SQL query.
 */
    private static Collection getBySQL(Connection conn, String sql)  {

        ArrayList wfList = new ArrayList();
        Statement sm = null;
        ResultSet rs = null;
        try {
            if ( conn.isClosed() ) {  // check that valid connection exists
                System.err.println("ERROR: WaveletTN Connection is closed");
                return null;
            }

            sm = conn.createStatement();

            // ResultSetDb rsdb = new ResultSetDb(org.trinet.jdbc.table.ExecuteSQL.rowQuery(sm, sql));
            rs = org.trinet.jdbc.table.ExecuteSQL.rowQuery(sm, sql);
            if (rs != null) {
              while ( rs.next() ) { // parse and add rows one-at-a-time
                wfList.add(parseResultSet(rs));
              }
            }
        }
        catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, sm);
          Utils.closeQuietly(rs, sm);
        }

        // convert vector to array
        //  if (wfList.size() == 0) return null;

        return (Collection) wfList;
    }

    /* Parse a resultset row that contains the results of a join + some stored procedures //
    static Wavelet parseResultSet(ResultSet rs) {
        // a jdbc.table Waveform
        org.trinet.jdbc.table.Waveform wfrow = new org.trinet.jdbc.table.Waveform();

        // a jsai Waveform
        WaveletTN wf = new WaveletTN();

        int offset = 0;

        // parse the first part of the return that is just a Waveform table row
        ResultSetDb rsdb = new ResultSetDb(rs);
        wfrow = (org.trinet.jdbc.table.Waveform) wfrow.parseOneRow(rsdb, offset);

        // transfer row contents to this Waveform
        wf.parseWaveformRow(wfrow);

        // Now, pick up the "extra" columns specified by the SQL query.
        offset += wfrow.MAX_FIELDS;

        try {
            // <AssocWaE.evid>
            //  event ID for which this event was saved
            int oid =  rs.getInt(++offset);   // get as int
            String str = String.valueOf(oid);
            wf.savingEvent.setValue(str);   // save as string

            // <WAVEFILE.path(AssocWaE.evid)>
            // time-series data file PATH from database package stored function
            wf.path  = rs.getString(++offset);

            // <WAVEFILE.dfile(Waveform.wfid)>
            // time-series data filename from database package stored function
            wf.filename = rs.getString(++offset);

            // Last column in query added -aww 2010/02/04
            wf.dbWcopy = rs.getInt(++offset); // the static int value passed into query
        }
        catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }

        // keep a copy of the row
     // wf.setWaveformRow(wfrow);

        return wf;
    }
    */

    private static final Wavelet parseResultSet(ResultSet rs) {

        // a jasi Waveform
        WaveletTN wf = new WaveletTN();

        int offset = 1;

        try {

            //wfid
            wf.wfid.setValue(rs.getLong(offset++));

            //Channel name
            Channel chan = wf.getChannelObj();
            chan.setNet(rs.getString(offset++));
            chan.setSta(rs.getString(offset++));
            chan.setSeedchan(rs.getString(offset++));
            chan.setLocation(rs.getString(offset++));
            //waveArchive
            wf.waveArchive.setValue(rs.getString(offset++));

            // wfr query string should have truetime.getEpoch(DATETIME_ON,'UTC') truetime.getEpoch(DATETIME_OFF,'UTC') 
            //timeStart
            double dd = rs.getDouble(offset++);
            if (!rs.wasNull()) wf.timeStart.setValue(dd);
            //timeEnd
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) wf.timeEnd.setValue(dd);

            //samplesPerSecond w.SAMPRATE 
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) wf.samplesPerSecond.setValue(dd);

            //waveType w.WAVETYPE
            wf.waveType.setValue(rs.getString(offset++));

            //byteCount w.NBYTES
            wf.byteCount = rs.getInt(offset++);

            //fileOffset w.TRACEOFF
            wf.fileOffset = rs.getInt(offset++);

            //waveStatus w.STATUS 
            wf.waveStatus.setValue(rs.getString(offset++));

            //format w.WAVE_FMT
            long lval = rs.getLong(offset++);
            if (!rs.wasNull()) wf.format.setValue(lval);

            //encoding w.FORMAT_ID 
            lval = rs.getLong(offset++);
            if (!rs.wasNull()) wf.encoding.setValue(lval);

            //encoding w.WORDORDER if added to query
            //lval = rs.getLong(offset++);
            //if (!rs.wasNull()) wf.wordOrder.setValue(lval);

            // The actual units of a waveform is NOT IN THE DATABASE!
            // so we assume its counts, you need the channel gain to get to real units
            wf.ampUnits = Units.COUNTS;

            // <AssocWaE.evid> for which this waveform was saved
            wf.savingEvent.setValue(String.valueOf(rs.getLong(offset++)));

            // <WAVEFILE.path(AssocWaE.evid)>
            // time-series data file PATH from database package stored function
            wf.path = rs.getString(offset++);

            // <WAVEFILE.dfile(Waveform.wfid)>
            // time-series data filename from database package stored function
            wf.filename = rs.getString(offset++);

            // Last column in query: constant passed by query -aww 2010/02/04
            // archive waveroots wcopy
            wf.dbWcopy = rs.getInt(offset++); 
        }
        catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }

        return wf;
    }

    // Transfer the needed DataObject's from the Waveform row to this Waveform.
    // Note that this gets DataObjects not values.
    // Note the use of 'wfr.EVID' for static constants rather then
    //    'org.trinet.jdbc.table.Waveform.EVID' for brevity.
    private Wavelet parseWaveformRow(org.trinet.jdbc.table.Waveform wfr) {

        wfid = (DataLong) wfr.getDataObject(wfr.WFID);

        // suck out the Channel description
        Channel cha = ChannelTN.parseChannelFromDataTableRow(wfr, DataTableRowUtil.AUTHSUBSRC);
        // call setChannel to synch with master channel list
        //setChannelObj(cha); // aww 04/05/2005
        this.chan = cha; // aww 04/05/2005

         // wfr query string should have truetime.getEpoch(DATETIME_ON,'UTC') truetime.getEpoch(DATETIME_OFF,'UTC') 
        timeStart   = (DataDouble) wfr.getDataObject(wfr.DATETIME_ON);
        timeEnd   = (DataDouble) wfr.getDataObject(wfr.DATETIME_OFF);
        //Kludge for NCEDC leap corrected data, to convert to nominal time -aww
        //DataSource.dbTimeBaseToNominal(timeStart);
        //DataSource.dbTimeBaseToNominal(timeEnd);
        // end of patch aww // removed 2008/01/24 for leap secs - aww

        samplesPerSecond=(DataDouble) wfr.getDataObject(wfr.SAMPRATE);

        waveArchive = (DataString) wfr.getDataObject(wfr.ARCHIVE);
        //dataMode    = (DataString) wfr.getDataObject(wfr.WAVETYPE);
        waveType = (DataString) wfr.getDataObject(wfr.WAVETYPE);
        waveStatus = (DataString) wfr.getDataObject(wfr.STATUS);

        format  = (DataLong) wfr.getDataObject(wfr.WAVE_FMT);
        encoding    = (DataLong) wfr.getDataObject(wfr.FORMAT_ID);
        //wordOrder = (DataLong) wfr.getDataObject(wfr.WORDORDER); // if added to query

        // file access stuff
        fileOffset = (int)wfr.getDataObject(wfr.TRACEOFF).longValue(); // (int) (wfr.getDataObject(wfr.FOFF).longValue() 
        byteCount  = (int) wfr.getDataObject(wfr.NBYTES).longValue(); // ?? TRACELEN ??
        // ?? what about TRACELEN attribute, which is greater NBYTES or TRACELEN, or are they always equal? -aww
        // is TRACELEN sum of NBYTES from different files, or is it a subset of NBYTES? -aww

        // the actual units of a waveform is NOT IN THE DATABASE!
        // so we assume its counts, you need the channel gain to get to real units
        ampUnits = Units.COUNTS;

        return this;

    }

    private static final String psWaverootStr = 
         "SELECT DISTINCT wcopy FROM WAVEROOTS WHERE ((net=?) OR (net='*'))" +
                 " AND truetime.putEpoch(?,'UTC') BETWEEN datetime_on AND datetime_off" +
                 " AND (archive=?) AND (wavetype=?) AND (status=?) ORDER BY 1";

    public static int [] getWaverootCopies(String net, String waveArchive, String waveType, String waveStatus, double datetime) {
        //  String sql = "select distinct wcopy from WAVEROOTS where net in ('"+net+"','*')" +
        //               " and truetime.putEpoch("+datetime+",'UTC') between datetime_on and datetime_off" +
        //               " and archive = '"+waveArchive+"' and wavetype = '"+waveType+"' and status = '"+waveStatus+"' order by 1";
        return getWaverootCopies(psWaverootStr, net, waveArchive, waveType, waveStatus, datetime);
    }

    public int [] getWaverootCopies() {
        //String sql = "select distinct wcopy from WAVEROOTS where net in ('"+getChannelObj().getNet()+"','*')" +
        //             " and truetime.putEpoch("+getStart().doubleValue()+",'UTC') between datetime_on and datetime_off" +
        //             " and archive = "+waveArchive.toStringSQL()+
        //             " and wavetype = "+waveType.toStringSQL()+
        //             " and status = "+waveStatus.toStringSQL()+
        //             " order by 1";
        return getWaverootCopies(getChannelObj().getNet(),waveArchive.toString(),
                waveType.toString(),waveStatus.toString(),getStart().doubleValue());
    }

    private static int [] getWaverootCopies(String sql, String net, String waveArchive, String waveType, String waveStatus, double datetime) {
        if (net == null || waveArchive == null || waveType == null || waveStatus == null || Double.isNaN(datetime) ) {
            return new int[0];
        }
        PreparedStatement sm = null;
        ResultSet rs = null;
        ArrayList aList = new ArrayList();

        try {
            Connection conn = DataSource.getConnection();
            if ( conn.isClosed() ) {  // check that valid connection exists
                System.err.println("ERROR: WaveletTN Connection is closed");
                return null;
            }

            sm = conn.prepareStatement(sql);
            int offset = 1;
            sm.setString(offset++,net);
            sm.setDouble(offset++, datetime);
            sm.setString(offset++,waveArchive);
            sm.setString(offset++,waveType);
            sm.setString(offset++,waveStatus);
            rs = sm.executeQuery();
            while (rs.next()) {
                aList.add(Integer.valueOf(rs.getInt(1)));
            }
        } catch (SQLException ex) {
            System.out.println ("SQL: "+sql);
            System.err.println(ex);
            ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, sm);
          Utils.closeQuietly(sm);
        }
        Integer [] iarray = (Integer[]) aList.toArray(new Integer[aList.size()]);
        int [] iout = new int[iarray.length];
        for (int idx = 0; idx<iarray.length; idx++) {
            iout[idx] = iarray[idx].intValue();
        }
        return iout;
    }

    public String [] getWaverootPaths() {
        return getWaverootPaths(getChannelObj().getNet(),waveArchive.toString(),
                waveType.toString(),waveStatus.toString(), getStart().doubleValue());
    }

    private static String [] getWaverootPaths(String net, String waveArchive, String waveType, String waveStatus, double datetime) {
        String sql =  "SELECT DISTINCT fileroot, wcopy FROM WAVEROOTS WHERE ((net=?) OR (net='*'))" +
                 " AND truetime.putEpoch(?,'UTC') BETWEEN datetime_on AND datetime_off" +
                 " AND (archive=?) AND (wavetype=?) AND (status=?) ORDER BY wcopy";
        PreparedStatement sm = null;
        ResultSet rs = null;
        ArrayList aList = new ArrayList();
        try {
            Connection conn = DataSource.getConnection();
            if ( conn.isClosed() ) {  // check that valid connection exists
                System.err.println("ERROR: WaveletTN Connection is closed");
                return null;
            }

            sm = conn.prepareStatement(sql);
            int offset = 1;
            sm.setString(offset++,net);
            sm.setDouble(offset++,datetime);
            sm.setString(offset++,waveArchive);
            sm.setString(offset++,waveType);
            sm.setString(offset++,waveStatus);
            rs = sm.executeQuery();
            while (rs.next()) {
                aList.add(rs.getString(1));
            }
        } catch (SQLException ex) {
            System.out.println ("SQL: "+sql);
            System.err.println(ex);
            ex.printStackTrace();
        }
        finally {
          JasiDatabasePropertyList.debugSQL(sql, sm);
          Utils.closeQuietly(sm);
        }
        String [] sarray = (String[]) aList.toArray(new String[aList.size()]);
        return sarray;
    }
/**
 * Get one waveform from the DataSource by Waveform record ID #
 * Returns empty Waveform object if no match is found.
 */
    public Wavelet getByWaveformId(long wfid) {
        StringBuffer sb = new StringBuffer(1024);
        //sb.append("SELECT ").append(org.trinet.jdbc.table.Waveform.QUALIFIED_COLUMN_NAMES);
        //sb.append(", AssocWaE.evid, WAVEFILE.path(Waveform.wfid, ").append(Wavelet.wcopy).append("), WAVEFILE.dfile(Waveform.wfid)");
        //sb.append(",").append(Wavelet.wcopy);  //in query string, value parsed later 
        //sb.append(" FROM Waveform, AssocWaE WHERE Waveform.wfid = AssocWaE.wfid (+)");
        //sb.append(" and Waveform.wfid = ").append(wfid);
        //ArrayList wfList = (ArrayList) getBySQL(sb.toString());

        sb.append(SelectWaveformColumns);
        sb.append(",a.EVID, WAVEFILE.path(w.wfid, ");
        sb.append(Wavelet.wcopy).append("),WAVEFILE.dfile(w.wfid)");
        sb.append(",").append(Wavelet.wcopy);  //in query string, value parsed later 
        sb.append(" FROM Waveform w, AssocWaE a WHERE (w.wfid=a.wfid(+))");
        sb.append(" AND (w.wfid=?)");

        PreparedStatement ps = null; 
        ResultSet rs = null; 
        ArrayList wfList = new ArrayList();
        final String sql = sb.toString();
        try {
            Connection conn = DataSource.getConnection();
            if ( conn.isClosed() ) {  // check that valid connection exists
                System.err.println("ERROR: WaveletTN Connection is closed");
                return null;
            }

            ps = conn.prepareStatement(sql);
            int offset = 1;
            ps.setLong(offset++, wfid);
            rs = ps.executeQuery();
            if (rs != null) {
              while ( rs.next() ) { // parse and add rows one-at-a-time
                wfList.add(parseResultSet(rs));
              }
            }
        }
        catch (SQLException ex) {
            System.out.println("SQL: " + sb.toString());
            ex.printStackTrace();
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, ps);
            Utils.closeQuietly(rs, ps);
        }

        if (wfList == null || wfList.size() == 0) {
          return Wavelet.create();
        } else {
          return (Wavelet) wfList.get(0);  // only one
        }

    }

/**
 * Get an array of waveforms from the DataSource by solution ID #
 */
    public Collection getBySolution(long evid) {
        StringBuffer sb = new StringBuffer(1024);
        // Changed to NO outer join, AssocWaE data row must exist -aww 08/18/2006
        //sb.append("SELECT ").append(org.trinet.jdbc.table.Waveform.QUALIFIED_COLUMN_NAMES);
        //sb.append(", AssocWaE.evid, WAVEFILE.path(Waveform.wfid,").append(Wavelet.wcopy).append("), WAVEFILE.dfile(Waveform.wfid)");
        //sb.append(",").append(Wavelet.wcopy);  //in query string, value parsed later 
        //sb.append(" FROM Waveform, AssocWaE WHERE Waveform.wfid = AssocWaE.wfid");
        //sb.append(" and AssocWaE.evid = ").append(id);
        //sb.append(" order by Waveform.net,Waveform.sta,Waveform.seedchan,Waveform.location,Waveform.datetime_on");
        //return getBySQL(sb.toString());
        sb.append(SelectWaveformColumns);
        sb.append(",a.EVID,WAVEFILE.path(w.wfid,");
        sb.append(Wavelet.wcopy).append("),WAVEFILE.dfile(w.wfid)");
        sb.append(",").append(Wavelet.wcopy);  //in query string, value parsed later 
        sb.append(" FROM Waveform w, AssocWaE a WHERE (w.wfid=a.wfid)");
        sb.append(" AND (a.evid=?)");
        sb.append(" order by w.net,w.sta,w.seedchan,w.location,w.datetime_on");

        PreparedStatement ps = null; 
        ResultSet rs = null; 
        ArrayList wfList = new ArrayList();
        final String sql = sb.toString();
        try {
            Connection conn = DataSource.getConnection();
            if ( conn.isClosed() ) {  // check that valid connection exists
                System.err.println("ERROR: WaveletTN Connection is closed");
                return null;
            }

            ps = conn.prepareStatement(sql);
            ps.setLong(1, evid);
            rs = ps.executeQuery();

            if (rs != null) {
              while ( rs.next() ) { // parse and add rows one-at-a-time
                wfList.add(parseResultSet(rs));
              }
            }
        }
        catch (SQLException ex) {
            System.out.println("SQL: " + sb.toString());
            ex.printStackTrace();
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, ps);
            Utils.closeQuietly(rs, ps);
        }
        return wfList;
    }

/**
 * Get an array of waveforms from the DataSource by solution ID #
 */
    public Collection getByChannel(Channelable ch, long evid) {
        if (ch == null || evid <=0l) return null;
        Channel chan = ch.getChannelObj();  

        StringBuffer sb = new StringBuffer(1024);
        //sb.append("SELECT ").append(org.trinet.jdbc.table.Waveform.QUALIFIED_COLUMN_NAMES);
        //sb.append(", AssocWaE.evid, WAVEFILE.path(Waveform.wfid, ").append(Wavelet.wcopy).append("), WAVEFILE.dfile(Waveform.wfid)");
        //sb.append(",").append(Wavelet.wcopy);  //in query string, value parsed later 
        //sb.append(" FROM Waveform, AssocWaE WHERE Waveform.wfid = AssocWaE.wfid (+)");
        //sb.append(" and AssocWaE.evid = ").append(evid);
        //sb.append(" and Waveform.net = ");
        //sb.append(chan.getNet());
        //sb.append(" and Waveform.sta = ");
        //sb.append(chan.getSta());
        //sb.append(" and Waveform.seedchan = ");
        //sb.append(chan.getSeedchan());
        //sb.append(" and Waveform.location = ");
        //sb.append(chan.getLocation());
        // return getBySQL(sb.toString());

        sb.append(SelectWaveformColumns);
        sb.append(",a.EVID,WAVEFILE.path(w.wfid, ");
        sb.append(Wavelet.wcopy).append("),WAVEFILE.dfile(w.wfid)");
        sb.append(",").append(Wavelet.wcopy);  //in query string, value parsed later 
        sb.append(" FROM Waveform w, AssocWaE a WHERE (w.wfid=a.wfid)");
        sb.append(" and (a.evid=?)");
        sb.append(" and (w.net=?)");
        sb.append(" and (w.sta=?)");
        sb.append(" and (w.seedchan=?)");
        sb.append(" and (w.location=?)");
        sb.append(" order by w.datetime_on");

        PreparedStatement ps = null; 
        ResultSet rs = null; 
        ArrayList wfList = new ArrayList();
        final String sql = sb.toString();
        try {
            Connection conn = DataSource.getConnection();
            if ( conn.isClosed() ) {  // check that valid connection exists
                System.err.println("ERROR: WaveletTN Connection is closed");
                return null;
            }

            ps = conn.prepareStatement(sql);
            int offset = 1;
            ps.setLong(offset++, evid);
            ps.setString(offset++, chan.getNet());
            ps.setString(offset++, chan.getSta());
            ps.setString(offset++, chan.getSeedchan());
            ps.setString(offset++, chan.getLocation());
            rs = ps.executeQuery();
            if (rs != null) {
              while ( rs.next() ) { // parse and add rows one-at-a-time
                wfList.add(parseResultSet(rs));
              }
            }
        }
        catch (SQLException ex) {
            System.out.println("SQL: " + sb.toString());
            ex.printStackTrace();
        }
        finally {
            JasiDatabasePropertyList.debugSQL(sql, ps);
            Utils.closeQuietly(rs, ps);
        }
        return wfList;
    }

    /** Count of distinct Channel descriptions having Waveform ids associated with the input event id. */
    public int getCountBySolution(long evid) {
        StringBuffer sb = new StringBuffer(1024);
        sb.append("SELECT DISTINCT w.net,w.sta,w.seedchan,w.location FROM Waveform w, AssocWaE a ");
        sb.append("WHERE a.evid = ").append(evid).append(" AND w.wfid = a.wfid");
        return JasiDbReader.getCountBySQL(DataSource.getConnection(), sb.toString());
    }

/*
    // Create AssocWaE record for existing WaveForm and pointing at this Solution.
    // The underlying stored procedure commits, so you don't have to.
    public boolean commit(Solution sol) {
        StringBuffer sb = new StringBuffer(80);
        //sb.append("call JASI.AssociateWaveform(");
        sb.append("{ ? = call EPREF.AssociateWaveform("); // as function
        sb.append(id.longValue());
        sb.append(", ");
        sb.append(sol.id.longValue());
        sb.append(") }");
        //sb.append(")");
        return doCallableSQL(sb.toString());

    }

    //  Execute the sql string, return false on error
    protected boolean doSql(String sql) {
        boolean status = true;
        Statement sm = null;
        try {
            sm = DataSource.getConnection().createStatement();
            sm.execute(sql);
        } catch (SQLException ex) {
            System.out.println ("SQL: "+sql);
            System.err.println(ex);
            ex.printStackTrace();
            status = false;
        }
        finally {
          try {
            if (sm != null) sm.close();
          }
          catch(SQLException ex) {}
        }
        return status;
    }

    // Execute the sql string, return false on error. 
    private static boolean doCallableSQL(String sql) {
        boolean status = true;
        CallableStatement sm = null;
        try {
            sm = DataSource.getConnection().prepareCall(sql);
            sm.registerOutParameter(1, java.sql.Types.INTEGER);
            sm.executeUpdate();
            status  = (sm.getInt(1) > 0);
        } catch (SQLException ex) {
            status = false;
            System.err.println ("WaveletTN doCallableSQL:\n"+sql);
            System.err.println(ex);
            ex.printStackTrace();
        }
        finally {
          try {
            if (sm != null) sm.close();
          }
          catch (SQLException ex) { }
        }
        return status;
    }
*/
    /**
     * For now, just returns String value of the file path.
     * To get file root from a database query of WAVEROOTS table would require
     * creating class instance archive, wavetype, status, wcopy data members
     */
    public String getFileRoot() {

        return path;

        // Can query db stored procedure only after creating instance wave archive,type,and status data members !
        /*
        String root = "";
        if (id.isNull()) return null;

        // get the data directory root from the data base package stored function
        String sql = "select WAVEFILE.root(?,?,?,?,?,?,?) from dual";
        PreparedStatement sm = null;
        ResultSet rs = null;
        try {
            sm = DataSource.getConnection().prepareStatement(sql);

            // Set values to match WAVEROOTS columns
            sm.setString(1, getChannelObj().getNet());
            //sm.setString(2, waveArchive); // new field to be parsed from WAVEFORM table ResultSet for example "SCEDC"
            //sm.setString(3, waveType) // new field to be parsed from WAVEFORM table ResultSet for example "T"
            //sm.setString(4, waveStatus); // new field to be parsed from WAVEFORM table ResultSet for example "T"
            sm.setInt(5, format.intValue());
            sm.setDouble(6, timeStart.doubleValue());
            // Wavelet.wcopy default =1, but no way to know this client must set value 
            sm.setInt(7, Wavelet.wcopy);  // or create a new wcopy instance  field to be parsed from ResultSet for example "1" ?

            rs = sm.executeQuery(sql);
            if (rs.next()) {
              root  = rs.getString(1);
            }
        }
        catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
        }
        finally {
          try {
            if (rs != null) rs.close();
            if (sm != null) sm.close();
          }
          catch(SQLException ex) {}
        }
        return root;
        */
    }

    /**
     * Return the waveform file root for a particular event id.
     * Access to this path is tested
     * to determine if files can be read locally.
     */    
    public String getFileRoot(long eventId) {

      String root = "";

      // get the data directory root from the database package stored function
      PreparedStatement sm = null;
      ResultSet rs = null;
      final String sql =  "select WAVEFILE.rootByEvid(?,?) from dual";
      try {
        sm = DataSource.getConnection().prepareStatement(sql);
        sm.setLong(1, eventId);
        sm.setInt(2, Wavelet.wcopy);
        rs = sm.executeQuery();
        if (rs.next()) root = rs.getString(1);
      }
      catch (SQLException ex) {
        System.err.println(ex);
        ex.printStackTrace();
      }
      finally {
        JasiDatabasePropertyList.debugSQL(sql, sm);
        Utils.closeQuietly(rs, sm);
      }
      return root;
    }

/**
 * Load the time series data for this Waveform. This is done as a separate step
 * because it is often more efficient to only load selected waveforms after
 * you have examined or sorted the Waveform objects by their header info.<p>
 *
 * The waveform is collapsed into the minimum number of WFSegments possible. More
 * then one WFSegment would only be necessary if there are time tears. The start-stop
 * times of the original data packets is preserved in the packetSpans array.<p>
 *
 * Fires ChangeEvents when the time-series is loaded so that time-series can be
 * loaded asynchronously (in background thread) then notify a graphics object when
 * loading is complete.<p>
 */
// Should method be synchronized so only one class will try to load waveform at a time?
    public boolean loadTimeSeries() {
       return loadTimeSeries(getStart().doubleValue(), getEnd().doubleValue());
    }

    /** Load timeseries for time span bounded by input start and end time values in true epoch seconds.*/
    public boolean loadTimeSeries(double startTime, double endTime) {

     // Try to prevent two threads trying to load from the server at the same time - aww
      synchronized (this) {  // 2005/08/12 -aww
          if (hasTimeSeries() && (getStart().doubleValue() == startTime) &&
           (getEnd().doubleValue() == endTime) ) return true;
     // Note that synchronizing the Method does NOT provide thread safety because
     // other threads could still modify the Waveform attributes via other methods.
         if (format.intValue() == SEED_FORMAT) { // only SEED is currently implemented
            // read data from Seed file
            //int segCount = 
             SeedReader.getData(this, startTime, endTime);
            //if (JasiDatabasePropertyList.debugSQL) System.out.println(" WaveletTN loadTimeSeries returned segment count: " + segCount);
            // came up empty
            if (!hasTimeSeries()) return false;
            // set max, min, bias values
            scanForAmps();
            scanForBias();
            // Added logic below to finesse Waveform row having samplerate undefined -aww 12/14/2006
            double rate = getSampleRate();
            if (rate <= 0. && segList.size() > 0) {
               rate = 1./((WFSegment)segList.get(0)).getSampleInterval();
            }
            setSampleRate(rate);
            // Notify any listeners that Waveform state has changed
            // (changeListener logic is in jasiObject())
            fireStateChanged();
            return true;    // success
        }
        return false;
      }
    }

    /**
     * Override Waveform.filesAreLocal() to always return false so dbase access of waveforms
     * will be used.
     */
    public boolean filesAreLocal() {
        return false;
    }

/*
  static final class Tester {
    public static void main(String args[]) {
      System.out.println("Making connection...");
      //DataSource ds = new TNDataSource("serverma");
      DataSource ds = TestDataSource.create("serverk2");
      //DataSource ds = TestDataSource.create("serveri");

    int evid = 3321769;

// TEST 0 -- test getting wiggle for latest waveform

    //int wfid = 82565122;        // 100 sps
    //int wfid = 13356047;        // 10 sec/sample DAN.VHZ
    //int wfid = 93588568;             // 1983, 50 sps converted CUSP data
    //  int wfid = 72549842;             // 1981, 50 sps converted CUSP data
    //int wfid = 30257693; // in databasei wierdly clipped wf
    int wfid = 131146464;

      Wavelet wfx = Wavelet.create();
      wfx = wfx.getByWaveformId(wfid);
      System.err.println(wfx.toString());
      System.err.println(wfx.toBlockString());
      System.err.println("Fetching waveform using DataSource: "+wfx.getChannelObj().toString());

    // force filename
//      wfx.setFilename("2046133");
//      wfx.setPath("/wavearchive2/trig/1992/199204/");
//      wfx.setFilename("701559");
//      wfx.setPath("/home/awwalter/");
      System.err.println(wfx.toString());

      if (wfx.loadTimeSeries()) {
        System.out.println("Got waveform: nsegs =  "+ wfx.getSegmentList().size() );
        //      System.out.println ("Expected samps = "+ wfx.sampleCount.toString() );
        System.out.println("Actual samps   = "+ wfx.samplesInMemory() );
        System.out.println("max = "+ wfx.getMaxAmp());
        System.out.println("min = "+ wfx.getMinAmp());
        System.out.println("bias= "+ wfx.getBias());

      } else {
        System.out.println("Waveform retreival failed.");
      }

         // test noise scan

      wfx.scanForNoiseLevel();

      // Test subsetting of waveform
      if(wfx.getSegmentList().size() > 0) {
        WFSegment[] seglist = (WFSegment[])(wfx.getSegmentList().
                               toArray(new WFSegment[wfx.getSegmentList().size()]));
        for (int i = 0; i < wfx.getSegmentList().size(); i++) {
             WFSegment seg = seglist[i];
             System.out.println( seg.toString() );
        }
      }

      TimeSpan ts = wfx.getTimeSpan();
      double start = ts.getStart() + 10.0;
      double end   = ts.getEnd() - 10.0;


      wfx.scanForNoiseLevel(start, end);

      ArrayList segList = (ArrayList) wfx.getSubSegments(start, end);
      System.out.println("Trimmed waveform: nsegs =  "+ segList.size() );

      for (int i = 0; i < segList.size(); i++) {
        WFSegment seg = (WFSegment) segList.get(i);
        System.out.println( seg.toString() );
      }

      // test wave client reader
      WaveClient waveClient = null;

      if (false) {
        // cheating
        String propFile = "/home/tpp/src/waveserver/rt/WaveClient.properties";
        try {
         // Make a WaveClient
         System.out.println("Creating WaveClient using: "+propFile);
         waveClient = WaveClient.createWaveClient().configureWaveClient(propFile); // property file name
         System.out.println(waveClient.toString());
         int nservers = waveClient.numberOfServers();
         if (nservers <= 0) {
            System.err.println("getDataFromWaveServer Error:"+
                   " no data servers specified in input file: " +
                   propFile);
            System.exit(-1);
          }
        }
        catch (Exception ex) {
          System.err.println(ex.toString());
          ex.printStackTrace();
        }
        finally {
          //  if (waveClient != null) waveClient.close();
        }

        // Get all time-series from a wave server
        wfx.setWaveSource(waveClient);
        System.out.println("***** Attempting to load time series from WaveServer *******");
        wfx.unloadTimeSeries();
        boolean status = wfx.loadTimeSeries();
        System.out.println("Status = "+status);
      }

      if (false) {
// TEST 1
        WaveletTN wf = new WaveletTN();
         wf = (WaveletTN)wf.getByWaveformId(wfid);

        System.out.println("filesAreLocal ="+ wf.filesAreLocal() );

        System.err.println(" One by wfid...");
        System.err.println(wf.toString());
        System.err.println("Got waveform: nsegs = "+ wf.segList.size() );
      }

    if (true) {
// TEST 2 -- test retreival by evid
        System.err.println(" Many by evid...");

        System.err.println("Fetching waveforms pointer info...");

        ArrayList wfarr = (ArrayList) Wavelet.create().getBySolution(evid);
        Wavelet wfa[] = new Wavelet[wfarr.size()];
        wfarr.toArray(wfa);

        //  Wavelet wfa[] = (Wavelet[]) wfarr.toArray(new Wavelet[0]);

        System.err.println("Waveform count = " + wfa.length);

        for (int i = 0; i<wfa.length; i++)
        {
            wfa[i].loadTimeSeries();
            System.err.println(wfa[i].toString());
        }
      }

    }
  } // end of Tester
*/
} // end of class
