package org.trinet.jasi;
import org.trinet.jasi.EW.*;
import org.trinet.jasi.TN.*;

public class TestDataSource extends DataSource {

  protected TestDataSource(String URL, String driver,
                  String str2, String str1) {
    super(URL, driver, str2, str1);
  }

  public static final DataSource create(String host) { 
    DataSource ds = null;
    if ( JasiObject.getDefault() == JasiFactory.EARTHWORM ) {
       throw new NullPointerException("TestDataSourceEW class DNE! write source for it!");
    }
    else if ( JasiObject.getDefault() == JasiFactory.TRINET ) {
       ds = new TestDataSourceTN(host);
    }
    return ds;
  }
  public static final DataSource create(String host, String dbasename) { 
    DataSource ds = null;
    if ( JasiObject.getDefault() == JasiFactory.EARTHWORM ) {
       throw new NullPointerException("TestDataSourceEW class DNE! write source for it!");
    }
    else if ( JasiObject.getDefault() == JasiFactory.TRINET ) {
       ds = new TestDataSourceTN(host, dbasename);
    }
    return ds;
  }
  public static final DataSource create(String host, String dbasename, String str2, String str1) { 
    DataSource ds = null;
    if ( JasiObject.getDefault() == JasiFactory.EARTHWORM ) {
       throw new NullPointerException("TestDataSourceEW class DNE! write source for it!");
    }
    else if ( JasiObject.getDefault() == JasiFactory.TRINET ) {
       ds = new TestDataSourceTN(host, dbasename, str2, str1);
    }
    return ds;
  }
  public static final DataSource create() { 
    DataSource ds = null;
    if ( JasiObject.getDefault() == JasiFactory.EARTHWORM ) {
       throw new NullPointerException("TestDataSourceEW class DNE! write source for it!");
    }
    else if ( JasiObject.getDefault() == JasiFactory.TRINET ) {
       ds = new TestDataSourceTN();
    }
    return ds;
  }
}
