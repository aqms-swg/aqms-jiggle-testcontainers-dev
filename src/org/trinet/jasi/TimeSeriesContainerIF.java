package org.trinet.jasi;
public interface TimeSeriesContainerIF {
  public boolean hasTimeSeries() ;
  public boolean loadTimeSeries() ;
  public boolean loadTimeSeries(double start, double end) ;
  public boolean unloadTimeSeries() ;
}
