package org.trinet.jasi;

import java.util.*;

import org.trinet.pcs.PcsPropertyList;
import org.trinet.util.*;
import org.trinet.util.gazetteer.LatLonZ;
import org.trinet.web.TriggerTN;

/**
 * Model that returns a set of ChannelTimeWindow including triggered channels.
 * This is based on the contents of the Trig_Channel table for the event. <p>
 */

public class TriggerChannelTimeWindowModel extends ChannelTimeWindowModel {

  public static final String defModelName = "Trigger";

  /** A string with an brief explanation of the model. For help and tooltips. */
  public static final String defExplanation = "channels associated in database with a trigger id, sorted by earliest";

  public static final double defMinDistance = 20.;

  private boolean triggerLLZearliestChan = false;

  TriggerTN trigger = null;
  
  {
    setModelName(defModelName);
    setExplanation(defExplanation);
    setRequiresLocation(false);   // default behavior
  }

  public TriggerChannelTimeWindowModel() {
    setMyDefaultProperties();
  }

  public TriggerChannelTimeWindowModel(GenericPropertyList gpl, Solution sol, ChannelableList channelSet) {
    super(gpl, sol, channelSet);
  }
  public TriggerChannelTimeWindowModel(Solution sol, ChannelableList channelSet) {
    super(sol, channelSet);
  }
  public TriggerChannelTimeWindowModel(ChannelableList channelSet) {
    super(channelSet);
  }
  public TriggerChannelTimeWindowModel(Solution sol) {
    this();
    super.setSolution(sol);
  }

  public TimeSpan getTimeSpan() {
    return (trigger == null) ? new TimeSpan() : trigger.getTimeSpan();
  }

  // method override
  public ChannelableList getChannelTimeWindowList() {

    if (sol == null) {
      System.err.println(getModelName() + " : ERROR Required solution reference is null, no-op.");
      return null;
    }

    // A list of triggered channels as stored in the Trig_Channel table by CarlSubTrig
    // It only has H_Z channels and all start and end at the same time.
    // Triggered channels may not be in this model's Candidate list.
    trigger = new TriggerTN();
    trigger.setSolution(sol);  //this creates ChannelTimeWindow list
    ChannelableList ctwList = trigger.getChannelTimeWindows();
    if (ctwList.isEmpty()) return ctwList; // return empty List    
    
    // sort by earliest triggered station
    sortList(trigger, ctwList, triggerSortOrder);

    int count = ctwList.size();

    ChannelableList cList = getCandidateList(); // loads and sorts candidate list, if any

    // 'getIncludeAllComponents==true' forces inclusion of all orientations (e.g. those not triggered)
    // if any is added to list, if candidateList is not null, i.e. all matching net.sta.seedchan(0,2) 
    //ChannelableList newList = (getIncludeAllComponents()) ? new ChannelTimeWindowSetList() : new ChannelableList(count); // replaced by below -aww 2009/08/05
    ChannelableList newList = (getIncludeAllComponents()) ?
        new ChannelTimeWindowSetList(cList) : new ChannelableList(count); // init with candidate list to get all orientations -aww 2009/08/05

    Channelable ch = null;
    for (int i=0; i< count; i++) { // add channels in trigger
      ch = ((Channelable) ctwList.get(i));
      // Can't Limit distance to user's requested maximum Triggers have lat/lon = 0.0/0.0 !!
      // So they'er all 99,999 km away and a distance cut-off will throw them all out!
      if (include(ch)) newList.add(ch); // adds related components when includeAllComponents is set, see above
    }
    
    //XXXXXXXXXXXXXXXXXXXXXXXXXXX
    // Assume no required picks exist without an existing ctw from trigger table association
    //XXXXXXXXXXXXXXXXXXXXXXXXXXX

    // If other components were force added to a ChannelableSetList,
    // they won't have start/stop times, so add times to any that
    // may have been added in the loop above
    TimeSpan ts = trigger.getTimeSpan();  // extrema
    ChannelTimeWindow ctw = null;
    count = newList.size();

    for (int i = 0; i<count; i++) {
      ctw = (ChannelTimeWindow) newList.get(i);
      if (ctw.getTimeSpan() == null) ctw.setTimeSpan(ts);
      /* TEST/DEBUG block below when enabled overrides input trigger start/end span with energy time span expected for a known event
      if (sol.hasLatLonZ()) {
        if( ctw.getChannelObj().hasLatLonZ()) ctw.setTimeSpan(getTimeWindow(ctw.getChannelObj().getHorizontalDistance()));
      }
      */
    }
     
    newList = filterListByChannelPropertyAttributes(newList);
    if (filterWfListByChannelList) newList = filterListByChannelList(newList); 

    return (newList.size() > getMaxChannels()) ? new ChannelableList(newList.subList(0,getMaxChannels())) : newList;
 }

/**
 * Sort the list by distance from the first triggered channel.
 * @param trigger
 * @param list
 */ 
  private void sortList(TriggerTN trigger, ChannelableList list, String triggerSortOrder) {

    if (! sol.getLatLonZ().isNull() && triggerSortOrder.equalsIgnoreCase("DIST")) { // dist from earliest sta sort
        list.distanceSort(sol);
    }
    else if (triggerSortOrder.equalsIgnoreCase("DIST")) { // dist from earliest sta sort
        Channel chan1  = trigger.getEarliestChannel().getChannelObj();
        DateTime trigTime =  trigger.sol.getDateTime();
          
        chan1 = chan1.lookUp(chan1, trigTime); // lookup channel lat/lon info
        
        LatLonZ loc = chan1.getLatLonZ();
        if (sol.getLatLonZ().isNull()) { // only reset if current latlonz of solution is null -aww 2008/09/23
          if (triggerLLZearliestChan) {
              boolean tf = sol.hasChanged();
              // NOTE below causes trigger.sol.hasChanged() to return true, thus prompting for save on program exit/new event load
              trigger.sol.setLatLonZ(loc.getLat(), loc.getLon(), 0.1); //make depth=0.1 here - aww
              if (!tf) sol.setUpdate(false);
          }
        }
        
        list.distanceSort(loc);
    }
    else { // Time sort
        if (sol.getLatLonZ().isNull()) { // only reset if current latlonz of solution is null -aww 2008/09/23
          if (triggerLLZearliestChan) {
            Channel chan1  = trigger.getEarliestChannel().getChannelObj();
            DateTime trigTime =  trigger.sol.getDateTime();
            chan1 = chan1.lookUp(chan1, trigTime); // lookup channel lat/lon info
            LatLonZ loc = chan1.getLatLonZ();
            boolean tf = sol.hasChanged();
            // NOTE below causes trigger.sol.hasChanged() to return true, thus prompting for save on program exit/new event load
            trigger.sol.setLatLonZ(loc.getLat(), loc.getLon(), 0.1); //make depth=0.1 here - aww
            if (!tf) sol.setUpdate(false);
          }
        }
        list.sort(new TriggerChannelTimeWindowSorter(TriggerChannelTimeWindowSorter.OLDEST_FIRST));
    }
  }

  public static class TriggerChannelTimeWindowSorter implements Comparator {

    public static final int RECENT_FIRST = 0;
    public static final int OLDEST_FIRST = 1;

    int order = RECENT_FIRST;

    ChannelNameSorter cnSorter = new ChannelNameSorter();

    /** 'ORDER' is either RECENT_FIRST or OLDEST_FIRST */
    public TriggerChannelTimeWindowSorter(int order) {
      this.order = order;
    }

    public int compare(Object o1, Object o2) {

      if ( ! (o1 instanceof TriggerChannelTimeWindow) && (o2 instanceof TriggerChannelTimeWindow)) {
         return 0;  // wrong object types, void class cast exception
      }

      TriggerChannelTimeWindow jr1 = (TriggerChannelTimeWindow) o1;
      TriggerChannelTimeWindow jr2 = (TriggerChannelTimeWindow) o2;

      double tt1 = jr1.getTriggerTime();
      double tt2 = jr2.getTriggerTime();
      // Sort unknown times to end
      if (tt1 == 0.) {
         if (tt2 == 0.) return cnSorter.compare( jr1.getChannelObj(), jr2.getChannelObj());
         else if (tt2 != 0.) return 1;
      }
      else if (tt1 != 0. && tt2 == 0.) return -1;

      double diff = tt1 - tt2;

      // double diff = jr1.getTriggerTime() - jr2.getTriggerTime(); // checked ok -aww 2008/02/11

      if (Math.abs(diff) < .00001) diff = 0.; // added this test to handle weird roundoff? -aww 2010/10/20

      if (diff < 0.0) {
        return (order == RECENT_FIRST) ? 1 : -1;
      } else if (diff > 0.0) {
        return (order == RECENT_FIRST) ? -1 : 1;
      } else { // same time sort by component type
        return cnSorter.compare( jr1.getChannelObj(), jr2.getChannelObj());
      }
    }

  }


  public void setDefaultProperties() {
      super.setDefaultProperties();
      setMyDefaultProperties();
  }

  private void setMyDefaultProperties() {
    candidateListName = null;    // force user to set default name ?
    oldCandidateListName = null;
    candidateList = null;
    minDistance = defMinDistance;
    triggerLLZearliestChan=false;
    trigger = null;
  }

    public void setProperties(GenericPropertyList props) {
        super.setProperties(props);   /// get all the standard stuff
        if (props != null) {
          String pre = getPropertyPrefix();
          if (props.isSpecified(pre+"triggerLLZearliestChan") )
              triggerLLZearliestChan = props.getBoolean(pre+"triggerLLZearliestChan");
        }
    }

    public String getParameterDescriptionString () {
        StringBuffer sb = new StringBuffer(1024);
        sb.append(super.getParameterDescriptionString());
        String pre = getPropertyPrefix(); 
        sb.append(pre).append("triggerLLZearliestChan = ").append(triggerLLZearliestChan).append("\n"); 
        return sb.toString();
    }

    public ArrayList getKnownPropertyKeys() {
        ArrayList list = super.getKnownPropertyKeys();
        String pre = getPropertyPrefix(); 
        list.add(pre + "triggerLLZearliestChan");
        return list;
    }

/* ///////////////////////////////////////////////////////
  public static void main(String args[])
  {

      System.out.println("Making connection...");
      DataSource init = TestDataSource.create();  // make connection

      PcsPropertyList newProps = new PcsPropertyList();

      newProps.setFiletype("rcg_rt"); 
      newProps.setFilename("RCG_Trigger.props");
      
      
      long evid = 10249769;

      System.out.println(" Getting trigger evid = "+evid);

      Solution sol = Solution.create().getById(evid);

      
 //     TriggerTN trig = new TriggerTN(sol);
 //     ArrayList list = (ArrayList) trig.getChannelTimeWindows();
      
      TriggerChannelTimeWindowModel model = new TriggerChannelTimeWindowModel();
      model.setProperties(newProps);
      model.setCandidateListName("RCG-TRINET");
      model.setIncludeAllComponents(true);
      
// forces load of list
      System.out.println("Loading channel list...");
      ChannelableList myCandidateList = model.getCandidateList();
      model.setSolution(sol);
      
   ArrayList list = (ArrayList) model.getChannelTimeWindowList();

      for (int i = 0; i<list.size(); i++)
      {
        System.err.println(list.get(i).toString());
      }

  }     
*/
}
