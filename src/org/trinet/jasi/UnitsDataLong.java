package org.trinet.jasi;
import org.trinet.jdbc.datatypes.*;

//extends generic DataNumber type like DataLong so should it be UnitsDataLong ?
public class UnitsDataLong extends DataLong {

     /** The units of the value. Default is Units.UNKNOWN.
     * @See: Units */
     protected int units = Units.UNKNOWN;

     public UnitsDataLong() { }

     public UnitsDataLong(UnitsDataLong data) {
         setValue(data);
     }

     public UnitsDataLong(long value, int units) {
         setValue(value);
         this.units = units;
     }

     /** Return the units string, e.g. "counts", "counts/(cm/sec)"
     * @See: Units */
     public String getUnitsString() {
       return Units.getString(units);
     }

     /** Return the value of the units type.
     * @See: Units
     */
     public int getUnits() {
       return units;
     }

     /** Set the units of the value. Returns false is the string is not a legal
     * units description.
     * @See: Units */
     public void setUnits(int iunits) {
       units = iunits;
     }

     /** Set the units of the value. Returns false is the string is not a legal
     * units description.
     * @See: Units */
     public boolean setUnits(String sunits) {
        if (Units.isLegal(sunits)) {
          units = Units.getInt(sunits);
          return true;
        }
        return false;
     }

     public void setValue(UnitsDataLong value) {
       if (value.isNull()) this.setNull(true);
       else this.setValue(value.longValue());
       this.units = value.units;
     }

     public String toString (){
       StringBuffer sb = new StringBuffer(32);
       sb.append(super.toStringSQL()).append(" ").append(getUnitsString());
       return sb.toString();
     }

     public Object clone() {
       return (UnitsDataLong) super.clone();
     }
}
