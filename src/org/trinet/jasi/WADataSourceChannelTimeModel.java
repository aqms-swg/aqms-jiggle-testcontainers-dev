package org.trinet.jasi;

import org.trinet.util.*;

/**
 * ChannelTimeWindowModel that returns a list of ChannelTimeWindow that is
 * derived from the waveforms associated with this solution in the DataSource.
 */

public class WADataSourceChannelTimeModel extends DataSourceChannelTimeModel {

    {
        setModelName("WA DataSource");
        setExplanation("channels associated with both Wood-Anderson amps and database waveforms");
        chanIncludeList = new String [] {"HHE","HHN"};
    }

    public WADataSourceChannelTimeModel() {
        //setMyDefaultProperties();
        super();
    }

    public WADataSourceChannelTimeModel(GenericPropertyList gpl, Solution sol, ChannelableList candidateList) {
        super(gpl, sol, candidateList);
    }

    public WADataSourceChannelTimeModel(Solution sol, ChannelableList candidateList) {
        super(sol, candidateList);
    }

    public WADataSourceChannelTimeModel(ChannelableList candidateList) {
        super(candidateList);
    }

    public WADataSourceChannelTimeModel(Solution sol) {
        this();
        super.setSolution(sol);
    }

}
