package org.trinet.jasi;

/**
 * Exception class that gets thrown when a method is passed an inapproriate
 * Coda object for its processing.
 *
*/
public class WrongCodaTypeException extends WrongDataTypeException {
    public WrongCodaTypeException(String msg) {
	super(msg);
    }    
} // WrongCodaTypeException
