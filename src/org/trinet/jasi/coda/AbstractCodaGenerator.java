package org.trinet.jasi.coda;
import java.util.*;

import org.trinet.jasi.*;
import org.trinet.util.*;
/*
 *TODO: break calcCoda(...) into more methods to make code logic more readable. 
 */

/** Base class for calculating time series coda data and get results in a Coda object.
* Concrete subclasses set parameters and override default methods.
*/
public abstract class AbstractCodaGenerator implements CodaGeneratorIF {

    public static final int MIN_VALID_AT_START = 2; // -aww 2011/03/18 min number of good amp windows at start of coda scan

    public static final int MAX_WINDOWS_TO_SCAN = 1000; // 2000s for widow center offset of 2s, 1000s for 1s offset

    //Kludge below convenience until Coda table datatype of time1-6 columns are finalized 08/2004 -aww
    public static final int WINDOW_TIME_ABSOLUTE = 0;
    public static final int WINDOW_TIME_RELATIVE = 1;
    public static int windowTimeBase = WINDOW_TIME_RELATIVE; // changed default from ABSOLUTE to RELATIVE 10/24/2004 aww

    public static final int MAX_TIME_AMPS = 6;

    // Peak half-amp counts for EW analog channel from 12-bit system
    public static final int EW_ANALOG_PEAK_AMP_CNTS = 2048;
    // Default Clipping thresholds (fraction of half-amp peak counts)
    double EW_AVG_TO_MAX_AMP_CLIP_RATIO = 0.40; // Clip: coda 2-sec avg abs amplitudes
    // Default clipping thresholds (counts) for an EW 12-bit system:
    public static final double  EW_ANALOG_CLIP_AMP_CNTS =  819.2; // for coda 2-sec avg abs amplitudes
    // Default coda termination averaged window (counts) for an EW 12-bit system.  
    public static final double EW_ANALOG_CUTOFF_CNTS = 49.14;

    public static final char  CODA_CALC_INVALID_TYPE       = '?'; //"unknown, failure, reject, can't extrapolate"
    public static final char  P_CODA_TYPE                  = 'P';
    public static final char  S_CODA_TYPE                  = 'S';
    public static final char  HUMAN_CODA_TYPE              = 'H'; //"human"
    public static final char  NOISE_CODA_TYPE              = 'N'; //"noise"
    public static final char  SHORT_CODA_TYPE              = 'S'; //"short"
    public static final char  CLIPPED_CODA_TYPE            = 'K'; //"clipped" to end of ts (see endOfCoda(...))
    public static final char  TAU_AT_CUTOFF_AMP            = 'N'; //"normal cutoff termination"
    public static final char  TAU_EXTRAPOLATED_FREE        = 'R'; //"qfree extrapolation"
    public static final char  TAU_EXTRAPOLATED_FIXED       = 'X'; //"qfix  extrapolation"
    public static final char  HUMAN_AMP_TERMINATION        = 'h'; //"humanTauPick"
    public static final char  CUTOFF_AMP_TERMINATION       = 'a'; //"ampAtCutoff"
    public static final char  CLIPPED_AMP_TERMINATION      = 'c'; //"clippedAmp"
    public static final char  LOW_START_AMP_TERMINATION    = 'l'; //"lowStartAmp"
    public static final char  LOW_SNR_TERMINATION          = 'n'; //"noiseAboveCutoffAmp"
    public static final char  NOISE_SURGE_TERMINATION      = 's'; //"noiseSurgeAboveLastAmp"
    public static final char  END_OF_SAMPLES_TERMINATION   = 't'; //"truncatedTimeSeries"
    public static final char  MAX_WINDOWS_TERMINATION      = 'w'; //"windowsCountMax"
 
    public static final String  UNITS                      = CalibrUnits.getString(CalibrUnits.COUNTS); // "c";

    protected boolean overlapStartingWindows = false; // don't overlap window spans
    protected int overlapWindowMax = 10; // must set to even number, default first 10 seconds overlap

    protected static boolean debug = false;
    protected boolean forceExtrap  = false;

    //protected Concat        fmt;                     // string buffer formater for output
    protected FilterIF      timeSeriesFilter = null;
    protected boolean       filterEnabled = false;

    protected String        algorithm = null;          // output to coda object
    protected int           goodWindowCount = 0;       // output to coda object or total windows?
    protected double        tauFinal = 0.;             // output to coda object
    protected double        aveAbsAmpAtEndOfCoda = 0.; // output to coda object good or not windows
    protected double        qFree = 0.;                // output to coda object
    protected double        aFree = 0.;                // output to coda object
    protected double        aFix = 0.;                 // output to coda object
    protected double        rms = 0.;                  // output to coda object
    protected double        quality = 0.;              // output to coda object
    protected StringBuffer  codaDesc = null;           // output to coda object
    //protected String        codaPhase = "S";         // output to coda object

    protected int     firstGoodWindow = 0;        // optional output
    protected double  firstWindowAveAbsAmp = 0.;  // optional output
    protected int     badWindowCount = 0;         // optional output
    protected int     exitStatus = 0;             // output via method
    protected String  exitStatusString = null;

    protected double[] goodWindowAmp = null;      // output to coda object
    protected double[] goodWindowTau = null;      // output to coda object

    protected double[] windowWeight = null;
    protected double[] log10WindowTime = null; // shared buffer used to cache window log10 time
    protected double[] log10WindowAmp = null;  // shared buffer used to cache window log10 amps

    protected int      maxGoodWindowCount = 0;
    protected int      minGoodWindowsToEndCalcOnClipping = 0;

    protected double   biasLTASecs = 0.;
    protected double   codaStartSignalToNoiseRatio = 0.;
    protected double   codaCutoffSignalToNoiseRatio = 0.;
    protected double   passThruNoiseDecayAmpRatio = 0.;

    protected double   qFix = 0.;

    protected boolean  resetOnClipping = false;

    protected double  windowSize = 0.;                 // shared set from method input
    protected double  secsPerSample = 0.;              // shared set from method input
    protected double  secsPastPTimeAtStartingIdx = 0.; // shared set from method input
    protected int     lastSampleSCodaIdx = 0;   // shared set from method input
    protected double  minCodaStartingAmp = 0.;         // shared set from method input
    protected double  maxCodaNoiseCutoffAmp = 0.;      // shared set from method input
    protected double  channelCutoffAmp = 0.;           // shared set from method input

    protected int     windowCount = 0;                 // shared calc counter could be output
    protected boolean isFirstWindow = true;            // shared calc flag
    protected double  aveAbsAmpOfLastGoodWindow = 0.;  // shared calc value

    protected AbstractCodaGenerator() {}

    public static CodaGeneratorIF create(int subtype) throws IllegalArgumentException {
      CodaGeneratorIF codaGen = null;
      switch (subtype) {
        case SOCAL_MC:
          codaGen = create("org.trinet.jasi.coda.SoCalMcCodaGenerator");
          break;
        case NOCAL_MD:
          codaGen = create("org.trinet.jasi.coda.NoCalMdCodaGenerator");
          break;
        case SOCAL_MD:
          codaGen = create("org.trinet.jasi.coda.SoCalMdCodaGenerator");
          break;
        case NOCAL_MC:
          codaGen = create("org.trinet.jasi.coda.NoCalMcCodaGenerator");
          break;
        default:
          //System.err.println("Not a known subtype value: " + subtype);
          throw new IllegalArgumentException("Not a known subtype value: " + subtype);
      }
      return codaGen;
    }

    public static CodaGeneratorIF create(String className) {
      CodaGeneratorIF codaGen = null;
      try {
        codaGen = (CodaGeneratorIF) Class.forName(className).newInstance();
      }
      catch (ClassNotFoundException ex) {
        ex.printStackTrace();
      }
      catch (InstantiationException ex) {
        ex.printStackTrace();
      }
      catch (IllegalAccessException ex) {
        ex.printStackTrace();
      }
      //is codaGen initialized or configured
      if (codaGen == null) System.err.println("ERROR - Unable to create coda generator class: " + className);
      return codaGen;
    }

    public String getAlgorithm() { return algorithm; }
    public void setAlgorithm(String name) { algorithm = name; }

    public static void setDebug(boolean tf) {
        debug = tf;
        if (debug) System.out.println("DEBUG AbstractCodaGenerator setDebug: TRUE");
    }

    protected void initBuffers(int maxCount) {
        this.maxGoodWindowCount = (maxCount <= MAX_WINDOWS_TO_SCAN) ?  maxCount : MAX_WINDOWS_TO_SCAN;
        windowWeight     = new double[maxCount];
        goodWindowTau    = new double[maxCount]; // output
        log10WindowTime  = new double[maxCount]; // shared buffer used to cache window log10 time
        goodWindowAmp    = new double[maxCount]; // output to coda object
        log10WindowAmp   = new double[maxCount]; // shared buffer used to cache window log10 amps
    }

    private void initOutputData() {

        codaDesc = new StringBuffer(4);
        codaDesc.append(P_CODA_TYPE);
        codaDesc.append(S_CODA_TYPE); // no 'P' type here since scan start > SminusP time
        codaDesc.append(CODA_CALC_INVALID_TYPE);
        codaDesc.append(CODA_CALC_INVALID_TYPE);

        qFree                     = 0.;
        aFree                     = 0.;
        aFix                      = 0.;
        rms                       = 0.;
        tauFinal                  = 0.;
        aveAbsAmpAtEndOfCoda      = 1.;
        firstWindowAveAbsAmp      = 1.;

        firstGoodWindow           = 0;
        badWindowCount            = 0;
        windowCount               = 0;
        goodWindowCount           = 0;
        quality                   = 0.;
        exitStatus                = 0;
        exitStatusString          = "";   // p.lombard bug fix - needed for correct output message text - aww 2008/03/03 
    }

    protected TimeAmp [] getGoodWindowTimeAmps() {
        TimeAmp [] timeAmps = new TimeAmp [goodWindowCount];
        for (int idx = 0; idx < goodWindowCount; idx++) {
            timeAmps[idx] = new TimeAmp(goodWindowTau[idx], goodWindowAmp[idx]);
        }
        return timeAmps;
    }

    // At uniform interval to determine the termination tau,
    // otherwise need to interpolate a final tau that lies
    // between termination offset and time of previous goodWindow
    protected double getWindowStartingSampleSecsOffset(int windowCount) {
        return windowCount*getWindowSize();
    }

    protected int getWindowStartingSampleIndexOffset(int windowCount, double secsPerSample) {
        return (int) Math.round(getWindowStartingSampleSecsOffset(windowCount)/secsPerSample); 
    }

// Coda object also needs to have its channel desc, datetime (Ptime) set by the invoking class (see CodaMagnitudeMethod)
    private Coda resultsToCoda(Coda aCoda) {
        Coda resultsCoda = aCoda; 
        if (resultsCoda == null) resultsCoda = Coda.create();
        resultsCoda.setPhaseDescription(codaDesc.toString().trim());
        resultsCoda.setProcessingState(JasiProcessingConstants.STATE_AUTO_TAG); // algorithm calculated, so force AUTO 
        resultsCoda.algorithm.setValue(algorithm);
        resultsCoda.ampUnits.setValue(UNITS);
        resultsCoda.windowSize.setValue(windowSize);
        resultsCoda.timeS.setValue(secsPastPTimeAtStartingIdx);

        // prevent value too large for database -aww 2010/05/11
        if (tauFinal > 99999.) {
            tauFinal = 99999.;
            quality = 0.;
        }
        if (aFree > 999.) aFree = 999.;
        if (qFree > 999.) qFree = 999.;
        // end of values reset

        resultsCoda.quality.setValue(quality);
        resultsCoda.tau.setValue(tauFinal);

        timeAmpsToResults(resultsCoda); // note absolute vs. relative (tau) window amp times

        if (goodWindowCount > 0) {
            resultsCoda.windowCount.setValue(goodWindowCount);
            resultsCoda.aFix.setValue(aFix);
            resultsCoda.aFree.setValue(aFree);
            resultsCoda.qFix.setValue(qFix);
            resultsCoda.qFree.setValue(qFree);
            resultsCoda.residual.setValue(rms);
         }

        return resultsCoda;
    }

 /* ALTERNATIVE - REMOVED -AWW
    // Convenience method for use by subclasses to store time-amp samples
    // e.g. Mc mode=0, Md mode=1.
    // NOTE: saveMode = 0 saves the FIRST good "n" time,amp windows from the beginning of the coda waveform scan
    //       saveMode = 1 saves the LAST good "n" time,amp windows from the end of the coda waveform scan
    //                     where n is min(window_count, MAX_TIME_AMPS)
    // If no good windows, saves the only 1 pair, the starting values at the beginning of the waveform scan.
    //
    protected Coda timeAmpsToResults(Coda resultsCoda, int saveMode) {
        // First check to see datetime is valid
        if (! resultsCoda.datetime.isValidNumber()) {
          System.err.println("ERROR CodaGenerator unable to update time-amp pairs, input coda has undefined datetime!");
          return resultsCoda; // bad init check for error
        }

        //double pTime = 0. ; // 4/2004 aww - note change in pTime effects CodaFlag logic in Jiggle 
        double pTime = resultsCoda.datetime.doubleValue(); // enabled 8/11/2004 aww - note change effects CodaFlag in Jiggle 

        TimeAmp [] timeAmps = getGoodWindowTimeAmps();
        int size = timeAmps.length;

        // COMMENT OUT BELOW SECTION TO REMOVE ARCANE "SELECTION" SCREEN
        // Not sure the section below here is correct, never tested -aww
        // To save only certain "tau" windows we scan good window times for match
        // with selected save start times (windowed relative to P, not S). 
        //
        //
        // First create empty list
        if (size > 0)  {
          ArrayList saveList = new ArrayList(size); // size >=1 
          double [] timeToSave = getWindowTimesToSave();
          int saveSize = timeToSave.length;
          if (saveSize <= 0) size = 0; // save nothing

          else { // we want to save values
            int startSave = 0;
            // loop over good values, less the last one
            for (int idx = 0; idx < size-1; idx++) {
              // loop over save values, starting at next time to save
              for (int isave = startSave; isave < saveSize; isave++) {
                // find first save time > than current good window,
                // else good value is too small so exit inner loop
                // and get next good time
                if (timeAmps[idx].getTimeValue() < timeToSave[isave] ) continue;

                // good tau greater than required save start time so save it 
                 // Changed from a relative tau secs to absolute datetime, below needs fix - aww 2/10/2004
                //saveList.add(timeAmp[idx]); // save value
                saveList.add( new TimeAmp(timeAmp[idx].getTimeValue()+pTime, timeAmp[idx].getAmpValue()) );
                // bump save loop start index to get next greater save time value 
                startSave = ++isave;
                break; // go to next good time value in outer loop for save check
              }
            }
            //saveList.add(timeAmp[size-1]); // save final good "tau" value (relative time)
            // change relative time to absolute here:
            saveList.add( new TimeAmp(timeAmp[size-1].getTimeValue()+pTime, timeAmp[size-1].getAmpValue()) );
            size = saveList.size(); // reset size, to saved count
          }
        }
        // END OF COMMENTED OUT SAVE

        Collection timeAmpPairs = resultsCoda.getWindowTimeAmpPairs();
        timeAmpPairs.clear(); // purge any existing data, in every case
        int saveCount = (size < MAX_TIME_AMPS) ? size : MAX_TIME_AMPS;
        if (debug)
          System.out.println("  CodaGenerator timeAmpsToResults goodWindowCount: "+size+" saveCount:"+saveCount);

        // Changed from a relative tau secs to absolute datetime - aww 2/10/2004
        // add absolute datetime of P arrival (pTime) secs to the window tau:
        //  timeX = FmP + datetime.doubleValue();
        double pTime = 0. ; // resultsCoda.datetime.doubleValue(); // 4/2004 aww - note any change effects CodaFlag in Jiggle 
        if (saveCount == 0) { // no good windows, save the first time,amp pair result
          if (debug) {
            System.out.print("    No good windows to save, 1stTau,1stAmp:"+getWindowTauSeconds(0)+","+firstWindowAveAbsAmp);
            System.out.println(" tauFinal,finalAmp:"+tauFinal+","+aveAbsAmpAtEndOfCoda);
          }
          timeAmpPairs.add(new TimeAmp(getWindowTauSeconds(0)+pTime, firstWindowAveAbsAmp)); // at beginning of calc
          //timeAmpPairs.add(new TimeAmp(tauFinal+pTime, aveAbsAmpAtEndOfCoda));  // at end of calc
        }
        else {
          // Up to the MAX_TIME_AMPS allowed for db
          // saveMode = 0,  save saveCount time-amp samples from start of array
          // saveMode = 1,  save saveCount time-amp samples back from end of array
          if (saveMode == 0) size = saveCount;
          for (int idx = 0; idx < saveCount; idx++) {
            int saveIdx = size-saveCount+idx;
            // add absolute coda start time (p-phase time), if any
            // in increasing time order into the Coda collection 
            timeAmpPairs.add( new TimeAmp(timeAmps[saveIdx].getTimeValue()+pTime, timeAmps[saveIdx].getAmpValue()) );
            if (debug) System.out.println("       timeAmp idx: "+saveIdx+" "+timeAmps[saveIdx].toString()+" wt: "+windowWeight[saveIdx]);
          }
        }
        return resultsCoda;
    }
*/

    // Simple logic to store only the 1st, and last good timeAmp pairs if any
    // otherwise just stores the 1st widow value good or not.
    protected Coda timeAmpsToResults(Coda resultsCoda) {
        // First check to see datetime is valid
        if (! resultsCoda.datetime.isValidNumber()) {
          System.err.println("ERROR CodaGenerator unable to update time-amp pairs, input coda has undefined datetime!");
          return resultsCoda; // bad init check for error
        }

        TimeAmp [] timeAmps = getGoodWindowTimeAmps();
        int size = timeAmps.length;

        Collection timeAmpPairs = resultsCoda.getWindowTimeAmpPairs();
        timeAmpPairs.clear(); // purge any existing data, in every case
        int saveCount = (size < MAX_TIME_AMPS) ? size : MAX_TIME_AMPS;

        // Changed default from a relative tau secs to absolute datetime - aww 2/10/2004
        // Changed default back to relative tau secs from absolute datetime - aww 10/25/2004
        // add absolute datetime of P arrival (pTime) secs to the window tau:
        //  timeX = FmP + datetime.doubleValue();
        double pTime = 0. ; // 4/2004 aww - note change in pTime effects CodaFlag logic in Jiggle 
        if (windowTimeBase == WINDOW_TIME_ABSOLUTE)        
           pTime = resultsCoda.datetime.doubleValue(); // enabled 8/11/2004 aww - note change effects CodaFlag in Jiggle 
        if (debug)
          System.out.println("  CodaGenerator timeAmpsToResults goodWindowCount: "+size+" saveCount:"+saveCount+" pTime: "+pTime);

        if (saveCount == 0) { // no good windows, save the first time,amp pair result
          if (debug) {
            System.out.print("    No good windows to save, 1stTau,1stAmp:"+getWindowTauSeconds(0)+","+firstWindowAveAbsAmp);
            System.out.println(" tauFinal,finalAmp:"+tauFinal+","+aveAbsAmpAtEndOfCoda);
          }
          timeAmpPairs.add(new TimeAmp(getWindowTauSeconds(0)+pTime, firstWindowAveAbsAmp)); // at beginning of calc
          //timeAmpPairs.add(new TimeAmp(tauFinal+pTime, aveAbsAmpAtEndOfCoda));  // at end of calc
        }
        else {
          //timeAmpPairs.add(timeAmps[0]); // uses relative tau time
          // convert to absolute time 1st good value 08/11/2004 -aww
          timeAmpPairs.add(new TimeAmp( timeAmps[0].getTimeValue()+pTime, timeAmps[0].getAmpValue() ));
          if (debug) System.out.print("       timeAmp( 0): "+timeAmps[0].toString()+" wt: "+windowWeight[0]);
          if (size > 1) {
            //timeAmpPairs.add(timeAmps[size-1]); // uses relative tau time
            // convert to absolute time last good value 08/11/2004 -aww 
            timeAmpPairs.add(new TimeAmp( timeAmps[size-1].getTimeValue()+pTime, timeAmps[size-1].getAmpValue() ));
            if (debug) System.out.println(" timeAmp("+(size-1)+"): "+
                            timeAmps[size-1].toString()+" wt: "+windowWeight[size-1]);
          }
        }
        return resultsCoda;
    }

// Method to collect coda window data along time series;
// adapted from  CUSP CODAT_TM_R4_SUB.FOR
// Add method to use waveform object and Coda object???
    public boolean calcCoda(Coda outCoda, double secsPerSample, double secsAfterPTime,
                            float [] timeSeriesAmps, int startingIdxOffset, double responseNormalizedCodaCutoffAmp,
                            double responseClippingAmp, double inLTA) { 

        boolean status = calcCoda(secsPerSample, secsAfterPTime,
                            timeSeriesAmps, startingIdxOffset, responseNormalizedCodaCutoffAmp,
                            responseClippingAmp, inLTA); 
        if (debug) System.out.println("DEBUG AbstractCodaGenerator calcCoda status to set codaResults: " + status);
        if (status) setCodaResults(outCoda);
        return status;
    }

    public boolean calcCoda(double secsPerSample, double secsAfterPTime,
                            float [] timeSeriesAmps, int startingIdxOffset, double responseNormalizedCodaCutoffAmp,
                            double responseClippingAmp, double inLTA) { 

        int numberOfSamples = timeSeriesAmps.length - startingIdxOffset;
        return calcCoda(secsPerSample, secsAfterPTime, timeSeriesAmps, startingIdxOffset, numberOfSamples, 
                               responseNormalizedCodaCutoffAmp, responseClippingAmp, inLTA);
    }

    public boolean calcCoda( double secsPerSample, double secsAfterPTime,
                             float [] timeSeriesAmps, int startingIdxOffset, int timeSeriesSamples,
                             double responseNormalizedCodaCutoffAmp, double responseClippingAmp,
                             double inLTA
                           ) { 

      try {

        initOutputData();

        if (timeSeriesSamples < 0 ) throw new IllegalArgumentException("input number of samples < 0");

        this.secsPerSample = secsPerSample;
        this.secsPastPTimeAtStartingIdx   = secsAfterPTime;
        this.channelCutoffAmp = responseNormalizedCodaCutoffAmp;

        int firstSampleSCodaIdx = startingIdxOffset;

        if (timeSeriesSamples > timeSeriesAmps.length) timeSeriesSamples = timeSeriesAmps.length;
        if (firstSampleSCodaIdx > 0 ) timeSeriesSamples -= firstSampleSCodaIdx;
        if (timeSeriesSamples < 1) {
            System.out.println("  CodaGenerator calcCoda input starting index beyond times series end.");
            return false;
        }
        this.lastSampleSCodaIdx = firstSampleSCodaIdx + timeSeriesSamples - 1;
        if (debug) System.out.println("  CodaGenerator calcCoda.secsPastPTimeAtStartingIdx: " +
            secsPastPTimeAtStartingIdx + " startingIdxOffset: " + firstSampleSCodaIdx +
            " lastSampleIdx: " + lastSampleSCodaIdx);

        boolean hasResponseClippingAmp = false;
        if (responseClippingAmp > 0.) hasResponseClippingAmp = true;   

        aveAbsAmpOfLastGoodWindow = 1.;
        isFirstWindow = true;

        int samplesPerWindow = (int) Math.round(getWindowSize()/secsPerSample);
        if (samplesPerWindow <= 0 ) {
            if (debug) System.out.println("  CodaGenerator calcCoda samplesPerWindow <= 0.  windowSize: " +
                    getWindowSize() + " secsPerSample: " + secsPerSample);
            return false;
        }

        int lowAmpWindowCount = 0;
        int startingAmpWindowCount = 0;  // count for amplitude check on first windows

        int windowStartingSampleIdx = 0;
        int windowEndingSampleIdx = 0;
        int currentWindowSamples = 0;

        double averagedAbsAmp = 0.;

        float [] filteredAmps = filterTimeSeries(timeSeriesAmps);

        int biasSamples = (int) Math.min(timeSeriesSamples, Math.floor(biasLTASecs/secsPerSample));  // bugfix: not timeSeriesSamples/sps -aww 2011/03/24

        // Note input timeseries typically windowed to include the bias span just before the P-time
        double wfBias = calcBias(filteredAmps, 0, biasSamples); // option change to pre-calculated LTA and bias for waveform input

        double startLTA = Math.abs(inLTA);
        if (inLTA == 0.) {
            // LTA for pre-P secs of timeseries - aww 2008/03/03
            // Note the start time is not shifted earlier by magnitude method's preP bias seconds property value !
            startLTA = calcStartLTA(filteredAmps, biasSamples, wfBias,
                                    firstSampleSCodaIdx-(int)Math.round(secsPastPTimeAtStartingIdx/secsPerSample));
        }
        startLTA = (double)Math.round(startLTA); // round to nearest count

        // LTA for ending seconds of timeseries -- test aww 2008/03/03
        double endLTA =  (double)Math.round(calcEndLTA(filteredAmps, biasSamples, wfBias)); // round to nearest count


        double lta = Math.min(startLTA, endLTA); // use lesser of starting or ending LTA, -aww added 2008/03/03

        //NOTE:
        //for SmallEvent->BigEvent the endLTA may be too high
        //for BigEvent->SmallEvent the startLTA may be too high
        //for BigEvent->SmallEvent->BigEvent both LTA's may be too high
        minCodaStartingAmp = codaStartSignalToNoiseRatio * lta ;

        //For case of short wfsegments (e.g. time-tear) the LTA would be close to 2-sec window avg
        //and "true" pre/post avg noise levels are unknown, thus the first 2-sec window avg
        //would get a "lowAmplitude" rejection inside windowing loop below thus to handle case of
        //missing enough timeseries for a valid LTA we set:
        if (inLTA == 0. && biasSamples >= timeSeriesSamples) minCodaStartingAmp = channelCutoffAmp; // revised 2011/03/24 -aww

        // setting prop minSNRatioCodaCutoff=0 => codaCutoffSignalToNoiseRation=0 => bypasses noise termination 
        maxCodaNoiseCutoffAmp = codaCutoffSignalToNoiseRatio * lta;

        //boolean flag below added for extra condition on noise termination test (as per p.lombard request) - aww 2008/03/03

        int iterCount = 0; // loop iteration counter

        if (debug) {
            System.out.println("->CodaGenerator calcCoda ampCut: " + channelCutoffAmp +
                               " startAmp: " +  Math.round(minCodaStartingAmp)  + " noiseCut: " + Math.round(maxCodaNoiseCutoffAmp) +
                               " startLTA:" + startLTA + " endLTA:" + endLTA + " lta: " + lta + " bias:" + Math.round(wfBias) + " biasSec: " + biasLTASecs +
                               " biasSamp: " + biasSamples + "\n-> " + inputToString()
                              );
        }

        while (windowCount <= MAX_WINDOWS_TO_SCAN) { // terminate coda duration, avoid infinite iteration loop 
            iterCount++;
            CheckStatus: {   // break out of this block to check coda termination return conditions

                windowStartingSampleIdx = firstSampleSCodaIdx +
                                              getWindowStartingSampleIndexOffset(windowCount, secsPerSample);
                windowEndingSampleIdx = windowStartingSampleIdx + samplesPerWindow - 1;       // end of current window

                if (windowStartingSampleIdx < 0) {
                    if (debug) System.out.println("  CodaGenerator starting sample index < 0" );
                    windowCount++;
                    badWindowCount++;
                    break CheckStatus; // before start of input time series data do next window
                }

                if (windowStartingSampleIdx >= lastSampleSCodaIdx) {  // end of data 
                    if (debug) System.out.println("  CodaGenerator starting sample index at or past end of series" );
                    currentWindowSamples = lastSampleSCodaIdx + samplesPerWindow - windowStartingSampleIdx;
                    endOfCoda(windowEndingSampleIdx, currentWindowSamples, averagedAbsAmp, responseClippingAmp);
                    return doCodaStatistics();
                }

                if (windowEndingSampleIdx > lastSampleSCodaIdx) {
                    windowEndingSampleIdx = lastSampleSCodaIdx; // a truncated window
                }

                currentWindowSamples = windowEndingSampleIdx - windowStartingSampleIdx + 1;   // length of current window
                // Do we have at least half of a window?
                //if (2 * currentWindowSamples < samplesPerWindow) {  // don't calc ave amp just add this length to coda duration
                if (currentWindowSamples < samplesPerWindow) {  // don't calc ave amp just end coda at previous window duration
                    if (debug) System.out.println("  CodaGenerator window samples < window size ending coda" );
                    currentWindowSamples = lastSampleSCodaIdx - windowStartingSampleIdx; 
                    endOfCoda(windowEndingSampleIdx, currentWindowSamples, averagedAbsAmp, responseClippingAmp);
                    return doCodaStatistics();
                }

                // Usual path for coda calcuation, determines averaged absolute amplitude of window samples;
                boolean hasNoClippedSamples = true;
                for (int idx = windowStartingSampleIdx; idx <= windowEndingSampleIdx; idx++) {
                  // Would need to have input argument for sampleClipValue e.g. codaMagCalibr.getClipAmpValue() 
                  //if( hasResponseClippingAmp && filteredAmps[idx] >= sampleClipValue) hasNoClippedSamples = false;
                    averagedAbsAmp += Math.abs((double) filteredAmps[idx] - wfBias);
                }
                averagedAbsAmp = averagedAbsAmp/currentWindowSamples;
                if( hasResponseClippingAmp && (averagedAbsAmp >= responseClippingAmp)) hasNoClippedSamples = false;

                if (debug) {
                    System.out.print("  CodaGenerator calcCoda windowStartIdx: " +
                        windowStartingSampleIdx + " endIdx: " + windowEndingSampleIdx); 
                    System.out.println(" clipAmp: " + responseClippingAmp +  " " +
                        " unClipped: " +  hasNoClippedSamples + " aveAmp: " + averagedAbsAmp);
                }
                if (averagedAbsAmp < 1.0) averagedAbsAmp = 1.0; // avoid LOG(amp) issues


       // Tests for low amp at start of scan are below here
       // "low" amp logic below is intended for cases where S is later than predicted from TT model, typically distant channels.
       // minCodaStartingAmp = codaStartSignalToNoiseRatio * lta  (a trigger threshhold determines whether window is in S-coda)
       // Channel coda calc is skipped if its first MIN_VALID_AT_START count S-window's AveAbsAmp values < minCodaStartingAmp
       // thus using overlapping "windows" (1-sec) might better discriminate short duration coda cases. 
       // If property codaStartSignalToNoiseRatio = 1, minCodaStartingAmp ~= pre/post event lta (without scaling) in 
       // this case if first window is before true S arrival, and its P-coda value is below responseNormalizedCodaCutoffAmp
       // the coda would be immediately terminated and incorrectly flagged as "normal" termination but in reality it is too short.
                startingAmpWindowCount++;           // bump window count 
                boolean hasLowAmplitude = false;    // intialize, assume good amp for 1st windows
                if (startingAmpWindowCount <= MIN_VALID_AT_START) {  // now execute only on 1st windows

                    int startLowAmpWindowCount = lowAmpWindowCount;// > 0 => low-amp in one or both of 1st windows

                    //if (averagedAbsAmp < channelCutoffAmp)  { // removed this test condition -aww 2008/03/04
                        //lowAmpWindowCount++;
                        //exitStatusString = "lowAmp#"+startingAmpWindowCount+"="+(int)averagedAbsAmp+" < normalCut="+channelCutoffAmp;
                    //} else

                    if (goodWindowCount <=  0 && averagedAbsAmp < minCodaStartingAmp) { // first window not good amp
                        lowAmpWindowCount++;
                        exitStatusString = (averagedAbsAmp < channelCutoffAmp) ?
                            "lowAmp#"+startingAmpWindowCount+"="+(int)averagedAbsAmp+" < normalCut="+channelCutoffAmp :
                            "lowAmp#"+startingAmpWindowCount+"="+(int)averagedAbsAmp+" < minStart="+minCodaStartingAmp;
                    }

                    if (lowAmpWindowCount >= 1) quality = 0.1;

                    if (startingAmpWindowCount == 1) {
                        firstWindowAveAbsAmp = (double)Math.round(averagedAbsAmp); // save first window amp (good or not)
                    }

                    aveAbsAmpAtEndOfCoda = averagedAbsAmp;

                    if (lowAmpWindowCount > startLowAmpWindowCount) {
                        hasLowAmplitude = true; // current window low
                    }

                    if (debug)
                      System.out.println("              calcCoda hasLowAmplitude: " + hasLowAmplitude +
                                         " validCount:" +startingAmpWindowCount+
                                         " MIN_VALID :" +MIN_VALID_AT_START+
                                         " startCount:" +startLowAmpWindowCount+
                                         " lowAmpCount:" + lowAmpWindowCount+
                                         " exitStatus:" + exitStatusString
                                      );  
                }

                // If consecutive low amp windows at start bail because coda window curve fitting gives bad results
                if (lowAmpWindowCount > 1 && startingAmpWindowCount > MIN_VALID_AT_START) {
                    if (debug) System.out.println("  TERMINATE calcCoda at lowAmpWindowCount: " + lowAmpWindowCount);
                    quality = 0.0;
                    exitStatus = -1;
                    //exitStatusString = "lowStartingAmp";
                    codaDesc.setCharAt(3, LOW_START_AMP_TERMINATION);
                    return false;

                    /* Normal calc failure return above, below forces return of coda for testing - aww 2008/03/04
                    // Rejected "low-amp" codas can be saved by code below here and examined in jiggle gui
                    System.out.println(" TERMINATE calcCoda at lowAmpWindowCount: " + lowAmpWindowCount);
                    quality = 0.1; // ?
                    endOfCoda(windowEndingSampleIdx, currentWindowSamples, averagedAbsAmp, responseClippingAmp);
                    aveAbsAmpAtEndOfCoda = aveAbsAmpOfLastGoodWindow;
                    codaDesc.setCharAt(0, SHORT_CODA_TYPE);
                    codaDesc.setCharAt(3, LOW_START_AMP_TERMINATION); // note amp could be < channelCutoffAmp
                    tauFinal = getWindowTauSeconds(windowCount); //  endOfCoda bumps windowCount
                    doCodaStatistics();
                    exitStatus = 6;
                    return true; // instead of normal return of false for low-amps at start of scan -aww 2008/03/04
                    */
                }

                if (! hasLowAmplitude) lowAmpWindowCount = 0; // if second window amp is ok, reset counter

                // if a low amp near start of scan, and no good amp windows, get the next window,
                // otherwise do other "termination" tests below this block for the current window 
                if (lowAmpWindowCount > 0 && goodWindowCount < 1) {
                    // should only be here when 1st window is bad amp, or when 1st is bad and then 2nd is bad
                    if (debug) System.out.println("   calcCoda break at lowAmpWindowCount: " + lowAmpWindowCount);
                    windowCount++;
                    badWindowCount++;
                    break CheckStatus;   // do next window
                }

      // End of starting window low amp tests

      // Normal window amp termination testing is below here

                // normalized to "60mv" analog counts equivalent (1 window less than total windows calculated);
                if (averagedAbsAmp <= channelCutoffAmp) {
                    if (debug) System.out.println(" TERMINATE calcCoda averagedAbsAmp <= normalized cutoff: " + averagedAbsAmp);
                    //tauFinal = getWindowTauSeconds(windowCount); //  endOfCoda bumps windowCount
                    endOfCoda(windowEndingSampleIdx, currentWindowSamples, averagedAbsAmp, responseClippingAmp); //test aww 4/21/2004
                    aveAbsAmpAtEndOfCoda = aveAbsAmpOfLastGoodWindow;
                    codaDesc.setCharAt(2, TAU_AT_CUTOFF_AMP);
                    codaDesc.setCharAt(3, CUTOFF_AMP_TERMINATION);
                    exitStatusString = "responseCutoffAmp";
                    exitStatus = 3;
                    return doCodaStatistics();
                }

                // high noise coda, background noise greater than cutoff counts;
                else if ((averagedAbsAmp <= maxCodaNoiseCutoffAmp)) {
                    if (debug) System.out.println(" TERMINATE calcCoda averagedAbsAmp <= maxCodaNoise cutoff: " + averagedAbsAmp);
                    //tauFinal = getWindowTauSeconds(windowCount); //  endOfCoda bumps windowCount
                    endOfCoda(windowEndingSampleIdx, currentWindowSamples, averagedAbsAmp, responseClippingAmp); //test aww 4/21/2004
                    aveAbsAmpAtEndOfCoda = aveAbsAmpOfLastGoodWindow;
                    codaDesc.setCharAt(0, NOISE_CODA_TYPE);
                    codaDesc.setCharAt(3, LOW_SNR_TERMINATION);
                    exitStatusString = "noiseCutoffAmp";
                    exitStatus = 4;
                    return doCodaStatistics();
                }

                // Check for clipped amp;
                boolean noClip = (hasNoClippedSamples && (averagedAbsAmp < responseClippingAmp)); 
                boolean noNoiseSurge = (averagedAbsAmp < aveAbsAmpOfLastGoodWindow*passThruNoiseDecayAmpRatio);

                if (goodWindowCount > 0) {        // check window average amp for clipping
                    // TEST code block: 
                    // If window amp within ~10% of cutoff don't extend coda windowing (amp increase=noise/reverb?) 08/10/2004 -aww
                    if (averagedAbsAmp > 1.2*aveAbsAmpOfLastGoodWindow && (aveAbsAmpOfLastGoodWindow * 0.9 < channelCutoffAmp)) {
                        noNoiseSurge = false;
                    }
                    // END of TEST code block

                    if (noClip && noNoiseSurge) {
                        
                        if (endOfCoda(windowEndingSampleIdx, currentWindowSamples, averagedAbsAmp, responseClippingAmp)) {
                            if (debug) System.out.println(" TERMINATE calcCoda endOfCoda true, averagedAbsAmp <= cutoff: " + averagedAbsAmp);
                            return doCodaStatistics();
                        }
                        else break CheckStatus;
                    }
                    else {                       // reset processing of coda windows
                        codaDesc.setCharAt(0, SHORT_CODA_TYPE);
                        if (noClip) { // > noise surge limit
                          codaDesc.setCharAt(3, NOISE_SURGE_TERMINATION);
                        } 
                        else { // clipped
                          codaDesc.setCharAt(3, CLIPPED_AMP_TERMINATION);
                        }
                        if ((goodWindowCount < minGoodWindowsToEndCalcOnClipping) && resetOnClipping) {
                            // reset counts and start again - WHY should this be done?
                            badWindowCount += goodWindowCount + 1;
                            windowCount = badWindowCount; // to get right seconds offset from start
                            goodWindowCount = 0;
                            isFirstWindow = true;
                            codaDesc.setCharAt(0, P_CODA_TYPE); // reset to original start type
                            if (debug) System.out.println("   calcCoda amp surge: " + averagedAbsAmp + " RESET windowing");

                            break CheckStatus; // do next window 
                        }
                        else { // end of processing
                            if (debug) {
                              System.out.println(
                                 " TERMINATE calcCoda goodWindowCount: "+goodWindowCount +
                                 " after clip/surge of averagedAbsAmp: " + averagedAbsAmp +
                                 " above aveAbsAmpOfLastGoodWindow: " + aveAbsAmpOfLastGoodWindow
                              );
                            }
                            aveAbsAmpAtEndOfCoda = aveAbsAmpOfLastGoodWindow;
                            tauFinal = getWindowTauSeconds(windowCount);
                            if (goodWindowCount < minGoodWindowsToEndCalcOnClipping) {
                              exitStatusString = "ampClipOrSurge";
                              exitStatus = 4;
                            }
                            else {
                              exitStatusString = "minGoodClipped";
                              exitStatus = 1;
                            }
                            return doCodaStatistics(); // added aww 9/00
                        }
                    }
                }
                else {   // no valid windows yet
                    if (noClip) {   // got a good one, invoke endOfCoda to bump goodWindowCount (tau)
                        if (endOfCoda(windowEndingSampleIdx, currentWindowSamples, averagedAbsAmp, responseClippingAmp)) {
                            if (debug) 
                              System.out.println(" TERMINATE calcCoda end of coda window; unclipped averagedAbsAmp : " + averagedAbsAmp);
                            return doCodaStatistics();
                        }
                        else break CheckStatus;
                    }
                    else { // discard current coda window;
                        if (debug) System.out.println("   calcCoda no good windows, current window CLIPPED: "+averagedAbsAmp);
                        windowCount++;
                        badWindowCount++;
                        break CheckStatus; // do next window
                    }
                }
            } // end of CheckStatus block

            if (goodWindowCount >= maxGoodWindowCount) {  // terminate prematurely may still have unanalyzed time series data
                    tauFinal = getWindowTauSeconds(windowCount);
                    aveAbsAmpAtEndOfCoda = aveAbsAmpOfLastGoodWindow;
                    codaDesc.setCharAt(0, SHORT_CODA_TYPE);
                    codaDesc.setCharAt(3, MAX_WINDOWS_TERMINATION);
                    exitStatusString = "maxGoodCount";
                    exitStatus = 5;
                    if (debug) System.out.println(" TERMINATE calcCoda maxGoodWindowCount reached; last amp : " + aveAbsAmpAtEndOfCoda);
                    return doCodaStatistics();
            }

            if (windowEndingSampleIdx == lastSampleSCodaIdx &&  windowCount > 0) { // series ended at last window done
                currentWindowSamples = lastSampleSCodaIdx - windowStartingSampleIdx + 1;
                endOfCoda(windowEndingSampleIdx, currentWindowSamples, averagedAbsAmp, responseClippingAmp);
                codaDesc.setCharAt(0, SHORT_CODA_TYPE);
                codaDesc.setCharAt(3, END_OF_SAMPLES_TERMINATION);
                exitStatusString = "endOfSeries";
                exitStatus = 2;
                if (debug) System.out.println(" TERMINATE calcCoda end of timeseries reached; last amp : " + averagedAbsAmp);
                return doCodaStatistics();
            }


        } // end of iteration while loop

        // Reached end of loop, unusual condition should not happen
        System.out.println("Error ? calcCoda TERMINATION at maximum allowed windowCount.");
        if (debug) {
            System.err.println(
                "DEBUG calcCoda iterations: " + iterCount +
                " windowCount: " + windowCount +
                " windowEndingSampleIdx:"+ windowEndingSampleIdx +
                " lastSampleSCodaIdx: " + lastSampleSCodaIdx +
                " aveAbsAmpOfLastGoodWindow : " + aveAbsAmpOfLastGoodWindow
            );
        }
        // Save last good values
        tauFinal = goodWindowTau[goodWindowCount]; 
        aveAbsAmpAtEndOfCoda = goodWindowAmp[goodWindowCount];
        codaDesc.setCharAt(0, SHORT_CODA_TYPE);
        codaDesc.setCharAt(3, MAX_WINDOWS_TERMINATION);
        exitStatusString = "maxIterationLimit";
        exitStatus = 7;
        return doCodaStatistics();

      }
      catch (IllegalArgumentException ex) {
        ex.printStackTrace();
        return false;
      }

    }
    public String describeFilter() {
        return (timeSeriesFilter != null) ? timeSeriesFilter.getDescription() : "No Filter";
    }
    public boolean isFilterEnabled() { return filterEnabled;}
    public boolean hasFilter() { return (timeSeriesFilter != null);}
    public void enableFilter() { filterEnabled = true; }
    public void disableFilter() { filterEnabled = false; }
    public void setFilter(Channel chan) {
        if (timeSeriesFilter != null) setFilter(timeSeriesFilter.getFilter((int)chan.getSampleRate()));
        else System.out.println("Warning : CodaGenerator setFilter(String) timeSeriesFilter is null");
    }
    /* removed the method below  -aww, see setFilter(Channel)
    public void setFilter(String seedchan) {
        if (timeSeriesFilter != null) setFilter(timeSeriesFilter.getFilter(seedchan));
        else System.out.println("Warning : CodaGenerator setFilter(String) timeSeriesFilter is null");
    }
    */

/** Sets the filter to apply to input waveform time series. */
    public void setFilter(FilterIF filter) {
        timeSeriesFilter = filter;
    }

    protected float [] filterTimeSeries(float [] timeSeriesAmps) { // de-spiking then bandpass would be nice 
        if (timeSeriesFilter == null ) return timeSeriesAmps;
        return (filterEnabled) ? (float []) timeSeriesFilter.filter(timeSeriesAmps) : timeSeriesAmps;
    }

    protected double getWindowTauSeconds(int windowCount) {
        return secsPastPTimeAtStartingIdx + getWindowStartingSampleSecsOffset(windowCount)
                + 0.5 * windowSize; 
    }

// Method invoked when time series scan is ended because of amplitude tests or end of data
    private boolean endOfCoda(int windowEndingSampleIdx, int currentWindowSamples,
                              double averagedAbsAmp, double responseClippingAmp) {

        boolean haveWholeWindow = (Math.abs(currentWindowSamples*secsPerSample - getWindowSize()) < (secsPerSample+secsPerSample/2.));

        // Test for end of data coda time series scanning
        if (windowEndingSampleIdx > lastSampleSCodaIdx || goodWindowCount >= maxGoodWindowCount || !haveWholeWindow ) {
            // Don't pad the coda with partial window length, use last good 2-sec window value -aww 2011/03/18
            //tauFinal = secsPastPTimeAtStartingIdx + getWindowStartingSampleSecsOffset(windowCount) + currentWindowSamples*secsPerSample;
            tauFinal = getWindowTauSeconds(windowCount);  // changed from above tauFinal -aww 2011/03/18
            aveAbsAmpAtEndOfCoda = aveAbsAmpOfLastGoodWindow;

            if (windowEndingSampleIdx >= lastSampleSCodaIdx || !haveWholeWindow) {
              // Added input arg for responseClippingAmp and test to make desc char code 'K'  - aww 08/11/2004
              if (goodWindowCount < 1 && responseClippingAmp > 0. && averagedAbsAmp > responseClippingAmp) 
                      codaDesc.setCharAt(0, CLIPPED_CODA_TYPE);
              else
                      codaDesc.setCharAt(0, SHORT_CODA_TYPE);

              if (debug) System.out.println("  CodaGenerator endOfCoda past END OF TIME SERIES");
              codaDesc.setCharAt(3, END_OF_SAMPLES_TERMINATION);
              exitStatusString = "endOfSeries";
              exitStatus = 2;
            }
            else {
              if (debug) System.out.println("  CodaGenerator endOfCoda reached MAX GOOD WINDOW COUNT");
              codaDesc.setCharAt(0, SHORT_CODA_TYPE);
              codaDesc.setCharAt(3, MAX_WINDOWS_TERMINATION);
              exitStatusString = "endOfSeries";
              exitStatus = 2;
            }

            return true; // do coda statistics
        }

        // Not at coda end thus set values for current iteration
        if (averagedAbsAmp <= 0.) {
            System.err.println("Coda error bad averagedAbsAmp:" + averagedAbsAmp + " ; reset to 1. count.");
            averagedAbsAmp = 1.;
        }
        aveAbsAmpOfLastGoodWindow = averagedAbsAmp;

        double tauCurrentWindow = getWindowTauSeconds(windowCount);
        if (tauCurrentWindow <= 0.5) { // used to be 0. which, rounds down to zero -aww 20100507
            System.err.println("Coda error bad tauCurrentWindow:"+ tauCurrentWindow+ " ; reset to 1.0 seconds.");
            tauCurrentWindow = 1.;
        }
        tauFinal = tauCurrentWindow;

        windowCount++;
        goodWindowCount++;

        saveBufferValues(tauCurrentWindow, averagedAbsAmp);
        if (isFirstWindow) {
            firstGoodWindow = windowCount;
            isFirstWindow = false;
        }

        return false;
    }

    protected void saveBufferValues(double tauCurrentWindow, double averagedAbsAmp) {
        int idx = goodWindowCount - 1;
        windowWeight[idx]    = getWindowWeight(tauCurrentWindow, averagedAbsAmp);
        log10WindowAmp[idx]  =  windowWeight[idx]*MathTN.log10(averagedAbsAmp);
        log10WindowTime[idx] = -windowWeight[idx]*MathTN.log10(tauCurrentWindow);
        goodWindowTau[idx]   = tauCurrentWindow;
        goodWindowAmp[idx]   = averagedAbsAmp;
        if (debug) System.out.println("  CodaGenerator saveBufferValues fit goodWindowCount: " +
            goodWindowCount + " " + tauCurrentWindow + " " +  averagedAbsAmp + " Wt: " + windowWeight[idx]);
    }

    protected double getWindowWeight(double tauCurrentWindow, double averagedAbsAmp) {
        return 1.0;
    }

    private boolean extrapolateTau(boolean forceExtrapolation) {
    // on entry tauFinal is 0, if it hasn't been set by observing a legitimate amp window cutoff time

        char codaType = codaDesc.charAt(0); // Extrapolate a 'noise' or 'short' coda type
        if ( (codaType != NOISE_CODA_TYPE && codaType != SHORT_CODA_TYPE) && ! forceExtrapolation) return false;

        // If the free l1fit was good, use that to extrapolate the coda */
        if( goodWindowCount >= 3 && rms < 0.3  && aFree > 0.0  && qFree > 1.0  && qFree < 5.0 ) {
          tauFinal = Math.pow(10., (aFree - MathTN.log10(channelCutoffAmp))/qFree) + 0.5;
          codaDesc.setCharAt(2,TAU_EXTRAPOLATED_FREE);  // qFree extrapolation
          return true;
        }
        // if at least one on-scale coda amp, use fixed l1fit information to extrapolate tau
        else if ( goodWindowCount >= 1 && aFix > 0.0 && qFix > 1.0 ) {
          tauFinal =  Math.pow(10., (aFix - MathTN.log10(channelCutoffAmp))/qFix) + 0.5;
          codaDesc.setCharAt(2,TAU_EXTRAPOLATED_FIXED); // qFix  extrapolation
          return true;
        }
        // Otherwise, can't extrapolate the coda properly; keep the original tau and set descriptor flag
        else {
          codaDesc.setCharAt(2,CODA_CALC_INVALID_TYPE); // unknown, extrapolation failure, reject
          return false;
        }
    }

// Method is invoked at end coda data collection for each time series.
    private boolean doCodaStatistics() {

        tauFinal             = Math.abs(tauFinal);  // when is it less than zero if starting idx is negative?
        aveAbsAmpAtEndOfCoda = (double)Math.round(aveAbsAmpAtEndOfCoda);

        if (goodWindowCount < 1) {    // no valid coda windows
            exitStatusString = "noGoodWindows";
            exitStatus = -2;
            return false;
        }
        else if (qFix < .01) {       // why, if ever true?
            exitStatusString = "badQFix";
            exitStatus = -3;
            return false;
        }

        calcL1Norm();

        // forceExtrap == true -> any type, false -> only "short" or "noisy" coda
        //extrapolated values seem to be smaller than normal termination cutoff?
        //extrapolateTau(forceExtrap);
        if (extrapolateTau(forceExtrap) && debug) System.out.println("   doCodaStatistics() EXTRAPOLATED tauFinal= " + tauFinal);

        return true;
    }

// l1 norm fit of data to determine Q and A parameters 
    protected void calcL1Norm() {
        double smallestResidualSum = 999999.;
        for (int idx = 0; idx < goodWindowCount; idx++) {  // solve for Afix using Qfix
            double Q0 = qFix;
            double A0 = (log10WindowAmp[idx] - log10WindowTime[idx] * Q0)/windowWeight[idx];
            double sum = 0.;

            for (int idx3 = 0; idx3 < goodWindowCount; idx3++) {
                double residual = windowWeight[idx3]*A0 + log10WindowTime[idx3]*Q0 - log10WindowAmp[idx3];
                sum += Math.abs(residual);
            }

            if (sum <= smallestResidualSum) {
                aFix = A0;
                smallestResidualSum = sum;
            }
        }

        if (goodWindowCount >= 2) { // solve for Afree and Qfree

            smallestResidualSum = 999999.;
            for (int idx = 1; idx < goodWindowCount; idx++ ) {
                for (int idx2 = 0; idx2 < idx; idx2++) {
                    double numerator = log10WindowAmp[idx]*log10WindowTime[idx2] - log10WindowAmp[idx2]*log10WindowTime[idx];
                    double denominator = windowWeight[idx]*log10WindowTime[idx2] - windowWeight[idx2]*log10WindowTime[idx];
                    if (denominator < 0.001) denominator = 0.001;
                    double A0 = numerator/denominator;
                    double Q0 = (log10WindowAmp[idx] - windowWeight[idx]*A0)/log10WindowTime[idx];
                    double sum = 0.;
                    for (int idx3 = 0; idx3 < goodWindowCount; idx3++) {
                        double residual = windowWeight[idx3]*A0 + log10WindowTime[idx3]*Q0 - log10WindowAmp[idx3];
                        sum += Math.abs(residual);
                    }
                    if (sum <= smallestResidualSum) {
                        aFree = A0;
                        qFree = Q0;
                        smallestResidualSum = sum;
                    }
                }
            }
        }

        if (goodWindowCount > 2) {
            rms = smallestResidualSum/(goodWindowCount-2);       // save l1-fit as rms for qfree and afree
            quality = 1.;
        }
        else {
            if (goodWindowCount == 2) {
                rms = smallestResidualSum;
                quality = .5;
            }
            else {
                rms = 0.0;
                quality = .25;
            }
        }
    }

/** Print input setting to System.out*/
    public void printInputSettings() {
        System.out.println(inputToString());
    }

/** Print last results to System.out*/
    public void printResults() {
        System.out.println(outputToString());
    }

/** Print good time amp pair values to System.out*/
    public void printTimeAmps() {
        System.out.println(timeAmpsToString());
    }

/** Return String of any good time amp values.*/
    public String timeAmpsToString() {
        TimeAmp [] ta = getGoodWindowTimeAmps();
        int size = ta.length;
        if (size <= 0) return null;
        StringBuffer sb = new StringBuffer(512);
        for (int idx = 0; idx < size; idx++) {
            sb.append(" ");
            sb.append(ta[idx].toString(','));
            if (idx % 5 == 0) sb.append("\n");
        }
        return sb.toString();
    }

    public String inputToString() {
        StringBuffer sb = new StringBuffer(256);
        sb.append(" minWin: ").append(minGoodWindowsToEndCalcOnClipping);
        sb.append(" maxWin: ").append(maxGoodWindowCount);
        sb.append(" winSize: ").append(windowSize);
        sb.append(" nsamp: ").append(lastSampleSCodaIdx+1);
        sb.append(" sps: ").append(secsPerSample);
        sb.append(" SmP: ").append(secsPastPTimeAtStartingIdx);
        sb.append(" startSNR: ").append(codaStartSignalToNoiseRatio);
        sb.append(" endSNR: ").append(codaCutoffSignalToNoiseRatio);
        sb.append(" passNSR: ").append(passThruNoiseDecayAmpRatio);
        sb.append(" clpReset: ").append(resetOnClipping);
        sb.append(" olap: ").append(overlapStartingWindows);
        if (overlapStartingWindows) sb.append(" olapMax: ").append(overlapWindowMax);
        return sb.toString();
    }

    public String outputToString() {
        StringBuffer sb = new StringBuffer(256);
        //if (fmt == null) fmt = new Concat();
        sb.append("    ").append(codaDesc).append(" ");
        Concatenate.format(sb, quality, 2, 1);
        Concatenate.format(sb, tauFinal, 4, 1);
        Concatenate.format(sb, firstWindowAveAbsAmp, 9, 0);
        Concatenate.format(sb, aveAbsAmpAtEndOfCoda, 9, 0);
        Concatenate.format(sb, goodWindowCount, 4);
        Concatenate.format(sb, badWindowCount,  4);
        Concatenate.format(sb, firstGoodWindow, 4);
        Concatenate.format(sb, rms, 3, 2);
        Concatenate.format(sb, qFix, 3, 2);
        Concatenate.format(sb, qFree, 3, 2);
        Concatenate.format(sb, aFree, 3, 2);
        Concatenate.format(sb, aFix, 3, 2);
        sb.append(" ");
        String filterStr = (isFilterEnabled() && hasFilter()) ?  "true   " : "false  ";
        sb.append(filterStr);
        sb.append(getExitStatusString());
        return sb.toString();
    }
    public String outputToLabeledString() {
        StringBuffer sb = new StringBuffer(256);
        sb.append(" status: ").append(exitStatus);
        sb.append(" desc: ").append(codaDesc);
        sb.append(" qual: ").append(quality);
        sb.append(" tau: ").append(tauFinal);
        sb.append(" startAmp: ").append(firstWindowAveAbsAmp);
        sb.append(" endAmp: ").append(aveAbsAmpAtEndOfCoda);
        sb.append(" ngood: ").append(goodWindowCount);
        sb.append(" nbad: ").append(badWindowCount);
        sb.append(" 1stGoodAt: ").append(firstGoodWindow);
        sb.append(" rms: ").append(rms);
        sb.append(" QFix: ").append(qFix);
        sb.append(" QFree: ").append(qFree);
        sb.append(" AFree: ").append(aFree);
        sb.append(" AFix: ").append(aFix);
        sb.append(" desc: ").append(getExitStatusString());
        return sb.toString();
    }
    public String outputHeaderString() {
        StringBuffer sb = new StringBuffer(132);
        sb.append("            phase qual   tau startAmp    endAmp good bad 1st   rms  QFix  QFree AFree AFix Filter   Status");
        return sb.toString();
    }

// Methods to  initialize global data values
/** Returns last results in input  Coda object */
    public Coda setCodaResults(Coda aCoda) {
        return resultsToCoda(aCoda) ;
    }

/** Returns maximum value allowed for aveAbsAmpCurrentCodaWindow/aveAbsAmpLastGoodCodaDecayWindow.
* If value is exceeded (e.g. a noise burst or new phase arrival) the timeseries scan of coda is terminated.
 */
    public double getPassThruNoiseDecayAmpRatio() {
        return passThruNoiseDecayAmpRatio;
    }

/** Sets maximum value allowed for aveAbsAmpCurrentCodaWindow/aveAbsAmpLastGoodCodaDecayWindow. */
    public void setPassThruNoiseDecayAmpRatio(double passThruNoiseDecayAmpRatio) {
        if (passThruNoiseDecayAmpRatio > 0.) this.passThruNoiseDecayAmpRatio = passThruNoiseDecayAmpRatio;
    }

/** Returns Qfix */
    public double getQFix() {
        return qFix;
    }

/** Sets QFix value. */
    public void setQFix(double qFix) {
        if (qFix > 0.) this.qFix = qFix;
    }

    public void setResetOnClipping(boolean tf) {
        resetOnClipping = tf;
    }

    public boolean isResetOnClipping() {
        return resetOnClipping;
    }

/** Returns the minimum number of good coda time series windows needed to terminate the coda calculation 
*   if the current window amplitude is above the clipping level.
*/
    public int getMinGoodWindows() {
        return minGoodWindowsToEndCalcOnClipping;
    }

    public void setMinGoodWindows(int numberOfWindows) {
        this.minGoodWindowsToEndCalcOnClipping = numberOfWindows;
    }
 
/** Returns maximum number of valid coda time series windows allowed before coda calculation is terminated.*/
    public int getMaxGoodWindows() {
        return maxGoodWindowCount;
    }

/** Sets maximum number of valid coda time series windows allowed before coda calculation is terminated.*/
    public void setMaxGoodWindows(int maxCount) {
        if (maxCount > this.maxGoodWindowCount) initBuffers(maxCount);
        else this.maxGoodWindowCount = maxCount;  // need for lower count resets - aww 09/21/2006
    }

/** Gets maximum number coda seconds duration for coda cutoff */
    public double getMaxCodaDurationSecs() {
        return getWindowStartingSampleSecsOffset(maxGoodWindowCount) + windowSize;
    }

/** Returns size in seconds of the time series window over which absolute amplitude values are averaged.*/
    public double getWindowSize() {
        return windowSize;
    }

/** Sets size in seconds of the time series window over which to average the absolute amplitude values.*/
    public void setWindowSize(double windowSize) {
        this.windowSize = windowSize;
    }

/** Returns count of 2s windows that overlap at start of scan, i.e. 1s offset between window centers.*/
    public int getOverlapWindowMax() {
        return overlapWindowMax;
    }

/** Set count of 2s windows that overlap at start of scan, i.e. a 1s offset between window centers.
 *  Input seconds must be even number, value is set to input 2*((count+1)/2). */
    public void setOverlapWindowMax(int count) {
        overlapWindowMax = 2 * ((count+1)/2);
    }

    public void setOverlapStartingWindows(boolean tf) {
        overlapStartingWindows = tf;
    }

    public boolean getOverlapStartingWindows() {
        return overlapStartingWindows;
    }

/** Valid coda window counting begins if averagedAbsoluteAmpofWindow/averagedAbsoluteValueOfNoise > snrToStart.  */
    public void setCodaStartSignalToNoiseRatio(double snrToStart) {
        codaStartSignalToNoiseRatio = snrToStart;
    }

    public double getCodaStartSignalToNoiseRatio() {
        return codaStartSignalToNoiseRatio ;
    }

/** Valid coda window counting ends if averagedAbsoluteAmpofWindow/averagedAbsoluteValueOfNoise < snrToEnd.  */
    public void setCodaCutoffSignalToNoiseRatio(double snrToEnd) {
        codaCutoffSignalToNoiseRatio = snrToEnd;
    }

    public double getCodaCutoffSignalToNoiseRatio() {
        return codaCutoffSignalToNoiseRatio ;
    }

/** Returns the average of the absolute demeaned values of the time series amplitudes before the coda start. */
    private static double calcStartLTA(float [] timeSeriesAmps, int numberOfSamples, double bias, int pTimeIdx) {

        int nSamples = (numberOfSamples < timeSeriesAmps.length) ? numberOfSamples : timeSeriesAmps.length;
        int startIdx = pTimeIdx - nSamples;
        if (startIdx < 0) startIdx = 0;
        int endIdx = startIdx + nSamples - 1;

        // Below logic doesn't not work when using a "late" time-tear segment, missing pre-event, e.g. it's pTimeIdx=0
        // Removed 2011/03/18 -aww
        //if (endIdx > pTimeIdx) {
        //    nSamples = pTimeIdx - startIdx + 1;
        //    endIdx = pTimeIdx;
        //}
        //if ( nSamples <= 0 ) {
        //    System.err.println("AbstractCodaGenerator.calcStartLTA nSamples <= 0, resetting endIdx past coda start index");
        //    endIdx = startIdx+numberOfSamples - 1;
        //}

        double sum = 0.;
        nSamples = (endIdx-startIdx)+1;
        for (int idx = startIdx; idx <= endIdx; idx++) {
            sum += Math.abs((double) timeSeriesAmps[idx] - bias); 
        }
        return sum/nSamples;
    }

/** Returns the average of the absolute demeaned values of the time series amplitudes at end of timeseries. */
    private static final double calcEndLTA(float [] timeSeriesAmps, int numberOfSamples, double bias) {

        int nSamples = (numberOfSamples < timeSeriesAmps.length) ? numberOfSamples : timeSeriesAmps.length;
        if ( nSamples <= 0 ) {
            System.err.println("AbstractCodaGenerator.calcEndLTA nSamples <= 0, unable to calculate LTA");
            return (double) Long.MAX_VALUE;
        }

        int startIdx = timeSeriesAmps.length - nSamples;
        if (startIdx < 0) startIdx = 0;
        int endIdx = startIdx + nSamples - 1;

        double sum = 0.;
        nSamples = (endIdx-startIdx)+1;
        for (int idx = startIdx; idx <= endIdx; idx++) {
            sum += Math.abs((double) timeSeriesAmps[idx] - bias); 
        }

        return sum/nSamples;
    }

/** Returns the mean of the values of the time series amplitudes over the specified range. */
    private static final double calcBias(float [] timeSeriesAmps, int startSampleIdx, int numberOfSamples) {
        int endSampleIdx = startSampleIdx + numberOfSamples;
        if (endSampleIdx > timeSeriesAmps.length) endSampleIdx = timeSeriesAmps.length; // reset samples to array length -aww 2008/06/30
        if (startSampleIdx < 0 || endSampleIdx < startSampleIdx || numberOfSamples < 0 )
            throw new IllegalArgumentException("starting or ending index is outside of input array bounds: " + 
                    startSampleIdx + " numberOfSamples: " + numberOfSamples + " array length: " + timeSeriesAmps.length);

        if (numberOfSamples == 0) return 0.;

        double sum = 0.;

        for (int idx = startSampleIdx; idx < endSampleIdx; idx++) {
            sum += (double) timeSeriesAmps[idx]; 
        }

        return sum/(endSampleIdx-startSampleIdx); // note endSampleIdx is one past last desired array sample (last-first+1)
    }

/** Returns bias LTA window size in seconds. */
    public double getBiasLTASecs() {
        return biasLTASecs;
    }

/** Sets bias LTA window size in seconds, cannot be set less than 5 seconds. */
    public void setBiasLTASecs(double secs) {
        biasLTASecs = Math.max(5.0, secs);
    }

/** Returns a status value set by the coda calculations methods. */
    public int getExitStatus() {
        return exitStatus;
    }

/** Returns the String value of the exit code set by the coda calculations methods. */
    public String getExitStatusString() {
        return exitStatusString;
    }
}
