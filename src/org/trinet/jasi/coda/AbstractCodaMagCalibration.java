package org.trinet.jasi.coda;
import org.trinet.jasi.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

/**
* Abstract type for a generic coda calibration for a station channel.
*/
public abstract class AbstractCodaMagCalibration extends AbstractAmpMagCalibration {
    // Sum of abs ave of window samples = 63% of sample clipping for sine wave at 10 hz
    // for example reject windows with amps > .63*2048 =  1290 counts ?

    // below 1000mv vs. 800mv => 655L cnts see TIMIT$DEV$CHAR; 360L old cusp value from MCA.for
    public static double   CODA_CLIP_AMP_E_DEFAULT = 819.2;
    public static double   DEFAULT_CODA_CLIP_PEAK_AMP_RATIO = 0.4;
    public static double   CODA_CLIP_AMP_H_DEFAULT = Math.round(DEFAULT_CODA_CLIP_PEAK_AMP_RATIO * MAX_AMP_H_DEFAULT);

    public static double   CODA_CUTOFF_AMP_E_DEFAULT = 49.14; // EW or CUSP analog
    public static double   CODA_CUTOFF_AMP_H_DEFAULT = 150.;  // nice round number

    protected DataDouble codaCutoffAmp = new DataDouble(); // averaged amplitude counts for coda duration termination
    protected DataDouble codaClipAmp   = new DataDouble(); // min averaged amplitude counts for clipped signal.

    protected int codaCutoffAmpUnits = CalibrUnits.AVG_ABS_COUNTS;
    protected int codaClipAmpUnits   = CalibrUnits.COUNTS; // subclasses should set as stored in database table


    protected AbstractCodaMagCalibration() {
        this(null, null);
    }

    protected AbstractCodaMagCalibration(ChannelIdIF id) {
        this(id, null);
    }
    protected AbstractCodaMagCalibration(ChannelIdIF id, DateRange dateRange) {
        super(id, dateRange);
    }
    protected AbstractCodaMagCalibration(ChannelIdIF id, DateRange dateRange, double value, String corrType) {
        this(id, dateRange, value, corrType, null, null);
    }
    protected AbstractCodaMagCalibration(ChannelIdIF id, DateRange dateRange, double value, String corrType,
                    String corrFlag) {
        super(id, dateRange, value, corrType, corrFlag, null);
    }
    protected AbstractCodaMagCalibration(ChannelIdIF id, DateRange dateRange, double value, String corrType,
                    String corrFlag, String authority) {
        super(id, dateRange, value, corrType, corrFlag, authority);
    }

    public boolean hasMagCorr(MagnitudeAssocJasiReadingIF jr) {
      return ( (jr instanceof org.trinet.jasi.Coda) && ! corr.isNull()
               && isValidFor(jr, jr.getLookUpDate())
             );
    }
    public boolean hasGainCorr(MagnitudeAssocJasiReadingIF jr) {
      return ( (jr instanceof org.trinet.jasi.Coda) && ! gainCorr.isNull()
                && isValidFor(jr, jr.getLookUpDate())
             );
    }

    public double getDefaultCodaClipAmp() {
        String chan = channelId.getSeedchan();
        String sub = (chan.length() == 0) ? "_" : chan.substring(0,1);
        if (codaClipAmpUnits == CalibrUnits.AVG_ABS_COUNTS) { // bug? was codaCutoffAmpUnits -aww 2009/03/18
          if (sub.equals("E") || sub.equals("S")) { // like "EHZ" and "SHZ" ?
            return CODA_CLIP_AMP_E_DEFAULT;
          }
          else {
            return CODA_CLIP_AMP_H_DEFAULT;
          }
        }
        else {
          if (sub.equals("E")) {
            return CODA_CLIP_AMP_E_DEFAULT/DEFAULT_CODA_CLIP_PEAK_AMP_RATIO;
          }
          else {
            return CODA_CLIP_AMP_H_DEFAULT/DEFAULT_CODA_CLIP_PEAK_AMP_RATIO;
          }
        }
    }
    public double getCodaClipAmpValue() {
        // returns "default" values for "E" and "H" types when null, should they return 0 or ?  -aww
        //return (codaClipAmp.isNull()) ? getDefaultCodaClipAmp() : codaClipAmp.doubleValue(); // removed 2010/05/20 
        return (codaClipAmp.isNull()) ? 0. : codaClipAmp.doubleValue(); // return 0. if unknown, -aww added 2010/05/20
    }
    public void setCodaClipAmp(double clipAmp) {
        codaClipAmp.setValue(clipAmp);
    }
    public void setCodaClipAmpUnits(int units) {
        codaClipAmpUnits = units;
    }
    public int getCodaClipAmpUnits() {
        return codaClipAmpUnits;
    }

    public double getDefaultCodaCutoffAmp() {
        String chan = channelId.getSeedchan();
        String sub = (chan.length() == 0) ? "_" : chan.substring(0,1);
        if (codaCutoffAmpUnits == CalibrUnits.AVG_ABS_COUNTS) {
          if (sub.equals("E") || sub.equals("S")) { // like "EHZ" and "SHZ" ?
            return CODA_CUTOFF_AMP_E_DEFAULT;
          }
          else {
            return CODA_CUTOFF_AMP_H_DEFAULT;
          }
        }
        else {
          if (sub.equals("E")) {
            return CODA_CUTOFF_AMP_E_DEFAULT/DEFAULT_CODA_CLIP_PEAK_AMP_RATIO;
          }
          else{
            return CODA_CUTOFF_AMP_H_DEFAULT/DEFAULT_CODA_CLIP_PEAK_AMP_RATIO;
          }
        }
    }
    public double getCodaCutoffAmpValue() {
        // returns "default" values for "E" and "H" types when null, should they return 0 or ?  -aww
        //return  (codaCutoffAmp.isNull()) ? getDefaultCodaCutoffAmp() : codaCutoffAmp.doubleValue(); // removed 2010/05/20 
        return  (codaCutoffAmp.isNull()) ? 0. : codaCutoffAmp.doubleValue(); // return 0. if unknown, -aww added 2010/05/20
    }
    public void setCodaCutoffAmp(double cutoffAmp) {
        codaCutoffAmp.setValue(cutoffAmp);
    }
    public void setCodaCutoffAmpUnits(int units) {
        codaCutoffAmpUnits = units;
    }
    public int getCodaCutoffAmpUnits() {
        return codaCutoffAmpUnits;
    }

/**
* Assigns the data member values of the input instance to this instance
* Returns false if input is null or input is not an instance of this.
*/
    public boolean copy(ChannelDataIF calibr) {
        return ( getClass().isInstance(calibr) ) ?
            copy((AbstractCodaMagCalibration) calibr) : false;
    }

    public boolean copy(AbstractCodaMagCalibration calibr) {
        if ( ! super.copy(calibr) ) return false;
        this.codaCutoffAmp.setValue(calibr.codaCutoffAmp);
        this.codaClipAmp.setValue(calibr.codaClipAmp);
        return true;
    }

// override super methods:
    public String toString() {
        StringBuffer sb = new StringBuffer(132);
        sb.append(super.toString());
        sb.append(" ").append(codaCutoffAmp.toStringSQL());
        sb.append(" ").append(codaClipAmp.toStringSQL());
        return sb.toString();
    }

    public boolean equals(Object object) {
        if (! super.equals(object)) return false;
        AbstractCodaMagCalibration calibr = (AbstractCodaMagCalibration) object;
        return ( codaCutoffAmp.equals(calibr.codaCutoffAmp) &&
                 codaClipAmp.equals(calibr.codaClipAmp) );
    }

    public Object clone() {
        AbstractCodaMagCalibration calibr = (AbstractCodaMagCalibration) super.clone();
        calibr.codaCutoffAmp  = (DataDouble) this.codaCutoffAmp.clone();
        calibr.codaClipAmp  = (DataDouble) this.codaClipAmp.clone();
        return calibr;
    }

    public String toNeatString() {
      StringBuffer sb = new StringBuffer(200);
      sb.append(super.toNeatString());
      Format df1 = new Format("%10.2f");
      sb.append(df1.form(codaCutoffAmp.doubleValue())).append(" ");
      sb.append(df1.form(codaClipAmp.doubleValue())).append(" ");
      return sb.toString();
    }

    public String getNeatHeader() {
        return super.getNeatHeader() + " codaCutoff   codaClip";
    }
}
