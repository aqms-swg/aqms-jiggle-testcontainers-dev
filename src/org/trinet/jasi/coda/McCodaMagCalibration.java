package org.trinet.jasi.coda;

import java.sql.*;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

/**
* Abstract type for coda calibrations origininating from TriNet using NCDC schema.
*/
public abstract class McCodaMagCalibration extends AbstractCodaMagCalibration implements Cloneable {

    public static final double QFIX_DEFAULT    = 1.8 ;
    public static final double AFIX_DEFAULT    = 0.;
    public static final double SLOPE_DEFAULT   = 0.;

    protected DataLong   neq    = new DataLong();
    protected DataDouble slope  = new DataDouble();
    protected DataDouble aFix   = new DataDouble();
    protected DataDouble qFix   = new DataDouble();
    protected DataDouble aFree  = new DataDouble();
    protected DataDouble qFree  = new DataDouble();

    protected McCodaMagCalibration() {
        this(null, null);
    }

    protected McCodaMagCalibration(ChannelIdIF id) {
        this(id, null);
    }
    protected McCodaMagCalibration(ChannelIdIF id, DateRange dateRange) {
        super(id, dateRange);
        corrType.setValue(CorrTypeIdIF.MC);
    }
    protected McCodaMagCalibration(ChannelIdIF id, DateRange dateRange, double value) {
        this(id, dateRange, value, null, null);
    }
    protected McCodaMagCalibration(ChannelIdIF id, DateRange dateRange, double value,
                    String corrFlag) {
        this(id, dateRange, value, corrFlag, null);
    }
    protected McCodaMagCalibration(ChannelIdIF id, DateRange dateRange, double value,
                    String corrFlag, String authority) {
        super(id, dateRange, value, CorrTypeIdIF.MC, corrFlag, authority);
    }

    public boolean hasMagCorr(MagnitudeAssocJasiReadingIF jr) {
      return ( super.hasMagCorr(jr)) ? ((org.trinet.jasi.Coda) jr).isMcType() : false;
    }

    /*
    //public Double getMagCorr(MagnitudeAssocJasiReadingIF jr) {
      //return (hasMagCorr(jr)) ?
            // new Double(-aFix.doubleValue()) : null;
            // + (qFix.doubleValue() - aCoda.qFix.doubleValue())*slope.doubleValue()) : null;
    //}
    */

    protected long getNumberOfValuesFit() { return neq.longValue(); }
    public double getAFix()    { return aFix.doubleValue(); }
    public double getQFix()    { return qFix.doubleValue(); }
    public double getAFree()   { return aFree.doubleValue(); }
    public double getQFree()   { return qFree.doubleValue(); }
    public double getSlope()   { return slope.doubleValue(); }

    protected void setLineFitParms(long neq, double afix, double qfix, double afree, double qfree, double slope) {
        if (neq <= 0) throw new IllegalArgumentException("number of observation fit must be >= 0");
        this.neq.setValue(neq);
        this.aFix.setValue(afix);
        this.qFix.setValue(qfix);
        this.aFree.setValue(afree);
        this.qFree.setValue(qfree);
        this.slope.setValue(slope);
    }

    public static double getDefaultQFix() {
        return QFIX_DEFAULT;
    }
    public static double getDefaultAFix() {
        return AFIX_DEFAULT;
    }
    public static double getDefaultSlope() {
        return SLOPE_DEFAULT;
    }

    public static McCodaMagCalibration create() {
        return create(JasiObject.DEFAULT);
    }
    public static final McCodaMagCalibration create(int schemaType) {
        return create(JasiFactory.suffix[schemaType]);
    }
    public static final McCodaMagCalibration create(String suffix) {
        return (McCodaMagCalibration) JasiObject.newInstance("org.trinet.jasi.coda.McStaCorrCodaMagCalibrView", suffix);
    }

    public AbstractChannelCalibration createDefaultCalibration(ChannelIdIF chan) {
        McCodaMagCalibration calibr = (McCodaMagCalibration) McCodaMagCalibration.create();
        calibr.channelId = (ChannelIdIF) chan.clone(); // try test for cloneable?
        calibr.corr.setValue(getDefaultCorrection());
        calibr.maxAmp.setValue(getDefaultMaxAmp());
        calibr.clipAmp.setValue(getDefaultClipAmp());
        calibr.gainCorr.setValue(getDefaultGainCorr());
        calibr.codaClipAmp.setValue(getDefaultCodaClipAmp());
        calibr.codaCutoffAmp.setValue(getDefaultCodaCutoffAmp());
        calibr.qFix.setValue(getDefaultQFix());
        calibr.aFix.setValue(getDefaultAFix());
        calibr.slope.setValue(getDefaultSlope());
        return calibr;
    }

    /**
    * Assigns the data member values of the input instance to this instance
    * Returns false if input is null or input is not an  instance of McCodaMagCalibration.
    */
    public boolean copy(ChannelDataIF calibr) {
        return (getClass().isInstance(calibr)) ?
           copy((McCodaMagCalibration) calibr) : false ;
    }

    public boolean copy(McCodaMagCalibration calibr) {
        if (calibr == null) return false;
        McCodaMagCalibration input = (McCodaMagCalibration) calibr;
        if (! super.copy(input)) return false;
        this.neq       = (DataLong) input.neq.clone();
        this.aFix      = (DataDouble) input.aFix.clone();
        this.qFix      = (DataDouble) input.qFix.clone();
        this.aFree     = (DataDouble) input.aFree.clone();
        this.qFree     = (DataDouble) input.qFree.clone();
        this.slope     = (DataDouble) input.slope.clone();
        return true;
    }

// override super methods:
    public boolean equals(Object object) {
        if ( ! super.equals(object) ) return false;
        McCodaMagCalibration calibr = (McCodaMagCalibration) object;
        return ( aFix.equals(calibr.aFix) &&
                 qFix.equals(calibr.qFix) &&
                 slope.equals(calibr.slope)
               );
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(256);
        sb.append(super.toString());
        sb.append(" ").append(neq.toStringSQL());
        sb.append(" ").append(aFix.toStringSQL());
        sb.append(" ").append(qFix.toStringSQL());
        sb.append(" ").append(aFree.toStringSQL());
        sb.append(" ").append(qFree.toStringSQL());
        sb.append(" ").append(slope.toStringSQL());
        return sb.toString();
    }

    public String toNeatString() {
        StringBuffer sb = new StringBuffer(256);
        sb.append(super.toNeatString());
        sb.append(super.toString());
        Format df1 = new Format("%6d");
        sb.append(" ").append(df1.form(neq.longValue()));
        Format df2 = new Format("%7.2f");
        sb.append(" ").append(df2.form(aFix.doubleValue()));
        sb.append(" ").append(df2.form(qFix.doubleValue()));
        sb.append(" ").append(df2.form(aFree.doubleValue()));
        sb.append(" ").append(df2.form(qFree.doubleValue()));
        sb.append(" ").append(df2.form(slope.doubleValue()));
        return sb.toString();
    }
    public String getNeatHeader() {
        return super.getNeatHeader() + " count    afix    qfix   afree   qfree   slope";
    }

    public Object clone() {
        McCodaMagCalibration calibr = (McCodaMagCalibration) super.clone();
        calibr.neq        = (DataLong)   this.neq.clone();
        calibr.aFix       = (DataDouble) this.aFix.clone();
        calibr.qFix       = (DataDouble) this.qFix.clone();
        calibr.aFree      = (DataDouble) this.aFree.clone();
        calibr.qFree      = (DataDouble) this.qFree.clone();
        calibr.slope      = (DataDouble) this.slope.clone();
        return calibr;
    }

} // end of class McCodaMagCalibration
