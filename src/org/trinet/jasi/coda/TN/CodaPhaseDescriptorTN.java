package org.trinet.jasi.coda.TN;
import org.trinet.jasi.coda.*;

public class CodaPhaseDescriptorTN extends CodaPhaseDescriptor {
    /** Initializes to CodaType.P, CodaDurationType.A  for Mc calcuation with no phase description remark.*/
    public CodaPhaseDescriptorTN() {
        super(CodaType.P, CodaDurationType.A, null);
    }
    /** Initializes to CodaType.P, CodaDurationType.A  for Mc calcuation with input phase description.*/
    public CodaPhaseDescriptorTN(String description) {
        super(CodaType.P, CodaDurationType.A, description);
    }
    /** Initializes to CodaType.P, with input durationType and phase description.*/
    public CodaPhaseDescriptorTN(CodaDurationType durationType, String description) {
        super(CodaType.P, durationType, description);
    }
}

