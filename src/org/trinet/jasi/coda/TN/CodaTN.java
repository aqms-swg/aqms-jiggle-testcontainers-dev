//TODO: Migrate common "reading" method function up to abstracted TN class
package org.trinet.jasi.coda.TN;

import java.util.*;
import java.sql.*;

import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;
import org.trinet.jasi.TN.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.table.*;
import org.trinet.util.*;

//  CodaTN.java
/**
* The CodaTN class contains data which relate the amplitude time decay
* of a seismic event's S-wave recorded by a station channel to an event magnitude.
<pre>
  There are two source states for Coda objects:
        1) those initialized with data read from the database, see getByXXX(...) methods.
           By convention, the Coda table rows are not deleted or updated,
           but a new AssocCoO or AssocCoM table row may be created.

        2) those newly created by an application, see create(...) methods.
           To preserve this data, it must be inserted into the Coda table,
           the AssocCoO table and if magnitude is defined, the AssocCoM table.
</pre>
*/
abstract public class CodaTN extends org.trinet.jasi.Coda implements DbMagnitudeAssocIF, DbReadableJasiObjectIF {

    // if not static init with this instance so access by getBy(...) preserves correct joinType state for parsing resultset 
    protected static JasiDbReader jasiDataReader =  null;  // lazy instatiate in subclass only if needed, new JasiDbReader(); // if not static

    /* init of database reader after instantiation was moved to jasi.TN.CodaTN a subclass of this class
    protected static JasiDbReader jasiDataReader = new JasiDbReader("org.trinet.jasi.TN.Coda");
    protected static JasiDbReader jasiDataReader =  new JasiDbReader();
    {
       jasiDataReader.setFactoryClassName(getClass().getName());
    }
    */

    /** Set flag true to enable debug output Strings printed to System.out. */
    protected static boolean debug = false;

    /**
    * Value determines which database operations are performed to preserve
    * the data by the commit() method.
    * True implies instance data were orginally initialized from a database
    * (see  getByXXX() methods).
    * @see #commit()
    */
    protected boolean fromDbase = false;

    /** Value indicates data were initialized by a join of Coda and an AssocCoO table and AssocCoM. */
    protected static final int Type_AssocNone = 0;
    /** Value indicates data were initialized by a join of Coda and an AssocCoO table. */
    protected static final int Type_AssocCoO = 1;
    /** Value indicates data were initialized by a join of Coda and an AssocCoM table. */
    protected static final int Type_AssocCoM = 2;
    /** Value indicates data were initialized by a join of Coda and an AssocCoO table and AssocCoM. */
    protected static final int Type_Assoc = 3;

    /** Value indicates data were initialized by a join of Coda and an associated table. */
    protected int joinType = Type_AssocNone;

    private static final String psSqlPrefix=
        "SELECT c.COID,c.STA,c.NET,c.AUTH,c.SUBSOURCE,c.CHANNEL,c.CHANNELSRC,c.SEEDCHAN," + // 1-8
        "c.LOCATION,c.AFIX,c.AFREE,c.QFIX,c.QFREE,c.TAU,c.NSAMPLE,c.RMS,c.DURTYPE,c.UNITS," + // 9-18
        "c.TIME1,c.AMP1,c.TIME2,c.AMP2,c.QUALITY,c.RFLAG,TRUETIME.getEpoch(c.DATETIME,'UTC'),c.ALGORITHM,c.WINSIZE";  // 19-27
        //"c.TIME1,c.AMP1,c.TIME2,c.AMP2,c.QUALITY,c.RFLAG,c.DATETIME,c.ALGORITHM,c.WINSIZE";  // 19-27

    private static final String psSqlPrefixByCoid = psSqlPrefix + " FROM CODA c WHERE (c.coid=?)";

    private static final String psSqlPrefixByMagid =  psSqlPrefix + 
        ",m.WEIGHT,m.IN_WGT,m.MAG,m.MAGRES,m.MAGCORR FROM CODA c, ASSOCCOM m WHERE (c.coid=m.coid) AND (m.magid=?)";

    private static final String psSqlPrefixByOrid = psSqlPrefix + 
        ",o.DELTA,o.SEAZ FROM CODA c, ASSOCCOO o WHERE (c.coid=o.coid) AND (o.orid=?)";

    private static final String psSqlPrefixByTime = psSqlPrefix +
        " FROM CODA c WHERE (c.DATETIME between TRUETIME.putEpoch(?,'UTC') AND TRUETIME.putEpoch(?,'UTC')) ORDER BY c.DATETIME";

    private static final String psSqlOrderSuffix = " ORDER BY c.NET,c.STA,c.SEEDCHAN,c.LOCATION,c.LDDATE DESC,c.COID";

    /*
    // The start of SQL queries to get all rows from Coda table. 
    protected static String sqlPrefix = "Select " + org.trinet.jdbc.table.Coda.QUALIFIED_COLUMN_NAMES + " from Coda ";
    // The start of SQL queries to get all rows associated with a origin, joins Coda, AssocCoO tables 
    protected static String sqlPrefixByOrigin = "Select " +
        org.trinet.jdbc.table.Coda.QUALIFIED_COLUMN_NAMES + "," +
        AssocCoO.QUALIFIED_COLUMN_NAMES +
        " from Coda, AssocCoO where (Coda.coid = AssocCoO.coid) ";
    // The start of SQL queries to get all rows associated with a magnitude, joins Coda, AssocCoM tables 
    protected static String sqlPrefixByMag = "Select "+
        org.trinet.jdbc.table.Coda.QUALIFIED_COLUMN_NAMES + "," +
        AssocCoM.QUALIFIED_COLUMN_NAMES+
        " from Coda, AssocCoM where (Coda.coid = AssocCoM.coid) ";
    // The start of SQL queries to get all rows associated with a magnitude, joins Coda, AssocCoO, and AssocCoM tables 
    protected static String sqlPrefixByAssoc = "Select "+
        org.trinet.jdbc.table.Coda.QUALIFIED_COLUMN_NAMES + "," +
        AssocCoO.QUALIFIED_COLUMN_NAMES+ "," +
        AssocCoM.QUALIFIED_COLUMN_NAMES+
        " from Coda, AssocCoO, AssocCoM where (Coda.coid = AssocCoO.coid) AND (Coda.coid = AssocCoM.coid) ";
    */

    //Origin identifier, sequence number of an Origin table row. 
    //protected DataLong orid = new DataLong();        // maps to Trinet schema AssocCoO.orid
    //protected DataLong magid = new DataLong();       // maps to Trinet schema AssocCoM.magid

    /** Coda identifier, sequence number of a Coda table row. */
    protected DataLong coid = new DataLong();        // maps to Trinet schema Coda.coid and AssocCoO.coid

// **** moved all below to Coda.java *****

    /** Coda duration calculated from P-wave onset  (e.g. use q,a fitting 60mv cutoff ampi). */
    //    protected DataDouble tau = new DataDouble();     // maps to Trinet schema Coda.tau

    /** Coda log(amp)-log(tau) line fit parameter when slope (q) is fixed and tau = 1. */
    //    protected DataDouble aFix = new DataDouble();    // maps to Trinet schema Coda.aFix

    /** Coda line slope parameter determined by best fit of known calibration set of
    earthquake qFree data for known magnitudes. */
    //    protected DataDouble qFix = new DataDouble();    // maps to Trinet schema Coda.qFix

    /** Coda log(amp)-log(tau) line fit parameter when slope (q) is free and tau=1. */
    //    protected DataDouble aFree = new DataDouble();   // maps to Trinet schema Coda.aFree

    /** Coda line slope parameter determined by least absolute residual line fit of
    single events windowed time amp data. */
    //    protected DataDouble qFree = new DataDouble();   // maps to Trinet schema Coda.qFree

    //
    // Make default constructor public for reflection security permissions;
    // most applications use factory create(...) methods to instantiate
    // an instance of network specific concrete subtype.
    //
    protected CodaTN() {
    }

// Methods
    public static void setDebug(boolean tf) {
        debug = tf;
        if (jasiDataReader != null) jasiDataReader.setDebug(debug);
    }

    /** Returns String identifying solution, magnitude, origin, and coda sequence number identifiers. */
    public String toAssocIdString() {
        StringBuffer sb = new StringBuffer(super.toAssocIdString());
        sb.append(" orid: ").append(getOridValue()); // orid.toStringSQL());
        return sb.toString();
    }

    /** Returns String identifying data source and input configuration parameters for coda fit algorithm. */
    protected String inParmsString() {
        StringBuffer sb = new StringBuffer(super.inParmsString());
        sb.append(" fromDB: ").append(fromDbase);
        if (fromDbase) {
            sb.append(" join: ");
            if (joinType == Type_AssocCoO) sb.append("AssocCoO");
            else if (joinType == Type_AssocCoM) sb.append("AssocCoM");
            else if (joinType == Type_Assoc) sb.append("Assoc");
            else sb.append("None");
        }
        return sb.toString();
    }

    /** Returns String describing the calculated coda fit parameters. */
    protected String outParmsString() {
        StringBuffer sb = new StringBuffer(super.outParmsString());
        sb.append('\n');
        sb.append(" aFix: ").append(aFix.toStringSQL());
        sb.append(" aFree: ").append(aFree.toStringSQL());
        sb.append(" qFix: ").append(qFix.toStringSQL());
        sb.append(" qFree: ").append(qFree.toStringSQL());
        sb.append(" tau: ").append(tau.toStringSQL());
        return sb.toString();
    }

    //need to implement a this from the top of the jasi hierarchy down AWW :   public Object clone() {

    /**
    * Changes the CodaPhaseDescriptor values if input is not equivalent to current descriptor.
    * if descriptor values are changed, descriptor.hasChanged() returns true. 
    * @throws ClassCastException input not instance of CodaPhaseDescriptorTN
    */
    public void changeDescriptor(CodaPhaseDescriptor cd) {
        if (! (cd instanceof CodaPhaseDescriptorTN)) throw new ClassCastException();
        descriptor.copy(cd);
    }

    /**
    * Action taken will depend on the initialization status of the coda (fromDbase == true).
    * This state controls whether DataSource data is created, deleted, modified, or unchanged.
    * To enable writing to the DataSource, DataSource.setWriteBackEnabled(true) must be invoked BEFORE data is read.
    * Because deleting a Coda table row in the database would possibly invalidate an existing
    * coda associations, this implementation writes new AssocCoO and AssocCoM table rows
    * to preserve changes to the Coda data.
    * @see DataSource#setWriteBackEnabled(boolean)
    * @see DataSource#isWriteBackEnabled()
    */
    public boolean commit() {

        if (! DataSource.isWriteBackEnabled()) return false;
        setDebug(JasiDatabasePropertyList.debugTN); 
        if (debug) {
            System.out.println("DEBUG CodaTN commit() fromDB:"+fromDbase+" "+toNeatString());
            System.out.println(toString());
        }

        // Reading is deleted or not associated! So don't write new rows.
        // Note we NEVER actually delete a row, just don't write it out
        // nor make an association if it came from the database.
        if ( isDeleted() || ! isAssignedToSol()) return true;

        boolean status = false; // commit status
        if (fromDbase) {  // "existing" (came from the dbase)
            if (getNeedsCommit() || hasChanged()) {  // force new row (sequence number) and association
                if (debug) System.out.println("(changed) INSERT and assoc");
                status = dbaseInsert() && dbaseAssociate(false);
            } else { // if no changes, just make new association
                if (debug) System.out.println("(unchanged) assoc only");
                status = dbaseAssociate(true);
                if (status) setUpdate(false); // mark as up-to-date
            }
        } else {
            if (debug) System.out.println("(new) INSERT and assoc");
            status = dbaseInsert() && dbaseAssociate(false);
        }
        setNeedsCommit(! status); // hopefully false?
        return status;
    }

    // accessor methods are needed when subclasses belong to other packages -aww
    // since protected can access across package boundaries by Solution and Magnitude
    public boolean isFromDataSource() {
       return fromDbase;
    }

    /**
    * Does a no-op, returns true, no existing codas are deleted.
    */
    protected boolean dbaseDelete() {
        return true;
    }

    /**
    * Makes a new AssocCoO table row if isAssignedToSol() == true.
    * then makes a new AssocCoM table row if isAssignedToMag() == true.
    * Returns true upon successfully creating new association row(s).
    * Returns false if isAssignedToSol() == false or
    * an error occurs creating any association row.
    * @see #isAssignedToSol()
    * @see #isAssignedToMag()
    * @see #dbaseAssociateWithOrigin(boolean)
    * @see #dbaseAssociateWithMag(boolean)
    */
    protected boolean dbaseAssociate(boolean updateExisting) {
      // must have a valid origin association
      if (! dbaseAssociateWithOrigin(updateExisting) ) return false;  // abort here
      return (isAssignedToMag()) ? dbaseAssociateWithMag(updateExisting) : true; // return true, associated mag not required
    }

    /**
    * Makes a new AssocCoO table row for this Coda, if isAssignedToSol() == true.
    * Returns true upon successfully creating new association row.
    * Returns false if no association exists or an error occurs creating association row.
    * @see #isAssignedToSol()
    * @see #dbaseAssociate(boolean)
    */
    protected boolean dbaseAssociateWithOrigin(boolean updateExisting) {
        if ( ! isAssignedToSol() ) return false;
        // Check that 'orid' of associated event has been set
        if ( ((SolutionTN)sol).getOridValue() == 0l ) {
          if (debug) System.out.println("DEBUG CodaTN  Error associated Solution has 0 orid "+
                          getChannelObj().getChannelId().toString());
          return false;
        }
        if (getCoid() == 0l) {
          if (debug) System.out.println("DEBUG CodaTN  Error 0 coid "+
                          getChannelObj().getChannelId().toString());
          return false;
        }

        // force row check unless a boolean input arg for doing row check is implemented 
        return DataTableRowUtil.rowToDb(DataSource.getConnection(), toAssocCoORow(), updateExisting);
    }

    /**
    * Makes or updates a new AssocCoM table row for this Coda, if isAssignedToMag() == true.
    * Returns true upon successfully creating or updating the association row.
    * Returns false if no association exists or an error occurs.
    * @see #isAssignedToMag()
    */
    protected boolean dbaseAssociateWithMag(boolean updateExisting) {
        if ( ! isAssignedToMag() ) return false;
        // check that 'magid' has been set, it can't be null
        if (getMagidValue() == 0l) { 
          if (debug) System.out.println("DEBUG CodaTN Error associated Magnitude has 0 magid "+
                          getChannelObj().getChannelId().toString());
          return false; // added, don't allow "zero" magid in assoc table  -aww 02/07/2005
        }
        if (getCoid() == 0l) {
          if (debug) System.out.println("DEBUG CodaTN Error 0 coid "+
                          getChannelObj().getChannelId().toString());
          return false;
        }
        // if (! getAssociatedMag().isCodaMag()) { }

        // force row check unless a boolean input arg for doing row check is implemented 
        return DataTableRowUtil.rowToDb(DataSource.getConnection(), toAssocCoMRow(), updateExisting);
    }

    /**
    * Inserts a Coda row and any associated AssocCoO and/or AssocCoM table rows(s) into the DataSource database.
    * Returns true upon success. Returns false if isAssignedToSol() == false or an error occurs attempting to create
    * any of these new rows.
    */
    protected boolean dbaseInsert() {
        //if (fromDbase) return false;  // but isn't a "change" worthy of update? - aww
        StnChlTableRow aRow = toTableRow();
        aRow.setProcessing(DataTableRowStates.INSERT);
        // write row
        boolean status = (aRow.insertRow(DataSource.getConnection()) > 0);
        if (status) {
          //Note: Solution, Magnitude commit() invoke amp commit() which write new assoc - aww 09/03
          //status = dbaseAssociate(); // (removed see above note) commit() invokes it.
          // now its "from" the dbase
          setUpdate(false); // mark as up-to-date (fromDbase=true) fixed bug aww -03/03
        }
        return status;
    }

    /**
    * Returns the value of the coda id number, corresponding to 'coid' of the table row, if initialized from the database.
    * If identifier is  not initialized, 0 is returned.
    * @see #getBySolution(long)
    * @see #getBySolution(Solution)
    * @see #getByMagnitude(Magnitude)
    */
    public long getCoid() {
        return (coid.isValidNumber()) ? coid.longValue() : 0l;
    }
    public void setCoid(long id) {
        coid.setValue(id);
    }

    public Object getIdentifier() {return coid;}
    public void setIdentifier(Object obj) {
      coid.setValue((DataLong) obj);
    }
    /**
    * Returns true if this instance was not initialized from database data.
    * or hasChangedAttributes() == true.
    */
    public boolean hasChanged() {
        return (! fromDbase || hasChangedAttributes());
    }

    /** Returns true if certain data member values were modified after initialization.
     * (descriptor, datetime, tau, afix, afree, qfix, qfree)
     */
    protected boolean hasChangedAttributes() {
        return (
            datetime.isUpdate() || tau.isUpdate() || descriptor.hasChanged()) ||
            (aFix.isUpdate() || aFree.isUpdate() || qFix.isUpdate() || qFree.isUpdate()
        );
    }

    /**
     * Set the isUpdate() flag for all data dbase members the given boolean value.
     * */
    protected void setUpdate (boolean tf) {
      fromDbase = !tf;
      tau.setUpdate(tf);
      datetime.setUpdate(tf);
      descriptor.setUpdate(tf);
      aFix.setUpdate(tf);
      aFree.setUpdate(tf);
      qFix.setUpdate(tf);
      qFree.setUpdate(tf);
    }

    protected String makeTypeClause(String[] typeList) {
           StringBuffer sb = new StringBuffer(80);
           if (typeList == null || typeList.length == 0) return "";
           sb.append("and (c.codatype='P') and (");
           for (int i = 0; i < typeList.length; i++) {
               if (i > 0) sb.append(" or ");
               sb.append("(c.durtype like '").append(typeList[i]).append("')");
           }
           sb.append(")");
           return sb.toString();
    }

    /*
    private static void associateList(Solution sol, List coda) {
        if (sol.getAmpList().isEmpty()) {
            sol.fastAssociateAll(coda); // does not filter input elements
        }
        else {
            sol.associateAll(coda); // filters input for dupes or nulls
        }
    }
    private static void associateList(Magnitude mag, List coda) {
        if (!mag.isCodaMag()) {
           System.err.println("WARNING - CodaTN associateList input mag is not coda mag subtype!");
        }
        if (mag.getReadingList().isEmpty()) {
            mag.fastAssociateAll(coda); // does not filter input elements
        }
        else {
            mag.associateAll(coda); // filters input for dupes or nulls
        }
    }
    */
     

    /**
     * Returns a list of Codas associated in the default DataSource
     * with the identifier of the input Magnitude and whose
     * type qualifiers match those specified in the input typeList.
     * If no data exists, returns an empty collection.
     * @see #getByMagnitude(Magnitude)
     * */
    public Collection getByMagnitude(Magnitude mag, String[] typeList) {
        return getByMagnitude(mag, typeList, true);
    }
    public Collection getByMagnitude(Magnitude mag, String[] typeList, boolean assoc) {

        setJoinType(Type_AssocCoM);

        long inMagid = ((MagnitudeTN)mag).getMagidValue(); // input arg id
        if (inMagid <= 0l) return null; // return is 0 if not valid (null), so don't query

        StringBuffer sb = new StringBuffer(1024);
        //sb.append(sqlPrefixByMag);
        //sb.append(" and (AssocCoM.magid = ").append(inMagid).append(")");
        //sb.append(makeTypeClause(typeList));
        //sb.append(" order by Coda.net, Coda.sta, Coda.seedchan, Coda.lddate, Coda.coid");
        //ArrayList dbList = (ArrayList) ((JasiDbReader)getDataReader()).getBySQL(sb.toString());
        //ArrayList dbList = (ArrayList) ((JasiDbReader)getDataReader()).getBySQL(DataSource.getConnection(), sb.toString(), this);

        sb.append(CodaTN.psSqlPrefixByMagid);
        sb.append(makeTypeClause(typeList));
        sb.append(CodaTN.psSqlOrderSuffix);

        ArrayList aList = null;
        PreparedStatement psByMagid = null;
        final String sql = sb.toString();
        try {
            Connection conn = DataSource.getConnection();
            psByMagid = conn.prepareStatement(sql);
            psByMagid.setLong(1, inMagid);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByMagid, this, true);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            JasiDatabasePropertyList.debugSQL(sql, psByMagid, debug);
            Utils.closeQuietly(psByMagid);
        }

        if (aList == null) return aList;
        int dbCount  = aList.size();
        //System.out.println("getByMagnitude: coda found ="+dbCount);
        if (dbCount < 1) return aList; // no data

        // option to associate elements of list with the input argument
        // otherwise returned list elements are orphaned from context
        //if (assoc) associateList(mag, aList);
        if (assoc) mag.associate(aList);

        return aList;
    }
    /**
     * Returns a list of Codas associated in the default DataSource
     * with the identifier of the input Magnitude.
     * Returned Codas are affiliated with the input Magnitude, but 
     * they are not added to its internal lists.
     * This does a join using magid and the AssocCoM link table.
     * */
    public Collection getByMagnitude(Magnitude mag) {
        return getByMagnitude(mag, null);
    }

        /* Select * logic to remove duplicates for same channel id but different coid
        sb.append("Select * from (Select ");
        sb.append(org.trinet.jdbc.table.Coda.QUALIFIED_COLUMN_NAMES).append(",");
        sb.append(AssocCoO.QUALIFIED_COLUMN_NAMES);
        // NOTE: should partition by location, but data in db has mixed null and empty strings, needs fix!!!! -aww
        //sb.append(", dense_rank() over (partition by net,sta,seedchan,location order by Coda.lddate desc) drank");
        sb.append(", dense_rank() over (partition by Coda.net,Coda.sta,Coda.seedchan order by Coda.lddate desc, Coda.coid desc) drank");
        sb.append(" from Coda, AssocCoO where (Coda.coid = AssocCoO.coid)");
        sb.append(" and (AssocCoO.orid = ").append(((SolutionTN)aSol).getOridValue()).append(")");
        sb.append(makeTypeClause(typeList));
        sb.append(") t where t.drank=1");
        */

    public Collection getBySolution(Solution aSol) {
        return getBySolution(aSol, null);
    }
    public Collection getBySolution(Solution aSol, String [] typeList) {
        return getBySolution(aSol, typeList, true);
    }
    public Collection getBySolution(Solution aSol, String [] typeList, boolean assoc) {

        setJoinType(Type_AssocCoO);

        long inOrid = ((SolutionTN)aSol).getOridValue(); // input arg value
        if (inOrid <= 0l) return null; // return is 0 if not valid (null), so don't query - aww 02/07/2005

        StringBuffer sb = new StringBuffer(1024);
        //sb.append(sqlPrefixByOrigin);
        //sb.append(" and (AssocCoO.orid = ").append(inOrid).append(")");
        //sb.append(makeTypeClause(typeList));
        //sb.append(" order by Coda.net, Coda.sta, Coda.seedchan, Coda.lddate, Coda.coid");
        sb.append(CodaTN.psSqlPrefixByOrid);
        sb.append(makeTypeClause(typeList));
        sb.append(CodaTN.psSqlOrderSuffix);

        //ArrayList aList = (ArrayList) ((JasiDbReader)getDataReader()).getBySQL(sb.toString());
        //ArrayList aList = (ArrayList) ((JasiDbReader)getDataReader()).getBySQL(DataSource.getConnection(), sb.toString(), this);

        ArrayList aList = null;
        PreparedStatement psByOrid = null;
        final String sql = sb.toString();
        try {
            Connection conn = DataSource.getConnection();
            psByOrid = conn.prepareStatement(sql);
            psByOrid.setLong(1, inOrid);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByOrid, this, true);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            JasiDatabasePropertyList.debugSQL(sql, psByOrid, debug);
            Utils.closeQuietly(psByOrid);
        }

        if (aList == null) return null;
        int dbCount  = aList.size();
        //System.out.println("getBySolution: coda found ="+dbCount);
        if (dbCount < 1) return aList; // no data

        // option to associate elements of list with the input argument 
        // otherwise returned list elements are orphaned from context
        //if (assoc) associateList(aSol, aList);
        if (assoc) aSol.associate(aList); // line added to match amp,phase logic 05/21/04 aww

        return aList;
    }
    public Collection getBySolution(long id) {
        return getBySolution(id, null);
    }
    public Collection getBySolution(long id, String [] typeList) {
        Solution tmpSol = (Solution) Solution.create().getById(id);
        // must create Solution, then get prefor for evid
        return (tmpSol == null) ? null : getBySolution(tmpSol, typeList, true);
    }

    public Collection getByTime(double start, double end) {
        return getByTime(DataSource.getConnection(), start, end);
    }
    public Collection getByTime(Connection conn, double start, double end) {
        //Kludge for NCEDC request with leap seconds added // removed 2008/01/24 for leap time -aww
        //start = DataSource.nominalToDbTimeBase(start); //end  = DataSource.nominalToDbTimeBase(end); //end of patch - aww

        setJoinType(Type_AssocNone);

        //StringBuffer sb = new StringBuffer(1024);
        //sb.append(sqlPrefix);
        //sb.append(" where CODA.DATETIME between TRUETIME.putEpoch(");  // added for leap secs 2008/01/24 -aww 
        //sb.append(start).append(", 'UTC') AND TRUETIME.putEpoch(").append(end).append(", 'UTC')");
        //sb.append(" order by Datetime");
        // This is quite SLOW, need dbase indexing?
        //return ((JasiDbReader)getDataReader()).getBySQL(conn, sb.toString());
        //return  ((JasiDbReader)getDataReader()).getBySQL(conn, sb.toString(), this);
        ArrayList aList = null;
        PreparedStatement psByTime = null;
        try {
            psByTime = conn.prepareStatement(CodaTN.psSqlPrefixByTime);
            psByTime.setDouble(1, start);
            psByTime.setDouble(2, end);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByTime, this, true);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            JasiDatabasePropertyList.debugSQL(CodaTN.psSqlPrefixByTime, psByTime, debug);
            Utils.closeQuietly(psByTime);
        }

        return aList;
    }

    // Might have to make public.
    protected org.trinet.jasi.Coda getByCoid (long id) {

        setJoinType(Type_AssocNone);

        //StringBuffer sb = new StringBuffer(1024);
        //sb.append(sqlPrefix);
        //sb.append(" and coid = ").append(id);
        //ArrayList aList = (ArrayList) ((JasiDbReader)getDataReader()).getBySQL(sb.toString());
        //ArrayList aList = (ArrayList) ((JasiDbReader)getDataReader()).getBySQL(DataSource.getConnection(), sb.toString(), this);
        ArrayList aList = null;
        PreparedStatement psByCoid = null;
        try {
            Connection conn = DataSource.getConnection();
            psByCoid = conn.prepareStatement(CodaTN.psSqlPrefixByCoid);
            psByCoid.setLong(1, id);
            aList = (ArrayList) ((JasiDbReader) getDataReader()).getBySQL(conn, psByCoid, this, true);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            JasiDatabasePropertyList.debugSQL(CodaTN.psSqlPrefixByCoid, psByCoid, debug);
            Utils.closeQuietly(psByCoid);
        }
        return (aList == null || aList.isEmpty()) ? (org.trinet.jasi.Coda) null : (org.trinet.jasi.Coda) aList.get(0);
    }

    /**
     * Return an Coda from the default DataSource whose identifier matches
     * the input id number.
     * Returned Coda has null Solution and Magnitude association assigments.
     * Returns null if there is no match.
     * */
    public JasiCommitableIF getById(long id) {
        return getByCoid(id);
    }

    public JasiCommitableIF getById(Object id) {
        if (id instanceof Number)
            return  getById(((Number)id).longValue());
        else if (id instanceof DataNumber)
            return getById(((DataNumber)id).longValue());
        else return null;
    }

    /**
     * Derives from the DataSource a Coda instance corresponding to the input coda id ( coid in the NCDC schema).
     * Retrieves Coda, AssocCoO and AssocCoM data from DataSource database.
     * Returns null if no data is found for the specified id.
    // Method is NOT USED by code base, else may have to make public.
    protected org.trinet.jasi.Coda getByCoidAssoc(long id) {
        setJoinType(Type_Assoc);
        StringBuffer sb = new StringBuffer(1024);
        sb.append(sqlPrefixByAssoc);
        sb.append(" and Coda.coid = ").append(id); // noticed coid missing from sql -aww 11/11/2004
        //ArrayList codaList = (ArrayList) ((JasiDbReader)getDataReader()).getBySQL(sb.toString());
        ArrayList codaList = (ArrayList) ((JasiDbReader)getDataReader()).getBySQL(DataSource.getConnection(), sb.toString(), this);
        return (codaList == null || codaList.isEmpty()) ? null : (org.trinet.jasi.Coda) codaList.get(0); // only returns one Coda
    }
   */

    public JasiObject parseData(Object rsdb) {
      return parseResultSetDb((ResultSetDb)rsdb);
    }
    public JasiObject parseResultSetDb (ResultSetDb rsdb) {
      return parseResultSet(rsdb);
    }
    /**Access to the JasiDbReader to allow generic SQL queries. */
    abstract public JasiDataReaderIF getDataReader();

    // A matching row key exists in datasource but its actually data may differ from instance
    public boolean existsInDataSource() {
        JasiDbReader jdr = (JasiDbReader) getDataReader();
        int count = jdr.getCountBySQL(DataSource.getConnection(), "CODA", "COID", coid.toString()); 
        return (count > 0);
    }

    // method like that in AmpTN
    protected int getJoinType() {
      return joinType;
    }

    // methods like that in AmpTN
    protected void setJoinType(int type) {
      joinType = type;
    }

    /**
    * Parses a ResultSet table row that possibly contains the ordered concatenation of
    * Coda, AssocCoO and AssocCoM table columns.
    */
    protected org.trinet.jasi.Coda parseResultSet(ResultSetDb rsdb) {
      return parseResultSet(rsdb, getJoinType());
    }

    /*
    private static org.trinet.jasi.Coda parseResultSet(ResultSetDb rsdb, int joinType) {

        org.trinet.jdbc.table.Coda coda = new org.trinet.jdbc.table.Coda();  // create new jdbc object

        int offset = 0;
        coda = (org.trinet.jdbc.table.Coda) coda.parseOneRow(rsdb, offset);
        // invoking coda.setMutable(true) allow changes to CodaTN fields aliased to this DataTableRow.
        if (DataSource.isWriteBackEnabled()) coda.setMutable(true).setMutableAllValues(true); //aww make key mutable 07/07/04

        // actual instance is a subclass of this class
        org.trinet.jasi.TN.CodaTN newCodaTN = (org.trinet.jasi.TN.CodaTN)
                org.trinet.jasi.Coda.create("TN");

        newCodaTN.parseCoda(coda);
        offset += org.trinet.jdbc.table.Coda.MAX_FIELDS;

        // Parse depends on whether you did joint with AssocCoO or AssocCoM table.
        if (joinType == Type_AssocCoO || joinType == Type_Assoc) {
            AssocCoO  assoc = new AssocCoO();
            assoc = (AssocCoO) assoc.parseOneRow(rsdb, offset);
            if (DataSource.isWriteBackEnabled()) assoc.setMutable(true).setMutableAllValues(true);//aww make key mutable 07/07/04
            newCodaTN.parseAssocCoO(assoc);
            offset += AssocCoO.MAX_FIELDS;
        }
        if (joinType == Type_AssocCoM || joinType == Type_Assoc) {
            AssocCoM  assoc = new AssocCoM();
            assoc = (AssocCoM) assoc.parseOneRow(rsdb, offset);
            if (DataSource.isWriteBackEnabled()) assoc.setMutable(true).setMutableAllValues(true);//aww make key mutable 07/07/04
            newCodaTN.parseAssocCoM(assoc);
            offset += AssocCoM.MAX_FIELDS;
        }
        newCodaTN.setUpdate(false); // remember that we got this from the dbase
        return newCodaTN;
    }
    */

    private static org.trinet.jasi.Coda parseResultSet(ResultSetDb rsdb, int joinType) {
        // build up the Coda from the jdbc objects
        org.trinet.jasi.TN.CodaTN codaNew = new org.trinet.jasi.TN.CodaTN();

        Channel chan = codaNew.getChannelObj();
        ResultSet rs = rsdb.getResultSet();
        //
        // Parse query resultset columns
        //
        try {
            //"SELECT c.COID,c.STA,c.NET,c.AUTH,c.SUBSOURCE,c.CHANNEL,c.CHANNELSRC,c.SEEDCHAN," + // 1-8
            //"c.LOCATION,c.AFIX,c.AFREE,c.QFIX,c.QFREE,c.TAU,c.NSAMPLE,c.RMS,c.DURTYPE,c.UNITS," + // 9-18
            //"c.TIME1,c.AMP1,c.TIME2,c.AMP2,c.QUALITY,c.RFLAG,TRUETIME.getEpoch(c.DATETIME,'UTC'),c.ALGORITHM,c.WINSIZE";  // 19-27
            int offset = 1;
            //coid
            codaNew.coid.setValue(rs.getLong(offset++));

            //sta
            chan.setSta(rs.getString(offset++));
            //net
            chan.setNet(rs.getString(offset++));

            //auth
            codaNew.authority.setValue(rs.getString(offset++));
            //subsource
            codaNew.source.setValue(rs.getString(offset++));

            //channel
            chan.setChannel(rs.getString(offset++));
            //channelsrc
            chan.setChannelsrc(rs.getString(offset++));
            //seedchan
            chan.setSeedchan(rs.getString(offset++));
            //location
            chan.setLocation(rs.getString(offset++));

            //codatype for now, assumed to always be 'P'
            String type = "P"; // rs.getString(offset++);

            //afix
            double dd = rs.getDouble(offset++);
            if (!rs.wasNull()) codaNew.aFix.setValue(dd);

            //afree
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) codaNew.aFree.setValue(dd);

            //qfix
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) codaNew.qFix.setValue(dd);

            //qfree
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) codaNew.qFree.setValue(dd);

            //tau
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) codaNew.tau.setValue(dd);

            //nsample
            int nwin = rs.getInt(offset++);
            if (!rs.wasNull()) codaNew.windowCount.setNull(true);
            codaNew.windowCount.setValue(nwin);

            //"c.COID,c.STA,c.NET,c.AUTH,c.SUBSOURCE,c.CHANNEL,c.CHANNELSRC,c.SEEDCHAN," + // 1-8
            //"c.LOCATION,c.AFIX,c.AFREE,c.QFIX,c.QFREE,c.TAU,c.NSAMPLE,c.RMS,c.DURTYPE,c.UNITS," + // 9-18
            //"c.TIME1,c.AMP1,c.TIME2,c.AMP2,c.QUALITY,c.RFLAG,c.DATETIME,c.ALGORITHM,c.WINSIZE" +  // 19-27

            //rms
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) codaNew.residual.setValue(dd);

            //durtype
            type = rs.getString(offset++);
            CodaDurationType durType = (rs.wasNull()) ?  CodaDurationType.UNKNOWN : CodaDurationType.getDurationType(type);
            // Note no IPHASE for coda descriptor is currently stored in database 
            codaNew.descriptor = new CodaPhaseDescriptorTN(durType, "");

            //units
            codaNew.ampUnits.setValue(rs.getString(offset++));

            codaNew.windowTimeAmpPairs.clear();
            //time1
            double time = rs.getDouble(offset++);
            if (rs.wasNull()) time = -1.;
            //amp1
            double amp = rs.getDouble(offset++);
            if (rs.wasNull()) amp = -1.;
            if (time > 0. && amp > 0.) codaNew.windowTimeAmpPairs.add(new TimeAmp(time, amp));
            //time2
            time = rs.getDouble(offset++);
            if (rs.wasNull()) time = -1;
            //amp2
            amp = rs.getDouble(offset++);
            if (rs.wasNull()) amp = -1;
            if (time > 0. && amp > 0.) codaNew.windowTimeAmpPairs.add(new TimeAmp(time, amp));

            //quality
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) codaNew.quality.setValue(dd);
            //codaNew.quality.setNull(true);

            //rflag
            codaNew.processingState.setValue(rs.getString(offset++));

            //datetime
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) codaNew.datetime.setValue(dd);

            //algorithm
            codaNew.algorithm.setValue(rs.getString(offset++));

            //winsize
            dd = rs.getDouble(offset++);
            if (!rs.wasNull()) codaNew.windowSize.setValue(dd);

            //java.sql.Date date = rs.getDate(offset++);
            //if (!rs.wasNull()) this.timeStamp.setValue(date);
  
            // Parse depends on whether you did joint with AssocCoO or AssocCoM table.
            if (joinType == Type_AssocCoO) {
              //o.ORID
              //orid.setValue(rs.getLong(offset++)); // removed from query, instance orid data member removed

              //o.DELTA
              dd = rs.getDouble(offset++);
              if (!rs.wasNull()) codaNew.setHorizontalDistance(dd);

              //o.SEAZ 
              dd = rs.getDouble(offset++);
              if (!rs.wasNull()) codaNew.setAzimuth(dd);
            }
            else if (joinType == Type_AssocCoM) {
              //codaNew.magid.setValue(rs.getLong(offset++)); // magid not used, instead it refs the id of associated Magnitude
              //
              //m.WEIGHT 
              dd = rs.getDouble(offset++);
              if (!rs.wasNull()) codaNew.channelMag.weight.setValue(dd);

              //m.IN_WGT 
              dd = rs.getDouble(offset++);
              if (!rs.wasNull()) {
                  codaNew.channelMag.inWgt.setValue(dd);
                  if (codaNew.channelMag.inWgt.isValid()) { // handle "rejected" coda read from db 
                      codaNew.setReject(codaNew.channelMag.inWgt.doubleValue() == 0.);
                  }
              }
              // Some historic mags may have null in_wgt and a weight>0. ?
              else if (codaNew.channelMag.weight.doubleValue() > 0.) {
                  codaNew.channelMag.inWgt.setValue(1.);
              }

              //m.MAG 
              dd = rs.getDouble(offset++);
              if (!rs.wasNull()) codaNew.channelMag.value.setValue(dd);

              //m.MAGRES 
              dd = rs.getDouble(offset++);
              if (!rs.wasNull()) codaNew.channelMag.residual.setValue(dd);

              //m.MAGCORR
              dd = rs.getDouble(offset++);
              if (!rs.wasNull()) codaNew.channelMag.correction.setValue(dd);

            }
            codaNew.setUpdate(false);
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }

        // remember that we got this from the dbase
        return (org.trinet.jasi.Coda) codaNew;
    }

    /**
    * Parse data member values from input org.trinet.jdbc.table.Coda object data.
    */
    protected void parseCoda(org.trinet.jdbc.table.Coda coda) {
        //Identifier (long) of coda row id
        coid = (DataLong) coda.getDataObject(org.trinet.jdbc.table.Coda.COID);
        // use setChannel to synch with master channel list
        Channel cha = ChannelTN.parseChannelFromDataTableRow(coda, DataTableRowUtil.AUTHSUBSRC);
        //setChannelObj(cha); // aww 04/05/2005
        this.chan = cha; // aww 04/05/2005
        authority = (DataString) coda.getDataObject(org.trinet.jdbc.table.Coda.AUTH);
        datetime = (DataDouble) coda.getDataObject(org.trinet.jdbc.table.Coda.DATETIME);
        //Kludge for NCEDC leap corrected data, to convert to nominal time -aww
        //DataSource.dbTimeBaseToNominal(datetime); // removed 2008/01/24 -aww for leap secs

        // Create codaDescriptor where codaType always the same
        //String codaType  = ((DataString) coda.getDataObject(org.trinet.jdbc.table.Coda.CODATYPE)).toString();
        DataString ds  = (DataString) coda.getDataObject(org.trinet.jdbc.table.Coda.DURTYPE);
        CodaDurationType durType = ds.isNull() ?
            CodaDurationType.UNKNOWN : CodaDurationType.getDurationType(ds.toString());

        // Removed IPHASE coda descriptor database read 10/25/2004 - aww
        //ds  = (DataString) coda.getDataObject(org.trinet.jdbc.table.Coda.IPHASE);
        //String desc = ds.isNull() ?  "" : ds.toString(); // null =  "" , no description
        //descriptor = new CodaPhaseDescriptorTN(durType, desc);
        descriptor = new CodaPhaseDescriptorTN(durType, "");

        source = (DataString) coda.getDataObject(org.trinet.jdbc.table.Coda.SUBSOURCE);

        tau = (DataDouble) coda.getDataObject(org.trinet.jdbc.table.Coda.TAU);
        aFix = (DataDouble) coda.getDataObject(org.trinet.jdbc.table.Coda.AFIX);
        aFree = (DataDouble) coda.getDataObject(org.trinet.jdbc.table.Coda.AFREE);
        qFix = (DataDouble) coda.getDataObject(org.trinet.jdbc.table.Coda.QFIX);
        qFree = (DataDouble) coda.getDataObject(org.trinet.jdbc.table.Coda.QFREE);

        algorithm = (DataString) coda.getDataObject(org.trinet.jdbc.table.Coda.ALGORITHM);
        uncertainty = (DataDouble) coda.getDataObject(org.trinet.jdbc.table.Coda.ERAMP);
        ampUnits = (DataString) coda.getDataObject(org.trinet.jdbc.table.Coda.UNITS);
        windowSize = (DataDouble) coda.getDataObject(org.trinet.jdbc.table.Coda.WINSIZE);

        windowCount = (DataLong) coda.getDataObject(org.trinet.jdbc.table.Coda.NSAMPLE);
        quality = (DataDouble) coda.getDataObject(org.trinet.jdbc.table.Coda.QUALITY);
        residual = (DataDouble) coda.getDataObject(org.trinet.jdbc.table.Coda.RMS);
        processingState = (DataString) coda.getDataObject(org.trinet.jdbc.table.Coda.RFLAG);
        parseTimeAmps(coda);
        // save Coda.LDDATE in timeStamp? -aww
        this.timeStamp = (DataDate) coda.getDataObject(org.trinet.jdbc.table.Coda.LDDATE); // date of first commit?
    }


    // Would be better if time amps fields in schema coda table row changed from integer to double
    // EW/Hypoinverse time,amp value interval has non-uniform spacing algorithm stops at 144. secs.
    protected void parseTimeAmps(org.trinet.jdbc.table.Coda coda) {

      // blow any existing data, there shouldn't be any
      if (windowTimeAmpPairs.size() > 0) windowTimeAmpPairs.clear();

      int cnt = windowCount.intValue();  // windowCount of good timeAmp pairs used for line fit 
      boolean windowCntMatches = true;

      for (;;) {  // infinite loop wrapper

        DataNumber time = (DataNumber) coda.getDataObject(org.trinet.jdbc.table.Coda.TIME1);
        DataNumber amp  = (DataNumber) coda.getDataObject(org.trinet.jdbc.table.Coda.AMP1);


        if ( (time.isValidNumber() && amp.isValidNumber()) && amp.doubleValue() > 0.) {
          if (cnt < 1) windowCntMatches = false;
          // Note for relative window amp times that nominal==leap seconds
          //DataSource.dbTimeBaseToNominal(time); // kludge for possible NCEDC leap secs - aww removed 2008/01/24
          windowTimeAmpPairs.add(new TimeAmp(time, amp)); // have 1
        }
        else {
          break; // assume no more data to parse
        }

        time = (DataNumber) coda.getDataObject(org.trinet.jdbc.table.Coda.TIME2);
        amp  = (DataNumber) coda.getDataObject(org.trinet.jdbc.table.Coda.AMP2);
        if ( (time.isValidNumber() && amp.isValidNumber()) && amp.doubleValue() > 0.) {
          if (cnt < 2) windowCntMatches = false;
          //DataSource.dbTimeBaseToNominal(time); // kludge for possible NCEDC leap secs - aww removed 2008/01/24
          windowTimeAmpPairs.add(new TimeAmp(time, amp)); // have 2
        }
        else {
          break; // assume no more data to parse
        }

        // Mc DEBUG test
        if (this.isMcType()) {
          // Extra check to find in coda 
          System.out.println("CodaTN INFO - More than two time-amp window pairs parsed from db row for Mc type.");
        }

        time = (DataNumber) coda.getDataObject(org.trinet.jdbc.table.Coda.TIME3);
        amp  = (DataNumber) coda.getDataObject(org.trinet.jdbc.table.Coda.AMP3);
        if ( (time.isValidNumber() && amp.isValidNumber()) && amp.doubleValue() > 0.) {
          if (cnt < 3) windowCntMatches = false;
          //DataSource.dbTimeBaseToNominal(time); // kludge for possible NCEDC leap secs - aww removed 2008/01/24
          windowTimeAmpPairs.add(new TimeAmp(time, amp)); // have 3
        }
        else {
          break; // assume no more data to parse
        }

        time = (DataNumber) coda.getDataObject(org.trinet.jdbc.table.Coda.TIME4);
        amp  = (DataNumber) coda.getDataObject(org.trinet.jdbc.table.Coda.AMP4);
        if ( (time.isValidNumber() && amp.isValidNumber()) && amp.doubleValue() > 0.) {
          if (cnt < 4) windowCntMatches = false;
          //DataSource.dbTimeBaseToNominal(time); // kludge for possible NCEDC leap secs - aww removed 2008/01/24
          windowTimeAmpPairs.add(new TimeAmp(time, amp)); // have 4
        }
        else {
          break; // assume no more data to parse
        }

        time = (DataNumber) coda.getDataObject(org.trinet.jdbc.table.Coda.TIME5);
        amp  = (DataNumber) coda.getDataObject(org.trinet.jdbc.table.Coda.AMP5);
        if ( (time.isValidNumber() && amp.isValidNumber()) && amp.doubleValue() > 0.) {
          if (cnt < 5) windowCntMatches = false;
          windowTimeAmpPairs.add(new TimeAmp(time, amp)); // have 5
        }
        else {
          break; // assume no more data to parse
        }

        time = (DataNumber) coda.getDataObject(org.trinet.jdbc.table.Coda.TIME6);
        amp  = (DataNumber) coda.getDataObject(org.trinet.jdbc.table.Coda.AMP6);
        if ((time.isValidNumber() && amp.isValidNumber()) && amp.doubleValue() > 0.) {
          if (cnt < 6) windowCntMatches = false;
          //DataSource.dbTimeBaseToNominal(time); // kludge for possible NCEDC leap secs - aww removed 2008/01/24
          windowTimeAmpPairs.add(new TimeAmp(time, amp)); // have 6, that's all
        }
        else {
          break;
        }

        // no more data to parse
        break; // insurance to get out of loop
      }

      if (! windowCntMatches && debug) {
          System.out.println("DEBUG CodaTN Warning - mismatch? Good window line fit count: "+ cnt +
            ", time-amps count: " + windowTimeAmpPairs.size() + " for: " +
            this.toDelimitedSeedNameString("_")
          );
      }

    }

    /**
     * Assign contents of this Coda object into a new Coda (StnChlTableRow) object.
     * Gets a new 'coid' from a sequence in the database.
     * @See: org.trinet.jdbc.Coda().
     */
    protected StnChlTableRow toTableRow() {
        // Always make a new row, get unique key #
        // long newId = SeqIds.getNextSeq("coseq");
        long newId = SeqIds.getNextSeq(org.trinet.jdbc.table.Coda.SEQUENCE_NAME);
        coid.setValue(newId);

        org.trinet.jdbc.table.Coda codaRow = new org.trinet.jdbc.table.Coda(newId);
        codaRow.setUpdate(true); // sets flag to enable processing

        setRowChannelId(codaRow, getChannelObj());

        //Kludge for NCEDC leap corrected data, to convert to leap time -aww
        //DataDouble dt = (DataDouble) datetime.clone();
        // Note for relative window amp times that nominal==leap seconds
        //DataSource.nominalToDbTimeBase(dt);  // removed 2008/01/24 for leap secs -aww
        //codaRow.setValue(org.trinet.jdbc.table.Coda.DATETIME, dt);
        // end of leap patch aww
        codaRow.setValue(org.trinet.jdbc.table.Coda.DATETIME, datetime);

        codaRow.setValue(org.trinet.jdbc.table.Coda.AUTH, getClosestAuthorityString());

        CodaType codaType = descriptor.getCodaType();
        if (!(codaType == CodaType.UNKNOWN))
            codaRow.setValue(org.trinet.jdbc.table.Coda.CODATYPE, descriptor.getCodaTypeName());

        CodaDurationType durType = descriptor.getDurationType();
        if (!(durType == CodaDurationType.UNKNOWN))
            codaRow.setValue(org.trinet.jdbc.table.Coda.DURTYPE, descriptor.getDurationTypeName());

        // Removed IPHASE coda descriptor database write 10/25/2004 - aww
        //String desc = descriptor.getDescription();
        //if (! NullValueDb.isBlank(desc)) codaRow.setValue(org.trinet.jdbc.table.Coda.IPHASE, desc);

        if (source.isValid()) codaRow.setValue(org.trinet.jdbc.table.Coda.SUBSOURCE, source);

        if (tau.isValid())   codaRow.setValue(org.trinet.jdbc.table.Coda.TAU, tau);
        if (aFix.isValid())  codaRow.setValue(org.trinet.jdbc.table.Coda.AFIX, aFix);
        if (aFree.isValid()) codaRow.setValue(org.trinet.jdbc.table.Coda.AFREE, aFree);
        if (qFix.isValid())  codaRow.setValue(org.trinet.jdbc.table.Coda.QFIX, qFix);
        if (qFree.isValid()) codaRow.setValue(org.trinet.jdbc.table.Coda.QFREE, qFree);

        if (algorithm.isValid()) codaRow.setValue(org.trinet.jdbc.table.Coda.ALGORITHM, algorithm);
        if (uncertainty.isValid()) codaRow.setValue(org.trinet.jdbc.table.Coda.ERAMP, uncertainty);
        if (ampUnits.isValid()) codaRow.setValue(org.trinet.jdbc.table.Coda.UNITS, ampUnits);
        if (windowSize.isValid()) codaRow.setValue(org.trinet.jdbc.table.Coda.WINSIZE, windowSize);
        if (windowCount.isValid()) codaRow.setValue(org.trinet.jdbc.table.Coda.NSAMPLE, windowCount);
        if (quality.isValid()) codaRow.setValue(org.trinet.jdbc.table.Coda.QUALITY, quality);
        if (residual.isValid()) codaRow.setValue(org.trinet.jdbc.table.Coda.RMS, residual);
        if (processingState.isValid()) codaRow.setValue(org.trinet.jdbc.table.Coda.RFLAG, processingState);

        timeAmpsToCodaRow(codaRow);

        if (debug) {
            System.out.println("DEBUG CodaTN toCodaRow(): "+codaRow.toString());
        }
        return codaRow;
    }

    protected void timeAmpsToCodaRow(org.trinet.jdbc.table.Coda codaRow) {
        if (windowTimeAmpPairs == null) return; // no data
        int size = windowTimeAmpPairs.size();
        if (size == 0) return;

        int idx = 0;
        TimeAmp ta =  (TimeAmp) windowTimeAmpPairs.get(idx);
        DataDouble dd = ta.getTime();
        if (! dd.isValidNumber()) return; // assume no more valid data pairs

        // Changed from a relative tau secs to absolute datetime - aww 2/10/2004
        // add absolute datetime of P arrival (pTime) secs to the window tau:
        //  timeX = FmP + datetime.doubleValue();
        // First check to see datetime is valid
        if (! datetime.isValidNumber()) {
          System.err.println("CodaTN Unable to update Coda database row have an invalid datetime!");
          return;
        }

        // Coda pTime should already have been added to window tau by CodaGenerator so make it 0. here:
        double pTime = 0. ; // datetime.doubleValue(); // aww change 4/2004
        //double absTime = DataSource.nominalToDbTimeBase(dd.doubleValue() + pTime); // removed for leap secs 2008/01/24 -aww
        double absTime = dd.doubleValue() + pTime;
        codaRow.setValue(org.trinet.jdbc.table.Coda.TIME1, absTime);
        dd = ta.getAmp();
        if (dd.isValidNumber()) codaRow.setValue(org.trinet.jdbc.table.Coda.AMP1, dd);
        if (++idx == size) return;

        ta =  (TimeAmp) windowTimeAmpPairs.get(idx);
        dd = ta.getTime();
        if (! dd.isValidNumber()) return;
        //absTime = DataSource.nominalToDbTimeBase(dd.doubleValue() + pTime); // removed for leap secs 2008/01/24 -aww
        absTime = dd.doubleValue() + pTime;
        codaRow.setValue(org.trinet.jdbc.table.Coda.TIME2, absTime);
        dd = ta.getAmp();
        if (dd.isValidNumber()) codaRow.setValue(org.trinet.jdbc.table.Coda.AMP2, dd);
        if (++idx == size) return;

        ta =  (TimeAmp) windowTimeAmpPairs.get(idx);
        dd = ta.getTime();
        if (! dd.isValidNumber()) return;
        //absTime = DataSource.nominalToDbTimeBase(dd.doubleValue() + pTime); // removed for leap secs 2008/01/24 -aww
        absTime = dd.doubleValue() + pTime;
        codaRow.setValue(org.trinet.jdbc.table.Coda.TIME3, absTime);
        dd = ta.getAmp();
        if (dd.isValidNumber()) codaRow.setValue(org.trinet.jdbc.table.Coda.AMP3, dd);
        if (++idx == size) return;

        ta =  (TimeAmp) windowTimeAmpPairs.get(idx);
        dd = ta.getTime();
        if (! dd.isValidNumber()) return;
        //absTime = DataSource.nominalToDbTimeBase(dd.doubleValue() + pTime); // removed for leap secs 2008/01/24 -aww
        absTime = dd.doubleValue() + pTime;
        codaRow.setValue(org.trinet.jdbc.table.Coda.TIME4, absTime);
        dd = ta.getAmp();
        if (dd.isValidNumber()) codaRow.setValue(org.trinet.jdbc.table.Coda.AMP4, dd);
        if (++idx == size) return;

        ta =  (TimeAmp) windowTimeAmpPairs.get(idx);
        dd = ta.getTime();
        if (! dd.isValidNumber()) return;
        //absTime = DataSource.nominalToDbTimeBase(dd.doubleValue() + pTime); // removed for leap secs 2008/01/24 -aww
        absTime = dd.doubleValue() + pTime;
        codaRow.setValue(org.trinet.jdbc.table.Coda.TIME5, absTime);
        dd = ta.getAmp();
        if (dd.isValidNumber()) codaRow.setValue(org.trinet.jdbc.table.Coda.AMP5, dd);
        if (++idx == size) return;

        ta =  (TimeAmp) windowTimeAmpPairs.get(idx);
        dd = ta.getTime();
        if (! dd.isValidNumber()) return;
        //absTime = DataSource.nominalToDbTimeBase(dd.doubleValue() + pTime); // removed for leap secs 2008/01/24 -aww
        absTime = dd.doubleValue() + pTime;
        codaRow.setValue(org.trinet.jdbc.table.Coda.TIME6, absTime);
        dd = ta.getAmp();
        if (dd.isValidNumber()) codaRow.setValue(org.trinet.jdbc.table.Coda.AMP6, dd);
        if (++idx == size) return;
        return;
    }

    /** Put the channel name attributes in the Coda DataTableRow */
    protected void setRowChannelId(StnChlTableRow aRow, Channel chan) {
        //DataTableRowUtil.setRowAuthChannelId(aRow, chan); // generic, but below is quicker
        ChannelName cn = chan.getChannelName();
        // by default all ChannelName fields are "" rather then null strings: see ChannelName
        aRow.setValue(org.trinet.jdbc.table.Coda.STA, cn.getSta()); // this attribute can't be null
        String str = cn.getNet();
        if (str.length() > 0) aRow.setValue(org.trinet.jdbc.table.Coda.NET, str);
        str = cn.getAuth();
        if (str.length() > 0) aRow.setValue(org.trinet.jdbc.table.Coda.AUTH, str);
        str = cn.getSubsource();
        if (str.length() > 0) aRow.setValue(org.trinet.jdbc.table.Coda.SUBSOURCE, str);
        str = cn.getChannel();
        if (str.length() > 0) aRow.setValue(org.trinet.jdbc.table.Coda.CHANNEL, str);
        str = cn.getChannelsrc();
        if (str.length() > 0) aRow.setValue(org.trinet.jdbc.table.Coda.CHANNELSRC, str);
        str = cn.getSeedchan();
        if (str.length() > 0) aRow.setValue(org.trinet.jdbc.table.Coda.SEEDCHAN, str);
        str = cn.getLocation();
        if (str.length() > 0) aRow.setValue(org.trinet.jdbc.table.Coda.LOCATION, str);
    }


    /**
    * Copy DataObjects out of a jdbc.table.AssocCoO object into this Coda.
    */
    protected void parseAssocCoO(AssocCoO assoc) {
        //orid = (DataLong) assoc.getDataObject(AssocCoO.ORID);
        DataDouble dd = (DataDouble) assoc.getDataObject(AssocCoO.DELTA);
        //if (dd.isValid()) setDistance(dd.doubleValue()); // no it should not be slant aww 06/11/2004
        if (dd.isValid()) setHorizontalDistance(dd.doubleValue()); // epicentral from archive aww 06/11/2004
        dd = (DataDouble) assoc.getDataObject(AssocCoO.SEAZ);
        if (dd.isValid()) setAzimuth(dd.doubleValue());
    }

    /** Copy DataObjects out of a jdbc.table.AssocCoM object into this Coda. */
    protected void parseAssocCoM(AssocCoM assoc) {
        channelMag.value      = (DataDouble) assoc.getDataObject(AssocCoM.MAG);
        channelMag.residual   = (DataDouble) assoc.getDataObject(AssocCoM.MAGRES);
        // AN IMPORTANCE COLUMN DOESN'T EXIST IN AssocCoM
        //channelMag.importance    = (DataDouble) assoc.getDataObject(AssocCoM.IMPORTANCE);
        channelMag.correction = (DataDouble) assoc.getDataObject(AssocCoM.MAGCORR);
        channelMag.weight     = (DataDouble) assoc.getDataObject(AssocCoM.WEIGHT);
        //quality = (DataDouble) assoc.getDataObject(AssocCoM.IN_WGT); // aww 3/04
        channelMag.inWgt = (DataDouble) assoc.getDataObject(AssocCoM.IN_WGT); //aww 3/04
        //
        // Below handles "rejected" coda read from db -aww 2009/06/12
        if (channelMag.inWgt.isValid()) {
            setReject(channelMag.inWgt.doubleValue() == 0.);
        }

        // DataBoolean db = (DataBoolean) assoc.getDataObject(AssocCoM.IN_WGT_USE);
        // if (db.isValid()) reject = ! db.booleanValue(); // aww 06/22/2006 not implemented yet
    }

    /** Creates a AssocCoM object from the data of this Coda.*/
    private AssocCoM toAssocCoMRow() {

        AssocCoM assocRow = new AssocCoM(); // keys are magid, coid
        assocRow.setUpdate(true); // set flag to enable processing

        // For assoc row using auth and subsource from reading is confusing
        // if the assoc row is supposed to refer to its "creator" then should 
        // use auth and subsource of the associated magnitude - aww 01/11/2005
        MagnitudeTN magTN = (MagnitudeTN) magnitude;

        // Constrained "NOT NULL" in the schema, hypothetically "" or NaN could be a legit value string 
        assocRow.setValue(AssocCoM.MAGID, magTN.magid);
        assocRow.setValue(AssocCoM.COID, coid);
        //String str = magTN.getAuthority(); // removed -aww 2011/08/17
        String str = getClosestAuthorityString(); // added -aww 2011/08/17
        // force null check here
        if (!NullValueDb.isNull(str)) assocRow.setValue(AssocCoM.AUTH, str);

        // ALLOWED TO BE NULL
        //if (source.isValid()) assocRow.setValue(AssocCoM.SUBSOURCE, source); // removed 01/11/2005 aww
        str = magTN.getSource();
        if (!NullValueDb.isNull(str)) assocRow.setValue(AssocCoM.SUBSOURCE, str);
        //if (quality.isValid())             assocRow.setValue(AssocCoM.IN_WGT, quality); // aww 03/04
        if (channelMag.inWgt.isValid())      assocRow.setValue(AssocCoM.IN_WGT, channelMag.inWgt); // aww 03/04
        if (channelMag.value.isValid())      assocRow.setValue(AssocCoM.MAG, channelMag.value);
        if (channelMag.residual.isValid())   assocRow.setValue(AssocCoM.MAGRES, channelMag.residual);
        // AN IMPORTANCE COLUMN DOESN'T EXIST IN AssocCoM
        //if (channelMag.importance.isValid()) assocRow.setValue(AssocCoM.IMPORTANCE, channelMag.importance);
        if (channelMag.correction.isValid()) assocRow.setValue(AssocCoM.MAGCORR, channelMag.correction);
        if (channelMag.weight.isValid())     assocRow.setValue(AssocCoM.WEIGHT, channelMag.weight);
        if (processingState.isValid())       assocRow.setValue(AssocCoM.RFLAG, processingState);
        //? if (channelMag.quality.isValid()) assocRow.setValue(AssocCoM.IMPORTANCE, channelMag.quality);

        //assocRow.setValue(AssocCoM.IN_WGT_USE,  reject); // aww 06/22/2006 not implemented yet

        if (debug) {
          System.out.println("DEBUG CodaTN toAssocCoMRow(): "+assocRow.toString());
        }

        return assocRow;
    }

    /**
    * Creates a AssocCoO object from the data of this Coda.
    */
    private AssocCoO toAssocCoORow() {

        AssocCoO assocRow = new AssocCoO(); // keys are orid and coid
        assocRow.setUpdate(true); // set flag to enable processing

        // For assoc row using auth and subsource from reading is confusing
        // if the assoc row is supposed to refer to its "creator" then should 
        // use auth and subsource of the associated origin - aww 01/11/2005
        SolutionTN solTN = (SolutionTN) sol;
        long assocOrid = solTN.getOridValue();
        //setOrid(assocOrid);

        // Constrained "NOT NULL" in the schema, hypothetically "" or NaN could be a legit value string 
        assocRow.setValue(AssocCoO.ORID, assocOrid);
        assocRow.setValue(AssocCoO.COID, coid);
        //String str = solTN.getAuthority(); // removed -aww 2011/08/17
        String str = getClosestAuthorityString(); // added  -aww 2011/08/17
        // force null check here
        if (!NullValueDb.isNull(str)) assocRow.setValue(AssocCoO.AUTH, str);

        // ALLOWED TO BE NULL
        //if (source.isValid()) assocRow.setValue(AssocCoO.SUBSOURCE, source); // aww 01/11/2005 removed
        str = solTN.getSource();
        if (!NullValueDb.isNull(str)) assocRow.setValue(AssocCoO.SUBSOURCE, str);

        //double x = getDistance(); // no don't save slant - aww 06/11/2004
        double x = getHorizontalDistance(); // archive epicentral aww 06/11/2004
        if ( ! (x == Channel.NULL_DIST || Double.isNaN(x)) )
            assocRow.setValue(AssocCoO.DELTA, x);
        x = getAzimuth();
        if ( ! (x == Channel.NULL_AZIMUTH || Double.isNaN(x)) )
            assocRow.setValue(AssocCoO.SEAZ, x);
        if (processingState.isValid()) assocRow.setValue(AssocCoO.RFLAG, processingState);

        if (debug) {
          System.out.println("DEBUG CodaTN toAssocCoORow(): "+assocRow.toString());
        }

        return assocRow;
    }

    /** Does a no-op, returns false. Should copy data from the input to this instance. */
    public boolean replace(org.trinet.jasi.Coda aCoda) {
        return false;
    }
    /** Does a no-op, returns false. Should copy dependent data from the input to this instance. */
    public boolean copySolutionDependentData(org.trinet.jasi.Coda newCoda) {
        return false;
    }

    //public void setOrid(long id) {orid.setValue(id);}
    public Object getOriginIdentifier() {
        //return orid;
        return (sol == null) ? null : sol.getPreferredOriginId();
    }
    public long getOridValue() {
        //return (orid.isValidNumber()) ? orid.longValue() : 0l;
        if (sol == null) return 0l;
        DataLong orid = (DataLong) sol.getPreferredOriginId();
        return  (orid.isValidNumber()) ? orid.longValue() : 0l;
    }
    public long getMagidValue() {
        DataLong dl = (DataLong)getAssociatedMag().getIdentifier();
        return  (dl.isValidNumber()) ?  dl.longValue() : 0l;
    }

    public Object clone() {
        CodaTN cdTN = (CodaTN) super.clone();
        cdTN.coid = (DataLong) coid.clone();
        //cdTN.orid = (DataLong) orid.clone();
        return cdTN;
    }

} // End of CodaTN class
