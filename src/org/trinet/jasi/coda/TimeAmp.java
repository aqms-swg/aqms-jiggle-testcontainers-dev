package org.trinet.jasi.coda;
import org.trinet.jdbc.datatypes.DataDouble;
import org.trinet.jdbc.datatypes.DataNumber;

public class TimeAmp implements Cloneable, java.io.Serializable {
    private static final long serialVersionUID = 1L;
    private final DataDouble time;
    private final DataDouble amp;

    private TimeAmp(DataDouble time, DataDouble amp) {
        if (!time.isNull() && !amp.isNull()) {
            final double timeValue = time.doubleValue();
            final double ampValue = amp.doubleValue();
            if (timeValue <= 0.0D || ampValue <= 0.0D) {
                try {
                    throw new IllegalArgumentException("invalid value (" + timeValue + ", " + ampValue + ")");
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                // do not use absolute value
                // time.setValue(Math.abs(timeValue));
                // amp.setValue(Math.abs(ampValue));
            }
        }
        this.time = time;
        this.amp = amp;
    }

    public TimeAmp(DataNumber time, DataNumber amp) {
        this(time.isNull() ? new DataDouble() : new DataDouble(time.doubleValue()),
                amp.isNull() ? new DataDouble() : new DataDouble(amp.doubleValue()));
    }

    public TimeAmp(double time, double amp) {
      this(new DataDouble(time), new DataDouble(amp));
    }

    @Override
    public String toString() {
       StringBuffer sb = new StringBuffer(48);
       sb.append(time.toStringSQL()).append(" ").append(amp.toStringSQL());
       return sb.toString();
    }

    public String toString(char delimiter) {
       StringBuffer sb = new StringBuffer(48);
       sb.append(time.toStringSQL()).append(delimiter).append(amp.toStringSQL());
       return sb.toString();
    }

    public boolean hasNullValue() {
      return time.isNull() || amp.isNull();
    }

    public boolean isValid() {
      return !hasNullValue() && amp.doubleValue() > 0.0D && time.doubleValue() > 0.0D; 
    }

    public DataDouble getTime() { return time; }
    public DataDouble getAmp() { return amp; }
    public double getTimeValue() { return time.doubleValue();}
    public double getAmpValue() { return amp.doubleValue();}
    public void setAmpValue(double value) { amp.setValue(value);}
    public void setTimeValue(double value) { time.setValue(value);}

    @Override
    public boolean equals(Object obj) {
      if ( ! (obj instanceof TimeAmp) ) return false;
      TimeAmp ta = (TimeAmp) obj;
      return time.equalsValue(ta.time) && amp.equalsValue(ta.amp);
    }
    
    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + time.hashCode();
        result = 31 * result + amp.hashCode();
        return result;
    }

    @Override
    public Object clone() {
      return new TimeAmp((DataNumber) time, (DataNumber) amp);
    }
}

