package org.trinet.jasi.engines;

import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;

public abstract class AbstractChannelDataEngine extends AbstractGenericEngine implements ChannelDataEngineIF {

    /** Channel list for looking up lat/lon, gain, etc. */
    protected ChannelList chanList = null;

    protected String channelCacheFileName;

    protected boolean doLookUp = true; // should default be true or false?
    protected boolean debugDumpsChannel = false;

    protected AbstractChannelDataEngine() {
      super();
    }

    public void setDefaultProperties() {
        super.setDefaultProperties();
        doLookUp = true; // should default be true or false?
        debugDumpsChannel = false;
    }

    //?? DK 
    public void configure(int source, String location, String section, ChannelList channelList) {
      configure(source,location,section);
      setChannelList(channelList);
    }

    protected boolean loadProperties(String propFileName) {
        props =  new SolutionEditorPropertyList(); // empty list
        //Force user property filename to input
        boolean status = props.readPropertiesFile(propFileName);
        if (status) status = ((SolutionEditorPropertyList) props).setup();
        props.setUserPropertiesFileName(propFileName);
        return status;
    }

    public void setLookUp(boolean tf) {
      doLookUp = tf;
      //ancillary Channel data lookup flag, added 11/30/2005 -aww
      //Channel.setDbLookup(tf); // removed 2011/11/12 -aww
    }

    /** Set the channel list for looking up channel lat/lon, gain, etc. */
    public void setChannelList(ChannelList list) {
      chanList = list;
      if (debug && list == null) System.out.println("DEBUG: "+getClass().getName()+": setChannelList(NULL)");
    }
    public ChannelList getChannelList() {
      return chanList;
    }

    /**
     * Lookup channel info for the input Channelable collection from the List channels
     * tries to find data in DataSource if not found in input List and doLookUp == true
     */
    public void channelLookup(List list, java.util.Date date) {
      if (list == null || list.isEmpty()) {
        if (debug) System.out.println("DEBUG: Engine channelLookup(list,date) input data list null or empty " + getEngineName());
        return;
      }
      else if (debug) System.out.println("DEBUG: Engine channelLookup(list,"+date+") " + getEngineName());

      /* Check for empty or null channel list - removed 06/14/2005 -aww test to see if anything breaks!
      // force lookup from source
      if (! hasChannelList()) {
        if (debug) System.out.println("DEBUG: Engine no ChannelList exists, invoking initChannelList(true, date)");
        // false = don't try cache go to data source for data valid on input date
        if ( ! initChannelList(true, date)) return;
      }
      */

      Channelable jr = null;
      Channel ch = null;
      int count = list.size();
      for (int i = 0; i < count; i++) {
        jr = (Channelable) list.get(i);
        if (jr != null) {
          ch = jr.getChannelObj();
          // WARNING BEWARE!!!
          // BELOW TEST ASSUMES NO CHANGE TO ChannelName W/O UPDATE OF LatLonZ
          if (! ch.hasLatLonZ()) { // lookup, else ASSUME it's okay. 
            //ChannelList.debug = true;
            jr.setChannelObjData(channelLookup(ch, date));
            //ChannelList.debug = false;
            //if (debug) System.out.println("DEBUG: Engine channelLookup(List,date) at: "+ch.toDumpString());
          }
        }
      }
    }

    /**
     * Lookup channel info in the internal ChannelList for the input Channel.
     */
    public Channel channelLookup(Channel chan, java.util.Date date) {
      if (chan == null) return chan;
      Channel ch = null;
      if (hasChannelList()) { // use engine channel list if available
        ch = getChannelFromList(chan, date);
        if (ch != null) {
          if (ch != chan) {
            if (! ch.hasLatLonZ()) {
              System.out.println("WARNING master reference list channel MISSING LatLonZ: " + ch.toString());
            }
            return ch;
          }
        }
      }

      if (! doLookUp) return chan; // don't try a retrieve from datasource
      //if (debug) System.out.println("DEBUG: channelLookup invoking chan.lookUp(chan,date) ... ");
      ch = chan.lookUp(chan, date); // if not in list, lookup in data source
      if (debugDumpsChannel) {
        System.out.println("DEBUG: Engine DATASOURCE lookUp for date: "+date
          + "\n Returned chan: " + ((ch == null) ? "null" : ch.toDumpString())
        );
      }
      if (ch != null) { // have something
        if (ch != chan ) { // assume it's a new channel object
           if (chanList == null) {
             chanList = new ChannelList();
             if (debug) System.out.println("DEBUG: Engine channelLookup CREATED new chanList");
           }
           // LOOKOUT! check that ChannelList methods update map object if chanList contents are modified
           chanList.add(ch); // add new lookup to engine channelList
           if (verbose) System.out.println("INFO: Engine added "+ch+" to chanList, new size(): "+chanList.size());
        }
        return ch; // return lookup
      }
      else return chan; // return input if not found
    }

    /**
     * Calculate distance for each channel to the event hypocenter. The objects
     * in the list must implement the Channelable interface. The distance
     * value is put in Object.chan.distance. Input <i>z</i> must use depth convention,
     * positive depth downward, thus an elevation above the geoid would be negative.
     */
    public void calcDistances(List list, double lat, double lon, double z) {
      calcDistances(list, new LatLonZ (lat, lon, z));
    }

    /**
     * Calculates the distance from input location to the Channelable elements
     * of the input list and sets the distance attribute of these elements.
     */
    public void calcDistances(List list, GeoidalLatLonZ latLonZ) { // aww 06/11/2004
      int count = list.size();
      if (count == 0) return;
      for (int i = 0; i<count; i++) {
        ((Channelable) list.get(i)).calcDistance(latLonZ);
      }
    }

    public void setChannelCacheFileName(String name) {
      channelCacheFileName = name;
    }
    public boolean initChannelList(java.util.Date date) {
      return initChannelList(true, date);
    }
    public boolean initChannelList(boolean tryCacheFirst, java.util.Date date) {
        if (tryCacheFirst && channelCacheFileName != null && channelCacheFileName.length() > 0) {
          chanList = createChannelList(channelCacheFileName);
        }
        if (! hasChannelList() && doLookUp) {
          // time intensive lookUp of all datasource channels, better response using cached data
          chanList = createChannelListFromDataSource(date);
        }
        if (! hasChannelList()) {
            statusString = "ERROR: Engine channel list is null or empty ! " + getEngineName();
            System.err.println(statusString);
            return false;
        }
        chanList.createLookupMap();
        return true;
    }
    public final boolean hasChannelList() {
        return (chanList != null && ! chanList.isEmpty());
    }
    public boolean storeChannelList(String filename) {
        if (chanList == null || filename == null) return false;
        return chanList.writeToCache(filename);
    }
    public ChannelList createChannelList(String filename) {
        if (debug) System.out.println("DEBUG: Engine createChannelList(filename)" + filename);
        return (filename == null) ? null : ChannelList.readFromCache(filename);
    }
    public ChannelList createChannelListFromDataSource(java.util.Date date) {
        //chanList = ChannelListWrapper.getListBySQL(toChannelListSQLString(date));
        /* IF WE WANT to load only those types specified to contribute to result need method like:
          String [] acceptable = getAcceptableChannelTypes();
          if (acceptable.length > 0 ) chanList = ChannelList.getByComponent(acceptable, date);
          if (chanList.isEmpty()) {
            chanList = ChannelList.getByComponent(defaultComponentList, date);
            System.out.println("DEBUG: Engine initialized ChannelList gotByComponent: (" +
                            defaultComponentList+") count: "+chanList.size());
          }
        */
        System.out.println("INFO: Engine creating a reference ChannelList from DataSource for time: " +
                        date + " " + getEngineName());
        chanList = ChannelList.readList(date);
        if (channelCacheFileName != null && chanList != null && ! chanList.isEmpty()) {
          System.out.println("INFO: Engine storing channelList read to channelCacheFileName: " + channelCacheFileName); //aww test
          storeChannelList(channelCacheFileName);
        }
        return chanList;
    }

    public Channel getChannelFromList(Channel inputChannel, java.util.Date date) {
        if (! hasChannelList()) {
          statusString =
            "ERROR: Engine getChannelFromList null/empty list, do setChannelList(List) or initChannelList(date)";
          System.err.println(statusString);
          return null;
        }
        //Channel chan = chanList.findSimilarInList(inputChannel); // linear, map quicker
        Channel chan = chanList.findSimilarInMap(inputChannel, date);
        if (debugDumpsChannel) {
          System.out.println("DEBUG: Engine getChannelFromList for date: " +date
            + "\n Returned chan: " + ((chan == null) ? "null" : chan.toDumpString())
          );
        }
        if (chan == null) {
            if (verbose)
                System.out.println(
                   "INFO: Engine getChannelFromList unable to find channel:" +
                   inputChannel.toDelimitedSeedNameString()
                );
        }
        return chan;
    }

    // utility Level 
    protected static final double getMaxGap(double[] azimuth, int length) {
        int size = (azimuth == null) ? 0 : length;
        if (size < 2 ) return 360.;

        int maxIdx = size - 1;
        double deltaAz = 0.;
        double maxGap  = 0.;
        for (int idx = 0; idx < maxIdx; idx++) {
            deltaAz = Math.abs(azimuth[idx+1] - azimuth[idx]);
            if (deltaAz > maxGap) maxGap = deltaAz;
        }
        deltaAz = 360. - Math.abs(azimuth[maxIdx]) + Math.abs(azimuth[0]);
        return (deltaAz > maxGap) ? deltaAz : maxGap;
    }

    public void initializeEngine() {
      super.initializeEngine();
      if (props == null) props = new SolutionEditorPropertyList(); // empty list
      channelCacheFileName = props.getUserFileNameFromProperty("channelCacheFilename");
      String tmp = props.getProperty("channelDbLookUp");
      if (tmp != null) doLookUp = props.getBoolean("channelDbLookUp");
      else props.setProperty("channelDbLookUp", doLookUp);

      if (debug) System.out.println("DEBUG: " + getEngineName() + " db channelList lookup enabled: " + doLookUp);
      //
      if (props.isSpecified("channelDebugDump"))
        debugDumpsChannel = props.getBoolean("channelDebugDump");
      else
          props.setProperty("channelDebugDump", debugDumpsChannel); 
    }

}
