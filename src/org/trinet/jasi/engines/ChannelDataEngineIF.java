package org.trinet.jasi.engines;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;

public interface ChannelDataEngineIF extends EngineIF {

    //public boolean isAcceptable(Channel chan);

    public void setChannelCacheFileName(String name);

    /** Sets the ChannelList to be used to update the Channel site
     * coordinates and correction data of dependent observations
     * (Amplitude, Coda, etc).
     */
    public void setChannelList(ChannelList list);
    public ChannelList getChannelList();

    /** Set persistent Channel data from the ChannelList for each element of
     *  the input list of data observations  (Coda, Amplitudes, etc)
     */
    public void channelLookup(List list, java.util.Date date);

    /** Set persistent data from the ChannelList for the input 
     * Channel from an observation (Coda, Amplitudes, etc).
     */
    public Channel channelLookup(Channel chan, java.util.Date date);

    /** Set true looks for missing Channel data in data source, if found adds it to
     * the current ChannelList, if set false only probes the current ChannelList for data. */ 
    public void setLookUp(boolean tf);

    /** Calculate the Channel distances relative to the input coordinates
     *  for each element of the input List of data observations
     * (Coda, Amplitudes, etc).
     */
    public void calcDistances(List list,double lat,double lon,double z);
    public void calcDistances(List list, GeoidalLatLonZ latLonZ);// aww 06/11/2004

    // method below is it needed ? -aww
    public void configure(int source, String location, String section, ChannelList channelList);
}
