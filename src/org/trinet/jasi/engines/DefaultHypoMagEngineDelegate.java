// TODO: implement graphics in Jiggle,Mung override implementations
//hard-coded methods vs. notification of listeners via events?
    //abstract public boolean locate(Solution sol);
    //abstract public boolean calcMag(Magnitude mag, Solution sol);
    //abstract public boolean calcMag(Solution sol, String magType) ;
    //abstract public boolean calcMagFromWaveforms(Solution sol, String magType) ;
    //abstract protected void reportMessage(String titleType, String message);

package org.trinet.jasi.engines;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.sql.*;
import java.util.*;

import jregex.Pattern;

import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;
import org.trinet.jasi.engines.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.jdbc.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;
import org.trinet.util.locationengines.*;

public class DefaultHypoMagEngineDelegate extends AbstractHypoMagEngineDelegate {

    static protected boolean needsSolutionSolveUpdate = false;
    static protected boolean needsMagnitudeSolveUpdate = false;

    static protected String solHeaderStr;
    static protected String magHeaderStr;

    // current solution being processed by delegate
    static protected Solution  assocSol;
    //magnitude associated with assocSol, the older (prior) if new magnitude calc fails
    static protected Magnitude assocMag;

    public DefaultHypoMagEngineDelegate() { }

    public DefaultHypoMagEngineDelegate(String propFileName) {
      setDelegateProperties(propFileName);
    }
    public DefaultHypoMagEngineDelegate(GenericPropertyList props) {
      setDelegateProperties(props);
    }

// PropertyChangeListenerIF override implementation:
    public void propertyChange(PropertyChangeEvent e) {
        String propName = e.getPropertyName();
        if (propName.equals(EngineIF.SOLVED)) {
          Object value = e.getNewValue();
          if (value instanceof Solution) {
            Solution newSol = (Solution) value;
            if (newSol == assocSol) updateLocationResults(newSol);
          }
          else if (value instanceof Magnitude) {
            Magnitude newMag = (Magnitude) value;
            if (newMag.isAssociatedWith(assocSol)) updateMagnitudeResults(newMag);
          }
          firePropertyChange(propName, e.getOldValue(),e.getNewValue());
        }
    }

    // implementation of abstract method in super class
    protected void reportMessage(String titleType, String message) {
        System.out.println(titleType+" "+message);
        if (debug) System.out.println(" for engine delegate: " + getClass().getName());
    }

    public boolean locate(Solution aSol) {
      solveSuccess = false;
      assocSol = aSol;

      if (! okToLocate(aSol)) return false;

      /* TEST: moved block below from okToLocate above, so as to return no-op of "true" -aww 12/23/04
      if ( ! resolveSolPhaseList(aSol) ) {
          statusString = ">>> EngineDelegate too few phases; solution location skipped for id: " +
              aSol.getId().longValue();
          System.out.println(statusString);
          return true;
      }
      */

      logSolutionSummary("Prior Solution: ", true); // current panel associated solution settings
      needsSolutionSolveUpdate = true;

      LocationEngineIF locEng = getLocationEngine();
      solveSuccess = locEng.solve(aSol, aSol.getPhaseList());
      // NOTE after solveSuccess we don't reset the Event AUTH here since event already in the db (i.e. it's not a subnet TRIGGER or new event)
      // If policy is changed to reset event auth if it's inside a different network region, we need implementation either here or in the commit method.
      reportEngineCalculationStatus(locEng);
      return solveSuccess;
      // fired property change for solveSuccess, dependent panels should listen need to cascade downward
      // if solPanel id selection not in sync with jsaep and jsaep selection not in sync with jmaep contents
      // below done by Engine propertyChange listener: firePropertyChange(EngineIF.SOLVED, ...)
      // aSol.getPreferredMagnitude().setStale(true); // force stale state here?
      // System.out.println("DEBUG locate stale: " + aSol.isStale());
    }
    public boolean calcMag(Magnitude aMag, Solution aSol) {

        solveSuccess = false;
        assocMag = aMag;
        assocSol = aSol;

        // Check inputs for not null and association
        if (! checkInputAssocAreValid(aMag, aSol) ) return false;

        // Make sure solution location is ok
        if (! resolveLocation(aSol) ) return false;

        // Need channelList data for latlonz
        if (chanList == null || chanList.isEmpty()) {
           // Could create new channelList if list undefined and associated solution time is valid
           //System.out.println("... HypoMagEngineDelegate loading channel list...");
           //DateTime dt = assocSol.hasValidDateTime() ? assocSol.getDateTime() : null;
           //loadChannelList(dt);

           statusString = "EngineDelegate has null or empty ChannelList; No magnitude calculation possible.";
           reportStatusMessage("ERROR");
           return false;
        }

        // intialize an engine to solve for this magnitude type
        MagnitudeEngineIF magEng = initMagEngineForType(aSol, aMag.getTypeString());
        if (magEng == null || ! magEng.isEngineValid()) {
          statusString = "EngineDelegate can't calculate a magnitude of type: " + aMag.getTypeString();
          reportStatusMessage("ERROR");
          return false;
        }

        // Resolve input magnitude's data, if empty list,
        // load it from data source by assocSol or assocMag
        // depending on configuration properties 
        if (! resolveMagReadingList(aMag, aSol) ) return false;

        //
        //System.out.println("... HypoMagEngineDelegate:\n "+Solution.getSummaryStateString(assocSol, assocMag));
        //

        if (assocMag != null && assocMag.isSameType(aMag)) { // only report those of same type
          logMagnitudeSummary("Prior Magnitude:", true, "<OLD>");
        }

        needsMagnitudeSolveUpdate = true;
        magEng.solve(assocMag); // could set prefmag in event
        //System.out.println("DEBUG ... HypoMagEngineDelegate calcMag reading List" + aMag.getReadingList().toString());
        reportEngineCalculationStatus(magEng);
          if (magEng.success()) {
              solveSuccess = true;
              assocMag = magEng.getSummaryMagnitude(); // redundant insurance, assign 1st done by listener update
              Magnitude eprefmag = aSol.getPreferredMagnitude();
              Magnitude mlrMag = null;
              if (myProps.getBoolean("mlrMagOptionEnabled")) {
                if ( assocMag != null && aMag.getTypeString().endsWith("l") ) {
                  Object mm = magEng.getMagMethod();
                  if ( mm instanceof MlMagMethod ) {
                      mlrMag = ((MlMagMethod)mm).createMlrMag(assocMag);
                      if (mlrMag != null) aSol.setPrefMagOfType(mlrMag);
                  }
                }
              }
              // set the assocMag to the event preferred if no event preferred
              if (eprefmag == null) {
                if (assocMag != null) {
                  // set the mlrMag to the event preferred if it exists
                  if (mlrMag != null) {
                    aSol.setPreferredMagnitude( (assocMag.getPriority() >= mlrMag.getPriority()) ? assocMag : mlrMag );
                  }
                  else {
                    aSol.setPreferredMagnitude(assocMag);
                  }
                }
              }
              // set non-null mlrMag to the event preferred, if current preferred is of type ML
              else if (eprefmag.subScript.toString().equalsIgnoreCase(MagTypeIdIF.ML) && mlrMag != null) {
                  if (mlrMag.getPriority() >= eprefmag.getPriority()) aSol.setPreferredMagnitude(mlrMag);
              }
              else if ( aMag.getTypeString().endsWith("l") && eprefmag.subScript.toString().equalsIgnoreCase(MagTypeIdIF.MLR) && mlrMag == null) {
                  // override old event preferred Mlr with new Ml - aww 10/18/2016
                  eprefmag.setDeleteFlag(true);
                  aSol.setPreferredMagnitude(assocMag);
              }
          }
        return solveSuccess;
        // fire property change, done by Engine propertyChangeListener firePropertyChange("hasNewMag", ...);
        // dependent panels should listen, panel should scrollTextPane();
    }
    public boolean calcMagFromWaveforms(Solution aSol) {
        //cf. MagTypeIdIF.ML, MagTypeIdIF.MC, MagTypeIdIF.MD
        return calcMagFromWaveforms(aSol, aSol.getPreferredMagnitude().getTypeString());
    }
    public boolean calcMagFromWaveforms(Solution aSol, String magType) {
        solveSuccess = false;
        assocSol = aSol;
        if (aSol == null) return false;

        // Kludge to handle: ML or Ml or ml as input magType
        //String subType =
        //    magType.substring((magType.startsWith("M") || magType.startsWith("m")) ? 1 : 0).toLowerCase();

        // Get prior preferred magnitude of type, if any
        assocMag = aSol.getPrefMagOfType(magType.toLowerCase());
        // Perhaps missing a prefmag table, so we need to check event preferred for type
        if (assocMag == null) assocMag = aSol.getPreferredMagnitude(); // pre-EVENPREFMAG table? -aww

        if (! resolveLocation(aSol)) return false;

        MagnitudeEngineIF magEng = initMagEngineForType(aSol, magType);
        if (magEng == null || ! magEng.isEngineValid()) {
          statusString = "EngineDelegate can't calculate a magnitude of type: " + magType;
          reportStatusMessage("ERROR");
          return false;
        }

        if (assocMag != null && assocMag.getTypeString().equalsIgnoreCase(magType)) {
          logMagnitudeSummary("Prior Magnitude:", true, "<OLD>");
        }

        needsMagnitudeSolveUpdate = true;

          // Create a popup for the duration of the run, a progress object would be better
          StringBuffer sb = new StringBuffer(132);
          sb.append("Calculating ").append("M").append(magEng.getMagMethod().getMagnitudeTypeString());
          sb.append(" for event ").append(aSol.getId()).append("\n");
          if (verbose) System.out.println(sb.toString());
          //
          // Blow away the existing reading data associated with the solution,
          // otherwise repeated calls will accumulate copies of the same data!
          // Could instead try to do list additions by invoking addOrReplace - aww
          // aSol.getListFor(magEng.getMagMethod().getReadingClass()).clear(); // let engine clear it ? 
          //
          if (debug) System.out.println("DEBUG mag calc type: "+ magEng.getMagMethod().getMethodName()
                                 +" from waveforms for: "+aSol.toString());

          java.util.List wfList = (ctwModel != null) ?
              ctwModel.getWaveformList(aSol) : magEng.getSolutionWaveformList(aSol);

          // aww - changed method logic below here for assocMag update 01/10/2005
          magEng.solveFromWaveforms(aSol, wfList); // returns a valid magnitude only when successful
          reportEngineCalculationStatus(magEng);
          if (magEng.success()) {
              solveSuccess = true;
              assocMag = magEng.getSummaryMagnitude(); // redundant insurance, assign 1st done by listener update
              Magnitude eprefmag = aSol.getPreferredMagnitude();
              Magnitude mlrMag = null;
              if (myProps.getBoolean("mlrMagOptionEnabled")) {
                if ( assocMag != null && magType.endsWith("l") ) {
                  Object mm = magEng.getMagMethod();
                  if ( mm instanceof MlMagMethod ) {
                      mlrMag = ((MlMagMethod)mm).createMlrMag(assocMag);
                      if (mlrMag != null) aSol.setPrefMagOfType(mlrMag);
                  }
                }
              }
              // set the assocMag to the event preferred if no event preferred
              if (eprefmag == null) {
                if (assocMag != null) {
                  // set the mlrMag to the event preferred if it exists
                  if (mlrMag != null) {
                    aSol.setPreferredMagnitude( (assocMag.getPriority() >= mlrMag.getPriority()) ? assocMag : mlrMag );
                  }
                  else {
                    aSol.setPreferredMagnitude(assocMag);
                  }
                }
              }
              // set non-null mlrMag to the event preferred, if current preferred is of type ML
              else if (eprefmag.subScript.toString().equalsIgnoreCase(MagTypeIdIF.ML) && mlrMag != null) {
                  if (mlrMag.getPriority() >= eprefmag.getPriority()) aSol.setPreferredMagnitude(mlrMag);
              }
              else if ( magType.endsWith("l") && eprefmag.subScript.toString().equalsIgnoreCase(MagTypeIdIF.MLR) && mlrMag == null) {
                  // override old event preferred Mlr with new Ml - aww 10/18/2016
                  eprefmag.setDeleteFlag(true);
                  aSol.setPreferredMagnitude(assocMag);
              }
          }
          return solveSuccess; // false upon failure
    }

    //Method from new IF
    /** New magnitude created from Solution database readings. Solution and Magnitude reading lists
     * have "cloned" reading instances, i.e. the associated readings are not aliased like those of
     * the other methods. If readings are "edited" this could cause duplicates for same channel.
     * */
    public boolean calcMag(Solution aSol, String magType) {
        solveSuccess = false;
        assocSol = aSol;
        //if (aSol == null) return false;  // force a null pointer below?

        MagnitudeEngineIF magEng = initMagEngineForType(aSol, magType);
        if (magEng == null || ! magEng.isEngineValid()) {
          statusString = "EngineDelegate can't calculate a magnitude of type: " + magType;
          reportStatusMessage("ERROR");
          return false;
        }

        boolean hasPrefMag = true; // assume it already has some magnitude
        Magnitude prefMag = aSol.getPreferredMagnitude();
        // Should we make a new mag or reuse preexisting mag objects? 
        // Does a preferred magnitude exist of the same type as requested?
        // if not, need to create a new mag then copy the appropiate solution
        // associated reading list into this new mag of requested type. -aww 
        // NOTE: mag.isSameType(type) is case sensitive (uses no "M" prefix only the lowercase subscript)

        if (prefMag != null && prefMag.getTypeString().equalsIgnoreCase(magType)) { // event preferred
          // By default use existing readings, a new prefMag may not have been committed to database
          if (prefMag.getReadingList().size() < 1) prefMag.loadReadingList(); // load only if readings don't exist 
        }
        else { // preferred of type, but not the event preferred
          // If there an existing mapped prefmag of type use it rather than creating a "new" mag  - aww 10/19/2004 
          int startAt = (magType.startsWith("M")) ? 1 : 0; // need to ignore any leading "M"
          prefMag = aSol.getPrefMagOfType(magType.substring(startAt).toLowerCase());
          if (prefMag == null) {  // if no prefmag of type exists, THEN make a new mag
            prefMag = magEng.getNewMag(aSol); // engine initialized to prefmag magMethod type above
            hasPrefMag = false;
          }
          // If it's NOT a newly created prefmag and has an empty reading list 
          // then try loading prefmag assoc reading list data from datasource - aww 02/14/2007
          if (prefMag.getReadingsCount() < 1 && hasPrefMag) {
              prefMag.loadReadingList();
              prefMag.associateLists(aSol); // put newly loaded datasource readings into masterview sol reading list
          }

          if (prefMag.getReadingsCount() < 1) { // prefmag is either new or has no datasource reading associations 
              prefMag.setReadingListFromAssocSolution(); // copy reading list of assoc sol
              if (prefMag.getReadingsCount() < 1) { // assoc solution has an empty reading list 
                  // Load prefmag reading list with datasource data associated with it's solution
                  prefMag.loadReadingListBySolution();
              }
          }
        }

        assocMag = prefMag; // force assocMag to be active mag before return before logMagnitude call 2017/05/30 aww

        if (prefMag != null && prefMag.getTypeString().equalsIgnoreCase(magType)) {
          logMagnitudeSummary("Prior Magnitude:", true, "<OLD>");
        }

        needsMagnitudeSolveUpdate = true;
        prefMag = (magEng.solve(prefMag)) ? magEng.getSummaryMagnitude() : prefMag ;
        //assocMag = prefMag; // force assocMag to be active mag before return, assignment moved above log call 2017/05/30 aww 
        // if success, assocMag reassigned by listener update (to newMag?)
        reportEngineCalculationStatus(magEng);

        // engine should only return success if it's a valid magnitude
        if (magEng.success()) {
          solveSuccess = true;
          Magnitude eprefmag = aSol.getPreferredMagnitude();
          // Check for ml type
          Magnitude mlrMag = null;
          if (myProps.getBoolean("mlrMagOptionEnabled")) {
            if ( prefMag != null && magType.endsWith("l") ) {
              Object mm = magEng.getMagMethod();

              if ( mm instanceof MlMagMethod ) {
                  mlrMag = ((MlMagMethod)mm).createMlrMag(prefMag);
                  if (mlrMag != null) aSol.setPrefMagOfType(mlrMag);
              }
            }
          }
          // set the calculated prefMag to the event preferred if no event preferred already
          if (eprefmag == null) {
            if (prefMag != null) {
              // set the mlrMag to the event preferred if it exists
              if (mlrMag != null) {
                aSol.setPreferredMagnitude( (prefMag.getPriority() >= mlrMag.getPriority()) ? prefMag : mlrMag );
              }
              else {
                aSol.setPreferredMagnitude(prefMag);
              }
            }
          }
          // set non-null mlrMag to the event preferred, if current preferred is of type ML
          else if (eprefmag.subScript.toString().equalsIgnoreCase(MagTypeIdIF.ML) && mlrMag != null) {
              if (mlrMag.getPriority() >= eprefmag.getPriority()) aSol.setPreferredMagnitude(mlrMag);
          }
          else if ( magType.endsWith("l") && eprefmag.subScript.toString().equalsIgnoreCase(MagTypeIdIF.MLR) && mlrMag == null ) {
              // override old event preferred Mlr with new Ml - aww 10/18/2016
              eprefmag.setDeleteFlag(true);
              aSol.setPreferredMagnitude(prefMag);
          }
          if (! hasPrefMag) System.out.println("INFO: No existing prefmag for magtype, setting prefmag for : " + magType); 
          aSol.setPrefMagOfType(prefMag); // set "preferred" of magtype
        }

        return solveSuccess;
    }

    // To avoid synch and boolean update state toggles,
    // have inner class delegate as propertyChangeListener to Engines then
    // this delegate updates, redirecting fire property change to listening parent panel.
    private synchronized void updateMagnitudeResults(Magnitude newMag) {
        //System.out.println("updateMagnitudeResults newMag: " + newMag.toDumpString());
        if (! needsMagnitudeSolveUpdate) return;
        if (newMag == null) {
          statusString = "EngineDelegate associated magnitude update failed, the new magnitude is NULL !";
          reportStatusMessage("ERROR");
          return;
        }
        assocMag = newMag; // reassign, a new magnitude instance may have been generated from data

        // WARNING: Collections.sort uses List.set() which ActiveArrayList overrides
        // and check for dupes, thus using it here results in error.
        //do not use Collections.sort(assocMag.getReadingList(),channelSorter); // aww removed 11/26/2002
        //assocMag.getReadingList().sort(channelSorter); // aww above replaced by sort method of ActiveArrayList
        //assocMag.getReadingList().channelSort(true, JasiReadingListIF.SITE_TIME_SORT); // aww 03/18/03
        assocMag.getReadingList().distanceSort(true, JasiReadingListIF.SITE_SEEDCHAN_TIME_SORT); // aww 03/18/03 2008/02/07
        if (printMagEngineReadingList()) {
            System.out.println(" Calculated magnitude reading List:\n" +
                        assocMag.getReadingList().toNeatString());
        }
        String time = ( assocMag.getSolveDate().isNull() ) ? "Unknown time" : assocMag.getSolveDate().toDateString("yyyy-MM-dd HH:mm:ss");  // use toDateString for UTC -aww 2008/02/11
        logMagnitudeSummary("Magnitude attempt: "+assocMag.getVersion()+" calculated at: "+time, printHeaders(), "<NEW>");
        needsMagnitudeSolveUpdate = false;
    }
    protected void logMagnitudeSummary(String message, boolean printHead, String suffixId) {
        if (! printMagnitudes()) {
          System.out.println(Lines.STAR_TEXT_LINE);
          return;
        }
        else System.out.println(Lines.STAR_TEXT_LINE);
        System.out.println(message);
        if (printHead) System.out.println(getMagHeader());
        System.out.println(getMagSummary(suffixId));
        if (printHead) System.out.println(Lines.MINUS_TEXT_LINE);
        else System.out.println(Lines.EQUALS_TEXT_LINE);
    }

    private String getMagHeader() { 
      if (magHeaderStr == null) {
        StringBuffer sb = new StringBuffer(132);
        sb.append(assocMag.getNeatHeader()).append("          MagID");
        magHeaderStr = sb.toString();
      }
      return magHeaderStr;
    }
    private String getSolHeader() { 
      if (solHeaderStr == null) {
        StringBuffer sb = new StringBuffer(132);
        sb.append(assocSol.getNeatHeader()).append(" ").append(assocSol.getErrorStringHeader());
        solHeaderStr = sb.toString();
      }
      return solHeaderStr;
    }
    private String getMagSummary(String suffixId) {
      StringBuffer sb = new StringBuffer(132);
      sb.append(assocMag.toNeatString());
      Concatenate.rightJustify(sb, StringSQL.valueOf(assocMag.getIdentifier()), 15);
      if (suffixId != null && suffixId.length() > 0) sb.append(" ").append(suffixId);
      return sb.toString(); 
    }
    private String getSolSummary() {
      StringBuffer sb = new StringBuffer(132);
      sb.append(assocSol.toNeatString()).append(" ").append(assocSol.toErrorString());
      return sb.toString();
    }

    private void logLocationResults() {
      //logSolutionSummary("Solution calculated:", true);
      if (printSolutions()) System.out.println(getLocationEngine().getResultsHeaderString());
      logLocationPhaseResults();
    }

    private void logLocationPhaseResults() {
        PhaseList phaseList = assocSol.getPhaseList();
        int count = phaseList.size();
        if (count > 0)  {
          if (printLocEnginePhaseList()) {
            String filterStr = getLocationEngine().getResultsPhaseString();
            //DEBUG System.out.println(filterStr);
            // 1.4 filterStr = filterStr.replaceAll("0\\.000","     ");
            // 1.4 filterStr = filterStr.replaceAll("00\\.00","     ");
            // 1.4 filterStr = filterStr.replaceAll("60\\.00","xx.xx");
            // 1.4 filterStr = filterStr.replaceAll(" 0\\.00","     ");
            // 1.4 filterStr = filterStr.replaceAll("xx\\.xx","60.00");
            // 1.4 filterStr = filterStr.replaceAll("--  0","     ");
            // 1.4 filterStr = filterStr.replaceAll("    0 ","      ");
            //
            filterStr = new Pattern("0\\.000").replacer("     ").replace(filterStr);
            filterStr = new Pattern("00\\.00").replacer("     ").replace(filterStr);
            filterStr = new Pattern("60\\.00").replacer("xx.xx").replace(filterStr);
            filterStr = new Pattern(" 0\\.00").replacer("     ").replace(filterStr);
            filterStr = new Pattern("xx\\.xx").replacer("60.00").replace(filterStr);
            filterStr = new Pattern("--  0").replacer("     ").replace(filterStr);
            filterStr = new Pattern("    0 ").replacer("      ").replace(filterStr);
            System.out.println(filterStr);
          }

          //Below line requires latlonz first be set in channel object from db master list lookup:
          //phaseList.distanceSort(assocSol.getLatLonZ()); // not guaranteed
          //Sort here, but list table model, must be informed if list mapped to model input
          //no do not use Collections.sort(phaseList,channelSorter);
          // 03/18/03, use ActiveArrayList sort implementation, because of override of list set method
          //phaseList.sort(channelSorter); // aww above replaced by sort method of ActiveArraylist
          //phaseList.channelSort(true, JasiReadingListIF.SITE_TIME_SORT); // aww 03/18/03
          phaseList.distanceSort(true, JasiReadingListIF.SITE_TIME_SORT); // aww 03/18/03

          if (printSolutionPhaseList()) {
            System.out.println(Phase.getNeatStringHeader());
            for (int idx = 0; idx < count; idx++) {
               Phase phase = (Phase) phaseList.get(idx);
               if (! phase.isDeleted())
                  System.out.println(phase.toNeatString());
            }
          }
        }
    }
    private synchronized void updateLocationResults(Solution newSol) {
      //System.out.println("DEBUG updateLocationResults:\n "+Solution.getSummaryStateString(newSol, null));
      if (! needsSolutionSolveUpdate) return;
      if (newSol == null) {
        statusString = "EngineDelegate associated solution update failed, the new solution is NULL !";
        reportStatusMessage("ERROR");
        return;
      }
      assocSol = newSol; // reassign, a new solution instance may have been generated from data
      // NOTE: Some of below are redundant, since location engine may already have set these:
      assocSol.getEventAuthority(); // resets the authority if region changed -aww 2011/09/21 
      // NOTE: regional eventAuthority and solution's origin authority could be different
      //assocSol.authority.setValue(assocSol.getEventAuthority()); // -aww 2011/08/17, removed 2011/09/21
      assocSol.authority.setValue(EnvironmentInfo.getNetworkCode()); // reverted -aww 2011/09/21
      assocSol.source.setValue(EnvironmentInfo.getApplicationName());

      if ( assocSol.isEventDbType(EventTypeMap3.UNKNOWN) || assocSol.isEventDbType(EventTypeMap3.TRIGGER) ) {
        assocSol.setEventType(EventTypeMap3.getMap().toJasiType(EventTypeMap3.EARTHQUAKE));
      }
      if ( !(assocSol.isOriginGType(GTypeMap.LOCAL) || assocSol.isOriginGType(GTypeMap.REGIONAL)) ) assocSol.setOriginGType(GTypeMap.LOCAL);

      // saved state effected by solCommitResetsRflag property which sets currentSol.commitResetsRflag -aww 2015/04/03
      if ( assocSol.commitResetsRflag && !EnvironmentInfo.isAutomatic()) {
        assocSol.processingState.setValue(JasiProcessingConstants.STATE_HUMAN_TAG);
      }
      int count = assocSol.getPhaseList().size();
      if (count < getLocationEngine().getMinPhaseCount()) {
        //assocSol.dummyFlag.setValue(1); // do not reset
        //assocSol.validFlag.setValue(0); // reset invalid - remove 2014/10/30 why is it changing the "delete" status? -aww
        System.out.println("New solution phases < " + getLocationEngine().getMinPhaseCount()+  " minPhaseCount");
      }
      else {
        //assocSol.setProcessingState(JasiProcessingConstants.STATE_FINAL_TAG); // don't reset the state, should be done only on commit -aww 2009/08/13
        assocSol.dummyFlag.setValue(0); // reset to not bogus
        //assocSol.validFlag.setValue(1); // set valid - removed 2014/10/30 why is it changing the "delete" status? -aww
      }
      // just in case if not done by LocationEngine?
      assocSol.setStale(false);
      needsSolutionSolveUpdate = false;
      //System.out.println("DEBUG updateLocationResults stale: " + assocSol.isStale());
      logSolutionResults();
    }
    private void logSolutionResults() {
      int count = assocSol.getPhaseList().size();
      LocationEngineIF locEng = getLocationEngine();
      if (count < locEng.getMinPhaseCount()) {
        statusString = "Solution is stale, invalid, or bogus or phase count < min phases !" +
        "\n engine message: " + locEng.getStatusString();
        reportStatusMessage("INFO");
      }
      else { 
        String time = ( assocSol.getSolveDate().isNull() ) ? "Unknown time" : assocSol.getSolveDate().toDateString("yyyy-MM-dd HH:mm:ss");  // use toDateString for UTC -aww 2008/02/11
        logSolutionSummary("Solution attempt: "+assocSol.getVersion()+" calculated at: "+time, false); // test here aww, may be excessive
      }
      logLocationResults();
      System.out.println(Lines.MINUS_TEXT_LINE);
    }
    private void logSolutionSummary(String message, boolean printHead) {
      if (! printSolutions()) return;
      if (printHead) System.out.println(Lines.PLUS_TEXT_LINE);
      System.out.println(message);
      if (printHead) System.out.println(getSolHeader());
      System.out.println(getSolSummary());
      System.out.println(Lines.MINUS_TEXT_LINE);
    }

// Tied into action
    private boolean printMagEngineReadingList() {
      return myProps.getBoolean("printMagEngReadingList");
    }
    private boolean printLocEnginePhaseList() {
      return myProps.getBoolean("printLocEngPhaseList");
    }
    private boolean printSolutionPhaseList() {
      return myProps.getBoolean("printSolutionPhaseList");
    }
    private boolean printSolutions() {
      return myProps.getBoolean("printSolutions");
    }
    private boolean printMagnitudes() {
      return myProps.getBoolean("printMagnitudes");
    }
    private boolean printHeaders() {
      return myProps.getBoolean("printHypoMagHeaders");
    }

}
