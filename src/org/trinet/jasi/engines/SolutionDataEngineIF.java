package org.trinet.jasi.engines;
import java.util.*;
import org.trinet.jasi.*;

public interface SolutionDataEngineIF extends ChannelDataEngineIF {

    public Solution getSolution() ;
    public void setSolution(Solution sol) ;

    public void setUseExistingData(boolean tf);
    public boolean useExistingData() ;

    public boolean solve(Solution sol);

    public List getSolutionWaveformList(Solution sol);
    public boolean solveFromWaveforms(Solution aSol, List wfList);
}
