// ChannelList channel corr lookup vs. separate CalibrationList creation/lookup 
// a method in ChannelList to extract particular correction types by daterange 
// into a new ChannelDataMap collection of that type for all channels in list - aww
//
// A mag subtype class should exist for each reading type (e.g. Mc, Md, Ml),
//
// Data filtering: reading deletion and setting weights(in/out) = 0. 
// Set weights 0. e.g. setInputWeight(jr, 0.) in lieu of jr.deleteReading in orderto
// save data in lists for database commits.
// User must commit readings to preserve results before re-solving for the same mag subtype.
// A new mag instance should be either freshly loaded with data from the db or from
// readings cloned from an existing mag,sbl list appropiate for that mag subtype
//
//Proposed logic for the setting of input reading's weight-like attributes by implementations
//for calcChannelMag:
//  -> "init" all ChannelMags, set reading quality = 0 ? to remove ambiguity of whether the reading quality
//       is from the original waveform scan creation or post-filter calcMag assignWts, perhaps "generator"
//       can also put its original quality value as relative numeric positional text component of its descriptor 
//       user/program can always assign true "quality" weight by subsequent processing below.
//  -> preChanFilter(delete the unwanted initialized data )
//  -> calcChanMagValue (set new reading quality = assignWts(for those readings passed thru by filter)
//
//for calcSummaryMag:
//   NOTE: The input weight for summary mag processing is obtained from jr.channelMag.inWgt
//   which is initialzed from the reading's assigned weight (jr.quality)
//   -> preSumFilter(reject)  (for summary set ChannelMag.inWgt = reading quality or 0 if rejected, via )
//   -> iterate next two steps until imposed summary constraints are satified or failure:
//   ->   calcSummaryMagValue(get inWgt, process, set weight(Used) = inWgt or algorithm derived value (e.g. tapered wts)
//   ->   postSumFilter(reject) (inWgt = original inWgt or 0, if inWgt changed iterate again)
//Database Map:
//  jr.quality<=>XXTable.quality, jr.channelMag.inWgt<=>AssocXXM.in_wgt, jr.channelMag.weight<=>AssocXXM.weight

package org.trinet.jasi.magmethods;
import java.util.*;
import org.trinet.filters.*;
import org.trinet.jasi.*;
//import org.trinet.jdbc.table.ChannelIdIF;
import org.trinet.util.*;
import java.io.*;

/**
 * Abstract class from which all other Magnitude calculation methods
 * are derived.<p>
 */

public abstract class AbstractMagnitudeMethod extends Object implements MagnitudeMethodIF {

    public static final float DEFAULT_MAG_STD_DEVIATION_MULTIPLIER = 2.0f;
    protected float magStdDevMult = DEFAULT_MAG_STD_DEVIATION_MULTIPLIER;

    protected double chauvenetTrimValue = Chauvenet.STANDARD_CUTOFF_VALUE; // added as property - aww

    protected int defaultMinValidReadings = 1;
    protected int minValidReadings = defaultMinValidReadings;

    public static final String DEFAULT_BASE_PACKAGE_NAME = "org.trinet.jasi.magmethods";

    protected boolean debug = false;
    protected boolean verbose = false;
    protected boolean usesHoriz = true;

    protected boolean disableMagnitudeDistanceCutoff = false;

    /** Flag indicates whether or not a this instance is in a valid state.
    *  Initialized false until a concrete subclass sets it otherwise.
    * */
    protected boolean stateIsValid = false; // do we need this -aww?
    
    // Set false for including both HH and HN types, like for tests of amp ratios
    protected boolean cullRedundantReadings = true; // default

    protected GenericPropertyList props;

    protected ChannelDataTypeMapListIF calibrList = null; // can we convert to ChannelList?
    protected AbstractChannelCalibration defaultCalibrationFactory = null;

    protected AbstractChannelNameMatcher summaryChannelMatcher;
    protected AbstractChannelNameMatcher acceptChannelMatcher;
    protected AbstractChannelNameMatcher filterChannelMatcher;

    // Robust way would be to do db query for each event, channel, datetime combo to see if channel is "valid"
    // select 1 from JASI_CONFIG_VIEW where progId/Name = ? and SNCL = ? and ondate < datetime and offdate > datetime;
    //
    // Recovered from database query:
    protected Set acceptChanIdMap = null;
    protected Set summaryChanIdMap1 = null;
    protected Set summaryChanIdMap2 = null;

    protected String [] parmsRegionNames = new String [] { "DEFAULT" };
    protected String eventRegionName = "DEFAULT";

    /** String for storing messages relevant to method actions. */
    protected String resultsString = "";

    protected Solution currentSol = null; // current/last Solution used

    public int calcCount;  // calculated reading counter for debug use
    public int validCount; // valid reading counter for debug use

    /** Magnitude type subscript. For "Ml" this would equal "l". */
    protected String magSubScript = "";

    /** String describing the magnitude method like "SoCalMl" */
    protected String methodName = "";
    protected String appChannelsName = "";

    /** The generic waveform filter for converting waveforms to the correct
    * type for this magnitude method. */
    protected WaveformFilterIF wfFilter = null;

    /** This is a global correction that is added to all magnitudes calculated
    * by this method. It is usually 0.0 */
    protected double globalCorrection = 0.0;

// attributes pertaining to input data filtering (accept or reject):

    protected Object [] readingTypes = null;
    protected Class readingClass = null;

    protected String defaultAcceptChanPropString  = "";
    protected String defaultFilterChanPropString  = "";
    protected String defaultSummaryChanPropString = "";

    protected boolean requireBothHoriz = false;

    /** If value is non-zero, weights of data with channel magnitude residuals greater then this value
     * are zeroed and the summary magnitude recalculated. */
    protected double trimResidual = Double.MAX_VALUE;

    /** Do not calculate magnitudes for stations farther than this. This value
    * imposes a maximum limit on the distance calculated by getDistanceCutoff()*/
    protected double maxDistance = Double.MAX_VALUE;

    /** Always calculate magnitudes for stations closer than this. This value
    * imposes a minimum limit on the distance calculated by getDistanceCutoff()*/
    protected double minDistance = Double.MIN_VALUE;

    /** Do not calculate magnitudes for more than this many channels (not stations).*/
    protected int maxChannels = Integer.MAX_VALUE;

    /** Set true to require a channel-magnitude correction for the channel to be used.
     * The default is 'false'. */
    protected boolean requireCorrection = false;
    protected boolean useMagCorr = true;

    /** Set true to require waveform filter to copy any waveform input to methods.
     * The default is 'true'. */
    protected boolean filterCopiesWf = true;

    /** If 'true' only that part of a waveform that is likely to contain seismic
    * energy will be scanned. If 'false' the whole available waveform is scanned.<p>
    * If 'true' window scanned begins 1 sec before expected P-wave onset and
    * ends at P-wave onset plus 2x the S-P time.
    */
    protected boolean scanWaveformTimeWindow = true;
    protected boolean scanAllWaveformsForMag = true;

    //
    //protected boolean acceptUndefinedChannelTypes   = true; // is this needed ??
    //
    protected boolean useAssignedWts = true;
    protected boolean sumMagStatTrim = true;
    protected int summaryMagValueStatType = MagnitudeMethodIF.MEDIAN_MAG_VALUE;

    // don't change default boolean property values below unless required by subclass implementation 
    protected boolean deleteInvalid = true; // input property 03/18/2008 - aww
    protected boolean deleteRejected = false; // input property 11/13/2007 - aww
    protected boolean deleteByResidualTrim = false; // input property 11/13/2007 - aww
    protected boolean deleteByDistanceTrim = true; // input property 11/13/2007 - aww 
    protected boolean disableAppChannelsMap = false; // input property - aww 2008/02/27
    protected boolean oneTimeAppChannelsMap = false; // input property - aww 2010/04/20
    private int oneTimeAppChannelsFYI = 0; // flag to print out FYI message, reset by default properties initialization

    protected boolean doLookUp = true; // forces datasource queries, if required data missing

    protected String oldEventRegionName = null;
//----------------------------------------------------------------------------
    /** Constructor input argument is the type for the observations used to calculate Magnitude subtype.*/
    protected AbstractMagnitudeMethod(Class readingClass) {
       this.readingClass = readingClass;
       //setDefaultProperties(); // NOTE: invokes subclass override if any, removed 11/08/2007 -aww to force a call to initialize
    }

    /** Class named by input String must be a subtype which has no-argument constructor (usually public).
     * If a new instance is successfully created, its configure() method
     * is also invoked before it is returned, which may or may not change the state
     * of default settings. Usually the end user must set the properties of the returned
     * object in order to customize the settings for the specific application.
     * */
    public static MagnitudeMethodIF create(String className) {
      MagnitudeMethodIF magMethod = null;
      try {
        magMethod = (MagnitudeMethodIF) Class.forName(className).newInstance();
      }
      catch (ClassNotFoundException ex) {
        ex.printStackTrace();
      }
      catch (InstantiationException ex) {
        ex.printStackTrace();
      }
      catch (IllegalAccessException ex) {
        ex.printStackTrace();
      }
      magMethod.configure();
      return magMethod;
    }

    public void setCurrentSolution(Solution sol) {
        currentSol = sol;
        if (! disableAppChannelsMap) loadAcceptableChannelIdMap(currentSol);

        if (currentSol == null) return;
        // AWW get solution region here, check set region name and then re-initialize the parameters ??
        boolean insideRegion = false;
        for (int idx = 0; idx < parmsRegionNames.length; idx++) {
            insideRegion = currentSol.isInsideRegion(parmsRegionNames[idx]);
            //if (debug) System.out.println("DEBUG checking parms region "+ parmsRegionNames[idx] + " is event in region? " + insideRegion);
            if (insideRegion) { // change default velocity model to new region
                eventRegionName = parmsRegionNames[idx];
                break;
            }
            eventRegionName = "DEFAULT";
        }
        if (debug) System.out.println("DEBUG " + methodName + " current eventRegionName for parms is: " + eventRegionName);

        if ( oldEventRegionName == null || !oldEventRegionName.equals(eventRegionName) ) {
            if (debug) System.out.println("DEBUG     initializeMethod() called for eventRegionName = " + eventRegionName);
            //initializeMethod();
            initRegionDependentProps();
            oldEventRegionName = eventRegionName;
        }
 
    }

    protected void initRegionDependentProps() {
        // empty no-op
    }

    public void setDebug(boolean tf) {
        debug = tf;
        ChannelIdLoader.debug = tf;
        if (debug) verbose = true;
    }
    public void setVerbose(boolean tf) {
        verbose = tf;
    }

    /** Indicates whether or not a this instance is in a valid state.
    *  Initialized false until a concrete subclass sets it otherwise.
    * */
    public boolean hasValidState() {
        return stateIsValid;
    }

    public boolean requireBothHoriz() {
        return requireBothHoriz;
    }

    public void reset() {
        resultsString = "";
        currentSol = null; // current/last Solution used
        calcCount = 0;  // calculated reading counter for debug use
        validCount = 0; // valid reading counter for debug use
    }

    /** Configure the method (usually to defaults particular to the concrete subtype).*/ 
    //public abstract void configure();
    public void configure() {
        initializeMethod();
    }
    /**
     * Catch-all configuration method to specify optional configuration attributes.
     * @parameter configSrc      a source of configuration (DB, file, string)
     * @parameter configLoc      a filename, DB URL, etc.
     * @parameter configSection  a DB table, portion of a file, etc.
     */
    public abstract void configure(int configSrc, String configLoc, String configSection);

    /** Return input weight of reading used for a weighted median summary magnitude calculation. */
    public double getInputWeight(MagnitudeAssocJasiReadingIF jr) {
        return jr.getInWgt();
    }
    /** Set input weight of reading to use for a weighted median summary magnitude calculation. */
    public void setInputWeight(MagnitudeAssocJasiReadingIF jr, double wt) {
        jr.setInWgt(wt);
    }

    /** Default is true. True forces a data look up from the data source
     * when matching data is not found in an associated channel data list.
     * @see #hasRequiredChannelData(MagnitudeAssocJasiReadingIF)
     * @see #getCalibrationFor(Channelable, java.util.Date)
     */
    public void setLookUp(boolean tf) {
        doLookUp = tf;
    }

    /** Default is false. Set true forces all rejected readings to be virtually deleted,
     * making the reading and its associations with its Solution and Magnitude no
     * longer commitable back to the data source.
     */
    public void setDeleteRejected(boolean tf) {
        deleteRejected = tf;
    }

    /** Zeroes the reading's contributing weights to summary magnitude calculation.
     * Reading and its Solution and Magnitude associations can still be committed
     * to data source with zero weights.
     *  @see #setDeleteRejected(boolean)
     *  @see #deleteReading(MagnitudeAssocJasiReadingIF)
     * */
    public void rejectReading(MagnitudeAssocJasiReadingIF jr) {
        //jr.setDeleteFlag(true), jr.delete() don't notify reading list listeners 03/03
        if (deleteRejected) deleteReading(jr); // associations will not be commitable
        else {
            jr.setReject(true); // bug? line added to set flag in input reading - aww 2009/11/17
            // reset reading's summary mag attributes zeroes weights and mag residual
            jr.initChannelMagSummaryData(); //jr.setWeightUsed(0.0); // aww 03/04
        }
    }

    /** Deletes reading (virtually, use flag) from all calculations.
     *  The reading and it's Solution and Magnitude associations
     *  will no longer be commitable (saved) to the data source.
     *  @see #rejectReading(MagnitudeAssocJasiReadingIF)
     * */
    public final void deleteReading(MagnitudeAssocJasiReadingIF jr) {
        // PROBLEM when jr aliased in other mag lists this resets values
        // for the other magnitude, would need unique list element instances otherwise
        //
        // Reset reading's summary mag attributes zeroes weights and mag residual
        jr.initChannelMagSummaryData(); // jr.setWeightUsed(0.0); // aww 03/04
        if (jr.isAssignedToMag()) {
          jr.getAssociatedMag().delete(jr);
        }
        else if (jr.isAssignedToSol()) {
          jr.getAssociatedSolution().delete(jr);
        }
        else jr.setDeleteFlag(true); // not associated so a simple delete ok - aww
    }

    // ChannelMag's were coupled to the "readings" rather than the Magnitude subtypes 
    // (like by using a reading channelmap in the Magnitude subtype).
    // Thus shared reading instances in Magnitude subtype lists have
    // their associated ChannelMag values "reset" (e.g. Mh vs. Ml) by magMethod
    // calculations.  The coupling, requires a "commit" and possible data "refresh"
    // before the calculation and commit of the another magnitude subtype.
    //
    public final void removeReading(MagnitudeAssocJasiReadingIF jr) {
        // Kludge here: don't reset reading data values
        // they may be aliased in list of another magnitude?
        if (jr.isAssignedToMag() ) jr.getAssociatedMag().remove(jr);
    }


    /** Get results message. */
    public String getResultsString() {
        return resultsString;
    }

    /** Class reading type associated with a Magnitude processed by this method. */
    public Class getReadingClass() {
      return readingClass;
    }

    /**
    * Return the long name of the magnitude method
    */
    public String getMethodName() {
      return methodName;
    }

    /** Return the magnitude type subscript string.
     * For "Ml" this would return "l".
     * */
    public String getMagnitudeTypeString() {
      return magSubScript;
    }

    /** Set a global correction that is added to all magnitudes calculated
    * by this method. Default is 0.0 */
    public void setGlobalCorrection(double correction) {
        globalCorrection = correction;
    }

    public int getMinValidReadings() {
      return minValidReadings;
    }
    public void setMinValidReadings(int count) {
      if (count > 0) minValidReadings = count;
    }


    /** Return the global correction that is added to all magnitudes calculated
    * by this method. */
    public double getGlobalCorrection() {
      return globalCorrection;
    }

    /**
     * Return the distance correction appropriate for the method.
     * Some methods require horizontal (epicentral) distance others
     * the slant (hypocentral) distance.
     */
    public abstract double distanceCorrection(double distance) ;

    /**
     * Return the distance cutoff appropriate for the method. Data from
     * stations father then this will not be included in the summary magnitude.
     * Because it depends on the magnitude the cutoff can only be applied in a
     * second pass through the readings. <p>
     *
     * Default return of Math.max(getMinDistance(), getMaxDistance());
     * override in subclass to impose a different distance cutoff criterion.
     * The value is typically compared to epicentral distance kilometers.
     */
    public double getDistanceCutoff(double magValue) {
      return Math.max( getMinDistance(), getMaxDistance() );
    }
    public double getDistanceCutoff(Magnitude mag) {
      return getDistanceCutoff(mag.getMagValue());
    }
    public double getAmpDistanceCutoff(double magValue) {
        return getDistanceCutoff(magValue);
    }

    /** Set the Waveform filter */
    public void setWaveformFilter(WaveformFilterIF filter) {
       wfFilter = filter;
       if (wfFilter != null) wfFilter.copyInputWaveform(filterCopiesWf);
    }

    /** Return 'true' if the Channelable horizontal distance is within the cutoff distance.
    * @See: Channelable */
    public boolean isWithinDistanceCutoff(Channelable obj, double magValue) {
      //return (obj.getDistance() <= getDistanceCutoff(magValue)); // dont't use slant ? aww 06/11/2004
      return (obj.getHorizontalDistance() <= getDistanceCutoff(magValue)); // test horizontal dist - aww 06/11/2004
    }

    /** Subtypes should override this method to implement internally an explicit
     * type declaration or loaded acceptable type via property "includedReadingTypes",
     * a list of Strings describing acceptable reading types.
     * Returns false by default.
    */
    public boolean isIncludedReadingType(MagnitudeAssocJasiReadingIF jr) {
      if (readingTypes == null) return false;
      for (int i = 0; i < readingTypes.length; i++) {
        if (jr.getTypeQualifier().toString().equals(readingTypes[i].toString())) return true;
      }
      return false;
    }

    /** Returns true if the input magnitude type is appropriate for the magnitude method. */
    public boolean isIncludedMagType(Magnitude mag) {
      return mag.getTypeQualifier().equals(magSubScript);
    }

    /** Set true to use the currently assigned channel magnitude weights in the
     * summary magnitude calculation. Setting false forces all summary magnitude
     * calculations to use all reading's setting input weights to a full weight
     * (1.) regardless of input reading's quality.  The reading's quality, set
     * when the reading's channel magnitude is calculated, is normally used for
     * the reading's summary magnitude input weight.
     * @see #assignWeight(MagnitudeAssocJasiReadingIF) 
     * */
    public void setUseAssignedWts(boolean tf) {
        useAssignedWts = tf;
    }

    public boolean useAssignedWts() {
        return useAssignedWts;
    }

    /** Return 'true' only if the component type is included in magnitude calculations.
     */
    public boolean isIncludedComponent(Channel chan) {
      if (! acceptChannelType(chan)) {
          if (debug)
            System.out.println("DEBUG "+methodName+" isIncludedComponent() reject ! acceptChannelType:"+
                    chan.toDelimitedNameString());
          return false;
      }
      return true;
    }
    
    /** Return 'true' only if the ChannelObj component type is included in magnitude calculations.
     @see #isIncludedComponent(Channel)
     */
    public boolean isIncludedComponent(Channelable obj) {
      return isIncludedComponent(obj.getChannelObj());
    }

    /** Returns a collection of the readings found in the reading list associated
     * with input Magnitude that are not flagged deleted and whose types are
     * acceptable to this method for calculating the ChannelMag. The magnitude 
     * data of all rejected readings are re-initialized (nulled,zeroed) by method.
     * */ 
    public List getValidChannelMagData(Magnitude mag) {
        return getValidChannelMagData(mag.getReadingList());
    }

    public List getValidChannelMagData(List inList) {
      List dataList = inList;
      int count = dataList.size();
      ArrayList aList = new ArrayList(count);
      MagnitudeAssocJasiReadingIF jr = null;
      for (int idx = 0; idx < count; idx++) {
        jr = (MagnitudeAssocJasiReadingIF) dataList.get(idx);
        if (! jr.isDeleted() && isValid(jr)) aList.add(jr);
        else // jr.setWeightUsed(0.); // aww 03/04 replaced by below line
          jr.initChannelMag();  // reset all data values, (null mag data, zeroes wts) -aww 03/04
      }
      if (debug) System.out.println("DEBUG "+methodName+" getValidChannelData() "+aList.size()); 
      return aList;
    }

    /** Subclasses should implement criteria for determining whether the input's data
     * type, values, and state are acceptable for use in any magnitude calculations,
     * method can determine if data are valid, in bounds (not garbage). Invalid readings
     * are typically deleted by the calculation methods. The data of a deleted readings
     * CANNOT be committed back to the datasource archive.
     */
    abstract public boolean isValid(MagnitudeAssocJasiReadingIF jr);

    public boolean isValidForSummaryMag(MagnitudeAssocJasiReadingIF jr) {
        return isValid(jr);
    }

    /** Filter the input Magnitude's associated reading list BEFORE invoking 
     * channel magnitude calculation methods, deleting those readings unacceptable
     * for calculations (data for any deleted reading will not be committed).
     * Provides some time savings for those methods that scan waveform timeseries.
    * @see filterDataBeforeChannelMagCalc(List)
    * @see getValidChannelMagData(Magnitude)
    * @return <i>true</i> if a list was changed (at least one element rejected).
    */
    public boolean filterDataBeforeChannelMagCalc(Magnitude mag) throws WrongDataTypeException {
        if (! isIncludedMagType(mag)) { // should we throw WrongMagTypeException?
          throw new WrongMagTypeException(
            "MagnitudeMethod input mag wrong type: " + mag.getTypeQualifier() 
          );
        }
        return filterDataBeforeChannelMagCalc(getValidChannelMagData(mag));
    }

    /** Filter the input Magnitude's associated reading list BEFORE invoking 
     * channel magnitude calculation methods, deleting those readings unacceptable
     * for calculations (data for any deleted reading will not be committed).
     * Provides some time savings for those methods that scan waveform timeseries.
    * @see filterDataBeforeChannelMagCalc(Magnitude)
    * @see getValidChannelMagData(Magnitude)
    * @return <i>true</i> if a list was changed (at least one element rejected).
    */
    public boolean filterDataBeforeChannelMagCalc(List readingList) {
      boolean changed = false;
      MagnitudeAssocJasiReadingIF jr = null;
      int count = readingList.size();
      int rejectCnt = 0;
      for (int idx = 0; idx < count; idx++) {
          jr = (MagnitudeAssocJasiReadingIF) readingList.get(idx);
          boolean reject = false;
          if (! isIncludedComponent(jr)) { // wrong channel type for method
            if (verbose) reportMessage("filterDataBeforeChannelMagCalc DELETE: !isIncludedComponent: ", jr);
            reject = true;
          }
          if ( ! isIncludedReadingType(jr) ) { // wrong data type for method
            if (verbose) reportMessage("filterDataBeforeChannelMagCalc DELETE: !isIncludedReadingType: "+jr.getTypeQualifier(), jr);
            reject = true;
          }
          if ( ! hasRequiredChannelData(jr) ) {
            if (verbose) reportMessage("filterDataBeforeChannelMagCalc DELETE: !hasRequiredChannelData: ", jr);
            reject = true;
          }
          if (reject) {
            //set weights to zero, set delete flag so reading and its data are not commitable
            deleteReading(jr);
            changed = true;
          }
          // Do a maximum distance trim with deletion of data before doing any mag calculations 
          // user must remember to set maximum distance large to include all input readings in calcs
          else if ( trimByDistance(jr, getMaxDistance(), deleteByDistanceTrim) ) {
            // trim deleted reading, data is no longer commitable
            if (verbose) reportMessage("filterDataBeforeChannelMagCalc DELETE: trimmed by distance: " + jr.getHorizontalDistance(), jr); // use horizontal aww 06/11/2004
            changed = true; 
          }
          // line below added to null rejected reading's mag data values, delete above only zeroes wts
          if (changed) {
              jr.initChannelMag(); // aww 03/04
              rejectCnt++;
          }
      }
      if (verbose) reportMessage("filterDataBeforeChannelMagCalc TOTAL channels rejected: " + rejectCnt, null); // aww 11/21/2006
      return changed;
    }

    /** Filter the input Magnitude's associated reading list BEFORE invoking 
     * summary magnitude calculation methods, rejecting readings unacceptable
     * for summary calculation (rejected data has zero weight, but still can be committed).
     * All readings found invalid are deleted (their data cannot be committed to data source).
    * @see isValidForSummaryMag(MagnitudeAssocJasiReadingIF)
    * @see filterDataBeforeSummaryMagCalc(List)
    * @return <i>true</i> if a list was changed (at least one element rejected).
    */
    public boolean filterDataBeforeSummaryMagCalc(Magnitude mag) throws WrongMagTypeException {
        if (! isIncludedMagType(mag)) { // should we throw WrongMagTypeException?
          throw new WrongMagTypeException(
            "MagnitudeMethod input mag wrong type: " + mag.getTypeQualifier()
          );
        }
        return filterDataBeforeSummaryMagCalc(mag.getReadingList());
    }
    /** Filter the input Magnitude's associated reading list BEFORE invoking 
     * summary magnitude calculation methods, rejecting readings unacceptable
     * for summary calculation (rejected data has zero weight, but still can be committed).
     * All readings found invalid are deleted (their data cannot be committed to data source).
     * Checks readings magnitude corrections, if required, and acceptable component type. 
     * @see filterDataBeforeSummaryMagCalc(Magnitude)
     * @return <i>true</i> if a list was changed (at least one element rejected).
    */
    public boolean filterDataBeforeSummaryMagCalc(List readingList) {
    //NOTE: to save reading data back to db (rejectReading); to NOT save reading data back to db (deleteReading)
      boolean changed = false;
      int rejectCnt = 0;
      try {
        MagnitudeAssocJasiReadingIF jr = null;
        // First get rid of singletons, if we want more than 1 orientation from a station for it to be included in summary magnitude
        if (usesHoriz && requireBothHoriz) { 
            TriaxialGrouper grouper = new TriaxialGrouper((ChannelableListIF) readingList);  // break input into sublists by station
            ChannelableListIF stationList = null;
            while (grouper.hasMoreGroups()) { // go through subgroups
                stationList = grouper.getNext(); // output is same class type as original list
                if (stationList.size() == 1) { // reject singleton station before further filtering
                  jr = (MagnitudeAssocJasiReadingIF) stationList.get(0);
                  if (!jr.isReject()) { // if not already rejected, do so now
                      rejectReading(jr); 
                      changed = true;
                      if (verbose) reportMessage("filterDataBeforeSummaryMagCalc requires 2 components for sta: ", jr);
                  }
                }
            }
        }
        //

        double wgt = 0.;
        boolean hasZeroWts = false;
        int size = readingList.size();
        for (int idx = 0; idx < size; idx++) {
            jr = (MagnitudeAssocJasiReadingIF) readingList.get(idx);
            if (jr.isDeleted()) { // skip
               if (verbose) reportMessage("filterDataBeforeSummaryMagCalc is deleted: ",jr);
               if ( !jr.isReject() ) {
                 rejectReading(jr);
                 rejectCnt++;
               }
               continue; // skip
            }

            if (! isValidForSummaryMag(jr)) { // delete data, keep human generated now, override processing state 'F'
               if (deleteInvalid) deleteReading(jr); // -changed back to deleteReading(jr) per Kate request - aww 2008/02/25
               else rejectReading(jr); // to preserve data for testing, in lieu of deleteReading(jr) - aww 03/18/2008
               if (verbose) reportMessage("filterDataBeforeSummaryMagCalc delete !isValidForSummaryMag : " + jr.toNeatString() , null);
               changed = true;
               rejectCnt++;
               continue; // skip
            }

            // Before calculating a summary mag first null,zero any prior
            // assoc magnitude data, as do deleteReading(jr), rejectReading(jr)
            jr.initChannelMagSummaryData();

            if (jr.isReject()) { // skip tests on those in list that are already rejected - aww 2009/11/16
               if (verbose) reportMessage("filterDataBeforeSummaryMagCalc is rejected: ",jr);
               rejectCnt++;
               continue;
            }

            // Note do not set reading's weightUsed until after summary mag calc 
            Double sumWt = getSummaryWt(jr); // 03/17/2006 -aww
            wgt = (sumWt == null) ? 0. : sumWt.doubleValue(); // default to 0. don't include ? -aww
            int mode = (wgt == 0.) ? 1 : 0;

            double q = jr.getQuality();
            mode = (q == 0.) ? mode+2 : mode; // 0, 1, 2, 3

            if (wgt == 0.) { // don't use it in summary magnitude
                rejectReading(jr);
                mode = mode+4; // 4, 5, 6, 7
                changed = true;
                rejectCnt++;
            }
            else jr.setInWgt(1.); // valid for summary, set in_wgt=1. - aww 2009/06/12

            /*
            if (debug) {
              System.out.println(
                "DEBUG filterDataBeforeSummaryMagCalc getInWgt(): "+jr.getInWgt()+" "+getStnChlNameString(jr)
              );
            }
            */

            if (jr.getInWgt() <= 0. || jr.getQuality() == 0.) { // added getQuality test condition -aww 2009/06/12
              // Implies user rejected, zero quality, calibration absent, or calibration wgt = 0
              // mode=0 ok, has nonzero summary wt
              // mode=1 zero summary wt
              // mode=2 0 + zero quality
              // mode=3 1 + zero quality 
              // mode=4 0 + rejected
              // mode=5 1 + rejected
              // mode=6 2 + rejected
              // mode=7 3 + rejected
              hasZeroWts = true;
              if (verbose) reportMessage("filterDataBeforeSummaryMagCalc inWgt=0, mode="+mode+" : ", jr);
              continue; // skip more tests, already a reject
            }

            // Below tests may reject data from summary calc,
            // but still allow a commit of reading and its associations
            // rejected data has zero contributing weight to the summary mag.
            if (! summaryChannelType(jr) ) {
                if (verbose) reportMessage("filterDataBeforeSummaryMagCalc !summaryChannelType: ",jr);
                rejectReading(jr);
                changed = true;
                rejectCnt++;
                continue;
            }
            if (requireCorrection && ! hasMagCorr(jr)) {
                if (verbose) {
                    reportMessage("filterDataBeforeSummaryMagCalc reject !hasMagCorr for date: "+jr.getDateTime(), jr);
                    // Check ChannelMap_xxxParms and StaCorrections tables for date ranged data.
                }
                rejectReading(jr);
                changed = true;
                rejectCnt++;
                continue;
            }

        } // end of FOR loop

        if (usesHoriz && requireBothHoriz) {
            // Now 2nd pass to get rid of singletons resulting from above filtering
            // when we want more than 1 orientation from a station for it to be included in event summary magnitude
            TriaxialGrouper grouper = new TriaxialGrouper((ChannelableListIF) readingList);  // break input into sublists by station
            ChannelableListIF stationList = null;
            int cnt = 0;
            while (grouper.hasMoreGroups()) { // go through subgroups
                stationList = grouper.getNext(); // output is same class type as original list
                cnt = JasiReadingList.getChannelWithInWgtCount(stationList);
                if (cnt == 1) { // reject singleton stations
                    jr = (MagnitudeAssocJasiReadingIF) stationList.get(0); // only rejects those NOT already rejected, return count rejected
                    if (!jr.isReject()) {
                        rejectReading(jr);
                        rejectCnt++;
                        changed = true;
                        //if (verbose) System.out.println("    filterDataBeforeSummaryMagCalc requires 2 components from sta:\n" +stationList);
                          if (verbose) System.out.println("    filterDataBeforeSummaryMagCalc requires 2 components for sta: "+ getStnChlNameString(jr));
                    }
                }
            }
        }

        /*
        // Or instead of TriaxialGroup above, use alternative logic here: -aww 2010/01/11
        if (usesHoriz && requireBothHoriz) {
            Channel chnl = null;
            String seed = null;
            String band = null;
            String orient = null;
            MagnitudeAssocJasiReadingIF jr2 = null;
            for (int idx = 0; idx < size; idx++) {
                jr = (MagnitudeAssocJasiReadingIF) readingList.get(idx);
                if (jr.isDeleted() || jr.isReject()) continue;

                // make a copy the channel and change copy seedchan orientation
                chnl = (Channel) jr.getChannelObj().clone();
                seed = chnl.getSeedchan();
                band = seed.substring(0,2);
                orient = seed.substring(2,3);
                // Kludge: Only check N and E complements, not numeric orientations
                if (orient.equals("E")) seed = band + "N";
                else if (orient.equals("N")) seed = band + "E";
                chnl.setSeedchan(seed); // flip orientation
                // now look for match (same station) in list
                jr2 = readingList.getByChannel(chnl);
                // if none or complement orientation is already rejected, reject current reading (ie. a singleton contributor)
                if (jr2 == null || jr2.isDeleted() || jr2.isReject()) {
                    rejectReading(jr);
                    rejectCnt++
                    changed = true;  // because we toggled reject
                }
            }
        }
        */

        if (hasZeroWts && debug) {
            System.out.print("DEBUG     Weight Mode Key:\n mode=0 ok, has nonzero summary wt");
            System.out.print("\n     mode=1 zero summary wt\n mode=2 0 + zero quality (time tears?)");
            System.out.print("\n     mode=3 1 + zero quality\n mode=4 0 + rejected");
            System.out.println("\n     mode=5 1 + rejected\n mode=6 2 + rejected\n mode=7 3 + rejected");
        }
      }
      catch (WrongDataTypeException ex) {
        ex.printStackTrace();
      }
      if (verbose) reportMessage("filterDataBeforeSummaryMagCalc TOTAL channels rejected: " + rejectCnt, null); // aww 11/21/2006
      return changed;
    }
    /** Examine the input Magnitude reading list AFTER summary magnitude calculation,
    * zero-weighting readings to be saved and deleting those not to be saved. 
    * Filters data as does the pre-filter but in addition rejects those readings whose data exceed
    * a summary magnitude value based maximum channel distance or a maximum channel magnitude residual. 
    * @return <i>true</i> a reading rejected, summary magnitude stale so its value need to be recalculated.
    */
    public boolean filterDataAfterSummaryMagCalc(Magnitude mag) throws WrongMagTypeException {
        if (! isIncludedMagType(mag)) { // should we throw WrongMagTypeException?
          throw new WrongMagTypeException(
            "MagnitudeMethod input mag wrong type: " + mag.getTypeQualifier()
          );
        }

        boolean rejected = false; // true if reading rejected/deleted
        int rejectCnt = 0;

        /* Moved call to MagnitudeEngine.createSummaryMag(Mag) - it should be a one-shot - 2007/12/13 -aww
        if (sumMagStatTrim) { // allow user to disable this filter -aww
          rejected = trimBySummaryStatistics(mag); // rejects have wt = 0
           if (debug) System.out.println(methodName+
                           " filterDataAfterSummaryMagCalc trimBySummaryStatistics trimmed:"+ rejected);
        }
        */

        List aList = getDataUsedBySummaryMagCalc(mag); // undeleted data with wt > 0
        rejectCnt = mag.getReadingList().size() - aList.size(); // rejected count
        MagnitudeAssocJasiReadingIF jr = null;

        //NOTE: to save reading data back to db (rejectReading); to NOT save reading data back to db (deleteReading)
        for (int idx = 0; idx < aList.size(); idx++) {
            jr = (MagnitudeAssocJasiReadingIF) aList.get(idx);

            /* Don't need to redo these conditions here since filter B4 calc does this - 07/05/2007 aww
            if (! isValidForSummaryMag(jr) ) {
                if (verbose) reportMessage("filterDataAfterSummaryMagCalc !isValid delete:", jr);
                deleteReading(jr); // instead of rejectReading(jr) - aww
                rejectCnt++;
                rejected = true;
            }
            else if (! summaryChannelType(jr)) {
                if (verbose) reportMessage("filterDataAfterSummaryMagCalc !summaryChannelType reject: ", jr);
                // don't delete here, may need to commit or use it later, just set the wgt to 0.
                rejectReading(jr) ;
                rejectCnt++;
                rejected = true;
            }
            */

            // Depending on input value of mag, setting deleteByDistanceTrim = true
            // virtually deletes the rejected reading, preventing a commit of its data,
            // whereas setting deleteByDistanceTrim = false zeroes the contributing weight
            // of the data to the magnitude calc and still allows a commit of the rejected
            // reading and its association data to the data source tables.
            if ( trimByDistance(jr, getDistanceCutoff(mag), deleteByDistanceTrim) ) {
                if (verbose) reportMessage("filterDataAfterSummaryMagCalc trimByDistance: ", jr);
                rejectCnt++;
                rejected = true;
            }
            else if (trimByResidual(jr, trimResidual, deleteByResidualTrim) ) {
                if (verbose) reportMessage("filterDataAfterSummaryMagCalc trimByResidual: ", jr);
                rejectCnt++;
                rejected = true;
            }

            /* done by B4 filter -aww
            else if ( getRequireCorrection() && ! hasMagCorr(jr) ) {
                if (verbose) reportMessage("filterDataAfterSummaryMagCalc reject !hasMagCorr for: "+jr.getDateTime(), jr);
                rejectReading(jr);
                rejectCnt++;
                rejected = true;
            }
            */

        } // end of for loop through channels

        //
        if (usesHoriz && requireBothHoriz) {
            // Now 2nd pass to get rid of singletons
            // when we want more than 1 orientation from a station for it to be included in event summary magnitude
            ChannelableList readingList = new ChannelableList(aList);
            TriaxialGrouper grouper = new TriaxialGrouper((ChannelableListIF) readingList);  // break input into sublists by station
            ChannelableListIF stationList = null;
            int cnt = 0;
            while (grouper.hasMoreGroups()) { // go through subgroups
                stationList = grouper.getNext(); // output is same class type as original list
                cnt = JasiReadingList.getChannelWithInWgtCount(stationList);
                if (cnt == 1) { // reject singleton stations
                    jr = (MagnitudeAssocJasiReadingIF) stationList.get(0); // only rejects those NOT already rejected, return count rejected
                    if (!jr.isReject()) {
                        rejectReading(jr);
                        rejectCnt++;
                        rejected = true;
                        //if (verbose) System.out.println("    filterDataAfterSummaryMagCalc requires 2 components from sta:\n" +stationList);
                          if (verbose) System.out.println("    filterDataAfterSummaryMagCalc requires 2 components for sta: "+ getStnChlNameString(jr));
                    }
                }
            }
        }
        //

        // Reject most distant channels (end of list) if total used exceeds the configured maxChannels property
        int usedCount = 0;
        int maxIdx = 0;
        ChannelableList readingList = new ChannelableList(aList);
        readingList.distanceSort();  // force another sort since we don't know if list is already sorted by distance - aww 
        for (int idx = 0; idx < readingList.size(); idx++) {
            jr = (MagnitudeAssocJasiReadingIF) readingList.get(idx);
            if ( ! jr.isReject() ) usedCount++;
            if (usedCount > getMaxChannels()) break;
            maxIdx++;
        }

        if (usedCount > getMaxChannels()) {
            if (verbose) System.out.println( "    filterDataAfterSummaryMagCalc maxChannels: " + getMaxChannels() +
                    " for mag exceeded, rejecting " + (readingList.size() - maxIdx) + " more distant channels");
            for (int idx = maxIdx; idx < readingList.size(); idx++) {
                jr = (MagnitudeAssocJasiReadingIF) readingList.get(idx);
                if (!jr.isReject()) {
                    rejectReading(jr);
                    rejectCnt++;
                    rejected = true;
                }
                if (debug) reportMessage( "DEBUG filterDataAfterSummaryMagCalc maxChannels for summary exceeded, rejected: ", jr);
            }
        }
        //

        if (rejected) mag.setStale(true);

        if (verbose) reportMessage("filterDataAfterSummaryMagCalc TOTAL channels rejected: " + rejectCnt, null); // aww 11/21/2006

        return rejected;
    }

    /** Subclass implementations can override method with preferred summary statistic to return.
     * Default is median.
    * @see MagnitudeMethodIF.WT_MEDIAN_MAG_VALUE
    * @see MagnitudeMethodIF.MEDIAN_MAG_VALUE
    */
    public int getSummaryMagValueStatType() {
        return summaryMagValueStatType;

    }
    //abstract public setSummaryMagValueStatType(int statFlag);

    abstract public boolean trimBySummaryStatistics(Magnitude mag);

    /** Return collection of readings that would contribute to a
     * recalculation of the input Magnitude's value.
     * All undeleted readings having magnitude associated input weights > 0.
     *  @see JasiReading.getInWgt()
     * */
    public final List getDataForSummaryMagCalc(Magnitude mag) {
        return getDataForSummaryMagCalc(mag.getReadingList());
    }

    /** Return collection of readings that would contribute to a
     * calculation of a summary magnitude value.
     * All undeleted readings having magnitude associated input weights > 0.
     *  @see JasiReading.getInWgt()
     * */
    public List getDataForSummaryMagCalc(List dataList) {

      int count = dataList.size();
      List aList = new ArrayList(count);
      if (count == 0) System.out.println("DEBUG     getDataForSummaryMagCalc: No data, empty input list"); 

      org.trinet.jasi.MagnitudeAssocJasiReadingIF jr = null;
      for (int idx = 0; idx < count; idx++) {
        jr = (MagnitudeAssocJasiReadingIF) dataList.get(idx);
        if ( ! jr.isDeleted() && jr.getInWgt() > 0.) { // replace by below as test 2008/03/18 -aww
            if ( isValidForSummaryMag(jr) ) aList.add(jr);
            else rejectReading(jr);
        }

        // try Kate's alternative don't use low-gain unless high gain clipped -aww  2008/03/18 -aww
        //if (! isValidForSummaryMag(jr) ) rejectReading(jr); // 2008/03/18 -aww
        //aList.add(jr); // 2008/03/18 -aww

        if (debug) System.out.println("DEBUG     getDataForSummaryMagCalc "+getStnChlNameString(jr)+
                " isDeleted: " + jr.isDeleted() + " isReject: " + jr.isReject() + " inWgt: " + jr.getInWgt() + " quality: " + jr.getQuality() +
                " channelMag: " + jr.getChannelMag().toString());

      }

      // AWW - 01/22/2008 when appchannels lists are populated cull
      if (cullRedundantReadings && (summaryChanIdMap1 != null) && ! summaryChanIdMap1.isEmpty() ) { // assume summaryChanIdMap2 empty when Map1 empty 
          aList = collapseChannelDataForSummaryMagCalc(aList); // reject secondary (low gain) for sta when primary is present
      }
      //
        if (usesHoriz && requireBothHoriz) {
            // Get rid of singletons resulting from above filtering
            // when we want more than 1 orientation from a station for it to be included in event summary magnitude
            TriaxialGrouper grouper = new TriaxialGrouper(new ChannelableList(aList));  // break input into sublists by station
            ChannelableListIF stationList = null;
            int cnt = 0;
            while (grouper.hasMoreGroups()) { // go through subgroups
                stationList = grouper.getNext(); // output is same class type as original list
                cnt = JasiReadingList.getChannelWithInWgtCount(stationList);
                if (cnt == 1) { // reject group from summary having fewer than 2 acceptable components
                    jr = (MagnitudeAssocJasiReadingIF) stationList.get(0); // only rejects those NOT already rejected, return count rejected
                    if (!jr.isReject())  rejectReading(jr);
                    aList.remove(jr);
                    //if (verbose) System.out.println("    getDataForSummaryMagCalc requires 2 components from sta:\n" +stationList);
                      if (verbose) System.out.println("    getDataForSummaryMagCalc requires 2 components for sta: "+ getStnChlNameString(jr));
                }
            }
        }
     //

      if ( aList.size() == 0) {
         System.out.println("FYI : getDataForSummaryMagCalc: No data acceptable for summary mag"); 
      }

      return aList;

    }

    // Cull out the secondary list data when primary list data are ok.
    private List collapseChannelDataForSummaryMagCalc(List dataList) {
          if (debug)
            System.out.println("DEBUG     collapseChannelDataForSummaryMagCalc: " + new DateTime().toString());

          int count = dataList.size();
          if (count == 0) return dataList;

          if (debug) System.out.println("DEBUG     summaryChanIdMap1 count: " + ((summaryChanIdMap1 == null) ? 0 :summaryChanIdMap1.size()));
          if (debug) System.out.println("DEBUG     summaryChanIdMap2 count: " + ((summaryChanIdMap2 == null) ? 0 :summaryChanIdMap2.size()));

          ChannelableList aList1 = new ChannelableList(count);
          ChannelableList aList2 = new ChannelableList(((summaryChanIdMap2 == null) ? 0 : summaryChanIdMap2.size()));
          org.trinet.jasi.MagnitudeAssocJasiReadingIF jr = null;
          for (int ii = 0; ii < count; ii++) {
              jr = (MagnitudeAssocJasiReadingIF) dataList.get(ii);
              if (summaryChanIdMap1.contains(jr.getChannelObj().getChannelId())) aList1.add(jr);
              else if (summaryChanIdMap2 != null && summaryChanIdMap2.contains(jr.getChannelObj().getChannelId())) aList2.add(jr);
          }

          // Now loop through primary data, culling secondary list if not empty
          count = aList1.size();
          org.trinet.jasi.MagnitudeAssocJasiReadingIF jr2 = null;
          for (int ii = 0; ii < count; ii++) {
              // if valid primary reading, delete channel with same net,sta, & orientation from secondary list
              jr = (MagnitudeAssocJasiReadingIF) aList1.get(ii);
              if (isValidForSummaryMag(jr)) { // valid for summary, so cull out the low gain channels replace by below for test
              //if (! isClipped(jr)) { // try Kate's idea, reject all except clipped -aww
                for (int jj = 0; jj < aList2.size(); jj++) {
                  jr2 = (MagnitudeAssocJasiReadingIF) aList2.get(jj);
                  if ( jr.getChannelObj().sameStationAs(jr2.getChannelObj().getChannelId()) &&
                       jr.getChannelObj().sameOrientationAs(jr2.getChannelObj().getChannelId()) ) {
                         aList2.remove(jj); // delete it from secondary processing list
                         if (debug)
                             System.out.println("DEBUG     collapseChannelDataForSummaryMagCalc removed: " +
                                 jr2.getChannelObj().getChannelId().toString());
                      //break; // removed break, keep going in case of multiple seedchan at station with same orientation
                  }
                }
              }
              if (aList2.size() == 0) break;
          }

          // Now if secondary list is not empty, add all in this (low-gains) to primary list readings (high gains)
          if (aList2.size() > 0) aList1.addAll(aList2);

          return aList1;

    }

    public boolean isClipped(MagnitudeAssocJasiReadingIF jr) {
        return ! ((Amplitude) jr).isOnScale();
    }

    /** Return a collection of readings that contributed to the input Magnitude's value.
     *  All undeleted readings having associated magnitude output weights > 0.
     *  @see JasiReading.getWeightUsed()
     *  */
    public final List getDataUsedBySummaryMagCalc(Magnitude mag) {
        return getDataUsedBySummaryMagCalc(mag.getReadingList());
    }
    /** Return collection of readings that contributed to a summary magnitude value. */
    public final List getDataUsedBySummaryMagCalc(List dataList) {
      int count = dataList.size();
      ArrayList aList = new ArrayList(count);
      org.trinet.jasi.MagnitudeAssocJasiReadingIF jr = null;
      for (int idx = 0; idx < count; idx++) {
        jr = (MagnitudeAssocJasiReadingIF) dataList.get(idx);
        if (debug) System.out.println("DEBUG     getDataUsedBySummaryMagCalc "+getStnChlNameString(jr)+" isDeleted: " + jr.isDeleted() + " wtUsed: " + jr.getWeightUsed()+" inWgt: " + jr.getInWgt() + " quality: " + jr.getQuality());
        if ( ! jr.isDeleted() && jr.getWeightUsed() > 0. ) aList.add(jr);
      }
      return aList;
    }

    /** Return the Waveform filter used to condition timeseries prior to scanning for magnitude dependent data. */
    public WaveformFilterIF getWaveformFilter() {
      return wfFilter;
    }

    /** Return 'true' if this magnitude method has a Waveform filter */
    public boolean hasWaveformFilter() {
      return (wfFilter != null);
    }

    /** Return a filtered COPY of one Waveform.
    * Returns original Waveform if no filter is set.
    * Returns an empty Waveform if filtering fails.
    */
    //NOTE: Ml method filter use to fail if the waveform did not have an associated channel gain. Does it still? 
    public Waveform filter(Waveform wfIn) {
      if (wfFilter == null) return wfIn;
      return (Waveform) getWaveformFilter().filter(wfIn);
    }

    /** Don't calculate magnitudes for channels located greater then this far away (in km).*/
    public void setMaxDistance(double distKm ) {
      maxDistance = distKm;
    }
    /** Return the maximum distance value for reading inclusion.
     * Default return is Double.MAX_VALUE if it was not explicitly set, so it is safe to use.*/
    public double getMaxDistance() {
      return maxDistance;
    }

    /** Always calculate magnitudes for channels located less then this far away (in km).*/
    public void setMinDistance(double distKm ) {
      minDistance = distKm;
    }
    /** Return the minimum distance value below which a channel is included in the calculations.
     * Default return is Double.MIN_VALUE if it was not explicitly set, so it is safe to use.*/
    public double getMinDistance() {
      return minDistance;
    }

    /** Return the maximum number of channels to include in summary magnitude calculation.
     * Default return is Int.MAX_VALUE if it was not explicitly set, so it is safe to use.*/
    public int getMaxChannels() {
      return maxChannels;
    }
    /** Sets the maximum number of channels to included in summary magnitude calculation. */
    public void setMaxChannels(int maxChannels ) {
      this.maxChannels = maxChannels;
    }
    /** Set true to scan a limited window for seismic energy. */
    public void setScanWaveformTimeWindow(boolean tf) {
      scanWaveformTimeWindow = tf;
    }

    /**Returns true if a limited window will be scanned for seismic energy. */
    public boolean getScanWaveformTimeWindow() {
      return scanWaveformTimeWindow ;
    }

    /** Set true to scan a limited window for seismic energy. */
    public void setScanAllWaveformsForMag(boolean tf) {
      scanAllWaveformsForMag = tf;
    }

    /**Returns true if a limited window will be scanned for seismic energy. */
    public boolean getScanAllWaveformsForMag() {
      return scanAllWaveformsForMag;
    }


    /** If value is non-zero trim readings with residuals greater then this value and
    * recalc the median.*/
    public void setTrimResidual( double value ) {
      trimResidual = Math.abs(value);
    }
    /** Return the value of the trimResidual. Returns Double.MAX_VALUE if not set. */
    public double getTrimResidual() {
      return trimResidual;
    }

    /** Set the value of requireCorrections. */
    public void setRequireCorrection( boolean tf ) {
      requireCorrection = tf;
    }
    /** Return the value of requireCorrections. */
    public boolean  getRequireCorrection() {
      return requireCorrection;
    }

    /** Deletes input reading if its epicentral distance is farther than the input cutoff value. 
    * Input is flagged as deleted and its magnitude weights are set to 0.0. 
    * The reading and its associations will not be commitable to data source.
    * @see #deleteReading(MagnitudeAssocJasiReadingIF)
    * @see #trimByResidual(MagnitudeAssocJasiReadingIF,double)
    * @return <i>true</i> if input reading was rejected.
    * */
    public boolean trimByDistance(MagnitudeAssocJasiReadingIF jr, double cutoff) {
        return trimByDistance(jr, cutoff, true);
    }

    public boolean trimByDistance(List jrList, double cutoff, boolean deleteReading) {
      boolean changed = false;
      MagnitudeAssocJasiReadingIF jr = null;
      int count = jrList.size();
      int rejectCnt = 0;
      for (int idx = 0; idx < count; idx++) {
          jr = (MagnitudeAssocJasiReadingIF) jrList.get(idx);
          if (trimByDistance(jr, cutoff, deleteReading)) rejectCnt++;
      }
      if (verbose) reportMessage("trimByDistance TOTAL channels rejected: " + rejectCnt, null);
      return changed;
    }

    /** If the (<i>jr</i>) reading's epicentral distance in km is farther than <i>cutoff</i>,
     * deletes the reading if parameter <i>deleteReading </i> is <i>true </i>,
     * otherwise rejects the reading by setting its weights to zero. 
    * @see #deleteReading(MagnitudeAssocJasiReadingIF)
    * @see #rejectReading(MagnitudeAssocJasiReadingIF)
    * @return <i>true</i> if input reading was deleted or rejected.
     */
    public boolean trimByDistance(MagnitudeAssocJasiReadingIF jr, double cutoff, boolean deleteReading) {
      if (cutoff == Double.MAX_VALUE) return false;

      if (jr.isFinal()) return false; // accept all required flags, let user keep more distant ones he deems ok. -aww

      boolean changed = false;
      double jrDist = 
              jr.getChannelObj().getHorizontalDistance(); // horizontal distance - aww 06/11/2004
              //jr.getChannelObj().getSlantDistance();    // slant distance - aww 06/15/2004
              //jr.getChannelObj().getDistance();         // true distance - aww 06/11/2004
      if (jrDist > cutoff)  {
        changed = true;
        // reject or delete a property option - aww
        if (deleteReading) deleteReading(jr);
        else rejectReading(jr);
        if (debug) {
          System.out.println( ((deleteReading) ? "DELETE" : "REJECT") +
              ": distance > " + (int) cutoff + " km  "+
              jr.getChannelObj().toDelimitedNameString()+
              " dist = "+jrDist // slant - aww 06/15/2004
          );
        }
      }
      return changed;
    }

    /** Reject the input reading (set its magnitude weights to 0), if its magnitude
     * residual is greater than the input value. 
    * @see #rejectReading(MagnitudeAssocJasiReadingIF)
    * @see #trimByDistance(MagnitudeAssocJasiReadingIF,double)
    * @return <i>true</i> if input reading was rejected.
    * */
    public boolean trimByResidual(MagnitudeAssocJasiReadingIF jr, double residual) {
        return trimByResidual(jr, residual, false);
    }

    public boolean trimByResidual(MagnitudeAssocJasiReadingIF jr, double residual, boolean deleteReading) {
      if (residual == Double.MAX_VALUE) return false;
      boolean changed = false;

      if (jr.isFinal()) return false; // accept all required flags, let user keep residual values he deems ok. -to be like distance - aww 2009/09/22

      if (Math.abs(jr.getMagResidual()) > residual) {
        changed = true;
        // reject or delete a property option - aww
        if (deleteReading) deleteReading(jr);
        else rejectReading(jr);
        if (debug) {
          System.out.println( ((deleteReading) ? "DELETE" : "REJECT") +
             ": residual > " + residual + " " +
             jr.getChannelObj().toDelimitedNameString() +
             " error = " + jr.getMagResidual()
          );
        }
      }
      return changed;
    }

    /** Type of readings accepted by this algorithm (e.g. "WAS" for Synthethic Wood-Anderson Amp and
     *  and "Pd" for Coda Md type).
     */
    public void setIncludedReadingTypes(Object [] types) {
        readingTypes = types;
    }

    /** Override in subclass types, if necessary. */
    protected void initOutputProperties(GenericPropertyList props) {
       verbose = props.getBoolean("verbose");
       setDebug(props.getBoolean("debug"));
       if (debug) {
         System.out.println("DEBUG ---- initialize " + methodName+" : "+getClass().getName() + " input properties:");
         System.out.println(props.listToString());
         System.out.println("DEBUG ---- initialize " + methodName+" end of input properties"); 
       }
    }

    /** Sets method default properties, then configures method according to overriding input properties. */
    public void initializeMethod() {

       setDefaultProperties();  // set method default properties, calls subclass overrides if any

       if (props == null) return; // else set method's custom properties

       initOutputProperties(props);

       if (props.getProperty("methodName") != null)
               methodName = props.getProperty("methodName", methodName);

       appChannelsName = props.getProperty("appChannelsName", methodName);

       if (props.getProperty("parmsRegionNames") != null)
              parmsRegionNames = props.getStringArray("parmsRegionNames");

       if (props.getProperty("cullRedundantReadings") != null)
           cullRedundantReadings = props.getBoolean("cullRedundantReadings");

       // Multiples of the S-P time to add to the P-time for end time of the energy scan window
       //if ( props.isSpecified("wfSmPWindowMultiplier") ) {
       //   double d = getDouble("wfSmPWindowMultiplier");
       //   // Force to be at least 1.5
       //   if (d >= 1.5) AbstractWaveform.smpWindowMultiplier = d;
       //}

       if (props.getProperty("scanEnergyWindow") != null)
               setScanWaveformTimeWindow(props.getBoolean("scanEnergyWindow"));
       if (props.getProperty("scanAllWaveformsForMag") != null)
               setScanAllWaveformsForMag(props.getBoolean("scanAllWaveformsForMag"));
       if (props.getProperty("maxDistance") != null)
               setMaxDistance(props.getDouble("maxDistance"));
       if (props.getProperty("minDistance") != null)
               setMinDistance(props.getDouble("minDistance"));
       if (props.getProperty("maxChannels") != null)
               setMaxChannels(props.getInt("maxChannels"));
       if (props.getProperty("trimResidual") != null)
               setTrimResidual(props.getDouble("trimResidual"));
       if (props.getProperty("requireCorrection") != null)
               setRequireCorrection(props.getBoolean("requireCorrection"));
       if (props.getProperty("useMagCorr") != null)
               useMagCorr = props.getBoolean("useMagCorr");
       if (props.getProperty("includedReadingTypes") != null)
               setIncludedReadingTypes(props.getStringArray("includedReadingTypes"));
       if (props.getProperty("minValidReadings") != null)
               setMinValidReadings(props.getInt("minValidReadings"));

       if (props.getProperty("filterCopiesWf") != null) { 
               filterCopiesWf = props.getBoolean("filterCopiesWf");
               if (wfFilter != null) wfFilter.copyInputWaveform(filterCopiesWf);
       }

       if (props.getProperty("globalMagCorr") != null)
               setGlobalCorrection(props.getDouble("globalMagCorr"));

       // use matcher initialized method defaults unless explicit property is declared in file:
       if (props.getProperty("acceptChan") != null) {
         acceptChannelMatcher.createPatternList(props.getStringArray("acceptChan"));
       }
       if (props.getProperty("summaryChan") != null) {
         summaryChannelMatcher.createPatternList(props.getStringArray("summaryChan"));
       }
       if (props.getProperty("filterChan") != null) {
         filterChannelMatcher.createPatternList(props.getStringArray("filterChan"));
       }

       if (props.getProperty("useAssignedWts") != null) 
           useAssignedWts = props.getBoolean("useAssignedWts");

       if (props.getProperty("channelDbLookUp") != null) 
           setLookUp(props.getBoolean("channelDbLookUp"));

       if (props.getProperty("sumMagStatTrim") != null) 
           sumMagStatTrim = props.getBoolean("sumMagStatTrim");

       if (props.getProperty("summaryMagValueStatType") != null) {
           String str = props.getProperty("summaryMagValueStatType", "median"); //default
           if (str.equalsIgnoreCase("average") || str.equalsIgnoreCase("mean") || str.equalsIgnoreCase("avg")) {
               summaryMagValueStatType = MagnitudeMethodIF.AVG_MAG_VALUE;
           }
           else if (str.equalsIgnoreCase("weightedmedian") || str.equalsIgnoreCase("wmedian")) {
               summaryMagValueStatType = MagnitudeMethodIF.WT_MEDIAN_MAG_VALUE;
           }
           else if (str.equalsIgnoreCase("median")) {
               summaryMagValueStatType = MagnitudeMethodIF.MEDIAN_MAG_VALUE;
           }
           else {
               System.err.println(getClass().getName() + " UNKNOWN summaryMagValueStatType property value: " + str +
                       "\n ....  Defaulting to MEDIAN of channel magnitudes");
               summaryMagValueStatType = MagnitudeMethodIF.MEDIAN_MAG_VALUE;
           }
       }

       if (props.getProperty("chauvenetTrimValue") != null)
           chauvenetTrimValue = props.getDouble("chauvenetTrimValue");
           if (chauvenetTrimValue < Chauvenet.STANDARD_CUTOFF_VALUE)
             System.err.println("WARNING! "+methodName+" chauvenetTrimValue < standard "+Chauvenet.STANDARD_CUTOFF_VALUE);

       if (props.getProperty("deleteRejected") != null) 
           deleteRejected = props.getBoolean("deleteRejected");

       if (props.getProperty("deleteInvalid") != null) 
           deleteInvalid = props.getBoolean("deleteInvalid"); // bugfix 2008/11/20 -aww

       if (props.getProperty("deleteByDistanceTrim") != null) 
           deleteByDistanceTrim = props.getBoolean("deleteByDistanceTrim");

       if (props.getProperty("deleteByResidualTrim") != null) 
           deleteByResidualTrim = props.getBoolean("deleteByResidualTrim");

       if (props.getProperty("requireBothHoriz") != null) 
           requireBothHoriz = props.getBoolean("requireBothHoriz"); // reject stations with only one component from summary mag?

       if (props.getProperty("disableMagnitudeDistanceCutoff") != null) { // moved from CISNmlMagMethod2 to here -aww 2010/08/04
          disableMagnitudeDistanceCutoff = props.getBoolean("disableMagnitudeDistanceCutoff");
       }

       disableAppChannelsMap = props.getBoolean("disableAppChannelsMap"); // No db lookup at all, use default masks
       // else, to speed up remote access, set this true, to init channel id set mappings only on the first initialize call:
       oneTimeAppChannelsMap = props.getBoolean("oneTimeAppChannelsMap");
       /* removed id map creation here, instead do it when setCurrentSolution is invoked

       if (disableAppChannelsMap) {
           // throw away existing mapping, revert to templates
           acceptChanIdMap = null;
           summaryChanIdMap1 = null;
           summaryChanIdMap2 = null;
       }
/*
       else {
           loadAcceptableChannelIdMap(currentSol);
           if (acceptChanIdMap != null && oneTimeAppChannelsMap) {
              System.out.println("FYI: "+methodName+" using initial AppChannels maps for " +
                      appChannelsName+ ", maps are NOT reloaded from database for each new solution!");
           }
       }
*/


        // One-shot load : only if magMethod "debug" prop = true can you see debug info from loadAcceptableChannelIdMap 
        // AFTER initialization here JiggleEngineDelegate sets all magMethod's debug values to its "debug" setting
        if (debug) {
          System.out.print("DEBUG methodName: "+methodName+" appChannelsName: "+appChannelsName + " patterns: " +
          acceptChannelMatcher.getPatternCount()+" acceptChan="+props.toPropertyString(acceptChannelMatcher.getInputRegex()) + ", " +
          filterChannelMatcher.getPatternCount()+" filterChan="+props.toPropertyString(filterChannelMatcher.getInputRegex()) + ", " +
          summaryChannelMatcher.getPatternCount()+" summaryChan="+props.toPropertyString(summaryChannelMatcher.getInputRegex()));
        }
    }

    protected boolean loadAcceptableChannelIdMap(Solution sol) {
        if (sol == null) return false;
        //Acceptable channels for magnitude method are from JASI_CONFIG_VIEW where
        //methodName is mapped to name their corresponding progId in the applications table.

        // Test for case of processing bunch around same date, like recent to avoid network overhead of requerying
        if (acceptChanIdMap != null && oneTimeAppChannelsMap && oneTimeAppChannelsFYI == 0) {
            System.out.println("FYI: "+methodName+" using initial AppChannels maps for " +
                      appChannelsName+ ", maps were NOT reloaded from database for event:" + sol.getId().longValue());
            oneTimeAppChannelsFYI = 1; // toggle flag so as to not get repeated messages
            return true;
        }
        
        ChannelIdLoaderIF cidl = ChannelIdLoader.create();
        java.util.Date magDate = (sol != null) ? sol.getDateTime() : null; // appchannels ondate/offdate are what context?
        if (appChannelsName == null || appChannelsName == "") appChannelsName = methodName;
        acceptChanIdMap   = cidl.loadChannelConfigFor(appChannelsName, magDate);
        if (acceptChanIdMap == null)  System.out.println("INFO: For methodName: "+methodName+" appChannelsName: "+appChannelsName+ " acceptable channel config not found!");

        summaryChanIdMap1 = cidl.loadChannelConfigFor(appChannelsName, magDate, "1"); // 16 char limit to name, 64 for configuration
        if (summaryChanIdMap1 == null)  System.out.println("INFO: For methodName: "+methodName+" appChannelsName: "+appChannelsName+ " summary1 channel config not found!");
        summaryChanIdMap2 = cidl.loadChannelConfigFor(appChannelsName, magDate, "2"); // 16 char limit to name
        if (summaryChanIdMap2 == null)  System.out.println("INFO: For methodName: "+methodName+" appChannelsName: "+appChannelsName+ " summary2 channel config not found!");
        if (debug) {
            System.out.println("INFO "+methodName + " config: "+ appChannelsName+" acceptable channels from JASI_CONFIG_VIEW = "+
                    ((acceptChanIdMap == null) ? 0 : acceptChanIdMap.size()) + " for event date: " + magDate);
            System.out.println("INFO "+methodName + " config: "+appChannelsName+" summary1 channels from JASI_CONFIG_VIEW = "+
                    ((summaryChanIdMap1 == null) ? 0 : summaryChanIdMap1.size()));
            System.out.println("INFO "+methodName + " config: "+appChannelsName+" summary2 channels from JASI_CONFIG_VIEW = "+
                    ((summaryChanIdMap2 == null) ? 0 : summaryChanIdMap2.size()));
        }

        return (acceptChanIdMap != null);
    }

    /** Sets an instance property list used to configure the method properties and
     * initializes the method from these properties.
     * */
    public void setProperties(GenericPropertyList props) {
        this.props = props;
        if (props != null) initializeMethod();
    }
    /** Creates an instance property list from the named input file then initializes method. */
    public boolean setProperties(String propFileName) {
        if (loadProperties(propFileName)) {
          initializeMethod();
          if (debug) System.out.println("DEBUG "+methodName+ " " + " setProperties("+ propFileName + ")");
          return true;
        }
        else return false;
    }

    /** Creates an instance property list from the named input file but does not initializes method. */
    protected boolean loadProperties(String propFileName) {
        props =  new SolutionWfEditorPropertyList(); // changed list type -aww 2008/03/26
        return props.readPropertiesFile(propFileName);
    }

    /** Return a property list that has this instance's currently set parameters values. 
    * */
    public GenericPropertyList getProperties() {
      if (props == null) props = new SolutionWfEditorPropertyList(); // changed list type -aww 2008/03/26
      props.setProperty("debug", debug);
      props.setProperty("verbose", verbose);
      props.setProperty("scanEnergyWindow", scanWaveformTimeWindow);
      props.setProperty("scanAllWaveformsForMag", scanAllWaveformsForMag);
      props.setProperty("maxDistance", maxDistance);
      props.setProperty("minDistance", minDistance);
      props.setProperty("disableMagnitudeDistanceCutoff", disableMagnitudeDistanceCutoff); 
      props.setProperty("maxChannels", maxChannels);
      props.setProperty("trimResidual", trimResidual);
      props.setProperty("requireCorrection", requireCorrection);
      props.setProperty("requireBothHoriz", requireBothHoriz);
      props.setProperty("useMagCorr", useMagCorr);
      props.setProperty("includedReadingTypes", props.toPropertyString(readingTypes));
      props.setProperty("cullRedundantReadings", cullRedundantReadings);
      props.setProperty("minValidReadings", minValidReadings);
      props.setProperty("parmsRegionNames", parmsRegionNames);
      props.setProperty("filterCopiesWf", filterCopiesWf); 
      props.setProperty("globalMagCorr", globalCorrection);
      props.setProperty("acceptChan", props.toPropertyString(acceptChannelMatcher.getInputRegex()));
      props.setProperty("filterChan", props.toPropertyString(filterChannelMatcher.getInputRegex()));
      props.setProperty("summaryChan", props.toPropertyString(summaryChannelMatcher.getInputRegex()));
      props.setProperty("useAssignedWts", useAssignedWts); 
      props.setProperty("channelDbLookUp", doLookUp);
      props.setProperty("sumMagStatTrim", sumMagStatTrim); 
      props.setProperty("summaryMagValueStatType", getSummaryStatTypeName());
      props.setProperty("chauvenetTrimValue", chauvenetTrimValue);
      props.setProperty("deleteInvalid", deleteInvalid);
      props.setProperty("deleteRejected", deleteRejected);
      props.setProperty("deleteByDistanceTrim", deleteByDistanceTrim);
      props.setProperty("deleteByResidualTrim", deleteByResidualTrim);
      props.setProperty("disableAppChannelsMap", disableAppChannelsMap);
      props.setProperty("oneTimeAppChannelsMap", oneTimeAppChannelsMap);
      props.setProperty("methodName", methodName);
      props.setProperty("appChannelsName", ((appChannelsName == null || appChannelsName == "") ? methodName : appChannelsName));

      return props;
    }

    private String getSummaryStatTypeName() {
        if (summaryMagValueStatType == MagnitudeMethodIF.AVG_MAG_VALUE) {
            return "average";
        }
        else if (summaryMagValueStatType == MagnitudeMethodIF.WT_MEDIAN_MAG_VALUE) {
            return "weightedmedian";
        }
        //else if (summaryMagValueStatType == MagnitudeMethodIF.MEDIAN_MAG_VALUE) {
            return "median";
        //}
    }

/**
 * Set default values for essential properties so the program will work in the absence of
 * a default 'properties' file.
 */
    public void setDefaultProperties() {
      verbose = false;
      debug = false;
      usesHoriz = true;
      useMagCorr = true;
      // by default, don't include both in summary the high HH and low gain HN channels from same site
      cullRedundantReadings = true;
      requireBothHoriz = false;
      requireCorrection = false;

      chauvenetTrimValue = Chauvenet.STANDARD_CUTOFF_VALUE;
      sumMagStatTrim = true;
      summaryMagValueStatType = MagnitudeMethodIF.MEDIAN_MAG_VALUE;
      setUseAssignedWts(true);
      setScanWaveformTimeWindow(true);
      setScanAllWaveformsForMag(true);
      setMinDistance(Double.MIN_VALUE);
      setMaxDistance(Double.MAX_VALUE);
      setTrimResidual(Double.MAX_VALUE);
      setMaxChannels(Integer.MAX_VALUE);
      setRequireCorrection(false) ;
      setMinValidReadings(defaultMinValidReadings);
      setIncludedReadingTypes(null);
      initChannelMatchers();
      setGlobalCorrection(0.);
      filterCopiesWf = true;
      doLookUp = true;
      deleteInvalid = true;
      deleteRejected = false;
      deleteByDistanceTrim = true;
      deleteByResidualTrim = false;
      disableAppChannelsMap = false;
      oneTimeAppChannelsMap = false;
      oneTimeAppChannelsFYI = 0;
      appChannelsName = methodName;
      disableMagnitudeDistanceCutoff = false; 
      scanAllWaveformsForMag = true;
      scanWaveformTimeWindow = true;

      trimResidual = Double.MAX_VALUE;
      maxDistance = Double.MAX_VALUE;
      minDistance = Double.MIN_VALUE;
      maxChannels = Integer.MAX_VALUE;
  
      parmsRegionNames = new String [] { "DEFAULT" };

      useAssignedWts = true;
      oldEventRegionName = null;

    }

    /** Initialize channel filters. */ 
    protected void initChannelMatchers() {
      List aList =  parseStringTokenList(defaultAcceptChanPropString);
      acceptChannelMatcher = new SeedChanMatcher((String []) aList.toArray(new String [aList.size()]));
      aList =  parseStringTokenList(defaultSummaryChanPropString);
      summaryChannelMatcher = new SeedChanMatcher((String []) aList.toArray(new String [aList.size()]));
      aList =  parseStringTokenList(defaultFilterChanPropString);
      filterChannelMatcher = new SeedChanMatcher((String []) aList.toArray(new String [aList.size()]));
    }

    public Double getSummaryWt(MagnitudeAssocJasiReadingIF jr) {
        //if (! isIncludedReadingType(jr)) { // should we throw WrongDataTypeException?
        //  throw new WrongDataTypeException(
        //    "getSummaryWt input reading wrong type for MagnitudeMethod "+ methodName +
        //    " : " + jr.toNeatString()
        //  );
        //  return null;
        //}
        MagnitudeCalibrationIF calibrData = getCalibrationFor(jr);

        Double summaryWt = (calibrData != null) ? calibrData.getSummaryWt(jr) : null;  // should the default be 0. or 1. ??

        //Temporary print here remove after testing:
        if (debug)
            System.out.println("   DEBUG "+methodName+ " " + getStnChlNameString(jr) +
                " summaryWt :" + summaryWt);

        return summaryWt;
    }

    //abstract public Double getMagCorr(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException;
    // May need to make a correction derived from compound calibration data
    // otherwise could just use reading's Channel corrMap member to lookup corr by corrType?
    // Below return Double.NaN for missing corr instead of Double object?
    /** Returns the magnitude correction for the input reading, returns null if none. */
    public Double getMagCorr(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException {
        if (! isIncludedReadingType(jr)) { // should we throw WrongDataTypeException?
          throw new WrongDataTypeException(
            "getMagCorr input reading wrong type for MagnitudeMethod "+ methodName +
            " : " + jr.toNeatString()
          );
        }
        if (! useMagCorr) return Double.valueOf(0.);
        MagnitudeCalibrationIF calibrData = getCalibrationFor(jr);

        Double magCorr = (calibrData != null) ? calibrData.getMagCorr(jr) : null;

        if (debug) System.out.println("   DEBUG "+methodName+" requireCorr: " + requireCorrection +
                        " corr null?:" + (magCorr == null));

        return magCorr;
    }

    /** Returns the magnitude correction for the input reading, returns Double.NaN if none. */
    protected double getMagCorrValue(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException {
      Double corr = getMagCorr(jr);
      return (corr == null) ? Double.NaN : corr.doubleValue();
    }

    /** Returns true if the input reading has a magnitude correction, otherwise false. */
    public boolean hasMagCorr(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException {
       return (getMagCorr(jr) != null);
    }

    /** Returns the channel magnitude value. Returns either -9. or Double.NaN if not known.
     * Beware of small channel magnitudes with negative station corrections  
     * */
    abstract public double calcChannelMagValue(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException;


    /**
     * Calculate the magnitude for one observation from its internal data,
     * most likely that was generated from a scan of a Waveform timeseries.
     * Updates the ChannelMag instance of the input object with the result.<p>
     * The input observation must be of the correct type for the method,
     * if not, its Channel magnitude is not updated, instead the instance 
     * input weight is set to zero.<p> 
     * Input must have its disance value set, or have valid Channel location data
     * and be associated with a Solution with valid location (LatLonZ) value.
     * The Channel magnitude correction, if any, is applied to resulting value.<p>
     * The method's channel rejection criteria like are NOT checked. 
     * @return  the input argument.
     */
    public MagnitudeAssocJasiReadingIF calcChannelMag(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException {

        if (! hasValidState() ) {
          System.err.println(getClass().getName() + " has invalid initial state, can't do channel mag calculations!");
          return jr;
        }

        // make sure channel is "set up" and distance is calculated
        jr.initChannelMag();  // nulls input reading's mag related values and zeroes wts
        if ( hasRequiredChannelData(jr) ) {
          jr.setChannelMagType(getMagnitudeTypeString());
          jr.setChannelMagValue(calcChannelMagValue(jr) ); // method gets channel calibration data lookup from list

          // doesn't check here for requireCorrection state, just applies channel magnitude correction if found
          Double corr = getMagCorr(jr);
          if (corr != null) jr.setChannelMagCorrUsed(corr.doubleValue());

          // Auto assign new reading quality by an implemented method's criteria
          // however we need to implement some capability for a "human" editor
          // to chose to have a reading's weight maintain a human "fixed" value.
          // Probably not good to using "negative" wt values as a "fix" flag so 
          // we need an attribute in (ChannelMag, MagnitudeAssocJasiReading) to 
          // flag the bypassing of "auto" weighting. Could this be serviced by
          // the RFLAG attribute or do we need to create one?
          // if RFLAG in ('H','F') don't do assignWeight override of quality.
          if (jr.isAuto()) {
            assignWeight(jr); // derives a "wt" then jr.setQuality(wt) 
          }

        }
        if (debug) {
          System.out.println("DEBUG "+methodName+" calcChannelMag for "+ getStnChlNameString(jr) +
                          " isAuto?: " +jr.isAuto()+jr.getChannelMag().toNeatString());
          //    " mag= "+Concatenate.format(new StringBuffer(8),jr.getChannelMagValue(),2,2,2).toString());
        }
        return jr;
    }

    /** Evaluates the input reading's data to determine it's quality, the contributing weight in a weighted
     * summary magnitude algorithm. Implementation should derive a weight value based upon the input reading's
     * data then set the reading's quality attribute to that value. This method is typically invoked by the
     * methods implementing a channel magnitude calculation.
     * @see JasiReading.setQuality(double) 
     * @see calChannelMag(MagnitudeAssocJasiReadingIF) 
     * */
    abstract public void assignWeight(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException;
      // Coda and amplitude readings have an input weight (inWgt) that the
      // magnitude algorithm uses for its summary statistics.
      // Implementations of this method should set the reading quality attribute
      // based upon input's internal data, otherwise assign the reading quality
      // the maximum value of 1. (full weight).
      // Subsequent summary calculation methods map the quality to an inWgt.
      // A successful summary calculation results in an output weight
      // usually equal to the inWgt (unless using other dist,rms or tapered weighting).
      // see ChannelMag.weight.

    // NOTE: Need to have specific methods check for existance of correction/calibration
    // type of interest for input reading's channel then load it into channel map if absent
    /** Returns true if the input reading's Channel has the neccessary location data.
     * Overrides could also implemented a check for magnitude correction data and attempt to
     * load any missing data from the datasource. Returns false if required data is incomplete.
     * */
    protected boolean hasRequiredChannelData(MagnitudeAssocJasiReadingIF jr) {
        Channel jrChan = jr.getChannelObj();
        java.util.Date date = jr.getDateTime(); // reading observation time
        if ( ! jrChan.hasLatLonZ() ) {
          // missing, if doLookUp reloads from source, but why repeated queries ad nauseum ?
          // also see implementation of JasiReading.setChannelObj(Channel) - aww
          if (debug) System.out.println("DEBUG  "+methodName+" hasRequiredChannelData() doLookUp: "+doLookUp);
          if (doLookUp) jr.setChannelObjData(jrChan.lookUp(jrChan, date));
        }
        double dist = jrChan.getHorizontalDistance();// ok to use slant for test aww 06/11/2004
        if ( (dist == Channel.NULL_DIST) || (dist == 0.0) || Double.isNaN(dist) ) {
          jrChan.calcDistance(jr.getAssociatedSolution()); // input arg type change aww 06/11/2004
        }
        if (debug) {
          System.out.println(" DEBUG  "+methodName+" hasRequiredChannelData() chan:"+jrChan+
          " for date:"+date+" latLonZ? "+jrChan.hasLatLonZ() + " hdist: " + jrChan.getHorizontalDistance() +
          " az: " + jrChan.getAzimuth());// ok to use slant for test aww 06/11/2004
        }
        return (jrChan.hasLatLonZ()) ? true : false ;
    }

    /** Convenience wrapper to return the internally set value of the channel magnitude of the
     * input Magnitude associated reading.*/
    public static double getChannelMagValue(MagnitudeAssocJasiReadingIF jr) {
        return jr.getChannelMag().value.doubleValue();
    }

    /** Returns collection of Waveforms from the input list that satisfy acceptance criteria
     * such as maximum channel count, channel maximum distance, and whether Wavform's channel
     * component type is acceptable for the calculations.
     * @see #getMaxChannels();
     * @see #getMaxDistance();
     * @see #isIncludedComponent(Channelable) 
     * */
    public List filterWaveformList(List wfList) {
        Waveform wf[] = (Waveform []) wfList.toArray(new Waveform[wfList.size()]);
        Arrays.sort(wf, new ChannelDistanceSorter());
        int count = wf.length;
        // Removed count reset, input list may include wfs not eligible for mag, maxChannels should apply only to candidate seedchan -aww 2014/08/21
        //if (count > getMaxChannels()) { // enforce maxChannels after ascending distance sort
        //    count = getMaxChannels();
        //    if (debug) System.out.println("DEBUG "+methodName+"Will trim to maxChannels limit = "+count);
        //}

        if (debug) System.out.println("DEBUG "+methodName+" maxChannels to use: "+ getMaxChannels() + " input wf count: " + count);

        ArrayList aList = new ArrayList(count);

        double maxDist = getMaxDistance(); // typically 600 km or less for ML

        if (currentSol != null) { // use its magnitude to guess the max range
          Magnitude mag = currentSol.getPreferredMagnitude();
          if (mag != null && ! mag.hasNullType() && !mag.hasNullValue()) { // assume its in the ballpark 
            // reset distance below to skip calculation of amps from waveforms if seismic energy not likely
            // for catalog reprocessing, reduces # waveforms scanned and # of questionable amps at distance
            // For some methods like CISNml2 the calibrated MaxDistance (500 Km) could be less than this
            // so we want to allow acquiring amp data, though channel mags may not be possible.
            //maxDist = getAmpDistanceCutoff(mag.getMagValue()); // Mag range can be tweaked up or down by property settings
            maxDist = getDistanceCutoff(mag.getMagValue()); // -aww replaced above 2014/08/21
            //System.out.println(getMethodName() + " filterWaveformList maxDist = " + maxDist);
          }
        }
        for (int i=0; i<count; i++) {
            //if ( wf[i].getHorizontalDistance() > getMaxDistance() ) {// use horizontal - aww 06/11/2004
            if ( wf[i].getHorizontalDistance() > maxDist ) {// use horizontal - aww 06/11/2004
               if (verbose) {
                   reportMessage( "filterWaveformList: SKIPPING waveform amp scan for " + (count-i) +
                                  " channels located > maxDist km = " + maxDist + " starting at ", wf[i]
                                ); 
               }
               break;  //  break out of loop
            }
            if (isIncludedComponent(wf[i]) ) aList.add(wf[i]);
        }
        return aList;
    }
    /** Returns the collection of readings (e.g. Coda or Amplitudes) derived from a scan the timeseries of
     * the input Waveforms. These readings can be used to calculate magnitudes using the methods of
     * this instance. 
     * @see #createAssocMagDataFromWaveform(Solution, Waveform)
     * */
    public List createAssocMagDataFromWaveformList(Solution aSol, List wfList) {
        SolutionList sl = new SolutionList(1);
        sl.add(aSol,false);
        return createAssocMagDataFromWaveformList(sl, wfList);
    }
    //public List createAssocMagDataFromWaveformList(Solution aSol, List wfList) {
    public List createAssocMagDataFromWaveformList(SolutionList solList, List wfList) {

    /* TODO: solve waveform magnitude, if existing preferred magnitude of type has readings 
     * and rflag for a reading is set as "F" or "R" (final, required) use that older reading
     * in lieu of any new auto calculated reading (replace new reading with the older value)
     * unless "override" property is set.
     */
        if (! hasValidState() ) {
          System.err.println(getClass().getName() + " has invalid initial state, can't do waveform mag calculations!");
          return new ArrayList(0);
        }

        if (debug) {
            System.out.println("DEBUG " +methodName+ " createAssocMagDataFromWaveformList  wflist.size()= "+ ((wfList == null) ? 0 : wfList.size()));
        }

        Solution aSol = null;
        if (solList != null && solList.size() > 0 ) {
            if (debug) {
                System.out.println("DEBUG " +methodName+ " createAssocMagDataFromWaveformList solList.size()= "+ ((solList == null) ? 0 : solList.size()));
            }

            aSol = solList.getSolution(0);
        }

        if (aSol == null) {
          System.err.println("ERROR " + methodName + " createAssocMagDataFromWaveformList: input solution NULL, skipping mag calculation!");
          return new ArrayList(0);
        }

        if (wfList.isEmpty()) return new ArrayList(0);

        currentSol = aSol;

        List filteredWfList = filterWaveformList(wfList);
        int count = filteredWfList.size();
        if (debug) System.out.println("DEBUG    createAssocMagDataFromWaveformList filteredWflist.size() = "+ count);

        ArrayList dataList = new ArrayList(count); // container for new readings
        MagnitudeAssocJasiReadingIF jr = null;
        //
        // BEGIN test code block for split of hi/lo gain waveform processing 
        // Need configuration property option to enable (reduce processing overhead vs. saving unused data for calibration):
        // Split wfList by summaryChanIdMap1 (highGain) and summaryChanIdMap2 (lowGain) channelable lists
        // loop thru list1, check returned dataList results for valid data, removing the valid data stations from list2,
        // finally loop again to create data for those station channels still remaining in list2.
        //
        //
        if (debug) System.out.println("DEBUG    createAssocMagDataFromWaveformList start: " + new DateTime().toString());

        // Split lists like HH and HN ?
        if ( (summaryChanIdMap1 != null) && ! summaryChanIdMap1.isEmpty() ) { // assumes Map2 empty when Map1 empty 
          ChannelableList wfList1 = new ChannelableList(count);
          ChannelableList wfList2 = new ChannelableList(count);
          ChannelableList wfList0 = new ChannelableList(count);
          if (debug) System.out.println("DEBUG        summaryChanIdMap1 count: " + ((summaryChanIdMap1 == null) ? 0 : summaryChanIdMap1.size()));
          if (debug) System.out.println("DEBUG        summaryChanIdMap2 count: " + ((summaryChanIdMap2 == null) ? 0 : summaryChanIdMap2.size()));
          if (debug) System.out.println("DEBUG        acceptChanIdMap   count: " + ((acceptChanIdMap == null) ? 0 : acceptChanIdMap.size()));
          Waveform wf = null;
          for (int ii = 0; ii < count; ii++) {
              wf = (Waveform) filteredWfList.get(ii);
              if (summaryChanIdMap1 != null && summaryChanIdMap1.contains(wf.getChannelObj().getChannelId())) wfList1.add(wf);
              else if (summaryChanIdMap2 != null && summaryChanIdMap2.contains(wf.getChannelObj().getChannelId())) wfList2.add(wf);
              else wfList0.add(wf);
          }

          // First make the primary list's readings
          count = wfList1.size();
          if (debug) System.out.println("DEBUG        config=1 waveforms: " + count);
          for (int ii = 0; ii < count; ii++) {
              //jr = createAssocMagDataFromWaveform(aSol, (Waveform) wfList1.get(ii));
              jr = createAssocMagDataFromWaveform(solList, (Waveform) wfList1.get(ii));
              if (jr != null && jr.getChannelMag() != null) dataList.add(jr);
              if (debug) System.out.println("===================================================================================");
          }

          // Now loop through primary data, culling secondary list if not empty
          count = dataList.size();
          if (debug) System.out.println("DEBUG        config=1  created readings: " + count);
          for (int ii = 0; ii < count; ii++) {
              // if valid primary reading, delete channel with same net,sta, & orientation from secondary list
              jr = (MagnitudeAssocJasiReadingIF) dataList.get(ii);
              if (isValidForSummaryMag(jr)) { // valid for summary, so cull out the low gain channels replace by below for test
              //if (! isClipped(jr)) { // try Kate's idea, reject all except clipped -aww
                for (int jj = 0; jj < wfList2.size(); jj++) {
                  wf = (Waveform) wfList2.get(jj);
                  if ( jr.getChannelObj().sameStationAs(wf.getChannelObj().getChannelId()) &&
                       jr.getChannelObj().sameOrientationAs(wf.getChannelObj().getChannelId()) ) {
                      wfList2.remove(jj); // delete it from secondary processing list
                      if (debug) System.out.println("DEBUG    createAssocMagDataFromWaveformList  removed1: " + wf.getChannelObj().getChannelId().toString());
                      //break; // removed break, keep going in case of multiple seedchan at station with same orientation
                  }
                }
              }
              if (wfList2.size() == 0) break;
          }

          // Now if secondary list is not already empty, process this list for new readings (low-gains) 
          count = wfList2.size();
          if (debug) System.out.println("DEBUG        config=2 waveforms: " + count);
          for (int i=0; i<count; i++) {
              //jr = createAssocMagDataFromWaveform(aSol, (Waveform) wfList2.get(i));
              jr = createAssocMagDataFromWaveform(solList, (Waveform) wfList2.get(i));
              if (jr != null && jr.getChannelMag() != null) dataList.add(jr);
          }

          // Now assume rest of accept list is for config=0, when not empty, process wfList0 for readings (e.g. those with no stacorr)
          count = wfList0.size();
          if (debug) System.out.println("DEBUG        config=? waveforms: " + count);
          for (int i=0; i<count; i++) {
              //jr = createAssocMagDataFromWaveform(aSol, (Waveform) wfList0.get(i));
              jr = createAssocMagDataFromWaveform(solList, (Waveform) wfList0.get(i));
              if (jr != null && jr.getChannelMag() != null) dataList.add(jr);
          }

          if (debug)
            System.out.println("DEBUG    createAssocMagDataFromWaveformList  end1: " + new DateTime().toString() + " readings: " + dataList.size());
          return dataList;
        }
        // // END of test code block for split of hi/lo gain waveform processing 

        count = filteredWfList.size();
        for (int ii = 0; ii < count; ii++) {
              //jr = createAssocMagDataFromWaveform(aSol, (Waveform) filteredWfList.get(ii));
              jr = createAssocMagDataFromWaveform(solList, (Waveform) filteredWfList.get(ii));
              if (jr != null && jr.getChannelMag() != null) dataList.add(jr);
        }
        if (debug)
          System.out.println("DEBUG    createAssocMagDataFromWaveformList  end2: " + new DateTime().toString() + " readings: " + dataList.size());
        return dataList;
    }

    /** Returns the reading derived from a scan of the input Waveform timeseries whose type is appropriate
     * for magnitude calculation methods of this instance.
     */
    /* TODO: solve waveform magnitude, if existing preferred magnitude of type has readings 
     * and rflag for a reading is set as "F" or "R" (final, required) use that older reading
     * in lieu of any new auto calculated reading (replace new reading with the older value)
     * unless "override" property is set.
     */

    abstract public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(Solution aSol, Waveform wf);
    abstract public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(Solution aSol, Waveform wf, TimeSpan ts);

    abstract public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(SolutionList solList, Waveform wf);
    abstract public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(SolutionList solList, Waveform wf, TimeSpan ts);

    public final static String getStnChlNameString(Channelable obj) {
        StringBuffer sb = new StringBuffer(32);
        sb.append(obj.getChannelObj().toDelimitedNameString('_'));
        sb.append("  ");
        return sb.toString(); // sb.substring(0,12);
    }

    protected void reportMessage(String message, Channelable jr) {
      StringBuffer sb = new StringBuffer(methodName.length()+message.length()+32);
      sb.append(methodName);
      sb.append(" ");
      sb.append(message);
      sb.append(" ");
      sb.append((jr == null) ? "" : jr.getChannelObj().toDelimitedNameString());
      System.out.println(sb.toString());
    }

    //Method
    // Statistical Q-TEST for a max 12 sample test
    public static final double [] CONFIDENCE_TEST = {1.0, 1.0, 1.0, .94,.76,.64,.56,.51,.47,.44,.41,.39,.37};
    protected final boolean rejectChannelsByQTest(List dataList) {
        int count = dataList.size();
        if (count < 3) return false; // keep all data as valid no way of discriminating diff

        double [] magValue = new double[count];
        MagnitudeAssocJasiReadingIF jr = null;
        for (int idx = 0; idx < count; idx++) { // retrieve channel magnitude values
            jr = (MagnitudeAssocJasiReadingIF) dataList.get(idx);
            magValue[idx] = jr.getChannelMag().value.doubleValue();
        }
        int [] sorted = IndexSort.getSortedIndexes(magValue); // ascending sort of magnitude indices
        double range = Math.abs(magValue[sorted[count-1]] - magValue[sorted[0]]); //  Need extremes of data

        // Check magnitude values using the sorted indices
        boolean changed = false;
        for (int idx = 0; idx < count; idx++) {
            double hiDiff = (idx < count-1) ? Math.abs(magValue[sorted[idx]] - magValue[sorted[idx+1]]) : 99999.;
            double loDiff = (idx > 0) ? Math.abs(magValue[sorted[idx]] - magValue[sorted[idx-1]]) : 99999.;
            double testResult = Math.min(loDiff,hiDiff)/range;
            boolean reject = (testResult > CONFIDENCE_TEST[count]);
            if (reject) {
              jr = (MagnitudeAssocJasiReadingIF) dataList.get(sorted[idx]);
              rejectReading(jr);
              changed = true;
              if (debug)
                System.out.println("DEBUG "+methodName+" "+getStnChlNameString(jr)+
                    " Channel mag: "+formatMagValue(magValue[sorted[idx]])+
                    " rejectChannelsByQTest: "+reject);
            }
        }
        return changed;
    }
    protected final boolean rejectChannelsByStdDevTest(Magnitude mag, List dataList) {
        // note better to use a statistical "Q-Test" for 10 or fewer data pts
        int count = dataList.size();
        double magValue = mag.value.doubleValue();
        double magError = mag.error.doubleValue();
        MagnitudeAssocJasiReadingIF jr = null;
        boolean changed = false;
        for (int idx=0; idx < count; idx++) {
            jr = (MagnitudeAssocJasiReadingIF) dataList.get(idx);
            ChannelMag chanMag = jr.getChannelMag();
            double chanMagValue = chanMag.value.doubleValue();
            boolean reject = (Math.abs(magValue - chanMagValue) > magStdDevMult*magError);
            if (reject) {
              rejectReading(jr);
              changed = true;
              if (debug)
                System.out.println( "DEBUG " +methodName+" "+getStnChlNameString(jr)+
                           " Channel mag: " + formatMagValue(chanMagValue) +
                           " rejectChannelsByStdDevTest: " + reject
                           );
            }
        }
        return changed;
    }

    /** Trim outliers using Chauvenet's criterion. Returns 'true' if channel
     * magnitude of readings were rejected and 'false' if not.
     * Robust error analysis maintains it should only be applied once to a data set.
     * @see org.trinet.util.Chauvenet
    */
    public final boolean chauvenetTrim(List aList) {
        boolean changed = false;
        int count = aList.size();
        if (count < 1) return false;
        // get a array of the channel magnitudes
        double magv[] = new double[count];
        MagnitudeAssocJasiReadingIF jr = null;
        for (int i = 0; i < count; i++) {
          jr = (MagnitudeAssocJasiReadingIF) aList.get(i);
          magv[i] = jr.getChannelMagValue();
        }
        double mean = Stats.mean(magv);
        double std  = Stats.standardDeviation(magv);
        double stdDevsAway = 0.;

        /** Trim outliers */
        for (int i=0; i<count; i++) {
          //for single observation mean = value and std = 0.
          stdDevsAway = (count == 1) ?  0. : (mean - magv[i])/std; // aww else we get double NaN 05/03/2005
          //boolean reject = Chauvenet.reject(stdDevsAway, magv.length); // uses standard .5 cutoff - aww
          boolean reject = Chauvenet.reject(stdDevsAway, magv.length, chauvenetTrimValue); // aww 06/25/04
          if (reject) {
            jr = (MagnitudeAssocJasiReadingIF) aList.get(i);
            rejectReading(jr);
            changed = true;
            if (debug)
              System.out.println("DEBUG " +methodName+" "+getStnChlNameString(jr)+
                  " Channel mag: " + formatMagValue(magv[i]) +
                  " chauvenetTrim reject: " + reject + " isDeleted(): " + jr.isDeleted() + " inWgt:" + jr.getInWgt() + " quality:" + jr.getQuality());
          }
        }
        return changed;
    }
    private String formatMagValue(double magValue) {
        return Concatenate.format(new StringBuffer(8),magValue,2,2,2).toString();
    }

    /** Return true only if input seedchan matches any of the types declared for this
     * method that can contribute to a summary magnitude calculation.  The input Channelable
     * must be acceptable for a channel magnitude calculation, and if the method's
     * configuration property "summaryChan" is specified, the input's seedchan string must also
     * match one of the property's specified regex patterns.
     * @see #acceptChannelType(Channelable)
     * */
    /*
    public boolean summaryChannelType(Channelable chan) {
        //return summaryChannelType(chan.getChannelObj().getSeedchan());
        return (acceptChannelType(chan) && summaryChannelType(chan.getChannelObj().getSeedchan()));
    }
    */
    public boolean summaryChannelType(Channelable chan) {
        if (! acceptChannelType(chan)) return false;
        // if no maps use pattern templates
        if ( (summaryChanIdMap1 == null || summaryChanIdMap1.isEmpty()) && (summaryChanIdMap2 == null || summaryChanIdMap2.isEmpty())) {
          return summaryChannelType(chan.getChannelObj().getSeedchan());
        }
        // else use maps
        return (summaryChannelType1(chan) || summaryChannelType2(chan));
    }
    public boolean summaryChannelType1(Channelable chan) {
        return (summaryChanIdMap1 == null || summaryChanIdMap1.isEmpty()) ?
            false : summaryChanIdMap1.contains(chan.getChannelObj().getChannelId());
    }
    public boolean summaryChannelType2(Channelable chan) {
        return (summaryChanIdMap2 == null || summaryChanIdMap2.isEmpty()) ?
            false : summaryChanIdMap2.contains(chan.getChannelObj().getChannelId());
    }
    /**
     * Returns true if the input string matches one of the method's acceptable regex patterns for seedchan type.
     */
    public boolean summaryChannelType(String type) {
        //if (NullValueDb.isBlank(type) && acceptUndefinedChannelTypes) return true;
        return (summaryChannelMatcher.getPatternCount() == 0 || summaryChannelMatcher.matches(type));
    }
    /** Return true only if input seedchan matches any of the types whose waveform timeseries
     * would be filtered before being used to derive a channel magnitude.
     * Usually a subset of acceptable channel types.
     * If the method's configuration property "filterChan" is specified, 
     * the input's seedchan string must match one of the property's specified regex patterns.
     * */
    public boolean filterChannelType(Channelable chan) {
        return filterChannelType(chan.getChannelObj().getSeedchan());
    }
    /**
     * Returns true if the input string matches one of the method's acceptable regex patterns for seedchan type.
     */
    public boolean filterChannelType(String type) {
        //if (NullValueDb.isBlank(type) && acceptUndefinedChannelTypes) return true;
        return (filterChannelMatcher.getPatternCount() == 0 || filterChannelMatcher.matches(type));
    }
    /** Return true only if input seedchan matches any of the channel for which this method can 
     * calculate a valid channel magnitude. If a non-empty ChannelId set is loaded by a concrete subclass 
     * instance, returns true only if that set contains an id that matches's the input, otherwise,
     * if configuration property "acceptChan" is specified, the input seedchan must match one of the property's
     * specified regex patterns. If neither a ChannelId set or property is defined, all Channelables are accepted.
     * */
    public boolean acceptChannelType(Channelable chan) {
        return (acceptChanIdMap == null || acceptChanIdMap.isEmpty()) ?
            acceptChannelType(chan.getChannelObj().getSeedchan()) :
            acceptChanIdMap.contains(chan.getChannelObj().getChannelId());
    }
    /**
     * Returns true if the input string matches one of the method's acceptable regex patterns for seedchan type.
     */
    public boolean acceptChannelType(String type) {
        //if (NullValueDb.isBlank(type) && acceptUndefinedChannelTypes) return true;
        return (acceptChannelMatcher.getPatternCount() == 0 || acceptChannelMatcher.matches(type));
    }
    public String [] getAcceptableChannelTypes() {
      return acceptChannelMatcher.getInputRegex();
    }
    public String [] getFilterChannelTypes() {
      return filterChannelMatcher.getInputRegex();
    }
    public String [] getSummaryChannelTypes() {
      return summaryChannelMatcher.getInputRegex();
    }
    protected static List parseStringTokenList(String input) {
        StringTokenizer st = new StringTokenizer(input);
        List list = new ArrayList(st.countTokens());
        while (st.hasMoreTokens()) {
            list.add(st.nextToken());
        }
        return list;
    }

    public void setCalibrList(ChannelDataTypeMapListIF aList) { calibrList = aList; }

    public ChannelDataTypeMapListIF createDefaultCalibrList() {
      return new ChannelDataCorrectionMap(defaultCalibrationFactory);
    }

    public void setDefaultCalibrationFactory(AbstractChannelCalibration calibr) {
      defaultCalibrationFactory = calibr;
    }

    public MagnitudeCalibrationIF getCalibrationFor(MagnitudeAssocJasiReadingIF jr) {
        // need time/date from Solution current
        return getCalibrationFor(jr, jr.getDateTime()); // for UTC true -aww 2008/02/11
    }
    public MagnitudeCalibrationIF getCalibrationFor(Channelable chan, java.util.Date date) {
        /*
        MagnitudeCalibrationIF calibrData = null;
        calibrData = (MagnitudeCalibrationIF) calibrList.get(chan.getChannelObj().getChannelId(), date);
        if (calibrData == null) {
          // try loading single calibration for this station channel -aww 
          calibrData = (MagnitudeCalibrationIF) calibrList.load(chan, date);
        }
        return  calibrData;
        */
        /*
        if (calibrList == null || calibrList.isEmpty())  {
          loadCalibrations(date);
        }
        return (MagnitudeCalibrationIF) calibrList.lookUp(chan.getChannelObj().getChannelId(), date);
        */
        Channel sc = chan.getChannelObj();

        /*
        System.out.println("DEBUG getCalibrationFor(chan,date)");
        System.out.println("   date : " + EpochTime.dateToString(date));
        System.out.println("   magType: " + getMagnitudeTypeString());
        System.out.println("   channel dump : " + ((Channel)sc).toDumpString());
        System.out.println("   calibrList is NULL? " + (calibrList ==null) + " doLookUp: " + doLookUp);
        */

        // TODO: Convert from the calibrList member to using a MasterChannelList lookup -aww
        MagnitudeCalibrationIF magCalibr = null;
        if (calibrList == null) {
          // get from channel corrMap, else load from db, if not found return null
          //magCalibr = (MagnitudeCalibrationIF) sc.getCorrection(date, "m" + getMagnitudeTypeString());
          magCalibr = (MagnitudeCalibrationIF) sc.getCorrection(date, "m" + getMagnitudeTypeString(), doLookUp); // boolean flag to skip over -aww 2010/08/19
          //System.out.println( "DEBUG AbstractMagMeth: "+( (magCalibr == null) ? (sc.toDelimitedSeedNameString()+" NULL CALIBR") : magCalibr.toString()) ); 
        }
        else {
          if (doLookUp) { // get from collection, else query datasource, if found adds to collection, else return null
              magCalibr = (MagnitudeCalibrationIF) calibrList.lookUp(sc.getChannelName(), date);
          }
          else { // get from collection, if not found return null
              magCalibr = (MagnitudeCalibrationIF) calibrList.get(sc.getChannelName(), date);
          }
        }
        return magCalibr;
    }

    public void loadCalibrations(java.util.Date date) {
        if (calibrList == null) calibrList = createDefaultCalibrList();
        calibrList.load(date);
        if (calibrList.isEmpty()) {
          throw new EmptyCalibrationListException(methodName+" calibrList load returned empty list check database");
        }
        if (debug) ((ChannelDataMap)calibrList).printValues();
    }


    protected class EmptyCalibrationListException extends RuntimeException  {
        protected EmptyCalibrationListException () {
            super();
        }
        protected EmptyCalibrationListException (String message) {
            super(message);
        }
    }

    /* TODO : Perhaps a toString() or dump() method to list class name and its configuration properties
    public String acceptMapToString() {
        return (acceptChanIdMap != null) ? acceptChanIdMap.toString() : "" ;
    }
    public String primarySummaryMapToString() {
        return (summaryChanIdMap1 != null) ? summaryChanIdMap1.toString() : "" ;
    }
    public String secondarySummaryMapToString() {
        return (summaryChanIdMap2 != null) ? summaryChanIdMap2.toString() : "" ;
    }
    */

    /*
    // TODO: NEED to set magMethod properties filetype via delegate values, in order to use same system property for user path!
    public void readMapsFromCache() {
       acceptChanIdMap = readMapFromCacheFile(props.getUserFilePath() + GenericPropertyList.FILE_SEP + appChannelsName + "acceptChanIdMap.cache");
       summaryChanIdMap1 = readMapFromCacheFile(props.getUserFilePath() + GenericPropertyList.FILE_SEP + appChannelsName + "summaryChanIdMap1.cache");
       summaryChanIdMap2 = readMapFromCacheFile(props.getUserFilePath() + GenericPropertyList.FILE_SEP + appChannelsName + "summaryChanIdMap2.cache");
    }

    private HashSet readMapFromCacheFile(String filename) {
      HashSet cl = null;
      try { // if input filename is null, use default file location - aww
        FileInputStream instream = new FileInputStream(filename);
        ObjectInputStream stream = new ObjectInputStream(instream);
        cl = (HashSet) stream.readObject() ;
        stream.close();
      } catch (FileNotFoundException ex) {
        System.err.println ("Unable to find map file named: " + filename);
      } catch (InvalidClassException ex) {
        System.err.println ("Version of file object predates its class byte code recompilation time.");
      } catch (Exception ex) {
        ex.printStackTrace();
      }
      return (cl == null) ? new HashSet() : cl;
    }

    public boolean writeMapsToCache() {
      boolean status = writeMapToCacheFile(acceptChanIdMap, props.getUserFilePath() + GenericPropertyList.FILE_SEP + appChannelsName + "AcceptChanIdMap.cache");
      status &= writeMapToCacheFile(summaryChanIdMap1, props.getUserFilePath() + GenericPropertyList.FILE_SEP +appChannelsName+ "SummaryChanIdMap1.cache");
      status &= writeMapToCacheFile(summaryChanIdMap2, props.getUserFilePath() + GenericPropertyList.FILE_SEP +appChannelsName+ "SummaryChanIdMap2.cache");
      return status;
    }

    private boolean writeMapToCache(Object obj, String filename) {
      boolean status = true;
      ObjectOutputStream stream = null;
      // Now serialize contents:
      try {
        stream = new ObjectOutputStream(new FileOutputStream(filename));
        stream.writeObject(obj);
      } catch (IOException ex) {
        ex.printStackTrace();
        status = false ;
      } finally {
        if (stream != null) {
          try {
            stream.flush();
            stream.close();
          } catch (Exception ex2) { }
        }
      }
      return status;
    }
  */
}
