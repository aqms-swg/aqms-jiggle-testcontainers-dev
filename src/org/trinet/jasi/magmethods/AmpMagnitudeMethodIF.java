package org.trinet.jasi.magmethods;
import org.trinet.jasi.*;
public interface AmpMagnitudeMethodIF extends MagnitudeMethodIF {
    public double calcChannelMagValue(double distance, double value) ;
    public double calcChannelMagValue(double distance, double value, double staticCorrection) ;
    public double calcChannelMagValue(Amplitude amp) throws WrongDataTypeException ;
    public boolean hasAcceptableSNR(MagnitudeAssocJasiReadingIF jr);
    public void setMinSNR(double value);
    public double getMinSNR();
}
