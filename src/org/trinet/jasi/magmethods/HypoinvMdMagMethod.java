package org.trinet.jasi.magmethods;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;
import org.trinet.util.*;

//
// Changed unknown channel mag return from  -1 to -9 -aww 08/12/2004
//

/**
Eaton (1992).  MD(f-p) = -0.81 + 2.22*log (f-p) + 0.0011*D + Stacor + G + distCorr
where distCorr:
  + 0.005*(D - 40) if D < 40 km
  + 0.0006*(D - 350) if D > 350 km
  + 0.014*(Z - 10) if Z > 10 km

The supported form of the lapse-time expression is:
  MT(tau) = DMA0 + DMA1*log(tau) + DMA2*log_(tau) + DMLIN*tau + DMZ*Z + DMGN*G + STACOR + Component correction
where tau is the lapse time (P travel time + coda duration f-p), Z is the (positive) depth.
STACOR is the duration magnitude correction for the station, G is the gain correction.
Default coefficients for DMA0, DMA1, DMA2, DMLI, DMZ, DMGN are -1.03, 2.10, 0., .00268, 0., and 1.
But check the source of HYBDA.FOR to be sure.
*/
//
// Hypoinverse doc default: DMA0=-1.312, DMA1=2.329, DMA2=0, DMLIN=0.00197, DMZ=0, and DMGN=1. is wrong!
//
public class HypoinvMdMagMethod extends MdMagMethod {

    private class HypoinvParms {

        static final double DEFAULT_Fma1 = -.81;
        static final double DEFAULT_Fmb1 = 2.22;
        static final double DEFAULT_Fmz1 = 0.;
        static final double DEFAULT_Fmd1 = 0.0011;
        static final double DEFAULT_Fmf1 = 0.;

        static final double DEFAULT_Fma2 = 0.;
        static final double DEFAULT_Fmb2 = 0.;
        static final double DEFAULT_Fmz2 = 0.;
        static final double DEFAULT_Fmd2 = 0.;
        static final double DEFAULT_Fmf2 = 0.;

        static final double DEFAULT_Fmbrk = 9999.; // default not bilinear fit

        static final double DEFAULT_Dcofm1 = 0.005;
        static final double DEFAULT_Dbrkm1 = 40.;
        static final double DEFAULT_Dcofm2 = 0.0006; // was .0005, HYP2000 document says should be .0006, changed -aww 2010/05/13 
        static final double DEFAULT_Dbrkm2 = 350.;
        static final double DEFAULT_Zcofm  = 0.014;
        static final double DEFAULT_Zbrkm  = 10.;

        double fma1 = DEFAULT_Fma1;
        double fmb1 = DEFAULT_Fmb1;
        double fmz1 = DEFAULT_Fmz1;
        double fmd1 = DEFAULT_Fmd1;
        double fmf1 = DEFAULT_Fmf1;
        double fma2 = DEFAULT_Fma2;
        double fmb2 = DEFAULT_Fmb2;
        double fmz2 = DEFAULT_Fmz2;
        double fmd2 = DEFAULT_Fmd2;
        double fmf2 = DEFAULT_Fmf2;
        double fmbrk = DEFAULT_Fmbrk;

        double dcofm1 = DEFAULT_Dcofm1;
        double dbrkm1 = DEFAULT_Dbrkm1;
        double dcofm2 = DEFAULT_Dcofm2;
        double dbrkm2 = DEFAULT_Dbrkm2;
        double zcofm  = DEFAULT_Zcofm;
        double zbrkm  = DEFAULT_Zbrkm;

        public void copy(HypoinvParms hp) {
            fma1 = hp.fma1;
            fmb1 = hp.fmb1;
            fmz1 = hp.fmz1;
            fmd1 = hp.fmd1;
            fmf1 = hp.fmf1;
            fma2 = hp.fma2;
            fmb2 = hp.fmb2;
            fmz2 = hp.fmz2;
            fmd2 = hp.fmd2;
            fmf2 = hp.fmf2;
            fmbrk = hp.fmbrk;

            dcofm1 = hp.dcofm1;
            dbrkm1 = hp.dbrkm1;
            dcofm2 = hp.dcofm2;
            dbrkm2 = hp.dbrkm2;
            zcofm  = hp.zcofm;
            zbrkm  = hp.zbrkm;
        }

        public String toString() {
          StringBuffer sb = new StringBuffer(512);
          sb.append(" Md equation coef: ");
          sb.append(" fma1: ").append( fma1 );
          sb.append(" fmb1: ").append( fmb1 );
          sb.append(" fmz1: ").append( fmz1 );
          sb.append(" fmd1: ").append( fmd1 );
          sb.append(" fmf1: ").append( fmf1 );
          sb.append(" fma2: ").append( fma2 );
          sb.append(" fmb2: ").append( fmb2 );
          sb.append(" fmz2: ").append( fmz2 );
          sb.append(" fmd2: ").append( fmd2 );
          sb.append(" fmf2: ").append( fmf2 );
          sb.append(" fmbrk: ").append( fmbrk );
          sb.append(" dcofm1: ").append( dcofm1 );
          sb.append(" dbrkm1: ").append( dbrkm1 );
          sb.append(" dcofm2: ").append( dcofm2 );
          sb.append(" dbrkm2: ").append( dbrkm2 );
          sb.append(" zcofm: ").append( zcofm  );
          sb.append(" zbrkm: ").append( zbrkm  );
          return sb.toString();
        }
    }

    private HypoinvParms myparms = new HypoinvParms();
    private HashMap parmsMap = new HashMap(3);

    // Types: EATON,BAKUN&JOYNER,RICHTER,BKY-NORDQUIST,P-AMP
    private static final String DEFAULT_LOGA0_TYPE = "EATON";
    private String logA0RelationType = DEFAULT_LOGA0_TYPE; 

    //private static final boolean DEFAULT_USE_IN_WTS = true; // false => full weight
    //private boolean useAssignedWts = DEFAULT_USE_IN_WTS; // moved up - aww

    /* Below DEPRECATED:
    // property could be made "generic" and moved up in class hierarchy
    // table column value from ChannelMap_CodaParms is always used
    // value should be set to zero (or null?) when a gain_corr not needed.
    //private static final boolean DEFAULT_USE_GAIN_CORR = true; // deprecated
    //private boolean useGainCorr = DEFAULT_USE_GAIN_CORR; // deprecated

    // property "fmagGainCorrChan" accompanies "useGainCorr" to get the matching channel name patterns
    //protected AbstractChannelNameMatcher gainCorrChannelMatcher; // deprecated

    // KLUDGE Map stuff should be folded into stored table correction values like gain_corr 
    private HashMap channelMagCorrMap; // deprecated
    */

    {
        defaultMinValidReadings = 1;
        minValidReadings = defaultMinValidReadings;
        summaryMagValueStatType = MagnitudeMethodIF.WT_MEDIAN_MAG_VALUE;
    }

    public HypoinvMdMagMethod() {
        super();
        //note map to Hypoinverse code:
        //HypoinvMd,HypoinvMt,HypoinvMz => coda1,elapsed,coda2
        methodName = "HypoinvMd";
    }

    // punt here for now
    public void configure(int src, String location, String section) {
        setProperties(location); // location like a filename url
    }

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    // NoCal uses weighted median, now the default setting upon init -aww 2012/09/18
    //public int getSummaryMagValueStatType() {  // don't override allow user to set in parent properties -aww
    //  return WT_MEDIAN_MAG_VALUE;
    //} 

    // NCEDC post-processsing does not do a bilinear coda line fit, method
    // assumes no bilinear fit and zero source depth, since depth bogus, 
    // correction only the horizontal distance contribution.
    public double distanceCorrection(double distKm) {
        return distanceCorrection(distKm, 0., 0.);
    }
    // Assumes no bilinear fit for coda. Uses source depth.
    public double distanceCorrection(double distKm, double depthKm) { // positive increasing depth
        return distanceCorrection(distKm, depthKm, 0.); // zero implies tau unknown 
    }
    // Allows bilinear coda line fit, uses source depth and tau. Positive depth below ground datum.
    public double distanceCorrection(double distKm, double depthKm, double tau) {
      // bi-linear fit, distance break point based on tau
      double corr = (tau < myparms.fmbrk) ?  myparms.fmd1*distKm+myparms.fmz1*depthKm : myparms.fmd2*distKm+myparms.fmz2*depthKm;
      // Apply additional distance and depth corrections
      if (distKm < myparms.dbrkm1) corr += myparms.dcofm1*(distKm - myparms.dbrkm1);
      if (distKm > myparms.dbrkm2) corr += myparms.dcofm2*(distKm - myparms.dbrkm2);
      if (depthKm > myparms.zbrkm)  corr += myparms.zcofm*(depthKm - myparms.zbrkm);
      return corr;
    }

    /* removed since return is same as parent super class - aww 04/25/2005
    // hypoinverse scales number 0-4 wts in HYMAG fortran source.
    public void assignWeight(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException {
      if (debug) System.out.println("DEBUG " + methodName + " assigning weight to reading.");
      super.assignWeight(jr);
    }
    */

    public Double getMagCorr(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException {

        //Double magCorr = super.getMagCorr(jr);

        if (! isIncludedReadingType(jr)) { // should we throw WrongDataTypeException?
          throw new WrongDataTypeException(
            "getMagCorr input reading wrong type for MagnitudeMethod "+ methodName +
            " : " + jr.toNeatString()
          );
        }

        if (! useMagCorr) return Double.valueOf(0.);

        MdCodaMagCalibration calibrData = (MdCodaMagCalibration) getCalibrationFor(jr);

        Double magCorr = (calibrData != null) ? calibrData.getMagCorr(jr) : null;

        if (debug) {
            System.out.print(" DEBUG "+methodName+" requireCorr: " + requireCorrection +
                        " corr null?:" + (magCorr == null));
            if (magCorr != null) System.out.print(" corr=" + magCorr);
        }

        //
        if (magCorr == null) {
          //if (requireCorrection) return null; // removed 02/13/2006 to allow analog channel adjustments -aww
          magCorr = Double.valueOf(0.); // force site to zero and just recover gain and aux corr below
        }

        double chanMagCorr = magCorr.doubleValue(); 

        // Get auxillary gain correction data specific to Hypoinv Md method
        // Get gain correction, only if needed
        double gainCorr = 0.;
        //if (debug) System.out.print(" DEBUG gainCorrType?: " + gainCorrChannelType(jr)); // removed DEPRECATED
        //if (useGainCorr && gainCorrChannelType(jr)) { // removed DEPRECATED
          //MdCodaMagCalibration calibrData = (MdCodaMagCalibration) getCalibrationFor(jr); // removed DEPRECATED
          // 3.95 ASSUMED CAL OF AN ANALOG 15 DB STATION WITH A CORRECTION OF 0.0
          // CAL value from calibration table
          //Double cal = (calibrData != null) ? calibrData.getCal() : null;
          //gainCorr = (cal != null) MathTN.log10(cal.doubleValue()/3.95) : 0.;

          Double gcorr = (calibrData != null) ? calibrData.getGainCorr(jr) : null;

          if (debug) { // temp for testing aww
               System.out.print(" DEBUG gcorr: " + ((gcorr == null) ? "null" : String.valueOf(gcorr.doubleValue())));
          }

          // return if required and a valid site value is not found
          //if (gcorr == null) return null;  // removed, now assume if null, default to 0. -aww 2010/05/14
          gainCorr = (gcorr != null) ? gcorr.doubleValue() : 0.;

        //} // removed if condition test DEPRECATED
        chanMagCorr += gainCorr;

        // auxCorr below should be folded into table stored value for gain_corr 
        // if it isn't specific is this Md mag algorithm, it seems to apply to
        // all "low" gain analog channels like "VLZ" 
        /* DEPRECATED: 
        // NCEDC is going to "fold" this into their site correction in StaCorrections table - aww 03/18/2006
        Double auxCorr  = (Double) channelMagCorrMap.get(jr.getChannelObj().getChannel());
        if (debug) { // temp for testing aww
          System.out.print(" DEBUG auxCorr: " + ((auxCorr == null) ? "null" : String.valueOf(auxCorr.doubleValue())));
        }
        chanMagCorr += ((auxCorr == null) ? 0. : auxCorr.doubleValue());
        */

        return Double.valueOf(chanMagCorr);
    }

    /** Returns true if the input reading has a magnitude correction, otherwise false.
     * Override makes behavior consistent with regards to a site correction test only (StaCorrection value).
     * Ignores the gainCorr and/or auxCorr terms applied to analog source (EHZ) data streams.
     * */
    public boolean hasMagCorr(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException {
       return (super.getMagCorr(jr) != null ); // ignore gainCorr and/or auxCorr of analog streams - aww
    }

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    public double calcChannelMagValue(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException {

        if (debug) {  // for debug Abstract super has  static method getStnChlNameString(channelable)?
          System.out.print(
                            getStnChlNameString(jr) +
                          " incType?:" +isIncludedReadingType(jr) +
                          " type: " + jr.getTypeQualifier()
           );
        } // temp for testing - aww

        if (! isIncludedReadingType(jr)) { // should we throw WrongDataTypeException?
          throw new WrongDataTypeException("input wrong subtype for MagnitudeMethod "+
                        methodName);
        }

        Coda coda = (Coda) jr;
        double tau = getCodaTau(coda);
        double zc = coda.getVerticalDistance(); // this value differs per channel elevation
        // Constants for coda algorithm use Solution depth, no elevation delta
        // value constant for all assoc coda 
        double z = coda.getAssociatedSolution().getModelDepth(); // switched from getDepth -aww 2015/10/09
        if (debug) {
          System.out.print(" DEBUG tau: " +tau+
                           " dist:" +coda.getHorizontalDistance()+
                           " zSol:" +z+ " zCoda:" +zc
                          );
        } // temp for testing - aww

        // changed return -1 to -9 to make it obvious to user -aww 08/12/2004
        if ( tau <= 0. || Double.isNaN(tau) ) return -9.;

        Double magCorr = getMagCorr(jr);
        /* requireCorrection or NOT?
        if (magCorr == null) {
          if (requireCorrection) {
            //if (debug) reportMessage("input reading has no mag correction", jr);
              throw new WrongCodaTypeException(methodName+ " calcChannelMagValue input !hasMagCorr: " +
                            getStnChlNameString(jr));
          }
        }
        */

        if (debug) {
          if (magCorr == null) System.out.print(" calcChannelMagValue correction undefined, defaulting to ");
          System.out.println(
                           " magCorr: " + ((magCorr == null) ? 0. : magCorr.doubleValue())
                            );
        } // temp for testing - aww


        return calcChannelMagValue(
                        tau,
                        coda.getHorizontalDistance(),
                        z, 
                        (magCorr == null) ? 0. : magCorr.doubleValue()
               );
    }
    protected double calcChannelMagValue(double tau, double distKm, double vertKm, double chlMagCorr) {
      // changed return -1 to -9 to make it obvious to user -aww 08/12/2004
      if (tau <= 0.) return -9.0;

      double logTau =MathTN.log10(tau);
      double magValue = -9.;

      if (tau < myparms.fmbrk) { // bilinear fit break point based on tau
        magValue = myparms.fma1 +myparms.fmb1*logTau +myparms.fmd1*distKm +myparms.fmz1*vertKm + myparms.fmf1*tau + chlMagCorr;
      }
      else {
        magValue = myparms.fma2 +myparms.fmb2*logTau +myparms.fmd2*distKm +myparms.fmz2*vertKm + myparms.fmf2*tau + chlMagCorr;
      } 

      // Apply additional distance and depth corrections
      if (distKm < myparms.dbrkm1) magValue += myparms.dcofm1*(distKm -myparms.dbrkm1);
      if (distKm > myparms.dbrkm2) magValue += myparms.dcofm2*(distKm -myparms.dbrkm2);
      if (vertKm > myparms.zbrkm)  magValue += myparms.zcofm*(vertKm-myparms.zbrkm);

      // changed return -1 to -9 to make it obvious to user -aww 08/12/2004
      // return (magValue >= 0.) ? magValue : -9.;
      return magValue; // 07/05/2007 aww
    }

    public void initializeMethod() {

      super.initializeMethod(); // configure super class properties
      if (props == null) return; // may not have had props set yet

      logA0RelationType = props.getProperty("fmagLogA0RelationType", DEFAULT_LOGA0_TYPE);
      //useAssignedWts = Boolean.valueOf(props.getProperty("fmagUseAssignedWts",String.valueOf(DEFAULT_USE_IN_WTS))).booleanValue();

      initRegionDependentProps();

    }
  
    protected void initRegionDependentProps() {

      if (props == null) return; // may not have had props set yet

      HypoinvParms hp = (HypoinvParms)parmsMap.get("DEFAULT");

      StringTokenizer st = null;
  
      String propStr = props.getProperty("fmagDurParms");
      if (propStr != null) {
        st = new StringTokenizer(propStr);
        hp.fma1 = Double.parseDouble(st.nextToken());
        hp.fmb1 = Double.parseDouble(st.nextToken());
        hp.fmz1 = Double.parseDouble(st.nextToken());
        hp.fmd1 = Double.parseDouble(st.nextToken());
        hp.fmf1 = Double.parseDouble(st.nextToken());
        hp.fma2 = Double.parseDouble(st.nextToken());
        hp.fmb2 = Double.parseDouble(st.nextToken());
        hp.fmz2 = Double.parseDouble(st.nextToken());
        hp.fmd2 = Double.parseDouble(st.nextToken());
        hp.fmf2 = Double.parseDouble(st.nextToken());
        hp.fmbrk = Double.parseDouble(st.nextToken());
      }
   
      propStr = props.getProperty("fmagDistZParms");
      if (propStr != null) {
        st = new StringTokenizer(propStr);
        hp.dcofm1 = Double.parseDouble(st.nextToken());
        hp.dbrkm1 = Double.parseDouble(st.nextToken());
        hp.dcofm2 = Double.parseDouble(st.nextToken());
        hp.dbrkm2 = Double.parseDouble(st.nextToken());
        hp.zcofm  = Double.parseDouble(st.nextToken());
        hp.zbrkm  = Double.parseDouble(st.nextToken());
      }
      myparms.copy(hp);

      if (parmsRegionNames.length > 0) {
        //Arrays.sort(parmsRegionNames);
        String name = null;
        for (int idx=0; idx<parmsRegionNames.length; idx++) {
            name = "fmag."+parmsRegionNames[idx]+".DurParms";
            propStr = props.getProperty(name);
            if (propStr != null) {
                HypoinvParms rhp = (HypoinvParms) parmsMap.get(parmsRegionNames[idx]);
                if (rhp == null) {
                    rhp = new HypoinvParms();
                    parmsMap.put(parmsRegionNames[idx], rhp);
                }

                st = new StringTokenizer(propStr);
                rhp.fma1 = Double.parseDouble(st.nextToken());
                rhp.fmb1 = Double.parseDouble(st.nextToken());
                rhp.fmz1 = Double.parseDouble(st.nextToken());
                rhp.fmd1 = Double.parseDouble(st.nextToken());
                rhp.fmf1 = Double.parseDouble(st.nextToken());
                rhp.fma2 = Double.parseDouble(st.nextToken());
                rhp.fmb2 = Double.parseDouble(st.nextToken());
                rhp.fmz2 = Double.parseDouble(st.nextToken());
                rhp.fmd2 = Double.parseDouble(st.nextToken());
                rhp.fmf2 = Double.parseDouble(st.nextToken());
                rhp.fmbrk = Double.parseDouble(st.nextToken());
            }
            name = "fmag."+parmsRegionNames[idx]+".DistZParms";
            propStr = props.getProperty(name);
            if (propStr != null) {
                HypoinvParms rhp = (HypoinvParms) parmsMap.get(parmsRegionNames[idx]);
                if (rhp == null) {
                    rhp = new HypoinvParms();
                    parmsMap.put(parmsRegionNames[idx], rhp);
                }
                st = new StringTokenizer(propStr);
                rhp.dcofm1 = Double.parseDouble(st.nextToken());
                rhp.dbrkm1 = Double.parseDouble(st.nextToken());
                rhp.dcofm2 = Double.parseDouble(st.nextToken());
                rhp.dbrkm2 = Double.parseDouble(st.nextToken());
                rhp.zcofm  = Double.parseDouble(st.nextToken());
                rhp.zbrkm  = Double.parseDouble(st.nextToken());
            }
        }
      }

      // Set current parms to default region settings, or else to the solution's region specific values
      if (eventRegionName != null) {
          if (debug) System.out.println("DEBUG recovering parms from parmsMap for region: " + eventRegionName);
          hp = (HypoinvParms)parmsMap.get(eventRegionName);
          if (hp != null) {
              myparms.copy(hp);
              if (debug) System.out.println("DEBUG copied event region parms to myparms: " + myparms);
          }
          else if (debug) System.out.println("DEBUG NO parms found for region in map, copying DEFAULT parms to myparms");
      }
      else if (debug) System.out.println("DEBUG eventRegionName is NULL");

      if (debug) {
        System.out.println("DEBUG Current method myparms :");
        System.out.println(myparms.toString());

        System.out.println("DEBUG All known region parms :");
        System.out.println(parmsMap.toString());
      }
    }

    /* DEPRECATED
    protected void initChannelMatchers() {
      super.initChannelMatchers();
      List aList =  parseStringTokenList(defaultGainCorrChanPropString);
      gainCorrChannelMatcher = new ChanMatcher((String []) aList.toArray(new String [aList.size()]));
    }

    public String [] getGainCorrChannelTypes() {
      return gainCorrChannelMatcher.getInputRegex();
    }
    public boolean gainCorrChannelType(Channelable chan) {
      if (gainCorrChannelMatcher.getPatternCount() == 0) return false;
      if (debug) System.out.println("DEBUG gainCorrChannelType input chan: " + chan.getClass().getName()+ 
                      " : " +
                      chan.getChannelObj().getChannelName().toCompleteString("_"));
      return gainCorrChannelMatcher.matches(chan);
      //return gainCorrChannelType(chan.getChannelObj().getChannel());
    }
    public boolean gainCorrChannelType(String type) {
      if (gainCorrChannelMatcher.getPatternCount() == 0) return false;
      if (debug) System.out.println("DEBUG gainCorrChannelType input type: " + type);
      return gainCorrChannelMatcher.matches(type);
    }
    */

    public GenericPropertyList getProperties() {
      props =  super.getProperties();
      /* DEPRECATED:
      props.setProperty("fmagGainCorrChan", props.toPropertyString(gainCorrChannelMatcher.getInputRegex()));
      props.setProperty("fmagUseGainCorr", useGainCorr); 
      */
      //props.setProperty("fmagUseAssignedWts", useAssignedWts); 
      props.setProperty("fmagLogA0RelationType", logA0RelationType);

      HypoinvParms hp = (HypoinvParms) parmsMap.get("DEFAULT");

      StringBuffer sb = new StringBuffer(132);
        sb.append(hp.fma1).append(" ");
        sb.append(hp.fmb1).append(" ");
        sb.append(hp.fmz1).append(" ");
        sb.append(hp.fmd1).append(" ");
        sb.append(hp.fmf1).append(" ");
        sb.append(hp.fma2).append(" ");
        sb.append(hp.fmb2).append(" ");
        sb.append(hp.fmz2).append(" ");
        sb.append(hp.fmd2).append(" ");
        sb.append(hp.fmf2).append(" ");
        sb.append(hp.fmbrk);
      props.setProperty("fmagDurParms", sb.toString());
      sb = new StringBuffer(132);
        sb.append(hp.dcofm1).append(" ");
        sb.append(hp.dbrkm1).append(" ");
        sb.append(hp.dcofm2).append(" ");
        sb.append(hp.dbrkm2).append(" ");
        sb.append(hp.zcofm).append(" ");
        sb.append(hp.zbrkm);
      props.setProperty("fmagDistZParms", sb.toString());

      ArrayList aList = new ArrayList(parmsMap.keySet());
      Collections.sort(aList);
      String keyname = null;
      for (int idx=0; idx<aList.size(); idx++) {
           keyname = (String) aList.get(idx);
           if (keyname.equals("DEFAULT")) continue;
            hp = (HypoinvParms) parmsMap.get(keyname);
            sb = new StringBuffer(132);
            sb.append(hp.fma1).append(" ");
            sb.append(hp.fmb1).append(" ");
            sb.append(hp.fmz1).append(" ");
            sb.append(hp.fmd1).append(" ");
            sb.append(hp.fmf1).append(" ");
            sb.append(hp.fma2).append(" ");
            sb.append(hp.fmb2).append(" ");
            sb.append(hp.fmz2).append(" ");
            sb.append(hp.fmd2).append(" ");
            sb.append(hp.fmf2).append(" ");
            sb.append(hp.fmbrk);
          props.setProperty("fmag."+keyname+".DurParms", sb.toString());
          sb = new StringBuffer(132);
            sb.append(hp.dcofm1).append(" ");
            sb.append(hp.dbrkm1).append(" ");
            sb.append(hp.dcofm2).append(" ");
            sb.append(hp.dbrkm2).append(" ");
            sb.append(hp.zcofm).append(" ");
            sb.append(hp.zbrkm);
            props.setProperty("fmag."+keyname+".DistZParms", sb.toString());

      }

 // KLUDGE Map stuff should be folded into stored table correction values like gain_corr 
 /* DEPRECATED:
      sb = new StringBuffer(132);
      Collection entrySet = channelMagCorrMap.entrySet();
      int count = entrySet.size();
      if (count > 0) {
        Map.Entry [] me = (Map.Entry []) entrySet.toArray(new Map.Entry [count]);
        for (int i = 0; i < count; i++) { 
          sb.append(me[i].getKey()).append(" ").append(me[i].getValue()).append(" ");
        } 
      }
      props.setProperty("fmagChannelDefaultCorr", sb.toString());
*/

      return props;
    }

    public void setDefaultProperties() {
      super.setDefaultProperties();

      setDefaultCoeff();

      logA0RelationType = DEFAULT_LOGA0_TYPE;

      //useGainCorr       = DEFAULT_USE_GAIN_CORR; // DEPRECATED
      // note gainCorrChan filter is by id "channel" not "seedchan" attribute
      // to filter digitals out, otherwise populate table with gain_corr null or zero.
      //defaultGainCorrChanPropString = "EHZ SHZ ELZ EHE EHN"; // replaced by below spec
      //defaultGainCorrChanPropString = "VHZ SHZ VLZ VLE VLN"; // DEPRECATED
      //defaultAcceptChanPropString   = "EHZ SHZ ELZ EHE EHN HHZ"; // HHZ?
      //defaultFilterChanPropString   = "HHZ";
      //defaultSummaryChanPropString  = "EHZ";
      // KLUDGE Map stuff should be folded into stored table correction values like gain_corr 
      // default for NC menlo park?
      /* Below DEPRECATED:
      if (channelMagCorrMap == null) channelMagCorrMap = new HashMap(11);
      else channelMagCorrMap.clear();
      channelMagCorrMap.put("ELZ",Double.valueOf(-0.06));
      channelMagCorrMap.put("ELE", Double.valueOf(-0.30));
      channelMagCorrMap.put("ELN",Double.valueOf(-0.30));
      */

    }

    private void setDefaultCoeff() {


        parmsMap.clear();
        parmsMap.put("DEFAULT", new HypoinvParms());

        /*
        fma1 = DEFAULT_Fma1;
        fmb1 = DEFAULT_Fmb1;
        fmz1 = DEFAULT_Fmz1;
        fmd1 = DEFAULT_Fmd1;
        fmf1 = DEFAULT_Fmf1;
        fma2 = DEFAULT_Fma2;
        fmb2 = DEFAULT_Fmb2;
        fmz2 = DEFAULT_Fmz2;
        fmd2 = DEFAULT_Fmd2;
        fmf2 = DEFAULT_Fmf2;
        fmbrk = DEFAULT_Fmbrk;
    
        dcofm1 = DEFAULT_Dcofm1;
        dbrkm1 = DEFAULT_Dbrkm1;
        dcofm2 = DEFAULT_Dcofm2;
        dbrkm2 = DEFAULT_Dbrkm2;
        zcofm  = DEFAULT_Zcofm;
        zbrkm  = DEFAULT_Zbrkm;
        */
    }
}

/* Properties file override default data:
useAssignedWts = true
#DUR FMA1,FMB1,FMZ1,FMD1,FMF1, FMA2,FMB2,FMZ2,FMD2,FMF2, FMBRK,FMGN
fmagDurParms = -.81 2.22 0 .0011 0 0 0 0 0 0 9999 
#DU2 DCOFM1,DBRKM1,DCOFM2,DBRKM2,ZCOFM,ZBRKM  #Extra dist & depth terms for Eaton
fmagDistZParms = .005 40. .0006 350. .014 10.
fmagLogA0RelationType = "EATON"
fmagUseGainCorr = true
#DUG fmagGainCorrChan = VHZ VLZ VLE VLN SHZ   #apply dur mag gain corrs to these channels
#fmagGainCorrChan = EHZ ELZ SHZ ELE ELN
fmagGainCorrChan = VHZ VLZ VLE VLN SHZ
#FCM compCorr  why aren't they in the table for the stacorrections by component?
#fmagChannelDefaultCorr = ELZ  -0.06 ELE -0.30 ELN -0.30
fmagChannelDefaultCorr = VLZ  -0.06 VLE -0.30 VLN -0.30
#FC1 acceptChan = EHZ SHZ VHZ VLZ VHE VHN 
acceptChan = EHZ SHZ ELZ EHE EHN 
*/
