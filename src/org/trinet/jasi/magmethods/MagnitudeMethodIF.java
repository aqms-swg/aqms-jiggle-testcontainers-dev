package org.trinet.jasi.magmethods;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.*;

public interface MagnitudeMethodIF extends ConfigurationSourceIF {
    /** Known methods subtype Class constants */
    public static final int  ML_MAG_METHOD  = 0;
    public static final int  MC_MAG_METHOD  = 1;
    public static final int  MD_MAG_METHOD  = 2;
    /*
    public static final String ML_MAG_BASE_CLASS_NAME = "MlMagMethod";
    public static final String MC_MAG_BASE_CLASS_NAME = "McMagMethod";
    public static final String MD_MAG_BASE_CLASS_NAME = "MdMagMethod";
    */

    public static final int MEDIAN_MAG_VALUE    = 0; // flags statistic type 
    public static final int WT_MEDIAN_MAG_VALUE = 1; // flags statistic type
    public static final int AVG_MAG_VALUE = 2; // flags statistic type

   // Return name of method
   public String getMethodName();
   // Substring describing the Magnitude subtype.
   public String getMagnitudeTypeString();
   //String describe methods result/state
   public String getResultsString();
   // Initialize default values.
   public void setDefaultProperties();

   // set derived results dependent data to an initial state (cleanup after calcs). 
   public void reset();

   public void initializeMethod(); // ? return boolean or throws IllegalStateException ?
   public boolean hasValidState(); // return true init and ready, valid state

   // Configure method parameter settings from properties, initializes.
   public void setProperties(GenericPropertyList props);
   public boolean setProperties(String propertyFileName);
   public GenericPropertyList getProperties();

   public void setCurrentSolution(Solution sol);

   // Reading (observation) data value generator for this mag method type:
    public void setWaveformFilter(WaveformFilterIF filter);
    public WaveformFilterIF getWaveformFilter();
    public boolean hasWaveformFilter();
    public Waveform filter(Waveform wfIn);
    public void setScanWaveformTimeWindow(boolean tf);
    public boolean getScanWaveformTimeWindow();

   // Apply correction to Channel magnitude calculation
    public void setGlobalCorrection(double correction);
    public double getGlobalCorrection();
    public double distanceCorrection(double distance);

   // Return cutoff (chanel rejection) distance proportional to Mag size
    public double getDistanceCutoff(double magValue);
    public double getDistanceCutoff(Magnitude aMag);
    public double getAmpDistanceCutoff(double magValue);
    public boolean isWithinDistanceCutoff(Channelable obj, double magValue);

   // Channel meets criteria appropiate for this magnitude method
    public boolean isIncludedComponent(Channelable obj);

    // Input is of type appropiate for method to calculate magnitude.
    public boolean isIncludedReadingType(MagnitudeAssocJasiReadingIF jr);
    public boolean isIncludedMagType(Magnitude mag);

   // Reject channel with source distance dist >  distKm.
    public void setMaxDistance(double distKm);
    public double getMaxDistance();

   // Include channel with source distance < distKm.
    public void setMinDistance(double distKm); 
    public double getMinDistance();

    // Maximum number of channels to include in summary Magnitude calcuation.
    public int getMaxChannels();
    public void setMaxChannels(int maxChannels);

    // Set true, channels without corrections are rejected from summary calculation. 
    public void setRequireCorrection(boolean tf);
    public boolean getRequireCorrection();
    public Class getReadingClass();

    // Reject channel magnitudes whose rms > value, from summary median Magnitude calculation.
    public void setTrimResidual(double value);
    public double getTrimResidual();

    public double calcChannelMagValue(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException;

    public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(Solution aSol, Waveform wf);
    public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(SolutionList solList, Waveform wf);

    public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(Solution aSol, Waveform wf, TimeSpan ts);
    public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(SolutionList solList, Waveform wf, TimeSpan ts);

    public List createAssocMagDataFromWaveformList(Solution aSol, List wfList);
    public List createAssocMagDataFromWaveformList(SolutionList solList, List wfList);

    public MagnitudeAssocJasiReadingIF calcChannelMag(MagnitudeAssocJasiReadingIF jr)
                         throws WrongDataTypeException;

    public Double getSummaryWt(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException;
    public Double getMagCorr(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException;
    public boolean hasMagCorr(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException;

    // If the input channel distance from source is greater than the last set
    // set cutoff distance and not less than the last set mininum distance,
    // the input attributes are set such that is not included in the summary
    // magnitude calculation. A no-op if no trim distance cutoff value was set. 
    // Returns true if input is trimmed, i.e. attributes were modified to indicate
    // rejection, false if input is acceptable, or a no-op.
    public boolean trimByDistance(MagnitudeAssocJasiReadingIF jr, double cutoff); // ?
    // the value last set for the trim residual, input attributes are set such that
    // it is not included in the summary magnitude calculation.
    // A no-op if no trim maximum distance cutoff value was set. 
    // Returns true if input is trimmed, i.e. attributes were modified to indicate
    // rejection, false if input is acceptable, or a no-op.
    public boolean trimByResidual(MagnitudeAssocJasiReadingIF jr, double residual); // ?

    public boolean trimBySummaryStatistics(Magnitude mag);
    public double getInputWeight(MagnitudeAssocJasiReadingIF jr);
    public void assignWeight(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException ;
    public boolean useAssignedWts();

    // Returns true if input reading list elements were changed based upon filter criteria.
    public boolean filterDataBeforeChannelMagCalc(Magnitude mag) throws WrongDataTypeException ;
    public boolean filterDataBeforeChannelMagCalc(List aList);
    public boolean filterDataBeforeSummaryMagCalc(Magnitude mag) throws WrongDataTypeException ;
    public boolean filterDataBeforeSummaryMagCalc(List aList);
    public boolean filterDataAfterSummaryMagCalc(Magnitude mag) throws WrongDataTypeException ;
    //Undeleted, valid data, weight can be 0.
    public List getValidChannelMagData(Magnitude mag);
    public List getValidChannelMagData(List aList);
    //Undeleted, valid data, with weight > 0.
    public List getDataForSummaryMagCalc(Magnitude mag);
    public List getDataForSummaryMagCalc(List aList);
    public List getDataUsedBySummaryMagCalc(Magnitude mag);
    public List getDataUsedBySummaryMagCalc(List aList);

    // Returns true is reading data is sufficient to calculate a channel Magnitude.
    public boolean isValid(MagnitudeAssocJasiReadingIF jr);
    // Returns true is reading data is useable for summary mag (extra constraints beyond channel mag criteria).
    public boolean isValidForSummaryMag(MagnitudeAssocJasiReadingIF jr);

    public int getMinValidReadings();
    public void setMinValidReadings(int count);

    // Flags input as virtually deleted.
    public void deleteReading(MagnitudeAssocJasiReadingIF jr);
    public void removeReading(MagnitudeAssocJasiReadingIF jr);
    // Sets input summary weight to zero, implementation could also delete reading.
    public void rejectReading(MagnitudeAssocJasiReadingIF jr);
    //
    public boolean summaryChannelType(Channelable type) ;
    public boolean filterChannelType(Channelable type) ;
    public boolean acceptChannelType(Channelable type) ;

    public boolean summaryChannelType(String type) ;
    public boolean filterChannelType(String type) ;
    public boolean acceptChannelType(String type) ;

    public String [] getSummaryChannelTypes() ;
    public String [] getFilterChannelTypes() ;
    public String [] getAcceptableChannelTypes() ;

    public void logHeader();
    public void logChannelResidual(MagnitudeAssocJasiReadingIF jr);

    public void setDeleteRejected(boolean tf) ;
    public void setIncludedReadingTypes(Object [] types);
    public ChannelDataTypeMapListIF createDefaultCalibrList() ;
    public void setDefaultCalibrationFactory(AbstractChannelCalibration calibr);
    public void setCalibrList(ChannelDataTypeMapListIF aList) ;
    public MagnitudeCalibrationIF getCalibrationFor(MagnitudeAssocJasiReadingIF jr) ;
    public MagnitudeCalibrationIF getCalibrationFor(Channelable chan, java.util.Date date) ;
    public void loadCalibrations(java.util.Date date) ;
    public void setLookUp(boolean tf);

    //public setSummaryMagValueStatType(int statFlag); 
    public int getSummaryMagValueStatType();

    // Average amps or taus at all channels in site and derive single site magnitude.
    //public double calcStaMagFromReadings(List readings);
    // returns sta mag, its weight is returned in  single element input wt array 
    public double calcStaMagFromReadings(List readings, String avgStaMagType, boolean resetChanMag2StaAvg, double[] wt);

    public boolean requireBothHoriz();

    public void setDebug(boolean tf);
    public void setVerbose(boolean tf);
}

