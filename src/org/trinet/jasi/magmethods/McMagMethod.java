package org.trinet.jasi.magmethods;
import java.util.*;
//import org.trinet.filters.*;
import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;
import org.trinet.jasi.magmethods.TN.*;
import org.trinet.jdbc.*;
import org.trinet.util.*;

public abstract class McMagMethod extends CodaMagnitudeMethod {

    public McMagMethod() {
      super();
      magSubScript = MagTypeIdIF.MC;
      methodName = "Mc";
      setMinDistance(20.0);
      setMaxDistance(600.0);
      defaultCalibrationFactory = McCodaMagCalibration.create();
    }

    // punt here for now
    public void configure(int src, String location, String section) {
        setProperties(location); // location like a filename url
    }
    public static MagnitudeMethodIF create() {
        /*
        StringBuffer sb = new StringBuffer(132);
        String suffix = JasiObject.getDefaultSuffix();
        sb.append(AbstractMagnitudeMethod.DEFAULT_BASE_PACKAGE_NAME);
        sb.append(".").append(suffix).append("McMagMethod").append(suffix);
        return AbstractMagnitudeMethod.create(sb.toString());
        */
        if (JasiObject.getDefault() == JasiFactory.TRINET)
          return new McMagMethodTN();
        else return null;
    }

    public double getDistanceCutoff(double magValue) {
      if (! props.getBoolean("disableMagnitudeDistanceCutoff") ) {
        if (magValue < 1.4) return 60.;
        if (magValue < 2.0) return 120.;
        if (magValue < 2.5) return 240.;
        if (magValue < 3.0) return 320.;
      }
      return Math.max( getMinDistance(), getMaxDistance() );
    }

    protected MagnitudeAssocJasiReadingIF createDefaultReadingType() {
        Coda coda = Coda.create();
        coda.setProcessingState(JasiProcessingConstants.STATE_AUTO_TAG); // algorithm calculated, so force AUTO 
        coda.setDurationType(CodaDurationType.A);
        return coda;
    }

    public boolean isIncludedMagType(Magnitude mag) {
      return mag.isCodaMag() && mag.getTypeQualifier().toString().equals(MagTypeIdIF.MC); // "Mc" vs. "Md" type
    }

    public boolean isIncludedReadingType(MagnitudeAssocJasiReadingIF jr) {
      if ( ! (jr instanceof Coda) ) return false;
      Coda coda = (Coda) jr;
      //System.out.println("DEBUG CodaMagnitudeMethod coda type qual: \""+coda.getTypeQualifier().toString()+"\"");
      // use the type list if it's defined else default to type Pa (codaTypei=P+durationType=a) amplitude
      return ( super.isIncludedReadingType(jr) || coda.getTypeQualifier().toString().equals("Pa"));
    }

    /* evaluate to decide whether or not to save station channel coda
    public boolean isValidCoda(org.trinet.jasi.Coda coda) {
      return super.isValidCoda(coda) ; // &&
    }
    */

    protected CodaGeneratorParms createCodaGeneratorParms(GenericPropertyList props) {
      String parmsType = props.getProperty("codaGeneratorParmsType");
      if (parmsType == null) parmsType = "null";

      if (parmsType.equalsIgnoreCase("NC")) {
        return CodaGeneratorParms.NC_MC;
      }
      else if (parmsType.equalsIgnoreCase("CI")) {
        return CodaGeneratorParms.CI_MC;
      }
      else {
        System.err.println(methodName +
          " Error null or unknown type, property codaGeneratorParmsType: " + parmsType
          + " in property filename: " + props.getFilename()
          );
        return null;
      }
    }

    protected CodaGeneratorIF createCodaGenerator(GenericPropertyList props) {
      String generatorType = props.getProperty("codaGeneratorType");
      if (generatorType == null) generatorType = "NULL";

      if (generatorType.equalsIgnoreCase("CI")) {
        return new SoCalMcCodaGenerator();
      }
      /*
      else if (generatorType.equalsIgnoreCase("NC")) {
        return new NoCalMcCodaGenerator();
      }
      */
      else {
        System.err.println(methodName +
          " Error null or unknown type, property codaGeneratorType: " + generatorType);
        return null;
      }
    }

    public void setDefaultProperties() {
      defaultAcceptChanPropString = "EHZ HHZ";
      defaultFilterChanPropString = "HHZ";
      defaultSummaryChanPropString = "EHZ";
      super.setDefaultProperties();
    }

    /*
    public double getInputWeight(MagnitudeAssocJasiReadingIF jr) {
        return 1.;
    }
    */

    public double calcStaMagFromReadings(List readings, String avgStaMagType, boolean resetChanMag2StaAvg, double[] wt) {
        return Double.NaN;
    }

    public double calcChannelMagValue(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException {
        if (! isIncludedReadingType(jr)) { // should we throw WrongDataTypeException?
          throw new WrongDataTypeException("input wrong subtype for MagnitudeMethod "+
                        methodName);
        }
        Coda coda = (Coda) jr;
        double value = coda.getAFix();
        if (Double.isNaN(value)) return -9.; // changed from -1 to -9 -aww

        Double magCorr = getMagCorr(jr);
        /* test for requireCorrection OR NOT?
        if (magCorr == null) {
          if (requireCorrection) {
            //if (debug) reportMessage("input reading has no mag correction", jr);
            throw new WrongCodaTypeException(methodName+ " calcChannelMagValue input !hasMagCorr: " +
                            getStnChlNameString(jr));
          }
        }
        */
        return value + ((magCorr == null) ? 0. : magCorr.doubleValue());
    }
}
