package org.trinet.jasi.magmethods;

import java.util.*;
import org.trinet.filters.*;
import org.trinet.jasi.*;
import org.trinet.jasi.magmethods.TN.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;

/**
 * Methods for ML (local magnitude) calculation.<p>
 *
 * Formula is defined by Richter in Elementry Seismology, pg. 340: <p>
 *
 *                        ML = log A + Cd + Cs<p>
 * Where:<br>
 *                A  = half amp, in mm from a 2,800x Wood-Anderson torsion seismometer<br>
 *                Cd = Richter distance correction<br>
 *                Cs = emperical station correction<p>
 *
 * The method can use either corrected or uncorrected Wood-Anderson amplitudes
 * (types WA, WAS, WAC and WAU)
 * @see AmpType()
 */

public abstract class MlMagMethod extends AmpMagnitudeMethod  {

    //private double summaryFilterRejectMinDist = -1.;

    protected boolean requireGain = false;
    protected boolean interpolate = false;
    protected boolean useClosestDistCorr = true;

    protected double mlrMagMin = 9;
    protected double mlrMagMax = -9;
    protected double mlrMagSlope = 1.;
    protected double mlrMagIntercept = 0.;

    //protected boolean roundUp = false;
    protected double scanSpanMaxWidth = 0.;

    protected static final String []  defaultReadingTypes = new String [] {
          AmpType.getString(AmpType.WAS),
          AmpType.getString(AmpType.WASF),
          AmpType.getString(AmpType.WA),
          AmpType.getString(AmpType.WAU),
          AmpType.getString(AmpType.WAC)
    };

/* Richter 1958 Elementary Seismology ...
Km    logA0
 0     1.4
 5     1.4
10     1.5
15     1.6
20     1.7
22     1.8
25     1.9
--
Richter 1935 BSSA paper...
25  1.65
--
30  2.10
35  2.32
40  2.43
45  2.54
50  2.63
55  2.70
60  2.77
65  2.79
70  2.83
75  2.87
80  2.90
85  2.94
90  2.96
95  2.98
100 3.00
105 3.03
110 3.08
115 3.10
120 3.12
125 3.15
130 3.19
135 3.21
140 3.23
145 3.28
150 3.29
155 3.30
160 3.32
165 3.35
170 3.38
175 3.40
180 3.43
185 3.45
190 3.47
195 3.50
200 3.53
205 3.56
210 3.59
215 3.62
220 3.65
225 3.68
230 3.70
235 3.72
240 3.74
245 3.77
250 3.79
255 3.81
260 3.83
265 3.85
270 3.88
275 3.92
280 3.94
285 3.97
290 3.98
295 4.00
300 4.02
305 4.05
310 4.08
315 4.10
320 4.12
325 4.15
330 4.17
335 4.20
340 4.22
345 4.24
350 4.26
355 4.28
360 4.30
365 4.32
370 4.34
375 4.36
380 4.38
385 4.40
390 4.42
395 4.44
400 4.46
405 4.48
410 4.50
415 4.51
420 4.52
425 4.54
430 4.56
435 4.57
440 4.59
445 4.61
450 4.62
455 4.63
460 4.64
465 4.66
470 4.68
475 4.69
480 4.70
485 4.71
490 4.72
495 4.73
500 4.74
505 4.75
510 4.76
515 4.77
520 4.78
525 4.79
530 4.80
535 4.81
540 4.82
545 4.83
550 4.84
555 4.85
560 4.86
565 4.87
570 4.88
575 4.89
580 4.90
585 4.91
590 4.92
595 4.93
600 4.94
    
    protected static double DEFAULT_CORR_DIST[] = {
        10.,  15.,  20., 22.5, 25, 27.5, 30, 32.5, 35,
        40.,  45.,  50., 55.,   60., 80.,   90., 110., 130.,
        150., 170., 190., 210., 220., 230., 250., 270., 290.,
        310., 330., 350., 380., 400., 430., 470., 510., 560.,
        601
    };

    protected static double DEFAULT_CORR[] = {
        1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1, 2.2,
        2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0, 3.1,
        3.2, 3.3, 3.4, 3.5, 3.6, 3.65,3.7, 3.8, 3.9,
        4.0, 4.1, 4.2, 4.3, 4.4, 4.5, 4.6, 4.7, 4.8,
        4.9
    };
*/

//REPLACED tables above with those  below to match EW localmag Richter.tab which uses values from table on p.342 of Elementary Seismology book -aww 2015/06/25
// NOTE 70 indices
    protected static double DEFAULT_CORR_DIST[] = {
        0.,5.,10.,15.,20.,
        25.,30.,35.,40.,45.,
        50.,55.,60.,65.,70.,
        80.,85.,90.,95.,100.,
        110.,120.,130.,140.,150.,
        160.,170.,180.,190.,200.,
        210.,220.,230.,240.,250.,
        260.,270.,280.,290.,300.,
        310.,320.,330.,340.,350.,
        360.,370.,380.,390.,400.,
        410.,420.,430.,440.,450.,
        460.,470.,480.,490.,500.,
        510.,520.,530.,540.,550.,
        560.,570.,580.,590.,600.
    };
    protected static double DEFAULT_CORR[] = {
        1.40,1.40,1.50,1.60,1.70,
        1.90,2.10,2.30,2.40,2.50,
        2.60,2.70,2.80,2.80,2.80,
        2.90,2.90,3.00,3.00,3.00,
        3.10,3.10,3.20,3.20,3.30,
        3.30,3.40,3.40,3.50,3.50,
        3.60,3.65,3.70,3.70,3.80,
        3.80,3.90,3.90,4.00,4.00,
        4.10,4.10,4.20,4.20,4.30,
        4.30,4.30,4.40,4.40,4.50,
        4.50,4.50,4.60,4.60,4.60,
        4.60,4.70,4.70,4.70,4.70,
        4.80,4.80,4.80,4.80,4.80,
        4.90,4.90,4.90,4.90,4.90
    };
    
    protected static double[] CORR_DIST = DEFAULT_CORR_DIST;
    protected static double[] CORR = DEFAULT_CORR;;

    public MlMagMethod() {
       
        // Magnitude type subscript. For example: for "Ml" this would equal "l".
        magSubScript = MagTypeIdIF.ML;
        // String describing the magnitude method
        methodName = "Ml";
        // set the default waveform filter to Synthetic Wood-Anderson
        setWaveformFilter(new WAFilter());
        // NOTE: property defaults are done by initialize method -aww
        //setMinDistance(20.0); //setMaxDistance(600.0);
        defaultCalibrationFactory = MlAmpMagCalibration.create();
    }

    public static MagnitudeMethodIF create() {
        /*
        StringBuffer sb = new StringBuffer(132);
        String suffix = JasiObject.getDefaultSuffix();
        sb.append(AbstractMagnitudeMethod.DEFAULT_BASE_PACKAGE_NAME);
        sb.append(".").append(suffix).append("MlMagMethod").append(suffix);
        return AbstractMagnitudeMethod.create(sb.toString());
        */
        if (JasiObject.getDefault() == JasiFactory.TRINET)
          return new RichterMlMagMethod();
        else return null;
    }

    public boolean getInterpolate() {
        return interpolate;
    } 

    public void setInterpolate(boolean tf) {
        interpolate = tf;
    } 

    public boolean useClosestDistCorr() {
        return useClosestDistCorr;
    } 

    public void setUseClosestDistCorr(boolean tf) {
        useClosestDistCorr = tf;
    } 

    public void initializeMethod() {
       super.initializeMethod();
       if (props == null) return; // custom properties
       // slant distance < value are rejected
       /*
       if (props.getProperty("summaryFilterRejectMinDist") != null) {
         summaryFilterRejectMinDist = props.getDouble("summaryFilterRejectMinDist");
         System.out.println("FYI - " +getMethodName()+
               " filterDataBeforeSummaryMagCalc configured to REJECT channels with slant distance <= "
               + summaryFilterRejectMinDist); 
       }
       */
       if (props.getProperty("requireGain") != null) requireGain = props.getBoolean("requireGain");
       if (props.getProperty("interpolate") != null) interpolate = props.getBoolean("interpolate");
       if (props.getProperty("useClosestDistCorr") != null) useClosestDistCorr = props.getBoolean("useClosestDistCorr");

       if (props.getProperty("mlrMagMin") != null) mlrMagMin = props.getDouble("mlrMagMin");
       if (props.getProperty("mlrMagMax") != null) mlrMagMax = props.getDouble("mlrMagMax");
       if (props.getProperty("mlrMagSlope") != null) mlrMagSlope = props.getDouble("mlrMagSlope");
       if (props.getProperty("mlrMagIntercept") != null) mlrMagIntercept = props.getDouble("mlrMagIntercept");

       WAFilter.WA_MAGNIFICATION_DEFAULT = WAFilter.WA_MAGNIFICATION_2080;
       if (props.getProperty("WAmagnification") != null) {
           if (props.getProperty("WAmagnification").equals("2800")) {
               WAFilter.WA_MAGNIFICATION_DEFAULT = WAFilter.WA_MAGNIFICATION_2800;
           }
       }
       //setWaveformFilter(new WAFilter()); // re-use one set by constructor or setDefaultProperties()
       ((WAFilter)getWaveformFilter()).setWAmagnification(WAFilter.WA_MAGNIFICATION_DEFAULT); 

       scanSpanMaxWidth = props.getDouble("scanSpanMaxWidth", 0.);

       //System.out.println("DEBUG readingTypes = " + Arrays.asList(readingTypes).toString());
       if (debug) {
         System.out.println("DEBUG "+methodName+ "     WAFilter.getWAmagnification()= " + 
            ( ( ((WAFilter)getWaveformFilter()).getWAmagnification() == WAFilter.WA_MAGNIFICATION_2800) ? "2800" : "2080" )
         );
       }
    }

    public GenericPropertyList getProperties() {
        GenericPropertyList props = super.getProperties();
        //props.setProperty("summaryFilterRejectMinDist", summaryFilterRejectMinDist); 
        props.setProperty("requireGain", requireGain);
        props.setProperty("interpolate", interpolate);
        props.setProperty("useClosestDistCorr", useClosestDistCorr);
        props.setProperty("scanSpanMaxWidth", scanSpanMaxWidth);
        // current WA magnification if specified
        props.setProperty("WAmagnification", (WAFilter.WA_MAGNIFICATION_DEFAULT == WAFilter.WA_MAGNIFICATION_2800) ? "2800" : "2080");
        props.setProperty("mlrMagMin", mlrMagMin);
        props.setProperty("mlrMagMax", mlrMagMax);
        props.setProperty("mlrMagSlope", mlrMagSlope);
        props.setProperty("mlrMagIntercept", mlrMagIntercept);
        return props;
    }

    public void setDefaultProperties() {
        // set channel lists before call to super, since super parses these
        defaultAcceptChanPropString   = "HHE HHN HLE HLN HNE HNN EHE EHN ";
        defaultSummaryChanPropString  = "HHE HHN";
        super.setDefaultProperties();
        setTrimResidual(1.0) ;
        setMinDistance(20.0);
        setMaxDistance(600.0);
        setMaxChannels(9999);
        setRequireCorrection(true);
        requireGain = false;
        interpolate = false;
        useClosestDistCorr = true;
        scanSpanMaxWidth = 0.;
        //summaryFilterRejectMinDist = -1.;

        //Reset WA filter to defaults
        //WA_MAGNIFICATION_2800 = 0 WA_MAGNIFICATION_2080 = 1;
        WAFilter.WA_MAGNIFICATION_DEFAULT = WAFilter.WA_MAGNIFICATION_2080;
        if (! (getWaveformFilter() instanceof WAFilter)) setWaveformFilter(new WAFilter()); // reset to default

        setIncludedReadingTypes(defaultReadingTypes);

        CORR = DEFAULT_CORR;
        CORR_DIST = DEFAULT_CORR_DIST;

        mlrMagMin = 9;
        mlrMagMax = -9;
        mlrMagSlope = 1.;
        mlrMagIntercept = 0.;

    }

/*
    public boolean filterDataBeforeSummaryMagCalc(List readingList) {
        boolean changed = super.filterDataBeforeSummaryMagCalc(readingList);
        if (summaryFilterRejectMinDist < 0) return changed;
        int size = readingList.size();
        MagnitudeAssocJasiReadingIF jr = null;
        double dist = 0.;
          //NOTE: to save reading data back to db (rejectReading)
          // to NOT save reading data back to db (deleteReading)
        for (int idx = 0; idx < size; idx++) {
            jr = (MagnitudeAssocJasiReadingIF) readingList.get(idx);
            // channel slant distance < property value are rejected
            if (jr.isDeleted() || (jr.getDistance() > summaryFilterRejectMinDist)) continue;
            if (jr.getInWgt() > 0) {
              rejectReading(jr);
              changed |= true;
            }
        }
        return changed;
    }
*/

    protected double getAmpValue(Amplitude amp) throws WrongAmpTypeException {
      double ampval = 0.0;
      if (amp.units == Units.MM) { // make sure units are mm
        ampval = amp.value.doubleValue();
      } else if (amp.units == Units.CM) {
        ampval = amp.value.doubleValue() * 10.0;        // cm -> mm
      } else if (amp.units == Units.M) {
        ampval = amp.value.doubleValue() * 1000.0;      // m -> mm <- bug fix 02/22/2006 P. Lombard
      } else {
        throw new WrongAmpTypeException(
            "Wrong Amp units for MagnitudeMethod "+methodName+
            " : " +amp.toNeatString() // +Units.getString(amp.units)
        );
      }
      if (! amp.halfAmp) ampval /= 2.0; // make sure is half-amp
      return ampval;
    }

    public Double getSummaryWt(MagnitudeAssocJasiReadingIF jr) {
        //Assume "summary_wt" column in Channelmap_Ampparms table/view 
        MagnitudeCalibrationIF calibrData = getCalibrationFor(jr);
        Double summaryWt = (calibrData != null) ? calibrData.getSummaryWt(jr) : null;
        if (debug)
            System.out.println("DEBUG "+methodName+ "     " + getStnChlNameString(jr) + " summaryWt :" + summaryWt);
        // should the ML default wt be 0. or 1. for missing db calibration data ?
        return (summaryWt == null) ? Double.valueOf(1.) : summaryWt;

        // Default is to full-weight channel magnitude value
        //return Double.valueOf(1.); // replaced with above logic 06/28/2006 -aww
    }

    public Double getMagCorr(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException {
        if (! isIncludedReadingType(jr)) { // should we throw WrongDataTypeException?
          throw new WrongDataTypeException(
            "getMagCorr input reading wrong type for MagnitudeMethod "+ methodName +
            " : " + jr.toNeatString()
          );
        }

        if (! useMagCorr) return Double.valueOf(0.);

        Amplitude amp = (Amplitude) jr;
        if (amp.isCorrected())  return Double.valueOf(0.);
        // Above return means shouldn't get below here for WAC type

        MagnitudeCalibrationIF calibrData = getCalibrationFor(jr);
        return (calibrData != null) ? calibrData.getMagCorr(jr) : null;
    }

    /*
    // Average amps at all channels at site, then derive single site magnitude.
    // ? Do you average the corrections ? Only works if all channels have same correction?
    //public double calcStaMagFromReadings(List readings, boolean resetCorr) { // flag to reset corr here?
    //public double calcStaMagFromReadings(List readings, double[] corr) {  // ? or return avg corr in array to caller?
    public double calcStaMagFromReadings(List readings) {

        if (readings == null || readings.size() == 0) return Double.NaN;

        double value = 0.;
        double sum = 0.;
        int cnt = 0;
        double corr = 0.;
        Amplitude amp = null;

        Format ff = (debug) ? new Format("%5.2f") : null;

        for (int idx=0; idx<readings.size(); idx++) {
            amp = (Amplitude) readings.get(idx);
            try {
                value = getAmpValue(amp);
            }
            catch (WrongDataTypeException ex) {
                ex.printStackTrace();
            }
            if (!Double.isNaN(value)) {
                sum += value;
                corr += amp.getChannelMagCorrUsed();
                cnt++;
            }
        }
        corr /= cnt; // average correction
        value = sum/cnt; // average amplitude

        double mag = calcChannelMagValue(amp.getHorizontalDistance(), value, corr);

        if (debug) {
            System.out.println("MlMagMethod calcStaMagFromReadings " + amp.getChannelObj().getNet() + "."  + amp.getChannelObj().getSta() +
                " staMag: " + mag + " chan cnt: " + cnt + " avgAmp: " + ff.form(value) + " avgCorr: " + ff.form(corr));
        }

        //
        //if (resetCorr) {
        //  for (int idx=0; idx<readings.size(); idx++) {
        //    amp = (Amplitude) readings.get(idx);
        //    if (amp.hasChannelMagValue()) amp.setChannelMagCorrUsed(corr);
        //  }
        //}
        //

        return mag;
    }
    */

    public double calcStaMagFromReadings(List readings, String avgStaMagType, boolean resetChanMag2StaAvg, double[] wt) {

        int avgType = 0;
        if (avgStaMagType.equalsIgnoreCase("amps")) avgType = 1;
        else if (avgStaMagType.equalsIgnoreCase("mags")) avgType = 2;
        if (avgType == 0) {
            System.err.println(methodName+" Invalid input arg avgStaMagType: " +avgStaMagType+ ", should be \"amps\" or \"mags\"");
            return Double.NaN;
        }

        double mag = Double.NaN;
        double wgt = 0.;
        double corr = 0.;
        double sum = 0.;
        int cnt = 0;

        Amplitude amp = null;

        Format ff = (debug) ? new Format("%5.2f") : null;

        if (avgType == 1) { // by amps

            double value = 0.; // for half amp mm value

            for (int idx=0; idx<readings.size(); idx++) {

                amp = (Amplitude) readings.get(idx);
                if (amp.isReject()) continue; // skip

                try {
                    value = getAmpValue(amp);
                }
                catch (WrongDataTypeException ex) {
                    ex.printStackTrace();
                }

                if (Double.isNaN(value)) continue; // skip, invalid amp value

                sum += value;
                wgt += useAssignedWts() ? amp.getQuality() : 1.;
                corr += amp.getChannelMagCorrUsed();

                cnt++;
                if (cnt > 2) {
                    //System.out.println("WARNING? MlMagMethod calcStaMagFromReadings >2 sta channels:\n" + readings);
                    System.out.println("WARNING? MlMagMethod calcStaMagFromReadings >2 sta channels: " + getStnChlNameString(amp));
                }
            }

            // averaged correction, wt, and amplitude valeu
            corr /= cnt;
            wgt /= cnt;
            value = sum/cnt; // should be half amp mm

            // calculate sta mag as channel mag using average amp and corr
            mag = calcChannelMagValue(amp.getHorizontalDistance(), value, corr);

            if (debug) {
                System.out.println("MlMagMethod calcStaMagFromReadings cnt: " +cnt+
                                   " average by : "+ avgStaMagType +
                                   " staMag: "+ ff.form(mag) +" corr: "+ ff.form(corr) +" wt: "+ ff.form(wgt) +
                                   " avgAmp: "+ ff.form(value) +
                                   " "+ amp.getChannelObj().getNet() +"."+ amp.getChannelObj().getSta()
                                  );
            }
        }
        else { // average channel mags
            //Assumes existing channel mags are already calculated with channel corrections applied
            for (int idx=0; idx<readings.size(); idx++) {

                amp = (Amplitude) readings.get(idx);
                if (amp.isReject() || !amp.hasChannelMagValue()) continue;

                sum += amp.getChannelMagValue();
                corr += amp.getChannelMagCorrUsed();
                wgt += useAssignedWts() ? amp.getQuality() : 1.;

                cnt++;
                if (cnt > 2) {
                    //System.out.println("WARNING? MlMagMethod calcStaMagFromReadings >2 sta channels:\n" + readings);
                    System.out.println("WARNING? MlMagMethod calcStaMagFromReadings >2 sta channels:\n" + getStnChlNameString(amp));
                }
            }

            corr /= cnt; // average correction
            wgt /= cnt; // average wts

            mag = sum/cnt; // average amplitude

            if (debug) {
                System.out.println("DEBUG " + methodName + " calcStaMagFromReadings cnt: "+ cnt +
                                   " average by : "+ avgStaMagType +
                                   " staMag: "+ ff.form(mag) +" corr: "+ ff.form(corr) +" wt: "+ ff.form(wgt)+
                                   " "+ amp.getChannelObj().getNet() +"."+ amp.getChannelObj().getSta()
                                  );
            }

        }


        // Reset summary output wt for all sta channels 
        for (int idx=0; idx<readings.size(); idx++) {
            amp = (Amplitude) readings.get(idx);
            amp.setWeightUsed(wgt);
            if (resetChanMag2StaAvg) { // reset original channel mags, and corr to the sta average
                amp.setChannelMagValue(mag);
                amp.setChannelMagCorrUsed(corr);
            }
        }

        //Return sta wgt in input array:
        if (wt != null) wt[0] = wgt;

        return mag;
    }

    /**
     * Return the Local Magnitude (ML) as defined by Richter in Elementry
     * Seismology, pg. 340. Retrieves from the input Amplitude the station's
     * epicentral distance in km and its half peak-to-peak amplitude equivalent to
     * a Wood-Anderson torsion seismomemter with 2,800 magnification measured in mm. */
    public double calcChannelMagValue(Amplitude amp) throws WrongDataTypeException {
      if (! isIncludedReadingType(amp) ) {
          throw new WrongAmpTypeException(
            "Wrong Amp type for MagnitudeMethod " +methodName+
            " : " +amp.toNeatString() // +AmpType.getString(amp.type)
          );
      }
      if (amp.isClipped()) { // should we throw WrongDataTypeException?
          throw new WrongAmpTypeException(
            "Amp isClipped for MagnitudeMethod "+methodName+
            " : " +amp.toNeatString()
          );
      }
      //to discriminate for small events with negative channel corrections changed to -9.
      double value = -9.; // changed 0. to -9 to make it obvious to user - aww 8/12/2004
      try {
        Double correction = getMagCorr(amp);
        value = calcChannelMagValue(
                        //amp.getDistance(), // don't use slant distance - aww 06/11/2004
                        amp.getHorizontalDistance(), // aww 06/11/2004
                        getAmpValue(amp),
                        (correction == null)? 0. : correction.doubleValue()
                );
      }
      catch (WrongDataTypeException ex) {
        ex.printStackTrace();
      }
      return value;
    }

    /**
     * Return the Local Magnitude (ML) as defined by Richter in Elementry
     * Seismology, pg. 340. Epicentral distance is given in km, amplitude is
     * half peak-to-peak amplitude of a Wood-Anderson torsion seismometer with
     * 2,800 magnification measured in positive mm's.
     * The station correction given as the third
     * argument will be added to the magnitude. */

    public double calcChannelMagValue(double distance,
                            double mmHalfAmp,
                            double staticCorrection) {

      mmHalfAmp = Math.abs(mmHalfAmp);      // make sure its +positive

      if (debug) {
        Format ff = new Format("%7.3f");
        System.out.println(
            "DEBUG " + methodName + "     mmHalfAmp, log(mmHalfAmp), dist, distCorr, staticCorr: "+
            ff.form(mmHalfAmp)+ " "+ ff.form(MathTN.log10(mmHalfAmp)) +" " + ff.form(distance) +" " +
            ff.form(distanceCorrection(distance)) +" " + ff.form(staticCorrection));
      }


      double value = MathTN.log10(mmHalfAmp) +         // calculated mag
             distanceCorrection(distance) + // distance correction
             staticCorrection +             // channel correction
             getGlobalCorrection() ;        // global correction

      // use this with distanceCorrection method printf
      //System.out.printf(" =>chanMag: %4.2f\n", value);

      return value;
    }

    /**
     * Return the Local Magnitude (ML) as defined by Richter in Elementry
     * Seismology, pg. 340. Epicentral distance is given in km, amplitude is
     * half peak-to-peak amplitude of a Wood-Anderson torsion seismomemter with
     * 2,800 magnification measured in mm. */
    public double calcChannelMagValue(double distance, double mmHalfAmp) {
      return calcChannelMagValue(distance, mmHalfAmp, 0.0);
    }

    /**
     * Return the Richter distance correction. (from Richter, Table 22-1, pg. 342)
     * Correction should be added to magnitude calculated by ML.value().
     * Input distance is epicentral distance to station in kilometers. 
     */
    public double distanceCorrection(double distance) {

      if (distance < 0.) throw new IllegalArgumentException("input distance < 0");

      double value = 0.;

      if (distance < CORR_DIST[0]) {
          value = CORR[0];
          return value;
      }
      else {
        for (int i=1; i<CORR_DIST.length; i++) {
            if (CORR_DIST[i] > distance) {
                if (interpolate) {
                   value =  CORR[i-1] + ((distance - CORR_DIST[i-1])*(CORR[i]-CORR[i-1]))/(CORR_DIST[i]-CORR_DIST[i-1]);
                }
                else {
                   // modified 2015/05/14 -aww
                   // If not interpolated, the CORR index to return depends on the alignment of corr and distance array values.
                   // The corr arrays values appear to be shifted right when not starting at zero, e.g. the corr for 10 km is mapped
                   // to next distance element at 15 km etc. thus method should return corr[i] but for case where arrays values are
                   // not not shifted as when starting distance at 0 km, for idx 0 you should return corr[i-1] value.
                   //
                   if ( ! useClosestDistCorr ) {
                     //value = (roundUp) ? CORR[i] : CORR[i-1];
                     // To preserve behavior of shifted Richter defaults would do this:
                     //value = CORR[i]; // shifted, used to return corr[i] value for dist[i] > distance
                     value = CORR[i-1]; // for unshifted, one-to-one distance corr array mapping (using table from 1958 Richter).
                   }
                   else {
                     //closest array element 1-to-1:
                     value = ((CORR_DIST[i] - distance) < (distance - CORR_DIST[i-1]) ) ? CORR[i] : CORR[i-1];
                   }
                }
                //System.out.printf(" chanKm:%6.2f array i: %02d corrDist,corr[i]:%6.2f,%4.2f [i-1]:%6.2f,%4.2f =>chanDistCorr: %4.2f",
                //           distance,i,CORR_DIST[i],CORR[i],CORR_DIST[i-1],CORR[i-1],value);
                return value; // interpolated, rounded, or lesser corr interval value
            }
        }
      }
      // return maximum distance correction, previously would return 0. -aww 2015/05/14
      value = CORR[CORR.length-1]; 

      return value;

    }

    /**
     * Return the distance cutoff appropriate for the method. Amplitudes from
     * stations father then this will not be included in the summary magnitude.
     * Because it depends on the magnitude the cutoff can only be applied in a
     * second pass through the amplitude readings. In the
     * local (Richter) magnitude scheme the distance cutoff is simply 600 km
     * epicentral distance or the value set by the user.
     * @see #setMinDistance(double)
     * @see #setMaxDistance(double)
     */
    public double getDistanceCutoff(double magValue) {
      double dist = Math.max( getMinDistance(), 600.0 );
      return Math.min(dist, getMaxDistance() );
    }

    public double getAmpDistanceCutoff(double magValue) {
        return getDistanceCutoff(magValue);
    }

    /* Removed 11/08/2007, instead use parent strings initialized in constructor or by property -aww
    public boolean isIncludedReadingType(MagnitudeAssocJasiReadingIF jr) {
      if ( ! (jr instanceof Amplitude) ) return false;
      Amplitude amp = (Amplitude) jr;
      // reject clipped amps
      return (amp.type==AmpType.WAS || amp.type==AmpType.WASF || amp.type==AmpType.WA ||
              amp.type==AmpType.WAU || amp.type==AmpType.WAC) ;
    }
    */

    public Magnitude createMlrMag(Magnitude mag) {
        if (!mag.subScript.equalsValue(MagTypeIdIF.ML)) return null;

        double magval = mag.value.doubleValue();
        if (Double.isNaN(magval)) return null;

        if ( magval < mlrMagMin || magval > mlrMagMax ) return null;

        Magnitude mlrmag = (Magnitude) mag.clone();
        magval = getMlrMag(magval);
        mag.adjValue.setValue(magval);
        mlrmag.value.setValue(magval);
        mlrmag.adjValue.setNull(true);
        mlrmag.usedStations.setValue(mag.getStationsUsed());
        mlrmag.usedChnls.setValue(mag.getReadingsUsed());
        mlrmag.subScript.setValue(MagTypeIdIF.MLR);
        //mlrmag.clearDataLists(false);  // do not clear list, keep Ml amps for db save for Mlr - aww 2016/02/18
        mlrmag.ampList.assignAllTo(mlrmag);

        return mlrmag;
    }

    public double getMlrMag(double mag) {
        if (Double.isNaN(mag)) return Double.NaN;
        return (mlrMagSlope * mag) + mlrMagIntercept;
    }

    public boolean trimBySummaryStatistics(Magnitude mag) {
        if (! sumMagStatTrim) return false;
        List aList = getDataUsedBySummaryMagCalc(mag);
        return chauvenetTrim(aList);
    }

    // AWW - 01/18/2008 can disable code below when appchannels are populated and AMM uses the collapse method 
    public List getDataForSummaryMagCalc(List dataList) {

        List aList = super.getDataForSummaryMagCalc(dataList);
        if (aList == null || aList.size() <= 1) return aList;

        // assumes that summaryChanIdMap2 is empty when Map1 is empty 
        if ( (summaryChanIdMap1 != null) && ! summaryChanIdMap1.isEmpty() ) return aList;  // aww 2008/01/22
        // For HH/HN testing, amp ratios, leave all channel types:
        if (!cullRedundantReadings) return aList;

        // Otherwise use standard templates to cull low gains from list
        ChannelableList chList2 = new ChannelableList(aList); // stuff list into channelable list
        ChannelableList chList1 = new ChannelableList(chList2.size()); // make empty list to return
        Amplitude amp = null;
        for (int i = 0; i < chList2.size(); i++) { // loop over list adding all valid HH to list
            amp = (Amplitude) chList2.get(i);
            if (amp.getChannelObj().getSeedchan().substring(1,2).equals("H") ) {
                chList1.add(amp);
            }
        }
        for (int i = 0; i < chList1.size(); i++) { // remove all HH amps from original list, assume only low gains remain?
            chList2.remove( chList1.get(i) ); 
        }

        // Now loop through HH primary amps, culling secondary list if not empty
        if (chList2.size() > 0 ) {
          int count = chList1.size();
          Amplitude amp2 = null;
          for (int ii = 0; ii < count; ii++) {
              amp = (Amplitude) chList1.get(ii);
              // if current amp is valid primary reading, delete channel with same net,sta, & orientation from secondary list
              if (isValidForSummaryMag(amp)) { // valid for summary, so cull out the low gain channels replace by below for test
                for (int jj = 0; jj < chList2.size(); jj++) {
                  amp2 = (Amplitude) chList2.get(jj);
                  if ( amp.getChannelObj().sameStationAs(amp2.getChannelObj().getChannelId()) &&
                       amp.getChannelObj().sameOrientationAs(amp2.getChannelObj().getChannelId()) ) {
                         chList2.remove(jj); // delete it from secondary processing list
                         if (debug ) System.out.println("DEBUG getDataForSummaryMagCalc, removed: " + amp2.getChannelObj().getChannelId().toString());
                  }
                }
              }
              else {
                if (debug) System.out.println("DEBUG getDataForSummaryMagCalc, amp not valid for summary: " + amp.getChannelObj().getChannelId().toString());
              }
              if (chList2.size() == 0) break;
            }
            // Now if secondary list is not empty, add all in this (low-gains) to primary list readings (high gains)
            if (chList2.size() > 0) chList1.addAll(chList2);
          }
          return chList1;
    }
    //

    public boolean isValid(MagnitudeAssocJasiReadingIF jr) {
        if (! (jr instanceof Amplitude)) return false;
        // To discriminate subtype add check: //  && isIncludedReadingType(jr);
        boolean onScale = ((Amplitude) jr).isOnScale();
        if (debug) 
          System.out.println("DEBUG " + methodName + " isValid() for " +getStnChlNameString(jr) + " jr.isOnScale() :" + onScale );
        return onScale;
    }

    protected boolean hasRequiredChannelData(MagnitudeAssocJasiReadingIF jr) {
        if (! super.hasRequiredChannelData(jr)) return false;
        if (! requireGain) return true;

        Channel jrChan = jr.getChannelObj();
        java.util.Date date = jr.getDateTime();
        boolean isGainNull = jrChan.getGain(date, doLookUp).isNull(); // added lookup 2010/08/19 -aww
        // Or Use static setting of "dbLookup" in channel class, below removed 11/30/2005 -aww
        //if (isGainNull && doLookUp) {
          // use jrChan.loadAssocData(date) to lookup all data for channel including corrections
          //isGainNull = (jrChan.loadResponse(date)) ? jrChan.getGain(date).isNull() : true;
        //}
        if (debug) {
          System.out.print("DEBUG " + methodName + " hasRequiredChannelData() chan:" + jrChan);
          System.out.print(" distance, slantDistance:"+jrChan.getDistance() + ", " + jr.getSlantDistance());
          System.out.print(" for date:"+date + " doLookUp: " + doLookUp);
          System.out.println(" gain? "+ jrChan.getGain(date, doLookUp));
        }
        return ! isGainNull;
    }

    public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(Solution aSol, Waveform wf, TimeSpan scanSpan) {
        Amplitude amp = (Amplitude) super.createAssocMagDataFromWaveform(aSol, wf, scanSpan);
        if (amp != null) amp.setType(AmpType.WAS);
        return amp;
    }

/*
  public static final class Tester {
      public static void main(String args[]) {
        MlMagMethod ml = (MlMagMethod) MlMagMethod.create();
        ml.setInterpolate(false);
        //ml.roundUp = true;
        ml.setUseClosestDistCorr(false);
        //System.out.printf("-- ceiling to LARGER corr[i] (OLD BEHAVIOR) --%n");
        //System.out.printf("RichterML  2.701 =%6.3f%n", ml.calcChannelMagValue(  5.0, 20.000));
        //System.out.printf("RichterML  0.797 =%6.3f%n", ml.calcChannelMagValue( 23.0,  0.125));
        //System.out.printf("RichterML  3.377 =%6.3f%n", ml.calcChannelMagValue( 32.0, 18.945));
        //System.out.printf("RichterML  3.676 =%6.3f%n", ml.calcChannelMagValue( 43.0, 15.000));
        //System.out.printf("RichterML  3.777 =%6.3f%n", ml.calcChannelMagValue( 77.0,  9.491));
        //System.out.printf("RichterML  3.000 =%6.3f%n", ml.calcChannelMagValue( 100.,  1.));
        //System.out.printf("RichterML  4.157 =%6.3f%n", ml.calcChannelMagValue(308.0,  1.437));
        //System.out.printf("RichterML  4.070 =%6.3f%n", ml.calcChannelMagValue(600.0,  0.148));
        //ml.roundUp = false;
        System.out.printf("-- floor to SMALLER corr[i-1] (NEW BEHAVIOR) --%n");
        System.out.printf("RichterML  2.701 =%6.3f%n", ml.calcChannelMagValue(  5.0, 20.000));
        System.out.printf("RichterML  0.797 =%6.3f%n", ml.calcChannelMagValue( 23.0,  0.125));
        System.out.printf("RichterML  3.377 =%6.3f%n", ml.calcChannelMagValue( 32.0, 18.945));
        System.out.printf("RichterML  3.576 =%6.3f%n", ml.calcChannelMagValue( 43.0, 15.000));
        System.out.printf("RichterML  3.777 =%6.3f%n", ml.calcChannelMagValue( 77.0,  9.491));
        System.out.printf("RichterML  3.000 =%6.3f%n", ml.calcChannelMagValue( 100.,  1.));
        System.out.printf("RichterML  4.157 =%6.3f%n", ml.calcChannelMagValue(308.0,  1.437));
        System.out.printf("RichterML  4.070 =%6.3f%n", ml.calcChannelMagValue(600.0,  0.148));
        System.out.printf("-- CLOSEST corr (LOCALMAG BEHAVIOR) --%n");
        ml.setUseClosestDistCorr(true);
        System.out.printf("RichterML  2.701 =%6.3f%n", ml.calcChannelMagValue(  5.0, 20.000));
        System.out.printf("RichterML  0.997 =%6.3f%n", ml.calcChannelMagValue( 23.0,  0.125));
        System.out.printf("RichterML  3.377 =%6.3f%n", ml.calcChannelMagValue( 32.0, 18.945));
        System.out.printf("RichterML  3.676 =%6.3f%n", ml.calcChannelMagValue( 43.0, 15.000));
        System.out.printf("RichterML  3.877 =%6.3f%n", ml.calcChannelMagValue( 77.0,  9.491));
        System.out.printf("RichterML  3.000 =%6.3f%n", ml.calcChannelMagValue( 100.,  1.));
        System.out.printf("RichterML  4.257 =%6.3f%n", ml.calcChannelMagValue(308.0,  1.437));
        System.out.printf("RichterML  4.070 =%6.3f%n", ml.calcChannelMagValue(600.0,  0.148));
        ml.setInterpolate(true);
        // Need new interp values below since default array changed
        System.out.printf("%n-- INTERPOLATE=TRUE --%n");
        System.out.printf("RichterML  2.701 =%6.3f%n", ml.calcChannelMagValue(  5.0, 20.000));
        System.out.printf("RichterML  0.917 =%6.3f%n", ml.calcChannelMagValue( 23.0,  0.125));
        System.out.printf("RichterML  3.457 =%6.3f%n", ml.calcChannelMagValue( 32.0, 18.945));
        System.out.printf("RichterML  3.636 =%6.3f%n", ml.calcChannelMagValue( 43.0, 15.000));
        System.out.printf("RichterML  3.847 =%6.3f%n", ml.calcChannelMagValue( 77.0,  9.491));
        System.out.printf("RichterML  3.000 =%6.3f%n", ml.calcChannelMagValue( 100.,  1.));
        System.out.printf("RichterML  4.237 =%6.3f%n", ml.calcChannelMagValue(308.0,  1.437));
        System.out.printf("RichterML  4.068 =%6.3f%n", ml.calcChannelMagValue(600.0,  0.148));
      }
  }
*/
} // ML
