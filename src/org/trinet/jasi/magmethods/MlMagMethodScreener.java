package org.trinet.jasi.magmethods;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.pcs.PcsProcessingController;
import org.trinet.util.Concatenate;
import org.trinet.util.DateRange;
import org.trinet.util.Format;
import org.trinet.util.TimeSpan;

/**
 * Methods to screen for existance ML calculation channel Corr, LatLonZ, and Gain requirements.<p>
 */
public class MlMagMethodScreener extends org.trinet.jasi.magmethods.MlMagMethod  {
    Format  f = new Format("%8.2f");
    Format  d = new Format("%15d");

    public MlMagMethodScreener() {
        super();
    }

    public int getSummaryMagValueStatType() { return MEDIAN_MAG_VALUE; }

    public void configure(int src, String location, String section) {
        setProperties(location); // location like a filename url
    }

    protected boolean hasRequiredChannelData(MagnitudeAssocJasiReadingIF jr) {
      Channel jrChan = jr.getChannelObj();
      boolean status = true;
      try {
        java.util.Date date = jr.getDateTime(); // reading observation time

        if ( ! jrChan.hasLatLonZ() ) {
          if (doLookUp) jr.setChannelObjData(jrChan.lookUp(jrChan, date));
        }

        double dist = jrChan.getHorizontalDistance();
        if ( (dist == Channel.NULL_DIST) || (dist == 0.0) || Double.isNaN(dist) ) {
          jrChan.calcDistance(jr.getAssociatedSolution());
        }

        // Set requireGain property to "true" for testing those epochs with waveform timeseries data
        boolean isGainNull = (requireGain) ?  jrChan.getGain(date).isNull() : false;
        boolean hasMagCorr = hasMagCorr(jr);

        if (! jrChan.hasLatLonZ() || isGainNull || ! hasMagCorr) {
          Solution sol = jr.getAssociatedSolution();
          long eid =  (sol != null) ? sol.getId().longValue() : 0l;
          StringBuffer sb = new StringBuffer(132);
          sb.append("Missing metadata: ");
          sb.append(" km: ").append(f.form(jrChan.getHorizontalDistance()));
          sb.append(" slant: ").append(f.form(jr.getSlantDistance()));
          sb.append(" lat,lon?: ");
          Concatenate.rightJustify(sb, String.valueOf(jrChan.hasLatLonZ()).toUpperCase(), 5);
          sb.append(" corr?: ");
          Concatenate.rightJustify(sb, String.valueOf(hasMagCorr).toUpperCase(), 5);
          sb.append(" gain?: ");
          Concatenate.rightJustify(sb, String.valueOf(! isGainNull).toUpperCase(), 5);
          sb.append( " event: ").append(d.form(eid));
          sb.append(" date: ").append(date).append(" ");
          sb.append(jrChan.toDelimitedSeedNameString());
          System.out.println(sb.toString());

          ChannelName cn = jrChan.getChannelName();
          DateRange dr = (DateRange) PcsProcessingController.cdm.get(cn);

          if (dr == null) {
              dr = new DateRange(date, date);
              PcsProcessingController.cdm.put(cn, dr);
          }
          else dr.include(date);

          status = false;
        }
      }
      catch (WrongDataTypeException ex) {
        System.err.println(jrChan.toDelimitedSeedNameString() + " " + ex.getMessage());
        status = false;
      }
      return status;
    }

    public List filterWaveformList(List wfList) {
        Waveform wf[] = (Waveform []) wfList.toArray(new Waveform[wfList.size()]);
        Arrays.sort(wf, new ChannelDistanceSorter());
        // enforce maxChannels after ascending distance sort
        int count = wf.length;
        if (count > getMaxChannels()) {
            count = getMaxChannels();
            if (debug) System.out.println ("DEBUG "+methodName+"Will trim to maxChannels limit = "+count);
        }

        ArrayList aList = new ArrayList(count);

        // Override has no filter by distance
        for (int i=0; i<count; i++) {
            if (isIncludedComponent(wf[i]) ) aList.add(wf[i]);
        }
        return aList;
    }

    public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(Solution aSol, Waveform wf, TimeSpan scanSpan) {
       if (wf == null) return null;
       currentSol = aSol; // for tracking purposes
       Amplitude amp = Amplitude.create();
       amp.setChannelObj(wf.getChannelObj());
       amp.setType(AmpType.WAS);
       amp.setUnits(Units.CM);
       amp.assign(aSol);
       amp.setTime(aSol.getTime());
       amp.setValue(.00001, Units.CM);
       return ( hasRequiredChannelData(amp)) ? amp : null;
    }

    int goodCount = 0;
    int badCount = 0;
    public List createAssocMagDataFromWaveformList(Solution aSol, List wfList) {
        goodCount = 0;
        badCount = 0;
        if (! hasValidState() || aSol == null) {
          System.err.println(getClass().getName() + " has invalid initial state, can't do waveform mag calculations!");
          return new ArrayList(0);
        }

        if (debug) System.out.println("createAssocMagDataFromWaveformList input wflist.size() = "+ wfList.size());
        if (wfList.isEmpty()) return new ArrayList(0);
        currentSol = aSol;
        List filteredWfList = filterWaveformList(wfList);
        int count = filteredWfList.size();
        if (debug) System.out.println("createAssocMagDataFromWaveformList filtered wflist.size() = "+ wfList.size());
        ArrayList aList = new ArrayList(count);
        MagnitudeAssocJasiReadingIF jr = null;
        for (int i=0; i<count; i++) {
            jr = createAssocMagDataFromWaveform(aSol, (Waveform) filteredWfList.get(i));
            if (jr != null && jr.getChannelMag() != null) {
                //aList.add(jr);
                goodCount++;
            }
            else badCount++;
        }

        System.out.println("Event " + aSol.getId().longValue() + " has parametric data for: " + goodCount + ", but missing for: " + badCount);

        return aList;
    }

} // MLscreener
