package org.trinet.jasi.magmethods.TN;

import org.trinet.jasi.*;
import org.trinet.util.*;

public class MungSoCalMhMagMethod extends MungSoCalMlMagMethod {

  public MungSoCalMhMagMethod() {
      super();
      magSubScript = "h";
      methodName = "MungSoCalMh";
  }

  public boolean isIncludedReadingType(MagnitudeAssocJasiReadingIF jr) {
      if ( ! (jr instanceof Amplitude) ) return false;
      Amplitude amp = (Amplitude) jr;
      int type = amp.getType();
      return (type == AmpType.HEL || // used to be PGD aww b4 5/03
              type == AmpType.WA  ||
              type == AmpType.WAS ||
              type == AmpType.WAU ||
              type == AmpType.WAC
             );
  }

  public Double getMagCorr(MagnitudeAssocJasiReadingIF jr) throws WrongDataTypeException {

      if (! useMagCorr) return Double.valueOf(0.);

      Amplitude amp = (Amplitude) jr; 
      double magCorr = Double.NaN;
      int type = amp.getType();
      if (amp.isCorrected())  magCorr = 0.; //type Wood-Anderson corrected
      else if (
        type == AmpType.HEL ||
        type == AmpType.WA  ||
        type == AmpType.WAS ||
        type == AmpType.WAU
      ) {
        MagnitudeCalibrationIF calibr = null;
        calibr = getCalibrationFor(amp);
        if (calibr != null) {
           magCorr = calibr.getCorr().doubleValue();
        // failover use ML corr if MH undefined null or Double.NaN, recover it from reading 
        } 
        if (calibr == null || Double.isNaN(magCorr)) {
          calibr = (MagnitudeCalibrationIF) amp.getChannelObj().getCorrection(amp.getDateTime(), CorrTypeIdIF.ML);
          if (calibr != null) magCorr = calibr.getCorr().doubleValue();
        }
      }
      else {
        throw new WrongAmpTypeException("MH: Wrong amplitude type: "+ AmpType.getString(type));
      }
      return (Double.isNaN(magCorr) ) ? null : Double.valueOf(magCorr);
  }
} 
