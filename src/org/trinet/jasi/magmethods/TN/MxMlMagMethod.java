package org.trinet.jasi.magmethods.TN;

import java.util.*;
//import org.trinet.filters.*;
import org.trinet.jasi.*;
import org.trinet.jasi.magmethods.*;
import org.trinet.util.GenericPropertyList;
import org.trinet.util.LeapSeconds;
import org.trinet.util.TimeSpan;

public class MxMlMagMethod extends CISNmlMagMethod2 {

    // Below constants are from Hypoinverse2000 HYMAG.FOR subroutine which may differ from manual documentation for LOGR
    //private static final double Ut = 0.04883; // mm/count EW digitizer
    //private static final double Dd = 40.; // develocorder mm/v

    private static final double [] LOGF = {
      -0.795880017, -0.698970004, -0.602059991, -0.494850022, -0.397940009, -0.301029996, -0.200659451, -0.102372909, 0.,
      0.100370545, 0.201397124, 0.301029996, 0.399673721, 0.499687083, 0.599883072, 0.699837726, 0.800029359, 0.899820502, 1.,
      1.100370545, 1.201397124, 1.301029996, 1.399673721, 1.499687083, 1.599883072, 1.699837726,
    };

    private static final double [] LOGR = {
        0.288,0.432,0.561,0.68,0.786,0.891,0.983,1.066,1.138,
        1.205,1.276,1.355,1.443,1.535,1.63,1.726,1.822,1.916,2.007,
        2.09,2.145,2.099,1.878,1.546,1.172,0.771
    };

    private static double magCorrEHZ = 0.;
    private static double magCorrEHN = 0.;
    private static double magCorrEHE = 0.;
    private static double magCorrELZ = 0.;
    private static double magCorrELN = 0.;
    private static double magCorrELE = 0.;

    public MxMlMagMethod() {
        super();
        methodName = "MxMl";
        peakType = AbstractWaveform.PEAK2PEAK;
    }

    public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveform(Solution aSol, Waveform wf, TimeSpan scanSpan) {
        // need to subclass ChannelId with appchannels config member for adding by ChannelIdLoader to accept map 
        if ( wf.getChannelObj().getSeedchan().substring(0,1).equals("E") ) {
            return createAssocMagDataFromWaveformForShortPeriod(aSol, wf, scanSpan);
        }
        return super.createAssocMagDataFromWaveform(aSol, wf, scanSpan);
    }

    public MagnitudeAssocJasiReadingIF createAssocMagDataFromWaveformForShortPeriod(Solution aSol, Waveform wf, TimeSpan scanSpan) {

       if (wf == null) return null;

       // if there is no time-series, we load it here and unload it when done.
       boolean localLoad = false;
       if (!wf.hasTimeSeries())  {
          if (wf.loadTimeSeries()) {     // try to load it
            localLoad = true;
          } else {
            if (debug)
              System.out.println("DEBUG Mx createAssocMagDataFromWaveform b4 Scan NO timeseries: "+
                        wf.getChannelObj().toDelimitedSeedNameString(" "));
            return null ;                // nothing loaded
          }
       }
       // Need way of passing the "clipping" level to the waveform instance
       // so that the filter knows whether to flag the resulting signal/amp
       // as a clipped timeseries like: wf.setClipCounts(value), however the wf
       // may be clipped outside of the timespan of interest for the seismic energy,
       // thus to be robust you need to do the wf.getPeakAmplitude(ts) before filtering,
       // adding much overhead, yuck!!! -aww
       if (wf.isClipped()) {
           if (debug)
              System.out.println ("DEBUG Mx createAssocMagDataFromWaveform b4 filter, input wf is clipped, reject: "+
                      wf.getChannelObj().toDelimitedSeedNameString(" "));
           if (localLoad) wf.unloadTimeSeries(); // aww added 2016/12/30
           return null; // filter failed
       }

       Waveform filteredWf = wf;
       String inputFilterName = wf.getFilterName();

       /* NOTE SHORT PERIODS DON'T USE WAFilter !!!
       if (hasWaveformFilter()) {
          filteredWf = (Waveform) wfFilter.filter(wf);
          if (filteredWf == null) {
            if (debug)
              System.out.println (methodName + " DEBUG createAssocMagDataFromWaveform b4 Filter failed: "+
                      wf.getChannelObj().toDelimitedSeedNameString(" "));
              if (localLoad) wf.unloadTimeSeries(); // aww added 2016/12/30
              return null; // filter failed
          }
       }
       */

       Amplitude amp = null;

       currentSol = aSol; // for tracking purposes
       double originDateTime = aSol.getTime();
       double depth = aSol.getModelDepth(); // aww 2015/10/10
       if (Double.isNaN(depth)) depth = 0.;

       double wfHorizDist = wf.getHorizontalDistance();
       // double wfSlantDist = (wfHorizDist == Channel.NULL_DIST) ? 
       //    Channel.NULL_DIST : Math.sqrt(depth*depth + wfHorizDist*wfHorizDist);

       boolean outsideOfTimeSpan = false;

       // get peak in energy window
       // Note if "clipping" counts level is known to waveform
       // then it could flag it as clipped after scanning it.
       // if not already done so by filteredWf method.
       if (scanSpan != null) {
           outsideOfTimeSpan = ! filteredWf.getTimeSpan().contains(scanSpan);
           amp = filteredWf.getPeakAmplitude(scanSpan, peakType);
           if (debug) 
             System.out.println("DEBUG Mx createAssocMagDataFromWaveform Scan: "+
                             wf.getChannelObj().toDelimitedSeedNameString(" ")+
                             " windowsize= "+scanSpan.getDuration() + " " + scanSpan.toString());
       }
       else if (scanWaveformTimeWindow) {
           TimeSpan ts = (scanPwave) ?
               wf.getEnergyTimeSpan(originDateTime, wfHorizDist, depth) :
               wf.getSWaveEnergyTimeSpan(originDateTime, wfHorizDist, depth);

           if (scanSpanMaxWidth > 0. && ts.getDuration() > scanSpanMaxWidth) {
               ts.setEnd(ts.getStart()+scanSpanMaxWidth);
           }
           outsideOfTimeSpan = ! filteredWf.getTimeSpan().contains(ts);
           amp = filteredWf.getPeakAmplitude(ts, peakType);
           if (debug)
             System.out.println ("DEBUG Mx createAssocMagDataFromWaveform Scan: "+
                              wf.getChannelObj().toDelimitedSeedNameString(" ")+
                              " windowsize= "+ts.getDuration() + " " + ts.toString());
       } else { // get peak in whole waveform
           amp = filteredWf.getPeakAmplitude(filteredWf.getTimeSpan(), peakType);
       }

       // amp can be 'null' if no time series
       if (amp == null) {
          if (debug) 
            System.out.println ("DEBUG Mx createAssocMagDataFromWaveform peak amp NULL: " +
                   wf.getChannelObj().toDelimitedSeedNameString() + " check that timeseries exists for time span!");
           if (localLoad) wf.unloadTimeSeries(); // aww added 2016/12/30
           return null;
       }


       // flag amp as questionable if scan span truncated by end of timeseries
       if (outsideOfTimeSpan) {
           if (! amp.isAuto()) amp.setQuality(Amplitude.COMPLETENESS_UNKNOWN); // assume human will review it, so let it pass
           if (debug)
             System.out.println ("DEBUG Mx createAssocMagDataFromWaveform timespan bounds beyond end of series " +
                     wf.getChannelObj().toDelimitedSeedNameString() );

       }

       // Associate solution with amp to allow slant distance calc using JasiReading method
       amp.assign(aSol);
       if ( ! inputFilterName.trim().equals("") ) {
         if (debug) System.out.println("DEBUG Mx inputFilterName : " + inputFilterName);
         filteredWf.setFilterName(inputFilterName);
         amp.setType(AmpType.WASF);
         amp.comment.setValue(filteredWf.getFilterName());
       }
       else amp.setType(AmpType.WAS);

       // calc SNR
       float noiseLevel = Float.NaN;
       int scanType = WFSegment.scanNoiseType;
       TimeSpan tsp = wf.getPreEnergyTimeSpan(originDateTime, wfHorizDist, depth);

       if (scanType != WFSegment.RMS) { // AVG peak
           noiseLevel = filteredWf.scanForNoiseLevel(tsp);
           if (! Float.isNaN(noiseLevel)) {
               double snr = Math.abs( amp.value.doubleValue() ) / noiseLevel;
               if (!amp.halfAmp) snr /= 2.; // compare half
               amp.snr.setValue(snr);
           }
           // UNCOMMENT BELOW TO OUTPUT unfiltered noise level per peak channel amp 
           // float unfilteredNoiseLevel = wf.scanForNoiseLevel(tsp);
           // System.out.println(wf.getChannelObj().toDelimitedSeedNameString()+" noiseLevel,gain,unfilteredNoiseLevel : "+
           // noiseLevel+" "+wf.getChannelObj().getGain(aSol.getDateTime())+" "+unfilteredNoiseLevel);
       }
       else { // note a RMS SNR value of ~2 is equivalent to ~5 the avg peak way
           TimeSpan tsRMS = new TimeSpan(tsp);
           tsRMS.setEnd(tsRMS.getEnd()+3.); // adding 3 secs to get to within 1 sec p-arrival time
           tsRMS.setStart(Math.max(tsRMS.getStart(),tsRMS.getEnd()-6.)); // try a 6 sec noise window
           noiseLevel = filteredWf.scanForNoiseLevel(tsRMS);
           tsRMS = new TimeSpan(amp.datetime.doubleValue()-1.,amp.datetime.doubleValue()+1.); // try a 2 sec signal window
           float signalLevel = filteredWf.scanForNoiseLevel(tsRMS);
           if (! Float.isNaN(noiseLevel)) amp.snr.setValue( Math.abs(signalLevel)/noiseLevel );
           //String SNRstr = wf.getChannelObj().toDelimitedSeedNameString()+ " rmsSNR: "+ bwfFmt.form(signalLevel/noiseLevel);
       }


       // Check for clipping below
       double clip = 0.; // undefined clipping value is 0.
       // Either lookup clip amp from defined JASI_AMPPARMS_VIEW directly using waveform channel and amp date
       // clip = wf.getChannelObj().getClipAmpValue(amp.getDateTime());
       // or instead get clipping amp from correction map (which assumes correction lookup view joined to clip table)
       MagnitudeCalibrationIF calibrData = getCalibrationFor(amp);
       if (calibrData != null) {
         Double clipping = (Double) calibrData.getClipAmp(amp);
         if (clipping == null || clipping.isNaN()) clip = 0.;
         else {
             clip = Math.abs(clipping.doubleValue());
             if  (clip <= 4096.) {
                 clip = clip*clipFactorA;
                 if (debug) System.out.print(methodName + " DEBUG clip < 4096, assuming analog now using clipFactorA= " + clipFactorA +
                         " clip= " + Math.round(clip));
             }
             else clip = (wf.getChannelObj().isAcceleration()) ? clip*clipFactorDA : clip*clipFactorDV;
         }
       }

       // use clipping to reject amp only when clip is a defined value > 0.
       if (clip <= 0.) {
           if (debug) 
               System.out.println(methodName + " DEBUG  createAssocMagDataFromWaveform CLIPPING AMP IS ZERO for: " + wf.getChannelObj().toDelimitedSeedNameString(" "));
       }
       else if (clipCheck) {
         double value = amp.getValue().doubleValue();
         if (Double.isNaN(value)) {
             System.out.println(methodName + " DEBUG createAssocMagDataFromWaveform peak AMP IS NaN for: " + wf.getChannelObj().toDelimitedSeedNameString(" "));
             if (localLoad) wf.unloadTimeSeries(); // aww added 2016/12/30
             return null;
         }
         value = Math.abs(value);
         if (!amp.halfAmp) value /= 2.; // compare half

         if (value >= clip) {
           if (debug)
               System.out.println(methodName + " DEBUG createAssocMagDataFromWaveform PEAK >= CLIP: (" +value+ "," +clip+ ") rejecting " +
                           wf.getChannelObj().toDelimitedSeedNameString(" "));
           if (localLoad) wf.unloadTimeSeries(); // aww added 2016/12/30
           return null;
         }
         else {
             if (debug)
                 System.out.println(methodName + " DEBUG createAssocMagDataFromWaveform PEAK < CLIP: (" +value+ "," +clip+ ") accepting " +
                           wf.getChannelObj().toDelimitedSeedNameString(" "));
         }
       }

       /* REMOVED secondary BUTTERWORTH bandpass filtering:
       // Noise worst in winter, as much as WAS amp ~ .03 cm, more typically < .01, but in summer usually < .005
       // amps for ML 2.5 at 500 km comparable to fair weather microseismic background noise
       // most microseisms < .005 cm, mag > 4.5 energy overlaps with microseism spectra at great distance -aww
       try {
         // System.out.println("DEBUG Mx amp channel: " + amp.getChannelObj().toDumpString() + " value: " + amp.value.doubleValue() +
         // " period: " + amp.period.doubleValue() + " maxPeriod: " +  maxPeriodC1*Math.exp(maxPeriodC2*wfSlantDist) ); 
         boolean doPerFilter = amp.period.isValidNumber();  
         boolean doBWFilter = false;
         // You can set the value of bwMicroseismMaxMag to very small value, say -1 to disable this second filtering
         if (doPerFilter) {
             // at 20 km range most have periods < .7 and at 500 km most have periods < 2. 
             // some waveforms have arriving phases combining to create a pseudo period > 2. but usually have higher amp signals
             doBWFilter = (amp.period.doubleValue() > maxPeriodC1*Math.exp(maxPeriodC2*wfSlantDist));
         }
         doBWFilter |= ((Math.abs(amp.value.doubleValue()) < bwMicroseismMinAmp ) && (calcChannelMagValue(amp) < bwMicroseismMaxMag));

         //if ( ! inputFilterName.trim().equals("") &&
         //NOTE that setting input property bwFilterOrder to value <=0 disables secondary filtering here
         if (doBWFilter && bwfOrder > 0) {
           //System.out.println("Doing BWF on channel : " + amp.getChannelObj().toDelimitedSeedNameString());

           if (bwf == null  || bwfScalePassBandByDistance) {
               int rate = (int) wf.getSampleRate(); 
               double bwfHiCut = bwfHiFreq;
               double bwfLoCut = bwfLoFreq;
               if (bwfScalePassBandByDistance) {
                   //bwfLoCut = 1./Math.min(maxPeriod, maxPeriodC1*Math.exp(maxPeriodC2*wfSlantDist));
                   //bwfHiCut = 1./Math.max(minPeriod, minPeriodC1*Math.exp(minPeriodC2*wfSlantDist));
                   // range scale, somewhat arbitrary bandpass between .5 and (20 Hz at 10 km and 8 Hz at 600 km), roughly
                   //bwfLoCut = 0.5; // remove 2012/02/21 -aww
                   bwfHiCut = 1./Math.max(minPeriod, .05*Math.exp(.0015*wfSlantDist));
               }
               bwfHiCut = Math.min(bwfHiCut, ((double)rate)/2. - 0.01); // Note subtract 0.01 from hi frequency in beyond Nyquist
               bwfLoCut = Math.min(bwfLoCut, bwfHiCut); // screen for low sample rate

               if (bwfType == 1) { // bandpass type
                 bwf = ButterworthFilterSMC.createBandpass(rate, bwfLoCut, bwfHiCut, bwfOrder, true);
                 bwfTypeStr = "_BP_" + bwfFmt.form(bwfLoCut).trim() + "_" + bwfFmt.form(bwfHiCut).trim() + "_" + String.valueOf(bwfOrder); 
               }
               else { // highpass type
                 bwf = ButterworthFilterSMC.createHighpass(rate, bwfLoCut, bwfOrder, true);
                 bwfTypeStr = "_HP_" + bwfFmt.form(bwfLoCut).trim() + "_" + String.valueOf(bwfOrder); 
               }
               if (bwf != null) bwf.copyInputWaveform(false);
               else System.err.println("CISNmlMagMethod2 Error creating ButterworthFilter for low amp, skipping filter!");
           }

           if (bwf != null) { // else error?,  punt back to original amp
             filteredWf = (Waveform) bwf.filter(filteredWf);
             if ( ! inputFilterName.trim().equals("") )
                 filteredWf.setFilterName(bwfTypeStr +  "_+_" + inputFilterName);
             else
                 filteredWf.setFilterName(bwfTypeStr);
          
             if (scanSpan != null) {
               amp = filteredWf.getPeakAmplitude(scanSpan, peakType);
             }
             else if (scanWaveformTimeWindow) {
               //TimeSpan ts = wf.getEnergyTimeSpan(originDateTime, wfHorizDist, depth); // removed, does P energy, which is often noise -aww
               TimeSpan ts = wf.getSWaveEnergyTimeSpan(originDateTime, wfHorizDist, depth);
               if (scanSpanMaxWidth > 0. && ts.getDuration() > scanSpanMaxWidth) ts.setEnd(ts.getStart()+scanSpanMaxWidth);
               amp = filteredWf.getPeakAmplitude(ts, peakType);
             } else {
               amp = filteredWf.getPeakAmplitude(filteredWf.getTimeSpan(), peakType);
             }
  
             // amp can be 'null' 
             if (amp == null) {
                if (debug) 
                  System.out.println ("DEBUG Mx createAssocMagDataFromWaveform filtered peak amp NULL: " +
                         wf.getChannelObj().toDelimitedSeedNameString() + " check that timeseries exists for time span!");
                 return null;
             }

             amp.assign(aSol);
             amp.setType(AmpType.WASF);
             amp.comment.setValue(filteredWf.getFilterName());
             if (outsideOfTimeSpan && ! amp.isAuto()) amp.setQuality(Amplitude.COMPLETENESS_UNKNOWN); // assume human will review it, so let it pass
  
             noiseLevel = Float.NaN;
             scanType = WFSegment.scanNoiseType;
             tsp = wf.getPreEnergyTimeSpan(originDateTime, wfHorizDist, depth);
  
             if (scanType != WFSegment.RMS) { // AVG peak
                 noiseLevel = filteredWf.scanForNoiseLevel(tsp);
                 if (! Float.isNaN(noiseLevel)) {
                     double snr = Math.abs( amp.value.doubleValue() ) / noiseLevel;
                     if (!amp.halfAmp) snr /= 2.; // compare half
                     amp.snr.setValue(snr);
                 }
             }
             else {  // note a RMS SNR value of ~2 is equivalent to ~5 the avg peak way -aww
                 TimeSpan tsRMS = new TimeSpan(tsp);
                 tsRMS.setEnd(tsRMS.getEnd()+3.); // adding 3 secs to get to within 1 sec p-arrival time
                 tsRMS.setStart(Math.max(tsRMS.getStart(),tsRMS.getEnd()-6.)); // try a 6 sec noise window
                 noiseLevel = filteredWf.scanForNoiseLevel(tsRMS); // RMS
                 tsRMS =  new TimeSpan(amp.datetime.doubleValue()-1.,amp.datetime.doubleValue()+1.); // try a 2 sec signal window
                 float signalLevel = filteredWf.scanForNoiseLevel(tsRMS);
                 if (! Float.isNaN(noiseLevel)) amp.snr.setValue( Math.abs(signalLevel)/noiseLevel );
                 //String SNRstr = wf.getChannelObj().toDelimitedSeedNameString()+ " rmsSNR: "+ bwfFmt.form(signalLevel/noiseLevel) + " BW"; 
             }
           } // bwf filter is not null
         } // end of doBWFilter
       } catch (WrongDataTypeException ex) { }
       */
       
       //System.out.println(SNRstr); // debug test -aww

       // unload time-series
       if (localLoad) wf.unloadTimeSeries();

       //if (amp.halfAmp) System.out.println("Amp is ZERO2PEAK value: " + amp.value.doubleValue() + " "+ wf.getChannelObj().toDelimitedSeedNameString(" "));
       //else System.out.println("Amp is PEAK2PEAK value: " + amp.value.doubleValue() + " "+ wf.getChannelObj().toDelimitedSeedNameString(" "));
 
        //note: cgain is a ChannelGain object and is time dependent
        ChannelGain cgain = amp.getChannelObj().getGain(LeapSeconds.trueToDate(amp.getTime()), doLookUp); // should this do a db lookUp ?
        if (cgain.isNull()) {   // null or NaN
            System.err.println("Mx createAssocMagDataFromWaveform missing gain for " + amp.getChannelObj().toDelimitedSeedNameString() +
                "date: " + LeapSeconds.trueToString(amp.getTime()));
            return null;
        }

        double period = amp.period.doubleValue();
        if (Double.isNaN(period) || period <=0.) {
            System.out.println("Mx undefined PERIOD for peak value:" + amp.value.doubleValue() + " "+ wf.getChannelObj().toDelimitedSeedNameString(" "));
            return null;
        }
        double logf = Math.log10(1./period);

        // interpolate logR
        int i1 = -1;
        int i2 = LOGR.length;
        for (int i = 0; i<LOGR.length; i++) {
            if (logf < LOGF[i]) {
                i1 = i-1;
                i2 = i;
                break;
            }
        }

        double logR = 0.;
        if (i1 >= 0 && i2 <= LOGR.length) {
          logR = LOGR[i1] + (LOGR[i2]-LOGR[i1])*(logf-LOGF[i1])/(LOGF[i2]-LOGF[i1]);
        }
        else {
          System.out.println("Mx PERIOD out of range for amp:" + period + " "+ wf.getChannelObj().toDelimitedSeedNameString(" "));
          /*
          if (logf <= 1.27) {
            logR = -0.21*Math.pow(logf,4.) + 0.3059*Math.pow(logf,3.) + 0.0049*Math.pow(logf,2.) + 0.7729*logf + 1.1271;
          }
          else {
            logR = -2.8206*Math.pow(logf,2.) + 5.0941*logf + 0.2547;
          }
          */
          return null;
        }
        // Develocorder zero to peak amp Ad(mm):
        // Ad(mm) = S*G*Dd*V where motor constant S=.5v/cm/s, G is gain scalar, Dd=40mm/v, and V is ground velocity cm/s
        //
        // At HVO simple_response_gain is S*G*Dt, stored as counts/m/s, where Dt is the EW digitizer scalar, Dt=819 counts/v. 
        // zero2PeakAmpCounts = simple_response_gain(counts/m/s) * V(m/s), where V is velocity 
        //
        // At HVO to get equivalent develocorder mm, multiply peak counts by Dd=40 mm/v and divide by Dt=819 counts/v
        // so multiplying the peak counts Ut = Dd/Dt = 0.04883 mm/count we get:
        // Ad(mm) = zero2PeakAmpCounts*Ut(mm/count)
        //
        // from above, G = simple_response_gain*(1m/100cm)/(S*Dt), and the HYP2000 manual says CAL=G/883, so:
        // CAL = simple_response_gain*(1m/100cm)/(883.*S*Dt) 
        //
        // Awa/2(mm) = Ad(mm)/(CAL*R(f)*S),  CAL is dimensionless, not sure about units of R(f) if S v/cm/s ?
        //
        // Since Ad(mm) is zero2peak, rather than peak2peak:
        // Awa/2 = Ad/(CAL*R(f)*S) = zero2PeakAmpCounts*Ut/(CAL*R(f)*S) 
        //
        // Substituting Ut=Dd/Dt and CAL=simple_response_gain*(1m/100cm)/(883.*S*Dt) gives:
        //       = zero2PeakAmpCounts*(Dd/Dt)/(simple_response_gain*(1m/100cm)*R(f)*S/(883.*S*Dt)) 
        //
        // Awa/2 = zero2PeakAmpCounts*Dd/(simple_response_gain*(1m/100cm)*R(f)/883.) 
        //

        // convert db simple_response_gain to equivalent counts/cm/s:
        double gainScalar = Units.convertFromCommon(cgain.doubleValue(), ChannelGain.getResultingUnits(cgain));
        // Awa/2 = zero2PeakAmpCounts*Dd/(gainScalar*R(f)/883.)
        // where develocorder Dd=40 mm/v and R(f)=Math.pow(10.,logR), so after rearranging above scalar terms we get:
        gainScalar = 35320./(gainScalar * Math.pow(10.,logR));

        amp.setValue(amp.value.doubleValue()*gainScalar, Units.MM); // approximate peak2peak counts to equivalent mm 

        //NOTE: getAmpValue(amp) returns peak2peak/2. when amp=wf.getPeakAmplitude(scanSpan,AbstractWaveform.PEAK2PEAK)
       return amp;  // return (hasAcceptableSNR(amp)) ? amp : null, no instead let isValid(amp) method decide -aww
    }


    public double distanceCorrection(double distance) {
        double corr = 0.;

        if (distance < 70.) {
            corr = 0.82*Math.log10(distance) + .00405*distance  + 0.955 - .09*Math.sin(.07*(distance-25));
        }
        else if (distance < 185.3) {
            corr = 0.82*Math.log10(distance) + .00405*distance + 0.955;
        }
        else {
            corr = 3.57 + 2.55*(Math.log10(distance) - Math.log10(185.3));
        }

        return corr;
    }

    public double calcChannelMagValue(Amplitude amp) throws WrongDataTypeException {
      if (! isIncludedReadingType(amp) ) {
          throw new WrongAmpTypeException(
            "Wrong Amp type for MagnitudeMethod " +methodName+
            " : " +amp.toNeatString() // +AmpType.getString(amp.type)
          );
      }
      if (amp.isClipped()) { // should we throw WrongDataTypeException?
          throw new WrongAmpTypeException(
            "Amp isClipped for MagnitudeMethod "+methodName+
            " : " +amp.toNeatString()
          );
      }
      //to discriminate for small events with negative channel corrections changed to -9.
      double value = -9.; // changed 0. to -9 to make it obvious to user - aww 8/12/2004
      try {
        Double corr = getMagCorr(amp);
        double corrValue = (corr == null)? 0. : corr.doubleValue();

        // Global "vertical" component correction, if any
        if ( amp.getChannelObj().getSeedchan().equals("EHZ") ) {
            corrValue += magCorrEHZ;
        }
        else if ( amp.getChannelObj().getSeedchan().equals("ELZ") ) {
            corrValue += magCorrELZ;
        }
        else if ( amp.getChannelObj().getSeedchan().equals("ELN") ) {
            corrValue += magCorrELN;
        }
        else if ( amp.getChannelObj().getSeedchan().equals("ELE") ) {
            corrValue += magCorrELE;
        }
        else if ( amp.getChannelObj().getSeedchan().equals("EHN") ) {
            corrValue += magCorrEHN;
        }
        else if ( amp.getChannelObj().getSeedchan().equals("EHE") ) {
            corrValue += magCorrEHE;
        }

        value = calcChannelMagValue(
                        //amp.getDistance(), // don't use slant distance - aww 06/11/2004
                        amp.getHorizontalDistance(), // aww 06/11/2004
                        getAmpValue(amp),
                        corrValue
                );
      }
      catch (WrongDataTypeException ex) {
        ex.printStackTrace();
      }
      return value;
    }

    public void initializeMethod() {
       super.initializeMethod();
       if (props == null) return; // custom properties
       if (props.getProperty("magCorrEHZ") != null) {
         magCorrEHZ = props.getDouble("magCorrEHZ");
         System.out.println("FYI - " +getMethodName()+ " global EHZ correction value: " + magCorrEHZ); 
       }
       if (props.getProperty("magCorrEHN") != null) {
         magCorrEHN = props.getDouble("magCorrEHN");
         System.out.println("FYI - " +getMethodName()+ " global EHN correction value: " + magCorrEHN); 
       }
       if (props.getProperty("magCorrEHE") != null) {
         magCorrEHE = props.getDouble("magCorrEHE");
         System.out.println("FYI - " +getMethodName()+ " global EHE correction value: " + magCorrEHE); 
       }
       if (props.getProperty("magCorrELZ") != null) {
         magCorrELZ = props.getDouble("magCorrELZ");
         System.out.println("FYI - " +getMethodName()+ " global ELZ correction value: " + magCorrELZ); 
       }
       if (props.getProperty("magCorrELN") != null) {
         magCorrELN = props.getDouble("magCorrELN");
         System.out.println("FYI - " +getMethodName()+ " global ELN correction value: " + magCorrELN); 
       }
       if (props.getProperty("magCorrELE") != null) {
         magCorrELE = props.getDouble("magCorrELE");
         System.out.println("FYI - " +getMethodName()+ " global ELE correction value: " + magCorrELE); 
       }
    }

    public GenericPropertyList getProperties() {
        GenericPropertyList props = super.getProperties();
        props.setProperty("magCorrEHZ", magCorrEHZ); 
        props.setProperty("magCorrEHN", magCorrEHN); 
        props.setProperty("magCorrEHE", magCorrEHE); 
        props.setProperty("magCorrELZ", magCorrELZ); 
        props.setProperty("magCorrELN", magCorrELN); 
        props.setProperty("magCorrELE", magCorrELE); 
        return props;
    }

    public void setDefaultProperties() {
        super.setDefaultProperties();
        magCorrELZ = 0.;
        magCorrELN = 0.;
        magCorrELE = 0.;
        magCorrEHZ = 0.;
        magCorrEHN = 0.;
        magCorrEHE = 0.;
        peakType = AbstractWaveform.PEAK2PEAK;
    }

    /*
    public static final void main(String [] args) {
        double logf = Double.parseDouble(args[0]);
        // interpolate logR
        int i1 = 0;
        int i2 = 0;
        for (int i = 0; i<LOGR.length; i++) {
            if (logf < LOGF[i]) {
                i1 = i-1;
                i2 = i;
                break;
            }
        }

        if (i1 < 0) i1 = 0;
        double logR = 0.;
        logR = LOGR[i1] + (LOGR[i2]-LOGR[i1])*(logf-LOGF[i1])/(LOGF[i2]-LOGF[i1]);
        System.out.println("logR1: " + logR); 

        if (logf <= 1.27) {
            logR = -0.21*Math.pow(logf,4.) + 0.3059*Math.pow(logf,3.) + 0.0049*Math.pow(logf,2.) + 0.7729*logf + 1.1271;
        }
        else {
            logR = -2.8206*Math.pow(logf,2.) + 5.0941*logf + 0.2547;
        }
        System.out.println("logR2: " + logR); 
    }
    */
}
