package org.trinet.jasi.picker;

import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FileNotFoundException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Scanner;
import java.util.NoSuchElementException;

import javax.swing.JComponent;

import org.trinet.jasi.Channel;
import org.trinet.jasi.ChannelIdIF;
import org.trinet.jasi.Channelable;
import org.trinet.jasi.ChannelableList;
import org.trinet.jasi.ChannelableListIF;
import org.trinet.jasi.ChannelList;
import org.trinet.jasi.Magnitude;
import org.trinet.jasi.MasterChannelList;
import org.trinet.jasi.PhaseList;
import org.trinet.jasi.Solution;
import org.trinet.jasi.TravelTime;
import org.trinet.jasi.Waveform;
import org.trinet.jasi.WFSegment;

import org.trinet.util.DateTime;
import org.trinet.util.GenericPropertyList;
import org.trinet.util.LeapSeconds;
import org.trinet.util.TimeSpan;

public abstract class AbstractPicker implements PhasePickerIF {

    public static final int DEFAULT_MAX_SAMPLE_GAP = 15;
    public static final int DEFAULT_RESTART_LENGTH = 200;
    public static final double DEFAULT_WINDOW_HALFWIDTH = 2.;
    public static final double DEFAULT_START_OFFSET = 2.;
    public static final double DEFAULT_END_OFFSET = 0.;
    public static final double DEFAULT_MAX_DISTANCE = 100.;
    public static final double DEFAULT_MAGDIST_SLOPE = 85.;
    public static final double DEFAULT_MAGDIST_INTERCEPT = 10.;
    public static final double DEFAULT_NULL_MAG_VALUE = 3.0;
    public static final double DEFAULT_S_MAXDIST_SCALAR = 1.;

    protected String name = null;
    protected int lagAdjustment = 0;
    protected int maxSampleGap = DEFAULT_MAX_SAMPLE_GAP;
    protected int restartLength = DEFAULT_RESTART_LENGTH;
    protected double windowHalfWidth = DEFAULT_WINDOW_HALFWIDTH;
    protected double scanStartOffset = DEFAULT_START_OFFSET;
    protected double scanEndOffset = DEFAULT_END_OFFSET;
    protected double maxDistance = DEFAULT_MAX_DISTANCE;
    protected double magDistSlope = DEFAULT_MAGDIST_SLOPE;
    protected double magDistIntercept = DEFAULT_MAGDIST_INTERCEPT;
    protected double sMaxDistScalar = DEFAULT_S_MAXDIST_SCALAR; 
    protected boolean sDownWeightByOne = false; 

    protected String chanParmFile = null;
    private String absChanParmFile = null;

    protected boolean debug = false;
    protected boolean verbose = false;
    protected boolean PonH = false;
    protected boolean SonV = false;
    protected boolean velocityOnly = false;

    protected boolean firstMoOnHoriz=false;
    protected boolean firstMoOnS=false;
    protected double  firstMoQualityMin=0.7;

    protected HashMap chanParmMap = null;
    protected boolean autoLoadChannelParms = false;

    // below like ChannelTimeWindowModel filters:
    protected String staIncludeList[]  = {};
    protected String locIncludeList[]  = {};
    protected String chanIncludeList[] = {};
    protected String netIncludeList[]  = {};

    protected String staRejectList[]   = {};
    protected String locRejectList[]  = {};
    protected String chanRejectList[]  = {};
    protected String netRejectList[]   = {};

    protected String candidateListName = "";
    protected String oldCandidateListName = candidateListName;
    protected ChannelableList candidateList = null;
    protected boolean filterByCandidateList = false;
    protected boolean useMasterList = false;

    public AbstractPicker() { } 

    public AbstractPicker(GenericPropertyList gpl) {
        setProperties(gpl);
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean tf) {
        debug = tf;
        if (tf) verbose = true;
    }

    public boolean isVerbose() {
        return verbose;
    }

    public void setVerbose(boolean tf) {
        verbose = tf;
    }

    public void setAutoLoadChannelParms(boolean tf) {
        autoLoadChannelParms = tf;
    }

    public boolean isAutoLoadChannelParms() {
        return autoLoadChannelParms;
    }

    public String getName() {
        return name;
    }

    public void setDefaultProperties() {
        setMyDefaultProperties();
    }

    private void setMyDefaultProperties() {

        debug = false;
        verbose = false;

        firstMoOnHoriz=false;
        firstMoOnS=false;
        firstMoQualityMin=0.7;

        PonH = false;
        SonV = false;
        velocityOnly = false;
        autoLoadChannelParms = false;

        lagAdjustment = 0;

        filterByCandidateList = false;
        candidateListName = "";
        useMasterList = false;

        staIncludeList = new String [0];
        locIncludeList = new String [0];
        chanIncludeList = new String [0];
        netIncludeList = new String [0];

        staRejectList = new String [0];
        locRejectList = new String [0];
        chanRejectList = new String [0];
        netRejectList = new String [0];

        maxSampleGap = DEFAULT_MAX_SAMPLE_GAP;
        restartLength = DEFAULT_RESTART_LENGTH;
        windowHalfWidth = DEFAULT_WINDOW_HALFWIDTH;
        scanStartOffset = DEFAULT_START_OFFSET;
        scanEndOffset = DEFAULT_END_OFFSET;
        maxDistance = DEFAULT_MAX_DISTANCE;
        magDistSlope = DEFAULT_MAGDIST_SLOPE;
        magDistIntercept = DEFAULT_MAGDIST_INTERCEPT;
        sMaxDistScalar = DEFAULT_S_MAXDIST_SCALAR; 
        sDownWeightByOne = false; 

        chanParmFile = null;
        absChanParmFile = null;

    }


    public void setProperties(GenericPropertyList gpl) {

        setMyDefaultProperties();

        String pre = getPropertyPrefix();

        lagAdjustment = gpl.getInt(pre+"lagAdjustment", 0);
        maxSampleGap = gpl.getInt(pre+"maxSampleGap", DEFAULT_MAX_SAMPLE_GAP);
        restartLength = gpl.getInt(pre+"restartLength", DEFAULT_RESTART_LENGTH);
        windowHalfWidth = gpl.getDouble(pre+"windowHalfWidth", DEFAULT_WINDOW_HALFWIDTH);
        scanStartOffset = gpl.getDouble(pre+"scanStartOffset", DEFAULT_START_OFFSET);
        scanEndOffset = gpl.getDouble(pre+"scanEndOffset", DEFAULT_END_OFFSET);

        SonV = gpl.getBoolean(pre+"SonV", false);
        PonH = gpl.getBoolean(pre+"PonH", false);
        velocityOnly = gpl.getBoolean(pre+"velocityOnly", false);

        firstMoOnHoriz= gpl.getBoolean(pre+"firstMoOnHoriz", false);
        firstMoOnS= gpl.getBoolean(pre+"firstMoOnS", false);
        firstMoQualityMin=gpl.getDouble(pre+"firstMoQualityMin", .7);

        maxDistance = gpl.getDouble(pre+"maxDistance", DEFAULT_MAX_DISTANCE);
        magDistSlope = gpl.getDouble(pre+"magDistSlope", DEFAULT_MAGDIST_SLOPE);
        magDistIntercept = gpl.getDouble(pre+"magDistIntercept", DEFAULT_MAGDIST_INTERCEPT);
        sMaxDistScalar = gpl.getDouble(pre+"sMaxDistScalar", DEFAULT_S_MAXDIST_SCALAR);
        sDownWeightByOne = gpl.getBoolean(pre+"sDownWeightByOne", false);

        verbose = gpl.getBoolean(pre+"verbose", false);
        setDebug(gpl.getBoolean(pre+"debug", false));
 
        chanParmFile = gpl.getProperty( pre+"chanParmFile", "");
        absChanParmFile  = (chanParmFile.length() > 0)  ? gpl.getUserFileNameFromProperty(pre+"chanParmFile") : null;

        if (gpl.isSpecified(pre+"candidateListName") )
          setCandidateListName(gpl.getProperty(pre+"candidateListName"));

        if (gpl.isSpecified(pre+"allowedStaTypes") )
          setAllowedStaTypes(gpl.getStringArray(pre+"allowedStaTypes"));

        if (gpl.isSpecified(pre+"allowedSeedChanTypes") )
          setAllowedChannelTypes(gpl.getStringArray(pre+"allowedSeedChanTypes"));

        if (gpl.isSpecified(pre+"allowedNetTypes") )
          setAllowedNetTypes(gpl.getStringArray(pre+"allowedNetTypes"));

        if (gpl.isSpecified(pre+"rejectedStaTypes") )
          setRejectedStaTypes(gpl.getStringArray(pre+"rejectedStaTypes"));

        if (gpl.isSpecified(pre+"rejectedSeedChanTypes") )
          setRejectedChannelTypes(gpl.getStringArray(pre+"rejectedSeedChanTypes"));

        if (gpl.isSpecified(pre+"rejectedNetTypes") )
          setRejectedNetTypes(gpl.getStringArray(pre+"rejectedNetTypes"));

        if (gpl.isSpecified(pre+"filterByCandidateList"))
          filterByCandidateList = gpl.getBoolean(pre+"filterByCandidateList");

        if (gpl.isSpecified(pre+"useMasterListCandidateList"))
          useMasterList = gpl.getBoolean(pre+"useMasterListCandidateList");

        setDefaultParmValues(gpl);

        if (gpl.isSpecified(pre+"autoLoadChannelParms"))
          autoLoadChannelParms = gpl.getBoolean(pre+"autoLoadChannelParms");


        if (autoLoadChannelParms) loadChannelParms();
    }

    public GenericPropertyList getProperties(GenericPropertyList gpl) {

        if (gpl == null) gpl = new GenericPropertyList();

        String pre = getPropertyPrefix();

        gpl.setProperty(pre+"lagAdjustment", lagAdjustment);
        gpl.setProperty(pre+"maxSampleGap", maxSampleGap);
        gpl.setProperty(pre+"restartLength", restartLength);
        gpl.setProperty(pre+"windowHalfWidth",windowHalfWidth); 
        gpl.setProperty(pre+"scanStartOffset",scanStartOffset);
        gpl.setProperty(pre+"scanEndOffset", scanEndOffset);

        gpl.setProperty(pre+"SonV", SonV);
        gpl.setProperty(pre+"PonH", PonH);
        gpl.setProperty(pre+"velocityOnly", velocityOnly);
        gpl.setProperty(pre+"verbose", verbose);
        gpl.setProperty(pre+"debug", debug);

        gpl.setProperty(pre+"firstMoOnHoriz", firstMoOnHoriz);
        gpl.setProperty(pre+"firstMoOnS", firstMoOnS);
        gpl.setProperty(pre+"firstMoQualityMin", firstMoQualityMin);

        gpl.setProperty(pre+"maxDistance", maxDistance);
        gpl.setProperty(pre+"magDistSlope", magDistSlope);
        gpl.setProperty(pre+"magDistIntercept", magDistIntercept);
        gpl.setProperty(pre+"sMaxDistScalar", sMaxDistScalar);
        gpl.setProperty(pre+"sDownWeightByOne", sDownWeightByOne);

        gpl.setProperty(pre+"chanParmFile", (chanParmFile == null) ? "" : chanParmFile );

        gpl.setProperty(pre+"candidateListName", (candidateListName == null) ? "" : candidateListName);
        gpl.setProperty(pre+"allowedStaTypes", GenericPropertyList.toPropertyString(staIncludeList));
        gpl.setProperty(pre+"allowedLocationTypes", GenericPropertyList.toPropertyString(locIncludeList));
        gpl.setProperty(pre+"allowedSeedChanTypes", GenericPropertyList.toPropertyString(chanIncludeList));
        gpl.setProperty(pre+"allowedNetTypes", GenericPropertyList.toPropertyString(netIncludeList));
        gpl.setProperty(pre+"rejectedStaTypes", GenericPropertyList.toPropertyString(staRejectList));
        gpl.setProperty(pre+"rejectedLocationTypes", GenericPropertyList.toPropertyString(locRejectList));
        gpl.setProperty(pre+"rejectedSeedChanTypes", GenericPropertyList.toPropertyString(chanRejectList));
        gpl.setProperty(pre+"rejectedNetTypes", GenericPropertyList.toPropertyString(netRejectList));
        gpl.setProperty(pre+"filterByCandidateList", filterByCandidateList);
        gpl.setProperty(pre+"useMasterListCandidateList", useMasterList);
        gpl.setProperty(pre+"autoLoadChannelParms", autoLoadChannelParms);

        getDefaultParmValues(gpl);

        return gpl;
    }

    public abstract GenericPropertyList getDefaultParmValues(GenericPropertyList gpl);
    public abstract PickerParmIF getDefaultParm();
    public abstract void setDefaultParmValues(PickerParmIF parm);
    public abstract void setDefaultParmValues(GenericPropertyList gpl);

    public String toParseableString(PickerParmIF parm) {
        return (parm == null) ? null : parm.toParseableString();
    }

    public boolean hasChannelParm(Channelable ch) {
        return (getChannelParm(ch.getChannelObj().toDelimitedSeedNameString(".")) != null);
    }

    public boolean hasChannelParms() {
        return (chanParmMap != null); // && chanParmMap.size() > 0);
    }

    public boolean pOnH() {
        return PonH;
    }
    public void setPonH(boolean tf) {
        PonH = tf;
    }

    public boolean sOnV() {
        return SonV;
    }
    public void setSonV(boolean tf) {
        SonV = tf;
    }

    public boolean velocityOnly() {
        return velocityOnly;
    }
    public void setVelocityOnly(boolean tf) {
        velocityOnly =tf;
    }

    public void setMaxDistance(double km) {
        maxDistance = km;
    }
    public double getMaxDistance() {
        return maxDistance;
    }

    public double getMaxDistance(double magnitude) {
        return (magDistSlope*magnitude+ magDistIntercept);
    }

    public boolean withinPickerRange(Channel ch, Solution sol, String pickType) { 
            boolean retVal = true;
            double scalar =  ( pickType.equals("S") ) ? sMaxDistScalar() : 1.;
            double chDist = ch.getHorizontalDistance();
            double maxdist = getMaxDistance() * scalar;
            if (chDist > maxdist) {
                retVal = false;
                if (isVerbose()) 
                    System.out.printf("%s max distance exceeded: %6.1f Km %s%n", name, chDist, ch.toDelimitedSeedNameString("."));
            }
            if ( retVal ) {
                Magnitude prefmag = sol.getPreferredMagnitude();
                double mag = (prefmag == null || prefmag.hasNullValue() || prefmag.hasNullType()) ?
                             AbstractPicker.DEFAULT_NULL_MAG_VALUE : prefmag.getMagValue();
                maxdist = getMaxDistance(mag) * scalar;
                if (chDist > maxdist) {
                    retVal = false;
                    if (isVerbose()) 
                        System.out.printf("%s max prefmag distance exceeded: %6.1f Km %s%n",
                                            name, chDist, ch.toDelimitedSeedNameString("."));
                }
            }
            return retVal;
    }

    public double sMaxDistScalar() {
        return sMaxDistScalar;
    }

    public void setSMaxDistScalar(double value) {
        sMaxDistScalar = Math.abs(value);
    }

    public boolean sDownWeight() {
        return sDownWeightByOne;
    }

    public void setSdownWeight(boolean tf) {
         sDownWeightByOne = tf;
    }

    public boolean getFirstMoOnHoriz() {
        return firstMoOnHoriz;
    }
    public void setFirstMoOnHoriz(boolean tf) {
        firstMoOnHoriz = tf;
    }
    public boolean getFirstMoOnS() {
        return firstMoOnS;
    }
    public void setFirstMoOnS(boolean tf) {
        firstMoOnS = tf;
    }
    public double getFirstMoQualityMin() {
        return firstMoQualityMin;
    }
    public void setFirstMoQualityMin(double value) {
        firstMoQualityMin = value;
    }

    public double getScanStartOffset() {
        return scanStartOffset;
    }

    public double getScanEndOffset() {
        return scanEndOffset;
    }

    //
    // Code below modded from ChannelTImeWindowModel:
    //
    public void setCandidateListName(String newName) {
        oldCandidateListName = candidateListName; // save old name
        candidateListName = newName; // set new name
        if ((oldCandidateListName == null || ! oldCandidateListName.equals(candidateListName)) ) {
            if (candidateList != null) candidateList = null; // Reset list if name changes
        }
    }

    public String getCandidateListName() {
        return candidateListName;
    }
    public void setCandidateList(ChannelableList candidateListIn) {
        if (candidateListIn == null ) {
          candidateList = null;
          //if (debug || verbose)
              System.out.printf(" INFO %s setCandidateList NULL list%n", name);
          return;
        }

        if (candidateListIn instanceof ChannelList) candidateList = candidateListIn;
        else { // Parse out channels from input list if not already a ChannelList object
            candidateList = new ChannelList();
            toListOfChannels(candidateListIn, candidateList);
        }
        candidateList = filterByChannelPropertyAttributes(candidateList);
        if (debug || verbose) System.out.printf(" INFO %s setCandidateList size = %d%n", name, candidateList.size());
    }

    protected static void toListOfChannels(ChannelableList inList, ChannelableList outList) {
      if (inList == null) return; 
      int count = inList.size();
      outList.ensureCapacity(outList.size()+count);
      Channelable chn = null;
      for (int i=0; i<count; i++) {
        chn = (Channelable) inList.get(i);
        if (! outList.hasChannelId(chn)) outList.add(chn.getChannelObj()); // add only if channel id not already in list
      }
    }

    protected  ChannelableList filterByChannelPropertyAttributes(ChannelableList inList) {
        ChannelableList outList = inList; // aliased
        //
        // An empty include list means "allow all"
        //
        if (staIncludeList != null && staIncludeList.length > 0) {
            // NOTE: cull of an empty include input list returns NO channels
            //if (debug) System.out.printf("DEBUG %s cullByChannelNameAttribute ALLOWED STA %s%n", name, Arrays.asList(staIncludeList));
            outList =
              (ChannelableList) outList.cullByChannelNameAttribute(staIncludeList, ChannelIdIF.STA, ChannelableListIF.INCLUDE);
        }
        if (netIncludeList != null && netIncludeList.length > 0) {
            //if (debug) System.out.printf("DEBUG %s cullByChannelNameAttribute ALLOWED NET %s%n", name, Arrays.asList(netIncludeList));
            outList =
                (ChannelableList) outList.cullByChannelNameAttribute(netIncludeList, ChannelIdIF.NET, ChannelableListIF.INCLUDE);
        }
        if (chanIncludeList != null && chanIncludeList.length > 0) {
            // NOTE: cull of an empty include input list returns NO channels
            //if (debug) System.out.printf("DEBUG %s cullByChannelNameAttribute ALLOWED SEEDCHAN %s%n", name, Arrays.asList(chanIncludeList));
            outList =
              (ChannelableList) outList.cullByChannelNameAttribute(chanIncludeList, ChannelIdIF.SEEDCHAN, ChannelableListIF.INCLUDE);
        }

        if (locIncludeList != null && locIncludeList.length > 0) {
            // NOTE: cull of an empty include input list returns NO channels
            //if (debug) System.out.printf("DEBUG %s cullByChannelNameAttribute ALLOWED LOCATION %s%n", name, Arrays.asList(locIncludeList));
            outList =
              (ChannelableList) outList.cullByChannelNameAttribute(locIncludeList, ChannelIdIF.LOCATION, ChannelableListIF.INCLUDE);
        }

        // After includes now do REJECT
        if (netRejectList != null && netRejectList.length > 0) {
            // NOTE: cull of an empty include input list returns NO channels
            //if (debug) System.out.printf("DEBUG %s cullByChannelNameAttribute REJECTED NET %s%n", name, Arrays.asList(netRejectList));
            outList =
                (ChannelableList) outList.cullByChannelNameAttribute(netRejectList, ChannelIdIF.NET, ChannelableListIF.EXCLUDE);
        }
        if (chanRejectList != null && chanRejectList.length > 0) {
            // NOTE: cull of an empty include input list returns NO channels
            //if (debug) System.out.printf("DEBUG %s cullByChannelNameAttribute REJECTED SEEDCHAN %s%n", name, Arrays.asList(chanRejectList));
            outList =
              (ChannelableList) outList.cullByChannelNameAttribute(chanRejectList, ChannelIdIF.SEEDCHAN, ChannelableListIF.EXCLUDE);
        }
        if (staRejectList != null && staRejectList.length > 0) {
            // NOTE: cull of an empty include input list returns NO channels
            //if (debug) System.out.printf("DEBUG %s cullByChannelNameAttribute REJECTED STA %s%n", name, Arrays.asList(staRejectList));
            outList =
              (ChannelableList) outList.cullByChannelNameAttribute(staRejectList, ChannelIdIF.STA, ChannelableListIF.EXCLUDE);
        }

        if (locRejectList != null && locRejectList.length > 0) {
            // NOTE: cull of an empty include input list returns NO channels
            //if (debug) System.out.printf("DEBUG %s cullByChannelNameAttribute REJECTED LOCATION %s%n", name, Arrays.asList(locRejectList));
            outList =
              (ChannelableList) outList.cullByChannelNameAttribute(locRejectList, ChannelIdIF.LOCATION, ChannelableListIF.EXCLUDE);
        }

        return outList;
    }

    /** Return the master ChannelableList from which the model will select channels to include. */
    public ChannelableList getCandidateList() {
        if (candidateList == null && useMasterList) {
            if (debug) 
                System.out.printf("DEBUG %s getCandidateList() candidateList is MasterList of size: %d%n", name, 
                       ((MasterChannelList.get() != null) ? MasterChannelList.get().size() : 0));
            setCandidateList((ChannelList)MasterChannelList.get().clone());
        }
        else if (candidateList == null) {
          String listName = getCandidateListName(); 
          if (listName != null && listName.trim().length() > 0) {
            if (debug)
                System.out.printf("DEBUG %s getCandidateList() initializing candidateList by name: %s%n", name, listName); 
            DateTime time = new DateTime();
            setCandidateList(Channel.create().getNamedChannelList(listName, time));
          }
          else setCandidateList(new ChannelList(0));

          if (debug) {
            System.out.printf("DEBUG %s getCandidateList() returned candidateList of size: %d%n", name, (candidateList==null) ? 0 : candidateList.size());
          }
        }

        return candidateList;
    }

    public void setAllowedStaTypes(String[] staList) {
      staIncludeList = staList;
    }
    public void setAllowedLocationTypes(String[] staList) {
      locIncludeList = staList;
    }
    public void setAllowedChannelTypes(String[] chanList) {
      chanIncludeList = chanList;
    }
    public void setAllowedNetTypes(String[] netList) {
      netIncludeList = netList;
    }

    public void setRejectedStaTypes(String[] staList) {
      staRejectList = staList;
    }
    public void setRejectedLocationTypes(String[] staList) {
      locRejectList = staList;
    }
    public void setRejectedChannelTypes(String[] chanList) {
      chanRejectList = chanList;
    }
    public void setRejectedNetTypes(String[] netList) {
      netRejectList = netList;
    }

    public boolean reject(Channelable ch) {

      if (filterByCandidateList && !include(ch)) return true;

      ChannelableList outList = new ChannelableList(1);
      outList.add(ch);
      outList = filterByChannelPropertyAttributes(outList);
      return (outList.size() == 0); 
    }

    protected boolean include(Channelable chn) {
      // if it's name is set, and list is null, this creates candidate list by default
      getCandidateList(); // may be null if name not defined
      //if (debug) System.out.printf("DEBUG %s candidateList size: %d%n", name, (aCandidateList == null) ? 0 : aCandidateList.size() );
      return (candidateList == null || candidateList.isEmpty()) ? true : candidateList.hasChannelId(chn);
    }

    public static final String getPropertyPrefix() {
      return "picker.";
    }
    // End of ChannelTimeWindowModel modded code above

    /**
     * The Waveform must have load timeseries.
     * @param phasesToPickFlag PhasePickerIF.NONE, do not pick, a no-op.
     * @param phasesToPickFlag PhasePickerIF.P_ONLY, P arrival, ignores input sncl mapped PickerParmIF flag 
     * @param phasesToPickFlag PhasePickerIF.S_ONLY, S arrival, ignores input sncl mapped PickerParmIF flag.
     * @param phasesToPickFlag PhasePickerIF.P_AND_S, both arrivals, ignores input sncl mapped PickerParmIF flag.
     * @param phasesToPickFlag PhasePickerIF.DEFAULT, uses input sncl mapped PickerParmIF flag, if none, use defaults by component type.
     * @param scnlMappedParm  picker configuration parameters mapped to String of the form: net.sta.seed.loc derived from input waveform Channel.
     * @param scanSpan timespan over which to scan waveform timeseries for phase picks
     * @param pSpan timespan over which to assign a P pick.
     * @param sSpan timespan over which to assign a S pick.
     * @return list containing phases or empty list if no picks were made
     */
    public abstract PhaseList pick(Waveform wf, int phasesToPickFlag, PickerParmIF snclMappedParm, TimeSpan scanSpan, TimeSpan pSpan, TimeSpan sSpan);
    
    /**
     * @param phasesToPickFlag PhasePickerIF.NONE, do not pick, a no-op.
     * @param phasesToPickFlag PhasePickerIF.P_ONLY, P arrival, ignores channel sncl mapped PickerParmIF flag.
     * @param phasesToPickFlag PhasePickerIF.S_ONLY, S arrival, ignores channel sncl mapped PickerParmIF flag.
     * @param phasesToPickFlag PhasePickerIF.P_AND_S, both arrivals, ignores channel sncl mapped PickerParmIF flag.
     * @param phasesToPickFlag PhasePickerIF.DEFAULT, use sncl mapped PickerParmIF flag, if none default for component type to one of the values above.
     * @param velocityOnly true, pick channel only if its gain units are velocity
     * @param distFilter true, pick channel only if its epicentral distance &LT;= maximum km defined by input properties
     * @return list containing phases or empty list if no picks were made
     */
    public PhaseList pick(ChannelableList wfList, Solution sol, int phasesToPickFlag, boolean gainUnitsFilter, boolean distFilter) {
        if (sol == null) {
            System.err.printf("ERROR %s input Solution is null!%n", name);
            return new PhaseList(0);
        }

        int count = (wfList == null) ? 0 : wfList.size();
        PhaseList phList = new PhaseList(count);
        if (count == 0) {
            System.err.printf("ERROR %s input waveform is null or empty!%n", name);
            return phList;
        }

        boolean hasLatLonZ = sol.hasLatLonZ();
        if (distFilter && !hasLatLonZ) {
            System.err.printf("ERROR %s distance filtering and input sol has no location!%n", name);
            return phList;
        }

        Waveform wf = null;
        for (int idx=0; idx<count; idx++) {
            wf = (Waveform) wfList.get(idx);
            if (distFilter && !wf.getChannelObj().hasLatLonZ()) continue;
            if (hasLatLonZ) {
                phList.add( pick(wf, sol, phasesToPickFlag, gainUnitsFilter, distFilter) );
            }
            else {
                phList.add( pick(wf, phasesToPickFlag, gainUnitsFilter ) );
            }
        }
        return phList;
    }

    public PhaseList pick(Waveform wf, Solution sol, int phasesToPickFlag, boolean gainUnitsFilter, boolean distFilter) {

        PhaseList phList = new PhaseList(2); // new list for P and/or S pick(s)

        if (wf == null) {
            //if (debug || verbose)
                System.err.printf("ERROR %s input Waveform is null!%n", name);
            return phList; // bail if no solution for epicentral distance
        }

        // Find channel's picker params in lookup map intialized from data file named in properties
        Channel chan = wf.getChannelObj();
        String sncl = chan.toDelimitedSeedNameString(".");

        if (sol == null) {
            //if (debug || verbose) 
            System.err.printf("ERROR %s input Solution is null: %s%n", name, sncl);
            //return phList; // bail if no solution for epicentral distance
        }

        boolean solHasLatLonZ = sol.hasLatLonZ();
        
        if (distFilter && !solHasLatLonZ) {
            System.err.printf("ERROR %s distance filtering and input Solution does not have a location: %s%n", name, sncl);
            return phList;
        }
        
        if (!solHasLatLonZ || (!distFilter && !chan.hasLatLonZ())) {
            return pick(wf, phasesToPickFlag, gainUnitsFilter);
        }

        if (!sol.hasValidDateTime()) {
            //if (debug || verbose) 
                System.err.printf(" INFO %s solution %s origin time invalid: %s%n", sol.getId(), name, sncl);
            return phList; // bail if no origin time
        }

        if (! sol.depth.isValidNumber()) {
            // if (debug || verbose)
              System.err.printf(" INFO %s solution depth %s invalid: %s%n", sol.getId(), name, sncl);
            return phList; // bail if no solution depth
        }

        double hDist = Channel.NULL_DIST;
        if (solHasLatLonZ) {
            chan.calcDistance(sol.getLatLonZ());
            hDist = chan.getHorizontalDistance();
        }
        if (hDist == Channel.NULL_DIST) {
            if (debug || verbose) System.out.printf(" INFO %s channel has unknown distance, check Lat,Lon: %s%n", name, sncl);
            return phList; // bail if no epicentral distance
        }

        if (distFilter) {
            if (hDist > maxDistance) {
                if (debug) {
                  System.out.printf(" INFO %s skipping %s, distance %5.1f > %5.1f max distance property%n", name, sncl, hDist, maxDistance);
                }
                return phList; // bail if distance too great
            }

            Magnitude prefmag = sol.getPreferredMagnitude();
            double mag = (prefmag == null || prefmag.hasNullValue()) ? DEFAULT_NULL_MAG_VALUE : prefmag.getMagValue();
            double maxMagDist = getMaxDistance(mag);
            if (hDist > maxMagDist) {
                if (debug) {
                    System.out.printf(" INFO %s skipping %s, distance %5.1f > %5.1f max km allowed for M%3.1f%n", name, sncl, hDist, maxMagDist, mag);
                }
                return phList; // bail distance too great
            }
        } 

        TravelTime ttModel = wf.getTravelTimeModel();
        if (ttModel == null) {
            System.err.printf("ERROR %s Waveform TravelTimeModel is NULL: %s%n", name, sncl);
            return phList; //bail
        }

        // Reject ACC low-gain by property setting
        if (gainUnitsFilter && velocityOnly && !chan.isVelocity()) {
            if (debug) System.out.printf(" INFO %s velocityOnly and waveform channel gain not velocity units:%s%n", name, sncl);
            return phList; //bail
        }

        if (reject(wf)) {
            if (debug) System.out.printf(" INFO %s channel rejected by SNCL attribute or its absence from candidate list:%s%n", name, sncl);
            return phList;
        }

        PickerParmIF snclMappedParm = null;
        int phFlag = phasesToPickFlag;
        if (chanParmMap == null || chanParmMap.size() == 0) { // Use default global parameters
            //return phList;
            System.out.printf(" INFO: %s using DefaultParms for: %s%n", name, sncl);
            snclMappedParm = (PickerParmIF) getDefaultParm().clone();
            snclMappedParm.setSNCL(sncl);
            snclMappedParm.setPickFlag(phasesToPickFlag);
            if (phFlag >= PhasePickerIF.DEFAULT) { // Configure by property settings
                if (chan.isHorizontal() && !PonH) {
                  snclMappedParm.setPickFlag(PhasePickerIF.S_ONLY); // only S allowed on horizontals
                }
                else if (chan.isVertical() && !SonV) {
                  snclMappedParm.setPickFlag(PhasePickerIF.P_ONLY); // only P allowed on verticals
                }
            }
            //Don't add since it would invalidate having no map defined as config
            //addChannelParm(sncl, snclMappedParm); // add to or create new map,
        }
        else {
            snclMappedParm = (PickerParmIF) getChannelParm(sncl);
            if (snclMappedParm == null) {
                if (debug) {
                    System.out.printf(" INFO %s picking aborted, no parameters mapped for: %s%n", name, sncl);
                }
                return phList;
            }
            else if (!sncl.equals(snclMappedParm.getSNCL())) {
                System.err.printf("ERROR! %s waveform sncl != parm.sncl \"%s\" != \"%s\"%n", name, sncl, snclMappedParm.getSNCL());
            }
        }

        if (debug) System.out.println(snclMappedParm.toString());

        phFlag = phasesToPickFlag;

        if (phFlag >= PhasePickerIF.DEFAULT) {
            phFlag = snclMappedParm.getPickFlag();
            if (phFlag >=PhasePickerIF.DEFAULT) phFlag = 0; // undefined state, don't pick
        }
        if (debug) System.out.printf("DEBUG %s phFlag: %d%n", name, phFlag);
        if (phFlag <= 0) {
            if (debug) System.out.printf(" INFO %s channel flag is set to NO picks: %s%n", name, sncl);
            return phList; // don't pick
        }

        //if (chan.isHorizontal() && phFlag == P_ONLY && !PonH) return phList; // bail if only P-picking and no P allowed on horizontals
        //if (chan.isVertical() && phFlag == S_ONLY && !SonV) return phList; // bail if only S-picking and no S allowed on verticals
        // Else we try to pick one or more a phases

        // Determine Phase travel times to station from event origin
        double depth = sol.getModelDepth(); // aww 2015/10/10
        double ot =  sol.getTime();
        double ttp = ttModel.getTTp(hDist, depth);
        double tts = ttModel.getTTs(hDist, depth);

        boolean localLoad = false;
        if (!wf.hasTimeSeries())  { // try to load it
            if (wf.loadTimeSeries()) {
                localLoad = true;
            }
            else {
                if (debug) System.out.printf(" INFO %s no waveform timeseries for %s%n", name, sncl);
                return phList; // bail
            }
        }

        boolean havePtime = false;
        boolean haveStime = false;
        WFSegment[] segs = wf.getArray();
        // TOP OF LOOP over all WFSegment's for input channel waveform, does its timeseries contain the desired times?
        for (int idx = 0; idx<segs.length; idx++) {
            if ( (phFlag == P_ONLY) && (segs[idx].sampleIndexAtTime(ot+ttp) >= 0) ) {
                    havePtime = true;
                    break;
            }
            else if ( (phFlag == S_ONLY) && (segs[idx].sampleIndexAtTime(ot+tts) >= 0) ) {
                    haveStime = true;
                    break;
            }
            else if (phFlag == P_AND_S) {
                if (segs[idx].sampleIndexAtTime(ot+ttp) >= 0) havePtime = true;
                if (segs[idx].sampleIndexAtTime(ot+tts) >= 0) haveStime = true;
                if (havePtime && haveStime) break;
            }
        }
        if (!havePtime && !haveStime) {
            if (debug) System.out.printf(" INFO %s predicted P or S time not within waveform timespan: %s%n", name, sncl);
            if (localLoad) wf.unloadTimeSeries(); // aww added 2016/12/30
            return phList; // bail, nothing to pick
        }

        // Scan data to get the averages, but when do we start the real picking?
        // Do we want to start at the greater of the (ot+ttp-X) secs or at the beginning of the waveform timeseries?
        //
        //double spTime = (tts-ttp);
        // Determine desired pick window times for P:
        double pStartTime = ot + ttp - windowHalfWidth;
        double pEndTime = ot + ttp + windowHalfWidth;
        //double pEndTime = ot + ttp + Math.min(spTime, windowHalfWidth); // for small S-P time override current halfwidth ?

        TimeSpan pSpan = new TimeSpan(pStartTime, pEndTime);
        if (debug) System.out.printf("DEBUG %s P timespan: %s%n", name, pSpan);

        // Determine desired pick window times for S:
        double sStartTime = ot + tts - windowHalfWidth;
        //double sStartTime = ot + tts - Math.min(spTime, windowHalfWidth); // for small S-P time override current halfwidth ?
        double sEndTime = ot + tts + windowHalfWidth;

        TimeSpan sSpan = new TimeSpan(sStartTime, sEndTime);
        if (debug) System.out.printf("DEBUG %s S timespan: %s%n", name, sSpan);

        // Determine start time for input segments: 
        double scanStartTime = wf.getEpochStart();
        // Force always to start before expected P to get the background stats
        if (phFlag == P_AND_S || phFlag == P_ONLY)
            scanStartTime = Math.max(scanStartTime, (pStartTime - scanStartOffset));
        else if (phFlag == S_ONLY) scanStartTime = Math.max(scanStartTime, (sStartTime - scanStartOffset));

        // Determine end time for input segments: 
        double scanEndTime = wf.getEpochEnd();
        if (phFlag == P_ONLY) scanEndTime = Math.min(scanEndTime, pEndTime + scanEndOffset);
        else if (phFlag == P_AND_S || phFlag == S_ONLY) scanEndTime = Math.min(scanEndTime, sEndTime + scanEndOffset);

        if (debug) {
            System.out.printf("DEBUG %s scanStartOffset: %5.2f scanEndOffset: %5.2f%n", name, scanStartOffset, scanEndOffset);
            System.out.printf("DEBUG %s desired scanStart->scanEnd       : %s -> %s%n",
                                  name, LeapSeconds.trueToString(scanStartTime), LeapSeconds.trueToString(scanEndTime));
        }

        if (debug) System.out.printf(" INFO %s scanning %s timeseries for picks...%n", name, sncl);
        phList = pick(wf, phFlag, snclMappedParm, new TimeSpan(scanStartTime, scanEndTime), pSpan, sSpan);

        if (localLoad) wf.unloadTimeSeries(); // unload if loaded, but does this conflict with wfcache loader for gui ?

        return phList;

    } // end of pick method

    public PhaseList pick(Waveform wf, int phasesToPickFlag, boolean gainUnitsFilter) {
        PhaseList phList = new PhaseList(2); // new list for P and/or S pick(s)

        if (wf == null) {
            //if (debug || verbose)
                System.err.printf("ERROR %s input Waveform is null!%n", name);
            return phList; // bail if no solution for epicentral distance
        }

        // Find channel's picker params in lookup map intialized from data file named in properties
        Channel chan = wf.getChannelObj();
        String sncl = chan.toDelimitedSeedNameString(".");

        // Reject ACC low-gain by property setting
        if (gainUnitsFilter && velocityOnly && !chan.isVelocity()) {
            if (debug) System.out.printf(" INFO %s velocityOnly and waveform channel gain not velocity units.%n", name);
            return phList; //bail
        }

        if (reject(wf)) {
            if (debug) System.out.printf(" INFO %s channel rejected by SNCL attribute or its absence from candidate list.%n", name, sncl);
            return phList;
        }

        PickerParmIF snclMappedParm = null;
        int phFlag = phasesToPickFlag;
        if (chanParmMap == null || chanParmMap.size() == 0) { // Use default global parameters
            //return phList;
            System.out.printf(" INFO: %s using DefaultParms for: %s%n", name, sncl);
            snclMappedParm = (PickerParmIF) getDefaultParm().clone();
            snclMappedParm.setSNCL(sncl);
            snclMappedParm.setPickFlag(phasesToPickFlag);
            if (phFlag >= PhasePickerIF.DEFAULT) { // Configure by property settings
                if (chan.isHorizontal() && !PonH) {
                  snclMappedParm.setPickFlag(PhasePickerIF.S_ONLY); // only S allowed on horizontals
                }
                else if (chan.isVertical() && !SonV) {
                  snclMappedParm.setPickFlag(PhasePickerIF.P_ONLY); // only P allowed on verticals
                }
            }
            //Don't add since it would invalidate having no map defined as config
            //addChannelParm(sncl, snclMappedParm); // add to or create new map,
        }
        else {
            snclMappedParm = (PickerParmIF) getChannelParm(sncl);
            if (snclMappedParm == null) {
                if (debug) {
                    System.out.printf(" INFO %s picking aborted, no parameters mapped for: %s%n", name, sncl);
                }
                return phList;
            }
            else if (!sncl.equals(snclMappedParm.getSNCL())) {
                System.err.printf("ERROR! %s waveform sncl != parm.sncl \"%s\" != \"%s\"%n", name, sncl, snclMappedParm.getSNCL());
            }
        }

        if (debug) System.out.println(snclMappedParm.toString());

        phFlag = phasesToPickFlag;
        if (phFlag >= PhasePickerIF.DEFAULT) {
            phFlag = snclMappedParm.getPickFlag();
            if (phFlag >=PhasePickerIF.DEFAULT) phFlag = 0; // undefined state, don't pick
        }
        if (debug) System.out.printf("DEBUG %s phFlag: %d%n", name, phFlag);
        if (phFlag <= 0) {
            if (debug) System.out.printf(" INFO %s channel flag is set to NO picks: %s%n", name, sncl);
            return phList; // don't pick
        }

        //if (chan.isHorizontal() && phFlag == P_ONLY && !PonH) return phList; // bail if only P-picking and no P allowed on horizontals
        //if (chan.isVertical() && phFlag == S_ONLY && !SonV) return phList; // bail if only S-picking and no S allowed on verticals
        // Else we try to pick one or more a phases

        boolean localLoad = false;
        if (!wf.hasTimeSeries())  { // try to load it
            if (wf.loadTimeSeries()) {
                localLoad = true;
            }
            else {
                if (debug) System.out.printf(" INFO %s no waveform timeseries for %s%n", name, sncl);
                return phList; // bail
            }
        }

        if (debug) System.out.printf(" INFO %s scanning %s timeseries for picks...%n", name, sncl);
        double scanStartTime = wf.getEpochStart(); // + scanStartOffset; // do not add in offset
        double scanEndTime = wf.getEpochEnd();
        TimeSpan ts = new TimeSpan(scanStartTime,scanEndTime);
        phList = pick(wf, phFlag, snclMappedParm,  ts, ts, ts);

        if (localLoad) wf.unloadTimeSeries(); // unload if loaded, but does this conflict with wfcache loader for gui ?

        return phList;
    }

    /**
    * Interpolate samples and insert them at the beginning of the waveform timeseries.
    */
    // NOTE: Do we want to pass a copied wfseg segment and return this copy with expanded timeseries?
    public static WFSegment interpolate(double lastSampleValue, double lastSampleTime, WFSegment nextSeg, int gapSize ) {
        WFSegment wfseg = new WFSegment(nextSeg); // we want to alter a copied segment, not the original?
        float[] ts = wfseg.getTimeSeries();
        int nInterp = gapSize - 1;
        float[] ts2 = new float[ts.length+nInterp];
        System.arraycopy(ts, 0, ts2, nInterp, ts.length);
        double delta = (double)(ts[0] - lastSampleValue) / gapSize;
        for (int i=0; i<nInterp; i++) {
            ts2[i] = (float) (lastSampleValue + (i+1)*delta); // not rounded to int here
        }
        wfseg.setTimeSeries(ts2, true); // do scan for bias/peak amps
        wfseg.samplesExpected += nInterp;
        wfseg.setStart(lastSampleTime - nInterp*wfseg.getSampleInterval()); // epoch UTC start time
        return wfseg;
    }

    public void clearChannelParms() {
        if (chanParmMap != null) chanParmMap.clear();
        //chanParmMap = null;
    }

    public boolean saveChannelParm() {

        if (absChanParmFile == null || absChanParmFile.length() == 0) {
            System.err.printf(" INFO: %s saveChannelParms channel parm filename is undefined%n", name);
            return false; 
        }

        java.util.List aList = getChannelParms();
        if (aList == null) aList = new ArrayList(0);
        //if (aList.size() == 0) return true; // go ahead any way for empty file

        boolean status = true;
        BufferedWriter writer = null;
        try  {
            writer = new BufferedWriter(new FileWriter(absChanParmFile));
            PickerParmIF parm = null;
            for (int idx=0; idx<aList.size(); idx++) {
                parm = (PickerParmIF) aList.get(idx);
                writer.write(toParseableString(parm));
                writer.newLine();
            }
            writer.close();
        }
        catch (Exception ex) {
            status = false;
            try {
                if (writer != null) {
                    writer.close();
                }
            }
            catch (Exception ex2) {
                System.err.println(ex.getMessage());
            }
        }

        return status;
    }

    public boolean loadChannelParms() {
        clearChannelParms();
        final String fileName = absChanParmFile;
        if (fileName == null || fileName.length() == 0) {
            System.out.printf(" INFO: %s loadChannelParms channel parm filename is undefined, using default parm mapping%n", name);
            return true;
        }

        int scannedLines = 0;
        boolean status = true;
        try (Scanner scanner = new Scanner(new BufferedReader(new FileReader(fileName)))) {
          PickerParmIF parm = null;
          String parmLineStr = null;
          if (chanParmMap == null) chanParmMap = new HashMap<>(101);
          while (scanner.hasNextLine()) { // is there data ?
            parmLineStr = scanner.nextLine();
            scannedLines++;
            if (isComment( parmLineStr )) continue;
            parm = (PickerParmIF) parseChannelParm(parmLineStr);

            if (parm == null) {
                System.err.printf("ERROR %s loadChannelParms read failed at input line: %d%n%s%n", name, scannedLines, parmLineStr);
                status = false;
                break;
            }
            else {
                chanParmMap.put(parm.getSNCL(), parm);
            }
          }
        }
        catch (FileNotFoundException ex) {
            System.err.printf("ERROR: %s loadChannelParms channel parm filename not found: %s%n", name, fileName);
            System.err.println(ex.getMessage());
            return false; 
        }
        catch(NoSuchElementException ex) {
            System.err.printf("ERROR: %s scanner haveNextLine no more lines, at %d scannedLines from top%n", name, scannedLines);
            System.err.println(ex.getMessage());
            status = false;
        }
        catch(IllegalStateException ex) {
            System.err.printf("ERROR: %s scanner haveNextLine scanner closed, at %d scannedLines from top%n", name, scannedLines);
            System.err.println(ex.getMessage());
            status = false;
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        finally {
          if (debug || !status) {
              System.err.println("=====================================================================");
              System.err.printf("\nDEBUG %s loadChannelParms scanning file %s return status=%s, scannedLines=%d%n",
                      name, fileName, status, scannedLines);
          }
        }

        return status;
    }

    public boolean isComment(String str) {
        return (str == null || str.trim().length() == 0 || str.startsWith("#") );
    }

    /**
    * Parse picker channel parameters from text line
    */
    public abstract PickerParmIF parseChannelParm(String parmLineStr); 

    public void printChannelParms() {
        // Start with #, a commment
        System.out.printf("# %s channel parms for %d channels%n", name, ((chanParmMap == null) ? 0 : chanParmMap.size()));
        if (chanParmMap == null || chanParmMap.size() <= 0) return;
        Set keys = chanParmMap.keySet();
        ArrayList aList = new ArrayList(keys);
        Collections.sort(aList);
        PickerParmIF parm = null;
        for (int idx=0; idx<aList.size(); idx++) {
            parm =  (PickerParmIF) chanParmMap.get( aList.get(idx) );
            if (idx == 0) System.out.println( parm.getFormattedHeader() );
            else System.out.println( parm.toFormattedString() );
        }
        System.out.printf("%n");
    }

    public void printChannelParm(Channelable ch) {
        if (ch == null || ch.getChannelObj() == null) return;
        printChannelParm(ch.getChannelObj().toDelimitedSeedNameString("."));
    }

    public void printChannelParm(String sncl) {
        if (chanParmMap == null) return;
        PickerParmIF parm = (PickerParmIF) chanParmMap.get(sncl);
        if (parm == null) return;
        System.out.println(parm.toFormattedString());
    }

    public PickerParmIF getChannelParm(Channelable ch) {
        if (ch == null) return null;
        Channel chan = ch.getChannelObj();
        if (chan == null) return null;
        return getChannelParm(chan.getChannelObj().toDelimitedSeedNameString("."));
    }

    public PickerParmIF getChannelParm(String sncl) {
        if (chanParmMap == null || chanParmMap.size() == 0) return null;
        return (PickerParmIF) chanParmMap.get(sncl);
    }

    public java.util.List getChannelParms() {

        //if (chanParmMap == null) return new ArrayList(0);
        if (chanParmMap == null) return null;

        Set keys = chanParmMap.keySet();
        ArrayList keyList = new ArrayList(keys);
        Collections.sort(keyList);
        ArrayList valuesList = new ArrayList(keyList.size());
        for (int idx=0; idx<keyList.size(); idx++) {
            valuesList.add( chanParmMap.get(keyList.get(idx)) );
        }
        return valuesList; 
    }

    public PickerParmIF addChannelParm(String sncl, PickerParmIF parm) {
        if (sncl == null) throw new NullPointerException("Input sncl string cannot be null");
        if (parm == null) throw new NullPointerException("Input parm cannot be null"); // input could be default parm copy
        parm.setSNCL(sncl);
        if (chanParmMap == null) chanParmMap = new HashMap(31);
        return (PickerParmIF) chanParmMap.put(sncl, parm);
    }

    public PickerParmIF addChannelParm(Channelable ch, PickerParmIF parm) {
        if (ch == null) throw new NullPointerException("Input Channelable cannot be null");
        return addChannelParm(ch.toDelimitedSeedNameString("."), parm);
    }
    public PickerParmIF addChannelParm(PickerParmIF parm) {
        if (parm == null) throw new NullPointerException("Input parm cannot be null");
        return addChannelParm(parm.getSNCL(), parm);
    }

    public void addChannelParms(List parmList) {
        if (parmList == null) return;
        PickerParmIF parm = null;
        for (int idx=0; idx<parmList.size(); idx++) {
            parm = (PickerParmIF) parmList.get(idx);
            addChannelParm(parm.getSNCL(), parm);
        }
    }

    public PickerParmIF removeChannelParm(String sncl) {
        return (PickerParmIF) chanParmMap.remove(sncl);
    }
    public PickerParmIF removeChannelParm(Channelable ch) {
        return removeChannelParm(ch.getChannelObj().toDelimitedSeedNameString("."));
    }
    public PickerParmIF removeChannelParm(PickerParmIF parm) {
        return removeChannelParm(parm.getSNCL());
    }

    public abstract JComponent getPropertyEditor(GenericPropertyList gpl);
    public abstract JComponent getPropertyEditor(AutoPickGeneratorIF apg, GenericPropertyList gpl);
    public abstract JComponent getChannelParmEditor(String sncl);

    public JComponent getChannelParmEditor(Channelable ch) {
        String sncl = (ch == null) ? null : ch.toDelimitedSeedNameString(".");
        return getChannelParmEditor(sncl);
    }

    /**
        Rotates horizontal components of a seismogram. (code ported from from Obspy rotate.py)
        input North and East components of a seismogram are rotated to Radial and Transverse.
        input back-azimuth, ba, is angle from station back to source measured from North degrees.
        Input radial and transverse component arrays (r,t) are populated.
    */
    public static void rotate_NE_RT(double[] n, double[] e, double[] r, double[] t, double ba) {
        if ( n.length != e.length || r.length != t.length || n.length != r.length ) {
            throw new IllegalArgumentException("All input sample arrays must have same length");
        }
        if ( ba < 0. || ba > 360. ) {
            throw new IllegalArgumentException("Input back azimuth should be between 0. and 360. degrees.");
        }

        ba = (ba+180.)*2.*Math.PI/360.;

        for (int i=0; i < n.length; i++) {
            r[i] = e[i] * Math.sin(ba) + n[i] * Math.cos(ba);
            t[i] = e[i] * Math.cos(ba) - n[i] * Math.sin(ba);
        }
    }

    /**
        Rotates horizontal components of a seismogram. (code ported from from Obspy rotate.py)
        Rotates from radial and transverse components to North and East.
        Inverse of rotate_NE_RT.
    */
    public static void rotate_RT_NE(double[] n, double[] e, double[] r, double[] t, double ba) {
        rotate_NE_RT(n, e, r, t, 360.-ba);
    }

    /**
    Rotates all components of a seismogram. (code ported from from Obspy rotate.py)
    The components will be rotated from ZNE (Z, North, East, left-handed) to
    LQT (e.g. ray coordinate system, right-handed). The rotation angles are
    given as the back-azimuth and inclination.

    The transformation consists of 3 steps::
        1. mirroring of E-component at ZN plane: ZNE to ZNW
        2. negative rotation of coordinate system around Z-axis with angle ba: ZNW to ZRT
        3. negative rotation of coordinate system around T-axis with angle inc: ZRT to LQT
    Inputs:
    z   : Z component time series
    n   : North component time series
    e   : East component time series
    ba  : The back azimuth from station to source in degrees.
    inc : The inclination of the ray at the station in degrees.

    Returns L, Q and T components time series.
    */
    public void rotate_ZNE_LQT(double[] z, double[] n, double[] e, double[] l, double[] q, double[] t, double ba, double inc) {
        if (z.length != n.length || z.length != e.length) {
            throw new IllegalArgumentException("Input Z, North and East component time series arrays have different lengths!");
        }
        if (z.length != l.length || z.length != q.length || z.length != t.length) {
            throw new IllegalArgumentException("Input L, Q and T component time series output arrays must have same length as Z input!");
        }
        if ( ba < 0. || ba > 360. ) {
            throw new IllegalArgumentException("Input back azimuth should be between 0. and 360. degrees.");
        }
        if ( inc < 0. || inc > 360. ) {
            throw new IllegalArgumentException("Input inclination should be between 0. and 360. degrees.");
        }
        ba *= 2. * Math.PI/360.;
        inc *= 2. * Math.PI/360.;
        for (int idx=0; idx < z.length; idx++) {
            l[idx] = z[idx] * Math.cos(inc) - n[idx] * Math.sin(inc) * Math.cos(ba) - e[idx] * Math.sin(inc) * Math.sin(ba);
            q[idx] = z[idx] * Math.sin(inc) + n[idx] * Math.cos(inc) * Math.cos(ba) + e[idx] * Math.cos(inc) * Math.sin(ba);
            t[idx] = n[idx] * Math.sin(ba) - e[idx] * Math.cos(ba);
        }
    }


    /**
    The components will be rotated from LQT to ZNE. (code ported from from Obspy rotate.py)
    This is the inverse transformation of the transformation described in method rotate_ZNE_LQT.
    **/
    public void rotate_LQT_ZNE(double[] z, double[] n, double[] e, double[] l, double[] q, double[] t, double ba, double inc) {
        if (l.length != q.length || l.length != t.length) {
            throw new IllegalArgumentException("Input L, T and Q component time series arrays have different lengths!");
        }
        if ( ba < 0. || ba > 360. ) {
            throw new IllegalArgumentException("Input back azimuth should be between 0. and 360. degrees.");
        }
        if ( inc < 0. || inc > 360. ) {
            throw new IllegalArgumentException("Input inclination should be between 0. and 360. degrees.");
        }
        ba *= 2. * Math.PI/360.;
        inc *= 2. * Math.PI/360.;
        for (int idx=0; idx < l.length; idx++) {
            z[idx] =  l[idx] * Math.cos(inc) + q[idx] * Math.sin(inc);
            n[idx] = -l[idx] * Math.sin(inc) * Math.cos(ba) + q[idx] * Math.cos(inc) * Math.cos(ba) + t[idx] * Math.sin(ba);
            e[idx] = -l[idx] * Math.sin(inc) * Math.sin(ba) + q[idx] * Math.cos(inc) * Math.sin(ba) - t[idx] * Math.cos(ba);
        }
    }

} // End of AbstractPicker class 
