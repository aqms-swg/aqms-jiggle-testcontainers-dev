package org.trinet.jasi.picker;
public interface PickerParmIF {
    public String getFormattedHeader();
    public String toFormattedString();
    public String toParseableString();
    //public String toLabelledString();
    public void copy(PickerParmIF parm);
    public void setSNCL(String sncl);
    public String getSNCL();
    public void setPickFlag(int phasesToPickFlag);
    public int getPickFlag();
    public PickerParmIF clone();
}
