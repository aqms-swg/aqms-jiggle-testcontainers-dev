package org.trinet.jasi.picker;

import java.util.Arrays;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
// For jasi waveforms and events
import org.trinet.filters.ButterworthFilterSMC;
import org.trinet.jasi.AbstractWaveform;
import org.trinet.jasi.Channel;
import org.trinet.jasi.ChannelableList;
import org.trinet.jasi.ChannelableListIF;
import org.trinet.jasi.ChannelGain;
import org.trinet.jasi.JasiProcessingConstants;
import org.trinet.jasi.Phase;
import org.trinet.jasi.PhaseDescription;
import org.trinet.jasi.PhaseList;
import org.trinet.jasi.Solution;
import org.trinet.jasi.StationGrouper;
import org.trinet.jasi.TravelTime;
import org.trinet.jasi.TriaxialGrouper;
import org.trinet.jasi.Waveform;
import org.trinet.jasi.WFSegment;

import org.trinet.util.DateTime;
import org.trinet.util.Format;
import org.trinet.util.GenericPropertyList;
import org.trinet.util.LeapSeconds;
import org.trinet.util.TimeSpan;

public class TriaxialPicker2 extends PickEW implements TriaxialPickerIF {

    // Native shared object library containing Z.Ross's C++ functions for JNI
    static { System.loadLibrary("dbshear"); }
    public native double[] pick_native(double[] N, double[] E, double[] Z, double w_len, double dt, double k_len);
    //

    private double covarianceWindowSecs = 3.0; // default of 3 seconds suggested by Z Ross 2015
    private double kLengthSecs = 10.0; // default to 10 or 1 seconds?  Z Ross 2016
    private boolean rotateHoriz = false;

    private boolean doBWFilter = true;
    private ButterworthFilterSMC bwf = null;
    private static final int DEFAULT_BWF_TYPE = 1; // 0 = HIGHPASS,  1 = BANDPASS 
    private int    bwfType = DEFAULT_BWF_TYPE;
    private boolean bwfScalePassBandByDistance = false;
    private double bwfLoFreq = 3.00; // default of 3 Hz suggested by Z Ross 2015
    private double bwfHiFreq = 19.99; // Note for 40 sps stream, can't be exactly 20 Hz or  20sps => 9.99
    private int    bwfOrder = 2; // NOTE value must be even number, does not filter unless order is > 0 by property setting
    private double bwfMinPeriod = .04;
    private boolean bwfReversed = false;
    private Solution mySol = null;
    private Format bwfFmt = new Format("%6.2f");
    private String bwfTypeStr = (bwfType == 0) ?
        "Butterworth_HP_" + bwfFmt.form(bwfLoFreq).trim() + "_" + String.valueOf(bwfOrder): 
        "Butterworth_BP_" + bwfFmt.form(bwfLoFreq).trim() + "_" + bwfFmt.form(bwfHiFreq).trim() + "_" + String.valueOf(bwfOrder); 


    public TriaxialPicker2() { } 

    public TriaxialPicker2(GenericPropertyList gpl) {
        setProperties(gpl);
    }

    public void setDefaultProperties() {
        super.setDefaultProperties();
        setMyDefaultProperties();
    }

    private void setMyDefaultProperties() {
        name = "TriaxialPicker2";
        doBWFilter = true;
        bwfType = DEFAULT_BWF_TYPE; 
        bwfOrder = 2; // BW_SMC filter implementation requires even number here
        bwfHiFreq = 19.99;
        bwfLoFreq = 3.00;
        covarianceWindowSecs = 3.0;
        kLengthSecs = 10.0;
        rotateHoriz = false;
    }
    
    public void setProperties(GenericPropertyList gpl) {

        super.setProperties(gpl, false);
        setMyDefaultProperties();

        String pre = getPropertyPrefix();

        if (gpl.getProperty(pre+"bwFilterEnabled") != null) { 
          doBWFilter = gpl.getBoolean(pre+"bwFilterEnabled", true);
        }
        if (gpl.getProperty(pre+"bwFilterType") != null) { 
          if( gpl.getProperty(pre+"bwFilterType", "BANDPASS").toUpperCase().startsWith("HIGH") ) bwfType = 0;
        }
        if (gpl.getProperty(pre+"bwFilterLoFreq") != null) { 
          bwfLoFreq = gpl.getDouble(pre+"bwFilterLoFreq");
        }
        if (gpl.getProperty(pre+"bwFilterHiFreq") != null) { 
          bwfHiFreq = gpl.getDouble(pre+"bwFilterHiFreq");
        }
        if (gpl.getProperty(pre+"bwFilterOrder") != null) { 
          bwfOrder = Math.min(gpl.getInt(pre+"bwFilterOrder"), 2);
          if (bwfOrder < 0) bwfOrder = 0;
        }
        if (gpl.getProperty(pre+"bwFilterScalePassBandByDistance") != null) { 
          bwfScalePassBandByDistance = gpl.getBoolean(pre+"bwFilterScalePassBandByDistance", false);
        }
        if (gpl.getProperty(pre+"bwFilterReversed") != null) { 
          bwfReversed = gpl.getBoolean(pre+"bwFilterReversed", false);
        }

        if (gpl.getProperty(pre+"covarianceWindowSecs") != null) { 
          covarianceWindowSecs = gpl.getDouble(pre+"covarianceWindowSecs");
        }

        if (gpl.getProperty(pre+"kLengthSecs") != null) { 
          kLengthSecs = gpl.getDouble(pre+"kLengthSecs");
        }

        if (gpl.getProperty(pre+"rotateHoriz") != null) { 
          rotateHoriz = gpl.getBoolean(pre+"rotateHoriz", true);
        }

        if (debug || verbose) {
            getProperties(new GenericPropertyList()).dumpProperties();
        }
    }

    public GenericPropertyList getProperties(GenericPropertyList gpl) {

        GenericPropertyList newgpl = super.getProperties(gpl);
        if (gpl == null) gpl = new GenericPropertyList();

        String pre = getPropertyPrefix();

        newgpl.setProperty(pre+"bwFilterEnabled", doBWFilter);
        newgpl.setProperty(pre+"bwFilterType", ((bwfType == 0) ? "HIGHPASS" : "BANDPASS"));
        newgpl.setProperty(pre+"bwFilterOrder", bwfOrder);
        newgpl.setProperty(pre+"bwFilterLoFreq", bwfLoFreq);
        newgpl.setProperty(pre+"bwFilterHiFreq", bwfHiFreq);
        newgpl.setProperty(pre+"bwFilterReversed", bwfReversed);
        newgpl.setProperty(pre+"covarianceWindowSecs", covarianceWindowSecs);
        newgpl.setProperty(pre+"kLengthSecs", kLengthSecs);
        newgpl.setProperty(pre+"rotateHoriz", rotateHoriz);

        return newgpl;
    }

    // Need implementation of parent class abstract method for handling the picking of a single component Z station, e.g. EHZ, HNZ
    // no picking when missing Z and just E and N components available?
    /*
     * @param phasesToPickFlag PhasePickerIF.NONE, do not pick, a no-op, ignores input sncl mapped PickerParmIF flag.
     * @param phasesToPickFlag PhasePickerIF.P_ONLY, P arrival, ignores input sncl mapped PickerParmIF flag 
     * @param phasesToPickFlag PhasePickerIF.S_ONLY, S arrival, ignores input sncl mapped PickerParmIF flag.
     * @param phasesToPickFlag PhasePickerIF.P_AND_S, both arrivals, ignores input sncl mapped PickerParmIF flag.
     * @param phasesToPickFlag PhasePickerIF.DEFAULT, uses input sncl napped PickerParmIF flag, if none, use defaults by component type.
     * @param snclMappedParm  picker configuration parameters mapped to String of the form: net.sta.seed.loc derived from input waveform Channel.
     * @param scanSpan timespan over which to scan waveform timeseries for phase picks
     * @param pSpan timespan over which to assign a P pick.
     * @param sSpan timespan over which to assign a S pick.
     * @return list containing phases or empty list if no picks were made
     */
    public PhaseList pick(ChannelableList wfList, Solution sol, int phasesToPickFlag, boolean gainUnitsFilter, boolean distFilter) {

        if ( wfList == null || sol == null ) { 
            throw new NullPointerException("input wfList or sol parameter is null");
        }
        mySol = sol;

        if ( distFilter )  {
            wfList.distanceSort(sol);
        }

        PhaseList phList = new PhaseList(wfList.size()); // collection to hold picks for all stations in list, returned

        // Group input waveform channels by SNCL and distance
        TriaxialGrouper grouper = new TriaxialGrouper(wfList, StationGrouper.DIST_SORT);
        ChannelableListIF staList = null;
        Waveform wfZ = null;
        Waveform wfN = null;
        Waveform wfE = null;
        PhaseList phList2 = null; // picks for 1 station in loop
        Channel ch = null;
        Waveform wf = null;
        while (grouper.hasMoreGroups()) { // go through station 3-component subgroups
                staList = grouper.getNext(); // output list object is same class type as constructor list
                ch = ((Waveform) staList.get(0)).getChannelObj();
                // Below assumes wf list groups are distance sorted
                if ( distFilter) {
                    if ( ! withinPickerRange(ch, sol, "P") ) break;
                }

                if (staList.size() == 3) {  // can try to do S pick using 3-comps picking
                    wfZ = null;
                    wfN = null;
                    wfE = null;
                    for (int idx=0; idx<staList.size(); idx++) {
                        wf = ((Waveform)staList.get(idx));
                        if ( wf.getChannelObj().isVertical() ) {
                            wfZ = wf;
                        }
                        else { 
                           double azi = wf.getChannelObj().getSensorAzimuth();
                           if (( azi >= 315. || azi <= 45 ) || ( azi >= 135. && azi <= 225. )) {
                               wfN = wf;
                           }
                           else {
                               wfE = wf;
                           }
                        }
                    }
                    if ( withinPickerRange(ch, sol, "S") ) {
                          // note distance filter test above breaks loop before here when outside range
                          phList2 = pick(wfN, wfE, wfZ, sol, PhasePickerIF.DEFAULT, gainUnitsFilter, false);
                          //if (debug && phList2 != null) System.out.printf("DEBUG: %s N,E,Z S-phase picked:%n%s%n", name, phList2.toNeatString(true));
                          if (phList2 != null) System.out.printf("DEBUG: %s N,E,Z S-phase picked:%n%s%n", name, phList2.toNeatString(true));
                          if (phList2 != null && phList2.size() > 0) phList.addAll(phList2);
                    }

                    // Instead, try to P pick Z component
                    phList2 = pick(wfZ, sol, PhasePickerIF.DEFAULT, gainUnitsFilter, false);
                    if (phList2 != null && phList2.size() > 0) phList.addAll(phList2);
                    if (debug && phList2 != null) System.out.printf("DEBUG: %s Z P-phase picked:%n%s%n", name, phList2.toNeatString(true));
                }
                else {
                    if (debug) System.out.printf("DEBUG: %s sta group missing 3 components, not picking S for %s.%s%n", name, ch.getNet(), ch.getSta());
                    // Do we want to just try Z component P pick or E/N S pick or no-op here?
                    // Any way to handle case of missing Z but having N/E components available?
                    for (int idx=0; idx<staList.size(); idx++) {
                        wf = ((Waveform)staList.get(idx));
                        if ( wf.getChannelObj().isVertical() ) {
                            // Try P pick on Z component
                            phList2 = pick(wf, sol, PhasePickerIF.DEFAULT, gainUnitsFilter, false);
                            if (debug && phList2 != null) System.out.printf("DEBUG: %s Z P-phase picked:%n%s%n", name, phList2.toNeatString(true));
                            break;
                        }
                    }
                    if (phList2 != null && phList2.size() > 0) phList.addAll(phList2);
                    //
                }
        }
        return phList; // should contain picks made for all input station triaxial groups
    }

    /** Does 3-component S picking, returns empty phase list if no valid S pick. */
    public PhaseList pick(Waveform wfN, Waveform wfE, Waveform wfZ, Solution sol, int phasesToPickFlag, boolean gainUnitsFilter, boolean distFilter) {

        PhaseList phList = new PhaseList(2); // new list for P and/or S pick(s)

        if ( phasesToPickFlag == PhasePickerIF.P_ONLY || phasesToPickFlag == PhasePickerIF.NONE ) {
            return phList;
        }

        // return empty list if any input waveforms are null
        if (wfN == null) {
            System.err.printf("ERROR %s input Waveform N is null!%n", name);
            return phList;
        }
        if (wfE == null) {
            System.err.printf("ERROR %s input Waveform E is null!%n", name);
            return phList;
        }
        if (wfZ == null) {
            System.err.printf("ERROR %s input Waveform Z is null!%n", name);
            return phList;
        }
        mySol = sol;

        // Find channel's picker params in lookup map intialized from data file named in properties
        Channel chan = wfZ.getChannelObj();
        String sncl = chan.getNet() + "." + chan.getSta(); // toDelimitedSeedNameString(".");

        // Reject ACC(eleration) low-gain by property setting
        if (gainUnitsFilter && velocityOnly && !chan.isVelocity()) {
            if (debug) System.out.printf(" INFO %s velocityOnly and waveform channel gain not velocity units:%s%n", name, sncl);
            return phList; //bail
        }

        // Check channel rejection filters configured by properties else skip picking
        if (reject(wfZ)) {
            if (debug) System.out.printf(" INFO %s channel rejected for picking by SNCL attribute or its absence from candidate list:%s%n", name, sncl);
            return phList;
        }

        if (sol == null) {
            //if (debug || verbose) 
            System.err.printf("ERROR %s input Solution is null: %s%n", name, sncl);
            //return phList; // bail if no solution for epicentral distance
        }

        if ( ! sol.hasLatLonZ() ) {
            System.err.printf("ERROR %s input Solution does not have a location: %s%n", name, sncl);
            return phList;
        }
        
        if (!chan.hasLatLonZ()) {
            System.err.printf("ERROR %s input channel does not have a location: %s%n", name, sncl);
            return phList;
        }

        // Calculate channel epicentral distance if unknown
        double  hDist = chan.getHorizontalDistance();
        if (hDist == Channel.NULL_DIST) {
            chan.calcDistance(sol.getLatLonZ());
            hDist = chan.getHorizontalDistance();
            if (hDist == Channel.NULL_DIST) {
                if (debug || verbose) System.out.printf(" INFO %s channel has unknown distance, aborted picking, check Lat,Lon: %s%n", name, sncl);
                return phList; // bail if no epicentral distance
            }
        }

        if (distFilter) { // abort picking when distance too great
            if ( ! withinPickerRange(chan, sol, "P") ) return phList;
        } 

        // Check the attributes needed for determining P and S traveltimes
        TravelTime ttModel = wfZ.getTravelTimeModel();
        if (ttModel == null) {
            System.err.printf("ERROR %s Waveform TravelTimeModel is NULL: %s%n", name, sncl);
            return phList; //bail
        }

        if (!sol.hasValidDateTime()) {
            System.err.printf(" INFO %s solution %s origin time invalid, aborted picking for: %s%n", sol.getId(), name, sncl);
            return phList; // bail if no origin time
        }

        if (! sol.depth.isValidNumber()) {
            // if (debug || verbose)
              System.err.printf(" INFO %s solution depth %s invalid, aborted picking for: %s%n", sol.getId(), name, sncl);
            return phList; // bail if no solution depth
        }

        // Determine Phase travel times to station from event origin
        double depth = sol.getModelDepth();
        double ot =  sol.getTime();
        double ttp = ttModel.getTTp(hDist, depth);
        double tts = ttModel.getTTs(hDist, depth);

        boolean localLoadZ = false;
        boolean localLoadN = false;
        boolean localLoadE = false;
        // if no time-series, try to load it, else abort picking
        if (!wfZ.hasTimeSeries())  {
            if (wfZ.loadTimeSeries()) {
                localLoadZ = true;
            }
            else {
                if (debug) System.out.printf(" INFO %s no waveform Z timeseries for %s%n", name, sncl);
                return phList; // bail
            }

            if (wfN.loadTimeSeries()) {
                localLoadN = true;
            }
            else {
                if (debug) System.out.printf(" INFO %s no waveform N timeseries for %s%n", name, sncl);
                return phList; // bail
            }

            if (wfE.loadTimeSeries()) {
                localLoadE = true;
            }
            else {
                if (debug) System.out.printf(" INFO %s no waveform E timeseries for %s%n", name, sncl);
                return phList; // bail
            }
        }

        boolean havePtime = false;
        boolean haveStime = false;
        WFSegment[] segs = wfZ.getArray();
        // TOP OF LOOP over all WFSegment's for input channel waveform, does its timeseries contain the desired times?
        for (int idx = 0; idx<segs.length; idx++) {
            if (segs[idx].sampleIndexAtTime(ot+ttp) >= 0) havePtime = true;
            if (segs[idx].sampleIndexAtTime(ot+tts) >= 0) haveStime = true;
            if (havePtime && haveStime) break;
        }

        if (!havePtime && !haveStime) {
            if (debug) System.out.printf(" INFO %s predicted P and S time are not in timespan, aborted picking for: %s%n", name, sncl);
            return phList; // bail, nothing to pick
        }

        // Scan data to get the averages, but when do we start the real picking?
        // Do we want to start at the greater of the (ot+ttp-X) secs or at the beginning of the waveform timeseries?
        //double spTime = (tts-ttp);
        // Determine desired pick window times for P:
        double pStartTime = ot + ttp - windowHalfWidth;
        double pEndTime = ot + ttp + windowHalfWidth; // 2016/02/23 -aww try alternative below
        //double pEndTime = ot + ttp + Math.min(0.9*spTime, windowHalfWidth); // for small S-P time override current halfwidth ?

        TimeSpan pSpan = new TimeSpan(pStartTime, pEndTime);
        if (debug) System.out.printf("DEBUG %s P timespan: %s%n", name, pSpan);

        // Determine desired pick window times for S:
        double sStartTime = ot + tts - windowHalfWidth; // 2016/02/23 -aww try alternative below
        //double sStartTime = ot + tts - Math.min(0.9*spTime, windowHalfWidth); // for small S-P time override current halfwidth ?
        double sEndTime = ot + tts + windowHalfWidth;

        TimeSpan sSpan = new TimeSpan(sStartTime, sEndTime);
        if (debug) System.out.printf("DEBUG %s S timespan: %s%n", name, sSpan);

        // Determine start time for input segments, only use Z component for times, or check N and E too ??
        double scanStartTime = wfZ.getEpochStart();
        // Force always to start before expected P to get the background stats for LTA - ZRoss
        scanStartTime = Math.max(scanStartTime, (pStartTime - scanStartOffset));

        // Determine end time for input segments: 
        double scanEndTime = wfZ.getEpochEnd();
        scanEndTime = Math.min(scanEndTime, sEndTime + scanEndOffset);

        if (debug) {
            System.out.printf("DEBUG %s scanStartOffset: %5.2f scanEndOffset: %5.2f%n", name, scanStartOffset, scanEndOffset);
            System.out.printf("DEBUG %s desired scanStart->scanEnd       : %s -> %s%n",
                                  name, LeapSeconds.trueToString(scanStartTime), LeapSeconds.trueToString(scanEndTime));
        }

        PickerParmIF parm = getChannelParm(wfN.getChannelObj().toDelimitedSeedNameString("."));

        if (debug) System.out.printf(" INFO %s scanning %s timeseries for picks...%n", name, sncl);
        phList = pick(wfE, wfN, wfZ, phasesToPickFlag, parm, new TimeSpan(scanStartTime, scanEndTime), pSpan, sSpan);

        // unload if loaded, but does this conflict with wfcache loader for gui ?
        if (localLoadZ) wfZ.unloadTimeSeries();
        if (localLoadN) wfN.unloadTimeSeries();
        if (localLoadE) wfE.unloadTimeSeries();

        return phList;

    } // end of pick method

    /** Does 3-component S picking, returns empty phase list if no valid S pick. */
    private PhaseList pick(Waveform wfN, Waveform wfE, Waveform wfZ, int phasesToPickFlag, PickerParmIF snclMappedParm, TimeSpan scanSpan, TimeSpan pSpan, TimeSpan sSpan) {

        PhaseList phList = new PhaseList(2);

        double scanStartTime = scanSpan.getStart();
        double scanEndTime = scanSpan.getEnd();

        Channel chan = wfZ.getChannelObj();
        String sncl = chan.getNet() + "." + chan.getSta();

        int phFlag = phasesToPickFlag;
        if (phFlag >= PhasePickerIF.DEFAULT) {
            if (snclMappedParm != null) phFlag = snclMappedParm.getPickFlag();
            //else System.out.printf("DEBUG %s pick parm null, defaulting to using input flag %d for: %s%n", name, phFlag, sncl);
            else System.out.printf("DEBUG %s pick parm null, defaulting to using input flag %d for: %s%n", name, phFlag, wfN.getChannelObj().toDelimitedSeedNameString("."));
        }
        if (phFlag <= 0) {
            //if (debug)
                System.out.printf("DEBUG %s pick flag %d is set to NO picks: %s%n", name, phFlag, wfN.getChannelObj().toDelimitedSeedNameString("."));
            return phList; // don't pick
        }

        //
        // NOTE need to do logic below for ALL 3 components
        //
        Waveform pwfN = prepareWf(wfN, scanStartTime, scanEndTime);
        TimeSpan segSpanN = pwfN.getTimeSpan();
        double maxStart = segSpanN.getStart();
        double minEnd = segSpanN.getEnd();
        if (debug) System.out.printf("DEBUG %s N timespan: %s%n", name, segSpanN);

        Waveform pwfE = prepareWf(wfE, scanStartTime, scanEndTime);
        TimeSpan segSpanE = pwfE.getTimeSpan();
        if (segSpanE.getStart() > maxStart) maxStart = segSpanE.getStart();
        if (segSpanE.getEnd() < minEnd) minEnd = segSpanE.getEnd();
        if (debug) System.out.printf("DEBUG %s E timespan: %s%n", name, segSpanE);

        Waveform pwfZ = prepareWf(wfZ, scanStartTime, scanEndTime);
        TimeSpan segSpanZ = pwfZ.getTimeSpan();
        if (segSpanZ.getStart() > maxStart) maxStart = segSpanZ.getStart();
        if (segSpanZ.getEnd() < minEnd) minEnd = segSpanZ.getEnd();
        if (debug) System.out.printf("DEBUG %s Z timespan: %s%n", name, segSpanZ);

        // Require all waveforms to have their timeseries in a single segment with same start and end times 
        //if (debug)
            System.out.printf("INFO %s new N,E,Z timespan maxStart->minEnd: %s%n", name, new TimeSpan(maxStart,minEnd));
        pwfN = trimToSpan(pwfN, maxStart, minEnd);
        pwfE = trimToSpan(pwfE, maxStart, minEnd);
        pwfZ = trimToSpan(pwfZ, maxStart, minEnd);

        // Check that the timeseries trimming worked
        segSpanN = pwfN.getTimeSpan();
        segSpanE = pwfE.getTimeSpan();
        segSpanZ = pwfZ.getTimeSpan();
        if (! ( segSpanN.nearlyEquals(segSpanE) && segSpanN.nearlyEquals(segSpanZ) ) ) {
            System.out.printf("INFO: %s N,E, and Z timespans are not nearly equal?:%n%s%n%s%n%s for : %s%n", name, segSpanN.toString(), segSpanE.toString(), segSpanZ.toString(), sncl);
            return phList; // should be empty
        } 

        // Convert float array to double array for native method
        double[] tsN = getDoubleTimeSeriesArray(pwfN);
        double[] tsE = getDoubleTimeSeriesArray(pwfE);
        double[] tsZ = getDoubleTimeSeriesArray(pwfZ);

        // Check that the arrays are same length, same sample count
        if (! (tsN.length == tsE.length && tsN.length == tsZ.length) ) {
            System.out.printf("INFO: %s N,E, and Z samples are not equal : %s %s %s; aborted picking for : %s%n", name, tsN.length, tsE.length, tsZ.length, sncl);
            return phList; // should be empty
        } 

        if (rotateHoriz) { // should default be false?
            double[] tsR = new double[tsN.length];
            double[] tsT = new double[tsN.length];
            double az = wfN.getChannelObj().getAzimuth();
            // channel azimuth is Epicenter to Station, we want Station to Epicenter
            az = (az < 180.) ? az + 180. : az - 180.;
            AbstractPicker.rotate_NE_RT(tsN, tsE, tsR, tsT, az);
            // map N to R and E to T component picks
            tsN = tsR;
            tsE = tsT;
        }
        // Now do call to Z.Ross's native JNI C++ pick routine 
        // Assumes all station channel's waveforms have the same sampling interval
        // compare with ((WFSegment)pwfN.getSegmentList.get(0)).getSampleInterval();
        // No P data for Z returned by native method
        //System.out.printf("DEBUG TriaxialPicker2 pick_native Inputs covarWind: %3.1f kLenSecs: %4.1f NSampInt: %6.4f 1st3Samps: N: %10.9e %10.9e %10.9e E: %10.9e %10.9e %10.9e Z: %10.9e %10.9e %10.9e%n",
        //        covarianceWindowSecs, kLengthSecs, pwfN.getSampleInterval(), tsN[0], tsN[1], tsN[2],tsE[0], tsE[1], tsE[2],tsZ[0], tsZ[1], tsZ[2]);
        //System.out.printf("DEBUG TriaxialPicker2 pick_native Inputs covarianceWindowSecs: %3.1f kLengthSecs: %4.1f SampleInterval: %6.4f%n", covarianceWindowSecs, kLengthSecs, pwfN.getSampleInterval());
        //double[] newPicks = pick_native(tsN, tsE, tsZ, covarianceWindowSecs, pwfN.getSampleInterval()); // 2016-02-28
        double[] newPicks = pick_native(tsN, tsE, tsZ, covarianceWindowSecs, pwfN.getSampleInterval(), kLengthSecs); // 2016-03-14
        //
        // Convert relative pick times returned from native method call to absolute leap seconds time,
        // add returned pick time to the wf's span start time
        double s1 = newPicks[0];  // N ?
        double s2 = newPicks[1];  // E ?
        double snr1 = newPicks[2];
        double snr2 = newPicks[3];
        // Check that picks were returned
        // get channel epicentral distance for report
        double  dist = chan.getHorizontalDistance();
        if (dist == Channel.NULL_DIST) {
            chan.calcDistance(mySol.getLatLonZ());
            dist = chan.getHorizontalDistance();
        }
        long evid = (mySol != null) ? mySol.getId().longValue() : 0l; 

        if ( s1 == -1. && s2 == -1. ) {
            System.out.printf("INFO: %s N,E, and Z pick_native returned no picks for: %s %f5.1 km %d%n", name, sncl, dist, evid);
            return phList; // should be empty
        }

        // DEBUG output timeseries ascii files
        if (debug) {
            PrintWriter dstream = null;
            try {
                  dstream = new PrintWriter(new BufferedWriter(new FileWriter("/tmp/"+evid+"_"+wfN.getChannelObj().toDelimitedSeedNameString(".")+"_"+LeapSeconds.trueToString(segSpanN.getStart(),"yyyyMMddHHmmss.SSS")+".txt" )));
                  for ( int i=0; i<tsN.length; i++) {
                      dstream.print(tsN[i]);
                      dstream.println();
                  }
                  dstream.flush();
                  dstream.close();
                  //
                  dstream = new PrintWriter(new BufferedWriter(new FileWriter("/tmp/"+evid+"_"+wfE.getChannelObj().toDelimitedSeedNameString(".")+"_"+LeapSeconds.trueToString(segSpanE.getStart(),"yyyyMMddHHmmss.SSS")+".txt" )));
                  for ( int i=0; i<tsE.length; i++) {
                      dstream.print(tsE[i]);
                      dstream.println();
                  }
                  dstream.flush();
                  dstream.close();
                  //
                  dstream = new PrintWriter(new BufferedWriter(new FileWriter("/tmp/"+evid+"_"+wfZ.getChannelObj().toDelimitedSeedNameString(".")+"_"+LeapSeconds.trueToString(segSpanZ.getStart(),"yyyyMMddHHmmss.SSS")+".txt" )));
                  for ( int i=0; i<tsZ.length; i++) {
                      dstream.print(tsZ[i]);
                      dstream.println();
                  }
                  dstream.flush();
                  dstream.close();
                  //
            }
            catch (Exception ex) {
                System.err.println(ex.getMessage());
            }
        }

        // Assign returned pick times to appropriate component, where -1 indicates no pick
        Waveform wf = null;
        double time = 0.;
        int weight = 2; // for now, the default for any S pick?
        if (s1 == -1. ) {
            wf = pwfE;
            time = wf.getEpochStart() + s2;
            weight = setPhaseWeight(snr2);
        }
        else if ( s2 == -1 ) {
            wf = pwfN;
            time = wf.getEpochStart() + s1;
            weight = setPhaseWeight(snr1);
        }
        else { // have 2 picks, so decide
            if ( snr1 >= snr2 ) { // go with larger s1 signal?
                wf = pwfN;
                time = wf.getEpochStart() + s1;
                if ( sSpan.contains(time) ) {
                    wf = pwfN;
                    weight = setPhaseWeight(snr1);
                }
                else wf = null; // outside time range try other lower snr pick
            }
            if ( wf == null ) { // s1 has lower snr or is outside of desired S time span? 
                wf = pwfE;
                time = wf.getEpochStart() + s2;
                weight = setPhaseWeight(snr2);
            }
        }

        // Check selected pick time against the input pSpan and sSpan ranges to accept for new phase or reject 
        if ( sSpan.contains(time) ) {
            //if (debug)
                System.out.printf("DEBUG %s pick_native s1,s2,snr1,snr2: %5.2f %5.2f %6.1f %6.1f sSpan %s Spick %s for %s %5.1f km %d In=T%n", 
                    name, s1, s2, snr1, snr2, sSpan, LeapSeconds.trueToString(time), sncl, dist, evid);
            // ? can we scale returned SNR to a hypoinverse weight 0-4? S pick default wt=2.
            Phase ph = makePhase(wf.getChannelObj(), "S", PhaseDescription.DEFAULT_FM, time, weight);
            phList.add(ph);
        }
        else {
            //if (debug)
                System.out.printf("DEBUG %s pick_native s1,s2,snr1,snr2: %5.2f %5.2f %6.1f %6.1f sSpan %s Spick %s for %s %5.1f km %d In=F%n", 
                    name, s1, s2, snr1, snr2, sSpan, LeapSeconds.trueToString(time), sncl, dist, evid);

        }

        return phList;

    } // end of pick method

    private int setPhaseWeight(double snr) {
        int iwt = 2;
        //if (snr >= 3. ) { iwt = 0; }
        //else if ( snr >= 2.) { iwt = 1; }
        return iwt;
    }

    private Waveform trimToSpan(Waveform wf, double maxStart, double minEnd) {
        WFSegment wfs = (WFSegment) wf.getSegmentList().get(0);
        wfs.trim(maxStart,minEnd);
        ((AbstractWaveform)wf).trimSpanToWFSegmentSpan();
        return wf;
    }

    private double[] getDoubleTimeSeriesArray(Waveform wf) {
        double[] d = null;
        try {
          float[] f = ((WFSegment)wf.getSegmentList().get(0)).getTimeSeries(); 
          d = new double[f.length];
          for (int i = 0; i < f.length; i++) {
              d[i] = f[i];
          }
        }
        catch (NullPointerException ex) {
            System.err.println("Error " + name + " getDoubleTimeSeriesArray " + ex.getMessage() + " channel : " + wf.getChannelObj().toDelimitedSeedNameString("."));
        }
        return d;
    }

    private Waveform prepareWf(Waveform wf, double scanStartTime, double scanEndTime) {
        //
        // Create new Waveform here, EXTRACT subsegments from original, DEMEAN new waveform before scan for picks
        AbstractWaveform newWf = (AbstractWaveform) wf.createWaveform();
        newWf.copyHeader(wf);
        Channel chan = newWf.getChannelObj(); // alias new copy
        newWf.setSegmentList(wf.getSubSegments(scanStartTime, scanEndTime), true); // resets packet spans, not that it matters here
        newWf.trimSpanToWFSegmentSpan(); // reset epoch start and end in new wf
        //newWf.demean(); // assume filter below removes the bias from the timeseries
        WFSegment[] segs = newWf.getArray(); // get the new demeaned segments
        TimeSpan segSpan = newWf.getTimeSpan(); // new wf time span
        if (debug) System.out.printf("DEBUG %s scanning for picks in timespan of: %s%n", name, segSpan);

        //
        // TOP OF LOOP over all WFSegment's channel, scan timeseries for arrivals:
        //
        WFSegment wfseg = null;
        double endtime = 0.;
        double enddata = 0.;

        boolean badgaps = false;
        boolean interpolated = false;
        for (int idx = 0; idx<segs.length; idx++) { // LOOP over segments
            wfseg = segs[idx];
            // Skip from start of the waveform to ? before starting to average data
            segSpan = wfseg.getTimeSpan();

            if (segSpan.isBefore(scanStartTime)) {
                if (debug) System.out.printf("DEBUG %s segment %d skipped, timespan before %s%n", name, idx, LeapSeconds.trueToString(scanStartTime));
                continue; // shouldn't be the case if subsegs extracted, but skip forward
            }
            if (segSpan.isAfter(scanEndTime)) {
                if (debug) System.out.printf("DEBUG %s segment %d skipped, timespan after %s%n", name, idx, LeapSeconds.trueToString(scanEndTime));
                break; // no need to go further?, shouldn't be the case if subsegs extracted 
            }

            int gapSize = 0;
            int badGapSize = 0;
            if (idx > 0) { // not the 1st segment so check for time gaps, and if found, interpolate samples BETWEEN waveform segments
                // Compute the number of samples since the end of the previous message.
                // If (gapSize == 1), no data has been lost between messages.
                // If (1 < gapSize <= maxSampleGap), data will be interpolated.
                double gsd = (wfseg.getEpochStart() - endtime)/wfseg.getSampleInterval();
                // size < 0 Invalid. Time going backwards. 
                if ( gsd < 0. ) {
                    badgaps = true;
                    badGapSize = (int) Math.round(gsd); 
                }
                else {
                    gapSize = (int) Math.round(gsd);
                    if (gapSize > 1) { // valid time-tear?
                        // Interpolate missing samples and prepend them to the current timeseries buffer
                        //if (debug)
                            System.out.printf("INFO %s Found %4d sample gap. Interpolating channel:%s%n",
                                name, gapSize, chan.toDelimitedSeedNameString());
                        wfseg = interpolate(enddata, endtime, wfseg, gapSize);
                        interpolated = true;
                    }
                }
            }
            endtime = wfseg.getEpochEnd();
            float[] ts = wfseg.getTimeSeries();
            enddata = (double) ts[ts.length-1];

            if (badgaps) {
                System.out.printf("%s WARNING waveform segment concatenation terminated, gap of -%d samples for %s%n", name, badGapSize, chan.toDelimitedSeedNameString());
                break; // then do what?
            }

        } // end of loop

        if ( interpolated ) {
            if (debug) System.out.printf("DEBUG %s Collapsing segment list for :%s%n", name, chan.toDelimitedSeedNameString());
            newWf.setSegmentList( WFSegment.collapse(Arrays.asList(segs)), true); // resets packet spans, not that it matters here
            newWf.trimSpanToWFSegmentSpan(); // reset epoch start and end in new wf
        }

        // For newWf with a collapsed seg list we assume our timeseries of interest is in first segment because there should only be one.
        //  !!! Need to demean and BW bandpass filter final wfseg series samples before picking
        Waveform filteredWf = newWf;
        if (doBWFilter && bwfOrder > 0) {
            if (bwf == null  || bwfScalePassBandByDistance) {
              int rate = (int) filteredWf.getSampleRate(); 
              double bwfHiCut = bwfHiFreq;
              double bwfLoCut = bwfLoFreq;
              if (bwfScalePassBandByDistance)  bwfHiCut = 1./Math.max(bwfMinPeriod, .05*Math.exp(.0015*filteredWf.getChannelObj().getDistance()));
              bwfHiCut = Math.min(bwfHiCut, ((double)rate)/2. - 0.01); // Note subtract 0.01 from hi frequency in beyond Nyquist
              bwfLoCut = Math.min(bwfLoCut, bwfHiCut); // screen for low sample rate
              if (bwfType == 1) { // bandpass type
                bwf = ButterworthFilterSMC.createBandpass(rate, bwfLoCut, bwfHiCut, bwfOrder, bwfReversed);
                bwfTypeStr = "Butterworth_BP_" + bwfFmt.form(bwfLoCut).trim() + "_" + bwfFmt.form(bwfHiCut).trim() + "_" + String.valueOf(bwfOrder); 
              }
              else { // highpass type
                bwf = ButterworthFilterSMC.createHighpass(rate, bwfLoCut, bwfOrder, bwfReversed);
                bwfTypeStr = "Butterworth_HP_" + bwfFmt.form(bwfLoCut).trim() + "_" + String.valueOf(bwfOrder); 
              }
              if (bwf != null) bwf.copyInputWaveform(false);
              else System.err.println("Error creating ButterworthFilter, skipping filter!");
           }
           if (bwf != null) { // else error?
             if (debug) System.out.printf("%s DEBUG %s filtered waveform for: %s%n", name, bwfTypeStr, chan.getChannelObj().toDelimitedSeedNameString("."));
             filteredWf = (Waveform) bwf.filter(newWf);
             filteredWf.setFilterName(bwfTypeStr);
           }
        }

        return filteredWf;
    }

    private Phase makePhase(Channel chl, String type, String fm, double time, int weight) {

        Phase ph = Phase.create();

        ph.setChannelObj(chl);
        ph.setProcessingState(JasiProcessingConstants.STATE_AUTO_TAG);
        ph.datetime.setValue(time);
        ph.description.iphase = type;
        ph.description.setWeight(weight);
        ph.qualityToDeltaTime();

        double quality = ph.getQuality();
        boolean dofm = (quality > firstMoQualityMin);
        dofm &= (type.equals("P") && chl.isVertical());
        if (dofm) {
            ph.description.fm = fm;
        }
        else {
            ph.description.fm = PhaseDescription.DEFAULT_FM;
        }

        // Kludge here:
        if (weight <= 1) ph.description.ei = "i";
        else ph.description.ei = "e"; 
        
        return ph;
    }

} // End of TriaxialPicker2 class 
