package org.trinet.jasi.picker;
import org.trinet.jasi.ChannelableList;
import org.trinet.jasi.PhaseList;
import org.trinet.jasi.Solution;
import org.trinet.jasi.Waveform;
public interface TriaxialPickerIF extends PhasePickerIF {
    public PhaseList pick(ChannelableList wfList, Solution sol, int phasesToPickFlag, boolean gainUnitsFilter, boolean distFilter);
    public PhaseList pick(Waveform wfN, Waveform wfE, Waveform wfZ, Solution sol, int phasesToPickFlag, boolean gainUnitsFilter, boolean distFilter);
}
