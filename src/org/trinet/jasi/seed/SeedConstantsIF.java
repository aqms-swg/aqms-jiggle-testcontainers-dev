package org.trinet.jasi.seed;
public interface SeedConstantsIF {
// Data encoding formats described in SEED v2.3,  see SEED Ref. Man. pg. 106
// ONLY STEIM1 & STEIM2 ARE CURRENTLY SUPPORTED
//    public static final byte STEIM1     = 10;
//    public static final byte STEIM2     = 11;
//    public static final byte GEOSCOPE24   = 12;
//    public static final byte GEOSCOPE16_3 = 13;
//    public static final byte GEOSCOPE16_4 = 14;
//    public static final byte USNSN      = 15;
//    public static final byte CDSN   = 16;
//    public static final byte GRAEFENBERG  = 17;
//    public static final byte IGP    = 18;

    /**
    Data record types: DRQM,  HeaderTypes: DRQMVATS
    <pre>
        D  The state of quality control of the data is indeterminate.
        R  Raw Waveform Data with no Quality Control
        Q  Quality Controlled Data, some processes have been applied to the data.
        M  Data center modified, time-series values have not been changed.
    </pre>
    */
    public static final String DataHeaderTypes = "DRQM";
    public static final String HeaderTypes = DataHeaderTypes + "VATS";


    /** Bytes in header. */
    public static final int HeaderSize = 48;

    /** Bytes per frame. */
    public static final int FrameSize = 64;

    /** Bytes per read */
    public static final int READBUFFERSIZE = 16384;

    /** bytes per Seed packet work area, equal to maximum Seed packet size we can handle */
    public static final int DEFAULTSEEDBLOCKSIZE  = 8192;
}
