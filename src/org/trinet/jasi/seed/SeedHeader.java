package org.trinet.jasi.seed;

import java.io.*;
import org.trinet.util.Bits;
import org.trinet.util.LeapSeconds;
import org.trinet.util.DateTime;
import org.trinet.jasi.*;

/**
 * Information contained in the 48 byte SEED header.

    READ QUANTERRA SEED "RECORDS" (PACKETS)<P>

    Seed records are FIXED size and are always a power of 2; from
    from 512 to 8192 bytes.

    SEED "records" are made up of 64 byte "frames".
    There is ALWAYS a 48 byte fixed data header. The header may be
    followed by any number of "blockettes" which are addition data
    and information about the time series in the record. Any extra space
    in the header contains garbage.
    If there are enough blockettes they may extend into the next frame or frames.
    Therefore, data may begin in the 2nd, 3rd or Nth frame. A non-data
    record may consist of ONLY a header and some blockettes and garbage in
    the time-series frames.

   512-8192 byte fixed length records are divided into 64-byte "frames"
   The first frame contains the SEED data header and data blockettes.
   For non-time-series packets the remaining frames contain more
   blockettes (usually 1 or 2 total) followed by garbage.
   For time-series packets the remaining frames contain compressed data.

   Header types are V, A, S, T and D, R, Q, M.

c       The FIXED DATA HEADER contains how many blockettes follow and each
c       blockette contains the byte offset to the next blocket.
c
c  SEED "D" RECORD STRUCTURE (have Time-series "1001" Blockettes)
c
c Frame#
c       +-------------------------------------------------------+
c   1   |  Fixed data header  | 1000 blockette | 1001 blockette |
c       |     (48 bytes)      |   (8 bytes)    |   (8 bytes)    |
c       +-------------------------------------------------------+
c   2   |       Data Frame                                      |
c       +-------------------------------------------------------+
c   3   |       Data Frame                                      |
c       +-------------------------------------------------------+
c               .
c               .
c               .
c       +-------------------------------------------------------+
c   7   |       Data Frame                                      |
c       +-------------------------------------------------------+
c
c
c  SEED "other" RECORD STRUCTURE (All other Blockettes)
c
c Frame#
c       +-------------------------------------------------------+
c   1   |  Fixed data header  |   Blockette   |    Blockette... |
c       |     (48 bytes)      | (8-32 bytes)  |   (8-32 bytes)  |
c       +-------------------------------------------------------+
c   2   | ... more blockette(s)                                 |
c       +-------------------------------------------------------+
c   3   |       Garbage                                         |
c       +-------------------------------------------------------+
c               .
c               .
c               .
c       +-------------------------------------------------------+
c   7   |       Garbage                                         |
c       +-------------------------------------------------------+
c
c Data Blockette structure:
c
c       FIELD                   TYPE            LENGTH (bytes)
c       Blockette type          Byte            2       (interpret as int)
c       Byte of next blockette  Byte            2       (interpret as int)
c       Data fields             depends on blockette type
c        .
c        .
c        .
c
c Known Blockette types:
c
c       NUMBER          TYPE
c       100             sample rate
c       200             generic event detection
c       201             Murdoch-Hutt event detection
c       202             LOG-Z event detection
C       300             step calibration
C       310             sine calibration
C       320             pseudo-random calibration
C       390             generic calibration
C       395             calibration abort
C       400             Beam (no used by Quanterra)
C       405             Beam delay (no used by Quanterra)
c       500             time stamp
c       1000            Data format description
c       1001            Data (time-series)
*/
public class SeedHeader implements SeedConstantsIF {

    protected static SeedHeader header = null; // to save last header instance parsed

    // Header fields
    int sequenceNumber  = 0;
    String headerType = null;   //Header types are V, A, S, T, D, R, Q, M


    ChannelName chan = null;

    /** Header start time. */
    double datetime = 0.;

    /** Number of samples in this SEED data record */
    int sampleCount = 0;

    /** Nominal sample rate. */
    double samplesPerSecond = 0.;

    /** Get true sample rate by muliplying nominal rate by this factor.
    *  Will usually be = 1 */
    double sampleMultiplier = 1.0;

    /** Time correction in microseconds */
    int timeCorrection = 0;

    /** Bytes per data sample. */
    int bytesPerSample = 0;

    TimeSeriesQuality qualityFlags = new TimeSeriesQuality();

    int blockettesFollowing = 0;
    /**  Byte offset to start of data, counting from byte 0 */
    int dataOffset = 0;
    /**  Byte offset to start of 1st blockette, counting from byte 0 */
    int blocketteOffset = 0;

    // Blockette 1000
    int encodingFormat = 0;            // from blockette 1000
    /** Data order: 0 = little endian (VAX, Intel) order, 1 = big endian (Motorola, Unix, MAC)
    *  Java binary format files are stored big-endian. */
    int wordOrder = 0;         //0=VAX/Intel, 1=SUN/Motorola
    int dataRecordLength = 0;
    // Blockette 1001 info
    int timeQuality = 0;     // vender specific value, integer from 0-100(%)
    int microsecTimeCorrection = 0;  // add to datatime for msec time precision
    //int reserved;
    int framesInRecord = 0;  // used to indicate empty frames at end of record
    int seedBlockSize = 0;  // size of this record, 512, 1024, 2048, 4096 or 8192

  public SeedHeader() { }

  /** Return the one character SEED Control header type code. Assumes buffer is
   *  a well formed SEED header. Return null if invalid. */
  static String getHeaderType(byte[] buffer) {
    if (buffer.length < 7) return null;
    String type = new String(buffer, 6, 1);
    return (isValidType(type)) ?  type : null;
  }

  /** Return the SEED header sequence number. Returns -1 on error. */
  static int getSequenceNumber(byte[] buffer) {
    if (buffer.length < 6) return -1;
    return Bits.byteStringToInt (buffer, 0, 6);
  }

/*  Work in progress -- not functional
  public static SeedHeader parseSeedHeader (BufferedInputStream inbuff){
     byte[] bytes = new byte[FrameSize];
     int totalBytes = 0;
     int buffOffset = 0;
     // file read loop; while data is there and we haven't yet gotten all the bytes
     try {
       while (inbuff.available() > 0) {
         totalBytes +=
         inbuff.read(bytes, buffOffset, FrameSize); // read one Seed header (64 bytes)

       } // end of while (inbuff.available() > 0)
     } catch (IOException exc) {
       System.err.println ("IO error: " + exc.toString());
       exc.printStackTrace();
     } catch (Exception exc) {
       System.err.println ("General exception: " + exc.toString());
       exc.printStackTrace();
     }
  }
*/

    /**
     * Parse the buffer assuming its a SEED header. Apply any time corrections.
     *
     * Header types are V, A, S, T and D, R, Q, M
     * Returns null if its not a data record, type: "D,R,Q,M".
     */
     /* NOTE: THIS ASSUMES THE BUFFER IS THE COMPLETE HEADER. IT IS POSSIBLE
     *  FOR SEED HEADERS TO SPAN 64-BYTE FRAMES.
     */
    public static SeedHeader parseSeedHeader(byte[] buff)  {

      SeedHeader h = new SeedHeader();

      h.headerType   = getHeaderType(buff);
      if (h.headerType == null) {
          System.err.println("Error: SeedHeader input buffer invalid, headerType is null");
          return null;
      }

      if ( h.isData() ) {
        h.sequenceNumber = getSequenceNumber(buff);
        h.chan = new ChannelName( // make a channelname object  changed input arg order - aww
          new String(buff,18, 2), // net
          new String(buff, 8, 5), // sta
          new String(buff,15, 3), // seedchan
          new String(buff,13, 2), // location
          "",         // auth
          "",         // subsource
          new String(buff,15, 3), // channel
          ""          // channelsource
        );

        // Seed Iris Format requires Steim miniseed data recored to be boolean bigEndian = true;
        // Is last word of fixed header, the blockette offset, a decimal 48 (the header length)?
        // 48 becomes 12288 if word is litte-endian byte swapped
        boolean bigEndian = (Bits.byteToUInt2(buff, 46, true) == 48);
        if (!bigEndian) {
            System.out.println("SeedHeader blockette offset = " +  Bits.byteToUInt2(buff, 46, true));
            System.err.println("ERROR: SeedReader, data header integers are not BIG-ENDIAN, swapped, check wavesource");
        }
        //

        //h.datetime = DateTime.trimToMicros(SeedHeader.seedTimeUTC(buff, 20)); // removed 2008/02/04 -aww
        h.datetime = SeedHeader.seedTimeUTC(buff, 20, bigEndian); // added 2008/02/04 -aww
        h.sampleCount = Bits.byteToUInt2(buff, 30, bigEndian);

        // if dt>0 its samples/sec, else its sec/sample
        h.samplesPerSecond = Bits.byteToInt2(buff, 32, bigEndian);
        if (h.samplesPerSecond < 0)  {
          h.samplesPerSecond = -1.0/h.samplesPerSecond;
        }

        h.sampleMultiplier = Bits.byteToInt2(buff, 34, bigEndian);
        // apply rate multiplier
        if (h.sampleMultiplier > 0) {
          h.samplesPerSecond = h.samplesPerSecond * h.sampleMultiplier;
        } else {
          h.samplesPerSecond = h.samplesPerSecond / -h.sampleMultiplier;
        }

        // decode bit flags
        h.qualityFlags.setActivityFlags(buff[36]);
        h.qualityFlags.setIOFlags(buff[37]);
        h.qualityFlags.setQualityFlags(buff[38]);

        h.blockettesFollowing = (int) buff[39];

        // apply correction if it hasn't been done already
        // NOTE: this will mask from the user whether the correction happened
        // in the RAW DATA or here
        h.timeCorrection  = Bits.byteToInt4(buff, 40, bigEndian);
        if (!h.qualityFlags.timeCorrected) {
          h.datetime += h.timeCorrection/10000.; //BUGFIX: timeCorrection units are .0001 seconds - 2015/03/23 aww
          h.qualityFlags.timeCorrected = true;
        }

        h.dataOffset      = Bits.byteToUInt2(buff, 44, bigEndian);
        h.blocketteOffset = Bits.byteToUInt2(buff, 46, bigEndian);

        // process blockettes, will only do 1000 and 1001
        int nextBlocketteOffset = h.blocketteOffset;
        int blocketteType;
        int bOffset;    // offset from record start to blketts

        while (nextBlocketteOffset > 0) {
          bOffset = nextBlocketteOffset;
          blocketteType = Bits.byteToUInt2(buff, bOffset, bigEndian);
          nextBlocketteOffset =  Bits.byteToUInt2(buff, bOffset+2, bigEndian);
          /* get 1000 blockette. This tells us the data type and blocksize
                    INTEGER*2     BLKID
                    INTEGER*2     BOFF
                    BYTE          FORM            ! encoding format
                    BYTE          ORDER           ! byte order: 0 = vax; 1=sparc
                    BYTE          RECL            ! Record size  bytes: power of 2; 2**recl
                    BYTE          RESERVED
          */
          if (blocketteType == 1000) {
            h.encodingFormat = buff[bOffset+4];
            h.bytesPerSample = SeedEncodingFormat.getBytesInFormat(h.encodingFormat);
           // 0 = little endian (VAX, Intel) order, 1 = big endian (Motorola, Unix) order
            h.wordOrder      = buff[bOffset+5];
           // Seed block size, very important to reading the data portion correctly
            h.dataRecordLength  = buff[bOffset+6];
            h.seedBlockSize  = (int) Math.pow(2.0, (double)h.dataRecordLength);
            h.framesInRecord = h.seedBlockSize / FrameSize;
           // skip 'reserved' byte at end of blockette
          }

          /* get 1001 blockette: clock quality and greater precision info.
                    INTEGER*2     BLKID
                    INTEGER*2 BOFF
                    BYTE  QUAL                    ! quality of timing
                    BYTE  MICRO_S                 ! micro_secs of time corr
                    BYTE  RESERVED
                    BYTE  FRAMES                  ! frames in record
          */
          else if (blocketteType == 1001) {
            h.timeQuality    = buff[bOffset+4];
            h.microsecTimeCorrection = buff[bOffset+5];
            // skip reserved byte
            // int frameCount = buff[bOffset+7];
            //xxx//       h.framesInRecord = buff[bOffset+7];
            // This is bogus in all the data!!!
            //            if (h.framesInRecord != frameCount) {
            //              System.out.println ("Conflicting header info: header reports "+
            //              h.framesInRecord + " frames/record. Blockette 1001 reports "+frameCount);
            //            }
            // apply the microsecond time correction
            h.datetime += ((double)h.microsecTimeCorrection /1000000.0);
          }
        }
      } else if (h.isValidType()) {
        System.err.println("INFO: SeedHeader is not data, header type: " + h.headerType );
        // would parse other types here...
        h.sequenceNumber = getSequenceNumber(buff);
      }
      else { 
        System.err.println("Error: SeedHeader invalid header type : " + h.headerType );
        return null; // garbage
      }

      header = h; // save last instance parsed here
      return h;   // return the SeedHeader object
    }

    /** Create a new WFsegment with the info in this SEED header.
     *  Returns null if not a data header. */
    public WFSegment createWFsegment()  {

        if ( ! isData() ) return null;
        // should throw exception

        Channel ch = Channel.create();
        ch.setChannelName(chan);

        WFSegment wfseg = new WFSegment (ch);
        wfseg.setStart(datetime);
        wfseg.samplesExpected = sampleCount;
        wfseg.setSampleInterval(1.0/samplesPerSecond);
        // Trim to nanos to correct double precision jitter errors
        double tend = wfseg.getEpochStart() +
                (wfseg.getSampleInterval() * (wfseg.samplesExpected -1));
        wfseg.setEnd(DateTime.trimToMicros(tend));

        wfseg.fmt = JasiTimeSeries.SEED_FORMAT;
        wfseg.encoding = encodingFormat;   // added DDG 10/10/03 for WaveClient // aww 11/24
        wfseg.filename = "";

        // set quality flags
        wfseg.clockLocked  = qualityFlags.clockLocked;
        wfseg.ampSaturated = qualityFlags.ampSaturated;        // clipped?
        wfseg.digClipped   = qualityFlags.digClipped;
        wfseg.spikes       = qualityFlags.spikes;
        wfseg.glitches     = qualityFlags.glitches;
        wfseg.badTimeTag   = qualityFlags.badTimeTag;

        // convert from 0%->100% to 0->1 scale
        wfseg.setTimeQuality(timeQuality*0.01);

        // See Seed manual pg. 106
        wfseg.bytesPerSample = SeedEncodingFormat.getBytesInFormat(encodingFormat);

        return wfseg;

    }

    // Discriminate "Data" subtypes from rest of types, most records processed by JASI are Waveform data - 12/03/2007 -aww
    /** Returns true if string represents a valid one character
     *  SEED Control header DATA type code. */
    protected static boolean isValidDataType(String type) {
      if (type == null) return false;
      return (type.length() == 1 && DataHeaderTypes.indexOf(type) > -1);  // is it in the string?
    }

    /** Returns true if instance header type is any one of the characters:  "D,R,Q, or M" */
    public boolean isData() {
      return isValidDataType(headerType);  // is it in the string?
    }

    /** Returns true if string represents a valid one character
     *  SEED Control header type code. */
    protected static boolean isValidType(String type) {
      if (type == null) return false;
      return (type.length() == 1 && HeaderTypes.indexOf(type) > -1);  // is it in the string?
    }

    /** Returns true if string represents a valid one character
     *  SEED Control header type code. */
    public boolean isValidType() {
      return isValidType(headerType);  // is it in the string?
    }

    private boolean checkType(String str) {
      if (isValidType(headerType)) return headerType.equalsIgnoreCase(str);
      return false;
    }

    /** Return true if the header is of type "V" */
    public boolean isVolume() {
      return checkType("V");
    }
    /** Return true if the header is of type "A" */
    public boolean isDictionary() {
      return checkType("A");
    }
    /** Return true if the header is of type "S" */
    public boolean isStation() {
      return checkType("S");
    }
    /** Return true if the header is of type "T" */
    public boolean isTime() {
      return checkType("T");
    }

    // Stuff added by AWW to support DbBlobReader

    public String getHeaderType() { return headerType; }
    public double getDatetime() { return datetime; }
    public double getSamplesPerSecond() { return samplesPerSecond; }
    public double getSampleMultiplier() { return  sampleMultiplier; }
    public int getTimeCorrection() { return timeCorrection; }
    public int getBytesPerSample() { return bytesPerSample; }
    public int getSampleCount() { return sampleCount; }
    public int getEncodingFormat() { return encodingFormat; }
    public int getWordOrder() { return wordOrder; }
    public int getDataRecordLength() { return dataRecordLength; }
    public int getSeedBlockSize() { return seedBlockSize; }
    public ChannelName getChannelName() { return chan; }

    public String toString() {
          StringBuffer sb = new StringBuffer(512);
          sb.append(sequenceNumber);
          sb.append(" ");
          sb.append(headerType);
          sb.append(" ");
          sb.append(chan.toString());
          sb.append(" ");
          sb.append(LeapSeconds.trueToString(datetime)); // convert true leap to string - aww 2008/02/11 
          sb.append(" ");
          sb.append(sampleCount);
          sb.append(" ");
          sb.append(samplesPerSecond);
          sb.append(" ");
          sb.append(sampleMultiplier);
          sb.append(" ");
          sb.append(timeCorrection);
          sb.append(" ");
          sb.append(bytesPerSample);
          sb.append(" ");
          sb.append(qualityFlags.toString());
          sb.append(" ");
          sb.append(blockettesFollowing);
          sb.append(" ");
          sb.append(dataOffset);
          sb.append(" ");
          sb.append(blocketteOffset);
          sb.append(" ");
          sb.append(encodingFormat);
          sb.append(" ");
          sb.append(wordOrder);
          sb.append(" ");
          sb.append(dataRecordLength);
          sb.append(" ");
          sb.append(timeQuality);
          sb.append(" ");
          sb.append(microsecTimeCorrection);
          sb.append(" ");
          sb.append(framesInRecord);
          sb.append(" ");
          sb.append(seedBlockSize);
          return sb.toString();
    }

    /**
     * Decode a 10-byte Seed BTIME and convert to "UNIX" epoch time
        INTEGER*2     YR
        INTEGER*2     JDAY
        BYTE          HOUR
        BYTE          MIN
        BYTE          SEC
        BYTE          %FILL
        INTEGER*2     MSEC            ! ms * 10
     */
    public static double seedTimeNominal(byte[] buff, int idx) {
        return seedTimeNominal(buff, idx, false);
    }
    public static double seedTimeNominal(byte[] buff, int idx, boolean bigEndian) {
        return LeapSeconds.trueToNominal(seedTimeUTC(buff, idx, bigEndian)); //  for leap secs 2008/01/24 -aww
    }

    // preserve leading zeroes for place holding in format string
    //static final Format d4 = new Format("%04d");
    //static final Format d3 = new Format("%03d");
    //static final Format d2 = new Format("%02d");

    public static double seedTimeUTC(byte[] buff, int idx) {
        return seedTimeUTC(buff, idx, false);
    }
    public static double seedTimeUTC(byte[] buff, int idx, boolean bigEndian) {
      int yr   = Bits.byteToUInt2(buff, idx+0, bigEndian);
      int jday = Bits.byteToUInt2(buff, idx+2, bigEndian);
      int hr   = buff[idx+4];
      int mn   = buff[idx+5];
      int sec  = buff[idx+6];
      int frac = Bits.byteToUInt2(buff, idx+8, bigEndian); // ms * 10

      /* DateTime class keeps precision to microseconds (0.000001),
      // native Java time only good to milliseconds (0.001)
      frac = Math.round((float)frac/10.f);
      StringBuffer sb = new StringBuffer(24);
      sb.append(d4.form(yr)).append(" ");
      sb.append(d3.form(jday)).append(" ");
      sb.append(d2.form(hr)).append(":");
      sb.append(d2.form(mn)).append(":");
      sb.append(d2.form(sec)).append(".");
      sb.append(d3.form(frac));

      //Alternative to above, same string result
      //Concatenate.format(sb,(long)yr,4,4).append(" ");
      //Concatenate.format(sb,(long)jday,3,3).append(" ");
      //Concatenate.format(sb,(long)hr,2,2).append(":");
      //Concatenate.format(sb,(long)mn,2,2).append(":");
      //Concatenate.format(sb,(long)sec,2,2).append(".");
      //Concatenate.format(sb,(long)frac,3,3);

      // To .001 s precision
      return LeapSeconds.stringToTrue(sb.toString(), "yyyy DDD HH:mm:ss.SSS"); // aww 2008/02/04
      */

      // NCEDC tables data epoch times include leap seconds; 
      // internally, the jasi java code uses nominal epoch seconds.
      // Leap seconds should be substracted/added when data is 
      // read/written from/to datasource by the concrete subtypes,
      // Use "nominal" time in the data request to the db for
      // timeseries, the nominal times are used to filter the
      // miniseed packet header times.
      // DataSource:
      // isDbTimeBaseLeap(), isDbTimeBaseNominal() 

      double fsec = (double) sec + (double)frac/10000.0;
      DateTime datetime = new DateTime(yr, 0, jday, hr, mn, fsec);
      return datetime.getTrueSeconds(); //  aww 2008/02/04

    }
}
