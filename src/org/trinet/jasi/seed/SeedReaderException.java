package org.trinet.jasi.seed;

/** Exception thrown on error while reading and parsing SEED files. */
public class SeedReaderException extends RuntimeException {
    public SeedReaderException() {
        super();
    }

    public SeedReaderException(String message) {
        super(message);
    }
}
