package org.trinet.jdbc;
import java.sql.*;
import org.trinet.jiggle.common.DbConstant;
import org.trinet.jiggle.common.LogUtil;
import org.trinet.jiggle.database.JiggleConnection;
import org.trinet.jiggle.database.JiggleOracleConnection;
import org.trinet.jiggle.database.JigglePgConnection;

/**
* Utility class to create Oracle or PostgreSQL JDBC connections.
* Drivers no longer have to be explicitely registered, can use single
* connection factory for either PostgreSQL or Oracle.
*/
public class ConnectionFactory implements ConnectionFactoryIF {

    /** String to allow caller to show cause of error to the user. Needed
    *  because all exceptions are caught here and not passed along.
    */
    private static String status = "";

    // Null constructor
    public ConnectionFactory() {}

    /** 
    * Relevant for Oracle databases only. Returns true if instance is (stored). 
    * That is the JVM is local to database server host. 
    */
    public boolean onServer() {
        // if on db server, returns String representing Oracle database release, otherwise null
        return (System.getProperty("oracle.server.version") != null);
    }
    /**
     * Creates new Connection instance using the String url and user data.
     * If input driverName is not null or empty, creates instance of clapasswrds specified by driverName
     * otherwise, a default driver is used. 
     * Use to create mutiple Connection objects with different URL's.
     * 
     */
    public Connection createConnection(String url, String driverName, String user, String passwd){
        Connection conn = null;
        try {
            if (driverName != null) {
                Class.forName(driverName);
            }
        } catch (Exception ex) {
            // ignore
        }
        try {
            // NOTE: user,password are ignored by if using the a server internal default URL
            conn = DriverManager.getConnection(url, user, passwd);

            if (conn != null) {
              configureConnection(conn);
              status = "OK";
            } else {
              status = "null";
            }
        }
        catch (SQLException ex) {
            SQLExceptionHandler.handleException(ex, conn);
            status = ex.getMessage();
            System.err.println("ERROR Connection data is incorrect or database is unavailable:\n");
            System.err.println("CONNECTION URL: " + url + "   for USER: " + user + ". Also, please verify your password is correct." + "\n");
        }
        catch (Exception ex) {
            ex.printStackTrace ();
            status = ex.getMessage();
        }
        return conn;
    }

    private static void configureConnection(Connection conn) throws SQLException {
        if  ( conn.getMetaData().getDriverName().toLowerCase().contains(DbConstant.DbType.ORACLE.getDbName()) ) {
            oracle.jdbc.OracleConnection oconn = (oracle.jdbc.OracleConnection) conn;
            oconn.setAutoCommit(false);
            oconn.setDefaultRowPrefetch(200); // default=1; avoid row roundtrips
            oconn.setImplicitCachingEnabled(true);
            oconn.setStatementCacheSize(24);
        } else {
            if (!conn.getMetaData().getURL().toLowerCase().contains(DbConstant.DbType.DEFAULT_SERVER.getDbName())) {
                conn.setAutoCommit(false);
            }
        }
    }

    /** Return the status string of the last connection creation. */
    public String getStatus() {
        return status;
    }

/** Returns the connection object for the server driver of the host machine database.
*   This is for server-side use only, returns null if onServer() == false.
*/
    public Connection createInternalDbServerConnection() {
        // Assume to be default connection. Removing onserver check.
        // if (! onServer()) {
        //     System.err.println("ERROR ConnectionFactory internal connection not on server!");
        //    return null;
        // }

        Connection conn = null;

        try {
            conn = DriverManager.getConnection("jdbc:default:connection");
            if (conn == null) {
                String errMsg = "ERROR ConnectionFactory DriverManager getConnection is NULL";
                LogUtil.error(errMsg);
                System.err.println(errMsg);
            }

            configureConnection(conn);
            status = "OK";
        }
        catch (SQLException ex) {
            SQLExceptionHandler.prtSQLException(ex);
            status = ex.getMessage();
        }
        return conn;
    }

    /**
     * Create a database specific object type based on the Connection for database dependent logic
     */
    @Override
    public JiggleConnection getJiggleConnection(Connection conn) {
        JiggleConnection jiggleConn = null;
        if (conn != null) {
            DbConstant.DbType dbType = getDBType(conn);
            switch (dbType) {
                case ORACLE:
                    jiggleConn = new JiggleOracleConnection();
                    break;
                case POSTGRES:
                    jiggleConn = new JigglePgConnection();
                    break;
                default:
                    status = "Unknown database type detected. Please check your database connection";
                    System.err.println("Error in getJiggleConnection: " + status);
            }
        }
        return jiggleConn;
    }

    /**
     * Detect the database type
     */
    private DbConstant.DbType getDBType(Connection conn) {
        DbConstant.DbType dbType = DbConstant.DbType.UNKNOWN;
        try {
            dbType = DbConstant.getDBType(conn);
        } catch (SQLException ex) {
            ex.printStackTrace();
            status = ex.getMessage();
            System.err.println("Error in getDBType: " + status);
        }
        return dbType;
    }
}
