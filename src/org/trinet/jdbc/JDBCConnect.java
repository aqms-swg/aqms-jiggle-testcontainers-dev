package org.trinet.jdbc;
import java.io.*;
import java.sql.*;
import java.util.*;
import org.trinet.util.MissingPropertyException;
import org.trinet.jdbc.datasources.AbstractSQLDataSource;
//import oracle.jdbc.driver.*;

public class JDBCConnect {
    public static Connection createDefaultConnection() {
        return createDefaultConnection(System.getProperties());
    }

    public static Connection createDefaultConnection(String propertyFileName) {
        Properties props = new Properties();
        try {
           props.load(new BufferedInputStream(new FileInputStream(propertyFileName)));
        }
        catch(IOException ex) {
          ex.printStackTrace();
          return null;
        }
        catch(SecurityException ex) {
          ex.printStackTrace();
          return null;
        }
        return createDefaultConnection(props);
    }

    public static Connection createDefaultConnection(Properties props) {
        //Danger below results in infinite loop if implemented with dbms_java.set_output in pl/sql
        //DriverManager.setLogStream(System.out);
        Connection conn = null;
        if (System.getProperty("oracle.jserver.version") != null) {
            //System.out.println("serverside");
            conn = createConnectionOnServer();
        }
        else {
            //System.out.println("clientside");
            String url = props.getProperty("tpp.default.jdbc.URL");
            if (url == null) {
            throw new MissingPropertyException("JDBCConnect getDefaultConnection tpp.default.jdbc.URL not defined.");
        }
        String user = props.getProperty("tpp.default.jdbc.user");
        if (user == null) {
          throw new MissingPropertyException("JDBCConnect getDefaultConnection tpp.default.jdbc.user not defined.");
        }

        String password= props.getProperty("tpp.default.jdbc.password");
        if (password == null) {
          throw new MissingPropertyException("JDBCConnect getDefaultConnection tpp.default.jdbc.password not defined.");
        }

        conn = createConnection( url, AbstractSQLDataSource.DEFAULT_DS_DRIVER, user, password);
      }
      return conn;
    }

/** Returns the connection object for the server driver of the host machine database.
* This is for server-side use only.
*/
    public static Connection createConnectionOnServer() {
        Connection conn = null;
        try {
          oracle.jdbc.driver.OracleDriver odriver = new oracle.jdbc.driver.OracleDriver();
          DriverManager.registerDriver(odriver);
          conn = odriver.defaultConnection();
          configureConnection(conn);
        }
        catch (SQLException ex) {
          ex.printStackTrace();
        }
//        dumpSystemProps();
        return conn;
    }

/** Creates JDBC connection to a remote DB using input parameters. */
    public static Connection createConnection(String url, String driverName, String user, String passwd){
        Connection conn = null;
        try {
          Class.forName(driverName);
          conn = DriverManager.getConnection(url, user, passwd);
          configureConnection(conn);
        }
        catch (ClassNotFoundException ex) {
          System.err.println("Cannot find the database driver classes.");
          System.err.println(ex);
        }
        catch (SQLException ex) {
          ex.printStackTrace();
        }
        catch (Exception ex) {
          ex.printStackTrace ();
        }
 //          dumpSystemProps();
        return conn;
    }

    private static void configureConnection(Connection conn) {
        try {
          conn.setAutoCommit(false);
          ((oracle.jdbc.OracleConnection) conn).setDefaultRowPrefetch(200);
        }
        catch (SQLException ex) {
          ex.printStackTrace();
        }
    }

    private static void dumpSystemProps() {
        try {
          java.util.Properties props = System.getProperties();
          props.list(System.out);
/*
    PrintWriter pw = new PrintWriter(new FileOutputStream("/home/awwalter/propsys.out"));
                props.list(pw);
                pw.flush();
                pw.close();
*/
        }
        catch (Exception ex) {System.err.println(ex.getMessage()); }
    }
}
