package org.trinet.jdbc;
import java.sql.*;
import java.util.*;
import java.math.*;
import oracle.jdbc.driver.*;

/**
 * Create and manage up to multiple (MAXNUM) JDBC connections.
 */
public class JDBCConnectionPool{
/** Maximum number of connections allowed by this class. */
    public static final int MAXNUM = 5; // Maximum number of connections
/** Array of connection objects maintained by this class.*/
    public static Connection [] conn = new Connection[MAXNUM];
/** URL for connection object at same index offset. */
    public static String [] url = new String[MAXNUM];
/** Driver for connection object at same index offset. */
    public static String [] driver = new String[MAXNUM];
/** User login name for connection object at same index offset. */
    public static String [] user = new String[MAXNUM];
/** User pasword for connection object at same index offset. */
    public static String [] passwd = new String[MAXNUM];

/** Stack of connection indexes in instantiation order. */
    private static ArrayList lastConnectIndex = new ArrayList();
/** Number of open connection objects. */
    private static int idsInUse = -1;
    
    
    public JDBCConnectionPool() {};
    
/** Creates a new JDBC connection from pool using input data.
* Returns -1 if the maximum number of connections is already open.
* Returns -1 if an SQLException occurs.
*/
    public static int create(String URL, String driverName, String userName, String passWord){
        int id = -1;
        int retVal = -1;
    
        for (int i=0; i < MAXNUM; i++) {
            if (conn[i] == null) {
              id = i;
              break;
            }
        }
        if (id == -1) {
            System.err.println("JDBCConnectionPool maximum connections:" + MAXNUM + " exceeded.");
            return retVal;
        }
    
        url[id] = URL;
        driver[id] = driverName;
        user[id] = userName;
        passwd[id] = passWord;
    
        try {
            Class.forName(driverName);
            conn[id] = (Connection) DriverManager.getConnection(URL, userName, passWord);
            conn[id].setAutoCommit(false);
            setIdsInUse();
            retVal = id;
            lastConnectIndex.add(Integer.valueOf(id));
        }
        catch (ClassNotFoundException ex) {
            System.err.println("JDBCConnectionPool cannot find the database driver classes.");
            System.err.println(ex);
        }
        catch (SQLException ex) {   
            SQLExceptionHandler.prtSQLException(ex);
        }
        catch (Exception ex) {
            ex.printStackTrace ();
        }
        return retVal;
    }

/** Returns the URL string for the specified connection array index (id).
* Returns null if input index is out of range.
*/ 
    public static String getURL(int id) {
        if (id >= 0 && id < MAXNUM) return url[id];
        else
        {
            System.err.println("JDBCConnectionPool getURL invalid connection id;" +
            " id must be between 0 and " + MAXNUM);
            return null;
        }
    }
    
/** Returns the Driver string for the specified connection array index (id).
* Returns null if input index is out of range.
*/ 
    public static String getDriver(int id) {
        if (id >= 0 && id < MAXNUM)
            return driver[id];
        else
        {
            System.err.println("JDBCConnectionPool getDriver invalid connection id;" +
            " id must be between 0 and " + MAXNUM);
            return null;
        }
    }
    
/** Returns the User string for the specified connection array index (id).
* Returns null if input index is out of range.
*/ 
    public static String getUser(int id) {
        if (id >= 0 && id < MAXNUM)
            return user[id];
        else
        {
            System.err.println("JDBCConnectionPool getUser invalid connection id;" +
            " id must be between 0 and " + MAXNUM);
            return null;
        }
    }
    
/** Returns the Password string for the specified connection array index (id).
* Returns null if input index is out of range.
*/ 
    public static String getPassword(int id) {
        if (id >= 0 && id < MAXNUM)
            return passwd[id];
        else
        {
            System.err.println("JDBCConnectionPool getPassword invalid connection id;" +
            " id must be between 0 and " + MAXNUM);
            return null;
        }
    }

/** Returns the index (id) of the last created connection.
* Returns -1 if no connections are open.
*/ 
    public static int getLastConnectionId() {
        int retVal = -1;
        if (! lastConnectIndex.isEmpty()) {
            try {
            retVal = ((Integer) lastConnectIndex.get(lastConnectIndex.size()-1)).intValue();
            }
            catch (IndexOutOfBoundsException ex) {}
        }
        return retVal;
    }

/** Returns the index (id) of a connection matching the specified input connection. 
* Returns -1 if no connections match the input connection.
*/ 
    public static int getConnectionId(Connection connection) {
        for (int i = 0; i < MAXNUM; i++) {
            if (conn[i].equals(connection)) return i;
        }
        return -1;
    }
    
/** Returns the index (id) of a connection matching the specified input arguments. 
* Returns -1 if no connections match the input criteria.
*/ 
    public static int getConnectionId(String URL, String driverName, String userName, String passWord) {
        for (int i = 0; i < MAXNUM; i++) {
            if (url[i].equals(URL) && driver[i].equals(driverName) && user[i].equals(userName) &&  passwd[i].equals(passWord)) 
            return i;
        }
        return -1;
    }
    
/** Returns a Connection object matching the input connection data strings. 
* If none match, create a new Connection using the input criteria.
* Returns null if the maximun number of connections are in use.
* Returns null if an SQLException occurs.
*/ 
    public static Connection getConnection(String URL, String driverName, String userName, String passWord) {
// should check for existing match first 
 
        for (int i = 0; i < MAXNUM; i++) {
            if (url[i] == null) continue;
            if (url[i].equals(URL) && driver[i].equals(driverName) && user[i].equals(userName) &&  passwd[i].equals(passWord)) 
            return conn[i];
        }
        return getConnection(create(URL, driverName, userName, passWord));
    }
    
/** Returns the Connection object at the specified index offset. 
* Returns null if input index is out of range.
*/ 
    public static Connection getConnection(int id) {
        if (id >= 0 && id < MAXNUM)
            return conn[id];
        else
        {
            System.err.println("JDBCConnectionPool getConnection invalid connection id;" +
            " id must be between 0 and " + MAXNUM);
            return null;
        }
    }
    
/** Returns number of connections in use. */
    public static int getInUse() {
        return idsInUse;
    }
    
/** Resets the object making available the index position of any closed connection. 
* Sets all array data null where Connection.isClosed() == true.
* Sets the ids in use counter.
*/
    public static int setIdsInUse (){
        int icount = 0;
        for (int i=0; i < MAXNUM; i++) {
            if (conn[i] != null) {
                try {
                    if (conn[i].isClosed()) {
                        conn[i] = null;
                        url[i] = null;
                        driver[i] = null;
                        user[i] = null;
                        passwd[i] = null;
                    }
                    else icount++;
                }
                catch (SQLException ex) {
                    SQLExceptionHandler.prtSQLException(ex);
                }
            }
        }
        idsInUse = icount;
        return icount;
    }

/** Commits the connection at the specified index (id).
*/
    public static void commit(int id) {
        try {
            conn[id].commit();
        }
        catch (SQLException ex){
            SQLExceptionHandler.handleException(ex, conn[id]);
        }
    }

/** Closes the connection at the specified index (id).
* Makes its connection aray slot available for re-use.
*/
    public static void close(int id) {
        if (id >= 0 && id < MAXNUM && conn[id] != null) {
          try {
            conn[id].close();
          }
          catch (SQLException ex){
            SQLExceptionHandler.handleException(ex, conn[id]);
          }
          finally {
            conn[id] = null;
            url[id] = null;
            driver[id] = null;
            user[id] = null;
            passwd[id] = null;
            idsInUse--;
            lastConnectIndex.remove(Integer.valueOf(id));
          }
        }
        else
        {
          System.err.println("JDBCConnectionPool close invalid connection id;" +
          " id must be between 0 and " + MAXNUM + " and not NULL" );
          return;
        }
    }

/** Commits all connections maintained by this object. */
    public static void commitAll() {
        for (int id = 0; id < MAXNUM; id++) {
            try {
                if (conn[id] != null) conn[id].commit();
            }
            catch (SQLException ex){
                SQLExceptionHandler.handleException(ex, conn[id]);
            }
        }
    }

/** Closes all connections maintained by this object. */
    public static void closeAll() {
        for (int id = 0; id < MAXNUM; id++) {
            try {
                if (conn[id] != null) conn[id].close();
            }
            catch (SQLException ex){
                SQLExceptionHandler.handleException(ex, conn[id]);
            }
            finally {
                conn[id] = null;
                url[id] = null;
                driver[id] = null;
                user[id] = null;
                passwd[id] = null;
            }
        }
        idsInUse = -1;
        lastConnectIndex.clear();
    }

/** Prints the URL, Driver, User, and Password strings used to create the connection at the specified index (id).
* Does a no-op, if input index is out of range.
*/
    public static void printConnectionInfo(int idx) {
        if (idx < 0 || idx > MAXNUM) return;
/*
        System.out.println("URL     : " + getURL(idx));
        System.out.println("Driver  : " + getDriver(idx));
        System.out.println("User    : " + getUser(idx));
        System.out.println("Password: " + getPassword(idx));
        System.out.println("ConnectionId: " + idx);
*/
        System.out.println(getConnectionInfo(idx));
   }  

/** For the specified index (id), returns the URL, Driver, User, and Password strings separated by newlines.
* Return null, if input index is out of range.
*/
    public static String getConnectionInfo(int idx) {
        if (idx < 0 || idx > MAXNUM) return null;
        StringBuffer sb = new StringBuffer(256);
        sb.append("URL      : ").append(getURL(idx)).append("\n");
        sb.append("Driver   : ").append(getDriver(idx)).append("\n");
        sb.append("User     : ").append(getUser(idx)).append("\n");
        //sb.append("Password : ").append(getPassword(idx)).append("\n");
        sb.append("ConnectId: ").append(idx);
        return sb.toString();
   }  
 
/* Abstract class or extend to subclass a static method implement Vendor specifics:
 * Creates a new JDBC connection from pool to default connection (SERVER side only).
* Returns -1 if the maximum number of connections is already open.
* Returns -1 if an SQLException occurs.
        if (URL.equals("default")) return createDefaultConnection();
// DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
    public static int createDefaultConnection() {
        int id = -1;
        int retVal = -1;
    
        for (int i=0; i < MAXNUM; i++) {
            if (conn[i] == null) { // an available slot
              id = i;
              break;
            }
        }
        if (id == -1) {
    //      throw new DataFormatException("Max connections:" + MAXNUM + " exceeded.") ;
            System.err.println("JDBCConnectionPool maximum connections:" + MAXNUM + " exceeded.");
            return retVal;
        }
        // set chosen id connection description attributes: 
        url[id] = "default";
        driver[id] = "default";
        user[id] = "default";
        passwd[id] = "default";
    
        try {
            OracleDriver odriver = new OracleDriver();
            conn[id] = odriver.defaultConnection();
            conn[id].setAutoCommit(false);
            setIdsInUse();
            retVal = id;
            lastConnectIndex.add(Integer.valueOf(id));
        }
        catch (SQLException ex) {   
            SQLExceptionHandler.prtSQLException(ex);
        }
        catch (Exception ex) {
            ex.printStackTrace ();
        }
        return retVal;
    }
*/
}
