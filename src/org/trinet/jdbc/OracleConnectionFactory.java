package org.trinet.jdbc;
import java.sql.*;
import oracle.jdbc.driver.*;
import oracle.jdbc.*;
import org.trinet.jiggle.database.JiggleConnection;
import org.trinet.jiggle.database.JiggleOracleConnection;

/**
 * Utility class to creates Oracle JDBC connections.
 * NOTE: Loads ORACLE default driver upon initialization (not database generic).
 */
public class OracleConnectionFactory implements ConnectionFactoryIF {

    /** String to allow caller to show cause of error to the user. Needed
     *  because all exceptions are caught here and not passed along.*/
    private static String status = "";

    private static final Driver odriver = new oracle.jdbc.driver.OracleDriver(); 

    static { // register default Oracle driver once (its stored in a static vector)
        try {
          DriverManager.registerDriver(odriver);
        }
        catch (SQLException ex) {
          System.err.println("ERROR OracleConnectionFactory: Unable to register driver: " + ex.getMessage());
        }
    }

    // Null constructor
    public OracleConnectionFactory() {}

    /** Returns true if instance is (stored). That is the JVM is local to database server host. */
    public boolean onServer() {
        // if on db server, returns String representing Oracle database release, otherwise null
        return (System.getProperty("oracle.server.version") != null);
    }
    /**
     * Creates new Connection instance using the String url and user data.
     * If input driverName is not null or empty, creates instance of class specified by driverName
     * and registers it with the DriverManager, otherwise, a default driver is used. 
     * Use to create mutiple Connection objects with different URL's.
     * */
    public Connection createConnection(String url, String driverName, String user, String passwd){
        Connection conn = null;
        try {
            //
            // ORACLE9i server default internal URL's:
            //   "jdbc:oracle:kprb:" || "jdbc:default:connection:" 
            //

            if (driverName != null && driverName.length() > 0) { // aww
              Driver d = (Driver) Class.forName(driverName).newInstance();
              DriverManager.registerDriver(d); 
            }

            // NOTE: user,password are ignored by if using the a server internal default URL
            conn = DriverManager.getConnection(url, user, passwd);

            if (conn != null) {
              configureConnection(conn);
              status = "OK";
            } else {
              status = "null";
            }
        }
        catch (ClassNotFoundException ex) {
            System.err.println("ERROR Cannot find the database driver classes.");
            System.err.println(ex);
            status = ex.getMessage();
        }
        catch (SQLException ex) {
            SQLExceptionHandler.handleException(ex, conn);
            status = ex.getMessage();
            System.err.println("ERROR Connection data is incorrect or database is unavailable:\n");
            System.err.println("CONNECTION URL: " + url + "   for USER: " + user + ". Also, please verify your password is correct." + "\n");
        }
        catch (Exception ex) {
            ex.printStackTrace ();
            status = ex.getMessage();
        }
        return conn;
    }

    private static void configureConnection(Connection conn) throws SQLException {
        oracle.jdbc.OracleConnection oconn = (oracle.jdbc.OracleConnection) conn;
        oconn.setAutoCommit(false);
        oconn.setDefaultRowPrefetch(200); // default=1; avoid row roundtrips
        oconn.setImplicitCachingEnabled(true);
        oconn.setStatementCacheSize(24);
        //Below applies to Oracle batch update model executeUpdate()...sendBatch(),
        //not to the std JDBC batch model (s.addBatch... s.executeBatch())
        //oconn.setDefaultExecuteBatch(25);
        //System.out.println("OracleConn: " + oconn.getDefaultRowPrefetch() + " stmtCaching: " + oconn.getImplicitCachingEnabled() + " cacheSize: " + oconn.getStatementCacheSize());
    }

    /** Return the status string of the last connection creation. */
    public String getStatus() {
        return status;
    }

/** Returns the connection object for the server driver of the host machine database.
*   This is for server-side use only, returns null if onServer() == false.
*/
    public Connection createInternalDbServerConnection() {

        if (! onServer()) {
            System.err.println("ERROR OracleConnectionFactory internal connection not on server!");
            return null;
        }

        Connection conn = null;

        try {

            conn = DriverManager.getConnection("jdbc:oracle:kprb:");
            if (conn == null) System.err.println("ERROR OracleConnectionFactory DriverManager getConnection is NULL");

            // below works for Oracle9+ internal:
            //conn = DriverManager.getConnection("jdbc:default:connection:");
            //conn = odriver.defaultConnection();

            configureConnection(conn);
            status = "OK";
        }
        catch (SQLException ex) {
            SQLExceptionHandler.prtSQLException(ex);
            status = ex.getMessage();
        }
        return conn;
    }

    @Override
    public JiggleConnection getJiggleConnection(Connection conn) {
        return new JiggleOracleConnection();
    }

/** Returns an alias copy of a server connection object from the server 
 * of the host machine database.  
*   This is for server-side use only, returns null if onServer() == false.
    public Connection createDefaultConnectionCopy() {
        if (! onServer()) return null;
        Connection conn = null;
        try {
            conn = DriverManager.getConnection("jdbc:oracle:kprb:");
            // below works for Oracle9+ internal:
            //conn = DriverManager.Connection("jdbc:default:connection:");
            configureConnection(conn);
            status = "OK";
        }
        catch (ClassNotFoundException ex) {
            System.err.println("ERROR Cannot find the database driver classes.");
            System.err.println(ex);
            status = ex.getMessage();
        }
        catch (SQLException ex) {
            SQLExceptionHandler.prtSQLException(ex);
            status = ex.getMessage();
        }
        return conn;
    }
*/
}
