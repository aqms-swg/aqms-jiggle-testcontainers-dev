package org.trinet.jdbc;
import java.sql.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;

/** Wrapper around a JDBC ResultSet interface to provide methods for creating org.trinet.jdbc.datatypes.DataObjects
* from parsing the ResultSet column data. As with ResultSet objects column index references begin at 1, not 0.
* Used by NCEDC schema table class DataTableRow.
* @see org.trinet.jdbc.table.DataTableRow
*/
public class ResultSetDb {
/** Handle to ResultSet object */
    ResultSet rs;

/** Default constructor intializes a null ResultSet. */
    public ResultSetDb() {rs = null;}

/** Initializes this object with the specified JDBC ResultSet, presumably returned from a databasae table query. */
    public ResultSetDb(ResultSet rs) { this.rs = rs; }

/** Set the alias to the handle of JDBC ResultSet used by this object. */
    public void setResultSet(ResultSet rs) {
	this.rs = rs;
    }

/** Returns the alias handle of the JDBC ResultSet used by this object. */
    public ResultSet getResultSet() {
	return rs;
    }

/** If ResultSet object for specified column name is null, returns "NULL". */
    public String getStringNull(String colName) throws SQLException {
	    String str = rs.getString(colName);
	    if (str == null) return NullValueDb.NULL_STRING;
	    return str;
    }
/** If ResultSet object at specified column index is null, returns "NULL". */
    public String getStringNull(int colNumber) throws SQLException {
	    String str = rs.getString(colNumber);
	    if (str == null) return NullValueDb.NULL_STRING;
	    return str;
    }
/** If ResultSet object for specified column name is null, returns "". */
    public String getStringEmpty(String colName) throws SQLException {
	    String str = rs.getString(colName);
	    if (str == null) return "";
	    return str;
    }
/** If ResultSet object at specified column index is null, returns "". */
    public String getStringEmpty(int colNumber) throws SQLException {
	    String str = rs.getString(colNumber);
	    if (str == null) return "";
	    return str;
    }

/** Returns a new DataObject whose value is set to that found in the ResultSet for the specified column name.
* If ResultSet object for specified column name is null, returns NullValueDb.NULL_xxx for the contained value type.
* and sets the state flags isNull() == true and isUpdate() == true.
* @see org.trinet.jdbc.NullValueDb
* @see org.trinet.jdbc.datatypes.DataObject
* @see org.trinet.jdbc.datatypes.DataInteger
*/
    public DataInteger getDataInteger(String colName) throws SQLException {
        DataInteger value = new DataInteger(rs.getInt(colName));
	if (rs.wasNull()) {
	    value.setValue(NullValueDb.NULL_INT);
	    value.setNull(true);
	}
	value.setUpdate(false);
	return value;
    }

/** Returns a new DataObject whose value is set to that found in the ResultSet for the specified column index.
* If ResultSet object for specified column index is null, returns NullValueDb.NULL_xxx for the contained value type.
* and sets the state flags isNull() == true and isUpdate() == true.
* @see org.trinet.jdbc.NullValueDb
* @see org.trinet.jdbc.datatypes.DataObject
* @see org.trinet.jdbc.datatypes.DataInteger
*/
    public DataInteger getDataInteger(int colNumber) throws SQLException {
        DataInteger value = new DataInteger(rs.getInt(colNumber));
	if (rs.wasNull()) {
	    value.setValue(NullValueDb.NULL_INT);
	    value.setNull(true);
	}
	value.setUpdate(false);
	return value;
    }

/** Returns a new DataObject whose value is set to that found in the ResultSet for the specified column name.
* If ResultSet object for specified column name is null, returns NullValueDb.NULL_xxx for the contained value type.
* and sets the state flags isNull() == true and isUpdate() == true.
* @see org.trinet.jdbc.NullValueDb
* @see org.trinet.jdbc.datatypes.DataObject
* @see org.trinet.jdbc.datatypes.DataLong
*/
    public DataLong getDataLong(String colName) throws SQLException {
        DataLong value = new DataLong(rs.getLong(colName));
	if (rs.wasNull()) {
	    value.setValue(NullValueDb.NULL_LONG);
	    value.setNull(true);
	}
	value.setUpdate(false);
	return value;
    }

/** Returns a new DataObject whose value is set to that found in the ResultSet for the specified column index.
* If ResultSet object for specified column index is null, returns NullValueDb.NULL_xxx for the contained value type.
* and sets the state flags isNull() == true and isUpdate() == true.
* @see org.trinet.jdbc.NullValueDb
* @see org.trinet.jdbc.datatypes.DataObject
* @see org.trinet.jdbc.datatypes.DataLong
*/
    public DataLong getDataLong(int colNumber) throws SQLException {
        DataLong value = new DataLong(rs.getLong(colNumber));
	if (rs.wasNull()) {
	    value.setValue(NullValueDb.NULL_LONG);
	    value.setNull(true);
	}
	value.setUpdate(false);
	return value;
    }

/** Returns a new DataObject whose value is set to that found in the ResultSet for the specified column name.
* If ResultSet object for specified column name is null, returns NullValueDb.NULL_xxx for the contained value type.
* and sets the state flags isNull() == true and isUpdate() == true.
* @see org.trinet.jdbc.NullValueDb
* @see org.trinet.jdbc.datatypes.DataObject
* @see org.trinet.jdbc.datatypes.DataFloat
*/
    public DataFloat getDataFloat(String colName) throws SQLException {
        DataFloat value = new DataFloat(rs.getFloat(colName));
	if (rs.wasNull()) {
	    value.setValue(NullValueDb.NULL_FLOAT);
	    value.setNull(true);
	}
	value.setUpdate(false);
	return value;
    }

/** Returns a new DataObject whose value is set to that found in the ResultSet for the specified column index.
* If ResultSet object for specified column index is null, returns NullValueDb.NULL_xxx for the contained value type.
* and sets the state flags isNull() == true and isUpdate() == true.
* @see org.trinet.jdbc.NullValueDb
* @see org.trinet.jdbc.datatypes.DataObject
* @see org.trinet.jdbc.datatypes.DataFloat
*/
    public DataFloat getDataFloat(int colNumber) throws SQLException {
        DataFloat value = new DataFloat(rs.getFloat(colNumber));
	if (rs.wasNull()) {
	    value.setValue(NullValueDb.NULL_FLOAT);
	    value.setNull(true);
	}
	value.setUpdate(false);
	return value;
    }

/** Returns a new DataObject whose value is set to that found in the ResultSet for the specified column name.
* If ResultSet object for specified column name is null, returns NullValueDb.NULL_xxx for the contained value type.
* and sets the state flags isNull() == true and isUpdate() == true.
* @see org.trinet.jdbc.NullValueDb
* @see org.trinet.jdbc.datatypes.DataObject
* @see org.trinet.jdbc.datatypes.DataDouble
*/
    public DataDouble getDataDouble(String colName) throws SQLException {
        DataDouble value = new DataDouble(rs.getDouble(colName));
	if (rs.wasNull()) {
	    value.setValue(NullValueDb.NULL_DOUBLE);
	    value.setNull(true);
	}
	value.setUpdate(false);
	return value;
    }

/** Returns a new DataObject whose value is set to that found in the ResultSet for the specified column index.
* If ResultSet object for specified column index is null, returns NullValueDb.NULL_xxx for the contained value type.
* and sets the state flags isNull() == true and isUpdate() == true.
* @see org.trinet.jdbc.NullValueDb
* @see org.trinet.jdbc.datatypes.DataObject
* @see org.trinet.jdbc.datatypes.DataDouble
*/
    public DataDouble getDataDouble(int colNumber) throws SQLException {
        DataDouble value = new DataDouble(rs.getDouble(colNumber));
	if (rs.wasNull()) {
	    value.setValue(NullValueDb.NULL_DOUBLE);
	    value.setNull(true);
	}
	value.setUpdate(false);
	return value;
    }

/** Returns a new DataObject whose value is set to that found in the ResultSet for the specified column name.
* If ResultSet object for specified column name is null, returns NullValueDb.NULL_xxx for the contained value type.
* and sets the state flags isNull() == true and isUpdate() == true.
* @see org.trinet.jdbc.NullValueDb
* @see org.trinet.jdbc.datatypes.DataObject
* @see org.trinet.jdbc.datatypes.DataBoolean
*/
    public DataBoolean getDataBoolean(String colName) throws SQLException {
        DataBoolean value = new DataBoolean(rs.getBoolean(colName));
	if (rs.wasNull()) {
	    value.setValue(false);
	    value.setNull(true);
	}
	value.setUpdate(false);
	return value;
    }

/** Returns a new DataObject whose value is set to that found in the ResultSet for the specified column index.
* If ResultSet object for specified column index is null, returns NullValueDb.NULL_xxx for the contained value type.
* and sets the state flags isNull() == true and isUpdate() == true.
* @see org.trinet.jdbc.NullValueDb
* @see org.trinet.jdbc.datatypes.DataObject
* @see org.trinet.jdbc.datatypes.DataBoolean
*/
    public DataBoolean getDataBoolean(int colNumber) throws SQLException {
        DataBoolean value = new DataBoolean(rs.getBoolean(colNumber));
	if (rs.wasNull()) {
	    value.setValue(false);
	    value.setNull(true);
	}
	value.setUpdate(false);
	return value;
    }

/** Returns a new DataObject whose value is set to that found in the ResultSet for the specified column name.
* If ResultSet object for specified column name is null, returns NullValueDb.NULL_xxx for the contained value type.
* and sets the state flags isNull() == true and isUpdate() == true.
* @see org.trinet.jdbc.NullValueDb
* @see org.trinet.jdbc.datatypes.DataObject
* @see org.trinet.jdbc.datatypes.DataDate
*/
    public DataDate getDataDate(String colName) throws SQLException {
        DataDate value = null;
        // 03/29/2005 convert to Timestamp, using Date truncates time hr:mm to 00:00 local -aww
        String dstr = rs.getString(colName);
	    if (rs.wasNull()) {
            value = new DataDate();
	    }
        else {
            if (dstr.indexOf('.',dstr.length()-4) < 0) dstr += ".0";
            value = new DataDate(EpochTime.stringToDate(dstr));
        }
	    value.setUpdate(false);
	    return value;
    }

/** Returns a new DataObject whose value is set to that found in the ResultSet for the specified column index.
* If ResultSet object for specified column index is null, returns NullValueDb.NULL_xxx for the contained value type.
* and sets the state flags isNull() == true and isUpdate() == true.
* @see org.trinet.jdbc.NullValueDb
* @see org.trinet.jdbc.datatypes.DataObject
* @see org.trinet.jdbc.datatypes.DataDate
*/
    public DataDate getDataDate(int colNumber) throws SQLException {
        DataDate value = null;
        // 03/29/2005 convert to Timestamp, Date converts time to 00:00 local -aww
        //Timestamp ts = rs.getTimestamp(colNumber);
        String dstr = rs.getString(colNumber);
	    if (rs.wasNull()) {
            value = new DataDate();
	    }
        else {
            if (dstr.indexOf('.',dstr.length()-4) < 0) dstr += ".0";
            value = new DataDate(EpochTime.stringToDate(dstr));
        }
	    value.setUpdate(false);
	    return value;
    }

/** Returns a new DataObject whose value is set to that found in the ResultSet for the specified column name.
* If ResultSet object for specified column name is null, returns NullValueDb.NULL_xxx for the contained value type.
* and sets the state flags isNull() == true and isUpdate() == true.
* @see org.trinet.jdbc.NullValueDb
* @see org.trinet.jdbc.datatypes.DataObject
* @see org.trinet.jdbc.datatypes.DataTimestamp
*/
    public DataTimestamp getDataTimestamp(String colName) throws SQLException {
        DataTimestamp value = null;
        String dstr = rs.getString(colName);
	    if (rs.wasNull()) {
	        value = new DataTimestamp();
	    }
        else {
            if (dstr.indexOf('.',dstr.length()-4) < 0) dstr += ".0";
            value = new DataTimestamp(EpochTime.stringToDate(dstr).getTime());
        }
	    value.setUpdate(false);
	    return value;
    }

/** Returns a new DataObject whose value is set to that found in the ResultSet for the specified column index.
* If ResultSet object for specified column index is null, returns NullValueDb.NULL_xxx for the contained value type.
* and sets the state flags isNull() == true and isUpdate() == true.
* @see org.trinet.jdbc.NullValueDb
* @see org.trinet.jdbc.datatypes.DataObject
* @see org.trinet.jdbc.datatypes.DataTimestamp
*/
    public DataTimestamp getDataTimestamp(int colNumber) throws SQLException {
        DataTimestamp value = null;
        String dstr = rs.getString(colNumber);
	    if (rs.wasNull()) {
	        value = new DataTimestamp();
	    }
        else {
            if (dstr.indexOf('.',dstr.length()-4) < 0) dstr += ".0";
            value = new DataTimestamp(EpochTime.stringToDate(dstr).getTime());
        }
        value.setUpdate(false);
	    return value;
    }

/** Returns a new DataObject whose value is set to that found in the ResultSet for the specified column name.
* If ResultSet object for specified column name is null, returns NullValueDb.NULL_xxx for the contained value type.
* and sets the state flags isNull() == true and isUpdate() == true.
* @see org.trinet.jdbc.NullValueDb
* @see org.trinet.jdbc.datatypes.DataObject
* @see org.trinet.jdbc.datatypes.DataString
*/
    public DataString getDataString(String colName) throws SQLException {
        DataString value = new DataString(rs.getString(colName));
	if (rs.wasNull()) {
	    value.setValue(NullValueDb.NULL_STRING);
	    value.setNull(true);
	}
	value.setUpdate(false);
	return value;
    }

/** Returns a new DataObject whose value is set to that found in the ResultSet for the specified column index.
* If ResultSet object for specified column index is null, returns NullValueDb.NULL_xxx for the contained value type.
* and sets the state flags isNull() == true and isUpdate() == true.
* @see org.trinet.jdbc.NullValueDb
* @see org.trinet.jdbc.datatypes.DataObject
* @see org.trinet.jdbc.datatypes.DataString
*/
    public DataString getDataString(int colNumber) throws SQLException {
        DataString value = new DataString(rs.getString(colNumber));
	if (rs.wasNull()) {
	    value.setValue(NullValueDb.NULL_STRING);
	    value.setNull(true);
	}
	value.setUpdate(false);
	return value;
    }

/** Returns an object extending the DataObject.class whose class type is defined by the input Class argument,
* which must be a class extending the base DataObject class. The value of the returned DataObject is set to value
* as returned by the getDataXXX(int index)  methods in this class.
* If ResultSet object for specified column index is null, returns NullValueDb.NULL_xxx for the contained value type.
* and sets the state flags isNull() == true and isUpdate() == true.
* Checks input index against index of named column in ResultSet object.
* Returns null if the input Class is not one of the recognized extensions of DataObject.
* Throws NoSuchFieldException if specified column name is not found in ResultSet object.
* Throws IndexOutOfBoundsException if specified index exceeds column count in ResultSet object.
* @see org.trinet.jdbc.NullValueDb
* @see org.trinet.jdbc.datatypes.DataObject
* @see org.trinet.jdbc.datatypes.DataString
* @see org.trinet.jdbc.datatypes.DataLong
* @see org.trinet.jdbc.datatypes.DataDouble
* @see org.trinet.jdbc.datatypes.DataInteger
* @see org.trinet.jdbc.datatypes.DataFloat
* @see org.trinet.jdbc.datatypes.DataDate
* @see org.trinet.jdbc.datatypes.DataTimestamp
* @see org.trinet.jdbc.datatypes.DataBoolean
*/
//    public DataObject getDataObject (int index, String fieldName, Class fieldClass, DataTableRow row) throws NoSuchFieldException,
//    public DataObject getDataObject (int index, String fieldName, Class fieldClass) throws NoSuchFieldException,
    public DataObject getDataObject (int index, Class fieldClass) throws NoSuchFieldException,
		IndexOutOfBoundsException, SQLException {
//	int colIndex = this.findColumn(fieldName);
	int colIndex = index;
	if (colIndex < 0) throw new NoSuchFieldException("ResultSetDb getDataObject name table field not found. ");
	ResultSetMetaData rsmd = rs.getMetaData();
	if (index > rsmd.getColumnCount()) 
	    throw new IndexOutOfBoundsException("ResultSetDb getDataObject requested columnIndex offset not found in ResultSet");
//	try {
	    if (fieldClass.getName().endsWith("DataString")) {
		return getDataString(colIndex);
	    }
	    else if (fieldClass.getName().endsWith("DataDouble")) {
		return getDataDouble(colIndex);
	    }
	    else if (fieldClass.getName().endsWith("DataInteger")) {
		return getDataInteger(colIndex);
	    }
	    else if (fieldClass.getName().endsWith("DataDate")) {
		return getDataDate(colIndex);
	    }
	    else if (fieldClass.getName().endsWith("DataFloat")) {
		return getDataFloat(colIndex);
	    }
	    else if (fieldClass.getName().endsWith("DataLong")) {
		return getDataLong(colIndex);
	    }
	    else if (fieldClass.getName().endsWith("DataTimestamp")) {
		return getDataTimestamp(colIndex);
	    }
	    else if (fieldClass.getName().endsWith("DataBoolean")) {
		return getDataBoolean(colIndex);
	    }

/*	}
	catch (SQLException ex) {
	    System.out.println("ResultSetDb getDataObject field class: " + fieldClass.getName());
	    SQLExceptionHandler.prtSQLException(ex);
	}
*/
	return null;
    }

/** Returns the ResultSet column number corresponding the input column name string.
* Returns -1, if no such column or an SQLException occurs.
*/ 
    public int findColumn(String name) {
	int retVal = -1;
	try {
	    retVal =  rs.findColumn(name); 
	}
	catch (SQLException ex) {
	    System.out.println("ResultSetDb findColumn(name) table column not found?: " + name);
	    SQLExceptionHandler.prtSQLException(ex);
	}
	return retVal;
    }

    /** Returns true if another row exists in ResultSet, else false. */
    public boolean next() throws SQLException {
      return (rs == null) ? false : rs.next();
    }
}
