package org.trinet.jdbc;
import java.lang.*;
import java.sql.*;
/** Class to assist handling of JDBC SQLException */
public class SQLExceptionHandler {
/** Performs Connection.rollback() and prints the SQLException chain in detail. */
    public static void handleException(SQLException ex, Connection sqlCon) {
	try {
	    if (sqlCon != null) sqlCon.rollback();
	} catch (SQLException ex2) {}
	prtSQLException(ex);
    }
    
/** Prints the details of a SQLException chain.
* Writes ex.SQLState(), ex.getMessage(), ex.getErrorCode(), plus printStackTrace(ex)
*/
    public static void prtSQLException( SQLException ex) {
	System.err.println ("\n ----SQLexception caught---- \n");
	while (ex != null) {
	    System.err.println( "SQLstate: " + ex.getSQLState() );
	    System.err.println( "Message: " + ex.getMessage() );
	    System.err.println( "Vendor code: " + ex.getErrorCode() );
	    ex.printStackTrace(System.err);
	    ex = ex.getNextException();
	    System.err.println("");
	}
    }
}
