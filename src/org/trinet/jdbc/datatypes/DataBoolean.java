package org.trinet.jdbc.datatypes;
import java.util.*;
import org.trinet.jdbc.*;

/** Extends the base abstract DataObject class to implements a stateful boolean value.
*/
public class DataBoolean extends DataObject implements Comparable {
/** Data member holding value. */
    boolean value;

/** Constructor sets the object value to false and the object state flags to their default settings:
* (null:true, update:false, mutable:true).
*/
    public DataBoolean () {
        this.value = false;
    }
 
/** Constructor sets the object value to true if input argument != 0, else false.
* Sets the object state flags: isNull() == false, and isUpdate() == true. 
*/
    public DataBoolean (boolean value) {
        this.value = value;
        this.valueNull = false;
        this.valueUpdate = true;
    }
/** Constructor sets the object value to true if input argument != 0, else false.
* Sets the object state flags: isNull() == false, and isUpdate() == true. 
*/
    public DataBoolean (int value) {
        this((value != 0));
    }
/** Constructor sets the object value to true if input argument != 0, else false.
* Sets the object state flags: isNull() == false, and isUpdate() == true. 
*/
    public DataBoolean (long value) {
        this((value != 0l));
    }
/** Constructor sets the object value to true if input argument != 0, else false
 *  if input == 0. or NaN.
* Sets the object state flags isUpdate() == true and isNull() == false, unless input is NaN.
*/
    public DataBoolean (float value) {
        this();
        setValue(value);
    }
/** Constructor sets the object value to true if input argument != 0, else false
 *  if input is 0. or NaN.
* Sets the object state flags isUpdate() == true and isNull() == false, unless input is NaN.
*/
    public DataBoolean (double value) {
        this();
        setValue(value);
    }
/** Constructor sets the object value to true if argument is such that Double.parseDouble(inputString) != 0, else false.
* Sets the object state flags isUpdate() == true and isNull() == false, unless input is null.
*/
    public DataBoolean (String value) throws NumberFormatException {
        this();
        setValue(value);
    }

/** Copies the object value and state flag settings of the input argument object. 
* Null state is propagated.
*/ 
    public DataBoolean (DataBoolean object) {
        if (object == null) setNull(true);
        else {
           this.value        = object.value;
           this.valueNull    = object.valueNull;
           this.valueMutable = object.valueMutable;
           this.valueUpdate  = object.valueUpdate;
        }
    }

/** Returns the String equivalent of the object value: "true" or "false". */
    public String toString() {
        return String.valueOf(value); 
    }

/** Returns the String equivalent of the object value or "NULL" is isNull() == true. */
    public String toStringSQL() {
        if (valueNull) return "NULL";
        return StringSQL.valueOf(String.valueOf(value));
    }

/** Returns a String of "label: value" pairs for the object value and its state flags. 
* "Value: " + value.toString() + " Null: " + isNull() + " Update: " + isUpdate() + " Mutable: " + isMutable()
*/
    public String classToString() {
        StringBuffer sb = new StringBuffer(128);
        sb.append("                                           ");
        sb.insert(0, "Value:");
        sb.insert(7, value);
        sb.insert(32, "Null:");
        sb.insert(37, valueNull);
        sb.insert(43, "Update:");
        sb.insert(50, valueUpdate);
        sb.insert(56, "Mutable:");
        sb.insert(64, valueMutable);
        return sb.toString();
        //  return  "Value: " + value + " Null: " + valueNull + " Update: " + valueUpdate + " Mutable: " + valueMutable;
    }

/** Returns 1231 if the object value == true; 1237 if object value == false. See java.lang.Boolea.
* @see java.lang.Boolean
*/
    public int hashCode() {
        if (value == true) return 1231;
        else return 1237;
    }

/** Returns true if the input object is of type Boolean or DataBoolean and object.booleanValue() == this.booleanValue(). */
    public boolean equalsValue(Object object) {
        if (object == null ) return false;
        if (object instanceof Boolean) {
            return (value == ((Boolean) object).booleanValue());
        }
        else if (object instanceof DataBoolean) {
            return (value == ((DataBoolean) object).value);
        }
        else return false; 
    }

/** Returns true if the input object is of type DataBoolean, this.value == object.value, 
* and its state flags are equivalent to those of this object, else returns false.
*/
    public boolean equals(Object object) {
        if (object == null || ! (object instanceof DataBoolean)) return false;
        if (value == ((DataBoolean) object).value && 
           valueUpdate == ((DataBoolean) object).valueUpdate && 
           valueMutable == ((DataBoolean) object).valueMutable && 
           valueNull == ((DataBoolean) object).valueNull) return true;
        else return false;
    }

/** Returns 0 if this.value == object.value, returns 1 if this.value == true and object.value == false.
* Returns -1 if this.value == false and object.value == true.
* Input object must be of type java.lang.Boolean or DataBoolean.
*/
    public int compareTo(Object object) throws ClassCastException {
        if (object instanceof Boolean) { 
            if (this.value == ((Boolean) object).booleanValue()) return 0;
            else if (this.value == true && ((Boolean) object).booleanValue() == false) return 1;
            else return -1;
        }
        else if (object instanceof DataBoolean) { 
            return compareTo((DataBoolean) object);
        }
        else throw new ClassCastException("compareTo(object) argument must be a Boolean or DataBoolean class type: "
                + object.getClass().getName());
    }

/** Returns 0 if this.value == object.value, returns 1 if this.value == true and object.value == false.
* Returns -1 if this.value == false and object.value == true.
*/
    public int compareTo(DataBoolean object) {
        if (this.value == object.value) return 0;
        else if (this.value == true && ((DataBoolean) object).value == false) return 1;
        else return -1;
    }


/** Returns the object boolean value. */
    public boolean booleanValue() {
        return value;
    }

/** Returns 1 if the object boolean value is true, else 0. */
    public int intValue() {
        return (value) ? 1 : 0;
    }

/** Returns 1 if the object boolean value is true, else 0l */
    public long longValue() {
        return (value) ? 1l : 0l;
    }

/** Returns 1 if the object boolean value is true, else 0.f */
    public float floatValue() {
        return (value) ? 1.f : 0.f;
    }

/** Returns 1 if the object boolean value is true, else 0.d */
    public double doubleValue() {
        return (value) ? 1.: 0.;
    }

/** Sets the object boolean value to the boolean input argument.
* Sets the object state flags: isNull() == false, and isUpdate() == true .
* Does a no-op, if isMutable == false. 
*/
    public void setValue(boolean value) {
        if(! isMutable()) return;
        this.value = value;
        this.valueNull = false;
        this.valueUpdate = true;
    }

/** Sets the object boolean value to true if input argument != 0 .
* Sets the object state flags: isNull() == false, and isUpdate() == true .
* Does a no-op, if isMutable == false. 
*/
    public void setValue(int value) {
        setValue((value != 0));
    }

/** Sets the object boolean value to true if input argument != 0 .
* Sets the object state flags: isNull() == false, and isUpdate() == true .
* Does a no-op, if isMutable == false. 
*/
    public void setValue(long value) {
        setValue((value != 0l));
    }

/** Sets the object boolean value to true if input argument != 0 .
* Sets the object state flags: isUpdate() == true and isNull() == false, 
* unless input is NaN.
* Does a no-op, if isMutable == false. 
*/
    public void setValue(float value) {
        if (Float.isNaN(value)) setNull(true);
        else setValue((value != 0.f));
    }

/** Sets the object boolean value equals ignoring case to true if input argument != 0 .
* Sets the object state flags: isUpdate() == true and isNull() == false, 
* unless input is NaN.
* Does a no-op, if isMutable == false. 
*/
    public void setValue(double value) {
        if (Double.isNaN(value)) setNull(true);
        else setValue((value != 0.));
    }

/** Sets the object boolean value to true if either the input String equalsIgnoreCase("true") or
*  or the input can be parsed as a number != 0 and not NaN.
* Sets the object state flags: isNull() == false, and isUpdate() == true .
* Does a no-op, if isMutable == false. 
*/
    public void setValue(String value) {
        if(! isMutable()) return;
        if (value == null) {
            setNull(true);
        }
        else if (value.equalsIgnoreCase("true")) {
          setValue(true);
        }
        else if (Character.isDigit(value.trim().charAt(0))) {
          try {
            double d = Double.parseDouble(value);
            // true if a non-zero number value that is not NaN
            setValue( (d != 0. && Double.isNaN(d)) );
          }
          catch (NumberFormatException ex) {
            System.err.println("ERROR ! DataBoolean setValue NumberFormatException parsing : " + value);
          }
        }
        else setValue(false);
    }

/** Sets the object boolean value to true if input argument is a DataObject, Number,
 * or String object and its value parses != 0.
* Sets the state flags isUpdate() == true and isNull() == false,
* unless input is null or if input is a DataObject, object.isNull().
* Does a no-op, if isMutable == false. 
* Throws a ClassCastException if the input object is not one of these types.
*/
    public void setValue(Object object) throws ClassCastException {
        if(! isMutable()) return;
    //  if (object == null)  throw new NullPointerException("DataBoolean setValue(Object) null input argument");
        if (object == null) {
            setNull(true);
        }
        else if (Number.class.isInstance(object)) {
            setValue(((Number) object).intValue());
        }
        else if (DataObject.class.isInstance(object)) {
            DataObject obj = (DataObject) object;
            if (obj.isNull()) setNull(true);
            else setValue(obj.intValue());
        }
        else setValue(object.toString());
    }
/** Sets the object boolean value to false.
* Sets the object state flags: isNull() == tf, and isUpdate() == true .
* Does a no-op, if isMutable == false. 
*/
    public DataObject setNull(boolean tf) {
        if(! isMutable()) return this;
        this.value = false;
        this.valueUpdate = true;
        this.valueNull = tf;
        return this;
    }

/**
* Returns true if a value can be parsed from input StringTokenizer.
* Value is true only if tokenizer.nextToken.equalsIgnoreCase("true"), else value is set false..
* Does not set value and returns false if tokenizer.hasMoreTokens() == false.
*/
    public boolean parseValue(StringTokenizer tokenizer) {
        if (! tokenizer.hasMoreTokens()) return false;
        setValue( Boolean.valueOf(tokenizer.nextToken()).booleanValue() );
        return true;
    }

    public boolean isValidNumber() {
        return ! isNull();
    }
}
