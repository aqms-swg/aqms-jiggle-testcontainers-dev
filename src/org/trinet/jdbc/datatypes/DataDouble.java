package org.trinet.jdbc.datatypes;
import java.math.BigDecimal;
import java.util.*;
import org.trinet.jdbc.*;
import org.trinet.util.LeapSeconds;

/** Extends the DataObject class to implement a stateful double value. */
public class DataDouble extends DataObject implements DataNumeric, DataTime, Comparable, Cloneable {
/** Data member holding value. */
    double value;

/** Constructor sets the object value to NullValueDb.NULL_DOUBLE and the object state flags to their default settings
* (null: true; update: false; mutable: true)).
*/
    public DataDouble () {
        this.value = NullValueDb.NULL_DOUBLE;
    }
 
/** Constructor sets the object value to input argument and isNull() == false and isUpdate() == true. */
    public DataDouble (int value) {
        this((double) value);
    }

/** Constructor sets the object value to input argument and isNull() == false and isUpdate() == true. */
    public DataDouble (long value) {
        this((double) value);
    }

/** Constructor sets the object value to input argument.
 * Sets isUpdate() == true and isNull() == false, unless input satisfies Float.isNaN(value). 
 */
    public DataDouble (float value) {
        setValue(value);
    }

/** Constructor sets the object value to input argument.
 * Sets isUpdate() == true and isNull() == false, unless input satisfies Double.isNaN(value). 
 */
    public DataDouble (double value) {
        setValue(value);
    }

/** Constructor sets the object value to input argument.
 * Sets isUpdate() == true and isNull() == false, unless input is null.
 */
    public DataDouble (String value) throws NumberFormatException {
        this();
        setValue(value);
    }

/** Constructor copies the object value and state flag settings of the input argument object. 
* Null state is propagated.
*/
    public DataDouble (DataDouble object) {
        if (object == null) setNull(true);
        else {
            this.value = object.value;
            this.valueUpdate = object.valueUpdate;
            this.valueNull = object.valueNull;
            this.valueMutable = object.valueMutable;
        }
    }

/** Returns String representation of the object value.*/
    public String toString() {
        return String.valueOf(value); 
    }

/** Returns String representation of the object value or the string "NULL" if isNull() == true. */
    public String toStringSQL() {
        if (valueNull || Double.isNaN(value)) return "NULL";
        return StringSQL.valueOf(value);
    }

/** Returns a String of "label: value" pairs for the object value and its state flags. 
* If isNull() == true the string "NULL" is printed for the value.
* "Value: " + value.toString() + " Null: " + isNull() + " Update: " + isUpdate() + " Mutable: " + isMutable()
*/
    public String classToString() {
        StringBuffer sb = new StringBuffer(128);
        sb.append("                                           ");
        sb.insert(0, "Value:");
        if (isNull()) sb.insert(7, "NULL");
        else sb.insert(7, value);
        sb.insert(32, "Null:");
        sb.insert(37, valueNull);
        sb.insert(43, "Update:");
        sb.insert(50, valueUpdate);
        sb.insert(56, "Mutable:");
        sb.insert(64, valueMutable);
        return sb.toString().trim();
//        return  "Value: " + value + " Null: " + valueNull + " Update: " + valueUpdate + " Mutable: " + valueMutable;
    }

/** Returns Double.hashcode() for the object value. */
    @Override
    public int hashCode() {
        return Double.hashCode(value); 
    }

/** Returns true if the object value and its state flags are equivalent to those of the input object.
* Also returns true if the compared values are NaN and the state flags are equivalent.
* Returns false if the input object is null or is not of type DataDouble.
*/
    @Override
    public boolean equals(Object object) {
        if (object == null || ! (object instanceof DataDouble)) return false;
        if ( Double.isNaN(value) && Double.isNaN( ((DataObject) object).doubleValue() ) ) {
           if (valueUpdate == ((DataDouble) object).valueUpdate && 
               valueMutable == ((DataDouble) object).valueMutable && 
               valueNull == ((DataDouble) object).valueNull ) return true;
           else return false;
        }
        else if (value == ((DataDouble) object).value && 
           valueUpdate == ((DataDouble) object).valueUpdate && 
           valueMutable == ((DataDouble) object).valueMutable && 
           valueNull == ((DataDouble) object).valueNull) return true;
        else return false;
    }

/** Returns true if the object value is equivalent to that of the input object.
* Also returns true if the compared values are NaN.
* The state flags are not compared so input objects of Number type are allowed.
* Returns false if the input object is null or the input argument is not of class type Number or DataDouble.
*/
    public boolean equalsValue(Object object) {
        if (object == null ) return false;
        if (object instanceof DataObject) {
            if ( Double.isNaN(value) && Double.isNaN( ((DataObject) object).doubleValue() ) ) return true;
            else return  ( value == ((DataObject) object).doubleValue() );
        }
        else if (object instanceof Number) {
            if ( Double.isNaN(value) && Double.isNaN( ((Number) object).doubleValue() ) ) return true;
            else return ( value == ((Number) object).doubleValue() );
        }
        else return false; 
    }

/** Returns 0 if this.value == object.value; returns 1 if this.value > object.value;
 * returns -1 if this.value < object.value.
* Also returns 0 if the compared values are NaN.
* Throws ClassCastException if input object is not of type Double or DataDouble.
*/
    public int compareTo(Object object) throws ClassCastException {
        if (object instanceof Double) { 
            if ( Double.isNaN(value) && Double.isNaN( ((Double) object).doubleValue() ) ) return 0;
            else if (this.value == ((Double) object).doubleValue()) return 0;
            else if (this.value > ((Double) object).doubleValue()) return 1;
            else return -1;
        }
        else if (object instanceof DataDouble) { 
            return compareTo((DataDouble) object);
        }
        else throw new ClassCastException("compareTo(object) argument must be a Double or DataDouble class type: "
                        + object.getClass().getName());
    }

/** Returns 0 if this.value == object.value; returns 1 if this.value > object.value;
 * returns -1 if this.value < object.value.
* Also returns 0 if both objects have isNull() == true or their values are NaN.
*/
    public int compareTo(DataDouble object) {
        if (isNull() && object.isNull()) return 0;
        else if ( Double.isNaN(this.value) && Double.isNaN( object.doubleValue() ) ) return 0;
        else if (this.value == object.value) return 0;
        else if (this.value > object.value) return 1;
        else return -1;
    }

/** Returns the value rounded to an int. */
    public int intValue() {
        return (int) Math.round(value); // used to cast, truncate, now round -aww 20100507
    }

/** Returns the value rounded to a long. */
    public long longValue() {
        return Math.round(value); // used to cast, truncate, now round -aww 20100507
    }

/** Returns the value cast as an float, BEWARE !!!
 * returns Float.POSITIVE_INFINITY if double value is larger than Float.MAX_VALUE */
    public float floatValue() {
        return (float) value;
    }

/** Returns the value cast as a double. */
    public double doubleValue() {
        return (double) value;
    }

/** Returns the value cast as a BigDecimal. */
    public BigDecimal bigDecimalValue() {
        return BigDecimal.valueOf(value);
    }

/** Returns a Date object where the time is this true epoch seconds value converted to nominal millisseconds. */
    public java.util.Date dateValue() {
        return new java.util.Date(Math.round(LeapSeconds.trueToNominal(value)*1000.)); // for UTC seconds to date -aww 2008/02/12
    }

/** Returns a Timestamp object where time is this true epoch seconds value converted to nominal milliseconds. */
    public java.sql.Timestamp timestampValue() {
        return new java.sql.Timestamp(Math.round(LeapSeconds.trueToNominal(value)*1000.)); // for UTC seconds to timestamp -aww 2008/02/12
    }

/** Sets the object value to the input cast as a double.
* Does a no-op if isMutable() == false.
* Sets the state flags isNull() == false and isUpdate() == true .
*/
    public void setValue(int value) {
        setValue((double) value);
    }

/** Sets the object value to the input cast as a double.
* Does a no-op if isMutable() == false.
* Sets the state flags isNull() == false and isUpdate() == true .
*/
    public void setValue(long value) {
        setValue((double) value);
    }

/** Sets the object value to the input cast as a double.
* Does a no-op if isMutable() == false.
* Sets the state flags isUpdate() == true and isNull() == false,
* unless Float.isNaN(value)
*/
    public void setValue(float value) {
        if (Float.isNaN(value)) setNull(true);
        else setValue((double) value);
    }

/** Sets the object value to the input cast as a double.
* Warning: will return Double.NEGATIVE_INFINITY or 
* Double.POSITIVE_INFINITY if input has too many significant 
* digits for a double.
* Does a no-op if isMutable() == false.
* Sets the state flags isUpdate() == true and isNull() == false,
* unless Float.isNaN(value)
*/
    public void setValue(BigDecimal value) {
        setValue(value.doubleValue());
    }

/** Sets the object value to the input cast as a double.
* Does a no-op if isMutable() == false.
* Sets the state flags isUpdate() == true and isNull() == false,
* unless Double.isNaN(value)
*/
    public void setValue(double value) {
        if(! isMutable()) return;
        if (Double.isNaN(value)) setNull(true);
        else {
          this.value = value;
          this.valueNull = false;
          this.valueUpdate = true;
        }
    }

/** Sets the object value to the input cast as a double. 
* Does a no-op if isMutable() == false.
* Sets the state flags isUpdate() == true and isNull() == false,
* unless input is null or if input is a DataObject, object.isNull().
* Throws a ClassCastException if input object is not of type DataObject, Number, or String.
* Throws a NumberFormatException if String object cannot be parsed as a number.
*/
    public void setValue(Object object) throws ClassCastException, NumberFormatException  {
        if(! isMutable()) return;
//        if (object == null) throw new NullPointerException("setValue(Object) argument null");
        if (object == null) {
            setNull(true);
        }
        else if (Number.class.isInstance(object)) {
            setValue(((Number) object).doubleValue());
        }
        else if (DataObject.class.isInstance(object)) {
            DataObject obj = (DataObject) object;
            if (obj.isNull()) setNull(true);
            else setValue(obj.doubleValue());
            //setValue(((DataObject) object).doubleValue());
        }
        else if (String.class.isInstance(object)) {
            setValue(Double.parseDouble((String) object));
        }
        else throw new ClassCastException("setValue(Object) invalid object argument class type: " + object.getClass().getName());
    }

/** Sets the object value to NullValueDb.NULL_DOUBLE.
* Sets the state flags isNull() == tf and isUpdate() == true.
* Does a no-op, if isMutable == false. 
*/
    public DataObject setNull(boolean tf) {
        if(! isMutable()) return this;
        this.value = NullValueDb.NULL_DOUBLE;
        this.valueNull = tf;
        this.valueUpdate = true;
        return this;
    }

/**
* Returns true if a value can be parsed from input StringTokenizer.
* Does not set value and returns false if tokenizer.hasMoreTokens() == false
* or a double cannot be parsed from token.
*/
    public boolean parseValue(StringTokenizer tokenizer) {
        if (! tokenizer.hasMoreTokens()) return false;
        boolean retVal = false;
        try {
            setValue( Double.parseDouble(tokenizer.nextToken()) );
            retVal = true;
        }
        catch (NumberFormatException ex) {
//            System.err.println("DataDouble parseValue() " + ex.getMessage());
        }
        return retVal;
    }

    public boolean isValidNumber() {
        return ! (isNull() || Double.isNaN(value)); // what about Infinities?
    }
    public boolean isValid() {
        return isValidNumber();
    }
    public  boolean isNaN() {
        return Double.isNaN(value);
    }
}
