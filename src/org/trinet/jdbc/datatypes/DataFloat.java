package org.trinet.jdbc.datatypes;
import java.util.*;
import org.trinet.jdbc.*;
import org.trinet.util.LeapSeconds;

/** Extends the base abstract DataObject class to implements a stateful float value.
@ see DataDouble
*/
public class DataFloat extends DataObject implements DataNumeric, DataTime, Comparable, Cloneable {
    float value;

    public DataFloat () {
        this.value = NullValueDb.NULL_FLOAT;
    }
 
    public DataFloat (int value) {
        this((float)value);
    }

    public DataFloat (long value) {
        this((float)value);
    }

/** Constructor sets the object value to input argument.
 * Sets isUpdate() == true and isNull() == false, unless input satisfies Float.isNaN(value). 
 */
    public DataFloat (float value) {
        setValue(value);
    }

/** Constructor sets the object value to input argument.
 * Sets isUpdate() == true and isNull() == false, unless input satisfies Float.isNaN(value). 
 */
    public DataFloat (double value) {
        setValue(value);
    }

    public DataFloat (String value) throws NumberFormatException {
        this();
        setValue(value);
    }

/** Constructor copies the object value and state flag settings of the input argument object. 
* Null state is propagated.
*/
    public DataFloat (DataFloat object) {
        if (object == null) setNull(true);
        else {        
           this.value = object.value;
           this.valueUpdate = object.valueUpdate;
           this.valueNull = object.valueNull;
           this.valueMutable = object.valueMutable;
        }
    }

    public String toString() {
        return String.valueOf(value); 
    }
    public String toStringSQL() {
        if (valueNull || Float.isNaN(value)) return "NULL";
        return StringSQL.valueOf(value);
    }

/** Returns a String of "label: value" pairs for the object value and its state flags. 
* If isNull() == true the string "NULL" is printed for the value.
* "Value: " + value.toString() + " Null: " + isNull() + " Update: " + isUpdate() + " Mutable: " + isMutable()
*/
    public String classToString() {
        StringBuffer sb = new StringBuffer(128);
        sb.append("                                           ");
        sb.insert(0, "Value:");
        if (isNull()) sb.insert(7, "NULL");
        else sb.insert(7, value);
        sb.insert(32, "Null:");
        sb.insert(37, valueNull);
        sb.insert(43, "Update:");
        sb.insert(50, valueUpdate);
        sb.insert(56, "Mutable:");
        sb.insert(64, valueMutable);
        return sb.toString();
//        return  "Value: " + value + " Null: " + valueNull + " Update: " + valueUpdate + " Mutable: " + valueMutable;
    }

    public int hashCode() {
        Float fval = Float.valueOf(value);
        return fval.hashCode(); 
    }

/** Returns true if the object value and its state flags are equivalent to those of the input object.
* Also returns true if the compared objects have values of NaN and their state flags are equivalent.
* Returns false if the input object is null or is not of type DataFloat.
*/
    public boolean equals(Object object) {
        if (object == null || ! (object instanceof DataFloat)) return false;
        if ( Float.isNaN(value) && Float.isNaN( ((DataObject) object).floatValue() ) ) {
           if (valueUpdate == ((DataFloat) object).valueUpdate && 
               valueMutable == ((DataFloat) object).valueMutable && 
               valueNull == ((DataFloat) object).valueNull ) return true;
           else return false;
        }
        else if (value == ((DataFloat) object).value && 
           valueUpdate == ((DataFloat) object).valueUpdate && 
           valueMutable == ((DataFloat) object).valueMutable && 
           valueNull == ((DataFloat) object).valueNull) return true;
        else return false;
    }

/** Returns true if the object value is equivalent to that of the input object.
* Also returns true if the compared objects have values of NaN.
* Returns false if the input object is null or the input argument is not of class type Number or DataFloat.
* The state flags are not compared so input objects of type Number are allowed.
* Returns false if the input object is null or is not of type Number or DataFloat.
*/
    public boolean equalsValue(Object object) {
        if (object == null ) return false;
        if (object instanceof DataObject) {
            if ( Float.isNaN(value) && Float.isNaN( ((DataObject) object).floatValue() ) ) return true;
            else return ( value == ((DataObject) object).floatValue() );
        }
        else if (object instanceof Number) {
            if ( Float.isNaN(value) && Float.isNaN( ((Number) object).floatValue() ) ) return true;
            else return ( value == ((Number) object).floatValue() );
        }
        else return false; 
    }

/** Returns 0 if this.value == object.value; returns 1 if this.value > object.value;
 * returns -1 if this.value < object.value.
* Also returns 0 if the compared values are NaN.
* Throws ClassCastException if input object is not of type DataFloat or Float.
*/
    public int compareTo(Object object) throws ClassCastException {
        if (object instanceof Float) { 
            if ( Float.isNaN(value) && Float.isNaN( ((Float) object).floatValue() ) ) return 0;
            else if (this.value == ((Float) object).floatValue()) return 0;
            else if (this.value > ((Float) object).floatValue()) return 1;
            else return -1;
        }
        else if (object instanceof DataFloat) { 
            return compareTo((DataFloat) object);
        }
        else throw new ClassCastException("compareTo(object) argument must be a Float or DataFloat class type: "
                        + object.getClass().getName());
    }

/** Returns 0 if this.value == object.value; returns 1 if this.value > object.value;
 * returns -1 if this.value < object.value.
* Also returns 0 if the compared objects have isNull() == true or their values are NaN.
*/
    public int compareTo(DataFloat object) {
        if (isNull() && object.isNull()) return 0;
        else if ( Float.isNaN(this.value) && Float.isNaN( object.floatValue() ) ) return 0;
        else if (this.value == object.value) return 0;
        else if (this.value > object.value) return 1;
        else return -1;
    }

/** Returns the value rounded to a int. */
    public int intValue() {
        return Math.round(value); // used to cast, truncate, now round -aww 20100507
    }

/** Returns the value rounded to a long. */
    public long longValue() {
        return (long) Math.round(value); // used to cast, truncate, now round -aww 20100507
    }

    public float floatValue() {
        return (float) value;
    }

    public double doubleValue() {
        return (double) value;
    }

/** Returns a Date object where the time is this true epoch seconds value converted to nominal millisseconds. */
    public java.util.Date dateValue() {
        return new java.util.Date(Math.round(LeapSeconds.trueToNominal((double)value)*1000.)); // for UTC seconds to date -aww 2008/02/12
    }

/** Returns a Timestamp object where time is this true epoch seconds value converted to nominal milliseconds. */
    public java.sql.Timestamp timestampValue() {
        return new java.sql.Timestamp(Math.round(LeapSeconds.trueToNominal((double)value)*1000.)); // for UTC seconds to timestamp -aww 2008/02/12
    }

    public void setValue(int value) {
        setValue((float) value);
    }

    public void setValue(long value) {
        setValue((float) value);
    }

    public void setValue(float value) {
        if(! isMutable()) return;
        if (Float.isNaN(value)) setNull(true);
        else {
          this.value =  value;
          this.valueNull = false;
          this.valueUpdate = true;
        }
    }

    public void setValue(double value) {
        if (Double.isNaN(value)) setNull(true);
        else setValue((float) value);
    }

    public void setValue(Object object) throws ClassCastException, NumberFormatException  {
        if(! isMutable()) return;
//        if (object == null) throw new NullPointerException("setValue(Object) argument null");
        if (object == null) {
            setNull(true);
        }
        else if (Number.class.isInstance(object)) {
            setValue(((Number) object).floatValue());
        }
        else if (DataObject.class.isInstance(object)) {
            DataObject obj = (DataObject) object;
            if (obj.isNull()) setNull(true);
            else setValue(obj.floatValue());
            //setValue(((DataObject) object).floatValue());
        }
        else if (String.class.isInstance(object)) {
            setValue(Float.parseFloat((String) object));
        }
        else throw new ClassCastException("setValue(Object) invalid object argument class type: " +
                        object.getClass().getName());
    }

    public DataObject setNull(boolean tf) {
        if(! isMutable()) return this;
        this.value = NullValueDb.NULL_FLOAT;
        this.valueNull = tf;
        this.valueUpdate = true;
        return this;
    }

/**
* Returns true if a value can be parsed from input StringTokenizer.
* Does not set value and returns false if tokenizer.hasMoreTokens() == false
* or a float cannot be parsed from token.
*/
    public boolean parseValue(StringTokenizer tokenizer) {
        if (! tokenizer.hasMoreTokens()) return false;
        boolean retVal = false;
        try {
            setValue( Float.parseFloat(tokenizer.nextToken()) );
            retVal = true;
        }
        catch (NumberFormatException ex) {
            System.err.println("DataFloat parseValue()" + ex.getMessage());
        }
        return retVal;
    }

    public boolean isValidNumber() {
        return ! (isNull() || Float.isNaN(value)); // what about infinities?
    }
    public boolean isValid() {
        return isValidNumber();
    }
    public boolean isNaN() {
        return Float.isNaN(value);
    }
}
