package org.trinet.jdbc.datatypes;
import java.util.*;
import org.trinet.jdbc.*;

/** Extends the base abstract DataObject class to implements a stateful int value.
* @see DataLong
*/
public class DataInteger extends DataObject implements DataNumeric, Comparable {
    int value;

    public DataInteger () {
        this.value = NullValueDb.NULL_INT;
    }
 
    public DataInteger (int value) {
        this.value = value;
        this.valueNull = false;
        this.valueUpdate = true;
    }
    public DataInteger (long value) {
        this((int) value);
    }
/** Constructor sets the object value to input argument.
 * Sets isUpdate() == true and isNull() == false, unless input satisfies Float.isNaN(value). 
 */
    public DataInteger (float value) {
        setValue(value);
    }
/** Constructor sets the object value to input argument.
 * Sets isUpdate() == true and isNull() == false, unless input satisfies Double.isNaN(value). 
 */
    public DataInteger (double value) {
        setValue(value);
    }
    public DataInteger (String value) throws NumberFormatException {
         this();
         setValue(value);
    }

/** Constructor copies the object value and state flag settings of the input argument object. 
* Null state is propagated.
*/
    public DataInteger (DataInteger object) {
        if (object == null) setNull(true);
        else {
            this.value = object.value;
            this.valueUpdate = object.valueUpdate;
            this.valueNull = object.valueNull;
            this.valueMutable = object.valueMutable;
        }
    }

    public String toString() {
        return String.valueOf(value); 
    }
    public String toStringSQL() {
        if (valueNull) return "NULL";
        return StringSQL.valueOf(value);
    }

/** Returns a String of "label: value" pairs for the object value and its state flags. 
* If isNull() == true the string "NULL" is printed for the value.
* "Value: " + value.toString() + " Null: " + isNull() + " Update: " + isUpdate() + " Mutable: " + isMutable()
*/
    public String classToString() {
        StringBuffer sb = new StringBuffer(128);
        sb.append("                                           ");
        sb.insert(0, "Value:");
        if (isNull()) sb.insert(7,"NULL");
        else sb.insert(7, value);
        sb.insert(32, "Null:");
        sb.insert(37, valueNull);
        sb.insert(43, "Update:");
        sb.insert(50, valueUpdate);
        sb.insert(56, "Mutable:");
        sb.insert(64, valueMutable);
        return sb.toString();
//        return  "Value: " + value + " Null: " + valueNull + " Update: " + valueUpdate + " Mutable: " + valueMutable;
    }

    public int hashCode() {
        return value; 
    }


    public boolean equalsValue(Object object) {
        if (object == null ) return false;
        if (object instanceof DataObject) {
            return ( value == ((DataObject) object).intValue() );
        }
        else if (object instanceof Number) {
            return ( value == ((Number) object).intValue() );
        }
        else return false; 
    }

    public boolean equals(Object object) {
        if (object == null || ! (object instanceof DataInteger)) return false;
        if (value == ((DataInteger) object).value && 
           valueUpdate == ((DataInteger) object).valueUpdate && 
           valueMutable == ((DataInteger) object).valueMutable && 
           valueNull == ((DataInteger) object).valueNull) return true;
        else return false;
    }

    public int compareTo(Object object) throws ClassCastException {
        if (object instanceof Integer) { 
            if (this.value == ((Integer) object).intValue()) return 0;
            else if (this.value > ((Integer) object).intValue()) return 1;
            else return -1;
        }
        else if (object instanceof DataInteger) { 
            return compareTo((DataInteger) object);
        }
        else throw new ClassCastException("compareTo(object) argument must be a Integer or DataInteger class type: "
                        + object.getClass().getName());
    }

    public int compareTo(DataInteger object) {
        if (this.value == object.value) return 0;
        else if (this.value > object.value) return 1;
        else return -1;
    }

    public int intValue() {
        return (int) value;
    }

    public long longValue() {
        return (long) value;
    }

    public float floatValue() {
        return (float) value;
    }

    public double doubleValue() {
        return (double) value;
    }

    public void setValue(int value) {
        if(! isMutable()) return;
        this.value = value;
        this.valueNull = false;
        this.valueUpdate = true;
    }

    public void setValue(long value) {
        setValue((int) value);
    }

/** Sets the object value to the input "rounded" to a int.
* Does a no-op if isMutable() == false.
* Sets the state flags isUpdate() == true and isNull() == false,
* unless input satisifies Float.isNaN(value).
*/
    public void setValue(float value) {
        if (Float.isNaN(value)) setNull(true);
        else setValue(Math.round(value)); // used to truncate, now round - aww 20100507
    }

/** Sets the object value to the input "rounded" to a int.
* Does a no-op if isMutable() == false.
* Sets the state flags isUpdate() == true and isNull() == false,
* unless input satisifies Float.isNaN(value).
*/
    public void setValue(double value) {
        if (Double.isNaN(value)) setNull(true);
        setValue((int) Math.round(value)); // used to truncate, now round - aww 20100507
    }

    public void setValue(Object object) throws ClassCastException, NumberFormatException  {
        if(! isMutable()) return;
//        if (object == null) throw new NullPointerException("setValue(Object) argument null");
        if (object == null) {
            setNull(true);
        }
        else if (Number.class.isInstance(object)) {
            setValue(((Number) object).intValue());
        }
        else if (DataObject.class.isInstance(object)) {
            DataObject obj = (DataObject) object;
            if (obj.isNull()) setNull(true);
            else setValue(obj.intValue());
            //setValue(((DataObject) object).intValue());
        }
        else if (String.class.isInstance(object)) {
            setValue(Integer.parseInt((String) object));
        }
        else throw new ClassCastException("setValue(Object) invalid object argument class type: " + object.getClass().getName());
    }

    public DataObject setNull(boolean tf) {
        if(! isMutable()) return this;
        this.value = NullValueDb.NULL_INT;
        this.valueNull = tf;
        this.valueUpdate = true;
        return this;
    }

/**
* Returns true if a value can be parsed from input StringTokenizer.
* Does not set value and returns false if tokenizer.hasMoreTokens() == false
* or a float cannot be parsed from token.
*/
    public boolean parseValue(StringTokenizer tokenizer) {
        if (! tokenizer.hasMoreTokens()) return false;
        boolean retVal = false;
        try {
            setValue( Integer.parseInt(tokenizer.nextToken()) );
            retVal = true;
        }
        catch (NumberFormatException ex) {
            System.err.println("DataInteger parseValue()" + ex.getMessage());
        }
        return retVal;
    }

    public boolean isValidNumber() {
        return ! isNull();
    }
    public boolean isValid() {
        return isValidNumber();
    }
}
