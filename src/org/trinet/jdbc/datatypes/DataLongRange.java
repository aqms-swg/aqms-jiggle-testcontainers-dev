package org.trinet.jdbc.datatypes;
import java.util.*;

/**
 * General purpose range (pair) of long values
*/
public class DataLongRange extends DataObjectRange {

    public DataLongRange() {
	this(new DataLong(), new DataLong());
    }

    public DataLongRange(DataLong min, DataLong max) {
	super(min, max);
    }

    public DataLongRange(long min, long max) {
	this(new DataLong(min), new DataLong(max));
    }

/**
 * Returns the min value of this range.
*/
    public long getMinValue() {
	return getMin().longValue();
    }

/**
 * Returns the max value of this range.
*/
    public long getMaxValue() {
	return getMax().longValue();
    }

/**
 * Sets the min value of this range.
*/
    public void setMin(long min) {
	setMin(new DataLong(min));
    }

/**
 * Sets the max value of this range.
*/
    public void setMax(long max) {
	setMax(new DataLong(max));
    }

/**
 * Sets the max, min values of this range.
*/
    public void setLimits(long min, long max) {
	setLimits(new DataLong(min), new DataLong(max));
    }

    public void include(long val) {
	include(new DataLong(val));
    }

/**
 * Adjust current range to include range in argument
*/
    public void include(DataLongRange range) {
        include((DataObjectRange) range);
    }

/**
* Return true if number is within specified bounds inclusive.
*/
    public boolean contains(long val) {
        return contains(new DataLong(val));
    }

/**
* Return true if range is within this range inclusive.
*/
    public boolean contains(DataLongRange range) {
	return contains((DataObjectRange) range);
    }

/**
* Returns the difference between the upper and lower range bounds.
*/
    public long size() {
	return longExtent();
    }

/**
* Returns min + "," + max.
*/
    public String toString() {
        return min + "," + max;
    }


/**
* Returns true if a range can be parsed from input StringTokenizer.
* Does not set range and returns false if tokenizer.countTokens < 2 
* or a range cannot be parsed from tokenizer.
*/
    public boolean parseValue(StringTokenizer tokenizer) {
        if (tokenizer.countTokens() < 2) return false;
        boolean retVal = false;
        try {
            setLimits( Long.parseLong(tokenizer.nextToken()), Long.parseLong(tokenizer.nextToken()) );
            retVal = true;
        }
        catch (NumberFormatException ex) {
            System.err.println("DataLongRange parseValue()" + ex.getMessage());
        }
        return retVal;
    }

/*
    public static void main(String [] args) {

         System.out.println("++++++++ BEGIN TEST ++++++++++");


         DataLongRange dr = new DataLongRange(-10l, 10l);
         DataLongRange dr2 = new DataLongRange(-20l, 20l);
         DataFloatRange dr3 = new DataFloatRange(-20.f, 20.f);

         dr.dump(dr2, dr3);
         dr3.setLimits(1.f,21.f);
         dr.dump(dr2, dr3);

         dr.dump1(-11l);
         dr.dump1(-1l);
         dr.dump1(0l);
         dr.dump1(1l);
         dr.dump1(11l);

         dr.dump2(dr2);

         System.out.println("Test dr.setLimits(-14, 14)");
         dr.setLimits(-14l,14l);
         dr.dump2(dr2);

         System.out.println("Test dr.include(-16, 16)");
         dr.include(-16l);
         dr.include(16l);
         dr.dump2(dr2);

         System.out.println("Test dr.include(dr2)");
         dr.include(dr2);
         dr.dump2(dr2);

         System.out.println("Test dr.setMax(0); dr2.setMin(0)");
         dr.setMax(0l);
         dr2.setMin(0l);
         dr.dump2(dr2);

         System.out.println("Test dr.setMax(0); dr2.setMin(1)");
         dr.setMax(0l);
         dr2.setMin(1l);
         dr.dump2(dr2);

    }

    public void dump(DataObjectRange dr2, DataObjectRange dr3) {
         System.out.println("Test equalsRange: ");
         System.out.println("     dr2.toString(): " + dr2.toString());
         System.out.println("     dr3.toString(): " + dr3.toString());
         System.out.println("     dr2.equalsRange(dr3) : " +  dr2.equalsRange(dr3));
         System.out.println("------------------\n");
    }

     public void dump1(long number) {
         DataLong val = new DataLong(number);
         System.out.println("Dump Range bounds, size: " + getMinValue() + ", " + getMaxValue() + " size(): " + size());
         System.out.println(" Test input val: " + val);
         System.out.println(" after(val)    : " + after(val));
         System.out.println(" before(val)   : " + before(val));
         System.out.println(" excludes(val) : " + excludes(val));
         System.out.println(" contains(val) : " + contains(val));
         System.out.println("------------------\n");
    }

    public void dump2(DataLongRange dr2) {
         System.out.println("Dump Range, number : " + getMinValue() + ", " + getMaxValue() + " size(): " + size());
         System.out.println(" Test Range2 Min,max: " + dr2.getMinValue() + ", " + dr2.getMaxValue() );

         System.out.println(" within(dr2)       : " + within(dr2));
         System.out.println(" dr2.within(this)  : " + dr2.within(this)); 

         System.out.println(" overlaps(dr2)     : " + overlaps(dr2));
         System.out.println(" dr2.overlaps(this): " + dr2.overlaps(this));

         System.out.println(" dr2.after(this)   : " + dr2.after(this));
         System.out.println(" after(dr2)        : " + after(dr2));

         System.out.println(" dr2.before(this)  : " + dr2.before(this));
         System.out.println(" before(dr2)       : " + before(dr2));

         System.out.println(" dr2.excludes(this): " + dr2.excludes(this));
         System.out.println(" excludes(dr2)     : " + excludes(dr2));

         System.out.println(" dr2.contains(this): " + dr2.contains(this));
         System.out.println(" contains(dr2)     : " + contains(dr2));
         System.out.println("------------------\n");

    }
*/
}
