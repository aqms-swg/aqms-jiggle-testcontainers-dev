//TODO - possibly make class extend Number, override byteValue(), shortValue()
//TODO - Change setValue methods in subclasses to toggle isUpdate() true
//       only if the value changes, could fire off change event too.
//
package org.trinet.jdbc.datatypes;

import java.io.Serializable;
import org.trinet.jdbc.NullValueDb;

/** Abstract base class for stateful data objects. The database data are stored in the extensions of the DataTableRow class 
* in objects that extend this base class
* Defines boolean state fields for the data object and implements the DataState interface methods to act upon these state fields.
* The default state flag settings are Update:false, Null:true, Mutable:true.
* @see DataDate
* @see DataDouble
* @see DataFloat
* @see DataInteger
* @see DataLong
* @see DataString
* @see DataTimestamp
*/

// Made serializable - DDG 7/21/2000 This will make all other DataXxxx types will be  Serializable, too.

public abstract class DataObject implements DataObjectIF, ValueParser, Cloneable, Serializable { 
/** Object state data member flags data modified or updatable, indicates field value set or not. 
*/
    protected boolean valueUpdate = false;

/** Object state data member flags nullness, indicates field null or not. 
*/
    protected boolean valueNull = true;

/** Object state data member flags mutability, indicates field mutable or not. 
*/
    protected boolean valueMutable = true;

/** Overrides Object.hashCode() */
    public abstract int hashCode() ;
/** Overrides Object.equals() */
    public abstract boolean equals(Object object) ;
/** Overrides Object.toString() returns the string equivalent of the value. */
    public abstract String toString() ;
/** Method compares objects of same type required for Compareable interface. */
    public abstract int compareTo(Object object);
/** Method compares DataObject values and return true if they are equivalent. */
    public abstract boolean equalsValue(Object object) ;
/** Method returns the SQL string equivalent to the DataObject value. */
    public abstract String toStringSQL() ;
/** Method returns a String concatenating a labeled string value and labeled state flag values. */
    public abstract String classToString() ;

/** Method sets the value of this DataObject to the value of the object input argument.
* Does a no-op if isMutable() == false.
* Sets the update status flag, isUpdate() == true.
* Sets the null status flag, isNull() == false.
*/
    public abstract void setValue(Object object) throws ClassCastException, NumberFormatException ;



/** Provide clone support for extensions of this class; invokes Object.clone().
*/
    public Object clone() {
        Object obj = null;
        try {
            obj = super.clone();
        }
        catch (CloneNotSupportedException ex) {
            System.out.println("Cloneable not implemented for class: " + this.getClass().getName());
            ex.printStackTrace();
        }
        return obj;
    }

/** Sets the update flag to the specified boolean value; determines data legitimacy in implemented class extensions.
* An argument value of true == "set", false == "not set". Returns the handle (this) of the invoking class instance.
*/
    public DataObject setUpdate(boolean value) {
        this.valueUpdate = value;
        return this;
    }
        
/** Returns the boolean value of the data update flag for the invoking instance.
*/
    public boolean isUpdate() {
        return valueUpdate;
    }
        
/** Sets the null flag to the specified boolean value;
 * determines whether the data value is to be considered undefined or NULL.
* An argument value of  true == "NULL", false == "not NULL".
* Toggles isUpdate() == true.
* Does a no-op, if isMutable == false. 
* Returns the handle (this) of the invoking class instance.
*/
    public DataObject setNull(boolean tf) {
        if (! isMutable()) return this;
        this.valueNull = tf;
        this.valueUpdate = true;
        return this;
    }

/** Returns the boolean value of the data null flag for the invoking instance.
*/
    public boolean isNull() {
        return valueNull;
    }
/** Returns 'true' if !isNull() and a case insensitive string value is
 *  not equivalent to "NULL", "NaN", all blanks, or empty String.
 *  Subclasses could override and implement tests with more restrictive
 *  constraints on acceptable values.
 *  */
    public boolean isValidNumber() {
      return ! ( isNull() || NullValueDb.isBlank(toString()) );
    }

/** Convenience wrapper returns ! isNull(). 
 *  Subclasses could override and implement tests with more restrictive
 *  constraints on acceptable values.
 * */
    public boolean isValid() {
      return ! isNull();
    }

/** Sets the mutability flag to the specified boolean value; determines whether the data value can be modified.
* An argument value of  true == "mutable", false == "not mutable". Returns the handle (this) of the invoking class instance.
*/
    public DataObject setMutable(boolean value) {
        this.valueMutable = value;
        return this;
    }

/** Returns the boolean value of the data mutability flag for the invoking instance.
*/
    public boolean isMutable() {
        return valueMutable;
    }

//    public abstract boolean parseValue(StringTokenizer tokenizer) ;

}
