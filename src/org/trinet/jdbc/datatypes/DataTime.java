package org.trinet.jdbc.datatypes;
/** Interface methods implemented by extensions of the DataObject class to format values into time objects.
*/
public interface DataTime {
    java.util.Date dateValue();
    java.sql.Timestamp timestampValue();
}
