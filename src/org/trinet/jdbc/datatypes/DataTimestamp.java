package org.trinet.jdbc.datatypes;
import java.util.*;
import org.trinet.jdbc.*;

/** Extends the base abstract DataObject class to implements a stateful Timestamp value.
* Value is stored as a java.sql.Timestamp object.
*/
public class DataTimestamp extends DataObject implements DataTime, DateStringFormatter, Comparable, Cloneable {
/** Data member holding value. */
    java.sql.Timestamp value; 

/** Constructor sets the object value to NullValueDb.NULL_TIMESTAMP and the object state flags to their default settings
* (Null: true; Update: false; Mutable: true)).
*/
    public DataTimestamp() {
        this.value = (java.sql.Timestamp) NullValueDb.NULL_TIMESTAMP.clone();
    }
 
/** Constructor sets the timestamp to (long) value millisecs and the state flags, isNull() == false and isUpdate() == true.
*/
    public DataTimestamp(int value) {
        this((long) value);
    }

/** Constructor sets the timestamp to (long) value millisecs and the state flags, isNull() == false and isUpdate() == true.
*/
    public DataTimestamp(long value) {
        this.value = new java.sql.Timestamp(value);
        this.valueUpdate = true;
        this.valueNull = false;
    }

/** Constructor sets the timestamp to (long) value millisecs
 *  Sets the state flags isUpdate() == true and isNull() == false, unless 
 *  input satisfies Float.isNaN(value), then isNull() == true.
*/
    public DataTimestamp(float value) {
        setValue(value);
    }

/** Constructor sets the timestamp to (long) value millisecs and the state flags, isNull() == false and isUpdate() == true.
 *  Sets the state flags isUpdate() == true and isNull() == false, unless 
 *  input satisfies Double.isNaN(value), then isNull() == true.
*/
    public DataTimestamp(double value) {
        setValue(value);
    }

/** Constructor sets the timestamp to value of a string in SQL date escape format.
 *  or if a numeric string, to the numeric value as (long) millisecs.
 *  Sets the state flags isUpdate() == true and isNull() == false, unless 
 *  input is null, then isNull() == true.
*/
    public DataTimestamp(String value) {
        this();
        if (value == null) setNull(true);
        else setValue(value);
    }

/** Constructor sets the timestamp to a clone of the Timestamp object argument.
 *  Sets the state flags isUpdate() == true and isNull() == false, unless 
 *  input is null, then isNull() == true.
*/
    public DataTimestamp(java.sql.Timestamp object) {
        this();
        if (object == null) setNull(true);
        else {
            this.value = (java.sql.Timestamp) object.clone();
            this.valueUpdate = true;
            this.valueNull = false;
        }
    }

/** Constructor sets the timestamp to a clone of input object Timestamp member. 
* The state flag values are set equivalent to those of the input object.
* Null state is propagated.
*/
    public DataTimestamp(DataTimestamp object) {
        this();
        if (object == null) setNull(true);
        else {
            this.value = (java.sql.Timestamp) object.value.clone();
            this.valueUpdate = object.valueUpdate;
            this.valueNull = object.valueNull;
            this.valueMutable = object.valueMutable;
        }
    }

/** Returns the default string for the value.
*/
    public String toString() {
        return value.toString(); 
    }

/** Returns the representation of the value formatted using the input pattern string. 
*/
    public String toDateString(String pattern) {
        return org.trinet.util.EpochTime.epochToString(getMilliSecsTime()/1000., pattern); 
    }

/** Returns the default string representation of the value.
/** Returns an SQL syntax string value of the timestamp date field; if isNull() == true, returns the string "NULL".
*/
    public String toStringSQL() {
        if (valueNull) return "NULL";
        return StringSQL.valueOf(value);
    }

/** Returns a String of "label: value" pairs for the object value and its state flags. 
* If isNull() == true the string "NULL" is printed for the value.
* "Value: " + value.toString() + " Null: " + isNull() + " Update: " + isUpdate() + " Mutable: " + isMutable()
*/
    public String classToString() {
        StringBuffer sb = new StringBuffer(128);
        sb.append("                                           ");
        sb.insert(0, "Value:");
        if (isNull()) sb.insert(7, "NULL");
        else sb.insert(7, value);
        sb.insert(32, "Null:");
        sb.insert(37, valueNull);
        sb.insert(43, "Update:");
        sb.insert(50, valueUpdate);
        sb.insert(56, "Mutable:");
        sb.insert(64, valueMutable);
        return sb.toString();
//        return  "Value: " + value + " Null: " + valueNull + " Update: " + valueUpdate + " Mutable: " + valueMutable;
    }

/** Returns the value of java.sql.Timestamp.hashcode() for the timestamp value data member.
*/
    public int hashCode() {
        return value.hashCode(); 
    }

/** Returns true input object is an instance of DataTimestamp and it value and flags are equivalent to those of this instance..
* Returns false if input object is null or not an instance of DataTimestamp.
*/
    public boolean equals(Object object) {
        if (object == null || ! (object instanceof DataTimestamp)) return false;
        if (this.value.equals(((DataTimestamp) object).value) && 
           this.valueUpdate == ((DataTimestamp) object).valueUpdate && 
           this.valueMutable == ((DataTimestamp) object).valueMutable && 
           this.valueNull == ((DataTimestamp) object).valueNull) return true;
        else return false;
    }

/** Returns true if argument object is instanceof this DataTimestamp or java.sql.Timestamp and its timestamp values are equal.
*  No state flag values are checked.
*/
    public boolean equalsValue(Object object) {
        if (object == null) return false;
        if (object instanceof DataTimestamp) {
            return ( this.value.equals(((DataTimestamp) object).value) );
        }
        else if (object instanceof java.sql.Timestamp) {
            return ( this.value.equals((java.sql.Timestamp) object) );
        }
        else return false; 
    }

/** Returns the value of java.sql.Timestamp.compareTo() for the data value member of this object.
* A return of 0 == this value equals, <0 == value less than, >0 == value greater than, the value of the argument object.
* Throws an exception if argument object is not an instanceof DataTimestamp.
*/
    public int compareTo(Object object) throws ClassCastException {
        if (object instanceof java.sql.Timestamp) {
            return this.value.compareTo((java.sql.Timestamp)object);
        }
        else if (object instanceof DataTimestamp) {
            return compareTo((DataTimestamp) object);
        }
        else throw new ClassCastException("compareTo(object) argument must be a java.sql.Timestamp or DatTimestamp class type: "
                                + object.getClass().getName());
    }

    public int compareTo(DataTimestamp object) {
        return this.value.compareTo(object.value);
    }

/** Overrides the DataObject.class clone(). Calls super.clone() and also clones the contained java.sql.Timestamp data member.
* Returns handle to new object.
*/
    public Object clone() {
        DataTimestamp obj = null;
        obj = (DataTimestamp) super.clone();
        obj.value = (java.sql.Timestamp) obj.value.clone();
        return obj;
    }

    private double getMilliSecsTime() {
        return (double) value.getTime() + (double) value.getNanos()/1000000.;
    }

/** Returns the value of the timestamp interpreted as integer seconds (not millisecs).
*/
    public int intValue() {
        double time = getMilliSecsTime()/1000.;
        return (int) Math.round(time);
    }

/** Returns the value of the timestamp as integer milliseconds.
*/
    public long longValue() {
        return (long) getMilliSecsTime();
    }

/** Returns the value of the timestamp as interpreted as integer seconds (not millisecs).
*/
    public float floatValue() {
        return (float) (getMilliSecsTime()/1000.);
    }

/** Returns the value of the timestamp as integer milliseconds.
*/
    public double doubleValue() {
        return (double) getMilliSecsTime();
    }

/** Returns a new java.util.Date object derived from the timestamp value.
*/
    public java.util.Date dateValue() {
        return new java.util.Date(value.getTime());
    }

/** Returns a clone of the Timestamp object data member.
*/
    public java.sql.Timestamp timestampValue() {
        return (java.sql.Timestamp) value.clone();
    }

/** Sets the timestamp value to the argument value millisecs.
* Does a no-op if isMutable() == false.
* Sets the update status flag, isUpdate() == true.
* Sets the null status flag, isNull() == false.
*/
    public void setValue(int value) {
        setValue((long) value);
    }

/** Sets the timestamp value to the argument value millisecs.
* Does a no-op if isMutable() == false.
* Sets the update status flag, isUpdate() == true.
* Sets the null status flag, isNull() == false.
*/
    public void setValue(long value) {
        if(! isMutable()) return;
        long secs = (value/1000) * 1000;
        this.value.setTime(value);
        long nanosecs = (value - secs) * 1000000;
        this.value.setNanos((int) nanosecs);
        this.valueNull = false;
        this.valueUpdate = true;
    }

/** Sets the timestamp value to the argument value millisecs.
* Does a no-op if isMutable() == false.
* Sets the update status flag, isUpdate() == true.
* Sets the null status flag, isNull() == false, unless input is Float.NaN.
*/
    public void setValue(float value) {
        if (Float.isNaN(value)) setNull(true);
        else setValue((long) value);
    }

/** Sets the timestamp value to the argument value millisecs.
* Does a no-op if isMutable() == false.
* Sets the update status flag, isUpdate() == true.
* Sets the null status flag, isNull() == false, unless input is Double.NaN.
*/
    public void setValue(double value) {
        if (Double.isNaN(value)) setNull(true);
        else setValue((long) value);
    }

/** Sets the timestamp value to the time equivalent of the input object value.
* Input argument can be an instanceof Date, Number, DataObject, or String. Exceptions are thrown if the input argument can
* not be formatted/parsed as a Date.
* Does a no-op if isMutable() == false.
* Sets the state flags isUpdate() == true and isNull() == false,
* unless input is null or if input is a DataObject, object.isNull().
*/
    public void setValue(Object object) throws ClassCastException {
        if(! isMutable()) return;
//        if (object == null) throw new NullPointerException("setValue(Object) argument null");
        if (object == null) {
            setNull(true);
        }
        else if (java.sql.Timestamp.class.isInstance(object)) {
            java.sql.Timestamp ts = (java.sql.Timestamp) object;
            long millisecs = ts.getTime() + ts.getNanos()/1000000;
            setValue(millisecs);
        }
        else if (java.util.Date.class.isInstance(object)) {
            setValue(((java.util.Date) object).getTime());
        }
        else if (Number.class.isInstance(object)) {
            setValue(((Number) object).longValue());
        }
        else if (DataObject.class.isInstance(object)) {
            DataObject obj = (DataObject) object;
            if (obj.isNull()) setNull(true);
            else setValue(obj.longValue());
            //setValue(((DataObject) object).longValue());
        }
        else if (String.class.isInstance(object)) {
            setValue((String) object);
        }
        else throw new ClassCastException("setValue(Object) invalid object argument class type: " + object.getClass().getName());
    }

/** Sets the timestamp value to the time equivalent of the input String value.
* Throws exception if input cannot be formatted/parsed as a Date.
* Does a no-op if isMutable() == false.
* Sets the update status flag, isUpdate() == true.
* Sets the null status flag, isNull() == false, unless input is null.
*/
    public void setValue(String value) throws NumberFormatException {
        if(! isMutable()) return;
        if (value == null) {
            setNull(true);
        }
        else {
          try {
            this.value = java.sql.Timestamp.valueOf(value);
            this.valueUpdate = true;
            this.valueNull = false;
          }
          catch (IllegalArgumentException ex) {
            setValue(Double.parseDouble(value));
          }
        }
    }

/** Sets the object value to NullValueDB.NULL_TIMESTAMP.
 * sets the flags, isNull() == tf and isUpdate() == true .
* Does a no-op if isMutable() == false.
 * */
    public DataObject setNull(boolean tf) {
        if(! isMutable()) return this;
        this.value = NullValueDb.NULL_TIMESTAMP;
        this.value.setNanos(0);
        this.valueNull = tf;
        this.valueUpdate = true;
        return this;
    }


/**
* Returns true if a value can be parsed from input StringTokenizer.
* Does not set value and returns false if tokenizer.hasMoreTokens() == false
* or a float cannot be parsed from token.
*/
    public boolean parseValue(StringTokenizer tokenizer) {
        if (! tokenizer.hasMoreTokens()) return false;
        boolean retVal = false;
        try {
            setValue(tokenizer.nextToken());
            retVal = true;
        }
        catch (Exception ex) {
            System.err.println("DataTimestamp parseValue()" + ex.getMessage());
        }
        return retVal;
    }

    public boolean isValidNumber() {
        return ! isNull();
    }
}
