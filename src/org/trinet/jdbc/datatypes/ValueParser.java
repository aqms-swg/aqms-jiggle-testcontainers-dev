package org.trinet.jdbc.datatypes;
import java.util.*;

public interface ValueParser {
/**
* Returns true if a value can be parsed from input StringTokenizer.
* Does not set value and returns false if tokenizer.hasMoreTokens() == false
* or a valid value cannot be parsed from token.
*/
    boolean parseValue(StringTokenizer tokenizer) ;
}
