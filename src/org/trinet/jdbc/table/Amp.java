package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.*;
import java.sql.Connection;

/** Constructor uses static data members defined by the TableRowAmp interface to initialize the base class with 
* the parameters necessary to describe the schema table named by the interface String parameter DB_TABLE_NAME. 
* The class implements several convenience methods to provides class specific static arguments to the argument
* list of the DataTableRow base class methods. Base class methods are used to set or modify the values and states
* of the contained DataObjects. Because the base class uses a JDBC connection class to access the database containing
* the table described by this object, a connection object must be instantiated before using any of the database enabled
* methods of this class.
* Object states refering to data update, nullness, and mutability are inhereited from the DataTableRow base class
* implementation of the DataState interface. These state conditions are used to control the methods access to the
* object and its contained DataObjects, which also implement the DataState interface.
* The default constructor sets the default states to: setUpdate(false), setNull(true), setMutable(true), and 
* setProcessing(NONE).
*/

public class Amp extends StnChlTableRow implements TableRowAmp {
    public Amp () {
	super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
	initialize();
    }
    
/** Copy constructor invokes the default constructor, then clones all the DataObject classes of the input argument.
* The newly instantiated object state values are set to those of the input Object.
*/ 
    public Amp(Amp object) {
	this();
	for (int index = 0; index < MAX_FIELDS; index++) {
	    fields.set(index, ((DataObject) object.fields.get(index)).clone());
	}
	this.valueUpdate = object.valueUpdate;
	this.valueNull = object.valueNull;
	this.valueMutable = object.valueMutable;
    }

/** Constructor invokes default constructor, then sets the default connection object to the handle of input Connection argument.
* The newly instantiated object state values are those of the default constructor.
*/
    public Amp(Connection conn) {
	this();
        setConnection(conn);
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Amp(long ampid) {
	this();
        fields.set(AMPID, new DataLong(ampid));
	valueNull = false;
	valueUpdate = true;
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Amp(long ampid, double datetime, DataStnChl sc, double amplitude, String units, double wstart, double duration) {
	this(ampid);
	setDataStnChl(sc);
        fields.set(DATETIME, new DataDouble(datetime));
        fields.set(AMPLITUDE, new DataDouble(amplitude));
        fields.set(UNITS, new DataString(units));
	fields.set(WSTART, new DataDouble(wstart));
	fields.set(DURATION, new DataDouble(duration));
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Amp(long ampid, double datetime, DataStnChl sc, double amplitude, String units, double wstart, double duration,
		String iphase, String amptype, String ampmeas) {
	this(ampid, datetime, sc, amplitude, units, wstart, duration);
	fields.set(IPHASE, new DataString(iphase));
	fields.set(AMPTYPE, new DataString(amptype));
	fields.set(AMPMEAS, new DataString(ampmeas));
    }
    
/** Data string members used in base class method argument lists
*/
    protected void initialize() {
	keyColumn = FIELD_NAMES[KEY_COLUMNS[0]];
	assocTableOrigin = "ASSOCAMO";
	assocTableNetmag = "ASSOCAMM";
   }

    /*
     * Returns true if input column field name contains string "DATETIME", false otherwise,
     * subclasses should override this method to add column names whose values are
     * UTC epoch seconds.
     */
    protected boolean isColumnEpochSecsUTC(String columnName) {
        // Added method to allow subclasses handle non-standard names like WSTART, SAVESTART, TIME1 etc. -aww 2010/02/26
        return (columnName.indexOf("DATETIME") >= 0  || columnName.equals("WSTART")); // for UTC - aww 2010/02/26
    }
}
