package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.*;
import java.sql.Connection;
/**
 * Class to access and manipulate the AssocArO (association) table.
 */
public class AssocArO extends OriginAssocTableRow implements TableRowAssocArO {
    public AssocArO () {
        super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
    }
    
    public AssocArO(AssocArO object) {
	this();
        for (int index = 0; index < MAX_FIELDS; index++) {
            fields.set(index, ((DataObject) object.fields.get(index)).clone());
        }
        this.valueUpdate = object.valueUpdate;
        this.valueNull = object.valueNull;
        this.valueMutable = object.valueMutable;
    }
    
/** Constructor invokes default constructor, then assigns the default connection object to the handle of Connection argument.
* The newly instantiated object state values are those of the default constructor.
*/
    public AssocArO(Connection conn) {
	this();
        setConnection(conn);
    }

/** Constructor invokes default constructor, then assigns the input values to the column data members.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public AssocArO(long orid, long arid, String auth) {
	this();
        fields.set(ORID, new DataLong(orid));
        fields.set(ARID, new DataLong(arid));
        fields.set(AUTH, new DataString(auth));
	valueUpdate = true;
	valueNull = false;
    }
  
}
