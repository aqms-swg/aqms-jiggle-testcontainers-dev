package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.*;
import java.sql.Connection;

/** The class implements several convenience methods to provide implementing class extensions some base class methods.
* Because base class methods use a JDBC connection class to access the database containing the table described by this
* object, a connection object must be instantiated before using any of the database enabled methods of this class.
*/
public class AssocWaE extends DataTableRow implements TableRowAssocWaE {

    public AssocWaE() {
        super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
    }

/** Constructor invokes default constructor, then sets the default Connection object to the handle of the input Connection argument.
* The newly instantiated object state values are those of the default constructor.
*/
    public AssocWaE(Connection conn) {
	this();
        setConnection(conn);
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public AssocWaE(long wfid, long evid) {
	this();
        fields.set(WFID, new DataLong(wfid));
        fields.set(EVID, new DataLong(evid));
	valueUpdate = true;
	valueNull = false;
    }

/** Returns table row count.
*/
    public int getRowCount() {
	return ExecuteSQL.getRowCount(connDB, getTableName());
    }

/** Returns table row count corresponding to the specified input event id.
*/
    public int getRowCountByEventId(long evid) {
	String whereString = "WHERE EVID = " + evid ;
	return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns table row count corresponding to the specified input origin id.
*/
    public int getRowCountByOriginId(long orid) {
	String whereString = "WHERE EVID IN ( SELECT EVID FROM ORIGIN WHERE ORID = " + orid + " )";
	return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* WHERE the DATETIME column is BETWEEN the specified input times and the SUBSOURCE column has the specified input value.
* A return value of null indicates no data or an error condition.
*/
// Perhaps needs to qualify AUTH too?
    public AssocWaE [] getRowsByDateTimeRange(double tStart, double tEnd, String subsource) {
	StringBuffer sb = new StringBuffer(132);
        /* removed 2008/01/24 for leap secs -aww
	sb.append("WHERE ( (DATETIME_ON BETWEEN ");
	sb.append(StringSQL.valueOf(tStart) );
	sb.append(" AND ");
	sb.append(StringSQL.valueOf(tEnd));
	sb.append(") OR (DATETIME_OFF BETWEEN ");
	sb.append(StringSQL.valueOf(tStart));
	sb.append(" AND ");
	sb.append(StringSQL.valueOf(tEnd));
	sb.append(") )");
        */
        // below added for leap secs -aww 2008/01/24
	sb.append("WHERE ( (DATETIME_ON BETWEEN TRUETIME.putEpoch(");
	sb.append(StringSQL.valueOf(tStart) );
	sb.append(", 'UTC') AND TRUETIME.putEpoch(");
	sb.append(StringSQL.valueOf(tEnd));
	sb.append(", 'UTC)) OR (DATETIME_OFF BETWEEN TRUETIME.putEpoch(");
	sb.append(StringSQL.valueOf(tStart));
	sb.append(", 'UTC') AND TRUETIME.putEpoch(");
	sb.append(StringSQL.valueOf(tEnd));
	sb.append(", 'UTC)) )");
	if (! NullValueDb.isEmpty(subsource)) {
	    sb.append(" AND SUBSOURCE = ");
	    sb.append(StringSQL.valueOf(subsource)); 
	}
	return (AssocWaE []) getRowsEquals(sb.toString());
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified event id (evid). 
* A return value of null indicates no data or an error condition.
*/
    public AssocWaE [] getRowsByEventId(long evid) {
	return (AssocWaE []) getRowsEquals("EVID", evid);
    }
    
/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified event id (evid) and station channel data inputs. 
* A return value of null indicates no data or an error condition.
*/
    public AssocWaE [] getRowsByEventIdStnChl(long evid, DataStnChl sc) {
       String whereString = "WHERE EVID = " + evid + 
		"  AND " + "WFID IN (SELECT WFID FROM WAVEFORM WHERE " + sc.toStringSQLWhereCondition() + " )";
        return (AssocWaE []) getRowsEquals(whereString);
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified origin id (orid). 
* Method uses the JDBC Connection object assigned with setConnection().
* Returns null if no rows satisfy query or an error condition occurs.
*/
    public AssocWaE [] getRowsByOriginId(long orid) {
	String whereString = "WHERE EVID IN ( SELECT EVID FROM ORIGIN WHERE ORID = " + orid + " )";
	return (AssocWaE []) getRowsEquals(whereString);
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified origin id (orid) and station channel data inputs. 
* Method uses the JDBC Connection object assigned with setConnection().
* Returns null if no rows satisfy query or an error condition occurs.
*/
    public AssocWaE [] getRowsByOriginIdStnChl(long orid, DataStnChl sc) {
	String whereString = "WHERE EVID IN ( SELECT EVID FROM ORIGIN WHERE ORID = " + orid + " )" +
		"  AND " + "WFID IN (SELECT WFID FROM WAVEFORM WHERE " + sc.toStringSQLWhereCondition() + " )";
	return (AssocWaE []) getRowsEquals(whereString);
    }

/*
* Deletes rows from the database table defined by tableName associated with the specified event id (evid).
* Returns number of rows deleted for specified id. A return value of -1 indicates an error condition.
    public int deleteRowsByEvent(long evid) {
	String whereCondition = " WHERE EVID = " + evid;
	return ExecuteSQL.deleteRowsWhere(connDB, getTableName(), whereCondition);
    }

/** Deletes rows associated with the specified origin id (orid).
* Returns number of rows deleted for specified id. A return value of -1 indicates an error condition.
    public int deleteRowsByOrigin(long orid) {
	return ExecuteSQL.deleteRowsEquals(connDB, getTableName(), "ORID", orid);
    }
*/

}
