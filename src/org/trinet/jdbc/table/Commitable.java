package org.trinet.jdbc.table; 
// Temporary til JasiCommitableIF methods implemented elsewhere
public interface Commitable {
    public boolean commit(); //  throws JasiCommitException;
} // end of class CommitableChannelData 
