package org.trinet.jdbc.table;
import java.sql.*;
import java.util.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jasi.*;

public class DataTableRowUtil {

    public static final String DEFAULT_LOC = "  "; // 2-blanks

    public static final int SEEDCHAN   = -1;

    public static final int LOCATION   = 0;
    public static final int CHANNELSRC = 1;
    public static final int AUTH       = 2;
    public static final int AUTHSUBSRC = 3;
    public static final int ONDATE     = 4;
    public static final int OFFDATE    = 5;
    public static final int FULLDATE   = 6;

/**
* Sets the data members of the ChannelIdIF instance with only the input DataTableRow
* net,sta,seedchan,location identifier data.
* Returns the input reference. 
*/
    public static ChannelIdIF parseChannelLocationFromRow(DataTableRow dtr, ChannelIdIF id) {
      return parseChannelIdFromRow(dtr, id, LOCATION);
    }

/**
* Sets the data members of the ChannelIdIF instance with the input DataTableRow 
* net,sta,seedchan,location,channel,channelsrc identifier data.
* Returns the input reference. 
*/
    public static ChannelIdIF parseChannelSrcFromRow(DataTableRow dtr, ChannelIdIF id) {
      return parseChannelIdFromRow(dtr, id, CHANNELSRC);
    }

/**
* Sets the data members of the AuthChannelIdIF instance with the input DataTableRow identifier data:
* net,sta,seedchan,location,channel,channelsrc,auth,subsrc.
* Returns the input reference. 
*/
    public static AuthChannelIdIF parseChannelAuthSubSrcFromRow(DataTableRow dtr, AuthChannelIdIF id) {
      return (AuthChannelIdIF) parseChannelIdFromRow(dtr, id, AUTHSUBSRC);
    }

    public static ChannelIdIF parseChannelIdFromRow(DataTableRow dtr, ChannelIdIF id, int level) {
        try {
            //id.setNet(dtr.getStringValue("NET"));
            //id.setSta(dtr.getStringValue("STA"));
            //id.setSeedchan(dtr.getStringValue("SEEDCHAN"));
            DataObject aDO = dtr.getDataObject("NET");
            id.setNet(aDO.isNull() ? null : aDO.toString());
            aDO = dtr.getDataObject("STA");
            id.setSta(aDO.isNull() ? null : aDO.toString());
            aDO = dtr.getDataObject("SEEDCHAN");
            id.setSeedchan(aDO.isNull() ? null : aDO.toString());

            if (level == SEEDCHAN) return id;

            //id.setLocation(dtr.getStringValue("LOCATION"));
            aDO = dtr.getDataObject("LOCATION");
            id.setLocation(aDO.isNull() ? null : aDO.toString());

            if (level == LOCATION) return id;

            //id.setChannel(dtr.getStringValue("CHANNEL"));
            //id.setChannelsrc(dtr.getStringValue("CHANNELSRC"));
            aDO = dtr.getDataObject("CHANNEL");
            id.setChannel(aDO.isNull() ? null : aDO.toString());
            aDO = dtr.getDataObject("CHANNELSRC");
            id.setChannelsrc(aDO.isNull() ? null : aDO.toString());

            if (level == CHANNELSRC) return id;

            if (id instanceof AuthChannelIdIF) {
              //((AuthChannelIdIF) id).setAuth(dtr.getStringValue("AUTH"));
              //((AuthChannelIdIF) id).setSubsource(dtr.getStringValue("SUBSOURCE"));
              aDO = dtr.getDataObject("AUTH");
              ((AuthChannelIdIF) id).setAuth(aDO.isNull() ? null : aDO.toString());
              aDO = dtr.getDataObject("SUBSOURCE");
              ((AuthChannelIdIF) id).setSubsource(aDO.isNull() ? null : aDO.toString());
            }
        }
        catch (NoSuchFieldException ex) {
            System.err.println("NoSuchField - parsing channel id.");
            ex.printStackTrace(); // could remove after testing
        }
        return id;
    }


    public static void setRowChannelLocation(DataTableRow dtr, ChannelIdIF id) {
      setRowChannelIdData(dtr, id, LOCATION);
    }
    public static void setRowChannelId(DataTableRow dtr, ChannelIdIF id) {
      setRowChannelIdData(dtr, id, CHANNELSRC);
    }
    public static void setRowAuthChannelId(DataTableRow dtr, ChannelIdIF id) {
      setRowChannelIdData(dtr, id, AUTHSUBSRC);
    }

    public static void setRowChannelIdData(DataTableRow dtr, ChannelIdIF id, int level) {
        try {
             String tmpStr = id.getSta();
             if (! NullValueDb.isEmpty(tmpStr) )  dtr.setValue("STA", tmpStr);

             tmpStr = id.getNet();
             if (! NullValueDb.isEmpty(tmpStr) )  dtr.setValue("NET", tmpStr);
     
             tmpStr = id.getSeedchan();
             if (! NullValueDb.isEmpty(tmpStr) )  dtr.setValue("SEEDCHAN", tmpStr);
             if (level == SEEDCHAN) return; 

             tmpStr = id.getLocation();
             if (! NullValueDb.isEmpty(tmpStr) )  dtr.setValue("LOCATION", tmpStr);

             if (level == LOCATION) return; 

             tmpStr = id.getChannel();
             if (! NullValueDb.isEmpty(tmpStr) )  dtr.setValue("CHANNEL", tmpStr);
     
             tmpStr = id.getChannelsrc();
             if (! NullValueDb.isEmpty(tmpStr) )  dtr.setValue("CHANNELSRC", tmpStr);
     
             if (level == CHANNELSRC) return; 

             if (id instanceof AuthChannelIdIF) {
               tmpStr = ((AuthChannelIdIF) id).getAuth();
               if (! NullValueDb.isEmpty(tmpStr) ) dtr.setValue("AUTH", tmpStr);
       
               tmpStr = ((AuthChannelIdIF) id).getSubsource();
               if (! NullValueDb.isEmpty(tmpStr) ) dtr.setValue("SUBSOURCE", tmpStr);
             }
        }
        catch (NoSuchFieldException ex) {
            System.err.println("NoSuchField - setting channel id.");
            ex.printStackTrace(); // could remove after testing
        }
    }

    public static String toOnDateConstraintSQLWhereClause(String tableName, java.util.Date date) {
      return toDateConstraintSQLString(tableName, date, ONDATE) ;
    }
    public static String toOffDateConstraintSQLWhereClause(String tableName, java.util.Date date) {
      return toDateConstraintSQLString(tableName, date, OFFDATE) ;
    }
    public static String toDateConstraintSQLWhereClause(String tableName, java.util.Date date) {
      return toDateConstraintSQLString(tableName, date, FULLDATE) ;
    }
    public static String toDateConstraintSQLString(String tableName, java.util.Date date, int level) {
        StringBuffer sb = new StringBuffer(128);

        String tableStr = (NullValueDb.isEmpty(tableName)) ? "" : tableName + ".";

        String dateStr = (date == null) ? "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)" : StringSQL.toDATE(date);

        if (level == OFFDATE || level == FULLDATE) {
          sb.append(" (");
          sb.append(tableStr).append("OFFDATE >= ").append(dateStr);
          //sb.append(" OR ").append(tableStr).append("OFFDATE IS NULL");
          sb.append(")");
        }
        if (level == ONDATE || level == FULLDATE) {
          if (level == FULLDATE) sb.append(" AND ");
          sb.append("(");
          sb.append(tableStr).append("ONDATE <= ").append(dateStr);
          //sb.append(" OR ").append(tableStr).append("ONDATE IS NULL");
          sb.append(")");
        }
        return sb.toString();
    }

    public static String toChannelLocationSQLWhereClause(String tableName, ChannelIdIF id) {
      return toChannelIdSQLString(tableName, id, LOCATION);
    }
    public static String toChannelAuthSubSrcSQLWhereClause(String tableName, AuthChannelIdIF id) {
      return toChannelIdSQLString(tableName, id, AUTHSUBSRC);
    }
    public static String toChannelSrcSQLWhereClause(String tableName, ChannelIdIF id) {
      return toChannelIdSQLString(tableName, id, CHANNELSRC);
    }

    public static String toPreparedChannelIdDateClause(String tableName, ChannelIdIF id, java.util.Date date) {
      return toPreparedDateSQLClause(tableName, date, 0);
    }
    public static String toPreparedChannelIdClause(String tableName, ChannelIdIF id) {
      return toPreparedDateSQLClause(tableName, null, 1);
    }
    public static String toPreparedDateSQLClause(String tableName, java.util.Date date) {
      return toPreparedDateSQLClause(tableName, date, 2);
    }

    private static String toPreparedDateSQLClause(String tableName, java.util.Date date, int type) {

        String tableStr = (NullValueDb.isEmpty(tableName)) ? "" : tableName + ".";

        StringBuffer sb = new StringBuffer(128);

        if (type == 0 || type == 1) {
          sb.append("(");
          sb.append(tableStr);
          sb.append("STA=?");
  
          sb.append(" AND ");
          sb.append(tableStr);
          sb.append("SEEDCHAN=?");

          sb.append(" AND ");
          sb.append(tableStr);
          sb.append("NET=?");

          sb.append(" AND ");
          sb.append(tableStr);
          sb.append("LOCATION=?");
          sb.append(")");
        }

        if (type == 0 || type == 2) {
          if (type == 0) sb.append (" AND ");
          String dateStr = (date == null) ? "SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)" : "TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS')";
          sb.append(" (").append(dateStr).append(" BETWEEN ").append(tableStr).append("ONDATE AND ").append(tableStr).append("OFFDATE").append(")");
        }

        return sb.toString();
    }


    public static String toChannelIdSQLString(String tableName, ChannelIdIF id, int level) {
        StringBuffer sb = new StringBuffer(128);

        String tableStr = (NullValueDb.isEmpty(tableName)) ? "" : tableName + ".";

        String tmpStr = id.getSta();
        sb.append("(");
        if (! NullValueDb.isEmpty(tmpStr)) {
            sb.append(tableStr);
            sb.append("STA = ");
            sb.append(StringSQL.valueOf(tmpStr));
        }

        tmpStr = id.getSeedchan();
        if (! NullValueDb.isEmpty(tmpStr)) {
            sb.append(" AND ");
            sb.append(tableStr);
            sb.append("SEEDCHAN = ");
            sb.append(StringSQL.valueOf(tmpStr));
        }

        tmpStr = id.getNet();
        if (! NullValueDb.isEmpty(tmpStr)) {
            sb.append(" AND ");
            sb.append(tableStr);
            sb.append("NET = ");
            sb.append(StringSQL.valueOf(tmpStr));
        }
        if (level == SEEDCHAN) {
           sb.append(")");
           return sb.toString();
        }

        tmpStr = id.getLocation();
        if (! NullValueDb.isEmpty(tmpStr)) {
            sb.append(" AND ");
            sb.append(tableStr);
            sb.append("LOCATION = ");
            sb.append(StringSQL.valueOf(tmpStr, false)); // blank strings ok, don't trim to NULL -aww 07/14/05
        }
        if (level == LOCATION ) {
           sb.append(")");
           return sb.toString();
        }

        tmpStr = id.getChannel();
        sb.append(" AND ");
        if (! NullValueDb.isEmpty(tmpStr)) {
            sb.append(tableStr);
            sb.append("CHANNEL = ");
            sb.append(StringSQL.valueOf(tmpStr));
        }
        tmpStr = id.getChannelsrc();
        if (! NullValueDb.isEmpty(tmpStr)) {
            sb.append(" AND ");
            sb.append(tableStr);
            sb.append("CHANNELSRC = ");
            sb.append(StringSQL.valueOf(tmpStr));
        }
        if (level == CHANNELSRC ) {
           sb.append(")");
           return sb.toString();
        }

        if (id instanceof AuthChannelIdIF) {
          tmpStr = ((AuthChannelIdIF) id).getAuth();
          if (! NullValueDb.isEmpty(tmpStr)) {
            sb.append(" AND ");
            sb.append(tableStr);
            sb.append("AUTH = ");
            sb.append(StringSQL.valueOf(tmpStr));
          }

          tmpStr = ((AuthChannelIdIF) id).getSubsource();
          if (! NullValueDb.isEmpty(tmpStr)) {
            sb.append(" AND ");
            sb.append(tableStr);
            sb.append("SUBSOURCE = ");
            sb.append(StringSQL.valueOf(tmpStr));
          }
          sb.append(")");
      }
      return sb.toString();
    }

    /** For input DataTableRow and Connection, if input <i>updateExisting </i> is <i>false</i>,
     * attempts to insert a new row into the appropriate database table,
     * if it's <i>true</i> and the row matching the row key exists,
     * attempts an update of table row data. 
     */ 
    public static boolean rowToDb(Connection conn, DataTableRow aRow, boolean updateExisting) {
    // Could rewrite DataTableRow implementation to use Oracle SQL MERGE to do UPDATE or INSERT
    // would have to rewrite the "SQL" statement generating methods  - aww
        int retVal = 0;
        boolean rowExists = false;

        if (updateExisting) {
          rowExists = aRow.rowExists(conn);
          //System.out.println("DEBUG DataTableRowUtil.rowToDb "+aRow.getClass().getName()+" rowExists: "+rowExists);
        }

        if (rowExists) {
          aRow.setProcessing(DataTableRowStates.UPDATE);
          retVal = aRow.updateRow(conn);
        }
        else {
          aRow.setProcessing(DataTableRowStates.INSERT);
          retVal = aRow.insertRow(conn);
        }

        return (retVal > 0);
    }

} // end of DataTableRowUtil
