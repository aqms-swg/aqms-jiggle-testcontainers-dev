package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;
public interface DateRangedIF {
    public DateRange getDateRange();
    public void setDateRange(DateRange dr);
    public boolean hasDateRange();
    public boolean isValidFor(java.util.Date date);
    public boolean equalsDateRange(DateRange dr);
}
