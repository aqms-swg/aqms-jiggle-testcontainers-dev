package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.*;
import java.sql.Connection;

/** The class implements several convenience methods to provide implementing class extensions some base class methods.
* Because base class methods use a JDBC connection class to access the database containing the table described by this
* object, a connection object must be instantiated before using any of the database enabled methods of this class.
*/
public class EventPrefMag extends DataTableRow implements TableRowEventPrefMag {

    public EventPrefMag() {
        super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
    }

/** Constructor invokes default constructor, then sets the default Connection object to the handle of the input Connection argument.
* The newly instantiated object state values are those of the default constructor.
*/
    public EventPrefMag(Connection conn) {
	this();
        setConnection(conn);
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public EventPrefMag(long evid, long magid, String type) { 
	this();
        fields.set(EVID, new DataLong(evid));
        fields.set(MAGID, new DataLong(magid));
        fields.set(MAGTYPE, new DataString(type));
	valueUpdate = true;
	valueNull = false;
    }

/** Returns table row count.
*/
    public int getRowCount() {
	return ExecuteSQL.getRowCount(connDB, getTableName());
    }

/** Returns table row count corresponding to the specified input event id.
*/
    public int getRowCountByEventId(long evid) {
	String whereString = "WHERE EVID="+evid;
	return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified event id (evid). 
* A return value of null indicates no data or an error condition.
*/
    public EventPrefMag [] getRowsByEventId(long evid) {
	return (EventPrefMag []) getRowsEquals("EVID", evid);
    }
    
/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified event id (evid). 
* Method uses the JDBC Connection object assigned with setConnection().
* Returns null if no rows satisfy query or an error condition occurs.
*/
    public EventPrefMag [] getRowsByEventIdMagType(long evid, String magType) {
	String whereString = "WHERE (EVID=" + evid + ") AND (MAGTYPE='" + magType +  "')";
	return (EventPrefMag []) getRowsEquals(whereString);
    }

/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified magnitude subscript type (type). 
* Method uses the JDBC Connection object assigned with setConnection().
* Returns null if no rows satisfy query or an error condition occurs.
*/
    public EventPrefMag [] getRowsByMagType(String magType) {
	String whereString = "WHERE (MAGTYPE='" + magType + "')";
	return (EventPrefMag []) getRowsEquals(whereString);
    }
}
