package org.trinet.jdbc.table;

/** The class implements several convenience methods to provide implementing class extensions some base class methods.
* Because base class methods use a JDBC connection class to access the database containing the table described by this
* object, a connection object must be instantiated before using any of the database enabled methods of this class.
* Classes which associate the origin id (orid) and another table key extending this class:
* @see Event
* @see SignificantEvent
*/
public abstract class EventTableRow extends DataTableRow implements Cloneable {
    public EventTableRow (String tableName, String sequenceName, int maxFields, int[] keyColumnIndex,
		String [] fieldNames, boolean [] fieldNulls, int [] fieldClassIds ) {
	super(tableName, sequenceName, maxFields, keyColumnIndex, fieldNames, fieldNulls, fieldClassIds);
    }
    
/** Overrides DataTableRow.clone()
*/
    public Object clone() {
	EventTableRow obj = null;
	obj = (EventTableRow) super.clone();
	return obj;
    }

/** Returns table row count.
*/
    public int getRowCount() {
	return ExecuteSQL.getRowCount(connDB, getTableName());
    }

/** Returns row associated with the specified event id (evid). 
* A return value of null indicates no data or an error condition.
*/
    public EventTableRow getRowByEventId(long evid) {
	EventTableRow [] etr = (EventTableRow []) getRowsEquals("EVID", evid);
	if (etr == null) return null;
	else return etr[0];
    }
}
