package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.*;
import java.sql.Connection;
/**
 * Class to access and manipulate the Mec table.
 */

public class Mec extends DataTableRow implements TableRowMec {

/** Constructor uses static data members defined by the TableRowMec interface to initialize the base class with 
* the parameters necessary to describe the schema table named by the interface String parameter DB_TABLE_NAME. 
* The class implements several convenience methods to provides class specific static arguments to the argument
* list of the DataTableRow base class methods. Base class methods are used to set or modify the values and states
* of the contained DataObjects. Because the base class uses a JDBC connection class to access the database containing
* the table described by this object, a connection object must be instantiated before using any of the database enabled
* methods of this class.
* Object states refering to data update, nullness, and mutability are inhereited from the DataTableRow base class
* implementation of the DataState interface. These state conditions are used to control the methods access to the
* object and its contained DataObjects, which also implement the DataState interface.
* The default constructor sets the default states to: setUpdate(false), setNull(true), setMutable(true), and
* setProcessing(NONE).
*/
    public Mec() {
        super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
    }
    
    public Mec(Mec object) {
	this();
        for (int index = 0; index < MAX_FIELDS; index++) {
            fields.set(index, ((DataObject) object.fields.get(index)).clone());
        }
        this.valueUpdate = object.valueUpdate;
        this.valueNull = object.valueNull;
        this.valueMutable = object.valueMutable;
    }
    
/** Constructor invokes default constructor, then sets the default Connection object to the handle of the input Connection argument.
* The newly instantiated object state values are those of the default constructor.
*/
    public Mec(Connection conn) {
	this();
        setConnection(conn);
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Mec(long mecid) {
	this();
	fields.set(MECID, new DataLong(mecid));
	valueUpdate = true;
	valueNull = false;
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Mec(long mecid, double datetime, String auth) {
	this(mecid);
        fields.set(DATETIME, new DataDouble(datetime));
        fields.set(AUTH, new DataString(auth));
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Mec(long mecid, double datetime, String auth, long oridin) {
	this(mecid, datetime, auth);
        fields.set(ORIDIN, new DataLong(oridin));
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Mec(long mecid, double datetime, String auth, long oridin, long oridout) {
	this(mecid, datetime, auth, oridin);
        fields.set(ORIDOUT, new DataLong(oridout));
    }

/** Returns table row count.
*/
    public int getRowCount() {
	return ExecuteSQL.getRowCount(connDB, getTableName());
    }

/** Returns table row count corresponding to the specified input event id.
*/
    public int getRowCountByEventId(long evid) {
        String  whereString = " WHERE ORID IN ( SELECT ORID FROM ORIGIN WHERE EVID = " + evid + " )";
	return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns table row count corresponding to the specified input origin id.
*/
    public int getRowCountByOriginId(long orid) {
        String  whereString = " WHERE ORID = " + orid ;
	return ExecuteSQL.getRowCount(connDB, getTableName(), "*", whereString);
    }

/** Returns an array of this class where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified event id (evid). 
* A return value of null indicates no data or an error condition.
*/
    public Mec [] getRowsByEventId(long evid) {
	String  whereString = "WHERE ORID IN ( SELECT ORID FROM ORIGIN WHERE EVID = " + evid + " )";
	return (Mec []) getRowsEquals(whereString);
    }
   
/** Returns an array of this class where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified origin id (orid). 
* A return value of null indicates no data or an error condition.
*/
    public Mec [] getRowsByOriginId(long orid) {
	return (Mec []) getRowsEquals("ORID", orid);
    }
    
/** Returns an array where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the preferred net origin id (prefor) of the specified event id (evid). 
* A return value of null indicates no data or an error condition.
*/
    public Mec [] getRowsByPreferredOriginId(long evid) {
	String whereString = "WHERE ORID = ( SELECT PREFOR FROM EVENT WHERE EVID = " + evid + " )";
	return (Mec []) getRowsEquals(whereString);
    }

/** Returns an array of this class where each element contains the data from a single table row parsed from an SQL query
* for rows associated with the specified event id (evid). 
* A return value of null indicates no data or an error condition.
*/
    public Mec [] getRowsByPreferredMagId(long evid) {
	String  whereString = "WHERE MAGID = (SELECT PREFMAG FROM FROM EVENT WHERE EVID =" + evid + " )";
	return (Mec []) getRowsEquals(whereString);
    }
    
/*
* Deletes rows associated with the specified event id (evid).
* Returns number of rows deleted for specified id. A return value of -1 indicates an error condition.
    public static int deleteRowsByEventId(long evid) {
	String  whereString =
	  "WHERE MAGID IN (SELECT MAGID FROM NETMAG WHERE ORID IN (SELECT ORID FROM EVENT WHERE EVID =" + evid + " ))";
	return ExecuteSQL.deleteRowsEquals(connDB, getTableName(), whereString);
    }
*/

}
