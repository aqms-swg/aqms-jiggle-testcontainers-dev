package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.*;
import java.sql.Connection;

/** Constructor uses static data members defined by the TableRowOrigin interface to initialize the base class with 
* the parameters necessary to describe the schema table named by the interface String parameter DB_TABLE_NAME. 
* The class implements several convenience methods to provides class specific static arguments to the argument
* list of the DataTableRow base class methods. Base class methods are used to set or modify the values and states
* of the contained DataObjects. Because the base class uses a JDBC connection class to access the database containing
* the table described by this object, a connection object must be instantiated before using any of the database enabled
* methods of this class.
* Object states refering to data update, nullness, and mutability are inhereited from the DataTableRow base class
* implementation of the DataState interface. These state conditions are used to control the methods access to the
* object and its contained DataObjects, which also implement the DataState interface.
* The default constructor sets the default states to: setUpdate(false), setNull(true), setMutable(true), and
* setProcessing(NONE).
*/
public class Origin extends OriginTableRow implements TableRowOrigin {
    public Origin() {
        super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
    }
    
/** Invokes the default constructor, then sets the default connection object to the handle of the input Connection argument.
*/
    public Origin(Connection conn) {
        super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
        setConnection(conn);
    }
    
/** Copy constructor invokes the default constructor, then clones all the DataObject classes of the input argument.
* The newly instantiated object state values are set to those of the input object.
*/ 
    public Origin(Origin object) {
        this();
        for (int index = 0; index < MAX_FIELDS; index++) {
            fields.set(index, ((DataObject) object.fields.get(index)).clone());
        }
        this.valueUpdate = object.valueUpdate;
        this.valueNull = object.valueNull;
        this.valueMutable = object.valueMutable;
    }
    
/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Origin(long orid) {
        this();
        fields.set(ORID, new DataLong(orid));
        valueUpdate = true;
        valueNull = false;
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Origin(long orid, long evid, String auth) {
        this(orid);
        fields.set(EVID, new DataLong(evid));
        fields.set(AUTH, new DataString(auth));
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Origin(long orid, long evid, String auth, double datetime, double lat, double lon) {
        this(orid, evid, auth);
        fields.set(DATETIME, new DataDouble());
        fields.set(LAT, new DataDouble());
        fields.set(LON, new DataDouble());
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public Origin(long orid, long evid, String auth, double datetime, double lat, double lon, double depth) {
        this(orid, evid, auth, datetime, lat, lon);
        fields.set(DEPTH, new DataDouble());
    }

/** Returns an array of this class where each element contains the data from a single table row parsed from an SQL query
* WHERE the DATETIME column is BETWEEN the specified input times and the SUBSOURCE column has the specified input value.
* A return value of null indicates no data or an error condition.
*/
// Perhaps needs to qualify AUTH too?
    public Origin [] getRowsByDateTimeRange(double tStart, double tEnd, String subsource) {
        String whereString = null;
        /*
	if (NullValueDb.isEmpty(subsource))
	    whereString = "WHERE DATETIME BETWEEN " + StringSQL.valueOf(tStart) + " AND " + StringSQL.valueOf(tEnd);
	else
	    whereString = "WHERE DATETIME BETWEEN " + StringSQL.valueOf(tStart) + " AND " + StringSQL.valueOf(tEnd) 
			+ " AND SUBSOURCE = " + StringSQL.valueOf(subsource); 
        */
        if (NullValueDb.isEmpty(subsource)) // replace above with below for leap secs -aww 2008/01/24
            whereString =
                "WHERE DATETIME BETWEEN TRUETIME.putEpoch(" + StringSQL.valueOf(tStart) +
                ", 'UTC') AND TRUETIME.putEpoch(" + StringSQL.valueOf(tEnd) + ", 'UTC')";
        else
            whereString =
                "WHERE (DATETIME BETWEEN TRUETIME.putEpoch(" + StringSQL.valueOf(tStart) +
                ", 'UTC') AND TRUETIME.putEpoch(" + StringSQL.valueOf(tEnd) + ", 'UTC')" +
                ") AND SUBSOURCE = " + StringSQL.valueOf(subsource); 
        return (Origin []) getRowsEquals(whereString);
    }

    public Origin getPrefId(long evid) {
        return (Origin) getRowByPreferredOriginId(evid);
    }

}
