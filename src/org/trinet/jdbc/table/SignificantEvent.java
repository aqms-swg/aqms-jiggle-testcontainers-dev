package org.trinet.jdbc.table ;
import java.sql.Connection;
import org.trinet.jdbc.StringSQL;
import org.trinet.jdbc.datatypes.*;
/** Class to access and manipulate the NCEDC parametic schema Significant_Event table.
*/
public class SignificantEvent extends EventTableRow implements TableRowSignificantEvent {

/** Constructor uses static data members defined by the TableRowSignificantEvent interface to initialize the base class with 
* the parameters necessary to describe the schema table named by the interface String parameter DB_TABLE_NAME. 
* The class implements several convenience methods to provides class specific static arguments to the argument
* list of the DataTableRow base class methods. Base class methods are used to set or modify the values and states
* of the contained DataObjects. Because the base class uses a JDBC connection class to access the database containing
* the table described by this object, a connection object must be instantiated before using any of the database enabled
* methods of this class.
* Object states refering to data update, nullness, and mutability are inhereited from the DataTableRow base class
* implementation of the DataState interface. These state conditions are used to control the methods access to the
* object and its contained DataObjects, which also implement the DataState interface.
* The default constructor sets the default states to: setUpdate(false), setNull(true), setMutable(true), and
* setProcessing(NONE).
*/
    public SignificantEvent () {
        super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
    }
    
    public SignificantEvent(SignificantEvent object) {
        this();
        for (int index = 0; index < MAX_FIELDS; index++) {
            fields.set(index, ((DataObject) object.fields.get(index)).clone());
        }
        this.valueUpdate = object.valueUpdate;
        this.valueNull = object.valueNull;
        this.valueMutable = object.valueMutable;
    }

/** Constructor invokes default constructor, then sets the default Connection object to the handle of the input Connection argument.
* The newly instantiated object state values are those of the default constructor.
*/
    public SignificantEvent(Connection conn) {
        this();
        setConnection(conn);
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public SignificantEvent(long evid) {
        this();
        fields.set(EVID, new DataLong(evid));
        valueUpdate = true;
        valueNull = false;
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public SignificantEvent(long evid, String evname, String remarks) {
        this(evid);
        fields.set(EVNAME, new DataString(evname));
        fields.set(REMARKS, new DataString(remarks));
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public SignificantEvent(long evid, String evname, String remarks, int nfelt) {
        this(evid, evname, remarks);
        fields.set(NFELT, new DataLong(nfelt));
    }
    
/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public SignificantEvent(long evid, String evname, String remarks, int nfelt, int mmi) {
        this(evid, evname, remarks, nfelt);
        fields.set(MMI, new DataLong(mmi));
    }
    
/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public SignificantEvent(long evid, String evname, String remarks, int nfelt, int mmi, double pga) {
        this(evid, evname, remarks, nfelt, mmi);
        fields.set(PGA, new DataDouble(pga));
    }

/** Returns an SignificantEvent array whose event origin times are between the specified values inclusive.
* Returns null if no data is found or a JDBC SQLException occurs during processing the query, input times
* must be UTC seconds.
*/
    public SignificantEvent [] getRowsByDateTime(double start, double end) {
        String whereString = // replaced for leap secs 2008/01/24 -aww
           //"WHERE EVID IN (SELECT EVID FROM EVENT WHERE PREFOR IN (SELECT ORID FROM ORIGIN WHERE DATETIME BETWEEN "
           "WHERE EVID IN (SELECT EVID FROM EVENT WHERE PREFOR IN (SELECT ORID FROM ORIGIN WHERE DATETIME BETWEEN TRUETIME.putEpoch("
               + StringSQL.valueOf(start) + ",'UTC') AND TRUETIME.putEpoch(" + StringSQL.valueOf(end) + ",'UTC') ))";
        return (SignificantEvent []) getRowsEquals(whereString);
    }
}
