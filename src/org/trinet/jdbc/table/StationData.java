package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.*;
import java.sql.Connection;
import java.util.Date; // qualify as java.util not java.sql subclass -aww 2008/02/12

/** Constructor uses static data members defined by the TableRowStationData interface to initialize the base class with 
* the parameters necessary to describe the schema table named by the interface String parameter DB_TABLE_NAME. 
* The class implements several convenience methods to provides class specific static arguments to the argument
* list of the DataTableRow base class methods. Base class methods are used to set or modify the values and states
* of the contained DataObjects. Because the base class uses a JDBC connection class to access the database containing
* the table described by this object, a connection object must be instantiated before using any of the database enabled
* methods of this class.
* Object states refering to data update, nullness, and mutability are inhereited from the DataTableRow base class
* implementation of the DataState interface. These state conditions are used to control the methods access to the
* object and its contained DataObjects, which also implement the DataState interface.
* The default constructor sets the default states to: setUpdate(false), setNull(true), setMutable(true), and 
* setProcessing(NONE).
*/

public class StationData extends ResponseStnTableRow implements TableRowStationData {
    public StationData () {
	super(DB_TABLE_NAME, SEQUENCE_NAME, MAX_FIELDS, KEY_COLUMNS, FIELD_NAMES, FIELD_NULLS, FIELD_CLASS_IDS);
    }
    
/** Copy constructor invokes the default constructor, then clones all the DataObject classes of the input argument.
* The newly instantiated object state values are set to those of the input object.
*/ 
    public StationData(StationData object) {
	this();
	for (int index = 0; index < MAX_FIELDS; index++) {
	    fields.set(index, ((DataObject) object.fields.get(index)).clone());
	}
	this.valueUpdate = object.valueUpdate;
	this.valueNull = object.valueNull;
	this.valueMutable = object.valueMutable;
    }

/** Constructor invokes default constructor, then sets the default Connection object to the handle of the input Connection argument.
* The newly instantiated object state values are those of the default constructor.
*/
    public StationData(Connection conn) {
	this();
        setConnection(conn);
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public StationData(String net, String sta, java.util.Date onDate) { // qualify as java.util not java.sql subclass
	this();
        fields.set(NET, new DataString(net));
        fields.set(STA, new DataString(sta));
        fields.set(ONDATE, new DataDate(onDate));
	valueUpdate = true;
	valueNull = false;
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public StationData(String net, String sta, java.util.Date ondate, int word_32, int word_16) { // qualify as java.util not java.sql subclass
	this(net, sta, ondate);
        fields.set(WORD_32, new DataLong(word_32));
        fields.set(WORD_16, new DataLong(word_16));
    }

/** Constructor invokes default constructor, then sets the column data members to the input values.
* The newly instantiated object state values are isUpdate() == true and isNull == false.
*/
    public StationData(String net, String sta, java.util.Date ondate, int word_32, int word_16, double lat, double lon, double elev,
		String staname, int net_id) {
	this(net, sta, ondate, word_32, word_16);
        fields.set(LAT, new DataDouble(lat));
        fields.set(LON, new DataDouble(lon));
        fields.set(ELEV, new DataDouble(elev));
        fields.set(STANAME, new DataString(staname));
        fields.set(NET_ID, new DataLong(net_id));
    }
}
