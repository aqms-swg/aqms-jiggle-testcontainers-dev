package org.trinet.jdbc.table;

import org.trinet.jdbc.datatypes.DataClassIds;

/** Interface of static data constants defining the named table.
* @see Amp
*/
// DDG 3/2003 - made string definitions more dynamic so that:
// 1) Column names are only defined in one place (FIELD_NAMES)
// 2) A different table name can be specified if this interface is extended
//    as it is in UnassocAmp

//public interface TableRowAmp extends DataClassIds {
public interface TableRowAmp extends TableRowGenericAmp {
/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "AMP";

    public static final String QUALIFIED_COLUMN_NAMES =
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[0]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[1]+","+
        "truetime.getEpoch("+TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[2]+",'UTC'),"+// for db to UTC conversion -aww 2008/02/12
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[3]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[4]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[5]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[6]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[7]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[8]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[9]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[10]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[11]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[12]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[13]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[14]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[15]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[16]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[17]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[18]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[19]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[20]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[21]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[22]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[23]+","+
        "truetime.getEpoch("+TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[24]+",'UTC'),"+// for db to UTC conversion -aww 2008/02/25
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[25]+","+
        TableRowAmp.DB_TABLE_NAME+"."+FIELD_NAMES[26];

}
