package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see AssocCoO
*/
public interface TableRowAssocCoO extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "ASSOCCOO";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  9;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "";


/**  ASSOCCOO table "orid" column data object offset in collection stored by implementing class.
*/
    public static final int ORID = 0;

/**  ASSOCCOO table "coid" column data object offset in collection stored by implementing class.
*/
    public static final int COID = 1;

/**  ASSOCCOO table "commid" column data object offset in collection stored by implementing class.
*/
    public static final int COMMID = 2;

/**  ASSOCCOO table "auth" column data object offset in collection stored by implementing class.
*/
    public static final int AUTH = 3;

/**  ASSOCCOO table "subsource" column data object offset in collection stored by implementing class.
*/
    public static final int SUBSOURCE = 4;

/**  ASSOCCOO table "rflag" column data object offset in collection stored by implementing class.
*/
    public static final int RFLAG = 5;

/**  ASSOCCOO table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 6;

/**  ASSOCCOO table "delta" column data object offset in collection stored by implementing class.
*/
    public static final int DELTA = 7;

/**  ASSOCCOO table "seaz" column data object offset in collection stored by implementing class.
*/
    public static final int SEAZ = 8;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
   "ORID,COID,COMMID,AUTH,SUBSOURCE,RFLAG,LDDATE,DELTA,SEAZ";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "ASSOCCOO.ORID,ASSOCCOO.COID,ASSOCCOO.COMMID,ASSOCCOO.AUTH,ASSOCCOO.SUBSOURCE,ASSOCCOO.RFLAG,ASSOCCOO.LDDATE,ASSOCCOO.DELTA,ASSOCCOO.SEAZ";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"ORID", "COID", "COMMID", "AUTH", "SUBSOURCE", 
	"RFLAG", "LDDATE", "DELTA", "SEAZ"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, false, true, false, true, 
	true, true, true, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATALONG, DATALONG, DATALONG, DATASTRING, DATASTRING, 
	DATASTRING, DATADATE, DATADOUBLE, DATADOUBLE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0, 1};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 0, 0, 0, 0, 0, 1, 1
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	15, 15, 15, 15, 8, 2, 7, 5, 4
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))", null, null
    };
}
