package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see AssocWaE
*/
public interface TableRowAssocWaE extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "ASSOCWAE";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  5;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "";


/**  ASSOCWAE table "wfid" column data object offset in collection stored by implementing class.
*/
    public static final int WFID = 0;

/**  ASSOCWAE table "evid" column data object offset in collection stored by implementing class.
*/
    public static final int EVID = 1;

/**  ASSOCWAE table "datetime_on" column data object offset in collection stored by implementing class.
*/
    public static final int DATETIME_ON = 2;

/**  ASSOCWAE table "datetime_off" column data object offset in collection stored by implementing class.
*/
    public static final int DATETIME_OFF = 3;

/**  ASSOCWAE table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 4;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
   "WFID,EVID,truetime.getEpoch(DATETIME_ON,'UTC'),truetime.getEpoch(DATETIME_OFF,'UTC'),LDDATE"; // for db to UTC conversion -aww 2008/02/12

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "ASSOCWAE.WFID,ASSOCWAE.EVID,truetime.getEpoch(ASSOCWAE.DATETIME_ON,'UTC'),truetime.getEpoch(ASSOCWAE.DATETIME_OFF,'UTC'),ASSOCWAE.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"WFID", "EVID", "DATETIME_ON", "DATETIME_OFF", "LDDATE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, false, true, true, false
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATALONG, DATALONG, DATADOUBLE, DATADOUBLE, DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0, 1};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 10, 10, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	15, 15, 25, 25, 7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)) "
    };
}
