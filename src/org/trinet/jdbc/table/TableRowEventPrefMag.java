package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see EventPrefMag
*/
public interface TableRowEventPrefMag extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "EVENTPREFMAG";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  4;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "";


/**  EVENTPREFMAG table "evid" column data object offset in collection stored by implementing class.
*/
    public static final int EVID = 0;

/**  EVENTPREFMAG table "magid" column data object offset in collection stored by implementing class.
*/
    public static final int MAGID = 1;

/**  EVENTPREFMAG table "type" column data object offset in collection stored by implementing class.
*/
    public static final int MAGTYPE = 2;

/**  EVENTPREFMAG table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 3;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
   "EVID,MAGID,MAGTYPE,LDDATE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "EVENTPREFMAG.EVID,EVENTPREFMAG.MAGID,EVENTPREFMAG.MAGTYPE,EVENTPREFMAG.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"EVID", "MAGID", "MAGTYPE", "LDDATE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, false, false, false
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATALONG, DATALONG, DATASTRING, DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0, 2};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	15, 15, 6, 7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)) "
    };
}
