package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see Filename
*/
public interface TableRowFilename extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "FILENAME";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  7;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "FILESEQ";


/**  FILENAME table "fileid" column data object offset in collection stored by implementing class.
*/
    public static final int FILEID = 0;

/**  FILENAME table "dfile" column data object offset in collection stored by implementing class.
*/
    public static final int DFILE = 1;

/**  FILENAME table "datetime_on" column data object offset in collection stored by implementing class.
*/
    public static final int DATETIME_ON = 2;

/**  FILENAME table "datetime_off" column data object offset in collection stored by implementing class.
*/
    public static final int DATETIME_OFF = 3;

/**  FILENAME table "nbytes" column data object offset in collection stored by implementing class.
*/
    public static final int NBYTES = 4;

/**  FILENAME table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 5;

/**  FILENAME table "subdirid" column data object offset in collection stored by implementing class.
*/
    public static final int SUBDIRID = 6;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
   "FILEID,DFILE,truetime.getEpoch(DATETIME_ON,'UTC),truetime.getEpoch(DATETIME_OFF,'UTC),NBYTES,LDDATE,SUBDIRID"; // convert from db to UTC -aww 2008/02/12

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "FILENAME.FILEID,FILENAME.DFILE,truetime.getEpoch(FILENAME.DATETIME_ON,'UTC),truetime.getEpoch(FILENAME.DATETIME_OFF,'UTC),FILENAME.NBYTES,FILENAME.LDDATE,FILENAME.SUBDIRID";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"FILEID", "DFILE", "DATETIME_ON", "DATETIME_OFF", "NBYTES", 
	"LDDATE", "SUBDIRID"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, false, true, true, true, 
	false, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATALONG, DATASTRING, DATADOUBLE, DATADOUBLE, DATALONG, 
	DATADATE, DATALONG
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 10, 10, 0, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	15, 32, 25, 25, 8, 7, 15
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)) ", null
    };
}
