package org.trinet.jdbc.table;

import org.trinet.jdbc.datatypes.DataClassIds;

/** Interface of static data constants defining the named table.
* @see Amp
*/
// DDG 3/2003 - made string definitions more dynamic so that:
// 1) Column names are only defined in one place (FIELD_NAMES)
// 2) A different table name can be specified if this interface is extended
//    as it is in UnassocAmp

public interface TableRowGenericAmp extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static String DB_TABLE_NAME =  "GENERIC";  // dummy

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  27;   // 0 - 26

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "AMPSEQ";

/**  AMP table "ampid" column data object offset in collection stored by implementing class.
*/
    public static final int AMPID = 0;

/**  AMP table "commid" column data object offset in collection stored by implementing class.
*/
    public static final int COMMID = 1;

/**  AMP table "datetime" column data object offset in collection stored by implementing class.
*/
    public static final int DATETIME = 2;

/**  AMP table "sta" column data object offset in collection stored by implementing class.
*/
    public static final int STA = 3;

/**  AMP table "net" column data object offset in collection stored by implementing class.
*/
    public static final int NET = 4;

/**  AMP table "auth" column data object offset in collection stored by implementing class.
*/
    public static final int AUTH = 5;

/**  AMP table "subsource" column data object offset in collection stored by implementing class.
*/
    public static final int SUBSOURCE = 6;

/**  AMP table "channel" column data object offset in collection stored by implementing class.
*/
    public static final int CHANNEL = 7;

/**  AMP table "channelsrc" column data object offset in collection stored by implementing class.
*/
    public static final int CHANNELSRC = 8;

/**  AMP table "seedchan" column data object offset in collection stored by implementing class.
*/
    public static final int SEEDCHAN = 9;

/**  AMP table "location" column data object offset in collection stored by implementing class.
*/
    public static final int LOCATION = 10;

/**  AMP table "iphase" column data object offset in collection stored by implementing class.
*/
    public static final int IPHASE = 11;

/**  AMP table "amplitude" column data object offset in collection stored by implementing class.
*/
    public static final int AMPLITUDE = 12;

/**  AMP table "amptype" column data object offset in collection stored by implementing class.
*/
    public static final int AMPTYPE = 13;

/**  AMP table "units" column data object offset in collection stored by implementing class.
*/
    public static final int UNITS = 14;

/**  AMP table "ampmeas" column data object offset in collection stored by implementing class.
*/
    public static final int AMPMEAS = 15;

/**  AMP table "eramp" column data object offset in collection stored by implementing class.
*/
    public static final int ERAMP = 16;

/**  AMP table "flagamp" column data object offset in collection stored by implementing class.
*/
    public static final int FLAGAMP = 17;

/**  AMP table "per" column data object offset in collection stored by implementing class.
*/
    public static final int PER = 18;

/**  AMP table "snr" column data object offset in collection stored by implementing class.
*/
    public static final int SNR = 19;

/**  AMP table "tau" column data object offset in collection stored by implementing class.
*/
    public static final int TAU = 20;

/**  AMP table "quality" column data object offset in collection stored by implementing class.
*/
    public static final int QUALITY = 21;

/**  AMP table "rflag" column data object offset in collection stored by implementing class.
*/
    public static final int RFLAG = 22;

/**  AMP table "cflag" column data object offset in collection stored by implementing class.
*/
    public static final int CFLAG = 23;

/**  AMP table "wstart" column data object offset in collection stored by implementing class.
*/
    public static final int WSTART = 24;

/**  AMP table "duration" column data object offset in collection stored by implementing class.
*/
    public static final int DURATION = 25;

/**  AMP table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 26;

/**
 * Table column data field names. This is the only place the names are defined
 * all else is derived from this.
 */
    public static final String [] FIELD_NAMES  = {
        "AMPID", "COMMID", "DATETIME", "STA", "NET",
        "AUTH", "SUBSOURCE", "CHANNEL", "CHANNELSRC", "SEEDCHAN",
        "LOCATION", "IPHASE", "AMPLITUDE", "AMPTYPE", "UNITS",
        "AMPMEAS", "ERAMP", "FLAGAMP", "PER", "SNR",
        "TAU", "QUALITY", "RFLAG", "CFLAG", "WSTART",
        "DURATION", "LDDATE"
    };

/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
        FIELD_NAMES[0]+","+
        FIELD_NAMES[1]+","+
        "truetime.getEpoch("+FIELD_NAMES[2]+",'UTC'),"+ // for db to UTC conversion -aww 2008/02/12
        FIELD_NAMES[3]+","+
        FIELD_NAMES[4]+","+
        FIELD_NAMES[5]+","+
        FIELD_NAMES[6]+","+
        FIELD_NAMES[7]+","+
        FIELD_NAMES[8]+","+
        FIELD_NAMES[9]+","+
        FIELD_NAMES[10]+","+
        FIELD_NAMES[11]+","+
        FIELD_NAMES[12]+","+
        FIELD_NAMES[13]+","+
        FIELD_NAMES[14]+","+
        FIELD_NAMES[15]+","+
        FIELD_NAMES[16]+","+
        FIELD_NAMES[17]+","+
        FIELD_NAMES[18]+","+
        FIELD_NAMES[19]+","+
        FIELD_NAMES[20]+","+
        FIELD_NAMES[21]+","+
        FIELD_NAMES[22]+","+
        FIELD_NAMES[23]+","+
        "truetime.getEpoch("+FIELD_NAMES[24]+",'UTC'),"+ // for db to UTC conversion -aww 2010/02/25
        FIELD_NAMES[25]+","+
        FIELD_NAMES[26];

//   "AMPID,COMMID,DATETIME,STA,NET,AUTH,SUBSOURCE,CHANNEL,CHANNELSRC,SEEDCHAN,LOCATION,IPHASE,AMPLITUDE,AMPTYPE,UNITS,AMPMEAS,ERAMP,FLAGAMP,PER,SNR,TAU,QUALITY,RFLAG,CFLAG,WSTART,DURATION,LDDATE";
// String of table qualified column names delimited by ",". Note that the tablename value is
// variable in case this interface is extended, as it is in UnassocAmp.
/*
    public static final String QUALIFIED_COLUMN_NAMES =
        DB_TABLE_NAME+"."+FIELD_NAMES[0]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[1]+","+
        "truetime.getEpoch("+DB_TABLE_NAME+"."+FIELD_NAMES[2]+",'UTC'),"+// for db to UTC conversion -aww 2008/02/12
        DB_TABLE_NAME+"."+FIELD_NAMES[3]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[4]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[5]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[6]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[7]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[8]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[9]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[10]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[11]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[12]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[13]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[14]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[15]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[16]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[17]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[18]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[19]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[20]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[21]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[22]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[23]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[24]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[25]+","+
        DB_TABLE_NAME+"."+FIELD_NAMES[26];
*/
// "AMP.AMPID,AMP.COMMID,AMP.DATETIME,AMP.STA,AMP.NET,AMP.AUTH,AMP.SUBSOURCE,AMP.CHANNEL,AMP.CHANNELSRC,AMP.SEEDCHAN,AMP.LOCATION,AMP.IPHASE,AMP.AMPLITUDE,AMP.AMPTYPE,AMP.UNITS,AMP.AMPMEAS,AMP.ERAMP,AMP.FLAGAMP,AMP.PER,AMP.SNR,AMP.TAU,AMP.QUALITY,AMP.RFLAG,AMP.CFLAG,AMP.WSTART,AMP.DURATION,AMP.LDDATE";

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
        false, true, false, false, true,
        false, true, true, true, true,
        true, true, false, true, false,
        true, true, true, true, true,
        true, true, true, true, false,
        false, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
        DATALONG, DATALONG, DATADOUBLE, DATASTRING, DATASTRING,
        DATASTRING, DATASTRING, DATASTRING, DATASTRING, DATASTRING,
        DATASTRING, DATASTRING, DATADOUBLE, DATASTRING, DATASTRING,
        DATASTRING, DATADOUBLE, DATASTRING, DATADOUBLE, DATADOUBLE,
        DATADOUBLE, DATADOUBLE, DATASTRING, DATASTRING, DATADOUBLE,
        DATADOUBLE, DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
        0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 0, 4, 0,
        4, 1, 0, 0, 0, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
 * For use in JTables, etc.
 */
    public static final int [] FIELD_SIZES = {
        15, 15, 25, 6, 8, 15, 8, 3, 8, 3, 2, 8, 126, 8, 4, 1, 5, 4, 10, 126,
        7, 2, 2, 2, 126, 126, 7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
        null, null, null, null, null, null, null, null, null, null,
        null, null, null, null, null, null, null, null, null, null,
        null, null, null, null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))"
    };

}

