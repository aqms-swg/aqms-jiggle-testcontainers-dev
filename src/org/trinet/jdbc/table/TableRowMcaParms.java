package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see McaParms
*/
public interface TableRowMcaParms extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "MCAPARMS";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  17;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "";


/**  MCAPARMS table "net" column data object offset in collection stored by implementing class.
*/
    public static final int NET = 0;

/**  MCAPARMS table "sta" column data object offset in collection stored by implementing class.
*/
    public static final int STA = 1;

/**  MCAPARMS table "seedchan" column data object offset in collection stored by implementing class.
*/
    public static final int SEEDCHAN = 2;

/**  MCAPARMS table "channel" column data object offset in collection stored by implementing class.
*/
    public static final int CHANNEL = 3;

/**  MCAPARMS table "channelsrc" column data object offset in collection stored by implementing class.
*/
    public static final int CHANNELSRC = 4;

/**  MCAPARMS table "location" column data object offset in collection stored by implementing class.
*/
    public static final int LOCATION = 5;

/**  MCAPARMS table "neq" column data object offset in collection stored by implementing class.
*/
    public static final int NEQ = 6;

/**  MCAPARMS table "afix" column data object offset in collection stored by implementing class.
*/
    public static final int AFIX = 7;

/**  MCAPARMS table "qfix" column data object offset in collection stored by implementing class.
*/
    public static final int QFIX = 8;

/**  MCAPARMS table "afree" column data object offset in collection stored by implementing class.
*/
    public static final int AFREE = 9;

/**  MCAPARMS table "qfree" column data object offset in collection stored by implementing class.
*/
    public static final int QFREE = 10;

/**  MCAPARMS table "slope" column data object offset in collection stored by implementing class.
*/
    public static final int SLOPE = 11;

/**  MCAPARMS table "clip" column data object offset in collection stored by implementing class.
*/
    public static final int CLIP = 12;

/**  MCAPARMS table "cutoff" column data object offset in collection stored by implementing class.
*/
    public static final int CUTOFF = 13;

/**  MCAPARMS table "ondate" column data object offset in collection stored by implementing class.
*/
    public static final int ONDATE = 14;

/**  MCAPARMS table "offdate" column data object offset in collection stored by implementing class.
*/
    public static final int OFFDATE = 15;

/**  MCAPARMS table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 16;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
   "NET,STA,SEEDCHAN,CHANNEL,CHANNELSRC,LOCATION,NEQ,AFIX,QFIX,AFREE,QFREE,SLOPE,CLIP,CUTOFF,ONDATE,OFFDATE,LDDATE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "MCAPARMS.NET,MCAPARMS.STA,MCAPARMS.SEEDCHAN,MCAPARMS.CHANNEL,MCAPARMS.CHANNELSRC,MCAPARMS.LOCATION,MCAPARMS.NEQ,MCAPARMS.AFIX,MCAPARMS.QFIX,MCAPARMS.AFREE,MCAPARMS.QFREE,MCAPARMS.SLOPE,MCAPARMS.CLIP,MCAPARMS.CUTOFF,MCAPARMS.ONDATE,MCAPARMS.OFFDATE,MCAPARMS.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"NET", "STA", "SEEDCHAN", "CHANNEL", "CHANNELSRC", 
	"LOCATION", "NEQ", "AFIX", "QFIX", "AFREE", 
	"QFREE", "SLOPE", "CLIP", "CUTOFF", "ONDATE", 
	"OFFDATE", "LDDATE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, false, false, true, true, 
	false, false, false, false, false, 
	false, false, true, true, false, 
	true, true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATASTRING, DATASTRING, DATASTRING, DATASTRING, DATASTRING, 
	DATASTRING, DATALONG, DATADOUBLE, DATADOUBLE, DATADOUBLE, 
	DATADOUBLE, DATADOUBLE, DATALONG, DATALONG, DATADATE, 
	DATADATE, DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {1, 5, 2, 0};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 4, 4, 0, 0, 0, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	8, 6, 3, 3, 8, 2, 6, 7, 7, 7, 7, 7, 10, 6, 7, 7, 7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, null, "'00' ", null, null, null, null, 
	null, null, null, null, "TO_DATE('1970-01-01', 'YYYY-MM-DD') ", null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))"
    };
}
