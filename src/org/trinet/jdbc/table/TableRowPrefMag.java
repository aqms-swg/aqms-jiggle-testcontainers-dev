package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see PrefMag
*/
public interface TableRowPrefMag extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "PREFMAG";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  5;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "";


/**  PREFMAG table "evid" column data object offset in collection stored by implementing class.
*/
    public static final int EVID = 0;

/**  PREFMAG table "orid" column data object offset in collection stored by implementing class.
*/
    public static final int ORID = 1;

/**  PREFMAG table "magid" column data object offset in collection stored by implementing class.
*/
    public static final int MAGID = 2;

/**  PREFMAG table "type" column data object offset in collection stored by implementing class.
*/
    public static final int TYPE = 3;

/**  PREFMAG table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 4;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =
   "EVID,ORID,MAGID,TYPE,LDDATE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES = 
    "PREFMAG.EVID,PREFMAG.ORID,PREFMAG.MAGID,PREFMAG.TYPE,PREFMAG.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"EVID", "ORID", "MAGID", "TYPE", "LDDATE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, false, false, false, false
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATALONG, DATALONG, DATALONG, DATASTRING, DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {1, 3};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 0, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	15, 15, 15, 6, 7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP)) "
    };
}
