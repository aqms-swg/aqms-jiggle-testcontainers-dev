package org.trinet.jdbc.table;
import org.trinet.jdbc.datatypes.DataClassIds;
/** Interface of static data constants defining the named table.
* @see Waveform
*/
public interface TableRowWaveform extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "WAVEFORM";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  26;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "WASEQ";


/**  WAVEFORM table "wfid" column data object offset in collection stored by implementing class.
*/
    public static final int WFID = 0;

/**  WAVEFORM table "net" column data object offset in collection stored by implementing class.
*/
    public static final int NET = 1;

/**  WAVEFORM table "sta" column data object offset in collection stored by implementing class.
*/
    public static final int STA = 2;

/**  WAVEFORM table "auth" column data object offset in collection stored by implementing class.
*/
    public static final int AUTH = 3;

/**  WAVEFORM table "subsource" column data object offset in collection stored by implementing class.
*/
    public static final int SUBSOURCE = 4;

/**  WAVEFORM table "channel" column data object offset in collection stored by implementing class.
*/
    public static final int CHANNEL = 5;

/**  WAVEFORM table "channelsrc" column data object offset in collection stored by implementing class.
*/
    public static final int CHANNELSRC = 6;

/**  WAVEFORM table "seedchan" column data object offset in collection stored by implementing class.
*/
    public static final int SEEDCHAN = 7;

/**  WAVEFORM table "location" column data object offset in collection stored by implementing class.
*/
    public static final int LOCATION = 8;

/**  WAVEFORM table "archive" column data object offset in collection stored by implementing class.
*/
    public static final int ARCHIVE = 9;

/**  WAVEFORM table "datetime_on" column data object offset in collection stored by implementing class.
*/
    public static final int DATETIME_ON = 10;

/**  WAVEFORM table "datetime_off" column data object offset in collection stored by implementing class.
*/
    public static final int DATETIME_OFF = 11;

/**  WAVEFORM table "samprate" column data object offset in collection stored by implementing class.
*/
    public static final int SAMPRATE = 12;

/**  WAVEFORM table "wavetype" column data object offset in collection stored by implementing class.
*/
    public static final int WAVETYPE = 13;

/**  WAVEFORM table "fileid" column data object offset in collection stored by implementing class.
*/
    public static final int FILEID = 14;

/**  WAVEFORM table "foff" column data object offset in collection stored by implementing class.
*/
    public static final int FOFF = 15;

/**  WAVEFORM table "nbytes" column data object offset in collection stored by implementing class.
*/
    public static final int NBYTES = 16;

/**  WAVEFORM table "traceoff" column data object offset in collection stored by implementing class.
*/
    public static final int TRACEOFF = 17;

/**  WAVEFORM table "tracelen" column data object offset in collection stored by implementing class.
*/
    public static final int TRACELEN = 18;

/**  WAVEFORM table "status" column data object offset in collection stored by implementing class.
*/
    public static final int STATUS = 19;

/**  WAVEFORM table "wave_fmt" column data object offset in collection stored by implementing class.
*/
    public static final int WAVE_FMT = 20;

/**  WAVEFORM table "format_id" column data object offset in collection stored by implementing class.
*/
    public static final int FORMAT_ID = 21;

/**  WAVEFORM table "wordorder" column data object offset in collection stored by implementing class.
*/
    public static final int WORDORDER = 22;

/**  WAVEFORM table "recordsize" column data object offset in collection stored by implementing class.
*/
    public static final int RECORDSIZE = 23;

/**  WAVEFORM table "locevid" column data object offset in collection stored by implementing class.
*/
    public static final int LOCEVID = 24;

/**  WAVEFORM table "lddate" column data object offset in collection stored by implementing class.
*/
    public static final int LDDATE = 25;
/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =  // for db to UTC conversion -aww 2008/02/12
   "WFID,NET,STA,AUTH,SUBSOURCE,CHANNEL,CHANNELSRC,SEEDCHAN,LOCATION,ARCHIVE,truetime.getEpoch(DATETIME_ON,'UTC'),truetime.getEpoch(DATETIME_OFF,'UTC'),SAMPRATE,WAVETYPE,FILEID,FOFF,NBYTES,TRACEOFF,TRACELEN,STATUS,WAVE_FMT,FORMAT_ID,WORDORDER,RECORDSIZE,LOCEVID,LDDATE";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES =   // for db to UTC conversion -aww 2008/02/12
    "WAVEFORM.WFID,WAVEFORM.NET,WAVEFORM.STA,WAVEFORM.AUTH,WAVEFORM.SUBSOURCE,WAVEFORM.CHANNEL,WAVEFORM.CHANNELSRC,WAVEFORM.SEEDCHAN,WAVEFORM.LOCATION,WAVEFORM.ARCHIVE,truetime.getEpoch(WAVEFORM.DATETIME_ON,'UTC'),truetime.getEpoch(WAVEFORM.DATETIME_OFF,'UTC'),WAVEFORM.SAMPRATE,WAVEFORM.WAVETYPE,WAVEFORM.FILEID,WAVEFORM.FOFF,WAVEFORM.NBYTES,WAVEFORM.TRACEOFF,WAVEFORM.TRACELEN,WAVEFORM.STATUS,WAVEFORM.WAVE_FMT,WAVEFORM.FORMAT_ID,WAVEFORM.WORDORDER,WAVEFORM.RECORDSIZE,WAVEFORM.LOCEVID,WAVEFORM.LDDATE";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
	"WFID", "NET", "STA", "AUTH", "SUBSOURCE", 
	"CHANNEL", "CHANNELSRC", "SEEDCHAN", "LOCATION", "ARCHIVE", 
	"DATETIME_ON", "DATETIME_OFF", "SAMPRATE", "WAVETYPE", "FILEID", 
	"FOFF", "NBYTES", "TRACEOFF", "TRACELEN", "STATUS", 
	"WAVE_FMT", "FORMAT_ID", "WORDORDER", "RECORDSIZE", "LOCEVID", 
	"LDDATE"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
	false, false, false, false, false, 
	true, true, true, true, false, 
	true, true, true, true, false, 
	true, true, true, true, true, 
	true, true, true, true, true, 
	false
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
	DATALONG, DATASTRING, DATASTRING, DATASTRING, DATASTRING, 
	DATASTRING, DATASTRING, DATASTRING, DATASTRING, DATASTRING, 
	DATADOUBLE, DATADOUBLE, DATADOUBLE, DATASTRING, DATALONG, 
	DATALONG, DATALONG, DATALONG, DATALONG, DATASTRING, 
	DATALONG, DATALONG, DATALONG, DATALONG, DATASTRING, 
	DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {0};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 10, 8, 0, 0, 0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
	15, 8, 8, 15, 8, 8, 8, 8, 2, 8, 25, 25, 12, 2, 15, 10, 10, 10, 10, 2, 
	2, 2, 1, 5, 24, 7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
	null, null, null, null, null, null, null, null, null, null, 
	null, null, null, null, null, null, null, null, null, null, 
	null, null, null, null, null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))"
    };
}
