package org.trinet.jdbc.table;

import org.trinet.jdbc.datatypes.DataClassIds;

/** Interface of static data constants defining the named table.
* @see WaveformRequest
* @Deprecated
*/

/*
SQL> describe WAVEFORM_REQUEST ;
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 EVID                                               NUMBER(15)
 AUTH                                      NOT NULL VARCHAR2(15)
 SUBSOURCE                                 NOT NULL VARCHAR2(8)
 NET                                       NOT NULL VARCHAR2(8)
 STA                                       NOT NULL VARCHAR2(8)
 SEEDCHAN                                  NOT NULL VARCHAR2(8)
 STAAUTH                                   NOT NULL VARCHAR2(15)
 CHANNEL                                   NOT NULL VARCHAR2(8)
 DATETIME_ON                               NOT NULL NUMBER(25,10)
 DATETIME_OFF                              NOT NULL NUMBER(25,10)
 REQUEST_TYPE                              NOT NULL VARCHAR2(1)
 LDDATE                                             DATE
 DATA_OBTAINED                                      NUMBER(10,3)
 HOLD_WAVEID                                        NUMBER(15)
 RETRY                                              NUMBER(38)
 LASTRETRY                                          DATE


*/


public interface TableRowWaveformRequest extends DataClassIds {

/** Name of schema database table represented by this class.
*/
    public static final String DB_TABLE_NAME =  "WAVEFORM_REQUEST";

/** Number of column data fields in a table row.
*/
    public static final int MAX_FIELDS =  16;

/** Id sequence name for primary key column
*/
    public static String SEQUENCE_NAME = "";


/**  WAVEFORM_REQUEST table "evid" column data object offset in collection
stored by implementing class.  */
    public static final int EVID = 0;

/**  WAVEFORM_REQUEST table "auth" column data object offset in collection
stored by implementing class.  */
    public static final int AUTH = 1;

/**  WAVEFORM_REQUEST table "subsource" column data object offset in collection
stored by implementing class.  */
    public static final int SUBSOURCE = 2;

/**  WAVEFORM_REQUEST table "net" column data object offset in collection stored
by implementing class.  */
    public static final int NET = 3;

/**  WAVEFORM_REQUEST table "sta" column data object offset in collection stored
by implementing class.  */
    public static final int STA = 4;

/**  WAVEFORM_REQUEST table "seedchan" column data object offset in collection
stored by implementing class.  */
    public static final int SEEDCHAN = 5;

/**  WAVEFORM_REQUEST table "staauth" column data object offset in collection
stored by implementing class.  */
    public static final int STAAUTH = 6;

/**  WAVEFORM_REQUEST table "channel" column data object offset in collection
stored by implementing class.  */
    public static final int CHANNEL = 7;

/**  WAVEFORM_REQUEST table "datetime_on" column data object offset in
collection stored by implementing class.  */
    public static final int DATETIME_ON = 8;

/**  WAVEFORM_REQUEST table "datetime_off" column data object offset in
collection stored by implementing class.  */
    public static final int DATETIME_OFF = 9;

/**  WAVEFORM_REQUEST table "request_type" column data object offset in
collection stored by implementing class.  */
    public static final int REQUEST_TYPE = 10;

/**  WAVEFORM_REQUEST table "lddate" column data object offset in collection
stored by implementing class.  */
    public static final int LDDATE = 11;

/**  WAVEFORM_REQUEST table "data_obtained" column data object offset in
collection stored by implementing class.  */
    public static final int DATA_OBTAINED = 12;

/**  WAVEFORM_REQUEST table "hold_waveid" column data object offset in
collection stored by implementing class.  */
    public static final int HOLD_WAVEID = 13;

/**  WAVEFORM_REQUEST table "retry" column data object offset in collection
stored by implementing class.  */
    public static final int RETRY = 14;

/**  WAVEFORM_REQUEST table "lastretry" column data object offset in collection
stored by implementing class.  */
    public static final int LASTRETRY = 15;

/** String of know column names delimited by ",".
*/
    public static final String COLUMN_NAMES =  // for db to UTC conversion -aww 2008/02/12
   "EVID,AUTH,SUBSOURCE,NET,STA,SEEDCHAN,STAAUTH,CHANNEL,truetime.getEpoch(DATETIME_ON,'UTC'),truetime.getEpoch(DATETIME_OFF,'UTC'),REQUEST_TYPE,LDDATE,DATA_OBTAINED,HOLD_WAVEID,RETRY,LASTRETRY";

/** String of table qualified column names delimited by ",".
*/
    public static final String QUALIFIED_COLUMN_NAMES =   // for db to UTC conversion -aww 2008/02/12
    "WAVEFORM_REQUEST.EVID,WAVEFORM_REQUEST.AUTH,WAVEFORM_REQUEST.SUBSOURCE,WAVEFORM_REQUEST.NET,WAVEFORM_REQUEST.STA,WAVEFORM_REQUEST.SEEDCHAN,WAVEFORM_REQUEST.STAAUTH,WAVEFORM_REQUEST.CHANNEL,truetime.getEpoch(WAVEFORM_REQUEST.DATETIME_ON,'UTC'),truetime.getEpoch(WAVEFORM_REQUEST.DATETIME_OFF,'UTC'),WAVEFORM_REQUEST.REQUEST_TYPE,WAVEFORM_REQUEST.LDDATE,WAVEFORM_REQUEST.DATA_OBTAINED,WAVEFORM_REQUEST.HOLD_WAVEID,WAVEFORM_REQUEST.RETRY,WAVEFORM_REQUEST.LASTRETRY";

/**  Table column data field names.
*/
    public static final String [] FIELD_NAMES  = {
    "EVID", "AUTH", "SUBSOURCE", "NET", "STA", 
    "SEEDCHAN", "STAAUTH", "CHANNEL", "DATETIME_ON", "DATETIME_OFF", 
    "REQUEST_TYPE", "LDDATE", "DATA_OBTAINED", "HOLD_WAVEID", "RETRY", 
    "LASTRETRY"
    };

/** Nullable table column field.
*/
    public static final boolean [] FIELD_NULLS = {
    true, false, false, false, false, 
    false, false, false, false, false, 
    false, true, true, true, true, 
    true
    };

/**  Table column data field object class identifiers.
* @see org.trinet.jdbc.datatypes.DataClassIds
* @see org.trinet.jdbc.datatypes.DataClasses
*/
    public static final int [] FIELD_CLASS_IDS = {
    DATALONG, DATASTRING, DATASTRING, DATASTRING, DATASTRING, 
    DATASTRING, DATASTRING, DATASTRING, DATADOUBLE, DATADOUBLE, 
    DATASTRING, DATADATE, DATADOUBLE, DATALONG, DATALONG, 
    DATADATE
    };

/**  Column indices of primary key table columns.
*/
    public static final int [] KEY_COLUMNS = {1, 6, 4, 8, 2, 10, 3, 5, 9};

/**  Number of decimal fraction digits in table column data fields.
*/
    public static final int [] FIELD_DECIMAL_DIGITS = {
    0, 0, 0, 0, 0, 0, 0, 0, 10, 10, 0, 0, 3, 0, 0, 0
    };

/** Numeric sizes (width) of the table column data fields.
*/
    public static final int [] FIELD_SIZES = {
    15, 15, 8, 8, 8, 8, 15, 8, 25, 25, 1, 7, 10, 15, 38, 7
    };

/** Default table column field values.
*/
    public static final String [] FIELD_DEFAULTS = {
    null, null, null, null, null, null, null, null, null, null, 
    null, "(SYS_EXTRACT_UTC(CURRENT_TIMESTAMP))", null, null, null, null
    };
}
