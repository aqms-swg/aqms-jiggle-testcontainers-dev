package org.trinet.jiggle;

import java.awt.Color;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
import org.trinet.util.graphics.*;

/**
 * ActiveSolutionList.java
 * @author Doug Given
 */

/**
 * This is the Model for a MVC set that handles the SolutionList for Jiggle.
 * Observers are notified if the list is modified (solutions are added or 
 * removed. They are also notified if the Selected Solution changes.<p>
 * If you're making extensive changes to a SolutionList its better to get
 * the list with getList(), operate on it then put it back with setList().
 * Otherwise every incremental add() and remove() will notify observers.<p>
 * Observers' update() method should check the 'arg' value with instanceof
 * to see the change was to the selected event or the list.
 */

// Can't make this static !          ValueChangedEvent

public class ActiveSolutionList extends Observable {

    SolutionList solList = new SolutionList();

    public ActiveSolutionList() {

    }

    public ActiveSolutionList(SolutionList solList) {
        this.solList = solList;

        setColors();
    }

    /** Create a SolutionList based on this property description */
    public ActiveSolutionList (EventSelectionProperties properties) {
        this.solList = new SolutionList(properties);

        setColors();
    }

    /** This copies the SolutionList from this ActiveSolutionList to the
     * new ActiveSolutionList. It does not carry over observers. */
    public ActiveSolutionList(ActiveSolutionList model) {
        this.solList = new SolutionList(model.solList);
        // no mechanism for copying observers!

        setColors();
    }

    /** Set the SolutionList to be Observed */
    public void setList (SolutionList sl) {
        solList = sl;

        setColors();

        setChanged();
        notifyObservers(solList);
    }

    /** Return the SolutionList  */
    public SolutionList getList () {
        return solList;
    }

    /** Add one Solution to the SolutionList, notify observers of change */
    public void add (Solution sol) {
        solList.addByTime(sol);                // maintain time ordering

        setColor(sol);        // set uniqu color

        setChanged();
        notifyObservers(solList);
    }

    /** Delete one Solution from the SolutionList, notify observers of change */
    public boolean delete (Solution sol) {

        boolean status = sol.delete();

        setChanged();
        notifyObservers(solList);

        return status;
    }

    public Solution get(int idx) {
        return (Solution) solList.get(idx);
    }

    public Solution[] getArray() {
        return solList.getArray();
    }

    /** Remove one Solution from the SolutionList, notifiy observers of change
     * */
    public void remove (Solution sol) {

    // clear lists to list listeners are notified
     sol.phaseList.clear();
     sol.ampList.clear();
     sol.codaList.clear();

        solList.remove(sol);
        setChanged();
        notifyObservers(solList);
    }

    /** Return the size of the list
     */
    public int size() {
        return solList.size();
    }

// //////////////////////////////////////////////////////////////////////
// Single selected Solution
// //////////////////////////////////////////////////////////////////////

    /** Set the selected Solution */
    public boolean setSelected(Solution sol) {

        if (solList.setSelected(sol)) {

          setChanged();
          notifyObservers(sol);
       return true;
     } else {
       return false;
     }
    }

    /** Return the selected Solution */
    public Solution getSelected() {
        return (Solution) solList.getSelected();
    }

    /** Return the selected Solution's ID number */
    public long getSelectedId() {
        try {
            return ((Solution)solList.getSelected()).getId().longValue();
        } catch (NullPointerException ex) {
            return 0;
        }
    }

    public Solution getById (long id) {
        return solList.getById(id);
    }

    public String dumpToString() {

        Solution selected = getSelected();

        String str = "";
        Solution sol[] = this.getArray();
        if (sol != null) {
            for (int i = 0; i < sol.length; i++)
                {
                    str += sol[i].toSummaryString();
                    if (sol[i] == selected) {
                        str += " <= SELECTED\n";  // selected marker
                    } else {
                        str += "\n";
                    }
                }
        }
        return str;
    }

    /** hash table to keep track of the unique color of each solution in the list */
    Hashtable colorTable = new Hashtable(10);  // initial capacity = 10
    int maxColor = 0;

    /**
     * Return a unique color for this solution. The ActiveSolutionList keeps track
     * of a color for each solution in the list. Colors are assigned as solutions are
     * added to the list according t the scheme in org.trinet.util.ColorList.
     * The colors are used by GUI's to distingush between data like phases and amps
     * associated with the solutions
     */
    public Color getColorOf (Solution sol) {

     if (sol == null) return ColorList.UNASSOC;
        return (Color) colorTable.get(sol.id);              //sol.id is a DataObject
    }

    void setColor (Solution sol) {
        colorTable.put(sol.id, ColorList.getColor(maxColor++));
    }

    public void setColors () {

        Solution sol[] = solList.getArray();
        maxColor = 0; // reset it -aww 2008/08/01
        for (int i = 0; i < sol.length; i++) {

            setColor(sol[i]);
        }
    }
} // ActiveSolutionList

