// TODO: rework this class, buttons, listeners, and make use of JasiSolutionListPanel like Mung?
package org.trinet.jiggle;

import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jiggle.common.JiggleConstant;
import org.trinet.jiggle.common.JiggleSingleton;
import org.trinet.jiggle.common.LogUtil;
import org.trinet.jiggle.common.type.AssocEvent;
import org.trinet.jiggle.database.comment.RemarkDb;
import org.trinet.jiggle.database.event.EventAssoc;
import org.trinet.jiggle.database.event.EventAssocManager;
import org.trinet.jiggle.ui.dialog.event.EventAssociationDialog;
import org.trinet.jiggle.ui.panel.event.EventAssociation;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;
import org.trinet.util.gazetteer.TN.GazetteerType;
import org.trinet.util.graphics.*;
import org.trinet.util.graphics.table.*;
import org.trinet.util.graphics.text.*;

/**
 * Container for the Event catalog table that supports button actions on the selected table event.
 */
public class CatalogPanel extends JPanel implements Observer, TableModelListener {

    private static final int ID_COLUMN = 0;
    private static final ImageIcon LIGHT_BULB = new ImageIcon(IconImage.getImage("lightBulb.jpg"));
    private static final ImageIcon DARK_BULB = new ImageIcon(IconImage.getImage("darkBulb.jpg"));

    //private String [] columnOrderByName = CatalogTableConstants.columnNames;
    protected String [] columnOrderByName = {
            "EX_ID", "DATETIME", "OT", "LAT", "LON", "METHOD","Me", "Mw", "Ml", "Mlr","Md",
            "Ml-Md", "MAG", "MTYP", "MMETH", "MOBS", "MSTA", "MERR", "Z", "PM", "AUTH",
            "SRC", "GAP", "DIST", "RMS", "ERR_T", "ERR_H", "ERR_Z", "OBS", "USED",
            "AUSE", "S", "FM", "Q", "V", "B", "ETYPE", "GT", "ST", "ZF",
            "HF", "TF", "WRECS", "PR", "COMMENT", "OWHO", "MWHO", "CM", "VM",
            "VER", "LDDATE", "ASSOCIATION"
    };

    // "ID", "HDATUM", "VDATUM", "ERR_LAT", "ERR_LON", <--- COLUMNS NOT USED

    private Jiggle jiggle = null;
    private WhereIsEngine whereFrom = null;
    private SolutionList solutionList = null;

    private TablePanel tablePanel = null;
    private boolean tableModified = false;

    // Can't make it yet because properties aren't loaded yet
    private EventSelectionDialog eventSelectionDialog =  null;

    private final JLabel statusLabel = new JLabel(" ");

   /** JPanel containing action buttons below.*/
    private JPanel buttonPanel = null;

    private JButton fileButton = null;
    private JButton findButton = null;
    private JButton whereButton = null;
    private JButton loadButton = null;
    private JButton refreshButton = null;
    private JButton readButton = null;
    private JButton stateButton = null;
    private JButton filterButton = null;
    private JButton deleteButton = null;

    protected String catFileName = null;

    private ClipboardPopupMenu jpm = null;

    // Note Oracle drivers are CASE-SENSITIVE in statement below needs to be "call", not "CALL", stupid
    private static String csql = "{ ?=call WHERES.LOCALE(?,?) }";

    // Adapter must be added to the catalogTable.tableView  JTable in scollpane
    // for a right click there to execute a popup here.
    private MouseAdapter catalogRowHeadMouseAdapter = new MouseAdapter() {
        public void mouseReleased(MouseEvent e) {
            checkShowPopup(e);
        }
        private void checkShowPopup(MouseEvent e) {
            if ( e.isPopupTrigger() ) {
                //int row = tablePanel.catalogTable.getRowHeader().rowAtPoint(e.getPoint());
                //if (row >= 0) tablePanel.catalogTable.getRowHeader().setRowSelectionInterval(row, row);
                //jpm.show(e);
                createClipboardPopupMenu().show(e);
            }
        }
        public void mousePressed(MouseEvent e) {
            int mods = e.getModifiers();
            if ( (mods == MouseEvent.BUTTON1_MASK) && e.getClickCount() > 1) {
              int rowid = tablePanel.catalogTable.getRowHeader().getSelectedRow();
              if (rowid >= 0  && jiggle != null) {
                Solution sol = ((Solution)solutionList.get(tablePanel.catalogTable.getSortedRowModelIndex(rowid)));
                if (jiggle.isNetworkModeLAN()) { // DISABLE FOR WAN
                    if (jiggle.validateConnection("Load catalog solution")) { // ??
                      Solution newsol = (Solution) sol.getById(sol.getId()); // now refresh with latest db version - aww 05/03/2007
                      if (newsol != null) {
                        solutionList.addOrReplaceById(newsol); // then update Catalog list - aww 05/03/2007
                        sol = newsol; // use newsol if valid -aww 2011/06/06
                      }
                    }
                }
                // if getById has IOException like Broken pipe, the returned newsol is null, so here reuse old one - aww 06/06/2011
                if (sol != null) {
                    jiggle.loadSolution(sol.id.longValue(), false);
                }
              }
            }
            else checkShowPopup(e);
       }
    };

    ListDataStateAdapter myLDSL = new ListDataStateAdapter() {
        public void stateChanged(ListDataStateEvent e) {
            if (e.getSource() == getSolutionList()) {
                if (e.getStateChange().getStateName().equals("selected") ) {
                    int irow = tablePanel.catalogTable.modelToSortedRowIndex( getSolutionList().getSelectedIndex() );
                    //System.out.println("DEBUG CatalogPanel solution list selected index: " + getSolutionList().getSelectedIndex() );
                    //System.out.println("DEBUG CatalogPanel table row for solution list selection: " + irow);
                    JTable jtable = tablePanel.catalogTable.getRowHeader();
                    // Added row identity test - aww 05/04/2007
                    if (jtable != null && jtable.getSelectedRow() != irow) {
                        jtable.changeSelection(irow, 0, false, false);
                    }
                }
            }
        }
        /*
        public void intervalRemoved(ListDataStateEvent e) {
            doStatusUpdate();
        }
        public void intervalAdded(ListDataStateEvent e) {
            doStatusUpdate();
        }
        public void contentsChanged(ListDataStateEvent e) {
            doStatusUpdate();
        }
        */
    };

/**
* No-op Constructor state unintialized
*/
    protected CatalogPanel () {}

/**
 * Constructor for default empty Catalog SolutionList == null
*/
    public CatalogPanel(Jiggle jiggle, SolutionList solutionList) {
        this(jiggle, null, solutionList);
    }
/**
 * Constructor for default empty Catalog SolutionList == null
*/
    public CatalogPanel(Jiggle jiggle, String [] tableColumnNames) {
        this(jiggle, tableColumnNames, null);
    }

/** Constructor uses default Catalog columns
* @param solutionList org.trinet.jasi.SolutionList can be null.
*/
    public CatalogPanel(Jiggle jiggle, String[] tableColumnNames, SolutionList solutionList) {
        setJiggle(jiggle);
        if (tableColumnNames != null) setColumnOrder(tableColumnNames);
        initSolutionList(solutionList);
        initPanel();   // Need to rework via manangingFocus()?
    }

    public void setJiggle(Jiggle jiggle) {
        this.jiggle = jiggle;
        setWhereIsEngine(jiggle.getWhereIsEngine());
        if (tablePanel != null && tablePanel.catalogTable != null)
            tablePanel.catalogTable.setProperties(jiggle.getProperties().getCatalogProperties());
        if (isShowing()) repaint();
    }

    /** Set the column order */
    public void setColumnOrder(String [] order) {
        columnOrderByName = order;
    }
 
    private void setEventSelectionProps(String filename) {
      System.out.println("Setting eventSelectionProps property value to: " + filename);
      Jiggle.getProperties().setEventSelectionProps(filename);
      jiggle.saveProperties();
    }

    public void setWhereIsEngine(WhereIsEngine eng) {
        whereFrom = eng;
    }

    protected void initSolutionList(SolutionList list) {
        if (solutionList != null) {
            solutionList.removeListDataStateListener(myLDSL);
        }
        solutionList = (SolutionList) list ;
        if (solutionList != null) {
            solutionList.addListDataStateListener(myLDSL);
        }
    }

    public void destroy() {
        if (solutionList != null) {
            solutionList.removeListDataStateListener(myLDSL);
        }
    }

/** Sets the event solution list and creates new table.  */
    public void setSolutionList(SolutionList list) {
        initSolutionList(list);
        //tablePanel.catalogTable.getModel().setList(list);
        tablePanel.startTableThread();
    }

    public SolutionList getSolutionList() {
        //return tablePanel.catalogTable.getModel().getList();
        return solutionList;
    }

    public Solution getNextSortedSolution(Solution currentSol) {
        return tablePanel.catalogTable.getNextSortedSolution(currentSol);
    }

    public int getSortedTableRowIndex(Solution currentSol) {
        return tablePanel.catalogTable.getSortedTableRowIndex(currentSol);
    }

    public boolean hasNullSolutionList() {
        return (solutionList == null);
    }

    private String getSolutionIdsFileName(String oldFileName) {

        JFileChooser jfc = new JFileChooser(oldFileName);

        File file = null;
        if (oldFileName != null) file = new File(oldFileName);
        if (file != null) jfc.setSelectedFile(file);

        String filename = null;
        if (jfc.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            try {
              file =jfc.getSelectedFile();
              if (file != null) {
                  filename = file.getCanonicalPath();
                  if (file.exists()) {
                      return filename;
                  }
              }
              else {
                InfoDialog.informUser(getTopLevelAncestor(), "INFO", "Selected ids file D.N.E! : " + filename, statusLabel);
              }
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
            catch (SecurityException ex) {
                ex.printStackTrace();
            }
        }
        return filename; // it's null here
    }


    private void initPanel() {
        setLayout(new BorderLayout());

        tablePanel = new TablePanel();
        add(tablePanel, BorderLayout.CENTER);

        createButtonPanel();
        if (getButtonPanel() != null) {
            tablePanel.add(getButtonPanel(), BorderLayout.NORTH);
        }
        //createClipboardPopupMenu();

        createStatusPanel();

        ActionMap actionMap = new ActionMap();
        actionMap.setParent(getActionMap());
        actionMap.put("TABLOC", new JumpTabAction(Jiggle.TAB_LOCATION));
        actionMap.put("TABMAG", new JumpTabAction(Jiggle.TAB_MAGNITUDE));
        actionMap.put("TABMSG", new JumpTabAction(Jiggle.TAB_MESSAGE));
        actionMap.put("TABWAV", new JumpTabAction(Jiggle.TAB_WAVEFORM));
        setActionMap(actionMap);

        InputMap inputMap = new InputMap();
        inputMap.setParent(getInputMap());
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_L, 0), "TABLOC");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_L, Event.SHIFT_MASK), "TABLOC");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_M, 0), "TABMAG");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_M, Event.SHIFT_MASK), "TABMAG");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_T, 0), "TABMSG");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_T, Event.SHIFT_MASK), "TABMSG");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_W, 0), "TABWAV");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_W, Event.SHIFT_MASK), "TABWAV");
        setInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT, inputMap);

        //ComponentInputMap compInputMap = new ComponentInputMap(this);
        //KeyStroke [] keys = inputMap.keys();
        //for (int idx = 0; idx < keys.length; idx++) {
        //    compInputMap.put(keys[idx], inputMap.get(keys[idx]));
        //}
        //this.setInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW,compInputMap);
        //
        //Debug.println("CatalogPanel layout prefSize: " + ((BorderLayout) getLayout()).preferredLayoutSize(this));

        if (solutionList != null) tablePanel.startTableThread();
    }

    private void createStatusPanel() {
        statusLabel.setForeground(Color.red);
        statusLabel.setHorizontalAlignment(JLabel.LEFT);
        JPanel statusPanel = new JPanel();
        statusPanel.setBorder(BorderFactory.createEtchedBorder());
        statusPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        statusPanel.add(statusLabel);
        add(statusPanel, BorderLayout.SOUTH);
    }

    /** Return the JPanel containing the action button. Can be used to add more buttons. */
    protected JPanel getButtonPanel() {
        return buttonPanel;
    }

    private JButton makeButton(String labelStr, String imageStr) {
        Image image = IconImage.getImage(imageStr);
        JButton btn = null;
        if (image == null) {       // handle situation when .gif file can't be found
            btn = new JButton(labelStr);
        } else {
            btn = new JButton(new ImageIcon(image));
        }
        btn.setActionCommand(labelStr);
        int xy = (jiggle.getProperties().getBoolean("miniToolBarButtons")) ? 24 : 32;
        btn.setPreferredSize(new Dimension(xy,xy));
        btn.setMaximumSize(new Dimension(xy,xy));
        return btn;
    }

    private void createButtonPanel() {

        buttonPanel = new JPanel();

        FlowLayout flowLayout = (FlowLayout) buttonPanel.getLayout();
        flowLayout.setAlignment(FlowLayout.CENTER);
        flowLayout.setHgap(0);

        findButton = makeButton("Find", "mini-view.gif");
        findButton.addActionListener (new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                long input = (solutionList == null) ? -1l : solutionList.getSelectedId();
                NumberTextField ntf = NumberTextFieldFactory.createInputField(12, true);
                try {
                    ntf.setValue(input);
                    ntf.selectAll();
                    int ans = JOptionPane.showConfirmDialog(getTopLevelAncestor(),
                        ntf, "Enter id to find in catalog ", JOptionPane.YES_NO_OPTION);
                    if (ans == JOptionPane.YES_OPTION) {
                       input = ntf.getLongValue();
                       boolean tf = solutionList.setSelected(input);
                       if (tf) CatalogPanel.this.updateTableRowSelection();
                       else JOptionPane.showMessageDialog(getTopLevelAncestor(), "Input id:  " +input+ "  not found in catalog list.");
                    }
                }
                catch (IllegalArgumentException ex) {
                       JOptionPane.showMessageDialog(getTopLevelAncestor(), "Input id:  " +input+ "  must be numeric.");
                }
            }
        });
        findButton.setToolTipText("Find an event id in catalog list.");
        findButton.setEnabled(false);
        buttonPanel.add(findButton);

        fileButton = makeButton("File","file.gif");
        fileButton.addActionListener(new FileButtonHandler());
        fileButton.setToolTipText("Save contents of table to ASCII output file");
        fileButton.setEnabled(false);
        buttonPanel.add(fileButton);

        whereButton = makeButton("Where", "questionmark.gif");
        whereButton.addActionListener(new WhereButtonHandler());
        whereButton.setToolTipText("Show gazetteer info for first selected event row");
        whereButton.setEnabled(false);
        buttonPanel.add(whereButton);

        deleteButton = makeButton("Delete", "delete_xbold_red.gif");
        deleteButton.addActionListener (new DeleteButtonHandler() );
        deleteButton.setToolTipText("Delete selected event(s) from data source");
        deleteButton.setEnabled(false);
        buttonPanel.add(deleteButton);

        refreshButton = makeButton("Refresh", "refresh.gif");
        refreshButton.addActionListener (new RefreshButtonHandler() );
        refreshButton.setToolTipText("Refresh the event catalog");
        buttonPanel.add(refreshButton);

        readButton = makeButton("Read", "read.gif");
        readButton.addActionListener (new ReadButtonHandler() );
        readButton.setToolTipText("Import events whose ids are listed in ASCII input file");
        buttonPanel.add(readButton);

        // String pcsState = jiggle.getProperties().getProperty("catalog.pcsState");
        //if (pcsState != null) {
          stateButton = makeButton("State", "dot_red.gif");
          stateButton.addActionListener (new StateButtonHandler() );
          stateButton.setToolTipText("Load event ids found in PCS_STATE table for a dialog specified state" );
          buttonPanel.add(stateButton);
        //}

        filterButton = makeButton("Filter", "filter_blue.gif");
        filterButton.addActionListener (new FilterButtonHandler() );
        filterButton.setToolTipText("Define event selection criteria");
        buttonPanel.add(filterButton);

        loadButton = makeButton("Load", "inputarrowsmall.gif");
        loadButton.addActionListener (new LoadButtonHandler() );
        loadButton.setToolTipText("Load selected event's data and timeseries into viewer");
        loadButton.setEnabled(false);
        buttonPanel.add(loadButton);

        buttonPanel.setMinimumSize(new Dimension(25,25));
        buttonPanel.setMinimumSize(new Dimension(25,25));

    }

    private void enableCatalogButtons() {
        deleteButton.setEnabled(true);
        fileButton.setEnabled(true);
        loadButton.setEnabled(true);
        findButton.setEnabled(true);
        whereButton.setEnabled(true);
    }

    private ClipboardPopupMenu createClipboardPopupMenu() {
        jpm = new ClipboardPopupMenu();
        jpm.pack();
        return jpm;
    }

    public void updateTable() {
        if (tablePanel != null) tablePanel.updateTable();
    }

    public String [] getTableColumnHeaders() {
        if (tablePanel == null || tablePanel.catalogTable == null) return null;
        return tablePanel.catalogTable.getTableColumnHeaders();
    }


/** Required method to implement TableModelListener interface */
    public void tableChanged(TableModelEvent event) {
        updateTableRowSelection();
        tableModified = true;
        doStatusUpdate();
    }

    private void doStatusUpdate() {
        if (solutionList != null) statusLabel.setText("Table has " + getSolutionList().size() + " rows");
    }

    public void updateTableRowSelection() {
        SwingUtilities.invokeLater(
            new Runnable() {
                public void run() {
                    JTable table = tablePanel.catalogTable.getTable();
                    if (table == null) return;

                    // make sure selected sol is selected in JTable
                    int selRow = solutionList.getSelectedIndex();
                    if (selRow < 0) return;

                    int sortRow = tablePanel.catalogTable.modelToSortedRowIndex(selRow);
                    //System.out.println("DEBUG CatalogPanel updateTableRowSelection to index: " + selRow + " sortRow: " + sortRow);
                    table.setRowSelectionInterval(sortRow, sortRow);
                    //table.scrollRectToVisible(table.getCellRect(sortRow, 0, true)); // 01/29/2007 -aww try below
                    Rectangle r = table.getCellRect(sortRow, 0, true);
                    int h = table.getRowHeight();
                    r.y -= h*5; // go back up 5 rows
                    if (r.y < 0) r.y = 0; // if not possible default to top
                    r.height += h*50; // make make rect at least 50 rows wide
                    table.scrollRectToVisible(r);
                    if (tablePanel != null && tablePanel.spane != null)
                        tablePanel.spane.repaint(); // perhaps dirty region from popup in row header
               }
            }
        );
    }

    public void update() { // added to force kludge after solved if aliased list, else not needed aww 11/03
//        System.err.println("DEBUG CatalogPanel update fire");
        tablePanel.catalogTable.getModel().fireTableDataChanged();
    }

/** Required method to implement the observer interface.
*/
    public void update(Observable observable, Object arg) {
        //System.out.println("DEBUG CatalogPanel update() : Observable = " + observable.getClass().getName());
        //System.out.println("DEBUG CatalogPanel update() : Argument   = " + arg.getClass().getName());
        //System.out.println("DEBUG CatalogPanel update() : fireTableDataChanged()");
        if (org.trinet.jasi.SolutionList.class.isInstance(arg)) {
                setSolutionList((SolutionList) arg);
        }
// note the the catalog table data model is aliased solutionList reference in the class's constructor.
//        tablePanel.catalogTable.getModel().fireTableDataChanged();
    }
/**
 * Returns Solution array containing selected rows in table row header.
*/
    public Solution [] getSelectedSolutions() {
        if (tablePanel.catalogTable.getRowHeader() == null) return null;
        int selected [] = tablePanel.catalogTable.getRowHeader().getSelectedRows();
        int nselected = selected.length;
        if (nselected == 0) return null;
        Solution [] solutions = new Solution [nselected];

        SolutionList solutionList = tablePanel.catalogTable.getModel().getList();
        Solution [] master = solutionList.getArray();
        int masterLength = master.length;

        if (nselected > masterLength)
                 throw new ArrayIndexOutOfBoundsException("CatalogPanel: getSelectedSolutions() SelectedRows>SolutionList.size().");

        for (int index = 0; index < nselected; index++) {
            solutions[index] = master[tablePanel.catalogTable.getSortedRowModelIndex(selected[index])];
        }
        return solutions;
    }

/** Returns long array containing selected event ids from table row header.
*/
    public long [] getSelectedIds() {
        if (tablePanel.catalogTable.getRowHeader() == null) return null;
        int rowIds[] = tablePanel.catalogTable.getRowHeader().getSelectedRows();
        int nrows = rowIds.length;
        if (nrows == 0) return null;
        long [] ids = new long [nrows];
        CatalogTableModel ctm = tablePanel.catalogTable.getModel();
        for (int index = 0; index < nrows; index++) {
            ids[index] =
                ((DataObject) ctm.getValueAt(tablePanel.catalogTable.getSortedRowModelIndex(rowIds[index]), ID_COLUMN)).longValue();
        }
        return ids;
    }

    private int getSelectedRowCount() {
        return (tablePanel.catalogTable.getRowHeader() == null) ? 0 : tablePanel.catalogTable.getRowHeader().getSelectedRows().length;
    }

    public boolean hasModifiedTable() {
        return tableModified;
    }

    public boolean hasUnmodifiedTable() {
        return ! tableModified;
    }

    public void unsetTableModified() {
        tableModified = false;
    }

/** Returns true is table model data exists. */
    public boolean hasValidTable() {
        return tablePanel.hasValidTable();
    }
    public boolean hasInvalidTable() {
        return ! hasValidTable();
    }

    class JumpTabAction extends AbstractAction { // implements ActionListener {
      int tab = 0;
      public JumpTabAction(int tab) {
          this.tab = tab;
      }
      public void actionPerformed(ActionEvent evt) {
          SwingUtilities.invokeLater(
              new Runnable() {
                  public void run() {
                      if (jiggle != null) jiggle.selectTab(tab);
                  }
              }
          );
      }
    }

    /** Lookup gazetteer info about location of selected event.*/
    private class WhereButtonHandler implements ActionListener {
        public void actionPerformed(ActionEvent evt) {
            int rowid = tablePanel.catalogTable.getRowHeader().getSelectedRow();
            String head = "Where";
            String whereString = "No event selected!";
            if (rowid >= 0) {
                rowid = tablePanel.catalogTable.getSortedRowModelIndex(rowid);
                CatalogTableModel ctm = tablePanel.catalogTable.getModel();
                double lat = ((DataObject) ctm.getValueAt(rowid, ctm.findColumn("LAT"))).doubleValue();
                double lon = ((DataObject) ctm.getValueAt(rowid, ctm.findColumn("LON"))).doubleValue();
                long id =    ((DataObject) ctm.getValueAt(rowid, ctm.findColumn("ID"))).longValue();

                Format f74 = new Format("%7.4f");
                head = "Event "+id+" lat,lon: "+f74.form(lat)+", "+f74.form(lon);

                Connection conn = solutionList.getConnection();
                if (conn != null) {

                    if (whereFrom == null) {
                        //whereFrom = WhereIsEngine.create();
                        System.err.println("WhereIsEngine null in CatalogPanel; invoking application must set engine first!");
                        System.err.println("See setWhereIsEngine(WhereIsEngine eng) method.");
                    }
                    else {
                        whereString = whereFrom.where(lat, lon);
                        System.out.println(head + "\n" + whereString);
                    }
                    /*
                    CallableStatement cs = null;
                    try {
                      cs = conn.prepareCall(csql);
                      cs.registerOutParameter(1, java.sql.Types.VARCHAR);
                      cs.setDouble(2, lat);
                      cs.setDouble(3, lon);
                      cs.executeUpdate();
                      whereString = cs.getString(1);
                    }
                    catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                    finally {
                      try {
                        if (cs != null) cs.close();
                      }
                      catch (SQLException ex2) {}
                    }
                    */
                }
            }
            JTextArea jta = new JTextArea(whereString);
            jta.setBackground(CatalogPanel.this.getBackground());
            jta.setEditable(false);
            JOptionPane.showMessageDialog(getTopLevelAncestor(), jta, head, JOptionPane.PLAIN_MESSAGE);
        }
    }

    /** Save catalog data to file */
    private class FileButtonHandler implements  ActionListener {
        public void actionPerformed(ActionEvent evt) {
            statusLabel.setText(" ");
            if (tablePanel.catalogTable.getModel().getRowCount() > 0 ) {
                SaveCatalogTableToASCII saveToTextFile = new SaveCatalogTableToASCII();
                saveToTextFile.save (tablePanel.catalogTable, (Frame) getTopLevelAncestor());
            }
            else {
                InfoDialog.informUser(getTopLevelAncestor(), "ERROR", "No rows in table - execute new request.", statusLabel);
            }
        }
    }

    /** Set event selection criteria */
    private class FilterButtonHandler implements ActionListener {

        public void actionPerformed (ActionEvent evt) {

            String [] options = {"Select", "Edit", "Save", "Cancel"};

            int opt  = JOptionPane.showOptionDialog( getTopLevelAncestor(),
                    "Select new property file or edit, or save existing properties?",
                    "EventSelectionProperties", JOptionPane.DEFAULT_OPTION,
                    JOptionPane.QUESTION_MESSAGE, null, options, options[1]);

            if (options[opt].equals("Cancel") || opt == JOptionPane.CLOSED_OPTION ) return; // bail

            doFilterOption(options[opt], jiggle.getEventProperties());
        }
    }

    protected void doFilterOption(String option, final EventSelectionProperties eventProps) {

            if (option.equals("Select")) { // Select a file from which to read properties
                File file = new File(eventProps.getUserPropertiesFileName());
                System.out.println("DEBUG CatalogPanel Select event props file by name: " + eventProps.getUserPropertiesFileName());
                JFileChooser jfc = new JFileChooser(file);
                //jfc.setFileFilter(new MyFileFilter());
                //jfc.setFileSelectionMode(JFileChooser.FILES_ONLY); // forces save to be inside jiggle user dir -aww 2008/06/24
                jfc.setSelectedFile(file);

                if (jfc.showOpenDialog(jiggle) == JFileChooser.APPROVE_OPTION) {

                    file = jfc.getSelectedFile();
                    String pathname = file.getAbsolutePath();
                    String filename = file.getName();
                    //System.out.println("file:" + file.toString());
                    //System.out.println("path:" + pathname);
                    //System.out.println("filename:" + filename);

                    if (! file.exists() ) {
                      JOptionPane.showMessageDialog(getTopLevelAncestor(), pathname + " file D.N.E. !",
                           "Load Event Selection Properties", JOptionPane.ERROR_MESSAGE);
                     return;
                    }


                    // ask here to change prop in jiggle properties
                    int answer = JOptionPane.showConfirmDialog(getTopLevelAncestor(),
                        "Set selected filename as new eventSelectionProps value in " +
                        jiggle.getProperties().getFilename() + " ?\n (NO = just load it for now)",
                        "Change Event Selection Property", JOptionPane.YES_NO_OPTION);

                    boolean changeJiggleProp = (answer == JOptionPane.YES_OPTION);

                    if (changeJiggleProp) {
                      if ( jfc.getCurrentDirectory().getAbsolutePath().equals( eventProps.getUserFilePath() ) ) {
                        setEventSelectionProps(filename);
                      }
                      else { // save outside startup jiggle user dir
                        JOptionPane.showMessageDialog(getTopLevelAncestor(),
                           "Save directory is NOT the same as startup JIGGLE_USER_DIR",
                           "Changing eventSelectionProps", JOptionPane.WARNING_MESSAGE);
                        setEventSelectionProps(pathname);
                      }
                    }

                    //jiggle.setEventProperties(pathname); // doesn't work, method concatenates JIGGLE_USER_HOMEDIR and pathname
                    //jiggle.setEventProperties(filename); // works only if filename is inside JIGGLE_USER_HOMEDIR
                    EventSelectionProperties newEventProps = new EventSelectionProperties(pathname, eventProps.getDefaultPropertiesFileName());
                    newEventProps.setFiletype(eventProps.getFiletype());
                    jiggle.setEventProperties(newEventProps);

                    if (checkEventProperties(jiggle.getEventProperties())) {
                        catFileName = null; // after editing file list not desired ? (REFRESH does this after prompt) - aww 2009/08/04
                        jiggle.catSolList = null;
                        jiggle.resetCatPanel(true); // repopulate the catalog table
                    }
                    else doFilterOption("Edit", jiggle.getEventProperties());
                }
            }
            else if (option.equals("Edit")) {// Edit existing selection properties
                // modal dialog - force recreate since Map or other sources could update jiggle properties -aww 2008/04/25
                if (eventSelectionDialog != null) eventSelectionDialog.dispose(); // trash the existing one -aww 2008/04/25

                final org.trinet.util.SwingWorker worker = new org.trinet.util.SwingWorker() {
                    WorkerStatusDialog workStatus = new WorkerStatusDialog(jiggle, true);
                    PreferencesDialog prefDialog = null;
                    boolean status = false;
                    public Object construct() {
                      workStatus.setBeep(jiggle.beep);
                      workStatus.pop("EventSelectionDialog", "Configuring property tab panes...", true);
                      eventSelectionDialog = new EventSelectionDialog((JFrame)jiggle, "Event Selection", true, eventProps);
                      return prefDialog;
                    }

                    public void finished() { //Runs on the event-dispatching thread graphics calls OK.
                      setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR)); // set back to normal
                      workStatus.dispose(); // don't invoke unpop before dispose (may cause exception)
                      eventSelectionDialog.setVisible(true); // show the dialog
                      jiggle.statusPanel.clearOnlyOnMatch("Edit event properties...");
                      processEventSelectionDialog();
                    }

                    // if OK button was hit set Jiggle properties to the new selection properties
                    private void processEventSelectionDialog() {
                      if (eventSelectionDialog.getButtonStatus() == JOptionPane.OK_OPTION) {
                        EventSelectionProperties newEventProps = eventSelectionDialog.getEventSelectionProperties();
                        if (checkEventProperties(newEventProps)) {
                           jiggle.setEventProperties(newEventProps);
                           if (jiggle.getVerbose()) {
                             System.out.println("EventSelectionProperties after edit:\n"+jiggle.getEventProperties().listToString());
                           }
                            int answer = JOptionPane.showConfirmDialog( getTopLevelAncestor(),
                                            "Save your edits to an event selection properties file?",
                                            //+ "\n" + jiggle.getEventProperties().getUserPropertiesFileName(), // remove -aww
                                            "Save Event Selection Properties Edits", JOptionPane.YES_NO_OPTION
                                         );
                            if (answer == JOptionPane.YES_OPTION) {
                                //jiggle.getEventProperties().saveProperties(); // replaced by below, to allow user to redirect to new file
                                doFilterOption("Save", newEventProps); // added 2010/09/29 -aww
                            }
                            catFileName = null; // after editing file list not desired ? (REFRESH does this after prompt) - aww 2009/08/04
                            jiggle.catSolList = null;
                            jiggle.resetCatPanel(true); // repopulate the catalog table
                        }
                        else doFilterOption("Edit", newEventProps);
                      }
                    }
                };
                jiggle.initStatusPanelForWork("Edit event properties...");
                worker.start();  //required for SwingWorker 3
            }
            else if (option.equals("Save")) {// Save existing selection properties
                File file = new File(jiggle.getEventProperties().getUserPropertiesFileName());
                JFileChooser jfc = new JFileChooser(file);
                //jfc.setFileFilter(new MyFileFilter());
                //jfc.setFileSelectionMode(JFileChooser.FILES_ONLY); // forces save to be inside jiggle user dir -aww 2008/06/24
                jfc.setSelectedFile(file);

                if (jfc.showSaveDialog(jiggle) == JFileChooser.APPROVE_OPTION) {
                  file = jfc.getSelectedFile();
                  String pathname = file.getAbsolutePath();
                  String filename = file.getName();

                  /* see if file already exists, if so rename it before save
                  if (file.exists()) {
                    file.renameTo(pathname+EpochTime.epochToString(System.currentTimeMillis()/1000.,"yyyy-MM-dd_HHmmss"));
                  }
                  */

                  System.out.println("Jiggle saving event properties to file: " + pathname);
                  jiggle.getEventProperties().setUserPropertiesFileName(pathname);
                  jiggle.getEventProperties().saveProperties();

                  int answer = JOptionPane.showConfirmDialog(getTopLevelAncestor(),
                        "Set selected filename as new eventSelectionProps value in jiggle.props?",
                        "Change Event Selection Property", JOptionPane.YES_NO_OPTION);

                  boolean changeJiggleProp = (answer == JOptionPane.YES_OPTION);

                  if (changeJiggleProp) {
                    if ( jfc.getCurrentDirectory().getAbsolutePath().equals( jiggle.getEventProperties().getUserFilePath()) ) {
                      setEventSelectionProps(filename);
                    }
                    else { // save outside startup jiggle user dir
                      JOptionPane.showMessageDialog(getTopLevelAncestor(),
                           "Save directory is NOT the same as startup JIGGLE_USER_DIR",
                           "Changing eventSelectionProps", JOptionPane.WARNING_MESSAGE);
                      setEventSelectionProps(pathname);
                    }
                  }
                }
            }


        }

        private boolean checkEventProperties(EventSelectionProperties eventProps) {
            // Check event selection properties has specified a valid time span
            if (! eventProps.hasValidTimeSpan()) {
                JOptionPane.showMessageDialog( getTopLevelAncestor(),
                   "WARNING: Event selection properties specifies an invalid time span!\n"+
                   "You must reset you event selection time properties!",
                   "Invalid Catalog Time Span", JOptionPane.PLAIN_MESSAGE);
                return false;
            }

            // Check total days in TimeSpan requested
            int days = (int) (eventProps.getTimeSpan().getDuration()/ 86400. + 0.5);
            if (days > 31) {
                int yn = JOptionPane.showConfirmDialog( getTopLevelAncestor(),
                   "WARNING: event selection time range spans: "+days+" days.\n"+
                   "Do you want to reset the event time range properties?",
                   "Large Catalog Time Span", JOptionPane.YES_NO_OPTION
                );
                if (yn == JOptionPane.YES_OPTION) return false;
            }
            return true;
        }

    private class MyFileFilter extends javax.swing.filechooser.FileFilter {

        public String getDescription() {
            return ".props";
        }

        public boolean accept(File f) {
            if (f.isDirectory()) {
                return true;
            }
            String str = f.getName();
            int idx = str.lastIndexOf('.');
            String ext = null;
            if (idx > 0 &&  idx < str.length() - 1) {
                ext = str.substring(idx+1).toLowerCase();
            }
            if (ext != null) {
                if (ext.equalsIgnoreCase("props")) return true;
            }
            return false;
        }
    }


    /** Load a selected event */
    private class LoadButtonHandler implements ActionListener {
        public void actionPerformed (ActionEvent evt) {
            //String action =  evt.getActionCommand();
            Solution[] sol = getSelectedSolutions();
            // TODO: handle multiple selected events
            if (sol != null && sol.length > 0 ) {
                Solution aSol = sol[0];
                if (jiggle.isNetworkModeLAN()) { // DISABLE FOR WAN ?
                    if (jiggle.validateConnection("Load catalog solution")) { // ??
                      Solution newsol = (Solution) sol[0].getById(sol[0].getId()); // now refresh with latest db version - aww 05/03/2007
                      if (newsol != null) {
                        solutionList.addOrReplaceById(newsol); // then update Catalog list - aww 05/03/2007
                        aSol = newsol; // use newsol if valid -aww 2011/06/06
                      }
                    }
                }
                jiggle.loadSolution(aSol.id.longValue(), false);
            }
            else {
                JOptionPane.showMessageDialog( getTopLevelAncestor(),
                    "No CATALOG event id selected, must select an id to load",
                    "Load Waveforms", JOptionPane.ERROR_MESSAGE
                );
            }
        }
    }

    /** Delete a selected event */
    private class DeleteButtonHandler implements ActionListener {
        public void actionPerformed (ActionEvent evt) {
            Solution sol[] = getSelectedSolutions();
            if (sol != null && sol.length > 0) {
                // Like JiggleTooBar delete by id # and NOT by sol object
                // because Catalog and MasterView have different solution lists.
                //boolean status = jiggle.deleteSolution(sol[0].id.longValue());
                // Instead delete the selection in CatalogList from database
                for (int ii = 0; ii < sol.length; ii++) {
                    if (jiggle.deleteSolution(sol[ii], false)) {
                        // Below done by jiggle.deleteSolution above ?
                        //Solution nextSol = getNextSortedSolution(sol[ii]); // added 2008/04/16 -aww
                        //solutionList.remove(sol[ii]);
                        //solutionList.setSelected(nextSol); // added 2008/04/16 -aww
                    }
                }
                // refresh the DataSource Catalog snapshot to show change
                //jiggle.resetCatPanel(); // aww 05/10/2007
            }
            else
                JOptionPane.showMessageDialog(getTopLevelAncestor(),
                    "First select the event id(s) to copy",
                    "Copy id(s)", JOptionPane.ERROR_MESSAGE
                );
        }
    }

    /** Refresh the catalog view */
    private class RefreshButtonHandler implements ActionListener {
        public void actionPerformed (ActionEvent evt) {
            // show latest snapshot of the DataSource catalog
            if (catFileName != null) {
              int ans = JOptionPane.showConfirmDialog(getTopLevelAncestor(),
                        "Load ids from input file:\n"+catFileName, "Catalog Refresh", JOptionPane.YES_NO_OPTION);
              if (ans == JOptionPane.YES_OPTION) {
                  jiggle.resetCatPanel(false, catFileName);
                  return;
              }
              catFileName = null; // reset filename
            }
            // do event selection properties (time range relative or absolute)
            jiggle.catSolList = null;
            jiggle.resetCatPanel(true);
        }
    }

    /** Load ids from PCS_STATE table into the catalog view */
    private class StateButtonHandler implements ActionListener {
        public void actionPerformed (ActionEvent evt) {
            String pcsState =  jiggle.getProperties().getProperty("catalog.pcsState","");
            String state = JOptionPane.showInputDialog(getTopLevelAncestor(), "PCS_STATE table state to load", pcsState);
            if (state != null && state.length() > 0 ) { //  && !state.equals(pcsState)) {
                jiggle.getProperties().setProperty("catalog.pcsState", state);
                jiggle.resetCatPanel(false, "STATE");
            }
            else {
                jiggle.getProperties().setProperty("catalog.pcsState", "");
                jiggle.catPane.catFileName=null;
                jiggle.resetCatPanel(true, null);
            }
        }
    }

    /** Read ids from file into the catalog view */
    private class ReadButtonHandler implements ActionListener {
        public void actionPerformed (ActionEvent evt) {
            catFileName = getSolutionIdsFileName(catFileName);
            if (catFileName != null) {
              System.out.println("INFO: CatalogPanel Solution id(s) to be read from input file: " +  catFileName);
              jiggle.resetCatPanel(false, catFileName);
            }
            else {
              InfoDialog.informUser(getTopLevelAncestor(), "INFO", "No input filename selected.", statusLabel);
            }
        }
    }

    // Inner class containing essential tables and actions
    private class TablePanel extends JPanel implements Runnable {
        protected CatalogTable catalogTable;
        protected JScrollPane spane;
        private Object inputEventSrc;
        private Thread tableThread;
        private Runnable tableThreadPanelUpdate = new UpdateTablePanel();

        private Cursor waitCursor = new Cursor(Cursor.WAIT_CURSOR);
        private Cursor defaultCursor = new Cursor(Cursor.DEFAULT_CURSOR);

        private boolean imageFlag = true;
        private JLabel waitRequestLabel = new JLabel();
        {
            waitRequestLabel.setText("Table results displayed here");
            waitRequestLabel.setHorizontalAlignment(JLabel.CENTER);
        }

        private javax.swing.Timer tableTimer =
            new javax.swing.Timer(250,
                new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        waitRequestLabel.setText("table rendering wait...");
                        waitRequestLabel.setIcon(imageFlag ? LIGHT_BULB : DARK_BULB);
                        imageFlag = ! imageFlag;
                        waitRequestLabel.repaint();
                    }
                }
            );

        private int nrows = 0;
        private boolean interruptRequestFlag = false;
        private boolean tableRowHeadSelectionFlag;

// TablePanel constructor most user gui activity is performed by listeners of TablePanel class
        TablePanel () {
            TablePanel.this.setLayout(new BorderLayout());
            TablePanel.this.add(waitRequestLabel, BorderLayout.CENTER);

            CatalogTableModel ctm = new CatalogTableModel();
            ctm.addTableModelListener(CatalogPanel.this);
            ctm.setCellEditable(false);
            catalogTable = new CatalogTable(ctm);
            catalogTable.setProperties(jiggle.getProperties().getCatalogProperties());

            if (columnOrderByName == null) System.out.println("column name order not initialized");
            catalogTable.setColumnNameOrder(columnOrderByName);

        } // end of TablePanel() constructor

// TablePanel method to run thread to get data from database and generate table
        private void startTableThread() {
            if (spane != null) {
                try {
                    TablePanel.this.catalogTable.getModel().setList(solutionList);
                    catalogTable.initColumnSizes(catalogTable.getTable());
                }
                catch(NullPointerException ex) {
                    ex.printStackTrace();
                    return;
                }
                TablePanel.this.revalidate();
                unsetTableModified();
                return;
            }
            // component pane D.N.E so need new
            tableThread = new Thread(TablePanel.this, "solutionListTable");
            tableTimer.start();
            tableThread.start();
            setCursor(waitCursor);
        }

        public void run() {
            Thread thisThread = Thread.currentThread();
            nrows = 0;
            if (hasNullSolutionList()) {
                System.err.println("CatalogPanel TablePanel Thread run() SolutionList object is null.");
                updateTable();
                return;
            }
            try {
                thisThread.sleep(10l);
                TablePanel.this.catalogTable.getModel().createList(solutionList);
                nrows = CatalogPanel.this.solutionList.size();
                if (nrows > 0) {
                    createTable();
                    unsetTableModified();
                }
                else if (nrows <= 0) {
                    System.out.println("CatalogPanel No solutions found in list.");
                }
            }
            catch (InterruptedException ex) {
                System.err.println("Stopping CatalogPanel table creation thread");
            }
            catch (Exception ex) {
                System.err.println("Generic exception caught by run() this CatalogPanel table creation thread.");
                System.err.println(ex.getMessage());
                ex.printStackTrace();
            }
            finally {
                // Better to use a invokeLater(runnable) to make panel changes in the event-dispatch thread:
                updateTable();
            }
        }

        public void updateTable() {
           SwingUtilities.invokeLater(tableThreadPanelUpdate);
        }

        private void createTable() {
            JTable jtable = tablePanel.catalogTable.getRowHeader();
            if (jtable != null && jiggle != null) jtable.removeMouseListener(catalogRowHeadMouseAdapter);
            jtable = tablePanel.catalogTable.getTable();
            if (jtable != null && jiggle != null) jtable.removeMouseListener(catalogRowHeadMouseAdapter);
            spane = catalogTable.createTable();

            if (spane != null) {
                spane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
                spane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

                JButton lsb = makeButton(".","dot_red.gif");
                lsb.setToolTipText("Scroll to selected row");
                lsb.addActionListener( new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        updateTableRowSelection();
                    }
                });
                spane.setCorner(JScrollPane.LOWER_LEFT_CORNER, lsb);

                JButton tsb = makeButton("^","RedUp.gif");
                tsb.setToolTipText("Scroll to top of catalog");
                tsb.addActionListener( new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        Point p = spane.getViewport().getViewPosition();
                        p.y = 0;
                        spane.getViewport().setViewPosition(p);
                        spane.getViewport().repaint();
                    }
                });
                spane.setCorner(JScrollPane.UPPER_RIGHT_CORNER, tsb);

                JButton bsb = makeButton("v","RedDown.gif");
                bsb.setToolTipText("Scroll to bottom of catalog");
                bsb.addActionListener( new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        Rectangle r = spane.getViewport().getViewRect();
                        r.y = spane.getViewport().getView().getHeight() - r.height;
                        if (r.y <= 0) return;
                        Point p = new Point(r.x,r.y);
                        spane.getViewport().setViewPosition(p);
                        spane.getViewport().repaint();
                    }
                });
                spane.setCorner(JScrollPane.LOWER_RIGHT_CORNER, bsb);
            }

            jtable = tablePanel.catalogTable.getRowHeader();
            if (jtable != null && jiggle != null) jtable.addMouseListener(catalogRowHeadMouseAdapter);
            jtable = tablePanel.catalogTable.getTable();
            if (jtable != null && jiggle != null) jtable.addMouseListener(catalogRowHeadMouseAdapter);
            // Added below listener so panel can highlight the selected solution after sort -aww 10/26/2005
            TableMap tm = tablePanel.catalogTable.getSortedModel();
            tm.addTableModelListener(CatalogPanel.this);

        }

        private boolean hasInvalidTable() {
            return ! this.hasValidTable();
        }

        private boolean hasValidTable() {
            return ! (catalogTable.getTable() == null || hasNullSolutionList());
        }

        /*
        private void resetSelectedRowColumnSizes() {
            int rowid = catalogTable.getRowHeader().getSelectedRow();
            catalogTable.resetRowColumnSizes(catalogTable.getRowHeader(), rowid);
            catalogTable.resetRowHeader(catalogTable.getRowHeader(), TablePanel.this.spane);
            spane.revalidate();
        }
        */

// TablePanel event listener inner classes
        private class UpdateTablePanel implements Runnable {
            public void run() {
                tableTimer.stop();
                if (nrows > 0) remove(waitRequestLabel);
                else {
                    waitRequestLabel.setText("Table results displayed here");
                    waitRequestLabel.setIcon(null);
                }
                setCursor(defaultCursor);

                if (nrows > 0) {
//                  Debug.println("DEBUG: hasModifiedTable(): " + hasModifiedTable());
                    statusLabel.setText("Table has " + nrows + " rows");
                    TablePanel.this.add(spane, BorderLayout.CENTER);
                    CatalogPanel.this.enableCatalogButtons();
                }
                else if (nrows < 0) {
                    InfoDialog.informUser(CatalogPanel.this, "ERROR",
                            "Table creation failed; check input.", statusLabel);
                }
                else if (nrows == 0) {
                    System.out.println("No table rows satisfy input properties");
                }

                // make sure selected sol is selected in JTable
                CatalogPanel.this.updateTableRowSelection();
                CatalogPanel.this.tablePanel.revalidate();
                tableThread = null;
                interruptRequestFlag = false;
              }
        }

    } // end of TablePanel class

    private class ClipboardPopupMenu extends JPopupMenu implements ActionListener {

        Toolkit toolkit = null;
        Clipboard clip = null;
        MouseEvent lastMouseEvent = null;

        public ClipboardPopupMenu() {
            toolkit = getToolkit();
            clip = toolkit.getSystemClipboard();
            configureMenu();
        }

        protected void configureMenu() {
            // instead force user to left click mouse for selection
            /*
            jmi = new JMenuItem("Select");
            jmi.addActionListener(
                  new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                      int row = tablePanel.catalogTable.getRowHeader().rowAtPoint(e.getPoint());
                      if (row < 0) row = tablePanel.catalogTable.getTable().rowAtPoint(e.getPoint());
                      if (row >= 0) tablePanel.catalogTable.getRowHeader().setRowSelectionInterval(row, row);
                    }
                  }
            );
            add(jmi);
            addSeparator();
            */

            JMenuItem jmi = addMenuItem("Cancel", null, "No-op, close popup");
            jmi.setEnabled(true);
            addSeparator();

            int cnt = getSelectedRowCount();
            jmi = addMenuItem("Where", this, "Show gazetteer summary info for selected event");
            jmi.setEnabled((cnt == 1));
            jmi = addMenuItem("Load", this, "Load selected event's data and timeseries into viewer");
            jmi.setEnabled((cnt == 1));
            boolean tf = (cnt == 1 && solutionList != null && !((Solution)solutionList.getSelected()).isValid());
            if (tf) {
              jmi = addMenuItem("Undelete", this, "Undelete selected event");
              jmi.setEnabled(tf);
            }
            addSeparator();

            jmi = addMenuItem("Refresh id(s)", this, "Refresh selected event(s) with their current database data");
            jmi.setEnabled((cnt >= 1));
            addSeparator();

            jmi = addMenuItem("Delete id(s)", this, "Delete selected event(s) from database");
            jmi.setEnabled((cnt >= 1));
            addSeparator();

            //addMenuItem("Copy id", this, "Copy selected event id to clipboard");
            jmi = addMenuItem("Copy id(s)", this, "Copy the selected event ids to clipboard");
            jmi.setEnabled((cnt >= 1));
            jmi = addMenuItem("Copy summary(s)", this, "Copy event summaries for the selected rows to clipboard");
            jmi.setEnabled((cnt >= 1));
            jmi = addMenuItem("Copy row(s)", this, "Copy all table cells for the selected rows to clipboard (CSV)");
            jmi.setEnabled((cnt >= 1));
            if (jiggle.getProperties().getProperty("catalogColumnListToCopy") != null)  {
              jmi = addMenuItem("Copy row col(s)", this, "Copy specified column values for the selected rows to clipboard (CSV)");
              jmi.setEnabled((cnt >= 1));
            }
            jmi = addMenuItem("Copy data cell", this, "Copy the cell value closest to mouse click to clipboard");
            jmi.setEnabled((tablePanel.catalogTable.getModel().getRowCount() > 0 ));
            addSeparator();

            jmi = addMenuItem("Edit view id comment", this, "Edit the comment of waveform view selected event.");
            Solution sol = null;
            if (solutionList != null) {
                sol = (Solution) solutionList.getSelected();
            }

            Solution mvSol = jiggle.mv.getSelectedSolution(); // solution loaded in master view
            jmi.setEnabled( (cnt == 1) && sol != null && sol.idEquals(mvSol) );
            addSeparator();

            jmi = addMenuItem("Reset to waveform view id", this, "Select catalog row corresponding to id loaded in waveform view.");
            jmi.setEnabled( mvSol != null && (! (cnt == 1) || ! mvSol.idEquals(sol)) );
            addSeparator();

            jmi = addMenuItem("Scroll to currently selected id", this, "Scroll viewport to currently selected catalog row.");
            jmi.setEnabled((tablePanel.catalogTable.getModel().getRowCount() > 0 ) && solutionList.getSelectedIndex() >= 0);
            addSeparator();

            jmi = addMenuItem("Quarry check...", this, "Check if selected events are possibly quarry blast");
            jmi.setEnabled((tablePanel.catalogTable.getModel().getRowCount() > 0 ) && solutionList.getSelectedIndex() >= 0);

            addSeparator();
            addEventAssociationMenu(sol, cnt);

            addSeparator();
            jmi = addMenuItem("Select all", this, "Selected all catalog event ids");
            jmi.setEnabled((tablePanel.catalogTable.getModel().getRowCount() > 0 ));

            addSeparator();
            // Option to remove an event lock - one row selected and has not been loaded
            jmi = addMenuItem(JiggleConstant.POPUP_MENU_REMOVE_EVENT_LOCK, this, "Remove event lock for the selected event");
            jmi.setEnabled(isOneRowSelectedAndNotLoadedInMaster(cnt, sol, mvSol));

        }

        private void addEventAssociationMenu(Solution solution, int selectedCount) {
            JMenu associationMenu = new JMenu(JiggleConstant.POPUP_MENU_ASSOCIATION);
            JMenuItem assocMenuItem = new JMenuItem(JiggleConstant.POPUP_MENU_NEW_ASSOCIATION);
            assocMenuItem.setToolTipText(JiggleConstant.POPUP_MENU_NEW_ASSOCIATION);
            assocMenuItem.addActionListener(this);
            associationMenu.add(assocMenuItem);
            assocMenuItem.setEnabled(selectedCount >= 1 && isAllowNewAssociation(solution, selectedCount));
            this.add(associationMenu);

            assocMenuItem = new JMenuItem(JiggleConstant.POPUP_MENU_EDIT_ASSOCIATION);
            assocMenuItem.setToolTipText(JiggleConstant.POPUP_MENU_EDIT_ASSOCIATION);
            assocMenuItem.addActionListener(this);
            associationMenu.add(assocMenuItem);
            assocMenuItem.setEnabled(selectedCount == 1 && !solution.getAssociation().isEmpty());
            this.add(associationMenu);

            assocMenuItem = new JMenuItem(JiggleConstant.POPUP_MENU_DELETE_ASSOCIATION);
            assocMenuItem.setToolTipText(JiggleConstant.POPUP_MENU_DELETE_ASSOCIATION);
            assocMenuItem.addActionListener(this);
            associationMenu.add(assocMenuItem);
            assocMenuItem.setEnabled(selectedCount == 1 && !solution.getAssociation().isEmpty());
            this.add(associationMenu);
        }

        /**
         * Check if "New Association" should be allowed
         * @param solution
         * @param selectedCount
         * @return
         */
        private boolean isAllowNewAssociation(Solution solution, int selectedCount) {
            boolean allowed = true;

            if (selectedCount < 1) {
                // nothing selected
                allowed = false;
            } else if (selectedCount == 1 && !solution.getAssociation().isEmpty()) {
                // only one selected, but the current row has an association
                allowed = false;
            } else if (selectedCount > 1) {
                Solution [] selectedSolutions = getSelectedSolutions();
                if (isEventNotAssociated(selectedSolutions) == false){
                    // at least one event is already associated
                    allowed = false;
                }
            }

            return allowed;
        }

        /**
         * Check all solutions to see if an event has been associated to another
         * @param solutionList
         * @return
         */
        private boolean isEventNotAssociated(Solution [] solutionList) {
            boolean isNotAssociated = true;
            int idx = 0;
            while (isNotAssociated && idx < solutionList.length) {
                if (!solutionList[idx].getAssociation().isEmpty()) {
                    isNotAssociated = false;
                }
                idx++;
            }
            return isNotAssociated;
        }


        /**
         * Return true if only one row is selected that is not loaded in master/main view
         */
        private boolean isOneRowSelectedAndNotLoadedInMaster(int numSelected, Solution tblSelected, Solution masterViewLoaded) {
            if (numSelected == 1 &&
                tblSelected != null &&
                (masterViewLoaded==null || (masterViewLoaded != null && !masterViewLoaded.idEquals(tblSelected)))) {
                return true;
            } else return false;
        }

        private JMenuItem addMenuItem(String itemName, ActionListener itemListener, String tooltip) {
            JMenuItem jmi = new JMenuItem(itemName);
            jmi.setToolTipText(tooltip);
            if (itemListener != null) jmi.addActionListener(itemListener);
            add(jmi);
            return jmi;
        }

        public void actionPerformed(ActionEvent e) {
            try {
                JMenuItem source = (JMenuItem)(e.getSource());
                String txt = source.getText();
                if (txt.equals("Copy id(s)")) {
                  //JTable aTable = tablePanel.catalogTable.getRowHeader();
                  //int [] rows = aTable.getSelectedRows();
                  long [] rows = getSelectedIds();
                  if (rows != null && rows.length > 0) {
                    //Object obj = null;
                    StringBuffer sb = new StringBuffer(rows.length*16);
                    for (int ii = 0; ii < rows.length; ii++) {
                   //     obj = aTable.getValueAt(rows[ii], 0);
                   //     if (obj != null) {
                   //       sb.append(obj.toString()).append("\n");
                   //    }
                        sb.append(rows[ii]).append("\n");
                    }
                    StringSelection cellData = new StringSelection(sb.toString());
                    clip.setContents(cellData, cellData);
                  }
                  else
                        JOptionPane.showMessageDialog(getTopLevelAncestor(),
                            "First select the event id(s) to copy",
                            "Copy id(s)", JOptionPane.ERROR_MESSAGE
                        );
                }
                /*
                else if (txt.equals("Copy id")) {
                  JTable aTable = tablePanel.catalogTable.getRowHeader();
                  Object obj = aTable.getValueAt(aTable.getSelectedRow(), 0);
                  if (obj != null) {
                    StringSelection cellData = new StringSelection(obj.toString());
                    clip.setContents(cellData, cellData);
                  }
                  else System.out.println("Copy id menu: null id");
                }
                */
                else if (txt.equals("Copy data cell")) {
                  JTable aTable = tablePanel.catalogTable.getRowHeader();
                  Point pt = lastMouseEvent.getPoint();
                  int col = -1;
                  if (aTable.contains(pt)) col = aTable.columnAtPoint(pt);
                  else {
                     aTable = tablePanel.catalogTable.getTable();
                     if (aTable.contains(pt)) col = aTable.columnAtPoint(pt);
                  }
                  //Object obj = (col < 0) ? null : aTable.getValueAt(aTable.getSelectedRow(), col);
                  Object obj = (col < 0) ? null : aTable.getValueAt(aTable.rowAtPoint(pt), col);
                  if (obj != null) {
                    StringSelection cellData = new StringSelection(obj.toString());
                    clip.setContents(cellData, cellData);
                  }
                  else  {
                        JOptionPane.showMessageDialog(getTopLevelAncestor(),
                            "First right-click inside any data cell to copy",
                            "Copy data cell", JOptionPane.ERROR_MESSAGE
                        );
                  }
                }
                else if (txt.equals("Copy row col(s)")) {
                  // Have to work with TableModel and map from sorted table to the model indices
                  // TODO: stateful checkboxes by column names in list shown in popup dialog
                  JTable aTable = tablePanel.catalogTable.getRowHeader();
                  JTable bTable = tablePanel.catalogTable.getTable();

                  int [] rows = aTable.getSelectedRows();
                  String [] cols = null;
                  if (rows != null && rows.length > 0) {
                    cols = jiggle.getProperties().getStringArray("catalogColumnListToCopy");
                  }
                  //System.out.println("cat columns: " + cols.length);

                  if (cols != null && cols.length > 0) {
                    int kdx = 0;
                    TableColumn tc = null;
                    StringBuilder sb = new StringBuilder(4096);
                    int ii = 0;
                    OUTER:
                    for (int idx = 0; idx < rows.length; idx++) {
                      sb.append(aTable.getValueAt(rows[idx], 0)).append(",");
                      //ii = bTable.convertRowIndexToModel(rows[idx]); // java1.6
                      ii = tablePanel.catalogTable.getSortedRowModelIndex(rows[idx]);
                      for (int jdx = 0; jdx < cols.length; jdx++) {
                        tc = bTable.getColumn(cols[jdx]);
                        if (tc != null) {
                          kdx = tc.getModelIndex();
                        }
                        else {
                            JOptionPane.showMessageDialog(getTopLevelAncestor(),
                                cols[idx] + " column not found, specify a valid column name",
                                "Copy Row(s)", JOptionPane.ERROR_MESSAGE
                            );
                            break OUTER;
                        }
                        if (tc.getIdentifier().equals("DATETIME"))
                            sb.append(LeapSeconds.trueToString(((DataDouble) bTable.getModel().getValueAt(ii, kdx)).doubleValue()));
                        else
                            sb.append(bTable.getModel().getValueAt(ii, kdx));
                        if (jdx != cols.length-1) sb.append(",");
                      }
                      sb.append("\n");
                    }
                    StringSelection cellData = new StringSelection(sb.toString());
                    clip.setContents(cellData, cellData);
                  }
                  else {
                        JOptionPane.showMessageDialog(getTopLevelAncestor(),
                            "Nothing to copy, you must first select the rows and specify valid column names",
                            "Copy Row(s)", JOptionPane.ERROR_MESSAGE
                        );
                  }
                }
                else if (txt.equals("Copy row(s)")) {
                  JTable aTable = tablePanel.catalogTable.getRowHeader();
                  JTable bTable = tablePanel.catalogTable.getTable();
                  int [] rows = aTable.getSelectedRows();
                  if (rows != null && rows.length > 0) {
                    int maxCols =  bTable.getColumnCount();
                    StringBuilder sb = new StringBuilder(4096);
                    for (int idx = 0; idx < rows.length; idx++) {
                      sb.append(aTable.getValueAt(rows[idx], 0)).append(",");
                      for (int jdx = 0; jdx < maxCols; jdx++) {
                        if (bTable.getColumnName(jdx).equals("DATETIME"))
                            sb.append( LeapSeconds.trueToString(((DataDouble) bTable.getValueAt(rows[idx], jdx)).doubleValue()) );
                        else
                            sb.append(bTable.getValueAt(rows[idx], jdx));
                        if (jdx != maxCols-1) sb.append(",");
                      }
                      sb.append("\n");
                    }
                    StringSelection cellData = new StringSelection(sb.toString());
                    clip.setContents(cellData, cellData);
                  }
                  else {
                        JOptionPane.showMessageDialog(getTopLevelAncestor(),
                            "Nothing to copy, you must first select the rows",
                            "Copy Row(s)", JOptionPane.ERROR_MESSAGE
                        );
                  }
                }
                else if (txt.equals("Copy summary(s)")) {
                  /*
                  Solution sol = (Solution) solutionList.getSelected();
                  StringBuffer sb = new StringBuffer(128);
                  if (sol != null) {
                    sb.append(sol.toSummaryString());
                    if (sol.hasComment()) sb.append(" [").append(sol.getComment()).append("]");
                    sb.append("\n"); // add line feed so cursor goes to next line when pasted -aww 05/02/2006
                  }
                  */
                  //JTable aTable = tablePanel.catalogTable.getRowHeader();
                  //int [] rows = aTable.getSelectedRows();
                  Solution [] rows = getSelectedSolutions();
                  if (rows != null && rows.length > 0) {
                    //Object obj = null;
                    StringBuffer sb = new StringBuffer(128*rows.length);
                    //Solution sol = null;
                    for (int ii = 0; ii < rows.length; ii++) {
                        //sol = solutionList.getSolution(tablePanel.catalogTable.getSortedRowModelIndex(rows[ii]));
                        //sb.append(sol.toSummaryString()).append("\n");
                        sb.append(rows[ii].toSummaryString()).append("\n");
                    }
                    StringSelection cellData = new StringSelection(sb.toString());
                    clip.setContents(cellData, cellData);
                  }
                  else
                        JOptionPane.showMessageDialog(getTopLevelAncestor(),
                            "First select the ids of events to summarize",
                            "Copy Summary(s)", JOptionPane.ERROR_MESSAGE
                        );
                }
                else if (txt.equals("Edit view id comment")) {
                    Solution sol = (Solution) solutionList.getSelected();
                    Solution mvSol = jiggle.mv.getSelectedSolution();
                    if (sol != null && sol.idEquals(mvSol)) {
                      int rowid = tablePanel.catalogTable.getRowHeader().getSelectedRow();
                      if (rowid >= 0  ) {
                        Solution catsol =
                            (Solution) solutionList.get(tablePanel.catalogTable.getSortedRowModelIndex(rowid));
                        if (catsol.idEquals(mvSol)) jiggle.addComment();
                      }
                    }
                    else
                        JOptionPane.showMessageDialog(getTopLevelAncestor(),
                            "Selected event row id has to match event id in waveform view.",
                            "Edit Event Comment", JOptionPane.ERROR_MESSAGE
                        );
                }
                else if (txt.equals("Reset to waveform view id")) {
                    Solution sol = jiggle.mv.getSelectedSolution();
                    if (sol != null) {
                        // int selRow = solutionList.getSelectedIndex();
                        solutionList.setSelected(sol.getId().longValue());
                        updateTableRowSelection();
                        /*
                        int row = tablePanel.catalogTable.modelToSortedRowIndex(solutionList.getSelectedIndex());
                        tablePanel.catalogTable.getRowHeader().setRowSelectionInterval(row, row);
                        tablePanel.catalogTable.getTable().scrollRectToVisible(
                            tablePanel.catalogTable.getTable().getCellRect(row, 1)
                        );
                        */
                    }
                }
                else if (txt.equals("Scroll to currently selected id")) {
                    updateTableRowSelection();
                }
                else if (txt.equals("Refresh id(s)")) { // aww 05/02/2007
                  // NOTE: Might be a selected solution in MV the same with id
                  //Solution sol = (Solution) solutionList.getSelected();
                  //if (sol != null) {
                  Solution [] rows = getSelectedSolutions();
                  if (rows != null) {
                    Solution sol = null;
                    for (int ii = 0; ii < rows.length; ii++) {
                      // DISABLE FOR WAN ?
                      //newSol = (Solution) sol.getById(sol.getId());
                      //if (! jiggle.getEventProperties().matches(newSol)) solutionList.erase(sol);
                      //else solutionList.addOrReplaceById(newSol);
                      sol = rows[ii];
                      Solution newSol = sol.refresh(jiggle.getEventProperties());
                      if (newSol == null) {
                          Solution nextSol = getNextSortedSolution(sol); // added 2008/04/16 -aww
                          solutionList.remove(sol);
                          solutionList.setSelected(nextSol); // added 2008/04/16 -aww
                      }
                      else solutionList.addOrReplaceById(newSol);
                    }
                    CatalogPanel.this.update();
                  }
                  else
                        JOptionPane.showMessageDialog(getTopLevelAncestor(),
                            "First select the ids of events to refresh",
                            "Refresh id(s)", JOptionPane.ERROR_MESSAGE
                        );
                }
                else if (txt.equals("Delete id(s)")) {
                    deleteButton.doClick();
                }
                else if (txt.equals("Where")) {
                    whereButton.doClick();
                }
                else if (txt.equals("Load")) {
                    loadButton.doClick();
                }
                else if (txt.equals("Undelete")) {
                   Solution sol = (Solution) solutionList.getSelected();
                   if (sol == null) {
                        JOptionPane.showMessageDialog(getTopLevelAncestor(),
                            "First select the event to undelete",
                            "Undelete Event", JOptionPane.ERROR_MESSAGE
                        );
                   }
                   else if (! sol.isValid()) {
                       Solution mvSol = (jiggle == null) ? null : jiggle.mv.getSelectedSolution();
                       if (mvSol != null && sol.idEquals(mvSol)) sol = mvSol;
                       sol.validFlag.setValue(1);
                       sol.setDeleteFlag(false);
                       System.out.println("INFO: Jiggle catalog undeleting event: " + sol.getId().longValue());
                       jiggle.saveToDb(sol); // commit and update the selected catalog row
                       //tablePanel.catalogTable.getTable().repaint();
                       //tablePanel.catalogTable.getModel().fireTableDataChanged();
                   }
                   else {
                        JOptionPane.showMessageDialog(getTopLevelAncestor(),
                            "Selected event is not deleted",
                            "Undelete Event", JOptionPane.PLAIN_MESSAGE
                        );
                   }
                }
                else if (txt.equals("Select all")) {
                   JTable jtable = tablePanel.catalogTable.getRowHeader();
                   jtable.setRowSelectionInterval(0, jtable.getRowCount()-1);
                }
                else if (txt.equals("Quarry check...")) {
                  final Solution [] rows = getSelectedSolutions();

                  if (rows.length == 0) JOptionPane.showMessageDialog(getTopLevelAncestor(),
                        "No selections, you must first select ids in catalog", "Quarry Check Catalog", JOptionPane.PLAIN_MESSAGE);

                  int ans = JOptionPane.showConfirmDialog(getTopLevelAncestor(),
                        "List only those passing quarry test?", "Quarry Check Option", JOptionPane.YES_NO_OPTION);
                  final boolean onlyQ = (ans == JOptionPane.YES_OPTION);

                  jiggle.initStatusGraphicsForThread("Dump quarry info", "Getting quarry data ..."); // pops status
                  org.trinet.util.SwingWorker sw = new org.trinet.util.SwingWorker() {
                    int qcnt = 0;
                    String str = null;
                    Solution sol = null;
                    StringBuilder sb = new StringBuilder(rows.length*160);
                    //System.out.println(sol.getNeatStringHeader());

                    public Object construct() { // any GUI graphics in method should invokeLater

                      for (int idx = 0; idx < rows.length; idx++) {

                        sol = rows[idx];

                        if (sol.getLatLonZ().isNull() || sol.isOriginGType(GTypeMap.TELESEISM)) continue;

                        whereFrom.setReference(sol.lat.doubleValue(), sol.lon.doubleValue());
                        WhereSummary ws = whereFrom.whereSummaryType(GazetteerType.QUARRY);
                        String units = jiggle.getProperties().getProperty("whereUnits");
                        GeoidalUnits gu = ( (units != null)  && (units.startsWith("M") || units.startsWith("m")) ) ?
                                GeoidalUnits.MILES : GeoidalUnits.KILOMETERS;

                        String quarryName = ws.fromWhereString(gu, false);

                        if (sol.isQuarry()) {
                            if ( ! sol.isEventDbType(EventTypeMap3.QUARRY) ) {
                              qcnt++;
                            }
                            str = "*" + sol.toNeatString() + " " + quarryName;
                        }
                        else str = " " + sol.toNeatString() + " " + quarryName;

                        if (!onlyQ || str.startsWith("*")) {
                          //System.out.println(str); // write it to console log
                          sb.append(str).append("\n");
                        }
                      }
                      if (sb.length() > 0) str = sb.toString();
                      else str = "No quarry data found.";
                      return null;
                    }

                    public void finished() { // GUI update here
                        jiggle.resetStatusGraphicsForThread(); // unpops status
                        jiggle.dumpStringToTab(str, Jiggle.TAB_MESSAGE, true); // dump to msg tab
                        JOptionPane.showMessageDialog( getTopLevelAncestor(),
                            "Found " + qcnt + " suspected quarries", "Quarry Check", JOptionPane.PLAIN_MESSAGE);
                    }
                  }; // end of SwingWorker
                  sw.start(); // start long task worker thread
                } else if (txt.equals(JiggleConstant.POPUP_MENU_REMOVE_EVENT_LOCK)) {
                    unlockEventHandler();
                } else if (txt.equals(JiggleConstant.POPUP_MENU_NEW_ASSOCIATION)) {
                    newAssociationEventHandler(e);
                } else if (txt.equals(JiggleConstant.POPUP_MENU_EDIT_ASSOCIATION)) {
                    editAssociationEventHandler(e);
                } else if (txt.equals(JiggleConstant.POPUP_MENU_DELETE_ASSOCIATION)) {
                    deleteAssociationEventHandler(e);
                }
            } catch (Exception ex) {
                System.out.println(ex.toString());
            }
        }

        /**
         * Handle Event Association
         */
        private void newAssociationEventHandler(ActionEvent e) {
            Solution[] selectedEvent = getSelectedSolutions();
            if (selectedEvent != null) {
                EventAssociationDialog assocDialog = new EventAssociationDialog(JiggleSingleton.getInstance().getMainJFrame(), true, true);
                assocDialog.setPanContent(new EventAssociation(jiggle.catSolList, selectedEvent, true));
                assocDialog.setTitle("Create Event Association");
                assocDialog.pack();
                assocDialog.setLocationRelativeTo(JiggleSingleton.getInstance().getMainJFrame());
                assocDialog.setVisible(true);

                if (!assocDialog.isDialogCanceled()) {
                    updateEventAssociation(assocDialog);
                }
            }
        }

        protected void updateEventAssociation(EventAssociationDialog assocDialog) {
            long masterEventId = assocDialog.getUpdatedMasterEventId();
            java.util.List<AssocEvent> currentAssoc = EventAssocManager.getEventAssoc()
                    .getAssociatedEvents(masterEventId);
            String asocList = AssocEvent.getEventAssociationList(currentAssoc);

            int modelIndex = tablePanel.catalogTable.getModel().getIndexOf(Solution.create().getById(masterEventId));
            tablePanel.catalogTable.getModel().getSolution(modelIndex).setAssociation(asocList);
            tablePanel.catalogTable.getModel().fireTableDataChanged();
            tablePanel.catalogTable.getTable().revalidate();
            tablePanel.catalogTable.getTable().repaint();
        }

        // Edit event association
        private void editAssociationEventHandler(ActionEvent e) {
            int rowid = tablePanel.catalogTable.getRowHeader().getSelectedRow();
            if (rowid >= 0) {
                Solution selectedEvent = ((Solution)solutionList.get(tablePanel.catalogTable.getSortedRowModelIndex(rowid)));

                EventAssociationDialog assocDialog = new EventAssociationDialog(JiggleSingleton.getInstance().getMainJFrame(), true, false);
                assocDialog.setPanContent(new EventAssociation(jiggle.catSolList, new Solution[]{selectedEvent}, false));
                assocDialog.setTitle("Edit Event Association");
                assocDialog.pack();
                assocDialog.setLocationRelativeTo(JiggleSingleton.getInstance().getMainJFrame());
                assocDialog.setVisible(true);

                if (!assocDialog.isDialogCanceled()) {
                    updateEventAssociation(assocDialog);
                }
            }
        }

        // Delete event association
        private void deleteAssociationEventHandler(ActionEvent e) {
            int rowSelectedId = tablePanel.catalogTable.getTable().getSelectedRow();
            Solution solution = ((Solution)solutionList.get(tablePanel.catalogTable.getSortedRowModelIndex(rowSelectedId)));

            int response = JOptionPane.showConfirmDialog(JiggleSingleton.getInstance().getMainJFrame(),
                    "Delete event association for event id " + solution.getId() + "?",
                    "Delete Event Association",
                    JOptionPane.OK_CANCEL_OPTION,
                    JOptionPane.WARNING_MESSAGE);

            if (response == JOptionPane.OK_OPTION) {

                EventAssoc eventDb = EventAssocManager.getEventAssoc();
                AssocEvent eventComment = eventDb.getComment(solution.getId().longValue());
                boolean status = eventDb.deleteAssociatedEvent(solution.getId().longValue());
                if (status) {
                    eventDb.commit();

                    // delete associated comments
                    if (eventComment != null && eventComment.getCommentId() > 0) {
                        // if comment exits, then delete
                        boolean isDeleted = new RemarkDb().deleteRemark(eventComment.getCommentId());
                        if (!isDeleted) {
                            LogUtil.error("Could not delete a remark record for the event association. master: " + eventComment.getMasterEventId() +
                                    "comment: " + eventComment.getCommentId());
                        }
                    }

                    int modelIndex = tablePanel.catalogTable.getModel().getIndexOf(solution);
                    tablePanel.catalogTable.getModel().getSolution(modelIndex).setAssociation("");
                    tablePanel.catalogTable.getModel().fireTableDataChanged();
                    tablePanel.catalogTable.getTable().revalidate();
                    tablePanel.catalogTable.getTable().repaint();                                    
                } else { 
                    JOptionPane.showMessageDialog(JiggleSingleton.getInstance().getMainJFrame(),
                            "There may have been an error while deleting the event association. Please try again or restart Jiggle.",
                            "Error Deleting Association", 
                            JOptionPane.WARNING_MESSAGE);
                }
            }
        }
        
        /**
         * Remove event lock on the selected event
         */
        private void unlockEventHandler() {
            Solution[] selectedEvent = getSelectedSolutions();
            if (selectedEvent != null) {
                if (selectedEvent.length > 1) {
                    JOptionPane.showMessageDialog(JiggleSingleton.getInstance().getMainJFrame(),
                            "Event lock should be removed one at a time. Please select one event to continue.",
                            "Too many events", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    Long eventId = selectedEvent[0].getId().longValue();
                    String confirmMsg = "Any event lock for the Event " + eventId + " will be removed." +
                            JiggleConstant.NEWLINE +
                            "This could result in a data corruption if another user is editing the event." +
                            JiggleConstant.NEWLINE +
                            JiggleConstant.NEWLINE +
                            "Are you sure you want to continue?";
                    int isRemove = JOptionPane.showConfirmDialog(JiggleSingleton.getInstance().getMainJFrame(),
                            confirmMsg,
                           "Remove Event Lock", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                    if (isRemove == JOptionPane.OK_OPTION) {
                        SolutionLock lock = SolutionLock.create();
                        lock.setId(eventId);
                        boolean status = lock.unlock();

                        if (status) {
                            System.out.println("INFO: User removed event lock on event " + eventId);
                            JOptionPane.showMessageDialog(JiggleSingleton.getInstance().getMainJFrame(),
                                    "Successfully removed the event lock for the Event " + eventId,
                                    "Removed Event Lock",
                                    JOptionPane.INFORMATION_MESSAGE);
                        } else {
                            // Status is false if no updates were performed or there was an error
                            System.out.println("INFO: Event lock not found for event "  + eventId + ". " +
                                    "Either the event was not locked or look for an exception");
                            JOptionPane.showMessageDialog(JiggleSingleton.getInstance().getMainJFrame(),
                                    "Event lock was not found for Event " + eventId,
                                    "Event Lock Not Found",
                                    JOptionPane.INFORMATION_MESSAGE);
                        }
                    }
                }
            } else {
                JOptionPane.showMessageDialog(JiggleSingleton.getInstance().getMainJFrame(),
                "Please select an event to remove the lock!", "Select an Event", JOptionPane.INFORMATION_MESSAGE);
            }

        }

        public void show(MouseEvent e) {
            if (! e.getComponent().isShowing() ) return; // to counter exception seen in menlo log 01/18/2007 -aww
            lastMouseEvent = e;
            super.show(e.getComponent(), e.getX(), e.getY());
        }

    } // end of popupmenu inner class

    /*
    protected JTable getTable() {
        return tablePanel.catalogTable.getTable();
    }
    */

} // end of CatalogPanel class
