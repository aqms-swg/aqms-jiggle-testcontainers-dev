package org.trinet.jiggle;

import java.util.*;
//import java.awt.*;
import javax.swing.event.*;

/** Generic Change model. Allows listeners to be added and removed. */
public class ChangeModel {

     // allows control of whether or not events are fired
     protected boolean fireEvents = true;
     // Support of state change events
     EventListenerList listenerList = new EventListenerList();
     ChangeEvent changeEvent = null;

     public ChangeModel() { }

     /** Allows firing of change events for this model to be turned on and off.*/
     public void setFireEvents(boolean tf) {
        fireEvents = tf;
     }
     /** Returns true if change events will be fired. */
     public boolean getFireEvents() {
         return fireEvents;
     }

    /** Register listener with list of ChangeListeners.
     *  Does not allow the same listener to be added more than once. */
    public void addChangeListener(ChangeListener l) {
        if (!contains(l))
        listenerList.add(ChangeListener.class, l);
    }

    /** Unregister listener from list of ChangeListeners */
    public void removeChangeListener(ChangeListener l) {
        listenerList.remove(ChangeListener.class, l);
    }

    /** Unregister ALL listeners from list of ChangeListeners */
    public void clearChangeListeners() {
        EventListener [] listeners = listenerList.getListeners(ChangeListener.class);
        for (int i=0; i<listeners.length; i++) {
            listenerList.remove(ChangeListener.class, (ChangeListener)listeners[i]);
        }
        listenerList = new EventListenerList();
    }

    /** Notify all listeners that have registered interest for notification on
     * this event type.  The event instance is created on each call to reflect the
     * current state. <p> The return from changeEvent.getSource() is this class. */
     protected void fireStateChanged(Object src) {

         // bail if events are turned off
         if (!getFireEvents()) return;

        // bail if no listeners
        if (listenerList.getListenerCount() == 0) return;

        // Guaranteed to return a non-null array, :. no NULL check needed
        Object[] listeners = listenerList.getListenerList();

        ChangeEvent changeEvent = new ChangeEvent(src);

        // Notify the listeners last to first. Note: 'listeners' is an array of
        // PAIRS of ListenerType/Listener, hence the weird indexing.
        for (int i = listeners.length-2; i>=0; i-=2) {
            if (listeners[i] == ChangeListener.class) {
                ((ChangeListener)listeners[i+1]).stateChanged(changeEvent);
            }
        }
    }

    /** Return true if this Model's listener list contains this listener. */
    public boolean contains (ChangeListener listener) {
        // bail if no listeners
        if (listenerList.getListenerCount() == 0) return false;

        // Guaranteed to return a non-null array, :. no NULL check needed
        Object[] listeners = listenerList.getListenerList();
        // Search the listeners last to first. Note: 'listeners' is an array of
        // PAIRS of ListenerType/Listener, hence the weird indexing.
        for (int i = listeners.length-2; i>=0; i-=2) {
            if (listeners[i] == ChangeListener.class &&
                (ChangeListener)listeners[i+1] == listener) return true;
        }
        return false;
    }

    /** Return count of registered listeners. */
    public int countListeners() {
       return listenerList.getListenerCount();
    }

    /** Returns the internal model. */
    public EventListenerList getEventListenerList() {
       return listenerList;
    }

    public Object[] getEventListenerListCopy() {
        int i = listenerList.getListenerCount() * 2;
        Object[] tmp = new Object[i];
        System.arraycopy(listenerList.getListenerList(), 0, tmp, 0, i);
         return tmp;
    }
}
