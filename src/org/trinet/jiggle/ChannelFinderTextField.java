package org.trinet.jiggle;

import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

import org.trinet.jasi.*;
import java.awt.Dimension;

/**
 * JTextField for finding a channel in a ChannelableList. Optionally you can
 * set a MasterView which will be used to notify model listeners via its
 * SelectedWFViewModel of selections made by this component.
 */

public class ChannelFinderTextField extends JTextField {

  ChannelableList chanlist;
  MasterView mv = null;

  /** Null constructor. */
  public ChannelFinderTextField( ) {
  }

  /** Will notify model listeners via MasterView's SelectedWFViewModel. Since no
   * ChannelableList is given the WFViewList for the MasterView is used. */
  public ChannelFinderTextField(MasterView masterview) {
    this(masterview, masterview.wfvList);
  }
  /** Will notify model listeners via MasterView's SelectedWFViewModel.
   * There is no guarentee that all the channels in this ChannelableList
   * will be in the WFViewList for the MasterView used. */
  public ChannelFinderTextField(MasterView masterview, ChannelableList list) {
    this(list);
    setMasterView(masterview);
  }

  /** Unless setMasterView is called this method cannot notify model listeners
   * via MasterView's SelectedWFViewModel.  */
  public ChannelFinderTextField(ChannelableList list) {

    setChannelableList(list);

    TFDocListener listener = new ChannelFinderTextField.TFDocListener(this);
    getDocument().addDocumentListener(listener);
    setMinimumSize(new Dimension(65, 20));
    setPreferredSize(new Dimension(65,20));
  }
  public void setMasterView (MasterView masterview) {
    mv = masterview;
  }
  public void setChannelableList (ChannelableList list) {
    chanlist = list;
  }

  /*
  public static final class Tester {
    public static void main(String[] args) {
      System.out.println ("Making connection... ");

      DataSource db = TestDataSource.create();

      System.out.println (db.toString());

///////////////////////
// NOTE: this demonstrates making a static Channel list.
      System.out.println ("Reading CURRENT channels...");

      //ChannelList list = ChannelList.readCurrentList();
      ChannelList list = ChannelList.smartLoad();

      ChannelFinderTextField channelFinderTextField = new ChannelFinderTextField(list);

      JFrame frame = new JFrame("Main");
      frame.addWindowListener(new WindowAdapter() {
        public void windowClosing(WindowEvent e) {System.exit(0);}
      });

      frame.getContentPane().add(channelFinderTextField);
      frame.pack();
      frame.setVisible(true);

    }
  }
// end of Tester
*/

/**
 * This is better then an actionListener because it updates
 * the values as they are edited and does not require that the user
 * hit a <CR> to trigger the listener.
 */
  class TFDocListener implements DocumentListener {

    ChannelFinderTextField tf;
    String str;

    // Passes reference to the caller so we can see and change some stuff
    public TFDocListener(ChannelFinderTextField textField){
      tf = textField;  // need reference to see other variables
    }
    // an insert occurred
    public void insertUpdate(DocumentEvent e) {
      setValues(e);
    }
    // an update occurred
    public void removeUpdate(DocumentEvent e) {
      setValues(e);
    }
    // a change occurred
    public void changedUpdate(DocumentEvent e) {
      // we won't ever get this with a PlainDocument
    }

    private void setValues(DocumentEvent e) {

      str = tf.getText();
      if (str == null || str.length() == 0) return;

      Channelable chan = chanlist.getChannelableStartingWith(str); // may be null

// This us UGLY
      if (tf.mv == null || tf.mv.masterWFViewModel == null) {
        JOptionPane.showMessageDialog(
             tf.getTopLevelAncestor(),
             "ChannelFinder masterView is null, or has no waveforms!",
             "ChannelFinder",
             JOptionPane.PLAIN_MESSAGE);
      }
      else if (chan != null) {
        tf.mv.masterWFViewModel.set(chan, tf.mv.wfvList);
      }
    }
  }  // end of DocListener()
}
