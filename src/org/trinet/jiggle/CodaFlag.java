package org.trinet.jiggle;

import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Color;
import java.awt.Polygon;
import java.util.*;

/**
* Line bar plus curve graphic to show coda tau and decay.<p>
*
* Empty triangle means unassociate. Filled means associated.
*
* Colors are the same as for picks and are coded to the associated solution. <br>
*/

public class CodaFlag {

    // Default size of symbol in pixels
    final static int minWidth = 8;
    final static int minHeight= 8;

    int width  = minWidth;
    int height = minHeight;

    /** Position of the flag in pixel space. Straddles coda timespan and
    slammed against the bottom of the WFPanel or viewport. */
    Point pos1 = new Point();
    Point pos2 = new Point();

    // start/end of the coda window
    TimeAmp timeAmp1, timeAmp2;
    double  winStart, winEnd;
    double windowSize = 2.; // seconds

    /** Coda represented by this flag. */
    Coda coda = null;

    /** WFPanel where flag will be drawn. */
    WFPanel wfp = null;

    /** The waveform bias. */
    double bias = 0.0;

    /** Current color of the flag. */
    Color color = Color.pink;

    public final static int Bar  = 0;
    public final static int Free = 1;
    public final static int Fix  = 2;

    int flagType = Free;

    protected static boolean showExtrapolation = true; // aww made static for universal toggle on/off

    /**
     * Create an coda flag representing this Coda
     * to be displayed in this WFPanel. Specify the flag type. Options are:
     * CodaFlag.Bar, CodaFlag.Free, or CodaFlag.Fix. Default is Free.
     */
     public CodaFlag(WFPanel wfp, Coda coda, int type) {
       this(wfp, coda);
       setType(type);
     }

    /**
     * Create an coda flag representing this Coda
     * to be displayed in this WFPanel.
     */
     public CodaFlag(WFPanel wfp, Coda coda) {

        this.wfp  = wfp;
        this.coda = coda;

        // lookup origin's color code
        Solution sol = coda.getAssociatedSolution();
        color = wfp.wfv.mv.solList.getColorOf(sol);

        windowSize = coda.windowSize.doubleValue(); // usually 2 sec
        if (wfp.getWf() != null) bias = wfp.getWf().getBias();

        // Get the first and last time windows to define the total wf scanned
        // NOTE: CodaTN only saves the first and last so there will only be 2 in the dbase

        //double ptime = coda.getTime();
        double ptime = getCodaTime(); // Kludge here for RT 2009/10/28

        double t1 =  0;
        double t2 =  0;

        ArrayList list = (ArrayList) coda.getWindowTimeAmpPairs();
        if (list.size() > 0) {
            // First window
            timeAmp1 = (TimeAmp) list.get(0) ;
            t1 =  timeAmp1.getTimeValue();

            // Is window time seconds post 1970 nominal time? If so, assume absolute times
            // NOTE: when coda stores times as absolute times set ptime = 0. here
            if (t1 > 3600.) { // 1 hour seconds of duration
              ptime = 0; // enabled 08/11/2004 -aww
            }

            // last window
            timeAmp2 = (TimeAmp) list.get(list.size() - 1) ;
            // time of coda amp is at window mid-point ends plus halfWidth - aww
            t2 =  timeAmp2.getTimeValue();
        }

        // time of coda amp is at window mid-point starts less halfWidth - aww
        winStart = ptime + t1 - windowSize/2.;
        winEnd   = ptime + t2 + windowSize/2.;

        /*
            System.out.println ("CodaFlag<: "+org.trinet.util.LeapSeconds.trueToString(ptime)+  "  "+
                               timeAmp1.getTimeValue() + " "+timeAmp1.getAmpValue()); // for UTC time -aww 2008/02/10
            System.out.println ("CodaFlag>: "+org.trinet.util.LeapSeconds.trueToString(ptime)+  "  "+
                               timeAmp2.getTimeValue() + " "+timeAmp2.getAmpValue()); // for UTC time -aww 2008/02/10
        */
     }

     // Kludge here for RT 2009/10/28
     double codaTime = Double.NaN;
     private double getCodaTime() {
         if (Double.isNaN(codaTime)) { 
            double ptime = coda.getTime();
            if (coda.getSource().startsWith("RT") || ptime == 0.) { // logic added 2009/10/23 -aww to handle 0 datetime case ?
              TravelTime tt = TravelTime.getInstance();
              Solution sol = coda.getAssociatedSolution();
              ptime = tt.getTTp(wfp.wfv.getHorizontalDistance(), sol.getModelDepth()); // use model depth for TT -aww 2015/10/09
              ptime += sol.datetime.doubleValue(); // change from traveltime to absolute epoch time
              // Find phase in phaselist for this channel closest to predicted time
              Phase ph = (Phase) wfp.wfv.phaseList.getNearestToTime(ptime, sol);
              if (ph != null && ph.description.iphase.equals("P")) {
                  ptime = ph.getTime();
                //System.out.println("DEBUG CodaFlag ph: " +ph.getChannelObj().toDelimitedSeedNameString(".")+ " getTime(): "+org.trinet.util.LeapSeconds.trueToString(ptime));
              }
            }
            codaTime = ptime;
         }
         return codaTime;
     }

     /** Set the type of flag symbol to draw. Options are:
     * CodaFlag.Bar, CodaFlag.Free, or CodaFlag.Fix; Default is Free. */
     public void setType(int type) {
       // check its a valid value
       if (type >= 0 && type <= Fix) flagType = type;
     }
     /** Get the type of flag symbol to draw. Options are:
     * CodaFlag.Bar, CodaFlag.Free, CodaFlag.Fix; Default is Free. */
     public int getType() {
       return flagType;
     }

     /** If set true, will show amp-time curve extrapolation before the first fit window. */
     public void setShowExtrapolation(boolean tf) {
       showExtrapolation = tf;
     }
     /** Returns true if flag will show amp-time curve extrapolation before the first fit window. */
     public boolean setShowExtrapolation() {
       return showExtrapolation;
     }
/*
 * Draw the CodaFlag in the Graphics window. Check the Viewport limits to insure
 * that the flag is always in view in a zoomable WFPanel. If isDeleted()
 * == true the flag will NOT be drawn. */
// TODO: handle case where flags overlap

    public void draw(Graphics g) {

        // don't draw a delete phase (later may want to add ability to show these)
        if (coda.isDeleted()) return;

       int flag = flagType;
       String desc = coda.getPhaseDescription();
       if (flag == Free && desc.length() >= 3) { // if extrapolated reset to fix or free
         char tauType = coda.getPhaseDescription().charAt(2);
         if (tauType == AbstractCodaGenerator.TAU_EXTRAPOLATED_FREE) flag = Free;
         else if (tauType == AbstractCodaGenerator.TAU_EXTRAPOLATED_FIXED) flag = Fix;
       }
       //System.out.println("Debug CodaFlag draw flag= " + flag + " " + coda.getChannelObj().toDelimitedSeedNameString());

       if (flag == Bar) {
         drawBar(g);
       }
       else if (flag == Fix) {
         if (! drawFixCurve(g)) drawTau(g, "0");
       }
       else if (flag == Free) {
         if (! drawFreeCurve(g)) {
           if (! drawFixCurve(g)) drawTau(g, "0");
         }
       }
     }

/** Draw coda time window as a bar at the bottom of the WFPanel. */
     void drawBar(Graphics g) {
        // MUST RECALC. THIS EVERY TIME BECAUSE FRAME MIGHT HAVE RESCALED!
        pos1.x = wfp.pixelOfTime(winStart);
        pos2.x = wfp.pixelOfTime(winEnd);

        // If the wfPanel is in a ViewPort, keep flag within viewport so we can see it
//       if (wfp.getViewport() != null) {
//          pos1.y = wfp.getViewport().getViewPosition().y;
//       } else {
//         pos1.y = wfp.getHeight();        // bottom of panel
//       }
//       pos2.y = pos1.y;

        pos2.y = pos1.y = wfp.getHeight(); // dg added // aww rev 11/24

        Color oldColor = g.getColor();
        g.setColor(color);

//        System.out.println ("CodaFlag<<: "+ pos1.x + " "+pos1.y);
//        System.out.println ("CodaFlag>>: "+ pos2.x + " "+pos2.y);

       // this makes a point-down triangle at the bottom of the WFPanel
       int wid = width/2;
       Polygon triangle = new Polygon();
       triangle.addPoint(pos1.x, pos1.y);
       triangle.addPoint(pos1.x - wid, pos1.y - height);
       triangle.addPoint(pos1.x + wid, pos1.y - height);

        // fill if it is "used"
        if (coda.windowCount.longValue() > 1) {
          g.fillPolygon(triangle);
        } else {
          g.drawPolygon(triangle);
        }
        g.drawLine(pos1.x, pos1.y - height, pos2.x, pos2.y - height);
        g.setColor(oldColor);
    }

/**
 * Draw coda as a decay curve at the bottom of the WFPanel.
 * NOTE: this won't work if there is no waveform. The waveform is needed to
 * properly scale the coda dimension of the WFPanel.
 */
     boolean drawFixCurve(Graphics g) {
       String str = (coda.descriptor.getDescription().length() < 3) ?
           "+" : coda.descriptor.getDescription().substring(2,3);
       //System.out.println("FIX : a,q:" +coda.aFix.doubleValue() +", "+ coda.qFix.doubleValue());
       return drawCurve(g, coda.aFix.doubleValue(), coda.qFix.doubleValue(), str);
    }
/**
 * Draw coda as a decay curve at the bottom of the WFPanel.
 * NOTE: this won't work if there is no waveform. The waveform is needed to
 * properly scale the coda dimension of the WFPanel.
 */
     boolean drawFreeCurve(Graphics g) {
       String str = (coda.descriptor.getDescription().length() < 3) ?
           "-" : coda.descriptor.getDescription().substring(2,3);
       //System.out.println("FREE: a,q:" +coda.aFree.doubleValue() +", "+ coda.qFree.doubleValue());
       return drawCurve(g, coda.aFree.doubleValue(), coda.qFree.doubleValue(), str);
     }

     private boolean drawTau(Graphics g, String label) {
         double tau = coda.getTau();
         if (Double.isNaN(tau) || tau <= 0.) {
             return false;
         }

         //double ptime = coda.getTime();
         double ptime = getCodaTime(); // Kludge here for RT 2009/10/28

         // draw waveform "scanned" data curve fit
         Color oldColor = g.getColor();
         g.setColor(color);
 
         //buffers hold time,amp values in pixels plotted by graphics
         int x[] = new int[1];
         int y[] = new int[1];

         // mark tau value with a line segment 40 pixels height
         x[0] = wfp.pixelOfTime(ptime + tau);
         y[0] = wfp.pixelOfAmp(bias);

         int dy = wfp.getHeight()/8; // 12% of height
         g.drawLine(x[0]+1,y[0]+dy,x[0]+1,y[0]-dy);
         g.drawString(label,x[0]-3, y[0] - 3*dy/2);
         if (coda.contributes())
             g.drawLine(x[0],0,x[0],wfp.getHeight());
         else 
             g.drawLine(x[0],y[0] + 5*dy/4,x[0],y[0] - 5*dy/4);

         g.drawLine(x[0]-1,y[0]+dy,x[0]-1,y[0]-dy);

         // restore original color to graphics
         g.setColor(oldColor);

         return true;
     }
/**
 * Draw coda as a decay curve at the bottom of the WFPanel.
 * NOTE: this won't work if there is no waveform. The waveform is needed to
 * properly scale the coda dimension of the WFPanel.
 */
     boolean drawCurve(Graphics g, double Aval, double Qval, String label) {

 //     if (coda.aFree.doubleValue() == 0 || coda.qFree.doubleValue() == 0) return;
      if (Aval == 0.0 || Qval == 0.0 || Double.isNaN(Aval) || Double.isNaN(Qval)) {
          //System.out.println("CodaFlag A or Q input value(s) are 0 or NaN  A:" +Aval+ " Q:" +Qval+ " for type: " +label + " " + coda.getChannelObj().toDelimitedSeedNameString());
          return false;
      }

      // null implies no data value to plot here 
      if (timeAmp1 == null || timeAmp2 == null) {
          //System.out.println("CodaFlag drawcurve failed timeamp1 or timeamp2 is null " + coda.getChannelObj().toDelimitedSeedNameString());
          return false;
      }

      //double ptime = coda.getTime();
      double ptime = getCodaTime(); // Kludge here for RT 2009/10/28

      // time base for coda is either seconds after P-wave or absolute nominal time
      double t1 = timeAmp1.getTimeValue();
      double t2 = timeAmp2.getTimeValue();

      // Is window time seconds post 1970 nominal time? If so assume absolute
      if (t1 > 3600.) { // 1 hour seconds of duration
        // NOTE: if absolute times are used in TimeAmp use timeAmpTime - ptime
        t1 -= ptime; // enabled 8/11/2004 -aww
        t2 -= ptime; // enabled 8/11/2004 -aww
      }

      // Span of data 
      double span = (t2 - t1 + windowSize);
      int nsecs  = (int) span;
      // npts to plot should be at least 1 window, or 2 pts
      int npts = nsecs + 1;
      if (npts < 2) {
          //System.out.println("CodaFlag drawcurve failed span " + npts + " is too short < 2 secs " + coda.getChannelObj().toDelimitedSeedNameString());
          return false;
      }
      // put more details in curves for shorter tau, those starting near with  t1 < 2 -aww 2009/10/28
      if (coda.getTau() <= 33. && t1 < 2. && npts < 33) npts = 33;

      // time increment between points, 1 per second over the span
      double deltaT = span/(npts-1);
      // start time
      double ampTime   = t1 - windowSize/2.;

      // init amp of point at extrapolation time 
      double amp = 0.;

      // scalar amp scaling term
      double term1 =  Math.pow(10.0, Aval);

      //buffers hold time,amp values in pixels plotted by graphics
      int x[] = new int[npts];
      int y[] = new int[npts];

      for (int i = 0; i < npts; i++) {
        amp = term1 * Math.pow(ampTime, -Qval) + bias;
        // note time is absolute time
        x[i] = wfp.pixelOfTime(ptime + ampTime);
        y[i] = wfp.pixelOfAmp(amp);
        ampTime += deltaT;
      }

      // draw waveform "scanned" data curve fit
      Color oldColor = g.getColor();
      g.setColor(color);
      g.drawPolyline(x, y, npts);

      // Extrapolate the curve fit before/after he scanned data?
      if (showExtrapolation) {
        //g.setColor(Color.pink);// color too hard to see, like red - aww
        // use black to discriminate extrapolation from the scan fit - aww
        g.setColor(Color.black);

        //Span of extrapolation before scan fit
        ampTime  = 1.0; // 1 sec past ptime, intercept log-log log10(1.) = 0.
        span =  (t1 - windowSize/2. - ampTime);
        nsecs  = (int) (span * 2.);
        npts = nsecs + 1;
        if (npts > 1) {
          deltaT = span/nsecs;
          x = new int[npts];
          y = new int[npts];

          for (int i = 0; i < npts; i++) {
            amp = term1 * Math.pow(ampTime, -Qval) + bias;
            x[i] = wfp.pixelOfTime(ptime + ampTime);
            y[i] = wfp.pixelOfAmp(amp);
            ampTime += deltaT;
          }
          g.drawPolyline(x, y, npts);
        }

        // Extrapolate curve fit to tau if tau is valid value > t2 (end of scan).
        double tau = coda.getTau();
        if (! Double.isNaN(tau)) {
          ampTime = t2;
          // Span of extrapolation after scan fit
          span =  (tau - ampTime); 
          nsecs  = (int) span;
          npts = nsecs + 1;
          if (npts > 1) {
            deltaT = span/nsecs;
            x = new int[npts];
            y = new int[npts];

            for (int i = 0; i < npts; i++) {
              amp = term1 * Math.pow(ampTime, -Qval) + bias;
              x[i] = wfp.pixelOfTime(ptime + ampTime);
              y[i] = wfp.pixelOfAmp(amp);
              ampTime += deltaT;
            }
            g.drawPolyline(x, y, npts);
          }

          // mark tau value with a line segment 40 pixels height
          amp = term1 * Math.pow(tau, -Qval) + bias;
          x[0] = wfp.pixelOfTime(ptime + tau);
          y[0] = wfp.pixelOfAmp(amp);
          int dy = wfp.getHeight()/8; // 12% of height
          g.setColor(color);
          g.drawLine(x[0]+1,y[0]+dy,x[0]+1,y[0]-dy);
          g.drawString(label,x[0]-3, y[0] - 3*dy/2);
          if (coda.contributes())
              //g.drawLine(x[0],y[0] + 5*dy/4,x[0],y[0] - 5*dy/4);
              //g.drawLine(x[0],y[0],x[0],wfp.getHeight());
              g.drawLine(x[0],0,x[0],wfp.getHeight());
          else 
              g.drawLine(x[0],y[0] + 5*dy/4,x[0],y[0] - 5*dy/4);

          g.drawLine(x[0]-1,y[0]+dy,x[0]-1,y[0]-dy);

        }

         /* this makes a point-down triangle at the bottom of the WFPanel
         int wid = width/2;
         Polygon box = new Polygon();
         box.addPoint(x[0] - width/2, wfp.getHeight());
         box.addPoint(x[0] - width/2, wfp.getHeight() - height);
         box.addPoint(x[0] + width/2, wfp.getHeight() - height);
         box.addPoint(x[0] + width/2, wfp.getHeight());

          // fill if it is "used"
          if (coda.contributes()) {
              g.fillPolygon(triangle);
          } else {
              g.drawPolygon(triangle);
          }
          */
      }
      // restore original color to graphics
      g.setColor(oldColor);
      return true;
    }
} // end of CodaFlag
