package org.trinet.jiggle;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

import org.trinet.jasi.*;
import org.trinet.util.graphics.text.JTextClipboardPopupMenu;

/** Creates a centered, modal dialog box for entering a comment
text string. If the dialog is cancelled the resulting string is set null.
No limit is put on the size of the string but if it's longer then the text field
it will scroll.  */
public class CommentDialog extends CenteredDialog{

    private JPanel panel1 = new JPanel();
    private BorderLayout borderLayout1 = new BorderLayout();
    private JPanel jPanel1 = new JPanel();
    private JPanel jPanel2 = new JPanel();
    private JButton okButton = new JButton();
    private JButton cancelButton = new JButton();
    //private JTextField textField = new JTextField();
    private JTextArea textField = new JTextArea(2,80);

    private static final String title = "Add/Edit Comment";
    private static final boolean modal = true;
    private String commentString = null;
    private JButton deleteButton = new JButton();

    public CommentDialog(String initComment) {
        super((JFrame) null, title, modal);
        commentString = initComment;
        try  {
            jbInit();
            pack();
            centerDialog();
        }
        catch(Exception ex) {
            ex.printStackTrace();
    }
        setVisible(true);
    }

    public CommentDialog() {
        this("");
    }

    private void jbInit() throws Exception {

        panel1.setLayout(borderLayout1);
        // [OK] button
        okButton.setText("OK");
        okButton.setToolTipText("Set comment to text");
        okButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                finished_actionPerformed(e);
            }
        });
        //[CANCEL] button
        cancelButton.setText("CANCEL");
        cancelButton.setToolTipText("Do not change comment");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                returnValue = JOptionPane.CANCEL_OPTION;
                commentString = null;
                setVisible(false);            // dismiss dialog
            }
        });
        // the textfield
        textField.setMinimumSize(new Dimension(100, 21));
        textField.setPreferredSize(new Dimension(300, 21));
        if (commentString != null) textField.setText(commentString);
        /* NO listener when JTextArea with multiple lines
        textField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                finished_actionPerformed(e);
            }
        });
        */
        new JTextClipboardPopupMenu(textField);

        deleteButton.setText("DELETE");
        deleteButton.setToolTipText("Remove this comment");
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
                returnValue = JOptionPane.NO_OPTION;
                commentString = null;
                textField.setText("");
                setVisible(false);            // dismiss dialog
            }
        });

        getContentPane().add(panel1);
        panel1.add(jPanel1, BorderLayout.CENTER);
        jPanel1.add(new JScrollPane(textField), null);
        panel1.add(jPanel2, BorderLayout.SOUTH);
        jPanel2.add(okButton, null);
        jPanel2.add(deleteButton, null);
        jPanel2.add(cancelButton, null);
    }

    /** Return the comment string. */
    public String getString () {
        return commentString;
    }

    /** Either the OK button or carriage return in the text field */
    private void finished_actionPerformed(ActionEvent e) {
        returnValue = JOptionPane.OK_OPTION;
        commentString = textField.getText();
        setVisible(false);            // dismiss dialog
    }
}
