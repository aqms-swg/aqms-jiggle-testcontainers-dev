package org.trinet.jiggle;

import java.util.*;
import java.awt.*;
import java.text.*;

import org.trinet.jasi.Channel;
import org.trinet.jasi.ChannelGain;
import org.trinet.jasi.Waveform;
import org.trinet.jasi.Units;
import org.trinet.util.DateTime;

/**
 * Observable "model" that passes changes to the cursor location in an ActiveWFPanel.

 * Model:	This class. <p>
 *
 * Controllers: <p>
 *   1) ActiveWFPanel <p>
 *
 * Views: <p>
 *   1) CursorLocPanel <p>
 */

public class CursorLocModel extends Observable {

    Point cursorLoc = null;

    ActiveWFPanel wfp = null;

    double gn = Double.NaN;
    int gn_units = 0;

    Waveform lastWf = null; 

    static boolean includePhyUnits = true;

    public CursorLocModel() {
    }

    /**
     * Set the new cursor loc; notify observers. Needs whole WFPanel because it
     * needs both the cursorLocation and a reference to timeOfPixel() for a
     * particular WFPanel, otherwise it couldn't convert pixel location to time/amp.
     */
    public void set(ActiveWFPanel wfp, Point cursorLoc) {

        this.cursorLoc = cursorLoc;

        setWFPanel(wfp);

        setChanged();
        notifyObservers(this);   // pass wfv to observers
    }

    protected void setWFPanel(ActiveWFPanel wfp) {

        this.wfp = wfp;

        if (! includePhyUnits) return;

        Waveform wf = wfp.getWf();
        if ( wf != null ) {
          if (!wf.equalsChannelId(lastWf) ) {
            DateTime dt = new DateTime(wf.getEpochStart(), true);
            ChannelGain gain = wf.getChannelObj().getGain(dt, Channel.getDbLookup());
            if (!gain.isNull()) {
              gn_units = ChannelGain.getResultingUnits(gain);
              gn = Math.abs(Units.convertFromCommon(gain.doubleValue(), gn_units));
            }
            else {
              gn = Double.NaN;
              gn_units = 0;
            }
            //System.out.println("Check gain units " + gn_units);
            lastWf = wf;
          }
          // else keep it the same
        }
        else {
            lastWf = null;
            gn = Double.NaN;
            gn_units = 0;
        }

    }

    /** Returns a Point object specifying the x,y coordinates in pixels of the current cursor location. */
    public Point getCursorLoc() {
        return cursorLoc;
    }
} // end of class
