package org.trinet.jiggle;

import java.awt.*;
import java.text.*;
import java.util.*;
import java.math.*;

import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import org.trinet.jasi.*;

import org.trinet.util.LeapSeconds;

/*
 * Make a small panel with text areas that display the current
 * currsor location in both time and amp units and cursor x/y.
 * Automatically adds itself as an observer
 * to the CursorLocModel.
 */
public class CursorLocPanel extends JPanel implements Observer  {
/* NOTE: Originally used JLabels not JTextFields but the setText()
 * method cause a repaint of the ZoomableWFPanel that flickered like hell.
 * I think the setText() method caused a validate that worked up the
 * component hierarcy causing the repaint.
 */

    // The strings set the initial size of the text fields
    JTextField timeField = new JTextField(" xx:xx:xx.xx ");
    JTextField ampField  = new JTextField("-99999999", 10);
    JTextField ampField2  = new JTextField("-99.999999", 10);

    JLabel timeLabel = new JLabel("time");
    JLabel ampLabel  = new JLabel("cnts");
    JLabel ampLabel2  = new JLabel("");

    JTextField xyField;
    JLabel xyLabel;

    String fontType = "Monospaced";
    int fontStyle   = Font.PLAIN;
    int fontSize    = 12;                // point

    Font font = new Font(fontType, fontStyle, fontSize);
    Color forgroundColor = Color.red;
    Color backgroundColor = Color.white;

    DecimalFormat ampFormat = new DecimalFormat("0"); // integer
    DecimalFormat ampFormat2 = new DecimalFormat("0"); // integer

    static boolean includePhyUnits = true; // option to include physical units field
    static boolean includeXY = false; // option to include pixel XY field

    CursorLocModel cursorLocModel = null; 

    ActiveWFPanel wfp = null;

/**
 * Constructor
 */
  public CursorLocPanel(CursorLocModel cursorLocModel) {

      this.cursorLocModel = cursorLocModel;

      // the field objects
      if (includeXY) {
          xyField   = new JTextField(" XXXXX/YYYYY ", 10);
          xyLabel   = new JLabel("x/y");
          xyField.setForeground(forgroundColor);
          xyField.setBackground(backgroundColor);
          xyField.setEditable(false);
          xyField.setFont(font);
          xyField.setText(" XXXXX/YYYYY ");
          xyField.setMinimumSize(xyField.getSize());
     }

      timeField.setForeground(forgroundColor);
      timeField.setBackground(backgroundColor);
      timeField.setEditable(false);
      timeField.setFont(font);
      timeField.setText(" xx:xx:xx.xx ");
      //timeField.setMinimumSize(timeField.getSize());
      timeField.setMinimumSize(new Dimension(30, (int)timeField.getSize().getHeight()));

      ampField.setForeground(forgroundColor);
      ampField.setBackground(backgroundColor);
      ampField.setEditable(false);
      ampField.setFont(font);
      //ampField.setText("-xxxxxxxxxxxx");
      ampField.setText(ampFormat.format(0));
      ampField.setMinimumSize(new Dimension(30, (int)ampField.getSize().getHeight()));

      //if (includePhyUnits) {
          ampField2.setForeground(forgroundColor);
          ampField2.setBackground(backgroundColor);
          ampField2.setEditable(false);
          ampField2.setFont(font);
          //ampField2.setText("-xxxxxxxxxxxx");
          ampField2.setText(ampFormat2.format(0));
          ampField2.setMinimumSize(new Dimension(30, (int)ampField.getSize().getHeight()));
      //}

      add(timeLabel);
      add(timeField);

      add(ampLabel);
      add(ampField);

      add(ampField2);
      add(ampLabel2);

      if (includeXY) {
          add(xyLabel);
          add(xyField);
      }

      setShowPhyUnits(includePhyUnits);

      // add self to model's observers
      cursorLocModel.addObserver(this);

    }

    public static void setIncludePhyUnits(boolean tf) {
        CursorLocPanel.includePhyUnits = tf;
        CursorLocModel.includePhyUnits = tf;
    }

    public void setShowPhyUnits(boolean tf) {
        ampField2.setVisible(tf);
        ampLabel2.setVisible(tf);
    }

    /** Set the amp units and format. Use the value to determine the format. */
    public void setAmpFormat(int units, double value) {

        ampLabel.setText(Units.getString(units)); // aww 2007/11/15 should be Units.getString not AmpType.getString
        ampFormat  = getAmpFormat(value);

        if (includePhyUnits) {
            ampFormat2 = ampFormat;
            if (cursorLocModel.wfp.getWf().getAmpUnits() == Units.COUNTS) {
              if (cursorLocModel.wfp.getWf() != null) {
                  if (!Double.isNaN(cursorLocModel.gn)) {
                      ampFormat2 = getAmpFormat( value/cursorLocModel.gn );
                      ampLabel2.setText(Units.getString(cursorLocModel.gn_units));
                  }
                  else {
                      ampLabel2.setText("");
                  }
              }
              else {
                  ampField2.setText("");
                  ampLabel2.setText("");
              }
            }
        }
    }

    //
    public static DecimalFormat getAmpFormat(double value) {
         // construct a format
         double test = Math.abs(value);
         //System.out.println("getAmpFormat test value: " + value);
         DecimalFormat dFormat = null;
         if (test < 0.001) dFormat =  new DecimalFormat( "#0.###E0" );
         else if (test < 0.01) dFormat =  new DecimalFormat( "#0.######" );
         else if (test < 0.1) dFormat =  new DecimalFormat( "#0.####" );
         else if (test <= 1) dFormat =  new DecimalFormat( "#0.###" );
         else if (test <= 10) dFormat =  new DecimalFormat( "#0.##" );
         else if (test <= 100) dFormat =  new DecimalFormat( "#0.#" );
         else dFormat = new DecimalFormat("#0"); // integer

         return dFormat;
    }
    //

    /**
     * Model-View-Controller model. Note that the WFPanel class is both
     * Controller & View.
     */
    public void update(Observable obs, Object arg) {

        if (arg instanceof CursorLocModel )  {
            CursorLocModel model = (CursorLocModel) arg;
            setFields(model);
        }
    }

/**
 * Calc. the cursor position fields, need WFPanel for proper time decoding
 */
    void setFields(CursorLocModel model) {

        if (model.getCursorLoc() == null) return;
        if (model.wfp == null || model.wfp.getWf() == null) {
            timeField.setText("");
            ampField.setText("");
            ampField2.setText("");
            return;
        }

        double epochSec  = model.wfp.dtOfPixel(model.getCursorLoc());
        double ampCounts = model.wfp.ampOfPixel(model.getCursorLoc());

        /*NOTE: the "S" format of SimpleDateFormat returns milliseconds
        * as an integer. This is prone to unexpected formatting results.
        * For example, if the seconds part of a time = 12.03456. the "ss.SS" format returns "12.34" and
        * "ss.SSSS" reuturns "12.0034". Only "ss.SSS" gives a correct result.*/
        //String str = LeapSeconds.trueToString(epochSec + 0.005, "HH:mm:ss.SSS");  // +0.005 to round // for UTC time -aww 2008/02/10
        String str = LeapSeconds.trueToString(epochSec, "HH:mm:ss.fff");  // no need to round for UTC time with fff -aww 2015/11/20
        str = str.substring(0, 11);     // lop off the last digit to get "HH:mm:ss.SS"
        timeField.setText(str);         // write result to field

        str = ampFormat.format(ampCounts); // cheat to convert int -> string
        ampField.setText(str);  // String.format("%+d",(long)Math.round(ampCounts))          // write result to field

        if (includePhyUnits) {
          if (cursorLocModel.wfp.getWf().getAmpUnits() == Units.COUNTS) {
            if (!Double.isNaN(model.gn)) {
                ampField2.setText(ampFormat2.format(ampCounts/model.gn));
                //ampField2.setText( String.format("%+6.4g",ampCounts/model.gn));
            }
          }
        }

        // x,y field
        if (includeXY) {
            str = " " + model.getCursorLoc().getX() + "/" + model.getCursorLoc().getY();
            xyField.setText(str);        // write result to field
        }
    }

} // end of class
