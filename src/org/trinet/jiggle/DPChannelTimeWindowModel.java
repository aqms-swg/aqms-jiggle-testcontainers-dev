package org.trinet.jiggle;

import java.awt.*;
import java.util.Collection;
//import java.util.ArrayList;
import javax.swing.*;
import javax.swing.border.*;

import java.awt.event.*;
import java.util.*;

import java.util.ArrayList;
import org.trinet.jasi.*;

import org.trinet.util.Format;              // CoreJava printf-like Format class
import org.trinet.util.graphics.text.JTextClipboardPopupMenu;

/** A Dialog panel for choosing the ChannelTimeWindowModel.
 * The  ChannelTimeWindowModel are instantiated by and contained withing
 * the JiggleProperties class.*/
// This GUI component was built using JBuilder's design tool.

public class DPChannelTimeWindowModel extends JPanel {

    private JiggleProperties newProps;

    /** The currently selected model */
    private ChannelTimeWindowModel selectedModel = null;

    private JTextArea textArea = null;
    private JComboBox modelBox = null;

    /** Allow selection of waveform reading mode. */
    public DPChannelTimeWindowModel(JiggleProperties properties) {
        newProps = properties;
        try  {
            initGraphics();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    /** Build the GUI*/
    private void initGraphics() throws Exception {

        this.setLayout(new BorderLayout());
        Box vbox = Box.createVerticalBox();
        vbox.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white,new Color(148, 145, 140)),"Current ChannelTimeWindowModel"));
        JComponent jsp = makeTextArea(); // need to do this first since combobox setup configures textArea text

        vbox.add(makeModelBox()); // at top
        vbox.add(jsp); // center
        //vbox.add(makeButtonBox()); // bottom
        this.add(vbox, BorderLayout.CENTER);
    }

    /** Fill the JComboBox with the items in the wsgList which is the list of available models. */
    private JComponent makeModelBox() {
        modelBox = new JComboBox();
        modelBox.setMaximumSize(new Dimension(148,24));
        modelBox.setToolTipText("Choose current model for time windows, model properties are shown in text area");

        Box hbox = Box.createHorizontalBox(); // new JPanel();
        if (newProps.getChannelTimeWindowModelList() != null) {
          createCTWList();            // populate the box from the list

          // add AFTER box's model is populated else adding items fires events
          modelBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(ActionEvent e) {
               JComboBox src = (JComboBox) e.getSource();
               ChannelTimeWindowModel ctwm = (ChannelTimeWindowModel) src.getSelectedItem();
               if (ctwm != null) setSelectedModel(ctwm);
            }
          });

        } else {
          // This should never happen. JiggleProperties defines a default
          textArea.setText("No models defined. \n"+
                    "Add 'channelTimeWindowModelList' and 'currentChannelTimeWindowModel'\n"+
                    "to your properties file.");
        }

        JLabel modelBoxLabel = new JLabel("Model Name: ");
        modelBoxLabel.setToolTipText("Choose current model for time windows, model properties are shown in text area");
        modelBoxLabel.setMaximumSize(new Dimension(80,24));

        hbox.add(Box.createHorizontalGlue());
        hbox.add(modelBoxLabel);
        hbox.add(modelBox);
        hbox.add(Box.createHorizontalStrut(10));
        hbox.add(makeButtonBox());
        hbox.add(Box.createHorizontalGlue());
        hbox.setMaximumSize(new Dimension(300, 28));

        return hbox;
    }

    private JComponent makeTextArea() {
        textArea = new JTextArea();
        //textArea.setRows(25);
        //textArea.setColumns(80);
        //textArea.setPreferredSize(new Dimension(300,200));
        //textArea.setMaximumSize(new Dimension(300,200));
        textArea.setEditable(true); // until we come up with property parser for text -aww 09/20/2007
        textArea.setBackground(Color.white);
        textArea.setToolTipText("Edit text to add, remove, or change a property, you must press \"Set\" button to save edits");
        new JTextClipboardPopupMenu(textArea);
        JScrollPane jsp = new JScrollPane(textArea);
        jsp.setPreferredSize(new Dimension(325,325));
        jsp.setMaximumSize(new Dimension(525,325));

        return jsp;
    }

    private JComponent makeButtonBox() {
        JButton setButton = new JButton("Set");
        setButton.setMaximumSize(new Dimension(32,24));
        setButton.setToolTipText("Set the current model's properties from text area.");
        setButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String str = textArea.getText();
                ChannelTimeWindowModel ctwModel = newProps.getCurrentChannelTimeWindowModel();
                ArrayList list = ctwModel.getKnownPropertyKeys();
                // must also purge cloned jiggle properties in case text area list is missing known key value pairs
                for (int ii = 0; ii < list.size(); ii++) newProps.remove(list.get(ii));
                // add text area list to the jiggle properties clone via method parse key value pairs from string
                newProps.setProperties(str);
                // reset window model to defaults in case revised text area list is now missing previously set key value pairs
                ctwModel.setDefaultProperties();
                // now reset the model with the new jiggle values properties
                ctwModel.setProperties(newProps); // test this -aww
            }
        });

        //Box hbox = Box.createHorizontalBox();
        //hbox.add(Box.createHorizontalGlue());
        //hbox.add(setButton);
        //hbox.add(Box.createHorizontalGlue());
        //return hbox;

        return setButton;
    }

    /** Extract the ChannelTimeWindowModel objects from JiggleProperties for display in the JComboBox. */
    private void createCTWList() {

        ChannelTimeWindowModelList modelList = newProps.getChannelTimeWindowModelList();
        if (modelList.size() == 0) { // empty list so create a default model
            modelList.add("org.trinet.jasi.DataSourceChannelTimeModel", newProps);
            modelList.add("org.trinet.jasi.PowerLawTimeWindowModel", newProps);
            modelList.add("org.trinet.jasi.TriggerChannelTimeWindowModel", newProps);
            if (newProps != null) newProps.setChannelTimeWindowModel("org.trinet.jasi.DataSourceChannelTimeModel");
        }

        // populate the box with Models
        // NOTE: the model.toString() method is called to make the JComboBox labels
        ChannelTimeWindowModel model = null;
        // for (int i = 0; i<modelClassList.length; i++) {
        for (Iterator it=modelList.iterator(); it.hasNext(); ) {
          model =  (ChannelTimeWindowModel) it.next();
          if (model != null) {
            modelBox.addItem(model);
            // THIS SELECTS IT! add to selection box
            if (model.getClassname().equals(newProps.getCurrentChannelTimeWindowModelClassName())) {
              setSelectedModel(model);
              modelBox.setSelectedItem(model);
            }
          }
        }

    }


    /** Set selected model to this one.*/
    protected void setSelectedModel(ChannelTimeWindowModel model) {
        if (model != selectedModel) {
          selectedModel = model;
          setModelText(model);
          // change it in the properties
          newProps.setChannelTimeWindowModel(model.getClassname());
        }
    }

/*
    protected void setSelectedModel(String modelName) {
        ChannelTimeWindowModel model = ChannelTimeWindowModel.getInstance(modelName);
        if (model != null) setSelectedModel(model);
    }
*/
    /** Return the name of the selected model. */
    public ChannelTimeWindowModel getSelectedModel() {
        return selectedModel;
    }

/** Populate the text area. Example:
*  <tt>
*  DataSource Model
*  org.trinet.jasi.DataSourceChannelTimeModel
*  Channels defined by dbase associations.
*  </tt>
*/
    protected void setModelText(ChannelTimeWindowModel model) {
        StringBuffer sb = new StringBuffer(256);
        sb.append("#").append(model.getModelName()).append("\n");
        sb.append("#").append(model.getClass().getName()).append("\n");
        sb.append("#").append(model.getExplanation()).append("\n");
        sb.append("#").append("-- Properties --\n");
        sb.append(model.getParameterDescriptionString()).append("\n");
        textArea.setText(sb.toString());
        textArea.setCaretPosition(0);
    }
}
