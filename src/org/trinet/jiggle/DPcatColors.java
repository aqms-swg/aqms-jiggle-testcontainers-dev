package org.trinet.jiggle;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.trinet.jasi.ActiveList;
import org.trinet.util.graphics.table.CatalogColor;
import org.trinet.util.graphics.table.CatalogUtils;

/**
 * Panel for insertion into dialog box for selecting catalog row colors based
 * upon event type (etype), origin (gtype), subsource (RT, Jiggle), processing
 * state (rflag) etc.
 */
public class DPcatColors extends JPanel {
    private class CatalogRowColorChooserPanel extends JPanel {
        private class CatalogColorCategoryAction implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                String cmd = e.getActionCommand();
                String value = "0";
                if (cmd.equals("Event etype")) {
                    value = "1";
                } else if (cmd.equals("Processing state")) {
                    value = "2";
                } else if (cmd.equals("Subsource")) {
                    value = "3";
                } else if (cmd.equals("Origin gtype")) {
                    value = "4";
                }
                colorChanged = true; // assume changed, though user may have just toggled back to original value
                newProps.setProperty("colorCatalogByType", value);
            }
        }

        private class MyColorButton extends JButton {
            private static final long serialVersionUID = 1L;

            public boolean equals(Object obj) {
                if (obj instanceof MyColorButton) {
                    return getActionCommand().equals(((MyColorButton) obj).getActionCommand());
                }
                return false;
            }
        }

        private static final long serialVersionUID = 1L;

        private ActiveList catalogButtonList = new ActiveList();
        private Color[] catColors = CatalogUtils.createColors();
        private MyColorButton lastButton = null;

        private void addEventSourceColorButtons(JComponent comp) {
            int idx = -1;
            String key = null;
            for (Enumeration<?> e = newProps.keys(); e.hasMoreElements();) { // alpha sorted order key
                key = (String) e.nextElement();
                idx = key.indexOf("color.catalog.subsrc");
                if (idx >= 0) {
                    makeColorPropertyComponent(comp, key, newProps.getColor(key)); // getColor() returns null if
                                                                                   // unparseable
                }
            }
        }

        private void addEventStateColorButtons(JComponent comp) {
            String key = "color.catalog.state.A";
            makeColorPropertyComponent(comp, key, newProps.getColor(key));
            key = "color.catalog.state.H";
            makeColorPropertyComponent(comp, key, newProps.getColor(key));
            key = "color.catalog.state.I";
            makeColorPropertyComponent(comp, key, newProps.getColor(key));
            key = "color.catalog.state.F";
            makeColorPropertyComponent(comp, key, newProps.getColor(key));
            key = "color.catalog.state.C";
            makeColorPropertyComponent(comp, key, newProps.getColor(key));
        }

        private void addEventTypeColorButtons(JComponent comp) {
            int idx = -1;
            String key = null;
            for (Enumeration<?> e = newProps.keys(); e.hasMoreElements();) { // alpha sorted order key
                key = (String) e.nextElement();
                idx = key.indexOf("color.catalog.etype");
                if (idx >= 0) {
                    makeColorPropertyComponent(comp, key, newProps.getColor(key)); // getColor() returns null if
                                                                                   // unparseable
                }
            }
        }

        private void addNewEventSourceButton(JComponent comp) {
            String input = JOptionPane.showInputDialog(getTopLevelAncestor(), "Enter new event subsource",
                    "New Event Subsource Color", JOptionPane.PLAIN_MESSAGE);

            if (input != null && input.length() > 1) { // processing state are single letters, so skip
                String label = "color.catalog.subsrc." + input;
                MyColorButton jb = null;
                boolean inList = false;
                for (int idx = 0; idx < catalogButtonList.size(); idx++) {
                    jb = (MyColorButton) catalogButtonList.get(idx);
                    if (jb.getActionCommand().equals(label)) {
                        inList = true;
                        break;
                    }
                }

                if (inList)
                    jb.doClick();
                else {
                    makeColorPropertyComponent(comp, label, Color.white);
                    comp.revalidate();
                    if (lastButton != null)
                        lastButton.doClick();
                }
            }
        }

        private void addNewEventTypeButton(JComponent comp) {
            String input = JOptionPane.showInputDialog(getTopLevelAncestor(), "Enter new event etype",
                    "New Event Type Color", JOptionPane.PLAIN_MESSAGE);

            if (input != null && input.length() > 1) { // processing state are single letters, so skip
                String label = "color.catalog.etype." + input;
                MyColorButton jb = null;
                boolean inList = false;
                for (int idx = 0; idx < catalogButtonList.size(); idx++) {
                    jb = (MyColorButton) catalogButtonList.get(idx);
                    if (jb.getActionCommand().equals(label)) {
                        inList = true;
                        break;
                    }
                }

                if (inList)
                    jb.doClick();
                else {
                    makeColorPropertyComponent(comp, label, Color.white);
                    comp.revalidate();
                    if (lastButton != null)
                        lastButton.doClick();
                }
            }
        }

        private void addNewOriginGTypeButton(JComponent comp) {
            String input = JOptionPane.showInputDialog(getTopLevelAncestor(), "Enter new origin gtype",
                    "New Origin Gtype Color", JOptionPane.PLAIN_MESSAGE);

            if (input != null && input.length() > 1) { // processing state are single letters, so skip
                String label = "color.catalog.gtype." + input;
                MyColorButton jb = null;
                boolean inList = false;
                for (int idx = 0; idx < catalogButtonList.size(); idx++) {
                    jb = (MyColorButton) catalogButtonList.get(idx);
                    if (jb.getActionCommand().equals(label)) {
                        inList = true;
                        break;
                    }
                }

                if (inList)
                    jb.doClick();
                else {
                    makeColorPropertyComponent(comp, label, Color.white);
                    comp.revalidate();
                    if (lastButton != null)
                        lastButton.doClick();
                }
            }
        }

        private void addOriginGTypeColorButtons(JComponent comp) {
            int idx = -1;
            String key = null;
            for (Enumeration<?> e = newProps.keys(); e.hasMoreElements();) { // alpha sorted order key
                key = (String) e.nextElement();
                idx = key.indexOf("color.catalog.gtype");
                if (idx >= 0) {
                    makeColorPropertyComponent(comp, key, newProps.getColor(key)); // getColor() returns null if
                                                                                   // unparseable
                }
            }
        }

        private void initGraphics() {

            Box mainVbox = Box.createVerticalBox();
            mainVbox.setBorder(BorderFactory.createTitledBorder("Catalog Row Color Selection"));

            // Color by button box
            Box hbox = Box.createHorizontalBox();
            hbox.setAlignmentX(Component.CENTER_ALIGNMENT);
            ButtonGroup bg = new ButtonGroup();
            String str = newProps.getProperty("colorCatalogByType", "0");
            hbox.add(new JLabel("Color by "));
            ActionListener al3 = new CatalogColorCategoryAction();
            JRadioButton jrb = new JRadioButton("Event etype");
            jrb.setSelected(str.equals("1"));
            jrb.addActionListener(al3);
            bg.add(jrb);
            hbox.add(jrb);
            jrb = new JRadioButton("Origin gtype");
            jrb.setSelected(str.equals("4"));
            jrb.addActionListener(al3);
            bg.add(jrb);
            hbox.add(jrb);
            jrb = new JRadioButton("Processing state");
            jrb.setSelected(str.equals("2"));
            jrb.addActionListener(al3);
            bg.add(jrb);
            hbox.add(jrb);
            jrb = new JRadioButton("Subsource");
            jrb.setSelected(str.equals("3"));
            jrb.addActionListener(al3);
            bg.add(jrb);
            hbox.add(jrb);
            jrb = new JRadioButton("Uniform background");
            jrb.setSelected(str.equals("0"));
            jrb.addActionListener(al3);
            bg.add(jrb);
            hbox.add(jrb);
            mainVbox.add(hbox);

            CatalogUtils.setupColors(catColors, newProps);

            Box catColorHbox = Box.createHorizontalBox();

            Box vboxUS = Box.createVerticalBox();
            vboxUS.setAlignmentY(Component.TOP_ALIGNMENT);

            Box vboxU = Box.createVerticalBox();
            vboxU.setBorder(BorderFactory.createTitledBorder("Uniform Background"));
            makeColorPropertyComponent(vboxU, CatalogColor.COLOR_CATALOG_UNIFORM);
            vboxUS.add(vboxU);

            catalogButtonList.clear();

            Box vboxS = Box.createVerticalBox();
            vboxS.setBorder(BorderFactory.createTitledBorder("Processing State"));
            addEventStateColorButtons(vboxS);
            vboxUS.add(vboxS);

            final Box vboxT = Box.createVerticalBox();
            vboxT.setAlignmentY(Component.TOP_ALIGNMENT);
            vboxT.setBorder(BorderFactory.createTitledBorder("Event EType"));
            JButton jb = new JButton("Add new etype");
            jb.setToolTipText("Press to add new event etype color");
            jb.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    addNewEventTypeButton(vboxT);
                }
            });
            vboxT.add(jb);
            addEventTypeColorButtons(vboxT);

            final Box vboxG = Box.createVerticalBox();
            vboxG.setAlignmentY(Component.TOP_ALIGNMENT);
            vboxG.setBorder(BorderFactory.createTitledBorder("Origin GType"));
            jb = new JButton("Add new gtype");
            jb.setToolTipText("Press to add new origin gtype color");
            jb.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    addNewOriginGTypeButton(vboxG);
                }
            });
            vboxG.add(jb);
            addOriginGTypeColorButtons(vboxG);

            final Box vboxSS = Box.createVerticalBox();
            vboxSS.setAlignmentY(Component.TOP_ALIGNMENT);
            vboxSS.setBorder(BorderFactory.createTitledBorder("Subsource"));
            jb = new JButton("Add new subsource");
            jb.setToolTipText("Press to add new event subsource color");
            jb.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    addNewEventSourceButton(vboxSS);
                }
            });
            vboxSS.add(jb);
            addEventSourceColorButtons(vboxSS);

            catColorHbox.add(vboxT);
            catColorHbox.add(vboxG);
            catColorHbox.add(vboxSS);
            catColorHbox.add(vboxUS);

            mainVbox.add(catColorHbox);
            this.add(mainVbox);
        }

        private MyColorButton makeColorButton(final String label, final Color color) {
            final MyColorButton jb = new MyColorButton();
            jb.setActionCommand(label);
            jb.setBackground(color);
            jb.setToolTipText(Integer.toHexString(color.getRGB()));

            jb.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    try {
                        final JColorChooser jcc = new JColorChooser(color);
                        final class MyJLabel extends JLabel implements ChangeListener {
                            private static final long serialVersionUID = 1L;

                            public MyJLabel(String str) {
                                super(str);
                            }

                            public void stateChanged(ChangeEvent evt) {
                                MyJLabel.this.setBackground(jcc.getSelectionModel().getSelectedColor());
                                // MyJLabel.this.setForeground(jcc.getSelectionModel().getSelectedColor());
                                MyJLabel.this.repaint();
                            }
                        }
                        ;
                        final MyJLabel jlbl2 = new MyJLabel("1234567 2008-08-01:23:45:56 local 3.75 l Jiggle");
                        JPanel jp = new JPanel();
                        jp.setBorder(BorderFactory.createTitledBorder(jb.getActionCommand()));
                        jlbl2.setForeground(Color.black);
                        jlbl2.setBackground(color);
                        jlbl2.setOpaque(true);
                        jlbl2.setPreferredSize(new Dimension(260, 20));
                        jp.add(jlbl2);
                        jcc.getSelectionModel().addChangeListener(jlbl2);
                        jcc.setPreviewPanel(jp);
                        JDialog jd = JColorChooser.createDialog(DPcatColors.this.getParent(), "Select row color", true,
                                jcc, new ActionListener() {
                                    public void actionPerformed(ActionEvent evt) {
                                        Color selectedColor = jcc.getColor();
                                        if (!selectedColor.equals(color)) {
                                            colorChanged = true;
                                            newProps.setProperty(label, selectedColor);
                                            jb.setBackground(selectedColor);
                                            jb.setToolTipText(Integer.toHexString(selectedColor.getRGB()));
                                        }
                                    }
                                }, new ActionListener() {
                                    public void actionPerformed(ActionEvent evt) {
                                    }
                                });
                        jd.pack();
                        jd.setSize(500, 600);
                        jd.setVisible(true);
                    } catch (HeadlessException ex) {
                    }
                }
            });

            lastButton = jb;
            catalogButtonList.addOrReplace(jb);

            return jb;
        }

        private JLabel makeColorButtonLabel(String label) {
            int idx = label.lastIndexOf('.');
            JLabel jlbl = new JLabel(label.substring(idx + 1));
            jlbl.setPreferredSize(new Dimension(100, 16));
            jlbl.setMaximumSize(new Dimension(100, 16));
            jlbl.setToolTipText("Row type, press color to change its color");
            return jlbl;
        }

        private JComponent makeColorPropertyComponent(JComponent comp, final CatalogColor dcc) {
            return makeColorPropertyComponent(comp, dcc.getProperty(), catColors[dcc.ordinal()]);
        }

        private JComponent makeColorPropertyComponent(JComponent comp, final String label, final Color color) {
            Box hbox = Box.createHorizontalBox();
            hbox.add(makeColorButtonLabel(label));
            hbox.add(makeColorButton(label, color));
            hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
            comp.add(hbox);
            return comp;
        }
    } // CatalogRowColorChooserPanel

    private static final long serialVersionUID = 1L;

    protected boolean colorChanged = false;
    private JiggleProperties newProps; // new properties as changed by this dialog
    private CatalogRowColorChooserPanel rowColorChooserPanel = null;

    public DPcatColors(JiggleProperties props) {
        newProps = props;
        rowColorChooserPanel = new CatalogRowColorChooserPanel();
        try {
            setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
            rowColorChooserPanel.initGraphics();
            this.add(rowColorChooserPanel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
} // DPcatColors
