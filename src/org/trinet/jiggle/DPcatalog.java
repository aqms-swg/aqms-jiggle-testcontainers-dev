package org.trinet.jiggle;

import java.util.*;

/**
 * Panel for insertion into dialog box for selecting catalog column property.
 */
public class DPcatalog extends PreferencesPanelChooser {
    private static final long serialVersionUID = 1L;

    private static final String[] CAT_COL_NAME_TIPS = {
            /** ASSOCIATION */
            "Event Association",
            /** AUTH */
            "Event authority",
            /** B */
            "Bogus origin",
            /** CM */
            "Velocity model domain",
            /** COMMENT */
            "Event comment",
            /** DATETIME */
            "Origin time UTC)",
            /** DIST */
            "Closest sta (km)",
            /** DM */
            "Crustal model type",
            /** ERR_H */
            "Horizontal error (km)",
            /** ERR_T */
            "Time error (secs)",
            /** ERR_Z */
            "Depth error (km)",
            /** ESRC */
            "Event subsource",
            /** ETYPE */
            "Event type",
            /** EXT_ID */
            "External id",
            /** FM */
            "First motion count",
            /** GAP */
            "Gap",
            /** GT */
            "Geographic type",
            /** GZ */
            "Geoid depth",
            /** HF */
            "Fixed location",
            /** LAT */
            "Latitude",
            /** LDDATE */
            "Origin lddate",
            /** LON */
            "Longitude",
            /** MAG */
            "Event magnitude (event.prefmag)",
            /** MERR */
            "Event prefmag error",
            /** METHOD */
            "Origin algorithm",
            /** MMETH */
            "Event prefmag algorithm",
            /** MOBS */
            "Event prefmag channels",
            /** MOD */
            "Crustal model name",
            /** MSTA */
            "Event prefmag stations",
            /** MTYP */
            "Event prefmag type",
            /** MWHO */
            "Event prefmag who (attribution)",
            /** Md */
            "Md",
            /** Me */
            "Me",
            /** Ml */
            "Ml",
            /** Ml-Md */
            "Ml-Md",
            /** Mlr */
            "Mlr",
            /** Mw */
            "Mw",
            /** MZ */
            "Model depth (mdepth)",
            /** OAUTH */
            "Origin authority",
            /** OBS */
            "Total phases",
            /** OT */
            "Origin Type",
            /** OWHO */
            "Origin who (attribution)",
            /** PM */
            "Prefmec flag",
            /** PR */
            "Event priority",
            /** Q */
            "Origin Quality",
            /** RMS */
            "Origin RMS traveltime residual (secs)",
            /** S */
            "S phase count",
            /** SRC */
            "Origin subsource",
            /** ST */
            "Event processing state",
            /** TF */
            "Fixed Time",
            /** USED */
            "Phases used",
            /** V */
            "Event valid (primary)",
            /** VER */
            "Event update version",
            /** VM */
            "Velocity model id",
            /** WRECS */
            "Event waveform count",
            /** Z */
            "Origin depth",
            /** ZF */
            "Fixed Depth" };

    private static final String[] CAT_COL_NAMES = {
            /** Event Association */
            "ASSOCIATION",
            /** Event authority */
            "AUTH",
            /** Bogus origin */
            "B",
            /** Velocity model domain */
            "CM",
            /** Event comment */
            "COMMENT",
            /** Origin time UTC) */
            "DATETIME",
            /** Closest sta (km) */
            "DIST",
            /** Crustal model type */
            "DM",
            /** Horizontal error (km) */
            "ERR_H",
            /** Time error (secs) */
            "ERR_T",
            /** Depth error (km) */
            "ERR_Z",
            /** Event subsource */
            "ESRC",
            /** Event type */
            "ETYPE",
            /** External id */
            "EXT_ID",
            /** First motion count */
            "FM",
            /** Gap */
            "GAP",
            /** Geographic type */
            "GT",
            /** Geoid depth */
            "GZ",
            /** Fixed location */
            "HF",
            /** Latitude */
            "LAT",
            /** Origin lddate */
            "LDDATE",
            /** Longitude */
            "LON",
            /** Event magnitude (event.prefmag) */
            "MAG",
            /** Event prefmag error */
            "MERR",
            /** Origin algorithm */
            "METHOD",
            /** Event prefmag algorithm */
            "MMETH",
            /** Event prefmag channels */
            "MOBS",
            /** Crustal model name */
            "MOD",
            /** Event prefmag stations */
            "MSTA",
            /** Event prefmag type */
            "MTYP",
            /** Event prefmag who (attribution) */
            "MWHO",
            /** Md */
            "Md",
            /** Me */
            "Me",
            /** Ml */
            "Ml",
            /** Ml-Md */
            "Ml-Md",
            /** Mlr */
            "Mlr",
            /** Mw */
            "Mw",
            /** Model depth (mdepth) */
            "MZ",
            /** Origin authority */
            "OAUTH",
            /** Total phases */
            "OBS",
            /** Origin Type */
            "OT",
            /** Origin who (attribution) */
            "OWHO",
            /** Prefmec flag */
            "PM",
            /** Event priority */
            "PR",
            /** Origin Quality */
            "Q",
            /** Origin RMS traveltime residual (secs) */
            "RMS",
            /** S phase count */
            "S",
            /** Origin subsource */
            "SRC",
            /** Event processing state */
            "ST",
            /** Fixed Time */
            "TF",
            /** Phases used */
            "USED",
            /** Event valid (primary) */
            "V",
            /** Event update version */
            "VER",
            /** Velocity model id */
            "VM",
            /** Event waveform count */
            "WRECS",
            /** Origin depth */
            "Z",
            /** Fixed Depth */
            "ZF" };
    private static final Map<String, String> toolTipMap;
    static {
        int size = CAT_COL_NAMES.length;
        toolTipMap = new HashMap<>(size);
        for (int idx = 0; idx < size; idx++) {
            toolTipMap.put(CAT_COL_NAMES[idx], CAT_COL_NAME_TIPS[idx]);
        }
        Arrays.sort(CAT_COL_NAMES);
    }

    /**
     * Create the Catalog Columns preferences.
     * 
     * @param props the properties.
     */
    public DPcatalog(JiggleProperties props) {
        super(props, CAT_COL_NAMES, toolTipMap, "catalogColumnList", "Catalog Table Column Arrangement",
                "Catalog table");
    }
}
