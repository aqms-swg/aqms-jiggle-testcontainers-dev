package org.trinet.jiggle;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.text.*;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.*;

import org.trinet.jasi.DataSource;
import org.trinet.jasi.JasiDbReader;
import org.trinet.jasi.EnvironmentInfo;

/**
 * Panel for insertion into dialog box for setting miscellaneous properties.
 */
public class DPchannel extends JPanel {

    JiggleProperties newProps;    // new properties as changed by this dialog

    boolean channelCacheChanged = false;

    public DPchannel(JiggleProperties props) {

        newProps = props;

        try {
            initGraphics();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void initGraphics() throws Exception {

        //this.setLayout( new ColumnLayout() );
        Box hbox = null;
        Box vbox = null;
        JLabel jlbl = null;
        JCheckBox jcb = null;

//channellist
        vbox = Box.createVerticalBox();
        vbox.setBorder(BorderFactory.createTitledBorder("Master Channel List Options"));

        //channelCacheFilename=cisnChannelList16.cache
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel ("Channel cache filename ");
        jlbl.setToolTipText("Click button to change channel cache filename property");
        hbox.add(jlbl);

        JButton cacheFileButton = new JButton(newProps.getProperty("channelCacheFilename",""));
        cacheFileButton.addActionListener(new SetChannelCacheFilenamePropFileActionHandler() );
        cacheFileButton.setToolTipText("Click to change channel cache filename property");
        hbox.add(cacheFileButton);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);


        //channelGroupName=RCG-TRINET
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel ("Channel list group name ");
        hbox.add(jlbl);
        /*
        jtf = new JTextField(newProps.getProperty("channelGroupName",""));
        jtf.getDocument().addDocumentListener(new MiscDocListener("channelGroupName", jtf));
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);
        */

        java.util.List appNames = (DataSource.isNull() || DataSource.isClosed()) ?
            new ArrayList(0) : JasiDbReader.getApplicationNames(DataSource.getConnection());
        String selectedName = newProps.getProperty("channelGroupName");
        ArrayList aList = new ArrayList(appNames.size()+1);
        aList.addAll(appNames);
        if (selectedName != null && selectedName.length() > 0 && !appNames.contains(selectedName)) {
            aList.add(selectedName); 
        }
        final JComboBox chnlGrpComboBox = new JComboBox(aList.toArray());
        chnlGrpComboBox.setToolTipText("Select channel's group name, press X to set property.");
        chnlGrpComboBox.setEditable(true);
        chnlGrpComboBox.setSelectedItem(selectedName);

        JButton chnlGrpButton = new JButton("X");
        chnlGrpButton.setHorizontalTextPosition(AbstractButton.LEFT);
        chnlGrpButton.setMargin(new Insets(0,0,0,0));
        chnlGrpButton.setToolTipText("Press to set channel's group name property to combobox selection.");
        chnlGrpButton.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                String selectedAppName = (String) chnlGrpComboBox.getSelectedItem();
                if (selectedAppName != null) newProps.setProperty("channelGroupName", selectedAppName);
            }
        });
        hbox.add(chnlGrpComboBox);
        hbox.add(chnlGrpButton);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        jcb = makeCheckBox("Auto refresh cache", "Update cache file in background thread with latest db data","autoRefreshChannelList");
        vbox.add(jcb);
        jcb = makeCheckBox("Cache read", "Load initial channel list from cache file on startup","channelListCacheRead");
        vbox.add(jcb);

        jcb = makeCheckBox("Cache write", "Write current channel list to cache on exit","channelListCacheWrite");
        vbox.add(jcb);

        jcb = makeCheckBox("Load only LatLonZ", "Don't load gain or mag correction data","channelListReadOnlyLatLonZ");
        vbox.add(jcb);

        this.add(vbox);

    }

    private JCheckBox makeCheckBox(String text, String prop) {
        return makeCheckBox(text, text, prop);
    }

    private JCheckBox makeCheckBox(String text, String toolTip, String prop) {
        JCheckBox cb = new JCheckBox();
        cb.setText(text);
        cb.setSelected(newProps.getBoolean(prop));
        cb.setToolTipText(toolTip);
        cb.addItemListener(new BooleanPropertyCheckBoxListener(prop));
        cb.setAlignmentX(Component.LEFT_ALIGNMENT);
        return cb;
    }

    /*
    private class MiscDocListener implements DocumentListener { // inner class
        private JTextField jtf = null;
        private String prop = null;

        public MiscDocListener(String prop, JTextField jtf) {
            this.jtf = jtf;
            this.prop = prop;
        }

        public void insertUpdate(DocumentEvent e) {
            setValues(e);
        }
        public void removeUpdate(DocumentEvent e) {
            setValues(e);
        }
        public void changedUpdate(DocumentEvent e) {
            // we won't ever get this with a PlainDocument
        }

        // Make changes to properties "on the fly". 
        private void setValues(DocumentEvent e) {
            // no sanity checking on numbers !
            newProps.setProperty(prop, jtf.getText().trim());
        }
    } // end of inner class
    */

    private class BooleanPropertyCheckBoxListener implements ItemListener { // inner class
        private String prop = null;

        public BooleanPropertyCheckBoxListener(String prop) {
            this.prop = prop;
        }

        public void itemStateChanged(ItemEvent e) {
            boolean state =(e.getStateChange() == ItemEvent.SELECTED);
            newProps.setProperty(prop, state);
        }
    } // end of inner class

    private class SetChannelCacheFilenamePropFileActionHandler implements ActionListener {

      public void actionPerformed(ActionEvent evt) {

          try {
            JFileChooser jfc = new JFileChooser(newProps.getUserFilePath());
            String filename = newProps.getUserFileNameFromProperty("channelCacheFilename");
            String oldName = filename;
            if (filename != null) jfc.setSelectedFile(new File(filename));

            if (jfc.showDialog(DPchannel.this.getTopLevelAncestor(),"Select") == JFileChooser.APPROVE_OPTION) {

                File file = jfc.getSelectedFile();
                filename = file.getAbsolutePath();
                if (jfc.getCurrentDirectory().getAbsolutePath().equals(newProps.getUserFilePath()) ) {
                    filename = jfc.getSelectedFile().getName();
                }

                if (file.exists()) { // prompt to verify change jiggle.props delegate property
                  if (oldName == null || ! oldName.equals(filename)) { // prompt to change jiggle.props delegate property
                        newProps.setProperty("channelCacheFilename", filename);
                        ((JButton)evt.getSource()).setText(filename);
                        channelCacheChanged = true;
                  } else if ( JOptionPane.showConfirmDialog(null, "No filename change, reload channel cache again from this file ?",
                        "Update Jiggle properties", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)  {
                       channelCacheChanged = true; // cache filename the same 
                  }
                }
                // else prompt to verify change jiggle.props delegate property, perhaps it's a new file for next startup?
                else if ( JOptionPane.showConfirmDialog(getTopLevelAncestor(),
                      "Selected file D.N.E. ! set it's name as channeCacheFilename property ?",
                      "Update Jiggle Properties", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)  {
                       newProps.setProperty("channelCacheFilename", filename);
                       ((JButton)evt.getSource()).setText(filename);
                        channelCacheChanged = false; // don't let GUI load master list here since file doesn't exist (too long a wait for new cache) ??
                }
            }
        }
        catch(Exception ex) {
          ex.printStackTrace();
          JOptionPane.showMessageDialog(null,
            "Change of channel cache filename failed, check messages!",
            "Properties", JOptionPane.PLAIN_MESSAGE);
        }
      }

    }

}

