package org.trinet.jiggle;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.text.*;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.*;

import org.trinet.jasi.DataSource;
import org.trinet.jasi.JasiDbReader;
import org.trinet.jasi.EnvironmentInfo;

/**
 * Panel for insertion into dialog box for setting miscellaneous properties.
 */
public class DPcheckQC extends AbstractPreferencesPanel {

    public DPcheckQC(JiggleProperties props) {

        super(props);

    }

    protected void initGraphics() {

        this.setLayout( new ColumnLayout() );

        Box vbox = null;
        JCheckBox jcb = null;

        vbox = Box.createVerticalBox();
        vbox.setBorder(BorderFactory.createTitledBorder("QC GUI options"));
        jcb = makeCheckBox("Automatically load catalog on startup","unchecked=>DO NOT query database for event catalog at startup","autoLoadCatalog");
        vbox.add(jcb);
        jcb = makeCheckBox("Network connection WAN","Remote network access, catalog row is not auto-refreshed by event load","networkModeWAN");
        vbox.add(jcb);
        jcb = makeCheckBox("Disable version check","Don't check for new version on web site when properties are set","versionCheckDisabled");
        vbox.add(jcb);
        jcb = makeCheckBox("Disable event quarry check","Don't check event set to quarry satifies db quarry criteria", "disableQuarryCheck");
        vbox.add(jcb);
        jcb = makeCheckBox("Notify user with popup if location server fixes event depth", "locEngineFixedZNotify");
        vbox.add(jcb);
        jcb = makeCheckBox("Confirm waveform scan for magnitude","Popup to confirm scanning waveforms to create new magnitude readings",
                           "confirmWFScanForMag");
        vbox.add(jcb);
        jcb = makeCheckBox("Confirm menu deletes","Popup to confirm global delete of readings from menu","confirmMenuDeleteAction");
        vbox.add(jcb);
        jcb = makeCheckBox("Confirm waveform panel deletes","Popup to confirm global delete of readings from wfpanel popup","confirmWFPanelDeleteAction");
        vbox.add(jcb);
        jcb = makeCheckBox("Confirm new event creation","Popup to confirm creation of new event (clone)","confirmNewSolutionAction");
        vbox.add(jcb);
        jcb = makeCheckBox("Confirm event lat,lon on save","Popup to confirm has event a lat and lon on save","confirmLocationOnSave");
        vbox.add(jcb);
        jcb = makeCheckBox("Confirm finalized state on save","Popup to confirm preservation of final 'F' rflag on save","confirmFinalOnSave");
        vbox.add(jcb);
        //jcb = makeCheckBox("Allow first motion on horizontals","c,d,+,- for P or S phase","firstMoOnHoriz");
        //vbox.add(jcb);
        //jcb = makeCheckBox("Allow first motion on S","c,d,+,- for any component orientation", "firstMoOnS");
        //vbox.add(jcb);
        jcb = makeCheckBox("Test db connection","Send a test query to database before each db transaction","testConnection");
        vbox.add(jcb);
        jcb = makeCheckBox("Auto load after delete",
                "Load next event in viewer, if currently loaded event is deleted via a menu or toolbar action",
                "autoLoadAfterDelete");
        vbox.add(jcb);
        this.add(vbox);
        //

    }

}

