package org.trinet.jiggle;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.text.*;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.event.*;

import org.trinet.jasi.DataSource;
import org.trinet.jasi.JasiDbReader;
import org.trinet.jasi.EnvironmentInfo;

/**
 * Panel for insertion into dialog box for setting miscellaneous properties.
 */
public class DPdbSave extends AbstractPreferencesPanel {

    public DPdbSave(JiggleProperties props) {

        super(props);

    }

    protected void initGraphics() {

        this.setLayout( new ColumnLayout() );

        DecimalFormat df2 = new DecimalFormat ( "#0.00" );

        Box hbox = null;
        Box vbox = null;
        JLabel jlbl = null;
        JTextField jtf = null;
        JCheckBox jcb = null;

        JPanel cbPanel = new JPanel();

        cbPanel.setBorder(BorderFactory.createTitledBorder("Database Save Options"));
        vbox = Box.createVerticalBox();

        jcb = makeCheckBox("Database commit enabled", "Allow db inserts and updates","dbWriteBackEnabled");
        vbox.add(jcb);

        jcb = makeCheckBox("Disable event lock table check","Allow load of events locked by others","solLockingDisabled");
        vbox.add(jcb);

        jcb = makeCheckBox("Disable event type region check","Don't check that local/regional type is in/out side of net bounds","eventTypeCheckDisabled");
        vbox.add(jcb);

        jcb = makeCheckBox("Disable prefmag priority check","Don't check that preferred magnitude has highest priority", "prefmagCheckDisabled");
        vbox.add(jcb);

        jcb = makeCheckBox("Disable event duplicate check","Don't check that event has no duplicates in database", "duplicateCheckDisabled");
        vbox.add(jcb);

        jcb = makeCheckBox("Allow waveform association commits",
                "Copy original event's waveform database associations (parent) to new evid (child clone)","waveformCommitOk");
        vbox.add(jcb);

        jcb = makeCheckBox("Allow stale solution commits","Allow commit when relocation required","solStaleCommitOk");
        vbox.add(jcb);

        hbox = Box.createHorizontalBox();
        jcb = makeCheckBox("Allow stale magnitude commits","Allow commit when recalculation required","magStaleCommitOk");
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        hbox.add(jcb);

        jcb = makeCheckBox("Mag commit no-op if from db and stale","No change to magnitude db associations","magStaleCommitNoop");
        hbox.add(jcb);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);
        hbox = Box.createHorizontalBox();
        jtf = new JTextField(df2.format(newProps.getFloat("prefmagCheckValue")));
        jtf.setPreferredSize(new Dimension(40, 16));
        jtf.setMaximumSize(new Dimension(40, 16));
        jtf.getDocument().addDocumentListener(new MiscDocListener("prefmagCheckValue", jtf));
        jtf.setToolTipText("Confirm commit if the event preferred magnitude is >= value");
        jlbl = new JLabel ("  Confirm commit magnitude threshold"); 
        jlbl.setToolTipText("Confirm commit when the event preferred magnitude is >= value");
        hbox = Box.createHorizontalBox();
        hbox.add(jlbl);
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        cbPanel.add(vbox);
        this.add(cbPanel);

    }
}

