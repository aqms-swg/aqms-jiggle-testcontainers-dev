package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.*;
import java.text.*;

import javax.swing.*;

import org.trinet.jasi.*;
import org.trinet.jdbc.datatypes.*;

/**
Panel with components for input of magnitude information.
Allows changing: value, mag type, authority, source and method. The resulting mag
will NOT carry along any other info from the original mag, like nsta, because it would
no longer be valid. It also does not inherit the amplitude list.
Thus, a hand entered mag will have no amps.
*/
public class DPmagnitude extends JPanel {

    private Magnitude myMag = null;

    private JComboBox<String> valCombo = new JComboBox<>();
    private JComboBox<String> typeCombo = new JComboBox<>();
    private JComboBox<String> methodText = new JComboBox<>();

    private JTextField sourceText = new JTextField();
    private JTextField authText = new JTextField();
    private JTextField nreadingsText = new JTextField();
    private JTextField nstaText = new JTextField();
    private JTextField errorText = new JTextField();

    private DecimalFormat df = new DecimalFormat ("0.00"); // format for mag values

    private String [] magTypes = null;

    /** Keep track of whether or not anything changed. */
    private boolean changed = false;

    /** Create a Magnitude dialog panel with default values set to the values
    of the Solution preferred magnitude. */
    public DPmagnitude(Solution sol, String [] magTypes) {
        super(new FlowLayout(FlowLayout.LEFT));
        this.magTypes = magTypes;

        try  {
            initGraphics();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }

        Magnitude solMag = sol.getPreferredMagnitude();
        myMag = solMag;
        if (myMag == null || ! myMag.algorithm.toString().equals("HAND") ) {
            //System.out.println("DEBUG DPmagnitude, creating newMag");
            myMag = Magnitude.create();
            myMag.assign(sol);
            if (solMag != null) {
                myMag.value = (DataDouble) solMag.value.clone();
                myMag.subScript = (DataString) solMag.subScript.clone(); 
            }
            myMag.algorithm.setValue("HAND");
        }

        setValues();
    }

    private void setValues() {

        // source and authority are set in Magnitude.create so they're always safe
        sourceText.setText(myMag.source.toString());
        authText.setText(myMag.authority.toString());

        String typeStr = "Mh";
        if ( myMag.sol.isEventDbType(EventTypeMap3.TRIGGER) || myMag.getTypeString().equals("Mn") ) {
            typeStr = "Mn";
        }
        typeCombo.setSelectedItem(typeStr);

        double val = //(myMag == null || myMag.hasNullValue()) ? -1. : myMag.getMagValue();
            (myMag == null || myMag.hasNullValue()) ? -1. : myMag.getMagValue();

        String valStr = "0.";
        if (val >= 0.) { // at this point it's either Mh or Mn, not another type so df.format is not executed
            valStr = ( typeStr.equals("Mh") || typeStr.equals("Mn") ) ? String.format("%02.1f0", val) : df.format(val);
        }
        valCombo.setSelectedItem(valStr);

        methodText.setSelectedItem("HAND"); //methodText.setText("HAND");

        nreadingsText.setText(String.valueOf(myMag.getReadingsUsed())); // used to be StationsUsed - aww 2008/07/10
        nstaText.setText(String.valueOf(myMag.getStationsUsed())); // aww 2008/08/11

        valStr = myMag.error.isValid() ? df.format(myMag.error.doubleValue()) : "";
        errorText.setText(valStr);

        setupListeners();
    }

    private void setupListeners() {
        // add Listener keeps track of if something was edited/changed
        final java.awt.event.ActionListener changeListener =
            new java.awt.event.ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    changed = true;
                    if (((String)typeCombo.getSelectedItem()).equals("Mn")) {
                       valCombo.setSelectedItem("0.");
                    }
                }
            };
        sourceText.addActionListener(changeListener);
        authText.addActionListener(changeListener);
        valCombo.addActionListener(
            new java.awt.event.ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    changed = true;
                    if (((String)typeCombo.getSelectedItem()).equals("Mn")) {
                       valCombo.setSelectedItem("0.");
                    }
                }
            }
        );
        typeCombo.addActionListener(changeListener);
        methodText.addActionListener(changeListener);
        nreadingsText.addActionListener(changeListener);
        nstaText.addActionListener(changeListener);
        errorText.addActionListener(changeListener);
    }

    private void initGraphics() throws Exception {


        errorText.setToolTipText("Standard deviation of the magnitude");

        sourceText.setEditable(false);

        // populate the combos
        valCombo.setEditable(true);                    // allow freeform user input
        for (double m = 0.0; m <= 8.0; m += 0.1) {
          valCombo.addItem(df.format(m));
        }

        // this is the order of precidence for So Cal.
        boolean addDef = true;
        for ( int idx=0; idx<magTypes.length; idx++ ) {
            if ( magTypes[idx].equals("h") ) addDef = false;
        }
        if (addDef) typeCombo.addItem("Mh"); // HAND has highest priority (vs. variant Mh via Ml with Z comps)

        for ( int idx=0; idx<magTypes.length; idx++ ) {
          typeCombo.addItem("M"+magTypes[idx]);
        }
        addDef = true;

        for ( int idx=0; idx<magTypes.length; idx++ ) {
            if ( magTypes[idx].equals("n") ) addDef = false;
        }
        if (addDef) typeCombo.addItem("Mn");

        typeCombo.setMaximumRowCount(typeCombo.getItemCount());

        methodText.addItem("HAND");
        methodText.setEditable(true);

        JPanel p = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridy = 0;
        addComponents(p, c, "Type ", typeCombo, "Event preferred magnitude subtype");
        addComponents(p, c, "Value ", valCombo, "Preferred magnitude value");
        addComponents(p, c, "Authority ", authText, "Network authority of magnitude");
        addComponents(p, c, "Source ", sourceText, "Software/system used to generate magnitude");
        addComponents(p, c, "Chls Used ", nreadingsText, "Number of channels used for magnitude");
        addComponents(p, c, "Stns Used ", nstaText, "Number of stations used for magnitude");
        addComponents(p, c, "Error ", errorText, "RMS error in magnitude units");
        addComponents(p, c, "Method ", methodText, "Name of algorithm used to derive magnitude");
        this.add(p);
    }
    
    private static void addComponents(JPanel p, GridBagConstraints c, String labelText, JComponent jcomp, String tooltip) {
        c.anchor = GridBagConstraints.EAST;
        c.fill = GridBagConstraints.NONE;
        // c.gridx = 0;
        p.add(makeLabel(labelText, tooltip), c);
        if (tooltip != null) jcomp.setToolTipText(tooltip);
        c.anchor = GridBagConstraints.WEST;
        c.fill = GridBagConstraints.HORIZONTAL;
        // c.gridx++;
        p.add(jcomp, c);
        c.gridy++;
    }

    private static JLabel makeLabel(String text, String tooltip) {
        JLabel aLabel = new JLabel(text);
        if (tooltip != null) aLabel.setToolTipText(tooltip);
        aLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        return aLabel;
    }

    /** Reads the dialog components and returns a magnitude resulting from
    the the values in them. */
    public Magnitude getMag() {

        // make a virgin magnitude, don't keep field from old you can't support
        Magnitude newMag = Magnitude.create();

        // keep solution assignment
        if (myMag.sol != null) newMag.assign(myMag.sol);

        String str = (String) valCombo.getSelectedItem();
        if (str == null) str = "0.";

        Double dval = Double.valueOf(str.trim());
        double val = dval.doubleValue();
        newMag.value.setValue(val);

        // remove "M" on front
        String subscript = ((String) typeCombo.getSelectedItem()).substring(1);
//      newMag.subScript.setValue(subscript);
        newMag.setType(subscript);
        // Mn (no magnitude) should only have 0. value to avoid confusion
        if ( subscript.equals("n") && val != 0. ) {
            newMag.value.setValue(0.);
            valCombo.setSelectedItem("0.");
        }

        // treat word "null" or a blank as a real null
        parseText (newMag.authority, authText);
        parseText (newMag.source, sourceText);
        //parseText (newMag.algorithm, methodText);
        String method = (String) methodText.getSelectedItem();
        if ( method.equals("HAND") ) newMag.setDependsOnOrigin(false); // 01/26/2007 -aww
        newMag.algorithm.setValue(method);

        parseValue(newMag.usedChnls, nreadingsText); // new field -aww 2008/07/10
        parseValue(newMag.usedStations, nstaText); // new column added to NetMag table -aww 2008/08/11
        parseValue(newMag.error, errorText);

        // manually entered magnitude does not have a field values for this:
        newMag.processingState.setValue(JasiProcessingConstants.STATE_HUMAN_TAG);

        // set other fields null or zero here?

        return newMag;
    }

    /** Return true if some field was EDITED in the dialog.
    * Note the field value may not have changed.
    */
    public boolean hasChanged() {
        return changed;
    }

    /** Treat word "null" or a blank as a real null */
    private void parseText (DataString dataString, JTextField field) {
        String str = field.getText().trim();    // the field text

        if (str.equalsIgnoreCase("NULL") ||
            str.length() == 0) {
            dataString.setNull(true);
        } else {
            dataString.setValue(str);
        }
    }

    /** Treat word "null", "NaN" or a blank as a real null */
    private void parseValue(DataObject dataObj, JTextField field) {
        String str = field.getText().trim();    // the field text
        if (str.equalsIgnoreCase("NULL") ||
            str.equalsIgnoreCase("NaN") ||
            str.length() == 0) {
            dataObj.setNull(true);
        } else {
            Double dval = Double.valueOf( str );     // convert to double
            double val = dval.doubleValue();
            dataObj.setValue(val);
        }
    }
}
