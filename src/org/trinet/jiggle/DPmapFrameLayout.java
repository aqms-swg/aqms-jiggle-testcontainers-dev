package org.trinet.jiggle;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.text.*;

import javax.swing.*;
import javax.swing.event.*;

import org.trinet.jasi.PhaseDescription;

/**
 * Panel for insertion into dialog box to edit GUI layout of controls, waveform panels, etc.
 */
public class DPmapFrameLayout extends AbstractPreferencesPanel {

    protected boolean mapPropsChanged = false;

    public DPmapFrameLayout(JiggleProperties props) {

        super(props);

    }

    protected void initGraphics() {

        this.setLayout( new ColumnLayout() );

        Box hbox = null;
        Box vbox = null;
        JLabel jlbl = null;
        ButtonGroup bg = null;
        JRadioButton jrb = null;
        ActionListener al = null;

        vbox = Box.createVerticalBox();
        vbox.setBorder(BorderFactory.createTitledBorder("Map Panel"));

        //mapPropFilename=openmap.properties
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel ("Map properties file ");
        hbox.add(jlbl);

        JButton mapFileButton = new JButton(newProps.getProperty("mapPropFilename",""));
        mapFileButton.addActionListener(new SetMapPropFileActionHandler() );
        mapFileButton.setToolTipText("Click to change map properties filename or refresh map");
        hbox.add(mapFileButton);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        // map panel shown internal or separate window frame
        hbox = Box.createHorizontalBox();
        al = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String cmd = e.getActionCommand();
                if (cmd == "Catalog split") {
                    newProps.setProperty("mapInSplit", true);
                }
                else if (cmd == "Separate window") {
                    newProps.setProperty("mapInSplit", false);
                }
                updateGUI = true;
            }
        };
        jlbl = new JLabel("Map panel in ");
        hbox.add(jlbl);

        bg = new ButtonGroup();
        jrb = new JRadioButton("Catalog split");
        jrb.setSelected(newProps.getBoolean("mapInSplit"));
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("Separate window");
        jrb.setSelected(! newProps.getBoolean("mapInSplit"));
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        // mapSplit
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Catalog split orientation ");
        hbox.add(jlbl);

        al = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String cmd = e.getActionCommand();
                if (cmd == "Horizontal") {
                    newProps.setProperty("mapSplitOrientation", JSplitPane.HORIZONTAL_SPLIT);
                }
                else if (cmd == "Vertical") {
                    newProps.setProperty("mapSplitOrientation", JSplitPane.VERTICAL_SPLIT);
                }
                updateGUI = true;
            }
        };

        int splitVal = newProps.getInt("mapSplitOrientation", JSplitPane.VERTICAL_SPLIT); 
        jrb = new JRadioButton("Horizontal");
        jrb.setSelected((splitVal == JSplitPane.HORIZONTAL_SPLIT)); 

        bg = new ButtonGroup();
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);
        jrb = new JRadioButton("Vertical");
        jrb.setSelected((splitVal == JSplitPane.VERTICAL_SPLIT)); 
        jrb.addActionListener(al);
        bg.add(jrb);
        hbox.add(jrb);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        // done with creating Map panel components add result
        this.add(vbox);

    }

    private class SetMapPropFileActionHandler implements ActionListener {

      public void actionPerformed(ActionEvent evt) {

          try {
            JFileChooser jfc = new JFileChooser(newProps.getUserFilePath());
            String filename = newProps.getUserFileNameFromProperty("mapPropFilename");
            if (filename != null) jfc.setSelectedFile(new File(filename));

            if (jfc.showDialog(DPmapFrameLayout.this.getTopLevelAncestor(),"Select") == JFileChooser.APPROVE_OPTION) {
                // NOTE: Approve Action results in total map reset even when property filename remains the same 
                File file = jfc.getSelectedFile();
                filename = file.getAbsolutePath();
                if (jfc.getCurrentDirectory().getAbsolutePath().equals(newProps.getUserFilePath()) ) {
                    filename = file.getName();
                }
                if (file.exists()) { // prompt to verify change jiggle.props delegate property
                // NOTE: Approve Action results in total map reset here, even when property filename remains the same as before !!
                    newProps.setProperty("mapPropsFilename", filename);
                    ((JButton)evt.getSource()).setText(filename);
                    mapPropsChanged = true;
                }
                else { // prompt to verify change jiggle.props delegate property, perhaps new file for next startup?
                  if ( JOptionPane.showConfirmDialog(getTopLevelAncestor(),
                      "Selected file D.N.E. ! Set it's name as mapPropFilename property ?",
                      "Update Jiggle Properties", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION)  {
                       newProps.setProperty("mapPropsFilename", filename);
                       ((JButton)evt.getSource()).setText(filename);
                  }
                }
            }
        }
        catch(Exception ex) {
          ex.printStackTrace();
          JOptionPane.showMessageDialog(getTopLevelAncestor(), "Change of map properties filename failed, check messages!",
            "Properties", JOptionPane.PLAIN_MESSAGE);
        }
      }

    }
}
