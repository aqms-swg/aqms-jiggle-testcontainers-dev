package org.trinet.jiggle;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.text.*;

import javax.swing.*;
import javax.swing.event.*;

import org.trinet.jasi.PhaseDescription;

/**
 * Panel for insertion into dialog box to edit GUI layout of controls, waveform panels, etc.
 */
public class DPpickLayout extends AbstractPreferencesPanel {

    public DPpickLayout(JiggleProperties props) {

        super(props);

    }

    protected void initGraphics() {

        this.setLayout( new ColumnLayout() );

        Box hbox = null;
        Box vbox = null;
        JLabel jlbl = null;
        JTextField jtf = null;
        JCheckBox jcb = null;

        vbox = Box.createVerticalBox();
        vbox.setBorder(BorderFactory.createTitledBorder("Phase Pick Layout"));

        hbox = Box.createHorizontalBox();
        jcb = makeUpdateCheckBox("Popup menu flat", "phasePopupMenuFlat");
        hbox.add(jcb);
        hbox.add(Box.createHorizontalStrut(20));

        jcb = makeUpdateCheckBox("Show arrival cues", "showPhaseCues");
        hbox.add(jcb);
        hbox.add(Box.createHorizontalGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        hbox = Box.createHorizontalBox();
        jcb = new JCheckBox("Pick flag font bold");
        jcb.setSelected(newProps.getProperty("pickFlagFont").equals("BIG")); 
        //NEED Custom listener here
        jcb.addItemListener(new ItemListener() {
           public void itemStateChanged(ItemEvent evt) {
             boolean state = (evt.getStateChange() == ItemEvent.SELECTED);
             newProps.setProperty("pickFlagFont", (state ? "BIG" : "small"));
             updateGUI = true;
           }
        });
        jcb.setAlignmentX(Component.LEFT_ALIGNMENT);
        hbox.add(jcb);
        hbox.add(Box.createHorizontalStrut(10));

        jcb = makeUpdateCheckBox("One P or S phase per station","masterViewReplacePhasesByStaType");
        hbox.add(jcb);
        hbox.add(Box.createHorizontalGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Popup menu P descriptors ");
        hbox.add(jlbl);
        jtf = new JTextField(newProps.getProperty("phasePopupMenu.P.desc", ""));
        jtf.getDocument().addDocumentListener(new MiscDocListener("phasePopupMenu.P.desc",jtf));
        jtf.setMaximumSize(new Dimension(250,20));
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Popup menu S descriptors ");
        hbox.add(jlbl);
        jtf = new JTextField(newProps.getProperty("phasePopupMenu.S.desc", ""));
        jtf.getDocument().addDocumentListener(new MiscDocListener("phasePopupMenu.S.desc",jtf));
        jtf.setMaximumSize(new Dimension(250,20));
        hbox.add(jtf);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Min wt in S descriptor menu items ");
        jlbl.setToolTipText("Hide popup menu S descriptors whose weight < selected value");
        hbox.add(jlbl);
        java.util.ArrayList sList = new java.util.ArrayList(4);
        for (int idx=0; idx<=5; idx++) sList.add(String.valueOf(idx));
        JComboBox comboBox = new JComboBox(sList.toArray());
        comboBox.setEditable(false);
        comboBox.setSelectedItem(newProps.getProperty("phasePopupMenu.S.minWgt", "0"));
        comboBox.setToolTipText("Hide popup menu items whose S weight < selected value");
        comboBox.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent evt) {
               JComboBox comboBox = (JComboBox) evt.getSource();
               newProps.setProperty("phasePopupMenu.S.minWgt", (String)comboBox.getSelectedItem());
           }
        }); 
        comboBox.setMaximumSize(new Dimension(50,20));
        hbox.add(comboBox);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Max wt in menu descriptor with fm ");
        jlbl.setToolTipText("Hide popup menu descriptors having a first motion and greater weight");
        jlbl.setToolTipText("");
        hbox.add(jlbl);
        sList = new java.util.ArrayList(4);
        for (int idx=-1; idx<=4; idx++) sList.add(String.valueOf(idx));
        comboBox = new JComboBox(sList.toArray());
        comboBox.setEditable(false);
        comboBox.setToolTipText("Hide popup menu items with explicit first motion and greater weight");
        comboBox.setSelectedItem(newProps.getProperty("phasePopupMenu.maxWt4HumanFm", "1"));
        comboBox.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent evt) {
               JComboBox comboBox = (JComboBox) evt.getSource();
               newProps.setProperty("phasePopupMenu.maxWt4HumanFm", (String)comboBox.getSelectedItem());
           }
        }); 
        comboBox.setMaximumSize(new Dimension(50,20));
        hbox.add(comboBox);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

         // PhaseDescription first motion quality value fmQualCut
        jlbl = new JLabel("Remove auto fm above wt ");
        jlbl.setToolTipText("Remove auto first motions in phase descriptors having lesser quality");
        hbox = Box.createHorizontalBox();
        hbox.add(jlbl);
        //jtf = new JTextField(df2.format(newProps.getFloat("firstMoQualityMin")));
        //jtf.getDocument().addDocumentListener(new UpdateDocListener("firstMoQualityMin",jtf));
        //jtf.setMaximumSize(new Dimension(30,20));
        //hbox.add(jtf);
        sList = new java.util.ArrayList(4);
        for (int idx=0; idx<=4; idx++) sList.add(String.valueOf(idx));
        comboBox = new JComboBox(sList.toArray());
        comboBox.setEditable(false);
        int wt = PhaseDescription.toWeight(newProps.getDouble("firstMoQualityMin", 0.7));
        comboBox.setSelectedItem(String.valueOf(wt));
        comboBox.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent evt) {
               JComboBox comboBox = (JComboBox) evt.getSource();
               newProps.setProperty( "firstMoQualityMin", PhaseDescription.toQuality( Integer.parseInt((String)comboBox.getSelectedItem()) ));
           }
        }); 
        comboBox.setMaximumSize(new Dimension(50,20));
        hbox.add(comboBox);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);
        
        jcb = makeUpdateCheckBox("Popup confirmation to remove a fm when wt > cutoff wt",
                "When hand editing descriptors (e.g. via hot-keys or popup dialog)", "phasePopupMenu.phaseDescWtCheck");
        vbox.add(jcb);

        jcb = makeUpdateCheckBox("If no confirmation, always remove fm when wt > cutoff wt",
                "default is to NOT change the phase descriptor fm",
                "phasePopupMenu.alwaysResetLowQualFm");
        vbox.add(jcb);

        jcb = makeResetCheckBox("Hotkeys 0-4 change only wt",
                "default 0-4 mappings are auto fm descriptors: iP0, iP1, eP2, eP3, and eP4",
                "pickingPanelHotKeyNum2Wt");
        vbox.add(jcb);

        //hypoinvWgt2DeltaTime
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Delta times ");
        jlbl.setToolTipText("uncertainty seconds Arrival.DELTIM of mapped to the phase pick weight");
        hbox.add(jlbl);
        jtf = new JTextField(newProps.getProperty("hypoinvWgt2DeltaTime"));
        jtf.setPreferredSize(new Dimension(100, 16));
        //jtf.getDocument().addDocumentListener(new UpdateDocListener("hypoinvWgt2DeltaTime", jtf));
        jtf.getDocument().addDocumentListener(new MiscDocListener("hypoinvWgt2DeltaTime", jtf));
        jtf.setToolTipText("uncertainty seconds Arrival.DELTIM of phase pick");
        jtf.setMaximumSize(new Dimension(110,20));
        hbox.add(jtf);
        hbox.add(new JLabel(" for wgt 0, 1, 2, 3, 4"));
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        jcb = makeUpdateCheckBox("Allow first motion on horizontals","Human or auto c,d,+,- for the P or S phase","firstMoOnHoriz");
        vbox.add(jcb);

        jcb = makeUpdateCheckBox("Allow first motion on S","Human or auto c,d,+,- on any component", "firstMoOnS");
        vbox.add(jcb);
        //

        this.add(vbox);

    }
}
