package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.io.File;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.text.*;
import java.util.*;

import org.trinet.jasi.Waveform;
import org.trinet.jasi.AbstractWaveform;
import org.trinet.util.ValueChangedListener;
import org.trinet.util.ValueChangedEvent;
import org.trinet.util.graphics.text.NumberTextField;
import org.trinet.util.graphics.text.NumberTextFieldFactory;
import org.trinet.util.WaveClient;
import org.trinet.jasi.TN.WaveletTN;

/** A Dialog panel for choosing how waveforms will be read.
 Sets JiggleProperties.wavefromReadMode value. Allows editing of WaveServerGroup list. */
public class DPwaveSource extends JPanel {

     private JiggleProperties newProps;

     private JRadioButton dbButton = new JRadioButton();
     private JRadioButton wsButton = new JRadioButton();

     private JButton editButton = null;
     private JComboBox wsgBox = null;

     private ActionListener cbListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JComboBox src = (JComboBox) e.getSource();
                String name = (String) src.getSelectedItem();
                //System.out.println("DEBUG DPwaveSource combo box name: "+name);
                if (name != null) {
                    WaveServerGroup wsg = WaveServerGroup.getByName(newProps.getWaveServerGroupList(), name);
                    if (wsg != null) {
                        WaveServerGroup.setSelected(wsg, newProps.getWaveServerGroupList());
                        newProps.setProperty ("currentWaveServerGroup",  name); // 08/15/2006 -aww reset here
                    }
                }
            }
    };

    /** Allow selection of waveform reading mode. */
    public DPwaveSource(JiggleProperties props, MasterView mv) {

        newProps = props;
        // System.out.println(newProps.listToString());

        try  {
             initGraphics(mv);
        }
        catch(Exception ex) {
             ex.printStackTrace();
        }
    }

    /** Build the GUI.*/ 
    private void initGraphics(MasterView mv) throws Exception {

        configureRadioButtons(); // must do this first


        Box mainbox = Box.createVerticalBox();

        Box vbox = Box.createVerticalBox();
        vbox.setBorder(new TitledBorder("Waveforms Read From"));

        Box hbox = Box.createHorizontalBox();
        hbox.add(dbButton);
        JLabel jlbl = null;
        WFView wfv = null;
        if (mv != null && mv.wfvList != null) { 
          for (int ii = 0; ii < mv.wfvList.size(); ii++) { 
              wfv = (WFView) mv.wfvList.get(0);
              if (wfv.getWaveform() != null) break;
          }
        }
        //if (wfv != null) { 
          hbox.add(Box.createHorizontalStrut(20));
          jlbl = new JLabel("waveform archive copy ");
          jlbl.setToolTipText("Waveform archive file path copy (i.e. database Waveroots table wcopy column)");
          hbox.add(jlbl);

          int[] wcopies = null;
          Waveform wf = (wfv == null) ? null : wfv.getWaveform();
          if (wf == null) {
               wcopies = new int[] { 1 };
          }
          else {
               wcopies = WaveletTN.getWaverootCopies(
                                    wf.getChannelObj().getNet(),
                                    wf.getWaveArchive(),
                                    wf.getWaveType(),
                                    wf.getWaveStatus(),
                                    wf.getEpochStart()
                       );
          }
          String[] values = new String[wcopies.length]; 
          for (int idx=0; idx<wcopies.length; idx++) {
            values[idx] = String.valueOf(wcopies[idx]);
          }
          final JComboBox jcb = new JComboBox(values);
          jcb.setEditable(true);
          jcb.setSelectedItem(newProps.getProperty("waverootsCopy", "1"));
          jcb.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                //System.out.println("Setting waverootsCopy property to : " + (String)jcb.getSelectedItem());
                newProps.setProperty("waverootsCopy",(String)jcb.getSelectedItem());
            }
          });
          jcb.setToolTipText("Waveform file path copy (i.e. database Waveroots table wcopy column)");
          Dimension d = new Dimension(48,24);
          jcb.setPreferredSize(d);
          jcb.setMaximumSize(d);
          hbox.add(jcb);

          /*
          final NumberTextField ntf = NumberTextFieldFactory.createInputField(4, true, "#", getFont(), true);
          ntf.setDefaultValue(new Integer(1));
          ntf.setValue(newProps.getInt("waverootsCopy", 1));
          ntf.setToolTipText("Waveform file path copy (i.e. database Waveroots table wcopy column)");
          ntf.setPreferredSize(new Dimension(20,24));
          ntf.setMaximumSize(d);
          ntf.addValueChangedListener( new ValueChangedListener() {
            public void valueChanged(ValueChangedEvent evt) {
                newProps.setProperty("waverootsCopy", ntf.getIntValue());
            }
            public void propertyChange(PropertyChangeEvent evt) {
            }
          });
          //ntf.getDocument().addDocumentListener(new MiscDocListener("waverootsCopy", ntf));
          hbox.add(ntf);
          */
        //}

        hbox.add(Box.createGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        DecimalFormat df1 = new DecimalFormat ("####0");

        hbox = Box.createHorizontalBox();
        hbox.add(wsButton);
        hbox.add(Box.createGlue());
        hbox.add(Box.createHorizontalStrut(20));
        hbox.add(makeWsgBox());
        hbox.add(Box.createHorizontalStrut(10));
        hbox.add(makeEditButton());
        hbox.add(Box.createGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Connect to waveserver timeout(ms) ");
        hbox.add(jlbl);
        JTextField jtf = new JTextField(df1.format(newProps.getInt("waveServerConnectTimeout", WaveClient.DEFAULT_CONNECT_TIMEOUT)));
        jtf.setMinimumSize(new Dimension(16,16));
        jtf.setMaximumSize(new Dimension(64,20));
        jtf.setPreferredSize(new Dimension(48,20));
        jtf.getDocument().addDocumentListener(new MiscDocListener("waveServerConnectTimeout", jtf));
        jtf.setToolTipText("Connect to waveserver timeout millisecs");
        hbox.add(jtf);
        hbox.add(Box.createHorizontalGlue());
        vbox.add(hbox);

        mainbox.add(vbox);

        // waveform loader cache values
        vbox = Box.createVerticalBox();
        vbox.setBorder(new TitledBorder("Waveforms cached"));
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("From current cache ");
        hbox.add(jlbl);
        
        jtf = new JTextField(df1.format(newProps.getInt("cacheAbove", 10)));
        jtf.getDocument().addDocumentListener(new MiscDocListener("cacheAbove", jtf));
        jtf.setToolTipText("Waveform timeseries loaded above active one");
        hbox.add(jtf);

        jlbl = new JLabel(" above and ");
        hbox.add(jlbl);
        
        jtf = new JTextField(df1.format(newProps.getInt("cacheBelow", 10)));
        jtf.getDocument().addDocumentListener(new MiscDocListener("cacheBelow", jtf));
        jtf.setToolTipText("Waveform  timeseries loaded below active one");
        hbox.add(jtf);
        hbox.add(new JLabel(" below"));
        hbox.add(Box.createHorizontalGlue());
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);
        mainbox.add(vbox);

        vbox = Box.createVerticalBox();
        vbox.setBorder(new TitledBorder("Waveform cache directory"));

        JCheckBox wfCacheCheckBox = new JCheckBox("Waveform disk caching");
        wfCacheCheckBox.addItemListener(new BooleanPropertyCheckBoxListener("wfCache2File"));
        wfCacheCheckBox.setSelected(newProps.getBoolean("wfCache2File"));
        vbox.add(wfCacheCheckBox); // cache2File

        JCheckBox wfCachePurgedCheckBox = new JCheckBox("Purge cache on event change and exit");
        wfCachePurgedCheckBox.setSelected(newProps.getBoolean("wfCache2FilePurged"));
        wfCachePurgedCheckBox.addItemListener(new BooleanPropertyCheckBoxListener("wfCache2FilePurged"));
        vbox.add(wfCachePurgedCheckBox); // cache2FilePurged

        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Directory: ");
        hbox.add(jlbl);
        final JButton cacheWfDirButton = new JButton(newProps.getProperty("wfCache2FileDir", "."));
        cacheWfDirButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                try {
                  JFileChooser jfc = new JFileChooser();
                  jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                  jfc.setMultiSelectionEnabled(false);
                  String filename = newProps.getProperty("wfCache2FileDir", ".");
                  if (filename != null) jfc.setSelectedFile(new File(filename));
                  if (jfc.showDialog(DPwaveSource.this.getTopLevelAncestor(),"Select") == JFileChooser.APPROVE_OPTION) {
                      File file = jfc.getSelectedFile();
                      //System.out.println("DEBUG cacheWfDir: " +file); 
                      if (file.exists()) {
                         newProps.setProperty("wfCache2FileDir", file.getAbsolutePath());
                         cacheWfDirButton.setText(newProps.getProperty("wfCache2FileDir"));
                      }
                  }
              }
              catch(Exception ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(null,
                  "Change of waveform cache directory failed, check messages!",
                  "Properties", JOptionPane.PLAIN_MESSAGE);
              }
            }
        });
        cacheWfDirButton.setToolTipText("Click to change waveform cache directory");
        hbox.add(cacheWfDirButton);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);
        mainbox.add(vbox);

        mainbox.add(Box.createVerticalGlue());

        this.add(mainbox);

    }

    private class BooleanPropertyCheckBoxListener implements ItemListener { // inner class
        private String prop = null;

        public BooleanPropertyCheckBoxListener(String prop) {
            this.prop = prop;
        }

        public void itemStateChanged(ItemEvent e) {
            boolean state =(e.getStateChange() == ItemEvent.SELECTED);
            newProps.setProperty(prop, state);
        }
    } // end of inner class

    private class MiscDocListener implements DocumentListener { // inner class
        private JTextField jtf = null;
        private String prop = null;

        public MiscDocListener(String prop, JTextField jtf) {
            this.jtf = jtf;
            this.prop = prop;
        }

        public void insertUpdate(DocumentEvent e) {
            setValues(e);
        }
        public void removeUpdate(DocumentEvent e) {
            setValues(e);
        }
        public void changedUpdate(DocumentEvent e) {
            // we won't ever get this with a PlainDocument
        }

          /** Make changes to properties "on the fly". */
          private void setValues(DocumentEvent e) {
              // no sanity checking on numbers !
              newProps.setProperty(prop, jtf.getText().trim());
          }
    } // end of inner class

    private void configureRadioButtons() {
        // Set selected button BEFORE adding listeners, setSelected(true) triggers an action!
        if (newProps.getInt("waveformReadMode") == AbstractWaveform.LoadFromWaveServer) {
            wsButton.setSelected(true);
        } else {
            dbButton.setSelected(true);
        }

        Dimension d = new Dimension(100, 24);
        dbButton.setText("Database");
        dbButton.setMaximumSize(d);
        dbButton.setPreferredSize(d);
        dbButton.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if (! dbButton.isSelected()) return;
                newProps.setProperty("waveformReadMode", AbstractWaveform.LoadFromDataSource);
                // disable the OTHER selection
                wsgBox.setEnabled(false);
                editButton.setEnabled(false);
                JOptionPane.showMessageDialog(null,"Current ChannelTimeWindowModel = " +
                    newProps.getProperty("currentChannelTimeWindowModel", "Not defined") +
                    "\nPlease change this in model tab if not correct, or data may not be visible"); 
            }
        });

        wsButton.setText("WaveServer");
        wsButton.setMaximumSize(d);
        wsButton.setPreferredSize(d);
        wsButton.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if (! wsButton.isSelected()) return;
                newProps.setProperty("waveformReadMode", AbstractWaveform.LoadFromWaveServer);
                newProps.setupWaveServerGroup();
                setWsgList(newProps.getWaveServerGroupList());
                // disable the OTHER selection
                wsgBox.setEnabled(true);
                editButton.setEnabled(true);
                wsgBox.requestFocus();
                JOptionPane.showMessageDialog(null,"Current ChannelTimeWindowModel = " +
                    newProps.getProperty("currentChannelTimeWindowModel", "Not defined") +
                    "\nPlease change this in model tab if not correct, or data may not be visible"); 
            }
        });

        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(dbButton);
        buttonGroup.add(wsButton);
    }

    private JButton makeEditButton() {
        editButton = new JButton("Edit WaveServers");
        editButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                editButton_actionPerformed(e);
            }
        });
        editButton.setEnabled(wsButton.isSelected());
        return editButton;
    }

    private void editButton_actionPerformed(ActionEvent e) {
        WaveServerGroupEditDialog dialog = new WaveServerGroupEditDialog(newProps); // modal dialog opens
        // refresh combobox's view, wsg list may have changed by dialog add/delete action
        setWsgList(dialog.getProperties().getWaveServerGroupList());
    }

    /** Fill the JComboBox with the items in the properties WaveServerGroupList. */
    private JComboBox makeWsgBox() {
        wsgBox = new JComboBox();
        setWsgList(newProps.getWaveServerGroupList());
        wsgBox.setEnabled(wsButton.isSelected());
        if (wsgBox.isEnabled()) wsgBox.requestFocus();
        return wsgBox;
    }

    /** Set the list of WaveServerGroups displayed in the JComboBox. */
    private void setWsgList(Collection coll) {
        wsgBox.removeActionListener(cbListener);
        if (wsgBox.getItemCount() > 0) wsgBox.removeAllItems();

        ArrayList list = (ArrayList) coll;
        String name = null;
        if (list != null) {
          // populate the box from the list
          for (int i = 0; i<list.size(); i++) {
            name = ((WaveServerGroup)list.get(i)).getName();
            wsgBox.addItem(name);
          }
          // highlight currently selected itme
          WaveServerGroup sel = WaveServerGroup.getSelected(list);
          name = (sel == null) ?  "" : sel.getName();
          if (name != null && name.length() > 0) {
              newProps.setProperty ("currentWaveServerGroup",  name); // 08/15/2006 -aww reset here
          }
          wsgBox.setSelectedItem(name);
        }
        wsgBox.addActionListener(cbListener);
    }


}
