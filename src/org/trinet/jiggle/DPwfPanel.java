package org.trinet.jiggle;
import java.awt.*;
import java.awt.event.*;
import java.text.*;

import javax.swing.*;

import org.trinet.jiggle.common.PropConstant;

/**
 * Panel for insertion into dialog box to edit GUI layout of controls, waveform panels, etc.
 */
public class DPwfPanel extends AbstractPreferencesPanel {

    public DPwfPanel(JiggleProperties props) {

        super(props);

    }

    protected void initGraphics() {

        this.setLayout( new ColumnLayout() );

        Box hbox = null;
        Box vbox = null;
        JLabel jlbl = null;
        JTextField jtf = null;
        JCheckBox jcb = null;
        ButtonGroup bg = null;
        JRadioButton jrb = null;
        ActionListener al = null;

//GUI Layout

        vbox = Box.createVerticalBox();
        vbox.setBorder(BorderFactory.createTitledBorder("Waveform Panel Paint"));

        jcb = makeUpdateCheckBox("Antialias pick panel waveform", "antialiasWaveform");
        vbox.add(jcb);

        jcb = makeUpdateCheckBox("Show channel header box on group waveform panels","showRowHeaders");
        vbox.add(jcb);

        jcb = makeUpdateCheckBox("Show weight delta times on panels", "showDeltimes");
        vbox.add(jcb);

        jcb = makeUpdateCheckBox("Show pick residuals on panels", "showResiduals");
        vbox.add(jcb);

        jcb = makeUpdateCheckBox("Show cursor time as vertical line", "showCursorTimeAsLine");
        vbox.add(jcb);

        jcb = makeUpdateCheckBox("Show cursor amp as horizontal line", "showCursorAmpAsLine");
        vbox.add(jcb);

        jcb = makeUpdateCheckBox("Show time series samples on pick panel zoom", "showSamples");
        vbox.add(jcb);

        jcb = makeUpdateCheckBox("Show time series packet boundaries on pick panel zoom", "showSegments");
        vbox.add(jcb);

        jcb = makeUpdateCheckBox("Show HH:MM labels on time scale in waveform panels", "wfpanel.showTimeScaleLabel");
        vbox.add(jcb);

        jcb = makeUpdateCheckBox("Show time scale on waveform panels", "wfpanel.showTimeScale");
        vbox.add(jcb);

        // Paint the time scale ticks only at top of panels: 0=both 1=top 2=bottom (Waveform Paint)
        //wfpanel.timeTicksFlag=1
        int ttflag = newProps.getInt("wfpanel.timeTicksFlag", 0);
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Show time scale ticks at panel ");
        hbox.add(jlbl);
        al = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JRadioButton jrb = (JRadioButton) e.getSource();
                String cmd = jrb.getActionCommand();
                try {
                  int flag = -9;
                  if ( cmd.equals("top") && jrb.isSelected() ) {
                      flag = 1;
                  }
                  else if ( cmd.equals("bottom") && jrb.isSelected() ) {
                      flag = 2;
                  }
                  else if ( cmd.equals("both") && jrb.isSelected() ) {
                      flag = 0;
                  }
                  else if ( cmd.equals("none") && jrb.isSelected() ) {
                      flag = -1;
                  }
                  if (flag != -9) {
                     newProps.setProperty("wfpanel.timeTicksFlag", flag); 
                     updateGUI = true;
                  }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };

        bg = new ButtonGroup();
        jrb = new JRadioButton("top");
        jrb.addActionListener(al);
        jrb.setSelected((ttflag == 1)); 
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("bottom");
        jrb.addActionListener(al);
        jrb.setSelected((ttflag == 2)); 
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("both");
        jrb.addActionListener(al);
        jrb.setSelected((ttflag == 0)); 
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("none");
        jrb.addActionListener(al);
        jrb.setSelected((ttflag == -1)); 
        bg.add(jrb);
        hbox.add(jrb);

        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

// font of label (Waveform Paint)
        //wfpanel.channelLabel.font.name=SansSerif
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Channel label font ");
        jlbl.setToolTipText("SansSerif, Serif, Monospaced etc.");
        hbox.add(jlbl);
        jtf = new JTextField(newProps.getProperty("wfpanel.font.name", "Dialog"));
        jlbl.setToolTipText("SansSerif, Serif, Monospaced (Dialog, Arial ...) etc.");
        jtf.getDocument().addDocumentListener(new ResetDocListener("wfpanel.font.name",jtf));
        jtf.setMinimumSize(new Dimension(100,20));
        jtf.setMaximumSize(new Dimension(180,20));
        hbox.add(jtf);
        //wfpanel.channelLabel.font.style=BOLD
        final JComboBox jcbox = new JComboBox(new String[] {"PLAIN","BOLD","ITALIC"});
        jcbox.setMaximumSize(new Dimension(48,20));
        jcbox.setSelectedItem( newProps.getProperty("wfpanel.channelLabel.font.style","PLAIN") );
        jcbox.setEditable(false);
        jcbox.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                newProps.setProperty("wfpanel.channelLabel.font.style", (String)jcbox.getSelectedItem());
                updateGUI = true;
            }
        });
        jcbox.setMaximumSize(new Dimension(80,20));
        hbox.add(jcbox);

        //wfpanel.channelLabel.font.size=12
        jlbl = new JLabel(" pt size ");
        final JComboBox jcbox2 = new JComboBox(new String[] {"6","7","8","9","10","11","12","13","14","15","16","17","18"});
        jcbox2.setSelectedItem( newProps.getProperty("wfpanel.channelLabel.font.size","12"));
        jcbox2.setEditable(false);
        jcbox2.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                newProps.setProperty("wfpanel.channelLabel.font.size", (String)jcbox2.getSelectedItem());
                updateGUI = true;
            }
        });
        jcbox2.setMaximumSize(new Dimension(40,20));
        hbox.add(jlbl);
        hbox.add(jcbox2);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

// channel labeling
        //wfpanel.channelLabel.form=sncl, name only  =dist, include distance, =filter include filter description 
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Channel label elements ");
        jlbl.setToolTipText("Name only (sncl), name+distance, name+distance+azimuth, name+dist+azimuth+filter");
        hbox.add(jlbl);

        al = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JRadioButton jrb = (JRadioButton) e.getSource();
                newProps.setProperty("wfpanel.channelLabel.form", jrb.getActionCommand());
                updateGUI = true;
            }
        };

        String cform = newProps.getProperty("wfpanel.channelLabel.form", "sncl");
        bg = new ButtonGroup();
        jrb = new JRadioButton("sncl");
        jrb.addActionListener(al);
        jrb.setSelected(cform.equals("sncl"));
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("dist");
        jrb.addActionListener(al);
        jrb.setSelected(cform.equals("dist"));
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("Azimuth");
        jrb.addActionListener(al);
        jrb.setSelected(cform.equals(PropConstant.WAVEFORM_DESC_FORMAT_AZIMUTH.toLowerCase()));
        jrb.setActionCommand(PropConstant.WAVEFORM_DESC_FORMAT_AZIMUTH.toLowerCase());
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("filter");
        jrb.addActionListener(al);
        jrb.setSelected(cform.equals("filter"));
        bg.add(jrb);
        hbox.add(jrb);

        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

// channel labeling
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("Channel label placement ");
        hbox.add(jlbl);

        al = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JRadioButton jrb = (JRadioButton) e.getSource();
                String cmd = jrb.getActionCommand();
                try {
                  int channelLabelMode = -1;
                  if ( cmd.equals("Start") && jrb.isSelected() ) {
                      channelLabelMode = 0;
                  }
                  else if ( cmd.equals("Leader") && jrb.isSelected() ) {
                      channelLabelMode = 1;
                  }
                  else if ( cmd.equals("Trailer") && jrb.isSelected() ) {
                      channelLabelMode = 2;
                  }
                  else if ( cmd.equals("Both") && jrb.isSelected() ) {
                      channelLabelMode = 3;
                  }
                  else if ( cmd.equals("None") && jrb.isSelected() ) {
                      channelLabelMode = 4;
                  }

                  if (channelLabelMode != -1) {
                     newProps.setProperty("channelLabelMode", channelLabelMode); 
                     updateGUI = true;
                  }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };

        int chanLabelMode = newProps.getInt("channelLabelMode", 0);
        if (chanLabelMode < 0)  chanLabelMode = 0;

        bg = new ButtonGroup();
        jrb = new JRadioButton("Start");
        jrb.addActionListener(al);
        jrb.setSelected((chanLabelMode == 0)); 
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("Leader");
        jrb.addActionListener(al);
        jrb.setSelected((chanLabelMode == 1)); 
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("Trailer");
        jrb.addActionListener(al);
        jrb.setSelected((chanLabelMode == 2)); 
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("Both");
        jrb.addActionListener(al);
        jrb.setSelected((chanLabelMode == 3)); 
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("None");
        jrb.addActionListener(al);
        jrb.setSelected((chanLabelMode == 4)); 
        bg.add(jrb);
        hbox.add(jrb);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);


// masterview waveform alignment 
        hbox = Box.createHorizontalBox();
        jlbl = new JLabel("View alignment ");
        hbox.add(jlbl);

        al = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AbstractButton jrb = (AbstractButton) e.getSource();
                String cmd = jrb.getActionCommand();
                try {
                  if (cmd.startsWith("Scroll to")) {
                      WFScroller.scrollerMode = jrb.isSelected() ? WFScroller.SCROLL_TO_CENTER : WFScroller.SCROLL_TO_VISIBLE;
                      return;
                  }

                  int alignMode = -1;
                  if ( cmd.equals("T") && jrb.isSelected() ) {
                      alignMode = 0;
                  }
                  else if ( cmd.equals("P") && jrb.isSelected() ) {
                      alignMode = 1;
                  }
                  else if ( cmd.equals("S") && jrb.isSelected() ) {
                      alignMode = 2;
                  }
                  else if ( cmd.startsWith("V ") && jrb.isSelected() ) {
                      String vel = newProps.getProperty("masterViewWaveformAlignVel","");
                      vel = JOptionPane.showInputDialog(getTopLevelAncestor(), "Reduction Velocity", vel);
                      if (vel != null) {
                        double v = Double.parseDouble(vel);
                        if (v >= 0.3) { // speed of sound in air 
                          alignMode = 3;
                          newProps.setProperty("masterViewWaveformAlignVel", v);
                          jrb.setText("V "+v);
                        }
                        else {
                            JOptionPane.showMessageDialog(getTopLevelAncestor(),
                                    "Entered velocity < .3 km/s", "Invalid Reduction Velocity", JOptionPane.ERROR_MESSAGE);
                        }
                      }
                  }

                  if (alignMode != -1) {
                     newProps.setProperty("masterViewWaveformAlignMode", alignMode); 
                     updateGUI = true;
                  }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };

        int alignMode = newProps.getInt("masterViewWaveformAlignMode", 0);

        bg = new ButtonGroup();
        jrb = new JRadioButton("T");
        jrb.setToolTipText("align on absolute start times");
        jrb.addActionListener(al);
        if (alignMode < 0)  alignMode = 0;
        jrb.setSelected((alignMode == 0)); 
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("P");
        jrb.setToolTipText("align on predicted P times");
        jrb.addActionListener(al);
        jrb.setSelected((alignMode == 1)); 
        bg.add(jrb);
        hbox.add(jrb);

        jrb = new JRadioButton("S");
        jrb.setToolTipText("align on predicted S times");
        jrb.addActionListener(al);
        jrb.setSelected((alignMode == 2)); 
        bg.add(jrb);
        hbox.add(jrb);

        String vel = newProps.getProperty("masterViewWaveformAlignVel","");
        jrb = new JRadioButton("V "+ vel);
        jrb.setToolTipText("align by reduced velocity time");
        jrb.addActionListener(al);
        jrb.setSelected((alignMode == 3)); 
        bg.add(jrb);
        hbox.add(jrb);

        // Center in scroll panel
        hbox.add(Box.createHorizontalStrut(5));
        jcb = new JCheckBox("Scroll to center"); 
        jcb.setToolTipText("Scrolling arrow buttons scroll selection to center of group panel");
        jcb.setSelected(WFScroller.scrollerMode == WFScroller.SCROLL_TO_CENTER); 
        jcb.addActionListener(al);
        hbox.add(jcb);
        hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
        vbox.add(hbox);

        this.add(vbox);
    }
}
