package org.trinet.jiggle;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.event.*;
import org.trinet.jasi.*;
import org.trinet.jiggle.common.PropConstant;
import org.trinet.jiggle.common.type.OriginType;
import org.trinet.jiggle.common.type.OriginTypeList;
import org.trinet.jiggle.ui.panel.origin.PanelOriginTypes;
import org.trinet.util.*;
import org.trinet.util.gazetteer.LatLonZ;
import org.trinet.util.graphics.LatLonZChooser;
import org.trinet.util.graphics.DateTimeChooser;

/**
 * Create dialog for editing/setting solution parameters like magnitude and location.
 */
public class EventEditDialog extends CenteredDialog {

    private Solution sol;
    private DPmagnitude magPanel;
    private LatLonZChooser llzChooser;
    private DateTimeChooser dtChooser;
    private JCheckBox dtFixedCk;  // added option -aww 02/12/2007
    //private JCheckBox fixQuarryCk;
    private JCheckBox useTrialLocCk;
    private JCheckBox useTrialTimeCk;
    private JButton okButton;
    private JComboBox originQ = new JComboBox(new String [] { "A","B","C","D","U" });

    private boolean originHasChanged = false;

    private GenericPropertyList props;

    private JTabbedPane tabPane = new JTabbedPane();

    public final static int LLZTab = 0;
    public final static int MagTab = 1;

    // properties
    private boolean initDecimalLatLonStyle = true;
    private boolean initMagTabOnTop = true;

    // Origin types
    private OriginType selectedOriginType = null;

    public EventEditDialog(Solution sol) {
        this(sol, null, -1);
    }

    public EventEditDialog(Solution sol, GenericPropertyList props, int tabId) {
        this(sol, null, "Edit Solution Parameters", true, props, tabId);
    }

    public EventEditDialog(Solution sol, Frame frame, String title, boolean modal,
                           GenericPropertyList props, int tabId) {

        super(frame, title, modal);
        setSolution(sol);
        setProperties(props);

        try  {
            jbInit();
            if (tabId == LLZTab) tabPane.setSelectedIndex(LLZTab);
            else if (tabId == MagTab)  tabPane.setSelectedIndex(MagTab);
            okButton.setSelected(true);
            okButton.requestFocus();
            setVisible(true);
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    protected void setProperties(GenericPropertyList props) {
      if (props != null){
        this.props = props;
        if (props.isSpecified("EventEdit.decimalLatLon"))
            initDecimalLatLonStyle = props.getBoolean("EventEdit.decimalLatLon");
        if (props.isSpecified("EventEdit.magTabOnTop"))
            initMagTabOnTop = props.getBoolean("EventEdit.magTabOnTop");
      }
    }

    /** Return true if Decimal lat lon style is currently selected. */
    public boolean isDecimalLatLonStyle() {
        return llzChooser.latLonDecimalStyleSelected();
    }

    /** Return true if mag tab is currently on top. */
    public boolean isMagTabOnTop() {
        return (tabPane.getSelectedIndex() == MagTab);
    }

    /** Create the graphical component. */
    private void jbInit() throws Exception {

        // Make dialog button controls panel
        JButton cancelButton = new JButton();
        cancelButton.setText("CANCEL");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
               public void actionPerformed(ActionEvent e) {
                    cancelButton_actionPerformed(e);
               }
        });
        cancelButton.setMnemonic(KeyEvent.VK_C);
        okButton = new JButton();
        okButton.setText("OK");
        okButton.addActionListener(new java.awt.event.ActionListener() {
               public void actionPerformed(ActionEvent e) {
                    okButton_actionPerformed(e);
               }
        });
        okButton.setMnemonic(KeyEvent.VK_O);
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        // Make trial origin time panel
        JPanel dtPanel = new JPanel();
        dtPanel.setLayout(new GridLayout(0, 1));
        dtPanel.setBorder(BorderFactory.createTitledBorder("Trial Origin Time"));
        dtChooser = new DateTimeChooser();
        dtChooser.wholeSecs=false;
        dtChooser.setTrueSeconds(sol.getTime());
        dtPanel.add(dtChooser);
        dtFixedCk = new JCheckBox("Fix time", sol.timeFixed.booleanValue());
        dtPanel.add(dtFixedCk);

        // Make trial location/depth panel
        JPanel llzPanel = new JPanel();
        llzPanel.setBorder(BorderFactory.createTitledBorder("Trial Location and Depth"));
        llzPanel.setLayout(new GridLayout(0, 1));
        // LatLonZChooser
        int llzmode = LatLonZChooser.DM_MODE;
        if (initDecimalLatLonStyle) llzmode = LatLonZChooser.DEC_MODE;
        LatLonZ llz = sol.getLatLonZ();
        llz.setZ(sol.getModelDepth()); // need to use model depth for chooser, trial depth in hypoinverse -aww 2015/06/11
        llzChooser = new LatLonZChooser(llz,
                                        sol.locationFixed.booleanValue(),
                                        sol.depthFixed.booleanValue(),
                                        (sol.dummyFlag.longValue() == 1l),
                                        llzmode);
        llzPanel.add(llzChooser);

        JLabel jlq = new JLabel("Quality (set if known for imported event saved without relocation:");
        jlq.setToolTipText("Origin Quality A-D, or U unknown=0");
        originQ.setToolTipText("Origin Quality A-D, or U unknown=0");

        double q = sol.quality.doubleValue();
        if (Double.isNaN(q)) originQ.setSelectedItem("U");
        else if (q < .50) originQ.setSelectedItem("D");
        else if (q < .75) originQ.setSelectedItem("C");
        else if (q < 1.0) originQ.setSelectedItem("B");
        else originQ.setSelectedItem("A");

        originQ.setEditable(false);
        originQ.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String selection = (String) originQ.getSelectedItem();
                double qual = 0.;
                if (selection.equals("A")) qual = 1.;
                else if (selection.equals("B")) qual = .75;
                else if (selection.equals("C")) qual = .50;
                else if (selection.equals("D")) qual = .25;
                sol.quality.setValue(qual);
                //System.out.println("DEBUG selection" + selection + " = " quality: " + sol.quality.doubleValue() );
            }
          }
        );
        Box b = Box.createHorizontalBox();
        originQ.setMaximumSize(new Dimension(60,30));
        b.add(jlq);
        b.add(originQ);
        b.add(Box.createHorizontalGlue());
        llzPanel.add(b);

        // Per event or global trial value usage
        JPanel trialPanel = new JPanel();
        trialPanel.setLayout(new GridLayout(0, 1));
        trialPanel.setBorder(BorderFactory.createTitledBorder("For this event origin solutions"));

        CheckBoxListener checkBoxListener = new CheckBoxListener();
        useTrialTimeCk = new JCheckBox("Use Trial OriginTime", sol.useTrialTime() || props.getBoolean("useTrialOriginTime"));
        useTrialTimeCk.addItemListener(checkBoxListener);
        useTrialLocCk = new JCheckBox("Use Trial Location", sol.useTrialLocation() || props.getBoolean("useTrialLocation"));
        useTrialLocCk.addItemListener(checkBoxListener);

        if ( props.getBoolean("useTrialOriginTime")) {
          JLabel jl = new JLabel("Use trial origin time");
          jl.setToolTipText("Globally set for all events by Location Server Tab property");
          trialPanel.add(jl);
        }
        else {
            trialPanel.add(useTrialTimeCk);
        }

        if ( props.getBoolean("useTrialLocation")) {
          JLabel jl = new JLabel("Use trial location");
          jl.setToolTipText("Globally set for all events by Location Server Tab property)");
          trialPanel.add(jl);
        }
        else {
          trialPanel.add(useTrialLocCk);
        }

        //fixQuarryCk = new JCheckBox("Fix Quarry Depths", props.getBoolean("fixQuarryDepth"));
        //trialPanel.add(fixQuarryCk);
        //fixQuarryCk.addItemListener(checkBoxListener);

        // Container for all of the origin related panels
        Box vbox = Box.createVerticalBox();
        vbox.add(trialPanel);
        vbox.add(dtPanel);
        vbox.add(llzPanel);
        vbox.add(getOriginTypePanel());

        // Event magnitude editor panel
        magPanel = new DPmagnitude(sol, props.getStringArray("EventEdit.magTypeList"));

        // populate the tabs of the tabpane, this will set their order also
        tabPane.insertTab("Origin",  null, vbox, "Set event origin parameters", LLZTab);
        tabPane.insertTab("Magnitude", null, magPanel, "Set event preferred magnitude parameters.", MagTab);
        if (initMagTabOnTop) tabPane.setSelectedIndex(MagTab); // which on top LatLon or Mag
        tabPane.getModel().addChangeListener( new ChangeListener() {
            public void stateChanged(ChangeEvent ce) {
                okButton.setSelected(true);
                okButton.requestFocus();
            }
        });

        // Construct the dialog
        JPanel mainPanel  = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(tabPane, BorderLayout.CENTER);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        getContentPane().add(mainPanel);

        pack();
        centerDialog();
    }

    public OriginType getSelectedOriginType(){
        return this.selectedOriginType;
    }

    private JPanel getOriginTypePanel(){
        PanelOriginTypes originTypePanel = new PanelOriginTypes();
        JComboBox cmbOriginTypes = originTypePanel.getCmbOriginTypes();

        OriginTypeList originTypes = new OriginTypeList(this.props, false);
        cmbOriginTypes.setModel(new DefaultComboBoxModel(originTypes.getTypeList().toArray()));
        cmbOriginTypes.setEditable(false);

        String currOriginType = sol.getOriginType();
        if (!originTypes.getTypeList().contains(new OriginType(currOriginType, "", false))) {
            currOriginType = Solution.HYPOCENTER;
        }

        cmbOriginTypes.setSelectedItem(new OriginType(currOriginType, "", false)); // matching is done on "code" so desc not needed
        selectedOriginType = (OriginType) cmbOriginTypes.getSelectedItem();

        cmbOriginTypes.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                selectedOriginType = (OriginType) cmbOriginTypes.getSelectedItem();
            }
        });

        return originTypePanel;
    }

    private void cancelButton_actionPerformed(ActionEvent e) {
        returnValue = JOptionPane.CANCEL_OPTION;
        this.setVisible(false);    // dismiss dialog
    }

    private void okButton_actionPerformed(ActionEvent e) {
        returnValue = JOptionPane.OK_OPTION;
        this.setVisible(false);    // dismiss dialog
    }

    /** Set the solution. */
    public void setSolution(Solution sol) {
        this.sol = sol;
    }

    /** Returns the solution values as modified by this dialog. */
    public Magnitude getMagnitude() {
        return magPanel.getMag();
    }

    public boolean hasChangedOrigin() {
        return originHasChanged;
    }

    public boolean hasChangedMag() {
        return magPanel.hasChanged();
    }

    /** Returns the solution Origin values as modified by this dialog. */
    public Solution getSolution() {

        // Lat,lon,z
        LatLonZ llz = llzChooser.getLatLonZ();
        LatLonZ llzSol = sol.getLatLonZ();
        llzSol.setZ(sol.getModelDepth()); // need to use model depth for chooser, trial depth in hypoinverse -aww 2015/06/11

        if (!llz.equals(llzSol)) {
            sol.mdepth.setValue(llz.getZ()); // need mdepth in hypoinverse trial terminator record -aww 2015/06/11
            double datum = sol.calcOriginDatumKm();
            // subtract datum correction to the fixZ mdepth here - 2015/06/12 -aww
            llz.setZ((Double.isNaN(datum)) ? llz.getZ() : llz.getZ() - datum);
            sol.setLatLonZ(llz); // gdepth (datum removed)
            //System.out.println("LatLonZ has changed!");
            originHasChanged =true;
        }

        // Set bogus, force null to bogus -aww 08/04/2006
        boolean bogus = llzChooser.getBogus() || llz.isNull();
        sol.dummyFlag.setValue((bogus ? 1l : 0l));

        // depth fixed
        if (sol.depthFixed.booleanValue() != llzChooser.getFixDepth()) {
            sol.depthFixed.setValue(llzChooser.getFixDepth()) ;
            //System.out.println("FixDepth has changed!");
            originHasChanged =true;
        }
        // location fixed 
        if (sol.locationFixed.booleanValue() != llzChooser.getFixLatLon()) {
            sol.locationFixed.setValue(llzChooser.getFixLatLon()) ;
            //System.out.println("FixLatLon has changed!");
            originHasChanged =true;
        }

        // time
        double dt = dtChooser.getTrueSeconds(); // UTC added 2008/02/04 -aww
        double solTime = sol.getTime();
        // BUGFIX: need to check NaN, clone time is Double.NaN -aww 2009/09/01
        if (Double.isNaN(solTime) || (Math.abs(dt-solTime) > .01)) { 
            sol.setTime(dt);
            //System.out.println("Time has changed!");
            originHasChanged =true;
        }

        // time fixed
        if (sol.timeFixed.booleanValue() != dtFixedCk.isSelected()) {
            sol.timeFixed.setValue(dtFixedCk.isSelected());
            //System.out.println("TimeFixed has changed!");
            originHasChanged =true;
        }

        // Origin Type
        if (!sol.getOriginType().equals(this.getSelectedOriginType().getCode())) {
            sol.type.setValue(this.getSelectedOriginType().getCode());
            originHasChanged = true;
        }

        if (originHasChanged) {  // or just llz.isNull() ? -aww 2011/08/02
            double q = sol.quality.doubleValue();
            sol.clearOriginParameters();
            sol.quality.setValue(q);
            if (!sol.algorithm.toString().equals("HAND")) sol.algorithm.setValue("HAND");
        }

        return sol;
    }

    class CheckBoxListener implements ItemListener {
        public void itemStateChanged(ItemEvent e) {
            Object source = e.getItemSelectable();
            if (source == useTrialLocCk) {
                //props.setProperty("useTrialLocation", useTrialLocCk.isSelected() );
                sol.setUseTrialLocation(useTrialLocCk.isSelected());
            } else if (source == useTrialTimeCk) {
                //props.setProperty("useTrialOriginTime", useTrialTimeCk.isSelected() );
                sol.setUseTrialTime(useTrialTimeCk.isSelected());
            }
            //else if (source == fixQuarryCk) {
                //props.setProperty("fixQuarryDepth", fixQuarryCk.isSelected() );
            //}
            //System.out.println("CheckBoxListener Origin Has Changed!");
            originHasChanged =true; // not really, but user may want to relocate to flag it here
        }
    }

}
