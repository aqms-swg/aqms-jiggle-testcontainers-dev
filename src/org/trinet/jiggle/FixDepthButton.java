package org.trinet.jiggle;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import org.trinet.jdbc.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.datatypes.*;

import org.trinet.util.*;
import org.trinet.util.graphics.*;

/**
 */
public class FixDepthButton extends JButton implements ChangeListener, ListDataStateListener {

    Jiggle jiggle;
    //ActiveSolutionList solModel;

    Color fixedColor = Color.red;
    Color freeColor  = getBackground();    // default
    Icon lockIcon = null;
    Icon unlockIcon = null;

    {
        Image image = IconImage.getImage("lock_z.gif");
        if (image != null) lockIcon = new ImageIcon(image);
        image = IconImage.getImage("unlock_z.gif");
        if (image != null) unlockIcon = new ImageIcon(image);
    }

    boolean fixed = false;

    public FixDepthButton()  {
//      setEnabled(false);

      setActionCommand("Z");
      if (lockIcon == null) setText("Z");
      else setIcon(unlockIcon);
      setToolTipText("Toggle solution depth fixed, unfixed");

      setAlignmentY(CENTER_ALIGNMENT);
      setHorizontalTextPosition(AbstractButton.CENTER);
      setVerticalTextPosition(AbstractButton.BOTTOM);
      setMargin(new Insets(0,0,0,0));
      int ypix = (jiggle.getProperties().getBoolean("miniToolBarButtons")) ?  20 : 24;
      setPreferredSize(new Dimension(ypix,ypix));
      setMaximumSize(new Dimension(ypix,ypix));

    }

/**
 * Allow selection from a list of Solutions in the MasterView.
 * They are displayed as ID's.
 * The events in the JComboBox are color coded so the user can
 * connect phases with events by color.
 */
    public FixDepthButton(Jiggle jiggle) {

      this();

      this.jiggle = jiggle;

      setSolution();

    }

    /** Set button attributes to match solution. */
    public void setSolution() {
       setSolution((Solution)jiggle.mv.solList.getSelected());
    }

    /** Set button attributes to match solution. */
    public void setSolution(Solution sol) {

        if ( sol == null ) return;

        fixed = sol.depthFixed.booleanValue();
        
        SwingUtilities.invokeLater( // aww 
             new Runnable() {
               public void run() {
                   if (fixed) {
                      setBackground(fixedColor);
                      if (lockIcon != null) setIcon(lockIcon);
                      setToolTipText("Unfix the solution depth");
                   } else {
                      setBackground(freeColor);
                      if (unlockIcon != null) setIcon(unlockIcon);
                      setToolTipText("Fix the solution depth");
                   }
               }
             }
        );
    }

    /** Toggle the fixed state of the current event. Returns the fix state. */
    public boolean toggle() {
        Solution sol =  jiggle.mv.getSelectedSolution();

        // is fixed
        if (sol.depthFixed.booleanValue()) {
            sol.depthFixed.setValue(false);// unfix it
            jiggle.mv.solList.setSelected(sol);   // notify
            sol.setStale(true); // aww 06/16/2006
        // ain't fixed
        } else {
            DataDouble result = depthDialog();            // ask for value
            if (!result.isNull()) {
               double datum = sol.calcOriginDatumKm();
               // subtract datum correction to the fixZ mdepth here
               sol.depth.setValue((Double.isNaN(datum)) ? result.doubleValue() : result.doubleValue() - datum);
               sol.mdepth.setValue(result.doubleValue()); // depth in hypoinverse trial terminator record -aww 2015/06/11
               sol.depthFixed.setValue(true);
               jiggle.mv.solList.setSelected(sol);   // notify
               sol.setStale(true); // aww 06/16/2006
            }
        }
        setSolution();
        jiggle.updateFrameTitle(); // aww 06/16/2006
        if (jiggle.srlPhases != null) jiggle.srlPhases.resetSummaryTextArea(); // aww 01/07/2008
        return sol.depthFixed.booleanValue();
    }

    /** Dialog to get fix depth. Returned DataDouble is null if dialog is cancelled. */
    public DataDouble depthDialog() {

        DataDouble val = new DataDouble();

        if (jiggle.mv == null) return val;
        Solution sol = jiggle.mv.getSelectedSolution();

        if (sol == null) return val;

        // Get solution depth, default to 5 km if null
        //double fixZ = sol.depth.isValid() ? sol.depth.doubleValue() : 5.0;
        double fixZ = sol.mdepth.isValid() ? sol.mdepth.doubleValue() : 5.0;

        EventType etype = EventTypeMap3.getEventTypeByJasiType(sol.getEventTypeString());
        if (etype != null && etype.isDepthFixed()) {
          // for quarries depth is quarry fix depth if specified, else 0.
          fixZ = jiggle.getProperties().getBoolean("fixQuarryDepth") ? 
            jiggle.getProperties().getDouble("quarryFixDepth") : 0.;
        }

        Format fmt = new Format("%6.2f");
        String defStr = fmt.form(fixZ);

        String newStr = (String) JOptionPane.showInputDialog(
                                     null, "Enter depth (km)",
                                     "Fix Depth", JOptionPane.QUESTION_MESSAGE,
                                     null, null, defStr);

        if (newStr != null) {  // is null if [cancel] is hit
          newStr = newStr.trim();
          if (newStr.length() > 0) {
            val.setValue(Double.valueOf(newStr).doubleValue());
          }
        }

        return val;
    }



// ------ implements StateChangedListener
/** React to external change of Solution state (from solList). Some other component
* can change the Solution or the current Solution's fixed state. */
  public void stateChanged(ChangeEvent e) {
    Object arg = e.getSource();
    if (arg instanceof Solution || arg instanceof SolutionList) {
          setSolution(); // does invokeLater on graphics methods
    }
  }

// ListDataStateListener for list changes -aww
    public void intervalAdded(ListDataStateEvent e) {
       if (! (e.getSource() instanceof SolutionList)) return;
       setSolution(); // does invokeLater on graphics methods -aww
    }
    public void intervalRemoved(ListDataStateEvent e) {
       if (! (e.getSource() instanceof SolutionList)) return;
       setSolution();
    }
    public void contentsChanged(ListDataStateEvent e) {
       if (! (e.getSource() instanceof SolutionList)) return;
       setSolution();
    }
    public void orderChanged(ListDataStateEvent e) {
       if (! (e.getSource() instanceof SolutionList)) return;
       setSolution();
    }
    public void stateChanged(ListDataStateEvent e) {
       if (! (e.getSource() instanceof SolutionList)) return;
       setSolution();
    }

}
