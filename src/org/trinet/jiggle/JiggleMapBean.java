package org.trinet.jiggle;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import java.io.File;
import java.awt.Insets;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

import com.bbn.openmap.*;
import com.bbn.openmap.gui.*;
import com.bbn.openmap.layer.location.*;
import com.bbn.openmap.event.*;

import org.trinet.jiggle.map.*;
import org.trinet.jasi.*;

/**
 */
public class JiggleMapBean extends BufferedMapBean implements PropertyChangeListener {

  public static Jiggle jiggle = null;  //client object
  private static MapPropertyList  mapProps = null;

  public static MasterViewLayer masterViewLayer = null; // solutions in MasterView
  public static CatalogLayer catalogLayer = null;       // solutions in CatalogPanel
  public static StationLayer stationLayer = null;       // stations in ChannelList

  private float minZoomScale = MapPropertyList.DEFAULT_MIN_ZOOM_SCALE; 
  private float zoomOutFactor = MapPropertyList.DEFAULT_ZOOM_FACTOR; 
  private float zoomInFactor = (float) 1./zoomOutFactor;

  private boolean inProjListenerFlag = false;      //set true when proj listener method invoked
  private boolean constructorFinishedFlag = false; //set true after constructor work is finished:

  /** Action object for "Reset View" choice. */
  private final Action resetViewAction = new AbstractAction() {
      public void actionPerformed(ActionEvent evtObj) {
          setResetViewMapCenterScale();
      }
  };

  /** Action object for "Reset Zoom" choice. */
  private final Action resetZoomAction = new AbstractAction() {
      public void actionPerformed(ActionEvent evtObj) {
          setZoomScale();
      }
  };

  /**
  * Constructor creates an OpenMap map bean.
  * @param jiggle the Jiggle object.
  */
  private JiggleMapBean(Jiggle jiggle) {
    // NOTE need to get mapHandler from the Jiggle MapPanel need to get the LayerHandler
    this.jiggle = jiggle;
    this.jiggle.addPropertyChangeListener(this);

    // read from openmap.properties like file by jiggle init
    mapProps = new MapPropertyList(jiggle.getMapPropHandler().getProperties());

    setupZoomValues(mapProps);

    //add projection listener to track zoom-scale and enforce minimum when resizing map:
    addProjectionListener(new com.bbn.openmap.event.ProjectionListener() {
          public void projectionChanged( com.bbn.openmap.event.ProjectionEvent evtObj) {
            if(!inProjListenerFlag) { //not recursive call into this method
              inProjListenerFlag = true;       //set flag to prevent recursion
              if(getScale() < minZoomScale) setScale(minZoomScale);      
              inProjListenerFlag = false;      //clear in-method flag
            }
          }
    });


    //indicate constructor finished (for save-config in 'setLayers()' method):
    constructorFinishedFlag = true;
  }

  public void propertyChange(PropertyChangeEvent evt) {
    if (evt.getSource() == this.jiggle) {
       String name = evt.getPropertyName();
       //System.out.println("DEBUG JiggleMapBean propertyChange name = " + name); 
       if ( name.equals(Jiggle.NEW_CP_PROPNAME)       ||
            name.equals(Jiggle.NEW_MV_PROPNAME)       ||
            name.equals(Jiggle.NEW_MV_SLIST_PROPNAME) ||
            name.equals(Jiggle.NEW_MV_WFVIEW_PROPNAME) ||
            name.equals(Jiggle.NEW_VMODEL_PROPNAME)
          ) firePropertyChange(name, evt.getOldValue(), evt.getNewValue());
    }
  }

  /**
   * Creates an OpenMap map bean.
   * @param jiggle the Jiggle object.
   * @return map bean object or null if error.
   */
  public static JiggleMapBean createMapBean(Jiggle jiggle) {
    return  (jiggle == null) ? null : new JiggleMapBean(jiggle);
  }

  public void setLayers(LayerHandler lh) {

    if (lh == null) return;
    Layer [] layers = lh.getLayers();

    //for each layer
    for(int i=0; i<layers.length; ++i) {
      if (layers[i] instanceof CatalogLayer) { // from CatalogPanel table solutionList layer
        catalogLayer = (CatalogLayer)layers[i];
      }
      else if (layers[i] instanceof MasterViewLayer) { // from MasterView solList layer
        masterViewLayer = (MasterViewLayer)layers[i];
      }
      else if (layers[i] instanceof StationLayer) { // from MasterChannelList
        stationLayer = (StationLayer)layers[i];
      }
    }
  }

  /**
   * Return the layer handler.
   */
  public LayerHandler getLayerHandler() {
    return jiggle.mapPanel.getLayerHandler();
  }
  public MapHandler getMapHandler() {
    return jiggle.mapPanel.getMapHandler();
  }
  public PropertyHandler getPropertyHandler() {
    return jiggle.getMapPropHandler();
  }

  /**
   * Set the layers of the MapBean.
   * This gets called when layers are added, removed or updated.
   * @param evt LayerEvent
   */
  public void setLayers(LayerEvent evt)
  {
    super.setLayers(evt);
    //if not seperate ALL event type
    if (evt.getType() != LayerEvent.ALL) {
         //if constructor work is finished then allow properties
         // saving to file (constructor always fires this method
         // via 'mapLayerHandler.addLayerListener()' call):
      if (constructorFinishedFlag) {
          //mapProps.saveProperties("JiggleMapBean");
      }
    }
  }

  /**
   */
  public void configureControls(MapControlPanel mcp) {
    System.out.println("JiggleMapBean  -  configure controls");
    // uses the desired lat/lon for this program:
    MapNavigatePanel navPanelObj = mcp.getNavigatePanel(); 
    if(navPanelObj != null) { //navigation panel fetched OK; set center point
      //navPanelObj.setDefaultCenter( mapProps.getUserCenterLat(), mapProps.getUserCenterLon());
    }
    com.bbn.openmap.gui.ZoomPanel zoomPanelObj = mcp.getZoomPanel();
    if (zoomPanelObj != null) { //navigation panel fetched OK; set center point
      zoomPanelObj.setZoomInFactor(zoomInFactor);
      zoomPanelObj.setZoomOutFactor(zoomOutFactor);
    }

    //create "Reset View" button:
    final JButton rvButtonObj = new JButton("RV");
    Font fontObj = rvButtonObj.getFont();
    fontObj = fontObj.deriveFont(fontObj.getSize2D()-1.0f);
    rvButtonObj.setFont(fontObj);      //reduce font to reduce button size
    Insets marginsObj = rvButtonObj.getMargin();
    marginsObj.left /= 2;
    marginsObj.right /= 2;
    marginsObj.top /= 2;
    marginsObj.bottom /= 2;
    rvButtonObj.setMargin(marginsObj);
    rvButtonObj.setToolTipText("Reset map center and zoom");
    rvButtonObj.addActionListener(resetViewAction);   //set action for button

    //create "Reset Zoom" button:
    final JButton rzButtonObj = new JButton("RZ");
    rzButtonObj.setFont(fontObj);      //reduce font to reduce button size
    marginsObj = rzButtonObj.getMargin();
    marginsObj.left /= 2;
    marginsObj.right /= 2;
    marginsObj.top /= 2;
    marginsObj.bottom /= 2;
    rzButtonObj.setMargin(marginsObj);
    rzButtonObj.setToolTipText("Reset map zoom");
    rzButtonObj.addActionListener(resetZoomAction);   //set action for button

    final JPanel buttonPanelObj = new JPanel(new GridBagLayout());
    final GridBagConstraints constraints = new GridBagConstraints();
    constraints.weightx = 1.0;
    constraints.weighty = 1.0;
    constraints.gridx = 0;
    constraints.gridy = 0;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.CENTER;
    constraints.insets = new Insets(0,4,1,0);
    buttonPanelObj.add(rvButtonObj, constraints);  //add reset view button
    constraints.gridy++;
    constraints.insets = new Insets(1,4,0,0);
    buttonPanelObj.add(rzButtonObj, constraints);  //add reset zoom button
    mcp.add(buttonPanelObj);  //add button panel
    mcp.revalidate();
  }

  /**
   * Sets up the local zoom in and out values.
   * @param zoomFactorVal the zoom factor value to use.
   */
  private void setupZoomValues(MapPropertyList props) {
    // From QWClientBean

    minZoomScale = mapProps.getMinZoomScale();   

    float zoomFactorVal = mapProps.getZoomScaleFactor();   
    zoomOutFactor = zoomFactorVal;
    zoomInFactor = (zoomFactorVal != 0.0f) ? 1.0f/zoomFactorVal : 1.0f;
  }

  /**
   * Sets the center point for the map bean.
   * @param latVal latitude value.
   * @param lonVal longitude value.
   */
  public void setCenter(final float latVal,final float lonVal) {
    // From MapBean
    if(SwingUtilities.isEventDispatchThread()) super.setCenter(latVal,lonVal);
    else {
      SwingUtilities.invokeLater(new Runnable() {
            public void run() {
              JiggleMapBean.super.setCenter(latVal,lonVal);
            }
      });
    }
  }

  /**
   * Sets the zoom-scale value for the map bean.
   * @param scaleVal the zoom-scale value to use.
   */
  public void setScale(final float scaleVal) {
    // From MapBean
    if(SwingUtilities.isEventDispatchThread()) super.setScale(scaleVal);
    else {
      SwingUtilities.invokeLater(new Runnable() {
            public void run() {
              JiggleMapBean.super.setScale(scaleVal);
            }
      });
    }
  }

  /**
   * Sets the center point and scale value for the map bean.
   * @param latVal latitude value.
   * @param lonVal longitude value.
   * @param scaleVal the zoom-scale value to use.
   */
  public void setCenterAndScale(final float latVal,final float lonVal, final float scaleVal) {
    // From QWClientBean
    if(SwingUtilities.isEventDispatchThread()) {
      super.setCenter(latVal,lonVal);
      super.setScale(scaleVal);
    }
    else {
      SwingUtilities.invokeLater(new Runnable() {
            public void run() {
              JiggleMapBean.super.setCenter(latVal,lonVal);
              JiggleMapBean.super.setScale(scaleVal);
            }
      });
    }
  }

  /**
   * Pans the map bean.
   * @param degVal direction to pan in.
   */
  public void panMapBean(final float degVal) {
    // From QWClientBean
    if(SwingUtilities.isEventDispatchThread()) pan(new PanEvent(this,degVal));
    else {
      SwingUtilities.invokeLater(new Runnable() {
            public void run() {
              pan(new PanEvent(JiggleMapBean.this,degVal));
            }
      });
    }
  }

  /**
   * Zooms the map bean.
   * @param zoomVal zooms the map by a multiple of the given value.
   */
  public void zoomMapBean(final int zoomVal) {
    // From QWClientBean
    if(SwingUtilities.isEventDispatchThread()) {
      zoom(new ZoomEvent(JiggleMapBean.this, ZoomEvent.RELATIVE,
           ((zoomVal>=0) ? zoomVal*zoomOutFactor : -zoomVal*zoomInFactor)));
    }
    else {
      SwingUtilities.invokeLater(new Runnable() {
            public void run() {
              zoom(new ZoomEvent(JiggleMapBean.this, ZoomEvent.RELATIVE,
                   ((zoomVal>=0) ? zoomVal*zoomOutFactor : -zoomVal*zoomInFactor)));
            }
      });
    }
  }

  /**
   * Zoom the Map.
   * Part of the ZoomListener interface.  Sets the scale of the
   * MapBean projection, based on a relative or absolute amount.
   *
   * @param evt the ZoomEvent describing the new scale.
   */
  public void zoom(ZoomEvent evt) {
    // From MapBean
    float newScale = 0.f;
    if (evt.isAbsolute())
      newScale = evt.getAmount();
    else if (evt.isRelative())
      newScale = getScale() * evt.getAmount();
    else
      return;

    if (newScale < minZoomScale) newScale = minZoomScale;
    setScale(newScale);
  }

  /**
   * Sets the "user" center latitude/longitude and zoom-scale for the given
   * map bean.
   */
  public void setUserMapCenterScale() {
    // From QWClientBean
    //set center point and zoom-scale for map:
    setCenterAndScale(
        mapProps.getUserCenterLat(),
        mapProps.getUserCenterLon(),
        mapProps.getUserMapScale()
    );
  }

  /**
   * Sets the Reset-View center latitude/longitude and zoom-scale for the
   * given map bean.
   */
  public void setResetViewMapCenterScale() {
    // From QWClientBean
    setCenterAndScale(
        mapProps.getResetCenterLat(),
        mapProps.getResetCenterLon(),
        mapProps.getResetMapScale()
    );
  }

  /**
   * Sets the zoom-scale for the given map bean.
   */
  public void setZoomScale() {
    // From QWClientBean
    setScale(mapProps.getResetMapScale());
  }

  /**
   * Centers map at the latitude and longitude of the input Location.
   * @param obj a 'Location' instance 
   */
  public void centerByLocationObj(Object obj) {
    // From QWClientBean
      if (obj instanceof Location) {
          Location l = (Location) obj;
          setCenter(l.lat,l.lon); //center map
      }
  }

  /**
   * Return the CatalogLayer object (CatalogPanel Solutions).
   */
  public static CatalogLayer getCatalogLayer() {
    return catalogLayer;
  }

  /**
   * Return the MasterViewLayer object (MasterView Solutions).
   */
  public static MasterViewLayer getMasterViewLayer() {
    return masterViewLayer;
  }

  /**
   * Return the StationLayer object (MasterChannelList stations).
   */
  public static StationLayer getStationLayer() {
    return stationLayer;
  }

  public static ChannelList getChannelList() {
      return MasterChannelList.get();
  }

  public static MasterView getMasterView() {
      return jiggle.mv;
  }

  public static SolutionList getMvSolutionList() {
      return jiggle.mv.solList;
  }

  public static SolutionList getCpSolutionList() {
      if (jiggle == null || jiggle.catPane == null) {
          return null;
      }
      return jiggle.catPane.getSolutionList();
  }

}
