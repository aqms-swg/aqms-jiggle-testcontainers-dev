/*
//
//Note: SoCal kludge JiggleSolutionSolver also has property: defaultTriggerEventType=earthquake defaultTriggerGType=local
//
// Property names specific to Jiggle as of 12/04:
EventEdit.decimalLatLon EventEdit.magTabOnTop
EventEdit.decimalLatLon=true
EventEdit.magTabOnTop=true
EventEdit.magTypeList= h w s e l c d b n
LocationEngine WhereIsEngine whereUnits
ampStripDistance
ampStripValue
antialiasWaveform
autoRecalcMC autoRecalcMD autoRecalcML 
autoRefreshChannelList
autoDeleteOldLogs
cacheAbove cacheBelow
catalogColumnList
catalogLoadsFromFile
clippingAmpScalar.analog
clippingAmpScalar.digital.acc
clippingAmpScalar.digital.vel
clockQualityThreshold
codaStripDistance
codaStripValue
colorCatalogByType
color.seedchan.XX
confirmFinalOnSave
confirmLocationOnSave
confirmMenuDeleteAction
confirmNewSolutionAction
confirmWFPanelDeleteAction
confirmWFScanForMag
defaultTriggerEventType
defaultTriggerGType
defaultZoomFilterType
disableQuarryCheck
duplicateCheckDisabled
eventTypeCheckDisabled
fileChooserDir
fileMessageLogging
firstMoOnHoriz firstMoOnS firstMoQualityMin
fixQuarryDepth
group.horz.blockScrollFactor
group.horz.unitScrollPixels
helpFile
hypoinvCmdFile.default
hypoinvCmdFileEditing
hypoinvWgt2DeltaTime
iconDir
mainSplitOrientation
mainframeHeight mainframeWidth mainframeX mainframeY
mapInSplit
mapPropFilename
mapSplitOrientation
masterViewWaveformAlignMode
masterViewWaveformAlignVel
masterViewReplacePhasesByStaType
masterViewWaveformLoadMode
maxLogAgeDays
minDragTime
networkModeWAN
pcsPostingEnabled
pcsPostingState
phasePopupMenuFlat
pickStripDistance
pickStripValue
prefmagCheckDisabled
prefmagCheckValue
quarryFixDepth
savePropsOnExit
secsPerPage
showDeltimes showPhaseCues showResiduals showRowHeaders showSamples showSegments showCursorTimeAsLine showCursorAmpAsLine
solLockingDisabled
testConnection
tracesPerPage
useLoginDialog
versionCheckDisabled
waveformInTab
wfpPopupMenuFlat
zoomBiasButtonOn
zoomScaleList
//
// prop names below here are currently shared with other apps -aww
//
autoProcessing
eventSelectionProps
eventSelectionDefaultProps

channelCacheFilename
channelMatchMode
channelMatchLocation
channelUseFullName
channelTimeWindowModelList currentChannelTimeWindowModel
currentWaveServerGroup waveServerGroupList waveformReadMode waverootsCopy
dbTimeBase dbWriteBackEnabled
dbaseDomain dbaseDriver dbaseHost dbaseName dbasePasswd dbasePort dbaseUser
dbLastLoginURL
debug
jasiObjectType
listMatchMode=EQUIVALENT
localNetCode
velocityModelClassName
verbose

// prop names specific to Loc/Mag Engine Delegate also used by other apps
useTrialLocation
useTrialOriginTime
locEngProps locationEngine locationEngineAddress locationEnginePort locationEngineTimeoutMillis
delegateDebug
delegateLoadArrivalsFromDb
delegateLoadMagReadingsFromDb
magDataLoadClearsSolution
magDataLoadOption
magDataLoadClonesReadings
magEngine
mdMagEngineProps mdMagMethod mdMagMethodProps
mlMagEngineProps mlMagMethod mlMagMethodProps
mhMagEngineProps mhMagMethod mhMagMethodProps
mdDisable mlDisable mhDisable
*/
package org.trinet.jiggle;

import java.awt.Color;
import java.awt.Font;
import java.util.*;
//import java.net.InetAddress; // ? ref only in main test code
import javax.swing.JSplitPane;
import org.trinet.jasi.*;
import org.trinet.jasi.engines.HypoMagDelegatePropertyList;
import org.trinet.jasi.magmethods.AmpMagnitudeMethod;
import org.trinet.jiggle.common.PropConstant;
import org.trinet.util.graphics.ColorList;
import org.trinet.util.GenericPropertyList;

/**
 * Jiggle program properties. See PropertyList Class for more info.
 * Example of pseudo-properties file:
 * <pre>
EventEdit.decimalLatLon=true
EventEdit.magTabOnTop=true
EventEdit.magTypeList= h w s e l c d b n
LocationEngine=org.trinet.util.locationengines.LocationEngineHypoInverse
WhereIsEngine=org.trinet.util.gazetteer.TN.WheresFrom
ampStripDistance=600.
ampStripValue=1.
antialiasWaveform=false
autoRecalcMC=true
autoRecalcMD=true
autoRecalcML=true
autoRefreshChannelList=true
cacheAbove=50
cacheBelow=50
catalogColumnList=V DATETIME LAT LON Z MAG MTYP AUTH ETYPE GTYPE SRC GAP DIST RMS OBS USED Q ST PR COMMENT
catalog.loadFromFile=false
catalog.evidfile=catalog_evids.txt
channelCacheFilename=scedcChannelList.cache
channelDbLookUp=true
channelDebugDump=false
channelListCacheRead=true
channelListCacheWrite=false
channelListReadOnlyLatLonZ=false
channelMatchLocation=false
channelUseFullName=false
channelMatchMode=LOCATION
channelTimeWindowModelList=org.trinet.jasi.DataSourceChannelTimeModel,\
 org.trinet.jasi.SimpleChannelTimeModel, org.trinet.jasi.PicksOnlyChannelTimeModel,
clippingAmpScalar.analog=0.8
clippingAmpScalar.digital.acc=0.99
clippingAmpScalar.digital.vel=0.99
clockQualityThreshold=0.50
codaStripDistance=600.
codaStripValue=1.
currentChannelTimeWindowModel=org.trinet.jasi.DataSourceChannelTimeModel
currentWaveServerGroup=indian
dbLastLoginURL=jdbc\:oracle\:thin\:@MyHost.gps.caltech.edu\:1523\:bigdb
dbTimeBase=NOMINAL
dbWriteBackEnabled=true
dbaseDomain=gps.caltech.edu
dbaseDriver=oracle.jdbc.driver.OracleDriver
dbaseHost=MyHost
dbaseName=big
dbasePasswd=GuessMe
dbasePort=1523
dbaseUser=agitator
debug=true
defaultTriggerEventType=earthquake
defaultTriggerGType=local
defaultZoomFilterType=HIGHPASS
defaultZoomFilterAlways=false
delegateLoadArrivalsFromDb=true
delegateLoadMagReadingsFromDb=true
eventSelectionDefaultProps=eventSelection.props
eventSelectionProps=testEvent.props
fileChooserDir=.
fileMessageLogging=false
firstMoOnHoriz=false
firstMoOnS=false
firstMoQualityMin=0.5
fixQuarryDepth=true
group.horz.blockScrollFactor=.10
group.horz.unitScrollPixels=5
helpFile=https://aqms-swg.gitlab.io/jiggle-userdocs/install/index.html
hypoinvCmdFile.default=hypinst.
hypoinvCmdFileEditing=false
hypoinvWgt2DeltaTime = .02 .05 .10 .20 .40
iconDir=images
jasiObjectType=TRINET
localNetCode=CI
locationEngine=org.trinet.util.locationengines.LocationEngineHypoInverse
locationEngineAddress=diddle.gps.caltech.edu
locationEnginePort=6547
locationEngineTimeoutMillis=2000
locationEngineType=HYP2000
locEngineFixedZNotify=true
magDataLoadClearsSolution=false
magDataLoadClonesReadings=false
magDataLoadOption=m
magEngine=org.trinet.jasi.engines.TN.MagnitudeEngineTN
magStaleCommitNoop=false
magStaleCommitOk=true
mainSplitOrientation=0
mapInSplit=false
mapPropFilename=openmap.properties
mapSplitOrientation=0
mainframeHeight=741
mainframeWidth=1023
mainframeX=-2
mainframeY=4
mdDisable=false
mdMagEngineProps=mdMagEng.props
mdMagMethod=org.trinet.jasi.magmethods.HypoinvMdMagMethod
mdMagMethodProps=hypoMdMagMeth.props
mhMagMethod=org.trinet.jasi.magmethods.TN.CuspSoCalMhMagMethod
masterViewWaveformAlignMode=0
masterViewWaveformAlignVel=6.
masterViewReplacePhasesByStaType=false
masterViewWaveformLoadMode=3
minDragTime=1.0
mlMagEngineProps=mlMagEng.props
mlMagMethod=org.trinet.jasi.magmethods.TN.SoCalMlMagMethod
mlMagMethodProps=mlMagMeth.props
pickStripDistance=600.
pickStripValue=1.00
pcsPostingEnabled=false
pcsPostingState=PostProc Jiggle Selected 100 0
quarryFixDepth=0.0
savePropsOnExit=true
secsPerPage=60.0
showDeltimes=true
showPhaseCues=true
showCursorTimeAsLine=true
showCursorAmpAsLine=false
showRowHeaders=true
showSamples=true
showSegments=true
showResiduals=true
solCommitAllMags=false
solStaleCommitOk=false
tracesPerPage=5
useLoginDialog=false
useTrialLocation=false
useTrialOriginTime=false
velocityModelClassName=org.trinet.util.velocitymodel.HK_SoCalVelocityModel
verbose=true
waveServerGroupDefaultClient=org.trinet.waveserver.rt.WaveClientOld
waveServerGroupList=/india+0+0+false+false/china+0+0+false+false/korea+0+0+false+false/
waveformInTab=true
waveformReadMode=0
waverootsCopy=1
whereUnits=miles
zoomBiasButtonOn=false
zoomScaleList=1.00 2.00 5.00 10.00 20.00 30.00 60.00 90.00
zoom.horz.blockScrollFactor=.05
zoom.vert.blockScrollFactor=.10
waveformCommitOk=true
* </pre>
* @see: PropertyList
*/

public class JiggleProperties extends HypoMagDelegatePropertyList // JasiPropertyList
{
/**
* Environment variables below should be defined by system property mechanism when starting the JVM
* using the (-D<system_variable_name>=<value> switch.
* "JIGGLE_HOME" variable which defines the root directory of the application's default properties file.
* A "JIGGLE_USER_HOMEDIR" variable which defines the root directory path for a subdirectory named
* ".jiggle" where the user's properties file should be located.
*/

/**
 *  Constructor: reads no property file empty list.
 *  @see #setRequiredProperties()
 */
    public JiggleProperties() { }

/**
 *  Constructor: Makes a COPY of a property list. Doesn't reread the properties files,
 * assumes that was already done at instantiation of passed JiggleProperties object.
 * @see: JiggleProperties(String fileName)
 */
    public JiggleProperties(JiggleProperties props) {
        super(props);
        privateSetup();
    }

/**
 *  Constructor: input file is a name relative to the default user and system paths
 *  which are defined by System properties appending a default subdirectory of
 *  type "jiggle".
 */
    public JiggleProperties(String fileName) {
        super(fileName);  // does a reset()
        privateSetup();
    }

/**
*  Constructor: input filenames are not appended to constructed paths, but rather are
*  interpreted as specified (either relative to current working directory or a complete path).
*/
    public JiggleProperties(String userPropFileName, String defaultPropFileName) {
        super(userPropFileName, defaultPropFileName);
        privateSetup();
    }

    /*
    // Doug made constructor signature type like that below, but above constructor:
    // JiggleProperties(String userPropFileName, String defaultPropFileName) could be used instead.
    public JiggleProperties(String userPropFileName, boolean useUserDefaultPath)
        if (useUserDefaultPath) setFilename(userPropFileName); // appends a constructed user path
        // use String as a complete path/relative path vs. working dir
        else this.userPropertiesFileName = userPropertiesFileName;
        reset();
        privateSetup();
    }
    */

    public String getFiletype() {
        return (filetype == "") ? "jiggle" : filetype;  // property user home subdir type
    }

    /** Return a String of space-delimited property names used
     * by application classes utilizing this class.
     * */
    public List getKnownPropertyNames() {
        List aList = super.getKnownPropertyNames();
        List myList = classDeclaredPropertyNames();
        if (aList instanceof ArrayList)
            ((ArrayList)aList).ensureCapacity(aList.size() + myList.size());
        aList.addAll(myList);
        Collections.sort(aList);
        return aList;
    }


    public GenericPropertyList getCatalogProperties() {
        GenericPropertyList gpl = new GenericPropertyList();
        Enumeration en = propertyNames();
        String name = null;
        while (en.hasMoreElements()) {
            name = (String) en.nextElement();
            if (name.indexOf("atalog") > 0) gpl.setProperty(name, getProperty(name));
        }
        return gpl;
    }

    public static List classDeclaredPropertyNames() {

        ArrayList myList = new ArrayList(64);

        myList.add("EventEdit.decimalLatLon");
        myList.add("EventEdit.magTabOnTop");
        myList.add("EventEdit.magTypeList");
        myList.add("WhereIsEngine");
        myList.add("whereUnits");
        myList.add("ampStripDistance");
        myList.add("ampStripValue");
        myList.add("antialiasWaveform");
        myList.add("autoDeleteOldLogs");
        myList.add("autoRecalcMC");
        myList.add("autoRecalcMD");
        myList.add("autoRecalcML");
        myList.add("autoRefreshChannelList");
        myList.add("cacheAbove");
        myList.add("cacheBelow");
        myList.add("catalogColumnList");
        myList.add("channelCopyOnlySNCL");
        myList.add("clippingAmpScalar.analog");
        myList.add("clippingAmpScalar.digital.acc");
        myList.add("clippingAmpScalar.digital.vel");
        myList.add("clockQualityThreshold");
        myList.add("codaStripDistance");
        myList.add("codaStripValue");
        myList.add("colorCatalogByType");
        myList.add("confirmFinalOnSave");
        myList.add("confirmLocationOnSave");
        myList.add("confirmMenuDeleteAction");
        myList.add("confirmSolutionNewAction");
        myList.add("confirmWFPanelDeleteAction");
        myList.add("confirmWFScanForMag");
        myList.add("dbTrace");
        myList.add("defaultTriggerEventType");
        myList.add("defaultTriggerGType");
        myList.add("defaultWfType");
        myList.add("defaultZoomFilterType");
        myList.add("defaultZoomFilterAlways");
        myList.add("delegateDebug");
        myList.add("disableQuarryCheck");
        myList.add("eventTypeCheckDisabled");
        myList.add("eventTypeChoices");
        myList.add("fileChooserDir");
        myList.add("fileMessageLogging");
        myList.add("firstMoOnHoriz");
        myList.add("firstMoOnS");
        myList.add("firstMoQualityMin");
        myList.add("fixQuarryDepth");
        myList.add("group.horz.blockScrollFactor");
        myList.add("group.horz.unitScrollPixels");
        myList.add("helpFile");
        myList.add("hypoinvCmdFile.default");
        myList.add("hypoinvCmdFileEditing");
        myList.add("hypoinvWgt2DeltaTime");
        myList.add("iconDir");
        myList.add("labelToolBarButtons");
        myList.add("mainSplitOrientation");
        myList.add("mainframeHeight");
        myList.add("mainframeWidth");
        myList.add("mainframeX");
        myList.add("mainframeY");
        myList.add("mapInSplit");
        myList.add("mapPropFilename");
        myList.add("mapSplitOrientation");
        myList.add("masterViewLoadSpectralAmps");
        myList.add("masterViewLoadPeakGroundAmps");
        myList.add("masterViewRetrieveAmps");
        myList.add("masterViewRetrieveCodas");
        myList.add("masterViewRetrievePhases");
        myList.add("masterViewWaveformAlignMode");
        myList.add("masterViewWaveformAlignVel");
        myList.add("masterViewReplacePhasesByStaType");
        myList.add("masterViewWaveformLoadMode");
        myList.add("maxLogAgeDays");
        myList.add("minDragTime");
        myList.add("miniButtonWidth");
        myList.add("miniToolBarButtons");
        myList.add("networkModeWAN");
        myList.add("pcsPostingEnabled");
        myList.add("pcsPostingState");
        myList.add("phasePopupMenuFlat");
        myList.add("phasePopupMenu.P.desc");
        myList.add("phasePopupMenu.S.desc");
        myList.add("phasePopupMenu.S.minWgt");
        myList.add("phasePopupMenu.maxWt4HumanFm");
        myList.add("phasePopupMenu.phaseDescWtCheck");
        myList.add("phasePopupMenu.alwaysResetLowQualFm");
        myList.add("pickingPanelArrowLayout");
        myList.add("pickingPanelHotKeyNum2Wt");
        myList.add("pickFlagFont"); // make this a font string that can be parsed - aww
        myList.add("pickStripDistance");
        myList.add("pickStripValue");
        myList.add("prefmagCheckValue");
        myList.add("quarryFixDepth");
        myList.add("savePropsOnExit");
        myList.add("secsPerPage");
        myList.add("showCursorTimeAsLine");
        myList.add("showCursorAmpAsLine");
        myList.add("showDeltimes");
        myList.add("showHideTraceMenu");
        myList.add("showPhaseCues");
        //myList.add("showPickFlags");
        myList.add("showResiduals");
        myList.add("showRowHeaders");
        myList.add("showSamples");
        myList.add("showSegments");
        myList.add("solNextProcStateMask");
        myList.add("solPreferredRollbackEnabled");
        myList.add("solLockingDisabled");
        myList.add("stackPickingPanelButtons");
        myList.add("testConnection");
        myList.add("tracesPerPage");
        myList.add("triaxialScrollZoomWithGroup");
        myList.add("triaxialSelectionLocked"); // -aww 2010/09/01
        myList.add("triaxialZonH"); // -aww 2010/09/01
        myList.add("useLoginDialog");
        myList.add("versionCheckDisabled");
        myList.add("waveformInTab");
        myList.add("wfpPopupMenuFlat");
        myList.add("zoomScaleList");
        myList.add("zoom.cursorShowsPhyUnits");
        myList.add("zoom.horz.blockScrollFactor");
        myList.add("zoom.vert.blockScrollFactor");
        myList.add("zoomBiasButtonOn");

        Collections.sort(myList);

        return myList;
    }

/**
 * Set default values for essential properties so the program will work in the
 * absence of a user 'properties' file. Any values defined in the properties
 * file will override these.
 */
    public void setRequiredProperties() {

      setProperty("EventEdit.decimalLatLon", "true");
      setProperty("EventEdit.magTabOnTop", "true");
      setProperty("EventEdit.magTypeList", "h w s e l c d b n");
      setProperty("editableMagTypes", "Ml Md");

      setProperty("helpFile", "https://aqms-swg.gitlab.io/jiggle-userdocs/install/index.html");
      setProperty("hypoinvCmdFile.default", "hypinst.");
      setProperty("hypoinvCmdFileEditing", "false");
      setProperty("hypoinvWgt2DeltaTime",".02 .05 .10 .20 .40");
      setProperty("webSite", "https://aqms-swg.gitlab.io/jiggle-userdocs/config/");

      setProperty("savePropsOnExit", true);
      setProperty("debug", false);
      setProperty("debugSQL", "false");
      setProperty("debugCommit", "false");
      setProperty("delegateDebug", "false");
      setProperty("waveServerDebug", "false");
      setProperty("seedReaderVerbose", "false");
      //setProperty("seedReaderDebug", "false");

      setProperty("mainframeWidth", 1024);
      setProperty("mainframeHeight", 768);
      setProperty("mainframeX", 1);
      setProperty("mainframeY", 1);

      setProperty("group.horz.blockScrollFactor", .10);
      setProperty("group.horz.unitScrollPixels", 5);
      setProperty("tracesPerPage", 10);
      setProperty("secsPerPage", -1);
      setProperty("zoomBiasButtonOn", "false");
      setProperty("zoomScaleList", "1.00 2.00 5.00 10.00 20.00 30.00 60.00 90.00");
      setProperty("zoom.cursorShowsPhyUnits", "true");
      setProperty("zoom.horz.blockScrollFactor", .05);
      setProperty("zoom.vert.blockScrollFactor", .10);

      setProperty("antialiasWaveform", false);
      setProperty("minDragTime", .01);
      setProperty("waveformInTab", true);
      setProperty("mainSplitOrientation", JSplitPane.HORIZONTAL_SPLIT);
      setProperty("showDeltimes", true);
      setProperty("showSegments", false);
      setProperty("showPhaseCues", true);
      //setProperty("showPickFlags", true);
      setProperty("showRowHeaders", false);
      setProperty("showResiduals", "true");
      setProperty("showSamples", true);
      setProperty("showCursorTimeAsLine", true);
      setProperty("showCursorAmpAsLine", false);
      setProperty("showHideTraceMenu", true);

      setProperty("scopeMode", false);
      setProperty("scopeModeDuration", 60.);
      setProperty("scopeModeChannelFile", "scopeChannelList.txt");
      setProperty("scopeModeRefreshEndTime", "true");

      setProperty("listMatchMode", "EQUIVALENT");

      setProperty("channelDbLookUp", true);
      setProperty("channelGroupName", "RCG-TRINET");
      setProperty("channelListAddOnlyDbFound", true);
      // only For debug, default is using location:
        //setProperty("channelCopyOnlySNCL", "false");
        //setProperty("channelMatchLocation", "true");
        //setProperty("channelMatchMode", "LOCATION");

      setProperty("channelListCacheRead", true);
      setProperty("channelListCacheWrite", true);

      setProperty("channelListReadOnlyLatLonZ", false);

      String cacheName = getUserFilePath() + FILE_SEP + ChannelList.getDefaultCacheFilename();
      setProperty("channelCacheFilename",cacheName);
      setProperty("channelDebugDump", "false");
      setProperty("channelLabelMode", "0");
      setProperty("channelUseFullName", "false");
      setProperty("cacheAbove", 50);
      setProperty("cacheBelow", 50);
      setProperty("autoLoadCatalog", true);
      setProperty("autoRefreshChannelList", true);
      setProperty("autoProcessing", false);
      setProperty("autoDeleteOldLogs", false);
      setProperty("maxLogAgeDays", 14.);

      setProperty("ampStripDistance", 600.);
      setProperty("ampStripValue", 1.0);
      setProperty("codaStripDistance", 600.);
      setProperty("codaStripValue", 1.0);
      setProperty("pickStripDistance", 600.);
      setProperty("pickStripValue", 1.0);

      setProperty("codaFlagMode", 1);

      setProperty("firstMoQualityMin", 0.5);
      setProperty("firstMoOnS", false);
      setProperty("firstMoOnHoriz", false);
      setProperty("clippingAmpScalar.analog", AmpMagnitudeMethod.DEFAULT_A_CLIP );
      setProperty("clippingAmpScalar.digital.acc", AmpMagnitudeMethod.DEFAULT_DA_CLIP );
      setProperty("clippingAmpScalar.digital.vel", AmpMagnitudeMethod.DEFAULT_DV_CLIP );
      setProperty("clockQualityThreshold", 0.5);

      setProperty("fixQuarryDepth", true);     // fix quarries?
      setProperty("quarryFixDepth", 0.0);      // depth at which to fix quarries
      setProperty("useTrialLocation", false);  // use trial loc in relocs?
      setProperty("useTrialOriginTime", false); // use trial origin time in relocs?

      setProperty("autoRecalcML", true);      // auto recalc after relocs?
      setProperty("autoRecalcMD", true);      // auto recalc after relocs?
      setProperty("verbose", true);           // output detail

      setProperty("fileChooserDir", ".");        // set to home?
      setProperty("fileMessageLogging", "false"); // default is no log file 

      //Setting default below, may cause unexpected catalog when a user property value is missing but default value is set
      //so setting eventSelectionProps below acts as the "default", user can override 
      //setProperty("eventSelectionDefaultProps", "eventSelection.props");
      setProperty("eventSelectionProps", "eventSelection.props");

      setProperty("colorCatalogByType", 2);
      // define columns to show in CatalogPanel, evid is always shown.
      setProperty("catalogColumnList", "V B DATETIME TYPE LAT LON Z MAG MTYP MOBS AUTH ETYPE GTYPE" +
             " SRC GAP DIST RMS OBS USED Q ST PR OWHO MWHO COMMENT"); 

      setProperty("catalog.loadFromFile", "false"); // default is no file use event selection props
      setProperty("catalog.evidfile", "catalog_evids.txt"); // default

      //setProperty("waveServerGroupDefaultClient", "org.trinet.waveserver.rt.WaveClientNew");
      //setProperty("defaultWfType", "1");

      setProperty("waveformReadMode", AbstractWaveform.LoadFromDataSource);
      setProperty("waverootsCopy", 1);
      setProperty("waveServerGroupList", "/rts1+1+0+false+false");
      setProperty("currentWaveServerGroup", "rts1");

      setProperty("channelTimeWindowModelList",
                "org.trinet.jasi.DataSourceChannelTimeModel, "+
                "org.trinet.jasi.PowerLawTimeWindowModel, "+
                "org.trinet.jasi.TriggerChannelTimeWindowModel,"+
                "org.trinet.jasi.PicksOnlyChannelTimeModel,"+
                "org.trinet.jasi.MissingPicksChannelTimeModel,"+
                "org.trinet.jasi.MissingPickSimpleChannelTimeModel,"+
                "org.trinet.jasi.EnergyWindowModel,"+
                "org.trinet.jasi.NamedChannelTimeWindowModel,"+
                "org.trinet.jasi.WADataSourceChannelTimeModel,"+
                "org.trinet.jasi.MdDataSourceChannelTimeModel"
                );

      setProperty("currentChannelTimeWindowModel", "org.trinet.jasi.DataSourceChannelTimeModel");

      setProperty("org.trinet.jasi.DataSourceChannelTimeModel.debug", "false");
      setProperty("org.trinet.jasi.DataSourceChannelTimeModel.candidateListName", "RCG-TRINET");
      setProperty("org.trinet.jasi.DataSourceChannelTimeModel.includePhases", "false");
      setProperty("org.trinet.jasi.DataSourceChannelTimeModel.filterWfListByChannelList", "false");
      setProperty("org.trinet.jasi.DataSourceChannelTimeModel.rejectedStaTypes", "");
      setProperty("org.trinet.jasi.DataSourceChannelTimeModel.rejectedSeedChanTypes", "");
      setProperty("org.trinet.jasi.DataSourceChannelTimeModel.rejectedNetTypes", "");
      setProperty("org.trinet.jasi.DataSourceChannelTimeModel.rejectedLocationTypes", "");
      setProperty("org.trinet.jasi.DataSourceChannelTimeModel.allowedStaTypes", "");
      setProperty("org.trinet.jasi.DataSourceChannelTimeModel.allowedSeedChanTypes", "");
      setProperty("org.trinet.jasi.DataSourceChannelTimeModel.allowedNetTypes", "");
      setProperty("org.trinet.jasi.DataSourceChannelTimeModel.allowedLocationTypes", "");

      setProperty("org.trinet.jasi.PicksOnlyChannelTimeModel.debug", "false");
      setProperty("org.trinet.jasi.PicksOnlyChannelTimeModel.candidateListName", "RCG-TRINET");
      setProperty("org.trinet.jasi.PicksOnlyChannelTimeModel.synchCandidateListBySolution", "false");
      setProperty("org.trinet.jasi.PicksOnlyChannelTimeModel.includeDataSourceWf", "false");
      setProperty("org.trinet.jasi.PicksOnlyChannelTimeModel.includePhases", "true");
      setProperty("org.trinet.jasi.PicksOnlyChannelTimeModel.allowedStaTypes", "");
      setProperty("org.trinet.jasi.PicksOnlyChannelTimeModel.allowedSeedChanTypes", "");
      setProperty("org.trinet.jasi.PicksOnlyChannelTimeModel.allowedNetTypes", "");
      setProperty("org.trinet.jasi.PicksOnlyChannelTimeModel.allowedLocationTypes", "");
      setProperty("org.trinet.jasi.PicksOnlyChannelTimeModel.rejectedStaTypes", "");
      setProperty("org.trinet.jasi.PicksOnlyChannelTimeModel.rejectedSeedChanTypes", "");
      setProperty("org.trinet.jasi.PicksOnlyChannelTimeModel.rejectedNetTypes", "");
      setProperty("org.trinet.jasi.PicksOnlyChannelTimeModel.rejectedLocationTypes", "");

      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.debug", "false");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.candidateListName", "RCG-TRINET");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.useMasterListCandidateList", "false");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.maxNoEnergySta", "10");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.includeDataSourceWf", "false");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.includeAllComponents", "true");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.includeAllMag", "3.5");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.maxDistance", "999");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.maxWindowSize", "300");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.minWindowSize", "60");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.minDistance", "30");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.velConstant", "120.0");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.velExponent", "1.3");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.accConstant", "40.0");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.accExponent", "1.8");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.allowedStaTypes", "");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.allowedSeedChanTypes", "");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.allowedNetTypes", "");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.allowedLocationTypes", "");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.rejectedStaTypes", "");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.rejectedSeedChanTypes", "");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.rejectedNetTypes", "");
      setProperty("org.trinet.jasi.PowerLawTimeWindowModel.rejectedLocationTypes", "");

      setProperty("org.trinet.jasi.TriggerChannelTimeWindowModel.debug", "false");
      setProperty("org.trinet.jasi.TriggerChannelTimeWindowModel.candidateListName", "RCG-TRINET");
      setProperty("org.trinet.jasi.TriggerChannelTimeWindowModel.includeDataSourceWf", "false");
      setProperty("org.trinet.jasi.TriggerChannelTimeWindowModel.includeAllComponents", "true");
      setProperty("org.trinet.jasi.TriggerChannelTimeWindowModel.includeAllMag", "3.5");
      setProperty("org.trinet.jasi.TriggerChannelTimeWindowModel.allowedStaTypes", "");
      setProperty("org.trinet.jasi.TriggerChannelTimeWindowModel.allowedSeedChanTypes", "");
      setProperty("org.trinet.jasi.TriggerChannelTimeWindowModel.allowedNetTypes", "");
      setProperty("org.trinet.jasi.TriggerChannelTimeWindowModel.allowedLocationTypes", "");
      setProperty("org.trinet.jasi.TriggerChannelTimeWindowModel.rejectedStaTypes", "");
      setProperty("org.trinet.jasi.TriggerChannelTimeWindowModel.rejectedSeedChanTypes", "");
      setProperty("org.trinet.jasi.TriggerChannelTimeWindowModel.rejectedNetTypes", "");
      setProperty("org.trinet.jasi.TriggerChannelTimeWindowModel.rejectedLocationTypes", "");

      setProperty("org.trinet.jasi.EnergyWindowModel.debug", "false");
      setProperty("org.trinet.jasi.EnergyWindowModel.candidateListName", "RCG-TRINET");
      setProperty("org.trinet.jasi.EnergyWindowModel.includeDataSourceWf", "false");
      setProperty("org.trinet.jasi.EnergyWindowModel.allowedStaTypes", "");
      setProperty("org.trinet.jasi.EnergyWindowModel.allowedSeedChanTypes", "");
      setProperty("org.trinet.jasi.EnergyWindowModel.allowedNetTypes", "");
      setProperty("org.trinet.jasi.EnergyWindowModel.allowedLocationTypes", "");
      setProperty("org.trinet.jasi.EnergyWindowModel.rejectedStaTypes", "");
      setProperty("org.trinet.jasi.EnergyWindowModel.rejectedSeedChanTypes", "");
      setProperty("org.trinet.jasi.EnergyWindowModel.rejectedNetTypes", "");
      setProperty("org.trinet.jasi.EnergyWindowModel.rejectedLocationTypes", "");

      setProperty("org.trinet.jasi.MdDataSourceChannelTimeModel.debug", "false");
      setProperty("org.trinet.jasi.MdDataSourceChannelTimeModel.candidateListName", "RCG-TRINET");
      setProperty("org.trinet.jasi.MdDataSourceChannelTimeModel.includePhases", "false");
      setProperty("org.trinet.jasi.MdDataSourceChannelTimeModel.filterWfListByChannelList", "false");
      setProperty("org.trinet.jasi.MdDataSourceChannelTimeModel.allowedSeedChanTypes", "HHZ EHZ SHZ");
      setProperty("org.trinet.jasi.MdDataSourceChannelTimeModel.allowedStaTypes", "");
      setProperty("org.trinet.jasi.MdDataSourceChannelTimeModel.allowedNetTypes", "");
      setProperty("org.trinet.jasi.MdDataSourceChannelTimeModel.allowedLocationTypes", "");
      setProperty("org.trinet.jasi.MdDataSourceChannelTimeModel.rejectedStaTypes", "");
      setProperty("org.trinet.jasi.MdDataSourceChannelTimeModel.rejectedSeedChanTypes", "");
      setProperty("org.trinet.jasi.MdDataSourceChannelTimeModel.rejectedNetTypes", "");
      setProperty("org.trinet.jasi.MdDataSourceChannelTimeModel.rejectedLocationTypes", "");

      setProperty("org.trinet.jasi.MissingPicksChannelTimeModel.debug", "false");
      setProperty("org.trinet.jasi.MissingPicksChannelTimeModel.candidateListName", "RCG-TRINET");
      setProperty("org.trinet.jasi.MissingPicksChannelTimeModel.synchCandidateListBySolution", "false");
      setProperty("org.trinet.jasi.MissingPicksChannelTimeModel.includePhases", "true");
      setProperty("org.trinet.jasi.MissingPicksChannelTimeModel.allowedStaTypes", "");
      setProperty("org.trinet.jasi.MissingPicksChannelTimeModel.allowedSeedChanTypes", "");
      setProperty("org.trinet.jasi.MissingPicksChannelTimeModel.allowedNetTypes", "");
      setProperty("org.trinet.jasi.MissingPicksChannelTimeModel.allowedLocationTypes", "");
      setProperty("org.trinet.jasi.MissingPicksChannelTimeModel.rejectedStaTypes", "");
      setProperty("org.trinet.jasi.MissingPicksChannelTimeModel.rejectedSeedChanTypes", "");
      setProperty("org.trinet.jasi.MissingPicksChannelTimeModel.rejectedNetTypes", "");
      setProperty("org.trinet.jasi.MissingPicksChannelTimeModel.rejectedLocationTypes", "");

      setProperty("org.trinet.jasi.MissingPickSimpleChannelTimeModel.debug", "false");
      setProperty("org.trinet.jasi.MissingPickSimpleChannelTimeModel.candidateListName", "RCG-TRINET");
      setProperty("org.trinet.jasi.MissingPickSimpleChannelTimeModel.synchCandidateListBySolution", "false");
      setProperty("org.trinet.jasi.MissingPickSimpleChannelTimeModel.includeDataSourceWf", "false");
      setProperty("org.trinet.jasi.MissingPickSimpleChannelTimeModel.includePhases", "true");
      setProperty("org.trinet.jasi.MissingPickSimpleChannelTimeModel.includePeakAmps", "true");
      setProperty("org.trinet.jasi.MissingPickSimpleChannelTimeModel.allowedStaTypes", "");
      setProperty("org.trinet.jasi.MissingPickSimpleChannelTimeModel.allowedSeedChanTypes", "");
      setProperty("org.trinet.jasi.MissingPickSimpleChannelTimeModel.allowedNetTypes", "");
      setProperty("org.trinet.jasi.MissingPickSimpleChannelTimeModel.allowedLocationTypes", "");

      setProperty("org.trinet.jasi.NamedChannelTimeWindowModel.debug", "false");
      setProperty("org.trinet.jasi.NamedChannelTimeWindowModel.candidateListName", "RCG-TRINET");
      setProperty("org.trinet.jasi.NamedChannelTimeWindowModel.includeDataSourceWf", "false");
      setProperty("org.trinet.jasi.NamedChannelTimeWindowModel.includePhases", "false");
      setProperty("org.trinet.jasi.NamedChannelTimeWindowModel.includeMagAmps", "false");
      setProperty("org.trinet.jasi.NamedChannelTimeWindowModel.includePeakAmps", "false");
      setProperty("org.trinet.jasi.NamedChannelTimeWindowModel.includeCodas", "false");
      setProperty("org.trinet.jasi.NamedChannelTimeWindowModel.allowedStaTypes", "");
      setProperty("org.trinet.jasi.NamedChannelTimeWindowModel.allowedSeedChanTypes", "");
      setProperty("org.trinet.jasi.NamedChannelTimeWindowModel.allowedNetTypes", "");
      setProperty("org.trinet.jasi.NamedChannelTimeWindowModel.allowedLocationTypes", "");
      setProperty("org.trinet.jasi.NamedChannelTimeWindowModel.rejectedStaTypes", "");
      setProperty("org.trinet.jasi.NamedChannelTimeWindowModel.rejectedSeedChanTypes", "");
      setProperty("org.trinet.jasi.NamedChannelTimeWindowModel.rejectedNetTypes", "");
      setProperty("org.trinet.jasi.NamedChannelTimeWindowModel.rejectedLocationTypes", "");

      setProperty("org.trinet.jasi.WADataSourceChannelTimeModel.debug", "false");
      setProperty("org.trinet.jasi.WADataSourceChannelTimeModel.candidateListName", "RCG-TRINET");
      setProperty("org.trinet.jasi.WADataSourceChannelTimeModel.includePhases", "false");
      setProperty("org.trinet.jasi.WADataSourceChannelTimeModel.filterWfListByChannelList", "false");
      setProperty("org.trinet.jasi.WADataSourceChannelTimeModel.allowedSeedChanTypes", "HHE HHN HLE HLN HNN HNE HGN HGE");
      setProperty("org.trinet.jasi.WADataSourceChannelTimeModel.allowedStaTypes", "");
      setProperty("org.trinet.jasi.WADataSourceChannelTimeModel.allowedNetTypes", "");
      setProperty("org.trinet.jasi.WADataSourceChannelTimeModel.allowedLocationTypes", "");
      setProperty("org.trinet.jasi.WADataSourceChannelTimeModel.rejectedStaTypes", "");
      setProperty("org.trinet.jasi.WADataSourceChannelTimeModel.rejectedSeedChanTypes", "");
      setProperty("org.trinet.jasi.WADataSourceChannelTimeModel.rejectedNetTypes", "");
      setProperty("org.trinet.jasi.WADataSourceChannelTimeModel.rejectedLocationTypes", "");


      // Runtime loadable class
      //setProperty("LocationEngine", "org.trinet.util.locationengines.LocationEngineHypoInverse");
      setProperty("locationEngine", "org.trinet.util.locationengines.LocationEngineHypoInverse");
      // could use the location engine's name from engine instance instead of defined property -aww
      setProperty("locationEngineType", "HYP2000");
      setProperty("locationEnginePRT", "true");
      setProperty("locEngineFixedZNotify", "true");

      setProperty("WhereIsEngine", "org.trinet.util.gazetteer.TN.WheresFrom");
      setProperty("whereUnits", "km"); // aww  11/05/2004

      setProperty("iconDir", "images");  // no trailing "/"

      setProperty("localNetCode", EnvironmentInfo.getNetworkCode()); // 'CI' 'NC' etc/
      setProperty("jasiObjectType", "TRINET");  // could be "EARTHWORM"
      setProperty("dbaseDriver", org.trinet.jdbc.datasources.AbstractSQLDataSource.DEFAULT_DS_DRIVER);

      // user must redefine these in for use in his local network
      setProperty("locationEngineAddress", "bogus.gps.caltech.edu");
      setProperty("locationEnginePort", "6550");
      setProperty("locationEngineTimeoutMillis", "10000");

      // user must redefine these in for use in his local database
      setProperty("useLoginDialog", "false");
      setProperty("dbaseHost", "");
      setProperty("dbaseDomain", "");
      setProperty("dbaseName", "");
      setProperty("dbasePort", "");
      setProperty("dbaseUser", "browser"); // example of a "read-only" user name
      setProperty("dbasePasswd", "");
      setProperty("dbaseTNSname", "");
      setProperty("dbaseSubprotocol",org.trinet.jdbc.datasources.AbstractSQLDataSource.DEFAULT_DS_SUBPROTOCOL); 
      setProperty("dbTrace", "false");

      setProperty("delegateLoadArrivalsFromDb", false);
      setProperty("delegateLoadMagReadingsFromDb", true);

      setProperty("dbAttributionEnabled", true);
      setProperty("dbWriteBackEnabled", true);

      setProperty("magDataLoadClearsSolution", true);
      setProperty("magDataLoadClonesReadings", true);
      setProperty("magDataLoadOption", "mps");
      setProperty("magStaleCommitNoop", false);
      setProperty("magStaleCommitOk", true);
      setProperty("defaultPreferencesMagMethodType", "ml");
      setProperty("magEngine", "org.trinet.jasi.engines.TN.MagnitudeEngineTN");

      setProperty("mdDisable", "false");
      setProperty("mdMagEngineProps", "mdMagEng.props");
      setProperty("mdMagMethod", "org.trinet.jasi.magmethods.HypoinvMdMagMethod");
      setProperty("mdMagMethodProps", "hypoMdMagMeth.props");
      setProperty("mdTauOptionEnabled", "false");
      setProperty("mlDisable", "false");
      setProperty("mlMagEngineProps", "mlMagEng.props");
      setProperty("mlMagMethod", "org.trinet.jasi.magmethods.TN.RichterMlMagMethod");
      setProperty("mlMagMethodProps", "mlMagMeth.props");

      setProperty("solCommitAllMags", true);
      setProperty("solCommitOriginError", true);
      setProperty("solLockingDisabled", false);
      setProperty("solNextProcStateMask", "AHIFC");
      setProperty("solStaleCommitOk", false);
      setProperty("solPreferredRollbackEnabled", "false");

      setProperty("mapPropFilename", "openmap.properties");
      setProperty("mapInSplit", false);
      setProperty("mapSplitOrientation", "1");

      setProperty("color.pick.sightline", "ff808080"); // gray
      setProperty("color.pick.deltim", PickFlag.DEFAULT_DELTIM_COLOR);
      setProperty("color.pick.unused", PickFlag.DEFAULT_UNUSED_PICK_COLOR);
      setProperty("zeroBiasLineWidth", "2"); // pixels
      setProperty("color.tab.text.bg", "ffffffff");
      setProperty("color.tab.text.fg", "ff000000");
      setProperty("color.tab.text.selection.bg", "ffffff00");
      setProperty("color.tab.text.selection.fg", "ffff0000");

      setProperty("defaultTriggerEventType", "earthquake");
      setProperty("defaultTriggerGType", "local");
      setProperty("defaultZoomFilterType", "HIGHPASS");
      setProperty("defaultZoomFilterAlways", "false");

      setProperty("masterViewLoadSpectralAmps", "false");
      setProperty("masterViewLoadPeakGroundAmps", "false");
      setProperty("masterViewRetrieveAmps","true");
      setProperty("masterViewRetrieveCodas","true");
      setProperty("masterViewRetrievePhases","true");
      setProperty("masterViewWaveformAlignMode", "0");
      setProperty("masterViewWaveformAlignVel", "6.");
      setProperty("masterViewWaveformLoadMode", "3");
      setProperty("masterViewReplacePhasesByStaType", "true");

      setProperty("confirmFinalOnSave", "false");
      setProperty("confirmLocationOnSave", "true");
      setProperty("confirmMenuDeleteAction", "true");
      setProperty("confirmNewSolutionAction", "true");
      setProperty("confirmWFPanelDeleteAction", "true");
      setProperty("confirmWFScanForMag", "false");
      setProperty("prefmagCheckValue", 3.);
      setProperty("networkModeWAN", "false");
      setProperty("disableQuarryCheck", "false");
      setProperty("eventTypeCheckDisabled", "false");
      setProperty("versionCheckDisabled", "false");
      setProperty("prefmagCheckDisabled", "false");
      setProperty("duplicateCheckDisabled", "false");
      setProperty("eventTypeCheckDisabled", "false");
      setProperty("testConnection", "false");
      setProperty("eventTypeChoices", "eq ex ot qb sn uk");
      setProperty("labelToolBarButtons", "false");
      setProperty("miniButtonWidth", "32");
      setProperty("miniToolBarButtons", "true");
      setProperty("pcsPostingEnabled", "false");
      setProperty("pcsPostingState", "PostProc Jiggle Selected 100 0");
      setProperty("phasePopupMenuFlat", "true");
      setProperty("phasePopupMenu.P.desc","iP0 iP1 eP2 eP3");
      setProperty("phasePopupMenu.S.desc","iS0 iS1 eS2 eS3");
      setProperty("phasePopupMenu.S.minWgt", "0");
      setProperty("phasePopupMenu.maxWt4HumanFm", "1");
      setProperty("phasePopupMenu.phaseDescWtCheck", "true");
      setProperty("phasePopupMenu.alwaysResetLowQualFm", "false");
      setProperty("pickFlagFont", "BIG");
      setProperty("pickingPanelArrowLayout", "E");
      setProperty("pickingPanelHotKeyNum2Wt", "false");
      setProperty("stackPickingPanelButtons", "false");

      setProperty("printLocEngPhaseList", "true");
      setProperty("printMagEngReadingList", "true");
      setProperty("printMagnitudes", "true");
      setProperty("printSolutionPhaseList", "false");
      setProperty("printSolutions", "true");

      setProperty("scanNoiseType", "0");

      setProperty("enableSwarm", "true");
      setProperty("swarmInWaveformTabPane", "false");
      setProperty("waveformCommitOk", true);
      setProperty("wfpPopupMenuFlat", "false");

      setProperty("triaxialScrollZoomWithGroup", "true"); // -aww 2010/02/02
      setProperty("triaxialSelectionLocked", "false"); // -aww 2010/09/01
      setProperty("triaxialZonH", "true"); // -aww 2010/09/01

      setProperty("picker.selected.name", "PickEW");
      setProperty("picker.class.PickEW", "org.trinet.jasi.picker.PickEW");
      setProperty("picker.props.PickEW", "pickEW.props");

      setProperty("wfpanel.showTimeScale", "true");
      setProperty("wfpanel.showTimeScaleLabel", "true");
      setProperty("wfpanel.timeTicksFlag", "0");
      setProperty("wfpanel.ampScaleFactor","1.");
      setProperty("wfpanel.noiseScalar","10.");
      setProperty("wfpanel.noiseScanSecs","6.");
      setProperty("wfpanel.scaleBy","data");
      setProperty("wfpanel.maxVelGainScale", ".001");
      setProperty("wfpanel.maxAccGainScale", ".1");
      setProperty("wfpanel.maxAccUnitsScale", ".1");
      setProperty("wfpanel.maxVelUnitsScale", ".001");
      setProperty("wfpanel.maxDisUnitsScale", ".01");
      setProperty("wfpanel.maxCntUnitsScale", "2048.");
      setProperty("wfpanel.maxUnkUnitsScale", "1.");
      setProperty("wfpanel.channelLabel.font.fg", "ff000000");
      setProperty("wfpanel.channelLabel.font.size", "12"); 
      setProperty("wfpanel.channelLabel.font.style", Font.PLAIN);
      setProperty("wfpanel.channelLabel.font.name", "SansSerif"); 
      setProperty("wfpanel.channelLabel.form", "filter");

      // Origin Types - to remove an origin type, set the description to empty string in the jiggle prop file
      setProperty(PropConstant.ORIGIN_KEY_HYPOCENTER, PropConstant.ORIGIN_DESC_HYPOCENTER);
      setProperty(PropConstant.ORIGIN_KEY_CENTROID, PropConstant.ORIGIN_DESC_CENTROID);
      setProperty(PropConstant.ORIGIN_KEY_AMP, PropConstant.ORIGIN_DESC_AMP);
      setProperty(PropConstant.ORIGIN_KEY_DBLDIFF, PropConstant.ORIGIN_DESC_DBLDIFF);
      setProperty(PropConstant.ORIGIN_KEY_UNKNOWN, PropConstant.ORIGIN_DESC_UNKNOWN);
      setProperty(PropConstant.ORIGIN_KEY_NONLOCATABLE, PropConstant.ORIGIN_DESC_NONLOCATABLE);

      setProperty(PropConstant.EARTHQUAKE_REGIONAL_COMMENT, "");
      setProperty(PropConstant.SCOPE_REQUIRE_NEW_EVENT_ON_FINAL, "true");
      setProperty(PropConstant.SCOPE_USE_START_TIME, "false"); // Scope setting uses start time or end time when starting scope mode
    }

    /** Returns true if map layer same.*/
    public boolean mapInSplitEquals(JiggleProperties otherProps) {
        return ( getBoolean("mapInSplit") == otherProps.getBoolean("mapInSplit") );
    }

    /** Returns true if the current default velocity model name is the same.*/
    public boolean velocityModelEquals(JiggleProperties otherProps) { // added - aww 2008/03/25
        boolean status = getProperty("velocityModelClassName", "").equals(otherProps.getProperty("velocityModelClassName", ""));
        String name = getDefaultVelocityModelName();
        if (name == null) return status;
        return name.equals(otherProps.getDefaultVelocityModelName());
    }

    /** Override, returns true if LocationEngine properties equivalent, plus declared locationEngineType. */
    public boolean locationEngineEquals(JiggleProperties otherProps) {
        boolean status = super.locationEngineEquals(otherProps);
        status &= getProperty("locationEngineType", "").equals(otherProps.getProperty("locationEngineType") );
        status &= (getBoolean("useTrialLocation") ==  otherProps.getBoolean("useTrialLocation") );
        status &= (getBoolean("useTrialOriginTime") ==  otherProps.getBoolean("useTrialOriginTime") );
        status &= (getBoolean("fixQuarryDepth") == otherProps.getBoolean("fixQuarryDepth") );
        status &= (getBoolean("hypoinvCmdFileEditing") == otherProps.getBoolean("hypoinvCmdFileEditing") );
        return status;
    }

    /**
     * Setup class static, instance, and/or environment variables from current properties.
     * This method should be called after the pertinent properties are initially read
     * or changed through updates.
     */
    public boolean setup() {
      boolean status = super.setup();
      return (privateSetup() && status);
    }

    private boolean privateSetup() {

      boolean status = true;

      String file = getUserFileNameFromProperty("locEngineProps");
      if (file != null) {
          System.out.println("Jiggle loading locEng properties from: " + file); 
          if (! readPropertiesFile(file) ) {
              System.err.println("Jiggle ERROR: unable to load locEng properties from: " + file); 
              return false;
          }
      }


      // input property toggle debug graphics display
      // if (isSpecified("showCodaExtrapolation"))
      //   CodaFlag.showExtrapolation = getBoolean("showCodaExtrapolation");

      // setup PhaseDescriptor 1st motion resolution based on properties
      if (isSpecified("firstMoQualityMin"))
        PhaseDescription.setFmQualCut(getDouble("firstMoQualityMin"));
      if (isSpecified("firstMoOnS"))
        PhaseDescription.setFmOnS(getBoolean("firstMoOnS"));
      if (isSpecified("firstMoOnHoriz"))
        PhaseDescription.setFmOnHoriz(getBoolean("firstMoOnHoriz"));
      
      if (isSpecified("zeroBiasLineWidth")) {
        float fw = getFloat("zeroBiasLineWidth");
        if (fw >= 1) ZoomableWFPanel.biasLineWidth = fw; 
      }

      if (isSpecified("zoom.cursorShowsPhyUnits"))
          CursorLocPanel.setIncludePhyUnits(getBoolean("zoom.cursorShowsPhyUnits"));

      if (isSpecified("zoom.vert.blockScrollFactor"))
          ZoomableWFPanel.vertBlockScrollFactor = getDouble("zoom.vert.blockScrollFactor");
      if (isSpecified("zoom.horz.blockScrollFactor"))
          ZoomableWFPanel.horzBlockScrollFactor= getDouble("zoom.horz.blockScrollFactor");
      if (isSpecified("group.horz.blockScrollFactor"))
          WFGroupPanel.horzBlockScrollFactor= getDouble("group.horz.blockScrollFactor");
      if (isSpecified("group.horz.unitScrollPixels"))
          WFGroupPanel.horzUnitScrollPixels= getInt("group.horz.unitScrollPixels");

      // A scaleBy property with different state values: by wf data, by noise, by gain etc.
      int flag = 0;
      if (isSpecified("wfpanel.scaleBy")) {
          String type = getProperty("wfpanel.scaleBy", "data");
          if (type.equalsIgnoreCase("noise")) flag = 1;
          else if (type.equalsIgnoreCase("gain")) flag = 2;
          else if (type.equalsIgnoreCase("units")) flag = 3;
      }
      WFGroupPanel.wfDataBoxScaling = flag;

      // instead collapse into one property with both vel and acc values? 
      double scale = 1.;
      if (isSpecified("wfpanel.maxVelGainScale")) {
          scale = getDouble("wfpanel.maxVelGainScale", .001);
          if (scale > 0.) WFDataBox.MAX_VEL_GAIN = scale;
      }

      if (isSpecified("wfpanel.maxAccGainScale")) {
          scale = getDouble("wfpanel.maxAccGainScale", .1);
          if (scale > 0. ) WFDataBox.MAX_ACC_GAIN = scale;
      }

      if (isSpecified("wfpanel.maxAccUnitsScale")) {
          scale = getDouble("wfpanel.maxAccUnitsScale", .1);
          if (scale > 0. ) WFDataBox.MAX_ACC_UNITS = scale;
      }
      if (isSpecified("wfpanel.maxVelUnitsScale")) {
          scale = getDouble("wfpanel.maxVelUnitsScale", .001);
          if (scale > 0. ) WFDataBox.MAX_VEL_UNITS = scale;
      }
      if (isSpecified("wfpanel.maxDisUnitsScale")) {
          scale = getDouble("wfpanel.maxDisUnitsScale", .01);
          if (scale > 0. ) WFDataBox.MAX_DIS_UNITS = scale;
      }
      if (isSpecified("wfpanel.maxCntUnitsScale")) {
          scale = getDouble("wfpanel.maxCntUnitsScale", 2048.);
          if (scale > 0. ) WFDataBox.MAX_CNT_UNITS = scale;
      }
      if (isSpecified("wfpanel.maxUnkUnitsScale")) {
          scale = getDouble("wfpanel.maxUnkUnitsScale", 1.);
          if (scale > 0. ) WFDataBox.MAX_UNK_UNITS = scale;
      }

      // instead collapse into one property with both secs and scalar values?
      if (isSpecified("wfpanel.noiseScanSecs")) {
          scale = getDouble("wfpanel.noiseScanSecs", 6.);
          if (scale >= 2.) WFDataBox.NOISE_SCAN_SECS = scale; 
      }
      if (isSpecified("wfpanel.noiseScalar")) {
          scale = getDouble("wfpanel.noiseScalar", 10.0);
          if (scale < 1.) scale = 1.;
          WFDataBox.NOISE_SCALAR = scale;
      }

      WFPanel.embossedFontColor = Color.black;
      if (isSpecified("wfpanel.channelLabel.font.fg")) 
          WFPanel.embossedFontColor = getColor("wfpanel.channelLabel.font.fg");

      int cFontSize = 12;
      if (isSpecified("wfpanel.channelLabel.font.size")) { 
          cFontSize = getInt("wfpanel.channelLabel.font.size", 12); 
      }
      int cFontStyle = Font.PLAIN;
      if (isSpecified("wfpanel.channelLabel.font.style")) {
          String style = getProperty("wfpanel.channelLabel.font.style", "plain");
          if (style.equalsIgnoreCase("bold"))  cFontStyle = Font.BOLD; 
          else if (style.equalsIgnoreCase("italic"))  cFontStyle = Font.ITALIC; 
          else if (style.equalsIgnoreCase("plain")) cFontStyle = Font.PLAIN; 
      }
      String cFontName = "SansSerif";
      if (isSpecified("wfpanel.channelLabel.font.name")) {
          // Serif SansSerif Monospaced
          cFontName = getProperty("wfpanel.channelLabel.font.name", "SansSerif"); 
      }
      WFPanel.embossedFont = new Font(cFontName, cFontStyle, cFontSize);
      WFPanel.embossedLabelForm = getProperty("wfpanel.channelLabel.form", "filter");

      // bar = 0, free = 1, fix = 2
      if (isSpecified("codaFlagMode"))
          WFPanel.codaFlagMode = getInt("codaFlagMode");

      loadColors();

      PickFlag.setMarkerColorByPhase = getBoolean("pickFlag.colorByPhase", false);
      if (isSpecified("pickFlag.colorP")) PickFlag.colorP = getColor("pickFlag.colorP"); 
      if (isSpecified("pickFlag.colorS")) PickFlag.colorS = getColor("pickFlag.colorS");
      if (isSpecified("pickFlag.colorP.text")) PickFlag.colorPtext = getColor("pickFlag.colorP.text"); 
      if (isSpecified("pickFlag.colorS.text")) PickFlag.colorStext = getColor("pickFlag.colorS.text");

      // test for existance of dynamically loaded classes defined by input properties
      checkForRuntimeClassOf("WhereIsEngine");   // attempts to use default DataSource connection
      checkForRuntimeClassOf("locationEngine");

      //no longer static
      // should call model.setProperties(props)
      //if (debug) JBChannelTimeWindowModel.debug = true;

      return status;
    }

    public Color getDeltimLineColor() {
        Color c = getColor("color.pick.deltim");
        return (c != null) ? c : PickFlag.DEFAULT_DELTIM_COLOR;
    }

    public Color getSightLineColor() {
        Color c = getColor("color.pick.sightline");
        return (c != null) ? c : Color.gray;
    }

    public Color getUnusedPickColor() {
        Color c = getColor("color.pick.unused");
        return (c != null) ? c : PickFlag.DEFAULT_UNUSED_PICK_COLOR;
    }

    public Color getPPickFlagColor() {
        Color c = getColor("pickFlag.colorP");
        return (c != null) ? c : PickFlag.colorP;
    }
    public Color getSPickFlagColor() {
        Color c = getColor("pickFlag.colorS");
        return (c != null) ? c : PickFlag.colorS;
    }
    public Color getPPickFlagTextColor() {
        Color c = getColor("pickFlag.colorP.text");
        return (c != null) ? c : PickFlag.colorPtext;
    }
    public Color getSPickFlagTextColor() {
        Color c = getColor("pickFlag.colorS.text");
        return (c != null) ? c : PickFlag.colorStext;
    }

    /**
     * Determine if saving properties on exit.
     * 
     * @return true if saving properties on exit, false otherwise.
     */
    public boolean isSavePropsOnExit() {
        // save properties by default, unless overridden by user declaration
        return getBoolean("savePropsOnExit", true);
    }

    public void loadColors() {
       loadComponentColors();
       loadColorList();
       loadReadingListColors();
    }

    public void loadReadingListColors() {
        SelectableReadingList.fromProperties(this);
        if (debug) System.out.println("DEBUG JiggleProperties :\n"+SelectableReadingList.toPropertyString());
    }

    public void loadComponentColors() {
        ComponentColor.fromProperties(this);
        if (debug) System.out.println("DEBUG JiggleProperties :\n"+ComponentColor.toPropertyString());
    }

    public void loadColorList() {
        ColorList.fromProperties(this); // do the Solution list colors too - aww 2008/08/02
        if (debug) System.out.println("DEBUG JiggleProperties :\n"+ColorList.toPropertyString());
    }

    /**
     * Get the default properties header.
     * 
     * @return the default properties header.
     */
    protected String getDefaultPropertiesHeader() {
        return "--- JiggleProperties --- [" + getDefaultSavePropertiesFilename()+"]";
    }

    @Override
    protected boolean savePropertiesToFile(String header, String filename) {
        System.out.printf("Jiggle saving properties to file: %s\n", filename);
        return super.savePropertiesToFile(header, filename);
    }

    @Override
    public synchronized Object setProperty(String key, String value) {
        value = JigglePropertiesUtils.toValidValue(key, value);
        return super.setProperty(key, value);
    }

/*
// Tester class invoke main() for testing
//  static public final class Tester {
    public static void main(String args[]) {

      System.out.println("-- SYSTEM PROPERTIES --");
      System.getProperties().list(System.out);

      JiggleProperties test = new JiggleProperties("properties");
      System.out.println("-- Jiggle PROPERTIES --");
      test.dumpProperties();

// examples of local 'get' methods
//
        System.out.println("......... Test some stuff ...............");
        System.out.println("\n tracesPerPage = " + test.getInt("tracesPerPage") );
        System.out.println("catalogColumnList = " );
        String str[] = test.getStringArray("catalogColumnList");
        for (int i=0; i<str.length; i++) {
            System.out.println(i+"     "+str[i]);
        }

        try {
            System.out.println("IP address: "+
                InetAddress.getLocalHost().getHostName());
        } catch (Exception ex) {
            System.out.println("Bad IP address.");
        }

        System.out.println(test.getDbaseDescription().toString());

        System.out.println("user.home= "+System.getProperty("user.home", "."));
        System.out.println("user.timezone= "+System.getProperty("user.timezone", "."));
//
//
        ChannelTimeWindowModel model = test.getCurrentChannelTimeWindowModelInstance();
        System.out.println("model = "+ model.getModelName());
        System.out.println("model = "+ model.getExplanation());
//
        System.out.println("model = "+ test.getCurrentChannelTimeWindowModelClassName());
        JiggleProperties  shallowClone = (JiggleProperties) test.clone();
        JiggleProperties  deepClone = null;
        try {
          deepClone = (JiggleProperties) test.deepClone();
        }     catch (Exception ex) {ex.printStackTrace();}

        System.out.println("-- shallowClone Jiggle PROPERTIES --");
        shallowClone.dumpProperties();

        System.out.println("-- deepClone Jiggle PROPERTIES --");
        deepClone.dumpProperties();

        System.out.println(test.getWaveServerGroup().toString());
        test.saveProperties();
    } // end of main
//  } // end of Tester
*/
} // end of JiggleProperties

