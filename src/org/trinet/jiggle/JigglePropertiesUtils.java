package org.trinet.jiggle;

import java.util.Arrays;

import org.trinet.jasi.EventTypeMap3;
import org.trinet.jiggle.common.JiggleConstant;
import org.trinet.util.graphics.table.CatalogUtils;

public class JigglePropertiesUtils implements JiggleConstant {
    private static enum PropKey {
        /** eventTypeChoices */
        EVENT_TYPE_CHOICES("eventTypeChoices");

        /**
         * Parse the text to determine the property key.
         * 
         * @param s the text.
         * @return the property key or null if none.
         */
        public static PropKey parse(String s) {
            for (PropKey propKey : values()) {
                if (s.equals(propKey.getKey())) {
                    return propKey;
                }
            }
            return null;
        }

        private final String key;

        PropKey(String key) {
            this.key = key;
        }

        /**
         * Get the key.
         * 
         * @return the key.
         */
        public String getKey() {
            return key;
        }

        /**
         * Get the valid value text.
         * 
         * @param value the value text.
         * @return the valid value text.
         */
        public String toValid(final String value) {
            // get the values
            String s;
            final String[] values = value.split("\\s+");
            switch (this) {
            case EVENT_TYPE_CHOICES:
                for (int i = 0; i < values.length; i++) {
                    s = values[i];
                    // if not valid
                    if (!EventTypeMap3.isValidDbType(s)) {
                        System.err.printf("invalid value (%s) for property (%s)\n", s, getKey());
                        values[i] = EventTypeMap3.getDefaultDbType();
                    }
                }
                return sorted(values, value);
            }
            return value;
        }
    }

    /**
     * Initialize the properties.
     * 
     * @param props the properties.
     */
    public static void initProperties(JiggleProperties props) {
        CatalogUtils.initProperties(props);
    }

    /**
     * Get the sorted string.
     * 
     * @param values the values.
     * @param value  the original string.
     * @return the sorted string or the original string if already sorted.
     */
    private static String sorted(final String[] values, String value) {
        String s;
        // ensure no null values for sort
        for (int i = 0; i < values.length; i++) {
            s = values[i];
            if (s == null || s.isEmpty()) {
                values[i] = EMPTY_STRING;
            }
        }
        Arrays.sort(values);
        final StringBuilder sb = new StringBuilder(values.length);
        for (int i = 0; i < values.length; i++) {
            s = values[i];
            if (s != null && !s.isEmpty()) {
                s += ' ';
                // if not already appended
                if (sb.indexOf(s) == -1) {
                    sb.append(s);
                }
            }
        }
        String svalue = EMPTY_STRING;
        final int index = sb.length() - 1;
        if (index > 0) {
            sb.deleteCharAt(index);
            svalue = sb.toString();
        }
        if (!value.equals(svalue)) {
            return svalue;
        }
        return value;
    }

    /**
     * Get a valid version of the value.
     * 
     * @param key   the key.
     * @param value the value.
     * @return the valid value.
     */
    public static String toValidValue(String key, String value) {
        PropKey propKey = PropKey.parse(key);
        if (propKey != null) {
            value = propKey.toValid(value);
        }
        return value;
    }

    /**
     * Validate the properties.
     * 
     * @param props the properties.
     */
    public static void validate(JiggleProperties props) {
        String key;
        String value;
        for (PropKey propKey : PropKey.values()) {
            key = propKey.getKey();
            value = props.getProperty(key);
            if (value != null) { // if value exists
                String validvalue = propKey.toValid(value);
                // if valid value is not same as the value
                if (validvalue != null && !validvalue.equals(value)) {
                    // set the valid value
                    props.setProperty(key, validvalue);
                }
            }
        }
    }
}
