package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.*;
import javax.swing.event.*;
import javax.swing.border.*;
import org.trinet.jasi.AbstractWaveform;
import org.trinet.jasi.Waveform;
import org.trinet.jasi.DataSource;
import org.trinet.jasi.Solution;
import org.trinet.jasi.TravelTime;
import org.trinet.util.graphics.IconImage;
import org.trinet.util.locationengines.LocationServiceDescIF;

/**
 * The JiggleStatusBar will be visible at the bottom of the frame at all times.
 * This is a passive object that shows information but has no buttons etc.
 * It contains the following components: <p>
 *  o A text label that is describes the current action thats happening. <p>
 *  o A progress bar that shows that progress of the current action.<p>
 *  o A label that shows how many traces there are in the current MasterView<p>
 */
public class JiggleStatusBar extends JPanel {

    private StatusPanel statusPanel = new StatusPanel();

    private JLabel staCountLabel = new JLabel();
    private JLabel viewCountLabel = new JLabel();
    private JLabel phaseCountLabel = new JLabel();
    private JLabel ampCountLabel = new JLabel();
    private JLabel codaCountLabel = new JLabel();
    private JLabel solCountLabel = new JLabel();

    private JLabel dataSourceLabel = new JLabel();
    private JLabel wfSourceLabel = new JLabel();
    private JLabel mvModelLabel = new JLabel();
    private JLabel locServiceLabel = new JLabel();
    private JLabel velModelLabel = new JLabel();

    private JDialog findDialog = null;
    private JButton findButton = null;

    private ChannelFinderTextField channelFinder;

    protected Jiggle jiggle;

    public JiggleStatusBar(Jiggle jiggle) {

      this.jiggle = jiggle;

      setLayout( new BoxLayout(this, BoxLayout.X_AXIS) );

      channelFinder = new ChannelFinderTextField();
      channelFinder.setToolTipText("Enter a channel description of the form: NET.STA.SEEDCHAN.LOCATION");

      Image image = IconImage.getImage("mini-view.gif");
      if (image == null) {
          findButton = new JButton("F");
      } else {
          findButton = new JButton(new ImageIcon(image));
      }
      findButton.setMaximumSize(new Dimension(24,24));
      findButton.setPreferredSize(new Dimension(24,24));
      findButton.setToolTipText("Scroll to the specified channel's data in the loaded event ..."); 
      findButton.addActionListener(
        new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if (findDialog != null) {
                    findDialog.dispose();
                    findDialog = null;
                }
                findDialog = new JDialog((Frame)Jiggle.jiggle, false);
                findDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                Box hbox = Box.createHorizontalBox();
                findDialog.getContentPane().setLayout(new BorderLayout());
                channelFinder.setMaximumSize(new Dimension(150,20));
                channelFinder.setPreferredSize(new Dimension(150,20));
                hbox.add(channelFinder);
                findDialog.getContentPane().add(hbox, BorderLayout.NORTH);

                /*
                JButton closeButton = new JButton("CLOSE");
                closeButton.setMargin(new Insets(0,0,0,0));
                closeButton.setMaximumSize(new Dimension(48,24));
                closeButton.setPreferredSize(new Dimension(48,24));
                closeButton.addActionListener(
                    new ActionListener() {
                       public void actionPerformed(ActionEvent evt2) {
                           findDialog.dispose();
                       }
                });
                */

                hbox = Box.createHorizontalBox();
                JButton resetButton = new JButton("Reset");
                resetButton.setToolTipText("Clear channel description (NET.STA.SEEDCHAN)");
                resetButton.setMargin(new Insets(0,0,0,0));
                resetButton.setMaximumSize(new Dimension(48,20));
                resetButton.setPreferredSize(new Dimension(48,20));
                resetButton.addActionListener(
                    new ActionListener() {
                       public void actionPerformed(ActionEvent evt2) {
                           channelFinder.setText("");
                           channelFinder.requestFocus();
                       }
                });

                hbox = Box.createHorizontalBox();
                hbox.add(Box.createHorizontalGlue());
                hbox.add(resetButton);
                hbox.add(Box.createHorizontalGlue());
                findDialog.getContentPane().add(hbox, BorderLayout.SOUTH);

                findDialog.setTitle("Find Channel");
                findDialog.pack();
                findDialog.setVisible(true);
                findDialog.setLocationRelativeTo(findButton);
            }
      });
      add(findButton);

      // Text label
      statusPanel.setText("Application working status text shows here.");
      statusPanel.setToolTipText("Application working status text shows here.");
      statusPanel.setBorder(new EtchedBorder());
      add(statusPanel);

      add(Box.createGlue());

      mvModelLabel.setBorder(new EtchedBorder());
      add(mvModelLabel);

      wfSourceLabel.setBorder(new EtchedBorder());
      add(wfSourceLabel);

      dataSourceLabel.setBorder(new EtchedBorder());
      add(dataSourceLabel);

      locServiceLabel.setBorder(new EtchedBorder());
      add(locServiceLabel);

      velModelLabel.setBorder(new EtchedBorder());
      add(velModelLabel);
      //add(Box.createGlue());

      // solCount label
      solCountLabel.setBorder(new EtchedBorder());
      solCountLabel.setToolTipText("# of events currently in view (ids in toolbar combobox)");
      setSolutionCount(0);
      add(solCountLabel);

      // staCount label
      staCountLabel.setBorder(new EtchedBorder());
      staCountLabel.setToolTipText("# of sta viewable in group panel");
      setStaCount(0);
      add(staCountLabel);

      // WFViewCount label
      viewCountLabel.setBorder(new EtchedBorder());
      viewCountLabel.setToolTipText("# of channels viewable in group panel");
      setWFViewCount(0);
      add(viewCountLabel);

      // phaseCount label
      phaseCountLabel.setBorder(new EtchedBorder());
      phaseCountLabel.setToolTipText("# associated with the view's selected event");
      setPhaseCount(0);
      add(phaseCountLabel);

      // ampCount label
      ampCountLabel.setBorder(new EtchedBorder());
      ampCountLabel.setToolTipText("# associated with the view's selected event");
      setAmpCount(0);
      add(ampCountLabel);

      // codaCount label
      codaCountLabel.setBorder(new EtchedBorder());
      codaCountLabel.setToolTipText("# associated with the view's selected event");
      setCodaCount(0);
      add(codaCountLabel);

    } // end of createPanel

    protected void setEventEnabled(boolean tf) {
        if (findDialog != null) findDialog.dispose();
        findButton.setEnabled(tf);
    }

    public StatusPanel getStatusPanel(){
      return statusPanel;
    }

    private void resetFinderPanel() {
      if (findDialog != null) findDialog.dispose();
      channelFinder = new ChannelFinderTextField(jiggle.mv);
      channelFinder.setToolTipText("Enter a channel description of the form: NET.STA.SEEDCHAN.LOCATION");
    }

    /** Sets/resets all info panels with data from this MasterView. */
    public void setMasterView() { //public void setMasterView(MasterView masterView) {
      //mv = masterView;
      setCounts();
      resetFinderPanel();
      updateDataSourceLabels();
    }

    public void setCounts() {
      if (jiggle.mv == null) {
        setWFViewCount(0);
        setSolutionCount(0);
      }
      else {
        setStaCount(jiggle.mv.wfvList.getStationCount());
        setWFViewCount(jiggle.mv.wfvList.size());
        setSolutionCount(jiggle.mv.solList.size());
      }
      Solution sol = (jiggle.mv == null) ? null : jiggle.mv.getSelectedSolution();
      if (sol == null) {
        setPhaseCount(0);
        setAmpCount(0);
        setCodaCount(0);
      }
      else {
        // Should these be counts of list readings having inWgt > 0 ?
        setPhaseCount(sol.getPhaseList().size());
        setAmpCount(sol.getAmpList().size());
        setCodaCount(sol.getCodaList().size());
      }
    }

    public void updateVelModelLabel() {
      TravelTime tt = TravelTime.getInstance();
      setVelModelText(tt.getModel().getName(), "Velocity model used for traveltime estimates");
    }

    public void updateLocServiceLabel() {
      LocationServiceDescIF lsd = jiggle.getProperties().getLocationServerGroup().getSelectedService();
      if (lsd == null) setLocServiceText("No Loc Service", "");
      else setLocServiceText(lsd.getServiceName(), lsd.toString());
    }

    public void updateDataSourceLabels() {

      if (jiggle.mv != null) {
        setMvModelText(jiggle.mv.getChannelTimeWindowModel().getModelName(), "Channel time window model displaying "+
                    jiggle.mv.getChannelTimeWindowModel().getExplanation());

        String toolTip = "Source of waveform timeseries";
        String str = "";
        int type = AbstractWaveform.getWaveDataSourceType(); 
        if (type == AbstractWaveform.LoadFromDataSource) {
            str = AbstractWaveform.getWaveDataSourceName();
            if (str.equals("DS")) toolTip = DataSource.getDbaseConnectionDescription().getURL();
            setWfSourceText(str, toolTip);
        } else if (type == AbstractWaveform.LoadFromWaveServer) {
            setWfSourceText(((WaveServerGroup)AbstractWaveform.getWaveDataSource()).getName(), toolTip);
        } else {
            setWfSourceText("unknown", toolTip);
        }

        str = DataSource.getDbaseName() + "@" + DataSource.getHostName();
        if (str.equals("@")) str = "DS";
        setDataSourceText(str, DataSource.getDbaseConnectionDescription().getURL());
      }
      if (DataSource.getConnection() == null) {
        getDataSourceLabel().setOpaque(true);
        getDataSourceLabel().setBackground(Color.yellow);
      } else {
        getDataSourceLabel().setBackground(Jiggle.jiggle.getBackground());
      }
    }

/**
 * Set the value in the sta count label
 */
    public void setStaCount(int count)
    {
      staCountLabel.setText(count + " Sta");
    }

  /**
 * Set the value in the WFVCount label
 */
    public void setWFViewCount(int count)
    {
      viewCountLabel.setText(count + " Channel");
    }

/**
 * Set the value in the phase count label
 */
    public void setPhaseCount(int count)
    {
      phaseCountLabel.setText(count + " Arrival");
    }

 /**
 * Set the value in the amp count label
 */
    public void setAmpCount(int count)
    {
      ampCountLabel.setText(count + " Amp");
    }

 /**
 * Set the value in the doda count label
 */
    public void setCodaCount(int count)
    {
      codaCountLabel.setText(count + " Coda");
    }
/**
 * Set the value in the solution count label
 */
    public void setSolutionCount(int count) {
      solCountLabel.setText(count + " Event");
    }

    public void setWfSourceText(String label, String tooltip) {
      wfSourceLabel.setText(label);
      wfSourceLabel.setToolTipText(tooltip);
    }
    public void setMvModelText(String label, String tooltip) {
      mvModelLabel.setText(label);
      mvModelLabel.setToolTipText(tooltip);
    }
    public void setVelModelText(String label, String tooltip) {
      velModelLabel.setText(label);
      velModelLabel.setToolTipText(tooltip);
    }

    public void setDataSourceText(String label, String tooltip) {
      dataSourceLabel.setText(label);
      dataSourceLabel.setToolTipText(tooltip);
    }

    public void setLocServiceText(String label, String tooltip) {
      locServiceLabel.setText(label);
      locServiceLabel.setToolTipText(tooltip);
    }

    public JLabel getWfSourceLabel() {
      return wfSourceLabel;
    }
    public JLabel getMvModelLabel() {
      return mvModelLabel;
    }
    public JLabel getVelModelLabel() {
      return velModelLabel;
    }
    public JLabel getDataSourceLabel() {
      return dataSourceLabel;
    }
    public JLabel getLocServiceLabel() {
        return locServiceLabel;
    }
 } // end of class


