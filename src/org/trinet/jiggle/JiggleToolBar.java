//
// Can you make the buttons smaller? Does the icon image scale? - aww
//
package org.trinet.jiggle;
//
//A mnemonic is the key which when combined with the mouseless modifier
//(usually Alt) activates button if focus within button's ancestor window. 
//A mnemonic should be specified using one of the VK_XXX keycodes defined
//in java.awt.event.KeyEvent. Mnemonics are case-insensitive.
//NOTE: disabled mnemonics, reserve for other component, like panel hot-key input map

/**
 * This is the Main tool bar that will be visible at all times.<p>
 *
 * Images for button icons are loaded using the IconImage class to insure
 * transportablility. They must reside in the "/images" directory of the
 * CLASSPATH or the .jar file. (Ex: <classpath>/images/zagscroll.gif)

 *@see: IconImage */
// see:  http://java.sun.com/docs/books/tutorial/ui/swing/toolbar.html

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.*;
import javax.swing.text.JTextComponent;

import org.trinet.jasi.*;
import org.trinet.jasi.picker.PhasePickerIF;
import org.trinet.util.gazetteer.*;
import org.trinet.util.graphics.*;

public class JiggleToolBar extends JToolBar {

    static public boolean labelButtons = false; // default used to be true - aww 2008/06/06
    static public boolean miniButtons = false;
    private static int miniButtonWidth = 24;

    private String pre = "";


    // Panel containing event specific components; eventTypeChooser and solutionListComboBox
    protected SolutionPanel solPanel;


    // buttons that can be disabled
    protected JButton cacheWfButton;
    private JButton deleteButton;
    private JButton finalButton;
    private JButton saveButton;
    private JButton saveAllButton;
    private JButton nextButton;
    private JButton locateButton;
    private JButton magCalcButton;
    private JButton magPrefButton;
    private JPopupMenu magCalcMenu;
    //private JPopupMenu magPrefMenu;
    private JButton newSolButton;
    private JButton whereButton;

    private JButton mapButton;
    private JButton swarmButton;

    private JButton unpickButton;
    private JPopupMenu unpickMenu;
    private UnpickButtonHandler unpickButtonAction = null;

    private JButton autoPickButton;
    private JPopupMenu autoPickMenu;
    private AutoPickButtonHandler autoPickButtonAction = null;

    private JButton rollbackButton;
    private JButton triaxialButton;
    private JToggleButton fullTimeButton;

    private JToggleButton jtP;
    private JToggleButton jtA;
    private JToggleButton jtC;
    private JToggleButton jtS;

    private JButton scopeMinusButton;
    private JButton scopeHalfMinusButton;
    protected JButton scopeModeButton;
    private JButton scopeHalfPlusButton;
    private JButton scopePlusButton;

    // one handler for all buttons
    private ButtonHandler buttonHandler = new ButtonHandler();

// event handling stuff
    private String EventName;       // String label of the event (e.g. "Exit")
    private String ItemName;

    //private MasterView mv;
    private Jiggle jiggle;       // the main frame buttons act on

/**
 * Event Handling: Local events come from the Buttons and the ID list. Button
 * events are are handled by HandleEvent and the list is handled HandleList. If
 * a menu is ever added to this panel we would handle it in HandleEvent
 * also. <p> The Menu is a component of jiggle and not this panel. However,
 * we want to change a label on the panel in response to a menu selection in
 * jiggle. Therefore, the label object is public static allowing MinFrame to
 * change it.  */
    public JiggleToolBar(Jiggle jiggle) {
      setFrame(jiggle);
      makeToolBar();
      //setMasterView();
    }

/**
 * Create the button panel
 */
    private void makeToolBar() {
        labelButtons = jiggle.getProperties().getBoolean("labelToolBarButtons");
        miniButtons = jiggle.getProperties().getBoolean("miniToolBarButtons");
        miniButtonWidth = jiggle.getProperties().getInt("miniButtonWidth");
        if (miniButtonWidth <= 0) miniButtonWidth = 24;
        else if (miniButtonWidth > 48) miniButtonWidth = 48;

        setFloatable(true);   // allow user to move toolbar

        setMargin(new Insets(0,0,0,0));
        pre = (miniButtons) ? "mini-" : "";

        //mapButton = makeButton("Map",  pre+"QWClient_icon.gif", // Map icon
        //mapButton = makeButton("Map",  pre+"map-icon.gif", // Map icon
        mapButton = makeButton("Map",  pre+"greenGlobe.gif", // Map icon
             "Open Map", KeyEvent.VK_O);
        add(mapButton);

        //add(makeButton("Cat", pre+"table_selection.gif",
        //     "Show catalog panel", KeyEvent.VK_C));
        addSeparator();
        locateButton = makeButton("Loc", pre+"bullseye.gif",
             "Calculate location [and optionally summary magnitude]", KeyEvent.VK_L);
        add(locateButton);
        //addSeparator();

        // TODO: magnitude calc functions into an Action which can be
        // shared with ToolBar, action can be customized to disable mags or 
        // have property to set "waveform" versus "recalc" mode for action
        // or show a popup to chose which operation? 
        // begin test popup magmenu
        magCalcMenu = new JPopupMenu();
        ActionListener al = new MagMenuCalcActionListener();
        String [] magTypes = jiggle.getProperties().getMagMethodTypes();
        int mcnt = 0;
        boolean tf = false;
        for (int i = 0; i<magTypes.length; i++) {
            tf = !jiggle.getProperties().getBoolean(magTypes[i].toLowerCase()+"Disable");
            addToPopupMenu(magTypes[i], magCalcMenu, al, tf);
            // temporary user property to control whether magmethod item is enabled
            // by default since menu shows all magmethods listed in properties file 
            if (tf) mcnt++;
        }
        if (mcnt > 1) {
            addToPopupMenu("All", magCalcMenu, al, true);
        }
        String tip = "Calculate new summary magnitude of the selected type from waveforms";
        //magCalcButton = makeButton("CalcM", pre+"ML_32.gif", tip, KeyEvent.VK_M);
        magCalcButton = makeButton("CalcM", pre+"m.gif", tip, KeyEvent.VK_M);
        add(magCalcButton);
        //
        //magPrefMenu = new JPopupMenu();
        //al = new MagMenuPrefActionListener();
        //for (int i = 0; i<magTypes.length; i++) addToPopupMenu(magTypes[i], magPrefMenu, al, true);
        tip = "Set the event preferred magnitude to the selected type";
        //magPrefButton = makeButton("PrefM", pre+"MC_32.gif", tip, KeyEvent.VK_P);
        magPrefButton = makeButton("PrefM", pre+"pm-script.gif", tip, KeyEvent.VK_P);
        //System.out.println("DEBUG JiggleToolBar >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>adding magPrefButton");
        add(magPrefButton);
        // // end of test magmenu -aww

        addSeparator();
        swarmButton = makeButton("Swarm",pre+"swarm.gif","Show the selected waveform in a Swarm frame", KeyEvent.VK_Z);
        add(swarmButton);

        addSeparator();
        deleteButton = makeButton("Del", pre+"dynamite.gif",
            "Delete currently selected waveform panel event in database (id shown in toolbar/titlebar)", KeyEvent.VK_D);
        add(deleteButton);
        addSeparator();

        saveButton = makeButton("Save",  pre+"save.gif",
             "Save the selected event to the database.", KeyEvent.VK_S);
        add(saveButton);
        //addSeparator();

        saveAllButton = makeButton("SaveA", pre+"saveall.gif",
             "Save all events in this toolbar's dropdown id list to the database.", KeyEvent.VK_A);
        add(saveAllButton);
        //addSeparator();

        finalButton = makeButton("Final",  pre+"gavel.gif",
             "Save the selected event, setting it's processing state final \"F\" in the database.", KeyEvent.VK_F);
        add(finalButton);
        //addSeparator();

        unpickButton = makeUnpickBtn();
        add(unpickButton);
        //addSeparator();

        nextButton = makeButton("Next", pre+"forward.gif",
             "Load the next event or scoped view into the waveform viewer.", KeyEvent.VK_N);
        add(nextButton);
        //addSeparator();

        // add (empty) solPanel - can't create until there's a masterView
        solPanel = new SolutionPanel(jiggle);
        if (!miniButtons) solPanel.setBorder( new TitledBorder("Selected Solution") );
        add(solPanel);
        addPropertyChangeListener(solPanel);
        this.jiggle.getSolutionChangedListener().addSolutionChangedListener(solPanel);
        addSeparator();

        whereButton = makeButton("Where", pre+"question_blue.gif",
             "Gazetteer info about selected event.", KeyEvent.VK_W);
        add(whereButton);

        //addSeparator();
        newSolButton = makeButton("Event", pre+"new-blue.gif",
             "Add new event to the event list making it the current selection", KeyEvent.VK_B);
        add(newSolButton);
        //addSeparator();

        if (jiggle.getProperties().getBoolean("solPreferredRollbackEnabled")) {
          rollbackButton = makeButton("Rollb", pre+"mop.gif",
               "Reset event preferred to prior prefor and prefmag", KeyEvent.VK_E);
          add(rollbackButton);
        //  addSeparator();
        }

        fullTimeButton = makeToggleButton("FullT",
            pre+"window-time.gif",
            pre+"full-time.gif",
            "Toggle waveform group panel timespan, full vs. scrolled");
        
        boolean selected =  (jiggle.wfScroller == null) ? false : jiggle.wfScroller.getShowFullTime();
        fullTimeButton.setSelected(selected);
        fullTimeButton.addItemListener(
          new ItemListener () {
            public void itemStateChanged(ItemEvent e) {
              WFScroller wfs = jiggle.wfScroller;
              if (wfs == null) return;
              boolean isSelected = ( e.getStateChange() == ItemEvent.SELECTED); 
              double secs = Math.abs(jiggle.getProperties().getDouble("secsPerPage"));
              jiggle.getProperties().setProperty("secsPerPage", ((isSelected) ? -secs : secs));
              wfs.setShowFullTime(isSelected);
              wfs.setPanelsInView(wfs.viewsPerPage);
              wfs.repaint();
            }
          }
        );
        add(fullTimeButton);
        addSeparator();

        triaxialButton = makeButton("T", pre+"triaxialgray.png", "Toggle triaxial view in lower waveform group panel", KeyEvent.VK_V);
        triaxialButton.addActionListener(
          new ActionListener() {
            public void actionPerformed(ActionEvent e) {
              final WFScroller wfs = jiggle.wfScroller;
              if (wfs == null) return;
              fullTimeButton.setSelected(false);
              //wfs.jbTriaxial.setSelected(! wfs.jbTriaxial.isSelected());
              wfs.jbTriaxial.doClick();
            }
          }
        );
        triaxialButton.setEnabled(false);
        add(triaxialButton);
        addSeparator();

        autoPickButton = makeAutoPickBtn();
        add(autoPickButton);
        addSeparator();

        add(makeButton("Redo", pre+"paint_color.gif",
             "Redraw the screen", KeyEvent.VK_R));
        addSeparator();

        // scope test - aww 2008/05/12
        ActionListener scopeListener = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {

                String cmd = evt.getActionCommand();
                if (cmd.equals("Scope")) {
                    jiggle.scopeLoadMasterView(true);  // show dialog
                    if (!jiggle.scopeMode) {
                        setEventEnabled(false); // at start up there's no selected solution so...
                        scopeMinusButton.setEnabled(false);
                        scopeHalfMinusButton.setEnabled(false);
                        scopeHalfPlusButton.setEnabled(false);
                        scopePlusButton.setEnabled(false);
                        return;
                    }
                    scopeMinusButton.setEnabled(true);
                    scopeHalfMinusButton.setEnabled(true);
                    scopeHalfPlusButton.setEnabled(true);
                    scopePlusButton.setEnabled(true);
                }
/*
DISABLE waveform caching in scopeMode ?
Need to preserve new solutions, picks so don't create new masterview when left/right arrow button clicked
instead slide timespan window, just update MasterView timespan, update all WFView's timespan, update all wf's timespan
then unload/reload all wf's in MasterView WFViewList, in current view 
need new MasterView method like setAllViewSpans(TimeSpan ts, boolean doWfs), with extra code to do wf's 
*/                
                else if (cmd.equals("ScopeMinus")) {
                    if (jiggle.scopeConfig != null) jiggle.scopeLoadMasterView(false, -1); // advance 1/2 window width
                }
                else if (cmd.equals("ScopeHalfMinus")) {
                    if (jiggle.scopeConfig != null) jiggle.scopeLoadMasterView(false, -2); // retreat 1/2 window width
                }
                else if (cmd.equals("ScopeHalfPlus")) {
                    if (jiggle.scopeConfig != null) jiggle.scopeLoadMasterView(false, +2); // advance 1 window width
                }
                else if (cmd.equals("ScopePlus")) {
                    if (jiggle.scopeConfig != null) jiggle.scopeLoadMasterView(false, +1); // retreat 1 window width
                }
            }
        };
        scopeMinusButton = makeButton("ScopeMinus", "arrow-pageleft-grn.gif", "Move scope view backwards 1 width (earlier).");
        scopeMinusButton.setEnabled(jiggle.scopeMode);
        scopeMinusButton.addActionListener(scopeListener);
        add(scopeMinusButton);

        scopeHalfMinusButton = makeButton("ScopeHalfMinus", "arrow-left-grn.gif", "Move scope view backwards 1/2 width (earlier).");
        scopeHalfMinusButton.setEnabled(jiggle.scopeMode);
        scopeHalfMinusButton.addActionListener(scopeListener);
        add(scopeHalfMinusButton);

        scopeModeButton = makeButton("Scope", pre+"oscilloscope.gif", "View RT waveform snapshot", KeyEvent.VK_C);
        scopeModeButton.addActionListener(scopeListener);
        add(scopeModeButton); //scope test - aww 2008/05/12

        scopeHalfPlusButton = makeButton("ScopeHalfPlus", "arrow-right-grn.gif", "Move scope view forward 1/2 width (later).");
        scopeHalfPlusButton.setEnabled(jiggle.scopeMode);
        scopeHalfPlusButton.addActionListener(scopeListener);
        add(scopeHalfPlusButton);

        scopePlusButton = makeButton("ScopePlus", "arrow-pageright-grn.gif", "Move scope view forward 1 width (later).");
        scopePlusButton.setEnabled(jiggle.scopeMode);
        scopePlusButton.addActionListener(scopeListener);
        add(scopePlusButton);

        addSeparator();

        cacheWfButton = makeButton("CacheWf", pre+"cache2disk.gif",
            "Select waveform cache action...", KeyEvent.VK_K);

        cacheWfButton.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent evt) {

              // Should we force static AbstractWaveform.cache2File=true here ?
              if (!AbstractWaveform.cache2File || jiggle == null || jiggle.mv == null) return; // no-op

              //Problem when main thread "unloads" or "loads" WFV, e.g. WFViewList WFCacheManager and WFScroller actions in Jiggle
              Object[] options = new String [] {"Cache loaded wf", "Cache all wf", "Purge...", "Cancel"};

              int iaction = JOptionPane.showOptionDialog(JiggleToolBar.this.jiggle,
                             "Choose waveform cache action:",
                             "Waveform Disk Cache",
                             JOptionPane.DEFAULT_OPTION,
                             JOptionPane.PLAIN_MESSAGE,
                             null,
                             options,
                             options[0]
                         ); 

              if (iaction == 0 || iaction == 1) {
                  writeWfCache(iaction);
              }
              else if (iaction == 2) { 

                  options = new String [] {"Loaded Event", "All Events", "Cancel"};

                  iaction = JOptionPane.showOptionDialog(JiggleToolBar.this.jiggle,
                             "Delete timeseries cache for which event?",
                             "Waveform Disk Cache",
                             JOptionPane.DEFAULT_OPTION,
                             JOptionPane.PLAIN_MESSAGE,
                             null,
                             options,
                             options[0]
                         ); 
                 if (iaction == 0) { // loaded event
                     Solution selectedSol = jiggle.mv.getSelectedSolution();
                     if (selectedSol != null) 
                         AbstractWaveform.clearCacheDir(selectedSol.getId().longValue(), null, null);
                 }
                 else if (iaction == 1) { // all events
                     AbstractWaveform.clearCacheDir(0l, null, null);
                 }
              }
          }
        });

        add(cacheWfButton);
        addSeparator();

        //if ( jiggle.getProperties().getBoolean("addHideToolbar") ) {
          al =  new ActionListener() {
            public void actionPerformed(ActionEvent e) {
              final WFScroller wfs = jiggle.wfScroller;
              if (wfs == null) return;
              if (wfs.hideButtonPanel == null) {
                  wfs.hideButtonPanel = wfs.new HideButtonPanel();
              }

              final AbstractButton jtb = (AbstractButton) e.getSource();
              final String cmd = e.getActionCommand();
              //System.out.println( cmd + " isSelected? : " + jtb.isSelected());
              jtS.setSelected(false);
              switch (cmd) {
              case "S":
                  jtS.setSelected(true);
                  jtP.setSelected(false);
                  jtA.setSelected(false);
                  jtC.setSelected(false);
                  wfs.unsetButtons();
                  break;
              case "P":
                  jtA.setSelected(false);
                  jtC.setSelected(false);
                  if (jtb.isSelected()) {
                    wfs.jbNP.doClick();
                    wfs.bgAmp.clearSelection();
                    wfs.bgCoda.clearSelection();
                  }
                  else {
                    jtS.setSelected(true);
                    wfs.bgPick.clearSelection();
                    wfs.bgAmp.clearSelection();
                    wfs.bgCoda.clearSelection();
                  }
                  break;
              case "A":
                  jtP.setSelected(false);
                  jtC.setSelected(false);
                  if (jtb.isSelected()) {
                    wfs.jbNA.doClick();
                    wfs.bgPick.clearSelection();
                    wfs.bgCoda.clearSelection();
                  }
                  else {
                    jtS.setSelected(true);
                    wfs.bgPick.clearSelection();
                    wfs.bgAmp.clearSelection();
                    wfs.bgCoda.clearSelection();
                  }
                  break;
              case "C":
                  jtP.setSelected(false);
                  jtA.setSelected(false);
                  if (jtb.isSelected()) {
                    wfs.jbNC.doClick();
                    wfs.bgPick.clearSelection();
                    wfs.bgAmp.clearSelection();
                  }
                  else {
                    jtS.setSelected(true);
                    wfs.bgPick.clearSelection();
                    wfs.bgAmp.clearSelection();
                    wfs.bgCoda.clearSelection();
                  }
                  break;
              }
              wfs.doActionCmd(WFScroller.HIDE_CMD, false);
            }
         };

         jtP = makeToggleButton("P", null, null, "Show only panels w/pick (resets hides)");
         jtP.addActionListener(al);
         add(jtP);

         jtA = makeToggleButton("A", null, null, "Show only panels w/amp (resets hides)");
         jtA.addActionListener(al);
         add(jtA);

         jtC = makeToggleButton("C", null, null, "Show only panels w/coda (resets hides)");
         jtC.addActionListener(al);
         add(jtC);

         jtS = makeToggleButton("S", null, null, "Show all waveform panels (resets hides)");
         jtS.addActionListener(al);
         add(jtS);

         addSeparator();
      //}

    } // end of makeToolBar

    protected void writeWfCache(final int iaction) {
              // re using waveserver source, if the waveform.savingEvent is null, write method sets its value to mv selected evid
              //jiggle.initStatusGraphicsForThread("Cache Waveforms to Disk", "Writing timeseries to disk cache...\nDon't scroll group panel!"); // pops status
              System.out.println("Jiggle INFO: Starting thread to write view timeseries to disk cache files..."); // pops status

              org.trinet.util.SwingWorker sw = new org.trinet.util.SwingWorker() {
                  int count = 0;
                  public Object construct() { // any GUI graphics in method should invokeLater
                    try {

                      // Thread name set to prevent WFScroller Vert Scrollbar listener from setting WFViewList CacheMgr index
                      Thread t = Thread.currentThread();
                      t.setName("WFCacheSwingWorkerWFLoad");

                      WFView wfv = null;
                      int size = jiggle.mv.wfvList.size(); 
                      boolean hasTimeSeries = false;

                      for ( int ii=0; ii<size; ii++) {
                        wfv = (WFView) jiggle.mv.wfvList.get(ii);

                        if (!wfv.hasWaveform()) continue; // skip, no data

                        hasTimeSeries = wfv.hasTimeSeries();

                        // skip, if user only wants to cache those currently loaded
                        if (iaction == 0 && !hasTimeSeries) continue;

                        if (!hasTimeSeries && !wfv.getWaveform().hasCacheFile()) { // load && unload, skip if existing disk cache
                           //if (wfv.reloadTimeSeries(false) && wfv.unloadTimeSeries()) count++;
                           //Replace above with specific "copy" to reduce interference between WFView load/unload via user scrolling 
                           AbstractWaveform wf = (AbstractWaveform) wfv.getWaveform().copy();
                           if (wf.loadTimeSeries(false)) { // if loaded set evid 
                               if (wf.getSavingEvent() == null && jiggle.mv != null) {
                                 Solution sol =jiggle.mv.getSelectedSolution();
                                 if (sol != null) {
                                     if (sol.isClone()) { // need parent id for wf cache filename
                                         wf.setSavingEvent(String.valueOf(sol.getParentId().longValue()));
                                     }
                                     else { // use evid
                                         wf.setSavingEvent(String.valueOf(sol.getId().longValue()));
                                     }
                                 }
                              }
                              // Now loaded and evid set so write to disk
                              if (wf.unloadTimeSeriesToCache()) count++;
                           }
                        }
                        else { // timeseries already loaded so write to disk
                            if (wfv.writeWfToCache()) count++;
                        }
                      }
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    return null;
                  }

                  public void finished() { // GUI update here
                      //jiggle.resetStatusGraphicsForThread(); // unpops status
                      System.out.println("Jiggle INFO: total waveform cache files written to disk: " + count);
                  }
              }; // end of SwingWorker
              sw.start(); // start long task worker thread
    }

    protected void updateFullTimeButton() {
        fullTimeButton.setSelected((jiggle.wfScroller == null) ? false : jiggle.wfScroller.getShowFullTime());
    }

    private JButton makeUnpickBtn() {
        if (unpickButtonAction == null ) unpickButtonAction = new UnpickButtonHandler(); 
        JButton btn = new JButton();
        if (unpickButtonAction.getValue(Action.SMALL_ICON) != null)
            btn.putClientProperty("hideActionText", Boolean.TRUE);
        btn.setAction(unpickButtonAction);
        setButtonSize(btn, miniButtons);
        btn.setToolTipText("Set unpick mode, when type A(mp), C(oda), P(hasetime) is selected, delete that type nearest the mouse click on waveform panel");
        //btn.setMnemonic(KeyEvent.VK_U); // save hot-keys for panel actions
        return btn;
    }

    private JButton makeAutoPickBtn() {
        if (autoPickButtonAction == null ) autoPickButtonAction = new AutoPickButtonHandler(); 
        Image img = IconImage.getImage(pre+"autopick.png");
        ImageIcon icon =  null;
        if (img != null) icon = new ImageIcon(img);
        JButton btn = (icon == null) ? new JButton("Pick") : new JButton(icon);
        btn.setEnabled(false);
        btn.addActionListener(autoPickButtonAction);
        setButtonSize(btn, miniButtons);
        btn.setToolTipText("Autopick event waveforms: P-only, S-only, P&S, or channel default");
        return btn;
    }

    private void setButtonSize(AbstractButton btn, boolean miniButtons) {
        int labelHeight = (labelButtons && !miniButtons) ? 16 : 0;
        Dimension d = (miniButtons) ? new Dimension(miniButtonWidth,20) : new Dimension(32+labelHeight,32+labelHeight);

        btn.setPreferredSize(d);
        btn.setMaximumSize(d);
        btn.setMinimumSize(new Dimension(16,16));
        btn.setMargin(new Insets(0,0,0,0));
        btn.setIconTextGap((labelButtons && !miniButtons ? 1 : 0));
        btn.setAlignmentY(CENTER_ALIGNMENT);
        btn.setHorizontalTextPosition(AbstractButton.CENTER);
        btn.setVerticalTextPosition(AbstractButton.BOTTOM);
    }

    private void createUnpickMenu() {
        unpickMenu = new JPopupMenu();
        ActionListener al = new UnpickMenuItemHandler();

        JMenuItem jmi  = new JMenuItem("Off");
        jmi.addActionListener(al);
        unpickMenu.add(jmi);

        jmi  = new JMenuItem(UnpickMode.PHASE);
        jmi.addActionListener(al);
        unpickMenu.add(jmi);
        
        jmi = new JMenuItem(UnpickMode.AMP);
        jmi.addActionListener(al);
        unpickMenu.add(jmi);

        jmi = new JMenuItem(UnpickMode.CODA);
        jmi.addActionListener(al);
        unpickMenu.add(jmi);
    }

    protected void resetHidePanelToggleButtons() {
        jtP.setSelected(false);
        jtA.setSelected(false);
        jtC.setSelected(false);
        jtS.setSelected(true);
    }

    protected void resetUnpick() {
        unpickButtonAction.putValue(Action.NAME, "Off");
        unpickButtonAction.putValue(Action.ACTION_COMMAND_KEY, "Off");
        unpickButtonAction.putValue(Action.SHORT_DESCRIPTION, "Unpick mode");
        unpickButtonAction.putValue(Action.SMALL_ICON, unpickButtonAction.unpickOffImage); 
        jiggle.setUnpickMode(false);
    }

private class UnpickMenuItemHandler implements ActionListener {
  public void actionPerformed(ActionEvent evt) {
    String cmd = evt.getActionCommand();
    //System.out.println("JiggleToolBar DEBUG unpick cmd: "+cmd);
    if (cmd.equals("Off")) {
        unpickButtonAction.putValue(Action.NAME, "Off");
        unpickButtonAction.putValue(Action.ACTION_COMMAND_KEY, "Off");
        unpickButtonAction.putValue(Action.SMALL_ICON, unpickButtonAction.unpickOffImage); 
        jiggle.setUnpickMode(false);
    }
    else if (cmd.equals(UnpickMode.PHASE)) {
        unpickButtonAction.putValue(Action.NAME, UnpickMode.PHASE);
        unpickButtonAction.putValue(Action.ACTION_COMMAND_KEY, UnpickMode.PHASE);
        unpickButtonAction.putValue(Action.SMALL_ICON, unpickButtonAction.unpickPImage); 
        UnpickMode.setMode(UnpickMode.PHASE);
        jiggle.setUnpickMode(true);
    }
    else if (cmd.equals(UnpickMode.AMP)) {
        unpickButtonAction.putValue(Action.NAME, UnpickMode.AMP);
        unpickButtonAction.putValue(Action.ACTION_COMMAND_KEY, UnpickMode.AMP);
        unpickButtonAction.putValue(Action.SMALL_ICON, unpickButtonAction.unpickAImage); 
        UnpickMode.setMode(UnpickMode.AMP);
        jiggle.setUnpickMode(true);
    }
    else if (cmd.equals(UnpickMode.CODA)) {
        unpickButtonAction.putValue(Action.NAME, UnpickMode.CODA);
        unpickButtonAction.putValue(Action.ACTION_COMMAND_KEY, UnpickMode.CODA);
        unpickButtonAction.putValue(Action.SMALL_ICON, unpickButtonAction.unpickCImage); 
        UnpickMode.setMode(UnpickMode.CODA);
        jiggle.setUnpickMode(true);
    }
  }
}

class UnpickButtonHandler extends AbstractAction { // implements ActionListener {

  public ImageIcon unpickPImage = null;
  public ImageIcon unpickAImage = null;
  public ImageIcon unpickCImage = null;
  public ImageIcon unpickOffImage = null;

  public UnpickButtonHandler() {
      Image img = IconImage.getImage(pre+"unpickP.gif");
      if (img != null)  unpickPImage = new ImageIcon(img);
      img = IconImage.getImage(pre+"unpickA.gif");
      if (img != null)  unpickAImage = new ImageIcon(img);
      img = IconImage.getImage(pre+"unpickC.gif");
      if (img != null)  unpickCImage = new ImageIcon(img);
      img = IconImage.getImage(pre+"killPickOff.gif");
      if (img != null)  unpickOffImage = new ImageIcon(img);

      putValue(Action.ACTION_COMMAND_KEY, "Off");
      putValue(Action.NAME, "Off");
      putValue(Action.SHORT_DESCRIPTION, "Unpick mode");
      if (unpickOffImage != null) putValue(Action.SMALL_ICON, unpickOffImage); 
      setEnabled(false);
  }

  public void actionPerformed(ActionEvent evt) {
      if (unpickMenu == null) createUnpickMenu();
      unpickMenu.show((Component)evt.getSource(), 0, 0);
  }

}

/*
class WFScrollerViewAction extends AbstractAction { // implements ActionListener {

  private ImageIcon windowImage = null;
  private ImageIcon fullImage = null;
  private Jiggle jiggle = null;

  public WFScrollerViewAction(Jiggle jiggle) {
      Image img = IconImage.getImage(pre+"window-time.gif");
      if (img != null)  windowImage = new ImageIcon(img);
      img = IconImage.getImage(pre+"full-time.gif");
      if (img != null)  fullImage = new ImageIcon(img);
      this.jiggle = jiggle;
      configureAction((jiggle.getProperties().getInt("secsPerPage") < 0));
      setEnabled(false);
  }

  public void actionPerformed(ActionEvent evt) {
      AbstractButton jb = (AbstractButton) evt.getSource();
      jiggle.wfScroller.setShowFullTime(! jiggle.wfScroller.getShowFullTime() );
      boolean tf = jiggle.wfScroller.getShowFullTime();
      double secs = Math.abs(jiggle.getProperties().getDouble("secsPerPage"));
      jiggle.getProperties().setProperty("secsPerPage", ((tf) ? -secs : secs));
      jb.setSelected(tf);
      configureAction(tf);
      jiggle.wfScroller.repaint();
  }

  private void configureAction(boolean tf) {
      if (tf) {
          putValue(Action.NAME, "Full"); 
          putValue(Action.ACTION_COMMAND_KEY, "Full");
          putValue(Action.SMALL_ICON, fullImage); 
      } else {
          putValue(Action.NAME, "Window");
          putValue(Action.ACTION_COMMAND_KEY, "Window");
          putValue(Action.SMALL_ICON, windowImage); 
      }
  }

}
*/
    private JMenuItem addToPopupMenu(String itemActionCmd, JPopupMenu menu, ActionListener al, boolean enable) {
      JMenuItem m = new JMenuItem(itemActionCmd);
      if (al != null) m.addActionListener(al);
      m.setEnabled(enable);
      return menu.add(m);
    }
    private class MagMenuCalcActionListener implements ActionListener {
        public void actionPerformed(ActionEvent evt) {
            String magType = evt.getActionCommand();
            if (magType.equals("All")) {
              String [] magTypes = jiggle.getProperties().getMagMethodTypes();
              for (int i = 0; i<magTypes.length; i++) jiggle.calcMagFromWaveforms(magTypes[i]);
            }
            else jiggle.calcMagFromWaveforms(magType);
        }
    }
    private class MagMenuPrefActionListener implements ActionListener {
        public void actionPerformed(ActionEvent evt) {
            Solution sol = jiggle.mv.getSelectedSolution();
            String magType = evt.getActionCommand();
            int idx = magType.indexOf("M") + 1;
            Magnitude prefMag = sol.getPrefMagOfType(magType.substring(idx));
            if (prefMag != null) {
                sol.setPreferredMagnitude(prefMag);
                jiggle.updateMagTab(prefMag.getTypeString()); // reset the tabs to shown correct preferred mapping
            }
            else jiggle.popInfoFrame("Preferred Magnitude","No preferred magnitude of type: "+magType);
        }
    }
/**
 * Method to stream line adding of buttons. Makes sure all buttons have the same
 * action handler.
 */
    private JButton makeButton(String label, String iconFile, String text, ActionListener al) {
      JButton btn;
      Image image = IconImage.getImage(iconFile); // this handles finding the icons in the path
      if (image == null) {
        btn = new JButton(label);
      } else {
        btn = new JButton((labelButtons && !miniButtons ? label : null), new ImageIcon(image));
      }
      btn.setActionCommand(label);
      btn.addActionListener(al);
      if (text != null ) btn.setToolTipText(text);
      setButtonSize(btn, miniButtons);
      return btn;
    }

    private JButton makeButton(String label, String iconFile, String tip)
    {
      return makeButton(label, iconFile, tip, buttonHandler);
    }

    private JButton makeButton(String label, String iconFile, String tip, int mnem)
    {
      JButton btn = makeButton(label, iconFile, tip, buttonHandler);
      //btn.setMnemonic(mnem);     // doesn't work, needs more support for focus, actions: needs INPUT MAP
      return btn;
    }

/**
 * No action handler.
 */
    private JToggleButton makeToggleButton(String label, String iconFile,
            String selectedIconFile, String tip ) {
      JToggleButton btn;
      Image image = IconImage.getImage(iconFile); // this handles finding the icons in the path
      if (image == null) {
          btn = new JToggleButton(label);
      } else {
          btn = new JToggleButton(((labelButtons && !miniButtons)? label : null), new ImageIcon(image));
          if (selectedIconFile != null) {
            image = IconImage.getImage(selectedIconFile); // this handles finding the icons in the path
            if (image != null) btn.setSelectedIcon(new ImageIcon(image));
          }
      }
      btn.setActionCommand(label);
      setButtonSize(btn, miniButtons);
      btn.setToolTipText(tip);
      return btn;
    }

/**
 * Set a reference to the Jiggle frame so we can call its methods
 */
    protected void setFrame(Jiggle jiggle) {
        this.jiggle = jiggle;
    }


/**
 * Set anything on the main tool bar that is dependent on the MasterView. For example: <p>
 *  1) JOriginComboBox<p>
 */
    protected void setMasterView() { // protected void setMasterView(mv) {
        //this.mv = mv;
        solPanel.setMasterView();
    }
/** Enable/disable components that only make sense when there's selected
event.  */
    protected void setEventEnabled(boolean tf) {

        if (DataSource.isWriteBackEnabled()) {
          deleteButton.setEnabled(tf);
          finalButton.setEnabled(tf);
          saveButton.setEnabled(tf);
          saveAllButton.setEnabled(tf);
        }
        else {
          deleteButton.setEnabled(false);
          finalButton.setEnabled(false);
          saveButton.setEnabled(false);
          saveAllButton.setEnabled(false);
        }

        if (AbstractWaveform.cache2File) { 
          cacheWfButton.setEnabled(tf);
        }
        else {
          cacheWfButton.setEnabled(false);
        }

        triaxialButton.setEnabled(tf);
        locateButton.setEnabled(tf);
        magCalcButton.setEnabled(tf);
        magPrefButton.setEnabled(tf);
        whereButton.setEnabled(tf);
        newSolButton.setEnabled(jiggle.scopeMode || tf);
        unpickButton.setEnabled(tf);
        autoPickButton.setEnabled(tf);
        solPanel.setEnabled(tf);

        swarmButton.setEnabled( (tf || jiggle.scopeMode) && jiggle.getProperties().getBoolean("enableSwarm") );
        nextButton.setEnabled(tf || jiggle.scopeMode);
        fullTimeButton.setEnabled(tf || jiggle.scopeMode);

        jtP.setEnabled(tf);
        jtA.setEnabled(tf);
        jtC.setEnabled(tf);
        jtS.setEnabled(tf);
    }

// this is the contol panel's event handler class, does menus, buttons
    private class ButtonHandler implements ActionListener {
        public void actionPerformed(ActionEvent evt) {
        // save the string of the menu item selected

          EventName =  evt.getActionCommand();

 // Code responses to button, menu and list actions here...

 // *** a menu item
          if (EventName == "Exit") {
            jiggle.stop();
          }
          else if (EventName == "File") {
            //jiggle.fileChooser.show();
          }
          else if (EventName == "Loc") {
            jiggle.locate();
//          jiggle.relocate();
          }
          //
          // TODO: split out into an Action the magnitude calc functions which can be
          // shared with ToolBar, an action customized via properties 
          //
          // For mag buttons below, we could pop a choice dialog to decide
          // to create new readings from waveforms or do existing reading recalc?
          else if (EventName == "CalcM") {
            magCalcMenu.show(magCalcButton,0,0);
          }
          else if (EventName == "PrefM") {
            JPopupMenu magPrefMenu = new JPopupMenu();
            ActionListener al = new MagMenuPrefActionListener();
            Collection prefMags = jiggle.mv.getSelectedSolution().getPrefMags();
            if (prefMags.size() == 0) {
                jiggle.popInfoFrame("Preferred Magnitude","Event has no preferred magnitudes!");
            }
            else {
              Iterator iter = prefMags.iterator();
              while (iter.hasNext()) {
                addToPopupMenu(((Magnitude)iter.next()).getTypeString(), magPrefMenu, al, true);
              }
              magPrefMenu.show(magPrefButton,0,0);
            }
          }
          else if (EventName == "Event") {
            jiggle.createNewSolution();
          }
          else if (EventName == "Cat") {
            // Change focus to catalog tab
            jiggle.tabPane.setSelectedIndex(jiggle.TAB_CATALOG);
          }
          else if (EventName == "Del") {
            jiggle.deleteCurrentSolution();
          }
          else if (EventName == "Redo") {
            jiggle.repaint();
          }
          else if (EventName == "Rollb") {
            jiggle.rollbackEventPreferred();
          }
          else if (EventName == "Save") {
            jiggle.saveToDb();
          }
          else if (EventName == "Final") {
            jiggle.finalToDb();
          }
          else if (EventName == "Next") {
              if (jiggle.scopeMode) {
                  jiggle.scopeLoadMasterView(false, 0);
              } else {
                  jiggle.loadNextSolution();
              }
          }
          else if (EventName == "SaveA") {
            jiggle.saveAllToDb();
          }
          else if (EventName == "Map") {
            jiggle.showMap();
          }
          else if (EventName == "Where") {
            jiggle.showWhereDialog();
          }
          else if (EventName == "Swarm") {
              jiggle.showSwarmWindow();
          }
    /* Not needed - anything that changes the loc will resort automatically
          else if (EventName == "Sort") {// Sort/resort the WFViews
            jiggle.resortWFViews();
          }
    */
          // repaint the panel to update labels, etc.
          repaint();
        } // end of actionPerformed
    }   // end of HandleEvent class

    
     //autopick handler
    private class AutoPickButtonHandler implements ActionListener {

      public void actionPerformed(ActionEvent evt) {
          if (autoPickMenu == null) createAutoPickMenu();
          autoPickMenu.show((Component)evt.getSource(), 0, 0);
      }

      private void createAutoPickMenu() {

        autoPickMenu = new JPopupMenu();
        ActionListener al = new AutoPickMenuHandler();

        JMenuItem jmi  = new JMenuItem("P-only");
        jmi.setActionCommand(String.valueOf(PhasePickerIF.P_ONLY));
        jmi.addActionListener(al);
        autoPickMenu.add(jmi);

        jmi  = new JMenuItem("S-only");
        jmi.setActionCommand(String.valueOf(PhasePickerIF.S_ONLY));
        jmi.addActionListener(al);
        autoPickMenu.add(jmi);
        
        jmi = new JMenuItem("P and S");
        jmi.setActionCommand(String.valueOf(PhasePickerIF.P_AND_S));
        jmi.addActionListener(al);
        autoPickMenu.add(jmi);

        jmi = new JMenuItem("Default");
        jmi.setActionCommand(String.valueOf(PhasePickerIF.DEFAULT));
        jmi.addActionListener(al);
        autoPickMenu.add(jmi);
      }

    }

    private class AutoPickMenuHandler implements ActionListener {

        public void actionPerformed(ActionEvent evt) {

            WFScroller wfs = jiggle.wfScroller;
            if (wfs == null || jiggle.mv == null) return; // nothing to pick

            if (jiggle.picker == null) jiggle.setupPicker();
            if (jiggle.picker == null) {
                JOptionPane.showMessageDialog(JiggleToolBar.this.jiggle, 
                        "No auto picker is defined, must be configured through Jiggle properties",
                        "Auto Picker",
                        JOptionPane.ERROR_MESSAGE
                );
                return;
            }

            int pickFlag = PhasePickerIF.NONE;
            String cmd = evt.getActionCommand();
            try {
                pickFlag = Integer.parseInt(cmd);
            }
            catch (NumberFormatException ex) {
                System.err.println(ex.getMessage());
                JOptionPane.showMessageDialog(JiggleToolBar.this.jiggle, 
                        "Invalid action command:" + cmd + " should be an integer string value",
                        "Auto Picker",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }

            if (jiggle.pickWorker == null) jiggle.autoPick(pickFlag);
            else {
                Object[] options = new String [] {"Continue", "Restart"};
                int iaction = JOptionPane.showOptionDialog(JiggleToolBar.this.jiggle,
                                 "Picking already in progress, choose action...",
                             "AutoPick Action",
                             JOptionPane.DEFAULT_OPTION,
                             JOptionPane.PLAIN_MESSAGE,
                             null,
                             options,
                             options[0]
                ); 

                if (iaction == 1) { 
                    jiggle.pickWorker.interrupt();
                    jiggle.pickWorker = null;
                    jiggle.autoPick(pickFlag);
                }
            }
        }
    } // end of AutoPickHandler class

} // end of JiggleToolBar class


