package org.trinet.jiggle;

import java.awt.*;

public interface MarkerIF {

       /** Set the marker color. */
       public void setColor (Color color);

       /** Return the marker color. */
       public Color getColor();

       /** Draw the marker */
       public void draw(Graphics g);

       /** Set the marker size. */
       public void setSize (Dimension dim);
       /** Return the marker size. */
       public Dimension getSize();
}

