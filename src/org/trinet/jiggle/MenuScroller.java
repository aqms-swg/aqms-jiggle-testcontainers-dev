package org.trinet.jiggle;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.MenuSelectionManager;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

/**
 * A class that provides scrolling capabilities to a popup menu. A number of
 * items can optionally be frozen at the top and/or bottom of the menu.
 * <p>
 * http://www.camick.com/java/source/MenuScroller.java
 */
public class MenuScroller implements MouseWheelListener {
    private static enum MenuIcon implements Icon {

        DOWN(1, 9, 1), UP(9, 1, 9);

        final int[] xPoints = { 1, 5, 9 };
        final int[] yPoints;

        MenuIcon(int... yPoints) {
            this.yPoints = yPoints;
        }

        @Override
        public int getIconHeight() {
            return 10;
        }

        @Override
        public int getIconWidth() {
            return 0;
        }

        @Override
        public void paintIcon(Component c, Graphics g, int x, int y) {
            if (c.isVisible()) {
                Dimension size = c.getSize();
                Graphics g2 = g.create(size.width / 2 - 5, size.height / 2 - 5, 10, 10);
                if (c.isEnabled()) {
                    g2.setColor(Color.BLACK);
                    g2.fillPolygon(xPoints, yPoints, 3);
                } else {
                    g2.setColor(Color.GRAY);
                    g2.drawPolygon(xPoints, yPoints, 3);
                }
                g2.dispose();
            }
        }
    }

    private class MenuScrollItem extends JMenuItem implements ChangeListener {
        private static final long serialVersionUID = 1L;
        private MenuScrollTimer timer;

        public MenuScrollItem(MenuIcon icon, int increment, int interval) {
            setIcon(icon);
            setDisabledIcon(icon);
            timer = new MenuScrollTimer(increment, interval);
            addChangeListener(this);
        }

        public void setInterval(int interval) {
            timer.setDelay(interval);
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            if (isArmed() && !timer.isRunning()) {
                timer.start();
            }
            if (!isArmed() && timer.isRunning()) {
                timer.stop();
            }
        }
    }

    private class MenuScrollListener implements PopupMenuListener {

        @Override
        public void popupMenuCanceled(PopupMenuEvent e) {
            popupMenuWillBecomeInvisible(e);
        }

        @Override
        public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            menu.removeAll();
            for (Component component : menuItems) {
                menu.add(component);
            }
        }

        @Override
        public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
            firstIndex = 0;
            if (menuItems.length > scrollCount) {
                refreshMenu();
            }
        }
    }

    private class MenuScrollTimer extends Timer {
        private static final long serialVersionUID = 1L;

        public MenuScrollTimer(final int increment, int interval) {
            super(interval, new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    updateFirstIndex(increment);
                }
            });
        }
    }

    /**
     * Hide disabled up/down menu items, true to hide them or false to only disable
     * them.
     */
    private static boolean hideDisabled = true;

    private static final int interval = 100; // 100 milliseconds interval

    private static Dimension getComponentSize(JPopupMenu menu, int menuCount) {
        Component menuComp;
        Dimension compSize = null;
        for (int index = 0; index < menuCount; index++) {
            menuComp = menu.getComponent(index);
            compSize = menuComp.getPreferredSize();
            if (compSize != null) {
                break;
            }
        }
        return compSize;
    }

    private static int getScrollCount(JPopupMenu menu, int menuCount) {
        int height = -1;
        Dimension menuSize = menu.getPreferredSize();
        if (menuSize != null) {
            try {
                Toolkit toolkit = Toolkit.getDefaultToolkit();
                height = (int) toolkit.getScreenSize().getHeight();
                Jiggle jiggle = Jiggle.jiggle;
                if (jiggle != null) {
                    GraphicsConfiguration gc = jiggle.getGraphicsConfiguration();
                    if (gc == null) {
                        // If we don't have GraphicsConfiguration use primary screen
                        gc = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice()
                                .getDefaultConfiguration();
                    }
                    Insets insets = toolkit.getScreenInsets(gc);
                    height -= insets.bottom + insets.top;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                height = -1;
            }
        }
        if (height < 0) {
            height = menuSize.height;
        }
        Dimension compSize = null;
        if (height != -1 && (compSize = getComponentSize(menu, menuCount)) != null) {
            int scrollCount = height / compSize.height + 1;
            if (scrollCount > 0 && scrollCount < menuCount) {
                String text = null;
                Component invoker = menu.getInvoker();
                if (invoker instanceof AbstractButton) {
                    text = ((AbstractButton) invoker).getText();
                }
                System.out.printf(
                        "MenuScroller: text=%s, scrollCount=%s, height=%s, menuHeight=%s, compHeight=%s, menuCount=%s\n",
                        text, scrollCount, height, menuSize.height, compSize.height, menuCount);
                menuCount = scrollCount - 2;
            }
        }
        return menuCount;
    }

    /**
     * Sets up scrolling for menu items if needed.
     * 
     * @param menuBar the menu bar
     */
    public static void setScrollerFor(JMenuBar menuBar) {
        Component comp;
        JPopupMenu menu;
        int menuCount;
        int scrollCount;
        final int count = menuBar.getComponentCount();
        for (int index = 0; index < count; index++) {
            comp = menuBar.getComponent(index);
            if (comp instanceof JMenu) {
                menu = ((JMenu) comp).getPopupMenu();
            } else if (comp instanceof JPopupMenu) {
                menu = (JPopupMenu) comp;
            } else {
                menu = null;
            }
            if (menu != null) {
                menuCount = menu.getComponentCount();
                if (menuCount > 0) {
                    scrollCount = getScrollCount(menu, menuCount);
                    if (scrollCount != menuCount) {
                        new MenuScroller(menu, scrollCount);
                    }
                }
            }
        }
    }

    private final MenuScrollItem downItem;

    private int firstIndex = 0;

    private final JPopupMenu menu;

    private final Component[] menuItems;

    private final MenuScrollListener menuListener = new MenuScrollListener();

    private int scrollCount;

    private final MenuScrollItem upItem;

    /**
     * Constructs a <code>MenuScroller</code> that scrolls a popup menu with the
     * specified number of items to display in the scrolling region, the specified
     * scrolling interval, and the specified numbers of items fixed at the top and
     * bottom of the popup menu.
     * 
     * @param menu             the popup menu
     * @param scrollCount      the number of items to display in the scrolling
     *                         portion
     * @param bottomFixedCount the number of items to fix at the bottom. May be 0
     * 
     */
    private MenuScroller(JPopupMenu menu, int scrollCount) {
        if (scrollCount <= 0 || interval <= 0) {
            throw new IllegalArgumentException("scrollCount and interval must be greater than 0");
        }

        upItem = new MenuScrollItem(MenuIcon.UP, -1, interval);
        downItem = new MenuScrollItem(MenuIcon.DOWN, +1, interval);
        if (scrollCount <= 0) {
            throw new IllegalArgumentException("scrollCount must be greater than 0");
        }
        this.scrollCount = scrollCount;
        MenuSelectionManager.defaultManager().clearSelectedPath();
        upItem.setInterval(interval);
        downItem.setInterval(interval);

        this.menu = menu;
        menuItems = menu.getComponents();
        menu.addPopupMenuListener(menuListener);
        menu.addMouseWheelListener(this);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        updateFirstIndex(e.getUnitsToScroll());
        e.consume();
    }

    private void refreshMenu() {
        if (menuItems != null && menuItems.length > 0) {
            firstIndex = Math.min(menuItems.length - scrollCount, firstIndex);

            boolean upEnabled = firstIndex > 0;
            boolean downEnabled = firstIndex + scrollCount < menuItems.length;
            if (hideDisabled) {
                upItem.setVisible(upEnabled);
                downItem.setVisible(downEnabled);
            } else {
                upItem.setEnabled(upEnabled);
                downItem.setEnabled(downEnabled);
            }

            menu.removeAll();
            menu.add(upItem);
            for (int i = firstIndex; i < scrollCount + firstIndex; i++) {
                menu.add(menuItems[i]);
            }
            menu.add(downItem);
            JComponent parent = (JComponent) upItem.getParent();
            parent.revalidate();
            parent.repaint();
        }
    }

    private void updateFirstIndex(int amount) {
        int index = firstIndex + amount;
        if (index < 0) {
            index = 0;
        } else if (index >= menuItems.length) {
            index = menuItems.length - 1;
        }
        if (firstIndex != index) {
            firstIndex = index;
            refreshMenu();
        }
    }
}