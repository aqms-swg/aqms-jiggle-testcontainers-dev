package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;
import javax.swing.event.*;

import org.trinet.jasi.*;
import org.trinet.jasi.coda.TimeAmp;
import org.trinet.util.StringList;
import org.trinet.util.GenericPropertyList;

/**
 * Present the Phase pick popup menu
 */
public class PhasePopup {

  protected static boolean menuLayoutFlat = false; 

  /** Waveform panel pick is being made in*/
  private WFPanel wfp = null;

  /** The actual popup menu. */
  private JPopupMenu popup = new JPopupMenu();

  /** The epoch time of the pick */
  private double pickTime = 0.;

  /** String contining the software's guess at the phase that was picked. */
  private String guess = null;

  /** The resulting phase */
  private Phase newPh = null;

  /** The current Masterview selected Solution */
  private Solution sol = null;

  private ArrayList pItems = null;
  private ArrayList sItems = null;
  private int minSwgt = 0;
  private int maxWt4HumanFm = 4; 
  private String triaxialGroup = null;

  private boolean phaseDescWtCheck = true;
  private boolean alwaysResetLowQualFm = false;

  //private boolean triaxialZonH = true;

  /**
  * Create a PhasePopup. The WFPanel is needed to get
  * info on the waveform and panel being picked. The MasterView is
  * used to help figure out which phase this is; P or S.<p>
  * The PhasePopup is created fresh each invocation because the top
  * menu item changes with each pick.
  */
  public PhasePopup(Component refComp, ActiveWFPanel wfp, Point location)  {

    this.wfp = wfp;
    this.triaxialGroup = Triaxial.getGroup(wfp.wfv.getChannelObj().getSeedchan());
    
    // current active solution
    sol = wfp.wfv.mv.getSelectedSolution();

    // Figure the picktime of the bombsight
    pickTime = wfp.wfv.mv.masterWFWindowModel.getTimeSpan().getCenter();

    // Make tentitive phase: guess at pick type and description and put as first menu item
    //newPh = wfp.wfv.describePickAt(pickTime, sol);
    newPh = (sol == null) ? null : wfp.describePickAt(pickTime, sol); // 11/16/2006 -aww describe alternate wf if filtered

    JiggleProperties gpl = null;
    if (wfp != null && wfp.wfv != null && (wfp.wfv.mv.getOwner() instanceof Jiggle)) {
      gpl = ((Jiggle) wfp.wfv.mv.getOwner()).getProperties();
    }

    //if (gpl.isSpecified("triaxialZonH")) triaxialZonH = gpl.getBoolean("triaxialZonH");
    maxWt4HumanFm = gpl.getInt("phasePopupMenu.maxWt4HumanFm", 1);
    phaseDescWtCheck = gpl.getBoolean("phasePopupMenu.phaseDescWtCheck", true);
    //System.out.println(">>>>>>      DEBUG PhasePopup phasePopupMenu.phaseDescWtCheck=" + phaseDescWtCheck); 
    alwaysResetLowQualFm = gpl.getBoolean("phasePopupMenu.alwaysResetLowQualFm", false);
    minSwgt = gpl.getInt("phasePopupMenu.S.minWgt", 0);

    StringList aList = new StringList(gpl.getProperty("phasePopupMenu.P.desc", "iP0 iP1 eP2 eP3 eP4"));
    if (pItems == null) pItems = new ArrayList(11);
    else pItems.clear();

    String[] desc = aList.toArray();
    int wt = 0;
    for (int idx=0; idx<desc.length; idx++) {
        if (desc[idx].length() > 3) { // has fm
            wt = PhaseDescription.getWtFrom(desc[idx]);
            if (wt > maxWt4HumanFm ) { // remove above limit
                aList.getList().remove(desc[idx]);
                continue;
            }
        }
        if (wfp.wfv.getChannelObj().isHorizontal()) {  // remove if fm not allowed on horiz 
            if (!PhaseDescription.getFmOnHoriz() && desc[idx].length()>3) aList.getList().remove(desc[idx]);
        }
    }
    pItems.addAll(aList.getList());

    aList = new StringList(gpl.getProperty("phasePopupMenu.S.desc", "iS0 iS1 eS2 eS3 eS4"));
    if (sItems == null) sItems = new ArrayList(11);
    else sItems.clear();

    desc = aList.toArray();
    for (int idx=0; idx<desc.length; idx++) {
        wt = PhaseDescription.getWtFrom(desc[idx]);
        if (wt < minSwgt) {
            aList.getList().remove(desc[idx]);
            continue;
        }
        if (desc[idx].length() > 3) {
            if (wt > maxWt4HumanFm) {
                aList.getList().remove(desc[idx]);
                continue;
            }
        }
        if (!PhaseDescription.getFmOnS() && desc[idx].length()>3) {  // remove if fm not allowed on S 
            aList.getList().remove(desc[idx]);
            continue;
        }
        if (wfp.wfv.getChannelObj().isHorizontal()) {   // remove if fm P not allowed on horiz 
            if (!PhaseDescription.getFmOnHoriz() && desc[idx].length()>3) aList.getList().remove(desc[idx]);
        }
    }
    sItems.addAll(aList.getList());

    createPhasePopup(refComp, location);
  }

  private void createPhasePopup(Component refComp, final Point location) {

      ActionListener al = new PhasePopupListener();
      if (sol != null) {
        // add the P choices
        if (menuLayoutFlat) {
          for (int idx=0; idx<pItems.size(); idx++) {
              addPopupMenuItem((String)pItems.get(idx), popup, al);
          }
          //addPopupMenuItem("iP0", popup, al);
          //addPopupMenuItem("iP1", popup, al);
          //addPopupMenuItem("eP2", popup, al);
          //addPopupMenuItem("eP3", popup, al);
          //addPopupMenuItem("eP4", popup, al);
        }
        else {
          JMenu jmp = new JMenu("P") ;
          for (int idx=0; idx<pItems.size(); idx++) {
              addMenuItem((String)pItems.get(idx), jmp, al);
          }
          //addMenuItem("iP0", jmp, al);
          //addMenuItem("iP1", jmp, al);
          //addMenuItem("eP2", jmp, al);
          //addMenuItem("eP3", jmp, al);
          //addMenuItem("eP4", jmp, al);
          popup.add(jmp); 
        }
        popup.addSeparator();

        // add the S choices
        if (menuLayoutFlat) {
          for (int idx=0; idx<sItems.size(); idx++) {
              addPopupMenuItem((String)sItems.get(idx), popup, al);
          }
          //addPopupMenuItem("iS0", popup, al);
          //addPopupMenuItem("iS1", popup, al);
          //addPopupMenuItem("eS2", popup, al);
          //addPopupMenuItem("eS3", popup, al);
          //addPopupMenuItem("eS4", popup, al);
        }
        else {
          JMenu jms = new JMenu("S") ;
          for (int idx=0; idx<sItems.size(); idx++) {
              addMenuItem((String)sItems.get(idx), jms, al);
          }
          //addMenuItem("iS0", jms, al);
          //addMenuItem("iS1", jms, al);
          //addMenuItem("eS2", jms, al);
          //addMenuItem("eS3", jms, al);
          //addMenuItem("eS4", jms, al);
          popup.add(jms); 
        }
        popup.addSeparator();

        // Guess the current phase description from waveform timeseries attributes
        guess = newPh.description.toHypoinverseString(); // aww, I want to see the auto set fm 
        //Removed below option because quality (i,e,w) often wrong -aww 07/9/2006
        //addPopupMenuItem(guess, popup, al);
        //popup.addSeparator();

        // add other functions
        final Phase oldPh = (Phase) wfp.wfv.phaseList.getNearestToTime(pickTime, sol);
        if (oldPh != null) {
          JMenu jm = new JMenu("Filter");
          ActionListener al3 = new AcceptRejectDeleteListener(oldPh);
          addMenuItem("Delete", jm, al3);
          addMenuItem("Polarity", jm, al3);
          JMenu jm2 = new JMenu("Qual");
          addMenuItem("e", jm2, al3);
          addMenuItem("i", jm2, al3);
          addMenuItem("w", jm2, al3);
          jm.add(jm2);
          addMenuItem(oldPh.isReject() ? "Accept" : "Reject", jm, al3, " quality/wt when locating");
          popup.add(jm);
          popup.addSeparator();
        }
  
        addPopupMenuItem("Edit desc...", popup,
          new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //PhaseDescription inDesc = (PhaseDescription) newPh.description.clone();
                // BUGFIX ? used to be only newPh input below here - aww 2011/04/12
                Phase inPh = (oldPh == null) ? newPh : (Phase) oldPh.clone();
                guess = inPh.description.toHypoinverseString();
                PhaseDialog pd = new PhaseDialog((Frame) wfp.getTopLevelAncestor(), popup, wfp, wfp.wfv.mv, inPh);
                pd.phaseDescWtCheck = phaseDescWtCheck;
                //System.out.println(">>>>>  DEBUG PhasePopup PhaseDialog phasePopupMenu.phaseDescWtCheck=" + pd.phaseDescWtCheck); 
                pd.alwaysResetLowQualFm = alwaysResetLowQualFm;
            }
          },
          "Event association, phase type Pn, Sn, ..." 
        );
        popup.addSeparator();

        // Compose HYPO phase description from menu elements
        popup.add(makePopupDescMenu("i"));
        popup.add(makePopupDescMenu("e"));
        popup.addSeparator();
        //
        ActionListener ampListener = new AmpWindowMenuItemActionListener();

        // Renamed from "Window amps for coda"
        addPopupMenuItem("Set manual coda", popup, ampListener,
            "2-sec window of averaged absolute amps (centered on mouse click time, bias removed)"
        ); // end of menu item

        addPopupMenuItem("Set manual coda - Weight 0", popup, ampListener,
               "2-sec window of averaged absolute amps (centered on mouse click time, bias removed)"
        ); // end of menu item

        popup.addSeparator();

        addPopupMenuItem("Window amp levels", popup, ampListener,
            "Report amp statistics for a 2-sec window centered on mouse click time (bias removed)."
        ); // end of menu item
        popup.addSeparator();

      } 


      addPopupMenuItem("Scroll to", popup, 
        new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                wfp.wfv.mv.masterWFViewModel.set(wfp.wfv, true); // notify scroller
            }
        }, "Make this panel visible in lower group scroller panel (i.e. at top or bottom");
      /* Probably not needed since WFCacheManager should not unload the "selected" view -aww 2009/04/19 
      addPopupMenuItem("Load timeseries", popup, 
        new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                wfp.wfv.loadTimeSeries();
            }
        }, "Load timeseries into viewport");
      popup.addSeparator();

      */
      addPopupMenuItem( "Toggle bias line",
        popup, 
        new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (wfp instanceof ZoomableWFPanel) {
                    ZoomableWFPanel zwfp = (ZoomableWFPanel) wfp;
                    zwfp.paintBiasLine = ! zwfp.paintBiasLine;  
                    zwfp.repaint();
                }
            }
      }, "Toggle bias center line");
      addPopupMenuItem( "Toggle zero line",
        popup, 
        new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (wfp instanceof ZoomableWFPanel) {
                    ZoomableWFPanel zwfp = (ZoomableWFPanel) wfp;
                    zwfp.paintZeroLine = ! zwfp.paintZeroLine;  
                    zwfp.repaint();
                }
            }
      }, "Toggle amp zero line");
      popup.addSeparator();
      // Triaxial code added -aww
      //System.out.println("DEBUG PhasePopup triaxialGroup: " + triaxialGroup + " show triaxials?: " + wfp.showTriaxials);
      if (wfp.showTriaxials) { // -aww 2010/01/25
          ActionListener al2 = new ActionListener() {
              public void actionPerformed(ActionEvent evt) {
                  JCheckBoxMenuItem jcbmi = (JCheckBoxMenuItem) evt.getSource();
                  String orient = jcbmi.getActionCommand();

                  if (triaxialGroup == null) return;

                  int index = triaxialGroup.indexOf(orient.charAt(orient.length()-1));
                  Color c = null;
                  if (index == 0) c = WFPanel.Zcolor;
                  else if (index == 1) c = WFPanel.Ncolor;
                  else if (index == 2) c = WFPanel.Ecolor;

                  /*
                  if (orient.endsWith("Z")) {
                      c = WFPanel.Zcolor;
                  }
                  else if (orient.endsWith("N")) {
                      c = WFPanel.Ncolor;
                  }
                  else if (orient.endsWith("E")) {
                      c = WFPanel.Ecolor;
                  }
                  */
                  if (c != null) {
                      //System.out.println("wfp.repaint setPlotTriaxial(" + orient + "," + jcbmi.isSelected() + " )");
                      wfp.setPlotTriaxial(c, jcbmi.isSelected());
                      wfp.repaint();
                  }
              }
          };

          if (triaxialGroup == null) {
              System.err.println("Warning: PhasePopup found no triaxial group for seedchan: " + wfp.wfv.getChannelObj().getSeedchan());
          }
          else {
              JCheckBoxMenuItem jcbmi = new JCheckBoxMenuItem("Show " + triaxialGroup.charAt(0));
              //if (wfp.wfv.getChannelObj().isHorizontal()) wfp.setPlotTriaxial(wfp.Zcolor, triaxialZonH);
              jcbmi.setSelected(wfp.canPlotTriaxial(wfp.Zcolor));
              jcbmi.setForeground(wfp.Zcolor);
              jcbmi.addActionListener(al2);
              popup.add(jcbmi);

              if (triaxialGroup.length() >= 2) {
                  jcbmi = new JCheckBoxMenuItem("Show " + triaxialGroup.charAt(1));
                  jcbmi.setSelected(wfp.canPlotTriaxial(wfp.Ncolor));
                  jcbmi.setForeground(wfp.Ncolor);
                  jcbmi.addActionListener(al2);
                  popup.add(jcbmi);
              }

              if (triaxialGroup.length() >= 3) {
                  jcbmi = new JCheckBoxMenuItem("Show " + triaxialGroup.charAt(2));
                  jcbmi.setSelected(wfp.canPlotTriaxial(wfp.Ecolor));
                  jcbmi.setForeground(wfp.Ecolor);
                  jcbmi.addActionListener(al2);
                  popup.add(jcbmi);
              }
              popup.addSeparator();
          }
      }
      //
      if (sol != null) {
          popup.add(new JMenuItem(new MouseHelpAction()));
          popup.add(new JMenuItem(new PickingHelpAction()));
          popup.addSeparator();
      }
      //
      addPopupMenuItem( "Other actions...",
        popup,
        new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (wfp instanceof ActiveWFPanel) {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                           ((ActiveWFPanel)wfp).doMakePopup(location, false);
                        }
                    });
                }
            }
      }, "Other actions like delete/strip readings, sort, show channel info, etc.)");
      popup.addSeparator();
      //
      popup.add(new JMenuItem("Close popup")); // not needed -aww

      popup.pack();

      //popup is located relative to 'refComp'
      int xoffset = 0;
      int yoffset = 0;
      if (sol != null) {
        double pHeight = 0.;
        double sHeight = 0.;
        double height = 0.;
        Component [] comps = popup.getComponents();
        JMenuItem jmi = null;
        for (int ii=0; ii<comps.length; ii++) {
            double cHeight = comps[ii].getPreferredSize().getHeight();
            if (comps[ii] instanceof JMenuItem) {
              jmi = (JMenuItem) comps[ii];
              if (jmi.getActionCommand().indexOf("P") >= 0 && pHeight == 0.)
                pHeight = height + 0.5 * cHeight;
              if (jmi.getActionCommand().indexOf("S") >= 0 && sHeight == 0.)
                sHeight = height + 0.5 * cHeight;
              if (pHeight > 0. && sHeight > 0.) break; 
            }
            height += cHeight;
        }
        xoffset = (int) popup.getPreferredSize().getWidth() - 10;
        yoffset = (guess.indexOf("P") >= 0) ? (int) pHeight : (int) sHeight;
      }

      popup.show(refComp, location.x-xoffset, location.y-yoffset);
  }

  // For user generated phase descriptors built from cascading menu tree
  private class PhaseDescActionListener implements ActionListener {

      String str = "";
      String type = "";

      public PhaseDescActionListener(String desc, String ps) {
          str = (desc == null) ? "" : desc;
          type = ps;
      }

      public void actionPerformed(ActionEvent e) {
          JMenuItem mi = (JMenuItem) e.getSource();
          // user generated phase descriptors built from cascading menu tree
          str += mi.getText();
          //System.out.println("Descriptor: " + str);
          if (type.equals("P")) {
              newPh.setReject(newPh.description.parseHypoinverseString(str, wfp.wfv.getChannelObj())); // does S and Horiz fm check

          }
          else if (type.equals("S")) {
              newPh.setReject(newPh.description.parseShortString(str, wfp.wfv.getChannelObj())); // does S and Horiz fm check
          }
          /* Test fm on S see above it does this test and strips fm
          if (!PhaseDescription.getFmOnS()) newPh.description.fm = PhaseDescription.DEFAULT_FM;
              else if (!newPh.description.fm.equals(PhaseDescription.DEFAULT_FM)) {
                 int ans = JOptionPane.showConfirmDialog(wfp.getTopLevelAncestor(),
                     "First motion on S, remove first motion?", "Quality Check", JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
                 if (ans == JOptionPane.YES_OPTION) newPh.description.fm = PhaseDescription.DEFAULT_FM;
          }

          // Test fm on horiz
          if (wfp.wfv.getChannelObj().isHorizontal()) {  // remove if fm not allowed on horiz 
               if (!PhaseDescription.getFmOnHoriz() && !newPh.description.fm.equals(PhaseDescription.DEFAULT_FM)) {
                 int ans = JOptionPane.showConfirmDialog(wfp.getTopLevelAncestor(),
                     "First motion on horizontal, remove first motion?", "Quality Check", JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
                 if (ans == JOptionPane.YES_OPTION) newPh.description.fm = PhaseDescription.DEFAULT_FM;
               }
          }
          */

          // Test fm wt quality
          if (newPh.description.getQuality() < PhaseDescription.getFmQualCut() && !newPh.description.fm.equals(PhaseDescription.DEFAULT_FM)) {
              boolean tf = false;
              if (phaseDescWtCheck) {
                  int ans = JOptionPane.showConfirmDialog(wfp.getTopLevelAncestor(),
                     "Weight below quality for first motion, remove first motion?", "Quality Check",
                     JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
                  tf = (ans == JOptionPane.YES_OPTION);
              }
              else tf = alwaysResetLowQualFm; 
              if (tf) {
                  newPh.description.fm = PhaseDescription.DEFAULT_FM;
              }
          }

          newPh.qualityToDeltaTime(); // reset deltim

          wfp.wfv.mv.addReadingToCurrentSolution(newPh);

          if (wfp.wfv.mv.getOwner() instanceof Jiggle) {
              Jiggle jiggle = (Jiggle) wfp.wfv.mv.getOwner(); 
              jiggle.updateStatusBarCounts();
          }

      }
  }

  private JMenu makeDescAcceptMenuP(String ie) {
      PhaseDescActionListener al = new PhaseDescActionListener(ie+"P ", "P");
      JMenu _Menu = new JMenu(".");
      Font fnt = _Menu.getFont().deriveFont(Font.BOLD, 14.f);
      _Menu.setFont(fnt);
      addMenuItem("0", _Menu, al); // wouldn't first motion be obvious?
      addMenuItem("1", _Menu, al);
      addMenuItem("2", _Menu, al);
      addMenuItem("3", _Menu, al);
      addMenuItem("4", _Menu, al);

      JMenu cMenu = null;
      JMenu dMenu = null;
      JMenu pMenu = null;
      JMenu mMenu = null;

      if (PhaseDescription.getFmOnHoriz() || !wfp.wfv.getChannelObj().isHorizontal()) {  // remove if fm not allowed on horiz 
        al = new PhaseDescActionListener(ie+"Pc", "P");
        cMenu = new JMenu("c");
        addMenuItem("0", cMenu, al);
        if (maxWt4HumanFm >= 1) addMenuItem("1", cMenu, al);
        if (maxWt4HumanFm >= 2) addMenuItem("2", cMenu, al);
        if (maxWt4HumanFm >= 3) addMenuItem("3", cMenu, al);
        if (maxWt4HumanFm >= 4) addMenuItem("4", cMenu, al);

        al = new PhaseDescActionListener(ie+"Pd", "P");
        dMenu = new JMenu("d");
        addMenuItem("0", dMenu, al);
        if (maxWt4HumanFm >= 1) addMenuItem("1", dMenu, al);
        if (maxWt4HumanFm >= 2) addMenuItem("2", dMenu, al);
        if (maxWt4HumanFm >= 3) addMenuItem("3", dMenu, al);
        if (maxWt4HumanFm >= 4) addMenuItem("4", dMenu, al);

        al = new PhaseDescActionListener(ie+"P+", "P");
        pMenu = new JMenu("+");
        pMenu.setFont(fnt);
        addMenuItem("0", pMenu, al);
        if (maxWt4HumanFm >= 1) addMenuItem("1", pMenu, al);
        if (maxWt4HumanFm >= 2) addMenuItem("2", pMenu, al);
        if (maxWt4HumanFm >= 3) addMenuItem("3", pMenu, al);
        if (maxWt4HumanFm >= 4) addMenuItem("4", pMenu, al);

        al = new PhaseDescActionListener(ie+"P-", "P");
        mMenu = new JMenu("-");
        mMenu.setFont(fnt);
        addMenuItem("0", mMenu, al);
        if (maxWt4HumanFm >= 1) addMenuItem("1", mMenu, al);
        if (maxWt4HumanFm >= 2) addMenuItem("2", mMenu, al);
        if (maxWt4HumanFm >= 3) addMenuItem("3", mMenu, al);
        if (maxWt4HumanFm >= 4) addMenuItem("4", mMenu, al);
      }

      JMenu psMenu = new JMenu("P");
      if (ie.equals("e")) psMenu.add(_Menu);
      if (cMenu != null) psMenu.add(cMenu);
      if (dMenu != null) psMenu.add(dMenu);
      if (pMenu != null) psMenu.add(pMenu);
      if (mMenu != null) psMenu.add(mMenu);
      if (ie.equals("i")) psMenu.add(_Menu);
      return psMenu;
  }

  private JMenu makeDescAcceptMenuS(String ie) {
      PhaseDescActionListener al = new PhaseDescActionListener(ie+"S", "S");
      JMenu psMenu = new JMenu("S");
      if (minSwgt <= 0) addMenuItem("0", psMenu, al);
      if (minSwgt <= 1) addMenuItem("1", psMenu, al);
      if (minSwgt <= 2) addMenuItem("2", psMenu, al);
      if (minSwgt <= 3) addMenuItem("3", psMenu, al);
      addMenuItem("4", psMenu, al);
      // NOTE no FM on S implementation here like above for P
      return psMenu;
  }

  private JMenu makePopupDescMenu(String ie) {
      JMenu ieMenu = new JMenu(ie);
      ieMenu.add(makeDescAcceptMenuP(ie));
      ieMenu.add(makeDescAcceptMenuS(ie));
      return ieMenu;
  }
/**
 * Streamline adding of menu items
 */
    private void addMenuItem(String text, JMenu jm, ActionListener al) {
        addMenuItem(text, jm, al, null);
    }

    private void addMenuItem(String text, JMenu jm, ActionListener al, String ttt) {
        JMenuItem item = jm.add(new JMenuItem(text));
        item.addActionListener(al);
        if (ttt != null) item.setToolTipText(ttt);
    }

    private void addPopupMenuItem(String text, JPopupMenu jm, ActionListener al) {
        addPopupMenuItem(text, jm, al, null);
    }
    private void addPopupMenuItem(String text, JPopupMenu jm, ActionListener al, String ttt) {
        JMenuItem item = jm.add(new JMenuItem(text));
        item.addActionListener(al);
        if (ttt != null) item.setToolTipText(ttt);
    }

/**
/**
 * Menu ActionListener
 */
//TODO: the final disposition of the phase should be handled by a MVC model.

    private class AcceptRejectDeleteListener implements ActionListener {
        Phase oldPh = null;

        public AcceptRejectDeleteListener(Phase ph)  {
            oldPh = ph;
        }

        public void actionPerformed(ActionEvent evt) {

            String str = ((JMenuItem) evt.getSource()).getText();

            if ( oldPh == null || str.equals("Cancel") ) return;

            if ( str.equals("Delete") )  {
                sol.erase(oldPh); // aww used to be sol.deletePhase(oldPh);
                Jiggle.jiggle.updateStatusBarCounts();
            }
            else if ( str.equals("Reject") )  {  // aww
                oldPh.setReject(true);

            }
            else if ( str.equals("Accept") )  {  // aww
                oldPh.setReject(false);
            }
            else if (str.equals("Polarity")) {
                oldPh.description.flipPolarity();
            }
            else if (str.length() == 1 && "eiw".indexOf(str) > -1 ) {
                oldPh.description.ei = str;
            }

            // ? kludge below to fire event for update of group panel -aww
            wfp.wfv.mv.masterWFWindowModel.reset();
            wfp.repaint();

        }
    }


    private class PhasePopupListener implements ActionListener {
        public void actionPerformed(ActionEvent evt) {

            String str = ((JMenuItem) evt.getSource()).getText();
            //System.out.println("PhasePopup action str: " +str + " newPh.description.fm: " + newPh.description.fm );

            if (str.equals(guess)) { // Disabled above, no listener added above for this guess item
              newPh.setReject(newPh.description.parseHypoinverseString(str, wfp.wfv.getChannelObj()));
            }
            else { // an express menu item was selected
              // parse the string in the menu item (fm is ALREADY set by the describe pick guess at the timing line)
              if (str.length() == 3) {
                  newPh.setReject(newPh.description.parseShortString(str, wfp.wfv.getChannelObj())); // does S and Horiz fm check
                  // reset the existing fm if it doesn't satify the min cutoff quality -aww
                  //newPh.description.setQuality(newPh.description.getQuality(), true); // aww 02/07/2006
                  if (newPh.description.getQuality() < PhaseDescription.getFmQualCut()) newPh.description.fm = PhaseDescription.DEFAULT_FM;
              }
              else { // Takes the selected descriptor's FM
                  newPh.setReject(newPh.description.parseHypoinverseString(str, wfp.wfv.getChannelObj())); // does S and Horiz fm check
                  // full string, assume human override removes test for fm
                  //newPh.description.setQuality(newPh.description.getQuality(), true); // removed test for fm - aww 2011/04/12
                  if (newPh.description.getQuality() < PhaseDescription.getFmQualCut() && !newPh.description.fm.equals(PhaseDescription.DEFAULT_FM)) {
                      boolean tf = false;
                      if (phaseDescWtCheck) {
                          int ans = JOptionPane.showConfirmDialog(wfp.getTopLevelAncestor(),
                             "Weight below quality for first motion, remove first motion?", "Quality Check",
                             JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
                          tf = (ans == JOptionPane.YES_OPTION);
                      }
                      else tf = alwaysResetLowQualFm; 
                      if (tf) {
                          newPh.description.fm = PhaseDescription.DEFAULT_FM;
                      }
                  }
              }
            }

            newPh.qualityToDeltaTime();  // resets deltim

            // associate phase with current solution
            //newPh.assign(sol); // associate(sol) adds it to end of list, see addOrReplace below - aww removed 06/26/2006

            // add it to the Solution phase list
            //sol.addOrReplace(newPh); // aww removed 06/22/2006
            //sol.getPhaseList().addOrReplaceAllOfSameStaType(newPh); // - aww added 06/22/2006 removed 06/26/2006
            wfp.wfv.mv.addReadingToCurrentSolution(newPh);

            if (wfp.wfv.mv.getOwner() instanceof Jiggle) {
                Jiggle jiggle = (Jiggle) wfp.wfv.mv.getOwner(); 
                jiggle.updateStatusBarCounts();
            }

        }
    }

    private class AmpWindowMenuItemActionListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {

            if (! (wfp instanceof ActiveWFPanel)) return;

            String cmd = e.getActionCommand();

            final boolean doCoda = (cmd.indexOf("coda") >= 0) || (cmd.indexOf("Coda") >= 0);
            final boolean doCodaWeight0 = (cmd.toLowerCase().indexOf("weight 0") >= 0);

            SwingUtilities.invokeLater(
                new Runnable() {
                    public void run() {
                        double clickTime =  wfp.dtOfPixel(((ActiveWFPanel)wfp).mouseClickPoint);
                        // gain
                        ChannelGain cg = wfp.wfv.getChannelObj().getGain(clickTime);
                        // over timespan
                        Waveform wf = wfp.getWf();
                        if (wf == null) return;
                        //System.out.println("Click time : " + org.trinet.util.EpochTime.epochToString(clickTime));
                        int snt = WFSegment.scanNoiseType;
                        WFSegment.scanNoiseType = WFSegment.AAA;
                        float noiseAAA = wf.scanForNoiseLevel(clickTime-1.,clickTime+1.);
                        WFSegment.scanNoiseType = WFSegment.AVG;
                        float noiseAVG = wf.scanForNoiseLevel(clickTime-1.,clickTime+1.);
                        WFSegment.scanNoiseType = WFSegment.RMS;
                        float noiseRMS = wf.scanForNoiseLevel(clickTime-1.,clickTime+1.);

                        // Do we want to have peak measurement in window
                        SampleWithPeriod sample = (SampleWithPeriod) wf.scanForPeak(clickTime-1.,clickTime+1.);

                        String units = Units.getString(wf.getAmpUnits());
                        String peakStr = String.format("Peak amp 2-Sec window %13.4e %s Period=%5.2f secs at ",
                                sample.value, units, sample.period, org.trinet.util.LeapSeconds.trueToString(sample.datetime));

                        int cnts = -1;
                        int rmsCnts = -1;
                        int cutoff = 0;
                        if (!cg.isNull() && units.equalsIgnoreCase("counts") ) {
                            int iunits = ChannelGain.getResultingUnits(cg);
                            units = Units.getString(iunits);
                            double gVal = Math.abs(cg.doubleValue());
                            //gVal = Units.convertToCGS(gVal, iunits); // removed bugfix 2014/04/11 -aww
                            gVal = Units.convertFromCommon(gVal, iunits); // since metric units are the denominator
                            if (!Double.isNaN(gVal)) {
                              cutoff = (int) Math.round(gVal*1.7297E-5);
                              if (!Float.isNaN(noiseAAA)) {
                                  cnts = Math.round(noiseAAA);
                                  noiseAAA = (float) (noiseAAA/gVal);
                              }
                              if (!Float.isNaN(noiseAVG)) {
                                  noiseAVG = (float) (noiseAVG/gVal);
                              }
                              if (!Float.isNaN(noiseRMS)) {
                                  rmsCnts = Math.round(noiseRMS);
                                  noiseRMS = (float) (noiseRMS/gVal);
                              }
                              SampleWithPeriod sampleG = (SampleWithPeriod) sample.clone(); 
                              if (!Double.isNaN(sampleG.value)) {
                                  sampleG.value = sampleG.value/gVal; 
                                  peakStr += String.format(" %13.4g %s", sampleG.value,units);
                              }
                            }
                        }

                        String chanStr = wfp.wfv.getChannelObj().toDelimitedSeedNameString(".");
                        System.out.println(peakStr + " for " + chanStr);

                        WFSegment.scanNoiseType = snt;

                        String avgStr = "";
                        if (doCoda) {
                           avgStr = String.format("Avg Abs Amps:%13.4e %s, %8d counts (cutoff is %6d counts) at %s",
                                noiseAAA, units, cnts, cutoff, org.trinet.util.LeapSeconds.trueToString(clickTime));
                        }
                        else {
                           avgStr = String.format("Avg Peak noise:%13.4e RMS noise:%13.4e %s, %8d counts(rms) at %s",
                                noiseAVG, noiseRMS, units, rmsCnts, org.trinet.util.LeapSeconds.trueToString(clickTime));
                        }
                        avgStr = String.format("2-Sec Window %s", avgStr);

                        System.out.println(avgStr + " for " + chanStr);

                        // Coda measurement section
                        if (!doCoda) {
                            JOptionPane.showMessageDialog(wfp.getTopLevelAncestor(), peakStr+"\n"+avgStr,
                                    "2-Sec Window Amp Level " + chanStr, JOptionPane.PLAIN_MESSAGE);
                            return;
                        }
                        else { // doCoda option
                          Magnitude mag = sol.getPrefMagOfType("d");
                          boolean haveMd = (mag != null); 
                          if (!haveMd) {
                              mag = Magnitude.create();
                              mag.assign(sol); // alternative here also sets mag's orid to sol's
                              // NOTE: regional eventAuthority or solution's origin authority could be different
                              //mag.authority.setValue(mag.sol.getEventAuthority()); // added -aww 2011/08/17, removed 2011/09/21
                              mag.authority.setValue(EnvironmentInfo.getNetworkCode()); // removed -aww 2011/08/17, reverted 2011/09/21
                              mag.source.setValue(EnvironmentInfo.getApplicationName());
                              org.trinet.jasi.engines.MagnitudeEngineIF magEng = Jiggle.jiggle.solSolverDelegate.initMagEngineForType(sol, "md"); 
                              org.trinet.jasi.magmethods.MagnitudeMethodIF magMeth = (magEng != null) ? magEng.getMagMethod() : null;
                              mag.algorithm.setValue((magMeth != null) ? magMeth.getMethodName() : "unknown");
                              mag.subScript.setValue("d");
                              mag.value.setValue(-1.); // set "dummy" value to start
                          }

                          Coda oldCoda = (haveMd) ? (Coda) mag.getReadingList().getByChannel(wfp.wfv) : null; 

                          String cmdStr = (oldCoda != null) ?
                              avgStr + "\nClick YES to reset coda TAU to mouse click time" :
                              avgStr + "\nClick YES to create new coda reading for hand measured tau?";

                          int ans = -1;
                          String wt = null;
                          if (!doCodaWeight0) {
                            ans = JOptionPane.showConfirmDialog(wfp.getTopLevelAncestor(), cmdStr, "2-Sec Window Amp Level " + chanStr,
                                    JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
                            if (ans == JOptionPane.YES_OPTION) { // revise or make coda

                              // Get new weight
                              String[] wtStrs = new String[]{"0", "1", "2", "3", "4"};
                              wt = (String) JOptionPane.showInputDialog(wfp.getTopLevelAncestor(),
                                      "New coda weight (0 = full)", "Coda Weight", JOptionPane.PLAIN_MESSAGE, null, wtStrs, wtStrs[0]);
                            }
                          }

                          if ( (ans == JOptionPane.YES_OPTION && wt != null) || doCodaWeight0) { // revise or make coda

                            // set default wt here instead of above so that "weight" dialog can be canceled by a user
                            if (doCodaWeight0) {
                              wt = "0";
                            }

                            if (oldCoda != null) { // reuse OLD coda
                              double start = oldCoda.getTime();
                              if (start > 0.) {
                                double tau = Math.round(clickTime - start);
                                if (tau > 0.) {
                                    //System.out.println("    "+oldCoda.getNeatStringHeader());
                                    //System.out.println("OLD:" +oldCoda.toNeatString());
                                    // CLEAR existing amp time pairs here 
                                    ArrayList aList = oldCoda.getWindowTimeAmpPairs();
                                    TimeAmp ta = null;
                                    for (int idx=0; idx < aList.size(); idx++) {
                                        ta = (TimeAmp) aList.get(idx);
                                        if (ta.getTimeValue() > tau) aList.remove(ta); // time relative secs since P-time
                                    }
                                    aList.add(new TimeAmp(tau, cnts)); // 2-sec avg abs amp counts
                                    // reset window window count, typically two values the 1st and this human picked time-amp pair
                                    oldCoda.windowCount.setValue(aList.size());
                                    oldCoda.tau.setValue(tau);
                                    oldCoda.initChannelMag();
                                    oldCoda.setPhaseDescription("HSHh");
                                    //oldCoda.setProcessingState(JasiProcessingConstants.STATE_FINAL_TAG);
                                    oldCoda.require();
                                    oldCoda.algorithm.setValue("HAND");
                                    oldCoda.quality.setValue(Coda.toQuality(Integer.parseInt(wt)));
                                    //System.out.println("NEW:" +oldCoda.toNeatString());
                                    wfp.repaint();
                                    mag.setStale(true);
                                    Jiggle.jiggle.updateMagTab("Md");
                                    return;
                                }
                              }
                            } 
                            else { // Coda NULL, make new coda
                                Phase defPhase = Phase.create();
                                defPhase.setChannelObj(wfp.wfv.getChannelObj());
                                defPhase.description.set("P", "", "");
                                defPhase.sol = sol;

                                Phase ph = (Phase) sol.getPhaseList().getSameChanType(defPhase, false); 
                                double start = (ph == null) ? -1.0 : ph.getTime();
                                if (ph == null) {
                                   JOptionPane.showMessageDialog(wfp.getTopLevelAncestor(),
                                           "Channel does not have P-pick, setting coda start to predicted travel time",
                                           "No Channel P-phase Pick",
                                           JOptionPane.PLAIN_MESSAGE
                                   );

                                   double depth = sol.getModelDepth(); // aww 2015/10/10
                                   if (Double.isNaN(depth)) depth = 0.;

                                   start = sol.getTime() + wfp.getWf().getTravelTimeModel().getTTp(wfp.wfv.getHorizontalDistance(), depth);
                                } // phase == null

                                if (start > 0.) { // save data
                                    double tau = Math.round(clickTime - start);
                                    Coda newCoda = Coda.create(); 
                                    newCoda.ampUnits.setValue(CalibrUnits.getString(CalibrUnits.UNKNOWN).substring(0,3));
                                    newCoda.setChannelObj(wfp.wfv.getChannelObj());
                                    newCoda.setTime(start);
                                    newCoda.tau.setValue(tau);
                                    //newCoda.getWindowTimeAmpPairs().add(new TimeAmp(tau, noiseAAA));
                                    newCoda.setDurationType(org.trinet.jasi.coda.CodaDurationType.D);
                                    newCoda.setPhaseDescription("HSHh");
                                    //newCoda.setProcessingState(JasiProcessingConstants.STATE_FINAL_TAG);
                                    newCoda.require();
                                    newCoda.algorithm.setValue("HAND");
                                    newCoda.windowCount.setValue(1);
                                    newCoda.quality.setValue(Coda.toQuality(Integer.parseInt(wt)));
                                    System.out.println("NEW CODA:" + newCoda.toNeatString());

                                    if (!haveMd) sol.setPrefMagOfType(mag); // its new one
                                    mag.associate(newCoda);
                                    mag.setStale(true);

                                    wfp.wfv.updateCodaList();
                                    wfp.repaint();

                                    Jiggle.jiggle.updateMagTab("Md");
                                    Jiggle.jiggle.updateStatusBarCounts();
                                    return;
                                } // new start is valid
                            } // old coda was null (make new coda)
                          } // yes, revise coda
                        } // amps for coda menu option (doCoda)
                    } // run
                } // runnable
            ); // SwingUtilities
        } // actionPerformed
    } // listener class
}
