package org.trinet.jiggle;

import java.awt.Point;
import javax.swing.JScrollPane;

/**
* Extends ZoomableWFPanel to enable the phase picking popup menu.
*/
public class PickableWFPanel extends ZoomableWFPanel{

     public PickableWFPanel(JScrollPane scrollPane, MasterView mv) {
         super(scrollPane, mv);
     }

/** Create the phase picking popup menu that is activated by a right mouse click.
Overrides the makePopup() method in ActiveWFPanel.  */
    public void makePopup (Point point) {
        if (wfv.mv.getOwner() instanceof Jiggle) {
            Jiggle jiggle = (Jiggle) wfv.mv.getOwner();
            if (jiggle.pickPanel != null) {  //  && jiggle.pickPanel.pickingEnabled) {
                new PhasePopup(this, this, point);
            }
        }
    }

}
