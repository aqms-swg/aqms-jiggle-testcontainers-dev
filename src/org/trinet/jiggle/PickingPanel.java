package org.trinet.jiggle;

/**
 * PickingPanel.java
 *
 */
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import java.text.*;
import javax.swing.border.*;
import javax.swing.event.*;

import org.trinet.jasi.*;
import org.trinet.jasi.picker.AbstractPicker;
import org.trinet.jasi.picker.PhasePickerIF;
import org.trinet.jasi.picker.PickerParmIF;
import org.trinet.util.GenericPropertyList;
import org.trinet.util.LeapSeconds;
import org.trinet.util.TimeSpan;

/**
 * A PickingPanel allows all the function of a ZoomPanel plus supports phase
 * picking.
 */
public class PickingPanel extends ZoomPanel {

    private JButton autoPickButton = null;
    //private JButton autoPickButton2 = null;
    private JButton deleteButton = null;
    private JButton pickButton = null;
    private JButton rejectButton = null;
    protected boolean pickingEnabled = true;

    protected boolean phaseDescWtCheck = true;
    protected boolean alwaysResetLowQualFm = false;

   /**
     * Constructor
     */
    public PickingPanel() { }

   /**
     * Constructor with placeholder string. This is used to create a valid
     * PickingPanel when there is no data loaded yet.  */
    public PickingPanel(String str) {
        add(new JLabel(str));
    }

    /**
     * Constructor
     */
    public PickingPanel(MasterView masterView) {
        // NOTE have to add wfViewModel listener b4 the scrollZoom zwfp  -aww
        super(masterView);
    }

    protected void setupPicker() {
        if (mv.getOwner() instanceof Jiggle) {
            Jiggle jiggle = (Jiggle) mv.getOwner(); 
            jiggle.setupPicker();
        }
    }

    protected void setupPicker(String pickerClassName, GenericPropertyList gpl) {
        if (mv.getOwner() instanceof Jiggle) {
            Jiggle jiggle = (Jiggle) mv.getOwner();
            jiggle.setupPicker(pickerClassName, gpl, true);
        }
    }

    protected PhasePickerIF getPicker() {
        if (mv.getOwner() instanceof Jiggle) {
            Jiggle jiggle = (Jiggle) mv.getOwner();
            return jiggle.picker;
        }
        return null;
    }
/**
 * Build the panel
 */
    protected void buildPanel() {

        setOpaque(true);

        setLayout( new BorderLayout() );
        JPanel subpanel = new JPanel(new BorderLayout() );

        // put scroller in center
        subpanel.add("Center", makeScrollZoom());

        // create the button panels and put at bottom
        // The staLabel has file scope because it is changed by another method
        staLabel = new JLabel("Station Info here...");
        staLabel.setBackground(Color.black);
        staLabel.setForeground(Color.blue);
        Box box = Box.createHorizontalBox();
        box.add(staLabel);
        box.add(Box.createHorizontalGlue());
        String pos = ((Jiggle)mv.getOwner()).getProperties().getProperty("pickingPanelArrowLayout","");
        cursorLocPanel.add(new JLabel("Cursor"), 0);
        box.add(cursorLocPanel);
        subpanel.add("North", box);
        //Box bottom = Box.createVerticalBox();
        //bottom.add(makeButtonBox());
        //subpanel.add("South", bottom);

        if (pos.equalsIgnoreCase("W"))
            subpanel.add("West", makeArrowPanel(BoxLayout.Y_AXIS));
        else if (pos == "" || pos.equalsIgnoreCase("E") || ! pos.equalsIgnoreCase("S")) 
            subpanel.add("East", makeArrowPanel(BoxLayout.Y_AXIS));
        JToolBar jtb = new JToolBar();
        jtb.add(makeButtonBox((pos.equalsIgnoreCase("S") ? makeArrowPanel(BoxLayout.X_AXIS) : null)));
        this.add("South", jtb);
        this.add("Center",subpanel);

        configureKeyActionBindings();

    } // end of builder

    ActionListener makeCenterButtonActionListener() {
        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
              Object obj =  mv.getOwner();
              if (obj instanceof Jiggle) {
                Jiggle jiggle = (Jiggle) obj;
                jiggle.wfScroller.centerViewportOnPanel(zwfp.wfv.getChannelObj());
              }
            }
        };
    }

    /** Override ZoomPanel.makeScrollZoom() to use a PickableWFPanel rather than a ZoomableWFPanel. */
    protected JScrollPane makeScrollZoom() {
        
        scrollZoom = makeScrollZoom1();
        zwfp = new PickableWFPanel(scrollZoom, mv);
        setupZoomableWFPanel(zwfp);

        MouseInputAdapter mia = new MouseInputAdapter() {
            public void mouseMoved(MouseEvent e) {
              Object obj =  mv.getOwner();
              if (obj instanceof Jiggle) {
                Jiggle jiggle = (Jiggle) obj;
                double dt = zwfp.dtOfPixel(e.getPoint());
                double amp = zwfp.ampOfPixel(e.getPoint());
                //WFPanel wfp = jiggle.wfScroller.groupPanel.getWFPanel(mv.masterWFViewModel.get());
                ArrayList wfpList = jiggle.wfScroller.groupPanel.wfpList.getTriaxialList(zwfp.wfv);
                WFPanel wfp = null;
                for (int idx = 0; idx < wfpList.size(); idx++) {
                  wfp = (WFPanel) wfpList.get(idx);
                  if (wfp.isShowing()) {
                      wfp.cursorTime = dt;
                      if (zwfp.wfv.equalsChannelId(wfp.wfv)) {
                          wfp.cursorAmp = amp; // amp line tracking enabled on SAME channel - aww 2011/04/12
                      }
                      wfp.repaint(); // destroys ChannelLabelViewport labeling
                  }
                }
              }
            }

            public void mouseEntered(MouseEvent e) {
                if (zwfp.getWf() != null) cursorLocPanel.setAmpFormat(zwfp.getWf().getAmpUnits(), zwfp.getWf().getMaxAmp());
            }

            public void mouseExited(MouseEvent e) {
              Object obj =  mv.getOwner();
              if (obj instanceof Jiggle) {
                Jiggle jiggle = (Jiggle) obj;
                //WFPanel wfp = jiggle.wfScroller.groupPanel.getWFPanel(mv.masterWFViewModel.get());
                ArrayList wfpList = jiggle.wfScroller.groupPanel.wfpList.getTriaxialList(zwfp.wfv);
                WFPanel wfp = null;
                for (int idx = 0; idx < wfpList.size(); idx++) {
                  wfp = (WFPanel) wfpList.get(idx);
                  if (wfp.isShowing()) {
                      wfp.cursorTime = Double.NaN;
                      wfp.cursorAmp = Double.NaN;
                      wfp.repaint(); // destroys ChannelLabelViewport labeling
                  }
                }
              }
            }
        };
        zwfp.addMouseListener(mia);
        zwfp.addMouseMotionListener(mia);

        return scrollZoom;
    }

    private void createPickingButtons(Container c) {

        autoPickButton = addButton(c, "A", "mini-autopick.png", new AutoPickButtonHandler(),
                  "Auto pick phase(s) around phase cue(s)");
        /*
        autoPickButton2 = addButton(c, "a", "mini-autopick.png", new AutoPickButtonHandler(),
                  "Auto pick phase near center time");
        autoPickButton2.setBackground(Color.black);
        */
        rejectButton = addButton(c, "R", "plus-minus-gry.gif", new RejectButtonHandler(),
                  "Toggle rejection of pick nearest to centerline time");
        deleteButton = addButton(c, "D", "delete_xglyph_red.gif", new DelButtonHandler(),
                  "Delete pick nearest to centerline time");
        c.add(Box.createHorizontalStrut(5));
        createCenterTimeLabel(c);
        c.add(Box.createHorizontalStrut(5));
        pickButton =  addButton(c, "P", "clock_green.gif", null, "Make a new pick at centerline time");
        pickButton.addActionListener(new PickButtonHandler(pickButton));

        /*
        if (mv.getOwner() instanceof Jiggle) {
            Jiggle jiggle = (Jiggle) mv.getOwner();
            if (jiggle.scopeMode) disablePicking();
            else enablePicking();
        }
        */

          /*
        if (addPadding) {
          addButton(c, new JButton(), null, null);
          addButton(c, new JButton(), null, null);
          //c.add(new Label("")); // padding to center the time label near bombsite -aww
        }
          */

        /* These buttons take up GUI real estate, plus may be accidently clicked -aww
        btn = new JButton("C");
        btn.setEnabled(false);
        addButton(c, btn, new MagReadingButtonHandler("Coda"),
                  "Scan viewport timespan bounds for coda");

        btn = new JButton("A");
        addButton(c, btn, new MagReadingButtonHandler("Amp"),
                  "Scan viewport timespan bounds for amp");
        */
    }

    protected void enablePicking() {
        if (autoPickButton != null) autoPickButton.setEnabled(true);
        //if (autoPickButton2 != null) autoPickButton2.setEnabled(true);
        if (deleteButton != null) deleteButton.setEnabled(true);
        if (pickButton != null) pickButton.setEnabled(true);
        if (rejectButton != null) rejectButton.setEnabled(true);
        pickingEnabled = true;
        //System.out.println("Picking Panel INFO : enablePicking");
    }

    protected void disablePicking() {
        if (autoPickButton != null) autoPickButton.setEnabled(false);
        //if (autoPickButton2 != null) autoPickButton2.setEnabled(false);
        if (deleteButton != null) deleteButton.setEnabled(false);
        if (pickButton != null) pickButton.setEnabled(false);
        if (rejectButton != null) rejectButton.setEnabled(false);
        
        pickingEnabled = false;
        //System.out.println("Picking Panel INFO : disablePicking");
    }

    protected Box makeButtonBox(Component c) {
      Box box = null;
      if (((Jiggle)mv.getOwner()).getProperties().getBoolean("stackPickingPanelButtons")) {
        box = Box.createVerticalBox();
        box.add(Box.createGlue());
        Box box2 = Box.createHorizontalBox();
        box2.add(Box.createHorizontalGlue());
        createPickingButtons(box2);
        box2.add(Box.createHorizontalGlue());
        box.add(box2);
        box.add(super.makeButtonBox(false));
        if (c != null) box.add(c);
        box.add(Box.createGlue());
      }
      else {
        box = Box.createHorizontalBox();
        box.add(Box.createHorizontalGlue());
        createTimeScalingButtons(box);
        box.add(Box.createHorizontalStrut(64));
        createPickingButtons(box);
        createAmpScalingButtons(box);
        box.add(createFilterButton());
        box.add(createAlignmentButton());
        box.add(createTriaxialButton()); // -aww 2010/01/24
        if ( ((Jiggle)mv.getOwner()).getProperties().getBoolean("addEHGFilterButton") ) box.add(createButtonEHG()); 
        if ( ((Jiggle)mv.getOwner()).getProperties().getProperty("badChannelListName") != null) box.add(createButtonBCL()); 
        createZoomScaleComboBox(box);
        if (c != null) box.add(c);
        box.add(Box.createHorizontalGlue());
      }
      return box;
    }


//-----------------------------------------------------------------
// Event handling
//-----------------------------------------------------------------
// Handle button and menu events
  class JumpTabAction extends AbstractAction { // implements ActionListener {
      int tab = 0;
      public JumpTabAction(int tab) {
          this.tab = tab;
      }

      public void actionPerformed(ActionEvent evt) {
          Object obj = (mv == null) ? null : mv.getOwner();
          if (obj instanceof Jiggle) {
              SwingUtilities.invokeLater(
                  new Runnable() {
                      public void run() {
                          Object obj = (mv == null) ? null : mv.getOwner();
                          if (obj instanceof Jiggle) ((Jiggle) obj).selectTab(tab);
                      }
                  }
              );
          }
 

      }
  }
  class UndoObsAction extends AbstractAction { // implements ActionListener {
      public void actionPerformed(ActionEvent evt) {

          // NOTE !!!!
          // Have to either implementting a RESET of undo buffer with id change aka clone or
          // make sure you match correct sol and mag with readings, does erase remove assocs?

          if (mv == null ) return;

          Solution sol = mv.getSelectedSolution();
          if (sol == null) return; 

          int imods = evt.getModifiers();
          // SHIFT = 1,  CTRL = 2,  ALT = 8
          //System.out.println("DEBUG PickingPanel UndoObsAction evt modifiers: " + imods);
          // Make user having to type modifiers to avoid accidental restore
          int cnt = 0;          
          int ans = JOptionPane.YES_OPTION;
          int done = 0;
          if ( imods == 0) {
              JOptionPane.showMessageDialog(zwfp.getTopLevelAncestor(),
                     "z+SHIFT restores last deleted picks, +ALT last deleted amps, +CTRL last deleted codas", "Hot-key Info",
                            JOptionPane.PLAIN_MESSAGE
              );
              return;
          }
          if ( (imods & ActionEvent.SHIFT_MASK) == ActionEvent.SHIFT_MASK ) {
              cnt = mv.phUndoList.size();
              if (cnt > 1) {
                  ans = JOptionPane.showConfirmDialog(zwfp.getTopLevelAncestor(),
                     "Restore " + cnt + " picks?", "Restore Picks",
                     JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
              } 
              if (ans == JOptionPane.YES_OPTION) done = mv.undoPhases(true);
              System.out.println("Undo Data restored picks " + done);
          }
          if ( (imods & ActionEvent.SHIFT_MASK) == ActionEvent.SHIFT_MASK ) {
              ans = JOptionPane.YES_OPTION;
              cnt = mv.amUndoList.size();
              if (cnt > 1) {
                  ans = JOptionPane.showConfirmDialog(zwfp.getTopLevelAncestor(),
                     "Restore " + cnt + " amps?", "Restore Amps",
                     JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
              } 
              if (ans == JOptionPane.YES_OPTION) done = mv.undoAmps(true);
              System.out.println("Undo Data restored amps  " + done);
          }
          if ( (imods & ActionEvent.SHIFT_MASK) == ActionEvent.SHIFT_MASK ) {
              ans = JOptionPane.YES_OPTION;
              cnt = mv.coUndoList.size();
              if (cnt > 1) {
                  ans = JOptionPane.showConfirmDialog(zwfp.getTopLevelAncestor(),
                     "Restore " + cnt + " codas?", "Restore Codas",
                     JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
              } 
              if (ans == JOptionPane.YES_OPTION) done = mv.undoCodas(true);
              System.out.println("Undo Data restored codas " + done);
          }

          Object obj = (mv == null) ? null : mv.getOwner();
          if (obj instanceof Jiggle) {
              ((Jiggle) obj).updateStatusBarCounts();
          }

      }
  }
  //
  class DeleteObsAction1 extends AbstractAction { // implements ActionListener {
      public void actionPerformed(ActionEvent evt) {

          if (mv == null ) return;

          Solution sol = mv.getSelectedSolution();
          if (sol == null) return; 

          int imods = evt.getModifiers();
          // SHIFT = 1,  CTRL = 2,  ALT = 8
          //System.out.println("DEBUG PickingPanel DeleteObsAction1 evt modifiers: " + imods);
          if ( imods == 0) {
              JOptionPane.showMessageDialog(zwfp.getTopLevelAncestor(),
                  "BS+SHIFT deletes picks, amps, codas for SELECTED channel, BS+ALT, amps, BS+CTRL, codas", "Hot-key Info",
                            JOptionPane.PLAIN_MESSAGE
              );
              return;
          }

              SolutionAssociatedListIF aList = null;
              if ( (imods & ActionEvent.SHIFT_MASK) == ActionEvent.SHIFT_MASK ) {
                  aList = zwfp.wfv.phaseList.getAssociatedWith(sol);
                  if (aList.size() > 0) {
                      mv.addToPhaseUndoList(aList, true);
                      zwfp.wfv.phaseList.eraseAll(aList);
                  }

                  aList = zwfp.wfv.ampList.getAssociatedWith(sol);
                  if (aList.size() > 0) {
                      mv.addToAmpUndoList(aList, true);
                      zwfp.wfv.ampList.eraseAll(aList);
                          Magnitude mag = sol.getPrefMagOfType("l");
                          if (mag != null) mag.getReadingList().eraseAll(aList);
                  }

                  aList = zwfp.wfv.codaList.getAssociatedWith(sol);
                  if (aList.size() > 0) {
                      mv.addToCodaUndoList(aList, true);
                      zwfp.wfv.codaList.eraseAll(aList);
                          Magnitude mag = sol.getPrefMagOfType("d");
                          if (mag != null) mag.getReadingList().eraseAll(aList);
                  }

              }
              else {
                  if ( (imods & ActionEvent.CTRL_MASK) == ActionEvent.CTRL_MASK ) { 
                      aList = zwfp.wfv.codaList.getAssociatedWith(sol);
                      if (aList.size() > 0) {
                          mv.addToCodaUndoList(aList, true);
                          zwfp.wfv.codaList.eraseAll(aList);
                              Magnitude mag = sol.getPrefMagOfType("d");
                              if (mag != null) mag.getReadingList().eraseAll(aList);
                      }

                  }
                  if ( (imods & ActionEvent.ALT_MASK) == ActionEvent.ALT_MASK ) { 
                      aList = zwfp.wfv.ampList.getAssociatedWith(sol);
                      if (aList.size() > 0) {
                          mv.addToAmpUndoList(aList, true);
                          zwfp.wfv.ampList.eraseAll(aList);
                              Magnitude mag = sol.getPrefMagOfType("l");
                              if (mag != null) mag.getReadingList().eraseAll(aList);
                      }
                  }
              }

         Object obj = (mv == null) ? null : mv.getOwner();
         if (obj instanceof Jiggle) {
              ((Jiggle) obj).updateStatusBarCounts();
         }

      }
  }
  //
  class DeleteObsAction2 extends AbstractAction { // implements ActionListener {
      public void actionPerformed(ActionEvent evt) {

          if (mv == null ) return;

          Solution sol = mv.getSelectedSolution();
          if (sol == null) return; 

          // int imods = evt.getModifiers();
          // SHIFT = 1,  CTRL = 2,  ALT = 8
          //System.out.println("DEBUG PickingPanel DeleteObsAction2 evt modifiers: " + imods);

          SolutionAssociatedListIF aList = null;
          // Type once deletes coda, twice, coda+amp, thrice, coda+amp+picks
          while (true) {
              aList = zwfp.wfv.codaList.getAssociatedWith(sol);
              if (aList.size() > 0) {
                  mv.addToCodaUndoList(aList, true);
                  zwfp.wfv.codaList.eraseAll(aList);
                      Magnitude mag = sol.getPrefMagOfType("d");
                      if (mag != null) mag.getReadingList().eraseAll(aList);
                  break;
              }
              aList = zwfp.wfv.ampList.getAssociatedWith(sol);
              if (aList.size() > 0) {
                  mv.addToAmpUndoList(aList, true);
                  zwfp.wfv.ampList.eraseAll(aList);
                      Magnitude mag = sol.getPrefMagOfType("l");
                      if (mag != null) mag.getReadingList().eraseAll(aList);
                  break;
              }
              aList = zwfp.wfv.phaseList.getAssociatedWith(sol);
              if (aList.size() > 0) {
                  mv.addToPhaseUndoList(aList, true);
                  zwfp.wfv.phaseList.eraseAll(aList);
                  break;
              }

              break;
          }

          Object obj = (mv == null) ? null : mv.getOwner();
          if (obj instanceof Jiggle) {
              ((Jiggle) obj).updateStatusBarCounts();
          }

      }
  }

  //
  class DeleteObsAction3 extends AbstractAction { // implements ActionListener {
      public void actionPerformed(ActionEvent evt) {

          if (mv == null ) return;

          Solution sol = mv.getSelectedSolution();
          if (sol == null) return; 

          // int imods = evt.getModifiers();
          // SHIFT = 1,  CTRL = 2,  ALT = 8
          //System.out.println("DEBUG PickingPanel DeleteObsAction3 evt modifiers: " + imods);

          SolutionAssociatedListIF aList = null;
          // Type once deletes coda, twice, coda+amp, thrice, coda+amp+picks
          while (true) {
              aList = zwfp.wfv.phaseList.getAssociatedWith(sol);
              if (aList.size() > 0) {
                  mv.addToPhaseUndoList(aList, true);
                  zwfp.wfv.phaseList.eraseAll(aList);
                  break;
              }
              aList = zwfp.wfv.ampList.getAssociatedWith(sol);
              if (aList.size() > 0) {
                  mv.addToAmpUndoList(aList, true);
                  zwfp.wfv.ampList.eraseAll(aList);
                      Magnitude mag = sol.getPrefMagOfType("l");
                      if (mag != null) mag.getReadingList().eraseAll(aList);
                  break;
              }
              aList = zwfp.wfv.codaList.getAssociatedWith(sol);
              if (aList.size() > 0) {
                  mv.addToCodaUndoList(aList, true);
                  zwfp.wfv.codaList.eraseAll(aList);
                      Magnitude mag = sol.getPrefMagOfType("d");
                      if (mag != null) mag.getReadingList().eraseAll(aList);
                  break;
              }
              break;
          }

          Object obj = (mv == null) ? null : mv.getOwner();
          if (obj instanceof Jiggle) {
              ((Jiggle) obj).updateStatusBarCounts();
          }

      }
  }
  //
  class DeleteObsAction4 extends AbstractAction { // implements ActionListener {
      public void actionPerformed(ActionEvent evt) {

          if (mv == null ) return;

          Solution sol = mv.getSelectedSolution();
          if (sol == null) return; 

          int imods = evt.getModifiers();
          // SHIFT = 1,  CTRL = 2,  ALT = 8
          //System.out.println("DEBUG PickingPanel DeleteObsAction4 evt modifiers: " + imods);
          if ( imods == 0) {
              JOptionPane.showMessageDialog(zwfp.getTopLevelAncestor(),
                     "F9+SHIFT deletes ALL arrivals for EVENT, F9+ALT, ALL amps, F9+CTRL, ALL codas", "Hot-key Info",
                            JOptionPane.PLAIN_MESSAGE
              );
              return;
          }

          // Type once deletes coda, twice, coda+amp, thrice, coda+amp+picks
          boolean tf = false;

          if ( (imods & ActionEvent.SHIFT_MASK) > 0) {
              if (sol.getPhaseList().size() > 0) {
                  mv.addToPhaseUndoList(sol.getPhaseList(), true);
                  int cnt = sol.getPhaseList().eraseAll();
                  System.out.println("INFO: count of arrs erased: " + cnt);
                  tf = true;
              }
          }

          //
          // Note need to discriminate removal of all ML versus ME amp readings !!!!
          // MasterView only loads ML amps.  We might have spectral or other amps loaded in solution ?
          // Need to use: sol.ampList.eraseEquivalent(mag.getReadingList()), when ME (or other amp mag)
          // is included in the solution's amplist in masterview
          //
          if ( (imods & ActionEvent.ALT_MASK) > 0 || (imods & ActionEvent.SHIFT_MASK) > 0) {
              //
              if (sol.getAmpList().size() > 0) {
                  mv.addToAmpUndoList(sol.getAmpList(), true);
                  int cnt = sol.getAmpList().eraseAll();
                  System.out.println("INFO: count of amps erased: " + cnt);
                      Magnitude mag = sol.getPrefMagOfType("l");
                      if (mag != null) mag.getReadingList().eraseAll();
                  tf = true;
              }
              //
              
              /*
                Magnitude mag = sol.getPrefMagOfType("l");
                if (mag != null) {
                    if (mag.getReadingList().size() > 0 ) {
                        mv.addToAmpUndoList(mag.getReadingList(), true);
                        cnt = sol.ampList.eraseEquivalent(mag.getReadingList());
                        mag.getReadingList().eraseAll();
                        System.out.println("INFO: count of amps erased: " + cnt);
                    }
                }
              */

          }
          if ( (imods & ActionEvent.CTRL_MASK) > 0 || (imods & ActionEvent.SHIFT_MASK) > 0) {
              if (sol.getCodaList().size() > 0) {
                  mv.addToCodaUndoList(sol.getCodaList(), true);
                  int cnt = sol.getCodaList().eraseAll();
                  System.out.println("INFO: count of codas erased: " + cnt);
                      Magnitude mag = sol.getPrefMagOfType("d");
                      if (mag != null) mag.getReadingList().eraseAll();
                  tf = true;
              }
          }

          if (!tf) return;

          Object obj = (mv == null) ? null : mv.getOwner();
          if (obj instanceof Jiggle) {
              ((Jiggle) obj).updateStatusBarCounts();
          }

      }
  }
  //
  class SelectViewAction extends AbstractAction { // implements ActionListener {
      public void actionPerformed(ActionEvent evt) {

          int imods = evt.getModifiers();
          // SHIFT = 1,  CTRL = 2,  ALT = 8
          //System.out.println("DEBUG SelectViewAction evt modifiers: " + imods);

          if (imods == 0) {
              JOptionPane.showMessageDialog(zwfp.getTopLevelAncestor(),
                  "F7+SHIFT Toggles the top zoom panel's 'view selection' flag\n" +
                  "F7+ALT, 'Select' all unhidden views in scroller (sets views flags)\n" +
                  "F7+CTRL 'Unselect' all unhidden views in scroller (unsets views flags)",
                  "Hot-key Info",
                  JOptionPane.PLAIN_MESSAGE
              );
              return;
          }

          Object obj = (mv == null) ? null : mv.getOwner();
          if (! (obj instanceof Jiggle)) return;
          Jiggle jiggle = (Jiggle) mv.getOwner(); 

          WFPanel wfp = jiggle.wfScroller.groupPanel.getWFPanel(zwfp.wfv);

          if (imods == ActionEvent.SHIFT_MASK) {
              wfp.wfv.setSelected(!wfp.wfv.isSelected());
              wfp.setRowHeaderBackground();
          }
          else { 
              boolean isSelected = (imods == ActionEvent.ALT_MASK);
              if (isSelected && (imods & ActionEvent.CTRL_MASK) > 0) return; // undefined, 2-key press
              if (jiggle.wfScroller != null && jiggle.wfScroller.groupPanel != null) {
                  WFPanelList wfpList = jiggle.wfScroller.groupPanel.wfpList;
                  int cnt = wfpList.size(); 
                  wfp = null;
                  for (int idx = 0; idx < cnt; idx++) {
                      wfp = (WFPanel) wfpList.get(idx);
                      // Set view selection state for only those in the WFScroller group panel with "active" WFView's ?
                      if (wfp.wfv != null && wfp.wfv.isActive) {
                          wfp.wfv.setSelected(isSelected);
                          wfp.setRowHeaderBackground();
                      }
                  }
              }
          }
      }
  }
  //
  class ActiveViewAction extends AbstractAction { // implements ActionListener {
      public void actionPerformed(ActionEvent evt) {

          int imods = evt.getModifiers();
          // SHIFT = 1,  CTRL = 2,  ALT = 8
          //System.out.println("DEBUG ActiveViewAction evt modifiers: " + imods);

          if (imods == 0) {
              JOptionPane.showMessageDialog(zwfp.getTopLevelAncestor(),
                  "F6+SHIFT Toggles hiding of the selected channel's panel in lower scroller panel\n"+
                  "NOTE: If you select a new channel, the hidden panel will stay hidden, you must\n"+  
                  "      select the scroller's hide menu's 'Refresh...' item to unhide panel",
                  "Hot-key Info",
                  JOptionPane.PLAIN_MESSAGE
              );
              return;
          }

          Object obj = (mv == null) ? null : mv.getOwner();
          if (! (obj instanceof Jiggle)) return;
          Jiggle jiggle = (Jiggle) obj;

          WFPanel wfp = jiggle.wfScroller.groupPanel.getWFPanel(zwfp.wfv);
          if (imods == ActionEvent.SHIFT_MASK) {
              wfp.wfv.isActive = !wfp.wfv.isActive;
              wfp.setVisible(!wfp.isVisible());
              jiggle.wfScroller.hiddenReadings=true;
          }
      }
  }
  //
  class ClipWfAction extends AbstractAction { // implements ActionListener {
      public void actionPerformed(ActionEvent evt) {

          int imods = evt.getModifiers();
          // SHIFT = 1,  CTRL = 2,  ALT = 8
          //System.out.println("DEBUG ClipWfAction evt modifiers: " + imods);

          AbstractWaveform wf =  zwfp.wfv.getWaveform();
          if (wf == null) return;

          if (imods == 0) {
              JOptionPane.showMessageDialog(zwfp.getTopLevelAncestor(),
                  "F8+SHIFT Toggles waveform clipped flag", "Hot-key Info",
                  JOptionPane.PLAIN_MESSAGE
              );
              return;
          }

          else if (imods == ActionEvent.SHIFT_MASK) {

              wf.setClipped(!wf.isClipped());

              Object obj = (mv == null) ? null : mv.getOwner();
              if (! (obj instanceof Jiggle)) return;
              WFPanel wfp = ((Jiggle) obj).wfScroller.groupPanel.getWFPanel(zwfp.wfv);
              wfp.setRowHeaderBackground();
          }
      }
  }

  class DelButtonHandler extends AbstractAction { // implements ActionListener {
    public void actionPerformed(ActionEvent evt) {

        if (mv == null) return;

        double pickTime = mv.masterWFWindowModel.getTimeSpan().getCenter();

        //
        // Get evt mods and do ALT for amps CTRL for codas and SHIFT for all readings logic like done for BACKSPACE ?
          int imods = evt.getModifiers();
          // SHIFT = 1,  CTRL = 2,  ALT = 8
          //System.out.println("DEBUG DelButtonHandler evt modifiers: " + imods);

        //
        // Had some sort of scope problem with mv from the PickingPanel.
        // It was NOT THE SAME as the current one used by other components!
        //  PickingPanel.mv != zwfp.wfv.mv
        // The PhaseList in the MasterView is needed for the PhaseList MVC
        // interaction to work.

        MasterView mv = zwfp.wfv.mv;

        if ( imods == 0  || (imods & ActionEvent.MOUSE_EVENT_MASK) > 0 ) {
            //Phase phase = zwfp.wfv.phaseList.getNearestPhase(pickTime, mv.getSelectedSolution());
            Phase phase = (Phase)zwfp.wfv.phaseList.getNearestToTime(pickTime, mv.getSelectedSolution()); //aww
            if (phase == null) return;

            mv.addToPhaseUndoList(phase, true);
            // Delete it in Solution's list so listeners know
            //mv.getSelectedSolution().deletePhase(phase);
            mv.getSelectedSolution().erase(phase);

        }
        else if ( (imods & ActionEvent.SHIFT_MASK) > 0) { // erase all picks P and S for channel
            Solution sol = mv.getSelectedSolution();
            if (sol != null) {
                java.util.List aList = zwfp.wfv.phaseList.getAssociatedWith(sol);
                mv.addToPhaseUndoList(aList, true);
                sol.getPhaseList().eraseAll(aList);
            }
        }
        else if ( (imods & ActionEvent.CTRL_MASK) > 0) { // erase all picks for event
            Solution sol = mv.getSelectedSolution();
            if (sol != null) {
                java.util.List aList = sol.getPhaseList();
                mv.addToPhaseUndoList(aList, true);
                int cnt = sol.getPhaseList().eraseAll();
                System.out.println("INFO: count of arrs erased: " + cnt);
            }
        }
        else if ( (imods & ActionEvent.ALT_MASK) > 0) { // erase all automatic picks for event
            Solution sol = mv.getSelectedSolution();
            if (sol != null) {
                java.util.List aList = sol.getPhaseList().getAutomaticReadings();
                mv.addToPhaseUndoList(aList, true);
                int cnt = sol.getPhaseList().eraseAll(aList);
                System.out.println("INFO: count of automatic arrs erased: " + cnt);
            }
        }

        Object obj = (mv == null) ? null : mv.getOwner();
        if (obj instanceof Jiggle) {
            ((Jiggle) obj).updateStatusBarCounts();
        }

    }
  }

  class FilterButtonHandler extends AbstractAction {
    public void actionPerformed(ActionEvent evt) {
        toggleFilterOnOff();
    }
  }

  class FlipButtonHandler extends AbstractAction {
    public void actionPerformed(ActionEvent evt) {
        double pickTime = mv.masterWFWindowModel.getTimeSpan().getCenter();
        MasterView mv = zwfp.wfv.mv;
        Phase phase = (Phase)zwfp.wfv.phaseList.getNearestToTime(pickTime, mv.getSelectedSolution()); 
        if (phase == null) return;
        phase.description.flipPolarity();
        int idx = zwfp.wfv.phaseList.indexOf(phase);
        zwfp.wfv.phaseList.fireListElementStateChanged("PHASE_DESCR", idx, idx);
    }
  }

  class ScaleButtonHandler extends AbstractAction {
    public void actionPerformed(ActionEvent evt) {
        zwfp.showTimeScale = !zwfp.showTimeScale;
        zwfp.repaint();
    }
  }

  class FlagButtonHandler extends AbstractAction {

    String desc = null;

    public FlagButtonHandler(String desc) { 
        super(desc);
        this.desc = desc;
    }
    
    public void actionPerformed(ActionEvent evt) {
        if (desc.equals("FLAGS")) zwfp.showPickFlags = !zwfp.showPickFlags;
        else {
            zwfp.showResiduals = !zwfp.showResiduals; 
            zwfp.showDeltaTimes = !zwfp.showDeltaTimes; 
        }
        zwfp.repaint();
    }
  }

  private class PhaseWeightAction extends AbstractAction {

        String desc = null;
        int wt =  0;

        public PhaseWeightAction(String desc) {
            super(desc);
            this.desc = desc;
            wt = Integer.parseInt(desc);
            setEnabled(true);
        }

        public void actionPerformed(ActionEvent evt) {
            double pickTime = mv.masterWFWindowModel.getTimeSpan().getCenter();
            MasterView mv = zwfp.wfv.mv;
            Phase phase = (Phase)zwfp.wfv.phaseList.getNearestToTime(pickTime, mv.getSelectedSolution()); 
            if (phase == null) return;
            phase.description.setWeight(wt); // does not do the fm quality cutoff test
            phase.qualityToDeltaTime();  // resets deltim
            //
            mv.getSelectedSolution().setStale(true); // weight used in solution may have changed - aww 2011/05/12
            //
            if (PhaseDescription.toQuality(wt) < PhaseDescription.getFmQualCut() && !phase.description.fm.equals(PhaseDescription.DEFAULT_FM)) {
                boolean tf = false;
                if (phaseDescWtCheck) { 
                  int ans = JOptionPane.showConfirmDialog(zwfp.getTopLevelAncestor(),
                     "Weight below quality for first motion, remove first motion?", "Quality Check",
                     JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
                     tf = (ans == JOptionPane.YES_OPTION);
                }
                else tf = alwaysResetLowQualFm;
                if (tf) {
                    phase.description.fm = PhaseDescription.DEFAULT_FM;
                }
            }
            //
            int idx = zwfp.wfv.phaseList.indexOf(phase);
            zwfp.wfv.phaseList.fireListElementStateChanged("PHASE_DESCR", idx, idx);
        }
  }

  private class PhaseOnsetAction extends AbstractAction {
        String desc = null;
        public PhaseOnsetAction(String desc) {
            super(desc);
            setEnabled(true);
            this.desc = desc;
        }
        public void actionPerformed(ActionEvent evt) {
            double pickTime = mv.masterWFWindowModel.getTimeSpan().getCenter();
            MasterView mv = zwfp.wfv.mv;
            Phase phase = (Phase)zwfp.wfv.phaseList.getNearestToTime(pickTime, mv.getSelectedSolution()); 
            if (phase == null) return;
            phase.description.ei = desc;
            int idx = zwfp.wfv.phaseList.indexOf(phase);
            zwfp.wfv.phaseList.fireListElementStateChanged("PHASE_DESCR", idx, idx);
        }
  }
  private class ProcessingStateAction extends AbstractAction {
        String desc = null;
        public ProcessingStateAction(String desc) {
            super(desc);
            setEnabled(true);
            this.desc = desc;
        }
        public void actionPerformed(ActionEvent evt) {
            double pickTime = mv.masterWFWindowModel.getTimeSpan().getCenter();
            MasterView mv = zwfp.wfv.mv;
            Phase phase = (Phase)zwfp.wfv.phaseList.getNearestToTime(pickTime, mv.getSelectedSolution()); 
            if (phase == null) return;
            if (phase.isAuto()) {
                phase.setProcessingState( JasiProcessingConstants.STATE_HUMAN_TAG );
                int idx = zwfp.wfv.phaseList.indexOf(phase);
                zwfp.wfv.phaseList.fireListElementStateChanged("PROC_STATE", idx, idx);
            }
        }
  }

  private class FirstMotionAction extends AbstractAction {
        String desc = null;
        public FirstMotionAction(String desc) {
            super(desc);
            setEnabled(true);
            this.desc = desc;
        }
        public void actionPerformed(ActionEvent evt) {
            double pickTime = mv.masterWFWindowModel.getTimeSpan().getCenter();
            MasterView mv = zwfp.wfv.mv;
            Phase phase = (Phase)zwfp.wfv.phaseList.getNearestToTime(pickTime, mv.getSelectedSolution()); 
            if (phase == null) return;
            phase.description.fm = desc;
            //
            if (!PhaseDescription.getFmOnS() && phase.description.isS() && !desc.equals(PhaseDescription.DEFAULT_FM)) {
                boolean tf = true;
                //if (phaseDescWtCheck) { 
                //  int ans = JOptionPane.showConfirmDialog(zwfp.getTopLevelAncestor(),
                //     "First motion on S, remove first motion?", "Quality Check", JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
                //  tf = (ans == JOptionPane.YES_OPTION);
                //}
                //else tf = alwaysResetLowQualFm;
                if (tf) {
                    phase.description.fm = PhaseDescription.DEFAULT_FM;
                }
            }
            //
            if (zwfp.wfv.getChannelObj().isHorizontal()) {  // remove if fm not allowed on horiz 
               if (!PhaseDescription.getFmOnHoriz() && !phase.description.fm.equals(PhaseDescription.DEFAULT_FM)) {
                  boolean tf = true;
                  //if (phaseDescWtCheck) { 
                  //  int ans = JOptionPane.showConfirmDialog(zwfp.getTopLevelAncestor(),
                  //     "First motion on horizontal, remove first motion?", "Quality Check",
                  //     JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
                  //  tf = (ans == JOptionPane.YES_OPTION);
                  //}
                  //else tf = alwaysResetLowQualFm;
                  if (tf) {
                    phase.description.fm = PhaseDescription.DEFAULT_FM;
                  }
               }
            }
            //
            if (phase.getQuality() < PhaseDescription.getFmQualCut() && !phase.description.fm.equals(PhaseDescription.DEFAULT_FM)) {
                boolean tf = true;
                if (phaseDescWtCheck) { 
                  int ans = JOptionPane.showConfirmDialog(zwfp.getTopLevelAncestor(),
                     "Weight below quality for first motion, remove first motion?", "Quality Check",
                     JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE);
                  tf = (ans == JOptionPane.YES_OPTION);
                }
                else tf = alwaysResetLowQualFm;
                if (tf) {
                    phase.description.fm = PhaseDescription.DEFAULT_FM;
                }
            }
            //
            //
            int idx = zwfp.wfv.phaseList.indexOf(phase);
            zwfp.wfv.phaseList.fireListElementStateChanged("PHASE_DESCR", idx, idx);
        }
  }

  private class PickKeyAction extends AbstractAction {
        String desc = null;
        public PickKeyAction(String desc) {
            super(desc);
            setEnabled(true);
            this.desc = desc;
        }
        public void actionPerformed(ActionEvent evt) {
            if (zoomScaleComboBox != null && zoomScaleComboBox.getEditor().getEditorComponent().hasFocus()) return;
            makePick(desc); // short name?
        }
  }

  class RejectButtonHandler extends AbstractAction { // implements ActionListener { // aww added 11/09/2006
    public void actionPerformed(ActionEvent evt) {
        double pickTime = mv.masterWFWindowModel.getTimeSpan().getCenter();
        MasterView mv = zwfp.wfv.mv;
        Phase phase = (Phase)zwfp.wfv.phaseList.getNearestToTime(pickTime, mv.getSelectedSolution()); 
        if (phase == null) return;
        phase.setReject(!phase.isReject());
        int idx = zwfp.wfv.phaseList.indexOf(phase);
        //
        mv.getSelectedSolution().setStale(true); // weight used in solution may have changed - aww 2011/05/12
        //
        zwfp.wfv.phaseList.fireListElementStateChanged("PHASE_DESCR", idx, idx);
    }
  }

  class AutoPickButtonHandler extends AbstractAction { // added -aww 2010/09/27 
    public void actionPerformed(ActionEvent evt) {

        int option = (evt.getActionCommand().equals("A")) ? 1 : 2;

        if (getPicker() == null) setupPicker();

        final PhasePickerIF picker = getPicker();

        if (picker == null) {
            JOptionPane.showMessageDialog(
                (Component)mv.getOwner(), "Picker is null, check properties", "Auto Phase Picker", JOptionPane.PLAIN_MESSAGE
            );
            return;
        }

        // Lazy 1st time configuration of picker's station channel data initialize once unless changed
        if (! picker.hasChannelParms()) { // NOTE: make sure to set chanParmsMap null after changing picker's parms file data.
            ((Jiggle)mv.getOwner()).loadPickerChannelParms();
            /*
            final org.trinet.util.SwingWorker worker = new org.trinet.util.SwingWorker() {
                boolean status = false;
                WorkerStatusDialog workStatus = new WorkerStatusDialog((Frame)mv.getOwner(), true);
                public Object construct() {
                    workStatus.setBeep(1);
                    workStatus.pop("Auto Picker", "Loading channel picking parameters...", true);
                    status = picker.loadChannelParms();
                    return null;
                }
                //Runs on the event-dispatching thread graphics calls OK.
                public void finished() {
                    setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR)); // set back to normal
                    workStatus.dispose();

                    if (!status) {
                        System.err.println("ERROR: Picking Panel unable to load station parameters for picker: " + picker.getName() );
                        JOptionPane.showMessageDialog(
                            (Component)mv.getOwner(),
                            "Error loading station parameters for " + picker.getName(),
                            "Auto Phase Picker",
                            JOptionPane.PLAIN_MESSAGE
                        );
                    }
                }
            };
            worker.start();  //required for SwingWorker 3
            */
        }

        Waveform wf = zwfp.getRawWf();
       	// should be "raw" counts, not other units, nor filtered
        if (wf == null) return;

        if (!wf.hasTimeSeries()) return;

        Solution sol = mv.getSelectedSolution();
        if (sol == null) return;

        String dflt = wf.getChannelObj().isVertical() ? "P" : "S";
        int choice = JOptionPane.showOptionDialog((Component)mv.getOwner(), "Choose phase(s) to be picked", "AutoPick Phases",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
                    null, new String [] {"P", "S", "P&S", "DEFAULT", "EDIT", "CANCEL"}, dflt);

        int pickFlag = -1;
        if (choice == 0) {
            pickFlag = PhasePickerIF.P_ONLY;
        }
        else if (choice == 1) {
            pickFlag = PhasePickerIF.S_ONLY;
        }
        else if (choice == 2) {
            pickFlag = PhasePickerIF.P_AND_S;
        }
        else if (choice == 3) {
            pickFlag = PhasePickerIF.DEFAULT;
        }
        else if (choice == 4) {
            // Editor, popup dialog, do SwingUtilities invoke Later?
            choice = JOptionPane.showOptionDialog((Component)mv.getOwner(), "Only this channel's or the picker's default settings?", "Edit Parms",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE,
                    null, new String [] {"Channel", "Default", "Cancel"}, "Channel");
            if (choice < 0 || choice > 1) return;

            JComponent jc = picker.getChannelParmEditor((choice == 0) ? wf : (Channelable) null ); 
            if (jc == null) {
                JOptionPane.showMessageDialog((Component)mv.getOwner(),
                        "No picker parm editor available", "Edit Picker Channel Parms", JOptionPane.ERROR_MESSAGE);
                return;
            }

            choice = JOptionPane.showOptionDialog((Component)mv.getOwner(), jc, "Edit Picker Channel Parms",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE,
                    null, new String [] {"Close"}, "Close");
            return;
        }
        else return;

        // Ok to do it
        //System.out.printf("DEBUG PickingPanel dialog choice: %d = %d%n" choice, pickFlag); 
        // since human invoked, override velOnly setting, pick the ACC low-gains as well, ignore distance filtering
        //zwfp.wfv.synchDistAz(wf); // synch up range and azimuth with view?

        PhaseList phList  = null;
        if (option == 1) phList = picker.pick(wf, sol, pickFlag, false, false);
        else if (option == 2) {
            PickerParmIF parm = picker.getChannelParm(wf);
            if (parm != null) {
                double cTime = zwfp.wfv.mv.masterWFWindowModel.getCenterTime();
                org.trinet.util.TimeSpan pSpan = new org.trinet.util.TimeSpan(cTime-0.5, cTime+0.5);
                AbstractPicker ap = (AbstractPicker) picker;
                TimeSpan scanSpan = new TimeSpan(pSpan.getStart()-ap.getScanStartOffset(), pSpan.getEnd()+ap.getScanEndOffset());
                phList = picker.pick(wf, pickFlag, parm, scanSpan, pSpan, pSpan);
            }
            else {
                JOptionPane.showMessageDialog(
                        (Component)mv.getOwner(), "No picker parms for this channel", "Auto Phase Picker", JOptionPane.PLAIN_MESSAGE
                );
            }
        }

        if (phList == null || phList.size() == 0) {
            JOptionPane.showMessageDialog(
                (Component)mv.getOwner(), "No phase picks", "Auto Phase Picker", JOptionPane.PLAIN_MESSAGE
            );
            return;
        }

        //NOTE: Need to check for existing phases on other channels of same station, e.g. if HN and like HH pick exists, prompt for confirmation
        // only replace AUTO picks, confirm to replace HUMAN picks
        Phase newph = null;
        for (int idx=0; idx<phList.size(); idx++) {
            newph = (Phase)phList.get(idx);
            zwfp.wfv.mv.addReadingToCurrentSolution(newph, (idx==0));
            if (idx == 0) zwfp.wfv.mv.masterWFWindowModel.setCenterTime(newph.getTime());
        }
        //USE in WFScroller menu    ? sol = mv.getSelectedSolution(); if (sol != null) sol.getPhaseList().addOrReplaceAllOfSameStaType(phList, true); 
        //USE in ActiveWFPanel menu ? sol = mv.getSelectedSolution(); if (sol != null) sol.getPhaseList().addOrReplaceAllOfSameStaType(phList, true); 
        // What about picks on HN versus HH for same station
        // PhaseList oldPhList = (PhaseList) sol.getPhaseList().getAllOfSameStaType(jr); 
        // boolean doit = true;
        // for(int idx=0; idx<oldPhList.size(); idx++) // loop over all phases in oldPhList
        //   if (newPh.isLowGain() && oldPh.isHighGain) || (newPh.isAuto() && !oldPh.isAuto) ) continue; // don't remove old phase
        //   sol.getPhaseList().remove(oldPh);
        //   oldPhList.remove(oldPh);
        // }
        // If newPh is HN and ANY oldPh is HH or newPh is AUTO and ANY old is not, don't replace it, skip over add
        // oldList should be empty to add newPh:
        // if (oldPhList.size() == 0) sol.getPhaseList().add(newPh); // add ok, no human/final
        // DO NOT DO THIS: if (doit) sol.getPhaseList().addOrReplaceAllOfSameStaType(newPh, true); 

        if (zwfp.wfv.mv.getOwner() instanceof Jiggle) {
            Jiggle jiggle = (Jiggle) mv.getOwner(); 
            jiggle.updateStatusBarCounts();
        }

        zwfp.repaint();
    }
  }

  class PickButtonHandler extends AbstractAction { // implements ActionListener {
      Component c = null;

      PickButtonHandler(Component c)  {
          this.c = c;
      }

      public void actionPerformed(ActionEvent evt) {

          /*
        Solution sol = mv.getSelectedSolution();
        if (sol == null) return;
        java.util.List aList = zwfp.wfv.phaseList.getAssociatedWith(sol);
        // Look for P or S phase to delete/restore with VK_P, VK_S with mods
          */

        //double pickTime = mv.masterWFWindowModel.getTimeSpan().getCenter();
        // Had some sort of scope problem with mv from the PickingPanel.
        // It was NOT THE SAME as the current one used by other components!
        //  PickingPanel.mv != zwfp.wfv.mv
        // The PhaseList in the MasterView is needed for the PhaseList MVC
        // interaction to work.
        //MasterView mv = zwfp.wfv.mv;
        //Point xy = new Point(0,0);

        Point xy = new Point(c.getPreferredSize().width/2, 0);
        
        new PhasePopup(c, zwfp, xy);

    }


  }

  /*
  class MagReadingButtonHandler implements ActionListener { // aww added 11/22/2006

    String type = null;

    public MagReadingButtonHandler(String type) {
        this.type = type;
    }

    public void actionPerformed(ActionEvent evt) {
        zwfp.createMagReading(type, zwfp.wfv.mv.masterWFWindowModel.getTimeSpan());
    }

  }
  */

    // Figure the picktime of the bombsight, triggered by hot keys 0,1,2,3 etc
    private void makePick(String str) {
        double pickTime = zwfp.wfv.mv.masterWFWindowModel.getTimeSpan().getCenter();
        Solution sol = zwfp.wfv.mv.getSelectedSolution();
        Phase newph = zwfp.describePickAt(pickTime, sol);  // guesses at fm

        // parse the string in the menu item (note fm is already set above)
        newph.description.parseShortString(str, zwfp.wfv.getChannelObj());

        // jiggle property firstMoQualityMin set the static description member
        // fmQualCut tested to reset the existing fm if quality < cutoff quality 
        newph.description.setQuality(newph.description.getQuality(), true);
        newph.qualityToDeltaTime();  // resets deltim, does not change fm

        zwfp.wfv.mv.addReadingToCurrentSolution(newph);

        if (zwfp.wfv.mv.getOwner() instanceof Jiggle) {
            Jiggle jiggle = (Jiggle) mv.getOwner(); 
            jiggle.updateStatusBarCounts();
        }

    }

    private JMenuItem getMenuBarActionItem(JMenu menu, String actionCmd) {
       int itemCount = menu.getItemCount();
       if (itemCount <= 0) return null; 
       JMenuItem subMenuItem = null;
       for (int j = 0; j < itemCount; j++) {
           subMenuItem = menu.getItem(j);
           if (subMenuItem == null) continue; // possible
           if (subMenuItem.getActionCommand().equals(actionCmd) ) {
             break;
           }
           if (subMenuItem instanceof JMenu) {
             subMenuItem = getMenuBarActionItem((JMenu)subMenuItem, actionCmd);
             if (subMenuItem != null) {
               break;
             }
           }
       }
       return subMenuItem;
    }

    private class ZoomFilterAction extends AbstractAction {

        String desc = null;
        public ZoomFilterAction(String desc) {
            super(desc);
            this.desc = desc;
            setEnabled(true);
        }

        public void actionPerformed(ActionEvent evt) {
            JMenu jmenu = (JMenu) filterMenu.getComponent(2);
            JMenuItem jmi = getMenuBarActionItem(jmenu, desc);
            if (jmi != null) jmi.doClick();
        }
    }


    private void configureKeyActionBindings() {

        ActionMap actionMap = new ActionMap();

        // use map of zwfp to avoid makePick action when typing 0-4 in timescale combobox  -aww
        actionMap.setParent(zwfp.getActionMap());
        actionMap.put("iP0", new PickKeyAction("iP0"));
        actionMap.put("iP1", new PickKeyAction("iP1"));
        actionMap.put("eP2", new PickKeyAction("eP2"));
        actionMap.put("eP3", new PickKeyAction("eP3"));
        actionMap.put("eP4", new PickKeyAction("eP4"));

        actionMap.put("iS0", new PickKeyAction("iS0"));
        actionMap.put("iS1", new PickKeyAction("iS1"));
        actionMap.put("eS2", new PickKeyAction("eS2"));
        actionMap.put("eS3", new PickKeyAction("eS3"));
        actionMap.put("eS4", new PickKeyAction("eS4"));

        actionMap.put("UP", new ButtonAction(ButtonAction.UP));
        actionMap.put("DOWN", new ButtonAction(ButtonAction.DOWN));
        actionMap.put("PAGEUP", new ButtonAction(ButtonAction.PAGEUP));
        actionMap.put("PAGEDOWN", new ButtonAction(ButtonAction.PAGEDOWN));
        actionMap.put("TOP", new ButtonAction(ButtonAction.TOP));
        actionMap.put("BOTTOM", new ButtonAction(ButtonAction.BOTTOM));

        actionMap.put("DELETE", new DelButtonHandler());
        actionMap.put("REJECT", new RejectButtonHandler());

        actionMap.put("PICK", new PickButtonHandler(timeLabel));

        actionMap.put("FLAGS", new FlagButtonHandler("FLAGS"));
        actionMap.put("RESID", new FlagButtonHandler("RESID"));

        actionMap.put("SCALE", new ScaleButtonHandler());

        actionMap.put("FLIP", new FlipButtonHandler());

        actionMap.put("ONOFF", new FilterButtonHandler());

        actionMap.put("TIME+", new TimeScalingAction("<>"));
        actionMap.put("TIME-", new TimeScalingAction("><"));
        actionMap.put("TIME0", new TimeScalingAction("=="));
        actionMap.put("FULL", new TimeScalingAction("[]"));

        actionMap.put("AMP0", new AmpScalingAction("o"));
        actionMap.put("AMP+", new AmpScalingAction("+"));
        actionMap.put("AMP-", new AmpScalingAction("-"));

        actionMap.put("FMC", new FirstMotionAction("c."));
        actionMap.put("FMD", new FirstMotionAction("d."));
        actionMap.put("FM+", new FirstMotionAction("+."));
        actionMap.put("FM-", new FirstMotionAction("-."));
        actionMap.put("FM.", new FirstMotionAction("..")); // remove motion, a space in graphic string

        actionMap.put("PHI", new PhaseOnsetAction("i"));
        actionMap.put("PHE", new PhaseOnsetAction("e"));
        //actionMap.put("PHW", new PhaseOnsetAction("w")); // do we need this one? NOTE use "w" for WOOD-ANDERSON filtering

        actionMap.put("WT0", new PhaseWeightAction("0"));
        actionMap.put("WT1", new PhaseWeightAction("1"));
        actionMap.put("WT2", new PhaseWeightAction("2"));
        actionMap.put("WT3", new PhaseWeightAction("3"));
        actionMap.put("WT4", new PhaseWeightAction("4"));

        actionMap.put("PS", new ProcessingStateAction("H")); // set state to H

        actionMap.put("F-HP", new ZoomFilterAction("HIGHPASS"));
        actionMap.put("F-BP", new ZoomFilterAction("BANDPASS"));
        actionMap.put("F-LP", new ZoomFilterAction("LOWPASS"));
        actionMap.put("F-WA", new ZoomFilterAction("WOOD-ANDERSON"));
        actionMap.put("F-WA_BP", new ZoomFilterAction("WOOD-ANDERSON_BP"));
        actionMap.put("F-WA_HP", new ZoomFilterAction("WOOD-ANDERSON_HP"));
        actionMap.put("F-WA_EH", new ZoomFilterAction("WOOD-ANDERSON_EH"));

        actionMap.put("ACTIVEV", new ActiveViewAction());
        actionMap.put("SELECTV", new SelectViewAction());
        actionMap.put("CLIPV", new ClipWfAction());

        actionMap.put("UNDO", new UndoObsAction());

        actionMap.put("DELOBS1", new DeleteObsAction1());
        actionMap.put("DELOBS2", new DeleteObsAction2());
        actionMap.put("DELOBS3", new DeleteObsAction3());
        actionMap.put("DELOBS4", new DeleteObsAction4());

        actionMap.put("TABLOC", new JumpTabAction(Jiggle.TAB_LOCATION));
        actionMap.put("TABMAG", new JumpTabAction(Jiggle.TAB_MAGNITUDE));
        actionMap.put("TABCAT", new JumpTabAction(Jiggle.TAB_CATALOG));
        actionMap.put("TABMSG", new JumpTabAction(Jiggle.TAB_MESSAGE));

        // add map to zwfp to avoid makePick action when typing 0-4 in timescale combobox  -aww
        zwfp.setActionMap(actionMap);

        InputMap inputMap = new InputMap();
        // use map of zwfp to avoid makePick action when typing 0-4 in timescale combobox  -aww
        inputMap.setParent(zwfp.getInputMap());

        //KeyStroke.getKeyStroke(KeyEvent.VK_C, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask());
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0), "WT1");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F2, 0), "WT2");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F3, 0), "WT3");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F4, 0), "WT4");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0), "WT0");

        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0), "ACTIVEV");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F6, Event.SHIFT_MASK), "ACTIVEV");

        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F7, 0), "SELECTV");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F7, Event.SHIFT_MASK), "SELECTV");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F7, Event.ALT_MASK), "SELECTV");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F7, Event.CTRL_MASK), "SELECTV");

        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0), "CLIPV");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F8, Event.SHIFT_MASK), "CLIPV");

        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F9, 0), "DELOBS4"); // info about deleting all picks,amps, and codas from event
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F9, Event.SHIFT_MASK), "DELOBS4"); // deletes all picks,amps, and codas from event
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F9, Event.ALT_MASK), "DELOBS4"); // deletes all amps from event
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F9, Event.CTRL_MASK), "DELOBS4"); // deletes all codas from event

        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, 0), "DELOBS1"); // force user to use mod key, likely to have accidental hit
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, Event.SHIFT_MASK), "DELOBS1");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, Event.ALT_MASK), "DELOBS1");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, Event.CTRL_MASK), "DELOBS1");

        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "DELOBS2"); // first coda, then amp, then picks, DELOBS3 reverses delete order
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, Event.SHIFT_MASK), "DELOBS3"); // deletes all picks,amps, and codas from event

        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_Z, 0), "UNDO"); // force user to use mod key, likely to have accidental hit
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_Z, Event.SHIFT_MASK), "UNDO");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_Z, Event.ALT_MASK), "UNDO");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_Z, Event.CTRL_MASK), "UNDO");

        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_L, 0), "TABLOC");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_L, Event.SHIFT_MASK), "TABLOC");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_M, 0), "TABMAG");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_M, Event.SHIFT_MASK), "TABMAG");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_C, Event.SHIFT_MASK), "TABCAT");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_T, Event.SHIFT_MASK), "TABMSG");

        // ? Do CTL and ALT mod combined with VK_A key for flipping rflag to H state on Coda and Amps ?
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_A, 0), "PS");

        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_B, 0), "F-HP");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_B, Event.SHIFT_MASK), "F-BP");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_B, Event.ALT_MASK), "F-LP");
        //inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_B, Event.CTRL_MASK), "F-NBP");
      
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_W, 0), "F-WA");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_W, Event.SHIFT_MASK), "F-WA_BP");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_W, Event.ALT_MASK), "F-WA_HP");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_W, Event.CTRL_MASK), "F-WA_EH");
       
        //inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0), "F-WA");
        //inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F10, Event.SHIFT_MASK), "F-WA_BP");
        //inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F11, 0), "F-HP");
        //inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F11, Event.SHIFT_MASK), "F-LP");
        //inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F12, 0), "F-BP");

        //inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F10, Event.SHIFT_MASK), "F-VEL");
        //inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F11, Event.SHIFT_MASK), "F-DIS");
        //inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F12, Event.SHIFT_MASK), "F-ACC");
        
        //inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_10, Event.ALT_MASK), "F-SP03");
        //inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_11, Event.ALT_MASK), "F-SP10");
        //inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_12, Event.ALT_MASK), "F-SP30");

        inputMap.put(KeyStroke.getKeyStroke('i'), "PHI");
        inputMap.put(KeyStroke.getKeyStroke('e'), "PHE");
        //inputMap.put(KeyStroke.getKeyStroke('w'), "PHW"); // conflicts with W-A Filter

        inputMap.put(KeyStroke.getKeyStroke('.'), "FM.");
        inputMap.put(KeyStroke.getKeyStroke(' '), "FM.");
        inputMap.put(KeyStroke.getKeyStroke('c'), "FMC");
        inputMap.put(KeyStroke.getKeyStroke('d'), "FMD");
        inputMap.put(KeyStroke.getKeyStroke('-'), "FM-");
        inputMap.put(KeyStroke.getKeyStroke('+'), "FM+");

        inputMap.put(KeyStroke.getKeyStroke('['), "TIME-");
        inputMap.put(KeyStroke.getKeyStroke(']'), "TIME+");
        inputMap.put(KeyStroke.getKeyStroke(';'), "TIME0");
        inputMap.put(KeyStroke.getKeyStroke('"'), "FULL");
        inputMap.put(KeyStroke.getKeyStroke(':'), "FULL");
        inputMap.put(KeyStroke.getKeyStroke('\''), "AMP0");
        inputMap.put(KeyStroke.getKeyStroke('\\'), "AMP-");
        inputMap.put(KeyStroke.getKeyStroke('|'), "AMP+");
        inputMap.put(KeyStroke.getKeyStroke('/'), "AMP-");
        inputMap.put(KeyStroke.getKeyStroke('*'), "AMP+"); // SHFT8

        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_H, 0), "FLAGS");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_H, Event.SHIFT_MASK), "RESID");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_T, 0), "SCALE");

        //
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_X, 0), "DELETE");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_X, Event.ALT_MASK), "DELETE");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_X, Event.SHIFT_MASK), "DELETE");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_X, Event.CTRL_MASK), "DELETE");
        //
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_BACK_QUOTE, 0), "iP0");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_BACK_QUOTE, Event.SHIFT_MASK), "iS0");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_BACK_QUOTE, Event.ALT_MASK), "iS0");
        boolean tf = ((Jiggle)mv.getOwner()).getProperties().getBoolean("pickingPanelHotKeyNum2Wt", false);
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_0, 0), (tf) ? "WT0" : "iP0");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_1, 0), (tf) ? "WT1" : "iP1");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_2, 0), (tf) ? "WT2" : "eP2");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_3, 0), (tf) ? "WT3" : "eP3");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_4, 0), (tf) ? "WT4" : "eP4");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_0, Event.SHIFT_MASK), "iS0");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_1, Event.SHIFT_MASK), "iS1");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_2, Event.SHIFT_MASK), "eS2");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_3, Event.SHIFT_MASK), "eS3");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_4, Event.SHIFT_MASK), "eS4");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_0, Event.ALT_MASK), "iS0");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_1, Event.ALT_MASK), "iS1");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_2, Event.ALT_MASK), "eS2");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_3, Event.ALT_MASK), "eS3");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_4, Event.ALT_MASK), "eS4");
        //
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD0, 0), "iP0");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD1, 0), "iP1");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD2, 0), "eP2");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD3, 0), "eP3");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD4, 0), "eP4");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD0, Event.SHIFT_MASK), "iS0");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD1, Event.SHIFT_MASK), "iS1");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD2, Event.SHIFT_MASK), "eS2");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD3, Event.SHIFT_MASK), "eS3");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD4, Event.SHIFT_MASK), "eS4");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD0, Event.ALT_MASK), "iS0");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD1, Event.ALT_MASK), "iS1");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD2, Event.ALT_MASK), "eS2");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD3, Event.ALT_MASK), "eS3");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD4, Event.ALT_MASK), "eS4");

        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), "UP");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_UP, Event.SHIFT_MASK), "UP");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_UP, Event.ALT_MASK), "UP");

        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), "DOWN");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, Event.SHIFT_MASK), "DOWN");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, Event.ALT_MASK), "DOWN");

        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_UP, 0), "PAGEUP");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_UP, Event.SHIFT_MASK), "PAGEUP");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_UP, Event.ALT_MASK), "PAGEUP");

        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_DOWN, 0), "PAGEDOWN");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_DOWN, Event.SHIFT_MASK), "PAGEDOWN");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_DOWN, Event.ALT_MASK), "PAGEDOWN");

        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_HOME, 0), "TOP");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_HOME, Event.SHIFT_MASK), "TOP");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_HOME, Event.ALT_MASK), "TOP");

        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_END, 0), "BOTTOM");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_END, Event.SHIFT_MASK), "BOTTOM");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_END, Event.ALT_MASK), "BOTTOM");

        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_R, 0), "REJECT");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_R, Event.SHIFT_MASK), "REJECT");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_R, Event.ALT_MASK), "REJECT");

        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_P, 0), "PICK");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_P, Event.SHIFT_MASK), "PICK");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_P, Event.ALT_MASK), "PICK");

        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F, 0), "FLIP");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F, Event.SHIFT_MASK), "FLIP");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_F, Event.ALT_MASK), "FLIP");

        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_O, Event.ALT_MASK), "ONOFF");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_O, 0), "ONOFF");
        inputMap.put( KeyStroke.getKeyStroke(KeyEvent.VK_O, Event.SHIFT_MASK), "ONOFF");


        // add map to zwfp to avoid makePick action when typing 0-4 in timescale combobox  -aww
        zwfp.setInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT,inputMap);
        ComponentInputMap compInputMap = new ComponentInputMap(zwfp);
        KeyStroke [] keys = inputMap.keys(); 
        for (int idx = 0; idx < keys.length; idx++) {
            compInputMap.put(keys[idx], inputMap.get(keys[idx]));
        }
        zwfp.setInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW,compInputMap);
        /*
        zwfp.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent evt) {
                System.out.println(KeyEvent.VK_NUM_LOCK + " code: " +  KeyEvent.getKeyText(evt.getKeyCode()) +
                        " : " + evt.getModifiers() +
                        " : " + KeyEvent.getKeyModifiersText(evt.getModifiers())
                        );
            }
        }); 
        */
    }

} // PickingPanel
