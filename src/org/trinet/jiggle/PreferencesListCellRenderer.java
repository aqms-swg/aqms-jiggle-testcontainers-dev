package org.trinet.jiggle;

import java.awt.Component;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class PreferencesListCellRenderer<E> extends JLabel implements ListCellRenderer<E> {
    private static final long serialVersionUID = 1L;
    private final Map<String, String> toolTipMap;

    /**
     * Create the preferences list cell renderer.
     * 
     * @param toolTipMap  the tool tip map.
     */
    public PreferencesListCellRenderer(Map<String, String> toolTipMap) {
        this.toolTipMap = toolTipMap;
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends E> list, E value, int index, boolean isSelected,
            boolean cellHasFocus) {
        String s = value.toString();
        setText(s);
        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
        setToolTipText(toolTipMap.get(value));
        setEnabled(list.isEnabled());
        setFont(list.getFont());
        setOpaque(true);
        return this;
    }
}
