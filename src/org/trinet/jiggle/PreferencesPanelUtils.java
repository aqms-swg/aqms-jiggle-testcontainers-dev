package org.trinet.jiggle;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

public class PreferencesPanelUtils extends JPanel {
    private static final long serialVersionUID = 1L;

    /**
     * Create a grid panel with two columns.
     * 
     * @return the grid panel.
     */
    public static JPanel createGridPanel() {
        return new JPanel(new GridLayout(0, 2, 0, 0));
    }

    /**
     * Create a grid panel with the specified number of columns.
     * 
     * @param cols the number of columns, not 0.
     * @return the grid panel.
     */
    public static JPanel createGridPanel(int cols) {
        if (cols == 0) {
            throw new IllegalArgumentException("cols cannot be zero");
        }
        return new JPanel(new GridLayout(0, cols, 0, 0));
    }

    /**
     * Create the scroll component.
     * 
     * @param view  the component to view.
     * @param title the title or null if none.
     * @return the scroll component.
     */
    public static Component createScrollComponent(Component view, String title) {
        Border border = null;
        JScrollPane jsp = new JScrollPane(view);
        if (title != null) {
            border = BorderFactory.createTitledBorder(title);
            jsp.setBorder(border);
        }
        Dimension size = new Dimension(80, 640);
        updateSize(size, view, border);
        view.setPreferredSize(size);
        return jsp;
    }

    /**
     * Update the size.
     * 
     * @param size the size.
     * @param comp the component.
     */
    public static void updateSize(Dimension size, Component comp) {
        Border border = null;
        if (comp instanceof JComponent) {
            border = ((JComponent) comp).getBorder();
        }
        updateSize(size, comp, border);
    }

    /**
     * Update the size.
     * 
     * @param size   the size.
     * @param comp   the component.
     * @param border the border or null if none.
     */
    public static void updateSize(Dimension size, Component comp, Border border) {
        updateSize(size, comp.getPreferredSize());
        if (border instanceof TitledBorder) {
            Dimension bsize = ((TitledBorder) border).getMinimumSize(comp);
            updateSize(size, bsize);
        }
    }

    /**
     * Update the size.
     * 
     * @param size  the size.
     * @param csize the component size.
     */
    public static void updateSize(Dimension size, Dimension csize) {
        size.width = Math.max(size.width, csize.width);
        size.height = Math.max(size.height, csize.height);
    }
}
