package org.trinet.jiggle;

import java.util.*;
import org.trinet.jdbc.*;
import org.trinet.jasi.*;
import org.trinet.util.*;

/** This is a work in progress and is not finished. It is intended to allow
 *  a RealTime waveform view that scrolls with time. */
public class RTWFView extends WFView {

    double duration = 60.0;

    public RTWFView() { }
/**
 * Construct an empty view window with defined viewSpan time bounds.
 */
    public RTWFView(Channel ch, double duration, WaveServerGroup wsg) {

        super(ch);

        setDuration(duration);
        setTimeWindow();


        wf = (AbstractWaveform) AbstractWaveform.createDefaultWaveform();
        wf.setChannelObj(getChannelObj());

        this.setWaveform(wf);

        AbstractWaveform.setWaveDataSource(wsg);

    }

    public void setTimeWindow() {
        setTimeWindow(getDuration());
    }

    public void setTimeWindow(double duration) {
        double windowEnd = new DateTime().getTrueSeconds();  // now UTC -aww 2008/02/10
        double windowStart = windowEnd - getDuration();

        setViewSpan(new TimeSpan(windowStart, windowEnd));
    }

    public void setDuration(double secs) {
        duration = secs;
    }
    public double getDuration() {
        return duration;
    }
    public void setWaveServerGroup(WaveServerGroup wsg) {
        AbstractWaveform.setWaveDataSource(wsg);
    }
 /** Keep waveform synched with WFView time window */
    protected void refreshTimeSeries() {
        // toss old stuff before time window
        // get missing part
    }

    /** Set the time window's TimeSpan object. */
    public void setTimeSpan(TimeSpan ts) {
        wf.setTimeSpan(ts);

        WFSegment seg[] = wf.getArray();
        // trim segs that are now outside the viewSpan
        for (int i=0; i < seg.length; i++) {
          // trim seg (or toss it if it's completely outside the time window)
          if (!seg[i].trim(ts.getStart(), ts.getEnd())) wf.getSegmentList().remove(seg[i]);
        }

        // create a waveform representing the 'new' time window
        Waveform twf = AbstractWaveform.createDefaultWaveform();
        twf.setChannelObj(getChannelObj());

        double end = ts.getEnd();
        double start = getDataBox().getEndTime();

        twf.setTimeSpan(new TimeSpan(start, end));
        // get it
        twf.loadTimeSeries();
        // append segments to this waveform
        seg = twf.getArray();

        for (int i=0; i < seg.length; i++) {
          wf.getSegmentList().add(seg[i]);
        }

    }

//////// INNER CLASS
/** This is a thread that keeps the waveforms loaded and up to date. */
  class WFLoaderThread implements Runnable {

    RTWFView wfv;
    double refreshInterval = 5.0;
    boolean stopLoader;

    public Thread thread;

    // constructor - also starts thread
    public WFLoaderThread(RTWFView wfv, double interval) {

        this.wfv = wfv;
        refreshInterval = interval;

        thread = new Thread(this);

        stopLoader = false;
        thread.start();
    }

    public void run() {
        while (true) {
            Object wf = wfv.getWaveform();
            if (wf == null) return;
            //
            synchronized (wf) {
                wfv.refreshTimeSeries();
            }
            if (stopLoader) return;
        }
    } // end of run()

  } // end of WFLoaderThread class
////////
}
