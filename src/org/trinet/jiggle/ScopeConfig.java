package org.trinet.jiggle;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;

import org.trinet.jasi.*;
import org.trinet.jiggle.common.PropConstant;
import org.trinet.util.*;
import org.trinet.util.graphics.text.*;
import org.trinet.util.graphics.*;

public class ScopeConfig {

        Jiggle jiggle = null;
        DateTime scopeViewTime = new DateTime();
        double scopeDuration = 60.;
        boolean autoRefreshEndTime = true;
        boolean isNew = true;
        MasterView myMv = null;

        ChannelableListIF scopeChanList = new ChannelableList();
        String channelFilename = null;

        DateTimeChooser dtc = null;
        JButton view2DTCButton = null;
        JCheckBox autoRefreshCheckBox = null;
        NumberTextField durationField = null;
        JTextArea channelTextArea = null;
        JCheckBox scopeModeCheckBox = null;
        JLabel scopeModeLabel = null;
        JComboBox groupNameComboBox = null;
        JPanel mainPanel= null;
        ChannelableJTree cjt = null;
        int lastDirection = 0;
       
        ScopeConfig(Jiggle jiggle) {
            this.jiggle = jiggle;
            channelFilename = jiggle.getProperties().getUserFileNameFromProperty("scopeModeChannelFile");
            scopeDuration = jiggle.getProperties().getDouble("scopeModeDuration");
            autoRefreshEndTime = jiggle.getProperties().getBoolean("scopeModeRefreshEndTime");

            buildUI();
        }

        /**
         * Get the datetime to use on the UI. If start time, then subtract duration, else return current time
         * @return
         */
        private DateTime getScopeTimeStartOrEnd(double durationSecond) {
            DateTime tempTime = new DateTime();
            if (isUseStartTime()) {
                // If start time, subtract duration
                tempTime.setTime(tempTime.getTime() - ((long)durationSecond * 1000) );
            }
            return tempTime;
        }

        private void buildUI() {
            if (Double.isNaN(scopeDuration) || scopeDuration <= 0.) {
                scopeDuration = 60.;
            }

            Box vbox = Box.createVerticalBox();
            //scopeModeCheckBox = new JCheckBox("RT snapshot view mode enabled, unchecked for event Catalog mode", jiggle.scopeMode);
            scopeModeCheckBox = new JCheckBox("Scope mode", jiggle.scopeMode);
            //scopeModeCheckBox.setAlignmentX(Component.LEFT_ALIGNMENT); 
            //vbox.add(scopeModeCheckBox);
            scopeModeLabel = new JLabel(jiggle.scopeMode ? "Scope mode ON " : "Scope mode OFF");
            vbox.add(scopeModeLabel);

            String scopeViewTimeLabel = isUseStartTime() ? "Requested view start time" : "Requested view end time";

            // do datetime chooser here for absolute time ???
            dtc = new DateTimeChooser(getScopeTimeStartOrEnd(this.scopeDuration), scopeViewTimeLabel);
            dtc.setAlignmentX(Component.LEFT_ALIGNMENT); 
            vbox.add(dtc);
            Box hbox = Box.createHorizontalBox();
            JLabel jl = new JLabel("View width (secs)");
            jl.setToolTipText("Valid range 1 to 600");
            hbox.add(jl);

            durationField = NumberTextFieldFactory.createInputField( 5, false, "###.##", jiggle.getFont(), true);
            durationField.setDefaultValue(Double.valueOf(scopeDuration));
            durationField.setLimits(Double.valueOf(1.0), Double.valueOf(600.));
            durationField.setValue(scopeDuration);
            durationField.setMaximumSize(new Dimension(32,16));
            new JTextClipboardPopupMenu(durationField);
            hbox.add(durationField);

            view2DTCButton = new JButton("|");
            view2DTCButton.setMaximumSize(new Dimension(16,16));
            view2DTCButton.setToolTipText("Center scope viewport around zoom panel's current center time");
            view2DTCButton.addActionListener( new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    if (jiggle.mv != null && jiggle.mv.masterWFViewModel.get() != null) {
                        if (isUseStartTime()) {
                            dtc.setTrueSeconds(jiggle.mv.masterWFWindowModel.getCenterTime() - durationField.getDoubleValue() / 2.);
                        } else {
                            dtc.setTrueSeconds(jiggle.mv.masterWFWindowModel.getCenterTime() + durationField.getDoubleValue() / 2.);
                        }
                    }
                }
            });
            hbox.add(view2DTCButton);
            hbox.add(Box.createHorizontalStrut(10));

            String autoRefreshLabel = "Auto refresh view end time";
            String autoRefreshTooltip = "Update view end time to current time for each \"Next\" button click, else leave it unchanged";
            if (this.isUseStartTime()) {
                autoRefreshLabel = "Auto refresh view start time";
                autoRefreshTooltip = "Update view start time to current time for each \"Next\" button click, else leave it unchanged";
            }
            autoRefreshCheckBox = new JCheckBox(autoRefreshLabel, autoRefreshEndTime);
            autoRefreshCheckBox.setToolTipText(autoRefreshTooltip);
            autoRefreshCheckBox.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    autoRefreshEndTime = autoRefreshCheckBox.isSelected();
                }
            });
            hbox.add(autoRefreshCheckBox);
            hbox.add(Box.createHorizontalGlue());
            hbox.setAlignmentX(Component.LEFT_ALIGNMENT);
            vbox.add(hbox);

            JPanel chanPanel = new JPanel(new BorderLayout());
            chanPanel.setBorder(BorderFactory.createTitledBorder("View Channels (net.sta.chl.loc, use '--' for blank loc)"));
            channelTextArea = new JTextArea();
            channelTextArea.setToolTipText("Channel delimiters: blank,tab,CR,LF; name field delimiters: ./_+,");
            channelTextArea.setEditable(true);
            channelTextArea.setBackground(chanPanel.getBackground());
            DocumentListener myListener = new DocumentListener() {
                public void changedUpdate(DocumentEvent e) {
                    isNew = true;
                } 
                public void insertUpdate(DocumentEvent e) {
                    isNew = true;
                }
                public void removeUpdate(DocumentEvent e) {
                    isNew = true;
                }
            }; 
            channelTextArea.getDocument().addDocumentListener(myListener);
 
            if (channelFilename != null) setupChannelTextAreaFromFile(channelFilename);
            new JTextClipboardPopupMenu(channelTextArea);
            JScrollPane jsp = new JScrollPane();
            jsp.setViewportView(channelTextArea);
            jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            jsp.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            chanPanel.add(jsp, BorderLayout.CENTER);

            Box hbox2 = Box.createHorizontalBox();
            JButton fileButton = new JButton("Load File");
            //fileButton.setMaximumSize(new Dimension(32,16));
            fileButton.setToolTipText("Load text area with channel names parsed from input file listing.");
            fileButton.addActionListener( new ActionListener() {
              public void actionPerformed(ActionEvent evt) {
                  setupChannelTextAreaFromChooser();
              }
            });
            fileButton.setAlignmentX(Component.LEFT_ALIGNMENT);
            //hbox2.add(Box.createHorizontalGlue());
            hbox2.add(fileButton);
            hbox2.add(Box.createHorizontalStrut(10));

            JButton clearButton = new JButton("Clear");
            //clearButton.setMaximumSize(new Dimension(32,16));
            clearButton.setToolTipText("Clear text area of channel names");
            clearButton.addActionListener( new ActionListener() {
              public void actionPerformed(ActionEvent evt) {
                  channelTextArea.setText(""); // clear text 
              }
            });
            clearButton.setAlignmentX(Component.LEFT_ALIGNMENT);
            //hbox2.add(Box.createHorizontalGlue());
            hbox2.add(clearButton);

            /*
            JButton cacheButton = new JButton("Load Group");
            //cacheButton.setMaximumSize(new Dimension(32,16));
            cacheButton.setToolTipText("Load text area with channel names associated with named channel group in database.");
            cacheButton.addActionListener( new ActionListener() {
              public void actionPerformed(ActionEvent evt) {
                  String name = jiggle.getProperties().getChannelGroupName();
                  if (name == null) name = "";
                  name = JOptionPane.showInputDialog(jiggle,"Enter channel group name", name);
                  setupChannelTextAreaFromCache(name);
              }
            });
            cacheButton.setAlignmentX(Component.LEFT_ALIGNMENT);
            //hbox2.add(Box.createHorizontalGlue());
            hbox2.add(cacheButton);
            */

            //JLabel cacheLabel = new JLabel("Load group: "); // new JButton("Load Group");
            //cacheLabel.setAlignmentX(Component.LEFT_ALIGNMENT);
            //hbox2.add(cacheLabel);

            java.util.List appNames = JasiDbReader.getApplicationNames(DataSource.getConnection());
            ArrayList aList = new ArrayList(appNames.size()+1);
            aList.add("cache channels"); 
            aList.addAll(appNames);
            groupNameComboBox = new JComboBox(aList.toArray());
            groupNameComboBox .setAlignmentX(Component.LEFT_ALIGNMENT);

            JButton groupNameButton = new JButton("Load Group: ");
            groupNameButton.setToolTipText("Load text area with channel names associated with cache or group by name selected from combobox.");
            groupNameButton.setAlignmentX(Component.LEFT_ALIGNMENT);
            groupNameButton.addActionListener( new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    final String selectedAppName = (String) groupNameComboBox.getSelectedItem();
                    if (selectedAppName != null) {
                        final org.trinet.util.SwingWorker worker = new org.trinet.util.SwingWorker() {
                            boolean status = false;
                            public Object construct() {
                                  status = setupChannelTextAreaFromCache(selectedAppName);
                                  return null;
                            }
                            public void finished() {
                                jiggle.resetStatusGraphicsForThread();
                                if (!status ) {
                                    JOptionPane.showMessageDialog(jiggle, "No channel list for group name : " + selectedAppName);
                                }
                                else {
                                    int answer = JOptionPane.showConfirmDialog(
                                        jiggle,
                                        cjt,
                                        "Check those channels to load into scope view",
                                        JOptionPane.OK_CANCEL_OPTION,
                                        JOptionPane.PLAIN_MESSAGE);
                                    if (answer == JOptionPane.OK_OPTION) {
                                        channelTextArea.setText(""); // clear text before adding new list
                                        java.util.List aList = cjt.getSelectedNames();
                                        //java.util.List aList = cjt.getSelectedChannelables();
                                        //ChannelableList cList = new ChannelableList(aList);
                                        //cList.sortByStation();
                                        for (int idx = 0; idx < aList.size(); idx++) {
                                            channelTextArea.append( (String)aList.get(idx) );
                                            //channelTextArea.append( ((Channelable)cList.get(idx)).toDelimitedSeedNameString(".") );
                                            channelTextArea.append("\n");
                                        }
                                    }
                                }
                            }
                        };
                        jiggle.initStatusGraphicsForThread("Scope Channel Selection", "Loading channel name tree...");
                        worker.start();
                    }
                }
            });
            hbox2.add(groupNameButton);
            hbox2.add(groupNameComboBox);

            hbox2.add(Box.createHorizontalStrut(10));

            JButton saveButton = new JButton("Save");
            //saveButton.setMaximumSize(new Dimension(32,16));
            saveButton.setToolTipText("Save text area with channel names parsed to file listing.");
            saveButton.addActionListener( new ActionListener() {
              public void actionPerformed(ActionEvent evt) {
                  saveChannelTextAreaToFileFromChooser();
              }
            });
            saveButton.setAlignmentX(Component.LEFT_ALIGNMENT);
            hbox2.add(saveButton);
            //hbox2.add(Box.createHorizontalGlue());

            hbox2.setAlignmentX(Component.LEFT_ALIGNMENT);
            chanPanel.add(hbox2, BorderLayout.SOUTH);
            vbox.add(chanPanel);

            mainPanel= new JPanel(new BorderLayout());
            mainPanel.add(vbox, BorderLayout.NORTH);
            mainPanel.add(chanPanel, BorderLayout.CENTER);
            mainPanel.setPreferredSize(new Dimension(520,400));
            mainPanel.setMinimumSize(new Dimension(520,400));
        }

        /**
         * Return true if the scope configuration uses start time, false if using end time
         * @return
         */
        public boolean isUseStartTime() {
            return jiggle.getProperties().getBoolean(PropConstant.SCOPE_USE_START_TIME);            
        }

        public boolean processDialog(boolean showDialog, int direction) {
            //System.out.println("ScopeConfig processDialog show: " + showDialog + " direction: " + direction + " lastDirection: " + lastDirection);
            //System.out.println("Old Time: " + dtc.getDateTime() + " + seconds: " + (scopeDuration/direction));
            if (!showDialog) {
                if (direction == 0) {

                    autoRefreshEndTime = autoRefreshCheckBox.isSelected();

                    if (myMv != null && myMv.getSelectedSolution() != null) {
                        autoRefreshEndTime = false;
                    }

                    if (autoRefreshEndTime) {
                        dtc.setTime(getScopeTimeStartOrEnd(this.scopeDuration));
                    }
                } else {
                    DateTime dt = new DateTime();
                    double now = dt.getTrueSeconds();
                    double userSelectedTime = dtc.getTrueSeconds();
                    if (userSelectedTime < now || direction < 0) {
                        // Direction may be negative/positive to move the time span left/right
                        userSelectedTime += (scopeDuration / direction);
                    }
                    dtc.setTrueSeconds(userSelectedTime);
                }
            } else { // if lastDirection was to use current time
                if (lastDirection == 0 && autoRefreshEndTime) {
                    dtc.setTime(getScopeTimeStartOrEnd(this.scopeDuration));
                }
            }
            //System.out.println("New Time: " + dtc.getDateTime());

            int ans = JOptionPane.YES_OPTION;
            if (showDialog) {
                ans = JOptionPane.showOptionDialog(jiggle, mainPanel,
                        "RT Waveform Snapshot Settings",
                        JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE,
                        null, new String[]{"On", "Off", "Cancel",}, "On");

                if (ans == JOptionPane.YES_OPTION) {
                    scopeDuration = durationField.getDoubleValue();
                    if (scopeDuration < 1.0) {
                        scopeDuration = 1.;
                        durationField.setText("1.");
                    }

                    jiggle.getProperties().setProperty("scopeModeDuration", scopeDuration);
                    jiggle.getProperties().setProperty("scopeModeRefreshEndTime", autoRefreshEndTime);

                    scopeChanList = setupScopeChannelListFromTextArea(scopeChanList);

                    scopeModeCheckBox.setSelected(true);
                    scopeModeLabel.setText("Scope mode ON ");
                    jiggle.scopeMode = true;
                    System.out.println("Jiggle INFO: MasterView load changed to RT Scope mode");
                    jiggle.getProperties().setProperty("scopeMode", jiggle.scopeMode);
                    AbstractWaveform.cache2File = false;
                } else if (ans == JOptionPane.NO_OPTION) {
                    turnScopeOff();
                } else {
                    return false; // abort
                }
                if (myMv == null || myMv != jiggle.mv)
                    isNew = true; // reset here in case user wants to switch back again ?
            }

            lastDirection = direction;
            scopeViewTime = dtc.getDateTime();
            return true;

            //System.out.println("ScopeConfig processDialog ending scopeMode: " + jiggle.scopeMode + " lastDirection: " + lastDirection);

        }

        public DateTime getScopeUserSetViewTime() {
            return this.scopeViewTime;
        }

        protected boolean turnScopeOff() {
            boolean status = true;
            if (jiggle.scopeMode && myMv != null) {
                Solution lastNewSol = myMv.getSelectedSolution();
                if (lastNewSol != null && lastNewSol.getNeedsCommit()) {
                    int answer = JOptionPane.showConfirmDialog(jiggle,
                                      "Last new scope event needs commit\n Save this event (YES), or don't save (NO), or abort (CANCEL)?",
                                      "Scope Mode Off", JOptionPane.YES_NO_CANCEL_OPTION);
                    status = false;
                    if (answer == JOptionPane.YES_OPTION) {
                        status = jiggle.saveToDb(lastNewSol);
                    }
                    else if (answer == JOptionPane.NO_OPTION) {
                        status = true;
                    }
                }
            }
            jiggle.scopeMode = false;
            jiggle.getProperties().setProperty("scopeMode", jiggle.scopeMode);
            scopeModeCheckBox.setSelected(false);
            scopeModeLabel.setText("Scope mode OFF");
            System.out.println("Jiggle INFO: MasterView load changed to Event Catalog mode");
            AbstractWaveform.cache2File = jiggle.getProperties().getBoolean("wfCache2File");
            // If masterview has solutions force a new masterview on next dialog show (else they accumulate)
            if (myMv != null && myMv.solList != null && myMv.solList.size() > 0) {
                myMv = null;
                isNew = true;
            }
            return status;
        }

        private void saveChannelTextAreaToFileFromChooser() {
            channelFilename = getScopeChannelIdsFilename(channelFilename, false);
            if (channelFilename != null) saveChannelTextAreaToFile(channelFilename);
        }

        private void setupChannelTextAreaFromChooser() {
            if (channelFilename == null)
               channelFilename = jiggle.getProperties().getUserFilePath() + GenericPropertyList.FILE_SEP + "scopeChannels.txt";
            channelFilename = getScopeChannelIdsFilename(channelFilename, true);
            if (channelFilename != null) setupChannelTextAreaFromFile(new File(channelFilename));
        }

        private void saveChannelTextAreaToFile(String channelFilename) {
            if (channelFilename == null) return;
            int count = 0; // line counter
            BufferedWriter idStream = null;
            try {
              idStream = new BufferedWriter(new FileWriter(channelFilename));
              StringTokenizer strToke = new StringTokenizer(channelTextArea.getText(), "\n\r \t");
              while (strToke.hasMoreTokens()) {
                  idStream.write(strToke.nextToken());
                  idStream.newLine();
                  count++;
              }
            } // upon error do what ? 
            catch (FileNotFoundException ex) {
              ex.printStackTrace();
              System.err.println("Error: Output file not found: " + channelFilename);
            }
            catch (IOException ex) {
              ex.printStackTrace();
              System.err.println("Error: i/o for output file: " + channelFilename + " at line: " + count);
            }
            finally {
              try {
                if (idStream != null) {
                    idStream.flush();
                    idStream.close();
                }
              }
              catch (IOException ex2) { }
            }
        } 
       
        private boolean setupChannelTextAreaFromCache(String name) {
            if (name == null || name.length() == 0 ) {
                return false;  // abort
            }

            if (name.equalsIgnoreCase("cache channels")) {
                cjt = new ChannelableJTree( MasterChannelList.get() );
            }
            else {
                ChannelableList cList = Channel.create().getNamedChannelList(name);
                if (cList == null || cList.size() == 0) return false;
                cjt = new ChannelableJTree(cList);
            }
            cjt.getTree().setScrollsOnExpand(true);
            cjt.setScrollPanePreferredSize(new Dimension(780,500));
            cjt.setSelectedNames(setupScopeChannelListFromTextArea(new ChannelableList()));
            return true;
        }


        private void setupChannelTextAreaFromFile(String channelFilename) {
            if (channelFilename == null) return;
            File file = new File(channelFilename);
            if (file != null && file.canRead()) setupChannelTextAreaFromFile(file);
            else {
                JOptionPane.showMessageDialog(jiggle,
                    "File D.N.E. or cannot read!:\n" + channelFilename, 
                    "Scope Channel File Open Failure",
                    JOptionPane.ERROR_MESSAGE
                );
            }
        }

        private void setupChannelTextAreaFromFile(File file) {
            int count = 0; // line counter
            BufferedReader idStream = null;
            if (channelTextArea.getText().length() > 0) {
              int yn = JOptionPane.showConfirmDialog(jiggle,
                           "Clear text area before loading file list",
                           "Existing Channel List",
                           JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
              if (yn == JOptionPane.YES_OPTION) channelTextArea.setText("");
            }
            try {
              idStream = new BufferedReader(new FileReader(file));
              String idString = null;
              while (true) {
                  idString = idStream.readLine();
                  if (idString == null) break;
                  count++;
                  if (idString.length() == 0) continue;
                  channelTextArea.append(idString);
                  channelTextArea.append("\n");
              }
            } // upon error do what ? 
            catch (FileNotFoundException ex) {
              ex.printStackTrace();
              System.err.println("Error: Input file not found: " + file);
            }
            catch (IOException ex) {
              ex.printStackTrace();
              System.err.println("Error: i/o for input file: " + file + " at line: " + count);
            }
            finally {
              try {
                if (idStream != null) idStream.close();
              }
              catch (IOException ex2) { }
            }
        } 

        private String getScopeChannelIdsFilename(String oldFileName, boolean open) {
            File file = null;
            if (oldFileName != null) file = new File(oldFileName);
            JFileChooser jfc = new JFileChooser( (file == null) ? jiggle.getProperties().getUserFilePath() : file.getAbsolutePath() );
            if (file != null) jfc.setSelectedFile(file);
            String filename = null;
            int answer = (open) ? jfc.showOpenDialog(jiggle) : jfc.showSaveDialog(jiggle);
            if (answer == JFileChooser.APPROVE_OPTION) {
                file =jfc.getSelectedFile();
                System.out.println("Selected File: " + file);
                boolean isValid = (file != null && (!open || (open && file.exists())));
                if (isValid) {
                    try {
                      filename = file.getCanonicalPath();
                      jiggle.getProperties().setProperty("scopeModeChannelFile", filename); // added -aww 2008/06/16
                    }
                    catch (IOException ex) {
                      ex.printStackTrace();
                    }
                    catch (SecurityException ex) {
                      ex.printStackTrace();
                    }
                }
                else {
                     JOptionPane.showMessageDialog(jiggle,
                        "File D.N.E. or cannot open file!", 
                        "Scope Channel File Open Failure",
                         JOptionPane.ERROR_MESSAGE
                     );
                     filename = null;

                }

            }
            return filename;
        }

        private ChannelableListIF setupScopeChannelListFromTextArea(ChannelableListIF inList) {
            inList.clear();
            String text = channelTextArea.getText();
            if (text == null || text.length() == 0) return inList;

            StringTokenizer strTok = new StringTokenizer(text, " \t\n\r");
            Channel ch = null;
            while (strTok.hasMoreElements()) {
               ChannelName cn = ChannelName.fromDelimitedString(strTok.nextToken(), ".,/_+");
               ch = Channel.create();
               ch.setChannelName(cn);
               inList.add(ch.getChannelObj());
           }
           return inList;
        }

}

