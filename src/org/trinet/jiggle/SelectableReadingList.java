package org.trinet.jiggle;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;
import org.trinet.util.*;
import org.trinet.util.graphics.ColorList;
import org.trinet.util.graphics.IconImage;
import org.trinet.util.graphics.text.JTextClipboardPopupMenu;

/**
* A text JList of JasiReadings (Phase, Amp, Coda) that is selectable so that by
* clicking on a text line the channel is selected via the MasterView's masterWFWindowModel.
*
* This component does NOT listen to the masterWFWindowModel itself, :. doesn't
* change selection to match selections made in other views.
*/
public class SelectableReadingList extends JPanel {
    private final static Color defaultSummaryFgColor  = Color.black;         // 000000
    private final static Color defaultSummaryBgColor  = Color.yellow;        // ffff00
    private final static Color defaultListBgColor     = Color.lightGray;     // c0c0c0
    private final static Color defaultCellBgColor     = Color.white;         // ffffff
    private final static Color defaultSelectedBgColor = ColorList.litesmoke; // f0f0f0

    private final static Color defaultBorderColor     = Color.blue;  // 0000ff
    private final static Color defaultGoodColor       = Color.black; // 000000
    private final static Color defaultOutColor        = Color.red;   // ff0000
    private final static Color defaultNotUsedColor    = Color.blue;  // 0000ff
    private final static Color defaultWarnColor       = ColorList.burntOrange; // ff9600

    private final static double defaultOutThreshold  = 0.5;
    private final static double defaultWarnThreshold  = 0.25;

     private Solution sol = null;
     private Magnitude mag = null;

     // Selected wfview model listener enables setting selected reading in list to selected wfview - aww 02/28/2007
     SelectedWFViewListener selectedWFViewListener = new SelectedWFViewListener();

     private JasiReadingList readingList = null;
     private JasiReading selReading = null;

     /** The subset of readingList that is visible. Virtually deleted records should not
     * be shown. */
     private JasiReadingList visibleList = null;
     private boolean hideDeleted = true;

     AlteredListPopupListener alpListener = null;
     UnalteredListPopupListener ulpListener = null;

     private JasiReadingListListener jasiReadingListListener = new JasiReadingListListener();

     private JButton recalcButton = null;
     /** Optional actionListener that recalculates the summary value for these readings. */
     private ActionListener recalcListener = null;

     // MasterView is needed for the models
     private MasterView mv = null;

     private BorderLayout borderLayout1 = new BorderLayout();
     private BorderLayout borderLayout2 = new BorderLayout();
     private JPanel topPanel = new JPanel();
     private JTextArea summaryTextArea   = new JTextArea();
     private JScrollPane scroller = new JScrollPane();
     private JCheckBox hideDeletedCheckBox = new JCheckBox("Hide Virtually Deleted", true);

     private JList jlist = new JList();
     private int oldIndex = -1;

     private boolean editable = true;

     public static final int Unknown = -1;
     public static final int Phases = 0;
     public static final int Amps   = 1;
     public static final int Codas  = 2;
     // type of list we are showing
     private int type = Unknown;

      //                    type         face      size
     private Font font = new Font("Monospaced", Font.PLAIN, 12);
     private Font fontBold = new Font("Monospaced", Font.BOLD, 12);

     protected static boolean alignOnlyOnFocus = true; // default is align only when visble list hasFocus -aww
     private static boolean doingFind = false; // toggled true during Find... menu action -aww 2013/01/11

     protected static Color summaryFgColor = defaultSummaryFgColor;
     protected static Color summaryBgColor = defaultSummaryBgColor;
     protected static Color listBgColor = defaultListBgColor;
     protected static Color selectedBgColor =  defaultSelectedBgColor;
     //
     protected static Color goodColor = defaultGoodColor;
     protected static Color warnColor = defaultWarnColor;
     protected static Color outColor  = defaultOutColor;
     protected static Color notUsedColor  = defaultNotUsedColor;
     protected static Color cellBgColor  = defaultCellBgColor;
     protected static Color borderColor  = defaultBorderColor;

     private static Border lineBorder  = BorderFactory.createLineBorder(borderColor, 1);
     private static Border emptyBorder = BorderFactory.createEmptyBorder(1,1,1,1);

     protected static double outThresholdTT  = defaultOutThreshold;
     protected static double warnThresholdTT = defaultWarnThreshold;

     protected static double outThresholdMag  = defaultOutThreshold;
     protected static double warnThresholdMag = defaultWarnThreshold;

     public static void setWarnThresholdTT(double warnValue) {
         warnThresholdTT = warnValue;
     }

     public static void setOutlierThresholdTT(double outValue) {
         outThresholdTT = outValue;
     }

     public static void setWarnThresholdMag(double warnValue) {
         warnThresholdMag = warnValue;
     }

     public static void setOutlierThresholdMag(double outValue) {
         outThresholdMag = outValue;
     }

    public void refreshColors() {
        lineBorder = BorderFactory.createLineBorder(borderColor, 1);

        summaryTextArea.setForeground(summaryFgColor);
        summaryTextArea.setBackground(summaryBgColor);
        summaryTextArea.repaint();

        jlist.setBackground(listBgColor);
        jlist.setSelectionBackground(selectedBgColor);
        jlist.repaint();
    }

    public SelectableReadingList() {

        try  {
            jbInit();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }

        this.setFont(font);

        jlist.setFont(font);
        jlist.setBackground(listBgColor);

        jlist.setCellRenderer(new SelectableListCellRenderer());
        jlist.setSelectionBackground(selectedBgColor);
        jlist.setModel(new DefaultListModel());
        // add listener to the JList(rather than the model)
        jlist.addListSelectionListener(new SelectionListener());

        summaryTextArea.setFont(fontBold);
        summaryTextArea.setLineWrap(true);
        summaryTextArea.setForeground(summaryFgColor);
        summaryTextArea.setBackground(summaryBgColor);
        summaryTextArea.setEditable(false);
        summaryTextArea.setBorder(BorderFactory.createRaisedBevelBorder());

    }

    /** Using 'type', construct the approriate type of reading component. */
    public SelectableReadingList(MasterView mv, Solution aSol, int type) {
        this();
        setMasterView(mv);
        setSolution(aSol, type);
    }

    public SelectableReadingList(MasterView mv, Solution aSol, JasiReadingList readings) {
        this(mv, aSol, readings, true);
    }

    public SelectableReadingList(MasterView mv, Solution aSol, JasiReadingList readings, boolean sortList) {
        this();
        this.sol = aSol;
        setMasterView(mv);
        setReadingList(readings, sortList);  // do first so we can tell if phases, amp, or codas
    }

    public SelectableReadingList(MasterView mv, Magnitude aMag) {
        this();
        setMasterView(mv);
        setMagnitude(aMag);
    }

    // Effects whether menu options to change readings in list are made available to user, default is true
    public void setEditable(boolean tf) {
        editable =tf;
    }

    /*
    protected void setHeader(int type) {
        if (type == Phases) {
            header = Phase.getNeatStringHeader();
        } else if (type == Amps) {
            header = Amplitude.getNeatStringHeader();
        } else if (type == Codas){
            header = Coda.getNeatStringHeader();
        } else {
            header = "Unknown ReadingList Header Type: " + type;
        }
    }
    */

    public void setSolution(Solution aSol, int type) {

        sol = aSol;
        mag = null; // reset mag, in case solution changed - 01/24/2005 -aww
        this.type = type;

        if (sol == null)  {
            summaryTextArea.setText("Null Solution for type: " + type); // clear any old stuff
            return;
        }

        JasiReadingList readings;
        if (type == Phases) {
            readings = sol.phaseList;
        } else if (type == Amps) {
            readings = sol.ampList;
        } else if (type == Codas){
            readings = sol.codaList;
        } else {
            System.err.println("SelectableReadingList setSolution Unknown type: " + type);
            return;
        }

        setReadingList(readings);  // do first so we can tell if phases, amp, or codas
    }

    public void setMagnitude(Magnitude aMag) {

        mag = aMag;

        if (mag == null)  {
          summaryTextArea.setText("Null magnitude"); // clear any old stuff
          type = Unknown;
          return;
        }

        sol = mag.getAssociatedSolution();
        setReadingList((JasiReadingList) mag.getReadingList());  // do first so we can tell if phases, amp, or codas

    }

    public void setReadingList(JasiReadingList inList) {
        setReadingList(inList, true);
    }

    protected void setReadingList(JasiReadingList inList, boolean sortList) {
      try {
        if (inList == null) {
          if (jlist == null) jlist = new JList();
          jlist.setListData(new Object[0]); // null input, reset list? 10/20/2004 -aww
          type = Unknown;
        } else {
          if (inList instanceof PhaseList) {
            type = Phases;
          } else if (inList instanceof AmpList) {
            type = Amps;
          } else if (inList instanceof CodaList) {
            type = Codas;
          } else {
              System.err.println("SelectableReadingList setMagnitude unknown reading type: " +
                      inList.getClass().getName());
              type = Unknown;
              return;
          }
          // System.out.println("SelectableReadingList type: " +type+" sol: "+ (sol != null) +" mag: "+ (mag != null));

          setSummaryTextArea(type);

          if (readingList != inList) {        // a change
             // remove listener from old list so reference doesn't dangle
             if (readingList != null) {
                 //readingList.removeChangeListener(jasiReadingListListener); //aww
                 readingList.removeListDataStateListener(jasiReadingListListener); //aww
             }

             readingList = inList;

             // add THIS as a listener to the new list
             //readingList.addChangeListener(jasiReadingListListener); // aww
             readingList.addListDataStateListener(jasiReadingListListener); // aww

           }

           //readingList.timeSort();            // sort readings by time
           if (sortList && readingList.size() > 0) readingList.distanceSort(sol); // aww 06/11/2004

           // set the column header to the neatHeader
           JLabel hLabel = new JLabel(readingList.getNeatHeader());
           hLabel.setFont(getFont());
           scroller.setColumnHeaderView(hLabel);
           setVisibleList(readingList);

        }
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }

    private void setVisibleList(JasiReadingList jrl) {
        // flag state true to filter out unassoc or deleted readings
        visibleList = (JasiReadingList)jrl.getAssociatedWith(sol, hideDeleted);
        jlist.setListData(visibleList.toArray());
        setSelectedFromWFViewModel();
    }


    public void setSelectedFromWFViewModel() {
        // get selected channel from master model test for visibleList null 06/09/07 -aww
        if (mv == null ||  mv.masterWFViewModel.get() == null || visibleList == null || jlist == null) return;

        Channel selChan = mv.masterWFViewModel.get().getChannelObj();
        if (selReading != null && selChan != null && selReading.getChannelObj().equalsName(selChan)) { // ? what view problem does this block address?
           if (!alignOnlyOnFocus || jlist.hasFocus() ) { // -aww 2009/04/23 same channel 
              if (SwingUtilities.isEventDispatchThread()) {
                double centerTime = selReading.getDateTime().getTrueSeconds(); // UTC true seconds now -aww 2008/02/10
                //if (! mv.masterWFWindowModel.isFullAmp()) mv.masterWFWindowModel.setFullAmp(); // set amp scale to full scale WHY?
                if (mv.masterWFWindowModel.getCenterTime() != centerTime) mv.masterWFWindowModel.setCenterTime(centerTime);
              }
              else {
                SwingUtilities.invokeLater(
                    new Runnable() {
                        public void run() {
                            double centerTime = selReading.getDateTime().getTrueSeconds(); // UTC true seconds now -aww 2008/02/10
                            //if (! mv.masterWFWindowModel.isFullAmp()) mv.masterWFWindowModel.setFullAmp(); // set amp scale to full scale WHY?
                            if (mv.masterWFWindowModel.getCenterTime() != centerTime) mv.masterWFWindowModel.setCenterTime(centerTime);
                        }
                    }
                );
              }
            }
            return; // same channel
        }

        // will find *1st* occurance of Chan in the JList since List.indexOf(obj) does listElement.equals(obj) use:
        int newIndex = visibleList.getIndexOf(selChan); // the P-pick index when both P and S phases are present

        //if (newIndex >= 0 && newIndex < (jlist.getModel().getSize()-1)) { // > to >= 0 - aww 04/13/2005
        if (newIndex >=0 && newIndex < jlist.getModel().getSize()) { // - aww 2009/04/23
            oldIndex = jlist.getSelectedIndex();
            jlist.setSelectedIndex(newIndex);
            jlist.ensureIndexIsVisible(newIndex); // scroll up or down in viewport to reading -aww 03/13/2007
        }      
        else {
            oldIndex = -1;
            selReading = null;
            jlist.clearSelection(); // - aww 2009/04/23
        }
    }

    public void resetSummaryTextArea() {
        setSummaryTextArea(this.type);
    }

    protected void setSummaryTextArea(int type) {
        Format fb6 = new Format("%6s");
        StringBuffer sb = new StringBuffer(256);
        if (type == Phases) {
          sb.append(sol.getNeatStringHeader());
          sb.append("\n");
          sb.append(sol.toNeatString());
          sb.append("\n");
          sb.append(sol.getErrorStringHeader());
          sb.append(" ");
          sb.append(fb6.form("Zfix"));
          sb.append(" ");
          sb.append(fb6.form("Hfix"));
          sb.append(" ");
          sb.append(fb6.form("Tfix"));
          sb.append("\n");
          sb.append(sol.toErrorString());
          sb.append(" ");
          sb.append(fb6.form(sol.depthFixed.toString()));
          sb.append(" ");
          sb.append(fb6.form(sol.locationFixed.toString()));
          sb.append(" ");
          sb.append(fb6.form(sol.timeFixed.toString()));
          sb.append("\n");

          // below version could be output by solution toNeatString? sol.getSolveDate()?
          sb.append("Attempt: ").append(sol.getVersion()); // aww test 01/24/2005
          sb.append("  SrcVer: ").append(sol.getSourceVersion()); // aww test 01/08/2008
          sb.append("  Stale: ").append(sol.isStale());      // aww test 01/24/2005
          sb.append("  Bogus: ").append(sol.isDummy());      // 2011/05/11 aww 
          sb.append("  NeedsCommit: ").append(sol.getNeedsCommit());      // 10/17/2006 aww 
          sb.append("  SolveDate: "); // aww test 01/24/2005
          sb.append((sol.getSolveDate().isNull()) ? "?" : sol.getSolveDate().toDateString("MM/dd/yy HH:mm:ss"));
          sb.append("  Committed: "); // aww test 01/08/2008
          sb.append((sol.getTimeStamp().isNull()) ? "?" : sol.getTimeStamp().toDateString("MM/dd/yy HH:mm:ss"));
          sb.append("  State: "); // aww test 11/12/2008
          sb.append(sol.getProcessingStateString());
        } else if (type == Amps || type == Codas) { // must be amps or codas
          sb.append(Magnitude.getNeatStringHeader()).append("\n");
          // only if mag is null use the premag of solution     // aww      01/24/2005 
          if (mag == null) mag = sol.getPreferredMagnitude();
          sb.append(mag.toNeatString());
          sb.append("\n");
          sb.append("Attempt: ").append(mag.getVersion()); // aww test 01/08/2008
          sb.append("  Stale: ").append(mag.isStale());      // aww test 01/24/2005
          sb.append("  NeedsCommit: ").append(mag.getNeedsCommit());      // 01/08/2008 aww 
          sb.append("  SolveDate: "); // aww test 01/08/2008
          sb.append((mag.getSolveDate().isNull()) ? "?" : mag.getSolveDate().toDateString("MM/dd/yy HH:mm:ss"));
          sb.append("  Committed: "); // aww test 01/08/2008
          sb.append((mag.getTimeStamp().isNull()) ? "?" : mag.getTimeStamp().toDateString("MM/dd/yy HH:mm:ss"));
        } else {
          sb.append("Unknown Summary ReadingList type : ").append(type); 
        }
        summaryTextArea.setText(sb.toString());
    }

    /** If an "recalc" Listener is added to this component it is wired to a [Recalc]
    * button. We assume the action/button will recalc the summary mag.
    * The ActionListener is defined by the caller and passed to this class. */

    /* Alternatively we COULD recalc everytime there's a change. */
    public void addRecalcListener(ActionListener listener, String tooltip) {
        recalcListener = listener;
        if (recalcButton == null) {
            String label = "Recalc";
            Image image = IconImage.getImage("mini-recycled_dot.gif");
            recalcButton = (image == null) ? new JButton(label) : new JButton(null, new ImageIcon(image));
            recalcButton.setActionCommand(label);
            recalcButton.setAlignmentY(CENTER_ALIGNMENT);
            recalcButton.setHorizontalTextPosition(AbstractButton.CENTER);
            recalcButton.setVerticalTextPosition(AbstractButton.BOTTOM);
            recalcButton.setMargin(new Insets(0,0,0,0));
            recalcButton.setIconTextGap(0);
            recalcButton.setMaximumSize(new Dimension(20,20));
            recalcButton.setPreferredSize(new Dimension(20,20));
            recalcButton.setToolTipText(tooltip);
            topPanel.add(recalcButton, BorderLayout.EAST);
        }
        recalcButton.addActionListener(recalcListener);
    }

   /** Remove listeners from other components so we are not a memory leak. */
   public void destroy() {
       if (readingList != null) {
         //readingList.removeChangeListener(jasiReadingListListener); //aww
         readingList.removeListDataStateListener(jasiReadingListListener); //aww
       }
       readingList = null; // added below for extra insuance of GC - aww 2009/03/12

       if (mv != null && mv.masterWFViewModel != null) {
         mv.masterWFViewModel.removeChangeListener(selectedWFViewListener);
       }

       if (jlist != null) {
         ListSelectionListener[] lsl = jlist.getListSelectionListeners();
         for (int i=0; i<lsl.length; i++) {
           jlist.removeListSelectionListener(lsl[i]);
         }
       }
       jlist = null;

       mv = null;
       sol = null;
       mag = null;

   }

    /* Make the GUI - generated with JBuilder */
    private void jbInit() throws Exception {
        this.setLayout(borderLayout1);
        topPanel.setLayout(borderLayout2);

        jlist.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        // mouse action: show popup on right-click
        jlist.addMouseListener(new MouseAdapter() {

             public void mousePressed(MouseEvent e) {

               int mods = e.getModifiers();
               // popup menu on right-click
               if (e.isPopupTrigger() || (mods & MouseEvent.BUTTON3_MASK) != 0) makePopup(e.getPoint());
               /*
               else if ( ((mods & MouseEvent.BUTTON1_MASK) != 0) && ((mods & MouseEvent.SHIFT_MASK) != 0 ) ) {
                   SwingUtilities.invokeLater(
                       new Runnable() {
                           public void run() {
                               Object obj = (mv == null) ? null : mv.getOwner();
                               if (obj instanceof Jiggle) ((Jiggle) obj).selectTab(Jiggle.TAB_WAVEFORM);
                           }
                       }
                   );
               }
               */

             }
             public void mouseClicked(MouseEvent e) {
               int mods = e.getModifiers();
               if ( ((mods & MouseEvent.BUTTON1_MASK) != 0) && ((mods & MouseEvent.SHIFT_MASK) != 0 ) ) {
                   SwingUtilities.invokeLater(
                       new Runnable() {
                           public void run() {
                               Object obj = (mv == null) ? null : mv.getOwner();
                               if (obj instanceof Jiggle) ((Jiggle) obj).selectTab(Jiggle.TAB_WAVEFORM);
                           }
                       }
                   );
               }
             }
        });

        jlist.addKeyListener(new java.awt.event.KeyAdapter() {
             // event is fired on both key down and release so don't act until release
             public void keyReleased(KeyEvent e) {
               final int key = e.getKeyCode();
               // respond to [DEL] key
               if (key == KeyEvent.VK_DELETE) {
                   if (editable) deleteCurrentReading();
               }
               else if ( key == KeyEvent.VK_F && e.isControlDown() ) {
                   if (! findChannelReading() ) findChannelReading();
               }
               else {
                   SwingUtilities.invokeLater(
                       new Runnable() {
                           public void run() {
                              Object obj = (mv == null) ? null : mv.getOwner();
                              if (obj instanceof Jiggle) {
                                  if (key == KeyEvent.VK_W) {
                                       ((Jiggle) obj).selectTab(Jiggle.TAB_WAVEFORM);
                                  }
                                  else if (key == KeyEvent.VK_L) {
                                       ((Jiggle) obj).selectTab(Jiggle.TAB_LOCATION);
                                  }
                                  else if (key == KeyEvent.VK_M) {
                                       ((Jiggle) obj).selectTab(Jiggle.TAB_MAGNITUDE);
                                  }
                                  else if (key == KeyEvent.VK_T) {
                                       ((Jiggle) obj).selectTab(Jiggle.TAB_MESSAGE);
                                  }
                                  else if (key == KeyEvent.VK_C) {
                                       ((Jiggle) obj).selectTab(Jiggle.TAB_CATALOG);
                                  }
                              }
                           }
                       }
                   );
               }
             }
        });

        hideDeletedCheckBox.addItemListener( new ItemListener() {
            public void itemStateChanged(ItemEvent ie) {
                //hideDeleted = hideDeletedCheckBox.isSelected();
                hideDeleted = (ie.getStateChange() == ItemEvent.SELECTED) ? true : false;
                setVisibleList(readingList);
            }
        });
        topPanel.add(hideDeletedCheckBox, BorderLayout.SOUTH);
        topPanel.add(summaryTextArea, BorderLayout.CENTER);
        this.add(topPanel, BorderLayout.NORTH);
        this.add(scroller, BorderLayout.CENTER);
        scroller.getViewport().add(jlist, null);
    }

    public void requestListFocus() {
        if (jlist != null) jlist.requestFocusInWindow();
    }

    /**
    * Return the reading at this index in the list.
    * Returns null if no readings in list, or index is out-of-bounds.
    */
    public JasiReading getReading(int index) {
        if (visibleList.isEmpty() || index < 0 || index > visibleList.size()) return null;
        return  (JasiReading) visibleList.get(index);
    }

    /** Return the currently selected reading. Returns null if no readings in list
    or none selected.*/
    public JasiReading getSelectedReading() {
        return (JasiReading) jlist.getSelectedValue();
    }

   /*
   // sol => panel list
   protected void synchReadingListWithSolution() {
       if (sol == null) return;
       setReadingList((JasiReadingList)sol.getListFor(readingList));
       if (mag == null) return;
       mag.setReadingListFromAssocSolution();
   }
   //panel list => sol
   protected void synchSolutionWithReadingList() {
        sol.getListFor(readingList);
        solList.clear(false);
        sol.fastAssociateAll(readingList);
   }
   */

    /** Restore last deleted readings */
    private void restore() {
       if (type == Phases) {
           System.out.println("Restoring phases ...");
           mv.undoPhases(true);
       }
       else if (type == Amps) {
           System.out.println("Restoring amps ...");
           mv.undoAmps(true);
       }
       else if (type == Codas) {
           System.out.println("Restoring codas ...");
           mv.undoCodas(true);
       }
    }

    /** Undelete all readings */
    private void undeleteAllReadings() {

        if (readingList == null || readingList.size() == 0) return;
        JasiReading [] data = (JasiReading [])readingList.toArray(new JasiReading [readingList.size()]);
        for (int ii =0; ii < data.length; ii++) {
           data[ii].undelete();
        }

    }

    /** Delete all readings */
    private void deleteAllReadings() {

        if (readingList == null || readingList.size() == 0) return;
        JasiReadingList solList = (JasiReadingList) sol.getListFor(readingList);

        if (solList instanceof PhaseList) {
            mv.addToPhaseUndoList(solList, true);
            solList.eraseAll();
        }
        else if (solList instanceof AmpList) {
            mv.addToAmpUndoList(solList, true);
            solList.eraseAll();
            if (mag != null) mag.getReadingList().eraseAll();
        }
        else if (solList instanceof CodaList) {
            mv.addToCodaUndoList(solList, true);
            solList.eraseAll();
            if (mag != null) mag.getReadingList().eraseAll();
        }

        readingList.clear(); // should invoke listener update of JList

        /*
        // Remove:
        if (sol != null) {
            JasiReading [] data = (JasiReading [])readingList.toArray(new JasiReading [readingList.size()]);
            JasiReadingListIF aList = null;
            for (int ii =0; ii < data.length; ii++) {
              solList.removeAllOfSameChanType(data[ii]);
            }
        }
        readingList.clear(); // should invoke listener update of JList
        */

        /*
        // Hide:
        if (sol != null) {
            JasiReadingListIF aList = null;
            JasiReading [] data = (JasiReading [])readingList.toArray(new JasiReading [readingList.size()]);
            for (int ii =0; ii < data.length; ii++) {
              aList = solList.getAllOfSameChanType(data[ii]);
              for (int jj=0; jj < aList.size(); jj++) {
                  solList.delete(aList.get(jj)); 
              }
            }
        }
        setVisibleList(readingList);
        */
    }

    /** Delete the currently selected reading */
    private void deleteCurrentReading() {

        selReading = getSelectedReading();
        if (selReading == null) return;

        int oldIndex = jlist.getSelectedIndex();

        // List should be the same as current solution, but do below as insurance in case they are not
        JasiReadingList solList = (JasiReadingList) sol.getListFor(readingList); // -aww 2008/02/27

        if (solList instanceof PhaseList) {
            mv.addToPhaseUndoList((Phase) selReading, true);
            solList.erase(selReading);
        }
        else if (solList instanceof AmpList) {
            mv.addToAmpUndoList((Amplitude) selReading, true);
            solList.erase(selReading);
            if (mag != null) mag.getReadingList().erase(selReading);
        }
        else if (solList instanceof CodaList) {
            mv.addToCodaUndoList((Coda) selReading, true);
            solList.erase(selReading);
            if (mag != null) mag.getReadingList().erase(selReading);
        }

        /*
        // Erase:
        readingList.erase(selReading);     // aww fires event deletes and removes from list
        if (solList != readingList) solList.removeAllOfSameChanType(selReading); // -aww 2008/02/27
        */

        /*
        // Hide:
        readingList.delete(selReading);  // ? aww fires change event, virtual delete only, no remove
        if (solList != readingList) {
            JasiReadingListIF aList = solList.getAllOfSameChanType(selReading); 
            for (int jj=0; jj < aList.size(); jj++) {
                solList.delete(aList.get(jj)); 
            }
        }
        */

        // should keep "cursor" in same place but not fall off end
        int newIndex = Math.min(oldIndex, jlist.getModel().getSize()-1);
        if (newIndex > -1) jlist.setSelectedIndex(newIndex);

        setVisibleList(readingList);
    }

    /** Undelete the currently selected reading */
    private void undeleteCurrentReading() {
        getSelectedReading().setDeleteFlag(false);
    }

    /** Reject the currently selected reading */
    private void rejectCurrentReading() {
        getSelectedReading().setReject(true);
    }

    /** Unreject the currently selected reading */
    private void unrejectCurrentReading() {
        JasiReading jr = getSelectedReading();
        if (jr == null) return;
        jr.setReject(false);
    }

    /** Require the currently selected reading */
    private void requireCurrentReading() {
        JasiReading jr = getSelectedReading();
        if (jr == null) return;
        jr.require();
        //jr.setProcessingState(JasiProcessingConstants.STATE_FINAL_TAG);
    }

    /** Unrequire the currently selected reading */
    private void unrequireCurrentReading() {
        JasiReading jr = getSelectedReading();
        if (jr == null) return;
        jr.unrequire();
        //jr.setProcessingState(JasiProcessingConstants.STATE_HUMAN_TAG);
    }

    // Added masterview selected wfview model listener to set the selected reading in list - aww 02/28/2007
    public void setMasterView(MasterView mview) {
        removeListeners(mv);               // nuke old listeners
        mv = mview;
        addListeners(mv);
    }

   /** Add model listeners to this MasterView's models. */
   protected void addListeners(MasterView mv) {
       if (mv != null && mv.masterWFViewModel != null) {
           mv.masterWFViewModel.addChangeListener(selectedWFViewListener);
       }
   }
   /** Remove model listeners to this MasterView's models. */
   protected void removeListeners(MasterView mv) {
        if (mv != null && mv.masterWFViewModel != null) {
            mv.masterWFViewModel.removeChangeListener(selectedWFViewListener);
        }
    }

////////////////////////////////////////////////////////////////////////////////////
    /** React to a changes in the jasi reading list. Changes (delete, add, etc.)
    * may come from inside or outside this class. */
    class JasiReadingListListener implements  ListDataStateListener {
        /* remove ChangeListener implementation method - aww
        public void stateChanged(ChangeEvent evt) {
          // just redo the list if ANYTHING changed in the readingList
          setReadingList(readingList); // redo the whole thing
        }
        */

        //
        // ListDataStateListenerIF for list changes -aww
        //
        public void intervalAdded(ListDataStateEvent e) {
            updateListInEventQueue(true);
        }
        public void intervalRemoved(ListDataStateEvent e) {
            updateListInEventQueue(false);
        }
        public void contentsChanged(ListDataStateEvent e) {
            updateListInEventQueue(true);
        }
        public void orderChanged(ListDataStateEvent e) {
            // no-op for now
        }
        public void stateChanged(ListDataStateEvent e) {
            updateListInEventQueue(false);
        }

        private void updateListInEventQueue(final boolean sortList) {
           
          // Oops a threading issue: 
          // Choosing JiggleMenubar event menu option to erase all "auto" readings
          // causes SelectableReadingList to sort list  for each element removed,
          // but removal is happening in separate thread and change in list size
          // is causing a last index out of range exception.
          // Either do not invoke setReadingList in the EventDispatch or take option B,
          // use an override of setReadingList method to not do the distance sort for "removal" or "state" change events.
          //
          // Does it matter that this runs in EventQueue? Not sure, so taking optionB until someone can verify removing
          // the call to the "invokeLater" wrapper has no side-effect consequences with display update afterwards. -aww 2011/09/26
          SwingUtilities.invokeLater(
              new Runnable() {
                  public void run() {
                      setReadingList(readingList, sortList);
                  }
              }
          );
        }
    } // end of JasiReadingListListener

////////////////////////////////////////////////////////////////////////////////////
    class SelectionListener implements ListSelectionListener  {

        // The real work is done as a Runnable to guarantee that, even if called from another thread,
        // the selection action happens in the Event-Dispatch thread, and does not cause thread dead-locks. 
        Runnable runnable;
    
        Channel chan;
        double centerTime;
    
        WFViewList wfvList;
    
        public SelectionListener() {
            super();
    
            // insure this happens in event-dispatch thread because it fires events that do graphics updates
            runnable = new Runnable() {
                public void run() {
    
                  if (mv == null || wfvList == null) return;
    
                  WFView readingWFV = wfvList.get(chan);
                  if (readingWFV != null) { // update this WFView
                    // Kate didn't like the GUI behavior when P and S exist and P is deleted, panel jumps to S
                    // change scale and jump scroll to channel's pick -aww 04/13/2005
                    if (readingWFV != mv.masterWFViewModel.get() || mv.masterWFWindowModel.getCenterTime() != centerTime) {
                      int selIdx = jlist.getSelectedIndex();
                      if (selIdx != -1 && oldIndex != selIdx) {
                        mv.masterWFViewModel.set(readingWFV);
                        if (type == Phases) {
                            //mv.masterWFWindowModel.setFullAmp();  //TEST REMOVE for scaling test below -aww
                            Object obj = (mv == null) ? null : mv.getOwner();
                            if (obj instanceof Jiggle) {
                                //System.out.println("DEBUGGERY: Doing wfp for databox scaling....");
                                WFPanel wfp = ((Jiggle) obj).wfScroller.groupPanel.getWFPanel(readingWFV);
                                mv.masterWFWindowModel.setAmpSpan(wfp.getWf(), wfp.dataBox); // TEST noise,gain scaling -aww 2014/02/24
                            }
                            else if (! mv.masterWFWindowModel.isFullAmp()) mv.masterWFWindowModel.setFullAmp(); // set amp scale to full scale WHY?
                        }
                        else if (! mv.masterWFWindowModel.isFullAmp()) mv.masterWFWindowModel.setFullAmp(); // set amp scale to full scale WHY?

                        //if (mv.masterWFWindowModel.getCenterTime() != centerTime) mv.masterWFWindowModel.setCenterTime(centerTime);

                      }
                    }
                  }

                } // end of run()
            };
        }

        /** React to a JList selection. */
        public void valueChanged(ListSelectionEvent evt) {
             if (evt.getValueIsAdjusting()) return;   // don't react until mouseReleased

             JList list = (JList) evt.getSource();

             //System.out.println(">>> SRL valueChanged firstIdx:"+evt.getFirstIndex()+" lastIdx:"+evt.getLastIndex()+" oldIndex: " + oldIndex + " emptySelection: "+list.isSelectionEmpty());
             if (list.isSelectionEmpty()) return; // no selection - aww 2009/04/23

             selReading = getSelectedReading();

             if ((selReading != null)) {
                chan = selReading.getChannelObj();
                centerTime = selReading.getDateTime().getTrueSeconds(); // UTC true seconds now -aww 2008/02/10
                wfvList = (mv == null) ? null : mv.wfvList;

                // insures this happens in event-dispatch thread because it fires events that do graphics updates
                if (!alignOnlyOnFocus || jlist.hasFocus() || doingFind) { // added for doingFind for menu option action
                    SwingUtilities.invokeLater(runnable);  // -aww 2009/04/23
                    doingFind=false; // reset back to default if set true by "Find..." menu option -aww 2013/01/11
                }

             }
        }

    } // end of SelectionListener

////////////////////////////////////////////////////////////////////////////////////

     /** React to change in selected WFView from outside. Highlight the reading if there is one. */
    // Added 02/28/2007 -aww
    class SelectedWFViewListener implements ChangeListener {
        public void stateChanged(ChangeEvent changeEvent) {
            SwingUtilities.invokeLater(
                new Runnable() {
                    public void run() {
                        setSelectedFromWFViewModel();
                    }
                }
            );
        }
    }  // end of WFViewListener

    ////////////////////////////////////////////////////////////////////////////////////
    /** Make a renderer to make "out" amp red. */
    class SelectableListCellRenderer extends JLabel implements ListCellRenderer {

        private double getOutThreshold(int type) {
            return (type == Phases) ? outThresholdTT : outThresholdMag;
        }

        private double getWarnThreshold(int type) {
            return (type == Phases) ? warnThresholdTT : warnThresholdMag;
        }

        /** Render the component */
        public Component getListCellRendererComponent
            (JList list, Object obj, int index, boolean isSelected, boolean hasFocus) {

            if (!(obj instanceof JasiReading)) {
               setText("No Data...");
               return this;
            }

            JasiReading jreading = (JasiReading) obj;

            setText(jreading.toNeatString());
            setFont(list.getFont());
            // Every JasiReading can return a "warning level" from 0.0 - 1.0
            // indicating its "goodness"
            if (!jreading.contributes()) { // aww formerly jreading.wasUsed()
              setForeground(notUsedColor);
            } else if (jreading.getWarningLevel() >= getOutThreshold(type)) {
               setForeground(outColor);
            } else if (jreading.getWarningLevel() >= getWarnThreshold(type)) {
               setForeground(warnColor);
            } else {
               setForeground(goodColor);
            }

            // highlight selected row
            // setBackground DOES NOT WORK!
            if (isSelected) {
               setBorder(lineBorder);
               setBackground(selectedBgColor);
            } else {
               setBorder(emptyBorder);
               setBackground(cellBgColor);
            }
            setOpaque(true);
            return this;
        }
    }  // end of inner class SelectableListCellRenderer 

    /** Create the popup menu that is activated by a right mouse click */
    public void makePopup(Point point) {

        if (visibleList == null) return;

        if ( alpListener == null) alpListener = new AlteredListPopupListener();

        JMenuItem mi = null;

        JPopupMenu popup = new JPopupMenu();

        if (editable && visibleList.size() > 0) {
            mi = popup.add(new JMenuItem("Delete All"));
            mi.addActionListener(alpListener);
            popup.addSeparator();
        }
        if (editable && readingList.size() > visibleList.size()) {
            mi = popup.add(new JMenuItem("Undelete All"));
            mi.addActionListener(alpListener);
            popup.addSeparator();
        }

        boolean undo = false;
        if (type == Phases) {
            undo = (mv.phUndoList.size() > 0);
        }
        else if (type == Amps) {
            undo = (mv.amUndoList.size() > 0);
        }
        else if (type == Codas) {
            undo = (mv.coUndoList.size() > 0);
        }
        if (editable && undo) {
            mi = popup.add(new JMenuItem("Restore"));
            mi.addActionListener(alpListener);
            popup.addSeparator();
        }

        JasiReading jr = getSelectedReading();
        if (editable && jr != null) {
            if (jr.isDeleted()) {
                mi = popup.add(new JMenuItem("Undelete"));
                mi.addActionListener(alpListener);
                popup.addSeparator();
            }
            else {
                mi = popup.add(new JMenuItem("Delete"));
                mi.addActionListener(alpListener);
                popup.addSeparator();
            }

            if (jr.isReject()) {
               mi = popup.add(new JMenuItem("Unreject"));
               mi.addActionListener(alpListener);
               popup.addSeparator();
            }
            else {
                mi = popup.add(new JMenuItem("Reject"));
                mi.addActionListener(alpListener);
                popup.addSeparator();
            }

            if (jr.isFinal()) {
                mi = popup.add(new JMenuItem("Unrequire"));
                mi.addActionListener(alpListener);
                popup.addSeparator();
            }
            else {
                mi = popup.add(new JMenuItem("Require"));
                mi.addActionListener(alpListener);
                popup.addSeparator();
            }

            mi = popup.add(new JMenuItem("Strip more distant"));
            mi.addActionListener(alpListener);
            mi = popup.add(new JMenuItem("Strip more distant (all lists)"));
            mi.addActionListener(alpListener);
        }
        if (editable) {
            mi = popup.add(new JMenuItem("Strip by residual"));
            mi.addActionListener(alpListener);
            popup.addSeparator();
        }

        if (editable && jr != null) {
            mi = popup.add(new JMenuItem("Reject more distant"));
            mi.addActionListener(alpListener);
            mi = popup.add(new JMenuItem("Reject more distant (all lists)"));
            mi.addActionListener(alpListener);
        }
        if (editable) {
            mi = popup.add(new JMenuItem("Reject by residual"));
            mi.addActionListener(alpListener);
            popup.addSeparator();
        }

        if (editable && jr != null) {
            mi = popup.add(new JMenuItem("Unreject more distant"));
            mi.addActionListener(alpListener);
            mi = popup.add(new JMenuItem("Unreject more distant (all lists)"));
            mi.addActionListener(alpListener);
            popup.addSeparator();
        }

        if (visibleList.size() > 0) {
           /*
           mi = popup.add(new JMenuItem("Panel List => Solution"));
           mi.addActionListener(alpListener);
           popup.addSeparator();
           mi = popup.add(new JMenuItem("Solution List => Panel"));
           mi.addActionListener(alpListener);
           popup.addSeparator();
           */
            if (recalcListener != null) {
                mi = popup.add(new JMenuItem("Recalc"));
                mi.addActionListener( new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        recalcListener.actionPerformed(new ActionEvent(this, 0, "Recalc"));
                    }
                });
                popup.addSeparator();
            }

            if ( ulpListener == null) ulpListener =  new UnalteredListPopupListener();

            mi = popup.add(new JMenuItem("Sort by time")); // -aww 03/01/2010
            mi.addActionListener(ulpListener);
            popup.addSeparator();

            mi = popup.add(new JMenuItem("Sort by distance")); // -aww 03/01/2010
            mi.addActionListener(ulpListener);
            popup.addSeparator();

            mi = popup.add(new JMenuItem("Find..."));
            mi.addActionListener(ulpListener);
            popup.addSeparator();

            JMenu copyMenu = new JMenu("Copy...");
            if (jr != null) {
                mi = copyMenu.add(new JMenuItem("Copy selection"));
                mi.addActionListener(ulpListener);
                copyMenu.addSeparator();
            }
            mi = copyMenu.add(new JMenuItem("Copy used"));
            mi.addActionListener(ulpListener);

            if (! hideDeleted) {
                mi = copyMenu.add(new JMenuItem("Copy used+deleted"));
                mi.addActionListener(ulpListener);
            }
            popup.add(copyMenu);
            popup.addSeparator();
        }

        mi = popup.add(new JMenuItem("Close Popup"));
        mi.addActionListener(alpListener);

        //popup.show (this, point.x, point.y);
        // plot popup relative to jlist coords because that's what the mouse-click reports
        // popup has no size until painted so I can't use it's size to shift it up
        // to insure its visible and doesn't "fall off" the bottom of the screen.
        popup.show(this.jlist, point.x, point.y-(50));
        //popup.show (this.jlist, point.x, point.y-(popup.getHeight()/2));
    }

    // Inner class to handle popup menu events that do not effect location/magntide stale status 
    public class UnalteredListPopupListener implements ActionListener {

        public void actionPerformed(ActionEvent evt) {

            String action = evt.getActionCommand();

            if (action == "Close Popup") return;

            if (action.equals("Find...")) { // added menu option - aww 2013/01/11

                //requestListFocus();
                if (! findChannelReading() ) findChannelReading();

            } else if (action.equals("Copy selection")) {

               JasiReading jr = getSelectedReading();
               if (jr != null) {
                StringSelection cellData = new StringSelection(jr.toNeatString());
                getToolkit().getSystemClipboard().setContents(cellData, cellData);
               }

            } else if (action.equals("Copy used+deleted")) {

               if (visibleList != null) {
                   StringSelection cellData = new StringSelection(visibleList.toNeatString(true));
                   getToolkit().getSystemClipboard().setContents(cellData, cellData);
               }

            } else if (action.equals("Copy used")) {

               if (visibleList != null) {
                   StringSelection cellData = new StringSelection(visibleList.toNeatString(false));
                   getToolkit().getSystemClipboard().setContents(cellData, cellData);
               }

            } else if (action.equals("Sort by distance")) {

                readingList.distanceSort(sol); // aww 03/01/010
                setVisibleList(readingList);

            } else if (action.equals("Sort by time")) {

                readingList.timeSort(); // aww 03/01/2010
                setVisibleList(readingList);

            }

            repaint();
        }
    } // end of inner class UnalteredListPopupListener 
 
    // Inner class to handle popup menu events that may alter magnitude/solution stale status
    public class AlteredListPopupListener implements ActionListener {

        public void actionPerformed(ActionEvent evt) {

            String action = evt.getActionCommand();

            if (action == "Close Popup") return;

            //int index = jlist.getSelectedIndex();
            //if (index < 0) return;

           if (action.equals("Delete All")) {

              if ( JOptionPane.showConfirmDialog(null, action, "Confirm Action", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                deleteAllReadings();
              }

            } else if (action.equals("Delete")) {

                deleteCurrentReading();

            } else if (action.equals("Undelete")) {

                undeleteCurrentReading();

            } else if (action.equals("Undelete All")) {

                undeleteAllReadings();

            } else if (action.equals("Restore")) {

                restore();

            } else if (action.equals("Reject")) {

                rejectCurrentReading();

            } else if (action.equals("Unreject")) {

                unrejectCurrentReading();

            } else if (action.equals("Require")) {

                requireCurrentReading();

            } else if (action.equals("Unrequire")) {

                unrequireCurrentReading();

            } else if (action.equals("Strip by residual")) {

                if (type == Phases) {
                  mv.stripPhasesByResidual(mv.getSelectedSolution());
                } else if (type == Amps) {
                  mv.stripAmpsByResidual(mv.getSelectedSolution());
                } else if (type == Codas) {
                  mv.stripCodasByResidual(mv.getSelectedSolution());
                }

            } else if (action.equals("Strip more distant")) {

                JasiReading jreading = getSelectedReading();
                if (jreading == null) return;
                //double cutoff = jreading.getDistance(); // don't use slant aww 06/11/2004?
                double cutoff = jreading.getHorizontalDistance(); // aww 06/11/2004

                if (type == Phases) {
                  mv.stripPhasesByDistance(cutoff, mv.getSelectedSolution());
                } else if (type == Amps) {
                  mv.stripAmpsByDistance(cutoff, mv.getSelectedSolution());
                } else if (type == Codas) {
                  mv.stripCodasByDistance(cutoff, mv.getSelectedSolution());
                }

            } else if (action.equals("Strip more distant (all lists)")) {

                JasiReading jreading = getSelectedReading();
                if (jreading == null) return;

                double cutoff = jreading.getHorizontalDistance(); // aww 06/11/2004

                if (sol != null) {
                  mv.stripPhasesByDistance(cutoff, sol);
                  mv.stripAmpsByDistance(cutoff, sol);
                  mv.stripCodasByDistance(cutoff, sol);
                }

            } else if (action.equals("Reject by residual")) {

                if (type == Phases) {
                  mv.rejectPhasesByResidual(mv.getSelectedSolution());
                } else if (type == Amps) {
                  mv.rejectAmpsByResidual(mv.getSelectedSolution());
                } else if (type == Codas) {
                  mv.rejectCodasByResidual(mv.getSelectedSolution());
                }

            } else if (action.equals("Reject more distant") || action.equals("Unreject more distant")) {

                boolean tf = action.startsWith("Reject");

                JasiReading jreading = getSelectedReading();
                if (jreading == null) return;

                //double cutoff = jreading.getDistance(); // don't use slant aww 06/11/2004?
                double cutoff = jreading.getHorizontalDistance(); // aww 06/11/2004

                if (type == Phases) {
                  mv.rejectPhasesByDistance(cutoff, mv.getSelectedSolution(), tf);
                } else if (type == Amps) {
                  mv.rejectAmpsByDistance(cutoff, mv.getSelectedSolution(), tf);
                } else if (type == Codas) {
                  mv.rejectCodasByDistance(cutoff, mv.getSelectedSolution(), tf);
                }

            } else if (action.equals("Reject more distant (all lists)") || action.equals("Unreject more distant (all lists)")) {

                boolean tf = action.startsWith("Reject");

                JasiReading jreading = getSelectedReading();
                if (jreading == null) return;

                double cutoff = jreading.getHorizontalDistance(); // aww 06/11/2004

                if (sol != null) {
                  mv.rejectPhasesByDistance(cutoff, sol, tf);
                  mv.rejectAmpsByDistance(cutoff, sol, tf);
                  mv.rejectCodasByDistance(cutoff, sol, tf);
                }
            }

            /*
            else if (action.equals("Solution List => Panel")) {
                synchReadingListWithSolution();
            } else if (action.equals("Panel List => Solution")) {
                synchSolutionWithReadingList();
            }
            */

            Object obj = (mv == null) ? null : mv.getOwner();
            if (obj instanceof Jiggle) {
                ((Jiggle) obj).updateStatusBarCounts();
            }
            
            resetSummaryTextArea();

            repaint();
        }
    } // end of inner class AlteredListPopupListener 

    boolean findChannelReading() {
              boolean retVal = true;
              String str = JOptionPane.showInputDialog(null, "Enter NET.STA.CHL.LOC");
              if (str != null && str.length() > 0) {
                Channelable mychan = visibleList.getChannelableStartingWith(str); // may b
                if (mychan != null) {
                  int newIndex = visibleList.getIndexOf(mychan);
                  //System.out.println("DEBUG newIndex: " + newIndex + " jlist.size: " + jlist.size());
                  if (newIndex >= 0) {
                    // NOTE: jlist has no input focus, so masterview selected WFV is updated by runnable if we toggle doingFind=true 
                    doingFind=true; // so masterview selected WFV is updated by runnable -aww 2013/01/11
                    jlist.setSelectedIndex(newIndex);
                    jlist.ensureIndexIsVisible(newIndex);
                  }
                  else {
                    JOptionPane.showMessageDialog(null, str+ " list index: " + newIndex + " not found"); 
                    retVal = false;
                  }
                }
                else {
                    JOptionPane.showMessageDialog(null, str+ " : not found in list"); 
                    retVal = false;
                }
              }
              return retVal;
    }


    public static String toPropertyString() {
        Format f = new Format("%5.2f");
        StringBuffer sb = new StringBuffer(1024);

        sb.append("color.readings.cell.bg=" + Integer.toHexString(cellBgColor.getRGB())).append("\n");
        sb.append("color.readings.cell.bg.selected=" + Integer.toHexString(selectedBgColor.getRGB())).append("\n");
        sb.append("color.readings.cell.border=" + Integer.toHexString(borderColor.getRGB())).append("\n");
        sb.append("color.readings.cell.good=" + Integer.toHexString(goodColor.getRGB())).append("\n");
        sb.append("color.readings.cell.notUsed=" + Integer.toHexString(notUsedColor.getRGB())).append("\n");
        sb.append("color.readings.cell.outlier=" + Integer.toHexString(outColor.getRGB())).append("\n");
        sb.append("color.readings.cell.warn=" + Integer.toHexString(warnColor.getRGB())).append("\n");

        sb.append("color.readings.list.bg=" + Integer.toHexString(listBgColor.getRGB())).append("\n");

        sb.append("color.readings.summary.bg=" + Integer.toHexString(summaryBgColor.getRGB())).append("\n");
        sb.append("color.readings.summary.fg=" + Integer.toHexString(summaryFgColor.getRGB())).append("\n");

        sb.append("threshold.outlier.magResidual=" + f.form(outThresholdMag).trim()).append("\n");
        sb.append("threshold.outlier.ttResidual=" + f.form(outThresholdTT).trim()).append("\n");
        sb.append("threshold.warn.magResidual=" + f.form(warnThresholdMag).trim()).append("\n");
        sb.append("threshold.warn.ttResidual=" + f.form(warnThresholdTT).trim()).append("\n");

        return sb.toString();
    }

    public static void fromProperties(JiggleProperties props) {
        Color c = props.getColor("color.readings.cell.bg");
        if (c != null) cellBgColor = c;

        c = props.getColor("color.readings.cell.bg.selected");
        if (c != null) selectedBgColor = c;

        c = props.getColor("color.readings.cell.border");
        if (c != null) borderColor = c;

        c = props.getColor("color.readings.cell.good");
        if (c != null) goodColor = c;

        c = props.getColor("color.readings.cell.notUsed");
        if (c != null) notUsedColor = c;

        c = props.getColor("color.readings.cell.outlier");
        if (c != null) outColor = c;

        c = props.getColor("color.readings.cell.warn");
        if (c != null) warnColor = c;

        c = props.getColor("color.readings.list.bg");
        if (c != null) listBgColor = c;

        c = props.getColor("color.readings.summary.bg");
        if (c != null) summaryBgColor = c;

        c = props.getColor("color.readings.summary.fg");
        if (c != null) summaryFgColor = c;

        outThresholdTT = props.getDouble("threshold.outlier.ttResidual", outThresholdTT);
        warnThresholdTT = props.getDouble("threshold.warn.ttResidual", warnThresholdTT); 

        outThresholdMag = props.getDouble("threshold.outlier.magResidual", outThresholdMag);
        warnThresholdMag = props.getDouble("threshold.warn.magResidual", warnThresholdMag); 
    }

    public static void toProperties(JiggleProperties props) {

        Format f = new Format("%5.2f");

        props.setProperty("color.readings.cell.bg", cellBgColor); 
        props.setProperty("color.readings.cell.bg.selected", selectedBgColor);
        props.setProperty("color.readings.cell.border", borderColor); 
        props.setProperty("color.readings.cell.good", goodColor);
        props.setProperty("color.readings.cell.outlier", outColor); 
        props.setProperty("color.readings.cell.notUsed", notUsedColor);
        props.setProperty("color.readings.cell.warn", warnColor);

        props.setProperty("color.readings.list.bg", listBgColor);

        props.setProperty("color.readings.summary.bg", summaryBgColor);
        props.setProperty("color.readings.summary.fg", summaryFgColor);

        props.setProperty("threshold.outlier.ttResidual", f.form(outThresholdTT).trim());
        props.setProperty("threshold.outlier.magResidual", f.form(outThresholdMag).trim());
        props.setProperty("threshold.warn.ttResidual", f.form(warnThresholdTT).trim()); 
        props.setProperty("threshold.warn.magResidual", f.form(warnThresholdMag).trim()); 
    }

/*
  public static final class Tester {
     public static void main(String args[]) {

        JFrame frame = new JFrame("Main");
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {System.exit(0);}
          });

        int evid;

        if (args.length <= 0) {
          System.out.println ("Usage: java this-thing [evid])");

          evid = 13940452;

          System.out.println ("Using evid "+ evid+" as a test...");

        } else {

          Integer val = Integer.valueOf(args[0]);    // convert arg String to 'double'
          evid = (int) val.intValue();
        }
        BenchMark bm = new BenchMark();

        System.out.println ("Making connection...");
        DataSource init = TestDataSource.create();  // make connection
        init.setWriteBackEnabled(true);
        //System.out.println ("Reading master channel list...");
        //MasterChannelList.set(ChannelList.readCurrentList());

        System.out.println ("Getting data for evid = "+evid);

        MasterView mv = new MasterView();
        //org.trinet.jiggle.MasterView mv = new org.trinet.jiggle.MasterView();

        mv.setWaveformLoadMode(mv.LoadNone);
//        mv.setWaveformLoadMode(mv.Cache);
//        mv.setCacheSize(50, 50);

        mv.setLoadChannelData(true);
        mv.setAlignmentMode(MasterView.AlignOnTime);

        mv.defineByDataSource(evid);

        Solution sol = (Solution) mv.solList.getSelected();

        mv.distanceSort(sol);

        System.out.println ("Number of phases = "+sol.getPhaseList().size());

        bm.printTimeStamp("BenchMark: event parameters loaded ");
        bm.reset();

        final WFScroller wfScroller = new WFScroller(mv, true) ;            // make scroller
        //final WFScroller wfScroller = new WFScroller(mv, false) ;            // make scroller

        mv.masterWFViewModel.selectFirst(mv.wfvList);

              // make an empth Zoomable panel
        ZoomPanel zpanel = new ZoomPanel(mv);

        wfScroller.setMinimumSize(new Dimension(300, 100) );
        // make a split pane with the zpanel and wfScroller
         JSplitPane split =
            new JSplitPane(JSplitPane.VERTICAL_SPLIT,
                 false,       // don't repaint until resizing is done
                 zpanel,      // top component
                 wfScroller); // bottom component


        //frame.getContentPane().add(list);
        SelectableReadingList plist = new SelectableReadingList(mv, sol, sol.phaseList);
        SelectableReadingList alist = new SelectableReadingList(mv, sol, sol.ampList);
        SelectableReadingList clist = new SelectableReadingList(mv, sol, sol.codaList);

        JTabbedPane tabPane = new JTabbedPane();
        tabPane.addTab("Phases", plist);
        tabPane.addTab("Amplitudes", alist);
        tabPane.addTab("Codas", clist);

//        frame.getContentPane().add(wfScroller, BorderLayout.CENTER);
        frame.getContentPane().add(split, BorderLayout.CENTER);
        frame.getContentPane().add(tabPane, BorderLayout.SOUTH);

        frame.pack();
        frame.setVisible(true);

    }
  } // end of Tester
*/
}
