package org.trinet.jiggle;

import org.trinet.util.*;
import org.trinet.jasi.*;

/**
 * WFDataBox.java extends WFSelectionBox. Box adapts to changes of the
 * Waveform object.  The timespan and amp max/min values are not independently
 * settable. The getXXX() methods will always return the values determined from
 * the current Waveform data. This is necessary to insure that the correct value is
 * given even when the waveform is changed (or loaded for the first time).
 */

public class WFDataBox extends WFSelectionBox {
    
    Waveform wf = null;

    public static final int SCALE_BY_DATA = 0;
    public static final int SCALE_BY_NOISE = 1;
    public static final int SCALE_BY_GAIN = 2;
    public static final int SCALE_BY_UNITS = 3;

    protected int scaling = SCALE_BY_DATA;

    protected double noise = -1.;
    protected String lastNoiseFilter = null;
    protected Waveform lastWf = null;

    public static double NOISE_SCALAR = 10.;
    public static double NOISE_SCAN_SECS = 6.;

    protected double gainf = -1.;
    public static double MAX_ACC_GAIN = 10.; // 10/cmss
    public static double MAX_VEL_GAIN = 0.1; // 0.1 cms

    protected double unitsf = Double.NaN;
    protected static double MAX_VEL_UNITS = .001;
    protected static double MAX_ACC_UNITS = .1;
    protected static double MAX_DIS_UNITS = .01;
    protected static double MAX_CNT_UNITS = 2048.;
    protected static double MAX_UNK_UNITS = 1.;

    public WFDataBox() {
        
    }
    
    public WFDataBox(Waveform wf) {

        set(wf);
    }

    public WFDataBox(TimeSpan ts, int ampMax, int ampMin )
    {
        super(ts, ampMax, ampMin);
    }

    public WFDataBox( WFSelectionBox sb )
    {
        super(sb);
    }

    public void set(Waveform wf) {

        this.wf = wf;
        if (wf == null) return;

        if ( (scaling == SCALE_BY_NOISE) && wf.hasTimeSeries() && (wf != lastWf || !wf.getFilterName().equals(lastNoiseFilter)) ) {
            noise = wf.scanForNoiseLevel(wf.getEpochStart(), wf.getEpochStart()+NOISE_SCAN_SECS);
            lastNoiseFilter = wf.getFilterName(); 
            lastWf = wf;
            //System.out.printf("DEBUG WFDataBox set(wf) " + wf.getChannelObj().toDelimitedSeedNameString() + 
            //        " filter: " + wf.getFilterName() + " noise: %9.0f bias: %10.0f\n", noise, wf.getBias());
        }
        else if ( (scaling == SCALE_BY_GAIN) && !wf.equalsChannelId(lastWf) ) {
            gainf = -1.;
            if (wf.getAmpUnits() == Units.COUNTS) {
                DateTime dt = new DateTime(wf.getEpochStart(), true);
                ChannelGain gain = wf.getChannelObj().getGain(dt, Channel.getDbLookup());
                if (!gain.isNull()) {
                  //double gn = Units.getCountsCGS(gain.doubleValue(), gain.getUnits());
                  double gn = Units.convertFromCommon(gain.doubleValue(), ChannelGain.getResultingUnits(gain));
                  //System.out.print(wf.getChannelObj().toDelimitedSeedNameString()+
                  //" gain:" + gain.doubleValue() + " units:" + Units.getString(ChannelGain.getResultingUnits(gain)));
                  if (!Double.isNaN(gn)) {
                    if (gain.isAcceleration()) {
                      gainf = gn * MAX_ACC_GAIN;
                      //System.out.println(" bias="+ wf.getBias() + " gn:" + gn + " ACC:" + MAX_ACC_GAIN);
                    }
                    else if (gain.isVelocity()) {
                      gainf = gn * MAX_VEL_GAIN;
                      //System.out.println(" bias="+ wf.getBias() + " gn:" + gn + " VEL:" + MAX_ACC_GAIN);
                    }
                  }
                }
                if (Double.isNaN(gainf) || gainf <= 0) {
                    System.out.println("WARNING: " + wf.getChannelObj().toDelimitedSeedNameString()+ " gain scale is unknown !");
                    gainf = 1.;
                }
                lastWf = wf;
            }
        }
        else if ( (scaling == SCALE_BY_UNITS) && !wf.equalsChannelId(lastWf) ) {
            unitsf = -1.;
            String tag = wf.getAmpUnitsTag();
            if (tag.equals("CNT")) unitsf = MAX_CNT_UNITS;
            else if (tag.equals("VEL")) unitsf = MAX_VEL_UNITS;
            else if (tag.equals("ACC")) unitsf = MAX_ACC_UNITS;
            else if (tag.equals("DIS")) unitsf = MAX_DIS_UNITS;
            if (Double.isNaN(unitsf) || unitsf <= 0) {
                    System.out.println("WARNING: " + wf.getChannelObj().toDelimitedSeedNameString()+ " units scale is unknown: " + tag);
                    unitsf = MAX_UNK_UNITS;
            }
            lastWf = wf;
        }
    }
    
    public void setScaling(int flag) {
        scaling = flag;
    }

/**
 * Return the associated waveform's time span (start/end).
 * Returns an empty TimeSpan instance if there is no waveform.
 */
public TimeSpan getTimeSpan()
{
    if (wf == null) return new TimeSpan();
    return (wf.getTimeSpan());
}

/**
 * Return the associated waveform's maximum amplitude in sample counts.
 * Returns 0 if there is no waveform.
 */
public double getMaxAmp()
{
    if (wf == null) return 0.0;
    double val = wf.getMaxAmp();
    if (scaling == SCALE_BY_NOISE) {
       if (Double.isNaN(noise) || noise <= 0.) return val;
       val = wf.getBias() + NOISE_SCALAR*noise;
    }
    else if (scaling == SCALE_BY_GAIN) {
        if (Double.isNaN(gainf) || gainf <= 0.) return val;
        val = wf.getBias() + gainf;
    }
    else if (scaling == SCALE_BY_UNITS) {
        if (Double.isNaN(unitsf) || unitsf <= 0.) return val;
        val = wf.getBias() + unitsf;
    }

    return val;
}

/**
 * Return the associated waveform's minimum amplitude in sample counts. 
 * Returns 0 if there is no waveform.
 */
public double getMinAmp()
{
    if (wf == null) return 0.0;
    double val = wf.getMinAmp();
    if (scaling == SCALE_BY_NOISE) {
       if (Double.isNaN(noise) || noise == 0.) return val;
       val = wf.getBias() - NOISE_SCALAR*noise;
    }
    else if (scaling == SCALE_BY_GAIN) {
        if (Double.isNaN(gainf) || gainf <= 0.) return val;
        val = wf.getBias() - gainf;
    }
    else if (scaling == SCALE_BY_UNITS) {
        if (Double.isNaN(unitsf) || unitsf <= 0.) return val;
        val = wf.getBias() - unitsf;
    }

    return val;
}

    public int getScaling() {
        return scaling;
    }

    public String getScalingDesc() {
        return getScalingDesc(scaling);

    }

    public static String getScalingDesc(int scaling) {
        if (scaling == 0) return "data";
        else if (scaling == 1) return "noise";
        else if (scaling == 2) return "gain";
        else if (scaling == 3) return "units";
        return "unknown";
    }

} // WFDataBox
