// WFPanel.java
package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.util.HashMap;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.event.*;

import org.trinet.jasi.*;
import org.trinet.jiggle.common.PropConstant;
import org.trinet.util.*;
import org.trinet.util.graphics.ColorList;

/**
 * This class it the graphical representation of one WView in a panel.
 * The size of the WF is scaled to the size of the panel.
 * If it is not explicitely set, the panel size is set by the
 * layout manager.<p>
 *
 * Class allows the WFPanel to have an alternateWf. This is a second version
 * of the contained waveform and ususally represents a filtered time series.
 *
 * Has limited mouse support for finding out where you are in the trace.
 * You must extend this class and add mouse handelers to make it dynamic.
 * @see: ActiveWFPanel()
 * @see: ZoomedWFPanel()
 */

public class WFPanel extends JPanel implements Scrollable  {

    public static final String FILTER_CHANGED = "filterChanged";

    static int CHANNEL_LABEL_MODE = 0;
    static final Color Ecolor = new Color(Integer.parseInt("0000ca",16)); // darkblue // E
    static final Color Zcolor = new Color(Integer.parseInt("ca0000",16));  //darkred // Z
    static final Color Ncolor = new Color(Integer.parseInt("339900",16)); // darkgreen // N

    static Font embossedFont = null;
    static Color embossedFontColor = null;
    static String embossedLabelForm = "filter";

    HashMap plotTriaxialMap = new HashMap(3);
    Color triaxialColor = Color.black;
    //Color [] triaxialColors = new Color [] { Color.gray, Color.lightgray, Color.darkgray}
    boolean showTriaxials = false;
    ArrayList triaxialList = new ArrayList();

    double ampScaleFactor = 1.0; // for WFScroller -aww 2009/03/26

    /** Keep track of selected status.
     * If selected highlight with the selectedColor.
     */
    boolean amSelected = false;

    // Static default colors
    static Color selectedColor     = ColorList.skyBlue;
    static Color highlightColor    = Color.yellow;
    static Color dragColor         = Color.orange;
    static Color segmentEdgeColor  = Color.cyan;
    static Color unselectedColor   = ComponentColor.DEFAULT_BG;
    //static Color alternateUnselectedColor = ColorList.aliceBlue; //slight gray

    public static int codaFlagMode = CodaFlag.Free;

    // Actual colors
    Color wfColor           = Color.black;        // default colors
    Color textColor         = Color.black;
    Color backgroundColor   = unselectedColor;
    Color defaultScaleColor = Color.black;

    /** Allow display of an alternate (filtered) wf. */
    boolean showAlternateWf = false;
    /** The filter used to produce the alternate waveform. */
    FilterIF filter = null;
    boolean isTGMFilter = false; // only for debugging TotalGroundMotionFilter versus RSAFilter output in ZoomPanel

    /** The alternate waveform. */
    Waveform alternateWf = null;

    // aww added method and instance data member attribute for listener below
    ChangeListener wfLoadListener = null; // aww, one per instance

    /** The row header for WFPanels in scollers. */
    private WFRowHeader rowHeader = new WFRowHeader(this);

    /** the waveform view to be painted. */
    public WFView wfv = null;  // the waveform view to be painted

    /** Master view of the data set*/
    //MasterView mv ;   // should be same as static WFView mv right ? -aww


    /** Initial scale factor */
    private double ScaleFacX = 1.0;
    /** Initial scale factor */
    private double ScaleFacY = 1.0;

    boolean paintCursorTimeLine = true;
    boolean paintCursorAmpLine = false;

    double cursorTime = Double.NaN;
    double cursorAmp = Double.NaN;

    boolean showPickFlags = true;

    // 0 = both top,bottom 1= top only 2= bottom only, -1 none
    int timeTicksFlag = 0;

    /** If a time scale is plotted, controls if time labels are printed */
    boolean showTimeLabel = true;

    /** Controls if a time scale is plotted in the panel */
    boolean showTimeScale = true;

    /** Show or don't the WFSegment boundaries in the view */
    boolean showSegments = false;

    /** Controls if an amp scale is plotted in the panel */
    boolean showAmpScale = true;

    /** If true, show pick descriptions on pick marker flags. */
    boolean showPhaseDescriptions = true;

    /** If true, show pick deltatimes on pick marker flags. */
    boolean showDeltaTimes = true;

    /** If true, show pick residuals on pick marker flags. */
    boolean showResiduals = true;

    /** If true, show channel label. */
    boolean showChannelLabel = true;

    /** If true show a colored stripe (PhaseCue) where P an S should be. */
    boolean showPhaseCues = false;

    PhaseCue cue = new PhaseCue();

    /** show samples as red |'s if scale is expanded sufficiently */
    boolean showSamples = true;

    /** Show a red vertical line from median line to sample*/
    boolean showSampleBars = true;

    /** If true, antialias the waveform */
    boolean  antialiasing = false;

    // Optional viewport: to let paint() know limits of visible panel. This is used to insure
    // that PickMarkers, etc. are visible where WFPanel is expanded
    //  private JViewport vport;

    // scaling values
    double pixelsPerSecond;
    double pixelsPerCount;

    double         transY;           // this is needed to convert pixel position to amp

    // reduce instatiation overhead...
    double pixelInterval, xPosition;

    // for dynmic displays of current cursor location
    Point cursorLoc = new Point();

    public boolean continuousSelection = false;

    int initPanelWidth  = 300;        // default size (just temporary, layout manager
    int initPanelHeight = 100;        // will adjust this)

  //  static final Font fontBold = new Font("Monospaced", Font.BOLD, 12);
    static final DecimalFormat df1 = new DecimalFormat ( "###0.0" );  // distance format
    static final DecimalFormat df0 = new DecimalFormat ("#######0");  // amp format

    /** Time and amp bounds of the actual data in the WFView */
    WFDataBox dataBox = new WFDataBox();

    /** Time and amp bounds of this WFPanel
     *  (often longer then the dataBox to align traces in the
     *  MultiPanel or smaller if the trace is zoomed )*/
    WFSelectionBox panelBox = new WFSelectionBox();

/* Explanation of view areas used here.
Each "box" defines a 2-D area in time-amp space. Time is in seconds and amp is
in counts. These boxes are used to define and map time-amp areas into graphical
areas defined by 2-D boxes defined in terms of pixels in Height and Width.

We use 3 WFSelectionBox'es:
        1) dataBox - defines the extent of the waveform data in time and amp.
           If there is no waveform this is null as tested by 'dataBox.isNull()'
        2) panelBox - defines the time-amp bounds of this WFPanel. Normally it is
           the same as the dataBox for a zoomPanel. For a WFPanel in a
           GroupPanel the panelBox time bounds will usually be equal to the
           exteme bounds of the ViewList. This insures all data is visable and
           time aligned. For dataless WFPanels the amp dimension is problematic
           and is arbitrarily set to +100/-100 counts.
        3) viewBox - defines the bounds of the view. For example, in a zoomed
           panel the viewbox represents the JViewport and the WFPanel expands
           to accomodate the scaling of the viewBox to the JViewport. This is
           only used by ActiveWFPanels.


     panelBox
    +===================================================================+
    |                                                                   |
    |                                +-----------------+                |
    |                                |dataBox          |                |
    |                                |                 |                |
    |                                +-----------------+                |
    |                                                                   |
    +===================================================================+

*/

    /**
    * Constructors
    */
    public WFPanel() {
        setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));  // default JComponent max is Short.MAX_VALUE=32767 pixels -aww 2010/01/25
        setBackground(unselectedColor);
        setForeground(wfColor);
        setSize(initPanelWidth, initPanelHeight);
        setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
//        addWFLoadListener();
       // Need this to rescale panel drawing pixels when size changes 
        addComponentListener(
                new ComponentAdapter() {
                    public void componentResized(ComponentEvent e) {
                        //System.out.println(">>>> WFPANEL componentResized call setPanelBoxSize() <<<<");
                        setPanelBoxSize();
                        // repaint();
                    }
                }
       );
       resetPlotTriaxialMap();
       //
    }

    public WFPanel(WFView wfview) {
        this();
        setWFView(wfview);
    }

// -------------------------------------------------------------------

    /** Return true if this panel is selected. */
    public boolean isSelected() {
      return amSelected;
    }

    /** Set or change the WFView to be displayed in this WFPanel.  */
    public void setWFView(WFView wfview) {

        removeWFLoadListener(); // aww see added method below

        // System.out.println("DEBUG WFPanel setWFView view:" + ((wfview == null) ? "null" : wfview.toString()) );
        wfv = wfview;
        if (wfv == null) {
          clear();
          return;        // signal to GC to erase old view
        }


        // add change listener to waveform object to handle async. load/unloading
        addWFLoadListener();

        wfColor = ComponentColor.getForeground(wfv.getChannelObj());        // get proper color for this component

        setupWf();

    }

    protected void resetWfColor() {
        wfColor = ComponentColor.getForeground(wfv.getChannelObj());        // get proper color for this component
        if (isVisible()) {
          Graphics g = this.getGraphics();
          //paintTimeSeries(g);
          paintBackground(g);
          paintContents(g);
          paintForeground(g);
        }
    }

    // // -aww 2010/01/24
    public void clearTriaxials() {
        //oldPeak = null;
        triaxialList.clear();
        showTriaxials = false;
    }
    //Sample oldPeak = null;
    public void addTriaxialWf(Waveform wf) {
        Waveform newWf = wf; // assume input wf is already a copy (i.e. wf.copy());
        newWf.demean();
        //System.out.println("WFPanel addTriaxialWf: " + wf.toString());
        newWf.scanForAmps();
        triaxialList.add(newWf);
        //Sample sample = newWf.scanForPeak();
        //if (oldPeak == null || Math.abs(sample.value) > Math.abs(oldPeak.value)) {
          //oldPeak = sample;
          //dataBox.set(getWf());                // a null wf is OK here
          //setPanelBoxSize(); // -aww 2010/01/24 (does panelBox.set(wfv.viewSpan, wf.getMaxAmp(), wf.getMinAmp());)
          /*
          if (wf.getMinAmp() < panelBox.getMinAmp() || wf.getMaxAmp() > panelBox.getMaxAmp()) {
              panelBox.setAmpSpan(wf.getMaxAmp(), wf.getMinAmp());
              scalePanel();
              revalidate();
          }
          repaint();
          */
        //}
    }
    // // -aww 2010/01/24

    /** Set data and panel box sizes and creates alternate (filtered) waveform if a filter exists.
     *  Which will actually be displayed is toggled with setShowAlternateWf(). */
    public void setupWf() {
        // Below create must be done when filter is set to show the correct filtered waveform when wfv or filter changed in panel
        // Logic block for alternate creation below changed, only create if filter on, else unload -aww 2010/07/06 
        if (showAlternateWf) {
          if (! isTGMFilter) createAlternateWf(); // added a menu line for TotalGroundMotionFilter in ZoomPanel - aww
        }
        else if (alternateWf != null) {
            alternateWf.unloadTimeSeries();
            alternateWf = null;
        }
        clearTriaxials(); // -aww 2010/01/24
        // set box bounds based on the data
        dataBox.set(getWf());                // a null wf is OK here
        setDefaultText();
        setPanelBoxSize();

        setRowHeaderBackground();
    }

        /** Set the text in tooltip and header label */
        void setDefaultText() {
          if (wfv == null || wfv.getChannelObj() == null) { // this should never happen
            rowHeader.setChannelLabels("?", "?","?", "?");
            rowHeader.setToolTipText(null);
            rowHeader.distLabel.setText("");
          } else {
            rowHeader.setChannelLabels();
            updateToolTip();
          }
        }

        void updateToolTip() {
            // create 1st part of tooltip
            String str = wfv.getChannelObj().toDelimitedSeedNameString(" ") +
                  " " + df1.format(wfv.getHorizontalDistance()) + " Km" +
                  " " + df0.format((int)Math.round(wfv.getAzimuth())) + "\u00B0"; // aww 2010/07/15

            // if there's a waveform add that summary info to end of string
            if (getWf() != null) {
              Waveform wf = getWf();
              str += " ("+df0.format(wf.getBias())+", "+df0.format(wf.getMinAmp())+"/"+df0.format(wf.getMaxAmp()) + ") ";
                  //+wf.getAmpUnits();
            }
            setToolTipText(str);
            if (wfv.getComment() != null) str += wfv.getComment();
            rowHeader.setToolTipText(str);
            rowHeader.distLabel.setText( "   " + df1.format(wfv.getHorizontalDistance()) + " Km" );
        }


        protected void setRowHeaderBackground() {
            if (wfv == null) return;
            Color c = Color.darkGray;
            if (wfv.isSelected()) {
                c = (Color.pink); // trumps
                updateToolTip();
            }
            else {
              AbstractWaveform wf = (AbstractWaveform) getWf();
              if (wf != null) {
                if (wf.isClipped()) c = (Color.red); // trumps filtered
                else if (wf.isFiltered()) c = (Color.black);
              }
            }
            rowHeader.setBackground(c);
        }

    /** Create and setup the alternate (filtered) waveform. */
    protected Waveform createAlternateWf() {

        if (alternateWf != null) alternateWf.unloadTimeSeries(); // cleanup for GC -aww 2009/03/26

        if (wfv != null) {
            if ( alternateWf != null && !wfv.getChannelObj().equalsName(alternateWf.getChannelObj()) ) {
                alternateWf = null;
            }
        }

        if (filter != null && getRawWf() != null) {
            alternateWf = AbstractWaveform.copyWf(getRawWf());
        }
        else {
            alternateWf = null;
        }
        if (alternateWf == null) return null;

        // filter the time series of the new waveform
        int rate = (int) alternateWf.getSampleRate();
        //System.out.println("WFPanel createAlternateWf filter: " + filter.getDescription() + " alternate rate: " + rate);
        if (rate > 0) {
            FilterIF f = filter.getFilter(rate);
            alternateWf = alternateWf.filter(f);
            //System.out.println("WFPanel createAlternateWf alternate isFiltered: " +
            //alternateWf.isFiltered() +" rate: " + rate + "\n" + f.toString());
        }

        return alternateWf;

    }
    /**
     * Add the listener that handles waveform load events. This is to support
     * asynchronous loading of timeseries in background.  */
    //    private void addWFLoadListener() {
    protected void addWFLoadListener() {
        removeWFLoadListener(); // aww
        if (getRawWf() != null) {
          if (wfLoadListener == null) {
            wfLoadListener = new WFLoadListener(); // aww
          }
          getRawWf().addChangeListener(wfLoadListener); //aww
        }
    }

    protected void removeWFLoadListener() {
        if (getRawWf() != null) {
            if (wfLoadListener != null) {
              getRawWf().removeChangeListener(wfLoadListener);
            }
        }
    }

    /** INNER CLASS: Change events are expected from Waveform objects when the
     *  timeseries is loaded. Repaint the WFPanel when timeseries shows up.
     *  This is to support asynchronous loading of timeseries in background. */
    private class WFLoadListener implements ChangeListener {
        public void stateChanged(ChangeEvent changeEvent) {
            if (SwingUtilities.isEventDispatchThread()) {
                        setupWf(); // Waveform.unloadTimeSeries() changed to fire ChangeEvent -aww 2009/03/05
            }
            else {
              SwingUtilities.invokeLater(
                new Runnable() {
                    public void run() {
                        setupWf(); // Waveform.unloadTimeSeries() changed to fire ChangeEvent -aww 2009/03/05
                        //setShowAlternateWf(getShowAlternateWf()); // force selection of proper WF
                        //repaint(); // revalidate();
                    }
                }
              );
            }
        }
    }

    /** Set the filter to be used to create the alternate waveform. You only need
    * to do this once. */
    public void setFilter(FilterIF filter) {
        setFilter(filter, true);
    }
    public void setFilter(FilterIF filter, boolean createAltWf) {
         this.filter = filter;
         //if (filter != null) {  // always call createAlternateWf() to null any loaded alternate waveform -aww 2009/04/08
           //if (wfv != null) System.out.println("DEBUG WFPanel: " +wfv.getChannelObj().toString()+" filter: "+filter.toString());
           if (createAltWf) createAlternateWf();
         //}
    }
    public FilterIF getFilter() { return filter; }

    public void setVisible(boolean tf) {
        if (rowHeader != null) {
            rowHeader.setVisible(tf);
        }
        super.setVisible(tf);
    }

    public void setShowCursorAmpAsLine(boolean tf) {
        paintCursorAmpLine = tf;
    }
    public boolean getShowCursorAmpAsLine() {
        return paintCursorAmpLine;
    } 
    public void setShowCursorTimeAsLine(boolean tf) {
        paintCursorTimeLine = tf;
    }
    public boolean getShowCursorTimeAsLine() {
        return paintCursorTimeLine;
    } 

    public void setShowSamples(boolean tf) {
        showSamples = tf;
    }
    public boolean getShowSamples() {
        return showSamples;
    }

    public void setShowAmpScale(boolean tf) {
        if (showAmpScale != tf) {
          showAmpScale = tf;
        }
    }
    public boolean getShowAmpScale() {
        return showAmpScale;
    }

    public void setTimeTicksFlag(int flag) {
        timeTicksFlag = flag;
    }
    public void setShowTimeLabel(boolean tf) {
        showTimeLabel = tf;
    }
    public boolean getShowTimeLabel() {
        return showTimeLabel;
    }
    public void setShowTimeScale(boolean tf) {
        showTimeScale = tf;
    }
    public boolean getShowTimeScale() {
        return showTimeScale;
    }

    public void setShowChannelLabel(boolean tf) {
        showChannelLabel = tf;
    }
    public boolean getShowChannelLabel() {
        return showChannelLabel;
    }
    public void setShowSegments(boolean tf) {
        showSegments = tf;
    }
    public boolean getShowSegments() {
        return showSegments;
    }

/** If true, antialias the waveform */
    public boolean getAntialias() {
      return antialiasing;
    }
/** If true, antialias the waveform */
    public void setAntialias(boolean tf) {
      antialiasing = tf;
    }

    public void setShowPickFlags(boolean tf) {
        showPickFlags = tf;
    }

    public void setShowPhaseCues(boolean tf) {
        showPhaseCues = tf;
    }

    public void setShowResiduals(boolean tf) {
        showResiduals = tf;
    }

    public void setShowDeltaTimes(boolean tf) {
        showDeltaTimes = tf;
    }

    public boolean getShowPhaseCues() {
        return showPhaseCues;
    }

    public boolean getShowPickFlags() {
        return showPickFlags;
    }

    public boolean getShowDeltaTimes() {
        return showDeltaTimes;
    }

    public boolean getShowResiduals() {
        return showResiduals;
    }

    /** Show alternate trace if set true. */
    public void setShowAlternateWf(boolean tf) {
        if (showAlternateWf != tf) {   // its a change
          showAlternateWf = tf;
          if (showTriaxials) {
              setShowTriaxials(false);
              clearTriaxials();
          }
          // set box bounds based on the current WF
          dataBox.set(getWf());                // a null wf is OK here
          setPanelBoxSize();
          setRowHeaderBackground();
        }
    }

    public boolean getShowAlternateWf() {
        return showAlternateWf;
    }

    public Waveform getRawWf() {
        if (wfv == null) return null;
        return wfv.wf;
    }

    public Waveform getAlternateWf() {
        if (alternateWf == null) {
            createAlternateWf();  // try this here, on demand -aww 2010/07/06 
        }
        return alternateWf;
    }

    /**
     * Returns a partial Phase description for this WFView.
     * Only populates the Channel part of the Phase.
     * Leaves specific variables null, e.g. datetime, iphase, etc.
     */
    protected Phase getEmptyPhase() {
        Phase ph = Phase.create();
        Channel chan = wfv.chan;
        if (chan != null) ph.setChannelObj((Channel) chan.clone());
        return ph;
    }


/** Takes a shot at describing the phase pick at the given time. This guess is very crude.  */
//TODO: add estimate of quality, and phase (based on ttime to selected origin)
    public Phase describePickAt(double dtime, Solution selSol)  {

        Phase ph = getEmptyPhase();
        ph.datetime.setValue(dtime);
        ph.description.iphase = "P"; // guess P 

        // figure first motion it there's a waveform
        Waveform wf = getWf();
        double diff = 0.0;
        if (wf != null && wf.hasTimeSeries()) {
            Sample samp1 = wf.closestSample(dtime);
            Sample samp2 = wf.closestSample(dtime + wf.getSampleInterval());
            diff = samp1.value - samp2.value;

            // <> First motion
            if (diff < 0.0) {
                ph.description.fm = "c.";
            } else if (diff > 0.0) {
                ph.description.fm = "d.";
            } else  {
                ph.description.fm = PhaseDescription.DEFAULT_FM;  // "..";
            }
        }

        // <> Phase type, could be clever and check traveltimes
        if (selSol != null && wfv != null) {
            TravelTime tt = TravelTime.getInstance();
            double ttP = tt.getTTp(wfv.getHorizontalDistance(), selSol.mdepth.doubleValue()); // use mdepth -aww 2015/10/10
            double sMinusP = ttP * (tt.getPSRatio() - 1);
            // Guess "S" if time is closer to expected S than expected P
            // i.e. if time is greater then P-time + 1/2 S-P
            if ( (dtime - selSol.datetime.doubleValue()) >= (ttP + (0.5*sMinusP)) ) {
                ph.description.iphase = "S";
            } else {
                ph.description.iphase = "P";
            }
        }

        // <> "qual" (aka onset) i, e, w
        // "i" if first diff is 10% of the full range, "e" if 5% (this is a kludge)
        // but looks reasonable upon inspection.  TODO: improve algorithm
        if (wf != null) {
            double firstDiffRatio = (double) Math.abs(diff) /(double) wf.rangeAmp();
            if (firstDiffRatio >= 0.10) {
                ph.description.ei = "i";
                //ph.description.setQuality(0.);
            } else if (firstDiffRatio >= 0.05) {
                ph.description.ei = "e";
                //ph.description.setQuality(.50);
            } else {
                ph.description.ei = "w";
                //ph.description.setQuality(.25);
            }
        }

        //System.out.println("firstDiffRatio="+firstDiffRatio+" Math.abs(diff)="+Math.abs(diff)+" wf.rangeAmp()="+wf.rangeAmp());
        // <> "quality" (aka weight)
        ph.description.setQuality(1.0);

        // <> association, could be clever and check traveltimes
        //if (selSol != null) ph.assign(selSol); // associate(selSol); // adds it to phaseList

        return ph;

    }

/** Return the time of the sample nearest the pixel with he value of x. */
    public double getSampletimeNearX(int x) {

        double time = dtOfPixel(x);

        // force click to be on a sample, if there's a waveform
        if (getWf() != null) time = getWf().closestSampleInterval(time);

        return time;
    }
    /** Returns the currently selected waveform (raw or filtered). All access to
     *  timeseries should be via this method because it determines which one
     *  to use. */
    public Waveform getWf() {
       if (showTriaxials) {
          //System.out.println("SHOW TRIAXIALS GETWF!");
          Waveform wf = null;
          for (int idx = 0; idx<triaxialList.size(); idx++) {
            wf = (Waveform) triaxialList.get(idx);
            if (wf.getChannelObj().equalsChannelId((Channelable)wfv.getWaveform())) {
                //System.out.println("ZWFP getWf:" + wf.toString());
                break;
            }
          }
          return wf;
       }
       //System.out.println("! SHOW TRIAXIALS GETWF!");
       return (getShowAlternateWf()) ?  getAlternateWf() : getRawWf();
    }
/** Change this panel's selection state. Just changes the background color to
* indicate the panel is selected then repaints. */
    public void setSelected(boolean tf) {
        if (tf == isSelected()) return;   // no change, don't re-add listeners
        amSelected = tf;
        repaint();
    }

    public void setBackgroundColor(Color clr) {
        backgroundColor = clr;
    }

    public Color getBackgroundColor() {
        if (amSelected) {
            return selectedColor;
        } else {
            //if (getWf() != null && getWf().isFiltered()) return alternateUnselectedColor; // removed 2008/08/18 -aww
            return getUnselectedColor();
        }
    }

    private Color getUnselectedColor() {
        if (wfv == null || wfv.getChannelObj() == null) return unselectedColor;
        return ComponentColor.getBackground(wfv.getChannelObj());
    }

  /**
  * Set the size of the panel box.
  */
    public void setPanelBoxSize() {

        if (wfv == null) {
            //System.out.println("DEBUG: WFPanel setPanelBoxSize() instance wfv is null");
            return;
        }

        Waveform wf = getWf();

        synchronized (panelBox) {
            if (wf != null)  {
                //panelBox.set(wfv.viewSpan, wf.getMaxAmp(), wf.getMinAmp());
                panelBox.set(wfv.viewSpan, dataBox.getMaxAmp(), dataBox.getMinAmp());
            } else {
                // box must have non-zero dimension else /0 error happen elsewhere
                panelBox.set(wfv.viewSpan, 1, 0);
            }
            scalePanel();
            repaint(); // or revalidate(); ?
            //System.out.println("DEBUG setPanelBoxSize : " + panelBox.toString());
        }
    }

  /** Return a JPanel that acts as a row header in a scroll pane.*/
    public JComponent getRowHeader() {
        return rowHeader;
        //return new WFRowHeader(this);
    }


/**
 * Set the start/end time of the panel. This may be different from the time span
 * of the waveform data. For example, traces with different starting times
 * can be time aligned in the MultiPanel by setting all their timebounds the
 * same. Setting the time bounds can also be used to clip the data.
 */
    public void setTimeBounds(double start, double end) {
        panelBox.setTimeSpan(start, end);
    }

    public void setTimeBounds(TimeSpan ts) {
        panelBox.setTimeSpan(ts);
    }

/**
 * Clear this WFPanel so that it blank when displayed on a partially full
 * page that contains many WFPanels
 */
    public void clear() {
        removeWFLoadListener();
        if (alternateWf != null) alternateWf.unloadTimeSeries();
        clearTriaxials(); // -aww 2010/01/24
        alternateWf = null;
        dataBox.setNull();
        panelBox.setNull();
        //if (wfv != null) System.out.println("DEBUG wfpanel clear setting NULL wfv:\n" + wfv.toString());
        wfv = null; // do this last 
    }

// -------------------------------------------------------------------

/**
 * Set the WFPanel's scaling factors to fit the WFSelectionBox
 */
    public void scalePanel() {
        // Use the timeSpan of the panel box (for alignment) and the amps of the data box (for scaling)
        WFSelectionBox box = new WFSelectionBox(panelBox);
        if (wfv.hasTimeSeries())  box.setAmpSpan(dataBox);
        setScaleFactors(box);
    }

    /** Methods used by WFScroller for group panel scaling of amplitudes */
    public void setAmpScaleFactor(double factor) {
        //System.out.println("set amp scale: " + factor);
        ampScaleFactor = factor;
    }
    public double getAmpScaleFactor() {
        return ampScaleFactor;
    }

    protected void setWFDataBoxScaling(int flag) {
        dataBox.setScaling(flag);
        setupWf();
    }

/**
 * Set this WFPanel's scaling factors so that the given WFSelectionBox
 * (in time-amp space) fits completely inside this WFPanel. And the Waveform
 * is drawn at the correct scale.
 */
    //void scalePanelToBox(WFSelectionBox box)
    void setScaleFactors(WFSelectionBox box) {

        // pixels/second  (X-axis)
        // assume 0,0 left/right border insets
        //dumpScaleValues();
        //System.out.println(".... resetting pixel scales is event thread: " + SwingUtilities.isEventDispatchThread() + " " + new java.util.Date());
        pixelsPerSecond = (box.getTimeSize() == 0.0) ?  (double) getWidth() : ((double) getWidth())/box.getTimeSize();
        //System.out.println(".... WFPanel setScaleFactors " + wfv.getChannelObj().toDelimitedSeedNameString()+ " old pix/sec: " + pixelsPerSecond + " new pix/sec: " + ((double) getWidth())/box.getTimeSize());
        //System.out.println("WFPanel width: " + getWidth() + " box time size: " + box.getTimeSize());
        // pixels/count   (Y-axis)
             // assume 0,0 top/bottom border insets
        // Can windows be normalized by using Channels gain values in counts/cm/s ? -aww
        double ht = (double) getHeight();
        if (box.getAmpSize() == 0.0) {
           pixelsPerCount = ht;
        } else if (ampScaleFactor == 1.) { // aww 2009/03/26
           pixelsPerCount = ht/box.getAmpSize();
        }
        else {
           pixelsPerCount = (ht*ampScaleFactor)/box.getAmpSize(); // aww 2009/03/26
        }
        // Y-offset to zero in the data(pixel space is always positive, data
        // space can be negative.
        transY = box.getMaxAmp();

        // debug
        /*
        if ( ! (this instanceof ZoomableWFPanel)) return;
        System.out.println(wfv.getChannelObj().toDelimitedSeedNameString() + " setScaleFactors = "+pixelsPerSecond+"/"+pixelsPerCount+"  "+transY+ org.trinet.util.LeapSeconds.trueToString(panelBox.getStartTime()));
        dumpScaleValues();
        System.out.println("inputbox    " + box.toString());
        System.out.println("-------------------------------");
        */
    }

    public final double getPixelsPerSecond() {
        //return (double) getWidth()/panelBox.getTimeSpan().size();
        return pixelsPerSecond;
    }

/**
 * Dump values (for debugging)
 */
    public void dumpScaleValues() {
      if (wfv == null) {
        System.out.println("----------- WaveformView is null -----------");
      } else {
        System.out.println("----------- "+wfv.getChannelObj().toString()+" -----------");

        System.out.println("transY      " + transY);
        System.out.println("dataBox     " + dataBox.toString() );
        System.out.println("panelBox    " + panelBox.toString() );

        System.out.println("panelWidth  " + getWidth() );
        System.out.println("panelHeight " + getHeight() );

        System.out.println("pix/Sec     " + getPixelsPerSecond());
        System.out.println("pix/Count   " + pixelsPerCount);

      }
    }

    // -------------------------------------------------------------------
/**
 * Paints one WFView in the WFPanel. Must rescale every time because frame
 * may have been resized.
 */
    public void paintComponent(Graphics g)  {
        //System.out.println("Painting WFPanel.");  // snork
        super.paintComponent(g);        // give super and delegate chance to paint
        /* IMPORTANT!
         * You must recalculate ALL scaling here because this may be called as a result
         * of the frame being resized by the user with the frame border widgits.
         */
        //scalePanel(); // removed, instead let listeners set scale parameters needed for drawing -aww
        paintBackground(g);
        paintContents(g);
        paintForeground(g);  // to add any extra stuff to on top of contents
    }

/**
* Paints the contents of the panel. Called after paintBackground();
*/
    public void paintContents(Graphics g)  {
        if ( wfv == null ) return;        // bail if no WFView (no data loaded)
        paintData(g);
    }
/**
 * Shared basic painting of background.
 * This sets the panel to the color indicating it's selected. Its also
 * a workaround to stop a weird behavior where old components get
 * painted into the WFPanel on repaints. Just paint over with the background
 * color.
 */
    public void paintBackground(Graphics g) {
         Color oldColor = g.getColor();
         g.setColor(getBackgroundColor());
         g.fillRect(0, 0, getWidth(), getHeight());
         g.setColor(oldColor);
         if (getShowPhaseCues()) paintPhaseCues(g);   // paint phase location cues
    }
/**
 * Paints an overlay on contents of the panel. Called after paintBackground() & paintContents();
 * Paints cursor lines, if enabled.
*/
    public void paintForeground(Graphics g)  {
         //g.setColor(getForeground());
         paintCursor(g);
    }

    protected void paintCursor(Graphics g) {
          if (! paintCursorTimeLine || Double.isNaN(cursorTime)) return;
          int x = pixelOfTime(cursorTime);
          g.drawLine(x, 0, x, getHeight());

          if (! paintCursorAmpLine || Double.isNaN(cursorAmp)) return;
          //if ((!paintCursorAmpLine && !wfv.equalsChannelId(wfv.mv.masterWFViewModel.get())) || Double.isNaN(cursorAmp)) return;
          int y = pixelOfAmp(cursorAmp);
          g.drawLine(0, y, getWidth(), y);
    }

/**
* Shared basic painting of foreground "decorations" and data.
*/
    public void paintData(Graphics g) {

        // Plot all the segments in the view
        paintTimeSeries(g);

        // draw a scale (should be a seperate method)
        paintScale(g);

        // Put the picks on the view
        if (getShowPickFlags()) {
          paintPickFlags(g);
          paintAmpFlags(g);
          paintCodaFlags(g);
        }

        if (getShowChannelLabel() && wfv.getChannelObj() != null ) {
          // Try to label the trace
          int pHeight = getHeight();
          // Test:
          int fHeight = g.getFontMetrics().getHeight();
          if (pHeight > 13 && pHeight >= fHeight) { // don't do labels if out of bounds
              StringBuffer sb = new StringBuffer(90); // aww
              // Allow use of static ChannelName.useFullName property toggle for debug -aww
              sb.append(wfv.getChannelObj().toDelimitedNameString(' '));
              // Use hypocental
              //sb.append(" ").append(df1.format(wfv.getDistance())).append("km") ; // don't use slant aww 06/11/2004
              // Use epicentral
              if (embossedLabelForm.equals(PropConstant.WAVEFORM_DESC_FORMAT_FILTER) || embossedLabelForm.startsWith("dist") || embossedLabelForm.equals(PropConstant.WAVEFORM_DESC_FORMAT_AZIMUTH)) {
                  sb.append(" ").append(df1.format(wfv.getHorizontalDistance())).append("km "); // aww 06/11/2004
              }

              // Azimuth implies "dist" display format (i.e. channel - dist - azimuth)
              if (embossedLabelForm.equals(PropConstant.WAVEFORM_DESC_FORMAT_FILTER) || embossedLabelForm.equals(PropConstant.WAVEFORM_DESC_FORMAT_AZIMUTH)) {
                  // distance contains a space at the end
                  if (!Double.isNaN(wfv.getAzimuth())) {
                      sb.append(df1.format(wfv.getAzimuth())).append(PropConstant.WAVEFORM_DESC_FORMAT_AZIMUTH_UNIT + " ");
                  }
              }

              if (getWf() != null && getShowAlternateWf() && embossedLabelForm.equals(PropConstant.WAVEFORM_DESC_FORMAT_FILTER)) {
                  sb.append(getWf().getFilterName());
              }
              //embossedDrawString(g, sb.toString(), 9, fHeight-2);
              Rectangle rec = getVisibleRect();
              String str = sb.toString();
              int x  = 0;
              if (CHANNEL_LABEL_MODE==0) { // at start of panel only
                // Need new channel label mode to put static labels at panel "start" vs. "leading" view scroll 
                embossedDrawString(g, str, 9, pHeight-3);
              }
              else if (CHANNEL_LABEL_MODE==1) { // leading edge viewport
                x = (rec.x+9);
                embossedDrawString(g, str, x, pHeight-3);
              }
              else if (CHANNEL_LABEL_MODE==2) { // trailing edge viewport
                x = (rec.x+rec.width - g.getFontMetrics().stringWidth(str) - 10);
                embossedDrawString(g, str, x, pHeight-3);
              }
              else if (CHANNEL_LABEL_MODE==3) { // both viewport edges
                x = (rec.x+9);
                embossedDrawString(g, str, x, pHeight-3);
                x = (rec.x+rec.width - g.getFontMetrics().stringWidth(str) - 10);
                embossedDrawString(g, str, x, pHeight-3);
              }
          }
        }
    } // end of paintData()

/**
 * Paint the time-series, i.e. the waveform in the WFView (all its segments)
 */
    public void paintTimeSeries(Graphics g) {
        if (! showTriaxials || triaxialList == null || triaxialList.size() == 0) { // 2010/01/24 -aww
            paintTimeSeries(g, getWf());
        }
        else { // 2010/01/24 -aww
          Waveform wf = null;
          Color c = wfColor;
          for (int idx = 0; idx<triaxialList.size(); idx++) {
            wf = (Waveform) triaxialList.get(idx);
            /*
            if (wf.getChannelObj().sameSeedChanAs((Channelable)wfv)) Color.black;
            else {
                c = triaxialColors[colorIdx%3];
                colorIdx++;
            }
            */
            boolean isVert= ! wf.getChannelObj().isHorizontal();
            double azi = Math.abs(wf.getChannelObj().getSensorAzimuth());
            boolean isNorth = ( (azi <= 45. || azi >= 315.) || (azi > 135. && azi < 225.));
            char orient = Character.toUpperCase(wf.getChannelObj().getSeedchan().charAt(2));
            //System.out.println("Orient: " + orient + "isVert: " + isVert + " azi: " + azi + " dip: " +
            //        wf.getChannelObj().getSensorDip() + "seedchan: " + wf.getChannelObj().getSeedchan());
            if (isVert) {
              c = Zcolor;
            }
            else if (orient == 'Z' || orient == 'V' || orient == 'U') c = Zcolor;
            else if (orient == 'N' || orient == 'T') c = Ncolor;
            else if (orient == 'E' || orient == 'R') c = Ecolor;
            else if (Character.isDigit(orient)) {
               int i = Character.digit(orient,10)%3; 
               if (i == 0) {
                   c = ( isNorth ) ? Ncolor : Ecolor;
               }
               else if (i == 1) {
                  c = ( isNorth ) ? Ncolor : Ecolor;
               }
               else if (i == 2) {  
                  c = ( isNorth ) ? Ncolor : Ecolor;
               }
            }
            if (wf.getChannelObj().sameSeedChanAs((Channelable)wfv)) triaxialColor = c;
            //System.out.println("wfp plot triaxial color c: " + c );
            if (wf != null && canPlotTriaxial(c)) {
                paintTimeSeries(g, wf, c);
            }
          }
        }
    }

    public boolean canPlotTriaxial(Color c) {
        Boolean b = (Boolean) plotTriaxialMap.get(c);
        //if (b == null) System.out.println("WFPanel canPlotTriaxial b == null");
        if (b == null || b.booleanValue() == false) return false;
        //System.out.println("WFPanel canPlotTriaxial b = " + b);
        return true;
    }
    public void setPlotTriaxial(Color c, boolean tf) {
        if (tf) {
            plotTriaxialMap.put(c, Boolean.TRUE);
            //System.out.println("setPlotTriaxial true");
        }
        else {
            plotTriaxialMap.put(c, Boolean.FALSE);
            //System.out.println("setPlotTriaxial false");
        }
    }
    protected void resetPlotTriaxialMap() {
        plotTriaxialMap.put(Zcolor, Boolean.TRUE);
        plotTriaxialMap.put(Ncolor, Boolean.TRUE);
        plotTriaxialMap.put(Ecolor, Boolean.TRUE);
    }

    protected void setShowTriaxials(boolean tf) {
        showTriaxials = tf;
        //resetPlotTriaxialMap();
        /*
        if (showTriaxials) {
          Waveform wf = null;
          for (int idx = 0; idx<triaxialList.size(); idx++) {
            wf = (Waveform) triaxialList.get(idx);
            if (wf.getMinAmp() < panelBox.getMinAmp() || wf.getMaxAmp() > panelBox.getMaxAmp()) {
              panelBox.setAmpSpan(wf.getMaxAmp(), wf.getMinAmp());
              dataBox.setAmpSpan(wf.getMaxAmp(), wf.getMinAmp());
            }
          }
          setScaleFactors(panelBox);
        }
        */
        //else {
            //System.out.println("WFPanel reset setShow triaxials" + showTriaxials);
            dataBox.set(getWf()); // a null wf is OK here
            setPanelBoxSize();
        //}
        //
        //revalidate();
        repaint();  // or revalidate(); ?
    }

    public void paintTimeSeries(Graphics g, Waveform wf) {
        paintTimeSeries(g, wf, null);
    }

    protected void paintTimeSeries(Graphics g, Waveform wf, Color inColor) {

        //Waveform wf = getWf();

        if (wf == null || !wf.hasTimeSeries()) return;

// Antialias the waveform? (smooth graphic draw, reduce jagged edges)
        if (getAntialias()) {
          ((Graphics2D)g).setRenderingHint (RenderingHints.KEY_ANTIALIASING,
              RenderingHints.VALUE_ANTIALIAS_ON);
        }

        // last sample drawn in previous segment, to connect segments on plot
        Sample previousSample;

        // synchronize to make sure another thread doesn't change the wf underneith us
        synchronized (wf) {
          WFSegment wfseg[] = WFSegment.getArray(wf.getSegmentList());

          // optimization step, reduces method calls in loop ??
          int count = wfseg.length;

          // show start of WFSegment if showSegment is true
          // do it first so they are behind the waveform

          if (getShowSegments()) paintPacketSpans (g);
// plot each segment
          for (int i = 0; i < count; i++) {
            // connect with a line if segments are contiguous, else leave a gap
            if ( (i > 0) && WFSegment.areContiguous(wfseg[i-1], wfseg[i])) {
              previousSample = wfseg[i-1].getLastSample();
            } else {
              previousSample = null;
            }
            paintSegment(g, wfseg[i], previousSample, inColor);
          } // end of loop
        } // end of synch
    }
   /** Write with embossed effect - makes more readable when label is on top of waveform
     * Write label first in background color than one pixel up & right in text color
     */
    public void embossedDrawString(Graphics g, String str, int x, int y) {
      Font oldFont = g.getFont();
      if (embossedFont != null) {
          g.setFont(embossedFont);
      }
      Color oldColor = g.getColor();  // remember current color to reset to later
      g.setColor(backgroundColor);
      g.drawString(str, x, y);
      g.setColor(textColor);
      g.setColor((embossedFontColor == null) ? textColor : embossedFontColor);
      g.drawString(str, x+1, y+1);
      g.setColor(oldColor);
      g.setFont(oldFont);
    }
// --------------------------------------------------------------------
/**
 * Paint one WFSegment in the WFPanel.
 * THIS IS ONE OF THE MOST TIME CONSUMING PARTS OF THIS CODE. OPTIMIZE IF POSSIBLE
 */

 // This stuff is here in an effort to reduce declarations inside of heavily used
 // loops. I'm not sure it really matters. [see later note below]
   //double pixelsPerInterval;
   //int offsetX;
   //int jump;
   //int pointCount;
// NOTE: scoping these globally ment they were NOT null'ed and GC'ed when the
// WFPanel's scroll out of view and the waveform is deleted from memory.
// This constituted a huge memory leak because these arrays were COPIES of the time series.
// Found by Kalpesh - 11/25/03
//   int xb[], ymax[], ymin[];
//   int x[], y[];
//   float sample[];
   //int x0, y0, xPixel;
   //float maxAmp, minAmp;

    private void paintSegment(Graphics g, WFSegment wfseg, Sample previousSample, Color inColor) {

      if (wfseg == null) return;
/*
      * Recalculate 'pixelsPerInterval' because the sample interval (dt)
      * could be different for each segment.
 */
      // pixels between sample points
      double pixelsPerInterval = getPixelsPerSecond() * wfseg.getSampleInterval();

      // if time scale is compressed (>4 samples/pixel), paint as bars
      if (1.0/pixelsPerInterval >= 4.0) {
        paintSegmentAsBars (g, wfseg, previousSample, inColor);
        return;
      }

      // for time alignment
      int offsetX = pixelOfTime(wfseg.getEpochStart());

// Decimation factor is tied to scale such that we "over sample" by about x4.
// I arrived at this value by testing. Decimation speeds things up immensely.

        int jump = Math.max(1, (int) (wfseg.size()/getWidth()/2) );

        // count of points that will be in decimated plotting array
        int pointCount = (wfseg.size() / jump);

        // instantiate the x-y ploting arrays for use in Graphics.drawPolyline()
        int [] x = new int[pointCount];
        int [] y = new int[pointCount];
        float [] sample = wfseg.getTimeSeries(); //must be set null before method return ! -Kalpesh.

// Convert seismogram to pixel coordinate system

        // interval w/ decimation
        pixelInterval = pixelsPerInterval * (double)jump;

        xPosition = (double) offsetX;

        int idx = 0;
        for (int i = 0 ; i < pointCount; i++) {

          x[i] = (int) xPosition;

          //"inline" this method to optimize?
          y[i] = pixelOfAmp(sample[idx]);

          //TODO: might round off error accumulate here?
          xPosition += pixelInterval;        // inc. pixel by decimated interval
          idx += jump;
        }

        // set plot color specific to component sh log /sys
        Color oldColor = g.getColor();
        g.setColor((inColor == null) ? wfColor : inColor);

// plot line connecting segments if necessary
        if (previousSample != null) {

          int x0 = offsetX + (int) (-1 * pixelsPerInterval); // backup 1 interval
          int y0 = pixelOfAmp(previousSample.value);

          g.drawLine(x0, y0, x[0], y[0]);
        }

// plot the seismogram, this is the major resource sink of the program
// drawpolyline draws nothing if only one point so handle that case
        
        if (pointCount == 1) {         // special case - 1 point
            g.drawLine(x[0],y[0],x[0],y[0]);
        } else {
            g.drawPolyline (x, y, pointCount);
        }

// mark individual samples if scale allows (> 4 pixels per sample)
        //System.out.println("WFPanel pixelsPerInterval : " + pixelsPerInterval);
        if (pixelsPerInterval > 3.0) { // changed from 4. to 3. pixels -aww 2010/09/15
          if (showSamples) {
            if (! g.getColor().equals(inColor)) g.setColor(Color.red);
            for (int i = 0 ; i < x.length ; i++) {
              g.drawLine(x[i], y[i]-2, x[i], y[i]+2);
            }
          }
        }
        // null out work arrays so they can be GC'ed
        // Memory leak found by Kalpesh
        //sample = null;
        //x = null;
        //y = null;

        g.setColor(oldColor); // restore the original
} // end of paintSegment()



/**
 * Having trouble getting it to look right at some segment boundaries. The 1st bar
 * may be quite different from the last bar of the previous seg making it look like
 * a gap in the timeseries. Using the "priviousSample" helps but it is not the
 * previous bar.

*/

    private void paintSegmentAsBars(Graphics g, WFSegment wfseg, Sample previousSample, Color inColor) {

      if (wfseg == null) return;
/*
      * Recalculate 'pixelsPerInterval' because the sample interval (dt)
      * could be different for each segment.
 */
      // pixels between sample points
      double pixelsPerInterval = getPixelsPerSecond() * wfseg.getSampleInterval();

      // for time alignment
      int offsetX = pixelOfTime(wfseg.getEpochStart());

        // instantiate the x-y ploting arrays for use in Graphics.drawPolyline()
        int[] x = new int[wfseg.size()];
        int[] y = new int[wfseg.size()];
        float[] sample = wfseg.getTimeSeries(); //must be set null before method return -Kalpesh.

// Convert seismogram to XY array in pixel coordinate system

        // interval w/ decimation
        pixelInterval = pixelsPerInterval;
        xPosition = (double) offsetX;

        int idx = 0;
        for (int i = 0 ; i < wfseg.size(); i++) {

          x[i] = (int) xPosition;

          //"inline" this method to optimize?
          y[i] = pixelOfAmp(sample[idx]);

          //TODO: might round off error accumulate here?
          xPosition += pixelInterval;        // inc. pixel by decimated interval
          idx++;
        }

// Compress into new array with one BAR per pixel

        int k = 0;
        int kbar = 0;
        int lastx = x[0];
        int mx,mn = 0;

        int[] xb = new int[x.length];
        int[] ymax = new int[x.length];
        int[] ymin = new int[x.length];

// Set initial max/min values
        if (previousSample != null) {
          ymax[kbar] = ymin[kbar] =  pixelOfAmp(previousSample.value);
        } else {
          ymax[kbar] = ymin[kbar] = y[k];
        }

        while (k < x.length) {
          xb[kbar] = x[k];
          lastx = x[k];                    // pixel we're working on
          
          // Scan max/min of bar, accumulate points w/ same x-pixel value
          while (k < x.length && x[k] == lastx) {  
            ymax[kbar] = Math.max(y[k], ymax[kbar]);
            ymin[kbar] = Math.min(y[k], ymin[kbar]);
            k++;
          }
          // insure the bar has some dimension
          if (ymax[kbar] == ymin[kbar]) ymax[kbar]++;
          mx = ymax[kbar];
          mn = ymin[kbar];
          
          // insure overlap between adjacent bars (else looks gappy)
          if (++kbar < x.length) {
            ymax[kbar] = mn;
            ymin[kbar] = mx;  // starting max/min
          }
        }

        // set plot color specific to component sh log /sys
        Color oldColor = g.getColor(); // save the original
        g.setColor((inColor == null) ? wfColor : inColor);

// plot line connecting segments if necessary
//        if (previousSample != null) {
//
//          x0 = offsetX + (int) (-1 * pixelsPerInterval); // backup 1 interval
//          y0 = pixelOfAmp(previousSample.value);
//
//          g.drawLine(x0, y0, x[0], y[0]);
//        }
// plot bars
        for (int i = 0; i<kbar;i++) {
          g.drawLine(xb[i], ymax[i], xb[i], ymin[i]);
        }
        //sample = null; //Kalpesh, release memory
        //x = null;      //Kalpesh, release memory
        //y = null;      //Kalpesh, release memory
        //xb = null;     //Kalpesh, release memory
        //ymax = null;   //Kalpesh, release memory
        //ymin = null;   //Kalpesh, release memory

        g.setColor(oldColor); // restore the original
} // end of paintSegmentAsBars()

///**
// * Paints one WFSegment in the WFPanel using a shortcut technique that plot the
// * waveform by plotting a vertical bar, one pixel wide, that
// * represents the max/min range of the samples that lie within that pixel. This only
// * works if the time scale is compressed to the point that there is more than one sample
// * per pixel.  <p>
// * This speeds up plotting and makes the seismograms look better then simple decimation
// * without filtering.
// * Suggested by Paul Friberg.
// */
// /*
// * 2/14/2003  DDG - discovered 1 pixel gaps between segments. Doesn't usually show
// * because collapse() is usually called to concatinate contiguous segs.
// * Tried several fixes, to no avail. Possibly an "aliasing" problem deeper
// * in the graphics system. Doesn't happen with paintSegments().
// */
//    public void paintSegmentAsBars(Graphics g, WFSegment wfseg, Sample previousSample, int dummy) {
///*
// * Recalculate 'pixelsPerInterval' because the sample interval (dt)
// * could be different for each segment.
// */
//
//      // data check
//      if (!wfseg.hasTimeSeries()) return;
//
//      // pixels between sample points
//      double secPerPixel = 1.0/pixelsPerSecond;
//
//      sample = wfseg.getTimeSeries();
//
//// paint these first so they don't obscure the timeseries
////      if (getShowSegments()) paintPacketSpans (g);
//
//      // set plot color specific to component sh log /sys
//      g.setColor(wfColor);
//
//// Convert seismogram to pixel coordinate system
//     maxAmp = minAmp = sample[0];
//
//     double currentTime = wfseg.getEpochStart();
//     if (previousSample != null) {
//       currentTime = previousSample.datetime;
//       maxAmp = minAmp = (int) previousSample.value;
//     }
//     double pixelEndTime = currentTime + secPerPixel;
//     xPixel = pixelOfTime(currentTime);
//
//     for (int i = 0; i < sample.length; i++ ) {
//       // find max/min amps as we scan thru the samples that are represented
//       // by one pixel
//         maxAmp = Math.max(maxAmp, sample[i]);
//         minAmp = Math.min(minAmp, sample[i]);
//
//      // if we've passed a pixel boundary, plot the bar
//         if ( currentTime >= pixelEndTime ) {  // end of a pixel interval
//
//           // draw the bar
//           g.drawLine(xPixel, pixelOfAmp(maxAmp),
//                      xPixel, pixelOfAmp(minAmp));
//
//           // increment stuff and reset for next sample interval
//           xPixel++; // one pixel
//           pixelEndTime += secPerPixel;
//
//           // this actually included the last sample in the next interval so
//           // there are no gaps between bars
//           maxAmp = minAmp = sample[i];
//
//         }
//         currentTime += wfseg.getSampleInterval();  // increment
//
//     }
//}

/**
 * Draw lines at the packet boundaries.
 */
public void paintPacketSpans(Graphics g) {
    int start;
    int end;

    Waveform wf = getWf();

    if (wf != null) {
      Color oldColor = g.getColor();
      g.setColor (segmentEdgeColor);

      TimeSpan pkt[] = wf.getPacketSpans();

      if (pkt != null) {
       for (int i = 0; i< pkt.length; i++) {
            start = pixelOfTime(pkt[i].getStart());
            end   = pixelOfTime(pkt[i].getEnd());

            g.drawLine(start, 0 , start, getHeight()-1 );
            g.drawLine(end,   0 , end,   getHeight()-1 );
       }
      }
      g.setColor(oldColor);
    }
}

/**
 * Plot the phases on the waveform
 */
//TODO: could set an optional mode that would show deleted phases
    public void paintPickFlags(Graphics g) {
        if (wfv == null) return;        // bail if no WFView (no data loaded)
        // get the phase list
        Phase ph[] = (Phase[]) wfv.phaseList.getArray();
        //        wfv.phaseList.toArray(ph);

        PickFlag pf = null;

        // Note that PickFlag.draw(g) will not draw 'deleted' phases
        for (int i = 0; i < ph.length; i++) {
            if (! ph[i].isDeleted()) {
                pf = new PickFlag(this, ph[i]);
                pf.setShowDescription(showPhaseDescriptions);
                pf.setShowResidual(showResiduals);
                pf.setShowDeltaTime(showDeltaTimes);
                pf.draw(g);
            }
        }
    }

    /** Turns on or off the display of phase descriptions in the "flag" part of the pick flags. */
    public void showPhaseDescriptions(boolean tf) {
         showPhaseDescriptions = tf;
    }

/**
 * Plot the amplitudes on the Waveform
 */
    public void paintAmpFlags(Graphics g) {
        if (wfv == null) return;        // bail if no WFView (no data loaded)
        // get the phase list
        Amplitude amp[] = (Amplitude[]) wfv.ampList.getArray();

        AmpFlag flag;

        // Note that AmpFlag.draw(g) will not draw 'deleted' phases
        for (int i = 0; i < amp.length; i++) {

         if (! amp[i].isDeleted()) {
              flag = new AmpFlag(this, amp[i]);
              flag.draw(g);
         }
        }
    }
/**
 * Plot the codas on the waveform
 */
    public void paintCodaFlags(Graphics g)  {

        if (wfv == null) return;  // bail if no WFView (no data loaded)
        if (getWf() != null && getWf().getAmpUnits() != Units.COUNTS) return;  // only plot curve if amp units counts

        // get the coda list
        Coda coda[] = (Coda[]) wfv.codaList.getArray();

        CodaFlag flag = null;

        // Note that AmpFlag.draw(g) will not draw 'deleted' phases
        // DK 082802 added size test for coda without AvAmpWindows
        for (int i = 0; i < coda.length; i++) {
          if (! coda[i].isDeleted()) { //  !(coda[i].getWindowTimeAmpPairs().size() == 0)) {
            if (codaFlagMode == CodaFlag.Fix) 
                flag = new CodaFlag(this, coda[i], CodaFlag.Fix);
            else if ( codaFlagMode == CodaFlag.Bar)
                flag = new CodaFlag(this, coda[i], CodaFlag.Bar);
            else 
                flag = new CodaFlag(this, coda[i], CodaFlag.Free);
            flag.draw(g);
          }
        }
    }
/**
* Paint a graphical marker to show where P & S phases are expected to lie.
* Its just a colored rectangle.
*/
    public void paintPhaseCues(Graphics g) {

        if ( wfv == null ) return;        // bail if no WFView (no data loaded)
        Solution sol = wfv.mv.getSelectedSolution();
        if (sol == null || sol.datetime.isNull()) return;  // no location

        // note solution must be the first arg.
        TravelTime tt = TravelTime.getInstance();
        double ptime = tt.getTTp(wfv.getHorizontalDistance(), sol.mdepth.doubleValue()); // use mdepth -aww 2015/10/10
        double stime = ptime * tt.getPSRatio();
        //System.out.println("DEBUG WFPANEL chan, tt: "+wfv.getChannelObj().toDelimitedNameString("_")+" "+ptime);

        // change from traveltime to absolute epoch time
        ptime += sol.datetime.doubleValue();
        stime += sol.datetime.doubleValue();

        cue.set(this, ptime);
        cue.draw(g);
        cue.set(this, stime);
        cue.draw(g);

    }

/**
 * Paint the time scale. Also paints at bounding box.
 *
 */
    public void paintScale(Graphics g)
    {

        Color oldColor = g.getColor();

        Color scaleColor = defaultScaleColor;

        // Hardwired test to indicating bad time with red time ticks
        if (wfv != null && wfv.mv != null && wfv.getWaveform() != null &&
            wfv.getWaveform().getTimeQuality().doubleValue() < wfv.mv.getClockQualityThreshold()) {
            //System.out.println(wfv.getWaveform().toString() +"  "+ wfv.getWaveform().getTimeQuality().doubleValue());
            Color bckg = ComponentColor.getBackground(wfv.getChannelObj()); // get color for this component background
            if (bckg.equals(Color.red)) scaleColor = Color.pink;
            else scaleColor = Color.red;
        }

        if (getShowTimeScale()) {

            g.setColor(scaleColor);

            // paint the bounding box
            g.drawRect(0, 0, getWidth()-1, getHeight()-1);

            //        g.drawLine(0, 0, getWidth()-1, 0); //top horizontal line
            //        g.drawLine(0, getHeight()-1,
            //        getWidth()-1, getHeight()-1); //bottom horizontal line

            //int smallTick = Math.max(getHeight()/30, 1);  // at least 1 pixel
            int smallTick = (getHeight() < 40) ? 1 : 2;

            double startSec = Math.floor(panelBox.getStartTime());
            double endSec = panelBox.getEndTime();

            // pixels if pixels/sec scale is big draw 0.1 sec ticks too 
            double fsec =  ( (double)getWidth()/panelBox.getTimeSpan().size() > 40) ?  0.1 : 1.;

            double sec = startSec + fsec;  // increment by smallest tic interval desired
            // System.out.println("DEBUG WFPanel sec: " + LeapSeconds.trueToString(sec, "HH:mm:ss.fff")); // UTC seconds now - aww 2008/02/10
            int x = 0;
            int scalar = 1;
            int isec = 0;
            String label = null;
            int imod = 0;
            double dsec = 0.;
            double dt = 0.;

            // Temp kludge here to prevent loop hangup of drawing too many time tics when ending timestamp bad -aww 2015/03/20
            boolean drawTicks = (panelBox.getTimeSpan().size() <= 3600.);
            if (! drawTicks) {
                System.err.println("WARNING: WFPanel skipped drawing of time scale: check waveform times, duration > 3600 s");
                return;
            }

            while (drawTicks) {

            // NOTE : If panelBox timespan is very large (e.g. > 1 hr) may exceed graphic limits and a hang inside this loop !!!
            //        pixelsPerSec is very small and it tries to draw too many tics and labels.
            //        If this occurs for a typical local event check for erroneous datetime_off values in one of the waveform rows.

                    if (sec > endSec) break; 

                    scalar = 0;

                    x = pixelOfTime(sec);

                    label = LeapSeconds.trueToString(sec, "HH:mm:ss.fff"); // UTC seconds now - aww 2008/02/10
                    dsec = Double.parseDouble(label.substring(6));
                    isec = (int) Math.floor(Math.round(dsec+.001));
                    dt = Math.abs(dsec - Math.round(dsec));
                    imod = isec%10;
                    if ( dt <= .0015) {
                    //if (this instanceof ZoomableWFPanel && fsec < 1.) System.out.println(dsec +" " + isec + " " + dt + " imod:" + imod);
                      scalar = 1; // 1 sec tick
                      if (imod == 0) {  // ten second mark
                        scalar = 3;
                        if (isec == 60 || isec == 0) {
                          scalar =  4; // minute mark
                          if (showTimeLabel) {
                            if (isec == 60) {
                              label = String.format("%2s:%02d", label.substring(0,2),Integer.parseInt(label.substring(3,5))+1);
                            }
                            embossedDrawString(g, label.substring(0,5), x+2, 12); // hh:mm
                          }
                        }
                      }
                      else if (imod == 5) scalar = 2;          // 5 sec tic

                    }

                    if (scalar > 0 && fsec < 1.) scalar += 1;
                    else if (scalar == 0) scalar = 1;

                    if (timeTicksFlag >= 0 ) {
                      if (timeTicksFlag != 2) g.drawLine(x, 1, x, scalar*smallTick); // top scale
                      if (timeTicksFlag != 1) g.drawLine(x, getHeight()-1, x, getHeight()-scalar*smallTick-1); // bottom scale
                    }

                    sec += fsec; // sec++;
                    
            } // end of time tick label drawing
            //System.out.println("....WFPanel " + wfv.getChannelObj().toDelimitedSeedNameString()+ " pix/sec:" +pixelsPerSecond+ " start:" +startSec+ " end:" +endSec+ " size:" + getSize());
        }

        /*
        if (getShowAmpScale()) {
          // not implemented
        }
        */

        g.setColor(oldColor); // restore original

    } // end of paintScale()


    /**
     * Return a rectangle in pixels for the given WFSelectionBox.
     * This is a method of WFPanel because it needs the WFPanel
     * scaling information.
     */
    public Rectangle rectOfBox(WFSelectionBox box)
    {
        return new Rectangle(
            pixelOfTime(box.getStartTime()),               // x
            pixelOfAmp(box.maxAmp),                       // y
            (int) (box.getTimeSize() * getPixelsPerSecond()),   // width
            (int) (box.getAmpSize() * pixelsPerCount) );   // height

    }

    /**
     * Return a rectangle in pixels for the given WFSelectionBox.
     * This is a method of WFPanel because it needs the WFPanel
     * scaling information.
     */
    public WFSelectionBox boxOfRect(Rectangle rec)
    {
        TimeSpan ts = new TimeSpan(dtOfPixel(rec.x),
                                   dtOfPixel(rec.x + rec.width));

        return new WFSelectionBox(ts, (int) ampOfPixel(rec.y), (int) ampOfPixel(rec.y + rec.height));
    }

    /**
     *  Return dt Time of this x position in the panel
     */
    double dtOfPixel(Point pt) {
      return dtOfPixel(pt.x) ;
    }

    double dtOfPixel(int ix) {
      return ((double)ix/getPixelsPerSecond() + panelBox.getStartTime());
    }

    /**
     * Calculate the pixel on the WFPanel that corrisponds to this time.
     */
    int pixelOfTime(double dt) {
      // round() finds closest pixel, returns long so cast to int
      return (int) Math.round((dt - panelBox.getStartTime()) * getPixelsPerSecond());
    }

    /**
     * Return the amplitude of y position
     */
    double ampOfPixel(Point pt)
    {
      return ampOfPixel(pt.y);
    }

    /**
     * Return the amplitude of y position
     */
    double ampOfPixel(int iy)
    {
      if (ampScaleFactor == 1.) return  -((double)iy/pixelsPerCount - transY);

      // Below added for WFScroller group panel scaling using hide popup menu -aww 2009/03/26
      int h = getHeight(); // panel height
      double b = getWf().getBias(); // assume scanForBias was done already
      return (double)h/2. - (double)iy/pixelsPerCount + b;
      
    }

    /**
    * Return the amplitude of y position
    */
    int pixelOfAmp(double amp)
    {
      if (ampScaleFactor == 1.) return (int) Math.round( (transY - amp) * pixelsPerCount);

      // Below added for WFScroller group panel scaling using hide popup menu -aww 2009/03/26
      int h = getHeight(); // panel height
      //double b = dataBox.getCenterAmp(); // may be asymmetrical
      double b = getWf().getBias(); // assume scanForBias was done already
      int pix = (int) Math.round((double)h/2. - (amp-b)*pixelsPerCount);
      if (pix < 0) pix = 0;
      if (pix > h) pix = h;
      return pix;
    }
    /** Return a WFSelectionBox representing the part of this WFPanel that is
    * visible. Simply returns the panelBox. */
    public WFSelectionBox getVisibleBox() {
           return panelBox;
    }

    /** **************************************************************************************
     * Scrollable interface methods
     * These methods determine the behavior of this component if it is placed in a JScrollPane.
     * @See: Scrollable
     */

    boolean trackViewportX = false;
    boolean trackViewportY = false;

    public Dimension getPreferredScrollableViewportSize()
    {
        return getPreferredSize();
    }

    public boolean getScrollableTracksViewportWidth()
    {
        return trackViewportX;
    }

    public boolean getScrollableTracksViewportHeight()
    {
        return trackViewportY;
    }
    public void setScrollableTracksViewportWidth(boolean tf)
    {
        trackViewportX = tf;;
    }
    public void setScrollableTracksViewportHeight(boolean tf)
    {
        trackViewportY = tf;
    }

    /**
     * Determines scrolling increment when "track" is clicked. (Coarse scrolling)
     * Default is 1/10 of the WFPanel dimension. Override this method to change this
     * behavior.
     */
    public int getScrollableBlockIncrement(Rectangle visRec, int orient, int dir)
    {
        if (orient == SwingConstants.VERTICAL)
        {
            return getHeight()/10;            // jump 1/10 the height
        } else {
            return getWidth()/10;            // jump 1/10 the width
        }
    }

    /**
     * Determines scrolling increment when arrow widgit is clicked. (Fine scrolling)
     * Default is 1 pixel. Override this method to change this behavior.
     */
    //TODO: because of rounding, repeated scroll jumps cause cumulative error in whole.
    // :. should calc. jump amt. to be explicit to next sample.
    public int getScrollableUnitIncrement(Rectangle visRec, int orient, int dir)
    {
        if (orient == SwingConstants.VERTICAL) {
          return 5;            // jump 1/10 the height
        } else {
          return  Math.max(1, getWidth() / 100);
        }

    }


// End of Scrollable interface methods
/****************************************************************************************/

    /** Subclasses should override method to create the popup menu that is activated by a right mouse click, default is a no-op. */
    public void makePopup(Point point) {
            // no-op 
    }

    /*
        // Implementation below was inside the WFRowHeader class -aww 2011/03/29
        public void makePopup(Point point) {

           PopupListener popupListener = new PopupListener();
           JMenuItem mi;
           JMenu submenu;

           JPopupMenu popup = new JPopupMenu();

           mi = popup.add(new JMenuItem("Channel Info"));
           mi.addActionListener(popupListener);

           popup.show(this, point.x, point.y);
        }
    */

    private class WFRowHeader extends JPanel {
        //  ColumnLayout layout = new ColumnLayout();
        // Use of a BoxLayout here does not work! Can't control the size of the panel
        // with setPreferredSize() it sizes the box using the contents.
        //BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);

        /** The parent WFPanel. */
        WFPanel wfp = null;

        final float MaxTextSize = 9.f; // if no max text won't fit in label width

        /** Remember old frame height so we only reset font when it changes.*/
        int frameHeight;

        JLabel netLabel = new JLabel();
        JLabel staLabel = new JLabel();
        JLabel chanLabel = new JLabel();
        JLabel locLabel = new JLabel();
        JLabel distLabel = new JLabel();

        // constructor
        public WFRowHeader(WFPanel wfpanel) {
            wfp = wfpanel;
            wfp.addComponentListener(
                new ComponentAdapter() {
                    public void componentResized(ComponentEvent e) {
                        WFRowHeader.this.setMaximumSize(getPreferredSize());
                        WFRowHeader.this.setSize(getPreferredSize());
                        WFRowHeader.this.resetFont();
                    }
                }
            );
            //setDefaultText();

            // create the panel
            FlowLayout layout = new FlowLayout(FlowLayout.LEFT,0,0);
            setLayout(layout);
            setBackground(Color.darkGray);

            netLabel.setForeground(Color.white);
            staLabel.setForeground(Color.white);
            chanLabel.setForeground(Color.white);
            locLabel.setForeground(Color.white);
            distLabel.setForeground(Color.white);

            add(netLabel);
            add(staLabel);
            add(chanLabel);
            add(locLabel);
            add(distLabel);

            setBorder(BorderFactory.createLineBorder(Color.black, 1));

            addMouseListener( new MsHandler() );                // mouse button handler
        }

        /** Set the Net-Sta-SeedChan-Location values. */
        void setChannelLabels() {
          ChannelName ch = wfp.wfv.getChannelObj().getChannelName();
          setChannelLabels(ch.getNet(), ch.getSta(), ch.getSeedchan(), ch.getLocationString());
        }

        /** Set the Net-Sta-SeedChan-Location values. */
        void setChannelLabels(String net, String sta, String chan, String loc) {
          final String delim = ".";
          netLabel.setText(net+delim);
          staLabel.setText(sta+delim);
          chanLabel.setText(chan+delim);
          locLabel.setText(loc);
        }

        /** Reports the preferred size of this component as equal to the height of
         *  the WFPanel it represents. This keeps the row header aligned with the
         *  WFPanels in the WFGroupPanel. */
        public Dimension getPreferredSize() {
            //return new Dimension(60, wfp.getHeight());
            return new Dimension(75, wfp.getHeight()); // 75 for 12 pt String of "WWWWWW."?
        }

        /** Resize the font to adjust for changes in the panel size.
         * Synchronized in attempt to prevent repaint thrash as fonts are reset.
         * It's not clear it helps.*/
        synchronized void resetFont() {
            // this check prevents infinite recursion because setFont() queues a repaint
            if (frameHeight != wfp.getHeight()) {
              float fsize = Math.min(MaxTextSize, (float) (wfp.getHeight()/3.)); // divisor was 4.
              fsize = Math.max(fsize, 9.f); // set the min font size 
              Font font = netLabel.getFont().deriveFont(fsize);
              // these calls cause a repaint!
              netLabel.setFont(font);
              staLabel.setFont(font);
              chanLabel.setFont(font);
              locLabel.setFont(font);
              distLabel.setFont(font);
              frameHeight = wfp.getHeight();

              Graphics g = wfp.getGraphics();
              if (g != null) {
                  FontMetrics metrics = g.getFontMetrics(font);
                  int h = metrics.getHeight();
                  //System.out.println("fontSize:"+fsize+" max font sta width: "+metrics.stringWidth("WWWWWW.")); // 75 pixels?
                  int hh = (int) Math.floor(Math.round(frameHeight/2.));
                  //if (wfp.isShowing()) System.out.println("font height: " + h + " panelHeight: " + frameHeight + " maxDistHeight: " + hh);
                  distLabel.setVisible((h <= hh));
              }
            }
        }


        /* Inner class to handle popup menu events - removed -aww 2011/03/29
        private class PopupListener implements ActionListener {

            public PopupListener() {
              super();
            }
            public void actionPerformed(ActionEvent evt) {

              JComponent src = (JComponent) evt.getSource(); //the popup menu

              String action = evt.getActionCommand();

              // Delete the nearest pick associated with the selected solution
              if (action == "Channel Info") {

                String msg = "No Information";
                if (wfv.hasWaveform()) {
                  msg = wfv.getChannelObj().toDumpString();
                }
                JOptionPane.showMessageDialog(null, msg, "Channel Info", JOptionPane.PLAIN_MESSAGE);
              }
              else if (action == "Close Popup") {
                  // noop
              }
            }
        }
        */

        /** This handles display of the WFRowHeader popup.  */
        private class MsHandler extends MouseAdapter {
            int mods = 0;

            public void mousePressed(MouseEvent e) {

                mods = e.getModifiers();

                if (e.isPopupTrigger() || (mods & MouseEvent.BUTTON3_MASK) != 0) {
                  makePopup(e.getPoint()); // instead use implementation of method in WFPanel parent or its subclass - aww 2011/03/29
                }
                else { // set masterview model selected WFView for picking in zoom panel
                  wfv.mv.masterWFViewModel.set(wfv, true);
                  WFSelectionBox oldBox = wfv.mv.masterWFWindowModel.getSelectionBox(getWf()); // get old, may be new
                  if (oldBox.isNull()) { // null, if first time selected
                      wfv.mv.masterWFWindowModel.setFullView(getWf());
                  }
                  else { // scale amp window to this panel's wf, time unchanged
                      //wfv.mv.masterWFWindowModel.setFullAmp(getWf()); //TEST REMOVE for scaling test below -aww
                      wfv.mv.masterWFWindowModel.setAmpSpan(getWf(), dataBox); // TEST noise,gain scaling -aww 2014/02/24
                  }
                  
                }
            } // end of mouse pressed

        } // end of MsHandler class

    } // end of class WFRowHeader

} // end of WFPanel class

