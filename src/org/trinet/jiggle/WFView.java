package org.trinet.jiggle;

import java.util.*;
import org.trinet.jdbc.*;
import org.trinet.jasi.*;
//import org.trinet.jasi.TravelTime;
import org.trinet.jasi.coda.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;
/**
 * This is a container that holds a Waveform and bounds info for a
 * particular channel/time-window.
 * The viewSpan (time bounds) of the view may be set arbitrarily by the caller
 * and may be independent of the data that may actually be available.
 * Therefore, the viewSpan time window may
 * have no time series, be incomplete, or may clip a Waveform.
 * A WFView may have associated Phases, Amplitudes, etc. even if
 * there is no time series. <p>

<tt> <pre>
    WFView
    viewSpan.start                                             viewSpan.end
    |   |dataSpan.start                                 |dataSpan.end   |
    v   v                                               v               v
    +===WFView==========================================================+
    |   +---Waveform------------------------------------+               |
    |   +-----------+           +-----------------------+               |
    |   | WFSegment | time-tear |      WFSegment        |               |
    |   +-----------+           +-----------------------+               |
    |   tstart      tend        tstart                  tend            |
    |   +-----------------------------------------------+               |
    +===================================================================+
</pre></tt>
*/

public class WFView implements Channelable, DateLookUpIF {
    /** Channel description and info */
    // Note: a Waveform object also has a Channel's but this one is used for
    // sorting, etc.
    public Channel chan = null;

    boolean isActive = true; 

    private boolean isSelected = false; // for flagging candidate views for whatever reason determined by app

    protected String comment = null;

    /** Time series */
    public AbstractWaveform wf = null;

    /** List of original data packet start/stop times when read in from source. */
    //public TimeSpan packetSpans[]; // not used anywhere - aww 11/18/2005

    /** The time window of the virtural "view".
     *  It is independent of the actual data (dataSpan), especially if there is
     *  no waveform in the view.*/
    TimeSpan viewSpan = new TimeSpan();

    /** The span of the actual Waveform data. May contain time-tears. */
    TimeSpan dataSpan = new TimeSpan();

    /** List of Phases that are in this view */
    // This list is dynamically maintained by the update method in ActiveWFPanel.
    PhaseList phaseList = new PhaseList();

    /** List of Amplitudes in this view. */
    AmpList ampList = new AmpList();

    /** List of Amplitudes in this view. */
    CodaList codaList = new CodaList();

    /** One reference to the MasterView shared by all WFViews */
    // this looks suspect to me 1/17/02
    static MasterView mv = null;

    /** Travel time for P and S identification. */
    //TravelTime tt = TravelTime.getInstance();
    /**
     * Construct a nul view window.
     */
    public WFView() { }

    /**
     * Construct an empty view window.
     * ViewSpan and DataSpan will be empty.
     */
    public WFView(Channelable ch) {
        this.chan = Channel.create();
        this.chan.setChannelObj(ch.getChannelObj());
    }
    /**
     * Construct view window with defined viewSpan set to these time bounds.
     * DataSpan will be empty.
     */
     public WFView(Channelable ch, double t0, double tn) {
         this(ch);
         viewSpan.set(t0, tn);
     }

    /**
     * Construct view window with defined viewSpan time bounds.
     * DataSpan will be empty.
     */
     public WFView(Channelable ch, TimeSpan ts) {
         this(ch, ts.getStart(), ts.getEnd());
     }

    /**
     * Construct a view window containing the given WFSegment.
     * Set viewSpan time window to segment data time span.
     */
    public WFView(Waveform wf) {
        setWaveform(wf);
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean tf) {
        isSelected = tf;
    }

    /**
     * Give ALL WFViews a reference to the MasterView.
     */
    public static void setMasterView(MasterView masterView) {
        mv = masterView;
    }

    public String getComment() {
        return comment;
    }
    public void setComment(String rmk) {
        comment = rmk;
    }

    public void clear() {
        unloadTimeSeries();
        phaseList.clear();
        ampList.clear();
        codaList.clear();
    }

    /**
     * Set the WFSegment vector for this WFView to the one passed as arg.
     * Set viewSpan time window to include data time span.
     */
    public void setWaveform(Waveform wf) {

        if (wf == null) return;

        this.wf = (AbstractWaveform) wf;
        this.chan = Channel.create();
        this.chan.setChannelObj(wf.getChannelObj());

        dataSpan = new TimeSpan(wf.getTimeSpan());
        viewSpan.include(dataSpan);
    }

    /**
     * Set the viewSpan to the given Timespan. This is used to align traces by
     * setting all their viewSpans equal.
     */
    public void setViewSpan(TimeSpan ts) {
        viewSpan = new TimeSpan(ts);
    }

    public void setViewSpanStart(double start) {
        viewSpan.setStart(start);
    }

    public void setViewSpanEnd(double end) {
        viewSpan.setStart(end);
    }

    /**
     * Make sure the viewSpan includes the given TimeSpan.
     */
    public void viewSpanInclude(TimeSpan ts) {
        viewSpan.include(ts);
    }
    /**
     * Make sure the viewSpan includes the given time.
     */
    public void viewSpanInclude(double t0) {
        viewSpan.include(t0);
    }

    /**
     * Return the Waveform object. Will return 'null' if there is no waveform.
     */
    public AbstractWaveform getWaveform() {
        return wf;
    }

    /**
     * Returns 'true' if there is waveform header data in this WFView. The actual time-series
     * may or may not be loaded. Use hasTimeSeries() to check for actual time-series.
     */
    public boolean hasWaveform()  {
        // Possible for view created for a reading but have any associated waveform data!
        return (wf != null);
    }
    /**
     * Returns 'true' if there is actual time-series loaded for this WFView.
     */
    public boolean hasTimeSeries() {
        return (wf == null) ? false : wf.hasTimeSeries();

    }
    /**
     * Attempt to load the time series for his WFView. There may be none to load. Do
     * NOT reload if hasTimeSeries() is true. Returns 'true' if time-series was/is loaded.  */
    public synchronized boolean loadTimeSeries() {
        // don't reload if we already have it
        return (hasTimeSeries()) ? true : reloadTimeSeries();
    }

    /**
     * Attempt to reload the time series for his WFView. There may be none to load.
     Returns 'true' if time-series was reloaded.  */
    public boolean reloadTimeSeries() {
        return reloadTimeSeries(true);
    }

    public boolean reloadTimeSeries(boolean readCache) {
        // note that a view could be created for a reading with no waveform!
        return (hasWaveform()) ? wf.loadTimeSeries(readCache) : false;
    }

    /**
     * Attempt to unload the time series for his WFView. There may be none to load.
     Returns 'true' if time-series was unloaded.  */
    public synchronized boolean unloadTimeSeries() {
        if (wf == null) return false;
        if (wf.getSavingEvent() == null && mv != null) {
           Solution sol = mv.getSelectedSolution();
           if (sol != null) {
               if (sol.isClone()) { // need parent id for wf cache filename
                   wf.setSavingEvent(String.valueOf(sol.getParentId().longValue()));
               }
               else { // use evid
                   wf.setSavingEvent(String.valueOf(sol.getId().longValue()));
               }
           }
        }
        return wf.unloadTimeSeriesToCache();
    }

    public synchronized boolean writeWfToCache() {
        if (wf == null) return false;
        if (wf.getSavingEvent() == null && mv != null) {
           Solution sol = mv.getSelectedSolution();
           if (sol != null) {
               if (sol.isClone()) { // need parent id for wf cache filename
                   wf.setSavingEvent(String.valueOf(sol.getParentId().longValue()));
               }
               else { // use evid
                   wf.setSavingEvent(String.valueOf(sol.getId().longValue()));
               }
           }
        }
        return wf.writeToCache();
    }

/**
 * Returns an array of WFSegments that make up the Waveform in this WFView
 */
/*   public WFSegment [] getSegmentArray() {

       if (wf == null) return null;
       return WFSegment.getArray(wf.getSegmentList());

   }
*/
   // Implement the Channelable interface
    /** Set the Channel */
    public void setChannelObj(Channel chnl){
        if (chnl == null) this.chan = null; // remove if you want an NullPointerException  -aww
        else if (this.chan == null) this.chan = (Channel) chnl.clone(); // don't alias the original - aww
        else this.chan.copy((Channelable)chnl); // don't alias, copy contents only -aww
        // could update wf as side-effect well ? aww
        //if (wf != null) wf.setChannelObj(chnl);  // uses similar implementation to above logic
    }

    public void setChannelObjData(Channelable chnl) {
      // throws null pointer exception if input is null
      if (this.chan == null) this.chan = (Channel) chnl.getChannelObj().clone();
      else this.chan.setChannelObjData(chnl.getChannelObj()); // set instance data values to input's
    }

    /** Return the Channel. */
    public Channel getChannelObj() {
        return this.chan;
    }

    /** Calculate and set both epicentral (horizontal) distance and true (hypocentral)
     * distance of this channel from the given location in km.
     * Note: this is better than setDistance() or setHorizontalDistance() because
     * it does both and insures they are consistent. If the channel's LatLonZ or
     * the LatLonZ in the argument is null, both distances are set Channel.NULL_DIST.*/
    public double calcDistance(GeoidalLatLonZ loc) {
        double dist =  chan.calcDistance(loc);
         /* TEMP: Do this here to see if wf.toAmpInfoString returns correct dist or NULL_DIST -aww 2011/09/29 ???
         if (wf != null) {
             Channel wfch = wf.getChannelObj();
             if (wfch != null) {
                 wfch.setDistance(dist); // chan.getDistance();
                 wfch.setHorizontalDistance(chan.getHorizontalDistance());
                 wfch.setAzimuth(chan.getAzimuth());
             }
         }
         */
         return dist;
    }

    // In case you want to synch a revised calcDist 
    public void updateWfDistAz() {
        updateWfDistAz(this.wf);
    }

    public void updateWfDistAz(Waveform wf) {
        if (wf == null) return;
        Channel chnl = wf.getChannelObj();
        chnl.setDistance(getDistance());
        chnl.setHorizontalDistance(getHorizontalDistance());
        chnl.setAzimuth(getAzimuth());
    }

    /** Set the true (slant) distance. */
    public void setDistance(double distance) {
        chan.setDistance(distance);
    }

    /** Set the map (plan) distance. */
    public void setHorizontalDistance(double hdistance) {
        chan.setHorizontalDistance(hdistance);;
    }
    /** Return the true (slant) distance. */
    public double getDistance(){
        return chan.getDistance();
    }
    /** Return the map (plan) distance. */
    public double getHorizontalDistance(){
        return chan.getHorizontalDistance();
    }

    /** Return the vertical distance. */
    public double getVerticalDistance(){
        return chan.getVerticalDistance();
    }
    public boolean sameNetworkAs(Channelable ch) {
        return (ch == this) ? true : chan.sameNetworkAs(ch);
    }
    public boolean sameStationAs(Channelable ch) {
        return (ch == this) ? true : chan.sameStationAs(ch);
    }
    public boolean sameSeedChanAs(Channelable ch) {
        return (ch == this) ? true : chan.sameSeedChanAs(ch);
    }
    public boolean sameTriaxialAs(Channelable ch) {
        return (ch == this) ? true : chan.sameTriaxialAs(ch);
    }
    public boolean equalsChannelId(Channelable ch) {
        return (ch == this) ? true : chan.equalsChannelId(ch);
    }
    public void setAzimuth(double az) {
        chan.setAzimuth(az);
    }
    public double getAzimuth() {
        return chan.getAzimuth();
    }

    // see ChannelIdIF and ChannelIdentifiable interfaces
    public String toDelimitedSeedNameString(String delimiter) {
        return chan.toDelimitedSeedNameString(delimiter);
    }
    // see ChannelIdIF and ChannelIdentifiable interfaces
    public String toDelimitedNameString(String delimiter) {
        return chan.toDelimitedNameString(delimiter);
    }
// ChannelableIF  Channel update methods
    public boolean setChannelObjFromMasterList(java.util.Date aDate) {
      return chan.setChannelObjFromMasterList(aDate);
    }
    public boolean setChannelObjFromList(ChannelList chanList, java.util.Date aDate) {
      return chan.setChannelObjFromList(chanList, aDate);
    }
    public boolean matchChannelWithDataSource(java.util.Date aDate) {
      return chan.matchChannelWithDataSource(aDate);
    }

// Convenience methods - wrappers around interfaces methods:
    public boolean setChannelObjFromMasterList() {
      return setChannelObjFromMasterList(getLookUpDate());
    }
    public boolean setChannelObjFromList(ChannelList aList) {
      return setChannelObjFromList(aList, getLookUpDate());
    }
    public boolean matchChannelWithDataSource() {
      return matchChannelWithDataSource(getLookUpDate());
    }

    public java.util.Date getLookUpDate() {
        java.util.Date date = null;
        // use data time
        if (dataSpan != null) date = new DateTime(dataSpan.getStart(), true); // for UTC -aww 2008/02/10
        // use view time
        else if (viewSpan != null) date = new DateTime(viewSpan.getStart(), true); // for UTC -aww 2008/02/10
        // otherwise use current time
        else date = new DateTime(); // for UTC -aww 2008/02/10
        return date;
    }

    //AWW below method could be used for a Date sensitive ChannelList lookup
    //such as in the WFViewList loadChannelData method.
    //It doesn't check for span/range end time overlaps, assumes that data
    //valid at start apply to entire data time span of view, thus
    //no allowance for a Channel data change in "mid" view.
    public boolean copyChannel(ChannelList chanList) {
        if (chanList == null) return false;
        // Note: creates a ChannelList map if none exists:
        Channel listChan = chanList.findSimilarInMap(this.chan, getLookUpDate());
        return (listChan != null) ?  copyChannel(listChan) : false;
    }

    /**
     * Given a Collection of Channels (i.e. a station list), find the one that
     * matches this WFViews's current Channel. Replace the WFView's Channel object
     * with the "found" Channel which has complete Channel location & response info.
     * This is necessary for distance calcs, sorting, magnitudes, etc.  Return
     * 'true' if match was found, else return 'false'.  */
    public boolean copyChannel(Collection chanList) {
        if (chanList == null) return false;
        return copyChannel((Channel []) chanList.toArray(new Channel[chanList.size()]));
    }

    /**
     * Given an array of Channels (i.e. a station list),
     * find the one that matches this WFViews's current Channel.
     * Replace the WFView's Channel object with the "found" Channel
     * which has complete Channel location & response info.
     * This is necessary for distance calcs, sorting, magnitudes, etc.
     * Return 'true' if match was found, else return 'false'.  */
    public boolean copyChannel(Channel[] ch) {
        if (ch == null) return false;
        java.util.Date aDate = getLookUpDate();
        boolean status = false;
        for (int i = 0; i<ch.length; i++) {
          if (chan.equalsNameIgnoreCase(ch[i])) { // name match
            // Don't stop at first name match!
            // could be multiple Channels in list with same name
            // but with different active date ranges
            if ( ch[i].getDateRange().contains(aDate) ) { // date match
              status = copyChannel((Channel)ch[i]);
              if (status) break;  // got one, so bail.
            }
          }
        }
        return status;
    }

    public boolean copyChannel(Channelable ch) { // cf. Channel input arg -aww
        if (ch == null) return false;
        Channel chnl = ch.getChannelObj();
        return (this.chan != null && this.chan.equalsNameIgnoreCase(chnl)) ? copyChannel(chnl) : false;
    }

    // Note protected, no name comparison check here as above
    protected boolean copyChannel(Channel chanToCopy) { // - aww
        // if (chanToCopy == null) return false; //aww

        /* Don't copy ChannelName description - aww 04/07/2005
        String origChannelCodeString = chan.getChannel();
        // copy Channel data into WFView Channel (what about doing its waveform too? aww)
        setChannelObj(chanToCopy);
        // VH_/EH_ kludge to reset the "channel" of the Channel to the original
        chan.setChannel(origChannelCodeString);
        */

        setChannelObjData(chanToCopy); // only copy channel supporting data

        // could force side-effect copy update of wf as well below ? aww
        //if (wf != null) wf.setChannelObj(chan);

        return true;
    }

    /** Update the local phaseList by scanning the one in the MasterView. This
        * local list is used to more efficiently plot the phases. Returns the
        * number of phases in the phaselist for this WFView. */
    public int updatePhaseList() {
        return updatePhaseList(mv.getAllPhases());
    }
    /** Update the local phaseList by scanning the one passed. This
        * local list is used to more efficiently plot the phases. Returns the
        * number of phases in the phaselist for this WFView. */
    public int updatePhaseList(PhaseList list) {
        phaseList.clear();
        phaseList.addAll(list.getAssociatedByChannel(chan));
        return phaseList.size();
    }
    /** Update the local ampList by scanning the one in the MasterView. This
        * local list is used to more efficiently plotting. Returns the
        * number of amps in the list for this WFView. */
    public int updateAmpList() {
        return updateAmpList(mv.getAllAmps());
    }
    /** Update the local ampList by scanning the one in the MasterView. This
        * local list is used to more efficiently plotting. Returns the
        * number of amps in the list for this WFView. */
    public int updateAmpList(AmpList list) {
        ampList.clear();
        ampList.addAll(list.getAssociatedByChannel(chan));
        return ampList.size();
    }
    /** Update the local codaList by scanning the one in the MasterView. This
        * local list is used to more efficiently plotting. Returns the
        * number of codas in the list for this WFView. */
    public int updateCodaList() {
        return updateCodaList(mv.getAllCodas());
    }
    /** Update the local codaList by scanning the one in the MasterView. This
        * local list is used to more efficiently plotting. Returns the
        * number of codas in the list for this WFView. */
    public int updateCodaList(CodaList list) {
        codaList.clear();
        codaList.addAll(list.getAssociatedByChannel(chan));
        return codaList.size();
    }

    /** Update the local codaList by scanning the one in the MasterView. This
        * local list is used to more efficiently plotting. Returns the
        * number of codas in the list for this WFView. */
/*    public int updateJasiReadingList(JasiReadingList list) {

        list = (JasiReadingList) list.getAssociatedByChannel(chan);

        return list.size();
    }
*/
/** Return true if there are phases */
    public boolean hasPhases() {
        return !phaseList.isEmpty();
    }
/** Return true if there are amps */
    public boolean hasAmps() {
        return !ampList.isEmpty();
    }
/** Return true if there are codas */
    public boolean hasCodas() {
        return !codaList.isEmpty();
    }

/*
    public Phase getEmptyPhase() {
        Phase ph = Phase.create();
        ph.setChannelObj((Channel) chan.clone());
        return ph;
    }

    //TODO: add estimate of quality, and phase (based on ttime to selected origin)
    public Phase describePickAt (double dtime, Solution selSol)  {

        Phase ph = getEmptyPhase();
        ph.datetime.setValue(dtime);
//        ph.description.fm = PhaseDescription.DEFAULT_FM;
        double diff = 0.0;
        // figure first motion it there's a waveform
        if (hasTimeSeries()) {
            Sample samp1 = wf.closestSample(dtime);
            //        Sample samp2 = wf.closestSample(dtime + getSampleInterval() );
            Sample samp2 =
                wf.closestSample(dtime + wf.getSampleInterval());

            diff = samp1.value - samp2.value;

            // <> First motion
            if (diff < 0.0) {
                ph.description.fm = "c.";
            } else if (diff > 0.0) {
                ph.description.fm = "d.";
            } else  {
                ph.description.fm = PhaseDescription.DEFAULT_FM;
            }
        }

// <> Phase type, could be clever and check traveltimes
        double ttP = tt.getTTp(getHorizontalDistance(), mv.getSelectedSolution().getModelDepth()); // -aww 2015/10/10
        double ttS = ttP*tt.getPSRatio();
        double sMinusP = ttS - ttP;

        if (selSol == null) {
            ph.description.iphase = "P";        // guess P if no solution
        } else {
            double ot = selSol.datetime.doubleValue();

            double ttObs = dtime - ot;

        // Guess "S" if time is closer to expected S than expected P
        // i.e. if time is greater then P-time + 1/2 S-P
            if (ttObs >= (ttP+ (0.5 *sMinusP) ) ) {
            ph.description.iphase = "S";
         } else {
               ph.description.iphase = "P";
         }
        }

// <> "qual" (aka onset) i, e, w
// "i" if first diff is 10% of the full range, "e" if 5% (this is a kludge)
// but looks reasonable upon inspection.
//TODO: improve algorithm
        double firstDiffRatio = (double) Math.abs(diff) /(double) wf.rangeAmp();

        if (firstDiffRatio >= 0.10) {
            ph.description.ei = "i";
            //ph.description.setQuality(0.);
        } else if (firstDiffRatio >= 0.05) {
            ph.description.ei = "e";
            //ph.description.setQuality(.50);
        } else {
            ph.description.ei = "w";
            //ph.description.setQuality(.25);
        }
        //System.out.println ("firstDiffRatio = "+ firstDiffRatio + " Math.abs(diff)= "+ Math.abs(diff) + " wf.rangeAmp()= "+wf.rangeAmp());
// <> "quality" (aka weight)
        ph.description.setQuality(1.0);

// <> association, could be clever and check traveltimes
//        if (selSol != null) ph.assign(selSol); // associate(selSol); // adds it to phaseList

        return ph;

    }
*/
    /**
     * Return a WFSelectionBox the is based on the extremes of the data, including
     * waveforms and phases. If there is no waveform max/min amps cannot be set
     * based on the data so arbitrary values of +- 100 will be usex1d.  */
    public WFSelectionBox getViewBox() {
        if (wf != null && wf.hasTimeSeries())  {
          return new WFSelectionBox(viewSpan, wf.getMaxAmp(), wf.getMinAmp());
        } else {
          // box must have non-zero dimension else /0 error happen elsewhere
          return new WFSelectionBox(viewSpan, 1, 0);
        }
    }

    /** Return the TimeSpan of the view. */
    public TimeSpan getViewSpan() {
        return viewSpan;
    }

    /**
     * Return a WFSelectionBox the is based on the extremes of the data, including
     * waveforms and phases. If there is no waveform max/min amps cannot be set
     * based on the data so arbitrary values of +- 100 will be used.  */
    public WFSelectionBox getDataBox() {
        if (wf != null) {
          if (wf.hasTimeSeries()) {
             return new WFSelectionBox(dataSpan, wf.getMaxAmp(), wf.getMinAmp());
          }
          else {
             // preserve data range in case wf was unloaded and user changes masterview alignment choice P, S or V
             // which lookup the last used dataSpan via wfvList.getEarliestSampleTime to set the new view span -aww 2009/04/03
             return new WFSelectionBox(dataSpan, 1, 0);
          }
        } else { 
          // box must have non-zero dimension else /0 error happens elsewhere
          // can't use dataSpan because it depends on wf bounds
          return new WFSelectionBox(viewSpan, 1, 0);
        }
    }
    /**
    * Set the WFView's 'dataSpan' value to match the actual data in the Waveform.
    */
    public void autoSetDataSpan() {
        if (wf != null) dataSpan = wf.getTimeSpan();
    }

    public String toString() {
        return dumpToString();
    }

    /**
     * Dump some info about the view for debugging
     */
    public void dump() {
        System.out.println(dumpToString());
    }

    /**
     * Dump some info about the view for debugging
     */
    public String dumpToString() {
        StringBuilder sb = new StringBuilder(512);
        sb.append("WFView: ").append(this.chan.toString());
        sb.append(" hdist= ").append(String.format("%7.2f",getHorizontalDistance())); // don't use slant aww 06/11/2004

        if (wf != null) {
            sb.append(" nsegs= ").append(wf.getSegmentList().size());
            sb.append(" isActive= ").append(isActive);
            sb.append("\n");
            sb.append(" nsample= ").append(wf.samplesInMemory());
            sb.append(" maxamp= " ).append(wf.getMaxAmp());
            sb.append(" minamp= " ).append(wf.getMinAmp());
            sb.append(" nphase= " ).append(phaseList.size());
            sb.append(" namp= " ).append(ampList.size());
            sb.append(" ncoda= " ).append(codaList.size());
        } else {
            sb.append(" -- no waveform available --");
        }

        sb.append("\n   viewSpan= ").append(viewSpan.toString());
        sb.append(" : ").append(viewSpan.toEpochTimeString());  // get raw double time
        sb.append("\n");

        sb.append("   dataSpan= ").append(dataSpan.toString());
        sb.append(" : ").append(dataSpan.toEpochTimeString());  // get raw double time
        sb.append("\n");

        return sb.toString();
    }

} // end of class
