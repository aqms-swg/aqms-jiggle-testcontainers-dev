package org.trinet.jiggle;

import java.util.*;
//import java.sql.*;
import javax.swing.event.*;

import org.trinet.util.gazetteer.*;
import org.trinet.util.*;
import org.trinet.jdbc.*;
import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;

/**
 * A List of WFViews. This also provides sort capabilities and waveform caching.
 * This extends ChannelableList so changes to this list fire change events to
 * notify any listeners of changes to this list.<p>
 *
 * Via its own change event listener it keeps the private reading list of each
 * WFView up-to-date with
 */

public class WFViewList extends ChannelableList {

    //boolean debug = true;
    boolean debug = false;

    /** Optional one-shot background Waveform loading thread. Seldom used
    * because it tends to run the process out of memory for large events.*/
    WFLoaderThread wfLoaderThread;

    /** Optional Waveform cache manager */
    WFCacheManager cacheMgr;

    int cacheAbove = 0; // added -aww 2010/08/06
    int cacheBelow = 0; // added -aww 2010/08/06

    boolean cachingEnabled = false; // default

    /** Used to control and synchronize waveform loading thread. */
    Object  loaderLock   = new Object();
    boolean stopLoader   = false;

    WFView selectedWFView = null;

    public WFViewList() {}

    public WFViewList(int initialCapacity) {
        super(initialCapacity);
    }

    public WFViewList(Collection col) {
        super(col);
    }

/**
 * Return WFView for the given Channelable. Returns null if none found.
 */
    public WFView get(Channelable chan) {
        return (WFView) getByChannel(chan);
    }

    /**
     * Simplify the conversion of this ArrayList to an array.
     */
    public WFView[] getArray() {
        WFView wfv [] = new WFView[this.size()];
        this.toArray(wfv);
        return wfv;
    }


/**
     * Load the Channel object of each WFView with location and response info.
     * If there is a static Channel list in memory use that. If not, look up the
     * info  for each channel in the WFView list at the DataSource (dbase).
     * Use this method in lieu of MasterChanneList.loadChannelData(ChannelableList, Date)
     * because we must use the view timespans to lookup the data and also must also set
     * the channelObj in the view's Waveform.
     */
    public void loadChannelData() {

        int count = size();
        if (count == 0) return; // bail if no WFViews

        // its more efficient to load the whole list if there are more then 'n' channels.
        final int efficiencyCrossover = 100;   // if more then this many channels, load all
        if (MasterChannelList.isEmpty() && size() > efficiencyCrossover) {
          if (debug)
              System.out.println("DEBUG WFViewList loadChannelData, MasterChannelList is empty, readCurrentlist");
          //Could read list here if input arg of MasterView TimeSpan start see commented out code below - aww
          //MasterChannelList.set(ChannelList.readList(new DateTime(mv.timeSpan.getStart(), true)));
          MasterChannelList.set(ChannelList.readCurrentList());
        }

        WFView wfv[] = getArray();

        // channel info is already loaded
        if ( ! MasterChannelList.isEmpty() ) {

          Channel chArray[] = MasterChannelList.get().getArray();

          if (debug)
            System.out.println("DEBUG WFViewList loadChannelData() matching MasterChannelList to each WFView...");

          for (int i = 0; i<count; i++)  {
            // find matching ChannelList Channel and copy it to WFView Channel 
// Why not change setChannelObject and copyChannel methods in WFView
// to also update a non-null Waveform channel object as well ? aww
            if (wfv[i].copyChannel(chArray)) { // could use new method WFView.copyChannel(ChannelList) - aww
              // code line below could be moved to WFView copyChannel(ch) - aww
              if (wfv[i].wf != null) wfv[i].wf.setChannelObj(wfv[i].chan);
            } else {
              if (debug) {
                System.out.println("WFViewList loadChannelData() NO MasterChannelList Channel match for : " +
                                   wfv[i].getChannelObj().toDelimitedSeedNameString());
              }
              // Try individual channel lookup - channel may not be in MasterChannelList
              setWFViewChannelFromDataSource(wfv[i]);
            }
          }
        } else {
        // no MasterChannelList data, look up each Channel in dbase
            if (MasterChannelList.get() == null) MasterChannelList.set(new ChannelList()); // must make a new list
            if (debug)
              System.out.println( "DEBUG WFViewList loadChannelData() loading DataSource data for each WFView...");
            for (int i = 0; i<count; i++)  {
              setWFViewChannelFromDataSource(wfv[i]); // copy Channel info for each WFView
            }
        }
        if (debug) System.out.println("DEBUG WFViewLIst loadChannelData finished, looked up: "+count+" channels.");
    }

    private void setWFViewChannelFromDataSource(WFView wfv) {

        DateTime datetime = new DateTime(wfv.getViewSpan().getStart(), true); // for UTC time -aww 2008/02/11
        Channel newChan = wfv.chan;
        newChan = newChan.lookUp(newChan, datetime); // returns input wfv Channel if not in DataSource

        if (newChan == null) { // lookUp error, should return input or new channel
          if (debug) {
            System.out.println("WFViewList setWFViewChannelFromDataSource() NULL Channel returned by lookUp for : "
                + wfv.chan.toDelimitedSeedNameString(".") + " of Channel data for date: " + datetime.toString());
          }
          return;  // don't set a NULL channel reference in input wfv
        }
        if (wfv.chan != newChan) { // => Channel from DS
          MasterChannelList.get().add(newChan);
        }
        else { // => original Channel NOT from DS
          if (debug) {
            System.out.println("WFViewList setWFViewChannelFromDataSource() NO DataSource Channel match for : "
                + wfv.chan.toDelimitedSeedNameString(".") + " of Channel data for date: " + datetime.toString());
          }
          //If returned Channel added to MasterList only if from DataSource,
          //we do a lookUp for similar missing data on subsequent event loads, 
          //if added to MasterList even if the data was not found in DataSource
          //we do no DataSource lookUp for subsequent event Channel loads,
          //but we have no LatLonZ or mag/stacorrections for that Channel - aww
          //MasterChannelList.get().add(newChan); // force lookup, database may have been updated - aww 07/14/2005
        }
// Note the "copyChannel" vs. "setChannelObj"
// is this the same interpretation as in loadChannelData above -aww
        wfv.copyChannel((Channel)newChan); // substitute with channel attribute KLUDGE
        // AWW if not done by copyChannel or by WfView.setChannelObj method:
        // we must also set WFView's Waveform channel object here:
        if (wfv.wf != null) wfv.wf.setChannelObj(wfv.chan); // insurance - aww
    }

/**
 * Return array of WFViews sorted by increasing distance from a point (most
 * likely an epicenter) but do NOT sort the actual WFViewList.  Note that
 * further manipulation of the WFViewList will make this array obsolete.  */
  public WFView[] getNewSortedArray(GeoidalLatLonZ loc) { // aww 06/11/2004

      calcDistances(loc);

      WFView wfv[] = getArray();

      double distArray[] = new double[wfv.length]; // array for distances
      for ( int i = 0; i < wfv.length; i++) // copy the distance array
      {
          distArray[i] = wfv[i].getHorizontalDistance(); // slant dist ok for sort aww 06/11/2004
      }

      IndexSort indexSort = new IndexSort();
      int idx[] = indexSort.getSortedIndexes(distArray);    // sort by distance

// create new sorted WFView array

      WFView sortedwfv[] = new WFView[wfv.length]; // empty array, same size

      for (int i = 0; i < wfv.length; i++)
      {
      sortedwfv[i] = wfv[idx[i]];
      }
      return  sortedwfv;
  }

/**
 * Calculate the distance from location in the argument for each site in the
 * list.  */

  public void calcDistances(GeoidalLatLonZ loc) { // aww 06/11/2004

    WFView wfv [] = this.getArray();
    for ( int i=0; i < wfv.length; i++) {
         wfv[i].calcDistance(loc);
    }

  }

/** Return the ealiest view start time in the WFViewList. Returns 0.0 if no views. */
  public double getEarliestViewTime() {

    WFView wfv [] = getArray();

    double earliest = Double.MAX_VALUE;

    for ( int i=0; i < wfv.length; i++) {

      earliest = Math.min(earliest, wfv[i].getViewSpan().getStart());
    }
    return earliest;
  }
/** Return the latest view end time in the WFViewList. Returns 0.0 if no views. */
  public double getLatestViewTime() {

    WFView wfv [] = getArray();

    double latest = -2147483648.;

    for ( int i=0; i < wfv.length; i++) {

      latest = Math.max(latest, wfv[i].getViewSpan().getEnd());
    }
    return latest;
  }
/** Return the ealiest data sample time in the WFViewList. Returns 0.0 if no views. */
  public double getEarliestSampleTime() {

    WFView wfv [] = getArray();

    double earliest = Double.MAX_VALUE;

    for ( int i=0; i < wfv.length; i++) {
        earliest = Math.min(earliest, wfv[i].getDataBox().getStartTime());
    }
    return earliest;
  }
/** Return the latest data sample time in the WFViewList. Returns 0.0 if no views. */
  public double getLatestSampleTime() {

    WFView wfv [] = getArray();

    double latest = -2147483648.;

    for ( int i=0; i < wfv.length; i++) {
        latest = Math.max(latest, wfv[i].getDataBox().getEndTime());
    }
    return latest;
  }
/**
 * Load all the waveforms for WFViews in this list. This can be a time consuming
 * task and caller will block until this is done.
 * Use loadWaveformsInBackground() to do in a separate thread and not block.
 */
  public void loadWaveformsNow() {

    WFView wfv[] = getArray();

    for (int i = 0; i<wfv.length; i++)  {
        wfv[i].loadTimeSeries();
        // System.out.println("loaded wf: "+i+ " "+ wfv[i].toString());  // DEBUG
    }
  }
// ****** WOULD THERE BE A COLLISION IF OTHER DBASE ACTIVITY WAS DONE? *********

/** Begin loading waveforms in a background thread. Each waveform object will
fire a change event after it is loaded so components can do something, like repaint,
when each waveform load is done.*/
  public void loadWaveformsInBackground() {
    wfLoaderThread = new WFLoaderThread(this);
  }

/** Stop the waveforms loader thread. Thread will stop after the next waveform
    load is done. */
  public void stopLoadWaveformsInBackground() {
      synchronized (loaderLock) {
        stopLoader = true;
        loaderLock.notifyAll();
      }
  }
//////// INNER CLASS
/** This is a one-shot thread that loads ALL waveforms in background when
* it runs. Not to be confused with
* WFCacheManager which dynamically loads and unloads waveforms. */
  class WFLoaderThread implements Runnable {

    WFViewList wfvList;
    public Thread thread;

    // constructor - also starts thread
    public WFLoaderThread(WFViewList wfvList) {
        this.wfvList = wfvList;
        thread = new Thread(this);
        stopLoader = false;
        thread.start();
    }

    public void run() {

      try {

        WFView wfv[] = wfvList.getArray();
        for (int i = 0; i<wfv.length; i++)  {

          boolean status = wfv[i].loadTimeSeries();

          if (debug) {
            if (wfv[i].wf != null) {
              System.out.print("WFViewList wfv.loadTimeSeries for: "+ wfv[i].wf.toString()+" "+status);
            }
            else {
              System.out.print("WFViewList wfv.loadTimeSeries NULL waveform for: "+ wfv[i].chan.toDelimitedSeedNameString());
            }
          }

          synchronized (loaderLock) {
            if (stopLoader) return;
          }
        }
      }
      catch (Exception ex) {
          ex.printStackTrace();
      }
    } // end of run()
  } // end of WFLoaderThread class
////////

/** Dump the whole list. */
  public void dump() {
      WFView wfv [] = getArray();
      for ( int i=0; i < wfv.length; i++) {
          wfv[i].dump();
      }
  }

// //////////////////////////////////////////////////////////////////
// Waveform cache methods

/** Enable/disable waveform caching with default parameters. This does not start
    the cache manager, that is done by a call to setCacheIndex(int index). */
  public void setCachingEnabled(boolean tf) {

      cachingEnabled = tf;

      if (tf) {
        //cacheMgr = new WFCacheManager(this);  // removed need to use existing cache limits -aww 2010/08/06
        cacheMgr = new WFCacheManager(this, cacheAbove, cacheBelow);  // set cache limits here -aww 2010/08/06
      } else {
        stopCacheManager();
      }
  }

  public void stopCacheManager() {
    if (cacheMgr != null) {
        cacheMgr.stopCacheMgr();
        cacheMgr = null;
    }
  }

/** Returns 'true' if waveform caching is enabled */
  public boolean getCachingEnabled() {
    return cachingEnabled;
  }

/** Returns the WFCacheManager. Returns 'null' if waveform caching is not enabled. */
  public WFCacheManager getWFCacheManager() {
    return cacheMgr;
  }

/** Specify the size of the cache to use when caching is enabled. Sets the number of
 * panels above and below the current index. The current index is considered part of the
 * below value so that the Cache size = above + below. */
  public void setCacheSize(int above, int below) {
    //if (getCachingEnabled()) { // just set it, since mgr is not run if caching is disabled // removed -aww 2010/08/06
      cacheAbove = above; // added -aww 2010/08/06
      cacheBelow = below; // added -aww 2010/08/06
      if (cacheMgr != null) cacheMgr.setCacheSize(above, below);
    //}
  }

/** Move the logical index of the waveform cache to the new position. */
  public void setCacheIndex(int index) {

    if (debug) {
        System.out.println("WFViewList setCacheIndex("+index+ ") getCachingEnabled()= "+ getCachingEnabled() +
                " cacheAbove: " + cacheAbove + " cacheBelow: " + cacheBelow );
    }

    if (getCachingEnabled()) {
      if (cacheMgr == null) cacheMgr = new WFCacheManager(this, cacheAbove, cacheBelow);  // set cache limits here -aww 2010/08/06
      if (cacheMgr != null) cacheMgr.setIndex(index);
      else System.err.println("WFViewList: Error? CacheMgr null/stopped at index = " + index);
    }
  }

  public void loadWaveformsInCache() {
    setCacheIndex(-1);
  }

//////////////////////////////////////////////////////
/** INNER CLASS: Handle changes to the JasiObject lists. */
/*
class JasiReadingChangeListener implements ChangeListener {

     WFView wfv;
     JasiReading jr;

  public void stateChanged(ChangeEvent changeEvent) {

     // 'arg' will be either a JasiReading or a JasiReadingList
     Object arg = changeEvent.getSource();

        if (arg instanceof JasiReading) {

        jr = (JasiReading) arg;
        wfv = get(jr.getChannel());

        if (wfv != null) updateOneReadingList(wfv, jr);

        return;
      } else if (arg instanceof JasiReadingList) {
       updateAllReadingLists( (JasiReadingList)arg );
     }

     } // end of stateChanged


     protected void updateOneReadingList(WFView wfv, JasiReading jr) {

          if (jr instanceof Phase) {
        wfv.updatePhaseList();
      } else if (jr instanceof Coda) {
         wfv.updateCodaList();
      } else if (jr instanceof Amplitude) {
         wfv.updateAmpList();
       }
     }

     protected void updateAllReadingLists(JasiReadingList jrl) {
       WFView wfv [] = getArray();
          if (jrl instanceof PhaseList) {
         for ( int i=0; i < wfv.length; i++) { wfv[i].updatePhaseList(); }
      } else if (jrl instanceof CodaList) {
         for ( int i=0; i < wfv.length; i++) { wfv[i].updateCodaList(); }
      } else if (jrl instanceof AmpList) {
         for ( int i=0; i < wfv.length; i++) { wfv[i].updateAmpList(); }
       }
     }
} // end of JasiReadingChangeListener
*/

/*
  // Main for testing.
  public static class Tester {
    public static void main(String args[])  {
      int evid;

      if (args.length <= 0) {
        System.out.println("Usage: java WFViewList [evid])");

        evid = 9526852;

        System.out.println("Using evid "+ evid+" as a test...");

      } else {

        Integer val = Integer.valueOf(args[0]);    // convert arg String to 'double'
        evid = (int) val.intValue();
      }

      System.out.println("Making connection...");
      DataSource init = TestDataSource.create();  // make connection

      // NOTE: this demonstrates making a static Channel list.
      //        System.out.println("Reading in station list...");
      //  Channel.setList(Channel.getAllList());

      System.out.println("Making MasterView for evid = "+evid);

      MasterView mv = new MasterView();

      // test limit
      //  mv.setWaveformLoadLimit(200);
      mv.defineByDataSource(evid);

      //  mv.loadChannelData();

      mv.dump();

      //  mv.phaseList.dump();

      Solution sol[] = mv.solList.getArray();
      PhaseList pl = (PhaseList) mv.getSelectedSolution().phaseList.getAssociatedWith(sol[0]);
      pl.dump();

      int k = 1;
      while (mv.wfvList.wfLoaderThread.thread.isAlive()) {

        System.out.println("Waiting for wfloader to finish..."+ k++);

        try{
          Thread.sleep(1000);
        } catch (InterruptedException e){}

          // test killing loader thread
          System.out.println("### Killing loader thread...");
          mv.wfvList.stopLoadWaveformsInBackground();
      }

    } // end of main
  } // end of Tester
  */
} // end of WFViewList
