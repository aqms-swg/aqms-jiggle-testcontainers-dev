package org.trinet.jiggle;

import java.util.Collection;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.swing.JOptionPane;

import org.trinet.jasi.ActiveList;
import org.trinet.util.WaveClient;

/**
* A named list of WaveServers. This is an extension of org.trinet.waveserver.rt.WaveClient
* @See: org.trinet.waveserver.rt.WaveClient
*/
public class WaveServerGroup implements java.io.Serializable,
                                        org.trinet.waveserver.rt.WaveServerSource {

     /** Name for easy identification of this group of Waveservers */
     String name;

     /** Set 'true' if this WaveServerGroup is currently selected. */
     boolean selected = false;

     protected WaveClient wc;

     protected static String sDefaultWaveClientClassName = "org.trinet.waveserver.rt.WaveClientNew"; // 04/04/2006 -aww

     public static boolean doPopupOnAddServerError = false;

     // Millisecs
     protected static int connectTimeout = WaveClient.DEFAULT_CONNECT_TIMEOUT;

     public WaveServerGroup(String name) {
           this(sDefaultWaveClientClassName, name);
     }

     public WaveServerGroup(String sWaveClientImplementation, String name) {
            wc = WaveClient.createWaveClient(sWaveClientImplementation);
            setName(name);
     }
     /** Create this WaveServerGroup. The constructor actually tests the connection
         to the server, if it is bad it may take a long time for retries and timeouts. */
     public WaveServerGroup(String sWaveClientImplementation, String name, int maxRetries,
                            int maxTimeoutMilliSecs, int connectTimeoutMilliSecs,
                            boolean verifyWaveforms, boolean truncateAtTimeGap)
     {
            wc = WaveClient.createWaveClient(sWaveClientImplementation);

            wc.configureWaveClient(maxRetries, maxTimeoutMilliSecs, connectTimeoutMilliSecs,
                            verifyWaveforms, truncateAtTimeGap);
            setName(name);
     }

     /** Create this WaveServerGroup. The constructor actually tests the connection
         to the server, if it is bad it may take a long time for retries and timeouts. */
     public WaveServerGroup(String sWaveClientImplementation, String name, int maxRetries,
                            int maxTimeoutMilliSecs, boolean verifyWaveforms, boolean truncateAtTimeGap)
     {
             this(sWaveClientImplementation, name, maxRetries, maxTimeoutMilliSecs, connectTimeout,
                     verifyWaveforms, truncateAtTimeGap);
     }

     /** Create this WaveServerGroup. The constructor actually tests the connection
         to the server, if it is bad it may take a long time for retries and timeouts. */
     public WaveServerGroup(String sWaveClientImplementation, String name, String propertyFileName) {

            wc = WaveClient.createWaveClient(sWaveClientImplementation);

            wc.configureWaveClient(propertyFileName);
            setName(name);
     }

     /** Create this WaveServerGroup. The constructor actually tests the connection
         to the server, if it is bad it may take a long time for retries and timeouts. */
    public WaveServerGroup(String name, int maxRetries, int maxTimeoutMilliSecs, int connectTimeoutMilliSecs,
                            boolean verifyWaveforms, boolean truncateAtTimeGap)
    {
            this(sDefaultWaveClientClassName, name, maxRetries, maxTimeoutMilliSecs, connectTimeoutMilliSecs, verifyWaveforms, truncateAtTimeGap);

    }

     public WaveClient getWaveClient() {
       return wc;
     }

/** Set the name of this servergroup */
     public void setName(String name) {
       this.name = name;
     }
/** Return the name of this server group */
     public String getName() {
             return name;
     }

/** Sets selected flag true or false. */
     public void setSelected(boolean tf) {
            selected = tf;
     }

/** Returns boolean value of selected flag. */
     public boolean isSelected() {
        return selected;
     }

/** Override Object.equals() method so you can only add one WaveServerGroup with
* a given name to a Collection. */
    public boolean equals(Object obj) {
      if (!(obj instanceof WaveServerGroup)) return false;

      return name.equals( ((WaveServerGroup)obj).getName());

    }

/** Return a multiline string describing this server group. <br>
 *  Example:<p>
<tt>
Name: test maxRetrys = 3 max/timeout = 30.0 verifyWaveforms = true truncateAtTimeGap = false
Server count = 4
serverje.gps.caltech.edu:6500
serversp.gps.caltech.edu:6501
serversp.gps.caltech.edu:6500
serverje.gps.caltech.edu:6501
</tt> */
     public String toString() {

       String str = "Name: "+ getName() +
              " maxRetrys = "+ wc.getMaxRetries()+
              " max/timeout = "+ wc.getMaxTimeoutMilliSecs()/1000.0+
              " verifyWaveforms = "+ wc.isVerifyWaveforms() +
              " truncateAtTimeGap = "+ wc.isTruncateAtTimeGap();

       str += "\nServer count = "+wc.numberOfServers() +"\n";


       str += listToString();

       return str;
       }

/**
* Return the WaveServerGroup with the given name from the given list. Names ARE
* case sensitive. Returns null if no match is found.
*/
  public static WaveServerGroup getByName(Collection list, String name) {
       if (list == null || name == null) return null;
       WaveServerGroup ws[] = new WaveServerGroup[list.size()];
       list.toArray(ws);

       for (int i = 0; i<ws.length; i++ ) {
         if (ws[i].getName().equals(name)) return ws[i];
       }

       return null;
  }

 /**
* Return first WaveServerGroup in the list with its selected flag = true.
* Returns null if none is found.
*/
  public static WaveServerGroup getSelected(Collection list) {
       if (list == null) return null;
       WaveServerGroup ws[] = new WaveServerGroup[list.size()];
       list.toArray(ws);

       for (int i = 0; i<ws.length; i++ ) {
         if (ws[i].isSelected()) return ws[i];
       }

       return null;
  }

/**
* Sets the given WaveServerGroup as the only "selected" one in the list.
*/
  public static void setSelected(WaveServerGroup wsg, Collection list) {
       deselectAll(list);
       wsg.setSelected(true);
  }


/**
* Sets the default WaveClient implementation for WaveServerGroup.
*/
  public static void setDefaultWaveClient(String sWaveClient) {
     sDefaultWaveClientClassName = sWaveClient;
  }


/**
* Set the selected flag = false for ALL in the list.
*/
  public static void deselectAll(Collection list) {
       WaveServerGroup ws[] = new WaveServerGroup[list.size()];
       list.toArray(ws);

       for (int i = 0; i<ws.length; i++ ) {
         ws[i].setSelected(false);
       }
  }

       /** Return a string with just the servers listed, seperated by \n's */
       public String listToString() {
         String str = "";
         String server[] = wc.listServers();
         for (int i = 0; i<server.length;i++) { str += server[i]+"\n"; };
         return str;
       }
        /** Create a string to be written to a properties file. WaveServerGroups
        are delimited by "/"s tokens within a WaveServerGroup are space delimited.
        The url and port are delimited with a ":". <p>
        *
        * Example:<br>
        <tt>
/servergroup1+3+30000+true+false+serverje.gps.caltech.edu\:6500+serversp.gps.caltech.edu\:6501+serversp.gps.caltech.edu\:6500+serverje.gps.caltech.edu\:6501/server\ group\ 2+3+30000+true+false+serverje.gps.caltech.edu\:6500+serverho.gps.caltech.edu\:6501+serverho.gps.caltech.edu\:6500+serverje.gps.caltech.edu\:6501/
        </tt>
        */
        public static String toGroupListPropertyString(Collection wsgs) {
           String dim = "/";
           String str = dim;
           for (int i = 0; i< wsgs.size(); i++) {
              str += WaveServerGroup.toPropertyString((WaveServerGroup)((ArrayList) wsgs).get(i)) + dim;
           }
           return str;
        }

        /** Create a string to be written to a properties file.
        * WaveServerGroups are delimited by "/"s. */
        public static String toPropertyString(WaveServerGroup wsg) {

        // use "+" as delimiter to allow spaces in the group name
           String dim = "+";
           String str = wsg.getName() + dim +
                        wsg.getMaxRetries() + dim +
                        wsg.getMaxTimeoutMilliSecs() + dim +
                        wsg.isVerifyWaveforms() + dim +
                        wsg.isTruncateAtTimeGap();

          String server[] = wsg.listServers();
          for (int i = 0; i<server.length;i++) { str += dim +server[i]; };

          return str;

          }

          /** return collection of WaveServerGroups
          * Example:<br>
        <tt>
/servergroup1+3+30000+true+false+serverje.gps.caltech.edu\:6500+serversp.gps.caltech.edu\:6501+serversp.gps.caltech.edu\:6500+serverje.gps.caltech.edu\:6501/server\ group\ 2+3+30000+true+false+serverje.gps.caltech.edu\:6500+serverho.gps.caltech.edu\:6501+serverho.gps.caltech.edu\:6500+serverje.gps.caltech.edu\:6501/
        </tt>
        * Note the "escaped" characters like "\:" and "\ ".
        */
        public static ActiveList parsePropertyString(String str) {
        //public static ArrayList parsePropertyString(String str) {

            ActiveList wsgList = new ActiveList();
            // parse the list
            String del1 = "/";               // separator between groups
            // use "+" as delimiter to allow spaces in the group name
            String del2 = "+";               // separators between fields
            String del3 = ":";               // separators between url:port
            StringTokenizer strTok1 = new StringTokenizer(str, del1);

            try {
              while (strTok1.hasMoreTokens()) {

              // WaveServerGroup line
                 StringTokenizer strTok2 = new StringTokenizer(strTok1.nextToken(), del2);

                     WaveServerGroup wsg =
                          new WaveServerGroup(
                              strTok2.nextToken(),                   //name
                              Integer.parseInt(strTok2.nextToken()), //retry
                              Integer.parseInt(strTok2.nextToken()), //timeout
                              connectTimeout, // place holder here, else requires property format update to parse from input token
                              strTok2.nextToken().equalsIgnoreCase("true"), //verify
                              strTok2.nextToken().equalsIgnoreCase("true")  //truncate
                             );
                 // now get the url:port pairs
                 while (strTok2.hasMoreTokens()) {
                    StringTokenizer strTok3 = new StringTokenizer(strTok2.nextToken(), del3);
                    wsg.addServer(strTok3.nextToken(), Integer.parseInt(strTok3.nextToken()) );
                 }

                 wsgList.add(wsg);
              }
            } catch (Exception ex) {
              ex.printStackTrace();
              System.err.println("Bad syntax in property string:" +str);
              return null;
            }
            // set first as selected
            if (! wsgList.isEmpty()) {
              WaveServerGroup wsg = (WaveServerGroup) wsgList.get(0);
              if (wsg != null)  WaveServerGroup.setSelected(wsg, wsgList);
            }

            return  wsgList;
        }



  public int numberOfServers() {
    return wc.numberOfServers();
  }

  public void setTruncatetAtTimeGap(boolean bTruncate) {
    wc.setTruncateAtTimeGap(bTruncate);
  }

  public int getMaxRetries() {
    return wc.getMaxRetries();
  }

  public int setMaxRetries(int MaxRetries) {
    return wc.setMaxRetries(MaxRetries);
  }

  public static int setConnectTimeout(int timeoutMilliSecs) {
    connectTimeout = timeoutMilliSecs;
    return connectTimeout;
  }

  public static int getConnectTimeout() {
    return connectTimeout;
  }

  public int setMaxTimeoutMilliSecs(int MaxTimeoutMilliSecs) {
    return wc.setMaxTimeoutMilliSecs(MaxTimeoutMilliSecs);
  }

  public int getMaxTimeoutMilliSecs() {
    return wc.getMaxTimeoutMilliSecs();
  }

  public boolean isVerifyWaveforms() {
    return wc.isVerifyWaveforms();
  }

  public boolean setWaveformVerify(boolean bVerify) {
    return wc.setWaveformVerify(bVerify);
  }

  public boolean isTruncateAtTimeGap() {
    return wc.isTruncateAtTimeGap();
  }

  public boolean setTruncateAtTimeGap(boolean bTruncate) {
    return wc.setTruncateAtTimeGap(bTruncate);
  }

  public boolean addServer(String host, int port) {
    boolean status = wc.addServer(host,port);
    if (! status && doPopupOnAddServerError) {
        JOptionPane.showMessageDialog(null,
                "WaveClient add server failed for: " + host+":"+port+", see messages.",
                "WaveServerGroup Add Server", JOptionPane.ERROR_MESSAGE
        );
    }
    return status;
  }

  public String [] listServers() {
    return wc.listServers();
  }

/** Required for WaveServersource interface */
  public int getTimeSeries(org.trinet.jasi.Waveform jasiWaveform) {
    return getJasiWaveformDataRaw(jasiWaveform);
  }

  public int getJasiWaveformDataRaw(org.trinet.jasi.Waveform jasiWaveform) {
    return wc.getJasiWaveformDataRaw(jasiWaveform);
  }

/*
     public static void main(String[] args) {

          ArrayList wsgList = new ArrayList();

          WaveServerGroup wsg = new WaveServerGroup("test1");

          wsg.addServer("serverat.gps.caltech.edu", 6500);
          wsg.addServer("serverat.gps.caltech.edu", 6501);
          wsg.addServer("serverpa.gps.caltech.edu", 6500);
          wsg.addServer("serverpa.gps.caltech.edu", 6501);

          wsgList.add(wsg);

          wsg = new WaveServerGroup("test2 with space");

          wsg.addServer("serverho.gps.caltech.edu", 6500);
          wsg.addServer("serverho.gps.caltech.edu", 6501);
          wsg.addServer("serverje.gps.caltech.edu", 6500);
          wsg.addServer("serverje.gps.caltech.edu", 6501);

          wsgList.add(wsg);

          System.out.println(wsg.toString());

          String propString = WaveServerGroup.toGroupListPropertyString(wsgList);
          System.out.println(propString);

          // test parsing
          System.out.println("parse test...");
          ArrayList newList = (ArrayList) WaveServerGroup.parsePropertyString(propString);

          for (int i = 0; i<newList.size(); i++) {
            wsg = (WaveServerGroup)newList.get(i);

            System.out.println (wsg.toString());
          }

     }
*/
}
