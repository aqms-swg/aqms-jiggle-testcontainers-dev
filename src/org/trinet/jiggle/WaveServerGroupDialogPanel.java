package org.trinet.jiggle;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.StringTokenizer;
import javax.swing.border.*;
import javax.swing.border.TitledBorder;

import org.trinet.jasi.ActiveList;
import org.trinet.util.graphics.text.JTextClipboardPopupMenu;

/**
* Presents a panel that allows selection of WaveServer attributes.
*/
public class WaveServerGroupDialogPanel extends JPanel {

    private WaveServerGroup wsg = null;

    private ActiveList wsgList = null;

    private JTextArea textArea = null;
    private JTextField nameField = null;
    private JTextField retryField = null;
    private JTextField timeoutField = null;
    private JCheckBox verifyCheck = null;
    private JCheckBox truncateCheck = null;

    public WaveServerGroupDialogPanel(WaveServerGroup group) {
        set(group);
    }

    public WaveServerGroupDialogPanel() {
        this(new WaveServerGroup("<new>"));
    }

    /** Set the waveServer group to be shown in the panel. */
    public void set(WaveServerGroup group) {

        wsg = group;

        try  {
            if (textArea == null) jbInit();
            else jbSet();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

     /** Set the list of WaveServer groups that a new item would be added to
     from this panel */
    public void set(ActiveList list) {
        wsgList = list;
    }

    /** Build the panel */
    private JPanel createButtonPanel() {
        JPanel buttonPanel = new JPanel();
        JButton saveButton = new JButton("UPDATE");
        saveButton.setToolTipText("Update selected WaveServerGroup with the values set above.");
        saveButton.addActionListener(new ActionListener() {
               public void actionPerformed(ActionEvent e) {
                    saveButton_actionPerformed(e);
               }
          });
        JButton resetButton = new JButton("RESET");
        resetButton.setToolTipText("Reset selected WaveServerGroup to its initial values");
        resetButton.addActionListener(new ActionListener() {
               public void actionPerformed(ActionEvent e) {
                    resetButton_actionPerformed(e);
               }
        });

        JButton testButton = new JButton("TEST");
        testButton.setToolTipText("Test selected WaveServerGroup connections");
        testButton.addActionListener(new ActionListener() {
               public void actionPerformed(ActionEvent e) {
                    testButton_actionPerformed(e);
               }
        });
        buttonPanel.add(saveButton);
        buttonPanel.add(resetButton);
        buttonPanel.add(testButton);
        return buttonPanel;
    }

    private void jbSet() {
        nameField.setText(wsg.getName());
        retryField.setText(""+wsg.getMaxRetries());
        timeoutField.setText(""+wsg.getMaxTimeoutMilliSecs()/1000);
        verifyCheck.setSelected(wsg.isVerifyWaveforms());
        truncateCheck.setSelected(wsg.isTruncateAtTimeGap());
        String str = wsg.listToString();
        if (str == null || str.length() == 0) {
            str = "\nNo connection!";
            textArea.append(str);
        }
        else textArea.setText(str);
    }

    private JPanel createParamPanel() {
        JLabel groupName = new JLabel("Group Name");
        nameField = new JTextField(wsg.getName());

        JLabel retryLabel = new JLabel("Maximum Retries");
        retryLabel.setToolTipText("Set maximum times server will be retried");
        retryField = new JTextField(""+wsg.getMaxRetries());

        JLabel timeoutLabel = new JLabel("Maximum timeout (sec)");
        timeoutLabel.setToolTipText("Set maximum time until server times out");
        timeoutField = new JTextField(""+wsg.getMaxTimeoutMilliSecs()/1000);

        verifyCheck = new JCheckBox("Verify Waveforms");
        verifyCheck.setToolTipText("Check waveforms for errors");
        verifyCheck.setSelected(wsg.isVerifyWaveforms());

        truncateCheck = new JCheckBox("Truncate at gaps");
        truncateCheck.setToolTipText("Truncate waveform at data gap");
        truncateCheck.setSelected(wsg.isTruncateAtTimeGap());

        JPanel paramPanel = new JPanel();
        paramPanel.setLayout(new GridLayout(0, 2));
        paramPanel.add(groupName);
        paramPanel.add(nameField);
        paramPanel.add(retryLabel);
        paramPanel.add(retryField);
        paramPanel.add(timeoutLabel);
        paramPanel.add(timeoutField);
        paramPanel.add(verifyCheck);
        paramPanel.add(truncateCheck);
        return paramPanel;
    }

    private JPanel createListPanel() {
        String str = wsg.listToString();
        if (str == null || str.length() == 0) {
            str = "No connections";
        }
        textArea = new JTextArea(str);
        textArea.setToolTipText("To add, remove, or change 'address:port' entry in this group edit text, then press \"Update\"");
        new JTextClipboardPopupMenu(textArea);

        JLabel testLabel = new JLabel("Press [TEST] to check server(s)");
        JPanel testPanel = new JPanel(new BorderLayout());
        testPanel.add(testLabel, BorderLayout.CENTER);

        JPanel listPanel = new JPanel();
        listPanel.setBorder( new TitledBorder("Server host:port list") );
        listPanel.setLayout(new BorderLayout());
        listPanel.add(new JScrollPane(textArea), BorderLayout.CENTER);
        listPanel.add(testPanel, BorderLayout.SOUTH);
        listPanel.setPreferredSize(new Dimension(200, 125));
        return listPanel;
    }

    private void jbInit() throws Exception {
        setLayout(new BorderLayout());
        setMinimumSize(new Dimension(200, 400));
        add(createParamPanel(), BorderLayout.NORTH);
        add(createListPanel(), BorderLayout.CENTER);
        add(createButtonPanel(), BorderLayout.SOUTH);
    }

    /** Return a new WaveServerGroup as defined by the fields in this panel.  Returns null on error. */
    public WaveServerGroup getWaveServerGroup() {

        WaveServerGroup ws = new WaveServerGroup(nameField.getText());

        try {
          // parse from panel components
          int retries = Integer.parseInt(retryField.getText());
          ws.setMaxRetries(retries);

          int timeout = Integer.parseInt(timeoutField.getText());
          ws.setMaxTimeoutMilliSecs(timeout*1000);  // sec -> millis
            
          ws.setWaveformVerify(verifyCheck.isSelected());
          ws.setTruncatetAtTimeGap(truncateCheck.isSelected());

          // parse the list
          String delims = " :\t\n\r\f";   // consecutive pairs of delimited "address:port" parsed
          StringTokenizer strTok = new StringTokenizer(textArea.getText(), delims);

          // note the only syntax checking here is to catch the exception.
          // addServer() CHECKS THE CONNECTION AND WILL NOT ADD THE SERVER IF
          // IT DOES NOT RESPOND!
            while (strTok.hasMoreTokens()) {
                ws.addServer(strTok.nextToken(), Integer.parseInt(strTok.nextToken()) );
            }

        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            System.err.println("ERROR: WaveServerGroupDialog found bad syntax parsing input server:port pairs");
            ws = null;
        }
        return ws;
    }
    
    /** Save the current server group and add it to the list */
    private void saveButton_actionPerformed(ActionEvent e) {
         WaveServerGroup ws = getWaveServerGroup();
         if (ws != null) {
             set(ws);  // creates WSG object from data parsed from panel input fields
                       // and updates panel fields with the results of parse 

             // adds/replaces edited WaveServerGroup in list, reports to Observers
             WaveServerGroup.setSelected(ws, wsgList);
             if (wsgList != null) wsgList.addOrReplace(ws);
        }
        else JOptionPane.showMessageDialog(getTopLevelAncestor(), "Warning! unable to save WaveServerGroup check input");
    }

    /** Test the connection of the current server group. Does not add it to the list. */
    private void testButton_actionPerformed(ActionEvent e) {

       WaveServerGroup ws = getWaveServerGroup();
       if (ws != null) {
           set(ws);  // creates WSG object from data parsed from panel input fields
           System.out.println(ws.toString()); // dump edited info
       }
       int count = (ws == null) ? 0 : ws.listServers().length;
       if (count > 0) {
         JOptionPane.showMessageDialog(getTopLevelAncestor(), "WaveServer successful connection count : " + count);
       } else {
         JOptionPane.showMessageDialog(getTopLevelAncestor(), "Warning! unable to connect to WaveServerGroup check input");
       }

    }

    /** Reset current WaveServerGroup to its original settings */
    private void resetButton_actionPerformed(ActionEvent e) {
        set(wsg);
    }

}
