package org.trinet.jiggle;

import java.awt.*;
import java.awt.event.*;
import java.text.*;

import javax.swing.*;
import javax.swing.event.*;

import org.trinet.filters.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
import org.trinet.util.graphics.*;
import org.trinet.util.graphics.text.NumberTextField;
import org.trinet.util.graphics.text.NumberTextFieldFactory;

/**
 * This is a panel that supports zooming and scrolling interface functions. The
 * waveform is displayed in a ZoomableWFPanel in a BombSightViewport in a
 * JScrollPane.  There is also a button panel with scaling buttons and a label
 * panel with channel and status info.  This component does not support phase
 * picking. The PickingPanel class extends this class for that purpose. <p>
 *
 * If the view object has focus this component responds to the default keyboard actions
 * for JScrollPane as defined in the Java Look and Feel:<p>
 <tt>
JScrollPane (Java L&F) (Navigate refers to focus)
Navigate out forward      |  Tab
Navigate out backward     |  Shift+Tab
Move up/down              |  Up, Down
Move left/right           |  Left, Right
Move to start/end of data |  Ctrl+Home, Ctrl+End
Block move up/down        |  PgUp, PgDn
Block move right          |  Ctrl+PgDn
Block move left           |  Ctrl+PgUp
</tt>

/*
 The ZoomPanel contains several other nested graphical components:

 ZoomPanel

        labelBox  (JPanel)

        scrollZoom (JScrollPane)

                vport (BombSightViewport)

                        wfp  (ZoomableWFPanel)

        buttonPanel (JPanel)
<pre>
+----------------------------------------------------+
| ZoomPanel (JPanel)                                 |
|+--------------------------------------------------+|
|| ScrollZoom (JScrollPane)                         ||
||+------------------------------------------------+||
||| BombSightViewport (JViewport)                  |||
|||+----------------------------------------------+|||
|||| PickableWFPanel                              ||||
||||    (ZoomableWFPanel ActiveWFPanel WFPanel))  ||||
||||                                              ||||
|||+----------------------------------------------+|||
||+------------------------------------------------+||
|+--------------------------------------------------+|
+----------------------------------------------------+
</pre>
*/

public class ZoomPanel extends JPanel {

    TotalGroundMotionFilter tgmf = null;

    WFWindowListener wfWindowListener = null;
    WFViewListener wfViewListener = null;

    static boolean biasButtonOn = false;

    JLabel staLabel;
    JLabel timeLabel = null;

   int ampTwoExp = 0;

    public int width;
    public int height;
    public int UsableHeight;
    public int UsableWidth;
    public int BorderLeft;
    public int BorderRight;
    public int BorderTop;
    public int BorderBottom;

    //NOTE: dark color is easy to see but pick flag label with text "c" looks like "d" when overlayed with centerline - aww
    static Color bombSightColor = Color.gray;

    /** The ZoomableWFPanel that shows through the viewport. */
    public ZoomableWFPanel zwfp;

    TimeColumnHeader timeColumnHeader;
    ZoomScaleComboBox zoomScaleComboBox = new ZoomScaleComboBox();

    protected boolean filterEnabled = false;
    protected FilterButtonAction filterBtnAction = null;
    protected JPopupMenu filterMenu = null;
    private JMenuItem jmiOnOff = null;
    protected JToggleButton biasButton = null;
    protected JToggleButton zeroButton = null;

    // Could set via "property" as new constructor argument 
    // allow other default filter types 
    protected static String defaultFilterType = "HIGHPASS";
    protected static boolean defaultFilterAlways = false;

    private FilterIF zwfpFilter = FilterTypes.getFilter(FilterTypes.getType(defaultFilterType), 100, false); // note now NOT reversed- aww 2011/04/12 

    /** The master view of what's in the GUI */
    public MasterView mv = null;

    /** This extends JViewport and overrides its paint method to draw a bombsight for picking. */
    BombSightViewport bsvport = null; // new BombSightViewport(bombSightColor) ;

    /** JScrollPane that holds the zoomable WFPanel*/
    JScrollPane scrollZoom;

    /** Model of MVC for cursorLocLabel */
    CursorLocModel cursorLocModel = new CursorLocModel();
    CursorLocPanel cursorLocPanel = new CursorLocPanel(cursorLocModel);

    JToggleButton triaxialToggleButton = null;
    JToggleButton ehgButton = null;
    EHGFilter ehgFilter = null; 
    WFView oldWFV = null;

    /** Lable for channel and status info */
    JPanel labelBox = new JPanel();  //top panel with sta label and cursor info
    JButton uCenter = null;

    /** If true, allow ZoomPanel scroll bars to scroll the window */
    protected boolean scrollable = true;

    /** Format for distance in channel label: "####0.0"*/
    DecimalFormat di = new DecimalFormat("0");
    DecimalFormat df = new DecimalFormat("####0.0#");
    DecimalFormat df2 = new DecimalFormat("0.##");
    DecimalFormat dsci = new DecimalFormat("0.##E0");

    /** Handles async. loading of waveforms */
    ActiveWFLoadListener activeWFLoadListener = new ActiveWFLoadListener();

    /** Debug verbosity flag */
//    boolean debug = true;
    boolean debug = false;

//-----------------------------------------------------------------
/** No-op constructor. */
    public ZoomPanel() { }

    /*
     * Create a ZoomPanel based on information in this MasterView. This is only
     * needed to support the red up/down buttons. They need the WFViewList in
     * the master view to know the previous/next WFView.  */
    public ZoomPanel(MasterView masterView) {                 //constructor
        this();
        setMasterView(masterView);
    }

    protected void setMasterView(MasterView mv) {
        setMaximumSize(new Dimension(Integer.MAX_VALUE, Integer.MAX_VALUE));  // default JComponent max is Short.MAX_VALUE=32767 pixels -aww 2010/01/25
        this.mv = mv;
        buildPanel();
        addListeners(this.mv);
    }

    protected void clear() {
        if (zwfp != null) zwfp.clear();
    }

   // NOTE:
   // Listeners are called in the opposite order they were added.
   // Model Listener must be added to ZoomPanel BEFORE ZoomableWFPanel
   // via makeScrollZoom() because makeChannelLabel() here uses
   // the zwfp update results when the selected WFView changes.
   // Otherwise zwfp would need to be changed to update zoompanel
   // when WFView changes through another listener interface or
   // a callback reference to its parent container zoomPanel.
   //
   // WARNING: if listener is removed and readded to ZoomPanel 
   // or PickingPanel, it will be notified BEFORE ZoomableWFPanel,
   // SEE NOTE at top of makeChannelLabel() method -aww 
   //
    protected void addListeners(MasterView mv) {
        if (wfViewListener == null) wfViewListener = new WFViewListener();
        mv.masterWFViewModel.addChangeListener(wfViewListener);
        if (wfWindowListener == null) wfWindowListener = new WFWindowListener();
        mv.masterWFWindowModel.addChangeListener(wfWindowListener);
    }

    protected void removeListeners(MasterView mv) {
        mv.masterWFViewModel.removeChangeListener(wfViewListener);
        mv.masterWFWindowModel.removeChangeListener(wfWindowListener); 
        if (zwfp != null) zwfp.removeListeners(mv);
    }

    /** Builds the whole panel. */
    protected void buildPanel() {
        setOpaque(true);

        setLayout( new BorderLayout() );
        JPanel subpanel = new JPanel(new BorderLayout() );

        // create the main label 
        labelBox = makeInfoBar();
        subpanel.add("North", labelBox);

        // create the ZoomableWFPanel viewport scroller 
        subpanel.add("Center", makeScrollZoom());

        // arrows to move to next/previous time-series
        //subpanel.add("West", makeArrowPanel());
        subpanel.add("East", makeArrowPanel(BoxLayout.Y_AXIS));

        JToolBar jtb = new JToolBar();
        jtb.add(makeButtonBox(true));
        this.add("South", jtb);
        this.add("Center",subpanel);
    } // end of builder

    public boolean isFocusTraversable() { return true;}

    /** Construct the JScrollPane and the underlying ZoomableWFPanel that will show through the viewport. */
    // This is split into two steps to allow customizing by extending classes
    protected JScrollPane makeScrollZoom() {
        scrollZoom = makeScrollZoom1();
        zwfp = new ZoomableWFPanel(scrollZoom, mv);
        setupZoomableWFPanel(zwfp);
        return scrollZoom;
    }

    /** * First part of making the JScrollPane.  */
    protected JScrollPane makeScrollZoom1() {

        scrollZoom = new JScrollPane(
                         ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                         ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS
                     );

        scrollZoom.setMinimumSize(new Dimension(50,50));
        scrollZoom.setPreferredSize(new Dimension(250,150));

        // Replace the default viewport of the scrollPane with ours
        bsvport = new BombSightViewport(bombSightColor) ;
        //NOTE: if input viewport not new object, old object is removed and you get null exception on layout -aww
        scrollZoom.setViewport(bsvport);
        // For some reason the columnHeader viewport can't be same as bsvport here, else you get null ptr exception in ScrollPaneLayout
        scrollZoom.setColumnHeader(new BombSightViewport(bombSightColor));
        biasButton = createBiasButton();
        scrollZoom.add(biasButton, ScrollPaneConstants.UPPER_RIGHT_CORNER);
        //scrollZoom.add(createFilterButton(), ScrollPaneConstants.LOWER_RIGHT_CORNER);
        zeroButton = createZeroButton();
        scrollZoom.add(zeroButton, ScrollPaneConstants.LOWER_RIGHT_CORNER);

        // Had turned this off because of recursion problem with scroller
        if (scrollable) {        // control some of the scroll pane's behavior
            // set scrolling increments
            JScrollBar vBar = scrollZoom.getVerticalScrollBar();
            JScrollBar hBar = scrollZoom.getHorizontalScrollBar();
            // Add  adjustment listener to both
            vBar.addAdjustmentListener(new VerticalScrollListener());
            hBar.addAdjustmentListener(new HorizontalScrollListener());
        }
        //

        return scrollZoom;
    }

    protected void makeFilterMenu() {
        filterMenu = new JPopupMenu();
        ActionListener al = new FilterMenuItemHandler();
        jmiOnOff = new JMenuItem("On");
        jmiOnOff.addActionListener(al);
        filterMenu.add(jmiOnOff);
        
        JMenuItem m = new JMenuItem("Customize...");
        m.addActionListener(al);
        filterMenu.add(m);


        JMenu filterMenu2 = new JMenu("Choose...");
        filterMenu.add(filterMenu2);

        /*
        for (int ii =0; ii < FilterTypes.TYPES.length; ii++) {
            m = new JMenuItem(FilterTypes.TYPES[ii]);
            m.addActionListener(al);
            filterMenu2.add(m);
        }
        */

        m = new JMenuItem("HIGHPASS");
        m.addActionListener(al);
        filterMenu2.add(m);

        m = new JMenuItem("BANDPASS");
        m.addActionListener(al);
        filterMenu2.add(m);

        m = new JMenuItem("NARROW_BANDPASS");
        m.addActionListener(al);
        filterMenu2.add(m);

        m = new JMenuItem("LOWPASS");
        m.addActionListener(al);
        filterMenu2.add(m);

        m = new JMenuItem("VERY_LOWPASS");
        m.addActionListener(al);
        filterMenu2.add(m);

        m = new JMenuItem("NOTCH");
        m.addActionListener(al);
        filterMenu2.add(m);

        m = new JMenuItem("WOOD-ANDERSON");
        m.addActionListener(al);
        filterMenu2.add(m);

        m = new JMenuItem("WOOD-ANDERSON_BP");
        m.addActionListener(al);
        filterMenu2.add(m);

        m = new JMenuItem("WOOD-ANDERSON_HP");
        m.addActionListener(al);
        filterMenu2.add(m);

        m = new JMenuItem("WOOD-ANDERSON_EH");
        m.addActionListener(al);
        filterMenu2.add(m);

        m = new JMenuItem("WA_EHK");
        m.addActionListener(al);
        filterMenu2.add(m);

        m = new JMenuItem("SP0.3");
        m.addActionListener(al);
        filterMenu2.add(m);

        m = new JMenuItem("SP1.0");
        m.addActionListener(al);
        filterMenu2.add(m);

        m = new JMenuItem("SP3.0");
        m.addActionListener(al);
        filterMenu2.add(m);

        m = new JMenuItem("ACCELERATION");
        m.addActionListener(al);
        filterMenu2.add(m);

        m = new JMenuItem("VELOCITY");
        m.addActionListener(al);
        filterMenu2.add(m);

        m = new JMenuItem("DISPLACEMENT");
        m.addActionListener(al);
        filterMenu2.add(m);

        /*
        m = new JMenuItem("TGM-SP0.3");
        m.addActionListener(al);
        filterMenu2.add(m);

        m = new JMenuItem("TGM-SP1.0");
        m.addActionListener(al);
        filterMenu2.add(m);

        m = new JMenuItem("TGM-SP3.0");
        m.addActionListener(al);
        filterMenu2.add(m);
        */
    }

    public void setSightColor(Color c) {
        if (bsvport !=  null) {
            bsvport.setBombSightColor(c);
            BombSightViewport bsv = (BombSightViewport) scrollZoom.getColumnHeader();
            if (bsv != null) bsv.setBombSightColor(c);       
        }
        if (zwfp != null) {
            zwfp.biasLineColor = c;
        }
        if (scrollZoom != null) scrollZoom.repaint();
    }

     // Setup ZoomableWFPanel AFTER new ZoomableWFPanel(makeScrollZoom1(), mv) does 'scrollZoom.setViewport(vport)'
     protected void setupZoomableWFPanel(ZoomableWFPanel zwfp) {

         zwfp.setCursorLocModel(cursorLocModel);
         
         biasButton.setSelected(biasButtonOn);
         zwfp.paintBiasLine = biasButtonOn;

         // set the default filter HIGH pass and does reverse filtering to avoid phase shift
         zwfp.setFilter(zwfpFilter);

         // see override paintChildren in BombSightViewport
         zwfp.setOpaque(false); // necessary to get BombSight visible -aww
         zwfp.biasLineColor = bombSightColor;

         //if input viewport not new object, old object is removed and you get null exception -aww
         //scrollZoom.setColumnHeader(new BombSightViewport(bombSightColor)); // moved to makeScrollZoom1() -aww 2008/09/17

         // set scrolling time scale in column header
         timeColumnHeader = new TimeColumnHeader(zwfp) ;
         scrollZoom.setColumnHeaderView(timeColumnHeader);

         /* Had turned this off because of recursion problem with scroller
         if (scrollable) {        // control some of the scroll pane's behavior
             // set scrolling increments
             JScrollBar vBar = scrollZoom.getVerticalScrollBar();
             JScrollBar hBar = scrollZoom.getHorizontalScrollBar();

             // Add  adjustment listener to both
             vBar.addAdjustmentListener(new VerticalScrollListener());
             hBar.addAdjustmentListener(new HorizontalScrollListener());
         }
         */
         //return scrollZoom;
    }

    protected void createCenterTimeLabel(Container c) {
        // use UTC now - aww 2008/02/10
        this.timeLabel =
            new JLabel(LeapSeconds.trueToString(mv.masterWFWindowModel.getCenterTime(), "yyyyMMdd HH:mm:ss.fff").substring(0,21));
        mv.masterWFWindowModel.addChangeListener(new CenterTimeChangeListener(this.timeLabel));
        c.add(this.timeLabel);
    }

    /**
     * Make the panel with the up/down buttons for selecting next/prevous
     * WFPanel.
     */
    protected Box makeArrowPanel(int orient) {

        JButton btn = null;

        // arrow
        //Image image = IconImage.getImage("RedUp3.gif");
        Image image = IconImage.getImage("arrow-firstpage-grn.gif");
        if (image == null) {       // handle situation when .gif file can't be found
            btn = new JButton("^");
        } else {
            btn = new JButton(new ImageIcon(image));
        }
        btn.addActionListener(new ButtonAction(ButtonAction.TOP) );
        btn.setToolTipText("Select top waveform");
        btn.setMargin(new Insets(0, 0, 0, 0));
        btn.setMaximumSize(new Dimension(20,20));
        btn.setPreferredSize(new Dimension(20,20));
        //btn.setBackground(Color.white);
        btn.setMnemonic(KeyEvent.VK_HOME);
        JButton uFirst = btn;

        // arrow
        //image = IconImage.getImage("RedUp2.gif");
        image = IconImage.getImage("arrow-pageup-grn.gif");
        if (image == null) {
            btn = new JButton("^");
        } else {
            btn = new JButton(new ImageIcon(image));
        }
        btn.addActionListener(new ButtonAction(ButtonAction.PAGEUP) );
        btn.setToolTipText("Jump up a page");
        btn.setMargin(new Insets(0, 0, 0, 0));
        btn.setMaximumSize(new Dimension(20,20));
        btn.setPreferredSize(new Dimension(20,20));
        //btn.setBackground(Color.white);
        btn.setMnemonic(KeyEvent.VK_PAGE_UP);
        JButton uPage = btn;

        // arrow
        //image = IconImage.getImage("RedUp.gif");
        image = IconImage.getImage("arrow-one-up-grn.gif");
        if (image == null) {
            btn = new JButton("^");
        } else {
            btn = new JButton(new ImageIcon(image));
        }
        btn.addActionListener(new ButtonAction(ButtonAction.UP) );
        btn.setToolTipText("Select previous waveform");
        btn.setMargin(new Insets(0, 0, 0, 0));
        btn.setMaximumSize(new Dimension(20,20));
        btn.setPreferredSize(new Dimension(20,20));
        //btn.setBackground(Color.white);
        btn.setMnemonic(KeyEvent.VK_UP);
        JButton uOne = btn;

        // dot to scroll center
        //image = IconImage.getImage("dot_red.gif");
        image = IconImage.getImage("dot_grn.png");
        if (image == null) {
            btn = new JButton("X");
        } else {
            btn = new JButton(new ImageIcon(image));
        }
        btn.addActionListener(makeCenterButtonActionListener());
        btn.setToolTipText("Scroll to selected waveform group scroller (centered if possible)");
        btn.setMargin(new Insets(0, 0, 0, 0));
        btn.setMaximumSize(new Dimension(20,20));
        btn.setPreferredSize(new Dimension(20,20));
        //btn.setBackground(Color.white);
        btn.setMnemonic(KeyEvent.VK_INSERT);
        uCenter = btn;

        // arrow
        //image = IconImage.getImage("RedDown.gif");
        image = IconImage.getImage("arrow-one-down-grn.gif");
        if (image == null) {
            btn = new JButton("v");
        } else {
            btn = new JButton(new ImageIcon(image));
        }
        btn.addActionListener(new ButtonAction(ButtonAction.DOWN) );
        btn.setToolTipText("Select next waveform");
        btn.setMargin(new Insets(0, 0, 0, 0));
        btn.setMaximumSize(new Dimension(20,20));
        btn.setPreferredSize(new Dimension(20,20));
        //btn.setBackground(Color.white);
        btn.setMnemonic(KeyEvent.VK_DOWN);
        JButton dOne = btn;

        // arrow
        //image = IconImage.getImage("RedDown2.gif");
        image = IconImage.getImage("arrow-pagedown-grn.gif");
        if (image == null) {
            btn = new JButton("v");
        } else {
            btn = new JButton(new ImageIcon(image));
        }
        btn.addActionListener(new ButtonAction(ButtonAction.PAGEDOWN) );
        btn.setToolTipText("Jump down a page");
        btn.setMargin(new Insets(0, 0, 0, 0));
        btn.setMaximumSize(new Dimension(20,20));
        btn.setPreferredSize(new Dimension(20,20));
        //btn.setBackground(Color.white);
        btn.setMnemonic(KeyEvent.VK_PAGE_DOWN);
        JButton dPage = btn;

        // arrow
        //image = IconImage.getImage("RedDown3.gif");
        image = IconImage.getImage("arrow-lastpage-grn.gif");
        if (image == null) {
            btn = new JButton("v");
        } else {
            btn = new JButton(new ImageIcon(image));
        }
        btn.addActionListener(new ButtonAction(ButtonAction.BOTTOM) );
        btn.setToolTipText("Select bottom waveform");
        btn.setMargin(new Insets(0, 0, 0, 0));
        btn.setMaximumSize(new Dimension(20,20));
        btn.setPreferredSize(new Dimension(20,20));
        //btn.setBackground(Color.white);
        btn.setMnemonic(KeyEvent.VK_END);
        JButton dLast = btn;

        Box box = null;
        
        if (orient == BoxLayout.X_AXIS) {
            Box boxU = new Box(BoxLayout.X_AXIS);
            boxU.add(uOne);
            boxU.add(uPage);
            boxU.add(uFirst);
            Box boxD = new Box(BoxLayout.X_AXIS);
            boxD.add(dOne);
            boxD.add(dPage);
            boxD.add(dLast);
            Box arrowBox = new Box(BoxLayout.Y_AXIS);
            //box.add(Box.createGlue());
            arrowBox.add(boxU); // up on top -aww 01/02/2007
            arrowBox.add(boxD); // down below
            //box.add(Box.createGlue());
            box = new Box(BoxLayout.X_AXIS);
            box.add(arrowBox);
            box.add(uCenter);
            //box.add(Box.createGlue());
        }
        else {
            box = new Box(BoxLayout.Y_AXIS);
            box.add(Box.createGlue());
            box.add(uFirst);
            box.add(uPage);
            box.add(uOne);
            box.add(uCenter);
            box.add(dOne);
            box.add(dPage);
            box.add(dLast);
            box.add(Box.createGlue());
        }
        return box;
    }

    ActionListener makeCenterButtonActionListener() {
        return new ButtonAction(ButtonAction.CENTER);
    }

    /**
    *  Define motion buttons. Can't do in ButtonAction because you only do
    * statics in top level classes.
    */
    interface MotionButtons {
        static final String TOP        = "TOP";
        static final String UP         = "UP";
        static final String PAGEUP     = "PAGEUP";
        static final String CENTER     = "CENTER";
        static final String PAGEDOWN   = "PAGEDOWN";
        static final String DOWN       = "DOWN";
        static final String BOTTOM     = "BOTTOM";
    }

    /**
     * Handle next/prevous buttons actions. This allows control of the selected WFView.
     */
    class ButtonAction extends AbstractAction implements MotionButtons {
        private static final int top        = 0;
        private static final int pageup     = 1;
        private static final int up         = 2;
        private static final int center     = 3;
        private static final int down       = 4;
        private static final int pagedown   = 5;
        private static final int bottom     = 6;

        int action = -1;  // defines what action this listener should take.

        /**
         * Create the button listener and define what action this listener should take
         * based on the value of 'type'. Thus, on class can service all actions.
         */
        public ButtonAction(String type) {
            super(type);
            if (type.equals("DOWN"))          action = down;
            else if (type.equals("UP"))       action = up;
            else if (type.equals("PAGEDOWN")) action = pagedown;
            else if (type.equals("PAGEUP"))   action = pageup;
            else if (type.equals("BOTTOM"))   action = bottom;
            else if (type.equals("TOP"))      action = top;
            else if (type.equals("CENTER"))   action = center;
        }

        public void actionPerformed(ActionEvent evt) {
            //
            //reset WFScroller triaxial trace hiding mode when group PAGEing action is selected
            if (action == top || action == pagedown || action == pageup || action == bottom) {
              Object obj = mv.getOwner();
              if (obj instanceof WaveformDisplayIF) {
                final WaveformDisplayIF jiggle = (WaveformDisplayIF) obj;
                final WFScroller wfs = jiggle.getWFScroller();
                if (wfs.jbTriaxial.isSelected()) {
                  TriaxialGrouper tg = new TriaxialGrouper(new ChannelableList(mv.wfvList), StationGrouper.DIST_SORT);
                  ChannelableListIF clist = null;
                  WFView wfv =  mv.masterWFViewModel.get();
                  if (action == pageup) {
                      int total = tg.countGroups(); 
                      int cnt = 0;
                      ChannelableListIF lastGrpList = null;
                      while (cnt < total) {
                        clist = tg.getNext();
                        cnt++;
                        if (clist.contains(wfv)) break;
                        lastGrpList = clist;
                      }
                      if (cnt != 1) clist = lastGrpList;
                  }
                  if (action == pagedown) {
                      clist = tg.getGroupContaining(wfv);
                      clist = tg.getNext();
                  }
                  else if (action == top) {
                      clist = tg.getNext();
                  }
                  else if (action == bottom) {
                      //clist = tg.getGroupContaining((Channelable)mv.wfvList.get(mv.wfvList.size()-1));
                      clist = new ChannelableList();
                      clist.add((Channelable)mv.wfvList.get(mv.wfvList.size()-1));
                  }
                  if (clist == null || clist.size() == 0) return;

                  //System.out.println(clist);
                  wfv = (WFView) clist.get(0);

                  mv.masterWFViewModel.set(wfv);
                  mv.masterWFWindowModel.setFullAmp(zwfp.getWf());

                  wfs.doActionCmd("Show Triaxial", true);
                  return;
                }
              }
            }
            //

            switch (action) {

              case top:
                mv.masterWFViewModel.selectFirst(mv.wfvList);
                mv.masterWFWindowModel.setFullAmp(zwfp.getWf());
                break;

              case pageup:
                mv.masterWFViewModel.pageUp(mv.wfvList, false);
                mv.masterWFWindowModel.setFullAmp(zwfp.getWf());
                break;

              case up:
                mv.masterWFViewModel.selectPrevious(mv.wfvList);
                mv.masterWFWindowModel.setFullAmp(zwfp.getWf());
                break;

              case center:
                mv.masterWFViewModel.set(zwfp.wfv, true);
                break;

              case down:
                mv.masterWFViewModel.selectNext(mv.wfvList);
                mv.masterWFWindowModel.setFullAmp(zwfp.getWf());
                break;

              case pagedown:
                mv.masterWFViewModel.pageDown(mv.wfvList, false);
                mv.masterWFWindowModel.setFullAmp(zwfp.getWf());
                break;

              case bottom:
                mv.masterWFViewModel.selectLast(mv.wfvList);
                mv.masterWFWindowModel.setFullAmp(zwfp.getWf());
                break;

            }
        }

    }
    /**
     * Build the "north" bar of the panel display.
     */
    protected JPanel makeInfoBar() {

        JPanel jp = new JPanel();

        //
        GridBagLayout layout = new GridBagLayout();
        jp.setLayout(layout);
        GridBagConstraints GB  = new GridBagConstraints();
        // define a generic layout object that we'll reuse for diff. comps
        GB.weighty = 100;
        GB.fill    = GridBagConstraints.BOTH;
        GB.anchor  = GridBagConstraints.NORTHWEST;
        GB.gridx   = GridBagConstraints.RELATIVE;
        GB.gridy   = 0;
        //

        // The staLabel has file scope because it is changed by another method
        staLabel = new JLabel("Station Info here...");
        staLabel.setBackground(Color.black);
        staLabel.setForeground(Color.blue);

        //
        GB.weightx = 70;                // 60% of bar
        layout.setConstraints(staLabel, GB);
        jp.add(staLabel);

        // cursor position labels
        GB.weightx = 30;                        // 30% of bar
        layout.setConstraints(cursorLocPanel, GB);
        jp.add(cursorLocPanel);
        //
        //Box hbox = Box.createHorizontalBox();
        //hbox.add(staLabel);
        //hbox.add(Box.createHorizontalGlue());
        //hbox.add(cursorLocPanel);
        //jp.add(hbox);

        return jp;
}

//-----------------------------------------------------------------
// Make the button panel
    protected Box makeButtonBox(boolean labelCenterTime) {
        Box box = Box.createHorizontalBox();
        box.add(Box.createHorizontalGlue());
        createTimeAmpScalingButtons(box, labelCenterTime);
        box.add(Box.createHorizontalStrut(5));
        createZoomScaleComboBox(box);
        box.add(Box.createHorizontalGlue());
        return box;
    }

   private void createTimeAmpScalingButtons(Container c, boolean labelCenterTime) {
       if (labelCenterTime) c.add(Box.createHorizontalStrut(120));
       createTimeScalingButtons(c);
       c.add(Box.createHorizontalStrut(5));
       if (labelCenterTime) {
           createCenterTimeLabel(c);
           c.add(Box.createHorizontalStrut(5));
       }
       createAmpScalingButtons(c);
       c.add(Box.createHorizontalStrut(5));
       c.add(createFilterButton());
       c.add(createAlignmentButton());
       c.add(createTriaxialButton()); // -aww 2010/01/24
       if (Jiggle.jiggle != null && Jiggle.jiggle.getWfPanelProperties().getBoolean("addEHGFilterButton"))
           c.add(createButtonEHG()); 
       c.add(Box.createHorizontalStrut(5));
       if (Jiggle.jiggle != null && Jiggle.jiggle.getProperties().getProperty("badChannelListName") != null)
           c.add(createButtonBCL()); 
   }

   protected JButton createButtonBCL() {
       ActionListener al = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                zwfp.wfv.mv.wfvList.get(zwfp.wfv);
                String str = zwfp.wfv.getComment();
                if (str == null) str = "";
                if (str.length() > 64 ) str = str.substring(0,65);
                CommentDialog cd = new CommentDialog(str);
                zwfp.wfv.setComment(cd.getString());
                cd.dispose();
            }
       };

        String actionCmd = "BCL";
        JButton bclButton = new JButton(actionCmd);
        bclButton.addActionListener(al);
        bclButton.setToolTipText("Open dialog to set or change a 64-char limit comment for selected waveform");
        bclButton.setMargin(new Insets(0,0,0,0));
        Dimension btnSize = new Dimension(32,24);
        bclButton.setMaximumSize(btnSize);
        bclButton.setPreferredSize(btnSize);
        return bclButton;
   }

// EHG Custom button created for use by New Madrid network at Memphis (Mitch Withers request - 2013/01)
   protected JToggleButton createButtonEHG() {
        ActionListener al = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {

                Object obj = mv.getOwner();
                if ( ! (obj instanceof Jiggle)) return;
                Jiggle jg = (Jiggle) obj;

                Waveform oldWf = zwfp.getWf();
                boolean wasSelected=true;
                if (ehgButton != null && !ehgButton.isSelected()) {
                    // switch back to original from descaled
                    setShowAlternateWf(false);
                    wasSelected=false;
                }
                else {

                    // Check for applicable seedchan 
                    Channel oldCh = (Channel) oldWf.getChannelObj();
                    if ( !(oldCh.getNet().equals("NM") || oldCh.getNet().equals("ET")) ||  // Network doesn't have known G-chan type
                         !oldCh.getSeedchan().substring(0,2).equals("EH") || // it's the wrong seedchan group
                         oldCh.getSeedchan().substring(2,3).equals("G") // A gain channel doesn't correct itself
                       ) {
                        System.out.println("EHG INFO: filtering is a no-op for channel:" + oldCh.toDelimitedSeedNameString());
                        ehgButton.setSelected(false);
                        return;
                    }


                    Channel ehgChan = (Channel) oldCh.clone();
                    //Note set seedchan gain channel and search loaded group wfList
                    ehgChan.setSeedchan("EHG"); // for testing, you could comment out this line to pass raw zwfp wf

                    System.err.println("EHG INFO: searching wf list for EHG gain channel:" + ehgChan.toDelimitedSeedNameString());
                    WFPanel wfp = jg.getWFScroller().groupPanel.wfpList.get(ehgChan);
                    if ( wfp == null) {
                        System.err.println("EHG NOOP: the gain channel is missing from wf list:" + ehgChan.toDelimitedSeedNameString());
                        ehgButton.setSelected(false);
                        return;
                    }
                    else {
                        System.out.println("EHG INFO: matching EHG gain channel found\n"); 
                    }

                    // Ok have EH[ZNE] and EHG so turn off any existing filter first, first?
                    if ( zwfp.getShowAlternateWf() || zwfp.getWf().isFiltered() ) {
                        System.out.println("EHG INFO: resetting panel display to show raw waveform");
                        setShowAlternateWf(false);
                    }

                    // Setup filter to degain
                    if (ehgFilter == null) ehgFilter = new EHGFilter();
                    ehgFilter.setEhgWf( wfp.getRawWf() );
                    zwfp.setFilter(ehgFilter);  // this does the filtering
                    setShowAlternateWf(true);
                }

                // Rescale and plot filtered wf
                zwfp.setupWf();  // this invoke wf filter again when showAlternateWf=true
                zwfp.revalidate(); // is this needed?
                cursorLocPanel.cursorLocModel.setWFPanel(zwfp);
                if (zwfp.getWf() != null) cursorLocPanel.setAmpFormat(zwfp.getWf().getAmpUnits(), zwfp.getWf().getMaxAmp());
                makeChannelLabel();
                zwfp.repaint();
                repaint();
                // Fire property change event here to notify SwarmFrame listener for filtered waveform change in jiggle data source
                if ( (oldWf != zwfp.getWf()) || !oldWf.getFilterName().equals(zwfp.getWf().getFilterName()) ) {
                        //System.out.println("DEBUG : ZoomPanel notifying WFPanel.FILTER_CHANGED");
                        // put notification here for Swarm Frame
                        jg.propertyChange(new java.beans.PropertyChangeEvent(this, WFPanel.FILTER_CHANGED,
                                        oldWf.getFilterName(), zwfp.getWf().getFilterName()) );
                }
                if (! zwfp.getWf().isFiltered()) {
                    if (wasSelected) System.out.println("EHG INFO: filtering failed, resetting EHG button unselected");
                    else System.out.println("EHG INFO: EHG button unselected");
                    // Failed so reset everything back to raw state
                    ehgButton.setSelected(false);
                    setShowAlternateWf(false);
                }
                
            }
        };

        String actionCmd = "EHG";
        ehgButton = new JToggleButton(actionCmd);
        ehgButton.addActionListener(al);
        ehgButton.setToolTipText("Degain displayed EH[ZNE] channel using its associated EHG channel");
        ehgButton.setMargin(new Insets(0,0,0,0));
        Dimension btnSize = new Dimension(32,24);
        ehgButton.setMaximumSize(btnSize);
        ehgButton.setPreferredSize(btnSize);
        return ehgButton;

   }
//
   private Waveform getTriaxialWf(WFPanel wfp) {
        Waveform triaxialWf = AbstractWaveform.copyWf(wfp.getRawWf());
        //System.out.println("getTriaxialWf triaxialWf null ? " + (triaxialWf == null));
        if (triaxialWf == null) return null;
        //System.out.println("ZWFP filter != null: " + (zwfp.filter != null) + " getShowAlternateWf(): " + zwfp.getShowAlternateWf());
        if (zwfp.filter != null && zwfp.getShowAlternateWf()) { // filter timeseries of copied waveform
            int rate = (int) triaxialWf.getSampleRate();
            //System.out.println("ZWFP triaxialWf rate: " + rate);
            if (rate > 0) {
                FilterIF f = zwfp.filter.getFilter(rate);
                triaxialWf = triaxialWf.filter(f);
                //System.out.println("WFPanel getTriaxialWf triaxialWf isFiltered: " + triaxialWf.isFiltered() +" rate: " + rate + "\n" + f.toString());
                cursorLocPanel.setAmpFormat(triaxialWf.getAmpUnits(), triaxialWf.getMaxAmp());
            }
        }
        return triaxialWf;
   }

   protected JToggleButton createTriaxialButton() {
        triaxialToggleButton = null;
        String actionCmd = "O";
        ActionListener al = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                zwfp.clearTriaxials();
                if (! triaxialToggleButton.isSelected()) {
                    zwfp.setShowTriaxials(false);
                    zwfp.resetPlotTriaxialMap();
                    SwingUtilities.invokeLater( new Runnable() {
                        public void run() {
                            staLabel.setForeground(Color.blue);
                            makeChannelLabel();
                        }
                    });
                    return;
                }
                
                Object obj = mv.getOwner();
                if (obj instanceof WaveformDisplayIF) {

                  WaveformDisplayIF jiggle = (WaveformDisplayIF) obj;
                  ChannelableList clist = Triaxial.getSet(zwfp.wfv.getChannelObj());
                  //System.out.println("ZoomPanel getTriaxialWf for clist size:" + clist.size());
                  //int ii = clist.getIndexOf(zwfp.wfv.getChannelObj());
                  //if (ii >= 0) clist.remove(ii);
                  WFPanel wfp = null;
                  for (int idx=0; idx<clist.size(); idx++) {
                    Channel ch = (Channel) clist.get(idx);
                    wfp = jiggle.getWFScroller().groupPanel.wfpList.get(ch);
                    if (wfp != null) {
                        //System.out.println("ZoomPanel getTriaxialWf for" + ch);
                        Waveform wf = getTriaxialWf(wfp);
                        if (wf != null) zwfp.addTriaxialWf(wf);
                    }
                  }

                  if (zwfp.triaxialList.size() > 1) {
                    zwfp.setShowTriaxials(true);
                    if (jiggle.getWfPanelProperties().isSpecified("triaxialZonH")) {
                        if (zwfp.wfv.getChannelObj().isHorizontal()) {
                            zwfp.setPlotTriaxial(wfp.Zcolor, jiggle.getWfPanelProperties().getBoolean("triaxialZonH"));
                        }
                        else zwfp.setPlotTriaxial(wfp.Zcolor, true);
                    }
                    SwingUtilities.invokeLater( new Runnable() {
                        public void run() {
                            staLabel.setForeground(zwfp.triaxialColor);
                            makeChannelLabel();
                        }
                    });
                    repaint();
                  }
                //else triaxialToggleButton.setSelected(false); // is this the culprit for toggling off ? -aww 2010/09/22

                }
            }
        };
        Image image = IconImage.getImage("mini-triaxialgray.png"); // find the icon in the path
        triaxialToggleButton = (image == null) ?  new JToggleButton(actionCmd) : new JToggleButton(new ImageIcon(image));
        triaxialToggleButton.setActionCommand(actionCmd);
        triaxialToggleButton.addActionListener(al);
        triaxialToggleButton.setToolTipText("Overlay triaxial components of currently selected channel (red=Z,green=N,blue=E)");
        triaxialToggleButton.setMargin(new Insets(0,0,0,0));
        Dimension btnSize = new Dimension(32,24);
        triaxialToggleButton.setMaximumSize(btnSize);
        triaxialToggleButton.setPreferredSize(btnSize);
        return triaxialToggleButton;
   }
   //

   protected JToggleButton createAlignmentButton() {
        JToggleButton jtb = null;
        String actionCmd = "|";
        ActionListener al = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                String cmdName =  evt.getActionCommand();
                if (cmdName == "|") {
                    //SelectableReadingList.alignOnlyOnFocus = !SelectableReadingList.alignOnlyOnFocus;
                    if ( ((JToggleButton)evt.getSource()).isSelected() ) {
                          SelectableReadingList.alignOnlyOnFocus = false;
                    }
                    else  {
                          SelectableReadingList.alignOnlyOnFocus = true;
                    }
                }
            }
        };
        Image image = IconImage.getImage("vert-line-grn.png"); // find the icon in the path
        jtb = (image == null) ?  new JToggleButton(actionCmd) : new JToggleButton(new ImageIcon(image));
        jtb.setActionCommand(actionCmd);
        jtb.addActionListener(al);
        jtb.setToolTipText("When toggled, views are centered on data times when scrolled by up/down arrow button/key");
        jtb.setMargin(new Insets(0,0,0,0));
        Dimension btnSize = new Dimension(32,24);
        jtb.setMaximumSize(btnSize);
        jtb.setPreferredSize(btnSize);
        return jtb;
   }

   protected class TimeScalingAction extends AbstractAction {

            String cmdName = null;

            public TimeScalingAction() {
            }

            public TimeScalingAction(String cmd) {
                cmdName = cmd;
            }

            public void actionPerformed(ActionEvent evt) {
                String cmd =  (cmdName == null) ? evt.getActionCommand() : cmdName;
                // Kludge here: -aww 2010/02/02 - Pechmann Bug
                // disable horiz scroll group/zoom synch else setScale here doesn't work!
                WFScroller.triaxialScrollZoomWithGroup = false;
                // Object obj = mv.getOwner();
                // if (obj instanceof WaveformDisplayIF) {
                  // WaveformDisplayIF jiggle = (WaveformDisplayIF) obj;
                  //jiggle.getWfPanelProperties().setProperty("triaxialScrollZoomWithGroup", false);
                // }
                //

                if (cmd == "<>") {
                    zwfp.setScale(2.0, 1.0);
                }
                else if (cmd == "><") {
                    zwfp.setScale(0.5, 1.0);
                }
                else if (cmd == "==") {
                    zwfp.wfv.mv.masterWFWindowModel.setFullTime(zwfp.getWf());
                }
                else if (cmd == "[]") {
                    // notify selection model that the panelbox changed
                    mv.masterWFWindowModel.set(zwfp.getWf(), zwfp.dataBox);
                }
                // repaint the panel to update otherwise, you get PARTIAL repaint when you zoom.
                // System.out.println("repaint from ZoomPanel.actionPerformed...");
                repaint();
                Runnable runner = new Runnable() {
                  public void run() {
                    Runnable task = new Runnable() {
                      public void run() {
                         Object obj = mv.getOwner();
                         if (obj instanceof WaveformDisplayIF) {
                           WaveformDisplayIF jiggle = (WaveformDisplayIF) obj;
                           WFScroller.triaxialScrollZoomWithGroup = jiggle.getWfPanelProperties().getBoolean("triaxialScrollZoomWithGroup");
                         }
                      }
                    };
                    try {
                      Thread.sleep(200); // Sleep millisec
                      EventQueue.invokeLater(task);
                    } catch (InterruptedException ie) {
                      // ignore
                      return;
                    }
                  }
                };
                // Start delayed task
                new Thread(runner).start();
            }
   }

   protected void createTimeScalingButtons(Container c) {
        ActionListener al = new TimeScalingAction();
        //addButton(c, "[]", "expandFull_black.gif", al, "Full view");
        addButton(c, "[]", "mini-fullview_grn.gif", al, "Full view");
        addButton(c, "==", "expand_x_full_grn.gif", al, "Full timespan");
        addButton(c, "><", "collapse-x-wide.gif", al, "time scale / 2");
        addButton(c, "<>", "expand-x-wide.gif", al, "time scale * 2");
   }


   protected class AmpScalingAction extends AbstractAction {

            String cmdName = null;

            public AmpScalingAction() {
            }

            public AmpScalingAction(String cmd) {
                cmdName = cmd;
            }

            public void actionPerformed(ActionEvent evt) {
                String cmd =  (cmdName == null) ? evt.getActionCommand() : cmdName;
                if (cmd == "+") {
                    zwfp.setScale(1.0, 2.0);
                    ampTwoExp++;
                }
                else if (cmd == "-") {
                    zwfp.setScale(1.0, 0.5);
                    if (ampTwoExp > 0) ampTwoExp--;
                }
                else if (cmd == "o") {
                    zwfp.wfv.mv.masterWFWindowModel.setFullAmp();
                    ampTwoExp = 0;
                }
                else if (cmd == "*") {
                    ampTwoExp = -ampTwoExp; // reverse the scaling
                    zwfp.setScale(1.0, Math.pow(2.,ampTwoExp));
                }
                // repaint the panel to update otherwise, you get PARTIAL repaint when you zoom.
                // System.out.println("repaint from ZoomPanel.actionPerformed...");
                repaint();
            } // end of actionPerformed
   }
   protected void createAmpScalingButtons(Container c) {
        ActionListener al = new AmpScalingAction();
        addButton(c, "+", "expand-y-wide.gif", al, "amp scale * 2");
        addButton(c, "-", "collapse-y-wide.gif", al, "amp scale / 2");
        addButton(c, "o", "expand_y_full_grn.gif", al, "Full ampspan");
        addButton(c, "*", "dot_grn.png", al, "Apply/Unapply last amp scale magnification");
   }

   protected JToggleButton createBiasButton() {
        JToggleButton btn = new JToggleButton("B");
        btn.setToolTipText("Toggle showing of waveform bias line.");
        btn.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent evt) {
               zwfp.paintBiasLine = biasButton.isSelected();
               zwfp.repaint();
           }
        });
        btn.setMargin(new Insets(0,0,0,0));
        Dimension btnSize = new Dimension(24,24);
        btn.setMaximumSize(btnSize);
        btn.setPreferredSize(btnSize);
        btn.setHorizontalTextPosition(SwingConstants.LEADING);
        return btn;
   }

   protected JToggleButton createZeroButton() {
        JToggleButton btn = new JToggleButton("Z");
        btn.setToolTipText("Toggle showing of waveform zero counts line.");
        btn.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent evt) {
               zwfp.paintZeroLine = zeroButton.isSelected();
               zwfp.repaint();
           }
        });
        btn.setMargin(new Insets(0,0,0,0));
        Dimension btnSize = new Dimension(24,24);
        btn.setMaximumSize(btnSize);
        btn.setPreferredSize(btnSize);
        btn.setHorizontalTextPosition(SwingConstants.LEADING);
        return btn;
   }

   protected JButton createFilterButton() {
        if (filterBtnAction == null ) filterBtnAction = new FilterButtonAction(); 
        if (filterMenu == null) makeFilterMenu();
        JButton btn = new JButton();
        if (filterBtnAction.getValue(Action.SMALL_ICON) != null) btn.putClientProperty("hideActionText", Boolean.TRUE);
        btn.setAction(filterBtnAction);
        btn.setMargin(new Insets(0,0,0,0));
        Dimension btnSize = new Dimension(24,24);
        //btn.setBackground(Color.white);
        btn.setMaximumSize(btnSize);
        btn.setPreferredSize(btnSize);
        btn.setHorizontalTextPosition(SwingConstants.LEADING);
        btn.setEnabled(getFilterEnabled());
        return btn;
   }

   protected void createZoomScaleComboBox(Container c) {
        //c.add(new JLabel("secs/view="));
        zoomScaleComboBox = new ZoomScaleComboBox(mv); // Values are seconds for width of viewport.
        zoomScaleComboBox.setMaximumSize(new Dimension(72,24));
        zoomScaleComboBox.setPreferredSize(new Dimension(72,24));
        zoomScaleComboBox.setToolTipText("viewport timespan seconds");
        c.add(zoomScaleComboBox);
   }

/*-----------------------------------------------------------------
 *  method to stream line adding of buttons, with tooltips
 */
    protected JButton addButton(Container c, String label, String imageFile, ActionListener al, String tip) {
        Image image = IconImage.getImage(imageFile); // this handles finding the icons in the path
        JButton btn = (image == null) ?  new JButton(label) : new JButton(null, new ImageIcon(image));
        if (al != null) btn.addActionListener(al);
        if (tip != null) btn.setToolTipText(tip);
        btn.setActionCommand(label);
        btn.setMargin(new Insets(0,0,0,0));
        Dimension btnSize = new Dimension(32,24);
        btn.setMaximumSize(btnSize);
        btn.setPreferredSize(btnSize);
        //btn.setBackground(Color.white);
        c.add(btn);
        return btn;
    }

    /** Returns true if the filter button is enabled. */
    public boolean getFilterEnabled() {
       return filterEnabled;
    }

    /** Set Filtering enabled. Enables button in lower-right corner of the
    * scroller that toggles waveform filtering on/off. By default a Butterworth
    * highpass filter is used. A different filter can be set by calling
    * zwfp.setFilter(FilterIF). */
    public void setFilterEnabled(boolean tf) {
       filterEnabled = tf;
       //if (filterButton != null) filterButton.setEnabled(filterEnabled);
       if (filterBtnAction != null) filterBtnAction.setEnabled(filterEnabled);
    }

    /** Set the list of time scales in the drop-down menu. String list must contain
     valid numbers only.
     @See: ZoomScaleComboHandler.setList(String[] list)*/
    public void setScaleList(String list[]) {
      zoomScaleComboBox.setList(list);
    }

    /** Returns the current zoom scale value as a string (seconds). */
    public String getZoomValue() {
      return zoomScaleComboBox.getCurrentValue();
    }
    /** Returns the current zoom scale value as a string (seconds). */
    public void setZoomValue(String scale) {
      zoomScaleComboBox.setCurrentValue(scale);
    }

    class CenterTimeChangeListener implements ChangeListener {

        JLabel label = null;

        CenterTimeChangeListener(JLabel label) {
            this.label = label;
        }

        public void stateChanged(ChangeEvent changeEvent) {
            if (mv == null) return;
            if (changeEvent.getSource() == mv.masterWFWindowModel) {
              if (label != null) label.setText(
                //LeapSeconds.trueToString(mv.masterWFWindowModel.getCenterTime(), "HH:mm:ss.SSS").substring(0,12) // use UTC now - aww 2008/02/10
                LeapSeconds.trueToString(mv.masterWFWindowModel.getCenterTime(), "yyyyMMdd HH:mm:ss.fff").substring(0,21)
              );
            }
            //System.out.println("ZoomPanel CenterTimeChangeListener stateChanged event source: " + changeEvent.getSource().getClass().getName());
        }
    }

/** Handle filter button actions. */
class FilterButtonAction extends AbstractAction { // implements ActionListener {
  public ImageIcon filterOnImage = null;
  public ImageIcon filterOffImage = null;

  public FilterButtonAction() {
      Image img = IconImage.getImage("filter_yellow.gif");
      if (img != null)  filterOffImage = new ImageIcon(img);
      img = IconImage.getImage("filter_red.png");
      if (img != null)  filterOnImage = new ImageIcon(img);

      putValue(Action.ACTION_COMMAND_KEY, "F_Off");
      putValue(Action.NAME, "X");
      putValue(Action.SHORT_DESCRIPTION, "Show waveform filter popup menu");
      if (filterOffImage != null) putValue(Action.SMALL_ICON, filterOffImage); 
      setEnabled(false);
  }

  public void actionPerformed(ActionEvent evt) {
      filterMenu.show((Component)evt.getSource(), 0, 0);
  }

}

protected void setShowAlternateWf(boolean tf) {
    boolean selected = triaxialToggleButton.isSelected(); // -aww 2010/02/02
    triaxialToggleButton.setSelected(false);
    zwfp.setShowAlternateWf(tf);
    if (selected) triaxialToggleButton.doClick(); // -aww 2010/02/02
}


protected void toggleFilterOnOff() {
    if (jmiOnOff == null || filterBtnAction == null) return;
    else if (jmiOnOff.getText().equals("On")) {
        filterBtnAction.putValue(Action.NAME, "F");
        filterBtnAction.putValue(Action.ACTION_COMMAND_KEY, "F_On");
        if (filterBtnAction.filterOnImage != null)
            filterBtnAction.putValue(Action.SMALL_ICON, filterBtnAction.filterOnImage); 
        setShowAlternateWf(true);
        //if (zwfp != null) System.out.println("Filtering.... " + zwfp.wfv.getChannelObj().toDelimitedNameString('.')); //debug 2015/12/19 -aww
        jmiOnOff.setText("Off");
    }
    else if (jmiOnOff.getText().equals("Off")) {
        filterBtnAction.putValue(Action.NAME, "X");
        filterBtnAction.putValue(Action.ACTION_COMMAND_KEY, "F_Off");
        if (filterBtnAction.filterOffImage != null) 
            filterBtnAction.putValue(Action.SMALL_ICON, filterBtnAction.filterOffImage); 
        setShowAlternateWf(false);
        jmiOnOff.setText("On");
    }
    //revalidate(); // is this needed? -aww 2009/04/04
    cursorLocPanel.cursorLocModel.setWFPanel(zwfp);
    if (zwfp.getWf() != null) {
      cursorLocPanel.setAmpFormat(zwfp.getWf().getAmpUnits(), zwfp.getWf().getMaxAmp());
    }
    makeChannelLabel();
    zwfp.repaint();
    repaint();
}

class FilterMenuItemHandler implements ActionListener {
  public void actionPerformed(ActionEvent evt) {
    String cmd = evt.getActionCommand();
    FilterIF f = zwfp.filter;
    Waveform oldWf = zwfp.getWf();
    boolean status = true;
    //zwfp.isTGMFilter = false;
    //System.out.println("ZoomPanel DEBUG cmd: "+cmd+" filter b4: "+f.toString());
    if (cmd.equals("On")) {
        //filterButton.setText("F");
        filterBtnAction.putValue(Action.NAME, "F");
        filterBtnAction.putValue(Action.ACTION_COMMAND_KEY, "F_On");
        if (filterBtnAction.filterOnImage != null)
            filterBtnAction.putValue(Action.SMALL_ICON, filterBtnAction.filterOnImage); 
        setShowAlternateWf(true);
        jmiOnOff.setText("Off");
    }
    else if (cmd.equals("Off")) {
        //filterButton.setText("X");
        filterBtnAction.putValue(Action.NAME, "X");
        filterBtnAction.putValue(Action.ACTION_COMMAND_KEY, "F_Off");
        if (filterBtnAction.filterOffImage != null) 
            filterBtnAction.putValue(Action.SMALL_ICON, filterBtnAction.filterOffImage); 
        setShowAlternateWf(false);
        jmiOnOff.setText("On");
    }
    else {

      //setShowAlternateWf(false);

      if (cmd.equals("Customize...")) {
          status = setupFilter(f);
      }
      /*
      // Below commented out block only for comparison of RSAFilter versus TotalGroundMotion output waveforms
      else if (cmd.startsWith("TGM-")) {
          zwfp.isTGMFilter = true;
          if (tgmf == null) tgmf = new TotalGroundMotionFilter();
          tgmf.filter(zwfp.getRawWf().copy());
          if (cmd.endsWith("SP3.0")) {
              zwfp.alternateWf = tgmf.getFilteredWaveform(AmpType.SP30);
          }
          if (cmd.endsWith("SP1.0")) {
              zwfp.alternateWf = tgmf.getFilteredWaveform(AmpType.SP10);
          }
          if (cmd.endsWith("SP0.3")) {
              zwfp.alternateWf = tgmf.getFilteredWaveform(AmpType.SP03);
          }
          System.out.println("Returning from filter of type " + cmd);
          setShowAlternateWf(true);
          zwfp.setupWf();
          filterBtnAction.putValue(Action.NAME, "F");
          jmiOnOff.setText("Off");
          filterBtnAction.putValue(Action.NAME, "F");
          filterBtnAction.putValue(Action.ACTION_COMMAND_KEY, "F_On");
          if (filterBtnAction.filterOnImage != null)
              filterBtnAction.putValue(Action.SMALL_ICON, filterBtnAction.filterOnImage); 
          zwfp.revalidate(); // is this needed? -aww 2009/04/04
          if (zwfp.getWf() != null) cursorLocPanel.setAmpFormat(zwfp.getWf().getAmpUnits(), zwfp.getWf().getMaxAmp());
          makeChannelLabel();
          zwfp.repaint();
          repaint();
          return;
      }
      */
      else {
        int itype = FilterTypes.getType(cmd);
        //System.out.println("DEBUG ZoomPanel filter itype: " + itype);
        if (itype != f.getType() && itype > -1 )  {
          f = FilterTypes.getFilter(itype, f.getSampleRate(), (cmd.indexOf("WA") >= 0));
          status = (f != null);
        }
        //System.out.println(" class: " + ((! status) ? "null" : f.getClass().getName()));
      }
      if (status) {
        zwfp.setFilter(f, false);
        setShowAlternateWf(true);
        zwfp.setupWf();
        //zwfp.revalidate();
        //filterButton.setText("F");
        filterBtnAction.putValue(Action.NAME, "F");
        jmiOnOff.setText("Off");
        filterBtnAction.putValue(Action.NAME, "F");
        filterBtnAction.putValue(Action.ACTION_COMMAND_KEY, "F_On");
        if (filterBtnAction.filterOnImage != null)
            filterBtnAction.putValue(Action.SMALL_ICON, filterBtnAction.filterOnImage); 
      }
      else JOptionPane.showMessageDialog(getTopLevelAncestor(),
              "Invalid Filter - check text log", "Filter Menu", JOptionPane.ERROR_MESSAGE);
    }

    zwfp.revalidate(); // is this needed? -aww 2009/04/04
    cursorLocPanel.cursorLocModel.setWFPanel(zwfp);
    if (zwfp.getWf() != null) cursorLocPanel.setAmpFormat(zwfp.getWf().getAmpUnits(), zwfp.getWf().getMaxAmp());
    makeChannelLabel();
    zwfp.repaint();
    repaint();
    // Fire property change event here to notify SwarmFrame listener for filtered waveform change in jiggle data source
    Object obj = mv.getOwner();
    if (obj instanceof Jiggle) {
        Jiggle jiggle = (Jiggle) obj;
        if ( (oldWf != zwfp.getWf()) || !oldWf.getFilterName().equals(zwfp.getWf().getFilterName()) ) {
            //System.out.println("DEBUG : ZoomPanel notifying WFPanel.FILTER_CHANGED");
            // put notification here for Swarm Frame
            jiggle.propertyChange(new java.beans.PropertyChangeEvent(this, WFPanel.FILTER_CHANGED,
                        oldWf.getFilterName(), zwfp.getWf().getFilterName()) );
        }
                
    }

  }
}
    private boolean setupFilter(FilterIF f) {
        boolean status = true; 
        //System.out.println("DEBUG setupFilter filter: " + f.getDescription());
        if ( f instanceof AbstractWaveformFilter) {  
          if (f instanceof NoiseFilterIF) {
            WaveformBandpassFilterIF noiseFilter = ((NoiseFilterIF)f).getNoiseFilter();
            if (noiseFilter != null) {
              status =  setupFilter(noiseFilter);
              if (f instanceof WAFilter) f.setDescription("WAS_BW"+ ((ButterworthFilterSMC)noiseFilter).getBandString());
              else if (f instanceof WA_EHFilter) f.setDescription("WA_EH"+ ((ButterworthFilterSMC)noiseFilter).getBandString());
              else if (f instanceof WA_EHKFilter) f.setDescription("WA_EHK");
            }
          }
          else {
            AbstractWaveformFilter awf = (AbstractWaveformFilter) f;
            //NumberTextField alphaField = NumberTextFieldFactory.createInputField(6, false, "0.##", getFont(), true);
            //alphaField.setValue(awf.getCosineTaperAlpha());
            JSpinner alphaField = new JSpinner(new SpinnerNumberModel(awf.getCosineTaperAlpha(), 0., 1., 0.01));
            ((JSpinner.DefaultEditor)alphaField.getEditor()).getTextField().setEditable(false);
            Box b = Box.createHorizontalBox();
            b.add(new JLabel("Taper alpha:"));
            b.add(alphaField);
            JCheckBox alphaChkBox = new JCheckBox("Apply taper");
            alphaChkBox.setSelected(awf.getApplyTaper());
            JPanel jp = new JPanel();
            jp.setLayout(new BorderLayout());
            jp.add(b, BorderLayout.CENTER);
            jp.add(alphaChkBox, BorderLayout.SOUTH);
            //JCheckBox reverseChk = new JCheckBox("Reverse");
            //reverseChk.setSelected(nf.isReversed());
            //jp.add(reverseChk, BorderLayout.SOUTH);
            int ans =
                JOptionPane.showConfirmDialog(getTopLevelAncestor(), jp, awf.getDescription(), JOptionPane.YES_NO_OPTION);
            if (ans == JOptionPane.YES_OPTION) {
                status = true;
                //awf.setCosineTaperAlpha(alphaField.getDoubleValue());
                awf.setCosineTaperAlpha(((Number)alphaField.getValue()).doubleValue());
                awf.setApplyTaper(alphaChkBox.isSelected());
                //awf.setReversed(reverseChk.isSelected());
            }
          }
        }
        else if ( (f instanceof NotchFilterIF) && (f.getType() == FilterTypes.NOTCH)) {  
            NotchFilterIF nf = (NotchFilterIF) f;
            NumberTextField f1 = NumberTextFieldFactory.createInputField(6, false, "0.##", getFont(), true);
            NumberTextField f2 = NumberTextFieldFactory.createInputField(6, false, "0.##", getFont(), true);
            f1.setValue(nf.getNotchHz());
            f2.setValue(nf.getNotchWidth());
            Box b = Box.createHorizontalBox();
            b.add(new JLabel("RejectHz:"));
            b.add(f1);
            b.add(new JLabel("dw:"));
            b.add(f2);
            //JComboBox combo = new JComboBox(new Float [] { new Float(.01),new Float(.05),new Float(.10) });
            JPanel jp = new JPanel();
            jp.setLayout(new BorderLayout());
            jp.add(b, BorderLayout.CENTER);
            JCheckBox reverseChk = new JCheckBox("Reverse");
            reverseChk.setSelected(nf.isReversed());
            jp.add(reverseChk, BorderLayout.SOUTH);
            int ans = JOptionPane.showConfirmDialog(getTopLevelAncestor(), jp, "Notch", JOptionPane.YES_NO_OPTION);
            if (ans == JOptionPane.YES_OPTION) {
                status = nf.setNotch(nf.getSampleRate(), f1.getDoubleValue(), f2.getDoubleValue(), reverseChk.isSelected());
            }
        }
        else if (f instanceof BandpassFilterIF) { 
            BandpassFilterIF bf = (BandpassFilterIF) f;
            NumberTextField f1 = NumberTextFieldFactory.createInputField(6, false, "0.##", getFont(), true);
            NumberTextField f2 = NumberTextFieldFactory.createInputField(6, false, "0.##", getFont(), true);
            JComboBox combo = new JComboBox(new Integer [] {
                //Integer.valueOf(1),Integer.valueOf(2),Integer.valueOf(3),Integer.valueOf(4), Integer.valueOf(5), Integer.valueOf(6),Integer.valueOf(7),Integer.valueOf(8)
                Integer.valueOf(2),Integer.valueOf(4),Integer.valueOf(6),Integer.valueOf(8)
            });
            combo.setSelectedIndex(bf.getOrder()/2 - 1);
            combo.setToolTipText("Filter order restricted to even number <= 8");
            Box b = Box.createHorizontalBox();
            f1.setValue(bf.getLoHz());
            f2.setValue(bf.getHiHz());
            b.add(new JLabel("LoHz:"));
            b.add(f1);
            b.add(new JLabel("HiHz:"));
            b.add(f2);
            b.add(new JLabel("Order:"));
            b.add(combo);
            JPanel jp = new JPanel();
            jp.setLayout(new BorderLayout());
            jp.add(b, BorderLayout.CENTER);
            JCheckBox reverseChk = new JCheckBox("Reverse");
            reverseChk.setSelected(f.isReversed());
            jp.add(reverseChk, BorderLayout.SOUTH);
            int ans = JOptionPane.showConfirmDialog(getTopLevelAncestor(), jp, "BandPass", JOptionPane.YES_NO_OPTION);
            if (ans == JOptionPane.YES_OPTION) {
                status = bf.setFilter( bf.getSampleRate(), 
                               f1.getDoubleValue(),
                               f2.getDoubleValue(),
                               //combo.getSelectedIndex()+1,
                               ((Integer)combo.getSelectedItem()).intValue(),
                               reverseChk.isSelected()
                         );
            }
        }
        return status; // false is no config possible?
    }

/**
 * Make the Label describing the channel.
 */
  void makeChannelLabel() {

// Lost in listener land?
// Be certain that the child zwfp listener is called before this
// ZoomPanel instances view listener (order sequence). Otherwise,
// it gets to here, upon first waveform load with the zwfp.wfv
// still null, or subsequently referencing prior zwfp.wfv.  -aww
      String header = "";
       if (zwfp == null || zwfp.wfv == null) {
         header = "No View Data.";
       } else {

         header = zwfp.wfv.getChannelObj().toDelimitedNameString(' '); //aww

         Waveform wfx = zwfp.getWf();  // get selected waveform
         if (wfx != null && wfx.hasTimeSeries()) {

             //DecimalFormat fmt = di;
             DecimalFormat fmt = df;
             double maxAmp = Math.abs(wfx.getMaxAmp()); 
             if (maxAmp < 1.0) fmt = dsci;  // use Sci. Notatation for small numbers
             else if (maxAmp < 10) fmt = df2;

             // aww 06/11/2004 fixed to horiz dist
             header +=
               " " + df.format(zwfp.wfv.getHorizontalDistance()) + " Km" +
               " " + di.format((int)Math.round(zwfp.wfv.getAzimuth())) + "\u00B0" +
               " (bias,max-bias,min-bias)=(" + fmt.format((int) wfx.getBias() ) +
               ", " + fmt.format((wfx.getMaxAmp()-wfx.getBias())) + "," +
               fmt.format((wfx.getMinAmp()-wfx.getBias())) + ") ";
             String str = wfx.getFilterName();
             if (str != null && str.trim().length() > 0 ) header += " filter= " + str;
         } else {
           header += " No time series.";
         }
       }

       staLabel.setText(header);   //should repaint automatically
    }

    /** Return the ZoomableWFPanel */
    public ZoomableWFPanel getActiveWFPanel() {
        return zwfp;
    }

    void setWFView(WFView wfv) {

         if (Jiggle.jiggle != null ) {
           // FIRST TRY CHANGING SELECTED WFV with EHGFILTER BUTTON SELECTED WITHOUT THIS (THEN ENABLE THIS AND COMPARE) !
           // Custom button/property feature added 2013/01/25 for Mitch Withers Memphis Central US New Madrid to degain EH gain-ranged
           if ( Jiggle.jiggle.getWfPanelProperties().getBoolean("addEHGFilterButton") ) {
             if ( ehgButton != null && ehgFilter != null &&
                  ehgButton.isSelected() && ! ehgFilter.isValidForChannel(wfv.getChannelObj()) ) {
                 System.out.println("EHG INFO: resetting EHG button, invalid for new channel:" +
                       wfv.getChannelObj().toDelimitedSeedNameString());
                 ehgButton.setSelected(false);
                 setShowAlternateWf(false);
             }
           }
           else { // not CERI EHG - for Macbeth
             StringList list = Jiggle.jiggle.getProperties().getStringList("displayFilteredSeedchan");
             if (list != null && list.size() > 0) {
               Channel ch = (Channel) wfv.getChannelObj();
               if ( list.contains(ch.getSeedchan()) ) { // check to see if user wants certain channel filtered by default filter in view
                 if ( defaultFilterAlways ) zwfp.setFilter( FilterTypes.getFilter(FilterTypes.getType(defaultFilterType), 100, false) );
                 if (! zwfp.getShowAlternateWf() ) { // setup filter
                   //System.out.println("DEBUG ZoomPanel filtering seedchan: " + ch.getSeedchan());
                   toggleFilterOnOff();
                 }
               }
               else { // seedchan not in property string list
                 if ( zwfp.getShowAlternateWf() ) { // so turn off filter
                   //System.out.println("DEBUG ZoomPanel NOT filtering seedchan: " + ch.getSeedchan());
                   toggleFilterOnOff();
                 }
               }
             }
           }
         }

         if (ampTwoExp > 0) ampTwoExp = -ampTwoExp; // reverse the scaling

         staLabel.setForeground(Color.blue);
         makeChannelLabel();

         // set the cursor location panel's amp style
         if (wfv == null) return;

         Waveform wf = zwfp.getWf();

         if (wf != null) {
              synchronized (wf) {
                  cursorLocPanel.cursorLocModel.setWFPanel(zwfp);
                  cursorLocPanel.setAmpFormat(wf.getAmpUnits(), Math.max(Math.abs(wf.getMaxAmp()),Math.abs(wf.getMinAmp())));
                  if (!wf.hasTimeSeries())  {
                    // remove old one then add again, else might register multi. times
                    wf.removeChangeListener(activeWFLoadListener);
                    wf.addChangeListener(activeWFLoadListener);
                  }
              }
         }
         //revalidate(); // aww test
         boolean selected = triaxialToggleButton.isSelected(); // -aww 2010/02/02
         if (oldWFV != null) {
           if (selected) { 
             Object obj = mv.getOwner();
             if (obj instanceof WaveformDisplayIF) {
               WaveformDisplayIF jiggle = (WaveformDisplayIF) obj;
               if (Triaxial.sameTriaxialAs(oldWFV.getChannelObj(), wfv.getChannelObj()) || jiggle.getWfPanelProperties().getBoolean("triaxialSelectionLocked")) {
                 triaxialToggleButton.setSelected(false); // -aww 2010/02/02
               }
               triaxialToggleButton.doClick(); // -aww 2010/02/02
             }
           }
         }

         oldWFV = wfv;
         repaint(); // aww test
    }


    /** INNER CLASS: Change events are expected from Waveform objects when the
     *  timeseries is loaded. */
    class ActiveWFLoadListener implements ChangeListener {
        public void stateChanged(ChangeEvent changeEvent) {
            if (SwingUtilities.isEventDispatchThread()) {
                if (mv != null) setWFView(mv.masterWFViewModel.get());
            }
            else {
              SwingUtilities.invokeLater(
                new Runnable() {
                    public void run() {
                        if (mv != null) setWFView(mv.masterWFViewModel.get());
                    }
                }
              );
            }
        }
    }

/** INNER CLASS: Handle changes to the selected WFView. */
    class WFViewListener implements ChangeListener {
        public void stateChanged(ChangeEvent changeEvent) {
            if (SwingUtilities.isEventDispatchThread()) {
                if (mv != null) setWFView(mv.masterWFViewModel.get());
            }
            else {
              SwingUtilities.invokeLater(
                new Runnable() {
                    public void run() {
                        if (mv != null) setWFView(mv.masterWFViewModel.get());
                    }
                }
              );
            }
        }
    }  // end of WFViewListener

    class WFWindowListener implements ChangeListener {
        public void stateChanged(ChangeEvent changeEvent) {
            // This action is necessary, to get label updated after the zwfp is created, zwfp setup causes another window change event
            if (SwingUtilities.isEventDispatchThread()) {
                makeChannelLabel();
            }
            else {
              SwingUtilities.invokeLater(
                new Runnable() {
                    public void run() {
                        makeChannelLabel();
                    }
                }
              );
            }
        }
    }  // end of WFViewListener

/**
 * Adjustment events are triggered by BOTH mouse down and mouse up.
 * They are also fired by a frame resize but the 'value' is 0.
 * The object here is to keep the masterWFWindowModel in synch with
 * the viewport area.
 */
    class VerticalScrollListener implements AdjustmentListener {

      WFSelectionBox viewBox = new WFSelectionBox();   //viewport box
      Rectangle vrect;
      int oldValue = 0;
      int newValue = 0;

      public void adjustmentValueChanged(AdjustmentEvent e) {

          if (zwfp == null || zwfp.wfv == null) return;        // empty panel

          newValue = e.getValue();      // the "change" value (= pixels scrolled)
          if (newValue == 0) return;    // ignore resize events

          // the following prevents recursive thrash
          // rounding error in conversion from amp to pixels can cause a difference
          // of one pixel in the value even if there was no change
          if (Math.abs(newValue - oldValue) <= 1) return;   // ignore redundant events

          oldValue = newValue;

          // get current JViewport rectangle
          // NOTE: the viewport has already been adjusted by the underlying component!
          // So just synch masterWFWindowModel with current viewport.
          vrect = bsvport.getViewRect();
  
          double centerAmp = zwfp.ampOfPixel(vrect.y + (vrect.height/2));
          // THIS FIRES WFWindowModel change EVENT!!!!!!!!!!!!!!!!!!!!!!!!!!!!:
          mv.masterWFWindowModel.setCenterAmp(zwfp.getWf(), (int) Math.round(centerAmp) );
      }

    }  // end of VerticalScrollListener

    /**
     * Adjustment events are triggered by BOTH mouse down and mouse up.
     * They are also fired by a frame resize but the 'value' is 0.
     */
    class HorizontalScrollListener implements AdjustmentListener {

      WFSelectionBox viewBox = new WFSelectionBox();   //viewport box
      Rectangle vrect;
      int oldValue = 0;
      int newValue = 0;

      public void adjustmentValueChanged(AdjustmentEvent e) {

          if (zwfp == null || zwfp.wfv == null) return;        // empty panel

          int newValue = e.getValue();  // the "change" value (= pixels scrolled)
          if (newValue == 0) return;    // ignore resize events

          // the following prevents recursive thrash
          if (newValue == oldValue) return;

          oldValue = newValue;

          // get current JViewport rectangle
          vrect = bsvport.getViewRect();

          double centerTime = zwfp.dtOfPixel(vrect.x + (vrect.width/2));
          if (centerTime > 0.) { // remove to use with timeseries b4 Jan 1, 1970
               if (Math.abs(mv.masterWFWindowModel.getCenterTime() - centerTime) > 0.0001) {
                 //System.out.println("ZoomPanel horiz scroll to new centerTime: " + LeapSeconds.trueToString(centerTime));
                 mv.masterWFWindowModel.setCenterTime(centerTime); // THIS FIRES WFWindowModel change EVENT!
               }
          }
      }

    }  // end of HorizontalScrollListener

} // end of ZoomPanel class

