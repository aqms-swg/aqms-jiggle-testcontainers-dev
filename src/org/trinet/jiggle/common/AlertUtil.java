package org.trinet.jiggle.common;

import java.awt.Component;

import javax.swing.*;

import org.trinet.jiggle.ui.common.JIggleUiUtils;

public final class AlertUtil {

    /**
     * Alert users with a message dialog
     */
    public static void displayError(String title, String errorMsg) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final String errorDesc = errorMsg + JiggleConstant.NEWLINE +
                        JiggleConstant.NEWLINE +
                        "Please restart the application if the problem persists!";

                JOptionPane.showMessageDialog(null, errorDesc, title, JOptionPane.WARNING_MESSAGE);
                LogUtil.error(errorMsg);
            }
        });
    }

    public static void displayError(String title, String errorMsg, String actionMsg) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final String errorDesc = errorMsg + JiggleConstant.NEWLINE +
                        JiggleConstant.NEWLINE +
                        actionMsg;

                JOptionPane.showMessageDialog(null, errorDesc, title, JOptionPane.ERROR_MESSAGE);
                LogUtil.error(errorMsg);
            }
        });
    }

    /**
     * Display error with JOptionPane message type
     */
    public static void displayError(String title, String errorMsg, String actionMsg, int messageType) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final String errorDesc = errorMsg + JiggleConstant.NEWLINE +
                        JiggleConstant.NEWLINE +
                        actionMsg;

                JOptionPane.showMessageDialog(null, errorDesc, title, messageType);
                LogUtil.error(errorMsg);
            }
        });
    }

    /**
     * Display error and block for user input
     */
    public static int displayCrticialError(String title, String errorMsg) {
        final int[] response = new int[1];
        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                    final String errorDesc = errorMsg + JiggleConstant.NEWLINE +
                            JiggleConstant.NEWLINE +
                            "Continue processing?";

                    response[0] = JOptionPane.showConfirmDialog(JiggleSingleton.getInstance().getMainJFrame(),
                            errorDesc,
                            title,
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE);
                    LogUtil.warning(errorMsg + " User response is " + response[0]);
                }
            });
        } catch (Exception ex) {
            JiggleExceptionHandler.handleException(ex, "Error Displaying Dialog", "Error in displayCrticialError. ");
        }
        return response[0];
    }

    /**
     * Display the message.
     * 
     * @param parentComponent   the parent component or null if none.
     * @param title             the title.
     * @param message           the message text.
     * @param messageType       the type of message to be displayed.
     * @see javax.swing.JOptionPane#ERROR_MESSAGE
     * @see javax.swing.JOptionPane#INFORMATION_MESSAGE
     * @see javax.swing.JOptionPane#QUESTION_MESSAGE
     * @see javax.swing.JOptionPane#PLAIN_MESSAGE
     * @see javax.swing.JOptionPane#WARNING_MESSAGE
     */
    public static void displayMessage(Component parentComponent, String title, String message,
            int messageType) {
        final String lowerMessage = message.toLowerCase();
        final String contentType;
        final boolean hyperlinkListener;
        if (lowerMessage.startsWith("<html>")) {
            contentType = JIggleUiUtils.HTML_TEXT;
            if (lowerMessage.contains("href")) {
                hyperlinkListener = true;
            } else {
                hyperlinkListener = false;
            }
        } else {
            contentType = JIggleUiUtils.PLAIN_TEXT;
            hyperlinkListener = false;
        }
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                JComponent comp = JIggleUiUtils.getMessage(message, contentType, hyperlinkListener);
                JOptionPane.showMessageDialog(parentComponent, comp, title, messageType);
            }
        });
    }
}
