package org.trinet.jiggle.common;

import java.sql.Connection;
import java.sql.SQLException;

public final class DbConstant {
    private DbConstant() {}

    /**
     * Database type enum. Keep the name in lower cases
     */
    public enum DbType {
        ORACLE("oracle"),
        POSTGRES("postgres"),
        DEFAULT_SERVER("jdbc:default:connection"), // default server connection
        UNKNOWN("unknown");

        private final String dbName;

        DbType(String dbName) {
            this.dbName = dbName;
        }

        public String getDbName() {
            return this.dbName;
        }
    }

    /**
     * Detect the database type
     * @param conn the connection.
     * @return the database type.
     * @throws SQLException if a database access error occurs
     */
    public static DbType getDBType(Connection conn) throws SQLException {
        final DbType dbType;
        if (isOracle(conn)) {
          dbType = DbType.ORACLE;
        } else if (isPostgreSQL(conn)) {
          dbType = DbType.POSTGRES;
        } else {
          dbType = DbType.UNKNOWN;
        }
        return dbType;
    }
 
    /**
     * Determines if the database is Oracle.
     * @param conn the connection.
     * @return true if the database is Oracle, false otherwise.
     * @throws SQLException if a database access error occurs
     */
    public static boolean isOracle(Connection conn) throws SQLException {
        return conn.getMetaData().getDriverName().toLowerCase().contains(DbType.ORACLE.getDbName());
    }

    /**
     * Determines if the database is PostgreSQL.
     * @param conn the connection.
     * @return true if the database is PostgreSQL, false otherwise.
     * @throws SQLException if a database access error occurs
     */
    public static boolean isPostgreSQL(Connection conn) throws SQLException {
        return conn.getMetaData().getDatabaseProductName().toLowerCase()
          .contains(DbType.POSTGRES.getDbName());
    }

    public final static int REMARK_LENGTH = 80;

    public static String SQL_CUSTOM_EXCEPTION = "P0001"; // Postgres raise_exception SQL state code
    
    public final static String SCHEMA_ASSOCAMP = "assocamp";
    public final static String SCHEMA_EVENTPRIORITY = "eventpriority";
    public final static String SCHEMA_FILEUTIL = "file_util";
    public final static String SCHEMA_FORMATS = "formats";
    public final static String SCHEMA_PUBLIC = "public";
    public final static String SCHEMA_UTIL = "util";
    public final static String SCHEMA_WAVE = "wave";
    public final static String SCHEMA_WAVEARC = "wavearc";
    public final static String SCHEMA_WHERES = "wheres";
}
