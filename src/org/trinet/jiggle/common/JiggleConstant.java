package org.trinet.jiggle.common;

public interface JiggleConstant {
    public static final class Icons {
        public static final String JiggleLogoIcon = "JiggleLogo.gif";
    }

    String EMPTY_STRING = "";
    String ERROR_RESTART_JIGGLE = "Please retry or restart Jiggle!";
    String NEWLINE = System.getProperty("line.separator");
    String POPUP_MENU_ASSOCIATION = "Event Association";
    String POPUP_MENU_DELETE_ASSOCIATION = "Delete Association...";
    String POPUP_MENU_EDIT_ASSOCIATION = "Edit Association...";
    String POPUP_MENU_NEW_ASSOCIATION = "New Association...";
    String POPUP_MENU_REMOVE_EVENT_LOCK = "Remove Event Lock...";
}
