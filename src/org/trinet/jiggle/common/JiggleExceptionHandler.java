package org.trinet.jiggle.common;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Class to handle general Jiggle exception including SQLException. It does not
 * seem like there is a database base class to handle the exceptions. There is
 * SQLExceptionHandler, but may not be able to use it without affecting other
 * classes (i.e. error message dialog for UI)
 */
public class JiggleExceptionHandler {

    private static void errorHandler(Exception ex, String shortDesc) {
        LogUtil.error(shortDesc);
        ex.printStackTrace();
    }

    public static void handleDbException(Connection connection, SQLException ex) {
        handleDbException(connection, ex, "Database Error", "Database error detected");
    }

    /**
     * SQL Exception handler - clean up transaction and log error
     */
    public static void handleDbException(Connection connection, SQLException ex, String errorTitle,
            String description) {
        try {
            if (connection != null) {
                // Required for Postgres to prevent "ERROR: current transaction is aborted,
                // commands ignored until end
                // of transaction block"
                connection.rollback();
            }
        } catch (SQLException rollbackException) {
            rollbackException.printStackTrace();
            logError(rollbackException, "Cannot rollback transaction");
        }
        logError(ex, description);
        AlertUtil.displayError(errorTitle, description + JiggleConstant.NEWLINE + ex.getMessage());
    }

    public static void handleException(Exception ex, String errorTitle, String description) {
        logError(ex, description);
        AlertUtil.displayError(errorTitle, description + JiggleConstant.NEWLINE + ex.getMessage());
    }

    public static void logError(Exception ex, String shortDesc) {
        errorHandler(ex, shortDesc);
    }

    /**
     * Log SQLException
     **/
    public static void logError(SQLException ex, String shortDesc) {
        errorHandler(ex, shortDesc);
    }
}
