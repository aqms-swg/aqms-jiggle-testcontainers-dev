package org.trinet.jiggle.common;

import java.awt.*;
import org.trinet.jasi.EventSelectionProperties;
import org.trinet.jiggle.JiggleProperties;

public class JiggleSingleton {
    private Frame mainJFrame;
    private JiggleProperties jiggleProp;
    private EventSelectionProperties eventProp;
    
    private JiggleSingleton() {
    }

    private static class SingletonObj {
        private static JiggleSingleton instance = new JiggleSingleton();
    }

    public static final JiggleSingleton getInstance() {
        return SingletonObj.instance;
    }

    public Frame getMainJFrame() {
        return mainJFrame;
    }

    public void setMainJFrame(Frame mainJFrame) {
        this.mainJFrame = mainJFrame;
    }

    /**
     * @return the jiggleProp
     */
    public JiggleProperties getJiggleProp() {
        return jiggleProp;
    }

    /**
     * @param jiggleProp the jiggleProp to set
     */
    public void setJiggleProp(JiggleProperties jiggleProp) {
        this.jiggleProp = jiggleProp;
    }

    /**
     * @return the eventProp
     */
    public EventSelectionProperties getEventProp() {
        return eventProp;
    }

    /**
     * @param eventProp the eventProp to set
     */
    public void setEventProp(EventSelectionProperties eventProp) {
        this.eventProp = eventProp;
    }
    
    /**
     * Return "debug" flag
     */
    public boolean isDebug() {
        if (this.jiggleProp.isSpecified(PropConstant.DEBUG)) {
            return this.jiggleProp.getBoolean(PropConstant.DEBUG);
        } else {
            return false;
        }
    }
}
