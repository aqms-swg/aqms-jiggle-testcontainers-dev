package org.trinet.jiggle.common;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;

public class NullReadWriteLock implements ReadWriteLock {
    /** Singleton */
    private static final NullReadWriteLock lock = new NullReadWriteLock();

    /**
     * Get the null read write lock.
     * 
     * @return the null read write lock.
     */
    public static ReadWriteLock getInstance() {
        return lock;
    }

    /**
     * Create singleton
     */
    private NullReadWriteLock() {
    }

    /**
     * Returns the lock used for reading.
     *
     * @return null
     */
    @Override
    public Lock readLock() {
        return null;
    }

    /**
     * Returns the lock used for writing.
     *
     * @return null
     */
    @Override
    public Lock writeLock() {
        return null;
    }
}
