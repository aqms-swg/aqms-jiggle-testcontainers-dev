package org.trinet.jiggle.common.type;

import org.trinet.jiggle.common.PropConstant;

import java.util.Objects;

public class OriginType implements Comparable<OriginType> {
    private String code;
    private String desc;
    private boolean isDefault = false;

    public OriginType() {
    }
    
    public OriginType(String code, String desc, boolean isDefault) {
        this.code = code;
        this.desc = desc;
        this.isDefault = isDefault;
    }
    
    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OriginType that = (OriginType) o;
        return code.equalsIgnoreCase(that.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }

    public String getCode() {
        return this.code;
    }

    public String getDesc() {
        return this.desc;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String toString() {
        return this.desc;
    }

    public int compareTo(OriginType type) {
        return this.desc.compareToIgnoreCase(type.getDesc());
    }

    /**
     * Get origin type name in property format as lowercase.
     */
    public String getPropName(String type) {
        return PropConstant.ORIGIN_TYPE_PROP_NAME + type.toLowerCase();
    }
}