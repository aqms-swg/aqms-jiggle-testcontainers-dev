package org.trinet.jiggle.common.type;

import org.trinet.jiggle.common.PropConstant;
import org.trinet.util.GenericPropertyList;
import java.util.*;

/**
 * Origin types with one character type and long description
 */
public class OriginTypeList {

    private final String INVALID_TYPE = "INVALID ORIGIN TYPE";
    ArrayList<OriginType> originTypes = new ArrayList<OriginType>();

    public OriginTypeList(GenericPropertyList props, Boolean isIncludeAll) {
        String key = "";
        String desc = "";
        for (Enumeration e = props.propertyNames(); e.hasMoreElements();)  {
            key = ((String) e.nextElement()).toLowerCase();
            if (key.startsWith(PropConstant.ORIGIN_TYPE_PROP_NAME)) {
                desc = props.getProperty(key);
                String originCode = this.getOriginCode(key);

                // Add all origin types if flag is true, else only include ones that have a valid description
                if (isIncludeAll) {
                    originTypes.add(new OriginType(originCode, desc, isDefault(key)));
                } else {
                    if (desc != null && !desc.isEmpty()) {
                        originTypes.add(new OriginType(originCode, desc, isDefault(key)));
                    }
                }
            }
        }
    }

    /*
    Parse name and get the origin type code (i.e. H from origin.type.h)
     */
    private String getOriginCode(String name) {
        if (name.toLowerCase().startsWith(PropConstant.ORIGIN_TYPE_PROP_NAME)) {
            return name.replace(PropConstant.ORIGIN_TYPE_PROP_NAME, "").toUpperCase();
        } else {
            return "";
        }
    }

    /** Defaults are loaded in as default so it gets tricky to delete.
     *  default flag is used to allow edit on the description, but should not allow delete
     */
    private boolean isDefault(String originCode) {
        switch (originCode) {
            // approved Origin List as of 2021.
            case PropConstant.ORIGIN_KEY_AMP:
            case PropConstant.ORIGIN_KEY_CENTROID:
            case PropConstant.ORIGIN_KEY_DBLDIFF:
            case PropConstant.ORIGIN_KEY_HYPOCENTER:
            case PropConstant.ORIGIN_KEY_UNKNOWN:
            case PropConstant.ORIGIN_KEY_NONLOCATABLE:
                return true;
            default: return false;
        }
    }

    public ArrayList<OriginType> getTypeList() {
        Collections.sort(this.originTypes);
        return this.originTypes;
    }

    /**
     * Get long description for origin type code
     */
    public String getDescriptionForType(String type) {
        int index = this.originTypes.indexOf(new OriginType(type,"",false));
        if (index != -1) {
            return this.originTypes.get(index).getDesc();
        } else {
            return this.INVALID_TYPE;
        }
    }
}
