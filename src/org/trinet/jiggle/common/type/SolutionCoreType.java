package org.trinet.jiggle.common.type;

import java.util.Objects;
import org.trinet.jasi.Solution;
import org.trinet.jdbc.datatypes.DataLong;

public class SolutionCoreType {
    Solution solution;
    private AssocEvent assocEvent;
    
    public SolutionCoreType(){
        this.solution = null;
    }
    
    public SolutionCoreType(Solution solution){
        this.solution = solution;
    }
    
    public String toString() {
        return this.solution.getId() + " - " + this.solution.getSource() + " (" + this.solution.getDateTime() + ")";
    }
    
    public DataLong getEventId(){
        return this.solution.getId();
    }
    
    public Solution getSolutionType() {
        return this.solution;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.solution);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SolutionCoreType other = (SolutionCoreType) obj;
        return Objects.equals(this.solution, other.solution);
    }

    /**
     * @return the assocEvent
     */
    public AssocEvent getAssocEvent() {
        return assocEvent;
    }

    /**
     * @param assocEvent the assocEvent to set
     */
    public void setAssocEvent(AssocEvent assocEvent) {
        this.assocEvent = assocEvent;
    }
    
}
