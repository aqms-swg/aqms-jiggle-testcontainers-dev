package org.trinet.jiggle.database;

import org.trinet.util.gazetteer.TN.OracleLatLon;

import java.sql.Connection;
import java.sql.SQLException;

public abstract class JiggleConnection {
    public abstract String getSequenceQuery(String seqName);
    public abstract String getTableExistQuery();
    public abstract java.sql.Array getCoordinate(OracleLatLon[] latLon, Connection conn) throws SQLException;

    public abstract StringBuffer setRowLimitCondition(StringBuffer sb, int maxRow);
}
