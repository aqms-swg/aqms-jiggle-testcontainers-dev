package org.trinet.jiggle.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.trinet.jasi.DataSource;
import org.trinet.jdbc.datasources.DbaseConnectionDescription;
import org.trinet.jiggle.common.JiggleExceptionHandler;
import org.trinet.jiggle.common.JiggleSingleton;
import org.trinet.jiggle.common.LogUtil;

public class JiggleDb implements AutoCloseable {
    /**
     * Close the connection.
     * 
     * @param conn the connection.
     */
    public static void close(Connection conn) {
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException ex) {
            JiggleExceptionHandler.logError(ex, "Cannot close database connection in JiggleDb.close.");
        }
    }

    /**
     * Close the statement.
     * 
     * @param conn the connection.
     * @param s    the statement.
     */
    public static void close(Connection conn, Statement s) {
        if (s != null) {
            try {
                s.close();
            } catch (SQLException ex) {
                JiggleExceptionHandler.handleDbException(conn, ex);
            }
        }
    }

    private Connection conn;

    public JiggleDb() {
        this(DataSource.getConnection());
    }

    public JiggleDb(Connection conn) {
        this.conn = conn;
    }

    @Override
    public void close() {
        if (conn != null) {
            close(conn);
        }
    }

    protected void close(ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException ex) {
                JiggleExceptionHandler.handleDbException(this.conn, ex);
            }
        }
    }

    protected void close(Statement s) {
        close(conn, s);
    }

    /**
     * Commit all changes.
     */
    public void commit() {
        if (this.conn != null) {
            try {
                this.conn.commit();
            } catch (SQLException ex) {
                JiggleExceptionHandler.handleDbException(this.conn, ex, "JiggleDb commit error.",
                        "JiggleDb commit error.: ");
            }
        } else {
            LogUtil.warning("Database JiggleDb error. Cannot commit with null database connection");
        }
    }

    /**
     * Get database connection. Try reconnect if null or closed.
     * 
     * @return
     */
    public Connection getConnection() {
        try {
            if (this.conn == null || this.conn.isClosed()) {
                // Simplified db reconnect based on Jiggle.makeDataSourceConnection
                // Allows user to continue when the DB connection is restored instead of
                // restarting Jiggle
                DbaseConnectionDescription dbDesc = JiggleSingleton.getInstance().getJiggleProp().getDbaseDescription();
                new DataSource();
                DataSource.set(dbDesc);
                this.conn = DataSource.getConnection();
            }
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(conn, ex,
                    "Cannot get database connection in JiggleDb.getConnection. ", "Database Connection");
        }
        return this.conn;
    }

    public void rollback() {
        if (this.conn != null) {
            try {
                this.conn.rollback();
            } catch (SQLException ex) {
                JiggleExceptionHandler.handleDbException(this.conn, ex, "JiggleDb commit error.",
                        "JiggleDb commit error.: ");
            }
        } else {
            LogUtil.warning("Database JiggleDb error. Cannot rollback with null database connection");
        }
    }
}
