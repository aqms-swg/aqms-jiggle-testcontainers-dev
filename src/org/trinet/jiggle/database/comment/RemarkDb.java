package org.trinet.jiggle.database.comment;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.trinet.jasi.JasiDatabasePropertyList;
import org.trinet.jdbc.table.SeqIds;
import org.trinet.jiggle.common.JiggleExceptionHandler;
import org.trinet.jiggle.common.type.RemarkType;
import org.trinet.jiggle.database.JiggleDb;

public class RemarkDb extends JiggleDb {
    public RemarkDb() {
    }

    /**
     * Delete a remark by comment id
     */
    public boolean deleteRemark(long commentId) {
        int numRows = -1;
        final String sql = "DELETE FROM REMARK WHERE commid = ? ";
        PreparedStatement ps = null;
        try {
            ps = this.getConnection().prepareStatement(sql);
            ps.setLong(1, commentId);
            numRows = ps.executeUpdate();
            this.commit();
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(this.getConnection(), ex, "Error deleteRemark()",
                    "Error deleteRemark: ");
        } finally {
            JasiDatabasePropertyList.debugSQL(sql, ps);
            close(ps);
        }
        return numRows > 0;
    }

    /**
     * Inserts a comment for one line number. See SolutionTN.insertComment for
     * multi-line comments Return comment id inserted when successful
     */
    public RemarkType insertRemark(RemarkType remark) {
        int numRows = -1;
        final String sql = "INSERT INTO REMARK (commid, lineno, remark) VALUES (?, ?, ?)";
        PreparedStatement ps = null;
        try {
            long myCommid = SeqIds.getNextSeq(this.getConnection(), org.trinet.jdbc.table.Remark.SEQUENCE_NAME);
            remark.setCommentId(myCommid);

            ps = this.getConnection().prepareStatement(sql);
            ps.setLong(1, remark.getCommentId());
            ps.setInt(2, remark.getLineNumber());
            ps.setString(3, remark.getComment());

            numRows = ps.executeUpdate();
            this.commit();
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(this.getConnection(), ex, "Error insertRemark()",
                    "Error insertRemark: ");
        } finally {
            JasiDatabasePropertyList.debugSQL(sql, ps);
            close(ps);
        }
        return (numRows < 0 ? null : remark); // null indicates failed operation
    }

    /** Update one line comment for the comment id **/
    public boolean updateRemark(long commentId, String comment) {
        int numRows = -1;
        final String sql = "UPDATE REMARK SET remark = ? WHERE commid = ? ";
        PreparedStatement ps = null;
        try {
            ps = this.getConnection().prepareStatement(sql);
            ps.setString(1, comment);
            ps.setLong(2, commentId);
            numRows = ps.executeUpdate();
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(this.getConnection(), ex, "Error updateRemark()",
                    "Error updateRemark: ");
        } finally {
            JasiDatabasePropertyList.debugSQL(sql, ps);
            close(ps);
        }
        return numRows > 0;
    }
}
