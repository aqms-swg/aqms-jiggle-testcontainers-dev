package org.trinet.jiggle.database.event;

import java.util.List;
import java.util.Set;

import org.trinet.jiggle.common.type.AssocEvent;

public interface EventAssoc extends AutoCloseable {
    @Override
    void close();

    /**
     * Commit all changes.
     */
    void commit();

    /**
     * Delete all event association for the event ID
     * 
     * @param eventId the event ID
     * @return true if any rows were deleted, false otherwise.
     */
    boolean deleteAssociatedEvent(long eventId);

    /**
     * Get all unique event IDs, both master and associated, used by the eventId
     * 
     * @param eventId the event ID
     * @return the unique event IDs.
     */
    Set<Long> getAllEventId(long eventId);

    /**
     * Get associated events for the event ID
     * 
     * @param eventId the event ID
     * @return the associated events
     */
    List<AssocEvent> getAssociatedEvents(long eventId);

    /**
     * Get the current comment id on the event ID
     * 
     * @param eventId the event ID
     * @return the associated event with a comment or null if none.
     */
    AssocEvent getComment(long eventId);

    /**
     * Insert the associated event.
     * 
     * @param event the associated event
     * @return true if any rows were inserted, false otherwise.
     */
    boolean insertAssociatedEvent(AssocEvent event);

    /**
     * Check if combination of master event and associated event exists in the
     * database
     * 
     * @param masterEventId the master event ID.
     * @param assocEventId  the associated event ID.
     * @return the associated event if it exists, null otherwise.
     */
    AssocEvent isExist(long masterEventId, long assocEventId);

    /**
     * Undo all changes.
     */
    void rollback();
}
