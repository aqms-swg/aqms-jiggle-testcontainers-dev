package org.trinet.jiggle.database.event;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.trinet.jasi.DataSource;
import org.trinet.jasi.JasiDatabasePropertyList;
import org.trinet.jiggle.Jiggle;
import org.trinet.jiggle.common.JiggleExceptionHandler;
import org.trinet.jiggle.common.JiggleSingleton;
import org.trinet.jiggle.common.LogUtil;
import org.trinet.jiggle.common.type.AssocEvent;
import org.trinet.jiggle.database.JiggleDb;
import org.trinet.util.ParseString;

public class EventAssocCacheStore {
    private static Map<Long, List<AssocEvent>> _associatedEventsMap;
    private static long _loadTime;
    private static long _maxAge;
    /** ASSOCEVENTS cache maximum age property key */
    private static final String ASSOCEVENTS_CACHE_MAXAGE_PROPKEY = "eventAssociationCacheMaxage";
    private static final Duration DEFAULT_DURATION = Duration.ofSeconds(0L);
    private static final Map<Long, List<AssocEvent>> EMPTY_ASSOCIATED_EVENTS_MAP = Collections.emptyMap();
    private static final String ERROR_DESCRIPTION = "Error loading ASSOCEVENTS cache: ";
    static {
        _associatedEventsMap = EMPTY_ASSOCIATED_EVENTS_MAP;
        _loadTime = 0L;
        _maxAge = Long.MIN_VALUE;
    }

    synchronized static void clearAssociatedEvents() {
        clearAssociatedEvents(Jiggle.jiggle.getVerbose());
    }

    private synchronized static void clearAssociatedEvents(boolean verbose) {
        _loadTime = 0L;
        if (_associatedEventsMap != EMPTY_ASSOCIATED_EVENTS_MAP) {
            _associatedEventsMap = EMPTY_ASSOCIATED_EVENTS_MAP;
            if (verbose) {
                LogUtil.info("Cleared ASSOCEVENTS cache");
            }
        }
    }

    synchronized static Map<Long, List<AssocEvent>> getdAssociatedEvents() {
        // check if load is needed
        final long now = System.currentTimeMillis();
        // if previously loaded and not expired
        if (_loadTime != 0 && now - _loadTime <= getMaxAge()) {
            return _associatedEventsMap;
        }
        LogUtil.info("Loading ASSOCEVENTS cache");
        clearAssociatedEvents(false);
        int count = 0;
        Map<Long, List<AssocEvent>> associatedEventsMap = _associatedEventsMap;
        final Connection conn = DataSource.getConnection();
        final String sql = "SELECT * FROM ASSOCEVENTS";
        PreparedStatement ps = null;
        try {
            Long eventId;
            long commid;
            AssocEvent event;
            List<AssocEvent> list;
            ps = conn.prepareStatement(sql);
            try (final ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    if (associatedEventsMap == EMPTY_ASSOCIATED_EVENTS_MAP) {
                        associatedEventsMap = new HashMap<>();
                    }
                    do {
                        eventId = rs.getLong("evid");
                        event = new AssocEvent();
                        event.setMasterEventId(eventId);
                        event.setAssocEventId(rs.getLong("evidassoc"));
                        commid = rs.getLong("commid");
                        // if commid was null
                        if (commid == 0 && rs.wasNull()) {
                            commid = AssocEvent.DEFAULT_ID;
                        }
                        event.setCommentId(commid);
                        list = associatedEventsMap.get(eventId);
                        if (list == null) {
                            list = new ArrayList<>();
                            associatedEventsMap.put(eventId, list);
                        }
                        list.add(event);
                        count++;
                    } while (rs.next());
                }
            }
            _associatedEventsMap = associatedEventsMap;
            LogUtil.info("Loaded ASSOCEVENTS cache: " + count);
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(conn, ex, "Error EventAssocDbCache.getdAssociatedEvents()",
                    ERROR_DESCRIPTION);
        } catch (Exception ex) {
            JiggleExceptionHandler.logError(ex, ERROR_DESCRIPTION);
        } finally {
            // update load time even if error
            _loadTime = now;
            JasiDatabasePropertyList.debugSQL(sql, ps);
            JiggleDb.close(conn, ps);
        }
        return _associatedEventsMap;
    }

    /**
     * Get the maximum age for the cache.
     * 
     * @return the maximum age or 0 if disabled.
     */
    static synchronized long getMaxAge() {
        String s = null;
        if (_maxAge < 0L) {
            Duration duration = DEFAULT_DURATION;
            try {
                // get max age from environment variable
                s = System.getProperty(ASSOCEVENTS_CACHE_MAXAGE_PROPKEY);
                // if system property not found
                if (s == null || s.isEmpty()) {
                    // get max age from configuration file
                    Object value = JiggleSingleton.getInstance().getJiggleProp().get(ASSOCEVENTS_CACHE_MAXAGE_PROPKEY);
                    if (value != null) {
                        s = value.toString();
                    }
                }
                // if max age was specified
                if (s != null && !s.isEmpty()) {
                    duration = ParseString.parseDuration(s, duration);
                    LogUtil.info(ASSOCEVENTS_CACHE_MAXAGE_PROPKEY + " (" + s + "): " + duration);
                }
            } catch (Exception ex) {
                JiggleExceptionHandler.logError(ex, ASSOCEVENTS_CACHE_MAXAGE_PROPKEY + " (" + s + ")");
            } finally {
                _maxAge = duration.toMillis();
            }
        }
        return _maxAge;
    }
}
