package org.trinet.jiggle.database.event;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.trinet.jasi.JasiDatabasePropertyList;
import org.trinet.jiggle.common.AlertUtil;
import org.trinet.jiggle.common.JiggleExceptionHandler;
import org.trinet.jiggle.common.LogUtil;
import org.trinet.jiggle.common.type.AssocEvent;
import org.trinet.jiggle.database.JiggleDb;

public class EventAssocDb extends JiggleDb implements EventAssoc {
    /**
     * Get the event associated.
     * 
     * @return the event associated.
     */
    public static EventAssoc getEventAssoc() {
        return new EventAssocDb();
    }

    /**
     * Create the event associated.
     */
    private EventAssocDb() {
    }

    @Override
    public boolean deleteAssociatedEvent(long eventId) {
        int numRows = -1;
        final String sql = "DELETE FROM ASSOCEVENTS WHERE evid = ? ";
        PreparedStatement ps = null;
        try {
            ps = this.getConnection().prepareStatement(sql);
            ps.setLong(1, eventId);
            numRows = ps.executeUpdate();
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(this.getConnection(), ex, "Error deleteAssociatedEvent()",
                    "Error deleteAssociatedEvent: ");
        } finally {
            JasiDatabasePropertyList.debugSQL(sql, ps);
            close(ps);
        }
        return numRows > 0;
    }

    @Override
    public Set<Long> getAllEventId(long eventId) {
        Set<Long> result = new HashSet<>();
        final String sql = "SELECT * FROM ASSOCEVENTS WHERE (evid = ? OR evidassoc = ?) ";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            LogUtil.debug(JasiDatabasePropertyList.debugSQL, sql);

            ps = this.getConnection().prepareStatement(sql);
            ps.setLong(1, eventId);
            ps.setLong(2, eventId);
            rs = ps.executeQuery();

            while (rs != null && rs.next()) {
                long masterEventId = rs.getLong("evid");
                long assocEventId = rs.getLong("evidassoc");

                result.add(masterEventId);
                result.add(assocEventId);
            }
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(this.getConnection(), ex, "Error getAllEventId(eventiId)",
                    "Error getAllEventId(eventiId): ");
        } finally {
            JasiDatabasePropertyList.debugSQL(sql, ps);
            close(rs);
            close(ps);
        }
        return result;
    }

    @Override
    public List<AssocEvent> getAssociatedEvents(long eventId) {
        ArrayList<AssocEvent> result = new ArrayList<>();
        AssocEvent event;
        final String sql = "SELECT * FROM ASSOCEVENTS WHERE evid = ?";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = this.getConnection().prepareStatement(sql);
            ps.setLong(1, eventId);
            rs = ps.executeQuery();
            while (rs != null && rs.next()) {
                event = new AssocEvent();
                event.setMasterEventId(rs.getLong("evid"));
                event.setAssocEventId(rs.getLong("evidassoc"));
                event.setCommentId(rs.getLong("commid"));

                if (rs.wasNull()) {
                    event.setCommentId(AssocEvent.DEFAULT_ID);
                }

                result.add(event);
            }
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(this.getConnection(), ex, "Error getAssociatedEvents()",
                    "Error getting event association in getAssociatedEvents(): ");
        } finally {
            JasiDatabasePropertyList.debugSQL(sql, ps);
            close(ps);
        }
        return result;
    }

    @Override
    public AssocEvent getComment(long eventId) {
        AssocEvent result = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM ASSOCEVENTS WHERE evid = ? AND commid IS NOT NULL";

            LogUtil.debug(JasiDatabasePropertyList.debugSQL, sql);

            ps = this.getConnection().prepareStatement(sql);
            ps.setLong(1, eventId);
            rs = ps.executeQuery();

            if (rs != null && rs.next()) {
                result = new AssocEvent();
                long masterEventId = rs.getLong("evid");
                long assocEventId = rs.getLong("evidassoc");
                long commentId = rs.getLong("commid");

                // Get one row as they should share the same comment id
                result.setMasterEventId(masterEventId);
                result.setAssocEventId(assocEventId);
                result.setCommentId(commentId);
            }
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(this.getConnection(), ex, "Error getComment()",
                    "Error getComment(): ");
        } finally {
            close(rs);
            close(ps);
        }
        return result;
    }

    @Override
    public boolean insertAssociatedEvent(AssocEvent event) {
        int numRows = -1;
        final String sql = "INSERT INTO ASSOCEVENTS (evid, evidassoc, commid) VALUES (?, ?, ?)";
        PreparedStatement ps = null;
        try {
            ps = this.getConnection().prepareStatement(sql);
            ps.setLong(1, event.getMasterEventId());
            ps.setLong(2, event.getAssocEventId());

            if (event.getCommentId() == AssocEvent.DEFAULT_ID) {
                ps.setNull(3, java.sql.Types.BIGINT);
            } else {
                ps.setLong(3, event.getCommentId());
            }

            numRows = ps.executeUpdate();
            this.commit();
        } catch (SQLException ex) {
            if (ex.getMessage().toLowerCase().contains("unique_combinations")) {
                // Duplicate entry
                AlertUtil.displayError(
                        "Duplicate Association", "Association already exists between master event Id "
                                + event.getMasterEventId() + " and event id " + event.getAssocEventId(),
                        "Please remove the event and try again.");
            } else {
                JiggleExceptionHandler.handleDbException(this.getConnection(), ex, "Error insertAssociatedEvent()",
                        "Error insertAssociatedEvent: ");
            }
        } finally {
            JasiDatabasePropertyList.debugSQL(sql, ps);
            close(ps);
        }
        return numRows > 0;
    }

    @Override
    public AssocEvent isExist(long masterEventId, long assocEventId) {
        AssocEvent event = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM ASSOCEVENTS " + "WHERE (evid = ? AND evidassoc = ?) "
                    + " OR (evid = ? AND evidassoc = ?)";

            LogUtil.debug(JasiDatabasePropertyList.debugSQL, sql);

            // Check both combination as DB unique constraint uses greatest/least
            ps = this.getConnection().prepareStatement(sql);
            ps.setLong(1, masterEventId);
            ps.setLong(2, assocEventId);

            // switch order of event id
            ps.setLong(3, assocEventId);
            ps.setLong(4, masterEventId);
            rs = ps.executeQuery();

            while (rs != null && rs.next()) {
                event = new AssocEvent();
                event.setMasterEventId(rs.getLong("evid"));
                event.setAssocEventId(rs.getLong("evidassoc"));
                event.setCommentId(rs.getLong("commid"));

                if (rs.wasNull()) {
                    event.setCommentId(AssocEvent.DEFAULT_ID);
                }
            }
        } catch (SQLException ex) {
            JiggleExceptionHandler.handleDbException(this.getConnection(), ex, "Error isExist()",
                    "Error checking existing event in isExist(): ");
        } finally {
            close(rs);
            close(ps);
        }
        return event;
    }
}
