package org.trinet.jiggle.database.event;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.trinet.jiggle.common.type.AssocEvent;

public class EventAssocDbCache implements EventAssoc {
    /**
     * Get the event associated.
     * 
     * @return the event associated.
     */
    public static EventAssoc getEventAssoc() {
        return new EventAssocDbCache();
    }

    private EventAssoc _eventDb;

    private EventAssocDbCache() {
    }

    @Override
    public void close() {
        final EventAssoc eventDb = getEventDb(false);
        if (eventDb != null) {
            _eventDb = null;
            eventDb.close();
        }
        EventAssocCacheStore.clearAssociatedEvents();
    }

    @Override
    public void commit() {
        final EventAssoc eventDb = getEventDb(false);
        if (eventDb != null) {
            eventDb.commit();
            // database may have changed, clear cache
            EventAssocCacheStore.clearAssociatedEvents();
        }
    }

    @Override
    public boolean deleteAssociatedEvent(long eventId) {
        return getEventDb(true).deleteAssociatedEvent(eventId);
    }

    @Override
    public Set<Long> getAllEventId(long eventId) {
        long evid, evidassoc;
        Set<Long> result = Collections.emptySet();
        final Map<Long, List<AssocEvent>> associatedEventsMap = EventAssocCacheStore.getdAssociatedEvents();
        if (!associatedEventsMap.isEmpty()) {
            final Collection<List<AssocEvent>> allAssociatedEvents = associatedEventsMap.values();
            for (List<AssocEvent> associatedEvents : allAssociatedEvents) {
                for (AssocEvent event : associatedEvents) {
                    evid = event.getMasterEventId();
                    evidassoc = event.getAssocEventId();
                    if (evid == eventId || evidassoc == eventId) {
                        if (result.isEmpty()) {
                            result = new HashSet<>();
                        }
                        result.add(evid);
                        result.add(evidassoc);
                    }
                }
            }
        }
        return result;
    }

    @Override
    public List<AssocEvent> getAssociatedEvents(long eventId) {
        final Map<Long, List<AssocEvent>> associatedEventsMap = EventAssocCacheStore.getdAssociatedEvents();
        List<AssocEvent> associatedEvents = associatedEventsMap.get(eventId);
        if (associatedEvents == null) {
            associatedEvents = Collections.emptyList();
        }
        return associatedEvents;
    }

    @Override
    public AssocEvent getComment(long eventId) {
        final List<AssocEvent> associatedEvents = getAssociatedEvents(eventId);
        for (AssocEvent event : associatedEvents) {
            // if event has commid
            if (event.getCommentId() != AssocEvent.DEFAULT_ID) {
                return event;
            }
        }
        return null;
    }

    private EventAssoc getEventDb(boolean create) {
        if (create && _eventDb == null) {
            _eventDb = EventAssocDb.getEventAssoc();
        }
        return _eventDb;
    }

    @Override
    public boolean insertAssociatedEvent(AssocEvent event) {
        return getEventDb(true).insertAssociatedEvent(event);
    }

    @Override
    public AssocEvent isExist(long masterEventId, long assocEventId) {
        long evid, evidassoc;
        final Map<Long, List<AssocEvent>> associatedEventsMap = EventAssocCacheStore.getdAssociatedEvents();
        if (!associatedEventsMap.isEmpty()) {
            final Collection<List<AssocEvent>> allAssociatedEvents = associatedEventsMap.values();
            for (List<AssocEvent> associatedEvents : allAssociatedEvents) {
                for (AssocEvent event : associatedEvents) {
                    evid = event.getMasterEventId();
                    evidassoc = event.getAssocEventId();
                    if ((evid == masterEventId && evidassoc == assocEventId)
                            || (evid == assocEventId && evidassoc == masterEventId)) {
                        return event;
                    }
                }
            }
        }
        return null;
    }

    @Override
    public void rollback() {
        final EventAssoc eventDb = getEventDb(false);
        if (eventDb != null) {
            eventDb.rollback();
        }
    }
}
