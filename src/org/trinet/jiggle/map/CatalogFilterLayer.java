package org.trinet.jiggle.map;
//import com.bbn.openmap.layer;
//
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import com.bbn.openmap.dataAccess.shape.EsriShapeExport;
import com.bbn.openmap.event.*;
import com.bbn.openmap.layer.DrawingToolLayer;
import com.bbn.openmap.omGraphics.*;
import com.bbn.openmap.omGraphics.event.MapMouseInterpreter;
import com.bbn.openmap.util.*;
import com.bbn.openmap.proj.Length;
import com.bbn.openmap.proj.ProjMath;
import com.bbn.openmap.tools.drawing.DrawingTool;

import java.awt.*;
import java.awt.event.*;
import java.util.Properties;
import javax.swing.*;

import org.trinet.jiggle.Jiggle;
import org.trinet.jiggle.JiggleMapBean;
import org.trinet.jasi.EventSelectionProperties;
import org.trinet.jasi.TN.SolutionTN;
import org.trinet.jasi.DataSource;
import org.trinet.util.DoubleRange;
import org.trinet.util.StringList;
import org.trinet.util.gazetteer.LatLonZ;

/**
 * This layer can receive graphics from the OMDrawingToolLauncher, and
 * also sent it's graphics to the OMDrawingTool for editing.
 * <P>
 * 
 * The projectionChanged() and paint() methods are taken care of in
 * the OMGraphicHandlerLayer superclass.
 * <P>
 * 
 * This class responds to all the properties that the
 * OMGraphicHandlerLayer repsonds to, including the mouseModes
 * property. If the mouseModes property isn't set, the
 * SelectMouseMode.modeID mode ID is set. When the MapMouseInterpreter
 * calls select(OMGraphic), the OMGraphic is passed to the
 * DrawingTool. This class also responds to the showHints property
 * (true by default), which dictates if tooltips and information
 * delegator text is displayed when the layer's contents are moused
 * over.
 */
public class CatalogFilterLayer extends DrawingToolLayer implements PropertyChangeListener {
    protected Jiggle jiggle = null;
    protected GraphicAttributes defaultGraphicAttributes = new GraphicAttributes();
    public int maxHorNumLoaderButtons = -1;
    protected CatalogFilterLauncher launcher = null;
    private boolean querySave = false;

    public CatalogFilterLayer() {
        super();
        defaultGraphicAttributes.setRenderType(OMGraphic.RENDERTYPE_LATLON);
        defaultGraphicAttributes.setLineType(OMGraphic.LINETYPE_GREATCIRCLE);
        setList(new OMGraphicList());
        setAddToBeanContext(true);
        DTL_DEBUG = Debug.debugging("dtl");
    }

    /** * DrawingToolRequestor method.  */
    @Override
    public void drawingComplete(OMGraphic omg, OMAction action) {
        if (DTL_DEBUG) {
            String cname = (omg == null) ? "null" : omg.getClass().getName();
            int lastPeriod = cname.lastIndexOf('.');
            if (lastPeriod != -1) {
                cname = cname.substring(lastPeriod + 1);
            }
            Debug.output("DrawingToolLayer: DrawingTool complete for " + cname + " > " + action);
        }
        // First thing, release the proxy MapMouseMode, if there is one.
        releaseProxyMouseMode();
        if (omg != null) {
          // GRP, assuming that selection is off.
          OMGraphicList omgl = new OMGraphicList();
          omgl.add(omg);
          deselect(omgl);
        }
        OMGraphicList list = getList();
        if (list != null) {
            if (list.size() == 0) { // new list, empty
              if (omg != null) {
               // System.out.println("CFL: new list");
                doAction(omg, action);
              }
            }
            else { // have previous graphic in layerlist
              OMGraphic omgOld = list.getOMGraphicAt(0);
              if (omgOld instanceof OMGraphicList) { // assume POLYLIST 
               // System.out.println("CFL: have old polylist");
                if (omgOld != omg && (omg instanceof OMGraphicList)) { // not the same, so remove old, so we only have 1 
                  if (DTL_DEBUG) Debug.output("CatalogFilterLayer OMGraphicList doAction(omgOld, DELETE_GRAPHIC_MASK)");
                 // System.out.println("CFL: delete old polylist");
                  doAction(omgOld, new OMAction(OMGraphicConstants.DELETE_GRAPHIC_MASK));
                  if (omg != null) {
                     // System.out.println("CFL: add new polylist:" + action);
                      doAction(omg, action);
                  }
                }
                else if (EventSelectionProperties.REGION_POLYLIST.equals(jiggle.getEventProperties().getRegionType()) ) {
                 // System.out.println("CFL: add polygon to old polylist action:" + action);
                  // This bypasses FilterSupport, otherwise we must implement FS subclass, and create subclass upon init that handles this
                  Object name = omg.getAppObject();
                  if (name == null) {
                      // name the new "region" as a number, the list size
                      name = String.valueOf(((OMGraphicList)omgOld).size());
                      omg.setAppObject(name);
                  }
                  ((OMGraphicList) omgOld).doAction(omg, action);
                }
                else { // Properties are set for non-polylist region type
                 // System.out.println("CFL: delete old polylist");
                  if (DTL_DEBUG) Debug.output("CatalogFilterLayer OMGraphicList doAction(omgOld, DELETE_GRAPHIC_MASK)");
                  doAction(omgOld, new OMAction(OMGraphicConstants.DELETE_GRAPHIC_MASK));
                  if (omg != null) { // assume its a polygon inside old list 
                   // System.out.println("CFL: add omg:" + action);
                    doAction(omg, action);
                  }
                }
              }
              else { // Old graphic is not polylist type  (probably single OMPoly)
               // System.out.println("CFL: old region NOT a polylist");
                if (omgOld != omg) { // not the same, so remove old, so we only have 1 
                 // System.out.println("CFL: delete old region");
                  if (DTL_DEBUG) Debug.output("CatalogFilterLayer doAction(omgOld, DELETE_GRAPHIC_MASK)");
                  doAction(omgOld, new OMAction(OMGraphicConstants.DELETE_GRAPHIC_MASK));
                }
                if (omg != null) {
                   // System.out.println("CFL: add new omg");
                    doAction(omg, action);
                }
              }
            }
            doPrepare();
            //repaint();
        } else {
            Debug.error("Layer " + getName() + " received graphic: " + omg + ", and action: " + action +
                    ", with no list ready");
        }
        querySave = true;
    }

    @Override
    public OMGraphicList getList() {
        OMGraphicList omgl = super.getList();
        /*
        //System.out.println("DEBUG CatalogFilterLayer getList size: " +
               ((omgl == null) ? "NULL" :  String.valueOf(omgl.size())));
        if (omgl != null && omgl.size() > 0) {
            OMPoly omp = (OMPoly) omgl.getOMGraphicAt(0);
            StringBuffer sb = new StringBuffer(256);
            sb.append(omp.toString());
            double [] ll = omp.getLatLonArray();
            sb.append(" Pts: " + ll.length);
            for (int ii = 0; ii < ll.length; ii++) {
              sb.append(" ").append(ll[ii]);
            }
            sb.append(" RenType: " + omp.getRenderType());
            sb.append(" Width: " + ((BasicStroke)omp.getStroke()).getLineWidth());
            sb.append(" Color: " + omp.getLineColor().toString());
            //System.out.println(sb.toString());

        } 
        */
        return omgl;
    }

    @Override
    public void setProperties(String prefix, Properties props) {
        super.setProperties(prefix, props);
        // Use props to set default lineColor, width, and type of GraphicsAttributes
        // Note: input properties prefix can't end in "."
        defaultGraphicAttributes.setProperties(prefix, props);

    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getSource() instanceof JiggleMapBean) {
          String name = evt.getPropertyName();
          if (DTL_DEBUG) Debug.output(getClass().getName() + " DEBUG propertyChange name = " + name); 
          /*
          if ( name.equals(Jiggle.NEW_EVENTPROPS_PROPNAME) ) {
            setGraphicsFromProperties((EventSelectionProperties) evt.getNewValue());
          }
          else
          */
          if ( name.equals(Jiggle.NEW_CP_PROPNAME) ) {
            if (jiggle != null) setGraphicsFromProperties(jiggle.getEventProperties());
          }
        }
    }

    protected void setGraphicsFromProperties(EventSelectionProperties props) {
        if (DTL_DEBUG) Debug.output("DEBUG CatalogFilterLayer setGraphicsFromProperties");

        if (launcher != null) launcher.resetRegionGraphicButtons();

        String type = props.getRegionType();
        if (type == null) return;


        OMGraphic omg = null;
        if (type.equals(EventSelectionProperties.REGION_BOX)) {
                //System.out.println("CFL: BOX");
            DoubleRange minmaxLat = props.getRegionBoxLatRange();
            DoubleRange minmaxLon = props.getRegionBoxLonRange();
            // ? instead create Editable version with GraphicAttributes then set below as graphic object?
            if (minmaxLat != null && minmaxLon != null) {
                omg = new OMRect(
                          minmaxLat.getMaxValue(),
                          minmaxLon.getMinValue(),
                          minmaxLat.getMinValue(),
                          minmaxLon.getMaxValue(),
                          OMGraphicConstants.LINETYPE_GREATCIRCLE // or LINETYPE_STRAIGHT ?
                      );
            }
        }
        else if (type.equals(EventSelectionProperties.REGION_POINT)) {
                //System.out.println("CFL: POINT");
            LatLonZ p = props.getRegionRadiusPoint();
            if (p != null) {
                double radius = props.getRegionRadiusValue();
                if (radius > 0.) {
                    omg = new OMCircle(p.getLat(), p.getLon(), radius, Length.KM);
                }
            }
        }
        else if (type.equals(EventSelectionProperties.REGION_PLACE))  {
                //System.out.println("CFL: PLACE");
            String name = props.getRegionRadiusPlaceName();
            if (name != null && name.length() > 0) {
                LatLonZ p = SolutionTN.getLocationOfPlace(DataSource.getConnection(), name);
                if (p != null) {
                    double radius = props.getRegionRadiusValue();
                    if (radius > 0.) {
                        omg = new OMCircle(p.getLat(), p.getLon(), radius, Length.KM);
                    }
                }
            }
        }
        else if (type.equals(EventSelectionProperties.REGION_POLYGON))  {
                //System.out.println("CFL: POLYGON");
            //LatLonZ [] ll = props.getRegionPolygon();
            double [] ll = closeArray(props.getDoubleArray("regionPolygon"));
            if (ll != null) {
                omg = new OMPoly(
                          ll,
                          OMGraphicConstants.DECIMAL_DEGREES,
                          OMGraphicConstants.LINETYPE_GREATCIRCLE
                      );
            }
        }
        else if (type.equals(EventSelectionProperties.REGION_BORDER))  {
                //System.out.println("CFL: BORDER");
            String name = props.getRegionBorderPlaceName();
            double [] ll = closeArray(SolutionTN.getRegionByName(DataSource.getConnection(), name));
            if (ll != null) {
                omg = new OMPoly(
                          ll,
                          OMGraphicConstants.DECIMAL_DEGREES,
                          OMGraphicConstants.LINETYPE_GREATCIRCLE
                      );
            }
        }
        else if (type.equals(EventSelectionProperties.REGION_POLYLIST))  {
                //System.out.println("CFL: POLYLIST");
            EventSelectionProperties.EventRegion [] eregions = props.getEventRegions();
            if (eregions.length > 0) {
              omg = new OMGraphicList();
              OMGraphic omg2 = null;
              for (int idx=0; idx < eregions.length; idx++) {
                omg2 = null;
                double [] ll = eregions[idx].toLatLonArray();
                ll = closeArray(ll);
                if (ll != null) {
                  omg2 = new OMPoly(
                             ll,
                             OMGraphicConstants.DECIMAL_DEGREES,
                             OMGraphicConstants.LINETYPE_GREATCIRCLE
                         );
                  if (omg2 != null) {
                      omg2.setAppObject(eregions[idx].name);
                      defaultGraphicAttributes.setTo(omg2);
                      ((OMGraphicList)omg).add(omg2);
                  }
                }
              }
            }
            else omg = new OMGraphicList(); // try this as empty list to see what happens re filter
        }
        // null should force delete of prior graphic if any
        if (omg != null) defaultGraphicAttributes.setTo(omg);
        drawingComplete(omg, new OMAction(OMGraphicConstants.ADD_GRAPHIC_MASK));
    }

    private double [] closeArray(double [] ll) {
        if (ll == null) return null;

        int size  = ll.length;
        if (size < 3) {
            Debug.error("ERROR Map CatalogFilterLayer input regionPolygon has fewer than 3 vertices, check data!");
            return null;
        } 
        int lastIdx = size-1;
        // Check to see if first and last point are close, if not create new array and adding 1st pt to end.
        final double LATLON_RESOLUTION = .00001f;
        if ( (Math.abs(ll[0] - ll[lastIdx-1]) > LATLON_RESOLUTION) ||
             (Math.abs(ll[1]-ll[lastIdx]) > LATLON_RESOLUTION) ) {
            double [] ll_new = new double[size+2];
            System.arraycopy(ll, 0, ll_new, 0, size);
            ll_new[size] = ll[0];
            ll_new[size+1] = ll[1];
            ll = ll_new;
        }
        return ll;
    }

    @Override
    public void findAndInit(Object someObj) {
        if (someObj instanceof JiggleMapBean) {
            JiggleMapBean mapBean = (JiggleMapBean) someObj;
            mapBean.addPropertyChangeListener(this);
            this.jiggle = mapBean.jiggle;
            setGraphicsFromProperties(jiggle.getEventProperties());
        }
        else if (someObj instanceof CatalogFilterLauncher) {
            this.launcher = (CatalogFilterLauncher) someObj;
            defaultGraphicAttributes = launcher.getDefaultGraphicAttributes();
            if (this.jiggle != null) setGraphicsFromProperties(jiggle.getEventProperties());
        }
        super.findAndInit(someObj);
    }

    /**
     * BeanContextMembershipListener method. Called when a new object
     * is removed from the BeanContext of this object.
     */
    @Override
    public void findAndUndo(Object someObj) {
        if (someObj instanceof JiggleMapBean) {
            JiggleMapBean mapBean = (JiggleMapBean) someObj;
            mapBean.removePropertyChangeListener(this);
            setList(null);
            this.jiggle = null;
        }
        if (this.launcher == someObj) {
            this.launcher = null;
        }
        super.findAndUndo(someObj);
    }

    protected void setPropertiesFromGraphics() {
        if (DTL_DEBUG) Debug.output("DEBUG CatalogFilterLayer setPropertiesFromGraphics");
        OMGraphicList omgList = getList();
       // System.out.println("CFL setPropertiesFromGraphics size: " + omgList.size());

        EventSelectionProperties props = jiggle.getEventProperties();
        if (omgList == null || omgList.size() == 0) {
            if (DTL_DEBUG) Debug.output("CatalogFilterLayer has no Graphic to set EventSelectionProperties!");
            props.setRegionType(EventSelectionProperties.REGION_ANYWHERE);
        }
        else {
          if (omgList.size() > 1) {
            Debug.error("WARNING: CatalogFilterLayer has more than one selection Graphic!");
          }
          OMGraphic omg = omgList.getOMGraphicAt(0);
          //omg.generate(getProjection()); // ??

          if (DTL_DEBUG)
              Debug.output("DEBUG CatalogFilterLayer graphic: " + omgList.getOMGraphicAt(0).getClass().getName());

      if (! EventSelectionProperties.REGION_POLYLIST.equals(props.getRegionType()) ) {

          if (omg instanceof OMRect) {
            OMRect omr = (OMRect) omg;
            props.setRegionType(EventSelectionProperties.REGION_BOX);
            props.setRegionBoxLatRange(Math.min(omr.getSouthLat(), omr.getNorthLat()),Math.max(omr.getSouthLat(), omr.getNorthLat()));
            props.setRegionBoxLonRange(Math.min(omr.getWestLon(), omr.getEastLon()),Math.max(omr.getWestLon(), omr.getEastLon()));
          }
          else if (omg instanceof OMCircle) {
            OMCircle omc = (OMCircle) omg;
            props.setRegionType(EventSelectionProperties.REGION_POINT);
            double rDeg = omc.getRadius();
            LatLonPoint llp = LatLonPoint.valueOf(omc.getCenter());
            props.setRegionRadiusPoint(llp.getLatitude(), llp.getLongitude());
            props.setRegionRadiusValue(Length.KM.fromRadians(Length.DECIMAL_DEGREE.toRadians(rDeg)));
          }
          else if (omg instanceof OMPoly) {
            OMPoly omp = (OMPoly) omg;
            props.setRegionType(EventSelectionProperties.REGION_POLYGON);
            double [] latlon = omp.getLatLonArray(); // radians
            StringBuffer sb = new StringBuffer(256);
            for (int ii = 0; ii < latlon.length; ii ++) {
                sb.append(String.valueOf(ProjMath.radToDeg(latlon[ii]))).append(" ");
            }
            //System.out.println("DEBUG CatalogFilterLayer regionPolygon: " + sb.toString());
            props.setProperty("regionPolygon", sb.toString());
          }
      } else {
          if (omg instanceof OMGraphicList) {
            OMGraphicList ompList = (OMGraphicList) omg;
            OMPoly omp = null;
            props.setRegionType(EventSelectionProperties.REGION_POLYLIST);
            String name = null;
            StringList regionNameList = props.getRegionNameList();
           // System.out.println("CFL OMGraphicList ompList.size() : " + ompList.size());
            for (int idx=0; idx < ompList.size(); idx++) {
                omp = (OMPoly) ompList.getOMGraphicAt(idx);
                name = (String) omp.getAppObject();
                double [] latlon = omp.getLatLonArray(); // radians
                StringBuffer sb = new StringBuffer(256);
                for (int ii = 0; ii < latlon.length; ii ++) {
                    sb.append(String.valueOf(ProjMath.radToDeg(latlon[ii]))).append(" ");
                }
                if (DTL_DEBUG) 
                    Debug.output("DEBUG CatalogFilterLayer polyList region." +name+ "= " + sb.toString());
                props.setNamedRegionPolygon(name, sb.toString());
                if (! regionNameList.contains(name) ) regionNameList.add(name);
            }
            props.setRegionNameList(regionNameList.toArray());
          }
      }
          if (DTL_DEBUG) {
              Debug.output("DEBUG CatalogFilterLayer jiggle.eventProps after update ...");
              props.dumpProperties(); 
              Debug.output("DEBUG CatalogFilterLayer end of properties dump");
          }
        }    

        if (querySave) {

            int yn = JOptionPane.showConfirmDialog(
                       null, "Save edited region properties to user's directory file?",
                       "Save Region Catalog Filter Properties",
                       JOptionPane.YES_NO_OPTION);

            if (yn == JOptionPane.YES_OPTION) {
                String str = JOptionPane.showInputDialog("Save to filename: ", props.getFilename());
                if (str != null && str.length() > 0) {
                    props.setFilename(str);
                    props.saveProperties();
                    querySave = false;
                }
            }

        }

    }

    protected void resetCatalog() {
        setPropertiesFromGraphics();
        //prepare();
        jiggle.resetCatPanel(true);
    }

    /**
     * Query for what text should be placed over the information bar
     * when the mouse is over a particular OMGraphic.
     */
    @Override
    public String getInfoText(OMGraphic omg) {
        Object obj = omg.getAppObject(); 
        if (obj == null) return "";
        DrawingTool dt = getDrawingTool();
        if (dt != null && dt.canEdit(omg.getClass())) {
            return "Click to edit " + obj.toString();
        } else {
            return obj.toString();
        }
    }

    /**
     * Query for what text should be placed over the information bar
     * when the mouse is over a particular OMGraphic.
     */
    @Override
    public String getToolTipTextFor(OMGraphic omg) {
        Object obj = omg.getAppObject(); 
        if (obj == null) return "";
        DrawingTool dt = getDrawingTool();
        if (dt != null && dt.canEdit(omg.getClass())) {
            return "Click to edit " + obj.toString();
        } else {
            return obj.toString();
        }
    }


}

