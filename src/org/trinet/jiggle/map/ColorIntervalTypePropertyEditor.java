package org.trinet.jiggle.map;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyEditorSupport;

import javax.swing.ButtonModel;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class ColorIntervalTypePropertyEditor extends PropertyEditorSupport implements
        ActionListener, FocusListener {

    public final static String UNIFORM = "uniform";
    public final static String DEPTH   = "depth";
    public final static String TIME    = "time";
    public final static String TYPE    = "type";
    public final static String GTYPE   = "gtype";

    protected ButtonGroup buttonGroup = new ButtonGroup();
    protected JRadioButton uniformButton;
    protected JRadioButton depthButton;
    protected JRadioButton timeButton;
    protected JRadioButton typeButton;
    protected JRadioButton gtypeButton;

    protected String selection = "";

    public ColorIntervalTypePropertyEditor() {
        uniformButton = new JRadioButton(UNIFORM);
        depthButton = new JRadioButton(DEPTH);
        timeButton = new JRadioButton(TIME);
        typeButton = new JRadioButton(TYPE);
        gtypeButton = new JRadioButton(GTYPE);
    }

    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    /** Returns the editor GUI, ie a JTextField. */
    @Override
    public Component getCustomEditor() {
        JPanel panel = new JPanel();

        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        panel.setLayout(gridbag);

        uniformButton.setActionCommand(UNIFORM);
        depthButton.setActionCommand(DEPTH);
        timeButton.setActionCommand(TIME);
        typeButton.setActionCommand(TYPE);
        gtypeButton.setActionCommand(GTYPE);

        uniformButton.addActionListener(this);
        depthButton.addActionListener(this);
        timeButton.addActionListener(this);
        typeButton.addActionListener(this);
        gtypeButton.addActionListener(this);

        buttonGroup.add(uniformButton);
        buttonGroup.add(depthButton);
        buttonGroup.add(timeButton);
        buttonGroup.add(typeButton);
        buttonGroup.add(gtypeButton);

        gridbag.setConstraints(uniformButton, c);
        gridbag.setConstraints(depthButton, c);
        gridbag.setConstraints(timeButton, c);
        gridbag.setConstraints(typeButton, c);
        gridbag.setConstraints(gtypeButton, c);

        panel.add(uniformButton);
        panel.add(depthButton);
        panel.add(timeButton);
        panel.add(typeButton);
        panel.add(gtypeButton);

        return panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        setSelection(e.getActionCommand());
        //System.out.println("value changed");
        firePropertyChange();
    }

    @Override
    public void focusGained(FocusEvent e) {}

    @Override
    public void focusLost(FocusEvent e) {
        firePropertyChange();
    }

    protected void setSelection(String str) {
        if (UNIFORM.equalsIgnoreCase(str)) {
            selection = UNIFORM;
        }
        else if (DEPTH.equalsIgnoreCase(str)) {
            selection = DEPTH;
        }
        else if (TIME.equalsIgnoreCase(str)) {
            selection = TIME;
        }
        else if (TYPE.equalsIgnoreCase(str)) {
            selection = TYPE;
        }
        else if (GTYPE.equalsIgnoreCase(str)) {
            selection = GTYPE;
        }
    }

    protected void setButtonSelection(String str) {
        if (UNIFORM.equalsIgnoreCase(str)) {
            uniformButton.setSelected(true);
        }
        else if (DEPTH.equalsIgnoreCase(str)) {
            depthButton.setSelected(true);
        }
        else if (TIME.equalsIgnoreCase(str)) {
            timeButton.setSelected(true);
        }
        else if (TYPE.equalsIgnoreCase(str)) {
            typeButton.setSelected(true);
        }
        else if (GTYPE.equalsIgnoreCase(str)) {
            gtypeButton.setSelected(true);
        }
    }

    /** Sets String in JTextField. */
    @Override
    public void setValue(Object string) {
        if (!(string instanceof String)) return;
        setButtonSelection((String) string);
    }

    /** Returns String from ButtonGroup. */
    @Override
    public String getAsText() {
        ButtonModel bm = buttonGroup.getSelection();
        return (bm == null) ? "" : bm.getActionCommand();
    }
}
