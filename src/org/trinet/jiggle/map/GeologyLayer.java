package org.trinet.jiggle.map;

import java.awt.Color;
import java.util.Properties;
import java.util.Iterator;
import java.util.ArrayList;

import com.bbn.openmap.dataAccess.shape.EsriGraphicList;
import com.bbn.openmap.dataAccess.shape.DbfTableModel;
import com.bbn.openmap.omGraphics.OMColor;
import com.bbn.openmap.omGraphics.OMPoly;
import com.bbn.openmap.omGraphics.OMGraphic;
import com.bbn.openmap.plugin.esri.EsriLayer;
import com.bbn.openmap.util.PropUtils;
import com.bbn.openmap.util.ColorFactory;

/**
 */
public class GeologyLayer extends EsriLayer {

  int rCol = 255;
  int bCol = 255;
  int gCol = 255;
  int infoCol = -1; // col = 4

  //int count = 0;

  public GeologyLayer() { }

  public Properties getProperties(Properties props) {
        props = super.getProperties(props);
        return getProperties(propertyPrefix, props);
  }

  public Properties getProperties(String prefix, Properties props) {
      props = super.getProperties(props);
      prefix = PropUtils.getScopedPropertyPrefix(prefix);
      return props;
  }

  @Override
  public Properties getPropertyInfo(Properties list) {
    Properties props = super.getPropertyInfo(list);
    return props;
  }

  @Override
  public void setProperties(String prefix, Properties properties) {

    super.setProperties(prefix, properties);
    
    prefix = PropUtils.getScopedPropertyPrefix(prefix);

    //setup the colors
    final DbfTableModel model = getModel();
    if (model != null)  {
      final int columnCount = model.getColumnCount();
      //determine the columns for the rgb value
      String columnName = null;
      for (int column = 0; column < columnCount; column++) {
        columnName = model.getColumnName(column);
        if (columnName.equals("RED")) rCol = column;
        if (columnName.equals("GREEN")) gCol = column;
        if (columnName.equals("BLUE")) bCol = column;
        if (columnName.equals("GEOLOGY")) infoCol = column;
      }
      setColors(getEsriGraphicList());
    }
  }
  private void setColor(OMGraphic omg) {
      //get the application object which should be the record of dbf file (1..N)
      //count++;
      Object appObj = omg.getAppObject();
      Color c = OMColor.clear;
      if (appObj instanceof ArrayList) {
        ArrayList row = (ArrayList) appObj;
        c = ColorFactory.createColor(
            ((Number) row.get(rCol)).intValue(),
            ((Number) row.get(gCol)).intValue(),
            ((Number) row.get(bCol)).intValue(),
            255,
            false 
        );
        omg.setLinePaint(c);
        omg.setFillPaint(c);
      }
      //else System.out.println("DEBUG GeologyLayer appObject not arrayList at: " + count); 
  }
  private void setColor(EsriGraphicList list) {
      Object appObj = list.getAppObject();
      Color c = OMColor.clear;
      if (appObj instanceof ArrayList) {
        ArrayList row = (ArrayList) appObj;
        c = ColorFactory.createColor(
            ((Number) row.get(rCol)).intValue(),
            ((Number) row.get(gCol)).intValue(),
            ((Number) row.get(bCol)).intValue(),
            255,
            false 
        );
      }
      Iterator it = list.iterator();
      while (it.hasNext()) {
        final Object element = it.next();
        if (element instanceof OMPoly) {
          OMPoly omg = (OMPoly) element;
          omg.setLinePaint(c);
          omg.setFillPaint(c);
          omg.setAppObject(appObj);
        }
      }
}

  /**
   * Sets the colors for the layer graphics.
  */
  private void setColors(EsriGraphicList listObj) {
      if (listObj == null) return;
      //given list object is OK reverse the traverse mode
      Iterator it = listObj.iterator();
      while (it.hasNext()) {
        final Object element = it.next();
        if (element instanceof EsriGraphicList) {
          //System.out.println("DEBUG GeologyLayer new EsriGraphicList at count: " + count); 
          //Object obj = ((EsriGraphicList)element).getAppObject(); 
          //System.out.println("DEBUG GeologyLayer appObj: " + ((obj == null) ? "NULL" : obj.getClass().getName()) );
          //setColors((EsriGraphicList)element);
          
          setColor((EsriGraphicList)element);
        }
        else if (element instanceof OMPoly) {
          setColor((OMPoly) element);
        }
      }
      //System.out.println("DEBUG GeologyLayer setColor count = " + count); 
    }

  @Override
    public String getInfoText(OMGraphic omg) {
      if (infoCol < 0) return null;
      int index = getEsriGraphicList().indexOf(omg);
      if (index >= 0) return (String) getModel().getValueAt(index,infoCol);
      /*
      Object appObj = omg.getAppObject();
      if (appObj instanceof ArrayList) {
        ArrayList row = (ArrayList) appObj;
        if (infoCol >= 0 && row.size() > infoCol)
            return (String) row.get(infoCol);
      }
      */
      return null;
    }

  @Override
    public String getToolTipTextFor(OMGraphic omg) {
      if (infoCol < 0) return null;
      int index = getEsriGraphicList().indexOf(omg);
      if (index >= 0) return (String) getModel().getValueAt(index,infoCol);
      /*
      Object appObj = omg.getAppObject();
      if (appObj instanceof ArrayList) {
        ArrayList row = (ArrayList) appObj;
        if (infoCol >= 0 && row.size() > infoCol)
            return (String) row.get(infoCol);
      }
      */
      return null;
    }

 
}
