package org.trinet.jiggle.map;

import com.bbn.openmap.layer.OMGraphicHandlerLayer;
import com.bbn.openmap.omGraphics.OMGraphicList;

public abstract class JiggleGraphicHandlerLayer extends OMGraphicHandlerLayer {
  private static final long serialVersionUID = 1L;

  @Override
  public final synchronized OMGraphicList prepare() {
    OMGraphicList omgList = getList();
    if (omgList == null || omgList.isEmpty()) {
      if (omgList == null) {
        omgList = new OMGraphicList();
      }
      prepare(omgList);
    }
    return super.prepare();
  }

  /**
   * Prepare the graphics.
   * 
   * @param omgList the graphics list.
   * @return the graphics list.
   */
  protected void prepare(OMGraphicList omgList) {
    setList(generateGraphics(omgList));
  }   

  /**
   * Generates the graphics.
   * 
   * @param omgList the graphics list.
   * @return the graphics list.
   */
  protected abstract OMGraphicList generateGraphics(OMGraphicList omgList);
}
