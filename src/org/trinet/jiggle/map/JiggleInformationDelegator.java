package org.trinet.jiggle.map;

import com.bbn.openmap.InformationDelegator;
import com.bbn.openmap.event.InfoDisplayEvent;

public class JiggleInformationDelegator extends InformationDelegator {

    public JiggleInformationDelegator() {
        super();
    }

    /**
     * Display a line of text in a designated info line.
     */
    @Override
    public void displayInfoLine(String infoLine, int labelDesignator) {
        if (infoLineHolder != null) {

            int idx = infoLine.indexOf(" - x, y (");
            if (idx < 0) idx = infoLine.length();

            setLabel(
                (infoLine != null && infoLine.length() > 0) ?
                      infoLine.substring(0, idx) : " ", labelDesignator
            );
        }
    }

}

