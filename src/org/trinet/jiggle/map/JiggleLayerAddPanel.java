package org.trinet.jiggle.map;

import java.awt.event.ActionEvent;
import java.util.Properties;

import com.bbn.openmap.PropertyConsumer;
import com.bbn.openmap.util.propertyEditor.Inspector;

public class JiggleLayerAddPanel extends com.bbn.openmap.gui.LayerAddPanel {
    private static final long serialVersionUID = 1L;

    /**
     * Initialize the properties to use this class instead of
     * {@link com.bbn.openmap.gui.LayerAddPanel}.
     * 
     * @param properties the properties.
     */
    public static void init(Properties properties) {
        String classProperty = "addlayer.class";
        String className = properties.getProperty(classProperty);
        if (className == null || com.bbn.openmap.gui.LayerAddPanel.class.getName().equals(className)) {
            className = JiggleLayerAddPanel.class.getName();
            properties.setProperty(classProperty, className);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand() == Inspector.cancelCommand) {
            if (layer instanceof PropertyConsumer && propertyHandler != null) {
                final String prefix = ((PropertyConsumer) layer).getPropertyPrefix();
                if (prefix != null) {
                    propertyHandler.removeUsedPrefix(prefix);
                }
            }
            return;
        }
        super.actionPerformed(e);
    }
}
