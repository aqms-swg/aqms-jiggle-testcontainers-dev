package org.trinet.jiggle.map;

import com.bbn.openmap.omGraphics.*;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
//import com.bbn.openmap.util.Debug;

/**
 * The JiggleSymbolText graphic type lets you put text on the screen. The
 * location of the string is really the location of the lower left
 * corner of the first letter of the string.
 */
public class JiggleSymbolText extends com.bbn.openmap.omGraphics.OMText {


    /**
     * Default constructor. Produces an instance with no location and
     * an empty string for text. For this instance to be useful it
     * needs text (setData), a location (setX, setY, setLat, setLon)
     * and a renderType (setRenderType).
     */
    public JiggleSymbolText() {
        super();
    }

    /**
     * Creates a text object, with Lat/Lon placement, and default
     * SansSerif font.
     * 
     * @param lt latitude of the string, in decimal degrees.
     * @param ln longitude of the string, in decimal degrees.
     * @param stuff the string to be displayed.
     * @param just the justification of the string
     */
    public JiggleSymbolText(float lt, float ln, String stuff, int just) {
        super(lt, ln, stuff, just);
    }

    /**
     * Creates a text object, with Lat/Lon placement.
     * 
     * @param lt latitude of the string, in decimal degrees.
     * @param ln longitude of the string, in decimal degrees.
     * @param stuff the string to be displayed.
     * @param font the Font description for the string.
     * @param just the justification of the string
     */
    public JiggleSymbolText(float lt, float ln, String stuff, Font font, int just) {
        super(lt, ln, stuff, font, just);
    }

    /**
     * Creates a text object, with XY placement, and default SansSerif
     * font.
     * 
     * @param px1 horizontal window pixel location of the string.
     * @param py1 vertical window pixel location of the string.
     * @param stuff the string to be displayed.
     * @param just the justification of the string
     */
    public JiggleSymbolText(int px1, int py1, String stuff, int just) {
        super(px1, py1, stuff, just);
    }

    /**
     * Creates a text object, with XY placement.
     * 
     * @param px1 horizontal window pixel location of the string.
     * @param py1 vertical window pixel location of the string.
     * @param stuff the string to be displayed.
     * @param font the Font description for the string.
     * @param just the justification of the string
     */
    public JiggleSymbolText(int px1, int py1, String stuff, Font font, int just) {
        super(px1, py1, stuff, font, just);
    }

    /**
     * Creates a Text object, with lat/lon placement with XY offset,
     * and default SansSerif font.
     * 
     * @param lt latitude of the string, in decimal degrees.
     * @param ln longitude of the string, in decimal degrees.
     * @param offX horizontal offset of string
     * @param offY vertical offset of string
     * @param aString the string to be displayed.
     * @param just the justification of the string
     */
    public JiggleSymbolText(float lt, float ln, int offX, int offY, String aString, int just) {
        super(lt, ln, offX, offY, aString, just);
    }

    /**
     * Creates a Text object, with lat/lon placement with XY offset.
     * 
     * @param lt latitude of the string, in decimal degrees.
     * @param ln longitude of the string, in decimal degrees.
     * @param offX horizontal offset of string
     * @param offY vertical offset of string
     * @param aString the string to be displayed.
     * @param font the Font description for the string.
     * @param just the justification of the string
     */
    public JiggleSymbolText(float lt, float ln, int offX, int offY, String aString, Font font, int just) {
        super(lt, ln, offX, offY, aString, font, just);
    }

    @Override
    public synchronized void render(Graphics g) {
        // copy the graphic, so our transform doesn't cascade to
        // others...
        g = g.create();

        if (getNeedToRegenerate() || pt == null || !isVisible())
            return;

        g.setFont(getFont());
        setGraphicsForEdge(g);

        if (fm == null) {
            fm = g.getFontMetrics();
        }

        computeBounds(null);

        // If there is a rotation angle, the shape will be calculated
        // for that rotation. Don't need to rotate the Graphics for
        // the shape.

        if (shouldRenderFill()) {
            setGraphicsForFill(g);
            fill(g);

            if (textureMask != null && textureMask != fillPaint) {
                setGraphicsColor(g, textureMask);
                fill(g);
            }
        }

        if (isSelected()) {
            setGraphicsColor(g, getSelectPaint());
            draw(g);
        } else if (isMatted()) {
            setGraphicsColor(g, getMattingPaint());
            draw(g);
        }

        // to use later to unset the transform, if used.
        double rx = 0.0;
        // double ry = 0.0;
        double rw = 0.0;
        double rh = 0.0;
        double woffset = 0.0;
        int hoffset = 0;

        if (g instanceof Graphics2D && rotationAngle != DEFAULT_ROTATIONANGLE) {

            Rectangle rect = polyBounds.getBounds();

            rx = rect.getX();
            // ry = rect.getY();
            rw = rect.getWidth();
            rh = rect.getHeight();
            woffset = 0.0;
            hoffset = (int) rh/2;

            switch (justify) {
            case JUSTIFY_LEFT:
                // woffset = 0.0;
                break;
            case JUSTIFY_CENTER:
                woffset = rw / 2;
                break;
            case JUSTIFY_RIGHT:
                woffset = rw;
            }
            //rotate about our text anchor point
            ((Graphics2D) g).rotate(rotationAngle, rx + woffset, point.y + hoffset);
        }

        setGraphicsForEdge(g);

        int height;
        if (fmHeight == HEIGHT) {
            height = fm.getHeight();
        } else if (fmHeight == ASCENT_LEADING) {
            height = fm.getHeight() - fm.getDescent();
        } else if (fmHeight == ASCENT_DESCENT) {
            height = fm.getAscent() + fm.getDescent();
        } else {
            height = fm.getAscent();
        }
        hoffset = height/2;

        int baselineLocation = point.y; // baseline == BASELINE_BOTTOM,
                                     // normal.

        if (baseline == BASELINE_MIDDLE) {
            baselineLocation += (fm.getAscent() - fm.getDescent()) / 2;
        } else if (baseline == BASELINE_TOP) {
            baselineLocation += (fm.getAscent() - fm.getDescent());
        }

        switch (justify) {
        case JUSTIFY_LEFT:
            // Easy case, just draw them.
            for (int i = 0; i < parsedData.length; i++) {
                g.drawString(parsedData[i], point.x, baselineLocation
                        + (height * i) + hoffset);
            }
            break;
        case JUSTIFY_CENTER:
            computeStringWidths(fm);
            for (int i = 0; i < parsedData.length; i++) {
                g.drawString(parsedData[i],
                        point.x - (widths[i] / 2),
                        baselineLocation + (height * i) + hoffset);
            }
            break;
        case JUSTIFY_RIGHT:
            computeStringWidths(fm);
            for (int i = 0; i < parsedData.length; i++) {
                g.drawString(parsedData[i], point.x - widths[i], baselineLocation
                        + (height * i) + hoffset);
            }
            break;
        }
    }

}
