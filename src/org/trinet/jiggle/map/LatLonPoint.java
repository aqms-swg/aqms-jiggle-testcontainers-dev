package org.trinet.jiggle.map;

import java.awt.geom.Point2D;

/**
 * Encapsulates latitude and longitude coordinates.
 */
public class LatLonPoint extends Point2D {
  /**
   * Get the latitude and longitude coordinates.
   * 
   * @param x the x / longitude.
   * @param y the y / latitude.
   * @return the latitude and longitude coordinates.
   */
  public static LatLonPoint valueOf(double x, double y) {
    final LatLonPoint latLonPoint = new LatLonPoint();
    latLonPoint.setLocation(x, y);
    return latLonPoint;
  }

  /**
   * Get the latitude and longitude coordinates.
   * 
   * @param p the point.
   * @return the latitude and longitude coordinates.
   */
  public static LatLonPoint valueOf(final Point2D p) {
    final LatLonPoint latLonPoint = new LatLonPoint();
    latLonPoint.setLocation(p.getX(), p.getY());
    return latLonPoint;
  }

  /**
   * The X coordinate of this {@code Point2D}.
   * 
   * @since 1.2
   * @serial
   */
  public double x;

  /**
   * The Y coordinate of this {@code Point2D}.
   * 
   * @since 1.2
   * @serial
   */
  public double y;

  /**
   * Private, use <code>valueOf</code> method.
   */
  private LatLonPoint() {
  }

  /**
   * Get the latitude.
   * 
   * @return the latitude.
   */
  public double getLatitude() {
    return getY();
  }

  /**
   * Get the longitude.
   * 
   * @return the longitude.
   */
  public double getLongitude() {
    return getX();
  }

  @Override
  public double getX() {
    return x;
  }

  @Override
  public double getY() {
    return y;
  }

  @Override
  public void setLocation(double x, double y) {
    this.x = x;
    this.y = y;
  }
}
