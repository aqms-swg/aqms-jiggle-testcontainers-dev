package org.trinet.jiggle.map;

import java.util.Iterator;

import com.bbn.openmap.Environment;
import com.bbn.openmap.MapHandler;
import com.bbn.openmap.omGraphics.OMGraphic;
import com.bbn.openmap.omGraphics.OMGraphicList;
import com.bbn.openmap.proj.Projection;
import com.bbn.openmap.proj.ProjectionFactory;
import com.bbn.openmap.util.Debug;

public class OMUtils {
  private static ProjectionFactory _projectionFactory;

  /**
   * Get the graphic with the specified application object.
   * <p>
   * This logic was previously in <code>OMGraphicList</code>.
   * 
   * @param graphicList the graphic list.
   * @param obj         the application object or null if none.
   * @return the graphic or null if none.
   */
  public static OMGraphic getOMGraphicWithAppObject(OMGraphicList graphicList, Object obj) {
    if (obj == null) {
      return null;
    }
    Object graphicObj;
    OMGraphic graphic;
    final Iterator<?> it = graphicList.iterator();
    while (it.hasNext()) {
      graphicObj = it.next();
      if (!(graphicObj instanceof OMGraphic)) {
        continue;
      }
      graphic = (OMGraphic) graphicObj;
      graphicObj = graphic.getAppObject();
      if (obj == graphicObj || obj.equals(graphicObj)) {
        return graphic;
      }
      // For this object, if it is an OMGraphicList and is not vague, check its
      // OMGraphics for their application objects, too.
      if (graphic instanceof OMGraphicList && !((OMGraphicList) graphic).isVague()) {
        graphic = getOMGraphicWithAppObject((OMGraphicList) graphic, obj);
        if (graphic != null) {
          return graphic;
        }
      }
    }
    return null;
  }

  /**
   * Get the projection.
   * 
   * @param mh the map handler.
   * @return the projection.
   */
  public static Projection getProjection(MapHandler mh) {
    return getProjectionFactory(mh).getDefaultProjectionFromEnvironment(Environment.getInstance());
  }

  /**
   * Get the projection factory.
   * 
   * @param mh the map handler.
   * @return the projection factory.
   */
  private static ProjectionFactory getProjectionFactory(MapHandler mh) {
    ProjectionFactory projectionFactory = _projectionFactory;
    if (projectionFactory == null) {
      // Check MapHandler to see if a ProjectionFactory has been added.
      // If it hasn't, create one with the default ProjectionLoaders.
      // We might want to remove this at some point, but not having it here
      // will catch some people by surprise when 4.6.1 comes out.
      Object obj = mh.get(ProjectionFactory.class);
      if (obj instanceof ProjectionFactory) {
        projectionFactory = (ProjectionFactory) obj;
      } else {
        Debug.message("mappanel",
            "BasicMapPanel adding ProjectionFactory and projections to MapHandler since there are none to be found.");
        projectionFactory = ProjectionFactory.loadDefaultProjections();
        mh.add(projectionFactory);
      }
      _projectionFactory = projectionFactory;
    }
    return projectionFactory;
  }
}
