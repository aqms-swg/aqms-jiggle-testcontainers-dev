package org.trinet.jiggle.map;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentAdapter;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;

import com.bbn.openmap.gui.GoToMenu;
import com.bbn.openmap.MapBean;

import org.trinet.util.gazetteer.LatLonZ;
import org.trinet.jasi.Solution;

public class SelectedEventGoToMenu extends GoToMenu {

    JMenuItem masterMenuItem = null;
    JMenuItem catalogMenuItem = null;
    JMenu selectMenu = null;

    public SelectedEventGoToMenu() {
        super();
        addSelectMenu();

    }

    private void addSelectMenu() {
        add(new JSeparator());
        selectMenu = new JMenu("Selected Event...");
        selectMenu.setEnabled(false);
        add(selectMenu);
    }

    @Override
    public void findAndUndo(Object someObj) {
        super.findAndUndo(someObj);
        if (someObj instanceof MasterViewLayer) {
            if (masterMenuItem != null) selectMenu.remove(masterMenuItem);
            masterMenuItem = null;
        }
        else if (someObj instanceof CatalogLayer) {
            if (catalogMenuItem != null) selectMenu.remove(catalogMenuItem);
            catalogMenuItem = null;
        }
        selectMenu.setEnabled((selectMenu.getItemCount() > 0));
    }

    @Override
    public void findAndInit(Object someObj) {
        super.findAndInit(someObj);
        if (someObj instanceof CatalogLayer) {
            addListSelectedEventMenuItem("Catalog", (SolutionListLayer) someObj);
            selectMenu.setEnabled(true);
        }
        else if (someObj instanceof MasterViewLayer) {
            addListSelectedEventMenuItem("Jiggle", (SolutionListLayer) someObj);
            selectMenu.setEnabled(true);
        }
        selectMenu.setEnabled((selectMenu.getItemCount() > 0));
    }

    private void addListSelectedEventMenuItem(String name, SolutionListLayer ssl) {
        final JMenuItem jmi = new JMenuItem(name);
        jmi.addActionListener(new SelectedEventActionListener(ssl));
        ssl.addComponentListener(
          new ComponentAdapter() {
            public void componentHidden(ComponentEvent e) {
                jmi.setEnabled(false);
            }
            public void componentShown(ComponentEvent e) {
                jmi.setEnabled(true);
            }
          }
        );
        selectMenu.add(jmi);
    }

    public class SelectedEventActionListener implements ActionListener {
        SolutionListLayer slLayer = null;

        public SelectedEventActionListener(SolutionListLayer slLayer ) {
            this.slLayer = slLayer;
        }

        @Override
        public void actionPerformed(ActionEvent ae) {
            Solution sol = ((Solution)slLayer.getSolutionList().getSelected());
            if (sol != null) {
               LatLonZ llz = sol.getLatLonZ();
               MapBean map = getMap();
               if (map != null)
                   map.setCenter((float)llz.getLat(), (float)llz.getLon());
            }
            else {
              JOptionPane.showMessageDialog(null, "No event is selected yet, check gui.",
                      "Recenter Map View", JOptionPane.ERROR_MESSAGE); 
            }
        }
    }

}
