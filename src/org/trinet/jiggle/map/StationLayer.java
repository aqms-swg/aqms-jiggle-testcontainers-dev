//Station list should not include NR, RB, LI nets. 
//We should not be loading channel info for VH_, UM_, LL_ etc.

package org.trinet.jiggle.map;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.*;
import java.util.*;
import java.sql.*;

import javax.swing.AbstractAction;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JMenuItem;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JSeparator;

import com.bbn.openmap.MapHandler;
import com.bbn.openmap.Environment;
import com.bbn.openmap.I18n;
import com.bbn.openmap.event.*;
import com.bbn.openmap.layer.*;
import com.bbn.openmap.layer.location.*;
import com.bbn.openmap.omGraphics.*;
import com.bbn.openmap.proj.Projection;
import com.bbn.openmap.util.*;

import org.trinet.jiggle.*;
import org.trinet.jasi.*;
import org.trinet.util.*;

/**
 * Displays Stations in a ChannelList.
 */
public class StationLayer extends JiggleGraphicHandlerLayer implements 
        MapMouseListener, ListDataStateListener, PropertyChangeListener {

    private static String STATION_DATA_TABLE = "JASI_STATION_VIEW";
    //static {
        //STATION_DATA_TABLE = (EnvironmentInfo.getNetworkCode().equals("CI")) ? "SIS_STATION_DATA" : "STATION_DATA";
    //}
    //
    // Create separate ReadingsLayer for MasterView currentSolution readings info
    // ReadingsLayer extends Station Layer and its findAndInit needs to add it as
    // PropertyChangeListner to MasterViewLayer to currentSolution update changes.
    //
    private Jiggle jiggle = null;
    private Waveform [] wfa = new Waveform [0];

    private class StnHistory {
        String sta = null;
        String net = null;
        DateRange dr = null;

        public boolean equals(Object o) {
            if ( !(o instanceof StnHistory) ) return false;
            if (o == this) return true;
            StnHistory sh = (StnHistory) o;
            return sta.equals(sh.sta) && net.equals(sh.net) && dr.equals(sh.dr);
        }

        public int hashCode() {
            return getMapKey().hashCode();
        }

        public Object getMapKey() {
            return (net+sta);
        }
    }
    HashMap stnHistory = null; // container for StnHistory objects initialized from db connection



    // currently selected solution in MasterView solution List
    private Solution currentSol = null;
    private java.util.Date currentSolDate = null;

    public final static String STATION_SIZE_PROPERTY = "stationSize";
    public final static String CACHE_FILENAME_PROPERTY = "channelListCache";

    public final static String LINE_COLOR_PROPERTY = "lineColor";
    public final static String WF_LINE_COLOR_PROPERTY = "wfLineColor";
    public final static String SELECT_LINE_COLOR_PROPERTY = "selectLineColor";
    public final static String SELECT_WFV_LINE_COLOR_PROPERTY = "wfvSelectLineColor";

    public final static String VIEW_NAME_SCALE_PROPERTY = "viewNameScale";
    public final static String VIEW_NONE_SCALE_PROPERTY = "viewNoneScale";
    public final static String FONT_NAME_PROPERTY = "fontName";
    public final static String FONT_STYLE_PROPERTY = "fontStyle";

    public final static String ACCEPT_NET_PROPERTY = "acceptNet";
    public final static String ACCEPT_STA_PROPERTY = "acceptSta";
    public final static String ACCEPT_CHANNEL_PROPERTY = "acceptChannel";

    public final static String REJECT_NET_PROPERTY = "rejectNet";
    public final static String REJECT_STA_PROPERTY = "rejectSta";
    public final static String REJECT_CHANNEL_PROPERTY = "rejectChannel";

    public final static String DISPLAY_ACTIVE_PROPERTY = "displayOnlyActive";

    public final static int DEFAULT_STATION_SIZE = 3;
    private int edgeSize = DEFAULT_STATION_SIZE;

    public final static String DEFAULT_CACHE_FILENAME =  "channelList.cache";
    public final static float DEFAULT_VIEW_NONE_SCALE =  10000000.f;
    public final static float DEFAULT_VIEW_NAME_SCALE =  1000000.f;
    private float viewNameScale = DEFAULT_VIEW_NAME_SCALE;
    private float viewNoneScale = DEFAULT_VIEW_NONE_SCALE;

    public final static String DEFAULT_FONT_NAME = "Arial Narrow";
    public final static String DEFAULT_FONT_STYLE = "PLAIN";
    private static int defaultFontSize = 10;

    private String fontName = DEFAULT_FONT_NAME;
    private String fontStyle = DEFAULT_FONT_STYLE;
    private int iStyle = Font.PLAIN;
    private int minScaleFontSize = defaultFontSize;
    private int maxScaleFontSize = minScaleFontSize;

    // Make these parseable from input property String
    private String [] acceptSta = new String [0];     // {"PASC"}; // 
    private String [] acceptNet = new String [0];     // {"NR","RB","LI"}; // 
    private String [] acceptChannel = new String [0]; // {"VH","UM","LL"}; // 

    private String [] rejectSta = new String [0];     // {"PASC"}; // 
    private String [] rejectNet = new String [0];     // {"NR","RB","LI"}; // 
    private String [] rejectChannel = new String [0]; // {"VH","UM","LL"}; // 

    private static final String defaultLineColorString =  "000000";
    private static final String defaultWfLineColorString = "ffc0fac0";
    private static final String defaultSelectLineColorString = "ffffff00";
    private static final String defaultWfvSelectLineColorString = "ff0000ff";

    private static Color defaultFillColor =  OMGraphic.clear;
    private Color lineColor = null; // no wf info
    private Color wfLineColor = null; // wf info in MasterView
    private Color selectLineColor = null;
    private Color wfvSelectLineColor = null;

    private String cacheFileName = null;

    private ChannelList chanList = null;  // MasterChannelList for jiggle session (channels added only)

    private HashMap sidMap = new HashMap(1003);

    // a triangle
    private int [] xypoints = null;


    private JPanel gui = null; // layer GUI configuration panel.

    private OMGraphic lastSelected = null;
    private WFView lastSelectedWFV = null;
    private boolean showingInfoLine = false;

    // true => show only stations active on date of current solution
    private boolean displayOnlyActiveOnDate = false; // default, ignore date

    /**
     * Construct an StationLayer.
     */
    public StationLayer() {
        setProjectionChangePolicy(new com.bbn.openmap.layer.policy.ListResetPCPolicy(this));
        //setMouseModeIDsForEvents(new String[] { SelectMouseMode.modeID });
        //getMouseEventInterpreter(); // don't do this here
        initStnHistory();
    }

    @Override
    public void findAndInit(Object obj) {
        if (obj instanceof JiggleMapBean) {
            JiggleMapBean mapBean = (JiggleMapBean) obj;
            mapBean.addPropertyChangeListener(this);
            setChannelList(mapBean.getChannelList());
            setSolution((Solution) mapBean.getMvSolutionList().getSelected() );
            wfa = mapBean.getMasterView().getWaveformArray();
            lastSelectedWFV = mapBean.getMasterView().masterWFViewModel.get();
            if (com.bbn.openmap.util.Debug.debugging("station"))
              System.out.println(getClass().getName() + "DEBUG masterview wfa size: " + wfa.length);
            this.jiggle = mapBean.jiggle;
            mapBean.stationLayer = this;
        }
        super.findAndInit(obj);
    }

    @Override
    public void findAndUndo(Object obj) {
        if (obj instanceof JiggleMapBean) {
            JiggleMapBean mapBean = (JiggleMapBean) obj;
            mapBean.removePropertyChangeListener(this);
            setChannelList(null); // ? may not be necessary since relies only on static MasterList
            setSolution(null);    // removes listeners from any prior solution
            this.jiggle = null;
        }
        super.findAndUndo(obj);
    }

    @Override
    protected OMGraphicList generateGraphics(OMGraphicList omgList) {
        //
        // invoked to generate new list at start or when needed (see setChannelList()) 
        // otherwise ListDataStateListener show update an existing list for repaint
        // invoking omgList.project(getProjection());
        //
        /*
        MapHandler mh = (MapHandler) getBeanContext();
        if ( mh != null) {
          // update list graphics color and visibility
          // based upon MasterView selected solution, if any 
          // don't call get(...) since context iterator may fast fail concurrent
          // since other worker threads may be updating the beancontext list
          Object [] obj = mh.toArray();
          for (int i = 0; i < obj.length; i++) {
             if (obj[i] instanceof JiggleMapBean) {
                setSolution( (Solution) ((JiggleMapBean)obj[i]).getMvSolutionList().getSelected() );
                break;
             }
          }
        }
        */

        sidMap.clear();

        Projection p = getProjection(); // null, if projection not yet defined 
        if (p == null || p.getScale() > viewNoneScale)  return omgList;

        ChannelList chanList = getChannelList();
        if (chanList != null) {
          if (com.bbn.openmap.util.Debug.debugging("station"))
              System.out.println(getClass().getName() + "DEBUG StationLayer channelList size: " + chanList.size());
          Channel chan = null;
          OMGraphic omg = null;
          for (int i = 0; i < chanList.size(); i++) {
            chan = (Channel) chanList.get(i);
            if ( accept(chan) ) {
                omg = makeOMGraphic(chan);
                if (omg != null) omgList.add(omg);
            }
          }
        }

        updateSelectedWFViewStaOutline(omgList); //to preserve last selected sta in view when zoom or pan regenerates list -aww 2010/08/05

        return omgList;
    }

    //added method below to preserve last selected sta in view when zoom or pan regenerates list -aww 2010/08/05
    private void updateSelectedWFViewStaOutline(OMGraphicList omgList) {
        if (lastSelectedWFV != null) {
          StationData sid = (StationData) sidMap.get( StationData.getMapKey(lastSelectedWFV) );
          if (sid != null) {
            OMGraphic omg = OMUtils.getOMGraphicWithAppObject(omgList, sid);
            if (omg != null) {
                omg.setLinePaint(wfvSelectLineColor);
            }
          }
        }
    }

    protected boolean accept(Channel chan) {

        String net = chan.getNet();
        String sta = chan.getSta();
        String chl = chan.getSeedchan();

        boolean status = (acceptNet.length == 0) ? true : false;
        for (int i =0; i < acceptNet.length; i++) {
            if (net.equalsIgnoreCase(acceptNet[i])) {
                status = true;
                break;
            }
        }
        if (status == false) return false;

        status = (acceptSta.length == 0) ? true : false;
        for (int i =0; i < acceptSta.length; i++) {
            if (sta.equalsIgnoreCase(acceptSta[i])) {
                status = true;
                break;
            }
        }
        if (status == false) return false;

        status = (acceptChannel.length == 0) ? true : false;
        for (int i =0; i < acceptChannel.length; i++) {
            if (chl.length() > 2 && chl.substring(0,2).equalsIgnoreCase(acceptChannel[i].substring(0,2))) {
                status = true;
                break;
            }
        }
        if (status == false) return false;

        for (int i =0; i < rejectNet.length; i++) {
            if (net.equalsIgnoreCase(rejectNet[i])) {
                return false;
            }
        }
        for (int i =0; i < rejectSta.length; i++) {
            if (sta.equalsIgnoreCase(rejectSta[i])) {
                return false;
            }
        }
        for (int i =0; i < rejectChannel.length; i++) {
            if (chl.length() < 2 ) {
                // Seed channel length must be at least two to prevent exception when running substring
                return false;
            } else if (chl.substring(0,2).equalsIgnoreCase(rejectChannel[i].substring(0,2))) {
                return false;
            }
        }

        return true;
    }

    protected OMGraphic makeOMGraphic(Channel chan) {

        Object key = StationData.getMapKey(chan);
        StationData sid = (StationData) sidMap.get(key);

        if (sid != null) {
              sid.add(chan);
              return null; // assume graphic already exists for stn, avoid multiples kludge -aww
        }
        else {
              sid = new StationData();
              sid.add(chan);
              sidMap.put(key, sid);
              // Triangle at LatLonPoint?
              // ? Extend to make new OMGraphic with Channel object instance member?
              // to map Channel info directly without lookup via appObject route and
              // otherwise risk of ChannelList sorts changes appObject idx match.
        }

        //TRIANGLE = new int [] {0, -3, 3, 3, -3, 3, 0, -3 }; // triangle
        //HEXAGON = new int[] { 0, -5, 4, -2, 4, 2, 0, 5, -4, 2, -4, -2, 0, -5 }; // hexagon
        //STAR = new int [] { 0, -4, 1, -1, 5, -1, 2, 1, 3, 4, 0, 2, -3, 4, -2, 1, -5, -1, -1, -1, 0, -4 };
        if (xypoints == null) {
            xypoints = new int[] { 0, -edgeSize, edgeSize, edgeSize, -edgeSize, edgeSize, 0, -edgeSize}; // triangle
             /*
                  xypoints  = new int[] {
                       Math.round(-1.25*edgeSize), 0,
                       Math.round(-.5*edgeSize), edgeSize,
                       Math.round(.5*edgeSize), edgeSize,
                       Math.round(1.25*edgeSize), 0,
                       Math.round(.5*edgeSize), -edgeSize,
                       Math.round(-.5*edgeSize), -edgeSize,
                       Math.round(-1.25*edgeSize), 0 }; // hexagon

                  xypoints  = new int[] {
                       0, Math.round(-1.25*edgeSize),
                       edgeSize, Math.round(-.5*edgeSize),
                       edgeSize, Math.round(.5*edgeSize),
                       0, Math.round(1.25*edgeSize),
                       -edgeSize, Math.round(.5*edgeSize),
                       -edgeSize, Math.round(-.5*edgeSize),
                       0, Math.round(-1.25*edgeSize) }; // hexagon

                  xypoints  = new int[] {
                      0, -edgeSize,
                      Math.round(.25*edgeSize), Math.round(-.25*edgeSize),
                      Math.round(1.25*edgeSize), Math.round(-.25*edgeSize),
                      Math.round(.5*edgeSize), Math.round(.25*edgeSize),
                      Math.round(.75*edgeSize), edgeSize,
                      0, Math.round(.5*edgeSize),
                      Math.round(-.75*edgeSize), edgeSize,
                      Math.round(-.5*edgeSize), Math.round(.25*edgeSize),
                      Math.round(-1.25*edgeSize), Math.round(-.25*edgeSize),
                      Math.round(-.25*edgeSize), Math.round(-.25*edgeSize),
                      0, -edgeSize };  // star
             */

        }

        //Need to use the ATTRIBUTE map for a LABEL to use OMPoly in lieu of Place:
        OMGraphic omg0 = new OMPoly(sid.getLat(), sid.getLon(), xypoints, OMPoly.COORDMODE_ORIGIN);
        //
        OMGraphic omg = new Place(sid.getLat(), sid.getLon(), sid.getSta(), omg0);
        if (omg instanceof Location) {
            Location loc = (Location) omg;
            //loc.getLabel().setFontSizer( new FontSizer(new Font("Arial Narrow",Font.PLAIN, 8), 500000.f,1,8,12));
            if (minScaleFontSize == maxScaleFontSize)
              loc.getLabel().setFont(new Font(fontName, iStyle, minScaleFontSize)); 
            else {
              loc.getLabel().setFontSizer(
                new FontSizer(
                  new Font(fontName, iStyle, defaultFontSize),
                  500000.f,
                  1,
                  minScaleFontSize,
                  maxScaleFontSize
                )
              );
            }
            Projection p = getProjection();
            if (p != null) loc.setShowName( (p.getScale() <= viewNameScale) ); // 1,000,000.f
            else loc.setShowName(false);
        }
        //

        omg.setSelectPaint(selectLineColor);

        // toggle station polygon visibility based on station's on/off dates of operation vs solution datetime?
        if (displayOnlyActiveOnDate) {
           //omg.setVisible((currentSol == null) ? true : sid.getDateRange().contains(currentSol.getDateTime()));
           omg.setVisible( stationOnForDate(sid, currentSolDate) );
        }
        omg.setFillPaint( getFillColor(sid) );
        omg.setLinePaint( getLineColor(sid) );
        omg.setAppObject(sid); //remember index
        return omg;

    }

    private Color getLineColor(StationData sid) {
        //Color wfvColor = OMGraphic.clear;
        Color wfvColor = lineColor;
        if (lastSelectedWFV == null) return wfvColor;

        ChannelIdentifiable cid = sid.getChannelIdentifiable();
        //System.out.println("DEBUG StationLayer waveform array length:" + wfa.length);
        for (int i = 0; i < wfa.length ; i++) {
            if (wfa[i].getChannelObj().sameStationAs(cid)) {
                wfvColor = (wfa[i] == lastSelectedWFV.wf) ? wfvSelectLineColor : wfLineColor;
                break;
            }
        }
        return wfvColor;
    }

    private Color getFillColor(StationData sid) {
        Color fillColor = defaultFillColor;
        if (currentSol != null) {
          ChannelIdentifiable cid = sid.getChannelIdentifiable();
          boolean hasPhase = currentSol.getPhaseList().hasEntryForStation(cid);
          boolean hasAmp = currentSol.getAmpList().hasEntryForStation(cid);

          if (hasPhase && hasAmp) fillColor = Color.red;
          else if (hasPhase) fillColor = Color.orange;
          else if (hasAmp) fillColor = Color.pink;
        }
        return fillColor;
    }

    /**
     * Get the station data from scratch if MasterChannelList has not already been loaded. 
     * Do in SwingWorker thread, or you will freeze the application GUI at this point.
     * 
     * @return List containing Channel info from database.
     */
    protected ChannelList getChannelList() {
        // Station_Data vs. Channel_Data table differences in ondate offdate of operation?
        // since components at single station may have different operational dates. 
        if (chanList == null || chanList.isEmpty()) {
            if ( MasterChannelList.isEmpty() ) {
              if (cacheFileName != null) ChannelList.setCacheFilename(cacheFileName);
              MasterChannelList.set(ChannelList.readFromCache()); // MasterChannelList.smartLoad();
            }
            setChannelList( MasterChannelList.get() );
        }
        if (com.bbn.openmap.util.Debug.debugging("station")) {
            if (chanList != null) {
                System.out.println("StationLayer channelList size: " + chanList.size());
            }
            else System.out.println("Error getChannelList() returned List is null!");
        }
        return chanList;
    }

    public void setChannelList(ChannelList chanList) {
        if (this.chanList != null) {
            this.chanList.removeListDataStateListener(this);
        }
        this.chanList = chanList;
        if (this.chanList != null) {
            this.chanList.addListDataStateListener(this);
        }

        OMGraphicList omgList = getList();
        if (omgList != null) {
          omgList.clear(); // clear old graphics list
          setList(null);    
          repaint();
        }

        if (this.chanList != null) {
          doPrepare(); // build layer graphics in background thread
        }
    }

    /** The user interface palette for layer. */
    protected Box palette = null;

    /**
     * Gets the gui controls associated with the layer.
     * 
     * @return Component
     */

    /** Creates the interface palette. */
    @Override
    public java.awt.Component getGUI() {
        // STATION_SIZE_PROPERTY = "stationSize";
        // CACHE_FILENAME_PROPERTY = "channelListCache";
        // LINE_COLOR_PROPERTY = "lineColor";
        // WF_LINE_COLOR_PROPERTY = "wfLineColor";
        // SELECT_LINE_COLOR_PROPERTY = "selectLineColor";
        // SELECT_WFV_LINE_COLOR_PROPERTY = "wfvSelectLineColor";
        // VIEW_NAME_SCALE_PROPERTY = "viewNameScale";
        // VIEW_NONE_SCALE_PROPERTY = "viewNoneScale";
        // FONT_NAME_PROPERTY = "fontName";
        // FONT_STYLE_PROPERTY = "fontStyle";
        //
        // ACCEPT_NET_PROPERTY = "acceptNet";
        // ACCEPT_STA_PROPERTY = "acceptNet";
        // ACCEPT_CHANNEL_PROPERTY = "acceptChannel";
        //
        // REJECT_NET_PROPERTY = "rejectNet";
        // REJECT_STA_PROPERTY = "rejectNet";
        // REJECT_CHANNEL_PROPERTY = "rejectChannel";
        //
        // DISPLAY_ACTIVE_PROPERTY = "displayOnlyActive";

        if (palette == null) {
            if (com.bbn.openmap.util.Debug.debugging("station"))
                System.out.println("StationLayer: creating Station GUI Palette.");

            palette = Box.createVerticalBox();

            JPanel layerPanel = PaletteHelper.createPaletteJPanel(i18n.get(StationLayer.class,
                    "layerPanel",
                    "Station Fill Color Legend"));


            JPanel jp = new JPanel();
            jp.setLayout(new GridLayout(3,2));
            JLabel jl = new JLabel("Amp & Phase ");
            jp.add(jl);
            jl = new JLabel("");
            jl.setBackground(Color.red);
            jl.setOpaque(true);
            jp.add(jl);

            jl = new JLabel("Phase Only ");
            jp.add(jl);
            jl = new JLabel("");
            jl.setBackground(Color.orange);
            jl.setOpaque(true);
            jp.add(jl);

            jl = new JLabel("Amp Only   ");
            jp.add(jl);
            jl = new JLabel("");
            jl.setBackground(Color.pink);
            jl.setOpaque(true);
            jp.add(jl);

            layerPanel.add(jp);
            palette.add(layerPanel);

            ActionListener al = new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                  StationLayer.this.actionPerformed(e);
                  String command = e.getActionCommand();
                  if (command == RedrawCmd) {
                      //System.out.println("DEBUG StationLayer REDRAW lineColor: " + lineColor.toString());
                      getList().clear(); // clear old graphics list
                      setList(null); // blow it away
                      if (isVisible()) StationLayer.this.doPrepare();
                  }
                }
            };

            JPanel subbox = new JPanel(new GridLayout(0, 1));

            JButton setProperties = new JButton(i18n.get(StationLayer.class,
                    "setProperties", "Preferences"));
            setProperties.setActionCommand(DisplayPropertiesCmd);
            setProperties.addActionListener(al);
            subbox.add(setProperties);

            JButton redraw = new JButton(i18n.get(StationLayer.class,
                    "redraw", "Redraw Station Layer"));
            redraw.setActionCommand(RedrawCmd);
            redraw.addActionListener(al);
            subbox.add(redraw);
            palette.add(subbox);
        }
        return palette;
    }

    /**
     * Returns the MapMouseListener object that handles the mouse
     * events.
     * 
     * @return the MapMouseListener for the layer, or null if none
     */
    /*
    public MapMouseListener getMapMouseListener() {
        return this;
    }
    */

    //----------------------------------------------------------------
    // MapMouseListener interface methods
    //----------------------------------------------------------------

    /**
     * Return a list of the modes that are interesting to the
     * MapMouseListener. The source MouseEvents will only get sent to
     * the MapMouseListener if the mode is set to one that the
     * listener is interested in. Layers interested in receiving
     * events should register for receiving events in "select" mode:
     * <code>
     * <pre>
     * return new String[] { SelectMouseMode.modeID };
     * </pre>
     * <code>
     * @return String[] of modeID's
     * @see com.bbn.openmap.event.NavMouseMode#modeID
     * @see com.bbn.openmap.event.SelectMouseMode#modeID
     * @see com.bbn.openmap.event.NullMouseMode#modeID
     */
    @Override
    public String[] getMouseModeServiceList() {
        return new String[] { com.bbn.openmap.event.SelectMouseMode.modeID };
        //return getMouseModeIDsForEvents(); // ?
    }

    /**
     * Invoked when a mouse button has been pressed on a component.
     * 
     * @param e MouseEvent
     * @return true if the listener was able to process the event.
     */
    @Override
    public boolean mousePressed(MouseEvent e) {
        //System.out.println(getClass().getName() + " MousePressed StationLayer");
        boolean status = false;
        OMGraphicList omgList = getList();
        if (omgList != null && chanList != null) {

            //OMGraphic obj = omgList.findClosest(e.getX(), e.getY(), 4);
            OMGraphic obj = omgList.selectClosest(e.getX(), e.getY(), 2);
            if (obj != null) {
                lastSelected = obj;
                obj.setSelected(true);
                Object o = obj.getAppObject();
                if (o != null) {
                  int clickCount = e.getClickCount();
                  int mask = e.getModifiers();
                  //if ( (mask == (MouseEvent.BUTTON1_MASK | InputEvent.SHIFT_MASK) ) 
                  if ( mask == MouseEvent.BUTTON1_MASK && clickCount >= 2) {
                    // For a double click count:
                    ChannelIdentifiable cid = ((StationData)o).getChannelIdentifiable();
                    String str = cid.toDelimitedSeedNameString(".");
                    //System.out.println(getClass().getName() + " DEBUG StationLayer DOUBLE click on sta: " + str);
                    jiggle.setPickPanelToChannelStartingWith(str);
                  }
                  else if (e.isPopupTrigger()) {               
                    //System.out.println(getClass().getName() + " DEBUG StationLayer MOUSE EVENT popup trigger");
                    /* ? Popup Menu list of Seedchan upon right click ?
                    ChannelableList ch = ((StationData)o).getChannelables();
                    ch.sortByStation();
                    StringBuffer sb = new StringBuffer(64);
                    for (int idx = 0; idx < ch.size(); idx++) {
                      ((Channelable)ch.get(i)).getChannelObj().getSeedchan();
                    }
                    String dataStr = sb.toString();
                    */
                  }
                  else {
                    //System.out.println(getClass().getName() + " DEBUG StationLayer MOUSE EVENT left click INFO ");
                    String dataStr = ((StationData)o).toInfoString();
                    fireRequestInfoLine(dataStr);
                    //fireRequestMessage(dataStr);
                    fireRequestToolTip(dataStr);
                    showingInfoLine = true;
                    repaint();
                  }
                  status = true;
                }
            }
        }
        //System.out.println(getClass().getName() + " DEBUG StationLayer MOUSE EVENT return status: " + status);
        return status;
    }

    @Override
    public String getInfoText(OMGraphic omg) {
        return getToolTipTextFor(omg);
    }

    @Override
    public String getToolTipTextFor(OMGraphic omg) {
        Object obj = omg.getAppObject(); 
        if (obj instanceof StationData) { 
            return ((StationData) obj).toInfoString();
        }
        return null;
    }

    /**
     * Invoked when a mouse button has been released on a component.
     * 
     * @param e MouseEvent
     * @return true if the listener was able to process the event.
     */
    @Override
    public boolean mouseReleased(MouseEvent e) {
        return false;
    }

    /**
     * Invoked when the mouse has been clicked on a component. The
     * listener will receive this event if it successfully processed
     * <code>mousePressed()</code>, or if no other listener
     * processes the event. If the listener successfully processes
     * mouseClicked(), then it will receive the next mouseClicked()
     * notifications that have a click count greater than one.
     * 
     * @param e MouseEvent
     * @return true if the listener was able to process the event.
     */
    @Override
    public boolean mouseClicked(MouseEvent e) {
        return false;
    }

    /**
     * Invoked when the mouse enters a component.
     * 
     * @param e MouseEvent
     */
    @Override
    public void mouseEntered(MouseEvent e) { }

    /**
     * Invoked when the mouse exits a component.
     * 
     * @param e MouseEvent
     */
    @Override
    public void mouseExited(MouseEvent e) { 
        // clean up display
        if (showingInfoLine) {
            showingInfoLine = false;
            fireRequestInfoLine("");
        }
        if (lastSelected != null && lastSelected.distance(e.getX(),e.getY()) > 10) {
          fireHideToolTip();
          getList().deselect();
          lastSelected = null;
          repaint();
        }
    }

    /**
     * Invoked when a mouse button is pressed on a component and then
     * dragged. The listener will receive these events if it
     * successfully processes mousePressed(), or if no other listener
     * processes the event.
     * 
     * @param e MouseEvent
     * @return true if the listener was able to process the event.
     */
    @Override
    public boolean mouseDragged(MouseEvent e) {
        return false;
    }

    /**
     * Invoked when the mouse button has been moved on a component
     * (with no buttons down).
     * 
     * @param e MouseEvent
     * @return true if the listener was able to process the event.
     */
    @Override
    public boolean mouseMoved(MouseEvent e) {
        return false;
    }

    /**
     * Handle a mouse cursor moving without the button being pressed.
     * This event is intended to tell the listener that there was a
     * mouse movement, but that the event was consumed by another
     * layer. This will allow a mouse listener to clean up actions
     * that might have happened because of another motion event
     * response.
     */
    @Override
    public void mouseMoved() {}

    //----------------------------------------------------------------
    // PropertyConsumer Interface
    //----------------------------------------------------------------

    /**
     * Set the properties of the StationLayer.
     * 
     * @param prefix String
     * @param props Properties
     */
    @Override
    public void setProperties(String prefix, Properties props) {
        super.setProperties(prefix, props);
        prefix = PropUtils.getScopedPropertyPrefix(prefix);
        cacheFileName =
            props.getProperty(prefix + CACHE_FILENAME_PROPERTY, DEFAULT_CACHE_FILENAME);
        edgeSize =
            PropUtils.intFromProperties(props, prefix + STATION_SIZE_PROPERTY, DEFAULT_STATION_SIZE);
        // Stations without wf info
        lineColor =
            (Color) PropUtils.parseColorFromProperties(props, prefix + LINE_COLOR_PROPERTY, defaultLineColorString);
        // Stations with wf info from MasterView
        wfLineColor =
            (Color) PropUtils.parseColorFromProperties(props, prefix + WF_LINE_COLOR_PROPERTY, defaultWfLineColorString);
        selectLineColor =
            (Color) PropUtils.parseColorFromProperties(props, prefix + SELECT_LINE_COLOR_PROPERTY, defaultSelectLineColorString);
        wfvSelectLineColor =
            (Color) PropUtils.parseColorFromProperties(props, prefix + SELECT_WFV_LINE_COLOR_PROPERTY, defaultWfvSelectLineColorString);
        viewNoneScale =
            PropUtils.floatFromProperties(props, prefix + VIEW_NONE_SCALE_PROPERTY, DEFAULT_VIEW_NONE_SCALE);
        viewNameScale =
            PropUtils.floatFromProperties(props, prefix + VIEW_NAME_SCALE_PROPERTY, DEFAULT_VIEW_NAME_SCALE);

        fontName = props.getProperty(prefix+FONT_NAME_PROPERTY, DEFAULT_FONT_NAME);
        fontStyle = props.getProperty(prefix+FONT_STYLE_PROPERTY, DEFAULT_FONT_STYLE);
        if (fontStyle.equalsIgnoreCase("PLAIN")) iStyle = Font.PLAIN;
        else if (fontStyle.equalsIgnoreCase("BOLD")) iStyle = Font.BOLD;
        else if (fontStyle.equalsIgnoreCase("ITALIC")) iStyle = Font.ITALIC;

        minScaleFontSize =
            PropUtils.intFromProperties(props, prefix+"minFontSize", defaultFontSize);
        maxScaleFontSize =
            PropUtils.intFromProperties(props, prefix+"maxFontSize", defaultFontSize);

        String [] strArray =
            PropUtils.stringArrayFromProperties(props,prefix + ACCEPT_NET_PROPERTY, " ");
        if (strArray != null) acceptNet = strArray;  

        strArray =
            PropUtils.stringArrayFromProperties(props,prefix + ACCEPT_STA_PROPERTY, " ");
        if (strArray != null) acceptSta = strArray;  

        strArray =
            PropUtils.stringArrayFromProperties(props,prefix + ACCEPT_CHANNEL_PROPERTY, " ");
        if (strArray != null) acceptChannel = strArray;  

        strArray =
            PropUtils.stringArrayFromProperties(props,prefix + REJECT_NET_PROPERTY, " ");
        if (strArray != null) rejectNet = strArray;  

        strArray =
            PropUtils.stringArrayFromProperties(props,prefix + REJECT_STA_PROPERTY, " ");
        if (strArray != null) rejectSta = strArray;  

        strArray =
            PropUtils.stringArrayFromProperties(props,prefix + REJECT_CHANNEL_PROPERTY, " ");
        if (strArray != null) rejectChannel = strArray;  

        displayOnlyActiveOnDate = 
            PropUtils.booleanFromProperties(props,prefix + DISPLAY_ACTIVE_PROPERTY, false);
    }

    /**
     * Get the associated properties object.
     */
    @Override
    public Properties getProperties(Properties props) {
        return getProperties(propertyPrefix, props);
    }

    /**
     * Get the associated properties object. This method creates a
     * Properties object if necessary and fills it with the relevant
     * data for this layer.
     */
    public Properties getProperties(String prefix, Properties props) {
        props = super.getProperties(props);
        prefix = PropUtils.getScopedPropertyPrefix(prefix);
        props.put(prefix + CACHE_FILENAME_PROPERTY, cacheFileName);
        props.put(prefix + STATION_SIZE_PROPERTY, String.valueOf(edgeSize));

        String colorString = (lineColor == null) ?
            defaultLineColorString : Integer.toHexString(lineColor.getRGB());
        props.put(prefix + LINE_COLOR_PROPERTY, colorString);

        colorString = (wfLineColor == null) ?
            defaultWfLineColorString : Integer.toHexString(wfLineColor.getRGB());
        props.put(prefix + WF_LINE_COLOR_PROPERTY, colorString);

        colorString = (selectLineColor == null) ?
            defaultSelectLineColorString : Integer.toHexString(selectLineColor.getRGB());
        props.put(prefix + SELECT_LINE_COLOR_PROPERTY, colorString);

        colorString = (wfvSelectLineColor == null) ?
            defaultWfvSelectLineColorString : Integer.toHexString(wfvSelectLineColor.getRGB());
        props.put(prefix + SELECT_WFV_LINE_COLOR_PROPERTY, colorString);

        props.put(prefix + VIEW_NONE_SCALE_PROPERTY, String.valueOf(viewNoneScale));
        props.put(prefix + VIEW_NAME_SCALE_PROPERTY, String.valueOf(viewNameScale));
        
        props.put(prefix+FONT_NAME_PROPERTY, fontName);
        props.put(prefix+FONT_STYLE_PROPERTY, fontStyle);

        props.put(prefix+ACCEPT_NET_PROPERTY, GenericPropertyList.toPropertyString(acceptNet));
        props.put(prefix+ACCEPT_STA_PROPERTY, GenericPropertyList.toPropertyString(acceptSta));
        props.put(prefix+ACCEPT_CHANNEL_PROPERTY, GenericPropertyList.toPropertyString(acceptChannel));

        props.put(prefix+REJECT_NET_PROPERTY, GenericPropertyList.toPropertyString(rejectNet));
        props.put(prefix+REJECT_STA_PROPERTY, GenericPropertyList.toPropertyString(rejectSta));
        props.put(prefix+REJECT_CHANNEL_PROPERTY, GenericPropertyList.toPropertyString(rejectChannel));

        props.put(prefix+DISPLAY_ACTIVE_PROPERTY, String.valueOf(displayOnlyActiveOnDate));

        return props;
    }

    /**
     * Supplies the propertiesInfo object associated with this
     * StationLayer object. Contains the human readable
     * descriptions of the properties and the
     * <code>initPropertiesProperty</code> (see Inspector class.)
     */
    @Override
    public Properties getPropertyInfo(Properties info) {
        info = super.getPropertyInfo(info);

        info.put(initPropertiesProperty, 
                  LINE_COLOR_PROPERTY + " " +
                  WF_LINE_COLOR_PROPERTY + " " +
                  SELECT_LINE_COLOR_PROPERTY + " " +
                  SELECT_WFV_LINE_COLOR_PROPERTY + " " +
                  STATION_SIZE_PROPERTY + " " +
                  FONT_NAME_PROPERTY + " " +
                  FONT_STYLE_PROPERTY + " " +
                  VIEW_NAME_SCALE_PROPERTY + " " +
                  VIEW_NONE_SCALE_PROPERTY + " " +
                  ACCEPT_NET_PROPERTY + " " +
                  ACCEPT_STA_PROPERTY + " " +
                  ACCEPT_CHANNEL_PROPERTY + " " +
                  REJECT_NET_PROPERTY + " " +
                  REJECT_STA_PROPERTY + " " +
                  REJECT_CHANNEL_PROPERTY + " " +
                  DISPLAY_ACTIVE_PROPERTY + " " +
                  CACHE_FILENAME_PROPERTY
        );

        info.put(FONT_NAME_PROPERTY, "Location label font name");
        info.put(FONT_STYLE_PROPERTY, "Location label font style (PLAIN, BOLD, ITALIC)");

        info.put(STATION_SIZE_PROPERTY, "Edge size of station symbol (pixels)");

        info.put(CACHE_FILENAME_PROPERTY, "Channel cache filename in user directory");
        info.put(CACHE_FILENAME_PROPERTY + ScopedEditorProperty,
                "com.bbn.openmap.util.propertyEditor.FUPropertyEditor"); // File or URL chooser

        I18n i18n = Environment.getI18n();
        String interString = i18n.get(StationLayer.class,
                LINE_COLOR_PROPERTY,
                i18n.TOOLTIP,
                "Color for the station markers edges.");
        info.put(LINE_COLOR_PROPERTY, interString);
        interString = i18n.get(StationLayer.class, LINE_COLOR_PROPERTY, "Stn Marker line Color");
        info.put(LINE_COLOR_PROPERTY + LabelEditorProperty, interString);
        info.put(LINE_COLOR_PROPERTY + ScopedEditorProperty,
                "com.bbn.openmap.util.propertyEditor.ColorPropertyEditor");

        interString = i18n.get(StationLayer.class,
                WF_LINE_COLOR_PROPERTY,
                i18n.TOOLTIP,
                "Color for the waveform data station markers edges.");
        info.put(WF_LINE_COLOR_PROPERTY, interString);
        interString = i18n.get(StationLayer.class, WF_LINE_COLOR_PROPERTY, "Waveform Stn Marker line Color");
        info.put(WF_LINE_COLOR_PROPERTY + LabelEditorProperty, interString);
        info.put(WF_LINE_COLOR_PROPERTY + ScopedEditorProperty,
                "com.bbn.openmap.util.propertyEditor.ColorPropertyEditor");

        interString = i18n.get(StationLayer.class,
                SELECT_LINE_COLOR_PROPERTY,
                i18n.TOOLTIP,
                "Color for the selected station markers edges.");
        info.put(SELECT_LINE_COLOR_PROPERTY, interString);
        interString = i18n.get(StationLayer.class, SELECT_LINE_COLOR_PROPERTY, "Selected Waveform Stn Marker line Color");
        info.put(SELECT_LINE_COLOR_PROPERTY + LabelEditorProperty, interString);
        info.put(SELECT_LINE_COLOR_PROPERTY + ScopedEditorProperty,
                "com.bbn.openmap.util.propertyEditor.ColorPropertyEditor");

        interString = i18n.get(StationLayer.class,
                SELECT_WFV_LINE_COLOR_PROPERTY,
                i18n.TOOLTIP,
                "Color for the selected waveform view in Jiggle station markers edges.");
        info.put(SELECT_WFV_LINE_COLOR_PROPERTY, interString);
        interString = i18n.get(StationLayer.class, SELECT_WFV_LINE_COLOR_PROPERTY, "Selected WFView Stn Marker line Color");
        info.put(SELECT_WFV_LINE_COLOR_PROPERTY + LabelEditorProperty, interString);
        info.put(SELECT_WFV_LINE_COLOR_PROPERTY + ScopedEditorProperty,
                "com.bbn.openmap.util.propertyEditor.ColorPropertyEditor");

        info.put(VIEW_NONE_SCALE_PROPERTY, "Max zoom scale showing any station.");
        info.put(VIEW_NAME_SCALE_PROPERTY, "Max zoom scale showing all station labels by default.");

        info.put(ACCEPT_NET_PROPERTY, "Stations with these network codes are accepted");
        info.put(ACCEPT_STA_PROPERTY, "Stations with name are accepted");
        info.put(ACCEPT_CHANNEL_PROPERTY, "Stations whose first 2 seedchan chars matches these codes are accepted");

        info.put(REJECT_NET_PROPERTY, "Stations with these network codes are rejected");
        info.put(REJECT_STA_PROPERTY, "Stations with name are rejected");
        info.put(REJECT_CHANNEL_PROPERTY, "Stations whose first 2 seedchan chars matches these codes are rejected");

        info.put(DISPLAY_ACTIVE_PROPERTY, "Hide Stn not active on date of currently selected solution.");
        info.put(DISPLAY_ACTIVE_PROPERTY + ScopedEditorProperty,
                "com.bbn.openmap.util.propertyEditor.TrueFalsePropertyEditor");

        return info;
    }

    public StationData getSelectedStationData() {
        if (chanList == null || lastSelected == null)  return null;
        Object o = lastSelected.getAppObject();
        return (o == null) ? null : (StationData) o;
    }

    //ListDataStateListener interface methods
    @Override
    public void intervalAdded(ListDataStateEvent e) {
        // Valid state change, new Channels added to MasterChannelList
        if (e.getSource() == chanList) processChannelListLDSE(e); // ??
        // may be a changed amp,arrival, or coda on a station
        else if (e.getSource() instanceof JasiReadingList)
            processReadingListLDSE(e);
    }

    @Override
    public void intervalRemoved(ListDataStateEvent e) {
        if (e.getSource() instanceof JasiReadingList)
            processReadingListLDSE(e);
        // BOGUS, Should not be any replacement of Channels in MasterList
        // but perhaps in future something will, do we report error here?
        // if (e.getSource() == chanList) processChannelListLDSE(e); // ??
    }

    @Override
    public void contentsChanged(ListDataStateEvent e) {
        if (e.getSource() instanceof JasiReadingList)
            processReadingListLDSE(e);
        // BOGUS, Should not be any replacement of Channels in MasterList
        // but perhaps in future something will, do we report error here?
        //else if (e.getSource() == chanList) processChannelListLDSE(e); // ??
    }

    @Override
    public void orderChanged(ListDataStateEvent e) {
        // no-op, order doesn't matter yet
    }

    @Override
    public void stateChanged(ListDataStateEvent e) {
        if (e.getSource() instanceof JasiReadingList) 
            processReadingListLDSE(e);
        else if (e.getSource() instanceof SolutionList) {
            // from MasterView solList
            if (e.getStateChange().getStateName().equals("selected"))
                setSolution((Solution) e.getStateChange().getValue()); // color/visibility update of graphics
        }
        // BOGUS, Should not be any state change of Channels in MasterList
        // but perhaps in future something will, do we report error here?
        //else if (e.getSource() == chanList) processChannelListLDSE(e); // ??
    }

    private void processReadingListLDSE(ListDataStateEvent e) {
        // Lazy approach, do all event the same way, until some
        // other methods are implemented in class to discriminate types
        JasiReadingList src = (JasiReadingList) e.getSource();
        int first  = e.getIndex0();
        int last   = e.getIndex1();
        StateChange sc = e.getStateChange();
        Object data = sc.getValue();

        if (com.bbn.openmap.util.Debug.debugging("station")) {
          System.out.println(
            "DEBUG StationLayer processReadingListLDSE from: " +src.getClass().getName() +
            " type: " + sc.getStateName() + " from " + first + " to " + last
            + " value is : " + ((data == null) ? "null" : data.getClass().getName())
          );
        }

        if (data == null) {
          // don't know what changed, so redo existing graphics list elements visibility and colors
          refreshAllStationGraphics();
        }
        else if (first > -1 && last >= first) { // valid indices
          if (e.getType() == ListDataStateEvent.STATE_CHANGED) {

            if (com.bbn.openmap.util.Debug.debugging("station") && first == last)
                System.out.println( ((List) src).get(first).toString() );

            //For "stateChanged" case, the StateChange data is value of the new state, 
            // not the list element(s) changed, so have to use indices of the source
            // list to lookup element (hopefully source hasn't been resorted).
            for (int idx=first; idx <= last; idx++) {
              refreshStationGraphic( (Channelable) src.get(idx));
            }
          }
          else if (data instanceof Channelable) {
            refreshStationGraphic( (Channelable) data);
          }
          else { // assume event data object is an array of changed list elements
            Object [] d = (Object []) data;
            for (int idx=0; idx < d.length; idx++) {
              refreshStationGraphic( (Channelable) d[idx]);
            }
          }
        }

        OMGraphicList omgList = getList();
        if (omgList != null) {
            omgList.project(getProjection());
        }

        updateSelectedWFViewStaOutline(omgList); // 2010/08/05

        repaint();
    }

    // Iterate over graphics list setting visibility and color of station graphic
    public void refreshAllStationGraphics() {

        OMGraphicList omgList = getList();
        if (omgList == null) return;
        if (com.bbn.openmap.util.Debug.debugging("station"))
            System.out.println(getClass().getName() + " DEBUG refreshing list graphics...");
        OMGraphic omg = null;
        StationData sid = null;
        // active date to use for visibility filter
        // java.util.Date solDate =  (currentSol == null) ? null : currentSol.getDateTime();

        for (Iterator it = omgList.iterator(); it.hasNext();) {
            omg = (OMGraphic) it.next();
            sid = (StationData) omg.getAppObject();
            if (displayOnlyActiveOnDate) {
                //omg.setVisible( (solDate == null) ? true : sid.getDateRange().contains(solDate));
                omg.setVisible( stationOnForDate(sid, currentSolDate) );
            }
            omg.setFillPaint( getFillColor(sid) );
            omg.setLinePaint( getLineColor(sid) );
        }

        repaint();
    }

    private void refreshStationGraphic(Channelable chan) {
        refreshStationGraphic(chan, false);
    }

    private void refreshStationGraphic(Channelable chan, boolean select) {
        if (chan == null) return;
        OMGraphicList omgList = getList();
        if (omgList == null || omgList.isEmpty()) return;

        StationData sid = (StationData) sidMap.get( StationData.getMapKey(chan) );
        if (sid != null) {
            OMGraphic omg = OMUtils.getOMGraphicWithAppObject(omgList, sid);
            // If operational for solution epoch reset colors in case station amp/arrival relationship changed 
            if (omg != null && omg.isVisible()) {
              omg.setFillPaint( getFillColor(sid) );
              omg.setLinePaint( getLineColor(sid) );
              if (select) {
                  if (lastSelected != null) lastSelected.deselect();
                  omg.setSelected(select);
                  lastSelected = omg;
              }
            }

            if (omg == null) {
                System.out.println("Map StationLayer refreshStationGraphic: null graphic for sid: "+ sid.toInfoString() + " chan: " + chan.toString());
            }
        }
    }

    private OMGraphic getOMGraphicFor(Channelable chan) {
        if (chan == null) return null;

        OMGraphicList omgList = getList();
        if (omgList == null || omgList.isEmpty()) return null;

        StationData sid = (StationData) sidMap.get( StationData.getMapKey(chan) );
        return (sid == null) ? null : OMUtils.getOMGraphicWithAppObject(omgList, sid);
    }

    // listener method invoked when ChannelList elements change
    // removes existing OMGraphicList element matching changed channel
    // then if removeOnly == false creates a new replacement element
    // for same channel and adds it to the OMGraphicList
    private void processChannelListLDSE(ListDataStateEvent e) {

        if (chanList == null || chanList.isEmpty()) return;

        // Lazy approach, do all events the same way
        ChannelList src = (ChannelList) e.getSource();
        int first  = e.getIndex0();
        int last   = e.getIndex1();
        StateChange sc = e.getStateChange();
        Object data = sc.getValue();
        boolean removeOnly = (e.getType() == ListDataStateEvent.INTERVAL_REMOVED)  || 
                             (sc.getStateName() == StateChange.DELETED); 
        /*
        System.out.println(
          "DEBUG StationLayer processChannelListLDSE for: " +src.getClass().getName() +
          " type: " + sc.getStateName() + " from " + first + " to " + last
          + " value is : " + ((data == null) ? "null" : data.getClass().getName())
          + " removeOnly: " + removeOnly
        );
        */

        // Check for valid StateChange data element, else we don't know what changed,
        // so for this weird case we have to recreate a new list of station graphics
        if (data == null) {
          getList().clear(); // clear old graphics list
          setList(null);    
        }
        else if (first > -1 && last >= first) { // valid indices
          if (e.getType() == ListDataStateEvent.STATE_CHANGED) {
            //For "stateChanged" case, the StateChange data is value of the new state, 
            // not the list element(s) changed, so have to use indices of the source
            // list to lookup element (hopefully source hasn't been resorted).
            for (int idx=first; idx <= last; idx++) {
              updateGraphicListForStation( (Channelable) src.get(idx), removeOnly );
            }
          }
          // Data either single element or array of such
          else if (data instanceof Channelable) {
              updateGraphicListForStation( (Channelable) data, removeOnly );
          }
          else { // assume event data object is an array of changed list elements
              Object [] d = (Object []) data;
              for (int idx=0; idx < d.length; idx++) {
                updateGraphicListForStation( (Channelable) d[idx], removeOnly );
              }
          }
        }

        OMGraphicList omgList = getList();
        // If we have a list refresh the graphics, otherwise rebuild anew
        if (omgList != null) {
          omgList.project(getProjection());
          repaint();
        }
        else {
          doPrepare();  // use thread to construct anew
        }
    }

    private void updateGraphicListForStation(Channelable chan, boolean removeOnly) {
        if ( (chan == null) || ! accept(chan.getChannelObj()) ) return;

        OMGraphicList omgList = getList();
        StationData sid = (StationData) sidMap.get( StationData.getMapKey(chan) );
        if (sid != null) {
            OMGraphic g = OMUtils.getOMGraphicWithAppObject(omgList, sid);
            if (g != null) omgList.remove(g);
        }
        if (removeOnly) return;
        OMGraphic omg = makeOMGraphic(chan.getChannelObj());
        if (omg != null) omgList.add(omg);  // StationData removes existing key matches.
    }

    // Link by listener to the MasterView solution selection event
    // sets currentSolution here, which invokes the generation of
    // the new layer corrected graphics for new solution's readingLists.
    public void setSolution(Solution sol) {
        if (currentSol != null) {
          currentSol.getPhaseList().removeListDataStateListener(this);
          currentSol.getAmpList().removeListDataStateListener(this);
          //currentSol.getCodaList().removeListDataStateListener(this);
        }
        currentSol = sol;
        currentSolDate = (sol == null) ? null : sol.getDateTime();
        if (currentSol != null) {
          currentSol.getPhaseList().addListDataStateListener(this);
          currentSol.getAmpList().addListDataStateListener(this);
          //currentSol.getCodaList().addListDataStateListener(this);
        }
        refreshAllStationGraphics();  // update existing list?
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getSource() instanceof JiggleMapBean) {
          String name = evt.getPropertyName();
          //System.out.println(getClass().getName() + " DEBUG propertyChange name = " + name); 
          if (name.equals(Jiggle.NEW_MV_PROPNAME)) {
            MasterView mv = (MasterView) evt.getNewValue();
            wfa = mv.getWaveformArray();
            if (com.bbn.openmap.util.Debug.debugging("station"))
              System.out.println(getClass().getName() + "DEBUG masterview wfa size: " + wfa.length);
            setSolution( mv.getSelectedSolution() );
            mv.addSolutionListListener(this); // notify if list selection changes
          }
          else if (name.equals(Jiggle.NEW_MV_WFVIEW_PROPNAME)) {
              WFView wfv = (WFView) evt.getNewValue();
              //refreshStationGraphic(wfv, true);
              OMGraphic omg = getOMGraphicFor(lastSelectedWFV);
              if (omg != null) omg.setLinePaint( (lastSelectedWFV.hasWaveform() ? wfLineColor : lineColor) );
              omg = getOMGraphicFor(wfv);
              if (omg != null) omg.setLinePaint(wfvSelectLineColor);
              lastSelectedWFV = wfv; 
              repaint();
          }
        }
    }

    public void setJiggle(Jiggle jiggle) {
        this.jiggle = jiggle;
    }

    @Override
    public List getItemsForMapMenu(MapMouseEvent mme) {
        //System.out.println(getClass().getName() + " DEBUG getItemsForMapMenu" );
        List aList = new ArrayList();
        JMenuItem jmi = new JMenuItem("Recenter");
        final MapMouseEvent evt = mme;
        jmi.addActionListener( new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                MapHandler mh = (MapHandler) getBeanContext();
                if (mh != null) {
                    JiggleMapBean mapBean =
                            (JiggleMapBean) mh.get("org.trinet.jiggle.JiggleMapBean");
                    if (mapBean != null) {
                        LatLonPoint llp = LatLonPoint.valueOf(getProjection().inverse(evt.getX(), evt.getY()));
                        mapBean.setCenter(llp);
                    }
                }
            }
        });
        aList.add(jmi);
        //
        jmi = new JMenuItem("Zoom +");
        jmi.addActionListener( new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                MapHandler mh = (MapHandler) getBeanContext();
                if (mh != null) {
                    JiggleMapBean mapBean =
                            (JiggleMapBean) mh.get("org.trinet.jiggle.JiggleMapBean");
                    if (mapBean != null) mapBean.zoom(new ZoomEvent(this, ZoomEvent.RELATIVE, 0.5f));
                }
            }
        });
        aList.add(jmi);

        jmi = new JMenuItem("Zoom -");
        jmi.addActionListener( new ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                MapHandler mh = (MapHandler) getBeanContext();
                if (mh != null) {
                    JiggleMapBean mapBean =
                            (JiggleMapBean) mh.get("org.trinet.jiggle.JiggleMapBean");
                    if (mapBean != null) mapBean.zoom(new ZoomEvent(this, ZoomEvent.RELATIVE, 2.0f));
                }
            }
        });
        aList.add(jmi);

        // no-op return option
        aList.add(new JMenuItem("Cancel"));

        return aList;
    }

    @Override
    public List getItemsForOMGraphicMenu(OMGraphic omg) {
        //System.out.println(getClass().getName() + " DEBUG getItemsForGraphicMenu" );
        //return null;
        Object obj = omg.getAppObject();
        List aList = new ArrayList();
        if (obj instanceof StationData) {
            AbstractAction action = null;
            if (omg instanceof Location) {
              final Location loc = (Location) omg;
              JMenuItem jmi = new JMenuItem("Recenter");
              jmi.addActionListener( new ActionListener() {
                  public void actionPerformed(java.awt.event.ActionEvent e) {
                      MapHandler mh = (MapHandler) getBeanContext();
                      if (mh != null) {
                          JiggleMapBean mapBean =
                                  (JiggleMapBean) mh.get("org.trinet.jiggle.JiggleMapBean");
                          if (mapBean != null) {
                              LatLonPoint llp = LatLonPoint.valueOf(loc.lon, loc.lat);
                              mapBean.setCenter(llp);
                          }
                      }
                  }
              });
              aList.add(jmi);
              final JCheckBoxMenuItem cb = new JCheckBoxMenuItem();
              action   = new AbstractAction("ShowName") {
                  public void actionPerformed(ActionEvent e) {  
                      loc.setShowName(cb.getState());
                  }
              };
              cb.setAction(action);
              cb.setState(loc.isShowName());
              aList.add(cb);

              action = new AbstractAction("ShowDetails") {
                  public void actionPerformed(ActionEvent e) {  
                      loc.setDetails(((StationData)loc.getAppObject()).toHtmlString());
                      loc.showDetails();
                  }
              };
              jmi = new JMenuItem(action);
              aList.add(jmi);

              aList.add(new JSeparator());
            }

            // List menu of channel names, selecting one sets selected wfv zoom panel
            StationData sid = (StationData) obj;
            ChannelableList ch = sid.getChannelables();
            for (int i = 0; i < ch.size(); i++) {
               final String str = ((Channelable)ch.get(i)).getChannelObj().toDelimitedSeedNameString();
               action   = new AbstractAction(str) {
                   public void actionPerformed(ActionEvent e) {  
                       jiggle.setPickPanelToChannelStartingWith(str);
                   }
               };
               JMenuItem jmi = new JMenuItem(action);
               jmi.setToolTipText("Select wf panel in Jiggle view");
               aList.add(jmi);
            }
            if (!ch.isEmpty()) {
                aList.add(new JSeparator());
            }
            // no-op return option
            aList.add(new JMenuItem("Cancel"));
        }
        return aList;
    }

    private HashMap parseStnHistory() {
        HashMap aMap = new HashMap(1000);
        Connection conn =  DataSource.getConnection();
        if (com.bbn.openmap.util.Debug.debugging("station"))
            System.out.println(" DEBUG station parseStnHistory have Connection: " + (conn != null));
        if (conn == null) return aMap;

        StnHistory sh = null;
        try {
          Statement s = conn.createStatement();
          ResultSet rs = s.executeQuery("SELECT sta,net,ondate,offdate from " + STATION_DATA_TABLE);
          if (rs != null) {
            while (rs.next()) {
              sh = new StnHistory();
              sh.sta = rs.getString(1);
              sh.net = rs.getString(2);
              // Avoid string format get UTC time like this: -aww 2009/09/23
              sh.dr = new DateRange(rs.getDate(3, Calendar.getInstance(TimeZone.getTimeZone("UTC"))),
                                    rs.getDate(4, Calendar.getInstance(TimeZone.getTimeZone("UTC"))));
              aMap.put(sh.getMapKey(), sh);
            } 
            rs.close();
          }
          s.close();
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        if (com.bbn.openmap.util.Debug.debugging("station"))
            System.out.println(" DEBUG station parseStnHistory END aMap.size() : " + aMap.size());
        return aMap;
    }

    private boolean stationOnForDate(StationData sid, java.util.Date date) {
        if (sid == null) return false;
        if (date == null || stnHistory == null) return true;
        if (stnHistory.size() == 0) return true;
        StnHistory sh = (StnHistory) stnHistory.get(sid.getMapKey());
        if (sh == null) return true;
        return sh.dr.contains(date);
    } 

    private void initStnHistory() {
        stnHistory = parseStnHistory();
    }
/*
    //private static final String cs_sql = "{ ? = station_on(?, ?, ?) }";
    //private CallableStatement cs = null;

        Connection conn = DataSource.getConnection();
        if (conn != null) {
            cs = conn.prepareStatement(cs_sql);
            cs.registerOutParameter(1, java.sql.Types.INTEGER);
        }
        boolean tf = true;
        if (cs != null) {
                    cs.setParameter(2, sid.getSta());
                    cs.setParameter(3, sid.getNet());
                    cs.setParameter(4, solDate);
                    cs.execute();
                    tf = (cs.getInt(1) > 0);
        }
*/
}
