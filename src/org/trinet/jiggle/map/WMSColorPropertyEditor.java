// **********************************************************************
// Adapted from com/bbn/openmap/util/propertyEditor/ColorPropertyEditor.java
// Revision: 1.3.2.3 Date: 2004/10/14 18:27:47 Author: dietrick 
// Because JButton not declared protected couldn't override its class methods -aww
// 
// **********************************************************************
package org.trinet.jiggle.map;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Paint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyEditorSupport;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

import com.bbn.openmap.omGraphics.DrawingAttributes;
import com.bbn.openmap.omGraphics.OMColor;
import com.bbn.openmap.omGraphics.OMColorChooser;
import com.bbn.openmap.tools.icon.IconPartList;
import com.bbn.openmap.tools.icon.OMIconFactory;
import com.bbn.openmap.tools.icon.OpenMapAppPartCollection;
import com.bbn.openmap.util.ColorFactory;
import com.bbn.openmap.util.Debug;

/**
 * A PropertyEditor that brings up a JFileChooser panel to select a
 * file.
 */
public class WMSColorPropertyEditor extends PropertyEditorSupport {

    /** The Component returned by getCustomEditor(). */
    protected JButton button; // make access protected -aww

    public final static String title = "Select color...";
    protected int icon_width = 20;
    protected int icon_height = 20;

    /** Create FilePropertyEditor. */
    public WMSColorPropertyEditor() {
        button = new JButton(title);
    }

    //
    //  PropertyEditor interface
    //

    /**
     * PropertyEditor interface.
     * 
     * @return true
     */
    @Override
    public boolean supportsCustomEditor() {
        return true;
    }

    /**
     * Returns a JButton that will bring up a JFileChooser dialog.
     * 
     * @return JButton button
     */
    @Override
    public Component getCustomEditor() {
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                Color startingColor;
                try {
                    startingColor = ColorFactory.parseColor(getAsText(), true);
                } catch (NumberFormatException nfe) {
                    startingColor = OMColor.clear;
                }

                Color color = OMColorChooser.showDialog(button,
                        title,
                        startingColor);

                WMSColorPropertyEditor.this.setValue(color);
            }
        });

        JPanel panel = new JPanel();
        GridBagLayout gridbag = new GridBagLayout();
        GridBagConstraints c = new GridBagConstraints();
        panel.setLayout(gridbag);
        gridbag.setConstraints(button, c);
        panel.add(button);

        return panel;
    }

    public ImageIcon getIconForPaint(Paint paint, boolean fill) {

        if (paint == null)
            paint = Color.black;

        DrawingAttributes da = new DrawingAttributes();
        da.setLinePaint(paint);
        da.setStroke(new BasicStroke(2));
        if (fill) {
            da.setFillPaint(paint);
        }

        OpenMapAppPartCollection collection = OpenMapAppPartCollection.getInstance();
        IconPartList parts = new IconPartList();

        if (paint instanceof Color || paint == OMColor.clear) {
            Color color = (Color) paint;
            Color opaqueColor = new Color(color.getRed(), color.getGreen(), color.getBlue());
            DrawingAttributes opaqueDA = new DrawingAttributes();
            opaqueDA.setLinePaint(opaqueColor);
            opaqueDA.setStroke(new BasicStroke(2));

            if (fill) {
                opaqueDA.setFillPaint(opaqueColor);
            }

            parts.add(collection.get("LR_TRI", opaqueDA));
            parts.add(collection.get("UL_TRI", da));
        } else {
            parts.add(collection.get("BIG_BOX", da));
        }

        return OMIconFactory.getIcon(icon_width, icon_height, parts);
    }

    /** Implement PropertyEditor interface. */
    @Override
    public void setValue(Object someObj) {

        if (someObj == null) {
            setButtonForColor(Color.black);
        } else if (someObj instanceof Color) {
            setButtonForColor((Color) someObj);
        } else if (someObj instanceof String) {
            Color color = OMColor.clear;
            try {
                color = ColorFactory.parseColor((String) someObj, true);
            } catch (NumberFormatException nfe) {
                Debug.output("WMSColorPropertyEditor.setValue: " + someObj + "\n"
                        + nfe.getMessage());
            }

            setButtonForColor(color);
        }
    }

    // Replaced with WMS string convention for OxRRGGBB -aww
    protected void setButtonForColor(Color color) {
        button.setIcon(getIconForPaint(color, true));
        String val = Integer.toHexString(color.getRGB());
        int l = val.length();
        if (l < 8) { // leading zeroes stripped off
            StringBuffer sb = new StringBuffer();
            for (int i =0; i<8-l; i++) { // pad
                sb.append("0");
            }
            sb.append(val);
            val = sb.toString(); 
        }
        val = "0x" + val.substring(2);
        //System.out.println("DEBUG WMSColorPropertyEditor: " +val);
        button.setText(val);
    }


    /** Implement PropertyEditor interface. */
    @Override
    public String getAsText() {
        return button.getText();
    }
}
