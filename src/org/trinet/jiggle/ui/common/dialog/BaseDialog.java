package org.trinet.jiggle.ui.common.dialog;

import org.trinet.jiggle.common.JiggleConstant;
import org.trinet.util.graphics.IconImage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public abstract class BaseDialog extends JDialog {
    protected boolean dialogCanceled;

    public BaseDialog(Dialog owner, boolean modal) {
        super(owner, modal);
        init();
    }

    public BaseDialog(Frame owner, boolean modal) {
        super(owner, modal);
        init();
    }

    public BaseDialog(Window owner, ModalityType modalityType) {
        super(owner, modalityType);
        init();
    }

    abstract protected void btnCancelActionPerformed(ActionEvent evt);

    private void init() {
        this.setIconImage(IconImage.getImage(JiggleConstant.Icons.JiggleLogoIcon));

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                btnCancelActionPerformed(null);
            }
        });
    }
}
