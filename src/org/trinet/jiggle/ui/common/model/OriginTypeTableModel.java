package org.trinet.jiggle.ui.common.model;

import java.util.ArrayList;
import org.trinet.jiggle.common.type.OriginType;
import org.trinet.jiggle.common.type.OriginTypeList;

public class OriginTypeTableModel extends BaseTableModel {
    
    private final int idxType = 0;
    private final int idxDesc = 1;
    private final int idxDefault = 2;
    
    public OriginTypeTableModel(String[] colNames) {
        super();
        this.cache = new ArrayList();
        this.headers = colNames;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        OriginType data = (OriginType) cache.get(rowIndex);
        
        switch (columnIndex) {
            case idxType: return data.getCode();
            case idxDesc: return data.getDesc();
            case idxDefault: return (data.isDefault() ? "Yes" : "No");
            default: return "Unknown Index";
        }
    }
    
    public void loadData(OriginTypeList originList){
        cache = new ArrayList();
        colCount = this.headers.length;
        cache.addAll(originList.getTypeList());
    }
    
    public OriginType getDataAtRow(int row){
        return (OriginType) cache.get(row);
    }
    
    public void removeRow(int row) {
        cache.remove(row);
        this.fireTableRowsDeleted(row,row);
    }

    public boolean addRow(OriginType originType){
        // capitalize origin type to keep it in sync with most of records in db
        originType.setCode(originType.getCode().toUpperCase());

        boolean status = this.cache.add(originType);
        this.fireTableRowsInserted(this.cache.indexOf(originType),this.cache.indexOf(originType));
        return status;
    }

    public void updateRow(OriginType originType){
        // only description can be edited
        int idxRecord = this.cache.indexOf(originType);
        cache.set(idxRecord, originType);
        this.fireTableRowsUpdated(idxRecord,idxRecord);
    }

}
