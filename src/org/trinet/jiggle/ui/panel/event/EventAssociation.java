package org.trinet.jiggle.ui.panel.event;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import org.trinet.jasi.Solution;
import org.trinet.jasi.SolutionList;
import org.trinet.jiggle.common.JiggleSingleton;
import org.trinet.jiggle.common.LogUtil;
import org.trinet.jiggle.common.type.AssocEvent;
import org.trinet.jiggle.common.type.SolutionCoreType;
import org.trinet.jiggle.database.event.EventAssocManager;
import org.trinet.jiggle.ui.panel.base.MultiSelectPanel;

public class EventAssociation extends javax.swing.JPanel implements ItemListener {

    private SolutionList allMasterViewEvents;
    private boolean isNew;
    private Solution[] selectedSolution;
    private List<AssocEvent> originalAssocList;

    /**
     * Creates new form EventAssociation
     */
    public EventAssociation(SolutionList allEventsLoaded, Solution[] selectedSolution, boolean isNew) {
        initComponents();
        this.allMasterViewEvents = allEventsLoaded;
        this.isNew = isNew;
        this.selectedSolution = selectedSolution;

        initPanel(this.selectedSolution);
        setMasterEvent(null);                

        if (this.isNew) {
            // For new association, select the first one if there is only one
            if (selectedSolution != null && this.selectedSolution.length == 1) {
                setMasterEvent(this.selectedSolution[0]);
            } else {
                // when more than one, add all to the selected events
                for (Solution data : this.selectedSolution) {
                    this.addMspSolutionToSelected(data);
                }
            }
        } else {
            // Editing association
            if (this.selectedSolution != null) {
                setMasterEvent(this.selectedSolution[0]);
            }
            this.getCmbMaster().setEnabled(false);
            this.getCmbMaster().setEditable(true);
            
            //in edit mode - disable master selection. Workaround to make the text more visible
            JTextField txtMaster = ((JTextField)this.getCmbMaster().getEditor().getEditorComponent());                        
            txtMaster.setDisabledTextColor(UIManager.getColor("ComboBox.foreground"));
            txtMaster.setBackground(UIManager.getColor("ComboBox.background"));
            
            loadSelectedEvents();
        }
        
        // Delete all events that are in the database to prevent database unique constraint issue
        removeUsedEvents(selectedSolution);
        removeTimeRangeEvents(selectedSolution);
    }
    
    /**
     * Check for Time range eventSelectionProperties and remove those rows that within the range.
     */
    private void removeTimeRangeEvents(Solution[] selectedSolution) {
        int timeRange = JiggleSingleton.getInstance().getEventProp().getEventAssociationTimeRange();

        // time range prop is used and a master has been selected
        if (timeRange > 0) {        
            timeRange = timeRange * 60; //convert to seconds
            
            // should be the same when only 1 master event
            double masterMinTime = getMasterEventTimeMin(selectedSolution);
            double masterMaxTime = getMasterEventTimeMax(selectedSolution);
            
            // start/end range of the records. Inclusive on both ends
            double startRange = masterMinTime - timeRange;
            double endRange = masterMaxTime + timeRange;
            
            boolean isValid = true;
            Solution solution = null;
            for (int idx=this.getMultiSelectPanel().getAllListModel().size()-1; idx >= 0 ; idx--) {
                isValid = true;
                solution = ((SolutionCoreType)this.getMultiSelectPanel().getAllListModel().get(idx)).getSolutionType();
                
                LogUtil.debug(JiggleSingleton.getInstance().isDebug(), "startRange: " + startRange + ", Solution: " + solution.getTime() + ", endRange: " + endRange);                
                
                // Remove a solution in "All list" if not within a time range
                if (solution.getTime() < startRange || solution.getTime() > endRange) {
                    isValid = false;
                }
                
                if (!isValid) {
                    // Remove if solution is not within the range
                    this.getMultiSelectPanel().getAllListModel().remove(idx);
                }
            }            
        }
    }

    /**
     * Get list of event ids that are already used in association by selected "master event"
     * If there are multiple master events, used events associated to all master events are removed
     */
    private Set<Long> getEventAlreadyAssociated(Solution[] selectedSolution) {
        Set<Long> usedEvents;
        if (selectedSolution != null) {
            if (selectedSolution.length == 1) {
                usedEvents = EventAssocManager.getEventAssoc().getAllEventId(selectedSolution[0].getId().longValue());
            } else {
                usedEvents = new HashSet<>();
                for (Solution data : selectedSolution) {
                    usedEvents.addAll(EventAssocManager.getEventAssoc().getAllEventId(data.getId().longValue()));
                }
            }
        } else {
            usedEvents = Collections.emptySet();
        }
        return usedEvents;
    }

    /** Get earliest master event time
     */
    private double getMasterEventTimeMin(Solution[] selectedSolution) {
        double masterTime = 0;
        if (selectedSolution != null) {
            if (selectedSolution.length == 1) {
                masterTime = selectedSolution[0].getTime();
            } else {
                masterTime = selectedSolution[0].getTime();
                for (Solution data : selectedSolution) {
                    if (data.getTime() < masterTime) {
                        masterTime = data.getTime();
                    }
                }

            }
        }
        return masterTime;
    }

    /** Get the most recent master event time
     */
    private double getMasterEventTimeMax(Solution[] selectedSolution) {
        double masterTime = 0;
        if (selectedSolution != null) {
            if (selectedSolution.length == 1) {
                masterTime = selectedSolution[0].getTime();
            } else {
                masterTime = selectedSolution[0].getTime();
                for (Solution data : selectedSolution) {
                    if (data.getTime() > masterTime) {
                        masterTime = data.getTime();
                    }
                }            
            }
        }
        
        return masterTime;
    }
    
    /**
     * Remove events that are already used in existing association from the available list
     **/
    private void removeUsedEvents(Solution[] selectedSolution){
        Set<Long> existingEventIdList = getEventAlreadyAssociated(selectedSolution);
        
        SolutionCoreType solution = null;
        for (int idx=this.getMultiSelectPanel().getAllListModel().size()-1; idx >= 0 ; idx--) {
            // removing from the end of the list as it affects the list model
            solution = (SolutionCoreType)this.getMultiSelectPanel().getAllListModel().get(idx);
            if (existingEventIdList.contains(solution.getEventId().longValue())) {
                this.getMultiSelectPanel().getAllListModel().remove(idx);
            }
        }
    }
    
    /**
     * Load events to selected list
     */
    private void loadSelectedEvents() {
        List<AssocEvent> associatedEvents = EventAssocManager.getEventAssoc().getAssociatedEvents(this.selectedSolution[0].getId().longValue());
        
        // record the original list of association
        this.setOriginalAssocList(associatedEvents);
        
        AssocEvent assocEventRow = null;
        for (int iSelected = 0; iSelected < associatedEvents.size(); iSelected++) {
            assocEventRow = associatedEvents.get(iSelected);
            
            SolutionCoreType allEventRow = null;
            boolean isEventFound = false;
            for (int iAll = 0; iAll < this.getMultiSelectPanel().getAllListModel().size(); iAll++) {
                allEventRow = (SolutionCoreType)this.getMultiSelectPanel().getAllListModel().getElementAt(iAll);
                
                if (allEventRow.getEventId().longValue() == assocEventRow.getAssocEventId()) {
                    // set assocEvent type when a matching row is found to persist comment id
                    allEventRow.setAssocEvent(assocEventRow);
                    
                    // add row to selected
                    this.getMultiSelectPanel().getSelectedListModel().addElement(allEventRow);
                    
                    // remove element from available list
                    this.getMultiSelectPanel().getAllListModel().removeElement(allEventRow);
                    
                    isEventFound = true;
                }
            }
            
            if (!isEventFound) {
                // Event may not be found if the event is not in the catalog view. Create a new row in selected.
                allEventRow = new SolutionCoreType(Solution.create().getById(assocEventRow.getAssocEventId()));
                allEventRow.setAssocEvent(assocEventRow);
                
                // add row to selected
                this.getMultiSelectPanel().getSelectedListModel().addElement(allEventRow);                
            }
        }
    }
    
    /**
     * Set the master event in the combobox
     * @param solution 
     */
    private void setMasterEvent(Solution solution){
        if (solution != null ) {
            this.getCmbMaster().setSelectedItem(new SolutionCoreType(solution));                
        } else this.getCmbMaster().setSelectedIndex(-1);
    }
    
    /**
     * Initialize event association panel
     * @param solutionList 
     */
    private void initPanel(Solution[] solutionList) {
        this.getMultiSelectPanel().setBorder(BorderFactory.createTitledBorder("Select event(s) to associate to the master event"));
        this.getMultiSelectPanel().setAvailablePanelTitle("Available Events");
        this.getMultiSelectPanel().setSelectedPanelTitle("Selected Events");
                
        loadMasterList(solutionList);
        loadAllEvents();
        
        this.getCmbMaster().addItemListener(this);
    }
    
    /**
     * Load items in master list
     * @param solutionList 
     */
    protected void loadMasterList(Solution[] solutionList) {
        this.getCmbMaster().removeAllItems();
        
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        for (Solution data : solutionList) {
            model.addElement(new SolutionCoreType(data));
        }
        this.getCmbMaster().setModel(model);
        this.getCmbMaster().setSelectedIndex(-1);
    }
    
    /**
     * Get the multi-select panel component
     * @return 
     */
    public MultiSelectPanel getMultiSelectPanel() {
        return this.mspList;
    }

    /**
     * Load the available list box
     */
    protected void loadAllEvents() {
        for (Object data : this.getAllMasterViewEvents()) {
            this.getMultiSelectPanel().getAllListModel().addElement(new SolutionCoreType((Solution)data));
        }
    }
    
    public SolutionCoreType getSelectedMasterEvent() {
        return (SolutionCoreType)this.getCmbMaster().getSelectedItem();
    }
    
    /**
     * Get all user selected events from the selected list view
     * @return 
     */
    public ArrayList<SolutionCoreType> getSelectedEvents() {
        ArrayList<SolutionCoreType> result = new ArrayList<>();
        for (int idx = 0; idx < this.getMultiSelectPanel().getSelectedListModel().size(); idx++) {
            result.add((SolutionCoreType)this.getMultiSelectPanel().getSelectedListModel().getElementAt(idx));
        }
        return result;
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mspList = new org.trinet.jiggle.ui.panel.base.MultiSelectPanel();
        jLabel1 = new javax.swing.JLabel();
        cmbMaster = new javax.swing.JComboBox<>();

        jLabel1.setText("Master Event:");

        cmbMaster.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbMaster.setOpaque(true);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(mspList, javax.swing.GroupLayout.DEFAULT_SIZE, 581, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cmbMaster, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmbMaster, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mspList, javax.swing.GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> cmbMaster;
    private javax.swing.JLabel jLabel1;
    private org.trinet.jiggle.ui.panel.base.MultiSelectPanel mspList;
    // End of variables declaration//GEN-END:variables

    /**
     * @return the allEvents
     */
    public SolutionList getAllMasterViewEvents() {
        return allMasterViewEvents;
    }

    /**
     * @param allMasterViewEvents the allEvents to set
     */
    public void setAllMasterViewEvents(SolutionList allMasterViewEvents) {
        this.allMasterViewEvents = allMasterViewEvents;
    }

    /**
     * @return the cbxMaster
     */
    public javax.swing.JComboBox<String> getCmbMaster() {
        return cmbMaster;
    }

    /**
     * @param cbxMaster the cbxMaster to set
     */
    public void setCmbMaster(javax.swing.JComboBox<String> cbxMaster) {
        this.cmbMaster = cbxMaster;
    }
    
    /**
     * Monitor master event combo box selection changes and update available/selected events accordingly
     */
    @Override
    public void itemStateChanged(ItemEvent e) {
        SolutionCoreType solution = (SolutionCoreType)e.getItem();
        
        if (e.getStateChange() == ItemEvent.DESELECTED) {
            // Add to available
            this.getMultiSelectPanel().getAllListModel().addElement(solution);
        } else if (e.getStateChange() == ItemEvent.SELECTED) {
            // Remove from available and selected
            removeSolutionFromSelectionPanel(solution);
        }        
    }

    /**
     * @param mspList the mspList to set
     */
    public void setMspList(org.trinet.jiggle.ui.panel.base.MultiSelectPanel mspList) {
        this.mspList = mspList;
    }
    
    /**
     * Add an event from available to selected
     * @param solution 
     */
    protected void addMspSolutionToSelected(Solution solution) {
        // add solution to selected and remove from available
        SolutionCoreType core = new SolutionCoreType(solution);
        this.getMultiSelectPanel().getSelectedListModel().addElement(core);
        this.getMultiSelectPanel().getAllListModel().removeElement(core);
    }
    
    /**
     * Remove from selected, but add to available
     * @param solution 
     */
    protected void removeMspSolutionFromSelected(Solution solution) {
        // remove from selected and add back to available
        SolutionCoreType core = new SolutionCoreType(solution);
        this.getMultiSelectPanel().getSelectedListModel().removeElement(core);
        this.getMultiSelectPanel().getAllListModel().addElement(core);
    }
 
    /**
     * Remove the event from both available/selected
     * @param solution 
     */
    protected void removeSolutionFromSelectionPanel(SolutionCoreType solution ) {
        this.getMultiSelectPanel().getAllListModel().removeElement(solution);
        this.getMultiSelectPanel().getSelectedListModel().removeElement(solution);                    
    }

    /**
     * @return the originalAssocList
     */
    public List<AssocEvent> getOriginalAssocList() {
        return originalAssocList;
    }

    /**
     * @param originalAssocList the originalAssocList to set
     */
    public void setOriginalAssocList(List<AssocEvent> originalAssocList) {
        this.originalAssocList = originalAssocList;
    }
}
