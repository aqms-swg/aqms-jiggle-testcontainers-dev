package org.trinet.mung;
import org.trinet.util.graphics.table.*;
import org.trinet.jasi.*;
import org.trinet.util.*;

public class MungDefaultSolutionListParser extends MungAbstractSolutionListParser {

  public MungDefaultSolutionListParser() {
    initParser();
  }
  public MungDefaultSolutionListParser(GenericPropertyList gpl) {
    super(gpl);
    initParser();
  }

  public void setProperties(GenericPropertyList gpl) {
      super.setProperties(gpl);
      ampParser.setProperties(gpl);
      phaseParser.setProperties(gpl);
      codaParser.setProperties(gpl);
      magParser.setProperties(gpl);
      solParser.setProperties(gpl);
  }

  protected void initParser() {
    ampParser = new DefaultAmpTextParser();
    jaltm = new JasiAmpListTableModel();
    ( (JasiTableRowTextParserIF) ampParser).setRowModel(jaltm);

    phaseParser = new DefaultPhaseTextParser();
    jpltm = new JasiPhaseListTableModel();
    ( (JasiTableRowTextParserIF) phaseParser).setRowModel(jpltm);

    codaParser = new DefaultCodaTextParser();
    jcltm = new JasiCodaListTableModel();
    ( (JasiTableRowTextParserIF) codaParser).setRowModel(jcltm);

    magParser = new DefaultMagnitudeTextParser();
    jmltm = new JasiMagListTableModel();
    ( (JasiTableRowTextParserIF) magParser).setRowModel(jmltm);

    solParser =  new DefaultSolutionTextParser();
    JasiSolutionListTableModel jsltm = new JasiSolutionListTableModel();
    ( (JasiTableRowTextParserIF) solParser).setRowModel(jsltm);

  }
}

