package org.trinet.mung;
import org.trinet.jasi.*;
import org.trinet.util.locationengines.*;
/**
 * Calculate a location using the remote location server. DO NOT USE THIS CLASS !
 * @deprecated
 */
public class MungLocationEngine extends LocationEngineHypoInverse {
  public MungLocationEngine() {
    super();
  }
  public MungLocationEngine(String serverIPAddress, int serverPort) {
    super(serverIPAddress, serverPort);
  }
  protected int copyDependentData(Phase newPhase, PhaseList phaseList) {
    if (debug) System.out.println("DEBUG "+getClass().getName()+"\n copyDependentData input  Phase= "+newPhase.toString());
    // removed useLoc toggle 12/02/2005 to be consistent  -aww
    //boolean saveLocSetting =  ChannelName.useLoc; // KLUDGE to handle channel matching see toggle below
    //ChannelName.useLoc = false; // don't use the site location, not known in newly parsed phase from hypoinv until server hypoinverse code is updated
    //No required check of sol association 
    Phase oldPhase = (Phase) phaseList.getLikeWithSameTime(newPhase);
    //ChannelName.useLoc = saveLocSetting; // KLUDGE to handle hypoinverse channel matching
    //
    if (oldPhase == null)  {
      if (debug) {
        System.out.println(getClass().getName() +
          " ERROR copyDependentData code/data corrupt, no matching phase found in input phaseList size: "
          + phaseList.size());
      }
      return 0;   // no match
       
    }
    // copy stuff that depends on the solution (dist, rms, etc.)
    oldPhase.copySolutionDependentData(newPhase);
    if (debug) System.out.println (" copyDependentData output Phase= "+ oldPhase.toString());
    if (! oldPhase.getChannelObj().hasLatLonZ() ) {
        System.out.println(" WARNING! LocationEngine phase has no LatLonZ: Missing dist,az commit data:\n  "+ oldPhase.toString());
        //oldPhase.matchChannelWithDataSource();
    }
    return 1;
  }
  protected boolean sendPhaseList(PhaseList phaseList) {
    boolean status = false;
    /* DEPRECATED CLASS DON'T ALLOW USAGE -AWW
    int count = phaseList.size();
    status = true
    if (debug) System.out.println ("DEBUG MungLocationEngine sendPhaseList oldPhases:\n"+ phaseList.toNeatString());
    // for reset of deleted phase attributes (those not from this current solve for solution)
    Phase blankPhase = (count > 0) ? Phase.create() : null;
    for (int i = 0; i < count; i++) {
      Phase aPhase = (Phase) phaseList.get(i);
      if (aPhase.isDeleted()) {
        Channel chan = (Channel) aPhase.getChannelObj();
        chan.setDistanceBearingNull();
        // don't need to chan.clone() above, if setChannelObj belowjusts sets values; - aww 
        blankPhase.setChannelObj(chan);
        aPhase.copySolutionDependentData(blankPhase); //reset values
        continue;    // skip deleted phases after reset
      }
      if (aPhase.getAssociatedSolution() == currentSol) {
        String str = HypoFormat.toArchiveString(aPhase);
        // PRIVATE METHOD in parent: 
         status |= writeMessage(str); // write phase data to the solserver socket

        if (debug) System.out.println (str);
      }
    }
    */
    return status;
  }
}
