package org.trinet.pcs;

import java.util.concurrent.locks.*;
import org.trinet.jasi.*;
import org.trinet.jiggle.common.NullReadWriteLock;

public abstract class AbstractChannelListProcessingController extends AbstractSolutionProcessingController
    implements JasiChannelListProcessingControllerIF {
    /**
     * Create a lock.
     * 
     * @return a lock.
     * @see #setChanListLock(ReadWriteLock)
     */
    protected static ReadWriteLock createLock() {
        return new ReentrantReadWriteLock(true);
    }

    /**
     * The controller channel list, which should only be accessed while locked if
     * the lock is specified.
     * 
     * @see #setChanListLock(ReadWriteLock)
     */
    private ChannelList _cntrlChanList = null;
    /**
     * The the channel list lock or <code>NullReadWriteLock</code> if none, not
     * null.
     * 
     * @see #setChanListLock(ReadWriteLock)
     */
    private ReadWriteLock chanListLock = NullReadWriteLock.getInstance();
  
    protected AbstractChannelListProcessingController () {}

    protected AbstractChannelListProcessingController(String appName, String groupName, String threadName, String stateName) {
        this(appName, groupName, threadName, stateName, DEFAULT_THREAD_SLEEP_MILLIS,  null);
    }
    protected AbstractChannelListProcessingController(String appName, String groupName, String threadName, String stateName,
                                           int defaultSleepTimeMillis) {
        this(appName, groupName, threadName, stateName, defaultSleepTimeMillis,  null);
    }
    protected AbstractChannelListProcessingController (String appName, String groupName, String threadName, String stateName,
                                                 int defaultSleepTimeMillis, JasiSolutionChannelProcessorIF module) {
        super(appName, groupName, threadName, stateName, defaultSleepTimeMillis);
    }

    /**
     * Set the channel list lock. The default is no read write lock.
     * 
     * @param lock the lock or null if none.
     * @see #createLock()
     */
    protected void setChanListLock(ReadWriteLock lock) {
        if (lock == null) {
            lock = NullReadWriteLock.getInstance();
        }
        chanListLock = lock;
    }

    /**
     * Get the channel list read lock.
     * 
     * @return the channel list read lock or null if none.
     * @see #setChanListLock(ReadWriteLock)
     */
    protected Lock getChanListReadLock() {
        return chanListLock.readLock();
    }

    /**
     * Get the channel list write lock.
     * 
     * @return the channel list write lock or null if none.
     * @see #setChanListLock(ReadWriteLock)
     */
    protected Lock getChanListWriteLock() {
        return chanListLock.writeLock();
    }

    /** Setups datasource from properties, creates ChannelList for processing if none is 
     *  defined,then processes all posted event ids.
     *  Processing module and mode must already have been configured 
     *  for instance from settings or properties.
     *  @see AbstractProcessingController.processIds()
     */
    protected int setupForProcessing() {

        int resultCode = super.setupForProcessing();
        if (resultCode < 1) return resultCode;

        // For case where a channel list is not needed
        if (props.getBoolean("pcsDisableLoadChannelList")) {
            return ProcessingResult.UNIT_SUCCESS.getIdCode();
        }

        // If processor ChannelList D.N.E., create default masterlist
        ChannelList cntrlChanList = getChannelList();
        if (cntrlChanList == null) {
            loadChannelList(true);
            cntrlChanList = getChannelList();
        }

        // Do channel data exist?
        if (cntrlChanList == null || cntrlChanList.isEmpty()) {
          System.err.println(appName + " WARNING processor has no channel list data; check input properties!");
          return ProcessingResult.NO_CHANNEL_DATA.getIdCode();
        }
        return ProcessingResult.UNIT_SUCCESS.getIdCode();
    }

    public void setProcessingModule(JasiSolutionProcessorIF module) {
        super.setProcessingModule(module);
        final ChannelList cntrlChanList = getChannelList();
        if (module != null && cntrlChanList != null)
          ((JasiSolutionChannelProcessorIF)module).setChannelList(cntrlChanList);
    }

    @Override
    public boolean hasChannelList() {
        return (getChannelList() != null);
    }

    /** Set the ChannelList of the processing module to the input list.
     * @see #setProcessingModule(JasiSolutionProcessorIF)
     */
    @Override
    public final void setChannelList(final ChannelList chanList) {
        setChannelList(chanList, false, false);
    }

    /** Set the ChannelList of the processing module to the input list.
     * @see #setProcessingModule(JasiSolutionProcessorIF)
     */
    public void setChannelList(final ChannelList chanList, final boolean writeToCache, boolean isMaster) {
        final Lock lock = getChanListWriteLock();
        if (lock != null) {
            lock.lock();
        }
        try {
            _cntrlChanList = chanList;
            if (chanList == null)
                return;

            if (!chanList.hasLookupMap())
                chanList.createLookupMap();
            if (module != null)
                ((JasiSolutionChannelProcessorIF) module).setChannelList(chanList);
            if (writeToCache) {
                chanList.writeToCache();
            }
            if (isMaster) {
                MasterChannelList.set(chanList);
            }
        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }

    @Override
    public ChannelList getChannelList() {
        final Lock lock = getChanListReadLock();
        if (lock != null) {
            lock.lock();
        }
        try {
            return _cntrlChanList;
        } finally {
            if (lock != null) {
                lock.unlock();
            }
        }
    }
    /** Populates ChannelList with Channels found in file cache specified by configuration property 
     * <i>channelCacheFilename</i> otherwise adds to new list those Channels active in the database
     * on current date or date specified by the configuration property <i>channelLoadDate</i>.
     * If <i>isMaster</i>=<i>true</i>, the MasterChannelList is also set to the loaded ChannelList.
     */
    @Override
    public boolean loadChannelList(boolean isMaster) {

        if (props.getBoolean("pcsDisableLoadChannelList")) return false;

        StringBuffer sb = new StringBuffer(512);
        String cacheFileName = "";
        sb.append(appName + " INFO: Reading Channels from data source: ").append(DataSource.getDbaseName());
        if (props.isSpecified("channelCacheFilename")) {
          cacheFileName = props.getUserFileNameFromProperty("channelCacheFilename");
          sb.append(" channelCacheFileName: ").append(cacheFileName);
        }
        org.trinet.util.DateTime date = null;
        if (props.isSpecified("channelLoadDate")) {  // is there a channel load date ?
          date = props.getDateTime("channelLoadDate");
          sb.append(" loadDate: ").append(date);
        }
        
        // Must be defined or no channels are found, no default group name
        String channelListName = props.getChannelGroupName();

        System.out.println(sb.toString());

        ChannelList chList = ChannelList.smartLoad(date, channelListName);
        if (chList == null) return false;
        
        // create HashMap of channels in ChannelList, if not already one
        if (! chList.hasLookupMap()) chList.createLookupMap();
        System.out.println(appName + "INFO: >>> Loaded ChannelList total channels: " + chList.size());

        setChannelList(chList, false, isMaster);

        return (chList.size() > 0);
    }
}
