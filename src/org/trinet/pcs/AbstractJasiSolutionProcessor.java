package org.trinet.pcs;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.Lines;

public abstract class AbstractJasiSolutionProcessor implements JasiSolutionProcessorIF {
    protected boolean debug = false;
    protected boolean verbose = false;

    protected String hostAppRole = "unknown";
    protected String appName = "unknown";

    protected SolutionProcessingMode defaultProcessingMode = SolutionProcessingMode.UNKNOWN;
    protected SolutionProcessingMode processingMode = null;

    protected SolutionEditorPropertyList props = null;

    protected HashMap modeMap = new HashMap();

    protected JasiSolutionProcessingControllerIF controller = null;

    protected AbstractJasiSolutionProcessor() {
    }

    protected AbstractJasiSolutionProcessor(SolutionEditorPropertyList props) {
        this(props, null); // no default mode
    }

    protected AbstractJasiSolutionProcessor(SolutionEditorPropertyList props, SolutionProcessingMode defaultMode) {
        this.props = props;
        this.defaultProcessingMode = defaultMode; // ??
    }

    public void setProcessingController( JasiSolutionProcessingControllerIF controller) {
        this.controller = controller;
        if (appName != null && appName.equals("unknown") && controller != null) appName = controller.getAppName();
    }
    public JasiSolutionProcessingControllerIF getProcessingController() {
        return controller;
    }

    public boolean hasProcessingModeFor(String name) {
        return (getProcessingModeFor(name) != null);
    }

    public boolean hasProcessingModeFor(int modeId) {
        return (getProcessingModeFor(modeId) != null);
    }

    public SolutionProcessingMode getProcessingModeFor(int modeId) {
        return (SolutionProcessingMode) modeMap.get(Integer.valueOf(modeId));
    }

    public SolutionProcessingMode getProcessingModeFor(String name) {

        if (name == null || name.trim().equals("")) return null;

        Iterator iter = modeMap.values().iterator();
        SolutionProcessingMode mode = null;
        while (iter.hasNext()) {
            mode = (SolutionProcessingMode) iter.next();
            if (mode.getDescription().equals(name)) break;
        }
        return mode;
    }


    public void printKnownProcessingModes() {
        System.out.println("INFO Known Processing modes:\n" + modeMap.toString());
    }

    public int getProcessingModeId() {
        return (processingMode != null) ?
            processingMode.getId() : SolutionProcessingMode.UNKNOWN.getId();
    }

    public boolean setProcessingMode(int modeId) {
        processingMode = getProcessingModeFor(modeId);
        return (processingMode != null) ? true : false;
    }

    public boolean setProcessingMode(String modeName) {
        processingMode = getProcessingModeFor(modeName);
        return (processingMode != null) ? true : false;
    }

    public boolean hasProcessingModeFor(SolutionProcessingMode spm) {
        return (spm == null) ? false : hasProcessingModeFor(spm.getId());
    }

    public boolean setProcessingMode(SolutionProcessingMode spm) {
        return (spm == null) ? false : setProcessingMode(spm.getId());
    }

    public SolutionProcessingMode getProcessingMode() {
        return processingMode;
    }

    public boolean setProperties(SolutionEditorPropertyList props) {
        this.props = props; // Note must be SolutionWfEditor subclass to use scanNoiseType and channelTimeWindowModel props
        return initFromProperties();
    }

    public SolutionEditorPropertyList getProperties() {
        return props;
    }
    public void dumpProperties() {
        if (props != null) props.dumpProperties();
    }
    
    protected boolean initFromProperties() {
        debug = props.getProperty("debug", "false").equalsIgnoreCase("true");
        verbose = debug || props.getProperty("verbose", "false").equalsIgnoreCase("true");
        if (verbose) {

            props.dumpProperties();

            if (debug) {
              List aList = props.getKnownPropertyNames();
              System.out.println("DEBUG: property names KNOWN:\n" + aList);
              System.out.println(Lines.ANGLE_LR_TEXT_LINE);

              aList = props.getUnknownPropertyNames();
              if (aList.size() > 0) {
                System.out.println("INFO: property names UNRECOGNIZED:\n" + aList);
                System.out.println(Lines.ANGLE_LR_TEXT_LINE);
              }
              aList = props.getUndefinedPropertyNames();
              if (aList.size() > 0) {
                System.out.println("INFO: property names UNDEFINED:\n" + aList);
                System.out.println(Lines.ANGLE_RL_TEXT_LINE);
              }
            }
        }

        String appname = props.getProperty("pcsProcessorAppName");
        if (appname != null) setAppName(appname); // overrides controller appName setting

        String name = props.getProperty("pcsProcessorModeName");
        processingMode = (name == null) ? defaultProcessingMode : getProcessingModeFor(name);
        System.out.println("INFO: " + getClass().getName() + " processingMode : " + processingMode.getDescription());

        //int imode = (props.isSpecified("pcsProcessorMode")) ? props.getInt("pcsProcessorMode") : -1;
        //processingMode  = (imode < 0) ? defaultProcessingMode : getProcessingModeFor(imode);

        if (! hasProcessingModeFor(processingMode)) {
            printKnownProcessingModes();
            throw new IllegalStateException("Unknown SolutionProcessingMode, property \"pcsProcessorModeName\": "+
                    name + " maps to: " + processingMode);
        }
        return setProcessingMode(processingMode);
    }

    // Methods for app_host_role table lookup?
    public String getAppName() {
        return appName;
    }

    public void setAppName(String name) {
        appName = name;
    }

    public String getHostAppRole() {
        return hostAppRole;
    }

    public void setHostAppRole(String role) {
        hostAppRole = role;
    }

     //IF
    /** Process event with specified id from database.*/
    public int processSolution(long evid) {
        return processSolution(Solution.create().getById(evid));
    }

    //IF
    abstract public int processSolution(Solution sol);

}
