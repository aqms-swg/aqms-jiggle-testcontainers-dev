package org.trinet.pcs;

import org.trinet.jasi.*;
import org.trinet.util.BenchMark;
import org.trinet.util.DateTime;
import org.trinet.apps.Db2Gmp;

/**
 * Processing Module used in context of a PCS Processing Controller to generate
 * waveform "Request Cards" for events immediately after event declaration.
 * 
 * The 'pcsModuleClassName' property passed to the controller would equal
 * the name of this class.<p>
 * 
 * pcsModuleClassName=org.trinet.pcs.Db2GmpProcessingModule<p>
 *  
 */
public class Db2GmpProcessingModule extends AbstractSolutionChannelDataProcessor {

    // class static data enumeration:
    public static final int DEFAULT_MODE_ID = 0;

    public static final SolutionProcessingMode DEFAULT_MODE = 
           new SolutionProcessingMode("DEFAULT", DEFAULT_MODE_ID);

    // for each mode instance:
    {
        modeMap.put(Integer.valueOf(DEFAULT_MODE_ID), DEFAULT_MODE);
    }
    
    // ////////////////////////////////////////////
    
    private Db2Gmp db2gmp = new Db2Gmp();

    /** No-op Constructor. */
    public Db2GmpProcessingModule() { }

    /**
     * Constructor initializes DummyEngineDelegate with property list and
     * sets TEST_MODE_ID as the mode (default) for processing.
     */
    public Db2GmpProcessingModule(SolutionWfEditorPropertyList props) {
        this(props, DEFAULT_MODE); // what should the default behavior be?
    }

    /**
     * Constructor initializes DummyEngineDelegate with input property list and
     * sets mode (default) for processing to the specified input mode.
     */
    public Db2GmpProcessingModule(SolutionWfEditorPropertyList props, SolutionProcessingMode defaultMode) {
        super(props, defaultMode);
        // Don't call setProperties() here - it will be called by the creator controller
    }

    // This is called by the Controller when the module is instantiated.
    // It is passed the controller's property list.
    // You may put moduel-specific properties either:
    //   1) directly in the main property file OR
    //   2) set a property that is the name of the modules property file (recommended: 'pcsEngineDelegateProps')
    public boolean setProperties(SolutionEditorPropertyList props) {

        super.setProperties(props);

        String localPropFile = props.getUserFileNameFromProperty("pcsEngineDelegateProps");
        if (localPropFile == null) {
        	System.err.println("Bad property, pcsEngineDelegateProps = "+localPropFile);
        	return false;
        }
        // Set properties specific to the Db2Gmp engine
        boolean status =  db2gmp.setPropertiesFile(localPropFile);
        if (!status) {
            System.err.println ("Bad properties file: "+localPropFile);
            return false;
        }
        
        return true;
    }
    
    /** ************************************************************
     *  WHERE THE ACTION IS
     * Process input Solution with the delegate as configured.
     * ************************************************************* */
    public int processSolution(Solution sol)  {
    	
        if (sol == null) {
            return ProcessingResult.NULL_INPUT.getIdCode();
        }
        
    	boolean status = true;
        try {
            status = db2gmp.processSolution( sol.getId().longValue() );
        }       
        catch (Exception ex) {
            ex.printStackTrace(System.err) ;  // print but ignore errors
        }

        return (status) ? ProcessingResult.UNIT_SUCCESS.getIdCode() : ProcessingResult.FAILURE.getIdCode();
    }

    //IF subclass implementation might wrap delegate
    public boolean hasChannelList() { 
    	return false;
    }

    //IF subclass implementation might wrap delegate
    public void setChannelList(ChannelList chanList) {
    }

}
