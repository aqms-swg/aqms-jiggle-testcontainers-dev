package org.trinet.pcs;

  /**
   * Put an data object in the given state/rank.
   */
class GetStates {

  public static void main(String args[]) {

        StateRow sr = new StateRow();
        StateRow [] rows = null;

        switch (args.length) {

        case 3:
          sr.controlGroup = args[0];
          sr.sourceTable  = args[1];
          try {
              sr.id = Long.parseLong(args[2]);
              rows = sr.get(sr.controlGroup, sr.sourceTable, sr.id);
          }
          catch (NumberFormatException ex) {
              sr.state = args[2];
              rows = sr.get(sr.controlGroup, sr.sourceTable, sr.state);
          }
          break;

        case 2:
          sr.controlGroup = args[0];
          sr.sourceTable  = args[1];
          rows = sr.get(sr.controlGroup, sr.sourceTable);    
          break;

        case 1:
          sr.controlGroup = args[0];
          rows = sr.get(sr.controlGroup);    
          break;

        case 0:
          rows = sr.get();    
          break;

        default:
          System.out.println("SYNTAX: getstates <group> [<source>] [<id> || <state>]");
        }

        dumpResults(rows);

    }

    public static void dumpResults(StateRow sr[]) {
      if (sr == null) return;
      for (int i=0; i < sr.length; i++) System.out.println(sr[i].toOutputString());
    }
}
