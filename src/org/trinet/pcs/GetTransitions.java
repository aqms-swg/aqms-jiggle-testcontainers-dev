package org.trinet.pcs;

  /**
   * Put an data object in the given state/rank.
   *  % SYNTAX: GetTransitions <groupOld> <sourceOld> <stateOld> <resultOld>
   */
class GetTransitions {

  public static void main (String args[]) {
    TransitionRow t = new TransitionRow();
    TransitionRow [] tr = null;

    switch (args.length) {

        case 4:
          t.groupOld  = args[0];
          t.sourceOld = args[1];
          t.stateOld  = args[2];
          t.resultOld  = Integer.parseInt(args[3]);
          tr = TransitionRow.get(t.groupOld, t.sourceOld, t.stateOld, t.resultOld);
          break;

        case 3:
          t.groupOld  = args[0];
          t.sourceOld = args[1];
          t.stateOld  = args[2];
          tr = TransitionRow.get(t.groupOld, t.sourceOld, t.stateOld);
          break;

        case 2:
          t.groupOld = args[0];
          t.sourceOld= args[1];
          tr = TransitionRow.get(t.groupOld, t.sourceOld);    
          break;

        case 1:
          t.groupOld = args[0];
          tr = TransitionRow.get(t.groupOld);    
          break;

        case 0:
          tr = TransitionRow.get();    
          break;

        default:
          System.out.println("SYNTAX:  GetTransitions [<groupOld>] [<sourceOld>] [<stateOld>] [<resultOld>] ");
    }

    if (tr != null) TransitionRow.dump(tr);

  }
}
