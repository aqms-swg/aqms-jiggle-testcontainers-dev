package org.trinet.pcs;
import org.trinet.jasi.*;
import org.trinet.jasi.engines.AbstractHypoMagEngineDelegate;
import org.trinet.jasi.magmethods.MagnitudeMethodIF;
// NOTE: configure a module properties file to desired attributes

public class HypoMagProcessingController extends AbstractChannelListProcessingController {

    public HypoMagProcessingController() { }

    public HypoMagProcessingController(String appName, String groupName, String threadName, String stateName) {
        this(appName, groupName, threadName, stateName, null);
    }

    public HypoMagProcessingController(String appName, String groupName, String threadName, String stateName,
                                   String propFileName) {

        super(appName, groupName, threadName, stateName);

        if ( propFileName == null || propFileName.equals("") ) {
          // Load "default" properties file which should be in the
          // subdir below users home named: "."+appName.toLowerCase()
          setProperties((PcsPropertyList)null); 
        }
        else setProperties(propFileName);
    }

    // Need a static handle for oracle db stored procedure implementation 
    public static final int processEvents(String appName, String groupName, String threadName,
                                          String stateName, String propFileName) {
        return processEvents(groupName, threadName, stateName, propFileName, null);
    }

    public static final int processEvents(String appName, String groupName, String threadName,
                                          String stateName, String propFileName, String idFileName) {
        // create a processor instance configure by properties in specified input file
        try (HypoMagProcessingController pc =
                new HypoMagProcessingController(appName, groupName, threadName, stateName, propFileName)) {
            return (idFileName == null) ?
                    pc.setupAndProcessIds() : pc.setupAndProcessIds(idFileName);
        }
    }

    // override here does reconnect to waveservers when running in continuous loop mode
    // failure to connect on process startup results in a WaveClient without servers
    // thus no waveforms are ever loaded, so this attempts to force new connection with each id
    // which for continuous loop mode may be minutes to hours apart
    public int processId(long eventId) {
        if (runMode != 0) {
            SolutionWfEditorPropertyList props = (SolutionWfEditorPropertyList)((HypoMagSolutionProcessor)module).delegate.getDelegateProperties();
            //WaveServerGroup wsg = props.getWaveServerGroup();
            //if (wsg == null || wsg.numberOfServers() < 2) {
                System.out.println("INFO: HypoMagProcessingController configuring WaveServerGroup setup...");
                props.setupWaveServerGroup();
                // alternative is trying props.reset();
            //}
        }
        return super.processId(eventId);
    }

// Test method should be in a class of a user test package e.g. org.trinet.test.TestProcessor
    public static final void main(String [] args) {

        String propFileName     = ""; // "hypomag.props";
        String stateName        = "hypomag";
        String threadName       = "HypoMag";
        String groupName        = "PostProc";
        String user = null;
        String pass = null;
        String idFileName = null;
        long eventId = 0l;
        boolean alreadyPosted = true;

        int nargs = args.length;
        if (nargs < 1 || args[0].equals("?")) {
            System.out.println("args: [state] [propFile] [user] [pwd] [modeFlag] where:");
            System.out.println("  modeFlag = TRUE (ids are already posted in table (default))\n" +
                               "           = FALSE (post ids table by EventProperties filter query)\n" +
                               "           = idFileName in local dir (ascii file 1 id per line)"); 
            System.out.println("defaults:  hypomag hypomag.props null null false");
            return;
        }
        switch (nargs) {
            case 5 :
                alreadyPosted =
                    (args[4].equalsIgnoreCase("T") || args[4].equalsIgnoreCase("TRUE"));
                if ( ! (alreadyPosted || args[4].equalsIgnoreCase("FALSE")) ) {
                  try {
                    eventId = Long.parseLong(args[4]);
                  }
                  catch (NumberFormatException ex) {
                    idFileName = args[4];
                  }
                }
            case 4 :
                pass = args[3];
            case 3 :
                user = args[2];
            case 2:
                propFileName = args[1];
            case 1:
                stateName = args[0];
        }

        System.out.println("Processing events for: " + groupName + " " + threadName + " " + stateName);
        System.out.println("Using propertyfile: " + ((propFileName.equals("")) ? "DEFAULT" : propFileName));
        System.out.println(groupName + " " +  threadName + " " +  stateName);
        System.out.println("Already posted: " +  alreadyPosted);
        if (idFileName != null) System.out.println("Input id filename: " + idFileName);
        else  System.out.println("Input id : " + eventId);

        int result = 0;
        /*
        //Test 1 : process those already posted
        // uses input property file, if defined else the default subdir prop file to configure setup
        result = HypoMagProcessingController.processEvents(groupName,threadName,stateName,propFileName);
        */
        //
        //Test 2 : post and process those selected by EventSelectionProperties
        // uses default application subdir property file to configure setup
        try (HypoMagProcessingController pc = 
                new HypoMagProcessingController("hypomag", groupName, threadName, stateName, propFileName)) {

            // Not null or empty string override
            if (user != null && user.length() > 0) pc.props.setDbaseUser(user);
            if (pass != null && pass.length() > 0) pc.props.setDbasePasswd(pass);
            // Removed saving of properties here - aww 08/03/2006
            //pc.props.saveProperties("Properties updated : " + new DateTime.toString()); 

            // test user/pass logic by removing their defs from properties file. - aww
            // user and pass will be used for connection only if missing from properties
            result = pc.setupForProcessing();

            System.out.println(">>> Processing Settings Setup <<<");
            System.out.println(pc.getProcessingAttributesString());
            System.out.println(">>><<<");

            //long [] ids = new long [] { 1l,2l,3l };
            //pc.bulkPost(ids, "BULK", "BULK", "BULK", 50);

            //
            if (result > 0) {
                if (idFileName != null)
                    result = pc.processIdsBulkPost(pc.getIdListToProcess(idFileName));
                else if (eventId > 0l) {
                    result = pc.postId(eventId, 50); 
                    if (result > 0) {
                        System.out.println(">>>Posted id: " + eventId + " Rank: 50");
                        result = pc.processId(eventId); 
                    }
                    else System.err.println("ERROR HypoMagProcessingController: Unable to post input evid: " + eventId + " result: " + result);
                }
                else
                    result = (alreadyPosted) ? pc.processIds() : pc.processByEventProperties();
            }
            else System.err.println("ERROR HypoMagProcessingController: Check for configuration or DB ERROR.");
            //
            if (result > 0)
                System.out.println("\nHypoMagProcessingController processed: " + result);
            else if (result == 0)
                System.out.println("HypoMagProcessingController: Check for valid event id state postings or rank BLOCKING.");
            else if (result < 0)
                System.err.println("ERROR HypoMagProcessingController: Check LOG for processing error messages.");
            //
            pc.closeProcessing();
        }
    }
}
