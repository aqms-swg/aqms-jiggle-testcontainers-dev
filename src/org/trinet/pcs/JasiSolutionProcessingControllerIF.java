package org.trinet.pcs;
import org.trinet.jasi.*;

public interface JasiSolutionProcessingControllerIF extends ProcessingControllerIF, JasiSolutionLockProcessorIF {

    int resultSolution(Solution solution, int resultCode);
    int processSolution(Solution solution);
    int processSolution(Solution [] solutions);

    void setProcessingModule(JasiSolutionProcessorIF module);
    boolean setProcessingModuleMode(SolutionProcessingMode processingMode);
    boolean setProcessingModuleMode(int processingModeId);

    void setAutoCommit(boolean tf);
    boolean isAutoCommit();

    int processByEventProperties();
    int processByEventProperties(int rank);

    String getHostAppRole();
}
