package org.trinet.pcs;

import org.trinet.jasi.ChannelList;
import org.trinet.util.GenericPropertyList;

public interface PcsChannelListEngineDelegateIF {
  public boolean setDelegateProperties(GenericPropertyList props);
  public boolean setDelegateProperties(String propFileName);
  public GenericPropertyList getDelegateProperties();
  public boolean initializeEngineDelegate();

  public void loadChannelList(java.util.Date date);
  public void setChannelList(ChannelList chanList);
  public boolean hasChannelList();

}
