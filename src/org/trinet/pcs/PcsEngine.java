package org.trinet.pcs;

import org.trinet.jasi.Solution;

/** 
 * Generic PCS engine: handles PCS and signaling in a generically.
 * 
 * Example for other PCS driven processes.
 * 
 * Actual work is done by a user-written module that is set by the property 'pcsModuleClassName'.
 * That module must implement JasiSolutionProcessorIF or extend a class that does.
 * 
 * 
 * @author Doug Given
 *
 */
//public class PcsEngine extends AbstractSolutionProcessingController {
public class PcsEngine extends 	AbstractChannelListProcessingController {

// The AbstractSolutionProcessingController extends AbstractProcessingController 
// and does most of the book-keeping
	
	static String DefaultGroupName  = "PcsEngine";
	static String DefaultStateName  = "PcsEngine";
	static String DefaultThreadName = "PcsEngine";

	// Set some defaults in the superclass
	{
		appName = "PcsEngine";
	}

    public PcsEngine() { }

    public PcsEngine(String propFileName) {

        super();

        if ( propFileName == null || propFileName.equals("") ) {
          // Load "default" properties file which should be in the
          // subdir below users home named: "."+appName.toLowerCase()
          setProperties((PcsPropertyList)null); 
        }
        else setProperties(propFileName);
    }
    
    // Concrete versions of abstract methods
    // These are called at initialization just incase they aren't set in the props file
     public String getDefaultGroupName()  { return DefaultGroupName; }
     public String getDefaultThreadName() { return DefaultThreadName; }  // table
     public String getDefaultStateName()  { return DefaultStateName; }
/** 
 * 
 * This is where processing is done.
 * Should get an instance of the class defined by the 'module' property
 * an call its 'processSolution' method (but why?).
 * 
 * 
 * */
     // Called by super.processID()
     // Must:
     // 1) do your work
     // 2) result the event
//     public int processSolution(Solution sol) {
//    	 
//         if (sol == null) return ProcessingResult.NULL_INPUT.getIdCode();
//        
//    	 System.out.println ("Processing:");
//    	 System.out.println (sol.toNeatString());
//    	 
//    	 resultId(sol.getId().longValue(), 1);
//    	 
//    	 int resultCode = ProcessingResult.UNIT_SUCCESS.getIdCode();
//         return resultCode;   	 
//     }
   
     
     
    /**
     * 
     * @param args
     */
    public static final void main(String [] args) {
 
        String ARGstateName        = null;
        String ARGthreadName       = null;
        String ARGgroupName        = null;
     	String ARGpropFilename = "";
    	String ARGdbuser = null;
    	String ARGdbpwd  = null;
    	
        int nargs = args.length;
        if (nargs < 1 || args[0].equals("?")) {
            System.out.println("args: <propFile> [group] [table] [state] [dbuser] [dbpwd]");
            System.out.println(" <propFile> = properties file (required)\n" +
                               " [group] = PCS group name\n" +
                               " [table] = PCS table (or thread) name\n" +
                               " [state] = PCS state name\n" +
                               " [dbuser][dbpwd] = dbase username/password"); 
            System.out.println(" All but <propFile> may be defined in the properties file.\n");
            return;
        }
        switch (nargs) {

            case 6 :
            	ARGdbpwd = args[5];
            case 5 :
            	ARGdbuser = args[4];
            case 4 :
            	ARGstateName = args[3];
            case 3 :
            	ARGthreadName = args[2];
            case 2 :
            	ARGgroupName = args[1];
            case 1 :
            	ARGpropFilename = args[0];
        }
     
        System.out.println("Using propertyfile: " + ARGpropFilename);
     
        // Make a run-time instance of THIS controller
        try (AbstractSolutionProcessingController ctrl = 
                new PcsEngine(ARGpropFilename)) {

            //       System.out.println("Processing events for: " + groupName + 
            //       		" " + threadName + " " + stateName);

            // Over-ride props w/ cmd line values if defined
            if (ARGdbuser != null) ctrl.props.setDbaseUser(ARGdbuser);
            if (ARGdbpwd  != null) ctrl.props.setDbasePasswd(ARGdbpwd);      
            if ( ARGgroupName != null && ARGthreadName != null && ARGstateName != null) {
                ctrl.setProcessControlIdentifier(ARGgroupName, ARGthreadName, ARGstateName);
            }

            // test above user/pass logic by removing their defs from properties file.
            // user and pass will be used for connection only if missing from properties
            int result = ctrl.setupForProcessing();

            //        ctrl.runMode = 2;

            System.out.println(">>> Processing Settings Setup <<<");
            System.out.println(ctrl.getProcessingAttributesString());
            System.out.println(">>><<<");

            if (result <= 0) {
                System.err.println("ERROR : Check for configuration or DB ERROR.");
            }
            else {

                //
                // Should stay in this call for the duration of the run
                //
                result = ctrl.processIds();

                if (result > 0)
                    System.out.println("\n events processed: " + result);
                else if (result == 0)
                    System.out.println("Check for valid event id state postings or rank BLOCKING.");
                else if (result < 0)
                    System.err.println("ERROR : Check LOG for processing error messages.");

            }

            ctrl.closeProcessing();

        }
    }
}
