package org.trinet.pcs;

import org.trinet.jasi.*;

public class PcsEngineModuleExample extends AbstractSolutionChannelDataProcessor {

    // class static data enumeration:
    public static final int TEST_MODE_ID = 0;

    public static final SolutionProcessingMode TEST = 
           new SolutionProcessingMode("TEST", TEST_MODE_ID);

    // for each mode instance:
    {
        modeMap.put(Integer.valueOf(TEST_MODE_ID), TEST);
    }

    /** No-op Constructor. */
    public PcsEngineModuleExample() { }

    /**
     * Constructor initializes DummyEngineDelegate with property list and
     * sets TEST_MODE_ID as the mode (default) for processing.
     */
    public PcsEngineModuleExample(SolutionWfEditorPropertyList props) {
        //this(props, SolutionProcessingMode.UNKNOWN);
        this(props, TEST); // what should the default behavior be?
    }

    /**
     * Constructor initializes DummyEngineDelegate with input property list and
     * sets mode (default) for processing to the specified input mode.
     */
    public PcsEngineModuleExample(SolutionWfEditorPropertyList props, SolutionProcessingMode defaultMode) {
        super(props, defaultMode);
    }

    //IF
    /** Process input Solution with the delegate as configured.*/
    public int processSolution(Solution sol)  {
        if (sol == null) {
            ProcessingResult.NULL_INPUT.getIdCode();
        }
        
        // do something
        System.out.println (sol.toNeatString());
        
       // success
       return ProcessingResult.UNIT_SUCCESS.getIdCode();
    }

    //IF subclass implementation might wrap delegate
    public boolean hasChannelList() { return false; }

    //IF subclass implementation might wrap delegate
    public void setChannelList(ChannelList chanList) {}

}
