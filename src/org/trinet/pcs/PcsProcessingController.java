package org.trinet.pcs;

import java.io.PrintStream;
import java.util.*;

import org.trinet.jasi.ChannelList;
import org.trinet.util.BenchMark;
import org.trinet.util.DateTime;
import org.trinet.util.ThreadScheduler;

/**
 * 
 * General purpose PCS (Process Control System) processor.<p>
 * 
 * The actual processing of events is done by a "processor" class that is defined by
 * the <i>pcsModuleClassName</i> property. 
 * This class must implement the JasiSolutionProcessorIF interface and 
 * define a processSolution() method. 
 * Alternatively, you may extend one of the abstract or concrete children of that 
 * interface.
 * <p>
 * 
 * <code>
   args: [appDir] [propFile] [user] [pwd] [modeFlag] where:

    appDir     = application subdirectory under user's HOME, containing properties files.
    propFile   = name of properties file located in appDir.
    user       = database user account, overrides properties file value.
    pwd        = database user account password, overrides properties file value.
    modeFlag    = TRUE (ids are already posted in table)
                = FALSE (post ids table by EventProperties filter query)
                = idFileName in local dir (ascii file 1 id per line)
                = number (single evid to post and process)
                
    All the args are optional.                 
  </code>
 *
 */

// NOTE: configure a module properties file to desired attributes

public class PcsProcessingController extends AbstractChannelListProcessingController {

    public static HashMap cdm = new HashMap(73); // application specific (dumped by results output)

    private PrintStream consoleOut = null; // console terminal 

    public PcsProcessingController() { }

    public PcsProcessingController(String appName, String groupName, String threadName, String stateName) {
        this(appName, groupName, threadName, stateName, null);
    }

    public PcsProcessingController(String appName, String groupName, String threadName, String stateName,
                                   String propFileName) {

        super(appName, groupName, threadName, stateName);

        if ( propFileName == null || propFileName.equals("") ) {
          // Load "default" properties file which should be in the
          // subdir below users home named: "."+appName.toLowerCase()
          setProperties((PcsPropertyList)null); 
        }
        else setProperties(propFileName);
    }

    // Need a static handle for oracle db stored procedure implementation 
    public static final int processEvents(String appName, String groupName, String threadName,
                                          String stateName, String propFileName) {
        return processEvents(groupName, threadName, stateName, propFileName, null);
    }

    public static final int processEvents(String appName, String groupName, String threadName,
            String stateName, String propFileName, String idFileName) {
        // create a processor instance configure by properties in specified input file
        try (PcsProcessingController pc =
                new PcsProcessingController(appName, groupName, threadName, stateName, propFileName)) {
            return (idFileName == null) ?
                    pc.setupAndProcessIds() : pc.setupAndProcessIds(idFileName);
        }
    }

// Test method should be in a class of a user test package e.g. org.trinet.test.TestProcessor
    public static final void main(String [] args) {

        String appName = "unknown";
        String propFileName = "";
        String user = null;
        String pass = null;
        String idFileName = null;
        long eventId = 0l;
        boolean alreadyPosted = true;
        boolean postOnly = false;


        int nargs = args.length;
        if (nargs < 1 || args[0].equals("?")) {
            System.out.println("args: [appDir] [propFile] [user] [pwd] [modeFlag] [postOnly] where:");
            System.out.println("    appDir   = user's application subdirectory containing properties files.");
            System.out.println("    propFile = name of properties file located in appDir.");
            System.out.println("    user = database user account, overrides properties file value.");
            System.out.println("    pwd  = database user account password, overrides properties file value.");
            System.out.println("    modeFlag = TRUE (ids are already posted in table, the default)\n" +
                               "             = FALSE (post ids table by EventProperties filter query)\n" +
                               "             = idFileName in local dir (ascii file 1 id per line)\n" +
                               "             = number (single evid to post and process)\n" +
                               "    PostOnly = TRUE (only posts id(s), no state processing, default=false)"
                              ); 
            return;
        }

        switch (nargs) {
            case 6 :
                postOnly = (args[5].equalsIgnoreCase("T") || args[5].equalsIgnoreCase("TRUE"));
            case 5 :
                alreadyPosted =
                    (args[4].equalsIgnoreCase("T") || args[4].equalsIgnoreCase("TRUE"));
                if ( ! (alreadyPosted || args[4].equalsIgnoreCase("FALSE")) ) {
                  try {
                    eventId = Long.parseLong(args[4]);
                  }
                  catch (NumberFormatException ex) {
                    idFileName = args[4];
                  }
                }
            case 4 :
                pass = args[3];
            case 3 :
                user = args[2];
            case 2:
                propFileName = args[1];
            case 1:
                appName = args[0];
        }

        // Summarize user inputs
        System.out.println("Using propertyfile: " + ((propFileName.equals("")) ? "DEFAULT" : propFileName));
        System.out.println("Already posted: " +  alreadyPosted);
        if (idFileName != null) System.out.println("Input id filename: " + idFileName);
        else System.out.println("Input id : " + eventId);

        // Note these are not input args, assumes properties files has application desired values.
        String stateName = "unknown";
        String threadName = "unknown";
        String groupName = "unknown";

        PrintStream out = System.out; // save terminal console out

        // Use default application subdir property file to configure setup
        try (PcsProcessingController pc = 
                new PcsProcessingController(appName, groupName, threadName, stateName, propFileName)) {

            pc.setConsoleOut(out); // for outputs to terminal console

            // In case no defs already in  properties file - aww
            // if not null or empty string override
            if (user != null && user.length() > 0) pc.props.setDbaseUser(user);
            if (pass != null && pass.length() > 0) pc.props.setDbasePasswd(pass);

            int result = pc.setupForProcessing(); // ultimately calls loadChannelList()

            pc.reportAttributes();

            if (result > 0) {
                // Mode 1: bulk process from a file
                if (idFileName != null) {
                    if (! postOnly) { // post and process
                        result = pc.processIdsBulkPost(pc.getIdListToProcess(idFileName));
                    }
                    else { // only post them
                        pc.logMessage(">>>ONLY Posting ids in file: " + idFileName + " at rank: " + pc.getDefaultPostingRank() );
                        result = pc.bulkPost(pc.getIdListToProcess(idFileName), pc.getDefaultPostingRank() );
                        if (result <= 0) pc.logMessage("ERROR PcsProccessingController: Bulk post failed, ids may already be posted");
                    }
                }
                // Mode 2: process one posted ID
                else if (eventId > 0l) {
                    result = pc.postId(eventId); 
                    if (result > 0) {
                        pc.logMessage(">>>Posted id: " + eventId + " at rank: " + pc.getDefaultPostingRank() );
                        if (! postOnly) result = pc.processId(eventId); 
                    }
                    else pc.logMessage("ERROR PcsProccessingController: Unable to post input evid: " +
                            eventId + " result: " + result);
                }
                // Mode 3: enter continuous processing loop OR post & process events by properties
                else {
                    if (alreadyPosted) {
                        if (! postOnly) result = pc.processIds();
                    }
                    else {
                        if (! postOnly) result = pc.processByEventProperties();
                        else {
                            pc.logMessage(">>>ONLY Posting ids by event selection properties at rank: " + pc.getDefaultPostingRank() );
                            result = pc.bulkPost(pc.getIds(pc.getSolutionListFromEventProperties()), pc.getDefaultPostingRank());
                            if (result <= 0) pc.logMessage("ERROR PcsProccessingController: Bulk post failed, ids may already be posted");
                        }
                    }
                }
            }
            else pc.logMessage("ERROR PcsProccessingController: Check for configuration or DB ERROR.");

            pc.reportResults(result);

            // dump module properties
            if (pc.verbose && pc.module != null) {
                System.out.println("-------------------- Module Ending Properties Dump --------------------");
                pc.module.dumpProperties();
            }

            pc.closeProcessing();
        }
    }

    private void setConsoleOut(PrintStream out) {
        consoleOut = out;
    }

    private void reportAttributes() {
        String str = getProcessingAttributesString();
        reportAttributes(consoleOut, str);
        if (consoleOut != System.out) reportAttributes(System.out, str);
    }

    private void reportAttributes(PrintStream out, String str) {
        out.println(">>> Processor Processing Settings Setup <<<");
        out.println(str);
        out.println(">>>-------------------------------------<<<");
    }

    private void reportResults(int result) {
        reportResults(result, consoleOut);
        if (consoleOut != System.out) reportResults(result, System.out);

        if (! cdm.isEmpty()) { // map may contain channel date range processed or something else application specific
            Set s = cdm.entrySet();
            Iterator iter = s.iterator();
            Map.Entry me = null;
            System.out.println("Static map contents: \n");
            while (iter.hasNext()) {
              me = (Map.Entry) iter.next();
              System.out.println(me.getKey().toString() +" "+ me.getValue().toString() + "\n");
            }
        }
    }

    private void reportResults(int result, PrintStream out) {
        if (result > 0)
            out.println("\nPcsProcessingController events processed: " + result);
        else if (result == 0)
            out.println("PcsProccessingController: Check for valid event id state postings or rank BLOCKING.");
        else if (result < 0)
            out.println("ERROR PcsProccessingController: Check LOG for processing error messages.");
    }

    private void logMessage(String msg) {
        consoleOut.println(msg);
        if (consoleOut != System.out) System.out.println(msg);
    }
        
    // Below is override of AbstractChannelListProcessingController.loadChannelList to do timer thread refreshes of channel list
    /** Populates ChannelList with Channels found in file cache specified by 
     * configuration property <i>channelCacheFilename</i> otherwise creates a
     * new list with Channels active in the database on current date or date 
     * specified by the configuration property <i>channelLoadDate</i>.
     * If <i>isMaster=true</i>, the MasterChannelList is also set to 
     * the loaded ChannelList.
     */
    public boolean loadChannelList(boolean isMaster) {

        if (props.getBoolean("pcsDisableLoadChannelList")) return false;

        boolean status = super.loadChannelList(isMaster);  // load list according to props
        if (status) startReloadThread();
        return true;
    }

    /**
     * Start a timer thread that will reload the channel list at some interval.
     * @return
     */
    public boolean startReloadThread() {
            
        final int MIN_SLEEP_SECS = 60*30;        // 30 min
            
        // Schedule the reload thread if auto refresh property is true
        if (!props.getBoolean("autoRefreshChannelList")) return false;
        int sleepsecs = props.getInt("autoRefreshChannelListInterval");
        if (sleepsecs == -1) sleepsecs = 60*60*24; // undefined, default to 24hrs
        
        // sanity check on interval
         if (sleepsecs < MIN_SLEEP_SECS) {
                logMessage("Warning: autoRefreshChannelListInterval too short");
                sleepsecs = MIN_SLEEP_SECS;
                logMessage("         Resetting to minimum value of "+sleepsecs+" seconds");
        }

        // Create the thing to be scheduled
         // NOTE: these properties come from the Module and not the Controller props
        ChannelLoaderThread thread = new ChannelLoaderThread();
        thread.setAbstractChannelListProcessingController(this);
        
        // is there a channel load date ?
        if (props.isSpecified("channelLoadDate")) {  
            thread.setDate(props.getDateTime("channelLoadDate"));
         }
        // list name?  
        String listname = null;
        //listname = props.getProperty("channelListName"); // deprecated
        //if (listname != null) thread.setName(listname);                
        //else {
            listname = props.getProperty("channelGroupName");
            if (listname != null) thread.setName(listname);                
        //}
        
        // create the scheduler
        boolean isDaemon =true; // set daemon true, sudden death when main exits
        ThreadScheduler scheduler = new ThreadScheduler(thread, sleepsecs, isDaemon);
        scheduler.setVerbose(true);
        
        // force a reload immediately
        // Why? Because the list read at startup could be from a stale cache file.
        scheduler.runNow();  
        
        // this returns the size of the OLD list since thread isn't done yet
        //return (getChannelList().size() > 0);
        return true;
    }   
    /* **************************************************************************** */
    /**
     * This is a one-shot "Runnable" thread that does the following:
     * 1) reload channellist using the 'name', 'data' options
     * 2) if the cachefilename is defined, rewrite the cache file
     * 3) replace the current MasterChannelList (?)
     */
    class ChannelLoaderThread implements Runnable {
      
      Thread thread = null;
      java.util.Date date = null;
      String name = null;
      // The method AbstractSolutionProcessingController.setChannelList
      // is used to update the list.
      AbstractChannelListProcessingController pc = null;
      
      public void setDate(java.util.Date date) {
              this.date = date;
      }
      public void setName (String name) {
              this.name = name;
      }
      
      public void setAbstractChannelListProcessingController 
          (AbstractChannelListProcessingController pc) {
              this.pc = pc;
      }
      
      /** Work happens here. */
      public void run () {
                     
        BenchMark bm = new BenchMark();    // measures elapse time to do the work
        System.out.println ("ChannelLoaderThread starting @ "+ new DateTime().toString()); // UTC time - aww 2008/02/08
       
        // Re-read the list from the data source
        ChannelList newList = null;
        if (name == null) {
                if (date == null) {
                       newList = ChannelList.readCurrentList();
                } else {
                       newList = ChannelList.readList(date);
                }        
        } else  {   // name NOT null
                if (date == null) {
                       newList = ChannelList.readCurrentListByName(name);   
                } else {
                       newList = ChannelList.readListByName(name, date);
                }
        }
        
        if (newList != null) {

            setChannelList(newList);
            System.out.println("INFO: >>> Loaded ChannelList total channels: " + newList.size());
                
            // This is not really necessary because the cache file name is static and
                // was set via a static method in the ChannelList class.
                // But do this incase that changes in the future.
            String cachename = pc.getChannelList().getCacheFilename();
            newList.setCacheFilename(cachename);
            newList.writeToCache();
            System.out.println("INFO: >>> Wrote new ChannelList to cache: " + newList.getCacheFilename());

        } else {
            bm.printTimeStamp("ChannelLoaderThread failed, time ="); // UTC time - aww 2008/02/08
        }

        // replace the channel list if pc is defined
        // this will change it in the module also
        if (pc != null ) pc.setChannelList(newList);
        // SHOULD THIS BE SYNCRONIZED? - what if rcg thread is running when it is 
        // changed?
        
        bm.printTimeStamp("ChannelLoaderThread done, time ="); // UTC time - aww 2008/02/08
        System.out.println("-----------------------------------------------------");
      }
    } // end of internal class    
    
}
