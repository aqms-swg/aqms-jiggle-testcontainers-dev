package org.trinet.pcs;
import java.io.*;
/**
 * Stop processing of process whose sigid matches first input argument value
 */
public class PcsStopProcessing extends AbstractSolutionProcessingController {

    protected PcsStopProcessing() {}

    protected PcsStopProcessing(String appName, String groupName, String threadName, String stateName, String propFileName) {
        super(appName, groupName, threadName, stateName);
        if ( propFileName == null || propFileName.equals("") ) {
          setProperties((PcsPropertyList)null); 
        }
        else setProperties(propFileName);
    }

    public static final void main(String [] args) {

        String appName = "unknown";
        String propFileName = "";
        String user = null;
        String pass = null;

        long sigId = 0l;

        int nargs = args.length;
        if (nargs < 1 || args[0].equals("?")) {
            System.out.println("args: [appDir] [propFile] [sigID] where:");
            System.out.println("    appDir   = user's application subdirectory containing properties files.");
            System.out.println("    propFile = name of properties file located in appDir.");
            return;
        }

        switch (nargs) {
            case 5 :
                pass = args[4];
            case 4 :
                user = args[3];
            case 3 :
                sigId = Long.parseLong(args[2]);
            case 2:
                propFileName = args[1];
            case 1:
                appName = args[0];
        }

        // Summarize user inputs
        System.out.println("Using propertyfile: " + ((propFileName.equals("")) ? "DEFAULT" : propFileName));

        // Note these are not input args, assumes properties files has application desired values.
        String stateName = "unknown";
        String threadName = "unknown";
        String groupName = "unknown";

        // Use default application subdir property file to configure setup
        try (PcsStopProcessing pc = 
               new PcsStopProcessing(appName, groupName, threadName, stateName, propFileName)) {
    
            // In case no defs already in  properties file - aww
            // if not null or empty string override
            if (user != null && user.length() > 0) pc.props.setDbaseUser(user);
            if (pass != null && pass.length() > 0) pc.props.setDbasePasswd(pass);
    
            int result = pc.setupForProcessing(); // ultimately calls loadChannelList()
            if (result > 0) {
              pc.sigId = sigId;
              System.out.println("Signal stop for sigid: " + pc.sigId + " status: " + pc.signalStop());
            }
            else
              System.err.println("PcsStopProcessing connection setup failed with code: " + result);
    
            pc.closeProcessing();
            }
    }

    protected int setupForProcessing() {
        // processor must first make connection to db
        if (! setupDataSourceFromProperties()) {
          System.err.println(getClass().getName()+" ERROR processor DB connect/setup failed! Check init/properties.");
          return ProcessingResult.DB_NO_CONNECTION.getIdCode();
        }
        return 1;
    }
}

