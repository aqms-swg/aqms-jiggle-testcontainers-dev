
package org.trinet.pcs;

import java.io.*;
import java.sql.*;
import java.util.*;
import org.trinet.storedprocs.DbaseConnection;

// Utility Class referenced by Oracle stored PCS package
// METHODS MAY BE CALLED BY PLSQL WRAPPER PROCEDURE OR FUNCTION
/**
 * Static class for Process Control System
 */
public class ProcessControl {

    protected static boolean debug = false;

    private static Connection CONNECTION;
    protected static int DEFAULT_RANK = 100;
  
    // Instance holder for any values preset for default group and source 
    private static StateRow mySR = new StateRow();

    //Should be the largest column width found in the db table
    private static final int MAX_FIELD_WIDTH = 20;
    private static final char [] BLANKS = new char [MAX_FIELD_WIDTH];

    static {
        Arrays.fill(BLANKS, 0, MAX_FIELD_WIDTH, ' ');
    }

    /** Set the StateRow controlGroup to use for all transactions in this instance */
    public static void setControlGroup(String group) {
        mySR.controlGroup = group;
    }
    public static String getControlGroup() {
        return mySR.controlGroup;
    }
  
    /** Set the StateRow sourceTable to use for all transactions in this instance */
    public static void setSourceTable(String source) {
        mySR.sourceTable = source;
    }
    public static String getSourceTable() {
        return mySR.sourceTable;
    }
  
    //TODO: should methods return a status code or throw exception?
    //group, source, state can't be null but they have no defaults.
  
    /**
    * Return next ID for the given group/source/state.
    * If no id's returns 0.
    */
    public static long getNextId(String group, String source, String state) {
        return getNextId(group, source, state, 0);
    }

    /**
    * Return next ID for the given group/source/state
    * whose row lddate is older than lagSecs behind SYS_EXTRACT_UTC(CURRENT_TIMESTAMP).
    * If no id's returns 0.
    */
    public static long getNextId(String group, String source, String state, int lagSecs) {
        return StateRow.getNextId(group, source, state, lagSecs);
    }

    /** Insert a StateRow with these parameters.
     * Uses the current values of the instance StateRow controlGroup and sourceTable values.
     * Input processing state must be a non-null, non-blank String and input rank > 0.
     * After insert, if result code is NON-ZERO, invokes row result transition function
     * for input state description, id, and result.
     * Returns 1 for success, 0 for failure, perhaps already posted.
     * @see #setControlGroup(String group)
     * @see #setSourceTable(String source)
     * @see #putState(String group, String source, long id, String state, int rank, int result)
     * @see #doTransitions(String group, String source, String state, long eid)
     * */
    public static int putState(long id, String state, int rank, int result) {
        mySR.id = id;
        mySR.state = state;
        mySR.rank = rank;
        mySR.result = result;

        int istat = mySR.insert(); // rejects by SR constraints 
        // Immediately transition row if the input result is non-zero
        if (istat > 0 && result != 0) istat = doTransitions(mySR.controlGroup, mySR.sourceTable, state, id);

        return istat;
    }
  
    /** Insert a StateRow with these parameters.
     * Uses the current values of the instance StateRow controlGroup and sourceTable values.
     * Input processing state must be a non-null, non-blank String and input rank > 0.
     * Does NOT invoke row result transition function since result value defaults to 0.
     * Returns 1 for success, 0 for failure, perhaps already posted.
     * @see #setControlGroup(String group)
     * @see #setSourceTable(String source)
     */
    public static int putState(long id, String state, int rank) {
        return putState(id, state, rank, 0);  // default result is 0.
    }
  
    /** Insert a StateRow with these parameters. 
     * Sets the current values of the instance StateRow controlGroup and sourceTable values.
     * Input processing description must be non-null, non-blank Strings and input rank > 0.
     * Does NOT invoke row result transition function since result value defaults to 0.
     * Returns 1 for success, 0 for failure, perhaps already posted. 
     * @see #setControlGroup(String group)
     * @see #setSourceTable(String source)
    */
    public static int putState(String group, String source, long id, String state, int rank) {
        return putState(group, source, id, state, rank, 0);  // default result is 0.
    }

    /** Insert a StateRow with these parameters. 
     * Sets the current values of the instance StateRow controlGroup and sourceTable values.
     * Input processing description must be non-null, non-blank Strings and input rank > 0.
     * If input result code value is NON-ZERO, invokes row result transition function. 
     * Returns 1 for success, 0 for failure, perhaps already posted. 
     * @see #setControlGroup(String group)
     * @see #setSourceTable(String source)
     * @see #doTransitions(String group, String source, String state, long eid)
    */
    public static int putState(String group, String source, long id, String state, int rank, int result) {
        setControlGroup(group);
        setSourceTable(source);
        return putState(id, state, rank, result);
    }
  
    /**
     * Write a result code for StateRow corresponding to inputs
     * Input processing description should be non-null, non-blank Strings and id >=0.
     * A single id is processed matching the description Strings. 
     */
    public static int putResult(String group, String source, long id, String state, int rank, int result) {
        // Should we make this the alias for the putState method, insert row with result even if none exist? -aww
        // Implementation below doesn't insert row, only updates an existing row in database if result is different.
        //
        // Only if different result update table
        StringBuffer sb = new StringBuffer(256);
        sb.append("UPDATE ").append(StateRow.STATE_TABLE);
        sb.append(" SET result=").append(result);
        sb.append(" WHERE ");
        sb.append("controlGroup=").append(toStringSQL(group));
        sb.append(" AND sourceTable=").append(toStringSQL(source));
        sb.append(" AND state=").append(toStringSQL(state));
        sb.append(" AND rank=").append(String.valueOf(rank));
        sb.append(" AND id=").append(String.valueOf(id));
        sb.append(" AND (result IS NULL OR result !=").append(String.valueOf(result)).append(")");
        int istat = doUpdate(sb.toString());
        // Do transition here if update was done
        if (istat > 0) doTransitions(group, source, state, id);

        return istat;
    }
  
    /**
     * Update result code for all id entries which match input state description.
     * All ids matching the description Strings are processed. 
     */
    public static int putResultAll(String group, String source, String state, int rank, int result) {
        StringBuffer sb = new StringBuffer(256);
        sb.append("UPDATE ").append(StateRow.STATE_TABLE);
        sb.append(" SET result=").append(result);
        sb.append(" WHERE ");
        sb.append("controlGroup=").append(toStringSQL(group));
        sb.append(" AND sourceTable=").append(toStringSQL(source));
        sb.append(" AND state=").append(toStringSQL(state));
        sb.append(" AND rank=").append(String.valueOf(rank));
        int istat = doUpdate(sb.toString());
        // Do ALL ids transitions here not in trigger -aww
        if (istat > 0) doTransitions(group, source, state);
        return istat;
    }
  
    /**
     * Execute an Insert, Update, or Delete statement.
     */
    protected static int doUpdate(String sql) {
        int nrows = 0;
        Statement stmt = null;
        if (debug) System.out.println("DEBUG ProcessControl doUpdate SQL:\n" + sql);
        try {
          stmt = getConnection().createStatement();
          nrows = stmt.executeUpdate(sql);
          CONNECTION.commit();
        }
        catch (SQLException ex) {
          ex.printStackTrace(System.out);
          System.err.println("SQL: " + sql);
        }
        finally {
          try {
            if (stmt != null) stmt.close();
          }
          catch (SQLException ex) {}
        }
  
        return nrows;
    }

    private static void dbCommit() {
        try {
          CONNECTION.commit();
        }
        catch (SQLException ex) {
          ex.printStackTrace(System.out);
        }
    }

    /** Return -1 = id not posted; 0 = old result; 1 = ready;  >1 = rank blocked. */
    public static int runStatusOf(String group, String source, String state, long id) {

        // get all state postings for this id.
        StateRow sr[] = StateRow.get(id);
        if (sr == null) return -1; // no postings for id 
  
        // Find max posting rank of all postings for id.
        int maxRank = -1;
        for (int i = 0; i < sr.length; i++) {
          maxRank = Math.max(sr[i].rank, maxRank);
        }

        // Loop over array to find any group,source,state matching inputs
        for (int i = 0; i < sr.length; i++) {
            if (sr[i].controlGroup.equals(group)) { 
              if (sr[i].sourceTable.equals(source)) { 
                if (sr[i].state.equals(state)) { 
                  if (sr[i].result != 0) return 0; // stale, old result, already run
                  // return i+1; // i=0 ready to run, if i>0 state run is rank blocked
                  return (sr[i].rank >= maxRank) ? 1 : 2;
                }
              }
            }
        }
  
        return -1; // no postings match id
    }

    /** Id posted for state, result code 0 but is rank blocked. */
    public static boolean isRankBlocked(String group, String source, String state, long id) {
        return (runStatusOf(group, source, state, id) > 1);
    }
    /** Id posted for state, result code 0 and not rank blocked. */
    public static boolean isReady(String group, String source, String state, long id) {
        return (runStatusOf(group, source, state, id) == 1);
    }
    /** Id is posted for some state (result status undetermined). */
    public static boolean hasPosting(long id) {
        StateRow sr[] = StateRow.get(id);
        return (sr != null && sr.length > 0);
    }
    /** Id is posted for specified state (result status undetermined). */
    public static boolean hasPosting(String group, String source, String state, long id) {
        StateRow sr[] = StateRow.get(group, source, state, id);
        return (sr != null && sr.length > 0);
    }
    /** Id is posted for state and has a non-zero result implying already processed,
     * with either a success or an error status result. */
    public static boolean hasResult(String group, String source, String state, long id) {
        Integer value = getResultFor(group, source, state, id);
        return (value != null && value.intValue() != 0);
    }
    /** Id is either not posted for state, or has a negative result implying a processing error. */
    public static boolean hasNegativeResult(String group, String source, String state, long id) {
        return ! hasPositiveResult(group, source, state, id);
    }
    /** Id is posted for state and has a positive result implying already processed.*/
    public static boolean hasPositiveResult(String group, String source, String state, long id) {
        Integer value = getResultFor(group, source, state, id);
        return (value != null && value.intValue() > 0);
    }
    /** Return value equals the result value of StateRow that matches the inputs,
     * returns null if no matching row exists for the specified inputs.
     * A value of zero implies unprocessed, a negative value implies a processing error,
     * and a positive value implies processed (usually) successfully.*/
    public static Integer getResultFor(String group, String source, String state, long id) {
        StateRow sr[] = StateRow.get(group, source, state, id);
        return (sr == null || sr.length == 0) ? null : Integer.valueOf(sr[0].result);
    }
  
    /** Post entries from a text file where each line is of the form:
       <pre>
       id state rank group source
       </pre>
       @return count of postings
    */
    public static int postFromFile(String idFileName) {
        System.out.println("Postings id(s) to from file: " + idFileName);
        int count = 0;
        BufferedReader inStream = null;
        try {
            inStream = new BufferedReader(new FileReader(idFileName));
            String inString = null;
            StringTokenizer st = null;
            int rank = DEFAULT_RANK;
            long id = 0l;
            String state = "Test";
            String group = "Test";
            String source = "Test";

            while (true) {
                inString = inStream.readLine();
                if (inString == null) {
                    break;
                }

                st = new StringTokenizer(inString);
                int tokens = st.countTokens();
                if (tokens < 2) {
                  System.err.println("Too few tokens: "+tokens+" found for input line: " +
                      (count+1) +
                      " Syntax: id, [state, D='Test'], [rank, D=100], [group,D='Test'], [source, D='Test']");
                    return count; 
                }

                id = Long.parseLong(st.nextToken()); // token 1
                state = st.nextToken(); // token 2

                if (tokens > 2) rank = Integer.parseInt(st.nextToken());
                if (tokens > 3) group = st.nextToken();
                if (tokens > 4) source = st.nextToken();

                putState(group, source, id, state, rank);

                count++;
            }
        }
        catch (Exception ex) {
            System.out.println("Error while posting input file:" + idFileName +
                    " at line: " + (count+1));
            ex.printStackTrace();
        }
        finally {
            try {
                if (inStream != null) inStream.close();
            }
            catch (IOException ex2) { }
        }

        return count;
    }

    /**
     * Posts all id's found that match the input group, source, state, and result
     * to the input description values and rank. 
     * The new group, source, and state description values must all be
     * non-null, non-blank Strings, and the input rank must be > 0,
     * otherwise no new posting is created.
     * @return count of StateRows processed
     */
    public static int recycle(String group, String source, String state, int result,
                             String newGroup, String newSource, String newState, int rank) {

        StateRow [] sr = StateRow.get(group, source, state, result, true); // lock rows for update -aww 10/31/2006
        int count = (sr == null) ? 0 : sr.length;
        if (debug) System.out.println("ProcessControl count of rows eligible for recycling: " + count);
        if ( count < 1) {
            dbCommit();
            return 0;
        }

        boolean insertNewRows =
            (! (isBlank(newGroup) || isBlank(newSource) || isBlank(newState)
                || rank <= 0)  ) ? true : false;

        if (insertNewRows) {
            mySR.controlGroup = newGroup;
            mySR.sourceTable = newSource;
            mySR.state = newState;
            mySR.rank = rank;
            mySR.result = 0;  // reset here in case putState changed it
        }

        int nrows = 0;
        for (int index = 0; index < count; index++) {
            sr[index].delete();
            if (insertNewRows) {
                mySR.id = sr[index].id;
                mySR.insert(); // rejects by SR constraints 
            }
            nrows++;
        }
        return nrows;
    }

    /**
     * This scans the database PCS tables and does all pending transitions.
     * @return count of StateRows processed
     */
    public static int doTransitions() {
        // get all rows with an non-zero result code in State table
        return doTransitions(StateRow.getNonNullResults(true));
    }

    /**
     * This scans the database PCS tables and does all pending transitions.
     * The input group and source must be non-null, non-blank Strings.
     * @return count of StateRows processed
     */
    public static int doTransitions(String group, String source) {
        // get all rows with an non-zero result code
        return doTransitions(StateRow.getNonNullResults(group, source, true));
    }
  
    /**
     * This scans the database PCS tables and does all pending transitions.
     * The input group, source, and state must be non-null, non-blank Strings.
     * @return count of StateRows processed
     */
    public static int doTransitions(String group, String source, String state) {
        // get all rows with an non-zero result code
        return doTransitions(StateRow.getNonNullResults(group, source, state, true));
    }

    /**
     * This scans the database PCS tables and does all pending transitions for the
     * input event id. The input group, source, and state description must all be
     * non-null, non-blank Strings.
     * @return count of StateRows processed
     */
    public static int doTransitions(String group, String source, String state, long eid) {
        // Method added to handle single row case -aww  05/16/2005
        return doTransitions(StateRow.get(group, source, state, eid, true)); 
    }

    // Assume input rows have been locked so they are not changed before delete -aww 10/31/2006
    private static int doTransitions(StateRow [] sr) {
        if (sr == null) {
            dbCommit();
            return 0; // no rows
        }
  
        // for each row, do a transition if there is one defined
        int nrows = 0;
        // int istat = 0;

        for (int i = 0; i < sr.length; i++) {

          // input row should have non-null,non-blank processing description strings
          StateRow newStates[] = getNewStates(sr[i]); // filters out "NULL" new state 
          // debug
          //      System.out.println("getNewStates:");
          //      StateRow.dump(sr);
  
          if (newStates == null || newStates.length == 0) continue; // no transitions, do next StateRow
  
          // istat =
          sr[i].delete(); // delete old state row
  
          // insert new states
          for (int j = 0; j<newStates.length; j++) {
            if (! isBlank(newStates[j].state)) { // skip null new state, only used to flag delete 
                // istat = 
                newStates[j].insert();
            }
          }
          nrows++;
        }
        dbCommit();
        return nrows;
    }
  
    /**
     * Given a State table row with a non-zero result, return one or
     * more NEW State table rows that represent the next state(s) for this
     * data object. Returns null if no transitions are defined. This does NOT
     * write the new State table rows to the database.
     */
    public static StateRow[] getNewStates(StateRow sr) {

        // Get transition rows that pertain
        TransitionRow tr[] =
            TransitionRow.get(sr.controlGroup, sr.sourceTable, sr.state, sr.result);

        // debug
        // System.out.println("getNewStates: ");
        // TransitionRow.dump(tr);
        if (tr == null || tr.length == 0) return null; // nada
  
        // use the TransitionRows to make new State table entries
        ArrayList aList = new ArrayList(tr.length);
   
        for (int i = 0; i < tr.length; i++) {
            StateRow newSR = new StateRow(sr);  // copy original row data
            newSR.controlGroup = tr[i].groupNew; // not null
            newSR.sourceTable  = tr[i].sourceNew; // not null
            newSR.state        = tr[i].stateNew; // not null, value of 'null' flags delete
            newSR.rank         = tr[i].rankNew; // should be > 0
            newSR.result       = tr[i].resultNew; // 0 flags => unprocessed
            aList.add(newSR); 
        }
  
        int count = aList.size();
        if (count == 0) return null; // no new state to post
        return (StateRow []) aList.toArray(new StateRow[count]);
    }
  
    private static int getFillCount(String value, int width) {
      int fillCount = (value == null) ? width :  width - value.length();
      return (fillCount > 0) ? fillCount : 0;
    }
    protected static StringBuffer appendPaddedField(StringBuffer sb, String value) {
        return appendPaddedField(sb, value, MAX_FIELD_WIDTH);
    }
    protected static StringBuffer appendPaddedField(StringBuffer sb, String value, int width) {
      if (value != null && value.length() > width) {
        sb.append(value.substring(0, width-1)).append("* "); 
      }
      else sb.append(value).append(" ");
      sb.append(BLANKS, 0, getFillCount(value, width));

      return sb;
    }

  /**
   * Convert input numeric value to an SQL String value. 
   * Used for INSERT and UPDATE statements.
   * Return "NULL" if the input value == 0.
   */
    protected static String znull(int ival) {
        return (ival == 0) ? "NULL" : String.valueOf(ival);
    }
    protected static String znull(long ival) {
        return (ival == 0l) ? "NULL" : String.valueOf(ival);
    }

    /** Return input string wrapped with "'".
     * Return "NULL" if input is null, or its value equals
     * ignoring case the value "NULL".*/
    protected static String toStringSQL(String value) {
        if (value == null || value.equalsIgnoreCase("NULL"))
            return "NULL";
        StringBuffer sb = new StringBuffer(value.length()+2);
        sb.append("'");
        sb.append(value);
        sb.append("'");
        return sb.toString();
    }
  
  /**
   * Return string value of input column from input ResultSet row.
   * Return empty string if field is null.
   */
    protected static String getStringSafe(ResultSet rs, String column) {
        String str = null;
        try{
          str = rs.getString(column);
        } catch ( NullPointerException ex) {
          System.err.println("NullPointerException");
          ex.printStackTrace(System.err);
        } catch ( SQLException ex) {
          System.err.println("SQLException");
          ex.printStackTrace(System.err);
        }
        return (str == null) ? "" : str;
    }

    /** Returns true if input String is null, empty, padded with blanks,
     * or equals ignoring case "NULL".
     */
    protected static final boolean isBlank(String str) {
        return (isEmpty(str)) ? true : (str.trim().length() == 0) ;
    }

    /** Returns true if (isNull(str) || str.length()==0). */
    private static final boolean isEmpty(String str) {
        return (isNull(str)) ? true : (str.length() == 0) ;
    }
    /** Returns true if input is null, or equals ignoring case "NULL".
     */
    private static final boolean isNull(String str) {
        return (str == null ) ? true : str.equalsIgnoreCase("NULL");
    }

    /** Allow client to use a pre-existing user defined connection. */
    public static void setConnection(Connection conn) {
        CONNECTION = conn;
    }

  /**
   * Returns an existing connection or if null, makes a new connection.
   * If we are running in the context of the Oracle server,
   * make the "default" connection to the local instance.
   * Otherwise, uses the existing DataSource description if defined,
   * if not, the default TestDataSource for client code.
   * If the existing connection is closed a new one is created. 
   */
    public static Connection getConnection() {
        try {
            //if (CONNECTION == null) CONNECTION =
            //    org.trinet.jasi.DataSource.createInternalDbServerConnection();
            if (CONNECTION == null) CONNECTION =
                org.trinet.storedprocs.DbaseConnection.getConnectionWithExceptions();
            if (CONNECTION == null) throw new NullPointerException("CONNECTION is NULL");
        }
        catch (Exception ex) {
           System.err.println("ERROR ProcessControl.getConnection exception: "+ ex.getMessage());
        }
        return CONNECTION;
    }

    /* Example of getting collection type mapped from stored procedure into java method
    public static void dumpArray(java.sql.Array sqlArray) {
        long [] data = (long []) sqlArray.getArray();
        for (int i =0 ; i < data.length; i++) {
          System.err.println("Array value: " + data[i]);
        }
    }
    */
} // end of class ProcessControl
