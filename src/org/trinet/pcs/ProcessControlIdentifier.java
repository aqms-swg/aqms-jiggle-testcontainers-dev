
package org.trinet.pcs;
public class ProcessControlIdentifier implements Cloneable, Comparable {
    protected Integer hashCodeValue = null;
    protected String groupName = null;
    protected String threadName = null;
    protected String stateName = null;
    protected int rank = 0;

    public ProcessControlIdentifier () {}

    public ProcessControlIdentifier (String groupName, String threadName, String stateName) {
        this(groupName, threadName, stateName, 0);
    }

    public ProcessControlIdentifier (String groupName, String threadName, String stateName, int rank) {
        setProcessControlNames(groupName, threadName, stateName);
        this.rank = rank;
    }

    public void setProcessControlNames(String groupName, String threadName, String stateName) {
        this.groupName = groupName;
        this.threadName = threadName;
        this.stateName = stateName;
        hashCodeValue = null;
    }

    public String getGroupName() {
        return groupName;
    }
    public void setGroupName(String groupName) {
        this.groupName = groupName;
        hashCodeValue = null;
    }

    public String getThreadName() {
        return threadName;
    }
    public void setThreadName(String threadName) {
        this.threadName = threadName;
        hashCodeValue = null;
    }

    public String getStateName() {
        return stateName;
    }
    public void setStateName(String stateName) {
        this.stateName = stateName;
        hashCodeValue = null;
    }

    public int getRank() {
        return rank;
    }
    public void setRank(int rank) {
        this.rank = rank;
        hashCodeValue = null;
    }

    public Object clone() {
       ProcessControlIdentifier pcId = null;
       try {
           pcId = (ProcessControlIdentifier) super.clone(); 
       }
       catch (CloneNotSupportedException ex) {
          ex.printStackTrace();
       }
       return pcId;
    }
    public int hashCode() {
       if (hashCodeValue == null) calcHashCode();
       return hashCodeValue.intValue();
    }
    protected void calcHashCode() {
        hashCodeValue = Integer.valueOf((groupName + threadName + stateName).hashCode() + rank);
    }
    public int compareTo(Object object) {
        ProcessControlIdentifier pcId = (ProcessControlIdentifier) object; 
        return toString().compareTo(pcId.toString());
    }
    public boolean equals(Object object) {
        if (object == this ) return true;
        if (object == null || (object.getClass() != this.getClass()) ) return false;
        ProcessControlIdentifier pcId = (ProcessControlIdentifier) object;
        return ( toString().equals(pcId.toString()) && (this.rank == pcId.rank) );
    }
    public String toString () {
        return groupName + "." + threadName + "." + stateName + "." + rank;
    }
}
