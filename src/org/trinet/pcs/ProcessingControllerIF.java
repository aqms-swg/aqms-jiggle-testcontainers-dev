//  ProcessingControllerIF  implemented by any id Processor
package org.trinet.pcs;
import java.util.*;

public interface ProcessingControllerIF {

    String getAppName();

    //boolean signalMessage(String msg);
    boolean signalStart();
    boolean signalStop();
    boolean signalMessage(String message);
    boolean hasStopSignal();  // if true stop processing (e.g. list loops)

    /** Sleep for the interval set by setDefaultSleepTimeMillis()*/
    void sleep();
    void sleep(int millisecs);
    void setDefaultSleepTimeMillis(int millisecs);

    /** Return a connection to the PCS dbase. */
    java.sql.Connection getConnection();
    void setConnection(java.sql.Connection connection);

    void closeProcessing(); // close open statements and/or db connection

    boolean isValidId(long id);
    boolean isValidRank(int rank);

    int getDefaultNextLagSecs();
    void setDefaultNextLagSecs(int lagSecs);

    int getDefaultPostingRank();
    void setDefaultPostingRank(int rank);

    String getDefaultGroupName();
    String getDefaultStateName();
    String getDefaultThreadName();

    void setGroupName(String stateName);
    String getGroupName();
    void setThreadName(String threadName);
    String getThreadName();
    void setStateName(String stateName);
    String getStateName();

    ProcessControlIdentifier getProcessControlIdentifier();
    void setProcessControlIdentifier(ProcessControlIdentifier pcId);
    void setProcessControlIdentifier(String groupName, String threadName, String stateName);
    void setProcessControlIdentifier(String groupName, String threadName, String stateName, int rank);

    List getIdListToProcess(String idFileName);
    void setFastFailLoopProcessing(boolean tf);

    long getNextId();
    long getNextId(String groupName, String threadName, String stateName);
    long getNextId(ProcessControlIdentifier pcId);

    int processNextId();
    int processId(long id);

    int processIds();

    int processIds(List aList);
    int processIds(List aList, int rank);

    int postIds(List aList);
    int postIds(List aList, int rank);

    int postIds(int rank, org.trinet.util.DateRange dateRange);
    int postIds(int rank, double startTime, double endTime);

    int postId(long id);
    int postId(long id, int rank);
    int postId(long id, String groupName, String threadName, String stateName, int rank);

    int resultId(long id, int resultCode);
    int resultId(long id, String groupName,  String threadName, String stateName, int resultCode);
    int resultId(long id, String groupName,  String threadName, String stateName, int rank, int resultCode);
    int resultId(long id, ProcessControlIdentifier pcId, int resultCode);

    void setAutomatic(boolean tf);

    boolean setProperties(PcsPropertyList newProps);
    boolean setProperties(String propFileName);

}

