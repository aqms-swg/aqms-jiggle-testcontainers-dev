/** Similar to Exception to pass information back from a state processing module.
 *  Negative result code values are used for flagging errors. A positive result code value
 *  indicates success, for example, the total count processed for state.
 *  */
package org.trinet.pcs;
import java.util.HashMap;

public class ProcessingResult {

    // 
    // Basic primitive PCS processing code values returned from methods:
    //
    public static final int PASS = 1;   // method success, 1 unit processed
    public static final int FAIL = -1;  // error, unknown type
    public static final int NONE = 0;   // noop or no results
    //
    // enumeration of static data members of class instances:
    //
    public static final ProcessingResult FAILURE =
           new ProcessingResult("FAILURE", FAIL);

    public static final ProcessingResult PCS_SIGNAL_FAILURE =
           new ProcessingResult("PCS_SIGNAL_FAILURE", -2);

    public static final ProcessingResult NULL_POINTER =
           new ProcessingResult("NULL_POINTER", -5);

    public static final ProcessingResult NULL_INPUT =
           new ProcessingResult("NULL_INPUT", -10);

    public static final ProcessingResult DISQUALIFIED =
           new ProcessingResult("DISQUALIFIED", -15);

    public static final ProcessingResult INVALID_INPUT =
           new ProcessingResult("INVALID_INPUT", -20);

    public static final ProcessingResult INVALID_ID =
           new ProcessingResult("INVALID_ID", -30);

    public static final ProcessingResult INVALID_POSTING_RANK =
           new ProcessingResult("INVALID_POSTING_RANK", -40);

    public static final ProcessingResult ID_NOT_POSTED =
           new ProcessingResult("ID_NOT_POSTED", -41);

    public static final ProcessingResult ID_RANK_BLOCKED =
           new ProcessingResult("ID_RANK_BLOCKED", -42);

    public static final ProcessingResult ID_RESULT_FAILURE =
           new ProcessingResult("ID_RESULT_FAILURE", -43);

    public static final ProcessingResult MISSING_PROPERTY =
           new ProcessingResult("MISSING_PROPERTY", -50);

    public static final ProcessingResult INVALID_PROPERTY_VALUE =
           new ProcessingResult("INVALID_PROPERTY_VALUE", -55);

    public static final ProcessingResult NO_PROCESSOR_MODULE =
           new ProcessingResult("NO_PROCESSOR_MODULE", -60);

    public static final ProcessingResult UNKNOWN_PROCESSING_MODE =
           new ProcessingResult("UNKNOWN_PROCESSING_MODE", -65);

    public static final ProcessingResult LOC_CALC_FAILURE =
           new ProcessingResult("LOC_CALC_FAILURE", -100);

    public static final ProcessingResult MAG_CALC_FAILURE =
           new ProcessingResult("MAG_CALC_FAILURE", -120);

    public static final ProcessingResult PICKER_FAILURE =
           new ProcessingResult("PICKER_FAILURE", -130);

    public static final ProcessingResult DB_NO_CONNECTION =
           new ProcessingResult("DB_NO_CONNECTION", -500);

    public static final ProcessingResult DB_SQL_EXCEPTION =
           new ProcessingResult("DB_SQL_EXCEPTION", -510);

    public static final ProcessingResult DB_COMMIT_FAILURE =
           new ProcessingResult("DB_COMMIT_FAILURE", -520);

    public static final ProcessingResult DB_COMMIT_SOLUTION_STALE =
           new ProcessingResult("DB_COMMIT_SOLUTION_STALE", -521);

    public static final ProcessingResult DB_COMMIT_MAGNITUDE_STALE =
           new ProcessingResult("DB_COMMIT_MAGNITUDE_STALE", -522);

    public static final ProcessingResult DB_COMMIT_RFLAG_CANCELLED =
           new ProcessingResult("DB_COMMIT_RFLAG_CANCELLED", -525);

    public static final ProcessingResult DB_DATA_LOCK =
           new ProcessingResult("DB_DATA_LOCK", -530);

    public static final ProcessingResult MISSING_DB_DATA =
           new ProcessingResult("MISSING_DB_DATA", -200);

    public static final ProcessingResult INVALID_DATA =
           new ProcessingResult("INVALID_DATA", -210);

    public static final ProcessingResult NO_CHANNEL_DATA =
           new ProcessingResult("NO_CHANNEL_DATA", -220);

    public static final ProcessingResult NO_ENGINE_DELEGATE =
           new ProcessingResult("NO_ENGINE_DELEGATE", -225);

    public static final ProcessingResult JAVA_EXCEPTION =
           new ProcessingResult("JAVA_EXCEPTION", -9000);

    public static final ProcessingResult UNKNOWN_ERROR_TYPE =
           new ProcessingResult("UNKNOWN_ERROR_TYPE", -9999);

    // NOOP, success status dependent on usage
    public static final ProcessingResult NO_RESULTS =
           new ProcessingResult("NO_RESULTS", NONE);

    public static final ProcessingResult UNIT_SUCCESS =
           new ProcessingResult("UNIT_SUCCESS", PASS);

    public static final ProcessingResult ALT_SUCCESS =
           new ProcessingResult("ALT_SUCCESS", 2);

    private static HashMap resultMap = new HashMap(53);

    static {
      putInMap( FAILURE );
      putInMap( PCS_SIGNAL_FAILURE );
      putInMap( NULL_POINTER );
      putInMap( NULL_INPUT );
      putInMap( DISQUALIFIED );
      putInMap( INVALID_INPUT );
      putInMap( INVALID_ID );
      putInMap( INVALID_POSTING_RANK );
      putInMap( ID_NOT_POSTED );
      putInMap( ID_RANK_BLOCKED );
      putInMap( ID_RESULT_FAILURE );
      putInMap( MISSING_PROPERTY );
      putInMap( INVALID_PROPERTY_VALUE );
      putInMap( NO_PROCESSOR_MODULE );
      putInMap( UNKNOWN_PROCESSING_MODE );
      putInMap( LOC_CALC_FAILURE );
      putInMap( MAG_CALC_FAILURE );
      putInMap( PICKER_FAILURE );
      putInMap( DB_NO_CONNECTION );
      putInMap( DB_SQL_EXCEPTION );
      putInMap( DB_COMMIT_FAILURE );
      putInMap( DB_COMMIT_SOLUTION_STALE );
      putInMap( DB_COMMIT_MAGNITUDE_STALE );
      putInMap( DB_COMMIT_RFLAG_CANCELLED );
      putInMap( DB_DATA_LOCK );
      putInMap( MISSING_DB_DATA );
      putInMap( INVALID_DATA );
      putInMap( NO_CHANNEL_DATA );
      putInMap( JAVA_EXCEPTION );
      putInMap( UNKNOWN_ERROR_TYPE );
      putInMap( NO_RESULTS );
      putInMap( UNIT_SUCCESS );
      putInMap( ALT_SUCCESS );
    }

// Constructive elements below:
    private String description; 
    private Integer id;    
    private String message; // optional info text

    protected ProcessingResult(String description, int id) {
       this(description, id, null);
    }
    protected ProcessingResult(String description, int id, String message) {
       this(description, Integer.valueOf(id), message);
    }
    protected ProcessingResult(String description, Integer id, String message) {
       this.description = description;
       this.id = id;
       this.message=message;
    }

    public int getIdCode() {
        return id.intValue();
    }
    public String getDescription() {
        return description;
    }
    public String getMessage() {
        return message;
    }

    public String toString() {
        StringBuffer sb =  new StringBuffer(128);
        sb.append(id).append("=").append(description);
        if (message != null) sb.append(" : ").append(message);
        return sb.toString();
    }

    /** e.g. pr = FAILURE.copy("out of gas") */
    public ProcessingResult copy(String msg) {
       return new ProcessingResult(description, id, msg);
    }

    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || ! (obj instanceof ProcessingResult)) return false;
        ProcessingResult pr = (ProcessingResult) obj;
        return ( id.equals(pr.id) && description.equals(pr.description) ); 
    }

    public int hashCode() {
        return (id == null) ? 0 : id.intValue();
    }

    private static void putInMap(ProcessingResult pr) {
        Object prior = resultMap.put(pr.id, pr);
        if (prior != null) throw new IllegalArgumentException("Duplicate id code: " + pr.toString());
    }

    /** Return String describing the processing result, if any, mapped to the input code value.
     * @return null no mapping.
     * */
    public static String describe(int id) {
        ProcessingResult pr = (ProcessingResult) resultMap.get(Integer.valueOf(id));
        return (pr == null) ? null : pr.toString();
    } 
}
