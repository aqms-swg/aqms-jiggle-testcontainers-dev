package org.trinet.pcs;

/** Set the 'result' field of a data object in the given state.  */
public class PutResult {
    String group;
    String source;
    long id;
    String state;
    int rank;
    int result;

    public static void main(String args[]) {
        PutResult p = new PutResult();

        if (args.length < 6) {
          System.out.println("SYNTAX: PutResult <group> <source> <id> <state> <rank> <result>");
          return;
        }
        else {
          p.group = args[0];
          p.source = args[1];
          p.id = Long.parseLong(args[2]);
          p.state = args[3];
          p.rank = Integer.parseInt(args[4]); // string -> int
          p.result = Integer.parseInt(args[5]); // string -> int
        }

        int nchanged = ProcessControl.putResult(p.group, p.source, p.id, p.state, p.rank, p.result);

        // Provide some user feedback
        if (nchanged == 0) System.out.println(p.id+" : no such group/source/state/rank");
        else System.out.println(p.id+" : " + p.state +" result =>   "+p.result);

        // TODO: look up transition and inform user??
    }
}
