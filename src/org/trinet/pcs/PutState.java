package org.trinet.pcs;

  /**
   * Put an data object in the given state/rank.
   *  % SYNTAX: POST<group> <source> <id> <state> <rank>
   */
class PutState {
      //TODO: get defaults from a file?
      String group;        // = "ROUTINE";
      String table;        // = "EVENT";
      long    id ;
      String state;
      int    rank;

    public static void main(String args[]) {

        PutState p = new PutState();

        if (args.length < 5) {

          System.out.println("SYNTAX: putstate <group> <source> <id> <state> <rank>");
          return ;

        } else {
          
          p.group = args[0];
          p.table = args[1];
          p.id = Long.parseLong(args[2]);
          p.state = args[3];
          p.rank = Integer.valueOf(args[4]).intValue(); // string -> int

        }

        ProcessControl.putState(p.group, p.table, p.id, p.state, p.rank);

        // show all state/ranks for this id
        StateRow rows[] = StateRow.get(p.group, p.table, p.id);

        for (int i=0; i < rows.length; i++) {
          System.out.println(rows[i].toOutputString());
        }

    }

}
