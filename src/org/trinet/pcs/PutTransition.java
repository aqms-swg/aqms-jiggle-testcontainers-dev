package org.trinet.pcs;

  /**
   * Put an data object in the given state/rank.
   *  % SYNTAX: PutTransition <group> <source> <oldState> <result> <newState> <rank>
   */
class PutTransition {

  public static void main (String args[]) {
    TransitionRow tr = new TransitionRow();

    int count = args.length;
    if (count < 4) {
      System.out.println(
      "args: <groupOld> <sourceOld> <stateOld> <resultOld> [stateNew] [rankNew] [resultNew] [sourceNew] [groupNew]");
      System.out.println("Specify only the first 4 args to delete resulted pcs_state table rows without new posts.");
      return;
    }

    try {
      tr.groupOld   = args[0];
      tr.sourceOld  = args[1];
      tr.stateOld   = args[2];
      tr.resultOld  = Integer.valueOf(args[3]).intValue(); 

      if (count > 4) {
        tr.stateNew  = args[4];
        if (count > 5) tr.rankNew   = Integer.valueOf(args[5]).intValue();
        else tr.rankNew = 100;

        if (count > 6) tr.resultNew = Integer.valueOf(args[6]).intValue();          
        else tr.resultNew = 0;

        if (count > 7) tr.sourceNew = args[7];
        else tr.sourceNew = tr.sourceOld;

        if (count > 8) tr.groupNew  = args[8];
        else tr.groupNew = tr.groupOld;
      }

    } catch (Exception e){
         System.out.println(" Malformed command.");
    }

    tr.insert();

    // dump current transitions
    TransitionRow tra[] = TransitionRow.get (tr.groupOld, tr.sourceOld, tr.stateOld); 
    TransitionRow.dump(tra);

  }

}
