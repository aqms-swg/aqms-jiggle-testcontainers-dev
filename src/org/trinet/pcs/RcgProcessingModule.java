package org.trinet.pcs;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.trinet.jasi.*;
import org.trinet.util.BenchMark;
import org.trinet.util.DateTime;
import org.trinet.storedprocs.waveformrequest.RequestGenerator;
import org.trinet.storedprocs.waveformrequest.RequestCardTN;

/**
 * Processing Module used in context of a PCS Processing Controller to generate
 * waveform "Request Cards" for events immediately after event declaration.
 * 
 * The 'pcsModuleClassName' property passed to the controller would equal
 * the name of this class.<p>
 * 
 * pcsModuleClassName=org.trinet.pcs.RcgProcessingModule<p>
 *  
 */
public class RcgProcessingModule extends AbstractSolutionChannelDataProcessor {

    // class static data enumeration: added primary,shadow processing modes -aww 2012/03/06
    public static final int DEFAULT_MODE_ID = 0;
    public static final int PRIMARY_MODE_ID = 1;
    public static final int SHADOW_MODE_ID = 2;

    public static final SolutionProcessingMode DEFAULT_MODE = 
           new SolutionProcessingMode("DEFAULT", DEFAULT_MODE_ID);

    public static final SolutionProcessingMode PRIMARY_MODE = 
           new SolutionProcessingMode("PRIMARY", PRIMARY_MODE_ID);

    public static final SolutionProcessingMode SHADOW_MODE = 
           new SolutionProcessingMode("SHADOW", SHADOW_MODE_ID);

    // for each static mode instance:
    {
        modeMap.put(Integer.valueOf(DEFAULT_MODE_ID), DEFAULT_MODE);
        modeMap.put(Integer.valueOf(PRIMARY_MODE_ID), PRIMARY_MODE);
        modeMap.put(Integer.valueOf(SHADOW_MODE_ID), SHADOW_MODE);
    }
    
    // ////////////////////////////////////////////
    
    private RequestGenerator reqGen = new RequestGenerator();
    
    /** No-op Constructor. */
    public RcgProcessingModule() { }

    /**
     * Constructor initializes DummyEngineDelegate with property list and
     * sets processing mode to DEFAULT_MODE.
     */
    public RcgProcessingModule(SolutionWfEditorPropertyList props) {
        //this(props, SolutionProcessingMode.UNKNOWN);
        this(props, DEFAULT_MODE); // what should the default behavior be?
    }

    /**
     * Constructor initializes DummyEngineDelegate with input property list and
     * sets processing mode to the specified input mode.
     */
    public RcgProcessingModule(SolutionWfEditorPropertyList props, SolutionProcessingMode runMode) {
        super(props, runMode);
        
        // Instantiate the engine
        reqGen = new RequestGenerator(props);
        
        // Don't call setProperties() here - it will be called by the creator controller
    }

    // This is called by the Controller when the module is instantiated.
    // It is passed the controller's property list.
    // You may put moduel-specific properties either:
    //   1) directly in the main property file OR
    //   2) set a property that is the name of the modules property file (recommended)
    //      (Say, with 'pcsEngineDelegateProps')
    public boolean setProperties(SolutionEditorPropertyList propsObj) {
        this.props = propsObj;
        super.setProperties(props);
        String localPropFile = null;
        SolutionEditorPropertyList localProps;
        
        if (props.isSpecified("pcsEngineDelegateProps")) {
            localPropFile = props.getProperty("pcsEngineDelegateProps");
            localProps = new SolutionEditorPropertyList(localPropFile);
        } else {
            localProps = props;
        }
        
        if (localProps.isEmpty()) {
            System.err.println("Bad pcsEngineDelegateProps = "+localPropFile);
            return false;
            
        }

        // Set properties specific to the Request Generator 
        localProps.dumpProperties();
        reqGen.setProperties(localProps);
        
        return true;
    }
    
    /** ************************************************************
     * Process input Solution with the delegate as configured.
     * ************************************************************* */
    public int processSolution(Solution sol)  {
        
        int nreqs = 0;
        
        if (sol == null) {
            ProcessingResult.NULL_INPUT.getIdCode();
        }
        
        BenchMark bm = new BenchMark();
        long evid = sol.getId().longValue();

        boolean doit = true; // by default -aww 2012/03/06

        // Dynamic checking of database role (in case of role switch)
        SolutionProcessingMode mode = getProcessingMode();

        if (props.getBoolean("pcsCheckHostAppRole")) { // do we doit?
            String myhostname = "unknown";
            try {
              myhostname = InetAddress.getLocalHost().getHostName();
            } catch (UnknownHostException e) { }

            int idx = myhostname.indexOf(".");
            if (idx > 0) {
                myhostname = myhostname.substring(0, idx);
            }

            String myDbHostAppRole = getProcessingController().getHostAppRole(); // where role maps to mode of P, S, A or *
            if (debug)
                System.out.println("DEBUG RcgProcessingModule myhostname : " + myhostname + " myDbAppRole: " + myDbHostAppRole);

            if ( mode.equals(PRIMARY_MODE)  && myDbHostAppRole.startsWith("P") ) {
                //doit = DataSource.hasPrimaryRtRole(); // generates requests only if db is primary
                doit = DataSource.isPrimaryHost(myhostname); // generates requests only if host is primary
            }
            else if ( mode.equals(SHADOW_MODE) && myDbHostAppRole.startsWith("S") ) {
                //doit = !DataSource.hasPrimaryRtRole(); // generates requests only if db is shadow
                doit = !DataSource.isPrimaryHost(myhostname); // generates requests only if host is primary
            }
            else { // DEFAULT
                // if the host's role unknown doit by default
                doit = true;
                if (DataSource.isPrimaryHost(myhostname)) { // it's the primary host
                    // if app mode is All, Any, or Primary doit, but don't doit if app mode is run on shadow host
                    doit = !myDbHostAppRole.startsWith("S");
                }
                else { // host is the shadow standby 
                    // if app mode is All, Any, or Shadow doit, but don't if app mode is run on primary host 
                    doit = !myDbHostAppRole.startsWith("P");
                }
            }
        }

        System.out.println("-----------------------------------------------------"); 

        System.out.println("NEXT EVID: "+evid+ " found @ "+ new DateTime().toString()); // UTC time -aww 2008/02/08
        System.out.println (sol.toNeatString());

        if (!doit) { // a no-op, no requests generated, just result posting -aww 2012/03/06
            System.out.println ("SKIP EVID: "+evid+", no requests generated, db host app role does not match run mode: " + mode.getDescription());
            System.out.println("-----------------------------------------------------");        
            return ProcessingResult.UNIT_SUCCESS.getIdCode();
        }

        if ( sol.isOriginGType(GTypeMap.TELESEISM) ) {
            System.out.println ("SKIP EVID: "+evid+", no processing done, event origin gtype is teleseism");
            System.out.println("-----------------------------------------------------");        
            return ProcessingResult.UNIT_SUCCESS.getIdCode();
        }

        if ( ! sol.isEventDbType(EventTypeMap3.TRIGGER) ) {
          double minMag = props.getDouble("pcsMinMag", -9.);
          double mag = -9.;
          if (sol.magnitude != null) {
            mag = sol.magnitude.value.doubleValue();
          }
          if ( minMag > -9.  && (Double.isNaN(mag) || mag < minMag) ) {
            System.out.println ("SKIP EVID: "+evid+", no requests generated, event magnitude " + mag + " < " + minMag + " minMag acceptable");
            System.out.println("-----------------------------------------------------");        
            return ProcessingResult.UNIT_SUCCESS.getIdCode();
          }
        }

        int cnt = 0;
        if ( props.getBoolean("noopWhenWaveformsExist") ) {
          cnt = AbstractWaveform.createDefaultWaveform().getCountBySolution(evid);
          if (cnt > 0 ) {
            System.out.println ("SKIP EVID: "+evid+", no requests generated, event already has waveform count="+cnt);
            System.out.println("-----------------------------------------------------");        
            return ProcessingResult.UNIT_SUCCESS.getIdCode();
          }

        }

        if ( props.getBoolean("noopWhenRequestsExist") ) {
          cnt = RequestCardTN.countRequests(evid);
          if (cnt > 0 ) {
            System.out.println ("SKIP EVID: "+evid+", no requests generated, event already has request count ="+cnt);
            System.out.println("-----------------------------------------------------");        
            return ProcessingResult.UNIT_SUCCESS.getIdCode();
          }

        }

        // else do something
        int status = ProcessingResult.UNIT_SUCCESS.getIdCode();
        try {
          nreqs = reqGen.generateRequests(sol);
          //if (verbose || debug) reqGen.dumpChannelTimeWindowList(evid);
        }       
        catch (Exception ex) {
            status = ProcessingResult.FAILURE.getIdCode();
            ex.printStackTrace(System.err) ;  // print but ignore errors
        }
        
        System.out.println ("PROC EVID: "+evid+" generated "+reqGen.getChannelTimeWindowList().size()+
                " requests -- @ "+ new DateTime().toString());
        bm.printTimeStamp("PROC EVID: "+evid+" wrote     "+nreqs+ " requests,");

        System.out.println("-----------------------------------------------------");        

        // success or fail
        return status;
    }

    //IF subclass implementation might wrap delegate
    public boolean hasChannelList() { 
        return (reqGen.getCandidateList() != null);
    }

    //IF subclass implementation might wrap delegate
    public void setChannelList(ChannelList chanList) {
        reqGen.setCandidateList(chanList);
    }

}
