package org.trinet.pcs;
public class SolutionProcessingMode {

    public static final SolutionProcessingMode UNKNOWN = new SolutionProcessingMode();

    private String description = "UNKNOWN";
    private int id = -1;    

    private SolutionProcessingMode() {}

    // Changed from 'protected' to public for use outside this package. DDG 11/06
    public SolutionProcessingMode(String description, int id) {
       this.description = description;
       this.id = id;
    }

    public int getId() {
        return id;
    }
    public String getDescription() {
        return description;
    }
    public String toString() {
        return id + ":"+ description;
    }
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null  || ! (obj instanceof SolutionProcessingMode)) return false; 
        SolutionProcessingMode spm = (SolutionProcessingMode) obj;
        return ( id == spm.getId() && description.equals(spm.getDescription()) );
    }

    public int hashCode() { return id; }
}
