package org.trinet.pcs;

import org.trinet.jasi.*;
import org.trinet.util.BenchMark;
import org.trinet.util.DateTime;
import org.trinet.apps.UnassocAmps2Id;

/**
 * Processing Module wrapper used with a PCS Processing Controller
 * to invoke UnassocAmps2Id method associateAmps(Solution) for posted evid.
 * 
 * The 'pcsModuleClassName' property passed to the controller would equal
 * the name of this class.<p>
 * 
 * pcsModuleClassName=org.trinet.pcs.UnassocAmpsProcessingModule<p>
 *  
 */
public class UnassocAmpsProcessingModule extends AbstractSolutionChannelDataProcessor {

    // class static data enumeration:
    public static final int DEFAULT_MODE_ID = 0;

    public static final SolutionProcessingMode DEFAULT_MODE = 
           new SolutionProcessingMode("DEFAULT", DEFAULT_MODE_ID);

    // for each mode instance:
    {
        modeMap.put(Integer.valueOf(DEFAULT_MODE_ID), DEFAULT_MODE);
    }
    
    // ////////////////////////////////////////////
    
    private UnassocAmps2Id ua2id = new UnassocAmps2Id();

    /** No-op Constructor. */
    public UnassocAmpsProcessingModule () { }

    /**
     * Constructor initializes DummyEngineDelegate with property list and
     * sets TEST_MODE_ID as the mode (default) for processing.
     */
    public UnassocAmpsProcessingModule(SolutionWfEditorPropertyList props) {
        this(props, DEFAULT_MODE); // what should the default behavior be?
    }

    /**
     * Constructor initializes DummyEngineDelegate with input property list and
     * sets mode (default) for processing to the specified input mode.
     */
    public UnassocAmpsProcessingModule(SolutionWfEditorPropertyList props, SolutionProcessingMode defaultMode) {
        super(props, defaultMode);
        // Don't call setProperties() here - it will be called by the creator controller
    }

    // This is called by the Controller when the module is instantiated.
    // It is passed the controller's property list.
    // You may put module-specific properties either:
    //   1) directly in the main property file OR
    //   2) set a property that is the name of the modules property file (recommended: 'pcsEngineDelegateProps')
    public boolean setProperties(SolutionEditorPropertyList props) {

        super.setProperties(props);

        //String localPropFile = props.getUserFileNameFromProperty("pcsEngineDelegateProps");
        String localPropFile = props.getProperty("pcsEngineDelegateProps");
        if (localPropFile == null) {
        	System.err.println("Bad property, pcsEngineDelegateProps = "+localPropFile);
        	return false;
        }
        // Set properties specific to the app engine
        boolean status = ua2id.setProperties(localPropFile);
        if (!status) {
            System.err.println ("Bad properties file: "+localPropFile);
            return false;
        }
        
        return true;
    }
    
    /** ************************************************************
     * Process input Solution with the delegate app module as configured.
     * ************************************************************* */
    public int processSolution(Solution sol)  {
    	
        if (sol == null) {
            return ProcessingResult.NULL_INPUT.getIdCode();
        }

        if ( ! sol.isEventDbType(EventTypeMap3.TRIGGER) ) {
          double minMag = props.getDouble("pcsMinMag", -9.);
          double mag = -9.;
          if (sol.magnitude != null) {
            mag = sol.magnitude.value.doubleValue();
          }
          if ( minMag > -9.  && (Double.isNaN(mag) || mag < minMag) ) {
            System.out.println ("SKIP EVID: "+sol.getId().longValue()+", no amps associated with evid, event magnitude " + mag + " < " + minMag + " minMag acceptable");
            System.out.println("--------------------------------------------------------------------------------");        
            return ProcessingResult.UNIT_SUCCESS.getIdCode();
          }
        }
        
        boolean status = true;
        try {
            status = ( ua2id.associateAmps(sol) >= 0 ) ;
        }       
        catch (Exception ex) {
            status = false;
            ex.printStackTrace(System.err) ;  // print but ignore errors
        }

        return (status) ? ProcessingResult.UNIT_SUCCESS.getIdCode() : ProcessingResult.FAILURE.getIdCode();
    }

    //IF subclass implementation might wrap delegate
    public boolean hasChannelList() { 
    	return false;
    }

    //IF subclass implementation might wrap delegate
    public void setChannelList(ChannelList chanList) {
    }

}
