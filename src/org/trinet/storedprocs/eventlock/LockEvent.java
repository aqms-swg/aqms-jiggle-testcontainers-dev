package org.trinet.storedprocs.eventlock;

import org.trinet.jasi.*;
import java.util.ArrayList;

/**
 * LockEvent.java
 *
 *
 * Created: Tue Aug 15 17:13:54 2000
 *
 * @author Doug Given
 * @version
 */

public class LockEvent  {

    public LockEvent() {

    }

    public static void syntax() {

	System.out.println("Syntax: lockEvent <evid>");
    }

    public static void main (String args[]) {


	long evid;
	if (args.length == 0) {

	    syntax();
	    System.exit(0);
	}

	Long val = Long.valueOf(args[0]);
	evid = (long) val.longValue();

	new DataSource();	// make connection

	EnvironmentInfo.setApplicationName("LockEvent");

	SolutionLock solLoc = SolutionLock.create();

	solLoc.setId(evid);

	if (solLoc.isLocked()) {
	    System.out.println("evid "+ evid+ " is already locked");
	    System.out.println(solLoc.toString());
	} else {
	    if ( solLoc.lock() ) {
		//	    System.out.println("Locked: "+evid);
		System.out.println(solLoc.toString());
	    } else {
		System.out.println("Could not lock "+ evid);
	    }

	}
    }

} // LockEvent
