package org.trinet.storedprocs.eventlock;

import org.trinet.jasi.*;
import java.util.ArrayList;

/**
 * LockEvent.java
 *
 *
 * Created: Tue Aug 15 17:13:54 2000
 *
 * @author Doug Given
 * @version
 */

public class UnlockEvent  {

    public UnlockEvent() {

    }

    public static void syntax() {

	System.out.println("Syntax: UnlockEvent <evid>");
    }

    public static void main (String args[]) {


	long evid;
	if (args.length == 0) {

	    syntax();
	    System.exit(0);
	}

	Long val = Long.valueOf(args[0]);
	evid = (long) val.longValue();

	new DataSource();	// make connection

	SolutionLock solLoc = SolutionLock.create();

	solLoc.setId(evid);

	if (solLoc.isLocked()) {

	    if (solLoc.unlock() ) {
		System.out.println("Unlocked evid = "+evid);
	    } else {
		System.out.println("Could not unlock "+ evid);
	    }
	} else {
	    System.out.println("evid = "+evid+" is not locked.");
	}

    }

} // LockEvent
