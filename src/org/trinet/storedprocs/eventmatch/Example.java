package org.trinet.storedprocs.eventmatch;

import java.text.*;
import java.util.*;
import java.net.URL;
import java.sql.*;

import org.trinet.jasi.*;

public class Example

{

/** Make an empty Test2 */
    public Example () {}

    public static long getMatch (double lat, double lon, double z, double ot) {

    String sql = "{?=call MATCH.getMatch (?,?,?,?)}";

    try {

        CallableStatement cs = DataSource.getConnection().prepareCall(sql);

        cs.registerOutParameter (1, java.sql.Types.BIGINT);
        cs.setDouble(2, lat);
        cs.setDouble(3, lon);
        cs.setDouble(4, z);
        cs.setDouble(5, ot);

        ResultSet rs = cs.executeQuery();

        return rs.getLong(1);

        } catch (SQLException ex) {
        System.out.println ("SQL=\n"+sql);
        System.err.println(ex);
        ex.printStackTrace();
        return -1;
        }
    }


/**
 * Main for testing: % Test [hours-back]  (default = 1)
 */
    public static void main (String args[]) {

    TestDataSource.create(args[0]);    // make connection
    double lat =  32.614;
    double lon = -116.16;
    double   z = 6.0;
    double  ot = 948198526.0;

    System.out.println ("MATCH = "+ getMatch(lat, lon, z, ot));
    }

} // end of class


