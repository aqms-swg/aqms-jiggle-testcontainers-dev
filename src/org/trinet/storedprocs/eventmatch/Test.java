package org.trinet.storedprocs.eventmatch;

import java.text.*;
import java.util.*;
import java.net.URL;
import java.sql.*;

import org.trinet.jdbc.*;
import org.trinet.util.*;
import org.trinet.jasi.*;
import org.trinet.jasi.TN.SolutionTN;

//import org.trinet.eventmatch.*;
//import org.trinet.jiggle.*;

/**
 * This is quicker then CatalogList because it doesn't count the Waveforms.
 */


public class Test {

    /** Make an empty Test */
    public Test() { }

    /**
     * Main for testing: % Test host [hours-back]  (default = 1)
     */
    public static void main(String args[]) {

        if (args.length < 2) {
            System.out.println("Syntax main args:  hostName dbName [hrsBack]");
            System.exit(0);
        }

        double hoursBack;
        // final int secondsPerHour = 3600;

        if (args.length > 2) {
            Double val = Double.valueOf(args[2]);     // convert arg String to 'double'
            hoursBack = (double) val.doubleValue();
        } else {
            System.out.println("Usage: java Test [hours-back]  (default=24)");
            hoursBack = 24;   // default
        }

        // make connection
        System.out.println("Making connection...");

        TestDataSource.create(args[0], args[1]); // new DataSource (url, driver, user, passwd);    // make connection

        // double now = new DateTime().getTrueSeconds(); // current epoch sec (millisecs -> seconds)
        // double then = now - (secondsPerHour * hoursBack);  // convert to seconds

        System.out.println("Fetching last "+hoursBack+" hours...");

        // Get ALL events
        //Solution sol[] = Solution.create().getValidByTime(then, now);
        //Solution sol[] = Solution.create().getByTime(then, now);

        //selectflag 0 events
        //String sql = " Event.selectflag = 0 and datetime > "+then;

        //  437 ISAIAH events
        String sql = "Event.auth = 'RTP' and Event.evid > 10000000";

        System.out.println(sql);
        // Returns a collection  -aww
        SolutionTN sol[] = (SolutionTN []) new SolutionTN().getWhere(sql).toArray(new SolutionTN[0]);
        long evidm;

        for (int i = 0; i < sol.length; i++) {
            // Only operate on RT2 (backup) event
            //if (sol[i].source.toString().equals("RT2")){
              System.out.println("-------------");
              System.out.println(sol[i].toSummaryString() + " " +
                    sol[i].authority.toString() + " " +
                    sol[i].source.toString() + " " +
                    sol[i].processingState.toString());


              evidm = EventMatch.getMatchEvid(sol[i].lat.doubleValue(),
                      sol[i].lon.doubleValue(),
                      sol[i].depth.doubleValue(),
                      sol[i].datetime.doubleValue());

              if (evidm > -1) System.out.println("MATCH = "+evidm);
            //}

        }
    }
} // end of class
