package org.trinet.storedprocs.eventpriority;

import java.util.*;
import java.sql.*;

import org.postgresql.pljava.annotation.Function;
import org.trinet.jasi.JasiDatabasePropertyList;
import org.trinet.jiggle.common.DbConstant;
import org.trinet.storedprocs.CatEntry;
import org.trinet.storedprocs.DbaseConnection;
import org.trinet.util.DateTime;

/** Calculate the "priority" of the event based upon its magnitude, origin time and quality. */ 
public class EventPriority {

    /* Default values for factors */
    static final double  magFactorDef  = 5.0;
    static final double  ageFactorDef  = 1.0;
    static final double  qualFactorDef = 2.0;

    static double magFactor  = magFactorDef;
    static double ageFactor  = ageFactorDef;
    static double qualFactor = qualFactorDef;

    /** True if factors have been set from the dbase. */
    static boolean factorsSet = false;

    static final double SECS_PER_DAY = 86400.0;

    /** The database connection. */
    static Connection conn = null;


    public EventPriority() { }

    /** Option to set the dbase connection. If this is not called it will use
        default connection which is OK if you are internal to the dbase but not otherwise.
        @see: org.trinet.eventmatch.DbaseConnection
    */
    public static void setConnection(Connection connection) {
        conn = connection;
        CatEntry.conn = conn;
    }

    /**
     * Set the factor parameters from the database if they haven't been already. Will not
     reset if they were set earlier.
     @see: resetFactors()
     Factors are kept in a table called:
<tt>
     EventPriorityParams (
       magnitudeFactor  number  NOT NULL,
       ageFactor        number  NOT NULL,
       qualityFactor    number  NOT NULL
     )
</tt>
     */
    public static void setFactors() {
        if (factorsSet) return;  // already set
        resetFactors(); // only called if not already set
    }

    /**
     * Reset the factor parameters from EventPriorityParams table, using default DataSource connection. If no connection,
     * use default values  magnitudeFactor = 5., ageFactor = 1., and qualityFactor = 2.0.
<tt>
     EventPriorityParams (
       magnitudeFactor  number  NOT NULL,
       ageFactor        number  NOT NULL,
       qualityFactor    number  NOT NULL
     )
</tt>
     */
    public static void resetFactors() {

        factorsSet = true; // move below if you always want to test for db data 
        
        magFactor = magFactorDef;
        ageFactor = ageFactorDef;
        qualFactor = qualFactorDef;

        // use above default if none defined
        if (conn == null) {
            // aww fix below , conn is class static, not a local variable 03/08/2005
            setConnection(DbaseConnection.getConnection()); // aww 03/08/2005
        }

        if (conn == null) {
            if (JasiDatabasePropertyList.debugSQL) 
                System.out.println("DEBUG: EventPriorityParms null db connection, using defaults: "+magFactor+", "+ageFactor+", "+qualFactor);
            return;
        }

        String sql = "Select magFactor,ageFactor,qualFactor from EventPriorityParams";

        Statement stmt = null;
        ResultSet rs = null;

        try {
            stmt = conn.createStatement();
            if (JasiDatabasePropertyList.debugSQL) System.out.println(sql);
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
              magFactor  = rs.getDouble("magFactor");
              ageFactor  = rs.getDouble("ageFactor");
              qualFactor = rs.getDouble("qualFactor");
              if (JasiDatabasePropertyList.debugSQL) 
                System.out.println("DEBUG: EventPriorityParms table values magFactor,ageFactor,qualFactor: "+magFactor+", "+ageFactor+", "+qualFactor);
                //factorsSet = true; // have db data 
            }
            else if (JasiDatabasePropertyList.debugSQL) 
                System.out.println("DEBUG: EventPriorityParms table has NO rows, using defaults: "+magFactor+", "+ageFactor+", "+qualFactor);
           
        } catch (SQLException ex) {
            ex.printStackTrace(System.err);
        }
        finally {
          try {
            //close statement & resultset
            if (rs != null) rs.close();
            if (stmt != null) stmt.close();
          }
          catch (SQLException ex) { }
        }
    }

    /** Return the priority of an event with this mag, origin time and quality. Mag is
        the event local magnitude, age is how old the event is in days, and
        quality is the event quality in the range of 0.0 to 1.0, 1.0 being the
        best.*/
    // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    @Function(schema= DbConstant.SCHEMA_EVENTPRIORITY, name="getPriority")
    public static double getPriority(double mag, double originTime, double quality) {
         // to handle bad data input
         if (Double.isNaN(mag)) mag = 0.01;
         if (Double.isNaN(originTime)) originTime = 0.0;
         if (Double.isNaN(quality)) quality = 1.0;

        // make sure factors have been set (hopefully from db table)
        setFactors();

        double safeMag = (mag <= 0.0) ? 0.01 : mag;  // no /0 errors, please

         double xmag  = safeMag * magFactor; // larger mag, higher priority
         double xage  = 1.0 / (getAge(originTime) + 0.1) * ageFactor; // younger, higher priority
         double xqual = 1.0 / (quality+0.1) * qualFactor; // lower qual, higher priority

         // The largest mag, or older origin, or the worst quality has a higher priority
         double prio = xmag + xage + xqual;

        //System.out.println(xmag + "  "+xage+"  "+xqual+ "      = "+ prio);

        return prio;
    }


    /** Return the priority of the 'evid' in the dbase. Returns 0.0 if no such event.*/
    // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    @Function(schema= DbConstant.SCHEMA_EVENTPRIORITY, name="getPriority")
    public static double getPriority(long evid) {
        CatEntry cat = CatEntry.getCatEntryByEvid(evid) ;
        if (cat == null) return 0.0;
        return getPriority(cat.mag, cat.ot, cat.quality);
    }

    /** Return the current GMT time in true epoch seconds. */
    static double getCurrentTrueSeconds() {
        return (double) new DateTime().getTrueSeconds();  // current time true seconds UTC - aww 2008/02/08
    }

    /** Given an origin time in epoch seconds, return its age in DAYS. */
    public static double getAge(double originTime) {
        return  (getCurrentTrueSeconds() - originTime) / SECS_PER_DAY;
    }

/*
    public static void main(String[] args) {

        // test time
        System.out.println("GMT = "+getCurrentTrueSeconds());

          //for (double mag = 0; mag <= 6.0; mag = mag + 0.5) {
               //for (double age = 0; age <= 6.0; age = age + 0.5) {
                    //for (double qual = 0; qual <= 1.0; qual = qual + 0.25) {
                       //System.out.println(mag+" "+age+" "+qual+"\t"+ (int) EventPriority.getPriority(mag, age, qual));
          //}}}

        if (args.length <= 0) {
          System.out.println("Usage: EventPriority [evid] ");
          System.exit(0);
        }

        // event ID
        Long val = Long.valueOf(args[0]);
        long evid = (long) val.longValue();

        //     long evid = 9564897;
        CatEntry cat = CatEntry.getCatEntryByEvid(evid) ;

        System.out.println(cat.toString());
        System.out.println("Priority = "+ getPriority(evid));
    }
*/
}
