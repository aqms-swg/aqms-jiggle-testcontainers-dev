package org.trinet.storedprocs.waveformrequest;

import org.trinet.jasi.*;
import org.trinet.util.*;

//
//  THIS CODE IS INCOMPLETE
//

/**
 *Create a brand new event <b>FROM SCRATCH</b> using the passed argument values.
 * @author Doug Given
 */

/**
 * Mode #1 = New trigger (follows old "newtrig" syntax)
 * Syntax: EventGenerator -n <start-date> <start-time> <pre-event> <duration>
 * 
 * Mode #2 = New event
 */
public class EventGenerator {

    public EventGenerator() { }

    /*
     * On success, returns the event ID for the event as created in the dbase.
     * @param dt true UTC seconds origin time
     * @param llz LatLonZ of hypocenter
     * @param eventType
     * @param mag Magnitude object
     * @param makeWaveformReqs set true if waveform request are to be generated
     * @return the event ID for the event as created in the dbase or '0' on failure.
     */
     //  public static long generateEvent(double dt, LatLonZ llz, int eventType,
     //                                  Magnitude mag, boolean makeWaveformReqs ) throws Exception {

    /**
     * Creates new event in database whose parameters are those of the input Solution.
     * On success, returns the event ID for the event as created in the dbase.
     * @param makeWaveformReqs if true, generate wavequest request table rows
     */
    public static long generateEvent(Solution sol, boolean makeWaveformReqs) throws Exception {
        try {
            sol.commit();
        }
        catch (JasiCommitException ex) {
            System.err.println(ex.toString());
            return 0;
        }
        // debug
        System.out.println(sol.toNeatString()) ;

        if (makeWaveformReqs) generateWaveformReqs(sol);
        return sol.getId().longValue();
    }
    

    /**
     * Creates new event in database whose parameters are those of the input Solution.
     * On success, returns the event ID for the event as created in the dbase.
     * @param makeWaveformReqs if true, generate wavequest request table rows
     */
    public static long generateTrigger(Solution sol, boolean makeWaveformReqs) throws Exception {

        try {
            sol.commit();
        }
        catch (JasiCommitException ex) {
            System.err.println(ex.toString());
            return 0;
        }
        // debug
        System.out.println(sol.toNeatString()) ;
      
        //NamedChannelTimeWindowModel
        if (makeWaveformReqs) {
            generateWaveformReqs(sol);
        }
        return sol.getId().longValue();
    }
    
    /** Generate request cards for this solution. The solution must be committed to
    the dbase before this is called. */
    public static int generateWaveformReqs(Solution sol) throws Exception {
        RequestGenerator rg = new RequestGenerator();
        return rg.generateRequests(sol.getId().longValue());
    }

  
    public static void printHelp () {
        System.out.println("");
        System.out.println("");
        printCommandOptions();
        System.out.println("");
        System.out.println("");      
    }

    /**
    * Print out all legal command options.
    *
    */
    public static void printCommandOptions() {
        System.out.println("Switches:");
        System.out.println(" -h              Help");
        System.out.println(" -d <database>   Database (defaults to master)");
        System.out.println(" -w              Generate waveform requests");
        System.out.println(" -c <filename>   Config file name (Default: $HOME/.jiggle/reqCard.cfg)"); 
       
        System.out.println(" -t <originTime> Origin time: 'yyyy/mm/dd hh:mm:ss.sss'");
        System.out.println(" -l <location>   Location: 'xx.xxxx xxx.xxxx xx.x'   ");
        System.out.println(" -m <mag>        Mag and type '5.3 w' ");
        System.out.println(" -a <auth>       Authority: (e.g. 'CI', 'BK'");
        System.out.println(" -e <etype>      EventType: (e.g. 'eq', 'sn', 'qb'");
       
        System.out.println(" -f <filename>   QuakeML format even file.");
        System.out.println(" -v              Verbose mode - more results printed.");
    }

  /*
  public static void main(String[] args) {
//    EventGenerator eg = new EventGenerator();
      
// Pseudo args
      
//      LatLonZ llz = new LatLonZ(34.0, -118.0, 6.0);  
//      String auth = "CI";
//      String subsource = "EventGen";
//      int eventType = EventTypeMap.EARTHQUAKE;      // use 2-char code?
//      //int eventType = EventTypeMap.TRIGGER;      // use 2-char code?
      
      //double dt = new DateTime().getTrueSeconds();// now
      double dt = LeapSeconds.stringToTrue("2009-09-12 00:40:00.000");
      double preevent = 0; 
      double duration = 1200; //secs
      double startTime = dt - preevent;
      double stopTime  = dt + duration;
      
      String auth = "CI";
      String subsource = "EventGen";
      int eventType = EventTypeMap.TRIGGER;      // use 2-char code?
      String who = "TST";   // is there a method or is it a property?
      
//      Double magVal = 3.0;
//      String magType = "h";
//      String magAuth = auth;
//      String magSubsource = subsource;
//      String magAlgo = subsource;

      
// Sol
      
      Solution sol = Solution.create();
      sol.setTime(dt);
//      sol.setLatLonZ(llz);
      sol.setAuthority(auth);
      sol.setSource(subsource);
      sol.setWho(who);
      
      if (!EventTypeMap.isValid(eventType)) {
          System.err.println("Bad event type. " + eventType);
          System.exit(0);
      }
      
// Mag    
//    Magnitude mag = Magnitude.create();
//    mag.value.setValue(magVal);
//    mag.setType(magType);
//    mag.setAuthority(magAuth);
//    mag.setSource(magSubsource);
//    mag.algorithm.setValue(magAlgo);
//    sol.eventType.setValue( eventType ); // to not bump version -aww 2008/04/15
//
//    sol.setPreferredMagnitude(mag);
    
    
    //String dbaseHost = "iron";
    String dbaseHost = "iron";
    DataSource.set(TestDataSource.create(dbaseHost).getConnection());

    boolean makeReqCards = false;
    try {
//      EventGenerator.generateEvent(dt, llz, EventTypeMap.EARTHQUAKE, mag, makeReqCards);
        EventGenerator.generateEvent(sol, makeReqCards);
    }
    catch (Exception ex) {
        System.err.println(ex.toString());
    }
  }
*/

} // End of class
