package org.trinet.storedprocs.waveformrequest;

import java.sql.*;
import java.util.*;

import org.postgresql.pljava.annotation.Function;
import org.trinet.jiggle.common.DbConstant;
import org.trinet.storedprocs.DbaseConnection;
import org.trinet.jasi.*;
import org.trinet.jasi.TN.*;
import org.trinet.util.*;
import org.trinet.jdbc.datasources.*;

/**
 * <p>Title: </p>
 * <p>Description: Create Waveform Requests for an event.
 * Waveform requests are channel/time-windows that should include all seismic signal
 * of interest for and event. This class determines the channel/time-windows using the
 * JBChannelTimeWindowModel. It writes Waveform request rows to a table using the
 * RequestCardTN class. </p>
 *
 * This class is intended to run as a stored procedure from within the dbase but will
 * also function from outside.
 *
 * Properties values:<p>
 * model - full class name of ChannelTimeWindowModel to use. Default = org.trinet.jasi.JBChannelTimeWindowModel<br>
 * auth ? character string to be written to the ?Auth? field of the request row (Max length = 15) This is usually the NET code.<br>
 * staAuth ? character string to be written to the ?StaAuth? field of the request row (Max length = 15). Something like ?SCEDC?.<br>
 * subsource? character string to be written to the ?Subnet? field of the request row (Max length = 8)  The creator of the requests; defaults to ?ReqGen?.<br>
 * priority ? integer value indicating priority of retrieval. Usually set = 1.<br>
 * logfile ? path/name of the logfile<br>
 * logfileDuration ? length of time (in seconds) an instance of a log file is open.<br>
 * verbose - use verbose info and error logging (true or false)
 * <p>
 *
 * There should also be properties specific to the model. For example:<p>
 * org.trinet.jasi.JBChannelTimeWindowModel.includeAllComponents=true<br>
 * org.trinet.jasi.JBChannelTimeWindowModel.includeAllMag=3.5<br>
 * org.trinet.jasi.JBChannelTimeWindowModel.maxDistance=10000<br>
 * org.trinet.jasi.JBChannelTimeWindowModel.maxWindowSize=300<br>
 * org.trinet.jasi.JBChannelTimeWindowModel.minDistance=30<br>
 * org.trinet.jasi.JBChannelTimeWindowModel.minWindowSize=60<br>
 * org.trinet.jasi.JBChannelTimeWindowModel.noiseThreshold=25<br>
 *
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: USGS</p>
 * @author Doug Given
 * @version 1.0
 */

/*  TODO
o test and verify that the set of channel/time-windows is what we want
o test speed of writing request rows to the dbase
o set config values from a small, simple table or properties file
*/

public class NCRequestGenerator {

  /** The model used to define Channels and time windows to make requests for.*/
  static ChannelTimeWindowModel model =
      new org.trinet.jasi.NCChannelTimeWindowModel();
/** Database connection */
  static Connection conn;

  /** Default list of component types to include. Any other type is excluded.
   * Blank list means include all channels that are in the candidate list. */

 /** If false no request cards will be written to the dbase. Good for debugging.
  * Set with the 'writeReqs' property. */
  //static boolean writeReqs = false;
  static boolean writeReqs = true;
  /** If true verbose output is written. Good for debugging.
  * Set with the 'verbose' property. */
  static boolean verbose = true;

  /** The properties object. */
  static ReqGenProperties props = null;

  // If the filename is set by properties stdout & stderr are redirected to this file
  static Logger logger = new Logger();

  /** channel-timewindow list */
  static ChannelableList ctwList;
  /** RequestCardTN list */
  static ActiveArrayList rcList;
  static ChannelTN chTN = new ChannelTN(); // instance used to create candidate lists
/** These default values will be used to fill in request card fields.*/
  static RequestCardTN ExampleCard = new RequestCardTN();

  /** 
   * Flag set to true if the 'priority' property is set to "Mag".
   * Indicates that ReqCard priority should be set to the inter value
   * of the event magnitude.
   */
    static boolean setPriorityToMag = false;

  public NCRequestGenerator() {
    // establish dbase connection
    getConnection();
  }

  /** Just return how many request cards WOULD be generated but don't actually
   * create them. Used to test Oracle stored procedure.*/
  @Function(schema= DbConstant.SCHEMA_WAVEARC, name="GenerateRequestCount")
  public static int generateRequestCount(long evid) throws Exception {
    ChannelableList ctwList = createCTWlist(evid);
    if (ctwList == null) return 0;
    return ctwList.size();
  }

  /** Create a Channel-TimeWindow list for this solution. Include channels for
   * any parameters (mag amps, phases, or codas) that are associated with the solution.*/
  public static ChannelableList createCTWlist (long evid) throws Exception {
    if (getConnectionWithExceptions() == null) throw new Exception("Cannot make database connection.");
    Solution sol = Solution.create().getById(evid);
    if (sol == null) throw new Exception("No such event in dbase. Evid = "+evid);
    return createCTWlist(sol);
  }

  /** Create a Channel-TimeWindow list for this solution. Include channels for
   * any parameters (mag amps, phases, or codas) that are associated with the solution.*/
  public static ChannelableList createCTWlist (Solution sol) throws Exception {

    if (sol == null) throw new Exception("Null solution passed to createCTWlist()");
    if (getConnectionWithExceptions() == null) throw new Exception("Cannot make database connection.");
    if (!sol.hasLatLonZ())  throw new Exception("Solution has no location.");

    // create candidate channel list
    ChannelableList candidateList = NCRequestGenerator.getCandidateChannelList();
    if (candidateList == null) throw new Exception("Candidate channel list is null.");

// Added channels with parameter (picks, amps, codas) if requested by config
// DON'T use a ChannelableSetList for this because Triax components will be handled
// later in the processing - doing it here will make later code less efficient.
//  ChannelableSetList requiredChans = new ChannelableSetList();
// no longer needed
//    ChannelableList requiredChans = new ChannelableSetList();
// Set some model values
//    model = new JBChannelTimeWindowModel();
//    model.setCandidateList(candidateList);
//    model.setCandidateList();    // sets to default if not set yet
    model.setSolution(sol);

// Generate a ChannelTimeWindow List using the model
    ctwList = (ChannelableList) model.getChannelTimeWindowList();
    return ctwList;

  }

  /**
   * Generate the waveform requests for this event number. The solution will be read
   * from the dbase. Phases, Codas, & Mag amps will be read in.
   * @param evid
   * @return int = number of request rows written
   */
   /* SQL call spec.
    FUNCTION GenerateRequests Return NUMBER
        AS LANGUAGE JAVA
        Name 'org.trinet.storedprocs.waveformrequests.NCRequestGenerator.generateRequests
        ( long )
        return int';
   */
  @Function(schema= DbConstant.SCHEMA_WAVEARC, name="GenerateRequests")
  public static int generateRequests(long evid) throws Exception {

    if (getConnectionWithExceptions() == null) throw new Exception("Cannot make database connection.");
    //return 0;
    Solution sol = Solution.create().getById(evid);
    if (sol == null) throw new Exception("No such event in dbase. Evid = "+evid);

    if (getVerbose()) System.out.println(sol.toSummaryString());
    return generateRequests(sol);
  }

  /** */
//  public static int testCandidates() throws Exception {
//
//    if (getConnectionWithExceptions() == null) throw new Exception("Cannot make database connection.");
//    //return 0;
//    // create candidate channel list
//    ChannelableList possiblesList = NCRequestGenerator.getCandidateChannelList();
//    if (possiblesList == null)  throw new Exception("Cannot make candidate list.");
//    return possiblesList.size();
//  }

  /** */
//  public static int testException() throws Exception {
//    int val = 1234;
//    throw new Exception("This is exception text: "+val);
//  }
  /**
   * Generate the waveform requests for this Solution.
   * Assumes Phases, Codas, & Mag amps have already been read in if you want them included.
   * @param sol
   * @return int  number of request rows written
   */

  public static int generateRequests(Solution sol) throws Exception {

    if (sol == null) throw new Exception("No such event in dbase.");
    if (!sol.hasLatLonZ())  throw new Exception("Solution has no location.");
    if (getConnectionWithExceptions() == null) throw new Exception("Cannot make database connection.");

    int knt = 0;
    
    // Create CTW's using the default model
    ctwList = createCTWlist(sol);
    if (ctwList == null) throw new Exception("Null Channel-TimeWindow list.");

    // Collect the time channels
    String timeChannelNames[] = {"ATT"};
    ChannelList timeChannels = ChannelList.getByComponent(timeChannelNames);
    if (timeChannels.isEmpty()) {
    	System.out.println("NCRequestGenerator: no time channels configured.");
    }
    else if (getVerbose()) {
    	for (int i = 0; i < timeChannels.size(); i++){
    		System.out.println("time channel: " + timeChannels.get(i).toString());
    	}
    }

    // report
    if (getVerbose()) {
      System.out.println("Total chans req'ed = "+ctwList.size());
    }

    // //////////////////////////////////////////////////////////
    // EVILNESS: force any 'null' loccodes to '01'
    // These happen when a CTW is made from the required list, 
    // i.e. from the list of phases - because the RT system writes null loccodes!
      //forceNonnullLocs (ctwList, "01");
      forceNonnullLocs (ctwList, "  ");
      // Changed to "--"  12/16/05 DDG
    
    // //////////////////////////////////////////////////////////    
    // generate RequestCardTN "cards"
    RequestCardTN rcg = new RequestCardTN(DataSource.getConnection());

    ExampleCard.evid.setValue(sol.getId().longValue());      // will "associate" all rows w/ this evid
    // set priority to mag. Note priority is an integer, 
    // higher priority numbers are processed first. (DDG 4/13/06)
    if (setPriorityToMag)
    	ExampleCard.priority.setValue(sol.magnitude.getMagValue());
    
    rcList = rcg.makeList(ctwList, ExampleCard);
 
    if (getVerbose()) {
      System.out.println("Requested chans = "+rcList.size());
//      dumpRequestCardList();
    }
    // this is a logical stopper for debugging w/o writting to the dbase

    if (writeReqs) {

      // insert the requests
      try {
    	System.out.println("Writting requests to dbase... "+rcList.size());
        knt = rcg.dbaseInsertList(rcList);
      }
      catch (Exception ex) {
        ex.printStackTrace(System.err) ;  // print but ignore errors
      }

      // commit
      try {
      	System.out.println("Committing requests to dbase... "+ knt);
    	  DataSource.getConnection().commit();
      }
      catch (SQLException ex) {
        System.err.println("Commit failed");
        throw ex;   // rethrow (or throw new?)
      }
    } else {
    	System.out.println("No dbase rows written because 'writeReqs' = false.");
    }
     return knt;
  }

  // EVILNESS: force any 'null' loccodes to '01'
  // These happen when a CTW is made from the required list, 
  // i.e. from the list of phases - because the RT system writes null loccodes!
  static void forceNonnullLocs (ChannelableList ctwList, String locString) {
	  boolean oldval = ChannelName.useLoc;
	  ChannelName.useLoc=true;
	  
      for (int i = 0; i < ctwList.size(); i++) {
          Channel ch = ((ChannelTimeWindow) ctwList.get(i)).getChannelObj();
          if (ch.getLocation() == "NULL") {

        	  System.out.print("Forcing value for null Location Code: "+
        			  ch.toDelimitedNameString());
        	  ch.setLocation(locString);
        	  System.out.println(" -> "+
        			  ch.toDelimitedNameString());
          }
        }
      ChannelName.useLoc=oldval;
  }
  /** */
//  public static int testRequests(long evid) throws Exception {
//    int id = 0;
//    try {
//      if (getConnectionWithExceptions() == null) throw new Exception("Cannot make database connection.");
//      //return 0;
//      Solution sol = Solution.create().getById(evid);
//
//      if (sol == null) throw new Exception("No such event in dbase. Evid = "+evid);
//      id = (int) sol.getId().longValue();
//    } catch (Exception ex) {
//      throw ex;   // rethrow
//    }
//    return id;
//  }

  /**
   * Return the resulting ChannelableList.
   */
  public ChannelableList getChannelTimeWindowList () {
    return ctwList;
  }
  /**
   * Return the resulting ChannelableList.
   */
  public Collection getRequestCardList () {
    return rcList;
  }
  /**
   * Print out the resulting ChannelableList.
   */
  public void dumpChannelTimeWindowList () {
    if (ctwList != null) {
      for (int i = 0; i < ctwList.size(); i++) {
        System.out.println( ((ChannelTimeWindow) ctwList.get(i)).toString() );
      }
    }
  }
  /**
   * Print out the resulting ChannelableList.
   */
  public static void dumpRequestCardList () {
    if (rcList != null) {
      for (int i = 0; i < rcList.size(); i++) {
        System.out.println( ((RequestCardTN) rcList.get(i)).toString() );
      }
    }
  }
/**
 * Return a list of channels that are elegable for waveform retrieval. They are
 * current channels that are available in wavepool (e.g. Channel_Data.flag like "%T%")
 */
//  private static ChannelableList getCandidateChannelList (double dTime) {
// //    return chTN.getCandidateChannelList(dTime);
//    return model.getCandidateList(dTime);
//  }
  /**
 * Return a list of channels that are elegable for waveform retrieval. They are
 * current channels that are available in wavepool (e.g. Channel_Data.flag like "%T%")
 */
//  private static ChannelableList getCandidateChannelList () {
//    return chTN.getCandidateChannelList();
//  }
  private static ChannelableList getCandidateChannelList () {
    return model.getCandidateList();
  }

  /** Set the candidate list to this one. */
  public static void setCandidateList (ChannelableList candidateListIn) {
    model.setCandidateList(candidateListIn);
  }
  /** Set the candidate list to the default. */
//  public static void setCandidateList () {
//    model.setCandidateList();
//  }
  /** Get an internal default connection if running in the dbase, a working TestDataSource
   * if running outside.
   */
  private static Connection getConnectionWithExceptions() throws Exception {
    // use default if none defined
//    if (conn == null) conn = DbaseConnection.create();
    if (conn == null) conn = DbaseConnection.getConnectionWithExceptions();
    return conn;
  }
  private static Connection getConnection() {
    // use default if none defined
//    if (conn == null) conn = DbaseConnection.create();
    if (conn == null) conn = DbaseConnection.getConnection();
    return conn;
  }

  /** Set verbose output flag. */
  public static void setVerbose(boolean tf) {
    verbose = tf;
  }

  /** Get verbose output flag. */
  public static boolean getVerbose() {
    return verbose;
  }

  /** Output controlled by 'verbose' flag. */
  public static void dumpError (String str) {
    if (getVerbose()) System.err.println ("ReqGen: "+str);
  }

  private static void setProperties (String propfile) {

    props = new ReqGenProperties(propfile);

  }

  private GenericPropertyList getProperties() {
    return props;
  }

/**
 * Currently this runs one time on the evid specified on the command line.
 *
 * It would be more efficient in the future to have it run continuously and
 * get new evid's from the PCS system. This way the candidate channel list
 * would only be constructed once per run not once per event.
 *
 * A thread could be added that would keep the candidate list up-to-date
 * during idle time.
*/
  public static void main(String[] args) {

    long evid =  0;

    String propfile = "properties";

    if (args.length <= 0) {        // no args
      System.out.println ("Usage: NCRequestGenerator <evid> [<property-file>]");
      System.out.println ("Note: Full path/filename of property-file must be supplied.");
      System.out.println ("      unless an alternate <home> is defined with -DJIGGLE_HOME=");
      System.exit(0);
    }

    if (args.length > 0) {
      Integer val = Integer.valueOf(args[0]);
      evid = (int) val.intValue();
    }

    BenchMark bm = new BenchMark();
    
    if (args.length >1) {
      propfile = args[1];
    }

    setProperties(propfile);
    
    if (model == null) {
      dumpError("Model definition is invalid.");
      props.dumpProperties();
      System.exit(0);
    }
    System.out.println("Request generator running for event: " + evid);
    
    if (getVerbose()) props.dumpProperties();
    
    // DEBUG Turn on location code for printing
    //ChannelName.useFullName = true;

    // Turn on location code for SNCL discrimination
    ChannelName.useLoc = true;
    
    DbaseConnectionDescription dbaseDescr = props.getDbaseDescription();
    if (!dbaseDescr.isValid()) {
      System.err.println("Property file does not contain a valid data source description.");
      System.err.println(dbaseDescr.toString());
      props.dumpProperties();
      System.exit(0);
    }
    new DataSource(dbaseDescr);  // make connection

   // Prevent loading of Mag info until new tables get decided on - DDG 11/5/04
//    System.err.println("NOTICE: loading of magnitude correction data is OFF.");
    Channel.setLoadOnlyLatLonZ();
    Channel.setLoadResponse(false);  // Don't need gain in channel selection code: PNL,10/17/05
    // Channel chnl = Channel.create();

    // Use the candidate list as the master list
    // This save time

 //   NCRequestGenerator.setCandidateList();    // creates the default candidate list

//    ChannelableList candidateList = chnl.getCandidateChannelList();
    MasterChannelList.set(NCRequestGenerator.model.getCandidateList());

    bm.print(" Read in " + MasterChannelList.get().size() + " candidate channels in ");  //benchmark message

    int knt = 0;

    try {

      knt = NCRequestGenerator.generateRequests(evid);    // where the real work happens

    } catch (Exception ex) {
      //System.err.println(ex.toString());
      ex.printStackTrace(System.err);
    }
    bm.print(" Generated "+knt+" requests in ");  //benchmark message

  }
  // ///////////////////////////////////////////////////////////////////////
  /**
   * Runtime properties to control the behavior of this code.
   *
 * Properties values:<p>
 * model - full class name of ChannelTimeWindowModel to use. Default = org.trinet.jasi.JBChannelTimeWindowModel<br>
 * auth ? character string to be written to the ?Auth? field of the request row (Max length = 15) This is usually the NET code.<br>
 * staAuth ? character string to be written to the ?StaAuth? field of the request row (Max length = 15). Something like ?SCEDC?.<br>
 * subsource? character string to be written to the ?Subnet? field of the request row (Max length = 8)  The creator of the requests; defaults to ?ReqGen?.<br>
 * priority ? integer value indicating priority of retrieval. Usually set = 1.<br>
 * logfile ? path/name of the logfile<br>
 * logfileDuration ? length of time (in seconds) an instance of a log file is open.<br>
 * verbose - use verbose info and error logging (true or false)
   * */
  static class ReqGenProperties extends GenericPropertyList
  {
    DbaseConnectionDescription dbaseDescr;

    /**
     *  Constructor: sets defaults and reads the property file. Filename must
     * be complete, no assumptions are made as to what directories to search. If the
     * properties file is invalid the application will continue with defaults.
     */
    public ReqGenProperties(String fileName)
    {
      super(fileName, null); // this calls setRequiredProperties()
      setSpecifiedProperties();
    }
    /**
     * Set required default properties so things will still work even if they are not set
     * in the properties files or the files are not present.
     * This is called by super().
     */
    public void setRequiredProperties() {

      final String DEF_NAME = "ReqGen";
     // this string is used by GenericPropertyList to build environmental variables
     // called ReqGen_USER_HOMEDIR and ReqGen_HOME. If the full path to the properties
     // file is not specified these env's are used as the path.
      this.setFiletype(DEF_NAME);

      setProperty ("auth", "??");
      setProperty ("staAuth", "UNK");
      setProperty ("priority", 1);
      setProperty ("type", "T");
      setProperty ("subsource", DEF_NAME);
      setProperty ("writeReqs", "true");
    }
    /**
     * Set specified properties.
     */
    public void setSpecifiedProperties() {

      // do this 1st to redirect subsequent output to the log file
      // logfile stuff (if defined stdin & stderr are redirected to this file)
      if (this.isSpecified("logfile") )
          logger.setFilename(this.getProperty("logfile"));
      if (this.isSpecified("logfileDuration") )
          logger.setRotationInterval(this.getProperty("logfileDuration"));

      // database connection info
      dbaseDescr = parseDbaseDescription();

      if ( this.isSpecified("model") )   // model is specified
        model = ChannelTimeWindowModel.getInstance(this.getProperty("model"));

      if (model == null) {
        dumpError("Model name is invalid: "+getProperty("model"));
      } else {
      // whether default or specified - read model specific properties
        model.setProperties(this);
      }

      // Replace the default values in the ExampleCard with secified values

      if (this.isSpecified("auth") )   // Should be a net code
          ExampleCard.auth.setValue(this.getProperty("auth"));

      // A string starting with "mag" or an integer like "1" (DDG 4/13/06)
      if (this.isSpecified("priority") )  {  
    	  String str = this.getProperty("priority");
      	  str = str.toUpperCase();  // upcase for comparison
      	  if (str.startsWith("MAG")) {
      		  setPriorityToMag = true;
      	  } else {
      		  ExampleCard.priority.setValue(this.getInt("priority"));
      	  }
      }
 
      if (this.isSpecified("staAuth") )
          ExampleCard.staAuth.setValue(this.getProperty("staAuth"));

      if (this.isSpecified("type") )
          ExampleCard.type.setValue(this.getProperty("type"));

      if (this.isSpecified("subsource") )
          ExampleCard.subsource.setValue(this.getProperty("subsource"));

      if (this.isSpecified("verbose") )
          verbose = this.getBoolean("verbose");

      if (this.isSpecified("writeReqs") )
          writeReqs = this.getBoolean("writeReqs");
    }

    /** Parse and return the DbaseConnectionDescription object described by in property file.
     @See: DbaseConnectionDescription.parseFromProperties() */
    public DbaseConnectionDescription parseDbaseDescription () {
      dbaseDescr = new DbaseConnectionDescription();
      if (!dbaseDescr.parseFromProperties(this)) {
        dumpError("Properties file contains invalid dbase description.");
        dumpError(dbaseDescr.toString());
      }
      return dbaseDescr;
    }
    /** Return the DbaseConnectionDescription object described by in property file.
     If parseDbaseDescription has not been called this method will call it.
     The DbaseConnectionDescription is NOT guaranteed to be valid. */
    public DbaseConnectionDescription getDbaseDescription () {
      if (dbaseDescr == null) parseDbaseDescription ();
      return dbaseDescr;
    }
  }
}
