package org.trinet.storedprocs.waveformrequest;

import java.io.*;
import java.sql.*;
import java.util.*;

import org.trinet.storedprocs.DbaseConnection;
import org.trinet.jdbc.datasources.DbaseConnectionDescription;
import org.trinet.jasi.*;
import org.trinet.util.*;

/**
 * <p>Title: </p>
 * <p>Description: Create Waveform Requests for an event.
 * Waveform requests are channel/time-windows that should include all seismic signal
 * of interest for and event. This class determines the channel/time-windows using the
 * JBChannelTimeWindowModel. It writes Waveform request rows to a table using the
 * RequestCardTN class. </p>
 *
 * This class is intended to run as a stored procedure from within the dbase but will
 * also function from outside.
 *
 * Properties values:<p>
 * model - full class name of ChannelTimeWindowModel to use. Default = org.trinet.jasi.JBChannelTimeWindowModel<br>
 * authi character string to be written to the Auth field of the request row (Max length = 15) This is usually the NET code.<br>
 * staAuth character string to be written to the StaAuth field of the request row (Max length = 15). Something like SCEDC.<br>
 * subsource character string to be written to the Subnet field of the request row (Max length = 8)  The creator of the requests; defaults to ReqGen.<br>
 * priority integer value indicating priority of retrieval. Usually set = 1.<br>
 * logfile path/name of the logfile<br>
 * logfileDuration length of time (in seconds) an instance of a log file is open.<br>
 * verbose - use verbose info and error logging (true or false)
 * <p>
 *
 * There should also be properties specific to the model. For example:<p>
 * org.trinet.jasi.JBChannelTimeWindowModel.includeAllComponents=true<br>
 * org.trinet.jasi.JBChannelTimeWindowModel.includeAllMag=3.5<br>
 * org.trinet.jasi.JBChannelTimeWindowModel.maxDistance=10000<br>
 * org.trinet.jasi.JBChannelTimeWindowModel.maxWindowSize=300<br>
 * org.trinet.jasi.JBChannelTimeWindowModel.minDistance=30<br>
 * org.trinet.jasi.JBChannelTimeWindowModel.minWindowSize=60<br>
 * org.trinet.jasi.JBChannelTimeWindowModel.noiseThreshold=25<br>
 */

/*  TODO
o test and verify that the set of channel/time-windows is what we want
o test speed of writing request rows to the dbase
o set config values from a small, simple table or properties file
*/

public class RequestGenerator {

    private static BenchMark bm = new BenchMark();

    //This generates requests for the input evid specified on the command line.
    public static final String DEF_NAME = "RCG";

    /** The model used to define Channels and time windows to make requests for.
     * This should be specified at run-time via the "model" property.*/    
    private ChannelTimeWindowModel model =
        new org.trinet.jasi.PowerLawTimeWindowModel();
    /** Database connection */
    private Connection conn = null;

    /** Default list of component types to include. Any other type is excluded.
     * Blank list means include all channels that are in the candidate list. */

    /** If false no request cards will be written to the dbase. Good for debugging.
    * Set with the 'writeReqs' property. */
    private boolean writeReqs = true;

    private boolean wfArchiveStats = false;
    private boolean dumpNoWF4CTW = false;
    private boolean dumpNoCTW4WF = false;
    private boolean dumpAltSolList = false;
    private boolean dumpDurations = false;
    private double wfArchiveMinPercent = 90.;
    private double wfArchiveMinPercentTrig = 70.;

    /** If true verbose output is written. Good for debugging.
    * Set with the 'verbose' property. */
    private static boolean verbose = true;
    private static boolean debug = false;

    /** The properties object. */
    private JasiDatabasePropertyList props = null;

    /** channel-timewindow list */
    private ChannelableList ctwList = null;
    /** RequestCardTN list */
    private ActiveArrayList rcList = null;

    /** These default values will be used to fill in request card fields.*/
    private RequestCardTN ExampleCard = new RequestCardTN();
    
    /** 
     * Flag set to true if the 'priority' property is set to "Mag".
     * Indicates that ReqCard priority should be set to the inter value
     * of the event magnitude.
     */
    static boolean setPriorityToMag = false;
 
    public RequestGenerator() {}
    
    public RequestGenerator(JasiDatabasePropertyList propObj) {
          props = propObj;
    }
    
    /** Just return how many request cards WOULD be generated but don't actually
     * create them. Used to test technique.*/
    public int generateRequestCount(long evid) throws Exception {
        ChannelableList ctwList = createCTWlist(evid);
        if (ctwList == null) return 0;
        return ctwList.size();
    }

    /** Create a Channel-TimeWindow list for this solution. Include channels for
     * any parameters (mag amps, phases, or codas) that are associated with the solution.*/
    private ChannelableList createCTWlist(long evid) throws Exception {
        if (getConnectionWithExceptions() == null) throw new Exception("Cannot make database connection.");
        Solution sol = Solution.create().getById(evid);
        if (sol == null) throw new Exception("No such event in dbase. Evid = "+evid);
        return createCTWlist(sol);
    }

    /** Create a Channel-TimeWindow list for this solution. Include channels for
     * any parameters (mag amps, phases, or codas) that are associated with the solution.*/
    private ChannelableList createCTWlist(Solution sol) throws Exception {

        if (sol == null) throw new Exception("Null solution passed to createCTWlist()");
        if (getConnectionWithExceptions() == null) throw new Exception("Cannot make database connection.");
        if (model.getRequiresLocation() && !sol.hasLatLonZ())  {
            //throw new Exception("Solution has no location.");
            dumpError("unable to generate channel time windows, " + sol.getId().longValue() + " has zero LAT,LON, check etype!");
            return new ChannelableList(0);
        }

        // do this first so that if model is synched with solution time, new channellist is created
        model.setSolution(sol);

        // create candidate channel list
        ChannelableList candidateList = getCandidateList();
        if (candidateList == null) throw new Exception("Candidate channel list is null.");
        if (debug) {
            bm.printTimeStamp(" Model candidateList size = " + candidateList.size() + " return", System.out);  //benchmark message
        }

        // Added channels with parameter (picks, amps, codas) if requested by config
        // DON'T use a ChannelableSetList for this because Triax components will be handled
        // later in the processing - doing it here will make later code less efficient.
        //  ChannelableSetList requiredChans = new ChannelableSetList();
        // no longer needed
        //    ChannelableList requiredChans = new ChannelableSetList();
        // Set some model values
        //    model = new JBChannelTimeWindowModel();
        //    model.setCandidateList(candidateList);
        //    model.setCandidateList();    // sets to default if not set yet
        //model.setSolution(sol); // moved to above candidateList retrieval

        // Generate a ChannelTimeWindow List using the model
        ctwList = (ChannelableList) model.getChannelTimeWindowList();
        return ctwList;

    }

    /**
     * Generate the waveform requests for this event number. The solution will be read
     * from the dbase. Phases, Codas, & Mag amps will be read in.
     * @param evid
     * @return int = number of request rows written
     */
    public int generateRequests(long evid) throws Exception {
        if (getConnectionWithExceptions() == null) throw new Exception("Cannot make database connection.");
        //look up the Solution
        Solution sol = Solution.create().getById(evid);
        if (sol == null) throw new Exception("No such event in dbase. Evid = "+evid);
        if (verbose) System.out.println(sol.toSummaryString());
        return generateRequests(sol);
    }

    /**
     * Generate the waveform requests for this Solution and Model.
     * Assumes Phases, Codas, & Mag amps have already been read in if you want them included.
     * Requests will be written to the dbase unless 'writeReqs' is set false.
     * @param sol
     * @return int  number of request rows written
     */

    public int generateRequests(Solution sol) throws Exception {

      if (sol == null) throw new Exception("No such event in dbase.");
      if (model.getRequiresLocation() && !sol.hasLatLonZ()) throw new Exception("Solution has no location.");
      if (getConnectionWithExceptions() == null) throw new Exception("Cannot make database connection.");

      
      // Create CTW's using the default model
      ctwList = createCTWlist(sol);
      if (debug) {
          bm.printTimeStamp(" createCTWlist return", System.out);  //benchmark message
      }
      if (ctwList == null) throw new Exception("Null Channel-TimeWindow list.");

      int ctwCnt = ( ctwList == null ) ? 0 : ctwList.size();
      if (verbose) {
          System.out.println("Total channels requested = "+ctwCnt);
      }

      // //////////////////////////////////////////////////////////
      // EVILNESS: force any 'null' loccodes to '  '.
      // These happens when a CTW is made from the required list, 
      // i.e. from the list of phases - because the RT system writes null loccodes!
      forceNonnullLocs(ctwList, "  ");
      if (props.getBoolean("debug")) {
          bm.printTimeStamp(" forceNonnullLocs return", System.out);  //benchmark message
      }
      // //////////////////////////////////////////////////////////    

      // //////////////////////////////////////////////////////////    
      // generate RequestCardTN "cards"
      RequestCardTN rcg = new RequestCardTN(DataSource.getConnection());
      
      long evid = sol.getId().longValue();
      ExampleCard.evid.setValue(evid);      // will "associate" all rows w/ this evid

      // In RequestCardTN priority is an DataInteger, higher priority numbers are processed first.
      if (setPriorityToMag) {
          // Set default priority to value of magnitude * scalar rounded to integer by setValue call
          // scalar multiplier defaults to one 1., when not defined in properties
          double priorityMagScalar = props.getDouble("priorityMagScalar", 1.);
          double mag = 2.0;
          if ( sol.magnitude == null || sol.magnitude.hasNullType() || sol.magnitude.hasNullValue() ) {
              mag = props.getDouble("defaultNullMag", 2.0);
              System.out.println("WARNING event:" + evid + " magnitude NULL, using default of: " + mag);
          }
          else {   
              mag = sol.magnitude.getMagValue();
          }
          ExampleCard.priority.setValue(mag * priorityMagScalar);
      }
      
      rcList = RequestCardTN.makeList(ctwList, ExampleCard);
      if (debug) {
          bm.printTimeStamp(" RequestcardTN.makeList return", System.out);  //benchmark message
      }

      ctwCnt = rcList.size();
      if (verbose) System.out.println("Requested chans for "+evid+" = "+ctwCnt);
      if (debug || props.getBoolean("dumpChannelTimeWindows")) {
          dumpChannelTimeWindowList(evid);
      }

      // To scale the priority by distance (decreasing with distance) you must set priorityDistInterval > 0
      int priorityDistInterval = props.getInt("priorityDistInterval", 0);
      if (priorityDistInterval > 0) {
        // base priority for distance defaults to 1000, you can change value
        int priorityDistBaseValue = props.getInt("priorityDistBaseValue", 1000);
        int distPriority = 0;
        double dist = 0.;
        for (int idx=0; idx < ctwCnt; idx++) {
          rcg = (RequestCardTN) rcList.get(idx);
          dist = rcg.ctw.getChannelObj().getHorizontalDistance();
          if (dist == Channel.NULL_DIST) distPriority = 0;
          else {
              // priority decreases from the base value with increasing distance
              // prioritize all distances > 1000 the same
              if (dist > 1000. ) dist = 1000.;
              distPriority = Math.max(0, priorityDistBaseValue - (int) Math.round(dist/priorityDistInterval)); // forced to be >= 0 
          }
          if (distPriority > 0 ) rcg.priority.setValue(rcg.priority.intValue() + distPriority); // add distance priority, closer stations higher
        }
      }

      if (wfArchiveStats) doArchiveStats(sol);

      int rcgCnt = 0;
      if (writeReqs) { // this test is a stopper for debugging w/o writing to the dbase

        // insert the requests
        try {
          if (verbose) System.out.println("Writing requests to dbase... "+rcList.size());
          rcgCnt = rcg.dbaseInsertList(rcList);
          if (props.getBoolean("debug")) {
              bm.printTimeStamp(" RequestcardTN.dbaseInsertList return", System.out);  //benchmark message
          }
        }
        catch (Exception ex) {
          ex.printStackTrace(System.err) ;  // print but ignore errors
        }

        // commit
        try {
          if (verbose) System.out.println("Committing requests to dbase... "+ rcgCnt);
          DataSource.getConnection().commit();
        }
        catch (SQLException ex) {
          System.err.println("Commit failed");
          throw ex;   // rethrow (or throw new?)
        }
      } else {
          System.out.println("No dbase rows written because property 'writeReqs' = false.");
      }

      if ( getDebug() || props.getBoolean("dumpRequestCards")) {
          dumpRequestCardList(evid);
      }

      return rcgCnt;
    }

    private void doArchiveStats(Solution sol) {

          long evid = sol.getId().longValue();

          int wfCnt = sol.loadWaveformList();

          ChannelableList ctwList = getChannelTimeWindowList();

          if ( ctwList == null || ctwList.isEmpty() ) {
              System.out.printf("ARCHIVE_STATS evid: %10d, 0 requests created and %d waveforms in db.\n", evid, wfCnt); 
          }
          else {

            Channelable[] ctwArray = ctwList.getChannelableArray();
            int ctwCnt = ctwArray.length;

            if ( wfCnt <= 0 ) {
              System.out.printf("ARCHIVE_STATS evid: %10d, 0 waveforms in db and %d waveforms expected, CHECK-WFS.\n", evid, ctwCnt ); 
            }
            else {
              double archFrac = (ctwCnt > 0 ) ? (double)wfCnt/(double)ctwCnt : 0.;
              ChannelableList wfList = sol.getWaveformList();
              ChannelTimeWindow ctw = null;
              Waveform wf = null;
              int dcnt=0;
              double[] data = new double[ctwCnt];
              for (int ii = 0; ii < ctwCnt; ii++) {
                  ctw = (ChannelTimeWindow) ctwArray[ii];
                  wf = (Waveform) wfList.getByChannel(ctw);
                  if ( wf != null) {
                      double ctwDur = ctw.getDuration();
                      double wfDur = wf.getDuration();
                      if ( dumpDurations ) {
                         System.out.println(ctw + " " + ctwDur);
                         System.out.println(wf.getChannelObj().toDelimitedSeedNameString() + " " + wf.getTimeSpan() + " " + wfDur);
                      }
                      if ( wfDur > ctwDur ) wfDur = ctwDur;
                      data[dcnt++] = wfDur/ctwDur;
                  }
                  else if ( dumpNoWF4CTW ) {
                      System.out.println("ARCHIVE_STATS no matching wf for ctw: " + ctw.getChannelObj().toDelimitedSeedNameString());
                  }
              }
              if ( dumpNoCTW4WF ) {
                for (int ii = 0; ii < wfList.size(); ii++) {
                    wf = (Waveform) wfList.get(ii);
                    ctw = (ChannelTimeWindow) ctwList.getByChannel(wf);
                    if ( ctw == null) {
                        System.out.println("ARCHIVE_STATS no matching ctw for wf: " + wf.getChannelObj().toDelimitedSeedNameString());
                    }
                }
              }
              double median = Stats.median(data, dcnt)*100.; 
              double mean = Stats.mean(data, dcnt)*100.;
              archFrac *= 100.;
              String chkStr = ( median < wfArchiveMinPercent || mean < wfArchiveMinPercent ) ?  chkStr = "CHECK-WFS" : "";
              if ( !chkStr.equals("") ) { 
                  if ( archFrac < wfArchiveMinPercent ) {
                    System.out.printf("ARCHIVE_STATS evid: %10d #rcg>#wfs: a prior 'st' with no redoWfs?, or is event.prefmag > original rcg mag?\n", evid);
                  }
                  if (sol.loadAltSolList() > 0 ) {
                    GenericSolutionList gsl = (GenericSolutionList) sol.getAlternateSolutions();
                    System.out.printf("ARCHIVE_STATS evid: %10d had %2d previous origins\n", evid, gsl.size());
                    for ( int ii = 0; ii < gsl.size(); ii++ ) {
                       Solution aSol = gsl.getSolution(ii); 
                       if ( aSol.getSource().startsWith("RT") && aSol.isDummy() && aSol.getPreferredMagnitude().hasNullType() ) {
                           System.out.printf("ARCHIVE_STATS evid: %10d originally had bogus origin, no magnitude, likely a 'st'\n", evid);
                           chkStr = ( median >= wfArchiveMinPercentTrig ) ? "ST" : "CHECK-WFS ST";
                           break;
                       } 
                    }
                    if ( dumpAltSolList ) {
                      System.out.println(sol.getNeatStringHeader());
                      System.out.println(gsl.dumpToString());
                    }
                  }
              }
              System.out.printf(
                 "ARCHIVE_STATS evid: %10d #wfs/#rcg=%04d/%04d=%6.2f%%; for %4d durations wf/rcg mean%%,median%%: %5.2f,%5.2f std-dev:%5.2f %8s\n",
                 evid, wfCnt, ctwCnt, archFrac, dcnt, mean, median, Stats.standardDeviation(data, dcnt)*100., chkStr
              );

            }
          }
    }

    // EVILNESS: force any 'null' loccodes to the value of 'locString'
    // These happen when a CTW is made from the required list, 
    // i.e. from the list of phases - because the RT system writes null loccodes!
    private void forceNonnullLocs(ChannelableList ctwList, String locString) {
        boolean oldval = ChannelName.useLoc;
        ChannelName.useLoc=true;
            
        for (int i = 0; i < ctwList.size(); i++) {
            Channel ch = ((ChannelTimeWindow) ctwList.get(i)).getChannelObj();
            if (ch.getLocation() == "NULL") {
                System.out.print("Forcing value for null Location Code: "+ ch.toDelimitedNameString());
                ch.setLocation(locString);
                System.out.println(" -> "+ ch.toDelimitedNameString());
            }
        }
        ChannelName.useLoc=oldval;
    }

    /**
     * Return the resulting ChannelableList.
     */
    public ChannelableList getChannelTimeWindowList () {
        return ctwList;
    }
    /**
     * Return the resulting ChannelableList.
     */
    public Collection getRequestCardList() {
        return rcList;
    }

    /**
     * Print out the resulting ChannelableList.
     */
    public void dumpChannelTimeWindowList(long evid) {
        if (ctwList != null) {
            String dumpFile = props.getProperty("dumpChannelTimeWindowsPath");
            if ( dumpFile != null ) {
              try {
                dumpFile += GenericPropertyList.FILE_SEP + evid + "_" + "ctw.lis";
                BufferedWriter bw = new BufferedWriter(new FileWriter(new File(dumpFile)));
                for (int i = 0; i < ctwList.size(); i++) {
                    bw.write( ((ChannelTimeWindow) ctwList.get(i)).toString() );
                    bw.newLine();
                }
                bw.close();
              }
              catch (IOException ex) {
                  System.err.println("IO error for filename: " + dumpFile);
                  ex.printStackTrace();
              }
            }
            else {
              for (int i = 0; i < ctwList.size(); i++) {
                  System.out.println( ((ChannelTimeWindow) ctwList.get(i)).toString() );
              }
            }
        }
    }

    /**
     * Print out the resulting ChannelableList.
     */
    public void dumpRequestCardList(long evid) {
        if (rcList != null) {
            String dumpFile = props.getProperty("dumpRequestCardsPath");
            if ( dumpFile != null ) {
              try {
                dumpFile += GenericPropertyList.FILE_SEP + evid + "_" + "rc.lis";
                BufferedWriter bw = new BufferedWriter(new FileWriter(new File(dumpFile)));
                for (int i = 0; i < rcList.size(); i++) {
                    bw.write( ((RequestCardTN ) rcList.get(i)).toString() );
                    bw.newLine();
                }
                bw.close();
              }
              catch (IOException ex) {
                  System.err.println("IO error for filename: " + dumpFile);
                  ex.printStackTrace();
              }
            }
            else {
              System.out.println(RequestCardTN.getStringHeader());
              for (int i = 0; i < rcList.size(); i++) {
                  System.out.println( ((RequestCardTN) rcList.get(i)).toString() );
              }
            }
        }
    }

    /**
    * Return a list of channels that are elegable for waveform retrieval. They are
    * current channels that are available in wavepool (e.g. Channel_Data.flag like "%T%")
    */
    public ChannelableList getCandidateList() {
        return model.getCandidateList();
    }

    /** Set the candidate list to this one. */
    public void setCandidateList(ChannelableList candidateListIn) {
        model.setCandidateList(candidateListIn);
    }

    /** Get an internal default connection if running in the dbase, a working TestDataSource
     * if running outside.
     */
    private Connection getConnectionWithExceptions() throws Exception {
        // use default if none defined
        //if (conn == null) conn = DbaseConnection.create();
        if (conn == null) conn = DbaseConnection.getConnectionWithExceptions();
        return conn;
    }
    private Connection getConnection() {
        // use default if none defined
        //if (conn == null) conn = DbaseConnection.create();
        if (conn == null) conn = DbaseConnection.getConnection();
        return conn;
    }

    /** Set debug output flag. */
    public static void setDebug(boolean tf) {
        debug = tf;
        if (tf) verbose=true;
    }

    /** Get debug output flag. */
    public static boolean getDebug() {
        return debug;
    }

    /** Set verbose output flag. */
    public static void setVerbose(boolean tf) {
        verbose = tf;
    }

    /** Get verbose output flag. */
    public static boolean getVerbose() {
        return verbose;
    }

    /** Output error message. */
    private void dumpError(String str) {
        System.err.println("Error ReqGen: "+str);
    }

    /**
     * 
     * @param model, class name string like "org.trinet.jasi.PowerLawTimeWindowModel"
     */
    public void setModel(String modelname) {
        this.model = ChannelTimeWindowModel.getInstance(modelname);
    }
    /**
     * 
     * @param model ChannelTimeWindowModel
     */
    public void setModel(ChannelTimeWindowModel model) {
        this.model = model;
    }
    
    /**
     * Return the ChannelTimeWindowModel.
     * Note that if its a specialized model you cannot access extended
     * methods unless you cast it to the correct type. 
     */
    public ChannelTimeWindowModel getModel() {
        return model;
    }
    
    public JasiDatabasePropertyList getProperties() {
        return props;
    }

    /**
     * Sets instance properties, read specified input user property file
     * and configures environment. 
     * @param propFileName
     * @return
     */
    public boolean setProperties(String propFileName) {
        // reset() reads file, setup()
        JasiDatabasePropertyList newProps = new JasiDatabasePropertyList(); // the one with the most options 
        newProps.setFiletype(DEF_NAME);
        newProps.setFilename(propFileName);
        boolean status = newProps.reset(); // read user property files
        setProperties(newProps);
        return status;
    }
    
    public void setProperties(JasiDatabasePropertyList props) {

      this.props = props;

      setRequiredProperties();

      if (props.isSpecified("verbose") ) verbose = props.getBoolean("verbose");
      if (props.isSpecified("debug") ) {
          debug = props.getBoolean("debug");
      }

      // Props like for Gmp2Db must first set timestamp zone and pattern before setLogFileName:
      if (props.isSpecified("logTimestampZone"))
          Logger.TIMESTAMP_ZONE = props.getProperty("logTimestampZone");

      if (props.isSpecified("logTimestampPattern"))
          Logger.TIMESTAMP_PATTERN = props.getProperty("logTimestampPattern");

      if (props.isSpecified("logFileName")) {
          Logger.setFilename(props.getProperty("logFileName"));
      }
      else if (props.isSpecified("logfile") ) {
          Logger.setFilename(props.getProperty("logfile"));
      }

      // Redirect subsequent output to the log file
      if (props.isSpecified("logRotationInterval")) {
          Logger.setRotationInterval(props.getProperty("logRotationInterval"));
      }
      else if (props.isSpecified("logfileDuration") ) {
          Logger.setRotationInterval(props.getProperty("logfileDuration"));
      }

      if ( props.isSpecified("model") )   // model is specified
          //model = ChannelTimeWindowModel.getInstance(props.getProperty("model"));
          setModel(props.getProperty("model"));

      if (model == null) {
          dumpError("Model name is invalid: "+props.getProperty("model"));
      } else { // whether default or specified - read model specific properties
          model.setProperties(props);
      }
      
      // Replace the default values in the ExampleCard with secified values

      if (props.isSpecified("auth"))   // Should be a net code
          ExampleCard.auth.setValue(props.getProperty("auth"));

      // A string starting with "mag" or an integer like "1" (DDG 4/13/06)
      if (props.isSpecified("priority"))  {  
              String str = props.getProperty("priority");
                str = str.toUpperCase();  // upcase for comparison
                if (str.startsWith("MAG")) {
                        setPriorityToMag = true;
                } else {
                        ExampleCard.priority.setValue(props.getInt("priority"));
                }
      }
      if (props.isSpecified("staAuth"))
          ExampleCard.staAuth.setValue(props.getProperty("staAuth"));

      if (props.isSpecified("type"))
          ExampleCard.type.setValue(props.getProperty("type"));

      if (props.isSpecified("subsource"))
          ExampleCard.subsource.setValue(props.getProperty("subsource"));

      if (props.isSpecified("writeReqs"))
          writeReqs = props.getBoolean("writeReqs");

      if (props.isSpecified("wfArchiveStats"))
          wfArchiveStats = props.getBoolean("wfArchiveStats");

      if (props.isSpecified("wfArchiveStats.dumpNoWF4CTW"))
          dumpNoWF4CTW = props.getBoolean("wfArchiveStats.dumpNoWF4CTW");

      if (props.isSpecified("wfArchiveStats.dumpNoCTW4WF"))
          dumpNoCTW4WF = props.getBoolean("wfArchiveStats.dumpNoCTW4WF");

      if (props.isSpecified("wfArchiveStats.dumpAltSolList"))
          dumpAltSolList = props.getBoolean("wfArchiveStats.dumpAltSolList");

      if (props.isSpecified("wfArchiveStats.minPercent"))
          wfArchiveMinPercent = props.getDouble("wfArchiveStats.minPercent");

      if (props.isSpecified("wfArchiveStats.minPercentTrigger"))
          wfArchiveMinPercentTrig = props.getDouble("wfArchiveStats.minPercentTrigger");

      if (props.isSpecified("wfArchiveStats.dumpDurations"))
          dumpDurations = props.getBoolean("wfArchiveStats.dumpDurations");

      if (props.getBoolean("addChan2src")) RequestCardTN.addChan2src = true;

    }

    /**
     * Set the flag to allow/prevent actual writing of requests to the dbase.
     * If set in code this may over-ride what is set in the properties file.
     * @param tf
     */
    public void setWriteReqs(boolean tf) {
        writeReqs = tf;
    }

    /**
     * Set required default properties so things will still work even if they are not set
     * in the properties files or the files are not present.
     * This is called by super().
    */
    public void setRequiredProperties() {
        setRequiredProperties(props); 
    }

    public void setRequiredProperties(GenericPropertyList props) {
       // filetype this string is used by GenericPropertyList to build environmental variables
       // called ReqGen_USER_HOMEDIR and ReqGen_HOME. If the full path to the properties
       // file is not specified these env's are used as the path.
        props.setFiletype(DEF_NAME);

        if (!props.isSpecified("auth")) props.setProperty("auth", EnvironmentInfo.getNetworkCode());
        if (!props.isSpecified("staAuth")) props.setProperty("staAuth", "UNK");
        if (!props.isSpecified("priority")) props.setProperty("priority", 1);
        if (!props.isSpecified("type")) props.setProperty("type", "T");
        if (!props.isSpecified("subsource")) props.setProperty("subsource", DEF_NAME);
        if (!props.isSpecified("writeReqs")) props.setProperty("writeReqs", "true");
    }

    public static void main(String[] args) {

        if (args.length <= 0) {        // no args
            System.out.println("Usage: RequestGenerator <evid> [<property-file>]");
            System.out.println("Note: Full path/filename of property-file must be supplied.");
            System.out.println("      unless an alternate <home> is defined with -DJIGGLE_HOME=");
            System.exit(1);
        }

        //NOTE : CI evid =  14040032l ml=1.7  #wf's=544/557 or  = 10062205l ml=3.0

        long evid =  0l;
        try {
            evid = Long.parseLong(args[0]);
        }
        catch (NumberFormatException ex) {
            System.err.println("Input evid cannot be parsed: " + args[0]);
            System.err.println(ex.getMessage());
            System.exit(1);
        }

        String propfile = "rcg.props";
        if (args.length > 1) {
            propfile = args[1];
        }

        RequestGenerator rg = new RequestGenerator();

        File file = new File(propfile);
        if (file.isAbsolute()) {
            //initialize(DEF_NAME, String userFileName, String defaultFileName) 
            JasiDatabasePropertyList newProps = new SolutionEditorPropertyList(propfile, null);
            rg.setProperties(newProps);
        }
        else rg.setProperties(propfile);

        if (rg.model == null) {
            rg.dumpError("Model definition is invalid."); 
            rg.props.dumpProperties();
            System.exit(1);
        }

        if (rg.getVerbose()) rg.props.dumpProperties();
        System.out.println( "Channel dbLookUp = " + Channel.getDbLookup());

        DbaseConnectionDescription dbaseDescr = rg.props.getDbaseDescription();
        if (!dbaseDescr.isValid()) {
            System.err.println("Property file does not contain a valid data source description.");
            System.err.println(dbaseDescr.toString());
            rg.props.dumpProperties();
            System.exit(1);
        }

        DataSource ds = null;
        int status = 0;
        try {
            ds = new DataSource();
            // make connection
            ds.set(dbaseDescr);

            if ( rg.props.isSpecified("channelListReadOnlyLatLonZ") && rg.props.getBoolean("channelListReadOnlyLatLonZ") ) {
                Channel.setLoadOnlyLatLonZ();
                System.err.println("RCG properties: channelListReadOnlyLatLonZ=true");
            }
            else {
                if (rg.getVerbose())
                    System.out.println( "Channel associated load settings: response=true, corrections=false, clipping=false" );
                Channel.setLoadResponse(true);
                Channel.setLoadKnownCorrections(false);
                Channel.setLoadClippingAmps(false);
            }
    
            int rcCnt = 0;
            if (bm == null) bm = new BenchMark();
            bm.printTimeStamp(" Calling generateRequests for " + evid, System.out);  //benchmark message

            rcCnt = rg.generateRequests(evid);    // where the real work happens

            bm.printTimeStamp(" Generated "+rcCnt+" requests for " + evid + " time=", System.out);  //benchmark message
        }
        catch (Exception ex) {
             ex.printStackTrace();
             status = 1;
        }
        finally {
            if (ds != null) ds.close();
        }
         
        System.exit(status);
    }
}
