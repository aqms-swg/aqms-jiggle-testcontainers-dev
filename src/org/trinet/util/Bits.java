package org.trinet.util;

/**
 * Staic classes for bit and byte manipulation.
 */

public class Bits {

    /**
    * Convert arbitrary number of bytes to integer converting the specified
    * subarray of bytes using the platform's default character encoding.
    */
    public static int byteStringToInt(byte[] buff, int start, int len) {
        Integer i = Integer.valueOf(new String(buff, start, len));
        return ( i.intValue() ); // Integer -> int
    }

    /**
    * Convert zero-terminated byte array to string
    * using the platform's default character encoding.
    * Thus, the resulting string may be shorter than the byte array.
    */
    public static String bytesToString(byte[] buff) {
      StringBuffer sb = new StringBuffer(buff.length);
      for (int i=0; i < buff.length; i++) {
        if (buff[i] == 0) break; // hit zero termination
        sb.append((char)buff[i]);
      }
      return sb.toString();
    }

    /**
    * Intepret input 2 byte array as an signed integer. The bytes are taken from the byte array buff
    * beginning at array index 'start' (first postion is 0).
    * Returns 0 if buffer is too short or empty.
    */
    public static int byteToInt2(byte[] buff, int start, boolean bigEndian) {
        if (buff.length < start+1) return 0;   // prevent java.lang.ArrayIndexOutOfBoundsException
        // Note that "masking" (& 0xff) is necessary to preserve the original bit pattern
        // because Java has no unsigned data types (except char).
        return (bigEndian) ?
          // promote sign bit
          (buff[start] << 8) | (buff[start+1] & 0xff) : (buff[start+1] << 8) | (buff[start] & 0xff);
    }

    /** Intepret 2 bytes starting a input index as an unsigned integer. */
    public static int byteToUInt2(byte[] buff, int start, boolean bigEndian) {
        if (buff.length < start+1) return 0;   // prevent java.lang.ArrayIndexOutOfBoundsException
        // Note that "masking" (& 0xff) is necessary to preserve the original bit pattern
        // because Java has no unsigned data types (except char).
        return (bigEndian) ?
          // don't promote sign bit
          ((buff[start] & 0xff) << 8) | (buff[start+1] & 0xff) : ((buff[start+1] & 0xff) << 8) | (buff[start] & 0xff);
    }

    /** Put big-endian int into 4 byte array elements starting at input array index.
     * If bigEndian ==  true, array values are in big-endian byte order.
     * If bigEndian == false, array values are in little-endian byte order.
     * */
    public static void intToByteArray(int value, byte[] buff, int start, boolean bigEndian) {
        // Note >>> zero fill right shift (no sign bit fill)
       // byte cast of an int assigns byte the value of the 4th byte (least value) in int 
        if (bigEndian) {
          buff[start]   = (byte) (value >>> 24);
          buff[start+1] = (byte) (value >>> 16);
          buff[start+2] = (byte) (value >>> 8);
          buff[start+3] = (byte) (value);
        }
        else {
          buff[start]   = (byte) (value);
          buff[start+1] = (byte) (value >>> 8);
          buff[start+2] = (byte) (value >>> 16);
          buff[start+3] = (byte) (value >>> 24);
        }
    }

    /**
    * Intepret input 4 byte buffer as an signed integer. The bytes are taken from the byte array buff
    * beginning at array index 'start' (first postion is 0).
    */
    public static int byteToInt4(byte[] buff, int i, boolean bigEndian) {
        if (buff.length < i+3) return 0;   // prevent java.lang.ArrayIndexOutOfBoundsException
        return (bigEndian) ?
            ( (buff[i] << 24) | ((buff[i+1] & 0xff) << 16) | ((buff[i+2] & 0xff) <<  8) | (buff[i+3] & 0xff) ) :
            ( (buff[i] & 0xff) | ((buff[i+1] & 0xff) << 8) | ((buff[i+2] & 0xff) <<  16) | (buff[i+3] << 24) ) ;
    }

    /** Intepret input 4 byte array as an unsigned 4-byte integers, must convert to long. */
    public static long byteToUInt4(byte[] buff, int i, boolean bigEndian) {
        if (buff.length < i+3) return 0;   // prevent java.lang.ArrayIndexOutOfBoundsException
        return (bigEndian) ?
            0xffffffffL & ( (long) ((buff[i] & 0xff) << 24) | ((buff[i+1] & 0xff) << 16) | ((buff[i+2] & 0xff) <<  8) | (buff[i+3] & 0xff) ) :
            0xffffffffL & ( (long) (buff[i] & 0xff) | ((buff[i+1] & 0xff) << 8) | ((buff[i+2] & 0xff) <<  16) | (buff[i+3] & 0xff) ) ;
    }

    /**
    * Convert byte[] array containing 2-byte ints to signed int[] (4-bytes)
    */
    public static int[] byteArrayToInt2(byte[] b, int nbytes, boolean bigEndian) {
        int[] in = new int[nbytes/2];
        int idx=0;
        for (int i=0; i<nbytes; i=i+2) {
            in[idx] = byteToInt2(b, i, bigEndian);
            idx++;
        }
        return (in);
    }

    /**
    * Convert byte[] array containing 4-byte ints to signed int[] (4-bytes)
    */
    public static int[] byteArrayToInt4(byte[] b, int nbytes, boolean bigEndian) {
        int[] in = new int[nbytes/4];
        int idx=0;
        for (int i=0; i<nbytes; i=i+4) {
           in[idx] = byteToInt4(b, i, bigEndian);
           idx++;
        }
        return (in);
    }

    /**
    * See if a bit is set.
    * There must be a native Java way to do this but I can't find it.
    * Bits are numbered starting at 0.
    */
    public static boolean bitSet(byte b, int pos) {
        int val = (int) Math.pow(2, pos+1);
        return (boolean) ((b & (byte) val)==val);
    }

}
