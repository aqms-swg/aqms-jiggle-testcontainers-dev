// Modified from original version by AWalter, some members and methods were added.
/*********************************************************************
 *Copyright (C) 1999 Nathan Fiedler
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * PROJECT:      Abstract Data Types
 * MODULE:       Complex
 * FILE:         Complex.java
 *
 * AUTHOR:      Nathan Fiedler
 *
 * REVISION HISTORY:
 *      Name     Date           Description
 *      ----     ----           -----------
 *      nf       8/3/98         Initial version
 *
 * DESCRIPTION:
 *      Implements a complex number class.
 *      
 *      See the class documentation for more information.
 *
 ********************************************************************/

//package com.bluemarsh.adt;
package org.trinet.util;

/**
 * This class implements a complex number data type. It holds the
 * real and imaginary parts of a complex number and can perform
 * operations on that number.
 * <p>
 * This class provides the basic complex number operations you'd
 * expect. These include the basic add, subtract, multiply, and divide.
 * Also included are some basic trigonometric operations, such as sine,
 * cosine, tangent, and exponent. There are two utility methods for
 * finding the hyperbolic sine and cosine of a real number, which are
 * used in some of the complex number operations.
 *
 * @author  Nathan Fiedler 
 * @version 8/3/98
 */
public class Complex implements Cloneable, Comparable {
    public static final Complex ZERO = new Complex();
    public static final Complex ONE  = new Complex(1., 0.);
    public static final Complex I    = new Complex(0., 1.);

    /**
     * Real part of the complex number.
     */
    protected double real;
    /**
     * Imaginary part of the complex number.
     */
    protected double imag;

    /**
     * No-arg constructor for complex numbers. Creates a number
     * equal to 0+0i.
     */
    public Complex() { } // Complex

    /**
     * Two-arg constructor for complex numbers. Creates a number
     * equal to real+imag*i
     *
     * @param  real  real part of the new complex number
     * @param  imag  imaginary part of the new complex number
     */
    public Complex( double real, double imag ) {
        this.real = real;
        this.imag = imag;
    } // Complex

    /**
     * Adds the given complex number to this complex number.
     *
     * @param  b  complex number to add to this one
     */
    public void add( Complex b ) {
        real += b.real;
        imag += b.imag;
    } // add

    /**
     * Adds the given real number to this complex number.
     *
     * @param  b  real number to add to this one
     */
    public void add( double b ) {
        real += b;
    } // add

    /**
     * Adds the two given complex numbers and returns the result
     * as a new complex number.
     *
     * @param  a  first complex number to add
     * @param  b  second complex number to add
     * @return  sum of a and b
     */
    public static Complex add( Complex a, Complex b ) {
        double r = a.real + b.real;
        double i = a.imag + b.imag;
        return new Complex( r, i );
    } // add

    /**
     * Adds the given real number to the given complex number and
     * returns the result as a new complex number.
     *
     * @param  a  complex number to be added to
     * @param  b  real number to add to complex number
     * @return  sum of a and b
     */
    public static Complex add( Complex a, double b ) {
        return new Complex( a.real + b, a.imag );
    } // add

    /**
     * Returns the absolute value of this complex number.
     *
     * @return  absolute value of this number
     */
    public double abs() {
        return Math.sqrt( real * real + imag * imag );
    } // abs

    /**
     * Returns a new complex number object identical to this one.
     *
     * @return  copy of this complex number
     */
    public Object clone() {
        return new Complex( real, imag );
    } // clone

    /**
     * Compares this complex number to the given object for order.
     *
     * @return  a negative integer, zero, or a positive integer as
     *          this Object is less than, equal to, or greater than
     *          the given Object
     */
    public int compareTo( Object o ) {
        if ( o instanceof Complex ) {
            Complex a = (Complex)o;
            int val = (int)( abs() - a.abs() );
            a = null;
            return val;
        } else {
            return 0;
        }
    } // compareTo

    /**
     * Perform the cosine operation on this complex number.
     */
    public void cos() {
        double r = Math.cos( real ) * cosh( imag );
        imag = -Math.sin( real ) * sinh( imag );
        real = r;
    } // cos

    /**
     * Calculate the hyperbolic cosine of the given double value.
     *
     * @param  x  number to find cosh of
     * @return  cosh of x
     */
    public static double cosh( double x ) {
        return Math.exp( x ) + Math.exp( -x ) / 2;
    } // cosh

    /**
     * Divides the this complex number by the given complex number.
     *
     * @param  b  complex number to divide by this one
     */
    public void divide( Complex b ) {
        double r = ( real * b.real + imag * b.imag ) /
            ( b.real * b.real + b.imag * b.imag );
        imag = ( imag * b.real - real * b.imag ) /
            ( b.real * b.real + b.imag * b.imag );
        real = r;
    } // divide

    /**
     * Tests if this number is equal to the given number.
     *
     * @return  true if two numbers are equal, false otherwise
     */
    public boolean equals( Complex b ) {
        return ( real == b.real && imag == b.imag );
    } // equals

    /**
     * Perform the exponent operation on this complex number.
     */
    public void exp() {
        double r = Math.exp( real ) * Math.cos( imag );
        imag = Math.exp( real ) * Math.sin( imag );
        real = r;
    } // exp

    /**
     * Get the imaginary part of this complex number.
     *
     * @return  the imaginary part
     */
    public double getImag() {
        return imag;
    } // getImag

    /**
     * Get the real part of this complex number.
     *
     * @return  the real part
     */
    public double getReal() {
        return real;
    } // getReal

    /**
     * Multiplies the given complex number with this complex number.
     *
     * @param  b  complex number to multiply with this one
     */
    public void multiply( Complex b ) {
        double r = real * b.real - imag * b.imag;
        imag = real * b.imag + imag * b.real;
        real = r;
    } // multiply

    /**
     * Multiplies the given real number with this complex number.
     *
     * @param  b  real number to multiply with this complex number
     */
    public void multiply( double b ) {
        real *= b;
        imag *= b;
    } // multiply

    /**
     * Multiplies the given complex numbers with each other and
     * returns the result in a new complex number.
     *
     * @param  a  first complex number to multiply
     * @param  b  second complex number to multiply
     * @return  product of a and b
     */
    public static Complex multiply( Complex a, Complex b ) {
        double r = a.real * b.real - a.imag * b.imag;
        double i = a.real * b.imag + a.imag * b.real;
        return new Complex( r, i );
    } // multiply

    /**
     * Multiplies the given real number with the given complex
     * numer and returns the result in a new complex number.
     *
     * @param  a  complex number to multiply to
     * @param  b  real number to multiply with
     * @return  product of a and b
     */
    public static Complex multiply( Complex a, double b ) {
        return new Complex( a.real * b, a.imag * b );
    } // multiply

    /**
     * Set the real and imaginary parts of this complex number
     * to the new values given.
     *
     * @param  real  new real part value
     * @param  imag  new imaginary part value
     */
    public void set( double real, double imag ) {
        this.real = real;
        this.imag = imag;
    } // set

    /**
     * Perform the sine operation on this complex number.
     */
    public void sin() {
        double r = Math.sin( real ) * cosh( imag );
        imag = Math.cos( real ) * sinh( imag );
        real = r;
    } // sin

    /**
     * Calculate the hyperbolic sine of the given double value.
     *
     * @param  x  number to find sinh of
     * @return  sinh of x
     */
    public static double sinh( double x ) {
        return Math.exp( x ) - Math.exp( -x ) / 2;
    } // sinh

    /**
     * Subtracts the given complex number from this complex number.
     *
     * @param  b  complex number to subtract from this one
     */
    public void subtract( Complex b ) {
        real -= b.real;
        imag -= b.imag;
    } // subtract

    /**
     * Perform the tangent operation on this complex number.
     */
    public void tan() {
        Complex b = new Complex( real, imag );
        sin();
        b.cos();
        divide( b );
        b = null;
    } // tan

// aww added
/** Returns the negation of this complex number as a new Complex number. */
    public Complex negate() {
        return Complex.multiply(this, -1.);
    }

// aww added
/** Returns this*this as new Complex number. */
    public Complex squared() {
        return multiply(this,this);
    }

// aww added
/** Sets this complex number to value of this*this. */
    public void square() {
        multiply(this);
    }

// aww added
/** Performs sqrt operation on the input number and returns the root as new Complex number. */
    public static Complex sqrt(Complex in) {
        double r = Math.sqrt( (in.real + Math.sqrt(in.real*in.real + in.imag+in.imag))/2.);
        double i = Math.sqrt( (-in.real + Math.sqrt(in.real*in.real + in.imag+in.imag))/2.);
        return new Complex(r, i);
    }

// aww added
/** Sets this complex number to its sqrt value. */
    public void sqrt() {
        double r = Math.sqrt( (real + Math.sqrt(real*real + imag+imag))/2.);
        double i = Math.sqrt( (-real + Math.sqrt(real*real + imag+imag))/2.);
        set(r, i);
    }

// aww added
/** Returns the polar angle represented by this complex number. */
    public double angle() {
        return Math.atan2(imag, real);
    }

// aww added
/** Sets this number to its conjugate value. */
    public void conjugate() {
        imag = -imag ;
    }

    /**
     * Returns a string representation of this complex number.
     *
     * @return  string representing the value of this complex number
     */
    public String toString() {
        return "Complex=[" +
                           Double.toString( real ) +
                           ", " +
                           Double.toString( imag ) +
                           "]";
    } // toString

} // Complex
