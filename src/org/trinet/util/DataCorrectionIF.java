package org.trinet.util;
import org.trinet.jdbc.datatypes.*;
public interface DataCorrectionIF extends CorrTypeIdIF {
    public DataNumber getCorr();
    public String getCorrType();
    public String getCorrStatus();
    public void setCorr(DataNumber corr);
    public void setCorrValue(double value);
    public void setCorrType(String type);
    public void setCorrStatus(String status);

}
