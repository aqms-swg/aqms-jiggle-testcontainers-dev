package org.trinet.util;
public interface DateRangeIF {

     boolean after(java.util.Date date) ;

     boolean before(java.util.Date date) ;

     boolean excludes(java.util.Date date) ;
     boolean excludes(java.util.Date minDate, java.util.Date maxDate) ;

     boolean contains(java.util.Date minDate, java.util.Date maxDate) ;
     boolean contains(java.util.Date date) ;

     boolean overlaps(java.util.Date minDate, java.util.Date maxDate) ;

     boolean within(java.util.Date minDate, java.util.Date maxDate) ;

     void setMin(java.util.Date min) ;
     void setMax(java.util.Date max) ;

     void setLimits(java.util.Date min, java.util.Date max) ;

     void include(java.util.Date date) ;
}
