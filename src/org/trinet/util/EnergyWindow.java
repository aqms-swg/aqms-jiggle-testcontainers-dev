package org.trinet.util;

import org.trinet.jasi.TravelTime;

/**
* A TimeSpan defining a window where you are likely to find seismic energy.
* There are two ways of doing this: one if you have a magnitude and one when you don't.<p>
* If magnitude is available the window is defined by:<br>
* o start = preP seconds before the P-wave arrival time as calculated by the TravelTime model.<br>
* o end = P-wave arrival time + S-P time + coda duration time<p>
* If no magnitude is available the window is defined by:<br>
* o start = preP seconds before the P-wave arrival time as calculated by the TravelTime model.<br>
* o end = P-time + (2 * S-P time).<p>
*
* The window will never be less than the minWindow size (default = 10sec) or greater
* then maxWindow size (default = 120sec).
* This method uses the standard TravelTime model if none is set using setTravelTimeModel().<p>
* If setCodaModel() is not called this method uses a CodeModelMD model with the factors (0.87, 2.1, 0.0).
*
* @see: TravelTime()
* @see: CodaModelIF()
* @see: CodaModelMD()
*/

public class EnergyWindow extends TimeSpan {

    /** The travel-time model used to calculate the energy window.
     * If setTravelTimeModel() is not used to explicitely set the model, the
     * default, SoCal (Hadely/Kamamori) model is used. */
    TravelTime ttModel;
    /** Coda duration model. */
    CodaModelIF codaModel;

    /** Default time before P-arrival (secs) */
    final static double PrePtime = 1.0;

    /** Default minimum time window (secs) */
    final static double MinWindow = 20.0;   // had been 10 until 9/17/03

    /** Default maximum time window (secs) */
    final static double MaxWindow = 120.0;

    double preP;
    double minWindow;
    double maxWindow;

    public EnergyWindow() {
      setCodaModel(new CodaModel());
      setTravelTimeModel(TravelTime.getInstance());  // default to static default model
      setPreP(PrePtime);
      setMinWindow(MinWindow);
      setMaxWindow(MaxWindow);
    }

   /** Set the travel-time model used to calculate the energy window. */
    public void setTravelTimeModel(TravelTime model) {
      ttModel = model;
    }

    /** Return the travel-time model used to calculate the energy window. */
    public TravelTime getTravelTimeModel() {
      return ttModel;
    }

   /** Set the travel-time model used to calculate the energy window. */
    public void setCodaModel(CodaModelIF model) {
      codaModel = model;
    }

    /** Return the travel-time model used to calculate the energy window. */
    public CodaModelIF getCodaModel() {
      return codaModel;
    }

    public void setPreP(double secs) {
      preP = secs;
    }
    public double getPreP() {
      return preP;
    }
    public void setMinWindow(double secs) {
      minWindow = secs;
    }
    public double getMinWindow() {
      return minWindow;
    }
    public void setMaxWindow(double secs) {
      maxWindow = secs;
    }
    public double getMaxWindow() {
      return maxWindow;
    }

  /** Return a TimeSpan representing a window where you are likely to find
     * the seismic energy. Because the magnitude is not given and the coda cannot be
     * estimated, the the window length is based on twice the S-P time.
    */
    public TimeSpan getEnergyTimeSpan(double originTime, double horizDist) {

      DoubleRange ttps = ttModel.getTTps(horizDist);   // get both P and S times (after OT)

      double ptime = originTime + ttps.getMinValue();
      double SminusP  = ttps.size();    // this step for code clarity

      double windowSize = 2.0 * SminusP;
      windowSize = Math.max(windowSize, getMinWindow());
      windowSize = Math.min(windowSize, getMaxWindow());

      double energyStart = ptime - getPreP();
      double energyEnd   = energyStart + windowSize;

      return new TimeSpan(energyStart, energyEnd);
    }

  /** Return a TimeSpan representing a window where you are likely to find
    * the peak seismic energy. The the window length is based on the coda for
    * and event of this magnitude at this distance as calculated by the CodaModel.
    */
    public TimeSpan getEnergyTimeSpan(double originTime, double horizDist, double mag) {

      DoubleRange ttps = ttModel.getTTps(horizDist);   // get both P and S traveltimes (after OT)

      double pArrival = originTime + ttps.getMinValue();
      double SminusP  = ttps.size();    // this step for code clarity

      double windowSize = SminusP + codaModel.getDuration(horizDist, mag);

      // enforce max/min bounds
      windowSize = Math.max(windowSize, getMinWindow());
      windowSize = Math.min(windowSize, getMaxWindow());

      double energyStart = pArrival - getPreP();
      double energyEnd   = energyStart + windowSize;

      return new TimeSpan(energyStart, energyEnd);
    }
/* 
   public final static class Tester {
     public static void main(String args[]) {
       double dur, dist;
       double mag = 3.0;

       EnergyWindow ew = new EnergyWindow(); //static context

       for (int i = 0; i<= 500; i+=10 ){
         dist = (double) i;
         TimeSpan win1 = ew.getEnergyTimeSpan(0.0, dist);
         TimeSpan win2 = ew.getEnergyTimeSpan(0.0, dist, mag);
         System.out.println(mag +" " +i + " " +
             (int)win1.getDuration() +" " + (int)win2.getDuration()+
             " " + ew.getCodaModel().getMag(dist,win2.getDuration()));
       }
// coda windowing test
//     for (int i = 0; i<= 500; i+=10 ){
//
//       dist = (double) i;
//       dur = ew.getCodaModel().getDuration(dist, mag);
//       double remag = ew.getCodaModel().getMag(dist, dur);
//
//       System.out.println(mag +" " +i + " " + dur +" " + remag);
//     }
    } // end of main
  } // end of Tester
*/
}  // end of EnergyWindow class

/** Default coda duration calculation.
 *  This is based on MD as usd by HYPOINVERSE.
However, the distance factor is set to 0.0 not 0.0035. This factor caused the
coda to get shorter with distance. This is counter to observation so this model
disables that distance factor. Also the second factor is set to 2.1 rather than 2.0.
@see CodaModelMD
*/
class CodaModel extends CodaModelMD{
  public CodaModel() {
    setFactors(0.87, 2.1, 0.0);
  }
}  // end of CodaModel
