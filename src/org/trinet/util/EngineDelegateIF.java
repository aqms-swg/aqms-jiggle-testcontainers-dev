package org.trinet.util;
import java.beans.*;
public interface EngineDelegateIF {
// known engine types identifier keys

  public static final String LOCATION = "LOCATION";
  public static final String MAGNITUDE = "MAGNITUDE";

  public void addPropertyChangeListener(PropertyChangeListener pcl);
  public void removePropertyChangeListener(PropertyChangeListener pcl);
  public void addPropertyChangeListener(String propName, PropertyChangeListener pcl);
  public void removePropertyChangeListener(String propName, PropertyChangeListener pcl);
  public boolean solve(Object data);

  public boolean success();
  public void reset();              // resets engine state, messages etc.

  public String getStatusString();  // processing status
  public String getResultsString(); // out results status and/or summary data 

  public GenericPropertyList getDelegateProperties();
  public boolean setDelegateProperties(GenericPropertyList props);
  public boolean setDelegateProperties(String propFileName);
  public boolean initializeEngineDelegate();

  public void addEngine(String keyName, EngineIF engine);
  public boolean hasEngine(String type);

}
