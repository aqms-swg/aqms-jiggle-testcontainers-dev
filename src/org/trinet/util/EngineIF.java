package org.trinet.util;
import java.beans.*;
import org.trinet.jasi.JasiFactory;

public interface EngineIF extends JasiFactory, ConfigurationSourceIF {

  /** String name of SOLVED property.*/
  public static final String SOLVED = "wasSolved";
  public static final String FAILED = "failed";

  public void setEngineName(String name);
  public String getEngineName();

  public void addPropertyChangeListener(PropertyChangeListener pcl);
  public void addPropertyChangeListener(String propName, PropertyChangeListener pcl);
  public void removePropertyChangeListener(PropertyChangeListener pcl);
  public void removePropertyChangeListener(String propName, PropertyChangeListener pcl);

  //public void configure(); // see ConfigurationSourceIF
  //public void configure(int source, String location, String section); // see ConfigurationSourceIF

  public GenericPropertyList getProperties();
  public void setProperties(GenericPropertyList props);
  public boolean setProperties(String propFileName);

  public void initializeEngine() throws IllegalStateException;
  public boolean isEngineValid();

  public boolean solve(Object data);
  public boolean success();         // true if last solve succeeded
  public Object getResult();        // Object modified or created by engine
  public String getStatusString();  // current processing status
  public String getResultsString(); // status and/or results summary data 
  public void setStatusString(String status);  // current processing status
  public void setResultsString(String results); // status and/or results summary data 
  public void reset();              // resets engine status and results, etc.
  public void setDebug(boolean tf); // default state false, no debug
  public void setVerbose(boolean tf); // default state false, no verbose
}
