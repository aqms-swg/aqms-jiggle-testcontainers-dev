package org.trinet.util;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2005</p>
 * <p>Company: USGS</p>
 * @author Doug Given
 * @version 1.0
 *
 * Based on: http://www.cs.fit.edu/~ryan/java/programs/jsp/ExecuteBean-java.html
 */
import java.io.*;

public class ExecuteCommand implements Serializable {

    private String cmd;

    private String input = "";

    private int ret;

    private String stdout, stderr;


    public static void main (final String args[]) {
        final String input = (args.length>1)?args[1]:"";
        final ExecuteCommand b = new ExecuteCommand ();
        try {
            b.main (args[0], input);
            System.out.println ("Standard input :\n" + input);
            System.out.println ("Standard output:\n" + b.getStdout());
            System.out.println ("Standard error :\n" + b.getStderr());
            System.out.println ("Return code = " + b.getRet());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void main (final String command, final String input) throws IOException {
        setCmd (command);
        setInput (input);
        main ();
    }

    public void main () throws IOException {
        final Process proc = Runtime.getRuntime().exec(getCmd());
        final BufferedReader readConsole =
                new BufferedReader (new InputStreamReader (proc.getInputStream()));
        final BufferedReader errorConsole =
                new BufferedReader (new InputStreamReader (proc.getErrorStream()));
        final BufferedWriter writeConsole =
                new BufferedWriter (new OutputStreamWriter(proc.getOutputStream()));

        final WriterThread myWriterThread =
                new WriterThread (new StringReader (getInput()),writeConsole);
        final ReaderThread myReaderThread = new ReaderThread (readConsole);
        final ReaderThread myErrorReaderThread = new ReaderThread (errorConsole);

        try {
            myWriterThread.join();
            myReaderThread.join();
            myErrorReaderThread.join();
            setRet (proc.waitFor());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        setStdout (myReaderThread.output.toString());
        setStderr (myErrorReaderThread.output.toString());

    }

    public String getCmd () { return cmd; }
    public void setCmd (String c) { cmd=c; }

    public String getInput () { return input; }
    public void setInput (String i) { input=i; }

    public int getRet () { return ret; }
    private void setRet (int r) { ret=r; }

    public String getStdout () { return stdout; }
    private void setStdout (String s) { stdout=s; }
    public String getStderr () { return stderr; }
    private void setStderr (String s) { stderr=s; }


    static class ReaderThread extends Thread {
        private final BufferedReader from;
        public final StringBuffer output = new StringBuffer ();
        ReaderThread (BufferedReader r) { from=r; start(); }
        public void run() {
            String line;
            try {
                while ((line = from.readLine()) != null) {
                    output.append(line+"\n");
                }
                from.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    static class WriterThread extends Thread {
        private final BufferedReader from;
        private final BufferedWriter to;
        WriterThread (Reader r, BufferedWriter w) {
            from=new BufferedReader(r); to=w; start();
        }
        public void run() {
            String line;
            try {
                while ((line = from.readLine()) != null) {
                    to.write (line);
                    to.newLine();
                }
                from.close();
                to.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
