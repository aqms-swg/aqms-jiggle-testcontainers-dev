package org.trinet.util;

import java.util.*;
import java.io.*;

/**
 * Execute an external system command. Handles reading of StdIn and StdErr, etc.<p>
 * Note that the type and syntax of commands is NOT identical with working with the command shell.
 * To run the Windows command interpreter, execute either command.com or cmd.exe (depends on the OS).
 * In Unix use a shell like "/bin/sh". <p>
 * Redirects, pipes, etc. don't work.
 * Based on: http://www.javaworld.com/javaworld/jw-12-2000/jw-1229-traps.html
 *
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: USGS</p>
 * @author Doug Given
 * @version 1.0
 *
 *
 */

public class ExternalCommand {

  String cmd;
  int exitStatus = -1;
  StreamSucker stdErr;
  StreamSucker stdOut;

/**
 * Call will block until the external command is finished.
 */
  public ExternalCommand() {
  }
  public ExternalCommand(String cmd) {
    execute(cmd);
  }
  public ExternalCommand(String[] cmd) {
    execute(cmd);
  }
  /** Execute this command.
 * @See: Process.exec(String)
  */
  public int execute(String cmd) {
    String str[] = {cmd};
    return execute(str);
  }
  /** Execute this command.
  * @See: Process.exec(String[])
  *
  * On error the stack trace returns something like:<br>
  * java.io.IOException: CreateProcess: "FakeHypo.bat 13998604" error=2<p>
  *
  * The error number comes from the native OS. For Win32 error codes see:<br>
  * http://www.tharo.com/webhelp/win32_error_codes.htm
  */
  public int execute(String[] cmd) {

    exitStatus = 0;
    if (cmd == null) return exitStatus;

    try {
      // create the external process
      Runtime rt = Runtime.getRuntime();
      Process proc = rt.exec(cmd);

      // capture any output
      stdErr = new StreamSucker(proc.getErrorStream(), "ERROR");
      stdOut = new StreamSucker(proc.getInputStream(), "OUTPUT");

      //  wait for completion - normal exit status = 0
      exitStatus = proc.waitFor();

// The following produces: java.lang.IllegalMonitorStateException: current thread not owner
//      try {
//        proc.wait(timeout);
//        } catch (InterruptedException ex) { exitStatus = -1; }  // timedout
//        exitStatus = proc.exitValue();

    } catch (Throwable t) {
      t.printStackTrace();
    }

      return exitStatus;
  }
  public int exitStatus() {
    return exitStatus;
  }
  public StringBuffer getStdErr () {
    return stdErr.getStringBuffer();
  }
  public StringBuffer getStdOut () {
    return stdOut.getStringBuffer();
  }

/*
// test
  public static void main(String[] args) {

    //String command[] = {"cmd", "dir"};
//    String command[] = {"java"};
//      String command[] = {"T:\\CodeSpace\\solserver\\Test\\FakeHypo.bat"};
      String command[] = {"C:\\test.bat"};

      // Execute it
      ExternalCommand exComm = new ExternalCommand(command);
      // dump results
      System.err.println("command done:");

      System.err.println("stdout size = "+exComm.getStdOut().length());
      System.out.println(exComm.getStdOut().toString());

      System.err.println(exComm.getStdErr().toString());

  }
*/
// /////////////////////////////////////////////////////////////////////
/** Create thread to read external command's StdIn or StdErr streams. */
class StreamSucker extends Thread
  {
      InputStream is;
      String type;
      StringBuffer buffer = new StringBuffer();

      StreamSucker(InputStream is, String type)
      {
          this.is = is;
          this.type = type;
          this.start();
      }

      public void run()
      {
          try
          {
              InputStreamReader isr = new InputStreamReader(is);
              BufferedReader br = new BufferedReader(isr);
              String line;
              // Note: br.readLine() does NOT return the line terminator
              //       so we stick one on in the buffer.
              while ( (line = br.readLine()) != null) {
                buffer.append(line+"\n");
                // System.out.println(type + ">" + line);
              }
              } catch (IOException ioe)
              {
                System.err.println("Error reading stream: "+type);
                ioe.printStackTrace();
              }
      }
      public String getBuffer() {
        return buffer.toString();
      }
      public StringBuffer getStringBuffer() {
        return buffer;
      }
}

}
