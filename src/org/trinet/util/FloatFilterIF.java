package org.trinet.util;

public interface FloatFilterIF extends FilterIF {
/** Return reference to input object parameter which contains a representation of a waveform time series.
* of input and output objects.
*/
    float [] filter(float [] sampleSeries);
}
