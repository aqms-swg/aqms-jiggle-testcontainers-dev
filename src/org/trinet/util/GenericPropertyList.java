// Source for GenericPropertyList adapted from Jiggle/PropertyList (DG)
// modified for a more generic class (AW)
// Note differences in constructors for setting file paths and be aware
// that invoking setFilename(file) sets both the user and default pathnames
// versus a separate invocation of setUser.../setDefaultPropertiesFileName
// to explicit canonical paths.
package org.trinet.util;
import java.awt.Color;
import java.util.*;
import java.io.*;
import org.trinet.jasi.*; // for static method instance creation
import org.trinet.jiggle.common.PropConstant;
import org.trinet.util.gazetteer.LatLonZ;

/**
 * Generic properties list.  Note differences in the constructors for
 * setting the default and user property file paths. Be aware that invoking
 * setFilename() sets both the user and default pathnames versus the
 * invocation of setUserPropertiesFileName() or setDefaultPropertiesFileName()
 * to set an explicit file path.
 */
public class GenericPropertyList extends Properties implements Cloneable {
   private static final int SB_CAPACITY_DEF = 512;
   private static final char SEP_CHAR = ' ';
// Should change class implementation to have the pathname/filename members instead be found and set
// as named property, value pairs in the map.
// We would need to declare some static property name Strings mapped to values assigned to members below - aww
// Property path & file names
    protected String defaultPropertiesFileName; // usually the path qualified name
    protected String userPropertiesFileName; // usually the path qualified name
    protected String filename; // file only without absolute/relative path prefixed
    protected String filetype = "";

    private static final String TOKEN_DELIM = ", \t\n\r\f"; // includes ","
    // get system specific file separator so we can build a path string
    public static final String FILE_SEP  = System.getProperty("file.separator");  // "/" on UNIX, "\" on PC's ;  // "/" on UNIX, "\" on PC's

    /** Set true to turn on debug messages, default is false. */
    public boolean debug = false;

/**
 *  Constructor: make empty list. Need to Use setxxxxName() then reset() to load.
 */
    public GenericPropertyList() { }

/**
 * This form of Constructor uses explicit path/file as passed and
 * does not try to build a path using System enviroment properties
 * e.g.  "userhome".
 * @param userPropertiesFileName
 * @param defaultPropertiesFileName
 */
    public GenericPropertyList(String userPropertiesFileName,
                               String defaultPropertiesFileName)
    {
        this.userPropertiesFileName = userPropertiesFileName ;
        this.defaultPropertiesFileName = defaultPropertiesFileName ;
        reset();
    }

/**
 *  Constructor: Sets default properties to those found in input property list. Doesn't read the files.
 *  Note that the properties found in the input instance go into the default map, not the primary map.
 */
    public GenericPropertyList(Properties props)
    {
        super(props);
    }
/**
 *  Constructor: Sets default properties to those of input property list. Doesn't read the files.
 */
    public GenericPropertyList(GenericPropertyList props)
    {
        super(props);
        // copy class instance data members:
        this.userPropertiesFileName = props.userPropertiesFileName;
        this.defaultPropertiesFileName = props.defaultPropertiesFileName;
        this.filename = props.filename;
        this.filetype = props.filetype;
    }
/**
 *  Constructor: Sets the default properties to those of input list then reads properties from the files.
 */
    public GenericPropertyList(GenericPropertyList props, String dpfn, String upfn)
    {
        this(props);
        this.userPropertiesFileName = upfn;
        this.defaultPropertiesFileName = dpfn;
        reset();
    }
/**
 *  Constructor: Sets the default properties to those of the input list, then reads properties from specified file.
 */
    public GenericPropertyList(Properties props, String filename)
    {
        super(props);
        setFilename(filename);
        reset();
    }
/**
 *  Constructor: Reads the properties from the filename specified.
 *  Default and user pathnames are constructed from the values if set for system properties
 *  named by template: filetype+"_HOME" and "filetype+USER_HOMEDIR"
 */
    public GenericPropertyList(String filename)
    {
        this((Properties)null,filename);
    }
    //
    // End of Constructors class methods follow
    //
    //Used to be:
    // If <i>defaultFileName == null</i>, the filename will be the same
    // for both the user and default paths though their paths may differ.
    // If no System property is defined for the application's default
    // property file path, it defaults to current working directory.

    /** Set the subdirectory and name of the property file for user path
     * and default paths, set any default required values, then loads
     * properties from the default file, if any, and then those from the
     * user's properties file, if any.
     * If <i>userFilename == null</i>,  no user properties file is read.
     * and if <i>defaultFileName == null</i>, no default properties is read.
     * The filename can be the same though the absolute paths may differ.
     * If no System property is defined for the default property file path,
     * it defaults to current working directory.
     * @see #getDefaultFilePath()
     * @see #getUserFilePath()
     * */
    public boolean initialize(String subdir, String userFileName, String defaultFileName) {
        filetype = subdir;       // file home subdir relative to user's default path
        filename = userFileName; // name of user props file for path
        final StringBuilder sb = new StringBuilder(256);
        if (userFileName != null && userFileName.length() > 0)
            sb.append(getUserFilePath()).append(FILE_SEP).append(userFileName);
        userPropertiesFileName = sb.toString();

        sb.setLength(0);
        if (defaultFileName != null && defaultFileName.length() > 0)
            sb.append(getDefaultFilePath()).append(FILE_SEP).append(defaultFileName);
        defaultPropertiesFileName = sb.toString();
        if (true) {
            System.out.println("userPropFile   : " + userPropertiesFileName);
            System.out.println("defaultPropFile: " + defaultPropertiesFileName);
        }
        return reset(); // read the props from above file
    }

    /** Return a deep clone of this object. Returns null on failure.
     * Subclasses should inherit java.io.Serializable. Note that
     * Oracle connections are not Serializable and instantiated DataSource may
     * as element member of subclass list would cause an exception.
     * */
    public GenericPropertyList deepClone() {
        try {
          return (GenericPropertyList) org.trinet.util.Cloner.cloneObject(this);
        } catch (Exception ex) {
          ex.printStackTrace();
          return null;
        }
    }

    /** Login host user name (do not confuse with database user login name). */
    public String getUserName() {
      return System.getProperty("user.name", "");
    }

    /**
     * Set file and path names. Returns false if the file does not exist or cannot be read.
     */
    public final boolean setUserPropertiesFileName(String fn)
    {
      //if (fn == null) return true;  // setting to null is OK?
      userPropertiesFileName = (fn == null) ? "" : fn;
      File file = new File(getUserPropertiesFileName());
      return file.canRead();
    }
    public String getUserPropertiesFileName()
    {
        return userPropertiesFileName;
    }
    /**
     * Set file and path names. Returns false if the file does not exist or cannot be read.
     */
    public final boolean setDefaultPropertiesFileName(String fn)
    {
      defaultPropertiesFileName = fn;
      if (fn == null) return true;  // setting to null is OK
      File file = new File(getDefaultPropertiesFileName());
      return file.canRead();
    }
    public String getDefaultPropertiesFileName()
    {
        return defaultPropertiesFileName;
    }
    public void setFiletype(String type) {
      filetype = type;
    }
    /** Return the name for the property file. */
    public String getFilename() {
      return filename;
    }
    /** Return the type of the property file. Default is "" (blank).*/
    public String getFiletype() {
        return filetype;
    }

    /**
     * Set the user and default properties filename, input must be a filename,
     * not a complete path from root. Complete file paths are constructed using
     * the input filename and the rules for constructing the user and default paths.
     * @see #getUserFilePath()
     * @see #getDefaultFilePath()
     * @see #setFiletype(String type)
     */
    public final void setFilename(String fn)
    {
        filename = fn;
        final StringBuilder sb = new StringBuilder(256);
        sb.append(getDefaultFilePath()).append(FILE_SEP).append(filename);
        defaultPropertiesFileName = sb.toString();
        sb.setLength(0);
        sb.append(getUserFilePath()).append(FILE_SEP).append(filename);
        userPropertiesFileName = sb.toString();
    }

/** Constructs a path to the user file by resolving the environmental variable
 * [filetype]_USER_HOMEDIR defined on the commandline with the -D directive.
 * If it is not defined filetype is assumed to be a subdirectory preceded by
 * a "." and is appended to the system level user-home path.<p>
 *
 * Examples:<br>
 * % java mycode -DMYCODE_USER_HOMEDIR=C:/testing/configs props.cfg <br>
 * Will look for props.cfg at C:/testing/configs/props.cfg <p>
 *
 * % java mycode props.cfg  <br>
 * Will look for props.cfg at [system-specific-user-dir]/.mycode/props.cfg <p>
 *
 * @see: setFiletype(). */

 // This seems pretty weird
    public String getUserFilePath() {
        String userHome = System.getProperty(getFiletype().toUpperCase()+"_USER_HOMEDIR");
        if (userHome !=  null) return userHome;
        else userHome = System.getProperty("user.home", ".");
        String subdir = getFiletype();
        if (subdir.equals("")) return userHome;
        final StringBuilder sb = new StringBuilder(150);
        sb.append(userHome).append(FILE_SEP).append(".").append(subdir);
        return sb.toString();
    }

    /** Look-up default path System property.
    * The JVM System property name is of the form: "<filetype>_HOME"
    * and must be passed with -D option on java command line.
    * When a System property is not defined the default path is ".",
    * the current working directory of the invoking application.
    * Note that the native machine variables are not accessible from JVM.
    */
    public String getDefaultFilePath() {
        return System.getProperty(getFiletype().toUpperCase()+"_HOME", ".");
    }

    /**
     * Reset all property values to input file settings.
     * User property file settings override default property file settings.
     * If a property value is NOT either file, and not a required property,
     * its value is NOT reset.
     */
    public boolean reset() {
        setRequiredProperties();
        boolean status = readDefaultProperties();
        //override default props if any set above with user settings
        //status |= readUserProperties(); // require at least one file specified file to exist
        status &= readUserProperties();// require any specified file to exist
        return status;
    }

    /** Override parent to trim leading and trailing whitespace. Otherwise, inept typing
     * could result is a hard-to-find invalid property string.*/
    public String getProperty(String property) {
      String str = super.getProperty(property);
      if (str == null) return str;
      return str.trim();
    }

    public void setProperties(Properties props) {
        Enumeration e = props.propertyNames();
        String key = null;
        String value = null;
        while (e.hasMoreElements()) {
            key = (String)e.nextElement();
            value = props.getProperty(key);
            if (value != null) setProperty(key, value);
        }
    }

    public boolean setProperties(String str) {
        StringTokenizer stoke = new StringTokenizer(str, "\n\r"); 
        String key = null;
        String value = null;
        String property = null;
        boolean status = true;
        try {
          while (stoke.hasMoreTokens()) {
            property = stoke.nextToken();
            if (property.startsWith("#") || property.startsWith("!")) continue;
            // added ": \t" since they are valid key delimiters in Properties class -aww 2008/11/10 
            StringTokenizer stoke2 = new StringTokenizer(property, "=: \t");
            key =   (stoke2.hasMoreTokens()) ? stoke2.nextToken().trim() : null;
            value = (stoke2.hasMoreTokens()) ? stoke2.nextToken().trim() : "";
            // BUGFIX since adding "whitespace" delimiters, note that the 
            // desired value may be a list of elements delimited by whitespace, 
            // so find the substring whose starting index is at the first parsed value
            // found in the substring starting past the end of the key value of property string 
            if (key != null) {
              if (value != "") {
                // below needed for case where first parsed "value" string might be inside key string 2009/05/02 aww 
                String afterKey = property.substring(key.length());
                value = afterKey.substring(afterKey.indexOf(value)).trim();
              }
              setProperty(key, value);
            }
          }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            status = false;
        }
        return status;
    }

    public void setProperty(String str, int val) {
       setProperty(str, String.valueOf(val));
    }
    public void setProperty(String str, long val) {
       setProperty(str, String.valueOf(val));
    }
    public void setProperty(String str, float val) {
       setProperty(str, String.valueOf(val));
    }
    public void setProperty(String str, double val) {
       setProperty(str, String.valueOf(val));
    }
    public void setProperty(String str, boolean val) {
       setProperty(str, String.valueOf(val));
    }
 
    protected void logNullValue(String str) {
      System.err.println("null value for " + str);
    }

    public void setProperty(String str, Object val) {
       if (val == null) {
         logNullValue(str);
         return;
       }
       if (val instanceof Color) {
         setProperty(str, Integer.toHexString(((Color) val).getRGB()));
       } else {
         setProperty(str, val.toString());
       }
    }

    public void setProperty(String str, boolean [] values) {
      if (values == null) {
        logNullValue(str);
        return;
      }
      final StringBuilder sb = new StringBuilder(SB_CAPACITY_DEF);
      for (int ii=0; ii<values.length; ii++)  {
          sb.append(values[ii]).append(SEP_CHAR);
      }
      setProperty(str, sb.toString());
    }
    public void setProperty(String str, float [] values) {
      if (values == null) {
        logNullValue(str);
        return;
      }
      final StringBuilder sb = new StringBuilder(SB_CAPACITY_DEF);
      for (int ii=0; ii<values.length; ii++)  {
          sb.append(values[ii]).append(SEP_CHAR);
      }
      setProperty(str, sb.toString());
    }
    public void setProperty(String str, double [] values) {
      if (values == null) {
        logNullValue(str);
        return;
      }
      final StringBuilder sb = new StringBuilder(SB_CAPACITY_DEF);
      for (int ii=0; ii<values.length; ii++)  {
          sb.append(values[ii]).append(SEP_CHAR);
      }
      setProperty(str, sb.toString());
    }
    public void setProperty(String str, long [] values) {
      if (values == null) {
        logNullValue(str);
        return;
      }
      final StringBuilder sb = new StringBuilder(SB_CAPACITY_DEF);
      for (int ii=0; ii<values.length; ii++)  {
          sb.append(values[ii]).append(SEP_CHAR);
      }
      setProperty(str, sb.toString());
    }
    public void setProperty(String str, int [] values) {
      if (values == null) {
        logNullValue(str);
        return;
      }
      final StringBuilder sb = new StringBuilder(SB_CAPACITY_DEF);
      for (int ii=0; ii<values.length; ii++)  {
          sb.append(values[ii]).append(SEP_CHAR); 
      }
      setProperty(str, sb.toString());
    }

    public void setProperty(String str, String [] values) {
       if (values == null) {
         logNullValue(str);
         return;
       }
       final StringBuilder sb = new StringBuilder(SB_CAPACITY_DEF);
       for (int ii=0; ii<values.length; ii++)  {
           sb.append(values[ii]).append(SEP_CHAR); // aww bug fix 7/21/2005 - value was missing index subscript 
       }
       setProperty(str, sb.toString());
    }

    /**
     * Set required default properties so things will still work even if they are not set
     * in the properties files or the files are not present. This must be overridden by
     * subclasses to set their private essential property values.
     */
    public void setRequiredProperties() { }

    /**
     * Read properties from system default file.
     */
    public boolean readDefaultProperties() {
      if (defaultPropertiesFileName == null || defaultPropertiesFileName.equals("")) return true;
      return readPropertiesFile(defaultPropertiesFileName);
    }

    /**
     * Read properties from the user's file.
     */
    public boolean readUserProperties() {
      if (userPropertiesFileName == null || userPropertiesFileName.equals("")) return true;
      return readPropertiesFile(userPropertiesFileName);
    }

    /**
     * Return <i>true</i> if instance user properties file exists and can be read,
     * <i>false</i> otherwise.
     * */
    public boolean hasUserPropertiesFile() {
        return hasPropertiesFile(userPropertiesFileName);
    }
    /**
     * Return <i>true</i> if instance default properties file exists and can be read,
     * <i>false</i> otherwise.
     * */
    public boolean hasDefaultPropertiesFile() {
        return hasPropertiesFile(defaultPropertiesFileName);
    }
    private boolean hasPropertiesFile(String fileName) {
        File file = new File(fileName);
        boolean status = false;
        try {
          status = file.exists();
        }
        catch (SecurityException ex) {
          // accessing requires java Security permission io.FilePermission read on path & file
          System.err.println(getClass().getName()+" no permission to read property file " + fileName);
        }
        return status;
    }

    /**
     * Loads properties from the input file name if it specifies an absolute path.
     * Otherwise, for a relative file name, prefixes it with user path and and loads
     * the file if it exists; if is does not exist, attempts to load the input
     * fileName as resource relative to classpath. Accessing process requires java
     * Security permission io.FilePermission read on file.
     * 
     * @param fileName        the input file name.
     * @param ignoreNotExists true to ignore if file does not exist, false
     *                        otherwise.
     * @returns false if no file is found or load failure, otherwise true.
     */
    public final boolean readPropertiesFile(String fileName) {
        return readPropertiesFile(fileName, this, false);
    }

    /**
     * Loads properties from the input file name if it specifies an absolute path.
     * Otherwise, for a relative file name, prefixes it with user path and and loads
     * the file if it exists; if is does not exist, attempts to load the input
     * fileName as resource relative to classpath. Accessing process requires java
     * Security permission io.FilePermission read on file.
     * 
     * @param fileName        the input file name.
     * @param props the properties to load.
     * @param ignoreNotExists true to ignore if file does not exist, false
     *                        otherwise.
     * @returns false if no file is found or load failure, otherwise true.
     */
    public final boolean readPropertiesFile(String fileName, Properties props, boolean ignoreNotExists) {
        if (fileName == null || props == null) {
            return false;
        }

        InputStream inStream = null;
        // NOTE accessing process requires java Security permission io.FilePermission read on file
        try {
            File file = new File(fileName); // abstract file
            String parent = file.getParent(); 

            //if ( file.isAbsolute() && file.exists() ) { // a fully specified path+file
            if (parent != null) { // a specified path+file
                if (file.exists()) {
                    inStream = new BufferedInputStream(new FileInputStream(file));
                }
            } else { // file, 1st look for file prefixed by user home path
                final StringBuilder sb = new StringBuilder(256);
                file = new File(sb.append(getUserFilePath()).append(FILE_SEP).append(fileName).toString());
                if (file.exists()) {
                    inStream = new BufferedInputStream(new FileInputStream(file));
                } else { // not in user's domain, so next look for file using loader classpath
                         // such as to read of default resource file stored in jar or database
                    inStream = getClass().getResourceAsStream(fileName);
                }
            }

            // read properties from file input stream
            if (inStream != null) {
              props.load(inStream);
              return true;
            }
            if (!ignoreNotExists) {
                System.err.printf("%s property file not found (%s)\n", getClass().getName(), fileName);
            }
        }
        catch (IOException ex) {
            System.err.printf("%s I/O error reading property file (%s): %s\n", getClass().getName(), fileName, ex);
        }
        catch (SecurityException ex) {
            System.err.printf("%s no permission to read property file (%s): %s\n", getClass().getName(), fileName, ex);
        }
        catch (Exception ex) {
            System.err.printf("%s error reading property file (%s):\n", getClass().getName(), fileName);
            ex.printStackTrace();
        }
        finally { // close if stream opened
            try {
               if (inStream != null) inStream.close();
            }
            catch(IOException ex2) { ex2.printStackTrace(); }
        }
        return false;
    }

    /**
     * Get the default save properties filename.
     * @return the default save properties filename.
     * @see #saveProperties(String)
     */
    protected String getDefaultSavePropertiesFilename() {
        return userPropertiesFileName;
    }

    /**
     * Save the current set of properties
     */
    public boolean saveProperties(String header) {
        return savePropertiesToFile(header, getDefaultSavePropertiesFilename());
    }

    /**
    * Save the current set of properties
    */
    protected boolean savePropertiesToFile(String header, String filename) {
      // save previous
      File oldFile = new File(filename);
      if (oldFile.isDirectory()) {
        try {
          throw new IllegalArgumentException("Input filename must be regular file, not directory");
        }
        catch (IllegalArgumentException ex) {
            ex.printStackTrace();
        }
        return false;
      }

      if (oldFile.exists() && oldFile.isFile()) {
          File backFile = new File(filename+"~");
          if (backFile.exists()) backFile.delete();
          oldFile.renameTo(backFile);
      }

      boolean status = false;
      try {
           BufferedOutputStream out =
              new BufferedOutputStream(new FileOutputStream(filename));
        this.store(out, header);
        out.close();
        status = true;
      } catch (IOException e) {
          status = false;
          System.err.println(e);
      }
      return status;
    }

    /** Overrides Hashtable.keys() method to alpha-sort the keys before
    * they are written out the a properties file. Properties extends Hashtable.
    * See: http://forum.java.sun.com/thread.jsp?forum=31&thread=141144*/
    public synchronized Enumeration keys() {
      Enumeration keysEnum = super.keys();
      Vector keyList = new Vector();
      while(keysEnum.hasMoreElements()){
        keyList.add(keysEnum.nextElement());
      }
      Collections.sort(keyList);
      return keyList.elements();
    }

    /**
     * Return by default, an empty list, subclasses should override this method to return
     * a non-empty list of standard names that are utilized by instantiating applications.
     */
    public List getKnownPropertyNames() {
        return new ArrayList(0);
    }

    /**
     * Returns string of line-feed delimited string properties keys and values.
     * Like list() method in parent, except it doesn't truncate value length.
     * This method is useful for debugging.
     */
    public String listToString() {
       final StringBuilder sb = new StringBuilder(2048);
       // for (Enumeration e = propertyNames() ; e.hasMoreElements() ;) {
       for (Enumeration e = keys() ; e.hasMoreElements() ;) {    // alpha-sort the list
           String key = (String) e.nextElement();
           sb.append(key).append("=");   // key

           if (isHideProperty(key)) {
               sb.append(PropConstant.MASK_VALUE);
           } else {
               sb.append(getProperty(key, "-not defined-"));
           }
           sb.append(("\n"));
       }
       return sb.toString();
    }

    /** Returns true if this property is specified in the property list. */
    public boolean isSpecified(String property) {
      return (getProperty(property) != null);
    }

    public LatLonZ getLatLonZ(String property) {
        String str = getProperty(property);
        if (str == null) return null;

        StringTokenizer tok = new StringTokenizer(str, TOKEN_DELIM);
        if (tok.countTokens() < 2)
          throw new PropertyFormatException(property+" = "+str+" too few tokens for LatLonZ");
        return
            new LatLonZ( Double.valueOf(tok.nextToken()).doubleValue(),
                         Double.valueOf(tok.nextToken()).doubleValue(),
                         (tok.hasMoreTokens()) ?
                             Double.valueOf(tok.nextToken()).doubleValue() : 0.
                       );
    }

   // If other classes use getProperty() they will get a string and they often want an
   // int, float, etc. So we'll do the type conversion here.
   //
   // Perhaps negative for "undefined" would be better return value,
   // since data values are usually >= 0    aww
   // !!! I agree and made the change 11/12/04 - DDG

   /**
    * Return property as an integer.
    * Returns -1 if there is no such property defined. (There is no NaN for int.)
    */
   public int getInt(String property) {
       return getInt(property, -1);
   }
   /**
    * Return property as an integer.
    * Returns input <i>deafaultValue</i> if there is no such property defined. (There is no NaN for int.)
    */
   public int getInt(String property, int defaultValue) {
       String str = getProperty(property);
       if (str == null) return defaultValue;
       // return Integer.valueOf(str).intValue();
       // Rounding the float prevents error when user specifies a float for an int property
       return Math.round(Float.valueOf(str).floatValue());
   }

   /**
    * Return property as a long integer.
    * Returns -1 if there is no such property defined.
    */
   public long getLong(String property) {
       return getLong(property, -1l);
   }
   /**
    * Return property as an long.
    * Returns input <i>deafaultValue</i> if there is no such property defined. (There is no NaN for long.)
    */
   public long getLong(String property, long defaultValue) {
       String str = getProperty(property);
       if (str == null) return defaultValue;
       //return Long.valueOf(str).longValue();
       // Rounding the double prevents error when user specifies a float for an long property
       return Math.round(Double.valueOf(str).doubleValue());
    }

   /**
    * Return property as a float. Returns NaN if property is undefined.
    */
    public float getFloat(String property) {
        return getFloat(property, Float.NaN);
    }
    /** Return property as a float. 
    * Returns input <i>deafaultValue</i> if there is no such property defined.
    */
    public float getFloat(String property, float defaultValue) {
        String str = getProperty(property);
        if (str == null) return Float.NaN;
        if (str.equals("NaN")) return Float.NaN;
        if (str.equals("Inf")) return Float.MAX_VALUE;
        return Float.valueOf(str).floatValue();
    }

    /**
     * Return property as a double. Returns NaN if property is undefined.
     */
    public double getDouble(String property) {
        return getDouble(property, Double.NaN);
    }
    /** Return property as a double. 
    * Returns input <i>deafaultValue</i> if there is no such property defined.
    */
    public double getDouble(String property, double defaultValue) {
        String str = getProperty(property);
        if (str == null) return defaultValue;
        if (str.equals("NaN")) return Double.NaN;
        if (str.equals("Inf")) return Double.MAX_VALUE;
        return Double.valueOf(str).doubleValue();
    }

    /** Return a boolean property value. 
     * Returns <i>false'</i> if property is undefined. */
    public boolean getBoolean(String prop) {
      return getBoolean(prop, false);
    }
    /** Return a boolean property value. 
     * Returns input <i>defaultValue</i>if property is undefined. */
    public boolean getBoolean(String prop, boolean defaultValue) {
      String str = getProperty(prop);
      if (str == null) return defaultValue;
      return str.equalsIgnoreCase("true");
    }

    /** Returns true seconds from default date formatted property String value;
     * returns Double.NaN if the property is not defined or value is not a parseable date formatted String.*/ 
    public double getTrueSeconds(String property) {
        return getTrueSeconds(property, EpochTime.DEFAULT_FORMAT);
    }

    /** Returns true seconds from date input formatted property String value;
     * returns Double.NaN if the property is not defined or value is not a parseable date formatted as input fmt String.*/ 
    public double getTrueSeconds(String property, String fmt) {
        String str = getProperty(property);
        if (str == null) return Double.NaN;

        double dd = Double.NaN; 
        try {
          dd = LeapSeconds.stringToTrue(str, fmt);
        }
        catch (Exception ex) {
          System.err.println("GenericPropertyList getTrueSeconds("+property+","+fmt+")="+str+" <-"+ex.getMessage());
        }
        return dd;
    }

    /**
     * Set a time property value to default date formatted string conversion of the input true seconds value. 
     */
    public void setTrueSeconds(String property, double trueSecs) {
        put(property, LeapSeconds.trueToString(trueSecs));
    }

    /**
     * Set a time property value to date string input format pattern conversion of the input true seconds value. 
     */
    public void setTrueSeconds(String property, double trueSecs, String fmt) {
        put(property, LeapSeconds.trueToString(trueSecs, fmt));
    }


    /**
     * Parse the given property as a date/time string and return a DateTime object.
     * Returns current time if property value is undefined or its value equals "*".
     */
    public DateTime getDateTime(String property) {
        return getDateTime(property, null);
    }

    public DateTime getDateTime(String property, String fmt) {
        String str = getProperty(property,""); // default to empty string for test below
        if (str.equals("*")) return new DateTime(); // current UTC time
        // constructors that parse datetime string
        return (fmt == null) ? new DateTime(str) : new DateTime(str, fmt);
    }

    /**
     * Set a date/time property. Insures string format is parseable
     */
    public void setDateTime(String property, DateTime dt) {
        put(property, dt.toString());
    }

    /**
     * Set a date/time property. Only allows "*".
     */
    public void setDateTime(String property, String str) {
        if (str.equals("*")) put(property, str);
    }

    /**
     * Parse the given property as a range (pair of doubles).
     * Returns null if property is not defined.
     */
    public IntegerRange getIntegerRange(String property)
    {
        String str = getProperty(property);
        if (str == null) return null;
        StringTokenizer tok = new StringTokenizer(str, TOKEN_DELIM);
        if (tok.countTokens() < 2)
          throw new PropertyFormatException(property+" = "+str+" too few tokens, need 2 for valid range");
        return new IntegerRange(Integer.valueOf(tok.nextToken()), Integer.valueOf(tok.nextToken()));
    }

    /**
     * Parse the given property as a range (pair of longs).
     * Returns null if property is not defined.
     */
    public LongRange getLongRange(String property)
    {
        String str = getProperty(property);
        if (str == null) return null;
        StringTokenizer tok = new StringTokenizer(str, TOKEN_DELIM);
        if (tok.countTokens() < 2)
          throw new PropertyFormatException(property+" = "+str+" too few tokens, need 2 for valid range");
        return new LongRange(Long.valueOf(tok.nextToken()), Long.valueOf(tok.nextToken()));
    }

    /**
     * Parse the given property as a range (pair of doubles).
     * Returns null if property is not defined.
     */
    public DoubleRange getDoubleRange(String property)
    {
        String str = getProperty(property);
        if (str == null) return null;
        StringTokenizer tok = new StringTokenizer(str, TOKEN_DELIM);
        if (tok.countTokens() < 2)
          throw new PropertyFormatException(property+" = "+str+" too few tokens, need 2 for valid range");
        return new DoubleRange(Double.valueOf(tok.nextToken()), Double.valueOf(tok.nextToken()));
    }

    /**
     * Parse the given property as a range (pair of floats).
     * Returns null if property is not defined.
     */
    public FloatRange getFloatRange(String property)
    {
        String str = getProperty(property);
        if (str == null) return null;
        StringTokenizer tok = new StringTokenizer(str, TOKEN_DELIM);
        if (tok.countTokens() < 2)
          throw new PropertyFormatException(property+" = "+str+" too few tokens, need 2 for valid range");
        return new FloatRange(Float.valueOf(tok.nextToken()), Float.valueOf(tok.nextToken()));
    }

    private StringTokenizer getStringTokenizer(String prop) {
       StringTokenizer st = new StringTokenizer(getProperty(prop,""), TOKEN_DELIM);
       return st;
    }

    /** Return an array of strings for property value.
     * The property string should be a list delimited by ", \t\n\r\f"
     * Returns empty StringList if property is not defined.
     */
    public StringList getStringList(String prop) {
      return new StringList(getProperty(prop));
    }

    /** Parse the given property as an array by parsing the boolean
     * equivalent of (true,false) string tokens.
     * Returns empty array if property is not defined.
     * */
    public final boolean[] getBooleanArray(String prop) {
       StringTokenizer st = getStringTokenizer(prop);
       int tokenCount = st.countTokens();
       boolean [] tokens = new boolean [tokenCount];
       if (tokenCount == 0) return tokens;
       try {
         for (int idx = 0; idx < tokenCount; idx++) {
           tokens[idx] = st.nextToken().equalsIgnoreCase("true");
         }
       }
       catch (NumberFormatException ex) {
         System.err.println(ex.toString());
       }
       return tokens;
    }

    /** Return an array of strings for property value.
     * Returns empty array if property is not defined.
     */
    public final String[] getStringArray(String prop) {
       ArrayList list = new ArrayList();
       StringTokenizer st = getStringTokenizer(prop);
       while (st.hasMoreTokens()) {
           list.add(st.nextToken());
       }
       return (String []) list.toArray(new String[list.size()]);
    }

    /** Returns the space-delimited concatenation of the toString() values of the list elements.*/
    public static final String toPropertyString(List aList) {
      return toPropertyString((Object []) aList.toArray());
    }

    /** Returns the space-delimited concatenation of the toString() values of the array elements.*/
    public static final String toPropertyString(Object [] array) {
       if (array == null) return "";
       int count = array.length;
       final StringBuilder sb = new StringBuilder(256);
       for (int idx = 0; idx < count; idx++) {
               sb.append(array[idx].toString()).append(SEP_CHAR);
       }
       return sb.toString();
    }

    /** Return an array of int for property value.
     * Returns empty array if property is not defined.
     */
    public final int[] getIntArray(String prop) {
       StringTokenizer st = getStringTokenizer(prop);
       int tokenCount = st.countTokens();
       int [] tokens = new int [tokenCount];
       if (tokenCount == 0) return tokens;
       try {
         for (int idx = 0; idx < tokenCount; idx++) {
           tokens[idx] = Integer.parseInt(st.nextToken());
         }
       }
       catch (NumberFormatException ex) {
         System.err.println(ex.toString());
       }
       return tokens;
    }
    /** Return an array of long for property value.
     * Returns empty array if property is not defined.
     */
    public final long[] getLongArray(String prop) {
       StringTokenizer st = getStringTokenizer(prop);
       int tokenCount = st.countTokens();
       long [] tokens = new long [tokenCount];
       if (tokenCount == 0) return tokens;
       try {
         for (int idx = 0; idx < tokenCount; idx++) {
           tokens[idx] = Long.parseLong(st.nextToken());
         }
       }
       catch (NumberFormatException ex) {
         System.err.println(ex.toString());
       }
       return tokens;
    }
    /** Return an array of double for property value.
     * Returns empty array if property is not defined.
     */
    public final double[] getDoubleArray(String prop) {
       StringTokenizer st = getStringTokenizer(prop);
       int tokenCount = st.countTokens();
       double [] tokens = new double [tokenCount];
       if (tokenCount == 0) return tokens;
       try {
         for (int idx = 0; idx < tokenCount; idx++) {
           tokens[idx] = Double.parseDouble(st.nextToken());
         }
       }
       catch (NumberFormatException ex) {
         System.err.println(ex.toString());
       }
       return tokens;
    }
    /** Return an array of float for property value.
     * Returns empty array if property is not defined.
     */
    public final float[] getFloatArray(String prop) {
       StringTokenizer st = getStringTokenizer(prop);
       int tokenCount = st.countTokens();
       float [] tokens = new float [tokenCount];
       if (tokenCount == 0) return tokens;
       try {
         for (int idx = 0; idx < tokenCount; idx++) {
           tokens[idx] = Float.parseFloat(st.nextToken());
         }
       }
       catch (NumberFormatException ex) {
         System.err.println(ex.toString());
       }
       return tokens;
    }

   /**
    * Return property whose values is RGB hexadecimal number string as a Color.
    * Returns null if property is undefined.
    */
    public Color getColor(String property) {
        String rgbStr = getProperty(property);
        if (rgbStr == null || rgbStr.equals("")) return null;
        Color color = null;
        try {
          // Has to be Long not Integer parse if input has Alpha bits FF prefixing string RRGGBB
          color = (rgbStr.length() > 6) ?
              new Color((int)Long.parseLong(rgbStr.toUpperCase(),16), true): new Color((int)Long.parseLong(rgbStr.toUpperCase(),16));
        }
        catch (NumberFormatException ex) {
             System.err.println(getClass().getName() + " getColor(" + property + ") " + ex.toString());
        }
        return color;
    }

    public String getUserFileNameFromProperty(String propName) {
        String fileName = getProperty(propName, null);
        if (fileName == null) return null;
        //crude check if name is relative or absolute:
        //if (fileName.lastIndexOf(FILE_SEP) == -1) { // no user path
        if (! (new File(fileName)).isAbsolute()) {
          String pfile = null;
          if (userPropertiesFileName != null) {
              try {
                pfile = new File(userPropertiesFileName).getCanonicalFile().getParent();
              }
              catch (IOException ex) {
              }
          }
          if (pfile != null) {
            final StringBuilder sb = new StringBuilder(256);
            sb.append(pfile).append(FILE_SEP).append(fileName);
            fileName = sb.toString();
          }
          else {
            final StringBuilder sb = new StringBuilder(256);
            sb.append(getUserFilePath()).append(FILE_SEP).append(fileName);
            fileName = sb.toString();
          }
        }
        return fileName;
    }

    /** Check validity of a runtime loaded class, checks access, existance, but doesn't initialize. */
    public final boolean checkForRuntimeClassOf(String propName) {
      String className = getProperty(propName);
      if (className == null) return false;
      try {
        Class.forName(className, false, getClass().getClassLoader());
      } catch (ClassNotFoundException ex) {
        System.err.println("ERROR: Bad runtime class name: "+className);
        System.err.println("Check '"+propName+"' property in:");
        System.err.println(userPropertiesFileName); // the user path name
        System.err.println(defaultPropertiesFileName); // the default path name
        return false;
      }
      return true;
    }

    /** Create and return an instance of the class named by property String, else return null.
     * Convenience wrapper.
     * */
    public final Object getInstanceDefinedByProperty(String propName) {
      String className = getProperty(propName);
      return (className == null) ? null : JasiObject.newInstance(className);
    }

    /**
    * Prints string list all properties; useful for debugging.
    */
    public void dumpProperties() {
        System.out.println(toDumpString());
        /*
        System.out.println(Lines.ANGLE_L_TEXT_LINE);
        System.out.println("Listing of properties subclass: " + getClass().getName());
        System.out.println("  defaultPropertiesFileName: " + defaultPropertiesFileName);
        System.out.println("  userPropertiesFileName: " + userPropertiesFileName);
        System.out.println("  filename: " + filename);
        System.out.println("  filetype: " + filetype);
        System.out.println(Lines.CARET_TEXT_LINE);
        System.out.println("---------------> Properties <---------------");
        System.out.print(listToString());
        System.out.println("------------> End of Properties <-----------");
        System.out.println(Lines.ANGLE_R_TEXT_LINE);
        */
    }

    public String toDumpString() {
        final StringBuilder sb = new StringBuilder(8192);
        sb.append(Lines.ANGLE_L_TEXT_LINE);
        sb.append("\n");
        sb.append("Listing of properties subclass: " + getClass().getName());
        sb.append("\n");
        sb.append("  defaultPropertiesFileName: " + defaultPropertiesFileName);
        sb.append("\n");
        sb.append("  userPropertiesFileName: " + userPropertiesFileName);
        sb.append("\n");
        sb.append("  filename: " + filename);
        sb.append("\n");
        sb.append("  filetype: " + filetype);
        sb.append("\n");
        sb.append(Lines.CARET_TEXT_LINE);
        sb.append("\n");
        sb.append("---------------> Properties <---------------");
        sb.append("\n");
        sb.append(listToString());
        sb.append("\n");
        sb.append("------------> End of Properties <-----------");
        sb.append("\n");
        sb.append(Lines.ANGLE_R_TEXT_LINE);
        sb.append("\n");
        return sb.toString();
    }

    /**
     * Check if property should be hidden from logs and UI
     */
    protected boolean isHideProperty(String propName) {
        boolean retValue = false;
        if (propName.equals(PropConstant.DB_PASSWD)) {
            retValue = true;
        }
        return retValue;
    }

} // end of class
