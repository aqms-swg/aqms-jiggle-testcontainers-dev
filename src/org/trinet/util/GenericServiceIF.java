package org.trinet.util;
public interface GenericServiceIF {

    public String getServiceName();
    public void setServiceName(String name);

    public void setServiceByProperties(String propertyFileName);

    public void setServer(String ipAddress, int port);

    public int getPort();
    public void setPort(int port);

    public String getIPAddress();
    public void setIPAddress(String ipAddress);

    public void setServerTimeoutMillis(int timeout);
    public int getServerTimeoutMillis();

    public String describeConnection();

    public boolean connect();
    public boolean disconnect();

}
