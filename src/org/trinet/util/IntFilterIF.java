package org.trinet.util;

public interface IntFilterIF extends FilterIF {
/** Return reference to input object parameter which contains a representation of a waveform time series.
* of input and output objects.
*/
    int [] filter(int [] sampleSeries);
}
