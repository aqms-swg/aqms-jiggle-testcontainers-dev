package org.trinet.util;
/**
 * General purpose range (pair) of integer values
*/
public class IntegerRange extends NumberRange {

    public IntegerRange(int min, int max) {
	super(Integer.valueOf(min), Integer.valueOf(max));
    }

    public IntegerRange(Integer min, Integer max) {
	super(min, max);
    }

/** Returns true if both the minimum and maximum bound values == NullValueDb.NULL_INT.
* @see org.trinet.jdbc.NullValueDb 
*/
    public boolean isNull() {
        return ( min.equals( Integer.valueOf(org.trinet.jdbc.NullValueDb.NULL_INT)) &&
                 max.equals( Integer.valueOf(org.trinet.jdbc.NullValueDb.NULL_INT)) );
    }

/** Sets the minimum and maximum bounds == NullValueDb.NULL_INT.
* @see org.trinet.jdbc.NullValueDb 
*/

    public void setNull() {
        setMin(org.trinet.jdbc.NullValueDb.NULL_INT);
        setMax(org.trinet.jdbc.NullValueDb.NULL_INT);
    }

/**
 * Returns the min value of this range.
*/
    public int getMinValue() {
	return getMin().intValue();
    }

/**
 * Returns the max value of this range.
*/
    public int getMaxValue() {
	return getMax().intValue();
    }

/**
 * Sets the min value of this range.
*/
    public void setMin(int min) {
	setMin(Integer.valueOf(min));
    }

/**
 * Sets the max value of this range.
*/
    public void setMax(int max) {
	setMax(Integer.valueOf(max));
    }

/**
 * Sets the max, min values of this range.
*/
    public void setLimits(int min, int max) {
	setLimits(Integer.valueOf(min), Integer.valueOf(max));
    }

    public void include(int val) {
	include(Integer.valueOf(val));
    }

/**
 * Adjust current range to include range in argument
*/
    public void include(IntegerRange range) {
        include((NumberRange) range);
    }

/**
* Return true if number is within specified bounds inclusive.
*/
    public boolean contains(int val) {
        return contains(Integer.valueOf(val));
    }

/**
* Return true if range is within this range inclusive.
*/
    public boolean contains(IntegerRange range) {
	return contains((NumberRange) range);
    }

/**
* Returns the difference between the upper and lower range bounds.
*/
    public int size() {
	return (int) longExtent();
    }

/**
* Returns min + "," + max.
*/
    public String toString() {
        return min + "," + max;
    }

/*
    public static void main(String [] args) {

         System.out.println("++++++++ BEGIN TEST ++++++++++");

         IntegerRange dr = new IntegerRange(-10, 10);

         IntegerRange dr2 = new IntegerRange(-20, 20);

         FloatRange dr3 = new FloatRange(-20.f, 20.f);

         dr.dump(dr2, dr3);
         dr3.setLimits(1.f,21.f);
         dr.dump(dr2, dr3);

         dr.dump1(-11);
         dr.dump1(-1);
         dr.dump1(0);
         dr.dump1(1);
         dr.dump1(11);

         dr.dump2(dr2);

         System.out.println("Test dr.setLimits(-14, 14)");
         dr.setLimits(-14,14);
         dr.dump2(dr2);

         System.out.println("Test dr.include(-16, 16)");
         dr.include(-16);
         dr.include(16);
         dr.dump2(dr2);

         System.out.println("Test dr.include(dr2)");
         dr.include(dr2);
         dr.dump2(dr2);

         System.out.println("Test dr.setMax(0); dr2.setMin(0)");
         dr.setMax(0);
         dr2.setMin(0);
         dr.dump2(dr2);

         System.out.println("Test dr.setMax(0); dr2.setMin(1)");
         dr.setMax(0);
         dr2.setMin(1);
         dr.dump2(dr2);

    }

    public void dump(NumberRange dr2, NumberRange dr3) {
         System.out.println("Test equalsRange: ");
         System.out.println("     dr2.toString(): " + dr2.toString());
         System.out.println("     dr3.toString(): " + dr3.toString());
         System.out.println("     dr2.equalsRange(dr3) : " +  dr2.equalsRange(dr3));
         System.out.println("------------------\n");
    }

     public void dump1(int number) {
         Integer val = Integer.valueOf(number);
         System.out.println("Dump Range bounds, size: " + getMinValue() + ", " + getMaxValue() + " size(): " + size());
         System.out.println(" Test input val: " + val);
         System.out.println(" after(val)    : " + after(val));
         System.out.println(" before(val)   : " + before(val));
         System.out.println(" excludes(val) : " + excludes(val));
         System.out.println(" contains(val) : " + contains(val));
         System.out.println("------------------\n");
    }

    public void dump2(IntegerRange dr2) {
         System.out.println("Dump Range, number : " + getMinValue() + ", " + getMaxValue() + " size(): " + size());
         System.out.println(" Test Range2 Min,max: " + dr2.getMinValue() + ", " + dr2.getMaxValue() );

         System.out.println(" within(dr2)       : " + within(dr2));
         System.out.println(" dr2.within(this)  : " + dr2.within(this)); 

         System.out.println(" overlaps(dr2)     : " + overlaps(dr2));
         System.out.println(" dr2.overlaps(this): " + dr2.overlaps(this));

         System.out.println(" dr2.after(this)   : " + dr2.after(this));
         System.out.println(" after(dr2)        : " + after(dr2));

         System.out.println(" dr2.before(this)  : " + dr2.before(this));
         System.out.println(" before(dr2)       : " + before(dr2));

         System.out.println(" dr2.excludes(this): " + dr2.excludes(this));
         System.out.println(" excludes(dr2)     : " + excludes(dr2));

         System.out.println(" dr2.contains(this): " + dr2.contains(this));
         System.out.println(" contains(dr2)     : " + contains(dr2));
         System.out.println("------------------\n");

    }
*/
}
