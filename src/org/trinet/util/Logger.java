package org.trinet.util;

import java.io.*;
import java.util.*;

import org.trinet.util.GenericPropertyList;

/**
 * Simple handling of log files. Allows redirection of System.out and System.err
 * to a log file.
 * Closed out logs will be renamed with a timestamp in the name reflecting the time
 * they were closed.
 * @author Doug Given
 */

public class Logger {

  protected static String filename = null;
  protected static File currentFile = null;
  protected static PrintStream printStream = null;

  public static String TIMESTAMP_ZONE = TimeZone.getDefault().getID();
  public static String TIMESTAMP_PATTERN = "yyyy-MM-dd_HHmmss";

  protected static ThreadScheduler rotSched = null ;
  protected static RotationThread rotThread  = new RotationThread();

  protected static GenericPropertyList appProps;

  public Logger() { }


/** Redirect all System.out and System.err printing to this log file.
 * Specify path as well: example "/home/workingdir/logs/mytask.log". */
    public static void setFilename(String name) {
        setFilename(name, TIMESTAMP_ZONE);
    }

    public static void setFilename(String name, String timeZone) {

      filename = name;

      try {
        currentFile = new File(filename);

        // rename to preserve previous file if it already exists
        if (currentFile.exists()) {
          String ts = getTimeStampString(TIMESTAMP_ZONE);
          String oldFile = filename;
          String ext = "";
          int idx = filename.lastIndexOf(".");
          if ( idx > 0) {
              oldFile = filename.substring(0,idx);
              ext = filename.substring(idx);
          }
          oldFile = oldFile + "_" + ts + ext;
          currentFile.renameTo(new File(oldFile));
        }

        printStream = new PrintStream(new FileOutputStream(currentFile));

        System.setOut(printStream);
        System.setErr(printStream);
        // What about Benchmark printstream ? 
        // BenchMark.setPrintStream(printStream);

        printStream.println("** Logger opened log file at "+ EpochTime.dateToString(new Date()) ); // date at GMT -aww 2008/02/13

        // If set, dump application properties at top of new log
        if ( appProps != null && appProps.getBoolean("dumpProperties") ) appProps.dumpProperties();

      } catch(IOException e) {
        e.printStackTrace();    // no such dir, can't open, etc.
      }
    }

    public static String getFilename() {
      return filename;
    }

    public static void setAppProps(GenericPropertyList props) {
        appProps = props;
    }

    public static boolean touchLogFile() {
        return ( currentFile == null ) ? true : currentFile.setLastModified(new Date().getTime());
    }

/** Close current alogfile. Old log will be given an unique name by
  * appending a time stamp to it. For example: /home/workingdir/logs/mytask.log
  * becomes /home/workingdir/logs/mytask.log040326190016 */
    public static void closeLogFile() { // added method -aww 2009/09/01
      if (printStream == null) return;
      printStream.println("** Logger closing out log file at "+ EpochTime.dateToString(new Date()) ); // date at GMT -aww 2008/02/13
      printStream.close();
      printStream = null;
    }

/** Close current and open new logfile. Old log will be given an unique name by
  * appending a time stamp to it. For example: /home/workingdir/logs/mytask.log
  * becomes /home/workingdir/logs/mytask.log040326190016 */
    public static void rotateLogFile() {
      if (printStream == null) {
          System.out.println("** Logger no print stream exists for " + filename + " at "+ EpochTime.dateToString(new Date()) );
      }
      else {
          printStream.println("** Logger closing out log file at "+ EpochTime.dateToString(new Date()) ); // date at GMT -aww 2008/02/13
          printStream.close();
      }
      if (filename != null) setFilename(filename);
    }

/** Return a terse local timestamp string of the form "yyyy-MM-dd-HHmmss".
 * Example: "2010-01-03_232353" 
 */
    static String getTimeStampString() {
      return getTimeStampString(TIMESTAMP_ZONE); // default is local time zone
    }

/** Return a timestamp string of the form "yyyy-MM-dd-HHmmss" in specified time zone id.
 */
    static String getTimeStampString(String zone) {
      return EpochTime.dateToString(new Date(), TIMESTAMP_PATTERN, zone); // input time zone
    }

    /** Set rotation interval using a string that describes the units of the interval.
     * For example: "1200s" = 1200 seconds, "1d" = 1 day. A space between number & letter
     * is allowed: e.g. "3600 s". Case does not matter: "3600 s" = "3600 S".
     * Allowed qualifiers are: s = seconds, m = minutes, h = hours, d = days, w = weeks.
     * If there is no qualifier, seconds is assumed.
     * An illegal string will result in an error message and no log rotation. */
    public static void setRotationInterval(String instr) {

      String  str = instr.trim();  // trim whitespace

      int seconds = 0;
      int numberLen = str.length();

      char lastChar = str.charAt(numberLen-1);

      if (Character.isLetter(lastChar)) {
        numberLen--;
        lastChar = Character.toUpperCase(lastChar);
      } else {
        lastChar = 'S';   // default
      }

      try {
        // get the number part of the string - trim() allows space between number & letter
        seconds = Integer.parseInt(str.substring(0,numberLen).trim());
      }
      catch (NumberFormatException ex) {
        System.err.println ("Bad log rotation specification - "+ str);
        return;
      }

      if (lastChar == 'S') {
        // noop
      } else if (lastChar == 'M') {
        seconds *= 60;
      } else if (lastChar == 'H') {
        seconds *= 60 * 60;
      } else if (lastChar == 'D') {
        seconds *= 60 * 60 * 24;
      } else if (lastChar == 'W') {
        seconds *= 60 * 60 * 24 * 7;
      } else {
        System.err.println("Bad log rotation specification - "+ str);
        return;
      }
      System.out.println("Logger setRotationInterval " + seconds + " seconds");
      if (rotSched != null) rotSched.stop();        // stop old thread if any
      boolean isDaemon =true; // set daemon true, sudden death when main exits
      rotSched = new ThreadScheduler(rotThread, seconds, isDaemon);
      //rotSched.start();   // Thread creation starts itself
    }

    /** Automatically rotates logs every 'seconds' seconds.
     * 1 day = 24 * 60 * 60*/
    public static void setRotationInterval(int seconds) {
      if (rotSched != null) rotSched.stop();        // stop old thread if any
      boolean isDaemon =true; // set daemon true, sudden death when main exits
      rotSched = new ThreadScheduler(rotThread, seconds, isDaemon);
      //rotSched.start();   // Thread creation starts itself
    }

    public static int getRotationInterval() {
      if (rotSched != null) {
        return rotSched.getStandardInterval();
      } else {
        return 0;
      }
    }

    /** This is a one-shot, self-starting thread that associates amps in background.  */
    static class RotationThread implements Runnable {
        public void run() {
            Logger.rotateLogFile();
        }
    }

    static PrintStream getPrintStream() {
        return printStream;
    }
/*
  public static void main(String[] args) {

// test interval format parsing
    Logger.setRotationInterval("123s");
    Logger.setRotationInterval("123m");
    Logger.setRotationInterval("123h");
    Logger.setRotationInterval("123d");
    Logger.setRotationInterval("123 d");
    Logger.setRotationInterval("123x");
    Logger.setRotationInterval("123");

    //String file = "~/test.log";    // Unix style
    String file = "C:/temp/loggertest.log";    // PC style

    // test rotation (60 secs)
    int interval = 60;

    if (args.length > 0) {
      file = args[0]; // override filename
    }
    if (args.length > 1) { // override interval
      Integer val = Integer.valueOf(args[1]);
      interval = (int) val.intValue();
    }

    System.out.println("Logger log file = "+file+ "  interval = "+interval);
    Logger.setFilename(file);
    Logger.setRotationInterval(interval);
    System.out.println("Starting at ... "+ Logger.getTimeStampString());
    System.out.println("File: "+ Logger.getFilename());
    System.out.println("Interval: "+ Logger.getRotationInterval());
    try {
      Thread.sleep(1000);
    }
    catch (InterruptedException ex) {
    }

    org.trinet.util.BenchMark bm = new org.trinet.util.BenchMark();
    while (bm.getSeconds() < 2.*interval) { // cycle through 2 files
      String str = EpochTime.epochToString(EpochTime.currentEpoch());
      System.out.println("Logger out "+str);
      System.err.println("Logger err "+str);
      try {
        Thread.sleep(10000);
      } catch (InterruptedException ex) { }
    }
    // close last opened file
    Logger.closeLogFile();
    Logger.setFilename(file);

    System.exit(0);

  }
*/
}
