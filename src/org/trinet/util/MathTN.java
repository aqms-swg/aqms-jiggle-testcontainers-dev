package org.trinet.util;

/**
* Provide math functions left out of Java like LOG10 !
 */

public class MathTN {
      /**
     * Java does not provide a function for log(base10). This is how you
     * do it. (see Que, Using Java 1.1, pg. 506).
     */
    public static double log10 (double val) {
      final double logOf10 = Math.log(10.0);
      // should it throw exception or return NaN for 0 or NaN input arg value?
      return (val > 0) ? Math.log(val)/logOf10 : 0.;

    }
}
