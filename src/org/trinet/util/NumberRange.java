package org.trinet.util;

import java.io.*;
import org.trinet.jdbc.*;

public class NumberRange extends Object implements NumberRangeIF,
        Cloneable, Comparable, Serializable {

/**  Minimum value bound. */
    protected Number min;

/** Maximum value bound. */
    protected Number max;

/** Constructor, null bounds. */
    public NumberRange() {
        this(null, null);
    }

/** Constructor creates minimum and maximum bounds from the input object values.
* @exception java.lang.IllegalArgumentException bounds have different class types or isReversed == true.
*/
    public NumberRange(Number min, Number max) {
        super();
        if (min != null && max != null) {
            if (min.getClass() !=  max.getClass())
                throw new IllegalArgumentException("NumberRange min,max must be same class type.");
            if ( ((Comparable) min).compareTo(max) > 0) 
                throw new IllegalArgumentException("NumberRange min-max bounds reversed.");
        }
        this.min = min;
        this.max = max;
        setHashValue();
    }

/**
* Subclasses can override this method to return true if both the minimum and maximum bound values
* are equivalent to a defined unbounded value.
*/
    public boolean isUnlimited() {
        return ! isLimited();
    }

    public boolean isLimited() {
        return hasMinLimit() || hasMaxLimit() ;
    }

/** Check for a minimum limit.
*/
    public boolean hasMinLimit() {
        return (min != null) ;
    }

/** Check for a maximum limit.
*/
    public boolean hasMaxLimit() {
        return (max != null) ;
    }

    public void nullMinLimit() {
        min = null;
        setHashValue();
    }

    public void nullMaxLimit() {
        max = null;
        setHashValue();
    }

/** Subclasses can override this method to set the minimum and maximum bounds to a defined unbounded value.
* @see org.trinet.jdbc.NullValueDb 
*/
    public void unsetLimits() {
        nullMinLimit();
        nullMaxLimit();
    }

/** Returns the minimum bound. */
    public Number getMin() {
        return min;
    }

/** Returns the maximum bound. */
    public Number getMax() {
        return max;
    }

/** Returns the size of the range as a double. */
    public double doubleExtent() {
        return (hasMinLimit() && hasMaxLimit()) ? max.doubleValue() - min.doubleValue() : Double.POSITIVE_INFINITY;
    }

/** Returns the size of the range as a long. */
    public long longExtent() {
        return (hasMinLimit() && hasMaxLimit()) ? max.longValue() - min.longValue() : Long.MAX_VALUE;
    }

/** True if minimum bound of this instance is greater than the input value. */
    public boolean after(Number number) {
        if (number == null) return true;
        return (hasMinLimit()) ? (min.doubleValue() > number.doubleValue()) : false;
    }

/** True if minimum bound of this instance is greater than the input range's max value. */
    public boolean after(NumberRange range) {
        return (range == null) ? false : after(range.max); 
    }

/** True if maximum bound of this instance is less than the input value. */
    public boolean before(Number number) {
        if (number == null) return true;
        return (hasMaxLimit()) ? (max.doubleValue() < number.doubleValue()) : false;
    }

/** True if maximum bound of this instance less than mininum bound of input range. */
    public boolean before(NumberRange range) {
        return (range == null) ? false : before(range.min); 
    }

/** True if this object's range excludes input number. */
    public boolean excludes(Number number) {
        return ! contains(number);
    }

/** True if this object's range excludes input object's range. */
    public boolean excludes(NumberRange range) {
        return (range == null) ? true : ! overlaps(range.min, range.max);
    }

/** True if this object's range excludes input range. */
    public boolean excludes(Number min, Number max) {
        return ! overlaps(min, max);
    }

/** True if this object's range contains input object's range. */
    public boolean contains(NumberRange range) {
        return (range == null) ? false : contains(range.min, range.max); 
    }

/** True if this object's range contains input range. */
    public boolean contains(Number min, Number max) {
        return ( contains(min) && contains(max) );
    }

/** True if this object's range contains input number. */
    public boolean contains(Number number) {
        return ! ( after(number) || before(number) );
    }

/** True if this object's range overlaps input object's range. */
    public boolean overlaps(NumberRange range) {
        return (range == null) ? false : overlaps(range.min, range.max);
    }

/** True if this object's range overlaps input range. */
    public boolean overlaps(Number min, Number max) {
        return ! (after(max) || before(min) );
    }

/** True if this object's range lies within input object's range. */
    public boolean within(NumberRange range) {
        return (range == null) ? false : range.contains(this); 
    }

/** True if this object's range lies within input range. */
    public boolean within(Number min, Number max) {
        if (min != null) {
            if ( ! hasMinLimit() || (((Comparable) min).compareTo(this.min) > 0) )  return false;
        }

        if (max == null) return true;
        else if (! hasMaxLimit() ) return false;

        return ( ((Comparable) max).compareTo(this.max) > -1) ;
    }

/** Returns a "deep copy" of this instance. */
    public Object clone() {
        NumberRange range = null;
        try {
            range = (NumberRange) super.clone();
        }
        catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
        return range;
    }

/** Returns true only if the input object is an instance of NumberRange and
* its minimum and maximum bounds are equivalent in class type and value to this object's.
*/
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || ( getClass() != object.getClass()) ) return false;

        NumberRange range = (NumberRange) object;

        if (hasMinLimit()) {
            if (! range.hasMinLimit() || ! range.min.equals(min)) return false;
        }
        else if (range.hasMinLimit()) return false;

        if (hasMaxLimit()) {
            if (! range.hasMaxLimit() || ! range.max.equals(max)) return false;
        }
        else if (range.hasMaxLimit()) return false;

        return true;
    }

/** Returns true only if the doubleValue()'s of the input bounds are equivalent to this object's.
*/
    public boolean equalsRange(NumberRange range) {
        if (this == range) return true;
        if (range == null) return false;

        if (hasMinLimit()) {
            if ( ! range.hasMinLimit() ||  min.doubleValue() != range.getMin().doubleValue() ) return false;
        }
        else if (range.hasMinLimit()) return false;

        if (hasMaxLimit()) {
            if ( ! range.hasMaxLimit() ||  max.doubleValue() != range.getMax().doubleValue() ) return false;
        }
        else if (range.hasMaxLimit()) return false;

        return true;
    }

/** Input object must in an instance of NumberRange. 
* @return <pre>
* -1 this object's minimum value is less than the input object's value,
*    or it is equivalent and this object's maximum value is less than the input object's value.<br>
*  0 this object's minimum and maximum values are equivalent to the input object's values.<br>
*  1 this object's minimum value is greater than the input object's value,
*    or it is equivalent and its maximum value is greater than the input object's value.
* </pre>
* @exception java.lang.NullPointerException input object is null
* @exception java.lang.ClassCastException input object is not an instance of NumberRange.
*/
    public int compareTo(Object object) {
        NumberRange range = (NumberRange) object;
        if (hasMinLimit()) {
            if (range.hasMinLimit()) {
                int retVal = ((Comparable) min).compareTo(range.min);
                if (retVal != 0) return retVal;
            }
            else return 1;
        }
        else if (range.hasMinLimit()) return -1;

        if (hasMaxLimit())
            return (range.hasMaxLimit()) ? ((Comparable) max).compareTo(range.max) : -1;
        else if (range.hasMaxLimit()) return 1;
        return 0;
    }

/** Returns true if max < min. */
    public boolean isReversed() {
        return (! hasMinLimit() || ! hasMaxLimit()) ? false : ( ((Comparable) min).compareTo(max) > 0);
    }

/** Returns false if isReversed() */
    public boolean isValid() {
        return ! isReversed() ;
    }

/** Returns String concatenation of the minimum and maximum bounds values separated by ",".
*/
    public String toString() {
        StringBuffer sb = new StringBuffer(80);
        sb.append(min.toString());
        sb.append(",");
        sb.append(max.toString());
        return sb.toString();
    }

/** Returns String concatenation of minimum and maximum bound values separated by ", ".
* Inserts "NULL" if bound == null or its value is considered equivalent to NULL.
*/
    public String toStringSQL() {
        StringBuffer sb = new StringBuffer(80);
        sb.append(StringSQL.valueOf(min));
        sb.append(", ");
        sb.append(StringSQL.valueOf(max));
        return sb.toString();
    }

/** Convenience wrapper System.out.println(toString()). */
    public void print() {
        System.out.println(toString());
    }

/** 
* Sets the minimum bound to the input.
*/
    public void setMin(Number min) {
        if (min != null &&  this.max != null) {
             if(min.getClass() !=  max.getClass())
                 throw new IllegalArgumentException("NumberRange setMin min,max must be same class type.");
        }
        this.min = min;
        setHashValue();
    }

/** 
* Sets the maximum bound to the input.
*/
    public void setMax(Number max) {
        if (max != null && this.min != null) {
             if(min.getClass() !=  max.getClass())
                 throw new IllegalArgumentException("NumberRange setMax min,max must be same class type.");
        }
        this.max = max;
        setHashValue();
    }

/** 
* Sets the minimum, maximum bounds of the range to the input values.
*/
    public void setLimits(Number min, Number max) {
        if (min != null && max != null) {
            if (min.getClass() !=  max.getClass())
                throw new IllegalArgumentException("NumberRange setLimits() min,max must be same class type.");
            if ( ((Comparable) min).compareTo(max) > 0) 
                throw new IllegalArgumentException("NumberRange setLimits() min-max bounds reversed.");
        }
        this.min = min;
        this.max = max;
        setHashValue();
    }

/** 
* Sets the range bounds to a of the input range bounds.
*/
    public void setLimits(NumberRange range) {
        setLimits(range.min, range.max);
    }

/**
* Sets the appropiate minimum or maximum bound to extend the range to include the input value.
* Does a no-op if contains(Number) == true.
*/
    public void include(Number number) {
//        if (number == null) return;
        if (number.getClass() !=  min.getClass())
             throw new IllegalArgumentException("NumberRange include() input must be same class type as bounds.");
        if (after(number)) min = number;
        if (before(number)) max = number;
        setHashValue();
    }

/**
* Sets the appropiate range bounds to the input range bounds.
* Does a no-op if contains(NumberRange) == true.
*/
    public void include(NumberRange range) {
//        if (range == null) return;

        if (hasMinLimit()) {
            if (! range.hasMinLimit()) min = null;
            else if (after(range.min)) min = range.min;
        }
        if (hasMaxLimit()) {
            if (! range.hasMaxLimit()) max = null;
            else if (before(range.max)) max = range.max;
        }
        setHashValue();
    }

    private Integer hashKey;
    public int hashCode() {
      if (hashKey == null) setHashValue();
      return hashKey.intValue();
    }
    protected final void setHashValue() {
      hashKey = Integer.valueOf(
        ( (min == null) ? 0 : Double.valueOf(min.doubleValue()).hashCode() ) +
        ( (max == null) ? 0 : Double.valueOf(max.doubleValue()).hashCode() )
      );
    }
}
