package org.trinet.util;

public class ParseString {
  public static final String jversion =  java.lang.System.getProperty("java.version");
  /**
   * Nanoseconds per second.
   */
  public static final long NANOS_PER_SECOND =  1000_000_000L;

  private ParseString() {};

/**
 * Convert the specified field in the string to a double. String starts at offset 0.
* Returns 0. if input is empty or blank String.
* Returns Double.Nan if a NumberFormatException occurs parsing input String.
*/ 
// 3/245/03 DDG -- added test for non-empty string to avoid exceptions
//  substr = str.substring(start, stop).trim();
//  if (substr.length() == 0) return 0.0f;

   public static double getDouble(String str, int start, int stop)
   { 
    String substr = "";
    double dval = Double.NaN;

    try
    {
        substr = str.substring(start, stop).trim();
        if (substr.length() == 0) return 0.;
        dval = Double.parseDouble(substr); 

    }
    catch (NumberFormatException exc)
    {
        System.err.println ("ParseString getDouble: Double format conversion error: " + substr);
    }
    catch  (StringIndexOutOfBoundsException e)
    {
        System.err.println (e.getLocalizedMessage());
        System.err.println ("ParseString getDouble: String out of bounds error for start, stop:" + start + ", " + stop + "\n"
         + "\"" + str + "\"");
    }
    return dval;
   }

   /**
    * Parse the specified text to a duration.
    * 
    * @param str      the text to parse which may be a floating point number of
    *                 seconds or in the ISO-8601 duration format.
    * @param duration the default duration.
    * @return the duration or the default duration if error.
    * @see java.time.Duration
    */
   public static java.time.Duration parseDuration(String str, java.time.Duration duration) {
       try {
           if (str != null && !str.isEmpty()) {
               // if text is in the ISO-8601 duration format
               if (str.charAt(0) == 'P') {
                   duration = java.time.Duration.parse(str);
               } else {
                   double value = Double.parseDouble(str);
                   long seconds = (long) value;
                   value -= seconds; // subtract the seconds
                   if (value == 0.0) { // if no fractional seconds
                       duration = java.time.Duration.ofSeconds(seconds);
                   } else {
                       // convert to nanoseconds
                       long nanoseconds = Math.round(value * NANOS_PER_SECOND);
                       duration = java.time.Duration.ofSeconds(seconds, nanoseconds);
                   }
               }
           }
       } catch (Exception ex) {
           System.err.println("parseDuration(" + str + "): " + ex.getMessage());
       }
       return duration;
   }

/**
 * Convert the specified field in the string to a double. String starts at offset 0.
* Returns 0.f if input is empty or blank String.
* Returns Float.Nan if a NumberFormatException occurs parsing input String.
*/ 
   public static float getFloat(String str, int start, int stop)
   { 
    String substr = "";
    float fval = Float.NaN;

    try
    {
        substr = str.substring(start, stop).trim();
        if (substr.length() == 0) return 0.f;
        fval = Float.parseFloat(substr); 
    }
    catch (NumberFormatException exc)
    {
        System.err.println ("ParseString getFloat: Float format conversion error: " + substr);
    }
    catch  (StringIndexOutOfBoundsException e)
    {
        System.err.println ("ParseString getFloat: String out of bounds error for start,stop:" + start + ", " + stop + "\n"
         + "\"" + str + "\"");
    }
    return fval;
   }

/**
 * Convert the specified field in the string to an long value. String starts at offset 0.
* Returns 0l if input is empty or blank String.
* Returns Long.MIN_VALUE if a NumberFormatException occurs parsing input String.
 */
   public static long getLong(String str, int start, int stop)
   { 
    String substr = "";
    long lval = Long.MIN_VALUE;
    try
    {
        substr = str.substring(start, stop).trim();
        if (substr.length() == 0 ) return 0l;
        lval = Long.parseLong(substr);
    }
    catch (NumberFormatException exc)
    {
        System.err.println ("ParseString getLong: Long format conversion error: /"+substr+"/");
    }
    catch (StringIndexOutOfBoundsException e)
    {
        System.err.println ("ParseString getLong: String out of bounds error for start,stop:" + start + ", " + stop + "\n"
         + "\"" + str + "\"");
    }
    return lval;
   }

/**
 * Convert the specified field in the string to an int value. String starts at offset 0.
* Returns 0 if input is empty or blank String.
* Returns Integer.MIN_VALUE if a NumberFormatException occurs parsing input String.
 */
   public static int getInt(String str, int start, int stop)
   { 
    String substr = "";
    int ival = Integer.MIN_VALUE;
    try
    {
        substr = str.substring(start, stop).trim();
        if (substr.length() == 0) return 0;
        ival = Integer.parseInt(substr);
    }
    catch (NumberFormatException exc)
    {
        System.err.println ("ParseString getInt: Integer format conversion error: /"+substr+"/");
    }
    catch (StringIndexOutOfBoundsException e)
    {
        System.err.println ("ParseString getInt: String out of bounds error for start,stop:" + start + ", " + stop + "\n"
         + "\"" + str + "\"");
    }
    return ival;
   }

/**
 * Convert the specified field in the string to a float value. String starts at offset 0.
* Returns 0.f if input is empty or blank String.
* Returns Float.Nan if a NumberFormatException occurs parsing input String.
 */
   public static float getIntToFloat (String str, int start, int stop, int scale)
   { 
    String substr = "";
    float fval = Float.NaN;

    try
    {
        substr = str.substring(start, stop).trim();
        if (substr.length() == 0) return 0.f;
        fval = Float.parseFloat(substr);
        fval = fval * (float) java.lang.Math.pow(10.,(double) -scale);    
    }
    catch (NumberFormatException exc)
    {
        System.err.println ("ParseString getIntToFloat: Float format conversion error: /"+substr+"/");
    } catch (StringIndexOutOfBoundsException e) {
        System.err.println ("ParseString getIntToFloat: String out of bounds error for start,stop:" + start + ", " + stop + "\n"
         + "\"" + str + "\"");
    }
    return fval;
   }

/**
 * Convert the specified field in the string to a char value. String starts at offset 0.
 */
   public static char getChar (String str, int start)
   { 
    char cdum = ' ';

    try
    {
        cdum = str.charAt(start);

    }
    catch (StringIndexOutOfBoundsException e)
    {
        System.err.println ("ParseString getChar: String out of bounds error for start:" + start + "\n" + "\"" + str + "\"");
    }
    return cdum;
   }

/**
 * Convert the specified field in the string to an String object. String starts at offset 0.
 */
   public static String getString (String str, int start, int stop)
   { 
    String substr = "";

    try
    {
        substr = str.substring(start, stop);

    }
    catch (StringIndexOutOfBoundsException e)
    {
        System.err.println ("ParseString getString: String out of bounds error for start,stop:" + start + ", " + stop + "\n"
         + "\"" + str + "\"");
    }
    return substr;
   }

/**
 * Always return blanks if Index out of bounds.
 */
    public String safeSubstring(String str, int p0, int p1)
    {
    try {
        return str.substring(p0, p1);
    } catch (StringIndexOutOfBoundsException e)
    {
        int len = p1 - p0;
        return "                                                     ".substring(0, len);
    }
    }

/** Return true if the string is blank.
 * That is, it contains only those characters that
 * would be removed by a call to String.trim(). This includes all whitespace
 * including all ASCII control characters.
 * @See: String.trim()
 * @See: Character.isSpace() */
  // Why do we need this method at all? aww
  public static boolean isBlank(String str) {
    return (str.trim().length() == 0);
  }
}
