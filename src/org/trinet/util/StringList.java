package org.trinet.util;
import java.io.Serializable;
import java.util.*;

/**
 * List of string elements such as those parsed from a String property value.
*/
public class StringList implements java.io.Serializable {

    /** List of String objects. */
    List stringList = null;

/** Make an empty StringList.*/
    public StringList() {
      this((String[])null);
    }
/** Create a StringList from this array of strings. */
    public StringList(String [] strings) {
        if (strings == null) stringList = new ArrayList(0);
        //else stringList = Arrays.asList(strings); // this is a "fixed" list backed by array you can't add/remove  -aww
        else stringList = new ArrayList(Arrays.asList(strings)); // this is an add/remove capable list -aww
    }

/** Create a StringList by parsing this delimited string. The delimeters are ", \t\n\r\f"*/
    public StringList(String delimitedString) {
      this(delimitedString, ", \t\n\r\f");
    }

/** Create a StringList by parsing this delimited string. The 'delim' string
 * contains the list of delimiters to use in parsing 'delimitedString'.
 * Returns an zero-length list if either argument string is null or blank.*/
    public StringList(String delimitedString, String delim) {
      if (delimitedString == null || delim == null) {
        stringList = new ArrayList(0);
      }
      else {
        parseValue(new StringTokenizer(delimitedString, delim));
      }
    }

/** Returns number of elements in the list. */
    public int size() {
        return stringList.size();
    }

/**
 * Returns the list.
*/
    public List getList() {
        return stringList;
    }

/**
 * Adds a String element to end of list.
*/
    public void add(String value) {
        stringList.add(value);
    }

/**
* Removes the input String if it is in the list.
*/
    public boolean remove(String value) {
        return stringList.remove(value);
    }

/**
* Returns the String array comprising the list.
*/
    public String [] toArray() {
        return (String []) stringList.toArray(new String[stringList.size()]);
    }

/**
* Return true if input String is within specified bounds inclusive.
*/
    public boolean contains(String value) {
        return stringList.contains(value);
    }

/**
* Returns concatenation of String elements of list where each element is separated by ", "
*/
    public String toString() {
    	return toString(", ");
//        StringBuffer sb = new StringBuffer(256);
//        Iterator iter = stringList.iterator();
//
//        while (iter.hasNext()) {
//            sb.append((String) iter.next());
//            sb.append(", ");
//        }
//        int len = sb.length();
//        if (len > 0) return sb.substring(0, len - 2);
//        else return "";
    }

    /**
    * Returns concatenation of String elements of list where each element is 
    * separated by the given delimiter.
    */
        public String toString(String delim) {

            StringBuffer sb = new StringBuffer(256);
            Iterator iter = stringList.iterator();

            while (iter.hasNext()) {
                sb.append((String) iter.next());
                sb.append(delim);
            }
            int len = sb.length();
            // lop off the final delimiter
            if (len > 0) return sb.substring(0, len - delim.length());
            else return "";
        }
/**
* Returns true if a String list can be parsed from input StringTokenizer.
* Does not create list and returns false if tokenizer.hasMoreTokens() == false.
*/
    public boolean parseValue(StringTokenizer tokenizer) {
        int count = tokenizer.countTokens();
        if (count <= 0) {
          stringList = new ArrayList(0);
          return false;
        }

        stringList = new ArrayList(count);
        for (int index = 0; index < count; index++) {
            add(tokenizer.nextToken());
        }
        return true;
    }

}
