/**
 * A generic object that describes a span of time
 */
package org.trinet.util;

import java.io.Serializable;
import java.text.*;
import java.util.*;

/**
 * Object describes a span of time. A time span has a start time, and end time
 * and a duration. Absolute times, like start and end, are always in seconds
 * (double) in "epoch" time (starting January 1, 1970). Duratation is in seconds.
 */
public class TimeSpan implements TimeSpanIF, Cloneable, java.io.Serializable {
    //static final int NULL = (int) Math.pow(2.0, 31.0);
    //private double start = -NULL;
    //private double end   = NULL;

    //static final int NULL = Double.MAX_VALUE;

    // NOTE: these are reversed so that setEarliest/setLatest, contains, etc. will work properly
    private double start = Double.MAX_VALUE;
    private double end   = -Double.MAX_VALUE; // -aww 10/16/2006

    public TimeSpan() { }

    public TimeSpan(double t0, double tn) {
        set(t0, tn);
    }

    public TimeSpan(TimeSpanIF ts) {
        set(ts);
    }

    public TimeSpan(DoubleRange doubleRange) {
        set(doubleRange.getMinValue(), doubleRange.getMaxValue());
    }

    /**
     * Set start time of this TimeSpan.
     */
    public void setStart(double t) {
        start = t;
    }
    /**
     * Set end time of this TimeSpan.
     */
    public void setEnd(double t) {
        end = t;
    }
    /**
     * Set start & end time of this TimeSpan.
     */
    public void setLimits(double t0, double tn) {
      set(t0, tn);
    }
    /**
     * Set start & end time of this TimeSpan.
     */
    public void set(double t0, double tn) {
        start = t0;
        end   = tn;
    }
    /**
     * Set start & end time of this TimeSpan.
     */
    public void set(TimeSpanIF ts) {
        start = ts.getStart();
        end   = ts.getEnd();
    }
    /**
     * Set TimeSpan to "null". Note that this does NOT set the pointer to null but sets the
     * internal values of the TimeSpan object to its own defined null values.
     */
    public void setNull() {
        start = Double.MIN_VALUE;
        end   = Double.MAX_VALUE;
    }
    public void setMin(double t) {
      setEarliest(t);
    }
    public void setEarliest(double t) {
        start = Math.min(start, t);
    }
    public void setMax(double t) {
      setLatest(t);
    }
    public void setLatest(double t) {
        end = Math.max(end, t);
    }
    /**
     * Expand TimeSpan, if necessary, to include this time.
     */
    public void include(double t0) {
        setEarliest(t0);
        setLatest(t0);
    }
    /**
     * Expand TimeSpan, if necessary, to include this time span.
     */
    public void include(double t0, double tn) {
        setEarliest(t0);
        setLatest(tn);
    }
    /**
     * Expand TimeSpan to include the input TimeSpan if it's not null.
     */
    public void include(TimeSpanIF ts) {
        if (! ts.isNull()) include(ts.getStart(), ts.getEnd());
    }
    /**
     * Return true if TimeSpan passed in arg is completely contained by this TimeSpan.
     */
    public boolean contains(double dt) {
        return contains(dt, dt);
    }
    /**
     * Return true if time interval passed in args is completely contained by this TimeSpan.
     */
    public boolean within(double startTime, double endTime) {
      return contains(startTime, endTime);
    }
    /**
     * Return true if time interval passed in args is completely contained by this TimeSpan.
     */
    public boolean contains(TimeSpanIF ts) {
      return contains(ts.getStart(), ts.getEnd());
    }
    /**
     * Return true if time interval passed in args is completely contained by this TimeSpan.
     */
    public boolean contains(double startTime, double endTime) {
        return (startTime >= start && endTime <= end);
    }
    /**
     * Return true if time interval passed in args is NOT completely contained by this TimeSpan.
     */
    public boolean excludes(TimeSpanIF ts) {
      return excludes(ts.getStart(), ts.getEnd());
    }
    /**
     * Return true if time interval passed in args is NOT completely contained by this TimeSpan.
     */
    public boolean excludes(double startTime, double endTime) {
      return !contains(startTime, endTime);
    }
    /**
     * Return true if time interval passed in args is NOT completely contained by this TimeSpan.
     */
    public boolean excludes(double dt) {
      return excludes(dt, dt);
    }
    /**
     * Return true if TimeSpan passed in arg overlaps this TimeSpan.
     */
    public boolean overlaps(TimeSpanIF ts) {
        return overlaps(ts.getStart(), ts.getEnd());
    }
    /**
     * Return true if time interval passed in args overlaps this TimeSpan.
     */
    public boolean overlaps(double startTime, double endTime) {
        return ! (this.isBefore(startTime) && this.isBefore(endTime) ||
            this.isAfter(startTime)  && this.isAfter(endTime) ) ;
    }
    /**
     * Return true if time passed in arg is contained by this TimeSpan.
     */
    public boolean within(double dt) {
      return contains(dt);
    }

    /** Return true if the timespan is before the given time */
    public boolean isBefore(double dt) {
      return (end < dt);
    }
    /** Return true if the timespan is before the given time */
    public boolean before(double dt) {
      return isBefore(dt);
    }
    /** Return true if the timespan is after the given time*/
    public boolean isAfter(double dt) {
      return (start > dt);
    }
    /** Return true if the timespan is after the given time*/
    public boolean after(double dt) {
      return isAfter(dt);
    }
    /**
     * True only if start and end times have been set
     */
    public boolean isValid() {
        //return ! (start == Double.MIN_VALUE || end == Double.MAX_VALUE);
        return ! (start == Double.MAX_VALUE || end == -Double.MAX_VALUE); // -aww 10/16/2006
    }

    public double getStart() {
        return start;
    }

    public double getEnd() {
        return end;
    }

    public double getDuration() {
        return ( isValid() ? (end - start) : 0. );
    }
    /** Return the time span that the two timespans have in common. Returns null
     *  if there is no overlap. */
    public TimeSpan getCommonTimeSpan(TimeSpanIF ts) {

      if (!overlaps(ts)) return null;

    /*
    There are three 4 possibilities:
    1)  |---|
       |-----|

    2)  |---|
         |-|

    3)  |---|
       |--|

    4)  |---|
           |--|
    */
    // always the latest start and the earliest end
      double cstart = Math.max(ts.getStart(), start);
      double cend   = Math.min(ts.getEnd(), end);
      return new TimeSpan(cstart, cend);
    }

    public double size() {
        return ( isValid() ? (end - start) : 0. );
    }
    /**
     * Keep timespan duration the same but shift it to be centered on this time.
     */
    public void setCenter(double centerTime) {
        setCenter(centerTime, getDuration());
    }
    /**
     * Set timespan  size and shift to be centered on the given time.
     */
    public void setCenter(double centerTime, double duration) {
        double halfWidth = duration/2.0;
        set(centerTime - halfWidth, centerTime + halfWidth);

    }
    /**
     * Move timespan this many seconds.
     */
    public void move(double secs) {
        set(start + secs, end + secs);
    }
    /**
     * Move timespan start to this time, leave duration unchanged.
     */
    public void moveStart(double newStart) {
        move(newStart - start);

        //    double dur = getDuration();
        //    set (newStart, newStart + dur);
    }
    /**
     * Return the center time
     */
    public double getCenter() {
        return ( isValid() ? start + ( getDuration()/2.0 ) : 0. );
    }

    /** Returns true if both start and end times match. */
    public boolean equals(TimeSpanIF ts) {
      return ( (ts.getStart() == start) && (ts.getEnd() == end) );
    }
    /**
     * Returns a hash code value for the TimeSpan. This method is
     * supported for the benefit of hashtables such as those provided by
     * <code>java.util.Hashtable</code>.
     * <p>
     * @see     java.util.Hashtable
     */
    public int hashCode() {
        return ( Double.valueOf( getStart() * getEnd() ) ).hashCode();
    }

    /**
     * Times are "equal" within the delta resolution of digitization (0.005 sec)
     */
    public boolean nearlyEquals(TimeSpanIF ts) {
        return nearlyEquals(ts,0.005);
    }

    public boolean nearlyEquals(TimeSpanIF ts, double delta) {
        return ((Math.abs(ts.getStart()-start) < delta) && (Math.abs(ts.getEnd()-end) < delta)); 
    }

    public boolean isNull() {
    //    if ( start == NULL && end == -NULL) return true;
        return ( (start == Double.MIN_VALUE) && (end == Double.MAX_VALUE) );
    }

    /**
     * Return a string showing the time span in the format starting and ending dates.
     */
    public String toString() { // how about name of toDateFormattedString ? - aww
       return  (timeToString(getStart()) + " -> " + timeToString(getEnd()));
    }

    public String toEpochTimeString() { // better if toString() -aww
       Format df = new Format("%13.4f");
       return  (df.form(start) +" -> "+ df.form(end));
    }

    /**
     * Format time as a date string in the default format of DateTime class
     */
    public static String timeToString(double epochTime) {
        return new DateTime(epochTime, true).toString(); // for UTC time seconds -aww 2008/02/11
    }

    public Object clone() {
      TimeSpan ts = null;
      try {
        ts = (TimeSpan) super.clone();
      }
      catch (CloneNotSupportedException ex) {
        ex.printStackTrace();
      }
      return ts;
    }

} // end of class
