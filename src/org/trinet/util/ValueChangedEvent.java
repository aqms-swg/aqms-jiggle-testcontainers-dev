package org.trinet.util;
import java.beans.*;

public class ValueChangedEvent extends PropertyChangeEvent {
    boolean validity;

    public ValueChangedEvent(Object src, String propName, Object oldValue, Object newValue) {
        this(src, propName, oldValue, newValue, true);
    }
    public ValueChangedEvent(Object src, String propName, Object oldValue, Object newValue,  boolean validity) {
        super(src, propName, oldValue, newValue);
        this.validity = validity;
    }

    public boolean getValidity() { return validity; }
}
