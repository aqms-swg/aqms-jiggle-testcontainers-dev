package org.trinet.util;

import org.trinet.jasi.JasiObject;

/**
 * Generic abstract WaveClient implementation. You must create a concrete implementation
 * for each specific WaveServer type.
 * @see: org.trinet.waveserver.rt.WaveClient()
 * */
public abstract class WaveClient extends JasiObject
    implements java.io.Serializable, org.trinet.waveserver.rt.WaveServerSource {

  /** Default time to wait for a server response. */
  public static final int DEFAULT_MAX_TIMEOUT_MILLISECS = 30000;

  /** Time to wait for a server response. */
  protected int maxTimeout = DEFAULT_MAX_TIMEOUT_MILLISECS;

  /** Default number of retries for a connection attempt. */
  public static final int DEFAULT_MAX_RETRIES = 3;

  /** Number of reconnection attempts. */
  protected int maxRetries = DEFAULT_MAX_RETRIES;

  /** True == truncate waveforms at first time gap. */
  protected boolean truncateAtTimeGap = false;

  /** True == verify waveforms. */
  protected boolean verifyWaveforms = true;

  public static final String DEFAULT_WAVE_CLIENT="org.trinet.waveserver.rt.WaveClient";

  /** Default timeout milliseconds for new server connection. */
  public static int DEFAULT_CONNECT_TIMEOUT = 9000;

  /** Timer interrupt when no connection made within time ms.*/
  public int connectTimeout = DEFAULT_CONNECT_TIMEOUT;

  /** Name of client (defaults to "DEFAULT".*/
  protected String name = "DEFAULT";

  public boolean debug = false;

  /* Dummy Constructor */
  public WaveClient() { }

  public static WaveClient createWaveClient() {
      return(createWaveClient(DEFAULT_WAVE_CLIENT));
  }

  public static WaveClient createWaveClient(String sClientName) {
      WaveClient newWaveClient = null;
      try {
          newWaveClient =  (org.trinet.util.WaveClient)Class.forName(sClientName).newInstance();
      }
      catch (ClassNotFoundException ex) {
          ex.printStackTrace();
      }
      catch (InstantiationException ex) {
          ex.printStackTrace();
      }
      catch (IllegalAccessException ex) {
          ex.printStackTrace();
      }
      return newWaveClient;
  }

  public WaveClient configureWaveClient() {
    maxRetries = maxTimeout = 0;
    verifyWaveforms = truncateAtTimeGap = false;
    return(this);
  }

  /** This does not really work!!! */
  public WaveClient configureWaveClient(String propertyFileName) {
    //System.err.println("Warning: configureWaveClient(String propertyFileName) is NOT implemented.");
    WaveClient wc = configureWaveClient();
    wc.setup(propertyFileName);
    return wc;
  }

  public WaveClient configureWaveClient(int maxRetries, int maxTimeoutMs, boolean verifyWaveforms, boolean truncateAtTimeGap) {
      return configureWaveClient(maxRetries, maxTimeoutMs, DEFAULT_CONNECT_TIMEOUT, verifyWaveforms, truncateAtTimeGap);
  }
  public WaveClient configureWaveClient(int maxRetries, int maxTimeoutMs, int connectTimeoutMs,
                    boolean verifyWaveforms, boolean truncateAtTimeGap) {
    setMaxRetries(maxRetries);
    setMaxTimeoutMilliSecs(maxTimeoutMs);
    setConnectTimeout(connectTimeoutMs);
    setWaveformVerify(verifyWaveforms);
    setTruncateAtTimeGap(truncateAtTimeGap);
    return(this);
  }

  /** Setup the waveserver using properties from this property file.
   *<b>
   * Valid properties:<br>
   *</b>
   * <pre>
   * keyString                valueString<br>
   * server            name:port name2:port name3:port ... ("\" at EOL continues the input on next line)
   * verifyWaveforms   true or false                       (default=true)
   * truncateAtTimeGap true or false                       (default=false)
   * connectTimeout     int >= 0                           (default=9000 ms)
   * maxTimeout        int >= 0                            (default=30000 ms, 0 => infinite, socket reads)
   * maxRetries        int >= 0                            (default=3 reconnects attempted on socket error)
   * </pre>
  */
  abstract public void setup( String propertyFileName) ;

  /** Returns the number of servers currently known by this WaveClient */
  abstract public int numberOfServers();

  public void setDebug(boolean tf) {
      debug = tf;
  }

  /** Returns the maximum number of re-connection, re-requests attempted on any server connection before failing. */
  public int getMaxRetries() {
      return maxRetries;
  }

  /** Sets the maximum number of re-connection, re-requests attempted on any server connection error before failing.
  *   @exception java.lang.IllegalArgumentException input parameter < 0
  */
  public int setMaxRetries(int retries) {
      if (maxRetries < 0 )
            throw new IllegalArgumentException("WaveClient.setMaxRetries(int) input parameter < 0");
      maxRetries = retries;
      return maxRetries;
  }

  public int setMaxTimeoutMilliSecs(int maxTimeoutMs) {
    maxTimeout = maxTimeoutMs;
    return maxTimeout;
  }

  /** Returns the maximum timeout milliseconds for input from a server responses to complete. */
  public int getMaxTimeoutMilliSecs() {
      return maxTimeout;
  }

  public int setConnectTimeout(int timeoutMs) {
    connectTimeout = timeoutMs;
    if (debug)
        System.out.println("INFO WaveClient setConnectTimeout( " + connectTimeout + ")");
    return connectTimeout;
  }

  /** Returns the maximum timeout milliseconds for a server connection to complete. */
  public int getConnectTimeout() {
      return connectTimeout;
  }

  /** Gets the verification state setting for all retrieved time series data.
  * True implies data packets are checked for channel and time consistency.
  * Default value == true;
  */
  public boolean isVerifyWaveforms() {
      return verifyWaveforms;
  }

  /** Sets the default verification mode for any time series data retrieved from a server.
  * True value implies all data packets are checked for channel and time consistency,
  * which requires decoding data headers and adds overhead to the processing.
  * If waveforms are reliably transferred from servers set state to false for faster response.
  * Default value == true;
  */
  public boolean setWaveformVerify(boolean verify) {
    verifyWaveforms = verify;
    return verifyWaveforms;
  }

  /** Returns true if mode is set to truncate any requested time-series data at first time tear.
  * Data requested by the getData(...) and getPacketData(...) and getTimes(...) methods.
  * @see #getData(String, String, String, Date, int, Collection)
  * @see #getData(Channel, TimeWindow, Waveform)
  * @see #getJasiWaveformDataRaw(org.trinet.jasi.Waveform)
  * @see #getJasiWaveformDataSeries(org.trinet.jasi.Waveform)
  * @see #getPacketData(Channel, TimeWindow)
  * @see #getTimes(Channel, Collection)
  */
  public boolean isTruncateAtTimeGap() {
      return truncateAtTimeGap;
  }

  /** Returns true if mode is set to ignore all time gaps any requested time-series data, no data truncation.
  * Data requested by the getData(...) and getPacketData(...) and getTimes(...) methods.
  * @see #getData(String, String, String, Date, int, Collection)
  * @see #getData(Channel, TimeWindow, Waveform)
  * @see #getJasiWaveformDataRaw(org.trinet.jasi.Waveform)
  * @see #getJasiWaveformDataSeries(org.trinet.jasi.Waveform)
  * @see #getPacketData(Channel, TimeWindow)
  * @see #getTimes(Channel, Collection)
  */
  public boolean isIgnoreTimeGaps() {
      return ! truncateAtTimeGap;
  }

  /** Sets mode to truncate any requested time-series data at first time tear.
  * Data requested by the getData(...) , getPacketData(...) , getJasiWaveformDataXXX(...) and getTimes(...) methods
  * @see #getData(String, String, String, Date, int, Collection)
  * @see #getData(Channel, TimeWindow, Waveform)
  * @see #getJasiWaveformDataRaw(org.trinet.jasi.Waveform)
  * @see #getJasiWaveformDataSeries(org.trinet.jasi.Waveform)
  * @see #getPacketData(Channel, TimeWindow)
  * @see #getTimes(Channel, Collection)
  */
 public boolean setTruncateAtTimeGap(boolean value) {
     truncateAtTimeGap = value;
     return truncateAtTimeGap;
  }

  public abstract boolean addServer(String host, int port);

  public abstract String [] listServers();

  public int getTimeSeries(org.trinet.jasi.Waveform jasiWaveform) {
    return getJasiWaveformDataRaw(jasiWaveform);
  }

  public int getJasiWaveformDataRaw(org.trinet.jasi.Waveform jasiWaveform) {
    return(-1);
  }

  public void setName(String name) {
      this.name = name;
  }

  public String getName() {
      return name;
  }

}  /* end WaveClient */


