package org.trinet.util.gazetteer;
import java.text.*;
import java.util.*;

/** Utility class with methods for fixed field width formatting of decimal numbers into strings.
* Format disables thousands, millions grouping "," and the decimal separator not shown for integers.
* format(...) methods returns the input StringBuffer appended with the formatted input number.
* If the method input StringBuffer is null, the method creates one by default.<BR>
* If you want all formatted numbers to have the same constant field width, specify a maximum integer
* width that is larger than the decimals in largest signed number being formatted, all numbers 
* with fewer decimal places will be left-padded.
*/
public class Concat {
  NumberFormat nf;
  DecimalFormat df;
  FieldPosition fp;
  char [] pad;
  char [] zeroPattern;

/** Default formatting assumes left blank padding of number strings. */
  public Concat() {
    nf = NumberFormat.getNumberInstance();
    if (nf instanceof DecimalFormat) {
      df = (DecimalFormat) nf;
      df.setGroupingUsed(false);
      df.setDecimalSeparatorAlwaysShown(false);
    }
    fp = new FieldPosition(df.INTEGER_FIELD);
    pad = new char[48];
    for (int index = 0; index < 48; index ++) {
       pad[index] = ' ';
    }
    zeroPattern = new char[48];
    for (int index = 0; index < 48; index ++) {
       zeroPattern[index] = '0';
    }
  }

/** Left pad numeric field width with specified char in the string format */
  public Concat(char padchar) {
    this();
    for (int index = 0; index < 48; index ++) {
       pad[index] = padchar;
    }
  }

/** Width of the formatted field is variable with a minimum of 1 integer place.
* Example: format(sb, number) creates a string of the form "#0". 
* Thus 0 would be formatted into a string as "0" and 1234 would be "1234".
*/
  public StringBuffer format(StringBuffer sb, long number) {
    if (sb == null) sb = new StringBuffer(132);
    StringBuffer formatPatternBuffer = new StringBuffer("#0");
    df.applyPattern(formatPatternBuffer.toString());
    df.setDecimalSeparatorAlwaysShown(false);
    sb = df.format(number, sb, fp);
    return sb;
  }

/** Width of the formatted field is variable with a minimum of 1 integer place.
* Example: format(sb, number) creates a string of the form "#0.#".
* At least 1 numbers in the integer portion and zero or more fraction digits.
* Thus 3. would be formatted into a string as "3." and .70 as "0.7" and 1.234 as "1.234".
*/
  public StringBuffer format(StringBuffer sb, double number) {
    if (sb == null) sb = new StringBuffer(132);
    StringBuffer formatPatternBuffer = new StringBuffer("#0.#");
    df.applyPattern(formatPatternBuffer.toString());
    sb = df.format(number, sb, fp);
    return sb;
  }

/** Mininum width of the formatted field = specified integerFieldWidth.
* Example: format(sb, number, 5) creates a string of the form "####0". A mininum string length of 5.
* Thus 3 would be formatted into the buffer as "    3". and 1234567 as "1234567".
*/
  public StringBuffer format(StringBuffer sb, long number, int intFieldWidth) {
    if (sb == null) sb = new StringBuffer(132);
    StringBuffer formatPatternBuffer = new StringBuffer("#0");
    df.applyPattern(formatPatternBuffer.toString());
    df.setDecimalSeparatorAlwaysShown(false);
    sb = df.format(number, sb, fp);
    int istart = fp.getBeginIndex();
    int npad = intFieldWidth - fp.getEndIndex() + istart;
    if (number < 0) {
      if (istart > 0) istart--; 
      npad--;
    }
    if (npad > 0) {
       for (int index=0; index < npad; index++) {
           sb.insert(istart+index,pad[0]);
       }
    }
    return sb;
  }

/** Mininum width of the formatted field = (integerFieldWidth + minimumFractionWidth).
* Numbers smaller than the mininum width are padded with the padding char out to the width.
* Larger numbers can exceed the minimum width.
* Example: format(sb, number, 5, 3) creates a string of the form "##000". A string length of 9 with
* at least 2 numbers in the integer portion and at least 3 numbers in the fraction digits. Thus 3.
* would be formatted into a string as "  003" and 1234567 as "1234567".
*/
  public StringBuffer format(StringBuffer sb, long number, int intFieldWidth, int nZeroWidth) {
    if (sb == null) sb = new StringBuffer(132);
    StringBuffer formatPatternBuffer = new StringBuffer("#0");
    if (nZeroWidth > 1) formatPatternBuffer.append(zeroPattern, (int) 0, nZeroWidth-1);
    df.applyPattern(formatPatternBuffer.toString());
    df.setDecimalSeparatorAlwaysShown(false);
    sb = df.format(number, sb, fp);
    int istart = fp.getBeginIndex();
    int npad = intFieldWidth - fp.getEndIndex() + istart;
    if (number < 0) {
      if (istart > 0) istart--; 
      npad--;
    }
    if (npad > 0) {
       for (int index=0; index < npad; index++) {
           sb.insert(istart+index,pad[0]);
       }
    }
    return sb;
  }

/** Mininum width of the formatted field = (integerFieldWidth + minimumFractionWidth + ".").
* Numbers smaller than the mininum width are padded with the padding char out to the width.
* Larger numbers can exceed the minimum width.
* Example: format(sb, number, 5, 3) creates a string of the form "####0.000". A string length of 9 with
* at least 2 numbers in the integer portion and at least 3 numbers in the fraction digits. Thus 3.
* would be formatted into a string as "    3.000" and 1234567.89 as "1234567.890".
*/
  public StringBuffer format(StringBuffer sb, double number, int intFieldWidth, int minFracWidth) {
    if (sb == null) sb = new StringBuffer(132);
    StringBuffer formatPatternBuffer = new StringBuffer("#0.");
    formatPatternBuffer.append(zeroPattern, (int) 0, minFracWidth);
    df.applyPattern(formatPatternBuffer.toString());
    sb = df.format(number, sb, fp);
    int istart = fp.getBeginIndex();
    int npad = intFieldWidth - fp.getEndIndex() + istart;
    if (number < 0) {
      if (istart > 0) istart--; 
      npad--;
    }
    if (npad > 0) {
       for (int index=0; index < npad; index++) {
           sb.insert(istart+index,pad[0]);
       }
    }
    return sb;
  }

/** Returns the input StringBuffer appended with a formatted input number.
* If the input StringBuffer is null, creates one by default.
* Mininum width of the formatted field = (integerFieldWidth + minimumFractionWidth + ".").
* Numbers smaller than the mininum width are padded with the padding char out to the width.
* Larger numbers can exceed the minimum width.
* The nZeroWidth parameter refers to the minimum number of integer places (left of the decimal).<BR>
* Example: format(sb, number, 5, 3, 2) creates a string of the form "###00.000". A string length of 9 with
* at least 2 numbers in the integer portion and at least 3 numbers in the fraction digits. Thus 3.
* would be formatted into a string as "   03.000" and 1234567 as "1234567.000".
*/
  public StringBuffer format(StringBuffer sb, double number, int intFieldWidth, int minFracWidth, int nZeroWidth) {
    if (sb == null) sb = new StringBuffer(132);
    StringBuffer formatPatternBuffer = new StringBuffer("#0");
    if (nZeroWidth > 1) formatPatternBuffer.append(zeroPattern, (int) 0, nZeroWidth-1);
    formatPatternBuffer.append(".");
    formatPatternBuffer.append(zeroPattern, (int) 0, minFracWidth);
    df.applyPattern(formatPatternBuffer.toString());
    sb = df.format(number, sb, fp);
    int istart = fp.getBeginIndex();
    int npad = intFieldWidth - fp.getEndIndex() + istart;
    if (number < 0) {
      if (istart > 0) istart--; 
      npad--;
    }
    if (npad > 0) {
       for (int index=0; index < npad; index++) {
           sb.insert(istart+index,pad[0]);
       }
    }
    return sb;
  }
}
