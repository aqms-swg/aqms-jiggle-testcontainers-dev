package org.trinet.util.gazetteer;
import java.util.*;
public class DistanceSorter implements Comparator {
    /** Method input parameters must be instances of HorizontalDistanceBearingIF.*/
    public int compare (Object o1, Object o2) {
        HorizontalDistanceBearingIF c1 = (HorizontalDistanceBearingIF) o1;
        HorizontalDistanceBearingIF c2 = (HorizontalDistanceBearingIF) o2;
        double diff = c1.getHorizontalDistance() - c2.getHorizontalDistance();

        if (Math.abs(diff) < .00001) diff = 0.; // added this test to handle weird roundoff? -aww 2010/10/20

        if (diff < 0.0) return -1;
        if (diff > 0.0) return  1;
        return 0;
    }
} // end of DistanceSorter class
