
// *** PACKAGE NAME *** \
package org.trinet.util.gazetteer.EW;


// *** IMPORTS *** \
import java.util.Vector;
import java.sql.Connection;

import org.trinet.jasi.EW.SQL.EWSQLStatement;

// Individual SQL Statements
import org.trinet.jasi.EW.SQL.GetNearestPlacesStatement;

public class EWWhereIs
{


  // *** CLASS ATTRIBUTES *** \

  public static final int PLACE_TYPE_CITY       = 1;
  public static final int PLACE_TYPE_FAULT      = 4;
  public static final int PLACE_TYPE_STRUCTURE  = 5;
  public static final int PLACE_TYPE_NOISE_SITE = 6;

  // Data Attributes

  public long   idPlace;
  public double dLat;
  public double dLon;
  public double dDepth;
  public String sDescription;
  public String sName;
  public String sState;
  public int    iPlaceMajorType;
  public int    iPlaceMinorType;

  // *** CLASS SQL STATEMENTS *** \
  static EWSQLStatement es1;


  // *** CONSTRUCTORS *** \
  public EWWhereIs()
  {
    if(es1 == null)
      es1 = new GetNearestPlacesStatement(org.trinet.jasi.DataSource.getConnection());
  }

  // *** CLASS METHODS *** \
  public void setConnection(Connection IN_conn)
  {
    es1.SetConnection(IN_conn);
  }


  public Vector ReadList()
  {
    Vector ResultList = new Vector();
    es1.CallQueryStatement((Object)this, ResultList);
    return(ResultList);
  }  // end EWSolution:ReadList()

}
