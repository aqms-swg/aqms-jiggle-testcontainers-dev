// Geoidal classes implement it
package org.trinet.util.gazetteer;
// Internal code convention will be Z is the depth below datum, positive downwards.
// Thus elevations above the geoid datum should be signed negative!
public interface GeoidalLatLonZ {
    LatLonZ getLatLonZ();
    boolean hasLatLonZ();
    // must maintain directional convention in coda base
    // or you will need to add methods to Geoidal subtypes like:
    //boolean positiveDepths() or
    //boolean positiveElevs() // return ! positiveDepths
    //void setZType(int mode) // is it a ELEV +up, or DEPTH +down axis
}

