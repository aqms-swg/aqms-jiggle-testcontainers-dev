package org.trinet.util.gazetteer;
import java.io.Serializable;

// Made serializable - DDG 7/21/2000
public class GeoidalUnits implements Serializable {
    public static final GeoidalUnits KILOMETERS = new GeoidalUnits(0, " km");
    public static final GeoidalUnits METERS = new GeoidalUnits(1, " m");

    public static final GeoidalUnits MILES = new GeoidalUnits(2, " mi");
    public static final GeoidalUnits FEET = new GeoidalUnits(3, " ft");

    //public static final GeoidalUnits DEGREES = new GeoidalUnits(4, "\u00B0");
    public static final GeoidalUnits DEGREES = new GeoidalUnits(4, " deg");
    public static final GeoidalUnits MINSECS = new GeoidalUnits(5, "'");

    private int type;
    private String label;

    private GeoidalUnits(int type, String label) {
        this.type = type;
        this.label = label;
    }

    public int getType() { return type;};
    public String getLabel() { return label;};

    public boolean equals (Object object) {
        if (this.type == ((GeoidalUnits) object).getType()) return true;
        else return false;
    }
    public String toString() {
        return type + label;
    }
}

