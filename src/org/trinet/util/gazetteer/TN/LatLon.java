package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;

import java.math.*;
import java.text.*; 

/**
 * Geographical reference point latitude, longitude
 * Northern and eastern hemispheres are positive.
 * Southern and western hemispheres are negative.
 * Distances are returned in units set by the GeoidalConvert class.
 */
public class LatLon implements MapLocation {
    double latitude;
    double longitude;

/**
 * Null constructor
 */
    public LatLon() { }

/**
 * Contruct from existing LatLon.
 */
    public LatLon(LatLon latlon) {
        copy(latlon);
    }

/** 
 * Latitude and longitude in decimal degrees 
 */
    public LatLon(double lat, double lon) {
        set(lat, lon);
    }

/** 
 * Latitude and longitude in interger degrees and decimal minutes
 */
    public LatLon(int lat, double latm, int lon, double lonm) {
        set(lat, latm, lon, lonm);
    }

/**
 * Copy LatLon to this one.
 */
    public void copy(LatLon latlon) {
        set(latlon.latitude, latlon.longitude);
    }

/** 
 * Latitude and longitude in decimal degrees 
 */
    public void set(double lat, double lon) {
        latitude = lat;
        longitude = lon;
    }

/** 
 * Latitude and longitude in interger degrees and decimal minutes.
 * Sign is preserved only in the integer degrees; all minutes assume the same sign
 * as the degrees value.
 */
    public void set(int lat, double latm, int lon, double lonm) {
        if (lat >= 0) latitude  = (double) lat + Math.abs(latm)/60.0;
        else latitude  = (double) lat - Math.abs(latm)/60.0;

        if (lon >= 0) longitude = (double) lon + Math.abs(lonm)/60.0;
        else longitude = (double) lon - Math.abs(lonm)/60.0;
    }

    public void setLat(double lat) {
        latitude = lat;
    }
    public void setLon(double lon) {
        longitude = lon;
    }

    public LatLon getLatLon() {
        return this;
    }

/** Implementation of MapLocation interface method */
    public MapLocation getMapLocation() {
        return this;
    }

/** Implementation of Geoidal interface method */
    public double getLat() {
        return latitude;
    }

/** Implementation of Geoidal interface method */
    public double getLon() {
        return longitude;
    }

/**
 * Calculate the km horizontal distance from this location. 
 * it calculates the length of a chord on the surface of a sphere.
 */
    public double horizontalDistanceFrom(LatLon latlon) {
        return GeoidalConvert.horizontalDistanceKmBetween(this.latitude, this.longitude, latlon.latitude, latlon.longitude);
    }

    public boolean equals(Object object) {
        if (object == null || getClass() != object.getClass()) return false;
        LatLon latlon =  (LatLon) object;
        return (latitude == latlon.latitude && longitude == latlon.longitude) ? true : false;
    }

/** Returns String of latitude, longitude fields delimited by blanks. */
    public String toString() {
        StringBuffer sb = new StringBuffer(24);
        DecimalFormat df = new DecimalFormat ( "###0.0000" );
        sb.append(df.format(latitude)).append(" ").append(df.format(longitude));
        return sb.toString();
    }
}
