package org.trinet.util.gazetteer.TN;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.SQLData;
import java.sql.SQLInput;
import java.sql.SQLOutput;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;
//import oracle.sql.STRUCT;
//import oracle.jpub.runtime.MutableStruct;
// e.g. jpub -classpath <must include sqlj jar stuff under oracle home>
// -user CODE/CODE -url="jdbc:oracle:thin:@serveri.gps.caltech.edu:1521:databasei" -sql LATLON:OracleLatLon
public class OracleLatLon implements SQLData, java.io.Serializable
{
  //public static final String _SQL_NAME = "CODE.LATLON"; // jdbc qualifies with user schema if not fully qualified
  public static final String _SQL_NAME = "PUBLIC.LATLON"; // jdbc qualifies with user schema if not fully qualified
  public static final int _SQL_TYPECODE = OracleTypes.STRUCT;

  private java.math.BigDecimal m_Lat;
  private java.math.BigDecimal m_Lon;

  /* constructor */
  public OracleLatLon() { }

  public OracleLatLon(double lat, double lon) throws SQLException {
    this(new java.math.BigDecimal(lat), new java.math.BigDecimal(lon)); 
  }

  public OracleLatLon(java.math.BigDecimal Lat, java.math.BigDecimal Lon) throws SQLException
  {
    setLat(Lat);
    setLon(Lon);
  }
  public void readSQL(SQLInput stream, String type)
  throws SQLException
  {
      setLat(stream.readBigDecimal());
      setLon(stream.readBigDecimal());
  }

  public void writeSQL(SQLOutput stream)
  throws SQLException
  {
      stream.writeBigDecimal(getLat());
      stream.writeBigDecimal(getLon());
  }

  public String getSQLTypeName() throws SQLException
  {
    return _SQL_NAME;
  }

  /* Serialization interface */
  public void restoreConnection(java.sql.Connection conn) throws SQLException
  { }
  /* accessor methods */
  public java.math.BigDecimal getLat()
  { return m_Lat; }

  public void setLat(java.math.BigDecimal Lat)
  { m_Lat = Lat; }


  public java.math.BigDecimal getLon()
  { return m_Lon; }

  public void setLon(java.math.BigDecimal Lon)
  { m_Lon = Lon; }

  public String toString()
  { try {
     return "LATLON" + "(" +
       getLat() + "," +
       getLon() +
     ")";
    } catch (Exception e) { return e.toString(); }
  }

}
