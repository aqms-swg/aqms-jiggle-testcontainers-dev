package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;
// Tiger FIPS codes
public class USACountyType {
    public static final USACountyType INYO = new USACountyType(USAStateType.CA, 27, "Inyo");
    public static final USACountyType KERN = new USACountyType(USAStateType.CA, 29, "Kern");
    public static final USACountyType LOS_ANGELES = new USACountyType(USAStateType.CA, 37, "Los Angeles");
    public static final USACountyType ORANGE = new USACountyType(USAStateType.CA, 59, "Orange");
    public static final USACountyType RIVERSIDE = new USACountyType(USAStateType.CA, 65, "Riverside");
    public static final USACountyType SAN_BERNANDINO = new USACountyType(USAStateType.CA, 71, "San Bernadino");
    public static final USACountyType SAN_LUIS_OBISPO = new USACountyType(USAStateType.CA, 79,"San Luis Obispo");
    public static final USACountyType SAN_DIEGO = new USACountyType(USAStateType.CA, 73, "San Diego");
    public static final USACountyType SANTA_BARBARA = new USACountyType(USAStateType.CA, 83, "Santa Barbara");
    public static final USACountyType TULARE = new USACountyType(USAStateType.CA, 107, "Tulare");
    public static final USACountyType VENTURA = new USACountyType(USAStateType.CA, 111, "Ventura");

    private final USAStateType state;
    private final int code; 
    private final String name; 

    private USACountyType(USAStateType state, int code, String name) {
        this.state = state;
        this.code =  code;
        this.name =  name;
    }
    public USAStateType getState() {
        return state;
    }
    public int getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    public boolean equals(Object object) {
        if (object == null || ! (object instanceof USACountyType)) return false;
        if (getCode() == ((USACountyType) object).getCode()) {
            if(getName().equals(((USACountyType) object).getName())) return true; 
        }
        return false;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer(48);
        sb.append(state.toString()).append(" ");
        sb.append(String.valueOf(code)).append(" ");
        sb.append(name);
        return sb.toString();
    }
}
