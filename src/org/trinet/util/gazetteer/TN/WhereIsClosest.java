package org.trinet.util.gazetteer.TN;
import java.sql.*;
import java.util.*;

import org.trinet.jasi.JasiDatabasePropertyList;
import org.trinet.jasi.DataSource;
import org.trinet.util.gazetteer.*;

/** Every constructor and method assumes inputs are in units of decimal degrees and kilometers (depth or elevation).
*/
public abstract class WhereIsClosest extends WhereAmI implements AutoCloseable {

    CallableStatement cs = null;

/** Default constructor does not set a database connection or the reference target point for gazetteer table data comparison.
*/
    WhereIsClosest() { }

/** Constructor sets the database connection does not set the reference target point for gazetteer table data comparison.
*/
    WhereIsClosest(Connection conn) {
	super(conn);
    }

/** Constructor sets a database connection and the reference target point for gazetteer table data comparison.
*/
    WhereIsClosest(Connection conn, Geoidal reference) {
	super(conn, reference);
    }

/** Constructor the reference target point for gazetteer table data comparison, does not set a database connection.
*/
    WhereIsClosest(Geoidal reference) {
	super(reference);
    }

/** Constructor the reference target point for gazetteer table data comparison, does not set a database connection.
* Depth or elevation in assumed to be in kilometers units.
*/
    WhereIsClosest(double lat, double lon, double z) {
	super(lat, lon, z);
    }

    void closeStatement() {
	try {
          if (cs != null)  cs.close();
        }
	catch (SQLException ex) {}

        cs = null;
    }

    @Override
    public void close() {
	try {
          if (cs != null) cs.close();
        }
	catch (SQLException ex) {}
    }

/** Implemented by subclasses of different gazetteer datatypes.*/
    abstract protected Vector getDatabaseData() ;

    protected Vector getDatabaseData(String sql) {
	Vector v = new Vector(1);
	try {
	    if (cs == null) {
                if (JasiDatabasePropertyList.debugSQL) System.out.println(sql);
		cs = conn.prepareCall(sql);
		cs.registerOutParameter(4, Types.DOUBLE);
		cs.registerOutParameter(5, Types.DOUBLE);
		cs.registerOutParameter(6, Types.DOUBLE);
		cs.registerOutParameter(7, Types.VARCHAR);
	    }
	    cs.setDouble(1, reference.getLat());
	    cs.setDouble(2, reference.getLon());
	    cs.setDouble(3, reference.getZ());
	    cs.executeUpdate();
	    WhereSummaryItem whereItem = 
		new WhereSummaryItem(cs.getDouble(4), cs.getDouble(5), cs.getDouble(6), cs.getString(7), null);
	    v.addElement(whereItem);
	}
	catch (SQLException ex) {
	    System.err.println(ex.getMessage());
	    ex.printStackTrace(System.err);
	    return null;
	}
	return v;
    }

    public void setConnection(Connection conn) {
        closeStatement(); // force close of current statement, if any 
        super.setConnection(conn);
    }

    public Connection getConnection() {

      // use default if none defined
      if (conn == null) conn = DataSource.getConnection();

      try {
        if (conn.isClosed()) {
          System.out.println("Connection is closed, reestablishing...");
          conn = null;
          closeStatement(); // force reset of statement
          conn = DataSource.getConnection();
        }
      }
      catch (SQLException ex) {
        System.out.println("Error checking conn object.");
      }

      return conn;

    }
}
