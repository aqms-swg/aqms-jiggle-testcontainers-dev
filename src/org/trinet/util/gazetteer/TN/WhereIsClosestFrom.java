package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;
import java.sql.*;
import java.io.*;

/** Utility used by client applications to obtain the closest geographic types relative to a specified reference point. 
 * These classes are not used in production
*  Example usage:
<PRE>
        .
        .
        .
         
        WhereIsClosestFrom wicf = new WhereIsClosestFrom(conn);

        wicf.setReference(34.4801,-119.5104, 0.); // need to set source point to measure to relative to database data.

        wicf.setDistanceUnits(GeoidalUnits.MILES); // sets these units for only for where methods. 
        System.out.println(wicf.where(34.4801,-119.5104));
        System.out.print(wicf.whereType(34.3103, -117.6218, 0., GazetteerType.TOWN));

        WhereSummaryItem wsi= wicf.getClosestTown();
        System.out.println(wsi.fromWhereString(GeoidalUnits.KILOMETERS, false));
        DistanceAzimuthElevation dae = wsi.getDistanceAzimuthElevation();
        System.out.println("distance: " + dae.getDistance(GeoidalUnits.MILES) + " miles.");

        .
        .
        .

        wicf.closeStatements(); // release database resource when done.
</PRE>
*/
// This family of classes is not used anywhere in production
public class WhereIsClosestFrom {
    Connection connection = null;
    private WhereIsClosestTown wTown = null;
    private WhereIsClosestBigTown wBigTown = null;
    private WhereIsClosestQuarry wQuarry = null;
    private WhereIsClosestQuake wQuake = null;
    private WhereIsClosestEqPOI wEqPOI = null;
    private WhereIsClosestStation wStn = null;
    private WhereIsClosestFault wFault = null;
    // private WhereIsClosestVolcano wVolcano = null; // No such class

/** Constructor sets default gazetteer database connection object.
*   Does not set the reference point.
* @see #setReference(double, double, double)
*/
    public WhereIsClosestFrom(Connection connection) {
        wTown = new WhereIsClosestTown(connection);
        wBigTown = new WhereIsClosestBigTown(connection);
        wEqPOI = new WhereIsClosestEqPOI(connection);
        wQuarry = new WhereIsClosestQuarry(connection);
        wQuake = new WhereIsClosestQuake(connection);
        wStn = new WhereIsClosestStation(connection);
        wFault = new WhereIsClosestFault(connection);
        //wVolcano = new WhereIsClosestVolcano(connection);
    }

/** Returns the current WhereIsClosest object for the specified data type.
* Returns null if type is unknown.<BR>
* Example usage:
* <blockquote>
  <PRE>
*        wic = WhereIsClosestFrom.getWhereType(GazetteerType.TOWN); <BR>
*        wic.setReference(lat, lon, z); // specify target (event location) <BR>
*        WhereSummary [] wi = wic.getClosestByCount(1); <BR>
*        DistanceAzimuthElevation dae = wi[0].getDistanceAzimuthElevation(); <BR>
*        double distance = dae.getDistanceMiles();         // getDistance() default is km <BR>
  </PRE>
* </blockquote>
*/
    public WhereIsClosest getWhereType(String type) {
        if (type.equalsIgnoreCase(GazetteerType.TOWN)) {
            return wTown;
        }
        else if (type.equalsIgnoreCase(GazetteerType.BIG_TOWN)) {
            return wBigTown;
        }
        else if (type.equalsIgnoreCase(GazetteerType.QUARRY)) {
            return wQuarry;
        }
        else if (type.equalsIgnoreCase(GazetteerType.QUAKE)) {
            return wQuake;
        }
        else if (type.equalsIgnoreCase(GazetteerType.STATION)) {
            return wStn;
        }
        else if (type.equalsIgnoreCase(GazetteerType.FAULT)) {
            return wFault;
        }
        else if (type.equalsIgnoreCase(GazetteerType.EQ_POI)) {
            return wEqPOI;
        }
        /*
        else if (type.equalsIgnoreCase(GazetteerType.VOLCANO)) {
            return wVolcano;
        }
        */
        return null;
    }

/* True == database REMARK columns is included in output strings. */
    public void setIncludeRemark(boolean includeRemark) {
        wTown.setIncludeRemark(includeRemark);
        wBigTown.setIncludeRemark(includeRemark);
        wQuarry.setIncludeRemark(includeRemark);
        wQuake.setIncludeRemark(includeRemark);
        wStn.setIncludeRemark(includeRemark);
        wFault.setIncludeRemark(includeRemark);
        wEqPOI.setIncludeRemark(includeRemark);
        //wVolcano.setIncludeRemark(includeRemark);
    }

/** GeoidalUnits.MILES == distances are reported in miles in the where(...) method output text strings.
* GeoidalUnits.KILOMETERS (default) == distances reported in kilometers in the output text strings. */
    public void setDistanceUnits(GeoidalUnits units) {
        wTown.setDistanceUnits(units);
        wBigTown.setDistanceUnits(units);
        wQuarry.setDistanceUnits(units);
        wQuake.setDistanceUnits(units);
        wStn.setDistanceUnits(units);
        wFault.setDistanceUnits(units);
        wEqPOI.setDistanceUnits(units);
        //wVolcano.setDistanceUnits(units);
    }
    public void setDistanceUnitsKm() {
        setDistanceUnits(GeoidalUnits.KILOMETERS);
    }
    public void setDistanceUnitsMiles() {
        setDistanceUnits(GeoidalUnits.MILES);
    }

/** Returns String describing where the specified location is relative to closest database entry for all known database types. */
    public String where(double lat, double lon) {
        return where(lat, lon, 0.);
    }
/** Returns String describing where the specified location (int degrees and decimal minutes) is relative to the closest 
* database entry for all known types.  Uses z to determine elevation.
*/
    public String where(int lat, double latmin, int lon, double lonmin, double  z) {
        return where(GeoidalConvert.toDecimalDegrees(lat, latmin), GeoidalConvert.toDecimalDegrees(lon, lonmin),
                z);
    }
/** Returns String describing where the specified location is relative to closest database entry for all known database types.
* Uses z to determine elevation.
*/
    public String where(double lat, double lon, double z) {
        StringBuffer sb = new StringBuffer(2048);
        sb.append(wTown.from(lat, lon, z, 1));
        sb.append(wBigTown.from(lat, lon, z, 1));
        sb.append(wQuarry.from(lat, lon, z, 1));
        sb.append(wQuake.from(lat, lon, z, 1));
        sb.append(wStn.from(lat, lon, z, 1));
        sb.append(wFault.from(lat, lon, z, 1));
        //sb.append(wVolcano.from(lat, lon, z, 1));
        return sb.toString();
    }

/** Returns String describing where the specified location is relative to the closest database entry
* for the specified input database type. Uses z to determine elevation.
*/
    public String whereType(double lat, double lon, double z, String type) {
        if (type.equalsIgnoreCase(GazetteerType.TOWN)) {
            return wTown.from(lat, lon, z, 1);
        }
        else if (type.equalsIgnoreCase(GazetteerType.BIG_TOWN)) {
            return wBigTown.from(lat, lon, z, 1);
        }
        else if (type.equalsIgnoreCase(GazetteerType.QUARRY)) {
            return wQuarry.from(lat, lon, z, 1);
        }
        else if (type.equalsIgnoreCase(GazetteerType.QUAKE)) {
            return wQuake.from(lat, lon, z, 1);
        }
        else if (type.equalsIgnoreCase(GazetteerType.STATION)) {
            return wStn.from(lat, lon, z, 1);
        }
        else if (type.equalsIgnoreCase(GazetteerType.FAULT)) {
            return wFault.from(lat, lon, z, 1);
        }
        else if (type.equalsIgnoreCase(GazetteerType.EQ_POI)) {
            return wEqPOI.from(lat, lon, z, 1);
        }
        /*
        else if (type.equalsIgnoreCase(GazetteerType.VOLCANO)) {
            return wVolcano.from(lat, lon, z, 1);
        }
        */
        else {
            return null;
        }
    }

/** Sets this object's geographic reference source point.
*/
    public void setReference(Geoidal latLonZ)  {
        setReference(latLonZ.getLat(), latLonZ.getLon(), latLonZ.getZ());
    }
/** Sets this object's geographic reference source point.
*/
    public void setReference(double lat, double lon)  {
        setReference(lat, lon, 0.);
    }
/** Sets this object's geographic reference source point.
*   Distance, azimuth and elevation are calculated from the Gazetteer database points to this reference point.
*/
    public void setReference(double lat, double lon, double z)  {
        wTown.setReference(lat, lon, z);
        wBigTown.setReference(lat, lon, z);
        wQuarry.setReference(lat, lon, z);
        wQuake.setReference(lat, lon, z);
        wStn.setReference(lat, lon, z);
        wFault.setReference(lat, lon, z);
        wEqPOI.setReference(lat, lon, z);
    }

/** Closes all the JDBC callable statements used by this object.*/
    public void closeStatements() {
        wTown.closeStatement();
        wBigTown.closeStatement();
        wQuarry.closeStatement();
        wQuake.closeStatement();
        wStn.closeStatement();
        wFault.closeStatement();
        wEqPOI.closeStatement();
   }

/** Returns closest town summary data using currently set reference point. */
    public WhereSummaryItem getClosestTown() {
        return (WhereSummaryItem) wTown.getClosestItemsByCount(1) [0];
    }

/** Returns closest big town summary data using currently set reference point. */
    public WhereSummaryItem getClosestBigTown() {
        return (WhereSummaryItem) wBigTown.getClosestItemsByCount(1) [0];
    }

/** Returns closest quarry summary data using currently set reference point. */
    public WhereSummaryItem getClosestQuarry() { 
        return (WhereSummaryItem) wQuarry.getClosestItemsByCount(1) [0];
    }

/** Returns closest significant earthquake summary data using currently set reference point. */
    public WhereSummaryItem getClosestQuake() {
        return (WhereSummaryItem) wQuake.getClosestItemsByCount(1) [0];
    }

/** Returns closest significant earthquake summary data using currently set reference point. */
    public WhereSummaryItem getClosestEqPOI() {
        return (WhereSummaryItem) wEqPOI.getClosestItemsByCount(1) [0];
    }

/** Returns closest seismic station summary data using currently set reference point. */
    public WhereSummaryItem getClosestStation() {
        return (WhereSummaryItem) wStn.getClosestItemsByCount(1) [0];
    }

/** Returns closest fault summary data using currently set reference point. */
    public WhereSummaryItem getClosestFault() {
        return (WhereSummaryItem) wFault.getClosestItemsByCount(1) [0];
    }
}
