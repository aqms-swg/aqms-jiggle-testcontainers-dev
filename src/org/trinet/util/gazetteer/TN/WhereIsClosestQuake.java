package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;
import java.sql.*;
import java.util.*;
public class WhereIsClosestQuake extends WhereIsClosest {

    public WhereIsClosestQuake() {
    }
    public WhereIsClosestQuake(Connection conn) {
	super(conn);
    }
    public WhereIsClosestQuake(Connection conn, Geoidal reference) {
	super(conn, reference);
    }
    public WhereIsClosestQuake(Geoidal reference) {
	super(reference);
    }
    public WhereIsClosestQuake(double lat, double lon, double z) {
	super(lat, lon, z);
    }

/** Return Vector of WhereSummary for closest town constructed using gazetteer database data for town type. */
    protected Vector getDatabaseData() {
	String sql = "{ call WHERES.QUAKE(?,?,?,?,?,?,?) }";
	return super.getDatabaseData(sql);
    }
}
