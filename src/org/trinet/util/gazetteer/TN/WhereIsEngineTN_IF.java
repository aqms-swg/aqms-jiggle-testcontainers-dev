package org.trinet.util.gazetteer.TN;
public interface WhereIsEngineTN_IF {
    public String where(int lat, double latmin, int lon, double lonmin, double  z, int count) ;
    public String where(double lat, double lon, double z, int count) ;
    public String whereType(double lat, double lon, double z, int count, String type) ;
}
