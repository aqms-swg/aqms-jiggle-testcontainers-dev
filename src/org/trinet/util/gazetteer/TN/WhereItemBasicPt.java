package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;
/** Holds the data represented by a gazetteer table row object and 
* the DistanceAzimuthElevation from the gazetteer reference location to a target location.
* Includes a method to change the reference target.
* The equals(...), compareTo(...) methods are provided for distance comparison to enable sorting capability. 
* fromWhereString(...) formats the distance, azimuth and elevation data for summary reporting.
*/
public class WhereItemBasicPt extends WhereItem {

    public WhereItemBasicPt(BasicGazetteerPt data) {
	super(data);
    }

    public WhereItemBasicPt(Geoidal target, BasicGazetteerPt data) {
	super(target, data);
    }
}
