package org.trinet.util.gazetteer.TN;
import org.trinet.util.gazetteer.*;
import java.sql.*;
import java.util.*;
/** Polymorphic type extension of WhereBasicPt */
public class WhereVolcano extends WhereBasicPt {
    { prettyName = "volcano"; }

    public WhereVolcano() {
        super();
    }
    public WhereVolcano(Connection conn) {
        super(conn);
    }
    public WhereVolcano(Connection conn, Geoidal reference) {
        super(conn, reference);
    }
    public WhereVolcano(Geoidal reference) {
        super(reference);
    }
    public WhereVolcano(double lat, double lon, double z) {
        super(lat, lon, z);
    }

/** Returns Vector of WhereItems constructed using gazetteer database data for quarry type. */
    protected Vector getDatabaseData() {
        return super.getDatabaseData(SQL_QUERY_STRING, GazetteerType.getCode(GazetteerType.VOLCANO)); 
    }
}
