package org.trinet.util.gazetteer.TN;

import java.io.*;
import java.sql.*;
import java.util.*;

import org.postgresql.pljava.annotation.Function;
import org.trinet.jasi.DataSource; // DDG
import org.trinet.jdbc.JDBConn; // added for patch
import org.trinet.jiggle.common.DbConstant;
import org.trinet.jiggle.common.JiggleExceptionHandler;
import org.trinet.util.gazetteer.*;

// ddg 3/31/03 -- changed to use getConnection() and not CONNECTION // aww made it createConnection()
// so closed connections would get re-established when they are used.
// Also, setConnection().
public class WheresFrom extends WhereIsEngine implements WhereIsEngineTN_IF { //aww

  // All instances of a WhereIsEngine object will SHARE the static connection.
    protected static Connection CONNECTION;

    // Get a default connection from the static DataSource on object creation
    static {
      try {
        if (CONNECTION == null) CONNECTION = createConnection();
        if (CONNECTION == null) throw new NullPointerException("Warning: Default connection is null.");
        GazetteerType.init(CONNECTION);
      }
      catch (Exception ex) {
        System.err.println(ex.getMessage());
        ex.printStackTrace();
      }
    }

// calls to createConnection() get a new connection from DataSource if none is set
    protected static final WhereBasicPt wPoint = new WhereBasicPt(CONNECTION);
    protected static final WhereBasicTown wTown = new WhereBasicTown(CONNECTION);
    protected static final WhereBasicBigTown wBigTown = new WhereBasicBigTown(CONNECTION);
    protected static final WhereBasicEqPOI wEqPOI = new WhereBasicEqPOI(CONNECTION);
    protected static final WhereBasicQuarry wQuarry = new WhereBasicQuarry(CONNECTION);
    protected static final WhereBasicQuake wQuake = new WhereBasicQuake(CONNECTION);
    protected static final WhereStation wStn = new WhereStation(CONNECTION);
    protected static final WhereFault wFault = new WhereFault(CONNECTION);
    protected static final WhereVolcano wVolcano = new WhereVolcano(CONNECTION);

    protected static GeoidalUnits distanceUnits = GeoidalUnits.KILOMETERS;

/** Reference location */
    Geoidal reference;

    /** Return a new DataSource connection.
     * If existing static connection is not closed, closes the existing connection first. 
     * */
    public static Connection createConnection() {
      try {
          //if (CONNECTION != null && CONNECTION.isClosed()) CONNECTION = null; // gc the closed case;
          // for the dbase java server WHERES package if CONNECTION description not defined
          // you need to create default internal connection -aww
          //jdk1.4 version:
          //if (! DataSource.isNull()) System.out.println("DEBUG  WheresFrom  using client DataSource connection ....");
          //if (DataSource.isNull()) CONNECTION = DataSource.createDefaultDataSource().createInternalDbServerConnection();
          //else CONNECTION = (DataSource.isClosed()) ? DataSource.getNewConnect() : DataSource.getConnection();
          //Commented out above logic to share connection, we have to create "new" connection because otherwise
          //Jiggle freezes trying to updateWhereTab when creating a new MasterView in separate thread not sure why.

          if (CONNECTION != null && ! CONNECTION.isClosed()) {
            if (DataSource.isNull() || DataSource.getConnection() != CONNECTION) {
               CONNECTION.close();
               CONNECTION = null;
            }
          }

          if (! DataSource.isNull() && ! DataSource.isClosed()) {
              CONNECTION = DataSource.getConnection();
              /*
              if (CONNECTION instanceof JDBConn && !((JDBConn) CONNECTION).onServer())
                System.out.println("FYI: WheresFrom using current DataSource client connection ....");
              }
              */
          }
          else { // no datasource or it's closed
              if (DataSource.isNull()) { // assume serverside
                  CONNECTION = DataSource.createDefaultDataSource().createInternalDbServerConnection();
              }
              else if (DataSource.isClosed()) { // reset DataSource connection
                  CONNECTION = DataSource.getNewConnect(); // new one
                  if ( !DataSource.isNull() ) DataSource.set(CONNECTION);
              }
          }

          //patch for older server dbase java code:
          /*
          CONNECTION = (DataSource.getConnection() == null) ?
                        JDBConn.createDefaultConnection() : DataSource.getNewConnect();
                        //JDBConn.createInternalDbServerConnection() : DataSource.getNewConnect();
          */
          // end of patch
      }
      catch (SQLException ex) {
         System.err.println("WheresFrom createConnection error: "+ ex.getMessage());
      }
      return CONNECTION;
    }

    /** Set the a static connection object shared by all instances of WheresFrom. */
    public void setConnection(Connection conn) {
      if (! DataSource.isNull()) { // 2007/01/10 -aww
          Connection dsConn = DataSource.getConnection(); 
          if (dsConn != conn && conn != CONNECTION) closeConnection();
      }
      CONNECTION = conn;
      // aww, if you change it, you must also update the auxillary classes connection:
      wPoint.setConnection(CONNECTION);
      wTown.setConnection(CONNECTION);
      wBigTown.setConnection(CONNECTION);
      wEqPOI.setConnection(CONNECTION);
      wQuarry.setConnection(CONNECTION);
      wQuake.setConnection(CONNECTION);
      wStn.setConnection(CONNECTION);
      wFault.setConnection(CONNECTION);
      wVolcano.setConnection(CONNECTION);
      GazetteerType.init(CONNECTION);
    }

    public void closeConnection() { // 2007/01/10 -aww
      try {
        if (CONNECTION != null && ! CONNECTION.isClosed()) CONNECTION.close();
        CONNECTION = null;
      }
      catch (SQLException ex) {
          ex.printStackTrace();
      }
    }

    /** Returns the static connection object currently shared by all instances of WheresFrom. */
  // METHOD CALLED BY ORACLE STORED PROCEDURE !
    public Connection getConnection() {
      return CONNECTION;
    }

/** Returns the current WhereAmI object for the specified polymorphic type.
* Returns null if type is unknown.<BR>
* Example usage for a WhereAmI object, to recover a formatted String representation or the raw number data:
* <blockquote>
  <PRE>
*   wai = WheresFrom.getWhereType(GazettteerType.TOWN); <BR>
*   wai.setReference(lat, lon, z); // specify target (event location) <BR>
*   WhereSummary [] wi = wai.getClosestByCount(1); <BR>
*   String results = wi[0].toString(GeoidalUnits.MILES); // need to parse String for data items <BR>
*   DistanceAzimuthElevation dae = wi[0].getDistanceAzimuthElevation(); <BR>
*   double distance = dae.getDistanceMiles();    // getDistance() default is km <BR>
*       GazetteerData gd = wi.getGazetteerData();
*       String name = gd.getName();
  </PRE>
* </blockquote>
*/
  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static WhereAmI getWhereType(String type) {
      if (type.equalsIgnoreCase(GazetteerType.TOWN)) {
        return wTown;
      }
      else if (type.equalsIgnoreCase(GazetteerType.BIG_TOWN)) {
        return wBigTown;
      }
      else if (type.equalsIgnoreCase(GazetteerType.QUARRY)) {
        return wQuarry;
      }
      else if (type.equalsIgnoreCase(GazetteerType.QUAKE)) {
        return wQuake;
      }
      else if (type.equalsIgnoreCase(GazetteerType.STATION)) {
        return wStn;
      }
      else if (type.equalsIgnoreCase(GazetteerType.FAULT)) {
        return wFault;
      }
      else if (type.equalsIgnoreCase(GazetteerType.VOLCANO)) {
        return wVolcano;
      }
      else if (type.equalsIgnoreCase(GazetteerType.EQ_POI)) {
        return wEqPOI;
      }
      return wPoint;
    }


    /* added by DK 11/04/2002 to provide functionality spec'd in
     *  WhereIsEngine
     ************************************************************/
    public void setReference(double lat, double lon, double z)
    {
      this.reference = (Geoidal) new LatLonZ(lat, lon, z);
      wPoint.setReference(lat, lon, z);
      wTown.setReference(lat, lon, z);
      wBigTown.setReference(lat, lon, z);
      wEqPOI.setReference(lat, lon, z);
      wQuarry.setReference(lat, lon, z);
      wQuake.setReference(lat, lon, z);
      wStn.setReference(lat, lon, z);
      wFault.setReference(lat, lon, z);
      wVolcano.setReference(lat, lon, z);
    }


    public WhereSummary whereSummaryType(String type)
    {
      WhereAmI wai = WheresFrom.getWhereType(type);
      if (wai == null) return null;
      WhereSummary [] wi = wai.getClosestItemsByCount(1);
      if(wi == null || wi.length == 0)
        return(null);
      else
        return(wi[0]);
    }

    public Collection whereSummary() {
      Vector whereSummaryList = new Vector();

      whereSummaryList.add(whereSummaryType(GazetteerType.TOWN));
      whereSummaryList.add(whereSummaryType(GazetteerType.BIG_TOWN));
      whereSummaryList.add(whereSummaryType(GazetteerType.EQ_POI));
      whereSummaryList.add(whereSummaryType(GazetteerType.QUARRY));
      whereSummaryList.add(whereSummaryType(GazetteerType.QUAKE));
      whereSummaryList.add(whereSummaryType(GazetteerType.STATION));
      whereSummaryList.add(whereSummaryType(GazetteerType.FAULT));
      whereSummaryList.add(whereSummaryType(GazetteerType.VOLCANO));
      return(whereSummaryList);
    }

/* True == database REMARK columns is included in output strings. */
    public static void setIncludeRemark(boolean includeRemark) {
      wPoint.setIncludeRemark(includeRemark);
      wTown.setIncludeRemark(includeRemark);
      wBigTown.setIncludeRemark(includeRemark);
      wQuarry.setIncludeRemark(includeRemark);
      wQuake.setIncludeRemark(includeRemark);
      wStn.setIncludeRemark(includeRemark);
      wFault.setIncludeRemark(includeRemark);
      wVolcano.setIncludeRemark(includeRemark);
      wEqPOI.setIncludeRemark(includeRemark);
    }

/** GeoidalUnits.MILES == distances are reported in miles in the output text strings.
* GeoidalUnits.KILOMETERS (default) == distances reported in kilometers in the output text strings. */
  // METHOD CALLED BY ORACLE STORED PROCEDURE !
    public void setDistanceUnits(GeoidalUnits units) {
      wPoint.setDistanceUnits(units);
      wTown.setDistanceUnits(units);
      wBigTown.setDistanceUnits(units);
      wEqPOI.setDistanceUnits(units);
      wQuarry.setDistanceUnits(units);
      wQuake.setDistanceUnits(units);
      wStn.setDistanceUnits(units);
      wFault.setDistanceUnits(units);
      wVolcano.setDistanceUnits(units);
      distanceUnits = units;
    }
    public void setDistanceUnitsKm() {
      setDistanceUnits(GeoidalUnits.KILOMETERS);
    }
    public void setDistanceUnitsMiles() {
      setDistanceUnits(GeoidalUnits.MILES);
    }

/** Returns String describing where the specified location is relative to closest database entry for all known database types. */
    public String where() {
      return(where(reference.getLat(), reference.getLon(), reference.getZ()));
    }

/** Returns String describing where the specified location is relative to closest database entry for all known database types. */
  // METHOD CALLED BY ORACLE STORED PROCEDURE !
    public String where(double lat, double lon) {
      return where(lat, lon, 0., 1);
    }
/** Returns String describing where the specified location is relative to the specified number of database entries
* for all known database types.
*/
    public String where(double lat, double lon, int number) {
      return where(lat, lon, 0., number);
    }
/** Returns String describing where the specified location is relative to closest database entry for all known database types.
* Uses z to determine elevation.
*/
    public String where(double lat, double lon, double z) {
      return where(lat, lon, z, 1);
    }
/** Returns String describing where the specified location (int degrees and decimal minutes) is relative to the input number
* of database entries for all known types.  Uses z to determine elevation.
*/
  // METHOD CALLED BY ORACLE STORED PROCEDURE !
    public String where(int lat, double latmin, int lon, double lonmin, double  z, int number) {
      return where(GeoidalConvert.toDecimalDegrees(lat, latmin), GeoidalConvert.toDecimalDegrees(lon, lonmin),
        z, number);
    }
/** Returns String describing where the specified location is relative to input number of database entries for
*subset of known database types. Uses z to determine elevation.
*/
  // METHOD CALLED BY ORACLE STORED PROCEDURE !
    public String where(double lat, double lon, double z, int number) {
      StringBuffer sb = new StringBuffer(2048);
      sb.append(wTown.from(lat, lon, z, number));
      sb.append("\n");
      sb.append(wBigTown.from(lat, lon, z, number));
      sb.append("\n");
      //sb.append(wEqPOI.from(lat, lon, z, number));
      //sb.append("\n");
      sb.append(wQuarry.from(lat, lon, z, number));
      sb.append("\n");
      sb.append(wQuake.from(lat, lon, z, number));
      sb.append("\n");
      sb.append(wStn.from(lat, lon, z, number));
      sb.append("\n");
      sb.append(wFault.from(lat, lon, z, number));
      sb.append("\n");
      sb.append(wVolcano.from(lat, lon, z, number));

      return sb.toString();
    }

/** Returns String describing where the specified location is relative to the input number of database entries
* for the input database type. Uses z to determine elevation.
*/
    public String whereType(double lat, double lon, double z, String type) {
      return(whereType(lat,lon,z,1,type));
    }

/** Returns String describing where the specified location is relative to the input number of database entries
* for the input database type. Uses z to determine elevation.
*/
  // METHOD CALLED INDIRECTLY BY ORACLE STORED PROCEDURE !
    public String whereType(double lat, double lon, double z, int number, String type) {
      if (type.equalsIgnoreCase(GazetteerType.TOWN)) {
        return wTown.from(lat, lon, z, number);
      }
      else if (type.equalsIgnoreCase(GazetteerType.BIG_TOWN)) {
        return wBigTown.from(lat, lon, z, number);
      }
      else if (type.equalsIgnoreCase(GazetteerType.EQ_POI)) {
        return wEqPOI.from(lat, lon, z, number);
      }
      else if (type.equalsIgnoreCase(GazetteerType.QUARRY)) {
        return wQuarry.from(lat, lon, z, number);
      }
      else if (type.equalsIgnoreCase(GazetteerType.QUAKE)) {
        return wQuake.from(lat, lon, z, number);
      }
      else if (type.equalsIgnoreCase(GazetteerType.STATION)) {
        return wStn.from(lat, lon, z, number);
      }
      else if (type.equalsIgnoreCase(GazetteerType.FAULT)) {
        return wFault.from(lat, lon, z, number);
      }
      else if (type.equalsIgnoreCase(GazetteerType.VOLCANO)) {
        return wVolcano.from(lat, lon, z, number);
      }

      wPoint.setTypeId(GazetteerType.getCode(type));

      return wPoint.from(lat, lon, z, number);

    }

    public String whereType(String type) {
      if (type.equalsIgnoreCase(GazetteerType.TOWN)) {
        return wTown.reportClosestByCount(1);
      }
      else if (type.equalsIgnoreCase(GazetteerType.BIG_TOWN)) {
        return wBigTown.reportClosestByCount(1);
      }
      else if (type.equalsIgnoreCase(GazetteerType.EQ_POI)) {
        return wEqPOI.reportClosestByCount(1);
      }
      else if (type.equalsIgnoreCase(GazetteerType.QUARRY)) {
        return wQuarry.reportClosestByCount(1);
      }
      else if (type.equalsIgnoreCase(GazetteerType.QUAKE)) {
        return wQuake.reportClosestByCount(1);
      }
      else if (type.equalsIgnoreCase(GazetteerType.STATION)) {
        return wStn.reportClosestByCount(1);
      }
      else if (type.equalsIgnoreCase(GazetteerType.FAULT)) {
        return wFault.reportClosestByCount(1);
      }
      else if (type.equalsIgnoreCase(GazetteerType.VOLCANO)) {
        return wVolcano.reportClosestByCount(1);
      }

      wPoint.setTypeId(GazetteerType.getCode(type));

      return wPoint.reportClosestByCount(1);
      
    }

  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static final void getClosestPoint(String type, double lat, double lon, double z,
         double [] dist, double [] az, double [] elev, String [] place) {
      wPoint.setReference(lat, lon, z);
      wPoint.setTypeId(GazetteerType.getCode(type));
      WhereSummary []  whereItem = wPoint.getClosestItemsByCount(1);
      if (whereItem == null) return;
      setOutputParameters(whereItem[0], dist, az, elev, place);
    }

  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static final void getClosestTown(double lat, double lon, double z,
         double [] dist, double [] az, double [] elev, String [] place) {
      wTown.setReference(lat, lon, z);
      WhereSummary []  whereItem = wTown.getClosestItemsByCount(1);
      if (whereItem == null) return;
      setOutputParameters(whereItem[0], dist, az, elev, place);
    }

    // Called by PostgreSQL using PL/Java
    public static final void getClosestTown(double lat, double lon, double z, ResultSet out) {
        wTown.setReference(lat, lon, z);
        WhereSummary []  whereItem = wTown.getClosestItemsByCount(1);
        if (whereItem == null) {
            return;
        }
        setOutputParameters(whereItem[0], out);
    }

    // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static final void getClosestBigTown(double lat, double lon, double z,
         double [] dist, double [] az, double [] elev, String [] place) {
      wBigTown.setReference(lat, lon, z);
      WhereSummary []  whereItem = wBigTown.getClosestItemsByCount(1);
      if (whereItem == null) return;
      setOutputParameters(whereItem[0], dist, az, elev, place);
    }

    // Called by PostgreSQL using PL/Java
    public static final void getClosestBigTown(double lat, double lon, double z, ResultSet out) {
        wBigTown.setReference(lat, lon, z);
        WhereSummary []  whereItem = wBigTown.getClosestItemsByCount(1);
        if (whereItem == null) {
            return;
        }
        setOutputParameters(whereItem[0], out);
    }

    // Called by PostgreSQL using PL/Java
    public static final void getClosestEqPOI(double lat, double lon, double z, ResultSet out) {
        wEqPOI.setReference(lat, lon, z);
        WhereSummary []  whereItem = wEqPOI.getClosestItemsByCount(1);
        if (whereItem == null) {
            return;
        }
        setOutputParameters(whereItem[0], out);
    }

  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static final void getClosestEqPOI(double lat, double lon, double z,
         double [] dist, double [] az, double [] elev, String [] place) {
      wEqPOI.setReference(lat, lon, z);
      WhereSummary []  whereItem = wEqPOI.getClosestItemsByCount(1);
      if (whereItem == null) return;
      setOutputParameters(whereItem[0], dist, az, elev, place);
    }

    // Called by PostgreSQL using PL/Java
    public static final void getClosestQuarry(double lat, double lon, double z, ResultSet out) {
        wQuarry.setReference(lat, lon, z);
        WhereSummary []  whereItem = wQuarry.getClosestItemsByCount(1);
        if (whereItem == null) return;
        setOutputParameters(whereItem[0], out);
    }

  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static final void getClosestQuarry(double lat, double lon, double z,
         double [] dist, double [] az, double [] elev, String [] place) {
      wQuarry.setReference(lat, lon, z);
      WhereSummary []  whereItem = wQuarry.getClosestItemsByCount(1);
      if (whereItem == null) return;
      setOutputParameters(whereItem[0], dist, az, elev, place);
    }

    // Called by PostgreSQL using PL/Java
    public static final void getClosestQuake(double lat, double lon, double z, ResultSet out) {
      wQuake.setReference(lat, lon, z);
      WhereSummary []  whereItem = wQuake.getClosestItemsByCount(1);
      if (whereItem == null) {
          return;
      }
      setOutputParameters(whereItem[0], out);
    }

    // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static final void getClosestQuake(double lat, double lon, double z,
                                             double [] dist, double [] az, double [] elev, String [] place) {
        wQuake.setReference(lat, lon, z);
        WhereSummary []  whereItem = wQuake.getClosestItemsByCount(1);
        if (whereItem == null) return;
        setOutputParameters(whereItem[0], dist, az, elev, place);
    }

    // Called by PostgreSQL using PL/Java
    public static final void getClosestStation(double lat, double lon, double z, ResultSet out) {
        wStn.setReference(lat, lon, z);
        WhereSummary []  whereItem = wStn.getClosestItemsByCount(1);
        if (whereItem == null) {
            return;
        }
        setOutputParameters(whereItem[0], out);
    }

  // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static final void getClosestStation(double lat, double lon, double z,
         double [] dist, double [] az, double [] elev, String [] place) {
      wStn.setReference(lat, lon, z);
      WhereSummary []  whereItem = wStn.getClosestItemsByCount(1);
      if (whereItem == null) return;
      setOutputParameters(whereItem[0], dist, az, elev, place);
    }

    // Called by PostgreSQL using PL/Java
    public static final void getClosestFault(double lat, double lon, double z, ResultSet out) {
      wFault.setReference(lat, lon, z);
      WhereSummary []  whereItem = wFault.getClosestItemsByCount(1);
      if (whereItem == null) {
          return;
      }
      setOutputParameters(whereItem[0], out);
    }

    // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static final void getClosestFault(double lat, double lon, double z,
                                             double [] dist, double [] az, double [] elev, String [] place) {
        wFault.setReference(lat, lon, z);
        WhereSummary []  whereItem = wFault.getClosestItemsByCount(1);
        if (whereItem == null) return;
        setOutputParameters(whereItem[0], dist, az, elev, place);
    }

    // Called by PostgreSQL using PL/Java
    @Function(schema= DbConstant.SCHEMA_WHERES, name="volcano", type="RECORD")
    public static final boolean getClosestVolcano(double lat, double lon, double z, ResultSet out) {
      wVolcano.setReference(lat, lon, z);
      WhereSummary []  whereItem = wVolcano.getClosestItemsByCount(1);
      if (whereItem == null) {
          return false;
      }
      setOutputParameters(whereItem[0], out);
      return true;
    }

    // STATIC METHOD CALLED BY ORACLE STORED PROCEDURE !
    public static final void getClosestVolcano(double lat, double lon, double z,
                                               double [] dist, double [] az, double [] elev, String [] place) {
        wVolcano.setReference(lat, lon, z);
        WhereSummary []  whereItem = wVolcano.getClosestItemsByCount(1);
        if (whereItem == null) return;
        setOutputParameters(whereItem[0], dist, az, elev, place);
    }

    private static void setOutputParameters(WhereSummary whereItem, double [] dist, double [] az, double [] elev, String [] place) {
      DistanceAzimuthElevation distanceAzimuthElevation = whereItem.getDistanceAzimuthElevation();
      dist[0] = distanceAzimuthElevation.getDistance(distanceUnits);
      az[0] = distanceAzimuthElevation.getAzimuth();
      elev[0] = distanceAzimuthElevation.getElevation();
      place[0] = whereItem.fromPlaceString(true);
    }

    // Used by PL/Java to return out parameters
    private static void setOutputParameters(WhereSummary whereItem, ResultSet out) {
        DistanceAzimuthElevation distanceAzimuthElevation = whereItem.getDistanceAzimuthElevation();

        try {
            out.updateDouble(1, distanceAzimuthElevation.getDistance(distanceUnits));
            out.updateDouble(2, distanceAzimuthElevation.getAzimuth());
            out.updateDouble(3, distanceAzimuthElevation.getElevation());
            out.updateString(4, whereItem.fromPlaceString(true));
        } catch (SQLException ex) {
            JiggleExceptionHandler.logError(ex, "Error setOutputParameters in WheresFrom.");
        }
    }

    public double kmToFault(double lat, double lon, String name) {
        wFault.setReference(lat, lon, 0);
        return wFault.kmToFault(name);
    }

/*
    public static String whereTown(double lat, double lon) {
      return whereTown(lat, lon, 0., 1);
    }
    public static String whereTown(double lat, double lon, double z) {
      return whereTown(lat, lon, z, 1);
    }
    public static String whereTown(double lat, double lon, int number) {
      return whereTown(lat, lon, 0., number);
    }
    public static String whereTown(int lat, double latmin, int lon, double lonmin, double  z, int number) {
      return whereTown(GeoidalConvert.toDecimalDegrees(lat, latmin), GeoidalConvert.toDecimalDegrees(lon, lonmin), z, number);
    }
    public static String whereTown(double lat, double lon, double z, int number) {
      return whereType(lat, lon, z, number, GazetteerType.TOWN);
    }

    public static String whereQuarry(double lat, double lon) {
      return whereQuarry(lat, lon, 0., 1);
    }
    public static String whereQuarry(double lat, double lon, double z) {
      return whereQuarry(lat, lon, z, 1);
    }
    public static String whereQuarry(double lat, double lon, int number) {
      return whereQuarry(lat, lon, 0., number);
    }
    public static String whereQuarry(int lat, double latmin, int lon, double lonmin, double  z, int number) {
      return whereQuarry(GeoidalConvert.toDecimalDegrees(lat, latmin), GeoidalConvert.toDecimalDegrees(lon, lonmin), z, number);
    }
    public static String whereQuarry(double lat, double lon, double z, int number) {
      return whereType(lat, lon, z, number, GazetteerType.QUARRY);
    }

    public static String whereQuake(double lat, double lon) {
      return whereQuake(lat, lon, 0., 1);
    }
    public static String whereQuake(double lat, double lon, double z) {
      return whereQuake(lat, lon, z, 1);
    }
    public static String whereQuake(double lat, double lon, int number) {
      return whereQuake(lat, lon, 0., number);
    }
    public static String whereQuake(int lat, double latmin, int lon, double lonmin, double  z, int number) {
      return whereQuake(GeoidalConvert.toDecimalDegrees(lat, latmin), GeoidalConvert.toDecimalDegrees(lon, lonmin), z, number);
    }
    public static String whereQuake(double lat, double lon, double z, int number) {
      return whereType(lat, lon, z, number, GazetteerType.QUAKE);
    }
*/
}
