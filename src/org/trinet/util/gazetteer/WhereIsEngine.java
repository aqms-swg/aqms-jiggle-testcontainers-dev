package org.trinet.util.gazetteer;
import java.io.*;
import java.util.Collection;
import java.sql.Connection;
import org.trinet.jasi.JasiObject;
import org.trinet.util.gazetteer.TN.*;
import org.trinet.util.gazetteer.EW.*;

/** Utility used by client applications to obtain the closest geographic types relative to a specified reference point.
*  Example usage:
<PRE>
        .
        .
        .

        WhereIsEngine wicf = WhereIsEngine.create("org.trinet.util.gazetteer.TN.WheresFrom");

        wicf.setReference(34.4801,-119.5104, 0.); // need to set source point to measure to relative to database data.

        wicf.setDistanceUnits(GeoidalUnits.MILES); // sets these units for only for where methods.
        System.out.println(wicf.where(34.4801,-119.5104));
        System.out.print(wicf.whereType(34.3103, -117.6218, 0., GazetteerType.TOWN));

        WhereSummaryItem wsi= wicf.getClosestTown();
        System.out.println(wsi.fromWhereString(GeoidalUnits.KILOMETERS, false));
        DistanceAzimuthElevation dae = wsi.getDistanceAzimuthElevation();
        System.out.println("distance: " + dae.getDistance(GeoidalUnits.MILES) + " miles.");

        .
        .
        .

        wicf.closeStatements(); // release database resource when done.
</PRE>
*/
public abstract class WhereIsEngine implements WhereIsEngineIF {

    protected static String defaultEngineClassName = null;


    /** Factory methods to create an subclass instance of this abstract type.
    * Creates an instance described by defaultEngineClassName if not null,
    * otherwise an instance of the invoking network's default
    * implementation as flagged by the value of JasiObject.DEFAULT.
    * @see #setDefaultEngineClassName(String)
    * @see JasiObject
    * */
    public static WhereIsEngine create() {
      WhereIsEngine eng = null;
      try {
        if (defaultEngineClassName == null) eng = create(JasiObject.DEFAULT);
        else {
          eng = create(defaultEngineClassName);
        }
      }
      catch (IllegalArgumentException ex) {
        System.err.println("Not a known subtype value: " + JasiObject.DEFAULT);
      }
      return eng;
    }

    public static WhereIsEngine create(int subtype) throws IllegalArgumentException {
      WhereIsEngine eng = null;
      switch (subtype) {
        case JasiObject.TRINET:
          eng = create("org.trinet.util.gazetteer.TN.WheresFrom");
          break;
        case JasiObject.EARTHWORM:
          eng = create("org.trinet.util.gazetteer.EW.WhereIsEW");
          break;
        default:
          //System.err.println("Not a known subtype value: " + subtype);
          throw new IllegalArgumentException("Not a known subtype value: " + subtype);
      }
      return eng;
    }

    /* Real Construction Method */
    public static WhereIsEngine create(String sClassName) {
        WhereIsEngine newEng = null;

        try {
            newEng =  (WhereIsEngine)Class.forName(sClassName).newInstance();
            defaultEngineClassName = sClassName;
        }
        catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        catch (InstantiationException ex) {
            ex.printStackTrace();
        }
        catch (IllegalAccessException ex) {
            ex.printStackTrace();
        }

        return newEng;
    }

    public static void setDefaultEngineClassName(String className) {
        defaultEngineClassName = className;
    }

/** GeoidalUnits.MILES == distances are reported in miles in the where(...) method output text strings.
* GeoidalUnits.KILOMETERS (default) == distances reported in kilometers in the output text strings. */
    public abstract void setDistanceUnits(GeoidalUnits units) ;

    public void setDistanceUnitsKm() {
        setDistanceUnits(GeoidalUnits.KILOMETERS);
    }
    public void setDistanceUnitsMiles() {
        setDistanceUnits(GeoidalUnits.MILES);
    }

/** Returns String describing where the specified WhereIs is relative to closest database entry for all known database types. */
    public String where(double lat, double lon) {
        return where(lat, lon, 0.);
    }
/** Returns String describing where the specified WhereIs (int degrees and decimal minutes) is relative to the closest
* database entry for all known types.  Uses z to determine elevation.<p>
* Example:<br>
* 7.19 mi (ESE 52.2° Elv) from Anza, CA <br>
* 0.55 mi (W   86.6° Elv) from San Jacinto <p>
* Note that the string has a trailing \n.
*/
    public String where(int lat, double latmin, int lon, double lonmin, double  z) {
        return where(GeoidalConvert.toDecimalDegrees(lat, latmin), GeoidalConvert.toDecimalDegrees(lon, lonmin),
                z);
    }
/** Returns String describing where the specified WhereIs is relative to closest database entry for all known database types.
* Uses z to determine elevation.
*/
    public abstract String where(double lat, double lon, double z);

/** Returns String describing where the specified WhereIs is relative to the closest database entry
* for the specified input database type. Uses z to determine elevation.
*/
    public abstract String whereType(double lat, double lon, double z, String type);

/** Returns String describing where the specified WhereIs is relative to closest database entry for all known database types.
* Uses z to determine elevation.
*/
    public abstract String where();

/** Returns String describing where the specified WhereIs is relative to the closest database entry
* for the specified input database type. Uses z to determine elevation.
*/
    public abstract String whereType(String type);

/** Returns a Collection of WhereSummarys describing closest database entries for all known database types,
 *  from the current point of reference
*/
    public abstract Collection whereSummary();

/** Returns a WhereSummary describing the closest database entry for the given type,
 *  from the current point of reference.
*/
    public abstract WhereSummary whereSummaryType(String type);


/** Sets this object's geographic reference source point.
*/
    public void setReference(Geoidal latLonZ)  {
        setReference(latLonZ.getLat(), latLonZ.getLon(), latLonZ.getZ());
    }
/** Sets this object's geographic reference source point.
*/
    public void setReference(double lat, double lon)  {
        setReference(lat, lon, 0.);
    }
/** Sets this object's geographic reference source point.
*   Distance, azimuth and elevation are calculated from the Gazetteer database points to this reference point.
*/
    public abstract void setReference(double lat, double lon, double z);

/** Closes all the JDBC callable statements used by this object.*/
    public void closeStatements() {
    }

    abstract public void setConnection(Connection conn);
}
