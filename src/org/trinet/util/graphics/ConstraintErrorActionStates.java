package org.trinet.util.graphics;

public class ConstraintErrorActionStates {

    protected boolean logOnError   = true;
    protected boolean audioOnError = true;
    protected boolean resetOnError = true;
    protected boolean popupOnError = true;

    public ConstraintErrorActionStates () {}

    public ConstraintErrorActionStates (boolean log, boolean audio, boolean reset, boolean popup) {
        setErrorActionStates(log, audio, reset, popup);
    }

/** Enable/Disable state values. */
    public void setErrorActionStates(boolean log, boolean audio, boolean reset, boolean popup) {
        logOnError = log;
        audioOnError = audio;
        resetOnError = reset;
        popupOnError = popup;
    }

/** Enable/Disable error message logging to System.err. */
    public void setLogOnError(boolean value) { this.logOnError = value;}
    public boolean isLogOnError() { return logOnError;}

/** Enable/disable automatic reset of input field when found in violation of limits. */
    public void setAudioOnError(boolean value) { this.audioOnError = value;}
    public boolean isAudioOnError() { return audioOnError;}

/** Enable/disable automatic reset of input field when found in violation of limits. */
    public void setResetOnError(boolean value) { this.resetOnError = value;}
    public boolean isResetOnError() { return resetOnError;}

/** Enable/Disable popup information dialogs when input bounds error occurs. */
    public void setPopupOnError(boolean value) { this.popupOnError = value;}
    public boolean isPopupOnError() { return popupOnError;}

    public String toString() {
        return "log: " + logOnError + " audio: " + audioOnError + " reset: " + resetOnError + " popup: " +  popupOnError;
    }
}
