package org.trinet.util.graphics;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class DataSourceDialog extends JDialog {
    private static final int DEFAULT_FIELD_LENGTH = 15;
    private int fieldLength;
    //private JTextField profileField;  // TODO: a combobox loaded from user prefs
    private JTextField hostField;
    private JTextField domainField;
    private JTextField dbNameField;
    private JTextField schemaField;
    private JPasswordField passwordField;
    private boolean okPressed = false;

    public DataSourceDialog(JFrame aFrame) {
        this(aFrame, "DataSource Data", DEFAULT_FIELD_LENGTH);
    }

    public DataSourceDialog(JFrame aFrame,String dialogTitle,int fieldLength) {
        super(aFrame);
        setTitle(dialogTitle);
        setFieldLength(fieldLength);
        if (aFrame != null)  {
          setDisposeOnWindowClosing(aFrame);
        }
        else setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        makeDialogComponent();
    }
    public void setDisposeOnWindowClosing(Window aWindow) {
      setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
      aWindow.addWindowListener( new WindowAdapter() {
        public void windowClosing(WindowEvent e) {
          dispose();
        }
      });
    }
    public void setFieldLength(int length) {
        fieldLength = length;
    }
    public int getFieldLength() {
        return fieldLength;
    }
    /*
    public String getProfile() {
        return profileField.getText();
    }
    */
    public String getHost() {
        return hostField.getText();
    }
    public String getDomain() {
        return domainField.getText();
    }
    public String getDbaseName() {
        return dbNameField.getText();
    }
    public String getSchema() {
        return schemaField.getText();
    }
    public String getPassword() {
        return new String(passwordField.getPassword());
    }
    /*
    public void setProfile(String name) {
        profileField.setText(name);
        profileField.selectAll();
    }
    */
    public void setHost(String host) {
        hostField.setText(host);
        hostField.selectAll();
    }
    public void setDbaseName(String dbName) {
        dbNameField.setText(dbName);
        dbNameField.selectAll();
    }
    public void setDomain(String domain) {
        domainField.setText(domain);
        domainField.selectAll();
    }
    public void setSchema(String schema) {
        schemaField.setText(schema);
        schemaField.selectAll();
    }
    public void setPassword(String password) {
        passwordField.setText(password);
        passwordField.selectAll();
    }
    private void makeDialogComponent() {
        Dimension dd = new Dimension(100,20);
        Dimension dl = new Dimension(80,20);
        /*
        profileField = new JTextField(fieldLength);
        profileField.requestFocus();
        profileField.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            hostField.requestFocus();
          }
        });
        */

        hostField = new JTextField(fieldLength);
        hostField.setPreferredSize(dd);
        hostField.requestFocus();
        hostField.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            domainField.requestFocus();
          }
        });

        domainField = new JTextField(fieldLength);
        domainField.setPreferredSize(dd);
        domainField.requestFocus();
        domainField.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            dbNameField.requestFocus();
          }
        });

        dbNameField = new JTextField(fieldLength);
        dbNameField.setPreferredSize(dd);
        dbNameField.requestFocus();
        dbNameField.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            schemaField.requestFocus();
          }
        });

        passwordField = new JPasswordField(fieldLength);
        passwordField.setPreferredSize(dd);

        schemaField = new JTextField(fieldLength);
        schemaField.setPreferredSize(dd);
        schemaField.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            passwordField.requestFocus();
          }
        });

        Box aBox2 = Box.createVerticalBox();
        aBox2.add(Box.createVerticalGlue());

        Box aBox = Box.createHorizontalBox();
        JLabel aLabel = new JLabel("Host:");
        aBox.add(Box.createHorizontalGlue());
        aLabel.setPreferredSize(dl);
        aLabel.setToolTipText("Data source host machine.");
        aBox.add(aLabel);
        aBox.add(hostField);
        aBox2.add(aBox);

        aBox = Box.createHorizontalBox();
        aLabel = new JLabel("Domain:");
        aBox.add(Box.createHorizontalGlue());
        aLabel.setPreferredSize(dl);
        aLabel.setToolTipText("IP domain of host machine.");
        aBox.add(aLabel);
        aBox.add(domainField);
        aBox2.add(aBox);

        aBox = Box.createHorizontalBox();
        aLabel = new JLabel("Db name:");
        aBox.add(Box.createHorizontalGlue());
        aLabel.setPreferredSize(dl);
        aLabel.setToolTipText("Database name on host machine.");
        aBox.add(aLabel);
        aBox.add(dbNameField);
        aBox2.add(aBox);

        aBox = Box.createHorizontalBox();
        aLabel = new JLabel("User:");
        aBox.add(Box.createHorizontalGlue());
        aLabel.setPreferredSize(dl);
        aLabel.setToolTipText("Data source user (name/account/schema).");
        aBox.add(aLabel);
        aBox.add(schemaField);
        aBox2.add(aBox);

        aBox = Box.createHorizontalBox();
        aLabel = new JLabel("Password:");
        aBox.add(Box.createHorizontalGlue());
        aLabel.setPreferredSize(dl);
        aLabel.setToolTipText("Data source user account password");
        aBox.add(aLabel);
        aBox.add(passwordField);
        aBox2.add(aBox);
        aBox2.add(Box.createVerticalGlue());

        Box bbox = Box.createHorizontalBox();
        bbox.add(Box.createHorizontalGlue());
        JButton okButton = new JButton("OK");
        okButton.addActionListener( new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            okPressed = true;
            if (getDefaultCloseOperation() == JDialog.HIDE_ON_CLOSE) setVisible(false);
            else dispose();
          }
        });
        bbox.add(okButton);
        bbox.add(Box.createHorizontalGlue());

        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(aBox2, BorderLayout.CENTER);
        contentPane.add(bbox, BorderLayout.SOUTH);

        pack();
    }
    public boolean okPressed() { return okPressed; }

  /*
  static public final class Tester {
    public static final void main(String args[]) {
        //JFrame aFrame = new JFrame("A test frame");
        //aFrame.setSize(100,100);
        //aFrame.setVisible(true);
        //aFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        DataSourceDialog dsd = new DataSourceDialog(null);
        dsd.setVisible(true);
    }
  }
  */
}
