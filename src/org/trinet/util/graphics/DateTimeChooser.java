package org.trinet.util.graphics;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import org.trinet.util.*;

/**
 * Collection of JComboBoxes for specifying a DateTime; YR MO DY HR MN SEC
 */

public class DateTimeChooser extends JPanel {

    public boolean wholeSecs = true;

    public static final int DEFAULT_YRS_BACK = 10;
    public static final int DEFAULT_YRS_FORWARD = 0;

    private int oldYear = 0;
    private int yrsBack    = DEFAULT_YRS_BACK;
    private int yrsForward = DEFAULT_YRS_FORWARD;
    private boolean mutate = false;

    private DateTime defaultTime = new DateTime(); // now UTC time

    private NumberChooser yrChooser;
    private MonthChooser moChooser;
    private NumberChooser dyChooser;
    private NumberChooser hrChooser;
    private NumberChooser mnChooser;
    private NumberChooser secChooser;

    private String borderTitle;

    protected volatile boolean isAdjusting = false;

/**
 * constructor: sets default time to current time
 */ 
    public DateTimeChooser() {
        try  {
            jbInit();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }
/**
 * constructor: sets default time to current time. String = border title.
 */ 
    public DateTimeChooser(String title) {
        this(null, title);
    }
/**
 * Constructor that sets initial values to those in DateTime object
 */
    public DateTimeChooser(DateTime dt) {
        this(dt, null);
    }

/**
 * Constructor that sets initial values to those in DateTime object. String = border title.
 */
    public DateTimeChooser(DateTime dt, String title) {
        this(dt,title, DEFAULT_YRS_BACK, DEFAULT_YRS_FORWARD, false);
    }

/**
 * Constructor that sets initial values to those in DateTime object. String = border title.
 */
    public DateTimeChooser(DateTime dt, String title, int yrsBack, int yrsForward, boolean mutate) {
        if (dt != null)  defaultTime = dt;
        if (title != null) setTitle(title);
        this.yrsBack = yrsBack;
        this.yrsForward = yrsForward;
        this.mutate = mutate;

        try  {
            jbInit();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public void setMutate(boolean tf) {
        mutate = tf;
    }

    public void setTitle(String title) {
        borderTitle = title;
    }

    public void setTime(DateTime dt) {
        if (getDateTime().equals(dt)) return;
        synchronized (this) {
            isAdjusting = true;
            defaultTime = dt;
            yrChooser.setSelectedItem(String.valueOf(defaultTime.getYear()));
            moChooser.setSelectedIndex(defaultTime.getMonth());
            dyChooser.setSelectedItem(String.valueOf(defaultTime.getDay()));
            hrChooser.setSelectedItem(String.valueOf(defaultTime.getHour()));
            mnChooser.setSelectedItem(String.valueOf(defaultTime.getMinute()));
            if (wholeSecs) secChooser.setSelectedItem(String.valueOf(defaultTime.getSecond()));
            else secChooser.setSelectedItem(defaultTime.getSecondsStringToPrecisionOf(2));
            isAdjusting = false;
        }
    }

    public void setNominalSeconds(double nominalSec) {
        if (getNominalSeconds() == nominalSec) return;
        synchronized (this) {
            isAdjusting = true;
            String str = EpochTime.epochToString(nominalSec, "yyyyMMddHHmmss.fff");
            yrChooser.setSelectedItem(str.substring(0,4));
            moChooser.setSelectedIndex(Integer.parseInt(str.substring(4,6))-1);
            dyChooser.setSelectedItem(str.substring(6,8));
            hrChooser.setSelectedItem(str.substring(8,10));
            mnChooser.setSelectedItem(str.substring(10,12));
            if (wholeSecs) secChooser.setSelectedItem(str.substring(12,14));
            else secChooser.setSelectedItem(new Format("%05.2f").form(Float.parseFloat(str.substring(12,18))));
            isAdjusting = false;
        }
    }

    public void setTrueSeconds(double trueSec) { // added -aww 2008/02/04
        if (getTrueSeconds() == trueSec) return;
        synchronized (this) {
            isAdjusting = true;
            String str = LeapSeconds.trueToString(trueSec, "yyyyMMddHHmmss.fff");
            yrChooser.setSelectedItem(str.substring(0,4));
            moChooser.setSelectedIndex(Integer.parseInt(str.substring(4,6))-1);
            dyChooser.setSelectedItem(str.substring(6,8));
            hrChooser.setSelectedItem(str.substring(8,10));
            mnChooser.setSelectedItem(str.substring(10,12));

            if (wholeSecs) secChooser.setSelectedItem(str.substring(12,14));
            else secChooser.setSelectedItem(new Format("%05.2f").form( Float.parseFloat(str.substring(12,18))));
            isAdjusting = false;
        }
    }

    private void setNewYearChooserModel(int newYear) {
        //if (newYear == oldYear || yrChooser.getSelectedIndex() >= 0) return;
        if (newYear == oldYear) return;
        //System.out.println("newYear : " + newYear + "yrsBack " + yrsBack + " yrsForward: " + yrsForward);
        yrsForward = Math.min(defaultTime.getYear()-newYear, yrsBack); 
        ArrayList aList = new ArrayList(yrsBack+yrsForward+1);
        int count = yrsBack+yrsForward+1;
        int yrStart = newYear-yrsBack;
        for (int idx=0; idx < count; idx++) { 
          aList.add( String.valueOf(yrStart+idx) );
        }
        yrChooser.setModel( new DefaultComboBoxModel(aList.toArray()) );
        yrChooser.setSelectedItem(String.valueOf(newYear));
        oldYear = newYear;
    }

    private void jbInit() {
        int yr   = defaultTime.getYear();
        int mo   = defaultTime.getMonth();
        int dy   = defaultTime.getDay();
        int hr   = defaultTime.getHour();
        int mn   = defaultTime.getMinute();
        int sec  = defaultTime.getSecond();
        String secStr = defaultTime.getSecondsStringToPrecisionOf(2);

        int yrStart = yr-yrsBack, yrEnd = yr+yrsForward;
        int dyStart = 1, dyEnd = 31;
        int hrStart = 0, hrEnd = 23;
        int mnStart = 0, mnEnd = 59;
        int secStart = 0, secEnd = 61;  // added 2-sec for leap second -aww 2008/02/04
        int increment = 1;

    // choosers
        yrChooser = new IntegerChooser(yrStart, yrEnd, increment, yr);
        oldYear = yr;
        yrChooser.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
             if (mutate) setNewYearChooserModel(Integer.parseInt((String)yrChooser.getSelectedItem()));
          }
        });

        moChooser = new MonthChooser();
        moChooser.setSelectedIndex(mo);

        dyChooser = new IntegerChooser(dyStart, dyEnd, increment, dy);
        hrChooser = new IntegerChooser(hrStart, hrEnd, increment, hr);
        mnChooser = new IntegerChooser(mnStart, mnEnd, increment, mn);
        secChooser = new NumberChooser(secStart, secEnd, increment, sec);
        if (! wholeSecs) secChooser.setSelectedItem(secStr);

    // build the panel
        if (borderTitle != null) {
            TitledBorder border = new TitledBorder(borderTitle);
            border.setTitleColor(Color.black);
            setBorder( border );
        }

        JPanel aPanel = new JPanel();
        aPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridy = 0;
        aPanel.add(new JLabel("Year"), c);
        aPanel.add(new JLabel("Month"), c);
        aPanel.add(new JLabel("Day"), c);
        aPanel.add(new JLabel("Hour"), c);
        aPanel.add(new JLabel("Minute"), c);
        aPanel.add(new JLabel("Second"), c);
        c.gridy++;
        aPanel.add(yrChooser, c);
        aPanel.add(moChooser, c);
        aPanel.add(dyChooser, c);
        aPanel.add(hrChooser, c);
        aPanel.add(mnChooser, c);
        aPanel.add(secChooser, c);

        add(aPanel);
    }

/**
 * Return a DateTime object that is set by the dialog
 */
    public DateTime getDateTime() {
        return new DateTime(
                (int) yrChooser.getSelectedValue(),
                (int) moChooser.getSelectedIndex(),   // Jan.= 0
                (int) dyChooser.getSelectedValue(),
                (int) hrChooser.getSelectedValue(),
                (int) mnChooser.getSelectedValue(),
                secChooser.getSelectedValue());
    }
/** 
 * Return a double containing nominal epoch seconds that is set by the dialog
 */
    public double getNominalSeconds() {
        return getDateTime().getNominalSeconds();
    }

/** 
 * Return a double containing true epoch (includes leap) seconds that is set by the dialog
 */
    public double getTrueSeconds() { // added 2008/02/04 -aww
        return getDateTime().getTrueSeconds();
    }

    /** Enable/disable all the active components that make up this component.*/
    public void setEnabled(boolean tf) {
        yrChooser.setEnabled(tf);
        moChooser.setEnabled(tf);
        dyChooser.setEnabled(tf);
        hrChooser.setEnabled(tf);
        mnChooser.setEnabled(tf);
        secChooser.setEnabled(tf);
    }
/** 
 * For debugging
 */
    public void dump() {
        System.out.println(
                " yr= " + yrChooser.getSelectedValue()+
                " mo= " + moChooser.getSelectedIndex()+   // Jan.= 0
                " dy= " + dyChooser.getSelectedValue()+
                " hr= " + hrChooser.getSelectedValue()+
                " mn= " + mnChooser.getSelectedValue()+
                " sc= " + (double) secChooser.getSelectedValue());

        System.out.println(" New selection = " + getDateTime().toString());
    }

    public void addItemListener(ItemListener al) {
        yrChooser.addItemListener(al);
        moChooser.addItemListener(al);
        dyChooser.addItemListener(al);
        hrChooser.addItemListener(al);
        mnChooser.addItemListener(al);
        secChooser.addItemListener(al);
    }

    public void addActionListener(ActionListener al) {
        yrChooser.addActionListener(al);
        moChooser.addActionListener(al);
        dyChooser.addActionListener(al);
        hrChooser.addActionListener(al);
        mnChooser.addActionListener(al);
        secChooser.addActionListener(al);
    }

/*
  static public final class Tester {
    public static void main(String s[]) {
        final DateTimeChooser tc1 = new DateTimeChooser("Start Time");
        final DateTimeChooser tc2 = new DateTimeChooser("End Time");

        JFrame frame = new JFrame("Main");
        frame.addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });

        JPanel rangePanel = new JPanel();
        rangePanel.setLayout(new BoxLayout(rangePanel, BoxLayout.Y_AXIS));
        rangePanel.add(tc1);
        rangePanel.add(tc2);

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(rangePanel, BorderLayout.CENTER);
        JButton okButton = new JButton("OK");
        okButton.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent e) {
             System.out.println("-- Times AFTER --");
             System.out.println("start "+tc1.getDateTime().toString());
             System.out.println("end   "+tc2.getDateTime().toString());
             System.out.println("size = "+ tc1.yrChooser.getSize());
             System.out.println("msize = "+ tc1.yrChooser.getMaximumSize());
             System.out.println("psize = "+ tc1.yrChooser.getPreferredSize());
             System.out.println("-----------");
           }
        });
        panel.add(okButton, BorderLayout.SOUTH);
        frame.getContentPane().add(panel);
        frame.getContentPane().add(panel);
        //frame.setSize(200,30);        // this has no effect!
        frame.setBounds(20,20,200,100);        // this has no effect!
        frame.pack();
        frame.setVisible(true);
    } // end of main
  } // end of Tester
*/
} // end of DateTimeChooser

