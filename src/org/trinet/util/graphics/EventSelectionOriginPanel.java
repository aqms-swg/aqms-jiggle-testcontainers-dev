package org.trinet.util.graphics;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.border.*;

import org.trinet.jasi.*;
import org.trinet.util.*;

/** A panel that allows selection of various EventSelectionProperties attributes. */
public class EventSelectionOriginPanel extends JPanel {

    private EventSelectionProperties props;

    private MinMaxRangeChooser rcQual = null;
    private MinMaxRangeChooser rcDist = null;
    private MinMaxRangeChooser rcZ = null;
    private MinMaxRangeChooser rcRMS = null;
    private MinMaxRangeChooser rcGap = null;
    private MinMaxRangeChooser rcErrH = null;
    private MinMaxRangeChooser rcErrZ = null;
    private MinMaxRangeChooser rcPhCnt = null;
    private MinMaxRangeChooser rcPhCntS = null;
    private MinMaxRangeChooser rcPhCntUsed = null;
    private MinMaxRangeChooser rcFMo = null;
    private MinMaxRangeChooser rcAmpCnt = null;

    //EVENT PROPS
        //"eventAuth"
        //"eventSubsource"
        //"originAuth"
        //"originSubsource"
        //"originType"
        //"originAlgo"
        //
        //"originQualityRange"
        //"originGapRange"
        //"originDistRange"
        //"originSPhasesRange"
        //"originFirstMoRange"
        //"originDepthRange"
        //"originRmsRange"
        //"originErrHorizRange"
        //"originErrDepthRange"
        //"originPhaseCountRange"
        //"originPhaseCountUsedRange"
        //"originAmpCountRange"


    /** The EventSelectionProperties object that is passed is changed
     * immediately by user action in the panel, therefore, if the caller wants
     * to be able to undo user action and revert to the old properties (say in
     * response to clicking a [CANCEL] button in a dialog) the caller must either retain
     * a copy of the original properties or pass a copy to this class and only
     * use it when approriate (say when [OK] is clicked).
    */
    public EventSelectionOriginPanel(EventSelectionProperties props) {
        this.props = props;
        try  {
            jbInit();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    /** Return the modified EventSelectionProperties object. */
    public EventSelectionProperties getProperties() {
        return props;
    }

    private void jbInit() throws Exception {
        Box vbox = Box.createVerticalBox();
        TitledBorder border = new TitledBorder("Calculated Ranges");
        border.setTitleColor(Color.black);
        this.setBorder(border);

        JPanel jp = new JPanel();
        Box box = Box.createHorizontalBox();
        JLabel label = new JLabel("Name", SwingConstants.CENTER);
        label.setPreferredSize(new Dimension(80,20));
        label.setMaximumSize(new Dimension(80,20));
        label.setMinimumSize(new Dimension(80,20));
        box.add(label);
        box.add(Box.createHorizontalStrut(10));
        label = new JLabel("Min", SwingConstants.CENTER);
        label.setPreferredSize(new Dimension(80,20));
        label.setMaximumSize(new Dimension(80,20));
        label.setMinimumSize(new Dimension(80,20));
        box.add(label);
        box.add(Box.createHorizontalStrut(10));
        label = new JLabel("Max", SwingConstants.CENTER);
        label.setPreferredSize(new Dimension(80,20));
        label.setMaximumSize(new Dimension(80,20));
        box.add(label);
        box.add(Box.createHorizontalGlue());
        jp.add(box);
        vbox.add(jp);

        // boolean value true/false must match Double/Integer range declared in EventSelectionProperties class
        // Double
        rcQual = new MinMaxRangeChooser("Quality", "originQualityRange", true, 0., 1.);
        rcDist = new MinMaxRangeChooser("Distance", "originDistRange", true, 0., 9999.);
        rcZ = new MinMaxRangeChooser("Depth", "originDepthRange", true, -1., 9999.);
        rcRMS = new MinMaxRangeChooser("RMS", "originRmsRange", true, 0., 99.);
        rcErrH = new MinMaxRangeChooser("ErrHoriz", "originErrHorizRange", true, 0., 9999.);
        rcErrZ = new MinMaxRangeChooser("ErrDepth", "originErrDepthRange", true, 0., 9999.);
        //Integer
        rcGap = new MinMaxRangeChooser("Gap", "originGapRange", false, 0., 360);
        rcPhCnt = new MinMaxRangeChooser("Phases", "originPhaseCountRange", false, 0, 9999);
        rcPhCntS = new MinMaxRangeChooser("S Phases", "originSPhasesRange", false, 0, 9999);
        rcPhCntUsed = new MinMaxRangeChooser("Used Phases", "originPhaseCountUsedRange", false, 0, 9999);
        rcFMo = new MinMaxRangeChooser("First Motions", "originFirstMoRange", false, 0, 9999);
        rcAmpCnt = new MinMaxRangeChooser("Amps", "originAmpCountRange", false, 0, 9999);

        vbox.add(rcQual);
        vbox.add(rcDist);
        vbox.add(rcZ);
        vbox.add(rcRMS);
        vbox.add(rcGap);
        vbox.add(rcErrH);
        vbox.add(rcErrZ);
        vbox.add(rcPhCnt);
        vbox.add(rcPhCntS);
        vbox.add(rcPhCntUsed);
        vbox.add(rcFMo);
        vbox.add(rcAmpCnt);

        vbox.add(Box.createVerticalStrut(5));

        jp = new JPanel();
        JButton cb = new JButton("Clear All");
        cb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                EventSelectionOriginPanel.this.clear();
            }
        });
        cb.setMaximumSize(new Dimension(90,20));
        jp.add(cb);
        vbox.add(jp);

        vbox.add(Box.createVerticalGlue());
        this.add(vbox);

        this.setMinimumSize(new Dimension(400,700));
    }

    private class MinMaxRangeChooser extends JPanel implements ActionListener {
        private JTextField min = null;
        private JTextField max = null;
        private String propName = null;
        private boolean fp = false;
      
        double defaultMinValue = 0.;
        double defaultMaxValue = 9999.;

        public MinMaxRangeChooser(String title, String propName, boolean fp, double defMin, double defMax) {
            this.propName = propName;
            this.fp = fp;

            defaultMinValue = defMin;
            defaultMaxValue = defMax;

            NumberRange nr = (fp) ?
                (NumberRange) props.getDoubleRange(propName) : (NumberRange) props.getIntegerRange(propName);

            //TitledBorder border = new TitledBorder(title);
            //border.setTitleColor(Color.black);
            //setBorder(border);
            //setLayout(new GridLayout(1,2));
            Box box = Box.createHorizontalBox();
            JLabel label = new JLabel(title, SwingConstants.TRAILING);
            label.setPreferredSize(new Dimension(80,20));
            label.setMaximumSize(new Dimension(80,20));
            label.setMinimumSize(new Dimension(80,20));
            box.add(label);

            box.add(Box.createHorizontalStrut(10));

            //label = new JLabel("Min:");
            //label.setMaximumSize(new Dimension(80,20));
            //box.add(label);
            min = new JTextField(8);
            min.setMaximumSize(new Dimension(80,20));
            if (nr != null) min.setText(nr.getMin().toString());
            min.addActionListener(this);
            box.add(min);

            box.add(Box.createHorizontalStrut(10));

            //label = new JLabel("Max:");
            //label.setMaximumSize(new Dimension(80,20));
            //box.add(label);
            max = new JTextField(8);
            max.setMaximumSize(new Dimension(80,20));
            if (nr != null) max.setText(nr.getMax().toString());
            max.addActionListener(this);

            box.add(max);
            box.add(Box.createHorizontalStrut(5));
            JButton cb = new JButton("C");
            cb.setMaximumSize(new Dimension(20,20));
            cb.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    MinMaxRangeChooser.this.clear();
                }
            });
            box.add(cb);
            box.add(Box.createHorizontalGlue());
            add(box);
        }
      
      
        public void actionPerformed(ActionEvent e) {
            //updateProperty(); // don't do it until update invoked externally
        }
        
        private IntegerRange getIntegerRange() {
            String minStr = min.getText();
            String maxStr = max.getText();
            if (minStr.length() < 1 && maxStr.length() < 1) return null;
            else if (minStr.length() < 1) minStr = String.valueOf(defaultMinValue);
            else if (maxStr.length() < 1) maxStr = String.valueOf(defaultMaxValue);
            // In case user entered decimal:
            return new IntegerRange(Math.round(Float.parseFloat(minStr)),Math.round(Float.parseFloat(maxStr)));
        }

        private DoubleRange getDoubleRange() {
            String minStr = min.getText();
            String maxStr = max.getText();
            if (minStr.length() < 1 && maxStr.length() < 1) return null;
            else if (minStr.length() < 1) minStr = String.valueOf(defaultMinValue);
            else if (maxStr.length() < 1) maxStr = String.valueOf(defaultMaxValue);
            return new DoubleRange(Double.valueOf(minStr), Double.valueOf(maxStr));
        }

        public void updateProperty() {
            NumberRange nr = ( fp ) ? (NumberRange) getDoubleRange() : (NumberRange) getIntegerRange();
            if (nr != null) props.setProperty(propName, nr);
            else props.remove(propName);
        }

        public void clear() {
            min.setText(null);
            max.setText(null);
        }

    }

    public void updateProperties() {
        rcQual.updateProperty();
        rcDist.updateProperty();
        rcZ.updateProperty();
        rcRMS.updateProperty();
        rcGap.updateProperty();
        rcErrH.updateProperty();
        rcErrZ.updateProperty();
        rcPhCnt.updateProperty();
        rcPhCntS.updateProperty();
        rcPhCntUsed.updateProperty();
        rcFMo.updateProperty();
        rcAmpCnt.updateProperty();
    }

    public void clear() {
        rcQual.clear();
        rcDist.clear();
        rcZ.clear();
        rcRMS.clear();
        rcErrH.clear();
        rcErrZ.clear();
        //Integer
        rcGap.clear();
        rcPhCnt.clear();
        rcPhCntS.clear();
        rcPhCntUsed.clear();
        rcFMo.clear();
        rcAmpCnt.clear();
    }

}                                        
