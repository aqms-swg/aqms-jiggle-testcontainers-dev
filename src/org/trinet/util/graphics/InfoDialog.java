package org.trinet.util.graphics;
import java.awt.*;
import javax.swing.*;
public class InfoDialog {
    public static void informUser(Component component, String msgType, String strMessage) {
        informUser(component, msgType, strMessage, null);
    }

    public static void informUser(Component component, String msgType, String strMessage, JLabel statusLabel) {
	final String plainCommand = "PLAIN";
	final String infoCommand = "INFO";
	final String questionCommand = "QUESTION";
	final String errorCommand = "ERROR";
	final String warningCommand = "WARNING";

	if (statusLabel != null) statusLabel.setText(strMessage);

	String messageType = msgType.trim().toUpperCase();
        if (messageType.equals(plainCommand)) {
                    JOptionPane.showMessageDialog(component,
                                    strMessage,
                                    "Message",
                                    JOptionPane.PLAIN_MESSAGE);
	} else if (messageType.equals(infoCommand)) {
                    JOptionPane.showMessageDialog(component,
                                    strMessage,
                                    "Information",
                                    JOptionPane.INFORMATION_MESSAGE);
    
	} else if (messageType.equals(questionCommand)) {
                    JOptionPane.showMessageDialog(component,
                                    strMessage,
				    "Question",
                                    JOptionPane.QUESTION_MESSAGE);
	} else if (messageType.equals(errorCommand)) {
                    JOptionPane.showMessageDialog(component,
                                    strMessage,
                                    "Error",
                                    JOptionPane.ERROR_MESSAGE);
	} else if (messageType.equals(warningCommand)) {
                    JOptionPane.showMessageDialog(component,
                                    strMessage,
                                    "Warning",
                                    JOptionPane.WARNING_MESSAGE);
	}
	java.awt.Toolkit.getDefaultToolkit().beep();

    } // end of informUser method
}
