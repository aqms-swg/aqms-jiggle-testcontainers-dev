package org.trinet.util.graphics.table;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.text.*;
import java.io.*;
import java.lang.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;
import org.trinet.util.graphics.*;
import org.trinet.util.graphics.text.*;
public abstract class AbstractJasiListPanel extends JPanel implements
  Runnable, TableModelListener, PropertyChangeListener {
    private static final ImageIcon LIGHT_BULB =
        new ImageIcon(IconImage.getImage("lightBulb.jpg"));
    private static final ImageIcon DARK_BULB =
        new ImageIcon(IconImage.getImage("darkBulb.jpg"));

    //protected String [] columnOrderByName = null;  // removed, interferes with user property settings
    protected AbstractJasiListTable dataTable;
    protected ActiveArrayListIF inputList;
    protected JasiTableRowTextParserIF inputParser;
    protected String panelName = null;
    static protected final Insets inset = new Insets(0,0,0,0);

    private Thread tableThread;
    private Runnable tableThreadPanelUpdate;
    private Cursor waitCursor = new Cursor(Cursor.WAIT_CURSOR);
    private Cursor defaultCursor = new Cursor(Cursor.DEFAULT_CURSOR);
    private JLabel waitRequestLabel;
    private javax.swing.Timer tableTimer;
    protected JLabel statusLabel;
    protected String className;
    protected TextLogger aLog = new TextLogger();

    protected boolean parseable = true; 
    private boolean imageFlag = true;
    private boolean interruptRequestFlag = false;
    private boolean updateDB = false;
    private boolean tableModified = false;
    private boolean commitEnabled = false;
    protected boolean commitButtonEnabled = false;
    protected boolean listUpdateNeeded = false;
    protected boolean autoListUpdate = true; // aww 8/29/2002 false for debuggin list update
    protected boolean tableRowHeadSelectionFlag = true;
    protected StripOptionPane jopStrip;

    private int tableRowCount = 0;
    private JScrollPane spane;
    private JPanel buttonPanel;
    private JButton jbUnStrip;
    private JButton jbStrip;
    private JButton jbInput;
    private JButton jbInsert;
    private JButton jbClone;
    private JButton jbRemove;
    private JButton jbSort;
    protected JButton jbCommit;
    private JButton jbFile;
    //private JButton jbSave;
    //private JButton jbPurge;
    //private JButton jbReload;
 
    // String [] saveHeader = null; if member added here could
    // add below to listener or application AFTER table is created?
    //   saveHeader = aPanel.getTableColumnHeaders();
    //

/** Constructor
* @param textArea javax.swing.JTextArea to which output is appended can be null.
* @param updateDB true == allows table edits and database updates.
*/
    protected AbstractJasiListPanel() {
       this(null, false);
    }
    protected AbstractJasiListPanel(JTextArea textArea, boolean updateDB) {
        super();
        this.aLog.setTextArea(textArea);
        this.updateDB     = updateDB;
    }

// Begin panel methods
    public void setPanelName(String name) { this.panelName=name;}
    public String getPanelName() { return panelName;}

    public String getInputParserName() { 
      String parserName =
          this.dataTable.inputProperties.getProperty("inputParserName");
      if (parserName == null) {
        //System.err.println("FYI - inputParser property is not defined for " + className);
      }
      return parserName;
    }
    protected boolean initParser() { 
      return initParser(getInputParserName()); 
    }
    private boolean initParser(String parserName) { 
      if (parserName == null) return false;
      if (inputParser != null && inputParser.getClass().getName().equals(parserName)) return true;
      return createInputParser(parserName);
    }
    private boolean createInputParser(String parserName) { 
      inputParser = null;
      try {
        Debug.println(getClass().getName() + " input parser class name:\"" + parserName + "\"");
        inputParser =
          (JasiTableRowTextParserIF) Class.forName(parserName).newInstance();
        inputParser.setProperties(dataTable.inputProperties);
      }
      catch (Exception ex) {
        System.err.println("ERROR inputParser class not instantiated " + getClass().getName());
        //ex.printStackTrace();
        System.err.println(ex.getMessage());
      }
      return (inputParser != null) ? true : false;
    }
    protected void initPanel() { 
        className = getClass().getName();
        className = className.substring(className.lastIndexOf(".")+1);
        //if (columnOrderByName != null) dataTable.setColumnNameOrder(columnOrderByName); //removed
        getModel().setCellEditable(hasUpdateEnabled());  //JasiListModel
        addTableModelListener(this);
        initGraphics();
    }
    protected void initGraphics() {
        setLayout(new BorderLayout());
        //if (columnOrderByName == null) aLog.logTextnl(className + " column name order not yet initialized"); // removed
        add(createStatusLabelPanel(), BorderLayout.SOUTH);
        add(createWaitRequestLabel(), BorderLayout.CENTER);
        //add(createButtonPanel(), BorderLayout.NORTH);
        add(createMenuButtonPanel(), BorderLayout.NORTH);
        addAncestorListener(new AnAncestorListener());
        //Debug.println(className+" debug prefSize: "+((BorderLayout)getLayout()).preferredLayoutSize(this));
    }
    private JPanel createStatusLabelPanel() {
        statusLabel = new JLabel(" ");
        statusLabel.setForeground(Color.red);
        statusLabel.setHorizontalAlignment(JLabel.LEFT);
        JPanel statusPanel = new JPanel();
        statusPanel.setBorder(BorderFactory.createEtchedBorder());
        statusPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        statusPanel.add(statusLabel);
        return statusPanel;
    }
    private JLabel createWaitRequestLabel () {
        waitRequestLabel = new JLabel();
        waitRequestLabel.setText("Table results displayed here");
        waitRequestLabel.setHorizontalAlignment(JLabel.CENTER);
        return waitRequestLabel;
    }

    protected void enableParsing() { parseable = true; }
    protected void disableParsing() { parseable = false; }
    protected boolean canParseInput() {
      return ( parseable && (inputParser != null) );
    }
    protected abstract boolean insertRow();  // specific to concrete type

    // Subclasses need to implement "synch" actions only if input lists are cloned
    // for modelList. In which case we need application USER OPTION to select list 
    // add or replacement strategy.
    // Method Should reset the state of the listUpdateNeeded flag.
    protected int synchInputList() {
      Debug.println("DEBUG synching input list for panel " + panelName);
      if (! isListUpdateNeeded()) return 0;
      if (inputList == getList()) { // identical list object no-op
        setListUpdateNeeded(false);
        return 0;
      }
      System.err.println("ERROR synchInputList assertion failed check code " + "for panel " + panelName);
      return -1; // for debug, assert shouldn't be here
    }

    protected int updateList() {
      if (getList() == inputList ) listUpdateNeeded = false; // reset of flag
      if (! isListUpdateNeeded() || ! confirmUpdate()) return 0;
      return synchInputList(); // should reset the listUpdateNeeded flag
    }
    protected boolean isListUpdateNeeded() { return listUpdateNeeded;}
    protected void setListUpdateNeeded(boolean tf) { listUpdateNeeded = tf;}
    public void setAutoListUpdate(boolean tf) { autoListUpdate = tf; }
    public boolean hasAutoListUpdate() { return autoListUpdate; }
    protected boolean confirmUpdate() {
      if (! hasAutoListUpdate() && ! confirmSave()) return false;
      Debug.println(panelName + " updating lists");
      return true;
    }

    protected AbstractJasiListTable getTable() {
       return (AbstractJasiListTable) dataTable; // JasiListTable
    }
    protected AbstractJasiListTableModel getModel() {
       return (AbstractJasiListTableModel) dataTable.getTableModel(); // JasiListTable
    }
    public String [] getTableColumnHeaders() {
        return dataTable.getTableColumnHeaders(); // JasiListTable
    }

    /* Set the table column name order. 
    public void setColumnOrder(String [] order) { columnOrderByName = order; } // removed
    */

/** Sets the event data list and creates new table.  */
    protected void setList(ActiveArrayListIF list) {
        if (list == null) {
           aLog.logTextnl("setList(list) Input list is null; noop for : " +
                           className);
           //return;
        }
        if (list != inputList) initInputList(list);
        if (inputList != null) makeTable(false);
    }
    private void initInputList(ActiveArrayListIF list) {
      // below must precede assignment of new list
      if (inputList != null) {
        if (! inputList.isEmpty()) commitModifiedTable();
        /*
        if (inputList instanceof ActiveArrayListIF) {
          ActiveArrayListIF aal = (ActiveArrayListIF) inputList;
          aal.removeChangeListener(this);  // avoid redundant duplication?
          aal.removeListDataStateListener(this);  // avoid redundant duplication?
        }
        */
      }
      inputList = list;
      /*
      if (inputList != null) {
        if (inputList instanceof ActiveArrayListIF) {
          ActiveArrayListIF aal = (ActiveArrayListIF) inputList;
          aal.addChangeListener(this);
          aal.addListDataStateListener(this);
        }
      }
      */
    }

    public ActiveArrayListIF getList() {
        return getModel().getList();  //JasiListModel
    }
    public boolean hasNullList() {
        java.util.List aList = getList();
        return (aList == null || aList.isEmpty());
    }

    public boolean confirmSave() {
      int answer =
          JOptionPane.showConfirmDialog(this, "Save data for List " + panelName,
              "ListUpdate", JOptionPane.YES_NO_OPTION,
              JOptionPane.QUESTION_MESSAGE);
      /*
      try {
              throw new NullPointerException("AJLP TEST exception stack dump");
      }
      catch (NullPointerException ex) {
              ex.printStackTrace();
      }
      */
      return (answer == JOptionPane.YES_OPTION);
    }

    /** Return the JPanel containing the action button. Used to add more buttons. */
    protected JPanel getButtonPanel() {
       return buttonPanel;
    }

    public int sortedToModelRowIndex(int tableRowIndex) {
        return dataTable.sortedToModelRowIndex(tableRowIndex);
    }
    public int modelToSortedRowIndex(int modelRowIndex) {
       return dataTable.modelToSortedRowIndex(modelRowIndex);
    }
    public void setSelectedRow(int rowid) {
        dataTable.setRowSelectionInterval(rowid,rowid);
    }
    public void setSelectionMode(int mode) {
        if (mode == ListSelectionModel.SINGLE_SELECTION || 
            mode == ListSelectionModel.SINGLE_INTERVAL_SELECTION ||
            mode == ListSelectionModel.MULTIPLE_INTERVAL_SELECTION)
                dataTable.setSelectionMode(mode);
    }
    public int getRowIndexOf(JasiCommitableIF jc) {
        int index = getList().indexOf(jc);
        return  (index < 0) ? index : modelToSortedRowIndex(index);
    }
    public void setSelectedRow(JasiCommitableIF jc) {
        int index = getList().indexOf(jc);
        if (index < 0) return;
        dataTable.setRowSelectionInterval(
            modelToSortedRowIndex(index),
            modelToSortedRowIndex(index)
        );
    }
    /**
     * Index of the selected row in table row header.
     * Do NOT confuse with TableModel List index, table may have been sorted.
     */
    public int getSelectedRowIndex() {
        return dataTable.getSelectedRowIndex();
    }
    /**
     * Return array of the indices of rows selected in table row header.
     * Do NOT confuse with the TableModel List indices, table rows may have been sorted.
     * @see #getSelectedRowModelIndex()
     * @see #getSelectedRows()
     * */
    public int[] getSelectedRowsIndices() {
        return dataTable.getSelectedRowIndices();
    }
    /** Return the index in the TableModel List that corresponds
     * to the table row header selection.
     * */
    public int getSelectedRowModelIndex() {
        return dataTable.getSelectedRowModelIndex();
    }
    /** Return the JasiCommitableIF object in the TableModel List
     * that corresponds to the table row header selection.
     * */
    public JasiCommitableIF getSelectedRow() {
        return dataTable.getSelectedJasiCommitable();
    }
    /**
     * Return array of JasiCommitableIF objects in the TableModel List
     * that correspond to selected table row header indices.
     * */
    public JasiCommitableIF [] getSelectedRows() {
        return dataTable.getSelectedJasiCommitables();
    }

    //public abstract long getSelectedId();

    /*
     * Return array of the identifiers of the JasiCommitableIF 
     * objects in the TableModel List that correspond to selected table
     * row header indices.
     * */
    public long [] getSelectedIds() {
        return dataTable.getSelectedIds();
    }

    public boolean hasWritableConnection() {
        return (! DataSource.isReadOnly() && ! DataSource.isClosed() );
    }
    public boolean hasModifiedTable() {
        return tableModified;
    }
    public boolean hasUnmodifiedTable() {
        return ! tableModified;
    }
    public void unsetTableModified() {
        tableModified = false;
    }
    public boolean isModifiable() {
        return ( hasUpdateEnabled() && hasWritableConnection() && (getList() != null) ); 
    }
    public boolean isNotModifiable() {
        return ( hasUpdateDisabled() || ! hasWritableConnection() || (getList() == null) );
    }

    public void setCommitEnabled(boolean tf) { commitEnabled = tf;}
    public boolean hasCommitEnabled() {return commitEnabled;}

    public boolean isCommitable() {
        if (! commitEnabled) return false;
        return hasValidTable() ? isModifiable(): false;
    }
    public int commitModifiedTable() {
        if (hasUnmodifiedTable()) return 0;
        return commit();
    }
    public void setCommitButtonEnabled(boolean tf) {
        commitButtonEnabled = tf;
    }

    protected boolean canCommit() {
        boolean status = true;
        if (! isCommitable()) {
          aLog.logTextnl(panelName + " is NOT commitable");
          status = false;
        }
        /* removed 08/04/2004 -aww
        else {
          status = (hasModifiedTable()) ?
              commitConfirmed("Commit modified table") : commitConfirmed("Commit unmodified table");
        }
        */
        return status;
    }
    public int commit() {
        if (! canCommit()) return 0;
        // only for list adds only? or do retainAll on model list contents? 
        updateList();
        return updateDB();
    }
    protected boolean commitConfirmed(String message) {
        return ( JOptionPane.showConfirmDialog(this,
                message,
                "Verify Commit", JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE)
                == JOptionPane.YES_OPTION );
    }

/** Toggles allowing enabling of database updates from table using panel actions.
* @param updateDB true == allow updates, default == false.
*/
    public void setUpdateDB(boolean tf) {
        updateDB = tf;
        getModel().setCellEditable(tf);
    }
/** Return the value of the database update switch.
*/
    public boolean hasUpdateEnabled() {
        return updateDB;
    }
/** Return negation of the value of the database update switch.
*/
    public boolean hasUpdateDisabled() {
        return ! updateDB;
    }
    protected void informModel() {
      if (inputList != null && inputList.size() != getRowCount() ) {
        System.out.println("FYI - input data list size changed for : " + panelName);
       }
       //dataTable.getTableSorter().resetSort(); // removed since model listens for sort change aww 03/18/03
       //getModel().fireTableDataChanged(); // aww 03/18/03 removed, above fires model tableChanged event?
       //Resize columns here? could enable in the AbstractJasiListTable model listener,
       //or in the TableModelListener implemented below, but with more overhead processing 
       //each tableModel event with resizing columns. 03/03 aww 
       if (spane != null) dataTable.reinitializeTableCellWidths();
    }

//LISTENERS:
/** Required method to implement TableModelListener interface */
    public void tableChanged(TableModelEvent evt) {
        if (Debug.isEnabled()) {
          String typeStr = "Unknown";
          switch (evt.getType()) {
            case TableModelEvent.UPDATE: // perhaps reset column widths here?
              typeStr = "UPDATE";
              //Resize columns here, or in AbstractJasiListTable model listener? 03/03 aww 
              //dataTable.reinitializeTableCellWidths();
              break;
            case TableModelEvent.INSERT:
              typeStr = "INSERT";
              //Resize columns here, or in AbstractJasiListTable model listener? 03/03 aww 
              //dataTable.reinitializeTableCellWidths();
              break;
            case TableModelEvent.DELETE:
              typeStr = "DELETE";
              break;
          }
          System.out.println( panelName +
              " table modified "+ typeStr +
              " col: "+ evt.getColumn() +
              " firstRow: "+ evt.getFirstRow() +
              " lastRow: "+ evt.getLastRow()
          );
        }
        tableModified = true;
        // below not needed with listeners, only if modelList was cloned 
        listUpdateNeeded = true; // aww 02/03
        // for future delegation if needed:
        //firePropertyChange("tableChanged", event.getColumn(), event.getColumn());
    }

    final protected void debugProperty(PropertyChangeEvent e) {
        Debug.println(
          " DEBUG propertyChange: " + e.getPropertyName() +
          " oldValue:" + e.getOldValue() +
          " newValue:" + e.getNewValue() +
          " panel: " + panelName
        );
    }
    public void propertyChange(PropertyChangeEvent e) {
        debugProperty(e);
        String propName = e.getPropertyName();
        if (propName.equals(EngineIF.SOLVED)) {
          informModel();
        }
    }
    public void addTableModelListener(TableModelListener tml) {
            //getModel().addTableModelListener(tml);  // test removal aww 10/24/2002 
            dataTable.getTableSorter().addTableModelListener(tml);
    }

/** Adds a window listener to root Window into which this component is added.
* Needed to cleanup editing, or save work,  if window is closed inadvertently.
*/
    public void addWindowListener() {
        JRootPane jrp =  this.getRootPane();
        if (jrp != null) this.addWindowListener((Window) jrp.getParent());
    }

/** Adds a window listener to specified Window.
* Needed to cleanup editing, or save work, if specified input window is closed inadvertently.
*/
    public void addWindowListener(Window window) {
        window.addWindowListener(this.new WindowCloserListener());
    }
    public void stopTableCellEditing() {
        if (dataTable != null) dataTable.stopTableCellEditing();
    }

    public int getRowCount() { return getModel().getRowCount();}

    protected void makeTable(boolean commitModified) {
      //Debug.println("AJLP debug starting table thread.");
      if (spane == null) startTableThread(commitModified);
      else updateScrollPaneTable(commitModified);
    }

    private void updateScrollPaneTable(boolean commitModified) {
      //Debug.println("AJLP spane not null");
      if (commitModified) commitModifiedTable();
      try {
        //dataTable.getTableSorter().resetSort();
        getModel().setList(inputList);  //JasiListModel
        dataTable.reinitializeTableCellWidths(); // JasiListTable
        updateStatus();
      }
      catch(NullPointerException ex) {
        ex.printStackTrace();
        return;
      }
      revalidate();
      unsetTableModified();
      return;
    }
    // method to run thread to get data from database and generate table
    private void startTableThread(boolean commitModified) {
      //Debug.println("AJLP spane is null");
      tableThread = new Thread(this, className + "Thread");
      if (tableTimer == null) tableTimer = new javax.swing.Timer(250, new TableTimerActionListener());
      tableTimer.start();
      tableThread.start();
      setCursor(waitCursor);
    }

    private void updateStatusLabelCounts() {
        tableRowCount = getRowCount();
        statusLabel.setText("Table has " + tableRowCount + " rows " +
            dataTable.getColumnCount() + " nonkey columns" +
            " hiding " + dataTable.getHiddenColumns().size());
    }
    protected void updateStatus() {
        updateStatusLabelCounts();
        setContentButtonsState((tableRowCount > 0));
        insertionOnly((tableRowCount <= 0));
    }

/** Method required by Runnable interface */
    public void run() {
        Thread thisThread = Thread.currentThread();
        tableRowCount = 0;
        if (tableThreadPanelUpdate == null) {
            tableThreadPanelUpdate = new UpdateTablePanel();
        }
        //if (inputList == null || inputList.size() == 0) {
        if (inputList == null) {
            aLog.logTextnl(panelName + ": Thread run() Input list object is null.");
            SwingUtilities.invokeLater(tableThreadPanelUpdate);
            return;
        }
        try {
            thisThread.sleep(10l);  // allow interrupted exception trap here

            //System.out.println("DEBUG startTableThread run setTable List AJLP run : " + inputList.size());
            //dataTable.getTableSorter().resetSort();
            getModel().setList(inputList);  //JasiListModel
            tableRowCount = getRowCount();  //JasiListModel
            if (tableRowCount > 0) {
                createTable();
                unsetTableModified(); // data from db fresh, no edits yet
            }
            else if (tableRowCount <= 0) {
//              if (thisThread.isInterrupted() || interruptRequestFlag) resetConnection();
                //Debug.println(panelName + ": No data found in list." );
            }
        }
        catch (InterruptedException ex) {
            aLog.logTextnl("Stopping JasiListPanel table creation thread " + className);
        }
        catch (Exception ex) {
            System.err.println("Generic exception run() JasiListPanel thread " + className);
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
        finally {
            // Better to use a invokeLater(runnable) to make panel changes in the event-dispatch thread:
            SwingUtilities.invokeLater(tableThreadPanelUpdate);
        }
    }

    protected JPanel createButtonPanel() {
        buttonPanel = new JPanel();
        FlowLayout flowLayout = (FlowLayout) buttonPanel.getLayout();
        flowLayout.setAlignment(FlowLayout.CENTER);
        flowLayout.setHgap(0);
        buttonPanel.add(createCommitButton());
        buttonPanel.add(createFileButton());
        buttonPanel.add(createStripButton());
        buttonPanel.add(createUnStripButton());
        buttonPanel.add(createInsertButton());
        buttonPanel.add(createCloneButton());
        buttonPanel.add(createDeleteButton());
        buttonPanel.add(createSortButton());
        buttonPanel.add(createInputButton());
        //buttonPanel.add(createSaveButton());
        //buttonPanel.add(createPurgeButton());
        //buttonPanel.add(createReloadButton());
        if (hasUpdateDisabled()) setModifyButtonsState(false);
        setContentButtonsState(false);
        return buttonPanel;
    }
    private JButton createCommitButton() {
        jbCommit = new JButton("Commit");
        jbCommit.setActionCommand("Commit");
        jbCommit.setToolTipText("Update database with selected solution data.");
        jbCommit.addActionListener(new ModifyTableRowsActionListener());
        jbCommit.setMargin(inset);
        return jbCommit;
    }
    private JButton createFileButton() {
        jbFile   = new JButton("File");
        jbFile.setActionCommand("File");
        jbFile.setToolTipText("Write delimited table data to ASCII text file.");
        // Subclass action listener class, overide actionPerformed
        // write any associated selected ids panels thus saving to single file.
        // Otherwise the list panels all use the action listener default.
        jbFile.addActionListener(createSaveTableActionListener());
        jbFile.setMargin(inset);
        return jbFile;
    }
    private JButton createUnStripButton() {
        jbUnStrip= new JButton("UnStrip");
        jbUnStrip.setActionCommand("UnStrip");
        jbUnStrip.setToolTipText("Restore (undelete) stripped rows of data list.");
        jbUnStrip.addActionListener(new ModifyTableRowsActionListener());
        jbUnStrip.setMargin(inset);
        return jbUnStrip;
    }
    private JButton createStripButton() {
        jbStrip  = new JButton("Strip");
        jbStrip.setActionCommand("Strip");
        jbStrip.setToolTipText("Strip (delete) out of bounds data in list.");
        jbStrip.addActionListener(new ModifyTableRowsActionListener());
        jbStrip.setMargin(inset);
        return jbStrip;
    }
    private JButton createInsertButton() {
        jbInsert = new JButton("Insert");
        jbInsert.setActionCommand("Insert");
        jbInsert.setToolTipText("Add dummy row to data table; user must edit cells values.");
        jbInsert.addActionListener(new ModifyTableRowsActionListener());
        jbInsert.setMargin(inset);
        return jbInsert;
    }
    private JButton createCloneButton() {
        jbClone = new JButton("Clone");
        jbClone.setActionCommand("Clone");
        jbClone.setToolTipText("Add copy of selected row to data table; inserts new data into database upon commit.");
        jbClone.addActionListener(new ModifyTableRowsActionListener());
        jbClone.setMargin(inset);
        return jbClone;
    }
    private JButton createDeleteButton() {
        jbRemove = new JButton("Remove");
        jbRemove.setActionCommand("Delete");
        jbRemove.setToolTipText("Remove selected row from list.");
        jbRemove.addActionListener(new ModifyTableRowsActionListener());
        jbRemove.setMargin(inset);
        return jbRemove;
    }
    private JButton createSortButton() {
        jbSort = new JButton("Sort");
        jbSort.setActionCommand("Sort");
        jbSort.setToolTipText("Sort by last sort (e.g. time or distance).");
        jbSort.addActionListener(new ModifyTableRowsActionListener());
        jbSort.setMargin(inset);
        return jbSort;
    }
    private JButton createInputButton() {
        jbInput = new JButton("Input");
        jbInput.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
            if (! initParser()) return;
            inputParser.createInputTextDialog(AbstractJasiListPanel.this);
          }
        });
        jbInput.setMargin(inset);
        String parserName = getInputParserName();
        if (parserName != null) {
          boolean status = createInputParser(parserName); 
          jbInput.setEnabled(status);
          if (status) jbInput.setToolTipText("Parse text panel input into table rows.");
        }
        else {
          jbInput.setToolTipText("Input parser is undefined (null).");
        }
        jbInput.setEnabled(canParseInput());
        return jbInput;
    }
    /*
    private JButton createSaveButton() {
        jbSave   = new JButton("Sync");
        jbSave.setActionCommand("Sync");
        jbSave.setToolTipText("Update original input list with additions.");
        jbSave.addActionListener(new ModifyTableRowsActionListener());
        jbSave.setMargin(inset);
        return jbSave;
    }
    private void JButton createPurgeButton() {
        jbPurge  = new JButton("Purge");
        jbPurge.setActionCommand("Purge");
        jbPurge.setToolTipText("Purge (remove) all data rows from list.");
        jbPurge.addActionListener(new ModifyTableRowsActionListener());
        jbPurge.setMargin(inset);
        return jbPurge;
    }
    private JButton createReloadButton() {
        jbReload   = new JButton("Reload");
        jbReload.setActionCommand("Reload");
        jbReload.setToolTipText("Recover original input list (e.g. after purge)");
        jbReload.addActionListener(new ModifyTableRowsActionListener());
        jbReload.setMargin(inset);
        return jbReload;
    }
    */
    private void setContentButtonsState(boolean tf) {
        if (tf) {
          enableContentButtons();
          showContentButtons();
        }
        else {
          disableContentButtons();
          hideContentButtons();
        }
    }
    private void setModifyButtonsState(boolean tf) {
        if (tf) {
          showModifyButtons();
          enableModifyButtons();
        }
        else {
          hideModifyButtons();
          disableModifyButtons();
        }
    }
    protected void showContentButtons() {
        jbFile.setVisible(true);
        jbSort.setVisible(true);
    }
    protected void hideContentButtons() {
        jbFile.setVisible(false);
        jbSort.setVisible(false);
    }
    protected void enableContentButtons() {
        jbFile.setEnabled(true);
        jbSort.setEnabled(true);
    }
    protected void disableContentButtons() {
        jbFile.setEnabled(false);
        jbSort.setEnabled(false);
    }
    protected void enableModifyButtons() {
        if (isNotModifiable()) return; // disableModifyButtons();
        enableInsertButtons();
        jbCommit.setEnabled(commitButtonEnabled);
        jbStrip.setEnabled(true);
        jbUnStrip.setEnabled(true);
        jbClone.setEnabled(true);
        jbRemove.setEnabled(true);
        //jbPurge.setEnabled(true);
        //jbSave.setEnabled(true);
        //jbReload.setEnabled((inputList != null && !inputList.isEmpty()));
    }
    protected void disableModifyButtons() {
        disableInsertButtons();
        jbCommit.setEnabled(false);
        jbStrip.setEnabled(false);
        jbUnStrip.setEnabled(false);
        jbClone.setEnabled(false);
        jbRemove.setEnabled(false);
        //jbPurge.setEnabled(false);
        //jbSave.setEnabled(false);
        //jbReload.setEnabled((inputList != null && !inputList.isEmpty()));
    }
    protected void showModifyButtons() {
        if (commitButtonEnabled) jbCommit.setVisible(true);
        jbInput.setVisible(canParseInput());
        jbInsert.setVisible(true);
        jbStrip.setVisible(true);
        jbUnStrip.setVisible(true);
        jbClone.setVisible(true);
        jbRemove.setVisible(true);
        //jbPurge.setVisible(true);
        //jbSave.setVisible(true);
        //jbReload.setVisible((inputList != null && !inputList.isEmpty()));
    }
    protected void hideModifyButtons() {
        jbCommit.setVisible(false);
        jbStrip.setVisible(false);
        jbUnStrip.setVisible(false);
        jbInsert.setVisible(false);
        jbInput.setVisible(false);
        jbClone.setVisible(false);
        jbRemove.setVisible(false);
        //jbPurge.setVisible(false);
        //jbSave.setVisible(false);
        //jbReload.setVisible((inputList != null && !inputList.isEmpty()));
    }
    private void insertionOnly(boolean tf) {
        setModifyButtonsState(! tf);
        enableInsertButtons();
    }
    protected void disableInsertButtons() {
        jbInput.setEnabled(false);
        jbInsert.setEnabled(false);
        jbInput.setVisible(false);
        jbInsert.setVisible(false);
    }
    protected void enableInsertButtons() {
        jbInput.setEnabled(isModifiable() && canParseInput());
        jbInsert.setEnabled(isModifiable());
        jbInput.setVisible(true);
        jbInsert.setVisible(true);
    }

    protected ActionListener createSaveTableActionListener() {
       return new SaveTableActionListener();
    }

    protected void createTable() {
        spane = dataTable.createTableScrollPane(); // JasiListTable
        if (spane != null) {
          createTableColumnVisibleMenu();
        }
    }
    private boolean hasValidTable() {
        if (dataTable == null || hasNullList()) {
//          aLog.logTextnl(panelName + ": No rows in table - execute new request.");
//          InfoDialog.informUser(this, "ERROR", ": No rows in table - execute new request.", statusLabel);
            return false;
        }
        else return true;
    }
    public boolean hasInvalidTable() {
        return ! hasValidTable();
    }

    /*
    private int getModelRowId(int rowid) {
      return (rowid <= 0) ?
          getList().size() : sortedToModelRowIndex(rowid);
    }
    */
    protected void resetScrollBarPosition(int rowIndex) {
        dataTable.resetScrollBarPosition(rowIndex);
    }
    protected boolean addOrReplaceRow(JasiCommitableIF aRow) { 
        if (spane == null) return insertRow(aRow);
        boolean retVal = dataTable.addOrReplaceRow(aRow);
        if (retVal) {
          //setAllFlags(); // not needed with listeners.
          Debug.println(panelName + ": row added or replaced.");
        }
        return retVal;
    }
    protected boolean insertRow(JasiCommitableIF aRow) { 
        int rowid = getSelectedRowIndex();
        rowid = (rowid == -1) ? Math.max(0,getRowCount()-1) : rowid;
        boolean retVal = false;
        if (spane != null) {
          //System.out.println("Debug insertRow at : " + rowid); 
          retVal = dataTable.appendRow(rowid, aRow);
          //retVal = dataTable.insertRow(rowid, aRow);
        }
        else { // no data retrieved from db
          //Debug.println("AJLP debug insert new list created ...");
          getList().add(aRow);
          startTableThread(false); //setList(getList());
          retVal = waitForScrollPane();
          if (retVal) setSelectedRow(0);
        }
        //Debug.println("AJLP debug spane null?:"+(spane == null));
        if (retVal) {
            Debug.println(panelName +
                ": row insert SUCCESS at index: " + rowid);
            //setAllFlags(); // not needed with listeners.
        }
        else
          aLog.logTextnl(panelName +
              ": row insert FAILURE at index: " + rowid);
        return retVal;
    }

    // Sets panel associated solution or magnitude stale.
    // could force a list synch.
    protected abstract void setStale(boolean tf);
    // Returns true:
    //  if a solution or magnitude need recalculation for associated panel.
    protected abstract boolean isStale();

    /* since listeners control state, may not need this method: 
    private void setAllFlags() {
      setStale(true); // ? should I do this here.
      listUpdateNeeded = true;
      tableModified = true; // not needed if table event listener sets
    }
    */
    private boolean waitForScrollPane() {
      boolean retVal = false;
      try {
        Thread.currentThread().sleep(10l);
        //System.out.println("AJLP waiting thread join " + "tableThread null: " + (tableThread == null));
        if (tableThread != null) {
          tableThread.join(2000l);
          if (tableThread != null) tableThread.interrupt(); // kill it?
        }
        retVal = true;
      }
      catch (InterruptedException ex) {
        aLog.logTextnl(panelName +
             ": Table thread execution interrupted.");
      }
      return retVal;
    }
    private void resetSelectedRowColumnSizes() {
        int rowid = getSelectedRowIndex();
        //dataTable.resetRowColumnSizes(dataTable.getRowHeader(), rowid);
        //dataTable.resetRowHeader(dataTable.getRowHeader(), spane);
        //spane.revalidate();
        if (rowid > -1) dataTable.resetTableCellWidths(); //try this instead
    }

    protected int updateDB() {
        if (! canUpdateDB()) return -1;
        int nrow = dataTable.updateDB(); // JasiListTable
        resolveUpdateResults(nrow);
        return nrow;
    }

    protected boolean canUpdateDB() { 
        if (hasInvalidTable()) {
            InfoDialog.informUser(this, "ERROR",
                panelName +
                ": Unable to update DB; no table to update.",
                statusLabel);
            return false;
        }
        if (isNotModifiable()) {
            InfoDialog.informUser(this, "ERROR",
                panelName +
                ": Unable to update DB; table not write back enabled.",
                statusLabel);
            return false;
        }
        return true;
    }

    protected void resolveUpdateResults(int rowsUpdated) {
        if (rowsUpdated < 0) {
            InfoDialog.informUser(this, "ERROR", panelName +
                ": Unable to update DB; re-edit table and retry ",
                statusLabel);
            return;
        }
        else {
            aLog.logTextnl(panelName + ": Updated " + rowsUpdated + " rows in database.");
            getModel().fireTableDataChanged(); // aww test here for table rows new sequence ids 10/25/2002
        }
        unsetTableModified(); // ok if successful update of all rows 
    }
    protected boolean hasModifiedDataBeenSelected(JasiCommitableIF [] selectedTableRows, boolean printRows) {
      java.util.List modelList = getList();
      if (modelList == null) return false;
      int listCount = modelList.size();
      if (listCount == 0) return false;
      int modelNeedsCommitCount = 0;
      int unselectedCount = 0;
      java.util.List aSelectedRowList =
          Arrays.asList(selectedTableRows);
      boolean status = true;
      for (int idx=0; idx<listCount; idx++) {
        JasiCommitableIF aModelRow = (JasiCommitableIF) modelList.get(idx);
        if (aModelRow.getNeedsCommit()) {
          modelNeedsCommitCount++;
          if (! aSelectedRowList.contains(aModelRow)) { // model row not selected, reveal it
            if (printRows) {
              System.out.println("Unselected row with modified data: "  + aModelRow.toString());
            }
            unselectedCount++;
            status = false;
          }
        }
      }
      if (printRows) {
        System.out.println("Total rows needing commit count: "  +
            modelNeedsCommitCount + " unselected: " + unselectedCount);
      }
      return status;
    }
    protected int rowsNeedingCommitCount() {
      int listCount = getRowCount();
      int needCount = 0;

      /*
      if (! (getList() instanceof JasiCommitableListIF)) {
        System.err.println("ERROR list must implement JasiCommitableListIF");
        return -1;
      }
      */
      AbstractCommitableList modelList = (AbstractCommitableList) getList();
      
      for (int idx=0; idx<listCount; idx++) {
        if( ((JasiCommitableIF) modelList.get(idx)).getNeedsCommit()) needCount++;
      }
      return needCount;
    }
    protected boolean rejectAction(String actionMsg) {
        return (JOptionPane.showConfirmDialog(this, actionMsg, "Confirm", JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE) != JOptionPane.YES_OPTION);
    }

    protected boolean deleteRow() {
        int rowid = getSelectedRowIndex();
        if (rowid < 0) {
//              aLog.logTextnl(panelName + ": No selected row in table - make a selection and retry delete.");
            InfoDialog.informUser(this, "ERROR",
                panelName +
                ": No selected row in table - make selection, retry delete.",
                statusLabel);
            return false;
        }
        if (rejectAction("Delete selected row from list")) return false;
        boolean retVal = dataTable.deleteRow(rowid);
        if (retVal) {
            Debug.println(panelName +
                 ": row delete SUCCESS at index: " + rowid);
            //setAllFlags(); // not needed with listeners.
        }
        else {
            // user may have redone it, re-delete returns false, so check - aww 02/04/2005
            JasiCommitableIF jc = getSelectedRow();
            if ( jc != null && ! jc.isDeleted() ) 
              aLog.logTextnl(panelName + ": row delete FAILURE at index: " + rowid );
        }
        return retVal;
    }
    protected void undeleteRows() {
      getModel().undelete();  //JasiListModel
    }

    protected void purgeRows() {
      if(rejectAction("Delete ALL rows from list")) return;
      getModel().purgeRows();  //JasiListModel
      aLog.logTextnl(panelName +
                  ": DELETED ALL ROWS from table list.");
      //setAllFlags(); // not needed with listeners.
    }

    protected abstract void stripRows() ;
    /*
    protected void stripRows() {
      if (jopStrip == null) jopStrip = new StripOptionPane();
      jopStrip.presentDialog();
      String choiceStr = jopStrip.getSelectionString();
      int count = 0;
      if (choiceStr.equals("RMS") ) {
        double rmsCut = jopStrip.getRMSCutoffValue();
        aLog.logText("rmsCutoff = " +rmsCut+" ");
        // need strip methods to return counts for decision to setStale
        int count = 
        getModel().stripByResidual(rmsCut);  //requires model IF abstract method
      }
      //if (count > 0) setAllFlags(); // not needed with listeners.
      aLog.logTextnl(panelName + ": stripped row count: " + count);
    }
    */
     public boolean copyRow() {
        boolean retVal = false;
        int index = getSelectedRowIndex();
        //Debug.println(panelName+": DEBUG: b4 row copy at index: " + index);
        if (index >= 0) {
          //retVal = dataTable.copyRow(index, true);
          retVal = dataTable.copyRow(index);
          if (retVal) {
            Debug.println(panelName + ": row copy SUCCESS at index: " + index);
            //setAllFlags(); // not needed with listeners
          }
          else {
            aLog.logTextnl(panelName +
                 ": row copy FAILURE at index: " + index);
          }
        }
        else {
          InfoDialog.informUser(getParent(), "ERROR",
              panelName +
                 ": You must first select a row to copy.",
                 statusLabel);
        }
        return retVal;
    }
    public void setTextArea(JTextArea textArea) {
        aLog.setTextArea(textArea);
        if (dataTable != null) dataTable.getTextLogger().setTextArea(textArea);
    }
    public JTextArea getTextArea() {
        return aLog.getTextArea();
    }
    public TextLogger getTextLogger() {
        return aLog;
    }
    public void setTextLogger(TextLogger logger) {
        if (logger != null) {
          aLog = logger;
          if (dataTable != null) dataTable.setTextLogger(aLog);
        }
    }

// JasiListPanel inner classes
    protected class StripOptionPane extends JOptionPane {
        protected JComboBox jcb;
        protected JDialog jdStrip;

        public StripOptionPane() {
            super();
            initOptionPane();
        }
        protected void initOptionPane() {
            super.setMessage(createInputs());
            super.setMessageType(JOptionPane.QUESTION_MESSAGE);
            super.setOptionType(JOptionPane.DEFAULT_OPTION);
            createOptions();
        }
        protected Object [] createInputs() {
            createRMSComboBox();
            return new Object [] {new JLabel("Enter rms cutoff"), jcb };
        }
        protected void createRMSComboBox() {
            Double [] rmsValues = {Double.valueOf(.1), Double.valueOf(.25),
                    Double.valueOf(.50), Double.valueOf(1.0), Double.valueOf(2.)};
            jcb = new JComboBox(rmsValues);
            jcb.setEditable(true);
        }
        protected void createOptions() {
            super.setOptions(new String [] {"RMS","CANCEL"});
            super.setInitialValue("CANCEL");
        }
        public void presentDialog() {
              jdStrip = jopStrip.createDialog(this, "Strip Rows");
              jdStrip.pack();
              //jdStrip.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
              jdStrip.setVisible(true);
        }
        public String getSelectionString() {
            return (String) getValue();
        }
        public double getRMSCutoffValue() {
            int idx = jcb.getSelectedIndex();
            Object value = jcb.getSelectedItem();
            if (idx == -1 || value instanceof String) {
               return 9999.99;
            }
            else if (value instanceof Double) {
               return ((Double) value).doubleValue();
            }
            else return 0.;
        }
    }

    private class UpdateTablePanel implements Runnable {
        public void run() {
            tableTimer.stop();
            //Debug.println("AJLP debug tableRowCount: " + tableRowCount);
            if (tableRowCount > 0) remove(waitRequestLabel);
            else {
                waitRequestLabel.setText("Table results displayed here");
                waitRequestLabel.setIcon(null);
            }
            setCursor(defaultCursor);

            if (tableRowCount > 0) {
              AbstractJasiListPanel.this.add(spane, BorderLayout.CENTER);
              //Debug.println("AJLP added spane to Panel");
            }
            else if (tableRowCount < 0) {
              InfoDialog.informUser(getParent(), "ERROR",
                  panelName +
                  ": Table creation failed; check input.", statusLabel);
            }
            else if (tableRowCount == 0) {
              Debug.println(panelName + ": No table rows satisfy input properties.");
            }
            if (tableRowCount == 1) setSelectedRow(0);
            updateStatus();
            AbstractJasiListPanel.this.revalidate();
            tableThread = null;
            interruptRequestFlag = false;
            unsetTableModified(); // 03/03 update after virgin creation
        }
    }

    protected class ModifyTableRowsActionListener implements ActionListener {
        public void actionPerformed(ActionEvent evt) {
            statusLabel.setText(" ");
            String cmd = evt.getActionCommand();
//                Object src = evt.getSource();
            if ( !(cmd.equals("Insert") || cmd.equals("Reload")) &&
               AbstractJasiListPanel.this.hasInvalidTable()) return;
            if (cmd.equals("Insert")) {
                insertRow();
            }
            else if (cmd.equals("Sort")) {
              // need popup dialog here with list sorting options, select option, ok
              // then sort input list then do
              dataTable.getTableSorter().resetSort();
              //getModel().setList(inputList);  //aww this kludge removed 03/18/03
            }
            else if (cmd.equals("Clone")) {
              copyRow();
            }
            else if (cmd.equals("Delete")) {
                deleteRow();
            }
            else if (cmd.equals("Strip")) {
                stripRows();
            }
            else if (cmd.equals("UnStrip")) {
                undeleteRows();
            }
            else if (cmd.equals("Commit")) {
                commit();
            }
            /*
            else if (cmd.equals("Sync")) {
                aLog.logTextnl("Saved " + synchInputList() + " input rows in parent Solution lists.");
            }
            else if (cmd.equals("Purge")) {
                purgeRows();
            }
            else if (cmd.equals("Reload")) {
                // Implement to restore data e.g. after purge 
                // getModel().restore(); // don't skip deleted rows
            }
            */
            updateStatus();
        }
    }
    protected class SaveTableActionListener implements ActionListener {
        //SaveTableToASCII saveToTextFile = new SaveTableToASCII();
        //SaveTablePanelToFile saveToTextFile = new SaveTablePanelToFile();
        SaveJasiListTablePanelToFile saveToTextFile = new SaveJasiListTablePanelToFile();
        Frame frame = null;
        public void actionPerformed(ActionEvent evt) {
            statusLabel.setText(" ");
            if (getTable().getRowCount() <= 0 ) {
              InfoDialog.informUser(getParent(), "ERROR",
                  panelName + ": No rows in table to save.",
                  statusLabel);
              return;
            }
            boolean success =
                saveToTextFile.save(AbstractJasiListPanel.this, getFrame());
            if (! success) {
                InfoDialog.informUser(getParent(), "ERROR",
                  panelName + ": Save to file FAILURE - see output text.",
                  statusLabel);
            }
        }
        protected Frame getFrame() {
          if (frame == null) {
            Component ancestor = getTopLevelAncestor();
            if (ancestor instanceof Frame)
              frame = (Frame) ancestor;
            else {
              frame = new Frame();
              frame.setVisible(true);
            }
          }
          return frame;
        }
    }

    class TableTimerActionListener implements ActionListener {
        public void actionPerformed(ActionEvent evt) {
            AbstractJasiListPanel.this.waitRequestLabel.setText("table rendering wait...");
            AbstractJasiListPanel.this.waitRequestLabel.setIcon(AbstractJasiListPanel.this.imageFlag ? LIGHT_BULB : DARK_BULB);
            AbstractJasiListPanel.this.imageFlag = ! AbstractJasiListPanel.this.imageFlag;
            AbstractJasiListPanel.this.waitRequestLabel.repaint();
        }
    }
    protected class WindowCloserListener extends WindowAdapter {
        public void windowClosing(WindowEvent evt) {
            Debug.println("Window Closing Event for: " +
                  AbstractJasiListPanel.this.getClass().getName() +
                  " table modified: " + hasModifiedTable() + 
                  "\n    event source: " + evt.getSource().getClass().getName());
            stopTableCellEditing();
            if (AbstractJasiListPanel.this.hasModifiedTable()) {
               AbstractJasiListPanel.this.commit();
            }
        }
    }
    protected class AnAncestorListener implements AncestorListener {
        private boolean haveWindow = false;
        public void ancestorRemoved(AncestorEvent e) {}
        public void ancestorMoved(AncestorEvent e) {}
        public void ancestorAdded(AncestorEvent e) {
            if (haveWindow) return;
            Debug.println("Ancestor window listener added for: " + AbstractJasiListPanel.this.getClass().getName());
            Container topComponent = e.getComponent().getTopLevelAncestor();
            if (topComponent instanceof Window) {
               AbstractJasiListPanel.this.addWindowListener((Window)topComponent);
               haveWindow=true;
            }
        } 
    }
    
    protected JPanel createMenuButtonPanel() {
        JPanel menuPanel = new JPanel();
        menuPanel.setLayout(new BorderLayout());
        menuPanel.add(createMenuBar(),BorderLayout.NORTH);
        menuPanel.add(createButtonPanel(), BorderLayout.CENTER);
        return menuPanel;
    }

    private class TableColumnJCheckBoxMenuItem extends JCheckBoxMenuItem {
        TableColumn tc;
        int colModelIndex = -1;
        TableColumnJCheckBoxMenuItem(TableColumn tc, int colModelIndex) {
          super((String)tc.getIdentifier());
          this.tc = tc;
          this.colModelIndex = colModelIndex;
        }
        TableColumn getTableColumn() {return tc;}
        int getTableColumnModelIndex() {return colModelIndex;}
    }
          
    // The menubar probably should be moved to AbstractJasiListTable class
    // the ..ListTable class could probably be reworked to extend JPanel
    private JMenuBar jmBar;
    protected JMenuBar createMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("Columns", true); // tear-off ok
        menu.setMnemonic(KeyEvent.VK_C);
        menu.setToolTipText("Select visible table columns.");
        menuBar.add(menu); //menu bar position 0
        
        // TODO: future property configuration through menu listings 
        //menu = new JMenu("Settings");
        //menu.setToolTipText("Panel table or model property settings.");
        //menu.add(cbMenuItem);
        //menuBar.add(menu);


        menu = new JMenu("Properties");
        menu.setToolTipText("Dump table panel properties.");
        menu.add( new AbstractAction("Save") {
            public void actionPerformed(ActionEvent e) {
              dataTable.saveTableProperties();
              aLog.logTextnl("Properties saved for " + panelName);
            }
        });
        menu.add( new AbstractAction("List") {
            public void actionPerformed(ActionEvent e) {
              aLog.logTextnl("Properties listed for " + panelName);
              dataTable.listTableInputProperties();
            }
        });

        // Kludge here until properties sorted out between panel, table, and model
        // Test case:
        menu.addSeparator();
        JCheckBoxMenuItem cbMenuItem = 
          new JCheckBoxMenuItem("Model row logging"); 
        cbMenuItem.setActionCommand("logModelChanges");
        boolean tf = this.dataTable.inputProperties.getBoolean("logModelChanges");
        cbMenuItem.setSelected(tf);
        getModel().logChanges(tf);
        cbMenuItem.addItemListener( new ItemListener() {
          public void itemStateChanged(ItemEvent e) {
            JCheckBoxMenuItem source = (JCheckBoxMenuItem) e.getSource();
            boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
            String cmd = source.getActionCommand();
            dataTable.inputProperties.setProperty(cmd,String.valueOf(selected));
            getModel().logChanges(selected);
          }
        });
        menu.add(cbMenuItem);

        menu.setMnemonic(KeyEvent.VK_P);
        menuBar.add(menu); // menu bar position 1

        
        jmBar = menuBar;
        return menuBar;
    }

    protected void createTableColumnVisibleMenu() {
        JCheckBoxMenuItem cbMenuItem;
        //menu.add("toggle shown ...");
        JMenu menu = jmBar.getMenu(0); // first Menu in MenuBar
        menu.addSeparator();
        TableColumnModel tcm = dataTable.defaultColumnModel;
        ArrayList hiddenColumnList = dataTable.getHiddenColumns();
        for (int icol=0; icol < tcm.getColumnCount(); icol++) {
          //cbMenuItem = new JCheckBoxMenuItem(getModel().getColumnName(icol));
          TableColumn tc = tcm.getColumn(icol);
          cbMenuItem = new TableColumnJCheckBoxMenuItem(tc, icol);
          cbMenuItem.setSelected(! hiddenColumnList.contains(tc));
          cbMenuItem.addItemListener( new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                TableColumnJCheckBoxMenuItem source =
                   (TableColumnJCheckBoxMenuItem) e.getSource();
                boolean selected = (e.getStateChange() == ItemEvent.SELECTED);
                /*
                aLog.logTextnl(
                    "Event source: " + source.getText() + "\n" +
                    "for class: " +  source.getClass().getName() + "\n" +
                    "selected: " + selected;
                );
                */
                if (selected) {
                  try {
                    dataTable.getColumn(source.getText());
                  }
                  catch (IllegalArgumentException ex) {
                    dataTable.addTableColumn(source.getTableColumn(),
                                    source.getTableColumnModelIndex());
                  }
                }
                else { // if not nullable can't hide?
                  dataTable.removeTableColumn(source.getTableColumn());
                }
                updateStatus();
            }
          });
          menu.add(cbMenuItem);
          if ((icol%10) == 9) {
            menu.addSeparator();
            JMenu submenu = new JMenu("more...");
            menu.add(submenu);
            menu = submenu;
          }
        }
        menu.addSeparator();
    }

    /*
    // assume a sort key built in order table columns are selected
    // in gui dialog activated via button option add menu panel,
    // select ok when done, 
    // using selected items sort table rows
    protected JButton sortButton = new JButton("Sort");
    protected ArrayList tableColumnSortKeyList = new ArrayList(count);
    protected void createTableColumnSortMenu() {
        JCheckBoxMenuItem cbMenuItem;
        TableColumnModel tcm = dataTable.getTableColumnModel();
        int count = tcm.getColumnCount();
        tableColumnSortKeyList = new ArrayList(count);
        JMenu menu = new JMenu();
        for (int icol=0; icol < tcm.getColumnCount(); icol++) {
          TableColumn tc = tcm.getColumn(icol);
          cbMenuItem = new TableColumnJCheckBoxMenuItem(tc, icol);
          cbMenuItem.setSelected(false);
          cbMenuItem.addItemListener( new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
              TableColumnJCheckBoxMenuItem source =
                   (TableColumnJCheckBoxMenuItem) e.getSource();
              boolean selected =
                  (e.getStateChange() == ItemEvent.SELECTED);
              if (selected) {
                tableColumnSortKeyList.add(e.getTableColumn());
              }
              else {
                tableColumnSortKeyList.remove(e.getTableColumn());
              }
              updateStatus();
            }
          });
          menu.add(cbMenuItem);
          if ((icol%10) == 9) {
            menu.addSeparator();
            JMenu submenu = new JMenu("more...");
            menu.add(submenu);
            menu = submenu;
          }
        }
        menu.addSeparator();
    }
    */

} // end of AbstractJasiListPanel class
