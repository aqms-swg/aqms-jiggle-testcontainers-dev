package org.trinet.util.graphics.table;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;
import org.trinet.util.graphics.*;

public abstract class AbstractJasiListTableModel extends AbstractTableModel
  implements JasiListTableModelIF, ListDataStateListener {

    protected boolean deleteRemovesRowFromList = true; // menu/property setting?

    protected boolean logChanges = false;
    protected boolean cellEditable = false;
    protected TextLogger aLog = new TextLogger();
    protected ActiveArrayListIF modelList = null;
    protected int maxFields;
    protected int deleteColumnIndex;
    protected String []  tableColumnNames;
    protected Class   [] tableColumnClasses;
    protected boolean [] tableColumnAKey;
    protected boolean [] tableColumnShown;
    protected boolean [] tableColumnEditable;
    protected boolean [] tableColumnNullable;
    protected int [] tableColumnFractionDigits;
    protected int [] tableColumnCellWidthPadding;
    public static final DataDouble NULL_DATADOUBLE = new DataDouble();
    public static final DataString NULL_DATASTRING = new DataString();
    public static final DataLong NULL_DATALONG     = new DataLong();
    protected GenericPropertyList modelProps = new GenericPropertyList(); // default empty list?

    protected AbstractJasiListTableModel() {}

    protected AbstractJasiListTableModel(GenericPropertyList gpl) {
       this.modelProps = gpl;
    }

    protected AbstractJasiListTableModel(ActiveArrayListIF jasiCommitableList) {
        this(jasiCommitableList, false);
    }

    protected AbstractJasiListTableModel(ActiveArrayListIF jasiCommitableList, boolean cellEditable) {
        setList(jasiCommitableList);
        this.cellEditable = cellEditable;
    }

    public void setProperties(GenericPropertyList gpl) {
      modelProps = gpl;
    }

    protected GenericPropertyList getProperties() {
      return modelProps;
    }

    protected void setDeleteRemovesRowFromList(boolean tf) { // could set from panel property/menu?
      deleteRemovesRowFromList = tf;
    }

    public int getDeleteColumnIndex() { return deleteColumnIndex;}
    public void setDeleteColumnIndex(int index) {
        deleteColumnIndex = index;
    }
    public void setCellEditable(boolean cellEditable) {
        this.cellEditable = cellEditable;
    }

    protected static int valueToInt(Object value) {
        if (value == null) return 0;
        int intValue = 0; 
        if (value instanceof String) {
            intValue = Integer.parseInt((String)value);
        }
        else if (value instanceof DataNumber) {
            //DataNumber dn = (DataNumber) value;
            //intValue = (dn.isValidNumber()) ? dn.intValue() : 0;
            intValue = ((DataNumber)value).intValue();
        }
        else if (value instanceof Number) {
            intValue = ((Number)value).intValue();
        }
        return intValue;
    }
    protected static long valueToLong(Object value) {
        if (value == null) return 0l;
        long longValue = 0l; 
        if (value instanceof String) {
            longValue = Long.parseLong((String)value);
        }
        else if (value instanceof DataNumber) {
            //DataNumber dn = (DataNumber) value;
            //longValue = (dn.isValidNumber()) ? dn.longValue() : 0l;
            longValue = ((DataNumber)value).longValue();
        }
        else if (value instanceof Number) {
            longValue = ((Number)value).longValue();
        }
        return longValue;
    }
    protected static double valueToDouble(Object value) {
        if (value == null) return 0.;
        double doubleValue = 0.; 
        if (value instanceof String) {
            doubleValue = Double.parseDouble((String)value);
        }
        else if (value instanceof DataNumber) {
            //DataNumber dn = (DataNumber) value;
            //doubleValue = (dn.isValidNumber()) ? dn.doubleValue() : 0.;
            doubleValue = ((DataNumber)value).doubleValue();
        }
        else if (value instanceof Number) {
            doubleValue = ((Number)value).doubleValue();
        }
        return doubleValue;
    }

    protected void sortList() {};

    // FYI design
    // To eliminate complex list updating model could alias the input list.
    // and NOT create a new list (updates, addition done directly to original).
    // however would need to change delete,undelete,purge rows method behavior
    // TODO: addListDataStateListeners/removeListDataStateListeners notify tableModel of changes and fireEvents -AWW
    public void setList(ActiveArrayListIF jasiCommitableList) {
        if (modelList != jasiCommitableList) {
          if (modelList != null) modelList.removeListDataStateListener(this);
          modelList = jasiCommitableList;
          if (modelList != null) modelList.addListDataStateListener(this);
        }
        //System.out.println("DEBUG sortList in TableModel");
        sortList(); // implement a default sort, in subclass override
        // fire below may not needed if listDataStateListener added. -AWW
        if (modelList.size() > 0) fireTableDataChanged(); // inform model, sorter etc. about new contents
        //System.out.println("AJLTM Debug setList fireTableDataChanged()");
    }

    public ActiveArrayListIF getList() {
        return modelList;
    }

// Extend AbstractTableModel class and override
    public int getColumnCount() {
        return maxFields;
    }
    public Class getColumnClass(int colIndex) {
        return tableColumnClasses[colIndex];
    }
    public String getColumnName(int colIndex) {
        return tableColumnNames[colIndex];
    }
    public int getRowCount() {
        return (modelList == null) ? 0 : modelList.size();
    }
    public boolean copyRowAt(int rowIndex) {
        return copyRowAt(rowIndex, true);
    }
    public boolean copyRowAt(int rowIndex, boolean after) {
        if (rowIndex < 0 || rowIndex > getRowCount()-1) return false;
        boolean retVal = false;
        JasiCommitableIF jcRow = (JasiCommitableIF) modelList.get(rowIndex);
        JasiCommitableIF newRow = null;
        try {
          newRow =
            (JasiCommitableIF) JasiObject.newInstance(jcRow.getClass().getName());
          if (after) rowIndex++;
          modelList.add(rowIndex, newRow);
          if (logChanges) aLog.logTextnl("copied " + jcRow.toString());
          //fireTableRowsInserted(rowIndex, rowIndex);
          retVal = true;
        }
        catch (Exception ex) {
          ex.printStackTrace();
        }
        return retVal;
    }
    public JasiCommitableIF getJasiCommitable(int rowIndex) {
        return (modelList == null || modelList.size() == 0
                || rowIndex >= modelList.size() || rowIndex < 0) ?
            (JasiCommitableIF) null : (JasiCommitableIF) modelList.get(rowIndex);
    }
    public boolean isCellNullable(int rowIndex, int colIndex) {
        return tableColumnNullable[colIndex];
    }
    public boolean isCellEditable(int rowIndex, int colIndex) {
        if (! cellEditable) return false;
        if (colIndex == deleteColumnIndex)  {
                return true;
        }
        else if (getJasiCommitable(rowIndex).isDeleted()) {
                return false;
        }
        else {
                return tableColumnEditable[colIndex];
        }
    }
    public int findColumn(String name) {
        String test = name.trim().toUpperCase();
        for (int index = 0; index < maxFields; index++) {
            if (test.equals(getColumnName(index))) return index;
        }
        return -1;
    }
// end of AbstractTableModel methods
    public boolean [] getTableColumnShown() { return tableColumnShown;}
    public boolean [] getTableColumnEditable() { return tableColumnEditable;}
    public boolean [] getTableColumnNullable() { return tableColumnNullable;}
    public int [] getTableColumnCellWidthPadding() { return tableColumnCellWidthPadding;}
    public int [] getTableColumnFractionDigits() { return tableColumnFractionDigits;}
    public String [] getTableColumnNames() { return tableColumnNames;}

    public boolean isKeyColumn(int index) {
        return tableColumnAKey[index];
    }
    public boolean isKeyColumn(String name) {
        return tableColumnAKey[findColumn(name)];
    }
    public int getColumnFractionDigits(int index) {
        return tableColumnFractionDigits[index];
    }
    public boolean isColumnShown(int index) {
        return tableColumnShown[index];
    }
    public boolean isColumnHidden(int index) {
        return ! tableColumnShown[index];
    }
    public void setTableColumnShown(boolean [] tableColumnShown) {
        if (tableColumnShown.length != maxFields)
            throw new IndexOutOfBoundsException("tableColumnsShown:"+tableColumnShown.length+" != maxFields:"+maxFields);
        this.tableColumnShown = tableColumnShown;
    }
    public void setTableColumnEditable(boolean [] tableColumnEditable) {
        if (tableColumnEditable.length != maxFields) throw new IndexOutOfBoundsException();
        this.tableColumnEditable = tableColumnEditable;
    }
    public void setTableColumnNullable(boolean [] tableColumnNullable) {
        if (tableColumnNullable.length != maxFields) throw new IndexOutOfBoundsException();
        this.tableColumnNullable = tableColumnNullable;
    }
    public void setTableColumnCellWidthPadding(int [] tableColumnCellWidthPadding) {
        if (tableColumnCellWidthPadding.length != maxFields) throw new IndexOutOfBoundsException();
        this.tableColumnCellWidthPadding = tableColumnCellWidthPadding;
    }
    public void setTableColumnFractionDigits(int [] tableColumnFractionDigits) {
        if (tableColumnFractionDigits.length != maxFields) throw new IndexOutOfBoundsException();
        this.tableColumnFractionDigits = tableColumnFractionDigits;
    }

// Methods to modify table rows;
    public void purgeRows() {
      for (int idx = getRowCount()-1; idx >= 0 ; idx--) {
        deleteRow(idx);
      }
    }

    public void undelete() {
      int count = getRowCount();
      for (int idx = 0; idx < count; idx++) {
        JasiCommitableIF reading = getJasiCommitable(idx);
        if (reading.isDeleted()) setValueAt("false", idx, deleteColumnIndex);
        fireTableRowsUpdated(idx, idx);
      }
    }
    public boolean deleteRow(int rowIndex) {
        JasiCommitableIF jasiCommitable = ((JasiCommitableIF) modelList.get(rowIndex));
        if (jasiCommitable == null) return false;
        // set delete flag of instance:
        boolean retVal = true;
        if ( ! jasiCommitable.isDeleted() ) {
          jasiCommitable.setDeleteFlag(true);
        }
        else retVal = false;
        // property test says totally remove instance from list too?
        if (deleteRemovesRowFromList) modelList.remove(rowIndex);
        //else fireTableRowsUpdated(rowIndex, rowIndex); // 03/03 aww?
        if (retVal) {
          if (logChanges) aLog.logTextnl("deleted "+jasiCommitable.toString());
          //fireTableRowsDeleted(rowIndex, rowIndex);
        }
        return retVal;
    }

    public JasiCommitableIF initRowValues(JasiCommitableIF aNewRow) {
        //JasiSourceAssociationIF jasiSource = (JasiSourceAssociationIF) aNewRow;
        aNewRow.setAuthority(EnvironmentInfo.getNetworkCode());
        aNewRow.setSource(EnvironmentInfo.getApplicationName());
        aNewRow.setProcessingState(EnvironmentInfo.getAutoString());
        return aNewRow;
    }

    // this method wrapper is so subclasses can override behavior if needed
    // NOTE: associations are correct if addOrReplace is only method invoking this
    // otherwise have to set assign sol or mag in subclass override of this method
    protected boolean addRowToList(int rowIndex, JasiCommitableIF jasiCommitable) {
       // take all comers: if (modelList.contains(jasiCommitable)) return false; // uses equals(), beware
       modelList.add(rowIndex, jasiCommitable);
       //fireTableRowsInserted(rowIndex, rowIndex);
       if (logChanges) aLog.logTextnl("inserted row: "+rowIndex+" "+jasiCommitable.toString());
       return true;
    }
    public boolean insertRow(int rowIndex, JasiCommitableIF jasiCommitable) {
       return addRowToList(rowIndex, jasiCommitable);
    };
    /** Replaces row using equals() equivalence, else adds. */
    public boolean addOrReplaceRow(JasiCommitableIF jasiCommitable) {
      return addOrReplaceRow(modelList.size(), jasiCommitable);
    }
    protected boolean addOrReplaceRow(int rowIndex, JasiCommitableIF jasiCommitable) {
      int idx = modelList.indexOf(jasiCommitable); // locates via jc.equals()
      return (idx < 0 ) ?
        addRowToList(rowIndex, jasiCommitable) : updateRowInList(idx, jasiCommitable);
    }
    protected boolean updateRowInList(int rowIndex, JasiCommitableIF jasiCommitable) {
      // ((JasiCommitableIF)modelList.get(rowIndex)).setDeleteFlag(true); //alter delete state of old data?
      modelList.set(rowIndex, jasiCommitable, true); // from ActiveArrayList
      //fireTableRowsUpdated(rowIndex, rowIndex);
      if (logChanges) aLog.logTextnl(" updated row: "+rowIndex+" "+jasiCommitable.toString());
      return true;
    }

    public int updateDB(int index) {
      try {
        return (((JasiCommitableIF) modelList.get(index)).commit()) ? 1 : 0;
      }
      catch (JasiCommitException ex) {
        aLog.logTextnl(ex.toString());
      }
      return 0;
    }
    public int updateDB() {
      int count = modelList.size();
      int commitCount = 0;
      for (int index = 0; index < count; index ++ ) {
        commitCount += updateDB(index);
      }
      //if (logChanges)
        aLog.logTextnl(getClass().getName() +
        " updateDB() count:" +count+ " success:" + commitCount +
        " failed: " + (count-commitCount)  + " for " + getClass().getName());
      return commitCount;
    }

    protected boolean cellChangeVerified(Object value, int rowIndex, int colIndex) {
        if ((value == null) && ! (tableColumnNullable[colIndex])) return false;
        Object currentValue = getValueAt(rowIndex,colIndex);
        if (logChanges) aLog.logTextnl("oldValue: " + currentValue.toString() +
                                  " newValue: " + value.toString() +
                                  " row: " + rowIndex + " col: " + colIndex);
        return ! ((value == null) ? (currentValue == null) : value.equals(currentValue));
    }

    public void setTextLogger(TextLogger logger) {
        if (logger != null) aLog = logger;
    }
    public TextLogger getTextLogger() {
        return aLog;
    }
    public void logChanges(boolean tf) { logChanges = tf;}

//
// IF ListDataStateListener
//
//TODO: explore the StateChange object sent along in event for more info on objects updated
//
  public void intervalAdded(ListDataStateEvent e) {
    Debug.println("Model - fire interval added in : " +
          e.getSource().getClass().getName() + " " + e.getIndex0() + " " + e.getIndex1());
    if (e.getIndex0() >= 0 && e.getIndex1() >= 0) {
      fireTableRowsInserted(e.getIndex0(),e.getIndex1());
    }
    else fireTableDataChanged();
  }
  public void intervalRemoved(ListDataStateEvent e) {
    Debug.println("Model - fire interval removed in : " +
          e.getSource().getClass().getName() + " " + e.getIndex0() + " " + e.getIndex1());
    if (e.getIndex0() >= 0 && e.getIndex1() >= 0) {
      fireTableRowsDeleted(e.getIndex0(),e.getIndex1());
    }
    else fireTableDataChanged();
  }
  public void contentsChanged(ListDataStateEvent e) {
    Debug.println("Model - fire contents updated in : " +
          e.getSource().getClass().getName() + " " + e.getIndex0() + " " + e.getIndex1());
    if (e.getIndex0() >= 0 && e.getIndex1() >= 0) {
      fireTableRowsUpdated(e.getIndex0(),e.getIndex1());
    }
    else fireTableDataChanged();
  }
  public void stateChanged(ListDataStateEvent e) {
    Debug.println("Model - fire state updated in : " +
          e.getSource().getClass().getName() + " " + e.getIndex0() + " " + e.getIndex1());
    if (e.getIndex0() >= 0 && e.getIndex1() >= 0) {
      fireTableRowsUpdated(e.getIndex0(),e.getIndex1());
    }
    else fireTableDataChanged();
  }

  public void orderChanged(ListDataStateEvent e) {
    Debug.println("Model - order changed model list updated in : " +
          e.getSource().getClass().getName() + " " + e.getIndex0() + " " + e.getIndex1());
    fireTableChanged(new JasiListTableModelEvent(this, true));
  }
}
