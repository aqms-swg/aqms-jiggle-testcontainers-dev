package org.trinet.util.graphics.table;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.graphics.*;
// assumes list uniformly have save mag/sol associations
public abstract class AbstractJasiMagAssocTableModel
        extends AbstractJasiReadingTableModel implements JasiListTableModelAssocMagIF {

    protected Magnitude mag;

    //Setting true couples mag/sol list removal/delete actions:menu/property setting? 
    protected boolean synchDeletesWithSolList = false; // - made default false 07/26/2004 -aww

    protected AbstractJasiMagAssocTableModel() {
        super();
    }

    protected AbstractJasiMagAssocTableModel(ActiveArrayListIF associatedList) {
        this(associatedList, false);
    }

    protected AbstractJasiMagAssocTableModel(ActiveArrayListIF associatedList, boolean isCellEditable) {
        this();
        setList(associatedList);
        setCellEditable(isCellEditable);
    }

    protected void setSynchDeletesWithSolList(boolean tf) { // could set from panel property/menu?
      synchDeletesWithSolList = tf;
    }

    public Magnitude getAssocMagnitude() { return this.mag;}

    public void setAssocMagnitude(Magnitude mag) {
        this.mag = mag;
        if (mag != null) setAssocSolution(mag.getAssociatedSolution());
    }

    public void setList(ActiveArrayListIF aList) {
        super.setList(aList);
        if (this.modelList.size() > 0) {
           setAssocMagnitude(
             ((JasiMagnitudeAssociationIF) getJasiCommitable(0)).getAssociatedMag()
           );
        }
    }
    public JasiCommitableIF initRowValues(JasiCommitableIF aRow) {
        JasiMagnitudeAssociationIF jassoc =
            (JasiMagnitudeAssociationIF) aRow;
        // avoid update assoc list as side-effect of associate()
        jassoc.assign(mag);
        return super.initRowValues(jassoc); // solution assign
    }
    public boolean insertRow(int rowIndex, JasiCommitableIF jasiReading) {
       if (jasiReading == null) return false;
       JasiMagnitudeAssociationIF magReading = (JasiMagnitudeAssociationIF) jasiReading;
       if (this.mag != null && magReading.getAssociatedMag() != this.mag) magReading.assign(this.mag);
       boolean retVal = super.insertRow(rowIndex, magReading);
       //list/model listeners could add reading row to assoc solution list as well
       if (retVal) {
         if (this.mag != null) mag.getAssociatedSolution().add(magReading); // type abstraction
       }
       return retVal;
    }

    public boolean deleteRow(int rowIndex) {
        JasiCommitableIF jc = getJasiCommitable(rowIndex);
        if (jc == null) return false;
        boolean retVal = super.deleteRow(rowIndex);
        // property test says totally remove from sol too?
        if (retVal && synchDeletesWithSolList && deleteRemovesRowFromList) {
          if (this.mag != null) {
            Solution aSol = mag.getAssociatedSolution();
            if (aSol != null) aSol.remove((MagnitudeAssocJasiReading) jc);
          }
        }
        return retVal;
    }

    //Should it set channelmag wt to 0.?
    public int stripByResidual(double value) {
      if (value == 0. || Double.isNaN(value)) return 0;
      double absValue = Math.abs(value);
      int count = 0;
      int rows = modelList.size();
      MagnitudeAssocJasiReadingIF dataRow = null;
      for (int idx = 0; idx < rows; idx++) {
        dataRow = (MagnitudeAssocJasiReadingIF) modelList.get(idx);
        if (Math.abs(dataRow.getMagResidual()) >= absValue) {
          dataRow.setDeleteFlag(true); // delete();
          //dataRow.setReject(true); // up weight the reading -aww 08/23/2007 replaced
          count++;
          fireTableRowsUpdated(idx, idx);
        }
      }
      return count;
    }

    protected boolean addOrReplaceRow(int rowIndex, JasiCommitableIF jasiCommitable) {  // added override 05/29/2007  -aww
       JasiMagnitudeAssociationIF magReading = (JasiMagnitudeAssociationIF) jasiCommitable;
       if (this.mag != null && magReading.getAssociatedMag() != this.mag) magReading.assign(this.mag);
       return super.addOrReplaceRow(rowIndex, magReading);
    }

} // end of class
