package org.trinet.util.graphics.table;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.*;

// stale state to be controlled through tablemodel events not associate()
public abstract class AbstractJasiReadingTableModel 
        extends AbstractJasiAssociationTableModel {

    public static final String DUMMY_SITE_CODE = "XXX";
    public static final String DEFAULT_CHANNEL_LOCATION = "  ";
    public static final String [] DEFAULT_SEEDCHAN_MAP = {"EHZ", "EHN", "EHE"};
    public static final String [] DEFAULT_CHANNEL_MAP  = {"Z", "N", "E"};

    protected String dummyStaCode = DUMMY_SITE_CODE;
    protected String dummySeedchanCode = DUMMY_SITE_CODE;
    protected String defaultChannelLocation = DEFAULT_CHANNEL_LOCATION;
    protected String [] defaultSeedchanMap   = DEFAULT_SEEDCHAN_MAP;
    protected String [] defaultChannelMap = DEFAULT_CHANNEL_MAP;

    //Flags whether replaced readings in model should test for time match
    protected boolean replaceByTime = false;

    protected AbstractJasiReadingTableModel() {
        super();
    }

    protected AbstractJasiReadingTableModel(ActiveArrayListIF associatedList) {
        this(associatedList, false);
    }

    protected AbstractJasiReadingTableModel(ActiveArrayListIF associatedList, boolean isCellEditable) {
        this();
        setList(associatedList);
        setCellEditable(isCellEditable);
    }
/*
    public int stripByResidual(double value) {
      if (value == 0. || Double.isNaN(value)) return 0;
      if (Math.abs(value) <= 0.) return 0;
      JasiReading[] readings = (JasiReading [])modelList.toArray(new JasiReading[0]);
      int count = 0;
      for (int idx = 0; idx < readings.length; idx++) {
        if ( ! readings[idx].residual.isNull() && (Math.abs(readings[idx].residual.doubleValue()) >= value)) {
          readings[idx].setDeleteFlag(true);
          count++;
          fireTableRowsUpdated(idx, idx);
        }
      }
      return count;
    }
    public int stripByDistance(double value) {
      if (value <= 0. || Double.isNaN(value)) return 0;
      JasiReading[] readings = (JasiReading[]) modelList.toArray(new JasiReading[0]);
      int count = 0;
      for (int idx = 0; idx < readings.length; idx++) {
        if ( ! readings[idx].getChannelObj().dist.isNull() &&
          (readings[idx].getHorizontalDistance() >= value)) {
          readings[idx].setDeleteFlag(true);
          count++;
          fireTableRowsUpdated(idx, idx);
        }
      }
      return count;
    }
*/
    public boolean copyRowAt(int rowIndex, boolean after) {
      if (rowIndex < 0 || rowIndex > getRowCount()-1) return false;
      boolean retVal = false;
      JasiReading jcRow = (JasiReading) modelList.get(rowIndex);
      JasiReading newRow = null;
      try {
        newRow =
          (JasiReading) JasiObject.newInstance(jcRow.getClass().getName());
        initRowValues(newRow);
        Channel newChan = (Channel)(jcRow.getChannelObj().clone());
        newChan.getChannelId().setSeedchan(String.valueOf(getRowCount()));
        newChan.getChannelId().setChannel(newChan.getChannelId().getSeedchan());
        newRow.setChannelObj(newChan);
        newRow.setTime(jcRow.getTime());
        newRow.assign(jcRow.getAssociatedSolution()); //does not update state 
        if (after) rowIndex++;
        modelList.add(rowIndex, newRow);
        if (logChanges) aLog.logTextnl("copied " + jcRow.toString());
        //fireTableRowsInserted(rowIndex, rowIndex);
        retVal = true;
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
      return retVal;
    }
    public JasiCommitableIF initRowValues(JasiCommitableIF aRow) {
        JasiReading jr = (JasiReading) super.initRowValues(aRow);
        org.trinet.jasi.Channel chan = jr.getChannelObj();
        chan.setDistanceBearingNull();
        chan.setNet(jr.authority.toString());
        chan.setSta(dummyStaCode);
        //chan.setChannel("YYY");
        //chan.setSeedchan("ZZZ");
        chan.setSeedchan(String.valueOf(getRowCount()));
        chan.setChannel(chan.getChannelId().getSeedchan());
        chan.setAuth(jr.authority.toString());
        chan.setChannelsrc(jr.authority.toString());
        chan.setLocation(defaultChannelLocation);
        jr.setTime(sol.datetime.doubleValue());
        return jr;
    }

    public void setProperties(GenericPropertyList gpl) {
       super.setProperties(gpl);
       defaultChannelLocation = gpl.getProperty("defaultChannelLocation", defaultChannelLocation);
       dummyStaCode = gpl.getProperty("dummyStaCode", dummyStaCode);
       dummySeedchanCode = gpl.getProperty("dummySeedchanCode", dummySeedchanCode);

       String [] dummyArray = gpl.getStringArray("channelMap");
       if (dummyArray.length > 0) defaultChannelMap = dummyArray; // reset from current value
       dummyArray = gpl.getStringArray("seedchanMap");
       if (dummyArray.length > 0) defaultSeedchanMap = dummyArray; // reset from current value

    }
    protected String getSeedchanMap(String chan) {
      int count = defaultChannelMap.length;
      for (int idx = 0; idx < count; idx++) {
        if ( defaultChannelMap[idx].equals(chan) ||
             defaultSeedchanMap[idx].equals(chan) ) return defaultSeedchanMap[idx];
      }
      // not defined, return input?
      return chan; // or dummySeedchanCode;
    }

    // override superclass noop to define initial list sort as a time sort
    protected void sortList() {
      //((JasiReadingListIF) modelList).timeSort(true);
      //((JasiReadingListIF) modelList).channelSort(true, JasiReadingListIF.SITE_TIME_SORT);
      ((JasiReadingListIF) modelList).distanceSort(true, JasiReadingListIF.SITE_TIME_SORT);
    }

    /* Override here depends behavior of indexOf using jc.equals() in superclass.
     * Looks for equivalent(r), (cf. getLikeWithSameTime(jr)?) 
     */
    protected boolean addOrReplaceRow(int rowIndex, JasiCommitableIF jasiCommitable) {
       JasiReadingIF jr = (JasiReadingIF) jasiCommitable;
       if (this.sol != null && jr.getAssociatedSolution() != this.sol) jr.assign(this.sol); // added 05/29/2007 - aww
       // getLike...() getSame...()?
       JasiCommitableIF oldData = (replaceByTime) ?
          ((JasiReadingListIF) modelList).getLikeWithSameTime(jr)
          : ((JasiReadingListIF) modelList).getLikeChanType(jr);
       if (oldData == null) {
         return addRowToList(rowIndex, jr);
       }
       //oldData.setDeleteFlag(true); // is default to virtually delete?
       return updateRowInList(modelList.indexOf(oldData), jr);
    }

    // Added override below as test - aww 02/03
    public boolean insertRow (int rowIndex, JasiCommitableIF jc) {
       // Equivalent addOrReplace for amps, codas, phase test for 
       // similarity of contents rather than object identity.
       // indexOfEquivalent vs. indexOf in AbstractCommitableListIF
       // overrides method in super class.
       return addOrReplaceRow(rowIndex, jc);
    }

}
