package org.trinet.util.graphics.table;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.util.*;

public abstract class AbstractJasiReadingTextParser extends AbstractJasiTableRowTextParser {

  protected String sta;
  protected String chan;
  protected String seedchan;
  protected String phase;
  protected String defaultPhase;
  protected String [] channelMap;
  protected String [] seedchanMap;
  protected String defaultChannelSrc  = defaultAuth;
  public final String DEFAULT_LOCATION =  "  "; // changed 01 to "  " -aww 12/08/2005
  protected String defaultLocation  = DEFAULT_LOCATION;

  AbstractJasiReadingTextParser() {}

  public void setProperties(GenericPropertyList parserProps) {
      super.setProperties(parserProps);
      if (parserProps != null) {
        defaultLocation = parserProps.getProperty("defaultChannelLocation", defaultLocation);
        if ( defaultLocation.equals("") || defaultLocation.equals("--") ) defaultLocation = DEFAULT_LOCATION; 
        defaultChannelSrc = parserProps.getProperty("defaultChannelSrc", defaultAuth);
        //System.out.println("AbstractJasiReadingTextParser setProperties defaultChannelLocation: \""+defaultLocation+"\" ChannelSrcMap : "+defaultChannelSrc);
      }
  }
  protected void setChannel(JasiReading jr, String net, String sta, String chan, String seedchan) {
      ChannelName chanName = jr.getChannelObj().getChannelName();
      chanName.setNet(net);
      chanName.setSta(sta);
      chanName.setChannel(chan);
      chanName.setSeedchan(seedchan);
      chanName.setAuth(defaultAuth);
      chanName.setSubsource(defaultNetworkCode);
      chanName.setLocation(defaultLocation);
      chanName.setChannelsrc(defaultChannelSrc);
      jr.setSource(defaultSource);
  }
  protected boolean parseChannelName(StringTokenizer strToke) {
    sta = strToke.nextToken().toUpperCase();
    chan = strToke.nextToken().toUpperCase();
    seedchan = toSeedchan(chan);
    return (seedchan == null || seedchan.equals("")) ? false : true;
  }
  protected String toSeedchan(String chan) {
    if (channelMap == null && parserProps != null) {
      channelMap = parserProps.getStringArray("channelMap");
      seedchanMap = parserProps.getStringArray("seedchanMap");
    }
    if (channelMap != null) {
      if ( seedchanMap == null || (seedchanMap.length != channelMap.length) ) {
          System.err.println("Error: Check below values of table parser properties for: " + getClass().getName());
          System.err.println(" channelMap=" +  parserProps.getProperty("channelMap"));
          System.err.println(" seedchanMap=" + parserProps.getProperty("seedchanMap"));
      }
      else {
        for (int idx = 0; idx < channelMap.length; idx++) {
          if (chan.equals(channelMap[idx]) || chan.equals(seedchanMap[idx]))
            return seedchanMap[idx];
        }
      }
    }
    return (chan.length() == 3) ? chan : "XYZ";
  }
  /* Override assumes listDataListener to fire for this to work. 
  protected boolean addOrReplaceRow(JasiCommitableIF jc) {
    // first finds equivalent, if found then looks up index (thus two loops).
    JasiReadingListIF jrl =  (JasiReadingListIF) tablePanel.getList();
    jrl.addOrReplaceWithSameTime(jc);
  }
  */
}
