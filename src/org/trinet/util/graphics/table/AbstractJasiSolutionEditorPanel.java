// TODO: abstract identifier members as convenience tags in jasi data type classes (for list member equivalences) 
//TODO: enable/disable of buttons as per Actions propertyChange listener for stale/commit or other state.
//TODO: property/menu settable option to reset default "stale" commit state after fresh load
package org.trinet.util.graphics.table;
import java.beans.*;
import java.awt.*;
import java.awt.event.*;
import java.text.*;
import java.sql.Connection;
import java.util.*; // removed 02/15/2005 -aww
import javax.swing.*;
import javax.swing.plaf.basic.*;
import javax.swing.plaf.metal.*;
import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;
import org.trinet.jasi.magmethods.MagnitudeMethodIF;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;
import org.trinet.util.graphics.*;
import org.trinet.util.graphics.text.*;
import org.trinet.util.locationengines.*;

public abstract class AbstractJasiSolutionEditorPanel extends JPanel
   implements GraphicalHypoMagEngineDelegateIF, PropertyChangeListener {
    protected static boolean debug = false;
    public static final int PREFERRED_PANEL_WIDTH  = 980;
    public static final int PREFERRED_PANEL_HEIGHT = 250;
    public static final String DEFAULT_PROP_FILE_NAME = "mung.props"; // 02/15/2005 aww
    protected static final Insets inset = new Insets(0,0,0,0);
    // protected static GenericApplicationPrefs myPrefs; // static so global for subclasses once set. // remove 02/15/2005 -aww
    private JSplitPane jsp;
    protected TextLogger aLog = new TextLogger();
    protected JLabel aNameLabel;
    protected Solution aSol;
    protected Magnitude aMag;
    protected static WhereIsEngine whereEngine = null; // WhereIsEngine.create(); // used to be -aww

    // global for all data, behavior depends on data source update assumption
    static protected boolean restoreStateOnLoad = true; // default true test

    protected boolean upperControlsEnabled = true;
    protected boolean lowerControlsEnabled = true;
    protected boolean engineControlsEnabled = true;

    protected boolean enableWaveMagButton = false; // aww 05/2004
    protected boolean enableAllMagsCalc   = true; // aww 07/01/2004

    protected boolean commitButtonEnabled = false;
    protected boolean commitEnabled = false;
    protected boolean updateEnabled = false;
    protected boolean autoSolve = true; // resolves summary data derived from data list changes
    protected static HypoMagDelegatePropertyList editorProps;
    // TODO: collection of engine types for different analysis duties like HashMap by command name
    protected HypoMagEngineDelegateIF delegate;
    // TODO: move jbMag control below to an inner class of solution engine controls and use listeners to toggle
    protected JButton jbMag;
    protected JButton jbAllMags; // move to JasiMagnitudeAssocEditorPanel class?
    //protected JButton jbWaveMag;
    protected static boolean enableMagButton = false; // why, is there a feedback loop?

    public AbstractJasiSolutionEditorPanel() {
    }
    public AbstractJasiSolutionEditorPanel(Solution sol) {
      this();
      setSelectedSolution(sol);
    }
    /* removed 02/15/2005 aww
    static public void setPreferencesParent(GenericApplicationPrefs prefs) {
      myPrefs = prefs;
    }
    */
    public void setCommitButtonEnabled(boolean tf) {
        commitButtonEnabled = tf;
    }

    public void setWaveMagButtonEnabled(boolean tf) { enableWaveMagButton = tf; } // aww 05/2004

    public void setUpdateEnabled(boolean tf) { updateEnabled = tf; }
    public boolean isUpdateEnabled() { return updateEnabled; }
    public void setCommitEnabled(boolean tf) { commitEnabled = tf; }
    public boolean isCommitEnabled() { return commitEnabled; }
    public boolean isAutoSolve() { return autoSolve; }
    public void setAutoSolve(boolean tf) { autoSolve = tf; }

    protected abstract void setComponentUpdateState(boolean tf);
    protected abstract void setComponentCommitState(boolean tf);
    protected abstract void enableComponentCommitButton(boolean tf) ;
    protected abstract void commit();

    public void setPanelNameLabel(String text) {
            if (aNameLabel == null) {
               aNameLabel = new JLabel();
               aNameLabel.setHorizontalAlignment(JLabel.CENTER);
            }
            aNameLabel.setText(text);
    }
    protected JTextArea createTextArea() {
        JTextArea textArea = new JTextArea(80,132);
        textArea.setFont(new Font("Monospaced", Font.PLAIN, 12));
        textArea.setEditable(true);
        return textArea;
    }
    public TextLogger getTextLogger() {
        return aLog;
    }
    public void setTextLogger(TextLogger logger) {
        if (logger != null) aLog = logger;
    }

    public JTextArea getTextArea() {
        return aLog.getTextArea();
    }
    public void setTextArea(JTextArea textArea) {
        aLog.setTextArea(textArea);
    }
    protected Object getSolutionId() {
        return (aSol == null) ? null : aSol.getId(); 
    }
    protected Object getSolutionOriginId() {
        return (aSol == null) ? null : aSol.getPreferredOriginId();
    }
    protected Object getMagId() {
        return (aMag == null) ? null : aMag.getIdentifier(); 
    }

    public boolean hasSelectedSolution() { return (aSol != null); }
    protected Solution getSelectedSolution() {
        return aSol;
    }
    protected void setSelectedSolution(Solution sol) {
        if (sol == null)
           aLog.logTextnl("Warning associated solution is set null " + getClass().getName());
        aSol = sol;
        // check for waveform count in db to set enableWaveMagButton state?
        // enableWaveMagButtonState = (aSol.countWaveforms() > 0);
    }
    public boolean hasSelectedMagnitude() { return (aMag != null); }
    protected Magnitude getSelectedMagnitude() { return aMag; }
    protected void setSelectedMagnitude(Magnitude mag) {
        //if (mag == null) aLog.logTextnl("Warning associated input magnitude is null " + getClass().getName());
        if (mag != null && hasSelectedSolution()) {
            if ( ! mag.isAssignedToSol() || ! mag.isAssignedTo(aSol)) {
                aLog.logTextnl("Warning selected magnitude not associated with a selected solution, aborting." + getClass().getName());
                //mag.associate(aSol); // shouldn't associate loop association for readings in mag lists?
            }
        }
        aMag = mag;
    }

    protected abstract void loadData(); // depends on implemented subtype
    protected abstract boolean  hasModifiedTable(); // depends on implemented subtype

    public void initPanel() {
        if (DataSource.getConnection() == null)
          throw new NullPointerException("Need valid connection to DataSource");
        initProperties();
        initEngineDelegate();
        initPanelGraphics();
    }
    protected void initPanelGraphics() {
        this.setLayout(new BorderLayout());
        if (aNameLabel != null) this.add(aNameLabel, BorderLayout.NORTH);
        this.add(createEditorComponent(), BorderLayout.CENTER);
        /*
        addLowerControlsToPanel(this);
        */
        setComponentUpdateState(updateEnabled);
        setComponentCommitState(commitEnabled);
        enableComponentCommitButton(commitButtonEnabled); // for TN only commit from top level solution
    }
    public static void setProperties(HypoMagDelegatePropertyList propList) {
        editorProps = propList;
    }
    protected void initProperties() {
      if (editorProps == null) {
        editorProps = new HypoMagDelegatePropertyList();
        editorProps.setFiletype(EnvironmentInfo.getApplicationName().toLowerCase());
        // define an engine delegate to instantiate in property file
        //editorProps.setFilename( myPrefs == null) ? DEFAULT_PROP_FILE_NAME : myPrefs.getString("globalPropertyFileName", DEFAULT_PROP_FILE_NAME) );
        editorProps.setFilename(DEFAULT_PROP_FILE_NAME); // aww - removed above, replaced by prop 02/16/2005
        editorProps.reset();
        editorProps.setup();
      }
      debug = editorProps.getProperty("debug", "false").equalsIgnoreCase("true") || Debug.isEnabled();
      if (debug) {
        System.out.println("Editor panels properties file: "+editorProps.getUserPropertiesFileName());
        //editorProps.list(System.out);
      }
      if ( editorProps.getProperty("autoSolve") != null) // aww added property 6/28/2004 to toggle, default = true
        autoSolve = editorProps.getBoolean("autoSolve"); // default = true -aww

      initWhereEngine(); // added 02/03/2005 -aww

    }

    private void initWhereEngine() {
        String className = editorProps.getProperty("WhereIsEngine");
        whereEngine = (className == null) ? WhereIsEngine.create() : WhereIsEngine.create(className);
        // set Where units: km or miles
        String units = editorProps.getProperty("whereUnits");
        if (units != null) {
            units = units.toLowerCase();
            if ( units.startsWith("m") ) {
              whereEngine.setDistanceUnits(GeoidalUnits.MILES);
            } else if ( units.startsWith("k") ) {
              whereEngine.setDistanceUnits(GeoidalUnits.KILOMETERS);
            }
        }
    }

    protected JComponent createEditorComponent() {
        if (aLog.getTextArea() == null) aLog.setTextArea(createTextArea());
        return createSplitPaneForFrame(createTablePanel(), aLog.getTextArea());
    }

    protected JSplitPane createSplitPaneForFrame(JComponent top, JComponent bottom) {
        JScrollPane textScrollPane = new JScrollPane(bottom,
                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        JSplitPane jsp =
            new JSplitPane(JSplitPane.VERTICAL_SPLIT,
                           false,           // don't repaint until resized 
                           top,             // data component input + tab panels
                           textScrollPane); // bottom textArea component
        jsp.setOneTouchExpandable(true);
        jsp.setDividerSize(20);
        jsp.setResizeWeight(.50);
        jsp.setDividerLocation(PREFERRED_PANEL_HEIGHT);
        setSplitPaneBorders(jsp);
        this.jsp = jsp;
        return jsp;
    }
    protected void setSplitPaneBorders(JSplitPane jsp) { }
    protected void initSplitPaneBorders(JSplitPane jsp, Color aColor) {
        jsp.setBorder(BorderFactory.createMatteBorder(4,4,4,4, aColor));
        BasicSplitPaneDivider d = ((BasicSplitPaneUI) jsp.getUI()).getDivider();
        d.setBorder(BorderFactory.createMatteBorder(5,0,5,0, aColor));
        d.setBackground(aColor);
    }
    protected void scrollTextPane() {
      SwingUtilities.invokeLater(
        new Runnable() {
          final AbstractJasiSolutionEditorPanel ap = AbstractJasiSolutionEditorPanel.this;
          public void run() {
            ap.scrollTextPaneToMaximum();
          }
        }
      );
    }
    private void scrollTextPaneToMaximum() {
      JTextArea textArea = getTextArea();
      if (textArea != null) {
        Rectangle r = new Rectangle();
        textArea.computeVisibleRect(r);
        int incrPixels = 
          textArea.getScrollableUnitIncrement(r,SwingConstants.VERTICAL,1);
        int yDown1 = (incrPixels * (textArea.getLineCount() - 1)) - r.y;
        //int yDown2 = textArea.getPreferredSize().height - r.y;
        //System.out.println("visible r: "+r+" prefviewport: "+textArea.getPreferredScrollableViewportSize());
        //System.out.print(" incr: "+incrPixels+" rows: "+textArea.getRows()+" lines: "+textArea.getLineCount());
        //System.out.println(" yDown1: "+yDown1+" yDown2: "+yDown2);
        r.translate(0, yDown1);
        getTextArea().scrollRectToVisible(r);
      }
    }
    protected abstract JComponent createDataComponent();

    protected JPanel createTablePanel() {
        JPanel dataPanel = new JPanel();
        dataPanel.setPreferredSize(
                  new Dimension(PREFERRED_PANEL_WIDTH,PREFERRED_PANEL_HEIGHT));
        dataPanel.setLayout(new BorderLayout());
        if (upperControlsEnabled) {
          Component comp = createUpperControlComponent();
          if (comp != null) dataPanel.add(comp, BorderLayout.NORTH);
        }
        JComponent aComp = createDataComponent();
        aComp.setMinimumSize(new Dimension(300,200));
        dataPanel.add(aComp,BorderLayout.CENTER);
        return dataPanel;
    }
    //instead of the toolbar put ancillary control actions in menubar?
    protected Component createLowerControlComponent() {
      return createLowerControlBox();
    }
    protected Component createUpperControlComponent() {
      return createUpperToolBar();
    }
    protected void addLowerControlsToPanel(JPanel aPanel) {
      if (lowerControlsEnabled) {
        Component comp = createLowerControlComponent();
        if (comp != null) aPanel.add(comp, BorderLayout.SOUTH);
      }
    }
    /*
    protected JMenuBar createMenuBar() {
        JMenuBar menuBar = null;
        Action enginePropAction = createEnginePropertyAction();
        if (enginePropAction != null) {
          menuBar = new JMenuBar();
          menuBar.add(new JMenu(enginePropAction));
        }
        return menuBar;
    }
    */
    protected JToolBar createUpperToolBar() {
        Box box = createUpperControlBox();
        if (box != null) box.add(Box.createGlue());
        JToolBar toolBar = null;
        if (box != null) {
          toolBar = new JToolBar("Solution Editor Controls");
          // subpanel dialog blocks mouse input separate instance needed in each subpanel:
          toolBar.setFloatable(false);
          toolBar.add(box);
        }
        return toolBar;
    }
    /* Implement for configuration issues to allow users to set preferences dynamically
     * Should have panels dump last setting to preferences/props when dialog holding panel
     * closes window, then reload last saved settings next time it's newly created.
    private AbstractAction prefsAction;
    private AbstractAction createPreferencesAction() {
      AbstractAction prefsAction = new AbstractAction() {
              public void actionPerformed(ActionEvent e) {
              }
      };
      return prefsAction;
    }
    */
    protected Box createLowerControlBox() {
        Box box = Box.createHorizontalBox();
        box.add(Box.createGlue());
        box.add(createShrinkButton());
        box.add(createExpandButton());
        box.add(createClearButton());
        box.add(createSaveTextButton());
        box.add(createLoadButton());
        box.add(Box.createGlue());
        return box;
    }
    protected Box createUpperControlBox() {
        return createEngineControlBox();
    }
    protected JButton createEnginePropsButton() {
      Action enginePropAction = createEnginePropertyAction();
      JButton epb = null;
      if (enginePropAction != null) {
        epb = new JButton(enginePropAction);
        epb.setMargin(inset);
      }
      return epb;
    }
    protected Box createEngineControlBox() {
        Box box = null;
        if (delegate != null) {
          if (engineControlsEnabled) {
            box = Box.createHorizontalBox();
            box.add(Box.createGlue());
            JButton jb = createEnginePropsButton();
            if (jb != null) box.add(jb); // box.add(createEnginePropsButton());
            box.add(createSolveButton());
            box.add(createCalcAllMagsButton()); // move to JasiMagnitudeAssocEditorPanel class?
            box.add(createCalcMagButton());
            box.add(createCalcMagFromWaveformsButton());
            box.add(createTrialLocationButton());
            box.add(createFixLocationButton());
            box.add(createWhereEngineButton()); // might want this available for other altSol panel ?
          }
        }
        return box;
    }
    protected JButton createExpandButton() {
        JButton jbExpand = new JButton(MetalIconFactory.getInternalFrameMaximizeIcon(15)); //"Expand"
        jbExpand.setMargin(inset);
        jbExpand.setToolTipText("Expand panel size.");
        jbExpand.setActionCommand("Expand");
        jbExpand.setEnabled(true);
        jbExpand.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            //AbstractJasiSolutionEditorPanel.this.getTopLevelAncestor().setSize(getPreferredSize());
            AbstractJasiSolutionEditorPanel.this.getTopLevelAncestor().setSize(
              //jdk1.4 GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().getSize()
              1000,800 // kludge here -aww 02/24/2005
            );
            jsp.setDividerLocation(PREFERRED_PANEL_HEIGHT);
          }
        });
        return jbExpand;
    }
    protected JButton createShrinkButton() {
        JButton jbShrink = new JButton(MetalIconFactory.getInternalFrameMinimizeIcon(15)); //"Shrink"
        jbShrink.setMargin(inset);
        jbShrink.setToolTipText("Shrink panel size.");
        jbShrink.setActionCommand("Shrink");
        jbShrink.setEnabled(true);
        jbShrink.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            AbstractJasiSolutionEditorPanel.this.getTopLevelAncestor().setSize(getMinimumSize());
            jsp.setDividerLocation(-1); // reset so as to show only top panel buttons
          }
        });
        return jbShrink;
    }
    protected JButton createLoadButton() {
        JButton jbLoad = new JButton("Reload");
        jbLoad.setMargin(inset);
        jbLoad.setToolTipText("Reload panel data from database.");
        jbLoad.setActionCommand("Load");
        jbLoad.setEnabled(true);
        jbLoad.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            //if (JOptionPane.YES_OPTION == getDataListLoadOption()) loadData();
            loadData();
          }
        });
        return jbLoad;
    }
    protected JButton createClearButton() {
        JButton jbClear = new JButton("ClearText");
        jbClear.setMargin(inset);
        jbClear.setToolTipText("Erase all text text window area.");
        jbClear.setActionCommand("Clear");
        jbClear.setEnabled(true);
        jbClear.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                aLog.clear();
            }
        });
        return jbClear;
    }
    protected JButton createSaveTextButton() {
        JButton jbSaveText = new JButton("SaveText");
        jbSaveText.setMargin(inset);
        jbSaveText.setToolTipText("Save text in panel text area to file.");
        jbSaveText.setActionCommand("SaveText");
        jbSaveText.setEnabled(true);
        jbSaveText.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                StringBuffer sb = new StringBuffer(80);
                sb.append("jep");
                if (aSol != null && ! aSol.getId().isNull()) {
                  sb.append("_sol_");
                  sb.append(aSol.getId().toString());
                }
                else if (aMag != null && ! (aMag.getIdentifier() == null)) {
                  sb.append("_mag_");
                  sb.append(aMag.getIdentifier().toString());
                }
                sb.append(".log");
                aLog.saveToFile(sb.toString(), true);
            }
        });
        return jbSaveText;
    }

    // AWW start of toolbar dependent code for SolutionEditorPanel
    // perhaps need separate solve solution and mag buttons?
    protected JButton createSolveButton() {
        JButton jbSolve = new JButton("Locate");
        jbSolve.setMargin(inset);
        jbSolve.setToolTipText("Locate selected solution (no mag calc)");
        jbSolve.setActionCommand("Locate");
        jbSolve.setEnabled(hasLocationService());
        jbSolve.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            locate();
            if (aSol == null) {
              InfoDialog.informUser(getTopLevelAncestor(), "INFO",
                "Selected input solution is null!", null);
            }
            else {
              int minPhaseCount = getMinPhaseCount();
              if (minPhaseCount < 0) return;  // -1 => bail, no locationEngine defined
              PhaseList phList = aSol.getPhaseList();
              if (phList == null || phList.size() < minPhaseCount) {
                InfoDialog.informUser(getTopLevelAncestor(), "INFO",
                  "Solution phase count = " +phList.size()+
                  " < " +minPhaseCount+
                  " minimum; solution location failed!", null);
              }
            }
            return;
          }
        });
        return jbSolve;
    }
    protected JButton createCalcAllMagsButton() { // move to JasiMagnitudeAssocEditorPanel class?
        jbAllMags = new JButton("CalcAllMags");
        jbAllMags.setMargin(inset);
        jbAllMags.setToolTipText("Caculate all prefmag types known to delegate.");
        jbAllMags.setActionCommand("calcAllMags");
        jbAllMags.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            calcAllMags();
          }
        });
        // 02/10/2005 removed enableMagButton test condition for allMags case -aww
        jbAllMags.setEnabled((hasMagnitudeService() && hasLocationService()) && enableAllMagsCalc);
        jbAllMags.setVisible(enableAllMagsCalc);
        return jbAllMags;
    }
    protected JButton createCalcMagButton() {
        jbMag = new JButton("CalcMag");
        jbMag.setMargin(inset);
        jbMag.setToolTipText("Caculate new magnitude of type (row selected in MagList panel, else event preferred).");
        jbMag.setActionCommand("calcMag");
        jbMag.setEnabled((hasMagnitudeService() && hasLocationService()) && enableMagButton);
        jbMag.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            calcMag();
          }
        });
        return jbMag;
    }
    protected JButton createCalcMagFromWaveformsButton() {
        JButton jbWaveMag = new JButton("calcMagWaves");
        jbWaveMag.setMargin(inset);
        jbWaveMag.setToolTipText("Caculate new magnitude like the preferred type.");
        jbWaveMag.setActionCommand("calcMagWaves");
        // added check for enabling button - aww 05/2004
        jbWaveMag.setEnabled( enableWaveMagButton && hasMagnitudeService() && hasLocationService() );
        jbWaveMag.setVisible( enableWaveMagButton );
        jbWaveMag.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            //calcMagFromWaveforms(aSol); // aww 05/2004 see below
            //use panel associated mag type or preferred type if none
            Magnitude mag = (aMag == null) ? aSol.getPreferredMagnitude() : aMag;
            calcMagFromWaveforms(aSol, mag.getTypeString());
          }
        });
        return jbWaveMag;
    }
    protected JButton createFixLocationButton() {
        JButton jbFix = new JButton("FixLoc");
        jbFix.setMargin(inset);
        jbFix.setToolTipText("Fixed location settings.");
        jbFix.setActionCommand("fix");
        jbFix.setEnabled(hasLocationService());
        jbFix.addActionListener(new ActionListener() {
          JOptionPane jop = null;
          JDialog dialog = null;

          JCheckBox chkFixZ = new JCheckBox("FixZ");
          JCheckBox chkFixT = new JCheckBox("FixT");
          JCheckBox chkFixH = new JCheckBox("FixH");

          private void init() {
            JPanel fixPanel = new JPanel();
            fixPanel.add(chkFixZ);
            fixPanel.add(chkFixH);
            fixPanel.add(chkFixT);
            //
            JPanel inputPanel = new JPanel();
            inputPanel.setLayout(new BorderLayout());
            inputPanel.add(fixPanel, BorderLayout.CENTER);
            jop = new JOptionPane(
                  inputPanel,
                  JOptionPane.QUESTION_MESSAGE,
                  JOptionPane.YES_NO_OPTION,
                  null,
                  new Object [] {"OK","CLEAR"}, "OK"
                );
            dialog = jop.createDialog(AbstractJasiSolutionEditorPanel.this.getTopLevelAncestor(),
                            "Fix location values");
          }

          public void actionPerformed(ActionEvent e) {
             if (delegate == null) {
                InfoDialog.informUser(getTopLevelAncestor(), "INFO",
                "Location engine delegate not defined!", null);
                return;
             }
             Solution aSol = getSelectedSolution();
             if (aSol == null) return;
             if (jop == null) init();
             chkFixZ.setSelected(aSol.depthFixed.booleanValue());
             chkFixH.setSelected(aSol.locationFixed.booleanValue());
             chkFixT.setSelected(aSol.timeFixed.booleanValue());
             dialog.setVisible(true);
             String selection = (String) jop.getValue();
             if (selection == null) return;
             if (selection.equals("OK")) {
               aSol.depthFixed.setValue(chkFixZ.isSelected());
               aSol.locationFixed.setValue(chkFixH.isSelected());
               aSol.timeFixed.setValue(chkFixT.isSelected());
               aSol.setStale(false); // test to allow mag calc - aww 05/10/2005
             }
             else if (selection.equals("CLEAR")) {
               aSol.lat.setValue(0.);
               aSol.lon.setValue(0.);
               aSol.depth.setValue(0.);
               aSol.mdepth.setValue(0.);
               aSol.depthFixed.setValue(false);
               aSol.locationFixed.setValue(false);
               aSol.timeFixed.setValue(false);
               aSol.setStale(true); // test to allow loc,mag calc - aww 05/10/2005
             }
             firePropertyChange("fixLoc", null, Boolean.TRUE);
          }
        });
        return jbFix;
    }
    protected JButton createTrialLocationButton() {
        JButton jbTrial = new JButton("TrialLoc");
        jbTrial.setMargin(inset);
        jbTrial.setToolTipText("Trial location settings.");
        jbTrial.setActionCommand("trial");
        jbTrial.setEnabled(hasLocationService());
        jbTrial.addActionListener(new ActionListener() {
          JOptionPane jop = null;
          JDialog dialog = null;

          NumberTextField latField = null;
          NumberTextField lonField = null;
          NumberTextField zField = null;

          private void init() {
            DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance();
            latField = new NumberTextFieldFixed(12, df);
            lonField = new NumberTextFieldFixed(12, df);
            zField   = new NumberTextFieldFixed(12, df);
            latField.setValue(0.);
            lonField.setValue(0.);
            zField.setValue(0.);

            JPanel trialPanel = new JPanel();
            trialPanel.setLayout(new GridLayout(3,2));
            trialPanel.add(new JLabel("Trial Lat"));
            trialPanel.add(latField);
            trialPanel.add(new JLabel("Trial Lon"));
            trialPanel.add(lonField);
            trialPanel.add(new JLabel("Trial   Z"));
            trialPanel.add(zField);
            //
            JPanel inputPanel = new JPanel();
            inputPanel.setLayout(new BorderLayout());
            inputPanel.add(trialPanel, BorderLayout.CENTER);
            jop = new JOptionPane(
                  inputPanel,
                  JOptionPane.QUESTION_MESSAGE,
                  JOptionPane.YES_NO_OPTION,
                  null,
                  new Object [] {"OK","CLEAR"}, "CLEAR"
                );
            dialog = jop.createDialog(AbstractJasiSolutionEditorPanel.this.getTopLevelAncestor(),
                            "Select default trial location values");
          }

          public void actionPerformed(ActionEvent e) {
             if (delegate == null) {
                InfoDialog.informUser(getTopLevelAncestor(), "INFO",
                "Location engine delegate not defined!", null);
                return;
             }
             if (jop == null) init();
             dialog.setVisible(true);
             String selection = (String) jop.getValue();
             if (selection == null) return;
             if (selection.equals("OK")) {
               setTrialLocation(
                 latField.getDoubleValue(),
                 lonField.getDoubleValue(),
                 zField.getDoubleValue()
               );
             }
             else if (selection.equals("CLEAR")) {
               setTrialLocation(0.,0.,0.);
               latField.setValue(0.);
               lonField.setValue(0.);
               zField.setValue(0.);
             }
             firePropertyChange("trialLoc", null, Boolean.TRUE);
             System.out.println("Default trial lat, lon, z: " + getTrialLocation().toString());
          }
        });
        return jbTrial;
    }
    protected JButton createWhereEngineButton() {
        JButton jbWheres = new JButton("Where");
        jbWheres.setToolTipText("Report closest to selected Solution in table.");
        jbWheres.setActionCommand("Where");
        jbWheres.setMargin(inset);
        jbWheres.setEnabled(true);
        jbWheres.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                    Solution selectedSol = getSelectedSolution();
                    if (selectedSol == null) {
                        return;
                    }
                    if (! (DataSource.isNull() || DataSource.isClosed()) ) {
                        Connection conn = DataSource.getConnection();
                        whereEngine.setConnection(conn);
                        LatLonZ llz = selectedSol.getLatLonZ();
                        //note: depth (+) down, so elev up sign (-)
                        String whereString = (llz.hasLatLonZ()) ?
                                whereEngine.where(llz.getLat(), llz.getLon(),llz.getZ()) : "Solution has bogus location.";
                        aLog.logTextnl("Where : " + selectedSol.getId().longValue() + "\n" + whereString);
                        JOptionPane.showMessageDialog(getTopLevelAncestor(),
                                 whereString, "Where", JOptionPane.PLAIN_MESSAGE);
                    }
                    else {
                        JOptionPane.showMessageDialog(getTopLevelAncestor(),
                                 "DB Connection is null or closed.", "Where", JOptionPane.PLAIN_MESSAGE);
                        return;
                    }
            }
        });
        return jbWheres;
    }

    // AWW engine dependent code 
    protected void addEngineDelegate(EngineDelegateIF delegate) {
      if (delegate instanceof HypoMagEngineDelegateIF)
              this.delegate = (HypoMagEngineDelegateIF) delegate; 
      else {
        System.err.println("ERROR EngineDelegateIF type must be instanceof HypoMagEngineDelegateIF: " +
          ((delegate == null) ? "null" : delegate.getClass().getName()) );
      }
    }

    /*  HypoMagEngineDelegateIF delegate = getHypoMagEngineDelegate();
    protected HypoMagEngineDelegateIF getHypoMagEngineDelegate() {
       return this.delegate;
    }
    */

    protected void initEngineDelegate() {
      if (this.delegate != null) {
        delegate.addPropertyChangeListener(this);
        Debug.println("DEBUG initEngineDelegate AJSEP delegate: " + delegate.getClass().getName());
      }
    }

    // methods wrapper delegate action
    public MagnitudeEngineIF initMagEngineForType(String magType) {
      return (hasMagnitudeService()) ? delegate.initMagEngineForType(aSol, magType) : null;
    }
    public MagnitudeMethodIF initMagMethod(String magType) {
      return (hasMagnitudeService()) ? delegate.initMagMethod(aSol, magType) : null;
    }

    public MagnitudeEngineIF initMagEngineForType(Solution sol, String magType) {
      return (hasMagnitudeService()) ? delegate.initMagEngineForType(sol, magType) : null;
    }
    public MagnitudeMethodIF initMagMethod(Solution sol, String magType) {
      return (hasMagnitudeService()) ? delegate.initMagMethod(sol, magType) : null;
    }

    public boolean solve(Object data) {
      if (! (hasLocationService() && hasMagnitudeService())) return false;
      return delegate.solve(data);
    }
    public boolean solve(Magnitude mag) {
      if (! (hasLocationService() && hasMagnitudeService())) return false;
      return delegate.calcMag(mag, aSol);
    }
    public boolean solve(Solution sol) {
      if (! hasLocationService()) return false;
      return delegate.locate(sol);
    }
    public boolean solve(Solution sol, Magnitude mag) {
      if (! (hasLocationService() && hasMagnitudeService())) return false;
      return solve(sol,mag);
    }
    protected boolean calcMag() {
      if (! (hasLocationService() && hasMagnitudeService())) return false;
      return delegate.calcMag(aMag, aSol);
    }
    public boolean calcMag(Solution sol) {
        return calcMag(sol, sol.getPreferredMagnitude().getTypeString());
    }

    private boolean canCalcMag() {
      if (! (hasLocationService() && hasMagnitudeService())) {
        InfoDialog.informUser(getTopLevelAncestor(), "INFO", "No location or magnitude service, check your startup!", null);
        return false;
      }
      if (delegate == null) {
        InfoDialog.informUser(getTopLevelAncestor(), "INFO", "Location, Magnitude engine delegate null!", null);
        return false;
      }

      Solution selectedSolution = getSelectedSolution();
      if (selectedSolution == null) {
        InfoDialog.informUser(getTopLevelAncestor(), "INFO", "No Solution selected for panel!", null);
        return false;
      }

      if (selectedSolution.isStale() ) {
        InfoDialog.informUser(getTopLevelAncestor(), "INFO", "Selected solution is stale, relocate solution first!", null);
        if ( confirm("Selected solution is STALE, Click YES if you want to re-locate, else no magnitude calc.") ) {
          if (! locate(selectedSolution)) return false;
        }
      }
      return true;
    }

    // returns false if error, unable to calculate, or no data readings exist for any mag
    protected boolean calcAllMags() { // move to JasiMagnitudeAssocEditorPanel class?
      if (! canCalcMag()) return false; // bails if no service or location is stale

      // For invocation below:
      // loads list in JasiSolutionEditorPanel subclass if data not already loaded 
      // so a no-op method implementation is in JasiMagnitudeAssocEditorPanel subclass 
      Solution selectedSolution = getSelectedSolution();
      initSolutionMagList(selectedSolution);
      
      boolean status = false;

      /* Block below removed 02/09/2005 -aww
      // Note saved data - may be bogus - later edits may orphan it, so we go with duplicate reading clones.
      // KLUDGE:
      // Assume calcMag(sol,type) clones solution's reading list to allow each mag
      // to have "independent" data like "deleteFlag","weights","channelMagValue".
      // Commit here to preserve any edited solution readings so we won't get
      // "multiple" rows for same changed reading in list cloned for the
      // magnitude calculation when the alternate magList is subsequently committed. 
      boolean staleCommitOkB4 = Magnitude.staleCommitOk; 
      try { 
        System.out.println("SolutionEditorPanel committing Solution to save any altered readings... before magnitude loop.");
        Magnitude.staleCommitOk = false; // don't write to db any existing stale magnitudes, set as safety
        //selectedSolution.commit();  // preserves all changed data (includes magnitude commit). 
        status = selectedSolution.commitReadings();  // preserves event,origin,readings - no magnitude commit
      }
      catch (JasiCommitException ex) {
        // Punt here, an exception
        status = false; 
        System.err.println("calcAllMags exception selectedSolution.commitReadings() Check database or data!"); 
      }
      finally {
        Magnitude.staleCommitOk =  staleCommitOkB4; // reset static flag back to original value
      }
      if (!status) {
        InfoDialog.informUser(getTopLevelAncestor(), "INFO", 
            "calcAllMags failed with error; selected solution isStale(): "+selectedSolution.isStale(), null);
        return false;
      }
      // end of commit KLUDGE 
      */ // removed block on 02/09/2005 -aww

      // The prefmag identifier can be used only if it's not for same magType as any of the newMag calculated here.
      Magnitude prefMag =  selectedSolution.getPreferredMagnitude();

      //Use prefmag id only if valid and the preferred type is not one of those recalculated
      boolean setPrefMagById = false;
      Object prefMagId   = null;
      String prefMagType = null;
      if (prefMag != null) { // test in case no preferred mag was assigned by virgin data input -aww
        prefMagId = prefMag.getIdentifier();
        prefMagType = "m" + prefMag.getTypeSubString(); 
        setPrefMagById = ! StringSQL.valueOf(prefMagId).equals("NULL");
      }


      // Hard-coded, do h before l, since mlMagMethod "delete"s EHZ readings when not a cloned reading list.
      String [] magTypes = new String [] {"mh","ml","md"};
      // Delegate known magtypes are init'ed from an input property 
      java.util.List magTypeList = new ArrayList (Arrays.asList(editorProps.getMagMethodTypes()));
      for (int i = 0; i<magTypes.length; i++) {
          if (editorProps.getBoolean(magTypes[i].toLowerCase()+"Disable")) magTypeList.remove(magTypes[i]);
          else if (setPrefMagById && prefMagType.equals(magTypes[i])) setPrefMagById = false;
      }
      // doesn't re-use the old mag object only new mags with null ids are created
      for (int i = 0; i < magTypes.length; i++) {
        status = true;
        if (! magTypeList.contains(magTypes[i])) continue;

        System.out.println(Lines.DOT_TEXT_LINE);
        aLog.logTextnl("Calculating magnitudes for type: "+magTypes[i]+" index = "+i); 
        // resolveMagReadingList(...) in AbstractHypoMagEngineDelegate can be configured to use
        // all edited/inserted Solution list Readings associated with an input mag or load
        // or reading data from database store.
        // Here create new mag for this type which will use solution readings if any
        status &= delegate.calcMag(selectedSolution, magTypes[i]); // aww 07/14/2004
        if (!status) System.out.println("    Error calculating a magnitude for type: " + magTypes[i]);
        // override prior preferred upon success
        else prefMagType = magTypes[i]; // will be last array type successful aww 04/28/2005

        // KLUDGE NOTE:
        // a commit here in loop preserves channelMags, when not configured to clone solution's reading list.
        //selectedSolution.commit(); // commit reading's last channelMag associated magnitude values
      } // end of loop

      if (setPrefMagById) {
        System.out.println("calcAllMags setting EVENT preferred mag to be old preferred id:" + prefMagId);
        selectedSolution.setPreferredMagFromIdInList(prefMagId); // reset preferred magnitude to original instance id?
      }
      else {
        Magnitude mag = selectedSolution.getPrefMagOfType(prefMagType);
        if (mag != null && selectedSolution.getPreferredMagnitude() != mag) {
            System.out.println(
                "calcAllMags setting EVENT preferred mag to type: "+prefMagType+ " value: "+
                ( (mag == null) ? "NULL" : mag.toNeatString() )
            );
            selectedSolution.setPreferredMagnitude(mag);
        }
        //System.out.println("DEBUG calcAllMags selectedSolution needs Commit ? : " + selectedSolution.getNeedsCommit());
      }

      System.out.println(Lines.DOT_TEXT_LINE);
      aLog.logTextnl("Finished calculating all magnitudes for event id: "+selectedSolution.getId().longValue());
      System.out.println("CalcAllMags Summary : "+
                          selectedSolution.toNeatString()+"\n"+
                          selectedSolution.getMagList().toNeatString()
                        );
      System.out.println(Lines.DOT_TEXT_LINE);

      return status;
    }

    //used by calcAllMags():
    protected abstract void initSolutionMagList(Solution sol);

    public boolean calcMag(Solution sol, String magType) {
      if (! (hasLocationService() && hasMagnitudeService())) return false;
      return (delegate == null) ? false : delegate.calcMag(sol, magType);
    }
    public boolean calcMagFromWaveforms(Solution sol) {
        return calcMagFromWaveforms(sol, sol.getPreferredMagnitude().getTypeString());
    }
    public boolean calcMagFromWaveforms(Solution sol, String magType) {
      if (! (hasLocationService() && hasMagnitudeService())) return false;
      return (delegate == null) ? false : delegate.calcMagFromWaveforms(sol, magType);
    }
    protected boolean locate() {
      if (! hasLocationService()) return false;
      return (delegate == null) ? false : delegate.locate(aSol);
    }

    // EngineDelegateIF methods:
    public void reset() {
        if (delegate != null) delegate.reset();
    }
    public boolean success() {
        return (delegate == null) ? false : delegate.success();
    }
    public boolean calcMag(Magnitude mag, Solution sol) {
      if (! (hasLocationService() && hasMagnitudeService())) return false;
      return calcMag(mag, sol);
    }
    public boolean locate(Solution sol) {
      if (! hasLocationService()) return false;
      return locate(sol);
    }
    public Action createEnginePropertyAction() {
      return (delegate instanceof GraphicalHypoMagEngineDelegateIF) ?
         ((GraphicalHypoMagEngineDelegateIF)delegate).createEnginePropertyAction() : null;
    }
    public boolean hasLocationService() {
      return (delegate != null) ? delegate.hasLocationService() : false;
    }
    public boolean hasMagnitudeService() {
      return (delegate != null) ? delegate.hasMagnitudeService() : false;
    }
    public void setTrialLocation(double lat, double lon, double z) {
      if (delegate == null || delegate.getLocationEngine() == null) {
        InfoDialog.informUser(getTopLevelAncestor(), "INFO",
          "Can't set trial location, delegate LocationEngine is null!", null);
      }
      else delegate.getLocationEngine().setTrialLocation(lat,lon,z);
    }
    public LocationEngineIF getLocationEngine() {
      return (delegate == null) ? null : delegate.getLocationEngine();
    }
    public MagnitudeEngineIF getMagnitudeEngine() {
      return (delegate == null) ? null : delegate.getMagnitudeEngine();
    }
    public LatLonZ getTrialLocation() {
      //return (delegate == null) ? new LatLonZ(0.,0.,0.) : delegate.getTrialLocation();
      if (delegate == null || delegate.getLocationEngine() == null) {
        InfoDialog.informUser(getTopLevelAncestor(), "INFO",
          "Can't set trial location, delegate LocationEngine is null!", null);
        return null;
      }
      return delegate.getLocationEngine().getTrialLocation();
    }
    public int getMinPhaseCount() {
      if (delegate == null || delegate.getLocationEngine() == null) {
        InfoDialog.informUser(getTopLevelAncestor(), "INFO",
          "Can't get minumum phase count, delegate LocationEngine is null!", null);
        return -1; 
      }
      return delegate.getLocationEngine().getMinPhaseCount();
    }

    public String getStatusString() {
        return (delegate == null) ? "" : delegate.getStatusString();
    }
    public String getResultsString() {
        return (delegate == null) ? "" : delegate.getResultsString();
    }
    public GenericPropertyList getDelegateProperties() {
        return (delegate == null) ? null : delegate.getDelegateProperties();
    }
    public boolean setDelegateProperties(GenericPropertyList props) {
        return (delegate == null) ? false : delegate.setDelegateProperties(props);
    }
    public boolean setDelegateProperties(String fileName) {
        return (delegate == null) ? false : delegate.setDelegateProperties(fileName);
    }
    public boolean initializeEngineDelegate() {
      return (delegate == null) ? false : delegate.initializeEngineDelegate();
    }
    public void addEngine(String keyName, EngineIF engine) {
        if (delegate != null) delegate.addEngine(keyName, engine);
    }
    public boolean hasEngine(String type) {
        return (delegate == null) ? false : delegate.hasEngine(type);
    }
    public void setChannelList(ChannelList chanList) {
        if (delegate != null) delegate.setChannelList(chanList);
    }
    public boolean hasChannelList() {
        return (delegate != null) ? delegate.hasChannelList() : false;
    }
    public void loadChannelList(java.util.Date date) {   // input could be subclass DateTime too -aww 2008/02/08
        if (delegate != null) delegate.loadChannelList(date);
    }
    // end of EngineDelegateIF methods

    public int getDataListLoadOption() {
      return JOptionPane.showConfirmDialog(this,
             "Reload data lists from database",
             "Data List Load Resolution",
             JOptionPane.YES_NO_OPTION);
    }
    public boolean confirm(String msg) {
      int answer =
          JOptionPane.showConfirmDialog(this, msg,
              "Event Resolution", JOptionPane.YES_NO_OPTION,
              JOptionPane.QUESTION_MESSAGE);
      return (answer == JOptionPane.YES_OPTION);
    }

    public void propertyChange(PropertyChangeEvent e) {
        //Implement new event type (sol or mag solved) using EventListenerList?
        /*
        Debug.println(
          " DEBUG EDITOR property: " + getClass().getName() + "\n" +
          " propName:" + e.getPropertyName() +
          " oldValue:" + e.getOldValue() +
          " newValue:" + e.getNewValue()
        );
        */
        String propName = e.getPropertyName();
        if (propName.equals(EngineIF.SOLVED)) {
           //Object value = e.getNewValue();
           scrollTextPane();
           firePropertyChange(propName, e.getOldValue(),e.getNewValue());
        }
    }
    //DEBUG method for state reporting
    protected final void reportSummaryState() {
      String prefixMessage = getClass().getName();
      prefixMessage = "Summary state report for class: " + prefixMessage.substring(prefixMessage.lastIndexOf("."));
      System.out.println(prefixMessage);
      System.out.println(Solution.getSummaryStateString(aSol, aMag));
      System.out.println("  panel hasModifiedTable():" + hasModifiedTable());
      System.out.println("----- End of Report -----");
      scrollTextPane(); // advance the scroller down page?
    }
} // end of class

