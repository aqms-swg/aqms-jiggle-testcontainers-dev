package org.trinet.util.graphics.table;
import java.awt.*;
import java.util.*;
import javax.swing.*;

import org.trinet.jasi.*;
import org.trinet.util.*;

public abstract class AbstractSolutionAssociatedTextParser implements TableRowTextParserIF {
  protected static boolean isDefaultReversed = false;
  protected ActiveArrayList jrList;
  protected String parserName;
  protected String dialogTitle;
  protected String date;
  protected String oldDate; 
  protected int hr;
  protected int oldHr;
  protected int min;
  protected int oldMin;
  protected double sec;
  protected double oldSec;
  protected double datetime;
  protected boolean hasParseErrors = false;
  protected GenericPropertyList parserProps;
  protected String defaultSource      = EnvironmentInfo.getApplicationName();
  protected String defaultNetworkCode = EnvironmentInfo.getNetworkCode();
  protected String defaultAuth        = defaultNetworkCode;

  public AbstractSolutionAssociatedTextParser() {}

  public void setProperties(GenericPropertyList parserProps) {
      this.parserProps = parserProps;
      if (parserProps != null) {
        defaultSource = parserProps.getProperty("defaultDataSrc", defaultSource);
        defaultAuth = parserProps.getProperty("defaultDataAuth", defaultAuth);
        //System.out.println("default auth: "+defaultAuth+" source: "+defaultSource);
      }
  }
  public abstract void parseInputTableRows(String text);
  public abstract JasiCommitableIF createRow();
  // rather than setting flag throwing & catching
  // parse exception would be better implementation
  public boolean hasParseErrors() { return hasParseErrors; }

  public int parseCount() { return (jrList == null) ? 0 : jrList.size(); }
  public ArrayList parseTableRowList(String text) {
      parseInputTableRows(text);
      return jrList;
  }
  public void setFormatTipText(String title) {
    dialogTitle = title;
  }
  public String getFormatTipText() { return dialogTitle; }

  public boolean parseDateTime(String dateTime) {
    return parseDateTime(dateTime, isDefaultReversed); 
  }
  private boolean parseDateTime(String dateTime, boolean reversed) {
    return (reversed) ? parseDateTimeReversed(dateTime) :
          parseDateTimeForward(dateTime);
  }
//  date:hr:mm:ss  || :hr:mm:ss || ::mm:ss  || :::ss 
  private boolean parseDateTimeForward(String dateTime) {
    StringTokenizer dateToke = new StringTokenizer(dateTime, " :");
    int cnt = dateToke.countTokens(); 
    String token = null;
    try {
      switch (cnt) {
        case 4:
          if ((token = dateToke.nextToken()) != null) {
            oldDate = token;
          }
        case 3:
          if ((token = dateToke.nextToken()) != null) {
            oldHr = Integer.parseInt(token);
          }
        case 2:
          if ((token = dateToke.nextToken()) != null) {
            oldMin = Integer.parseInt(token);
          }
        case 1:
          if ((token = dateToke.nextToken()) != null) {
            oldSec = Double.parseDouble(token);
          }
          break;
        default:
           System.err.println("Error date time token count: " + cnt);
           return false;
        }
      }
      catch (Exception ex) {
        System.err.println(ex.toString());
        return false;
      }
      setDateTimeValues();
      return true;
    }
//  ss:mn:hr:date  || :ss:mn:hr || ss:mn  || ss 
    private boolean parseDateTimeReversed(String dateTime) {
      StringTokenizer dateToke = new StringTokenizer(dateTime, " :");
      int cnt = dateToke.countTokens(); 
      try {
        switch (cnt) {
          case 4:
            oldSec  = Double.parseDouble(dateToke.nextToken());
            oldMin  = Integer.parseInt(dateToke.nextToken());
            oldHr   = Integer.parseInt(dateToke.nextToken());
            oldDate = dateToke.nextToken();
            break;
          case 3:
            oldSec  = Double.parseDouble(dateToke.nextToken());
            oldMin  = Integer.parseInt(dateToke.nextToken());
            oldHr   = Integer.parseInt(dateToke.nextToken());
            break;
          case 2:
            oldSec  = Double.parseDouble(dateToke.nextToken());
            oldMin  = Integer.parseInt(dateToke.nextToken());
            break;
          case 1:
            oldSec  = Double.parseDouble(dateToke.nextToken());
            break;
          default:
            System.err.println("Error date time token count: " + cnt);
            return false;
        }
      }
      catch (Exception ex) {
        System.err.println(ex.toString());
        return false;
      }
      setDateTimeValues();
      return true;
  }
  protected void setDateTimeValues() {
      date = oldDate;
      hr   = oldHr;
      min  = oldMin;
      sec  = oldSec;

      String str = date+":"+hr+":"+min;
      datetime = LeapSeconds.stringToTrue(str, "yyyy-MM-dd:HH:mm"); // 2008/02/07 -aww
      datetime += sec;
  }
  public void setDefaultDateString(String dateString) {
    date = dateString;
    oldDate = dateString;
  }
}
