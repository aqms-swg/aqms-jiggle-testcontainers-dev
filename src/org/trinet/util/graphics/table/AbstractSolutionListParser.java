package org.trinet.util.graphics.table;
import java.awt.Component;
import java.io.*;
import java.text.*;
import java.util.*;
import javax.swing.*;
import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;
import org.trinet.util.*;

public abstract class AbstractSolutionListParser implements SolutionListParserIF {
  private Component graphicsOwner;
  private JFileChooser fileChooser;
  private String fileName;
  private String title = "Parse data from file";

  protected boolean atEOD = false;
  protected int lineCount = 0;
  protected TableRowTextParserIF solParser;
  protected TableRowTextParserIF magParser;
  protected TableRowTextParserIF phaseParser;
  protected TableRowTextParserIF ampParser;
  protected TableRowTextParserIF codaParser;

  protected GenericPropertyList parserProps;

  public AbstractSolutionListParser() { }

  public AbstractSolutionListParser(GenericPropertyList gpl) {
    setProperties(gpl);
  }

  abstract protected void initParser();
  abstract protected int getDataType(String s); // get data type amp,phase,sol etc.?
  abstract protected boolean isDataTypeDelimiter(String s);
  abstract protected String getDataTypeDelimiter(String s);
  abstract protected String removeDataTypeDelimiter(String s);
  abstract public Solution parseSolution(BufferedReader bfReader) throws IOException;

  static protected boolean isBlank(String s) {
    return (s == null || s.trim().length() == 0);
  }

  static protected String getSolutionMinuteString(Solution aSol) {
     return LeapSeconds.trueToString(aSol.getTime(), "yyyy-MM-dd:HH:mm:00.00"); // for UTC time 2008/02/07 -aww
  }

  public void setProperties(GenericPropertyList parserProps) {
    this.parserProps = parserProps;
  }

  public void setGraphicsOwner(Component aComponent) {
    graphicsOwner = aComponent;
  }
  public String getFileName() {
    return fileName;
  }
  public void setFileName(String fileName) {
    this.fileName = fileName;
  }
  protected BufferedReader getReader(String fileName) {
    BufferedReader buffReader = null;
    try {
      if (fileName == null) 
        throw new NullPointerException("null filename input parameter");
      buffReader = new BufferedReader(new FileReader(fileName));
      System.out.println(title + " " + fileName + " for: " +
                      getClass().getName());
      atEOD = false;
    }
    catch(IOException ex) {
      ex.printStackTrace();
    }
    catch(NullPointerException ex) {
      ex.printStackTrace();
    }
    return buffReader;
  }
  protected BufferedReader getReader() {
    if (graphicsOwner != null) fileName = setFileNameFromChooser();
    return (fileName == null) ? null : getReader(fileName);
  } 
  protected String setFileNameFromChooser() {
    if (fileChooser == null) fileChooser = new JFileChooser();
    fileChooser.setCurrentDirectory(new File(System.getProperty("user.dir")));
    fileChooser.setDialogTitle(title);
    if (fileName != null) {
      fileChooser.setSelectedFile(new File(fileName));
    }
    if (JFileChooser.APPROVE_OPTION == fileChooser.showOpenDialog(graphicsOwner)) {
      fileName = fileChooser.getCurrentDirectory().getAbsolutePath() +
        File.separator + fileChooser.getName(fileChooser.getSelectedFile());
    }
    else {
      System.out.println(title + " canceled for: " + getClass().getName());
      fileName = null;
    }
    return fileName;
  }

  public GenericSolutionList parseSolutions() {
    BufferedReader bfReader = null;
    GenericSolutionList aList = new GenericSolutionList();
    Solution aSol = null;
    lineCount = 0;
    atEOD = false;
    try {
      bfReader = getReader();
      if (bfReader == null) return aList;
      do {
       aSol = parseSolution(bfReader);
       if (aSol != null) aList.add(aSol);
      } while (aSol != null && ! atEOD);
    }
    catch (IOException ex) {
      ex.printStackTrace();
    }
    finally {
      try {
        if (bfReader != null) bfReader.close();
      }
      catch (IOException ex) {
        ex.printStackTrace();
      }
    }
    System.out.println("Solutions read from input: " + aList.size());
    return aList;
  }

  protected CodaList parseCodaList(BufferedReader bfReader, Solution aSol) throws IOException {
    return (CodaList) parseIntoList((CodaList)aSol.getCodaList(), bfReader, codaParser, CODA_TYPE);
  }

  protected AmpList parseAmpList(BufferedReader bfReader, Solution aSol) throws IOException {
    /*
    // REMOVED BLOCK - 02/10/2005 don't create a dummy preferred mag -aww
    Magnitude mag = aSol.getPreferredMagnitude();
    // Reuse same Ml? create new instance if last preferred not amp type?
    if (mag == null || ! mag.isAmpMag()) {
      mag = Magnitude.create();
      mag.subScript.setValue(MagTypeIdIF.ML);
      mag.setSource(aSol.getSource());
      mag.value.setValue(0.);
      mag.assign(aSol);  // associate new mag with sol to add parsed amps 
    }
    AmpList aList = parseAmpList(bfReader, mag);  // does add
     if (aList != null && aList.size() > 0) { aSol.setPreferredMagnitude(mag); } // move to parseMagList -aww
    return aList;
    */
    return (AmpList) parseIntoList((AmpList)aSol.getAmpList(), bfReader, ampParser, AMP_TYPE);
  }

  protected CodaList parseCodaList(BufferedReader bfReader, Magnitude aMag) throws IOException {
      if (! aMag.isCodaMag()) {
        System.err.println("parseCodaList Error: Input mag is a not an Coda magnitude");
        return CodaList.create();
      }
      Solution aSol = aMag.getAssociatedSolution();
      //if (aSol == null) aSol = Solution.create();
      codaParser.parseDateTime(getSolutionMinuteString(aSol));
      return (CodaList) parseIntoList((CodaList)aMag.getReadingList(), bfReader, codaParser, CODA_TYPE);
  }

  protected AmpList parseAmpList(BufferedReader bfReader, Magnitude aMag) throws IOException {
      if (! aMag.isAmpMag()) {
        System.err.println("parseAmpList Error: Input mag is a not an Amp magnitude");
        return AmpList.create();
      }
      Solution aSol = aMag.getAssociatedSolution();
      //if (aSol == null) aSol = Solution.create();
      ampParser.parseDateTime(getSolutionMinuteString(aSol));
      return (AmpList) parseIntoList((AmpList)aMag.getReadingList(), bfReader, ampParser, AMP_TYPE);
  }

  protected PhaseList parsePhaseList(BufferedReader bfReader, Solution aSol) 
    throws IOException {
      phaseParser.parseDateTime(getSolutionMinuteString(aSol));
      return (PhaseList) parseIntoList(aSol.getPhaseList(), bfReader, phaseParser, PHASE_TYPE);
  }
  protected MagList parseMagList(BufferedReader bfReader, Solution aSol) 
    throws IOException {
      MagList magList = (MagList) aSol.getMagList();  // clone();
      Magnitude mag = null;
      magParser.parseDateTime(getSolutionMinuteString(aSol));
      while (true) {
        mag = (Magnitude) parseRow(bfReader, magParser, MAG_TYPE);
        if (mag == null) break;
        if (mag.isAmpMag()) parseAmpList(bfReader, mag);
        else parseCodaList(bfReader, mag);

        // make last one parsed the event preferred
        // which should set it to preferred of type
        aSol.setPreferredMagnitude(mag);
        //System.out.println("DEBUG the mag PARSED: " + mag.toNeatString());
      } 
      //System.out.println("DEBUG prefmagmap: " + aSol.prefMagMapString());
      //magList.dump(); // test dump of list for debug
      return magList;
  }

  public Solution parseSolution() {
    Solution sol = null;
    try {
      BufferedReader bfReader = getReader();
      sol = (bfReader == null) ? null : parseSolution(bfReader);
    }
    catch(IOException ex) {
      ex.printStackTrace();
    }
    return sol;
  }

  protected ArrayList parseIntoList(ArrayList inputList,
    BufferedReader bfReader, TableRowTextParserIF parser, int type) throws IOException {
      Object aRow = null;
      do {
        aRow = parseRow(bfReader, parser, type);
        if (aRow != null) inputList.add(aRow);
      } while (aRow != null);
      //((AbstractCommitableList )inputList.dump(); // test dump of output list for debug
      return inputList;
  }
  protected Object parseRow(BufferedReader bfReader,
      TableRowTextParserIF parser, int type) throws IOException {
        bfReader.mark(132);  //read-ahead buffer of 132 max char to reset
        String aLine = bfReader.readLine();
        lineCount++;
        if (aLine == null) { // eof
           atEOD = true;
           return null;
        }
        // NOTE: isDataTypeDelimiter() could also set the data type in concrete instance
        // which the getDataType() implementation would return for comparison.
        if ( isBlank(aLine) || isDataTypeDelimiter(aLine) ) return null; // skip
        if (type != getDataType(aLine)) {
          bfReader.reset();
          lineCount--;
          return null;
        }
        //System.out.println("DEBUG parseIntoList : " + aLine);
        ArrayList aParsedList = parser.parseTableRowList(removeDataTypeDelimiter(aLine));
        if( parser.parseCount() < 1 || parser.hasParseErrors()) {
          if (parser.hasParseErrors()) System.err.println("parseIntoList parser error at line: " + lineCount);
          atEOD = true;
          return null;
        }
        return aParsedList.get(0);
  }
}
