package org.trinet.util.graphics.table;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.text.*;
import org.trinet.util.DateTime;

public class CalendarDateEditor extends AbstractCellEditor {
    private static final int CHARS_IN_DATE_STRING = 19;
    private static final int DEFAULT_FONT_SIZE = 12;
    private static final Font DEFAULT_FONT = new Font("Monospaced", Font.PLAIN, DEFAULT_FONT_SIZE);

    private DateFormat dateFormatter = DateFormat.getDateInstance();

    Font font = DEFAULT_FONT;
    int fontSize = DEFAULT_FONT_SIZE;

    JTextField component;

    public CalendarDateEditor() { 
	this(DEFAULT_FONT_SIZE, DEFAULT_FONT);
    }
  
    public CalendarDateEditor(int fontSize) { 
	this(fontSize, DEFAULT_FONT);
    }
  
    public CalendarDateEditor(Font font) { 
	this(DEFAULT_FONT_SIZE, font);
    }

    public CalendarDateEditor(int fontSize, Font font) { 
	super();
	if (fontSize > 0) this.fontSize = fontSize;
	if (font != null) this.font = font;

	component = new JTextField();
	component.setFont(this.font);
	component.setHorizontalAlignment(JTextField.LEFT);
	component.addActionListener(this);
    }
  
    public int getFontSize() {
	return fontSize;
    }

    public void setFontSize(int fontSize) {
	if (fontSize > 0) this.fontSize = fontSize;
	font = font.deriveFont((float) fontSize);
	component.setFont(font);
    }

    public boolean startCellEditing(EventObject anEvent) {
	if(anEvent == null) component.requestFocus();
	else if (anEvent instanceof MouseEvent) {
	    if (((MouseEvent)anEvent).getClickCount() < clickCountToStart)
	    return false;
	}
	return true;
    }

    public Component getComponent() {
	return component;
    }


    public void setCellEditorValue(Object value) {
	this.value = value;
	if (value == null) {
	    component.setText("");
	    return;
	}

	String text = null;
	if (value instanceof Calendar) {
	    text = dateFormatter.format(((GregorianCalendar) value).getTime());
	}
	else if (value instanceof java.util.Date) {
	    text = value.toString();
	    if (text.length() > CHARS_IN_DATE_STRING) text = text.substring(0,CHARS_IN_DATE_STRING);
	}
	else {
	    text = value.toString();
	}
	component.setText(text);
   }

    public Object getCellEditorValue() {
	String text = component.getText();
	if (text == null || text.length() == 0) return null;
//	Debug.println("value text:\"" + text + "\"" + " length: " + text.length());
	if (value instanceof Calendar) {
	    GregorianCalendar gregor = new GregorianCalendar();
	    gregor.setTime(java.sql.Date.valueOf(text));
	    return gregor;
	}
	else {
	    try {
		//return java.sql.Timestamp.valueOf(text); // removed 2008/02/07
                return new DateTime(text); // test here for UTC leap seconds -aww 2008/02/07
	    }
	    catch (Exception ex) {
		return null;
	    }
	}
    }


    public Component getTableCellEditorComponent( JTable table,Object value,
	boolean isSelected,int row,int column) {
	setCellEditorValue(value);
	return (Component) component;
    }
}
