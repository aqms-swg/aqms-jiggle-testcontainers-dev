package org.trinet.util.graphics.table;

import java.awt.Color;

public enum CatalogColor implements CatalogColorConst {
    /** color.catalog.etype.earthquake */
    COLOR_CATALOG_ETYPE_EARTHQUAKE("color.catalog.etype.earthquake", defaultColorEQ, "etype.earthquake"),
    /** color.catalog.etype.explosion */
    COLOR_CATALOG_ETYPE_EXPLOSION("color.catalog.etype.explosion", defaultColorExpl, "etype.explosion"),
    /** color.catalog.etype.longperiod */
    COLOR_CATALOG_ETYPE_LONGPERIOD("color.catalog.etype.longperiod", defaultColorLong, "etype.longperiod"),
    /** color.catalog.etype.nuclear */
    COLOR_CATALOG_ETYPE_NUCLEAR("color.catalog.etype.nuclear", defaultColorNuke, "etype.nuclear"),
    /** color.catalog.etype.nv-tremor */
    COLOR_CATALOG_ETYPE_NV_TREMOR("color.catalog.etype.nv-tremor", defaultColorNvTr, "etype.nv-tremor"),
    /** color.catalog.etype.quarry */
    COLOR_CATALOG_ETYPE_QUARRY("color.catalog.etype.quarry", defaultColorQry, "etype.quarry"),
    /** color.catalog.etype.sonic */
    COLOR_CATALOG_ETYPE_SONIC("color.catalog.etype.sonic", defaultColorSon, "etype.sonic"),
    /** color.catalog.etype.tremor */
    COLOR_CATALOG_ETYPE_TREMOR("color.catalog.etype.tremor", defaultColorVTr, "etype.tremor"),
    /** color.catalog.etype.trigger */
    COLOR_CATALOG_ETYPE_TRIGGER("color.catalog.etype.trigger", defaultColorTrg, "etype.trigger", "etype.subnet"),
    /** color.catalog.etype.unknown */
    COLOR_CATALOG_ETYPE_UNKNOWN("color.catalog.etype.unknown", defaultColorUnk, "etype.unknown"),
    /** color.catalog.etype.v-tremor */
    COLOR_CATALOG_ETYPE_V_TREMOR("color.catalog.etype.v-tremor", defaultColorVTr, "etype.v-tremor"),
    /** color.catalog.gtype.local */
    COLOR_CATALOG_GTYPE_LOCAL("color.catalog.gtype.local", defaultColorLoc, "gtype.local", "gtype.L"),
    /** color.catalog.gtype.regional */
    COLOR_CATALOG_GTYPE_REGIONAL("color.catalog.gtype.regional", defaultColorReg, "gtype.regional", "gtype.R"),
    /** color.catalog.gtype.teleseism */
    COLOR_CATALOG_GTYPE_TELESEISM("color.catalog.gtype.teleseism", defaultColorTel, "gtype.teleseism", "gtype.T"),
    /** color.catalog.state.A */
    COLOR_CATALOG_STATE_A("color.catalog.state.A", defaultColorA, "state.A"),
    /** color.catalog.state.C */
    COLOR_CATALOG_STATE_C("color.catalog.state.C", defaultColorC, "state.C"),
    /** color.catalog.state.F */
    COLOR_CATALOG_STATE_F("color.catalog.state.F", defaultColorF, "state.F"),
    /** color.catalog.state.H */
    COLOR_CATALOG_STATE_H("color.catalog.state.H", defaultColorH, "state.H"),
    /** color.catalog.state.I */
    COLOR_CATALOG_STATE_I("color.catalog.state.I", defaultColorI, "state.I"),
    /** color.catalog.subsrc.CUSP */
    COLOR_CATALOG_SUBSRC_CUSP("color.catalog.subsrc.CUSP", defaultColorCUSP, "subsrc.CUSP"),
    /** color.catalog.subsrc.Jiggle */
    COLOR_CATALOG_SUBSRC_JIGGLE("color.catalog.subsrc.Jiggle", defaultColorJiggle, "subsrc.Jiggle"),
    /** color.catalog.subsrc.mung */
    COLOR_CATALOG_SUBSRC_MUNG("color.catalog.subsrc.mung", defaultColorMung, "subsrc.mung"),
    /** color.catalog.subsrc.other */
    COLOR_CATALOG_SUBSRC_OTHER("color.catalog.subsrc.other", defaultColorOther, "subsrc.other"),
    /** color.catalog.subsrc.RT */
    COLOR_CATALOG_SUBSRC_RT("color.catalog.subsrc.RT", defaultColorRT, "subsrc.RT"),
    /** color.catalog.subsrc.sedas */
    COLOR_CATALOG_SUBSRC_SEDAS("color.catalog.subsrc.sedas", defaultColorSEDAS, "subsrc.sedas"),
    /** color.catalog.subsrc.unknown */
    COLOR_CATALOG_SUBSRC_UNKNOWN("color.catalog.subsrc.unknown", defaultColorUnk, "subsrc.unknown"),
    /** color.catalog.uniform */
    COLOR_CATALOG_UNIFORM("color.catalog.uniform", defaultColorBck, "uniform");

    private final Color defaultColor;
    private final String[] keys;
    private final String property;

    private CatalogColor(String property, Color defaultColor, String... keys) {
        if (keys == null || keys.length == 0) {
            keys = EMPTY_ARRAY;
        }
        this.property = property;
        this.defaultColor = defaultColor;
        this.keys = keys;
    }

    /**
     * Get the default color.
     * 
     * @return the default color.
     */
    public Color getDefaultColor() {
        return defaultColor;
    }

    /**
     * Get the color map keys.
     * 
     * @return the color map keys, not null.
     */
    public String[] getKeys() {
        return keys;
    }

    /**
     * Get the property.
     * 
     * @return the property.
     */
    public String getProperty() {
        return property;
    }
}
