package org.trinet.util.graphics.table;

import java.awt.Color;

public interface CatalogColorConst {
    Color defaultColorA = new Color(Integer.parseInt("fbbdfe", 16)); // pink, was ("8bffee") blue
    Color defaultColorBck = new Color(Integer.parseInt("ffffb2", 16)); // pale yellow
    Color defaultColorC = new Color(Integer.parseInt("fd0106", 16)); // red
    Color defaultColorCUSP = new Color(Integer.parseInt("a2acfc", 16)); // gray-blue
    Color defaultColorEQ = new Color(Integer.parseInt("abfbfe", 16)); // lt blue, was ("8bffee")
    Color defaultColorExpl = new Color(Integer.parseInt("d46ffc", 16)); // purple
    Color defaultColorF = new Color(Integer.parseInt("deffa8", 16)); // lt green, was ("89ff8a")
    Color defaultColorH = new Color(Integer.parseInt("ffffb2", 16)); // lt yellow, was ("899cfe") dk
    Color defaultColorHeadBck = new Color(Integer.parseInt("ffff00", 16)); // yellow
    Color defaultColorI = new Color(Integer.parseInt("ffe65f", 16)); // creamy orange,
    Color defaultColorJiggle = new Color(Integer.parseInt("abfbfe", 16)); // lt blue
    Color defaultColorLoc = new Color(Integer.parseInt("abfbfe", 16)); // lt blue, was ("8bffee")
    Color defaultColorLong = new Color(Integer.parseInt("a2acfc", 16)); // gray-blue
    Color defaultColorMung = new Color(Integer.parseInt("adcbfe", 16)); // dk blue
    Color defaultColorNuke = new Color(Integer.parseInt("fd0106", 16)); // red
    Color defaultColorNvTr = new Color(Integer.parseInt("dbbefe", 16)); // lt.purple
    Color defaultColorOther = new Color(Integer.parseInt("f0f0f0", 16)); // very lt gray
    Color defaultColorQry = new Color(Integer.parseInt("adcbfe", 16)); // blue, was ("899cfe") dk
    Color defaultColorReg = new Color(Integer.parseInt("dbc9fe", 16)); // lt purple, was ("ff9bfc")
    Color defaultColorRT = new Color(Integer.parseInt("ffffb2", 16)); // pale yellow
    Color defaultColorSEDAS = new Color(Integer.parseInt("dbc9fe", 16)); // purple
    Color defaultColorSon = new Color(Integer.parseInt("ccfdfc", 16)); // lt blue, was ("ffd0d0")
    Color defaultColorTel = new Color(Integer.parseInt("fbbdfe", 16)); // pink, was ("89ff8a") green
    Color defaultColorTrg = new Color(Integer.parseInt("ffffb2", 16)); // lt yellow, was ("ffccc6")
    Color defaultColorUnk = new Color(Integer.parseInt("f0f0f0", 16)); // very pale gray
    Color defaultColorVTr = new Color(Integer.parseInt("dbbefe", 16)); // lt.purple
    String[] EMPTY_ARRAY = new String[0];
    Color grayBck = new Color(Integer.parseInt("d9d9d9", 16)); // lt gray
}
