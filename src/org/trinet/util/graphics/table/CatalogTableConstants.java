package org.trinet.util.graphics.table;

import org.trinet.jasi.*;
import org.trinet.jdbc.datatypes.*;

/** Define constants used by a CatalogTableModel. These are the columns in a
 *  JTable that contains a SolutionList. */
public interface CatalogTableConstants {
// Constant values used to identify respective data members in lists, access methods, or models.
// !!! NOTE: SOME COLUMNS MAY BE HIDDEN FROM TABLE VIEW, TO BE ABLE TO VIEW THEM, CHANGE THE F to T VALUE in the showColumns array below !!!
    public static final int MAX_FIELDS         = 63;
    public static final int ID                 = 0;
    public static final int EXTERNALID         = 1;
    public static final int DATETIME           = 2;
    public static final int LAT                = 3;
    public static final int LON                = 4;
    public static final int DEPTH              = 5;
    public static final int HORIZDATUM         = 6;
    public static final int VERTDATUM          = 7;
    public static final int TYPE               = 8;
    public static final int METHOD             = 9;
    public static final int CRUSTMODEL         = 10;
    public static final int VELMODEL           = 11;
    public static final int AUTHORITY          = 12;
    public static final int SOURCE             = 13;
    public static final int GAP                = 14;
    public static final int DISTANCE           = 15;
    public static final int RMS                = 16;
    public static final int ERRORTIME          = 17;
    public static final int ERRORHORIZ         = 18;
    public static final int ERRORVERT          = 19;
    public static final int ERRORLAT           = 20;
    public static final int ERRORLON           = 21;
    public static final int TOTALREADINGS      = 22;
    public static final int USEDREADINGS       = 23;
    public static final int SREADINGS          = 24;
    public static final int FIRSTMOTIONS       = 25;
    public static final int QUALITY            = 26;
    public static final int VALIDFLAG          = 27;
    public static final int EVENTTYPE          = 28;
    public static final int PROCESSINGSTATE    = 29;
    public static final int DEPTHFIXED         = 30;
    public static final int LOCATIONFIXED      = 31;
    public static final int TIMEFIXED          = 32;
    public static final int WAVERECORDS        = 33;
    public static final int MAGNITUDE          = 34;
    public static final int PRIORITY           = 35; // added by aww 09/19/00 per DG request
    public static final int MAGTYPE            = 36; // added by aww 01/23/01 per DG request
    public static final int MAGALGO            = 37; // added by aww 2009/05/08
    public static final int COMMENT            = 38; // added by ddg 01/22/02
    public static final int BOGUSFLAG          = 39; // added by aww 01/05/06
    public static final int OWHO               = 40; // added by aww 02/24/06
    public static final int MWHO               = 41; // added by aww 02/24/06
    public static final int VERSION            = 42; // added by aww 04/15/08
    public static final int MAGOBS             = 43; // added by aww 08/19/10
    public static final int MAGSTA             = 44; // added by aww 08/19/10
    public static final int AUSED              = 45; // added by aww 10/28/10
    public static final int MD                 = 46; // added by aww 03/16/11
    public static final int ME                 = 47; // added by aww 03/16/11
    public static final int ML                 = 48; // added by aww 03/16/11
    public static final int MLR                = 49; // added by aww 03/16/11
    public static final int MW                 = 50; // added by aww 03/16/11
    public static final int MLMD               = 51; // added by aww 03/16/11
    public static final int MAGERR             = 52; // added by aww 03/24/11
    public static final int OAUTH              = 53;  // added by aww 09/21/11
    public static final int PREFM              = 54;  // added by aww 04/22/13
    public static final int LDDATE             = 55;  // added by aww 12/06/14
    public static final int ESOURCE            = 56;  // added by aww 03/11/15
    public static final int CMODNAME           = 57;  // added by aww 03/16/15
    public static final int CMODTYPE           = 58;  // added by aww 03/16/15
    public static final int MDEPTH             = 59;  // added by aww 03/16/15
    public static final int GTYPE              = 60;  // added by aww 03/16/15
    public static final int GZ                 = 61;  // added by aww 06/18/15
    public static final int ASSOCIATION        = 62;
    
    // note no ME magnitude element
// !!! NOTE: SOME COLUMNS MAY BE HIDDEN FROM TABLE VIEW, TO BE ABLE TO VIEW THEM, CHANGE THE false TO true VALUE in the showColumns array below !!!

// End of member identifier constants

    public static final Class [] columnClasses = {
        DataLong.class, DataString.class, DataDouble.class, DataDouble.class, DataDouble.class,
        DataDouble.class, DataString.class, DataString.class, DataString.class, DataString.class,
        DataString.class, DataString.class, DataString.class, DataString.class, DataDouble.class,
        DataDouble.class, DataDouble.class, DataDouble.class, DataDouble.class, DataDouble.class,
        DataDouble.class, DataDouble.class, DataLong.class, DataLong.class, DataLong.class,
        DataLong.class, DataDouble.class, DataLong.class, DataString.class, DataString.class,
        //String.class, String.class, String.class, DataLong.class, Magnitude.class,
        String.class, String.class, String.class, DataLong.class, DataDouble.class,
        DataDouble.class, String.class, String.class, DataString.class, DataLong.class,
        DataString.class, String.class, DataLong.class, DataLong.class, DataLong.class,
        DataLong.class, DataDouble.class, DataDouble.class, DataDouble.class, DataDouble.class,
        DataDouble.class, DataDouble.class, DataDouble.class, DataString.class, DataLong.class,
        DataString.class, DataString.class, DataString.class, DataString.class, DataDouble.class,
        DataString.class, DataDouble.class, String.class
    };

    public static final int [] columnFractionDigits = {
        0, 0, 2, 3, 3,
        2, 0, 0, 0, 0,
        0, 0, 0, 0, 1,
        2, 2, 2, 2, 2,
        3, 3, 0, 0, 0,
        0, 2, 0, 0, 0,
        0, 0, 0, 0, 2,
        1, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 2, 2, 2, 2,
        2, 2, 2, 0, 0,
        0, 0, 0, 0, 2,
        0, 2, 0
    };

    public static final String [] columnNames = {
//        ID, EXTERNALID, DATETIME, LAT, LON,
//        DEPTH, HORIZDATUM, VERTDATUM, TYPE, METHOD,
//        CRUSTMODEL, VELMODEL, AUTHORITY, SOURCE, GAP,
//        DISTANCE, RMS, ERRORTIME, ERRORHORIZ, ERRORVERT,
//        ERRORLAT, ERRORLON, TOTALREADINGS, USEDREADINGS, SREADINGS,
//        FIRSTMOTIONS, QUALITY, VALIDFLAG, EVENTTYPE, PROCESSINGSTATE,
//        DEPTHFIXED, LOCATIONFIXED, TIMEFIXED, WAVERECORDS, MAGNITUDE
//        PRIORITY,MAGTYPE,MAGALGO,COMMENT,BOGUSFLAG,
//        WHO refer to credit table alias
//        ORIGINWHO, MAGWHO, VERSION, MAGOBS, MAGSTA,
//        AUSED, MD, ME, ML, MLR,
//        MW, MLMD, MAGERR, OAUTH, PREFM,
//        LDDATE, ESOURCE, CMODNAME, CMODTYPE, MDEPTH,
//        GTYPE GZ
//  NOTE: no (Me) ME magnitude element
        "ID", "EXT_ID", "DATETIME", "LAT", "LON",
        "Z", "HDATUM", "VDATUM", "OT", "METHOD",
        "CM", "VM", "AUTH", "SRC", "GAP",
        "DIST", "RMS", "ERR_T", "ERR_H", "ERR_Z",
        "ERR_LAT", "ERR_LON", "OBS", "USED", "S",
        "FM", "Q", "V", "ETYPE", "ST",
        "ZF", "HF", "TF", "WRECS", "MAG",
        "PR", "MTYP", "MMETH","COMMENT","B",
        "OWHO","MWHO", "VER", "MOBS", "MSTA",
        "AUSE","Md","Me","Ml","Mlr",
        "Mw", "Ml-Md","MERR", "OAUTH", "PM",
        "LDDATE", "ESRC", "MOD", "DM", "MZ",
        "GT", "GZ", "ASSOCIATION"
    };

    public static final boolean [] columnAKey = {
        true, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false
    };

    public static final boolean [] columnEditable = {
        false, false, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, false, false,
        true, true, true, true, true,
        true, true, false
    };

    public static final boolean [] showColumn = {
//        ID, EXTERNALID, DATETIME, LAT, LON,
//        DEPTH, HORIZDATUM, VERTDATUM, TYPE, METHOD,
//        CRUSTMODEL, VELMODEL, AUTHORITY, SOURCE, GAP,
//        DISTANCE, RMS, ERRORTIME, ERRORHORIZ, ERRORVERT,
//        ERRORLAT, ERRORLON, TOTALREADINGS, USEDREADINGS, SREADINGS,
//        FIRSTMOTIONS, QUALITY, VALIDFLAG, EVENTTYPE, PROCESSINGSTATE,
//        DEPTHFIXED, LOCATIONFIXED, TIMEFIXED, WAVERECORDS, MAGNITUDE
//        DEPTHFIXED, LOCATIONFIXED, TIMEFIXED, WAVERECORDS, MAGNITUDE
//        PRIORITY,MAGTYPE,MAGALGO,COMMENT,BOGUSFLAG,
//        WHO refer to credit table alias
//        ORIGINWHO, MAGWHO, VERSION, MAGOBS, MAGSTA,
//        AUSED, MD, ML, MW,  MLMD, MAGERR, OAUTH, PREFM,
//        LDDATE, ESOURCE, CMODNAME, CMODTYPE, MDEPTH, GTYPE, ASSOCIATION
        true, true, true, true, true,
        true, false, false, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        false, false, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true
    };
}
