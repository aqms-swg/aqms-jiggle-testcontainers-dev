package org.trinet.util.graphics.table;
import java.util.*;
import javax.swing.event.*;
import org.trinet.jasi.*;
import org.trinet.jdbc.NullValueDb;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;

/** Model used to represent a list of jasi Solution objects (SolutionList)
 *  in a JTable. */

// DDG 1/22/01 added COMMENT field

public class CatalogTableModel extends javax.swing.table.AbstractTableModel
             implements CatalogTableConstants, ListDataStateListener {

    private boolean cellEditable = false;
    private SolutionList solutionList = null;
    private static final DataDouble NULL_DOUBLE = new DataDouble();

    public CatalogTableModel() {
    }

    public CatalogTableModel(SolutionList solutionList) {
        this(solutionList, false);
    }

    public CatalogTableModel(SolutionList solutionList, boolean cellEditable) {
        createList(solutionList);
        this.cellEditable = cellEditable;
    }

    public void setCellEditable(boolean cellEditable) {
        this.cellEditable = cellEditable;
    }

    public void createList(SolutionList solList) {
        if (solList == null) {
                System.err.println("CatalogTableModel createList(SolutionList) null input param");
                throw new NullPointerException("CatalogTableModel createList(SolutionList) param is null.");
        }
        if (solList.size() <= 0) return;
        /*
        this.solutionList = new SolutionList();
        ListIterator iter = solList.listIterator();
        // copy the solution list
        Solution sol;
        while (iter.hasNext()) {
            sol = (Solution) iter.next();
            if (sol.isDeleted()) continue;
            this.solutionList.add(sol);
        }
        solutionList.setSelected(solList.getSelected()); // retain selected sol
        */
        // Try alias instead -aww 09/12/2005
        if (solutionList != solList) {
          if (solutionList != null) solutionList.removeListDataStateListener(this);
          solutionList = solList; // XXX
          if (solutionList != null) solutionList.addListDataStateListener(this);
        }
    }

    public SolutionList getList() {
        return solutionList;
    }

    public void setList(SolutionList solutionList) {
        createList(solutionList);
        fireTableDataChanged(); // line below equivalent for jdk1.2.2 fireTableChanged(new TableModelEvent(this));
        return;
    }

// Extend AbstractTableModel class and override
    public int getColumnCount() {
        return MAX_FIELDS;
    }

    public Class getColumnClass(int colIndex) {
        return columnClasses[colIndex];
    }

    public String getColumnName(int colIndex) {
        return columnNames[colIndex];
    }

    public int getRowCount() {
        return solutionList.size();
    }

    public Solution getSolution(int rowIndex) {
        return (Solution) solutionList.get(rowIndex);
    }

    public int getIndexOf(Solution sol) {
        for (int ii = 0; ii < solutionList.size(); ii++) {
            if (((Solution) solutionList.get(ii)).idEquals(sol)) return ii;
        }
        return -1;
    }
    
/*
    protected Solution copyRow(Solution oldSol) {
        Solution newSol = Solution.create();

        newSol.id                = oldSol.id.clone();
        newSol.externalId        = oldSol.externalId.clone();
        newSol.datetime                = oldSol.datetime.clone();
        newSol.lat                = oldSol.lat.clone();
        newSol.lon                = oldSol.lon.clone();
        newSol.depth                = oldSol.depth.clone();
        newSol.horizDatum        = oldSol.horizDatum.clone();
        newSol.vertDatum        = oldSol.vertDatum.clone();
        newSol.type                = oldSol.type.clone();
        newSol.algorithm        = oldSol.algorithm.clone();
        newSol.crustModel        = oldSol.crustModel.clone();
        newSol.velModel                = oldSol.velModel.clone();
        newSol.eventAuthority        = oldSol.eventAuthority.clone();
        newSol.authority        = oldSol.authority.clone();
        newSol.source                = oldSol.source.clone();
        newSol.gap                = oldSol.gap.clone();
        newSol.distance                = oldSol.distance.clone();
        newSol.rms                = oldSol.rms.clone();
        newSol.errorTime        = oldSol.errorTime.clone();
        newSol.errorHoriz        = oldSol.errorHoriz.clone();
        newSol.errorVert        = oldSol.errorVert.clone();
        newSol.errorLat                = oldSol.errorLat.clone();
        newSol.errorLon                = oldSol.errorLon.clone();
        newSol.totalReadings        = oldSol.totalReadings.clone();
        newSol.usedReadings        = oldSol.usedReadings.clone();
        newSol.sReadings        = oldSol.sReadings.clone();
        newSol.firstMotions        = oldSol.firstMotions.clone();
        newSol.quality                = oldSol.quality.clone();
        newSol.validFlag        = oldSol.validFlag.clone();
        newSol.eventType        = oldSol.eventType.clone();
        newSol.processingState        = oldSol.processingState.clone();
        newSol.depthFixed        = oldSol.depthFixed.clone();
        newSol.locationFixed        = oldSol.locationFixed.clone();
        newSol.timeFixed        = oldSol.timeFixed.clone();
        newSol.waveRecords        = oldSol.waverecords.clone();
        newSol..magnitude.value        = oldSol.magnitude.value.clone();
        return newSol;
    }
*/
/* The values of the column indexes are defined in CatalogTableConstants. */
    public Object getValueAt(int rowIndex, int colIndex) {
        Solution sl = getSolution(rowIndex);
        switch (colIndex) {
           case ID:
                return sl.id;
           case EXTERNALID:
                return sl.externalId;
           case DATETIME:
                return sl.datetime;    //DataDouble
           case LAT:
                return sl.lat;        //DataDouble
           case LON:
                return sl.lon;        //DataDouble
           case DEPTH:
                return sl.depth;      //DataDouble
           case HORIZDATUM:
                return sl.horizDatum;
           case VERTDATUM:
                return sl.vertDatum;
           case TYPE:
                return sl.type;
           case METHOD:
                return sl.algorithm;
           case CRUSTMODEL:
                return sl.crustModel;
           case VELMODEL:
                return sl.velModel;
           case AUTHORITY:
                return sl.eventAuthority;
           case SOURCE:
                return sl.source;
           case ESOURCE:
                return sl.eventSource;
           case GAP:
                return sl.gap;
           case DISTANCE:
                return sl.distance;
           case RMS:
                return sl.rms;
           case ERRORTIME:
                return sl.errorTime;
           case ERRORHORIZ:
                return sl.errorHoriz;
           case ERRORVERT:
                return sl.errorVert;
           case ERRORLAT:
                return sl.errorLat;
           case ERRORLON:
                return sl.errorLon;
           case TOTALREADINGS:
                return sl.totalReadings;
           case USEDREADINGS:
                return sl.usedReadings;
           case SREADINGS:
                return sl.sReadings;
           case FIRSTMOTIONS:
                return sl.firstMotions;
           case QUALITY:
                return sl.quality;
           case VALIDFLAG:
                return sl.validFlag;
           case EVENTTYPE:
                return sl.eventType;
           case PROCESSINGSTATE:
                return sl.processingState;
           case DEPTHFIXED: // returns String
                return (sl.depthFixed.isNull()) ? "NULL" : Integer.toString(sl.depthFixed.intValue());
           case LOCATIONFIXED: // returns String
                return (sl.locationFixed.isNull()) ? "NULL" : Integer.toString(sl.locationFixed.intValue());
           case TIMEFIXED: // returns String
                return (sl.timeFixed.isNull()) ? "NULL" : Integer.toString(sl.timeFixed.intValue());
           case WAVERECORDS:
                //sl.countWaveforms(); // aww 02/17/2005 test
                return sl.waveRecords;
           case MAGNITUDE:
                Magnitude mag = sl.getPreferredMagnitude();
                return (mag == null) ? NULL_DOUBLE : mag.value;
           case MAGTYPE: // returns String
                mag = sl.getPreferredMagnitude();
                return (mag == null) ? "M " : mag.getTypeString();
           case MAGALGO: // returns String
                mag = sl.getPreferredMagnitude();
                return (mag == null || mag.algorithm.isNull()) ? "" : mag.algorithm.toString();
           case MAGOBS: // returns String
                mag = sl.getPreferredMagnitude();
                return (mag == null)  ? new DataLong(0l) : mag.usedChnls;
           case MAGSTA: // returns String
                mag = sl.getPreferredMagnitude();
                return (mag == null)  ? new DataLong(0l) : mag.usedStations;
           case MAGERR:
                mag = sl.getPreferredMagnitude();
                return (mag == null)  ? new DataLong(0l) : mag.error;
           case PRIORITY:
                return sl.priority;
           case COMMENT:
                return sl.comment;
           case BOGUSFLAG:
                return sl.dummyFlag;
           case OWHO:
                return sl.getWho();
           case MWHO: // returns String
                mag = sl.getPreferredMagnitude();
                return (mag == null) ? "---" : mag.getWho().toString();
           case VERSION:
                return new DataLong(sl.getSourceVersion());
           case AUSED:
                return sl.autoUsedReadings;
           case MD:
                mag = sl.getPrefMagOfType("d");
                return (mag == null) ? NULL_DOUBLE : mag.value;
           case ML:
                mag = sl.getPrefMagOfType("l");
                return (mag == null) ? NULL_DOUBLE : mag.value;
           case MLR:
                mag = sl.getPrefMagOfType("lr");
                return (mag == null) ? NULL_DOUBLE : mag.value;
           case MW:
                mag = sl.getPrefMagOfType("w");
                return (mag == null) ? NULL_DOUBLE : mag.value;
           case ME:
                mag = sl.getPrefMagOfType("e");
                return (mag == null) ? NULL_DOUBLE : mag.value;
           case MLMD:
                mag = sl.getPrefMagOfType("l");
                DataDouble mlVal = (mag == null) ? NULL_DOUBLE : mag.value;
                if (mlVal == NULL_DOUBLE) return mlVal;
                mag = sl.getPrefMagOfType("d");
                DataDouble mdVal = (mag == null) ? NULL_DOUBLE : mag.value;
                if (mdVal == NULL_DOUBLE) return mdVal;
                return new DataDouble(mlVal.doubleValue()-mdVal.doubleValue());
           case OAUTH:
                return sl.authority;
           case PREFM:
                return (sl.prefmec.isNull() ? Integer.valueOf(0) : Integer.valueOf(1));
           case LDDATE:
                //return sl.getTimeStamp();
                return new DataString(sl.getTimeStamp().toDateString().substring(0,19));
           case CMODNAME:
                return sl.crustModelName;
           case CMODTYPE:
                return sl.crustModelType;
           case MDEPTH:
                return sl.mdepth;
           case GTYPE:
                return new DataString(GTypeMap.toDbType(sl.getOriginGTypeString()).toUpperCase());
           case GZ:
                return sl.depth;
           case ASSOCIATION:
                return sl.getAssociation();
           default:
                return null;
        }
    }

    public void setValueAt(Object value, int rowIndex, int colIndex) {
        Solution sl = getSolution(rowIndex);
        switch (colIndex) {
           case ID:
                sl.id.setValue(value);
                break;
           case EXTERNALID:
                sl.externalId.setValue(value);
                break;
           case DATETIME:
                sl.datetime.setValue(value);
                break;
           case LAT:
                sl.lat.setValue(value);
                break;
           case LON:
                sl.lon.setValue(value);
                break;
           case DEPTH:
                sl.depth.setValue(value);
                break;
           case HORIZDATUM:
                sl.horizDatum.setValue(value);
                break;
           case VERTDATUM:
                sl.vertDatum.setValue(value);
                break;
           case TYPE:
                sl.type.setValue(value);
                break;
           case METHOD:
                sl.algorithm.setValue(value);
                break;
           case CRUSTMODEL:
                sl.crustModel.setValue(value);
                break;
           case VELMODEL:
                sl.velModel.setValue(value);
                break;
           case AUTHORITY:
                sl.eventAuthority.setValue(value);
                break;
           case SOURCE:
                sl.source.setValue(value);
                break;
           case ESOURCE:
                sl.eventSource.setValue(value);
                break;
           case GAP:
                sl.gap.setValue(value);
                break;
           case DISTANCE:
                sl.distance.setValue(value);
                break;
           case RMS:
                sl.rms.setValue(value);
                break;
           case ERRORTIME:
                sl.errorTime.setValue(value);
                break;
           case ERRORHORIZ:
                sl.errorHoriz.setValue(value);
                break;
           case ERRORVERT:
                sl.errorVert.setValue(value);
                break;
           case ERRORLAT:
                sl.errorLat.setValue(value);
                break;
           case ERRORLON:
                sl.errorLon.setValue(value);
                break;
           case TOTALREADINGS:
                sl.totalReadings.setValue(value);
                break;
           case USEDREADINGS:
                sl.usedReadings.setValue(value);
                break;
           case SREADINGS:
                sl.sReadings.setValue(value);
                break;
           case FIRSTMOTIONS:
                sl.firstMotions.setValue(value);
                break;
           case QUALITY:
                sl.quality.setValue(value);
                break;
           case VALIDFLAG:
                sl.validFlag.setValue(value);
                break;
           case EVENTTYPE:
                sl.eventType.setValue(value);
                break;
           case PROCESSINGSTATE:
                sl.processingState.setValue(value);
                break;
           case DEPTHFIXED:
                sl.depthFixed.setValue(value);
                break;
           case LOCATIONFIXED:
                sl.locationFixed.setValue(value);
                break;
           case TIMEFIXED:
                sl.timeFixed.setValue(value);
                break;
           case WAVERECORDS:
                sl.waveRecords.setValue(value);
                break;
           case MAGNITUDE:
                Magnitude mag = sl.getPreferredMagnitude();
                if (mag != null) mag.value.setValue(value);
                break;
           case MAGTYPE:
                mag = sl.getPreferredMagnitude();
                if (mag != null) mag.subScript.setValue(value);
                break;
           case MAGALGO:
                mag = sl.getPreferredMagnitude();
                if (mag != null) {
                  String str =  value.toString();
                  mag.algorithm.setValue(NullValueDb.isEmpty(str) ? "" : str);
                }
                break;
           case MAGSTA:
                mag = sl.getPreferredMagnitude();
                if (mag != null) mag.usedStations.setValue(value); 
                break;
           case MAGOBS:
                mag = sl.getPreferredMagnitude();
                if (mag != null) mag.usedChnls.setValue(value); 
                break;
           case MAGERR:
                mag = sl.getPreferredMagnitude();
                if (mag != null) mag.error.setValue(value); 
                break;
           case PRIORITY:
                sl.priority.setValue(value);
                break;
           case COMMENT:
                sl.setComment(value.toString());
                break;
           case BOGUSFLAG:
                sl.dummyFlag.setValue(value);
                break;
           case OWHO:
                sl.setWho(value.toString());
                break;
           case MWHO:
                sl.magnitude.setWho(value.toString());
                break;
           case VERSION:
                sl.setSourceVersion(Long.parseLong(value.toString()));
                break;
           case AUSED:
                sl.autoUsedReadings.setValue(value);
                break;
           case OAUTH:
                sl.authority.setValue(value);
                break;
           case PREFM:
                sl.prefmec.setValue(value);
                break;
           case LDDATE:
                break;
           case CMODNAME:
                sl.crustModelName.setValue(value);
                break;
           case CMODTYPE:
                sl.crustModelType.setValue(value);
                break;
           case MDEPTH:
                sl.mdepth.setValue(value);
                break;
           case GTYPE:
                sl.gtype.setValue(GTypeMap.fromDbType(((String)value).toLowerCase()));
                break;
           case GZ:
                sl.depth.setValue(value);
           case ASSOCIATION:
                sl.setAssociation((String)value);                
        }
        fireTableCellUpdated(rowIndex, colIndex);
    }

    public boolean isCellEditable(int rowIndex, int colIndex) {
        if (! cellEditable) {
            return false;
        } else if (solutionList.getSolution(rowIndex).isDeleted()) {
            return false;
        } else {
            return columnEditable[colIndex];
        }
    }

    public int findColumn(String name) {
        String test = name.trim().toUpperCase();
        for (int index = 0; index < MAX_FIELDS; index++) {
            if (test.equals(getColumnName(index))) return index;
        }
        return -1;
    }

// end of AbstractTableModel methods

    public boolean isKeyColumn(int index) {
        return columnAKey[index];
    }

    public boolean isKeyColumn(String name) {
        return columnAKey[findColumn(name)];
    }

    public int getColumnFractionDigits(int index) {
        return columnFractionDigits[index];
    }

    public boolean isColumnShown(int index) {
        return showColumn[index];
    }

    public boolean isColumnHidden(int index) {
        return ! showColumn[index];
    }

// Methods to modify table rows;
    public boolean deleteRow(int rowIndex) {
        boolean retVal = ((Solution) solutionList.get(rowIndex)).delete();
//        fireTableCellUpdated(rowIndex, 0);
        fireTableCellUpdated(rowIndex, findColumn("id"));
//        fireTableRowsDeleted(rowIndex, rowIndex);  // Virtual table delete not actually deleted from view?
        return retVal;
    };

    public boolean eraseRow(int rowIndex) {
        boolean retVal = solutionList.remove(solutionList.get(rowIndex));
        fireTableRowsDeleted(rowIndex, rowIndex);
        return retVal;
    };

    public boolean insertRow(int rowIndex, Solution sol) {
       solutionList.add(rowIndex, sol);
       fireTableRowsInserted(rowIndex, rowIndex);
       return true;
    };

    public int updateDB() {
        int count = solutionList.size();
        boolean status = true;
        int trueCount = 0;
        int falseCount = 0;
     try {
          for (int index = 0; index < count; index ++ ) {
             status = ((Solution) solutionList.get(index)).commit();
             if (status) trueCount++;
             else falseCount++;
          }
     } catch (JasiCommitException ex) {}
        Debug.println("CatalogTableModel.updateDB() trueCount:" + trueCount +  " falseCount: " + falseCount);
        return trueCount;
    }
//
// IF ListDataStateListener
//
//TODO: explore the StateChange object sent along in event for more info on objects updated
//
  public void intervalAdded(ListDataStateEvent e) {
    //System.out.println("Model - fire interval added in : " +
    //      e.getSource().getClass().getName() + " " + e.getIndex0() + " " + e.getIndex1());
    if (e.getIndex0() >= 0 && e.getIndex1() >= 0) {
      fireTableRowsInserted(e.getIndex0(),e.getIndex1());
    }
    else fireTableDataChanged();
  }
  public void intervalRemoved(ListDataStateEvent e) {
    //System.out.println("Model - fire interval removed in : " +
    //      e.getSource().getClass().getName() + " " + e.getIndex0() + " " + e.getIndex1());
    if (e.getIndex0() >= 0 && e.getIndex1() >= 0) {
      fireTableRowsDeleted(e.getIndex0(),e.getIndex1());
    }
    else fireTableDataChanged();
  }
  public void contentsChanged(ListDataStateEvent e) {
    //System.out.println("Model - fire contents updated in : " +
    //      e.getSource().getClass().getName() + " " + e.getIndex0() + " " + e.getIndex1());
    if (e.getIndex0() >= 0 && e.getIndex1() >= 0) {
      fireTableRowsUpdated(e.getIndex0(),e.getIndex1());
    }
    else fireTableDataChanged();
  }
  public void stateChanged(ListDataStateEvent e) {
    //System.out.println("Model - fire state updated in : " +
    //      e.getSource().getClass().getName() + " " + e.getIndex0() + " " + e.getIndex1());
    if (! e.getStateChange().getStateName().equals("selected") ) {
        if (e.getIndex0() >= 0 && e.getIndex1() >= 0) {
          fireTableRowsUpdated(e.getIndex0(),e.getIndex1());
        }
        else fireTableDataChanged();
    }
  }

  public void orderChanged(ListDataStateEvent e) {
    //System.out.println("Model - order changed model list updated in : " +
    //      e.getSource().getClass().getName() + " " + e.getIndex0() + " " + e.getIndex1());
    fireTableChanged(new JasiListTableModelEvent(this, true));
  }

}

