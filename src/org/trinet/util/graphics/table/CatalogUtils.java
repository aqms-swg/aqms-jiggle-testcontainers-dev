package org.trinet.util.graphics.table;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

import org.trinet.util.GenericPropertyList;

public class CatalogUtils implements CatalogColorConst {
    /**
     * Create the color map.
     * 
     * @return the color map.
     */
    public static Map<String, Color> createColorMap() {
        int numKeys = 0;
        String[] keys = null;
        for (CatalogColor cc : CatalogColor.values()) {
            keys = cc.getKeys();
            numKeys += keys.length;
        }
        // minimum size of 32
        numKeys = Math.max(numKeys, 32);
        return new HashMap<>(numKeys);
    }

    /**
     * Create the colors and set them to the default values.
     * 
     * @return the colors.
     */
    public static Color[] createColors() {
        Color[] catColors = new Color[CatalogColor.values().length];
        initColors(catColors);
        return catColors;
    }

    /**
     * Get the color.
     * 
     * @param catColors the colors.
     * @param cc        the catalog color.
     * @return the color.
     */
    public static Color getColor(Color[] catColors, CatalogColor cc) {
        return catColors[cc.ordinal()];
    }

    /**
     * Get the color.
     * 
     * @param colorMap the color map.
     * @param cc       the catalog color.
     * @return the color.
     */
    public static Color getColor(Map<String, Color> colorMap, CatalogColor cc) {
        Color c = null;
        final String[] keys = cc.getKeys();
        for (String key : keys) {
            c = colorMap.get(key);
            if (c != null) {
                break;
            }
        }
        return c;
    }

    /**
     * Get the background color.
     * 
     * @param catColors the colors.
     * @return the background color.
     */
    public static Color getColorBck(Color[] catColors) {
        return getColor(catColors, CatalogColor.COLOR_CATALOG_UNIFORM);
    }

    /**
     * Initialize the colors to the default values.
     * 
     * @param catColors the colors.
     */
    public static void initColors(Color[] catColors) {
        for (CatalogColor cc : CatalogColor.values()) {
            catColors[cc.ordinal()] = cc.getDefaultColor();
        }
    }

    /**
     * Initialize the properties.
     * 
     * @param props the properties.
     */
    public static void initProperties(GenericPropertyList props) {
        String property;
        Color c;
        for (CatalogColor cc : CatalogColor.values()) {
            property = cc.getProperty();
            c = props.getColor(property);
            if (c == null) {
                c = cc.getDefaultColor();
                props.setProperty(property, c);
            }
        }
    }

    /**
     * Set the color in the color map.
     * 
     * @param colorMap the color map.
     * @param key      the key.
     * @param c        the color o null if none.
     */
    public static void setColorMap(Map<String, Color> colorMap, String key, Color c) {
        if (c != null) {
            colorMap.put(key.intern(), c);
        }
    }

    /**
     * Set up the color map.
     * 
     * @param colorMap  the color map.
     * @param catColors the catalog colors.
     * @param props     the properties.
     */
    public static void setupColorMap(Map<String, Color> colorMap, Color[] catColors, GenericPropertyList props) {
        Color c = null;
        String[] keys = null;
        for (CatalogColor cc : CatalogColor.values()) {
            c = props.getColor(cc.getProperty());
            if (c == null) {
                c = cc.getDefaultColor();
            }
            keys = cc.getKeys();
            for (String key : keys) {
                setColorMap(colorMap, key, c);
            }
        }
    }

    /**
     * Setup the colors.
     * 
     * @param catColors the colors or null if none.
     * @param props     the properties or null if none.
     */
    public static void setupColors(Color[] catColors, GenericPropertyList props) {
        if (catColors == null && props == null) {
            return; // nothing to do
        }
        if (catColors == null) {
            initProperties(props);
        } else if (props == null) {
            initColors(catColors);
        } else {
            String property;
            Color c;
            for (CatalogColor cc : CatalogColor.values()) {
                property = cc.getProperty();
                c = props.getColor(property);
                if (c != null) {
                    catColors[cc.ordinal()] = c;
                } else {
                    props.setProperty(property, catColors[cc.ordinal()]);
                }
            }
        }
    }
}
