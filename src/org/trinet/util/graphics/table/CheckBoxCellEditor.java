package org.trinet.util.graphics.table;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class CheckBoxCellEditor extends AbstractCellEditor {
  JCheckBox component;

  public CheckBoxCellEditor(Object value) { 
    super();
    component = new JCheckBox();
    component.setHorizontalAlignment(SwingConstants.CENTER);
    component.setSelected(Boolean.valueOf(value.toString()).booleanValue());
    component.addItemListener(this);
    //component.addActionListener(this);
  }
  
// Methods to implement DefaultCellEditor 
  public boolean startCellEditing(EventObject anEvent) {
    if(anEvent == null) component.requestFocus();
    else if (anEvent instanceof MouseEvent) {
      if (((MouseEvent)anEvent).getClickCount() < clickCountToStart)
        return false;
    }
    return true;
  }
  public Component getComponent() {
    return this.component;
  }

  public Object getCellEditorValue() {
    return component.isSelected() ? Boolean.TRUE : Boolean.FALSE;
  }

  public void setCellEditorValue(Object value) {
    component.setSelected(Boolean.valueOf(value.toString()).booleanValue());
  }

  public Component getTableCellEditorComponent( JTable table, Object value,
        boolean isSelected,int row,int column) {
      setCellEditorValue(value);
      return component;
  }
}
