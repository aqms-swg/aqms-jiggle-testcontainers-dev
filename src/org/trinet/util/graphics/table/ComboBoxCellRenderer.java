package org.trinet.util.graphics.table;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;

/** Renderer to color table cells of deleted Solutions
*/
public class ComboBoxCellRenderer extends JComboBox implements TableCellRenderer, ColorableRendererIF {

    protected int fontSize=12;
    private String colName;
    protected Color defaultBackground = getBackground();
    protected Color defaultForeground = getForeground();
    private Border focusBorder = BorderFactory.createLineBorder(Color.black,2);
    private Border noFocusBorder = BorderFactory.createEmptyBorder(1,1,1,1);
    
    public ComboBoxCellRenderer (String colName, Object [] values) {
        this(colName, values, null);
    }
    public ComboBoxCellRenderer (String colName, Object [] values, Font font) {
        super(values);
        if (font == null)
                setFont(new Font("Monospaced", Font.PLAIN, this.fontSize));
        else {
          setFont(font);
          this.fontSize = font.getSize();
        }
        this.colName = colName;
    }

    public int getFontSize() {
      return getFont().getSize();
    }
    public void setFontSize(int fontSize) {
      if (fontSize > 0 && this.fontSize != fontSize) {
        this.fontSize = fontSize;
        setFont(getFont().deriveFont((float) fontSize));
      }
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
                 int row, int col) {
        if (table.getColumnName(col).equalsIgnoreCase(colName)) {
          if (hasFocus) {
             setBorder(focusBorder);
          }
          else setBorder(noFocusBorder);
          if (isSelected) {
             setBackground(table.getSelectionBackground());
             setForeground(table.getSelectionForeground());
          }
          else  {
             setForeground(table.getForeground());
             setBackground(defaultBackground);
          }
          setSelectedItem(value.toString());
          return this;
        }
        else {
          System.out.println("ComboBoxCell renderer return NULL: mismatch of table column name");
          return null;
        //table.getDefaultRenderer(value.getClass()).getTableCellRendererComponent(table,value,isSelected,hasFocus,row,col);
        }
    }

    public void setDefaultForeground(Color c) {
        defaultForeground = c;
        super.setForeground(c);
    }
    public void setDefaultBackground(Color c) {
        defaultBackground = c;
        super.setBackground(c);
    }
}
