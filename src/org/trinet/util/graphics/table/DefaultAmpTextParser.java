package org.trinet.util.graphics.table;
import java.io.*;
import java.util.*;
import javax.swing.*;
import org.trinet.jasi.*;
import org.trinet.util.*;
import org.trinet.util.graphics.*;

public class DefaultAmpTextParser extends AbstractJasiReadingTextParser {

  protected double amplitude;

  public DefaultAmpTextParser() {
    super();
    parserName = "Amp";
    dialogTitle = "<sta> <chan> <value(cm)> [yyyy-MM-dd:HH:mm:ss.s] [type(S)]";
    defaultPhase = "S";
    phase = defaultPhase;
  }

  public void parseInputTableRows(String text) {
    hasParseErrors = false;
    if (jrList == null) jrList = AmpList.create();
    else jrList.clear();
    if (text == null || text.length() == 0) return;
    BufferedReader buffReader = null;
    try {
      buffReader = new BufferedReader(new StringReader(text));
      String aLine = null;
      while ((aLine = buffReader.readLine()) != null) {
        if (aLine.trim().equals("")) continue;
        StringTokenizer strToke = new StringTokenizer(aLine);
        if (strToke.countTokens() < 3) {
          System.err.println("Error: Input phase has too few field tokens");
          System.err.println(aLine);
          hasParseErrors = true;
          continue;
        }
        if (! parseChannelName(strToke)) {
          System.err.println("Error: Input channel not known");
          System.err.println(aLine);
          hasParseErrors = true;
          continue;
        }
        String aToken = strToke.nextToken();
        try {
          amplitude = Double.parseDouble(aToken);
        }
        catch (NumberFormatException ex) {
          System.err.println("Error: Unable to parse phase amplitude");
          System.err.println(aLine);
          hasParseErrors = true;
          continue;
        }
        phase = defaultPhase; // override last phase setting to default
        if (strToke.hasMoreTokens()) {
          if (! parseDateTime(strToke.nextToken())) {
            System.err.println("Error: Unable to parse date time string");
            System.err.println(aLine);
            hasParseErrors = true;
            continue;
          }
          if (strToke.hasMoreTokens()) {
            aToken = strToke.nextToken().toUpperCase();
            if (aToken.equals("P") || aToken.equals("S")) {
              phase = aToken;
            }
            else {
              System.err.println("Error: Unable to parse phase type");
              System.err.println(aLine);
              hasParseErrors = true;
              continue;
            }
          }
        }
        else if (oldDate == null) {
          System.err.println("Error: Unable to set default date time string");
          System.err.println("Should enter a date time string for first amp reading");
          System.err.println(aLine);
          hasParseErrors = true;
        }
        Debug.println(sta +" "+  seedchan +" "+
          amplitude + " " + LeapSeconds.trueToString(datetime) //for UTC - aww 2008/02/07 
        ); 
        JasiCommitableIF aRow = createRow();
        if (aRow != null) jrList.add(aRow);
      }
    }
    catch (IOException ex) {
       ex.printStackTrace();
       hasParseErrors = true;
    }
    finally {
      try {
        if (buffReader != null) buffReader.close();
      }
      catch (IOException ex2) {
        ex2.toString();
      }
    }
  }
  public JasiCommitableIF createRow() {
      if (tableModel == null) return null;
      Amplitude aAmp = (Amplitude) tableModel.initRowValues(Amplitude.create());
      aAmp.phaseType.setValue(phase);
      setChannel(aAmp,defaultNetworkCode,sta,chan,seedchan);
      aAmp.setTime(datetime);
      aAmp.value.setValue(amplitude);
      aAmp.setQuality(1.0);
      aAmp.halfAmp = true;
      aAmp.windowStart.setValue(aAmp.getTime());
      aAmp.windowDuration.setValue(0.);
      aAmp.setClipped(Amplitude.ON_SCALE);
      if (seedchan != null && seedchan.substring(2,3).equalsIgnoreCase("Z")) {
          aAmp.setType(AmpType.HEL); // used to be PGD b4 05/03 for test aww
      }
      else {
          aAmp.setType(AmpType.WA);
      }
      aAmp.units = Units.CM;
      Debug.println("DEBUG parsed amp: " + aAmp.toString());
      return aAmp;
  }

  public void createInputTextDialog(JComponent tablePanel) {
    if (tablePanel instanceof AbstractJasiMagnitudeAssocPanel) {
      AbstractJasiMagnitudeAssocPanel aPanel = (AbstractJasiMagnitudeAssocPanel) tablePanel;
      //String magType = aPanel.getAssocMagnitude().getTypeString();
      //if (magType.equalsIgnoreCase("Ml") || magType.equalsIgnoreCase("Mh") ) {
      // Changed logic from above to allow Sol input and generic amp mag - aww 02/08/2005
      if ( ( aPanel.getAssocMagnitude() != null && aPanel.getAssocMagnitude().isAmpMag() )
             || aPanel.getAssocSolution() != null  ) {
        super.createInputTextDialog(tablePanel); 
      }
      else {
        InfoDialog.informUser(owner,"ERROR", "Solution or Amp Magnitude type must be associated with panel.", null);
      }
    }
    else {
      InfoDialog.informUser(owner,"ERROR", "Amp parser table panel class type unknown:"
                      + tablePanel.getClass().getName(), null);
    }
  }
}

