package org.trinet.util.graphics.table;
import java.io.*;
import java.util.*;
import javax.swing.*;
import org.trinet.jasi.*;
import org.trinet.jasi.coda.*;
import org.trinet.util.*;
import org.trinet.util.graphics.*;

public class DefaultCodaTextParser extends AbstractJasiReadingTextParser {

  protected double tau;

  public DefaultCodaTextParser() {
    super();
    parserName = "Coda";
    dialogTitle = "<sta> <chan> <tau(s)> [yyyy-MM-dd:HH:mm:ss.s P-arrival] [type(P)]";
    defaultPhase = "S";
    phase = defaultPhase;
  }

  public void parseInputTableRows(String text) {
    hasParseErrors = false;
    if (jrList == null) jrList = CodaList.create();
    else jrList.clear();
    if (text == null || text.length() == 0) return;
    BufferedReader buffReader = null;
    try {
      buffReader = new BufferedReader(new StringReader(text));
      String aLine = null;
      while ((aLine = buffReader.readLine()) != null) {
        if (aLine.trim().equals("")) continue;
        StringTokenizer strToke = new StringTokenizer(aLine);
        if (strToke.countTokens() < 3) {
          System.err.println("Error: Input phase has too few field tokens");
          System.err.println(aLine);
          hasParseErrors = true;
          continue;
        }
        if (! parseChannelName(strToke)) {
          System.err.println("Error: Input channel not known");
          System.err.println(aLine);
          hasParseErrors = true;
          continue;
        }
        String aToken = strToke.nextToken();
        try {
          tau = Double.parseDouble(aToken);
        }
        catch (NumberFormatException ex) {
          System.err.println("Error: Unable to parse coda tau");
          System.err.println(aLine);
          hasParseErrors = true;
          continue;
        }
        phase = defaultPhase; // override last phase setting to default
        if (strToke.hasMoreTokens()) {
          if (! parseDateTime(strToke.nextToken())) {
            System.err.println("Error: Unable to parse date time string");
            System.err.println(aLine);
            hasParseErrors = true;
            continue;
          }
          if (strToke.hasMoreTokens()) {
            aToken = strToke.nextToken().toUpperCase();
            if (aToken.equals("P") || aToken.equals("S") || aToken.equals("L")) {
              phase = aToken;
            }
            else {
              System.err.println("Error: Unable to parse phase type");
              System.err.println(aLine);
              hasParseErrors = true;
              continue;
            }
          }
        }
        else if (oldDate == null) {
          System.err.println("Error: Unable to set default date time string");
          System.err.println("Should enter a date time string for first coda reading");
          System.err.println(aLine);
          hasParseErrors = true;
        }
        Debug.println(sta +" "+  seedchan +" "+
          tau + " " + LeapSeconds.trueToString(datetime) //for UTC - aww 2008/02/07 
        ); 
        JasiCommitableIF aRow = createRow();
        if (aRow != null) jrList.add(aRow);
      }
    }
    catch (IOException ex) {
       ex.printStackTrace();
       hasParseErrors = true;
    }
    finally {
      try {
        if (buffReader != null) buffReader.close();
      }
      catch (IOException ex2) {
        ex2.toString();
      }
    }
  }
  public JasiCommitableIF createRow() {
      if (tableModel == null) return null;
      Coda aCoda = (Coda) tableModel.initRowValues(Coda.create());
      aCoda.setCodaType(CodaType.getCodaType(phase));
      setChannel(aCoda,defaultNetworkCode,sta,chan,seedchan);
      aCoda.setTime(datetime);
      aCoda.tau.setValue(tau);
      aCoda.setQuality(1.0);
      Debug.println("DEBUG parsed coda: " + aCoda.toString());
      return aCoda;
  }

  public void createInputTextDialog(JComponent tablePanel) {
    if (tablePanel instanceof AbstractJasiMagnitudeAssocPanel) {
      AbstractJasiMagnitudeAssocPanel aPanel = (AbstractJasiMagnitudeAssocPanel) tablePanel;
      //String magType = aPanel.getAssocMagnitude().getTypeString();
      //if (magType.equalsIgnoreCase("Md") || magType.equalsIgnoreCase("Mc") ) {
      // Changed logic from above to allow Sol input and generic coda mag 
      if ( ( aPanel.getAssocMagnitude() != null && aPanel.getAssocMagnitude().isCodaMag() )
             || aPanel.getAssocSolution() != null  ) {
        super.createInputTextDialog(tablePanel); 
      }
      else {
        InfoDialog.informUser(owner,"ERROR", "Solution or Coda Magnitude type must be associated with panel.", null);
      }
    }
    else {
      InfoDialog.informUser(owner,"ERROR", "Coda parser table panel class type unknown:"
                      + tablePanel.getClass().getName(), null);
    }
  }
}

