package org.trinet.util.graphics.table;
import javax.swing.*;
import javax.swing.table.*;
import java.text.*;
import org.trinet.jdbc.datatypes.*;

public class DeletedReadingRowCellNumberRenderer extends DeletedReadingRowCellRenderer {
  DecimalFormat df;

  public DeletedReadingRowCellNumberRenderer (AbstractTableModel tableModel, int referenceColumnIndex) { 
    super(tableModel,referenceColumnIndex);
    NumberFormat nf = NumberFormat.getNumberInstance();
    if (nf instanceof DecimalFormat) {
      df = (DecimalFormat) nf;
      df.setGroupingUsed(false);
      df.setDecimalSeparatorAlwaysShown(false);
    }
    setHorizontalAlignment(JLabel.RIGHT);
  }

  public void setValue(Object value) {
    if (value == null) {
      super.setValue("  ");
    }
    else if (value instanceof Number) {
      if (value instanceof Double) {
        if ( ((Double) value).isNaN() ) super.setValue("NaN");
        else super.setValue(df.format(value));
      }
      else if (value instanceof Float) {
        if ( ((Float) value).isNaN() ) super.setValue("NaN");
        else super.setValue(df.format(value));
      }
/*
      else if value instanceof Long) {
        if (value.longValue() == Long.MAX_VALUE) super.setValue("");
        else super.setValue(df.format(value));
      }
      else if value instanceof Integer) {
        if (value.intValue() == Integer.MAX_VALUE) super.setValue("");
        else super.setValue(df.format(value));
      }
*/
      else super.setValue(df.format(value));
    }
    else if (value instanceof DataObject) {
      if (value instanceof DataDouble) {
        if ( ((DataDouble) value).isNull() ) super.setValue("  ");
        else super.setValue(df.format(((DataDouble) value).doubleValue()));
      }
      else if (value instanceof DataFloat) {
        if ( ((DataFloat) value).isNull() ) super.setValue("  ");
        else super.setValue(df.format(((DataFloat) value).doubleValue()));
      }
      else if (value instanceof DataLong) {
        if (((DataLong)value).isNull() ) super.setValue("  ");
        else super.setValue(df.format(((DataLong) value).longValue()));
      }
/*
      else if value instanceof DataInteger) {
        if (((DataInteger)value).isNull() ) super.setValue("  ");
        else super.setValue(df.format(((DataInteger) value).longValue()));
      }
*/
      else super.setValue(df.format(((DataObject) value).longValue()));
   }
   else {
     super.setValue(value);
   }
 }
}
