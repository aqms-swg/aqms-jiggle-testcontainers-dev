package org.trinet.util.graphics.table;
import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import org.trinet.jasi.*;
import org.trinet.jasi.*;

/** Renderer to color table cells of deleted Solutions
*/
public class DeletedRowCellRenderer extends ColorableTextCellRenderer {
    protected Color deletedRowColor = Color.pink;
    protected AbstractTableModel tableModel;

    public DeletedRowCellRenderer (AbstractTableModel tableModel) {
        super();
        this.tableModel = tableModel;
    }

    public DeletedRowCellRenderer (AbstractTableModel tableModel, Color defaultBackgroundColor, Color deletedRowColor) {
        super();
        this.tableModel = tableModel;
        setDefaultBackground(defaultBackgroundColor);
        setDeletedRowColor(deletedRowColor);
    }

    public Component getTableCellRendererComponent(JTable jtable, Object value, boolean isSelected, boolean hasFocus,
                 int row, int col) {
        Component comp =  super.getTableCellRendererComponent(jtable, value, isSelected, hasFocus, row, col);
        if (!isSelected) {
          int rowIndex = row; 
          if (tableModel instanceof TableSorterAbs) rowIndex = ((TableSorterAbs)tableModel).indexes[row];
          // check sort if input table row index, need to convert to model index
          if ( ((JasiCommitableIF) ((JasiListTableModelIF) tableModel).getList().get(rowIndex)).isDeleted() ) {
            comp.setBackground(deletedRowColor); 
          }
          else  comp.setBackground(defaultBackground);
        }
        return comp;
    }

    public void setDeletedRowColor(Color deletedRowColor) {
        this.deletedRowColor = deletedRowColor;
    }
}
