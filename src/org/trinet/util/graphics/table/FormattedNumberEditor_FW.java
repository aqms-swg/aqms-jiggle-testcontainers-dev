package org.trinet.util.graphics.table;
import java.text.*;
import java.awt.*;
import java.awt.event.*;
import java.util.EventObject;
import javax.swing.*;
import org.trinet.util.Debug;
import org.trinet.util.graphics.text.*;

public class FormattedNumberEditor_FW extends AbstractCellEditor implements NullableEditorIF {
  protected NumberTextFieldFixed numberField;
  protected DecimalFormat df;
  protected int fontSize=12;

  public FormattedNumberEditor_FW() {
    this("#0.", 12); // note column count not fontsize here
  }

  public FormattedNumberEditor_FW(String pattern) {
    this(pattern, 12);
  }

  public FormattedNumberEditor_FW(String pattern, int columns) {
    NumberFormat nf = NumberFormat.getNumberInstance();
    if (nf instanceof DecimalFormat) {
      df = (DecimalFormat) nf;
      df.setGroupingUsed(false);
      df.setDecimalSeparatorAlwaysShown(false);
    }
    if (pattern == null || pattern.length() == 0) pattern = "#0."; 
    df.applyPattern(pattern);
    //Debug.println("FNE_FW pattern, columns:" + pattern + " " + columns);
    if (columns >= 0) numberField  = new NumberTextFieldFixed(0, columns, df);
    else numberField  = new NumberTextFieldFixed(0, 1, df);
    numberField.setHorizontalAlignment(NumberTextFieldFixed.RIGHT);
    numberField.setFont(new Font("Monospaced", Font.PLAIN, fontSize));
    numberField.addActionListener(this);
  }

  public boolean isNullAllowed() {
      return numberField.isNullAllowed();
  }

  public void setNullAllowed(boolean value) {
      numberField.setNullAllowed(value);
  }

  public int getFontSize() {
    return numberField.getFont().getSize();
  }

  public void setFontSize(int fontSize) {
    if (fontSize > 0 && this.fontSize != fontSize) {
      this.fontSize = fontSize;
      numberField.setFont(numberField.getFont().deriveFont((float)fontSize));
    }
  }

  public void setFont(Font font) {
    if (font == null) return;
    fontSize = font.getSize();
    numberField.setFont(font);
  }

//Override AbstractCellEditor methods

  public boolean startCellEditing(EventObject anEvent) {
    if(anEvent == null) numberField.requestFocus();
    else if (anEvent instanceof MouseEvent) {
      if (((MouseEvent)anEvent).getClickCount() < clickCountToStart)
        return false;
    }
    return true;
  }

  public Component getComponent() {
    return numberField;
  }

  public void setCellEditorValue(Object value) {
    if (value != null) {
    //Debug.println("FNE_FW setCellEditorValue value class: " + value.getClass().getName());
        numberField.setValue(value);
    }
    else numberField.setText("");
    this.value = value;
  }
 
//Override getCellEditorValue method to return a number, not a String:
  public Object getCellEditorValue() {
    //if (value != null)Debug.println("FNE_FW getCellEditorValue value class: " + value.getClass().getName());
    //Debug.println("FNE_FW numberField.getDataType(): " + numberField.getDataType());
    switch (numberField.getDataType()) {
      case NumberTextFieldFixed.INT_TYPE :
        return Integer.valueOf(numberField.getIntValue()) ;
      case NumberTextFieldFixed.LONG_TYPE :
        return Long.valueOf(numberField.getLongValue()) ;
      case NumberTextFieldFixed.FLOAT_TYPE :
        return Float.valueOf(numberField.getFloatValue()) ;
      case NumberTextFieldFixed.DOUBLE_TYPE :
        return Double.valueOf(numberField.getDoubleValue()) ;
      default:
        return numberField.getValue();
    } 
  }

  public Component getTableCellEditorComponent( JTable table, Object value, boolean isSelected, int row, int column) {
    //numberField.setText(value.toString());
    setCellEditorValue(value);
    return numberField; 
  }
}
