package org.trinet.util.graphics.table;
import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.text.*;
import org.trinet.jdbc.datatypes.*;

public class FormattedNumberRenderer extends ColorableTextCellRenderer {
  protected DecimalFormat df;

  public FormattedNumberRenderer() { 
    this(null, null);
  }

  public FormattedNumberRenderer(Font font) { 
    this(null, font);
  }

  public FormattedNumberRenderer(String pattern) {
    this(pattern, null);
  }

  public FormattedNumberRenderer(String pattern, Font font) {
    super(font);
    initDecimalFormat();
    if (df != null) df.applyPattern(pattern);
    setHorizontalAlignment(JLabel.RIGHT);
  }

  private void initDecimalFormat() {
    NumberFormat nf = NumberFormat.getNumberInstance();
    if (nf instanceof DecimalFormat) {
      df = (DecimalFormat) nf;
      df.setGroupingUsed(false);
      df.setDecimalSeparatorAlwaysShown(false);
    }
  }

  public void setValue(Object value) {
//  Debug.println("FormattedNumberRenderer setValue" + value.getClass().getName());
    if (value == null) {
      super.setValue("  ");
    }
    else if (value instanceof Number) {
      if (value instanceof Double) {
        if ( ((Double) value).isNaN() ) super.setValue("NaN");
        else super.setValue(df.format(value));
      }
      else if (value instanceof Float) {
        if ( ((Float) value).isNaN() ) super.setValue("NaN");
        else super.setValue(df.format(value));
      }
/*
      else if value instanceof Long) {
        if (value.longValue() == Long.MAX_VALUE) super.setValue("");
        else super.setValue(df.format(value));
      }
      else if value instanceof Integer) {
        if (value.intValue() == Integer.MAX_VALUE) super.setValue("");
        else super.setValue(df.format(value));
      }
*/
      else super.setValue(df.format(value));
    }
    else if (value instanceof DataObject) {
      if (value instanceof DataDouble) {
        if ( ((DataDouble) value).isNull() ) super.setValue("  ");
        else super.setValue(df.format(((DataDouble) value).doubleValue()));
      }
      else if (value instanceof DataFloat) {
        if ( ((DataFloat) value).isNull() ) super.setValue("  ");
        else super.setValue(df.format(((DataFloat) value).doubleValue()));
      }
      else if (value instanceof DataLong) {
        if (((DataLong)value).isNull() ) super.setValue("  ");
        else super.setValue(df.format(((DataLong) value).longValue()));
      }
      else if (value instanceof DataInteger) {
        if (((DataInteger)value).isNull() ) super.setValue("  ");
        else super.setValue(df.format(((DataInteger) value).longValue()));
      }
      else super.setValue(df.format(((DataObject) value).longValue()));
    }
    else {
        super.setValue(value);
    }
  }

  public static String getNumberFormatPattern(int scale) {
    StringBuffer pattern = new StringBuffer("#0");
    if (scale > 0) {
      char [] frac = new char[scale];
      Arrays.fill(frac, '0');
      pattern.append('.');
      pattern.append(frac);
    }
    return pattern.toString();
  }
}
