package org.trinet.util.graphics.table;
import org.trinet.util.EpochTime;
import org.trinet.jdbc.datatypes.DataObject;

public class IntervalRenderer extends ColorableTextCellRenderer {
  public void setValue(Object value) {
    if (value == null) {
      super.setValue("");
    }
    else if (value instanceof Number) {
      super.setValue(EpochTime.elapsedTimeToText((Number) value));
    }
    else if (value instanceof DataObject) {
      super.setValue(EpochTime.elapsedTimeToText( ((DataObject) value).intValue()));
    }
    else {
      super.setValue(value);
    }
  }
}
