// What is the coupling between units and amptype???
package org.trinet.util.graphics.table;
import java.util.*;
import javax.swing.event.*;
import org.trinet.jasi.*;
import org.trinet.jasi.TN.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;

/** Model represents a list of jasi Amplitude objects (AmpList) in a JTable. */
public class JasiAmpListTableModel extends AbstractJasiMagAssocTableModel
    implements JasiAmpListTableConstants {

    public JasiAmpListTableModel() {
        super();
        maxFields = JasiAmpListTableConstants.MAX_FIELDS;
        deleteColumnIndex = JasiAmpListTableConstants.DELETE;
        tableColumnNames = JasiAmpListTableConstants.columnNames; 
        tableColumnClasses = JasiAmpListTableConstants.columnClasses; 
        tableColumnFractionDigits = JasiAmpListTableConstants.columnFractionDigits;
        tableColumnAKey = JasiAmpListTableConstants.columnAKey;
        tableColumnEditable = JasiAmpListTableConstants.columnEditable;
        tableColumnNullable = JasiAmpListTableConstants.columnNullable;
        tableColumnShown = JasiAmpListTableConstants.showColumn;
        tableColumnCellWidthPadding = JasiAmpListTableConstants.cellWidthPadding;
    }

    public JasiAmpListTableModel(AmpList ampList) {
        this(ampList, false);
    }

    public JasiAmpListTableModel(Solution sol, boolean isCellEditable) {
        this(sol.getAmpList(), isCellEditable);
    }

    public JasiAmpListTableModel(AmpList ampList, boolean isCellEditable) {
        this();
        setList(ampList);
        setCellEditable(isCellEditable);
    }

    protected Amplitude getAmp(int rowIndex) {
        return (Amplitude) getJasiCommitable(rowIndex);
    }

    public Object getValueAt(int rowIndex, int colIndex) {
        Amplitude amp = getAmp(rowIndex);
        switch (colIndex) {
          case NET:
            return new DataString(amp.getChannelObj().getNet());
          case STA:
            return new DataString(amp.getChannelObj().getSta());
          case CHN:
            return new DataString(amp.getChannelObj().getChannel());
          case AU:
            return new DataString(amp.getChannelObj().getAuth());
          case SUBSRC:
            return new DataString(amp.getChannelObj().getSubsource());
          case CHNSRC:
            return new DataString(amp.getChannelObj().getChannelsrc());
          case SEED:
            return new DataString(amp.getChannelObj().getSeedchan());
          case LOC:
            return new DataString(amp.getChannelObj().getChannelName().getLocationString());
          case PHASE:
            return amp.phaseType;
          case QUALITY:
            return (amp.quality.isNull()) ? NULL_DATADOUBLE : amp.quality; 
            //return (amp.quality.isNull()) ? NULL_DATADOUBLE : new DataDouble(PhaseDescription.toWeight(amp.quality.doubleValue()));
          case DATETIME:
            return new DataDouble(amp.getTime());
          case RESIDUAL:  // mag calculation result, but not summary mag
            if (amp.channelMag == null) return NULL_DATADOUBLE;
            if (! amp.channelMag.residual.isNull()) return amp.channelMag.residual;
            double mag = (amp.getAssociatedMag() == null) ?
                   -9. : amp.getAssociatedMag().value.doubleValue();
            if (mag == -9.) return NULL_DATADOUBLE;
            return (amp.channelMag.value.isNull()) ?
              NULL_DATADOUBLE : new DataDouble(Math.abs(amp.channelMag.value.doubleValue() - mag));
          case DISTANCE:  // solution dependent
            //return amp.getChannelObj().dist;
            double value = amp.getHorizontalDistance(); // don't use slant amp.getDistance();
            return (value == Channel.NULL_DIST) ? NULL_DATADOUBLE : new DataDouble(value);
          case AZIMUTH: // solution dependent
            //return amp.getChannelObj().azimuth;
            value = amp.getAzimuth();
            return (Double.isNaN(value)) ? NULL_DATADOUBLE : new DataDouble(value);
          case IN_WGT: // summary mag result dependent
            return (amp.getChannelMag()==null) ? NULL_DATADOUBLE :
              new DataDouble(PhaseDescription.toWeight(amp.getChannelMag().getInWgt()));
          case WEIGHT: // summary mag result dependent
            return (amp.channelMag==null) ? NULL_DATADOUBLE :
              amp.channelMag.weight;
            //return new DataDouble(amp.getWeightUsed());
          case AUTH:
            return new DataString(amp.authority.toString());
          case SOURCE:
            return new DataString(amp.source.toString());
          case COMMENT:
            return new DataString(amp.comment.toString());
          case PROCESSING:
            return new DataString(amp.processingState.toString());
          case DELETE:
            return new DataString(String.valueOf(amp.isDeleted()));
          case WTIME :
            return amp.windowStart;
          case WDUR  :
            return amp.windowDuration;
          case VALUE :
            return amp.value;
          case UNITS :
            return Units.getString(amp.units);
          case TYPE  :
            return AmpType.getString(amp.type);
          case HALF  :
            return (amp.halfAmp) ? Boolean.TRUE : Boolean.FALSE;
          case ERR   : // dependent or not ?
            return amp.uncertainty;
          case PER   :
            return amp.period;
          case SNR   :
            return amp.snr;
          case CLP   :
            return new DataLong(amp.getClipped());
          case CMAG  : // mag calculation dependent not summary mag
            return (amp.channelMag == null) ? NULL_DATADOUBLE :
              amp.channelMag.value;
          case MCORR :
            return (amp.channelMag == null) ? NULL_DATADOUBLE :
              amp.channelMag.correction;
          case MAG   : // summary mag value dependent on group calculation
            return (amp.getAssociatedMag() == null) ? NULL_DATADOUBLE :
              amp.getAssociatedMag().value;
          case MAGID : // mag dependent on commit (else reuses old id)
            return (amp.getAssociatedMag() == null) ? NULL_DATALONG :
              amp.getAssociatedMag().magid;
          case EVID:
            return (amp.getAssociatedSolution() == null) ? NULL_DATALONG :
               amp.getAssociatedSolution().getId();
          case ORID: // solution dependent upon commmit (else reuses old id)
            if (amp instanceof AmplitudeTN) {
              SolutionTN solTN = (SolutionTN) amp.getAssociatedSolution();
              if (solTN != null) 
                 return (solTN.orid.isNull()) ? NULL_DATALONG : solTN.orid;
            }
            else return NULL_DATALONG;
          case AMPID:
            if (amp instanceof AmplitudeTN) {
              AmplitudeTN ampTN = (AmplitudeTN) amp;
              return (ampTN.getAmpid() > 0) ?
                  new DataLong(ampTN.getAmpid()) : NULL_DATALONG;
            }
            else return NULL_DATALONG;
          default:
            return "";
        }
    }
    public void setValueAt(Object value, int rowIndex, int colIndex) {
        // fireCellUpdate always?
        if (! cellChangeVerified(value, rowIndex, colIndex)) return;
        Amplitude amp = getAmp(rowIndex);
        boolean validCase = true;
        boolean effectsMagnitude = true;
      try {
        switch (colIndex) {
          case NET:
            amp.getChannelObj().setNet(value.toString());
            break;
          case STA:
            amp.getChannelObj().setSta(value.toString());
            break;
          case CHN:
            String chn = value.toString();
            amp.getChannelObj().setChannel(chn);
            // kludge need to have props set or new subclass for implementing particulars
            if (chn.length() > 0 && amp.getChannelObj().getLocation().equals(defaultChannelLocation)) {
              setValueAt(getSeedchanMap(chn), rowIndex, SEED);
            }
            //amp.getChannelObj().setChannel(value.toString());
            break;
          case AU:
            amp.getChannelObj().setAuth(value.toString());
            break;
          case SUBSRC:
            effectsMagnitude = false;
            amp.getChannelObj().setSubsource(value.toString());
            break;
          case CHNSRC:
            amp.getChannelObj().setChannelsrc(value.toString());
            break;
          case SEED:
            amp.getChannelObj().setSeedchan(value.toString());
            break;
          case LOC:
            amp.getChannelObj().setLocation(value.toString());
            break;
          case PHASE:
            amp.phaseType.setValue(value);
            break;
          case QUALITY:
            // don't sync with in_wgt here, infinite loop, the value change
            // should make mag stale to force in_wgt change upon mag solve.
            amp.quality.setValue(value);
            // could make Coda descriptor like Phase descriptor and have the "weight" included
            //amp.descriptor.setWeight(value);  // valueToInt(value);
            //Below is analog of phase, Coda description using hypoinv 0-4 wts:
            //amp.quality.setValue(PhaseDescription.toQuality(valueToInt(value)));
            break;
          case DATETIME:
            amp.setTime(((DataDouble)value).doubleValue());
            break;
          case RESIDUAL:
            effectsMagnitude = false;
            if (amp.channelMag != null)
              amp.channelMag.residual.setValue(value);
            break;
          case DISTANCE:
            effectsMagnitude = false;
            //amp.getChannelObj().dist.setValue(value);
            //amp.setDistance(((DataDouble)value).doubleValue()); // aww 06/11/2004
            amp.setHorizontalDistance(((DataDouble)value).doubleValue()); // aww 06/11/2004
            break;
          case AZIMUTH:
            effectsMagnitude = false;
            //amp.getChannelObj().azimuth.setValue(value);
            amp.setAzimuth(((DataDouble)value).doubleValue());
            break;
          case IN_WGT:
            //amp.getChannelMag().inWgt.setValue(value);
            Double q = Double.valueOf(PhaseDescription.toQuality(valueToInt(value)));
            // WARNING! for Waveform Assoc Events
            // Amp.QUALITY=completeness of data in time-span of peak Amp, it's not a Weight
            setValueAt(q, rowIndex, QUALITY); // force sync with quality
            amp.getChannelMag().inWgt.setValue(q);
            break;
          case WEIGHT:
            effectsMagnitude = false;
            amp.getChannelMag().weight.setValue(value);
            break;
          case AUTH:
            effectsMagnitude = false;
            amp.authority.setValue(value);
            break;
          case SOURCE:
            amp.source.setValue(value);
            break;
          case COMMENT:
            effectsMagnitude = false;
            amp.comment.setValue(value);
            break;
          case PROCESSING:
            effectsMagnitude = false;
            amp.processingState.setValue(value);
            break;
          case DELETE:
            boolean isDeleted = Boolean.valueOf(value.toString()).booleanValue();
            // Toggling delete flag in reading should evoke a "stale" event notification
            // Do not unassociate, it nulls references.
            // assume below doesn't unassociate() references
            // Alternative to delete here is to use list test notify below aww 03/03
            //amp.setDeleteFlag(isDeleted);

            // Code below implies GUI Magnitude level modification
            if (this.mag != null) {
               if (isDeleted) mag.delete(amp); // test 03/03 aww
               else mag.undelete(amp); // test 03/03 aww
               // should be unnecessary unless delete nulled a ref
               // below has coupling, does mag.sol too:
               if (! isDeleted) amp.associate(this.mag);
            }
            // Code below implies GUI Solution level modification
            // normally only edit from the Magnitude not the Solution panel
            // need to reload Amps and recalculate prefMag afterwards:
            else if (this.sol != null) { // force prefMag stale?
              if (isDeleted) sol.delete(amp); // test 03/03 aww
              else sol.undelete(amp); // test 03/03 aww
              if (! isDeleted) {
                Magnitude prefMag = this.sol.getPreferredMagnitude();
                if (prefMag != null && prefMag.isAmpMag()) {
                  // same logic as above, does this.sol too:
                  if (! isDeleted) amp.associate(prefMag);
                  // If prefMag ampList was not already loaded from DataSource 
                  // association of this amp with it here is futile.
                }
                // redundant, unless nulled by deletion:
                else amp.associate(this.sol);
              }
            }
            // below needed only if values are were reset by delete state change: 
            //fireTableCellUpdated(rowIndex, EVID);
            //fireTableCellUpdated(rowIndex, ORID);
            //fireTableCellUpdated(rowIndex, MAGID);
            break;
          case WTIME :
            effectsMagnitude = false; // maybe it does
            amp.windowStart.setValue(value);
            break;
          case WDUR  :
            effectsMagnitude = false; // maybe it does
            amp.windowDuration.setValue(value);
            break;
          case VALUE :
            amp.value.setValue(value);
            break;
          case UNITS :
            //amp.units = ((DataNumber)value).intValue();
            amp.units = Units.getInt((String)value);
            break;
          case TYPE  :
            //amp.type = ((DataNumber)value).intValue();
            amp.setType((String) value);
            break;
          case HALF  :
            amp.halfAmp = ((Boolean)value).booleanValue();
            break;
          case ERR   :
            effectsMagnitude = false;
            amp.uncertainty.setValue(value);
            break;
          case PER   :
            effectsMagnitude = false;
            amp.period.setValue(value);
            break;
          case SNR   :
            effectsMagnitude = false;
            amp.snr.setValue(value);
            break;
          case CLP   :
            amp.setClipped(((DataNumber)value).intValue());
            break;
          case CMAG  :
            if (amp.channelMag != null)
              amp.channelMag.value.setValue(value);
            break;
          case MCORR :
            if (amp.channelMag != null)
              amp.channelMag.correction.setValue(value);
            break;
          case MAG   :
            effectsMagnitude = false;
            if (amp.getAssociatedMag() != null)
              amp.getAssociatedMag().value.setValue(value);
            break;
          case MAGID :
            validCase = false;
            //if (amp.getAssociatedMag() != null) amp.getAssociatedMag().magid.setValue(value);
            break;
          case EVID:
            validCase = false;
            // don't ever enable this here dominoes effect all.
            //if (amp.getAssociatedSolution() != null)
            // amp.getAssociatedSolution().setId(((DataLong)value).longValue());
            break;
          case ORID:
            validCase = false;
            // don't ever enable this here dominoes effect all.
            //if (amp instanceof AmplitudeTN) {
            //  if (amp.getAssociatedSolution() != null)
            //    ((SolutionTN) amp.getAssociatedSolution()).dbaseSetPrefor((DataObject)value));
            //}
            break;
          case AMPID:
            validCase = false;
            //if (amp instanceof AmplitudeTN) {
            //   ((AmplitudeTN) amp).setAmpid(((DataLong)value).longValue());
            //}
            break;
          default :
             validCase = false;
        }
      }
      catch (Throwable thrown) {
         System.out.println("Exception, attempt to setValueAt failed for " + getClass().getName());
         return;
      }
        if (validCase) {
            amp.setNeedsCommit(true);  // for any change
            if (amp.isAssociatedWithMag() && effectsMagnitude) {
              amp.getAssociatedMag().setStale(true);
            }
            fireTableCellUpdated(rowIndex, colIndex);
            //if (amp.processingState.toString().equals(STATE_AUTO_TAG))
            //if (colIndex != PROCESSING && !amp.isHuman() ) // revert final?
            if (amp.isAuto())
                    setValueAt(JasiProcessingConstants.STATE_HUMAN_TAG, rowIndex, PROCESSING);
        }
    }
    public JasiCommitableIF initRowValues(JasiCommitableIF aRow) {
        Amplitude newAmp = (Amplitude) super.initRowValues(aRow);
        newAmp.phaseType.setValue("S");
        // not null, define: amplitude, units, wstart,      duration
        //           map to: value,     units, windowStart, windowDuration
        newAmp.value.setValue(0.);
        newAmp.units = Units.CM;
        newAmp.windowStart.setValue(newAmp.getTime());
        newAmp.windowDuration.setValue(0.);
        return newAmp;
    }
}
