package org.trinet.util.graphics.table;
import org.trinet.jasi.*;
public interface JasiListTableModelAssocMagIF extends JasiListTableModelAssocSolIF {
    public Magnitude getAssocMagnitude() ;
}
