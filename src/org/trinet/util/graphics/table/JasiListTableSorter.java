package org.trinet.util.graphics.table;
import java.awt.event.*;
import javax.swing.table.*;
import javax.swing.event.*;

public class JasiListTableSorter extends TableSorterAbs {

    public JasiListTableSorter() {
        super(); // For consistency.        
    }

    public JasiListTableSorter(TableModel model) {
        super(model);
    }

    public void tableChanged(TableModelEvent e) {
        if ( ! (e instanceof JasiListTableModelEvent) ) super.tableChanged(e); // pass it on
        //if list sorted order changed, reset map, then fire new event to model listeners:
        if ( ((JasiListTableModelEvent) e).wasOrderChanged() ) resetSort();
    }
}
