package org.trinet.util.graphics.table;
import java.awt.*;
import java.beans.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;
import org.trinet.util.graphics.*;
public class JasiMagListPanel extends AbstractJasiSolutionAssocPanel implements ListSelectionListener {
    private JButton jbJMAEP = null;
    //private JOptionPane jopJMAEP;
    //private JDialog assocDataDialog;
    private Dialog assocDataDialog;
    private boolean autoCloseDialogs = true; // by default close open dialog upon row selection change
    private HypoMagEngineDelegateIF engineDelegate = null;
    protected JasiMagnitudeAssocEditorPanel jmaePanel;

    public JasiMagListPanel() {this(null,false);}

    public JasiMagListPanel(JTextArea textArea, boolean updateDB) {
        super(textArea,updateDB);
        initPanel();
    }
    protected void initPanel() { 
        //if (tableColumnOrderByName == null) this.tableColumnOrderByName = JasiMagListTableConstants.columnNames;
        dataTable = new JasiMagListTable(new JasiMagListTableModel());
        //setCommitButtonEnabled(false);
        //setCommitEnabled(true);
        //setUpdateDB(true);
        super.initPanel();
    }

    private MouseAdapter tblMouseListener = new MouseAdapter() {
        public void mousePressed(MouseEvent e) {
            if ( e.getClickCount() > 1) jbJMAEP.doClick(); 
        }
    };

    protected void createTable() {
        JTable tbl = null;
        if (dataTable != null) {
            //dataTable.getSelectionModel().removeListSelectionListener(this);
            tbl = dataTable.getRowHeader();
            if (tbl != null) tbl.removeMouseListener(tblMouseListener);
        }

        super.createTable();

        if (dataTable != null) {
            dataTable.getSelectionModel().addListSelectionListener(this); // if selection changes whatever
            tbl = dataTable.getRowHeader();
            if (tbl != null) tbl.addMouseListener(tblMouseListener);
        }
    }
    protected JMenuBar createMenuBar() { // added menu option 06/29/2004 -aww
        JMenuBar menuBar = super.createMenuBar();
        JMenu menu = menuBar.getMenu(1);
        JCheckBoxMenuItem cbMenuItem = 
          new JCheckBoxMenuItem("Auto Close Dialogs"); 
        cbMenuItem.setActionCommand("autoCloseDialogs");
        boolean tf = autoCloseDialogs; // this.dataTable.inputProperties.getBoolean("autoCloseDialogs");
        cbMenuItem.setSelected(tf);
        autoCloseDialogs = tf;
        cbMenuItem.addItemListener( new ItemListener() {
          public void itemStateChanged(ItemEvent e) {
            autoCloseDialogs = (e.getStateChange() == ItemEvent.SELECTED);
          }
        });
        menu.add(cbMenuItem);
        return menuBar;
    }

    // implement ListSelectionListener for above
    // Autosolving here for stale mags on a row selection change
    // forces last changed assocPanel mag to be preferred mag.
    // Maybe don't do it here by default since editing may be a 
    // work in progress and user may not want to calcMag() yet?
    public void valueChanged(ListSelectionEvent e) {
      if (e.getValueIsAdjusting()) return;
      if (isListUpdateNeeded()) { // a row, new mag was added to table
        //DEBUG - print:
        //System.out.println("Magnitude selection changed - assoc data lists updated for prior selection");
        // update and solve, needed if return from jmaep dialog doesn't do update with autosolve
        updateAssocLists(true); // perhaps false arg here, see comment above on forced recalc/prefMag
      }
      // like SolutionList panel could add code here for force recalc if stale
      // when assocPanel mag is not the same as the prior selected mag row
      // aww 06/29/2004 force close of dialogs:
      if (autoCloseDialogs && assocDataDialog != null && assocDataDialog.isShowing()) assocDataDialog.dispose(); // aww test
    }
    protected void setList(Solution sol) {
        aSol = sol;
        setList(sol.getMagList());
    }

    boolean autoSolve = false;  // should be false by default
    public void setAutoSolve(boolean tf) { autoSolve = tf;}

    protected boolean hasModifiedAssocPanel() {
        return (jmaePanel != null) ? jmaePanel.hasModifiedTable() : false;
    }

    // one of the magnitudes in the list is stale
    protected boolean isStale() {
      return hasStaleMagnitude();
    }
    protected void setStale(boolean tf) {
      if (tf) {
        // only to force synch a cloned model list
        if (aSol != null) aSol.setNeedsCommit(tf);
        setListUpdateNeeded(tf);
      }
    }

    public boolean hasStaleMagnitude() {
      MagList modelList = (MagList) getList();
      if (modelList == null) return false;
      boolean retVal = false;
      for (int idx=0; idx < modelList.size(); idx++){
        Magnitude mag = (Magnitude) modelList.get(idx);
        if (mag.isStale()) {
          retVal = true; 
          break;
        }
      }
      return retVal;
    }

    // Assume if user made changes then autosolving as currently implemented
    // forces current mag to be the preferred mag. However, future changes
    // to engine logic may allow alternate mag to be updated without
    // changing the preferred magnitude of solution.
    // Could make solveWhenStale an optional property set through menu/properties
    protected void updateAssocLists(boolean solveWhenStale) {
      //System.out.println("DEBUG JMLP updateAssocLists hasModifiedAssocPanel(): "
      //    + hasModifiedAssocPanel() + " solveWhenStale: " + solveWhenStale);
      if (hasModifiedAssocPanel()) {
        jmaePanel.updateLists(false); // 12/17/2002 prevent recursive solve; see next line aww
        //if (solveWhenStale) jmaePanel.doEngineSolve(); // aww 06/29/2004 replaced by below
        if (autoSolve && solveWhenStale) jmaePanel.doEngineSolve(); // require autoSolve aww 06/29/2004
      }
    }
    protected int updateList() {
      return updateLists(true);
    }
    protected int updateLists(boolean solveWhenStale) {
      updateAssocLists(solveWhenStale);
      // update and solve here if assocPanel update doesn't solve
      // resolve selectedRow stale state, in method above, or when selection changes?
      return super.updateList();
    }
    /*
    protected int updateList() {
      if (! confirmUpdate()) return 0;
      return updateLists(true);
    }
    protected int updateLists(boolean solveWhenStale) {
      updateAssocLists(solveWhenStale);
      return synchInputList();
    }
    */

    /* Test removal, needed in case of cloned model list
    protected int synchInputList() { // removed 03/03 as test case
      Debug.println("DEBUG updating panel list for " + panelName);
      if (! isListUpdateNeeded()) return 0;
      MagList aList = (MagList) getList();
      if (inputList == aList) { // identical list object no-op
        setListUpdateNeeded(false);
        return 0;
      }
      boolean added = false;
      for (int idx=0; idx < aList.size(); idx++){
        Magnitude mag = aList.getMagnitude(idx);
        if (mag.isPreferred() && ! aSol.getPreferredMagnitude() == mag) {  
          aSol.setPreferredMagnitude(mag);  // should already be so; redundant
          added = true;
        }
        else {
          // below has unassociate check 02/03
          mag.assign(aSol);  // should already be so;
          //added = aSol.getMagList().add(mag); // only new Magnitude object added, no dupes
          // like above but also updates reading lists 02/03:
          added = aSol.add(mag);
          //All mags of same "type" replaced on list:
          //added = aSol.addOrReplaceByType(mag);
          //Matching identifier type mag replaced, even if prefmag:
          //added = aSol.addOrReplace(mag);
        }
        // below redundant, if done by sol add(Magnitude) method or listeners
        if (added) aSol.setNeedsCommit(true);
      }
      setListUpdateNeeded(false);
      return aList.size();
    }
   */

    protected boolean insertRow() { 
        Magnitude newMag = Magnitude.create();
        getModel().initRowValues(newMag);
        String selectedValue = (String) JOptionPane.showInputDialog(this,
              "Magnitude Type", "Select subscript", JOptionPane.QUESTION_MESSAGE, null,
              new String [] {"l","h","c","d","lr","a","b","s","e","w"}, "l" 
        ); 
        //System.out.println("DEBUG magListPanel subscript returned: " + selectedValue); // temp 03/03 aww
        newMag.subScript.setValue(selectedValue);
        return insertRow(newMag);
    }
    protected void addEngineDelegate(EngineDelegateIF delegate) {
        engineDelegate = (HypoMagEngineDelegateIF) delegate;
        if (jmaePanel != null) jmaePanel.addEngineDelegate(engineDelegate);
    }
    private void initJMAEP() {
        jmaePanel =
            new JasiMagnitudeAssocEditorPanel();
        jmaePanel.setUpdateEnabled(hasUpdateEnabled());
        jmaePanel.setCommitEnabled(hasCommitEnabled());
        jmaePanel.setCommitButtonEnabled(false);
        // disable waveform calc since new mag is generated, any panel readings shown would be for prior mag - aww 05/2004
        jmaePanel.setPanelLoadOptions(true,true);
        jmaePanel.setPanelNameLabel("");
        jmaePanel.setWaveMagButtonEnabled(false); //precedes initPanel
        jmaePanel.addEngineDelegate(engineDelegate); //precedes initPanel
        //adds jmaePanel as listener to SolutionListEditorPanel see below. 
        jmaePanel.initPanel();
        jmaePanel.setAutoSolve(autoSolve);
        jmaePanel.getTextArea().setDocument(getTextArea().getDocument()); // shared text document in textArea works 
        addPropertyChangeListener("magReadings", jmaePanel); // aww kludge to synch amp,coda lists
        //defer update by EngineIF.SOLVED, only update upon hasNewMag
        //when event is coming from other xxxEditorPanels?
        //if jmaePanel is not added listener to SolutionListEditorPanel then: 
        //jmaePanel.addPropertyChangeListener(EngineIF.SOLVED, this); // aww 
        //jopJMAEP = new JOptionPane(jmaePanel, JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION, null, null);
    }

    protected JPanel createButtonPanel() {
        jbJMAEP = new JButton("Data");
        jbJMAEP.setToolTipText("Show data associated with selected magnitude.");
        jbJMAEP.setActionCommand("JMAEP");
        jbJMAEP.setMargin(inset);
        jbJMAEP.setEnabled(false);
        jbJMAEP.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
              Magnitude mag = (Magnitude) getSelectedRow();
                //Debug.println("DEBUG JMLP selected mag.getNeedsCommit(): " + mag.getNeedsCommit());
              boolean resetSol = false;
              if (mag == null) {
                InfoDialog.informUser(getTopLevelAncestor(), "INFO",
                        "No magnitudes selected", statusLabel);
                return;
              }
              else { 
                if (jmaePanel==null) {
                    initJMAEP();
                    if (aSol != null) resetSol = ! aSol.getNeedsCommit();
                }
                if (resolvePriorMagnitude(mag) || ! hasAssocPanelData(mag)) loadAssocPanelData(mag);
                //else jmaePanel.setSelectedMagnitude(mag); // removed 8/30/2002
                if (jmaePanel.loadCanceled()) {
                  System.out.println("Load canceled");
                  return;
                }
                createAssocPanelDialog(mag).setVisible(true);
                //jopJMAEP.setValue(null); // resets pane state return
                //
                // REMOVED BELOW CODE, NOT MODAL DIALOG SO DON'T UPDATE NOW,
                // METHOD DROPS THROUGH TO RETURN UPDATE IS
                // INSTEAD INVOKED IN WINDOW LISTENER - AWW 12/20/02
                //informListeners(mag);
                /*
                // after dialog returns, check for edits, if any signal
                if (mag.getNeedsCommit() || jmaePanel.hasModifiedTable()) {
                  //Debug.println("DEBUG JMLP mag.getNeedsCommit(): " + mag.getNeedsCommit());
                  //Debug.println("DEBUG JMLP jmaePanel.hasModifiedTable(): " + jmaePanel.hasModifiedTable());
                  int idx = sortedToModelRowIndex(getSelectedRowIndex());
                  if (idx > -1) getModel().fireTableRowsUpdated(idx, idx);
                  //til mess fixed we need 12/16/02 aww:
                  firePropertyChange("magReadings", null, mag); // jmaePanel listens to refresh sol lists
                }
                */
                if (resetSol) aSol.resetStatusFlags();
              }
            }
            private boolean resolvePriorMagnitude(Magnitude mag) {
              Magnitude oldMag =  jmaePanel.getSelectedMagnitude();
              if (oldMag != null && mag != oldMag) {
                // modified flag is auto reset when list changed!
                //if (oldMag.getNeedsCommit() || jmaePanel.hasModifiedTable()) {
                  // recalculate old mag (stale/modified?)
                  updateAssocLists(true); // invoke dialog or automate?
                //}
              }
              return ((oldMag != mag) ? true : false);
            }
            private boolean hasAssocPanelData(Magnitude mag) {
              return (mag.getReadingsCount() > 0);
            }
            private void loadAssocPanelData(Magnitude mag) {
              if (getAssocSolution() != null) jmaePanel.setSelectedSolution(getAssocSolution()); //???
              else jmaePanel.setSelectedSolution(mag.getAssociatedSolution()); // test aww 10/7/2002?
              //Debug.println("JMLP debug selected sol : " + jmaePanel.getSelectedSolution());
              jmaePanel.setSelectedMagnitude(mag);
              //Debug.println("JMLP debug selected mag : " + jmaePanel.getSelectedMagnitude());
              DataObject magid = (DataObject) mag.getIdentifier();
              jmaePanel.setPanelNameLabel( "Magnitude id:"+magid.toStringSQL() );
              jmaePanel.loadData();
            }
            private Dialog createAssocPanelDialog(final Magnitude mag) {
              if (assocDataDialog == null) {
                //assocDataDialog = jopJMAEP.createDialog(getTopLevelAncestor(), null);
                //assocDataDialog.getContentPane().add(jopJMAEP); // ok button at bottom takes up to much space
                //assocDataDialog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
                assocDataDialog = new Dialog((Dialog)getTopLevelAncestor(), "", false);
                assocDataDialog.setResizable(true);
                assocDataDialog.setBounds(getTopLevelAncestor().getBounds());
                assocDataDialog.add(jmaePanel); // instead of optionPane
                assocDataDialog.addWindowListener(new WindowAdapter() {
                  public void windowClosing(WindowEvent e) {
                    // after dialog returns, check for updates, if any signal
                    try {
                      informListeners(mag);
                    }
                    catch (Throwable t) {
                      t.printStackTrace();
                    }
                    finally {
                      assocDataDialog.setVisible(false);
                    }
                  }
                });
              }
              //assocDataDialog.setTitle("Data for magnitude id:" + mag.getId());
              //assocDataDialog.setTitle(mag.toNeatString());
              assocDataDialog.setTitle(mag.toDumpString());
              return assocDataDialog;
            }
        }); // end of JMAEP data button action listener 

        JPanel aPanel = super.createButtonPanel();
        aPanel.add(jbJMAEP);
        return aPanel;
    } //end of create panel

    private void informListeners(Magnitude mag) {
      // ? Don't calcMag here since editing may be a work
      // in progress and user may not want to calcMag() yet ?
      if (mag.getNeedsCommit() || jmaePanel.hasModifiedTable()) {
        //Debug.println("DEBUG JMLP mag.getNeedsCommit(): " + mag.getNeedsCommit());
        //Debug.println("DEBUG JMLP jmaePanel.hasModifiedTable(): " + jmaePanel.hasModifiedTable());
        int idx = sortedToModelRowIndex(getSelectedRowIndex());
        if (idx > -1) getModel().fireTableRowsUpdated(idx, idx);
        //System.out.println("JMLP DEBUG FIRE UPDATE IDX: " + idx);
        // jmaePanel listens for event to refresh, a cloned model list, by replacing list with that of mag
        firePropertyChange("magReadings", null, mag); // should be a no-op for uncloned lists ==
      }
    }
    protected void showContentButtons() {
        super.showContentButtons();
        jbJMAEP.setVisible(true);
    }
    protected void hideContentButtons() {
        super.hideContentButtons();
        jbJMAEP.setVisible(false);
    }
    protected void enableContentButtons() {
        super.enableContentButtons();
        jbJMAEP.setEnabled(true);
    }
    protected void disableContentButtons() {
        super.disableContentButtons();
        jbJMAEP.setEnabled(false);
    }
    public int commit() {
      //if (jmaePanel != null) jmaePanel.commit();
      //return super.commit();
      return updateList(); // just update list, real commit() by solution 
    }

// override PropertyChangeListener method in parent
    public void propertyChange(PropertyChangeEvent e) {
        //super.propertyChange(e); // informModel below?
        debugProperty(e);
        if (e.getPropertyName().equals(EngineIF.SOLVED)) {
          if (dataTable.getRowCount() <= 0) return;
          Object newValue = e.getNewValue();
          Solution solvedSol = null;
          Magnitude solvedMag = null;
          if (newValue instanceof Solution) {
            solvedSol = (Solution) newValue;
            solvedMag = solvedSol.getPreferredMagnitude();
          }
          else if (newValue instanceof Magnitude) {
            solvedMag = (Magnitude) newValue;
            solvedSol = solvedMag.getAssociatedSolution();
            informListeners(solvedMag); // ? aww test kludge for amp,code list synch, redundant? aww 01/08/03
          }
          else {
            System.err.println("JMLP error: Unknown solved mag source type: " + newValue.getClass().getName());
          }
          // Because of problem with sol,mag diff behavior of coda vs. ml engine
          Magnitude selectedMag = (Magnitude) getSelectedRow();
          /* ???? is this needed here check aww 1/10/03
          if (solvedSol.equals(aSol) && solType) { // only do it for solution source
             // update panel table with the new mag list just in case new one added
             setList(aSol); // refresh, resets model table any row selection
          }
          */
          if (assocDataDialog != null) {
            if (jmaePanel.getSelectedMagnitude() == solvedMag)
              //assocDataDialog.setTitle(solvedMag.toNeatString());
              assocDataDialog.setTitle(solvedMag.toDumpString());
          }
          informModel();
          //dataTable.reinitializeTableCellWidths(); // redo for column changes, repaints values
          if (selectedMag != null) setSelectedRow(selectedMag); // reset the table row selection
        }
    }

    // possible listener to table model for future delegate use?
    // 03/24/2005 -aww try to notify JasiSolutionListPanel of preferred mag update
    public void tableChanged(TableModelEvent event) {
        super.tableChanged(event);
        int first = event.getFirstRow();
        int last = event.getLastRow();
        if (  (first >= 0)  && (first == last) ) {
          Magnitude mag = (Magnitude) getModel().getJasiCommitable(first);
          if (mag != null) {
            firePropertyChange("magTableChanged", null, mag.getAssociatedSolution());
          }
        }
    }
    //
    protected ActionListener createSaveTableActionListener() {
      return
        new AbstractJasiListPanel.SaveTableActionListener() {
          public void actionPerformed(ActionEvent evt) {
            statusLabel.setText(" ");
            if (getTable().getRowCount() <= 0)  {
              InfoDialog.informUser(getParent(), "ERROR",
                  panelName + ": No rows in table to save.",
                  statusLabel);
              return;
            }

            boolean success =
              saveToTextFile.save(JasiMagListPanel.this, getFrame());
              System.out.println("File write for " + panelName);
            if (success) {
              if (saveToTextFile.cascade() && jmaePanel != null) {
                Object magId = ((Magnitude) getSelectedRow()).getIdentifier();
                if (magId != null) {
                  Object assocMagId =  jmaePanel.getMagId();
                  System.out.println("id assoc:"+assocMagId+" selected: "+magId);
                  if ( magId.equals(assocMagId) ) {
                    success = saveAssocDataToFile();
                  }
                }
              }
            }
            else {
              InfoDialog.informUser(getParent(), "ERROR",
                panelName + ": Save to file FAILURE - see output text.",
                statusLabel);
            }
          }
          private boolean saveAssocDataToFile() {
            boolean success = true;
            if (jmaePanel.ampPanel != null) {
              success =
                success && saveToTextFile.save(jmaePanel.ampPanel,getFrame(), false);
                System.out.println("writing mag amps ...");
            }
            else if (jmaePanel.codaPanel != null) {
              success =
                success && saveToTextFile.save(jmaePanel.codaPanel,getFrame(), false);
                System.out.println("writing mag codas ...");
            }
            System.out.println("File write DONE status: " + success);
            return success;
          }
      };
    }
} // end of JasiMagListPanel class
