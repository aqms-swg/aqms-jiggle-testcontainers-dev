package org.trinet.util.graphics.table;
import org.trinet.jasi.*;
import org.trinet.jdbc.datatypes.*;
/** Define constants used by a MagListTableModel. These are the columns in a
 *  JTable that contains a MagList. */
public interface  JasiMagListTableConstants {
    public static final int MAX_FIELDS    = 21;
    public static final int VALUE         = 0;
    public static final int SUBSCRIPT     = 1;
    public static final int AUTH          = 2;
    public static final int SOURCE        = 3;
    public static final int METHOD        = 4;
    public static final int USED_STATIONS = 5;
    public static final int USED_CHNLS    = 6;
    public static final int READINGS      = 7;
    public static final int ERROR         = 8;
    public static final int GAP           = 9;
    public static final int DISTANCE      = 10;
    public static final int QUALITY       = 11;
    public static final int PROCESSING    = 12;
    public static final int DELETE        = 13;
    public static final int PREFERRED     = 14;
    public static final int STALE         = 15;
    public static final int EVID          = 16;
    public static final int ORID          = 17;
    public static final int MAGID         = 18;
    public static final int REMARK        = 19;  // not implemented, but shouldn't a mag have a comment?
    public static final int TYPEPREF      = 20;  // not implemented, but shouldn't a mag have a comment?

// End of member identifier constants
    public static final Class [] columnClasses = {
        DataDouble.class, DataString.class, DataString.class, DataString.class, DataString.class,
        DataLong.class, DataLong.class, DataLong.class, DataDouble.class, DataDouble.class,
        DataDouble.class, DataDouble.class, DataString.class, Boolean.class, Boolean.class,
        Boolean.class, DataLong.class, DataLong.class, DataLong.class, DataString.class,
        Boolean.class
    };

    public static final int [] columnFractionDigits = {
        2, 0, 0, 0, 0,
        0, 0, 0, 2, 1,
        1, 2, 0, 0, 0,
        0, 0, 0, 0, 0,
        0
    };

    public static final String [] columnNames = {
        "MAG","MT","AU","SRC","HOW",
        "STNS","CHLS","N","RMS","GAP",
        "DIST", "Q","PFLG","DFLG","PREF",
        "STALE", "EVID","ORID","MAGID","RMK",
        "TPREF"
    };

    public static final boolean [] columnAKey = {
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, true, false,
        false
    };

    public static final boolean [] columnEditable = {
        true, true, true, true, true,
        false, false, false, false, false,
        false, false, true, true, true,
        true, false, false, false, true,
        true
    };

    public static final boolean [] columnNullable = {
        false, false, false, false, false,
        true, true, true, true, true,
        true, false, false, false, false,
        false, true, true, false, true,
        false
    };

    public static final boolean [] showColumn = {
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true
    };

    public static final int [] cellWidthPadding = {
        0, 10, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0
    };
}
