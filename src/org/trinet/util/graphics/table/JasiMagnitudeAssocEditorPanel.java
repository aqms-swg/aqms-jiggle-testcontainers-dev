//TODO: property/menu settable option to reset "stale or needsCommit" state
//      after load from data source (depends on whether loaded data is archived final).
//? enable/disable of button actions related to modified state.
package org.trinet.util.graphics.table;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import javax.swing.*;
import javax.swing.event.*;
import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;
public class JasiMagnitudeAssocEditorPanel extends AbstractJasiSolutionEditorPanel {
    protected boolean loadAmps = true;
    protected boolean loadCoda = true;
    protected JasiAmpListPanel   ampPanel;
    protected JasiCodaListPanel  codaPanel;
    protected int loadOption;
    private JTabbedPane jtb;
    //calcAllMags Button here since in SolutionEditorPanel panel magList might not yet be loaded
    //and reloading it from database just might erase all of user's uncommitted data, else
    //could put button into super class panel if user if first queried for a database load of magList.
    //by default the list only has the preferred magnitude.
    //protected JButton jbAllMags;

    public JasiMagnitudeAssocEditorPanel() {}
    public JasiMagnitudeAssocEditorPanel(Solution sol) {
        this(sol,null);
    }
    public JasiMagnitudeAssocEditorPanel(Solution sol, Magnitude mag) {
            setSelectedSolution(sol);
            setSelectedMagnitude(mag);
    }

    public void setPanelLoadOptions(boolean loadAmps, boolean loadCoda) {
        this.loadAmps = loadAmps;
        this.loadCoda = loadCoda;
    }

    protected boolean hasModifiedTable() {
        return 
         ((ampPanel != null) ?  ampPanel.hasModifiedTable() : false) ||
         ((codaPanel != null) ? codaPanel.hasModifiedTable() : false);
    }
 
    protected boolean hasStaleMag() {
      Magnitude mag = getSelectedMagnitude();
      return ((mag != null) ? mag.isStale(): false);
    }

    protected boolean hasStaleSol() {
      Solution sol = getSelectedSolution();
      return (sol == null) ? false : sol.isStale();
    }
 
    protected void addTableModelListener(TableModelListener tml) {
        if (ampPanel != null) ampPanel.addTableModelListener(tml);
        if (codaPanel != null) codaPanel.addTableModelListener(tml);
    }

    protected void loadData() {
        loadData(true); // aww 07/13/2004
    }
    protected boolean loadCanceled() {
        return ! (loadOption == JOptionPane.YES_OPTION ||
                        loadOption == JOptionPane.NO_OPTION);
    }
    protected boolean reloadDataLists(boolean reloadLists) {
        loadOption = JOptionPane.NO_OPTION;
        Magnitude selMag = getSelectedMagnitude(); 
        boolean hasValidMagId =
                ((DataLong)selMag.getIdentifier()).isValidNumber(); // new mag may not have db id - 07/15/2004 -aww
        if (hasValidMagId && reloadLists) {
          loadOption = JOptionPane.YES_OPTION;
          // check for existing list data in panel magnitude, in any exists prompt to verify load
          if (selMag.getReadingList().size() > 0) // aww test 07/13/2004
                  loadOption = getDataListLoadOption();
        }
        else {
        }
        return (loadOption == JOptionPane.YES_OPTION) ? true : false;
    }
    protected void loadData(boolean reloadLists) {
        // rework so mag observations keyed on mag not solution;
        Magnitude mag = getSelectedMagnitude();

        if (mag == null) {
          aLog.logTextnl("No selected magnitude for : " +
                         getClass().getName()); 
          return;
        }
        boolean queryDB = reloadDataLists(reloadLists);
        aLog.logTextnl("Loading lists for event: " + StringSQL.valueOf(getSolutionId()) +
                        " prefor: " + StringSQL.valueOf(getSolutionOriginId()) +
                        " magid: " + StringSQL.valueOf(getMagId())
                      );
        aLog.logTextnl("Selected mag  " + mag.toDumpString());
        if (loadCoda && mag.isCodaMag()) {
            if (queryDB) loadCodaList();
            //DEBUG -
            if (mag.isCodaMag() && mag.getAmpCount() > 0) {
              /*
              if (confirm( "Magnitude is type: "+ mag.getTypeString()+
                " and has non-empty amp list?") ) mag.getAmpList().clear();
              */
              JOptionPane.showMessageDialog(JasiMagnitudeAssocEditorPanel.this,
                "Magnitude is type: "+ mag.getTypeString()+
                " and has non-empty amp list");
            }
            codaPanel.setAssocMagnitude(mag);
        }
        if (loadAmps && mag.isAmpMag()) {
            if (queryDB) loadAmpList();
            //DEBUG -
            if (mag.isAmpMag() && mag.getCodaCount() > 0) {
               /*
               if (confirm("Magnitude is type: " + mag.getTypeString() +
                 " and has non-empty coda list?") ) mag.getCodaList().clear();
               */
              JOptionPane.showMessageDialog(JasiMagnitudeAssocEditorPanel.this,
                "Magnitude is type: "+ mag.getTypeString()+
                " and has non-empty coda list");
            }
            ampPanel.setAssocMagnitude(mag);
        }
        enablePanelTabs(mag);
    }
    //could do via ancestor listener, thus removing need private member jtb
    private void enablePanelTabs(Magnitude mag) {
        boolean codaMag = mag.isCodaMag();
        int codaIdx = jtb.indexOfComponent(codaPanel);
        jtb.setEnabledAt(codaIdx, codaMag); 
        int ampIdx = jtb.indexOfComponent(ampPanel);
        jtb.setEnabledAt(ampIdx, ! codaMag);
        jtb.setSelectedIndex((codaMag) ? codaIdx : ampIdx);
    }

    protected void loadAmpList() {
        Magnitude mag = getSelectedMagnitude();
        if (mag == null || ! mag.isAmpMag()) return;
        boolean needsCommit = mag.getNeedsCommit();
        boolean isStale = mag.isStale();
        // Next force clear since no way to discriminate unique objects
        // with identical data; need addOrReplace association comparator  
        // to replace old data with new data else risk of duplication
        Solution aSol = getSelectedSolution();
        boolean solNeedsCommit = aSol.getNeedsCommit();
        boolean solIsStale = aSol.isStale();
        // with listeners line below blows away associated readings in Magnitudes:
        //test below don't clear list here
        //let listener addOrReplace reading 04/25/03 aww
        //if (aSol != null) aSol.getAmpList().clear(); // load assoc mag and sol
        //System.out.println("DEBUG JMAEP clearing sol amplist loading mag amplist");
        //mag.loadAmpList(); // does associate and reloads both mag,sol amplist
        mag.loadReadingList(true); // clears, does associate and reload both mag,sol amplist
        if (restoreStateOnLoad) {
          mag.setStale(isStale);
          mag.setNeedsCommit(needsCommit);
          aSol.setStale(solIsStale);
          aSol.setNeedsCommit(solNeedsCommit);
        }
        aLog.logTextnl(" mag amps: " + mag.getReadingList().size());
    }
    protected void loadCodaList() {
        Magnitude mag = getSelectedMagnitude();
        if (mag == null || ! mag.isCodaMag()) return;
        boolean needsCommit = mag.getNeedsCommit();
        boolean isStale = mag.isStale();
        // Next force clear since no way to discriminate unique objects
        // with identical data; need addOrReplace to replace old contents
        // with new objects else data duplication (keys)
        Solution aSol = getSelectedSolution();
        boolean solNeedsCommit = aSol.getNeedsCommit();
        boolean solIsStale = aSol.isStale();
        // with listeners line below blows away associated readings in Magnitudes:
        //test below don't clear list here
        //let listener addOrReplace reading 04/25/03 aww
        //if (aSol != null) aSol.getCodaList().clear(); // load does assoc mag and sol
        //mag.loadCodaList(); // does associate and reload both mag,sol codalist
        mag.loadReadingList(true); // does associate and reload both mag,sol codalist
        aLog.logTextnl(" mag codas: " + mag.getReadingList().size());
        if (restoreStateOnLoad) {
          mag.setStale(isStale);
          mag.setNeedsCommit(needsCommit);
          aSol.setStale(solIsStale);
          aSol.setNeedsCommit(solNeedsCommit);
        }
    }
    protected void setSplitPaneBorders(JSplitPane jsp) { 
        initSplitPaneBorders(jsp, new Color(160,130,100));
    }
    protected JComponent createDataComponent() {
        jtb = new JTabbedPane();
        if (loadAmps) {
             ampPanel = createAmpPanel();
             jtb.addTab(ampPanel.getPanelName(), null, ampPanel, "amplitude table rows");
        }
        if (loadCoda) {
             codaPanel = createCodaPanel();
             jtb.addTab(codaPanel.getPanelName(), null, codaPanel, "coda table rows");
        }
        JPanel topPanel = new JPanel();
        topPanel.setPreferredSize(
            new Dimension(PREFERRED_PANEL_WIDTH,PREFERRED_PANEL_HEIGHT));
        topPanel.setLayout(new BorderLayout());
        topPanel.add(jtb, BorderLayout.CENTER);
        addLowerControlsToPanel(topPanel);
        return topPanel;
    }
    protected Box createUpperControlBox() {
        Box inputBox = super.createUpperControlBox();
        if (getClass() == JasiMagnitudeAssocEditorPanel.class) { 
          if (inputBox == null) inputBox = Box.createHorizontalBox();
          inputBox.add(createCopyFromSolListButton());
          inputBox.add(createLoadBySolutionButton());
          //inputBox.add(createResetInputWeightsButton());
          if (engineControlsEnabled && delegate != null) { // must have delegate
             jbAllMags.setEnabled(false); // don't do all
             jbAllMags.setVisible(false);
          }
        }
        else if (engineControlsEnabled && delegate != null) { // must have delegate
          jbAllMags.setEnabled(enableAllMagsCalc); // do all ok, if enabled
          jbAllMags.setVisible(enableAllMagsCalc); 
        }

        return inputBox;
    }
    /* Force reset of ChannelMag input weights to quality settings for summary magnitude recalculation
    // An input weight reset to current quality is done automatically by magMethods -aww 
    protected JButton createResetInputWeightsButton() {
        JButton jbResetWeights = new JButton("ResetWeights");
        jbResetWeights.setToolTipText("Resets all readings input weights to quality. for selected mag.)");
        jbResetWeights.setActionCommand("resetWt");
        jbResetWeights.setMargin(inset);
        jbResetWeights.setEnabled(isUpdateEnabled());
        jbResetWeights.setVisible(true);
        jbResetWeights.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
            //Do we need an SwingUtilites.invokeLater(new Runnable()  { //public void run() {
                Magnitude mag = getSelectedMagnitude();
                if (mag == null) {
                  JOptionPane.showMessageDialog(JasiMagnitudeAssocEditorPanel.this,
                                  "You must select a magnitude first.");
                  return;
                }
                MagnitudeAssociatedListIF aList = (MagnitudeAssociatedListIF) mag.getReadingList();
                if (aList == null) return;
                int count = aList.size();
                MagnitudeAssocJasiReadingIF jr = null;
                for (int idx=0; idx < count; idx++) {
                  //((MagnitudeAssocJasiReadingIF) aList.get(idx)).setWeightUsed(1.); // removed see next 3 lines -aww
                  jr = (MagnitudeAssocJasiReadingIF) aList.get(idx));
                  jr.setInWgt(jr.getQuality());
                  jr.setWeightUsed(0.);
                }
                // instead of below could change ActiveArrayList by adding public method
                // to fire global state change event to listeners:
                aList.fireListElementStateChanged("wtUpdated", 0, count); // which does something like:
                //   aal.fireIntervalStateChanged(new StateChange(aList, "updated", null), 0, aList.size());
                //if (mag.isCodaMag()) codaPanel.getModel().fireTableDataChanged(); // aww test here 06/03
                //else ampPanel.getModel().fireTableDataChanged(); // aww test here 06/03
            }
        });
        return jbResetWeights;
    }
    */
    protected JButton createCopyFromSolListButton() {
        JButton jbCopySolList = new JButton("CopyFromSol");
        jbCopySolList.setToolTipText("Copy into table all readings currently in the Solution's table panel.");
        jbCopySolList.setActionCommand("copyFromSol");
        jbCopySolList.setMargin(inset);
        jbCopySolList.setEnabled(isUpdateEnabled());
        jbCopySolList.setVisible(true);
        jbCopySolList.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
            //Do we need an SwingUtilites.invokeLater(new Runnable()  {
            //public void run() {
                Magnitude mag = getSelectedMagnitude();
                if (mag == null) {
                  JOptionPane.showMessageDialog(JasiMagnitudeAssocEditorPanel.this,
                                  "You must select a magnitude first.");
                  return;
                }
                // Note design flaw in Phase, Amplitude, Coda that puts the
                // magnitude calculation results (AssocXxM relations) in these
                // reading subclasses. These should have been attributes of the
                // Magnitude subclasses. Thus the need to clone readings for 
                // when solving for both Ml and Mh (Ml with Z components).
                //
                // Clear mag data list, copy existing data in Solution list  - aww 07/19/2004
                // could include data which is not associated with this mag
                mag.setReadingListFromAssocSolution(
                  confirm("Clone readings? (yes, only if both Mh and Ml calc will be done for same event.)")
                );  // choice enabled 02/16/2005 -aww
                MagnitudeAssociatedListIF jrList = mag.getReadingList();

                if (mag.isCodaMag())
                  codaPanel.setList(jrList);
                else
                  ampPanel.setList(jrList);

                if (jrList.size() == 0) {
                  JOptionPane.showMessageDialog(
                    JasiMagnitudeAssocEditorPanel.this,
                    "<html> Found NO readings in the Solution's list .<p> Solution's table must have data rows. </html>"
                  );
                }
                // could use loop code here if need to have a different addOrReplace method
                /*
                MagnitudeAssociatedListIF myList = (MagnitudeAssociatedListIF) mag.getReadingList();
                MagnitudeAssociatedListIF solList =
                    (MagnitudeAssociatedListIF) getSelectedSolution().getListFor(myList));
                if (myList == null) return;
                int count = solList.size();
                for (int idx=0; idx<count; idx++) {
                   JasiMagnitudeAssociationIF jma = (JasiMagnitudeAssociationIF) solList.get(idx);
                   if (! jma.isAssignedToMag()) jma.assign(mag);
                   myList.addOrReplaceWithSameTime(jma);
                }
                */
            }
        });
        return jbCopySolList;
    }

    protected JButton createLoadBySolutionButton() {
        JButton jbLoadBySolution = new JButton("LoadBySol");
        jbLoadBySolution.setToolTipText("Load into table all readings associated in the database with Solution.");
        jbLoadBySolution.setActionCommand("loadBySol");
        jbLoadBySolution.setMargin(inset);
        jbLoadBySolution.setEnabled(isUpdateEnabled());
        jbLoadBySolution.setVisible(true);
        jbLoadBySolution.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
            //Do we need an SwingUtilites.invokeLater(new Runnable()  {
            //public void run() {
                Magnitude mag = getSelectedMagnitude();
                if (mag == null) {
                  JOptionPane.showMessageDialog(JasiMagnitudeAssocEditorPanel.this,
                                  "You must select a magnitude first.");
                  return;
                }
                // Clear mag data list, do Solution reload from database
                // could include data which is not associated with this mag
                mag.loadReadingListBySolution(); // loads assoc Solution list from data source aww 07/19/2004
                MagnitudeAssociatedListIF jrList = mag.getReadingList();

                if (mag.isCodaMag())
                  codaPanel.setList(jrList);
                else
                  ampPanel.setList(jrList);

                if (jrList.size() == 0) {
                  JOptionPane.showMessageDialog(
                    JasiMagnitudeAssocEditorPanel.this,
                    "<html> Found NO readings in db ASSOCIATED with Solution.<p> If no rows, Solution missing association rows in database. </html>"
                  );
                }
                // could use loop code here if need to have a different addOrReplace method
                /*
                MagnitudeAssociatedListIF myList = (MagnitudeAssociatedListIF) mag.getReadingList();
                MagnitudeAssociatedListIF solList =
                    (MagnitudeAssociatedListIF) getSelectedSolution().getListFor(myList));
                if (myList == null) return;
                int count = solList.size();
                for (int idx=0; idx<count; idx++) {
                   JasiMagnitudeAssociationIF jma = (JasiMagnitudeAssociationIF) solList.get(idx);
                   if (! jma.isAssignedToMag()) jma.assign(mag);
                   myList.addOrReplaceWithSameTime(jma);
                }
                */
            }
        });
        return jbLoadBySolution;
    }

    protected void setComponentUpdateState(boolean tf) {
        if (ampPanel   != null) ampPanel.setUpdateDB(tf);
        if (codaPanel  != null) codaPanel.setUpdateDB(tf);
    }
    protected void setComponentCommitState(boolean tf) {
        if (ampPanel   != null) ampPanel.setCommitEnabled(tf);
        if (codaPanel  != null) codaPanel.setCommitEnabled(tf);
    }
    protected void enableComponentCommitButton(boolean tf) {
        if (ampPanel   != null) ampPanel.setCommitButtonEnabled(tf);
        if (codaPanel  != null) codaPanel.setCommitButtonEnabled(tf);
    }
    protected void commit() {
        Debug.println(getClass().getName() + " DEBUG commit");
        if (ampPanel != null) ampPanel.commit();
        if (codaPanel != null) codaPanel.commit();
    }
    protected void updateLists() {
      updateLists(true);
    }
    protected void updateLists(boolean solveWhenStale) {
        Debug.println(getClass().getName() + " DEBUG update lists");
        if (ampPanel != null) ampPanel.updateList();
        if (codaPanel != null) codaPanel.updateList();
        //if (solveWhenStale) doEngineSolve(); // aww removed 6/29/2004
        if (autoSolve && solveWhenStale) doEngineSolve(); // require autoSolve property 06/29/2004 aww ?
    }
    protected void doEngineSolve() {
        Debug.println("DEBUG JMAEP doEngineSolve stale sol,mag: " + hasStaleSol() + " " + hasStaleMag());
        if (hasStaleMag()) {
          if (! isAutoSolve() &&
              ! confirm( "Solve for magnitude using updated list data for magid: " +
                         StringSQL.valueOf(getMagId()) )  ) {
                  return;
          }
          // test for solution delegate
          if (delegate != null) delegate.calcMag(getSelectedMagnitude(),getSelectedSolution());
        }
    }
    protected JasiAmpListPanel createAmpPanel() {
        JasiAmpListPanel aPanel = new JasiAmpListPanel() ;
        aPanel.setPanelName("Amp");
        aPanel.setTextLogger(getTextLogger());
        this.addPropertyChangeListener(EngineIF.SOLVED, aPanel);
        //aPanel.addAncestorListener(this);
        return aPanel;
    }
    protected JasiCodaListPanel createCodaPanel() {
        JasiCodaListPanel aPanel = new JasiCodaListPanel();
        aPanel.setPanelName("Coda");
        aPanel.setTextLogger(getTextLogger());
        this.addPropertyChangeListener(EngineIF.SOLVED, aPanel);
        //aPanel.addAncestorListener(this);
        return aPanel;
    }
    /*
    protected Box createEngineControlBox() {
        Box box = null;
        if (delegate != null) {
          if (engineControlsEnabled) {
            box = Box.createHorizontalBox();
            box.add(Box.createGlue());
            JButton jb = createEnginePropsButton();
            if (jb != null) box.add(jb); // box.add(createEnginePropsButton());
            box.add(createSolveButton());
            box.add(createCalcMagButton());
            box.add(createCalcAllMagsButton());
            box.add(createCalcMagFromWaveformsButton());
            box.add(createTrialLocationButton());
            box.add(createFixLocationButton());
            box.add(createWhereEngineButton());
          }
        }
        return box;
    }
    protected JButton createCalcAllMagsButton() {
        jbAllMags = new JButton("calcAllMags");
        jbAllMags.setMargin(inset);
        jbAllMags.setToolTipText("Caculate all magnitudes.");
        jbAllMags.setActionCommand("calcAllMags");
        jbAllMags.setEnabled( (hasMagnitudeService() && hasLocationService()) );
        jbAllMags.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            calcAllMags();
          }
        });
        return jbAllMags;
    }
    protected boolean calcAllMags() {
      if (! (hasLocationService() && hasMagnitudeService())) {
        InfoDialog.informUser(getTopLevelAncestor(), "INFO", "No location or magnitude service, check your startup!", null);
        return false;
      }
      if (delegate == null) {
        InfoDialog.informUser(getTopLevelAncestor(), "INFO", "Location, Magnitude engine delegate null!", null);
        return false;
      }
      Solution selectedSolution = getSelectedSolution();
      if (selectedSolution == null) return false;

      if (selectedSolution.isStale() ) {
        InfoDialog.informUser(getTopLevelAncestor(), "INFO", "Selected solution is stale, relocate solution first!", null);
        if ( confirm("Selected solution is STALE, Click YES if you want to re-locate.") )
          if (! locate(selectedSolution)) return false;
      }
      
      initSolutionMagList(selectedSolution);

      Object prefMagId = selectedSolution.getPreferredMagId();
      Magnitude mag = null;
      boolean status = false;
      java.util.List magList = null; // selectedSolution.getMagList();
      // Test Type for now below, but it should be master array of known types from an IF or class
      String [] magTypes = new String [] {"h","l","c","d"};
      for (int i = 0; i < magTypes.length; i++) {
        magList = selectedSolution.getMagnitudeByType(magTypes[i]); // assumes returns only undeleted elements
        int magCount = magList.size();
        aLog.logTextnl("Calculating all magnitudes for type: "+magTypes[i]+" count = "+magCount); 

        if (magCount > 1) { 
          InfoDialog.informUser(getTopLevelAncestor(), "WARNING", "<html>Skipping calcMag for magType="+magTypes[i]+
                          "<p> Found multiple magnitudes, first deleted undesired!</html>", null);
          continue;  // for now abort this type, don't do if type has multiple elements
        }

        for (int idx = 0; idx < magCount; idx++) { // enabled to do multiples
          mag = (Magnitude) magList.get(idx);

          if (! mag.isDeleted()) status = delegate.calcMag(mag, selectedSolution); // should already be undeleted only
        }

      }
      selectedSolution.setPreferredMagFromIdInList(prefMagId); // reset preferred magnitude to original instance id?
      System.out.println(Lines.DOT_TEXT_LINE);
      System.out.println(Lines.DOT_TEXT_LINE);
      return status;
    }
    */
    // used by calcAllMags override here does no-op uses current list, no prompt for load
    protected void initSolutionMagList(Solution sol) {
    }

    protected boolean calcMag() {
      if (delegate == null) return false;
      if (delegate instanceof JasiSolutionEditorPanel) {
        return ((JasiSolutionEditorPanel) delegate).calcMag();
      }
      else return delegate.calcMag(aMag, aSol);
    }
    protected boolean locate() {
      if (delegate == null) return false;
      return (delegate instanceof JasiSolutionEditorPanel) ?
        ((JasiSolutionEditorPanel) delegate).locate() : delegate.locate(aSol);
    }
    public void propertyChange(PropertyChangeEvent e) {
        String propName = e.getPropertyName();
        if (propName.equals(EngineIF.SOLVED)) {
           scrollTextPane();
           firePropertyChange(propName, e.getOldValue(),e.getNewValue());
        }
        // below code not needed if model lists not cloned
        else if (propName.equals("magReadings")) {
          Solution sol = getSelectedSolution();
          Magnitude mag = (Magnitude) e.getNewValue();
          if (sol != mag.getAssociatedSolution()) return; // should be the same
          if (mag != getSelectedMagnitude()) return;  // should be the same 
          if ( mag.isCodaMag() ) {
            if (codaPanel != null) codaPanel.setList(mag);  //unlike jsaePanel use mag and not sol to refresh?
          }
          else if ( mag.isAmpMag() ) {
            //System.out.println("DEBUG magReadingsProperty update ampPanel mag size " + mag.getReadingList().size());
            if (ampPanel != null) ampPanel.setList(mag);  //unlike jsaePanel use mag and not sol to refresh?
          }
        }
    }
}
