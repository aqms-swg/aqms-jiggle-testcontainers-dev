package org.trinet.util.graphics.table;
import org.trinet.jasi.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;
import java.util.Arrays;
import org.trinet.util.*;
import org.trinet.util.graphics.text.*;
import org.trinet.jdbc.datatypes.*;
public class JasiPhaseListTable extends AbstractJasiListTable {
    public JasiPhaseListTable(JasiPhaseListTableModel tm) {
            super(tm);
            loadTableProperties("PhaseListTable.props");
    }
    protected boolean configureColEditorRendererByName(TableCellEditorRenderer tcer,
            int modelIndex, String colName) {
          boolean setValues = super.configureColEditorRendererByName(tcer,modelIndex,colName);
          if (setValues)  return setValues;
          if (colName.equals("PH")) {
            tcer.tcEditor = new ComboBoxCellEditor("PH", new String [] {"P","S"});
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            //tcer.tcRenderer = new ComboBoxCellRenderer("PH", new String [] {"P","S"});
            setValues = true;
          }
          else if (colName.equals("FM")) {
            tcer.tcEditor = new ComboBoxCellEditor("FM", new String [] {"c.","+.","d.","-.",".."});
            //tcer.tcEditor = new ComboBoxCellEditor("FM", new String [] {"cu","cr","c.","+.","du","dr","d.","-.",".u", ".r",".."});
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            //tcer.tcRenderer = new ComboBoxCellRenderer("FM", new String [] {"cu","cr","c.","du","dr","d.",".u", ".r",".."});
            setValues = true;
          }
          else if (colName.equals("EI")) {
            tcer.tcEditor = new ComboBoxCellEditor("EI", new String [] {"i","e","w","I","E","W"});
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            //tcer.tcRenderer = new ComboBoxCellRenderer("EI", new String [] {"i","e","w","I","E","W"});
            setValues = true;
          }
          else if (colName.equals("Q")) {
            tcer.tcEditor = new ComboBoxCellEditor("Q", new String [] {"0","1","2","3","4"});
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            //tcer.tcRenderer = new ComboBoxCellRenderer("Q", new String [] {"0","1","2","3","4"});
            setValues = true;
          }
          /*
          else if (colName.equals("IWT")) {
            tcer.tcEditor = new ComboBoxCellEditor("IWT", new String [] {"0","1","2","3","4"});
            tcer.tcRenderer = new ColorableTextCellRenderer(font);
            //tcer.tcRenderer = new ComboBoxCellRenderer("Q", new String [] {"0","1","2","3","4"});
            setValues = true;
          }
          */
          return setValues;
    }
}
