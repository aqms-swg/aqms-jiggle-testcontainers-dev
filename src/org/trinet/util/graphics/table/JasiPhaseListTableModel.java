package org.trinet.util.graphics.table;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jasi.TN.*;
import org.trinet.jdbc.datatypes.*;

/** Model represents a list of jasi Phase objects (PhaseList) in a JTable. */
public class JasiPhaseListTableModel extends AbstractJasiReadingTableModel 
             implements JasiPhaseListTableConstants {

    public JasiPhaseListTableModel() {
        super();
        maxFields = JasiPhaseListTableConstants.MAX_FIELDS;
        deleteColumnIndex = JasiPhaseListTableConstants.DELETE;
        tableColumnNames = JasiPhaseListTableConstants.columnNames; 
        tableColumnClasses = JasiPhaseListTableConstants.columnClasses; 
        tableColumnFractionDigits = JasiPhaseListTableConstants.columnFractionDigits;
        tableColumnAKey = JasiPhaseListTableConstants.columnAKey;
        tableColumnEditable = JasiPhaseListTableConstants.columnEditable;
        tableColumnNullable = JasiPhaseListTableConstants.columnNullable;
        tableColumnShown = JasiPhaseListTableConstants.showColumn;
        tableColumnCellWidthPadding = JasiPhaseListTableConstants.cellWidthPadding;
    }

    public JasiPhaseListTableModel(PhaseList phaseList) {
        this(phaseList, false);
    }

    public JasiPhaseListTableModel(Solution sol, boolean isCellEditable) {
        this(sol.getPhaseList(), isCellEditable);
    }

    public JasiPhaseListTableModel(PhaseList phaseList, boolean isCellEditable) {
        this();
        setList(phaseList);
        setCellEditable(isCellEditable);
    }

    protected Phase getPhase(int rowIndex) {
        return (Phase) getJasiCommitable(rowIndex);
    }

    public Object getValueAt(int rowIndex, int colIndex) {
        Phase phase = getPhase(rowIndex);
        switch (colIndex) {
          case NET:
            return new DataString(phase.getChannelObj().getNet());
          case STA:
            return new DataString(phase.getChannelObj().getSta());
          case CHN:
            return new DataString(phase.getChannelObj().getChannel());
          case AU:
            return new DataString(phase.getChannelObj().getAuth());
          case SUBSRC:
            return new DataString(phase.getChannelObj().getSubsource());
          case CHNSRC:
            return new DataString(phase.getChannelObj().getChannelsrc());
          case SEED:
            return new DataString(phase.getChannelObj().getSeedchan());
          case LOC:
            return new DataString(phase.getChannelObj().getChannelName().getLocationString());
          case PHASE:
            return new DataString(phase.description.iphase);
          case EI:
            return new DataString(phase.description.ei);
          case FM:
            return new DataString(phase.description.fm);
          case QUALITY:
            //return new DataDouble(phase.description.getQuality());
            //return new DataLong(phase.description.getWeight(phase.isReject()));
            return new DataLong(phase.description.getWeight());
          case DATETIME:
            return new DataDouble(phase.getTime());
          case RESIDUAL:
            return ((DataDouble) phase.residual);
          case DELAY:
            return ((DataDouble) phase.delay);
          case DISTANCE:  // solution dependent
            //return phase.getChannelObj().dist;
            double value = phase.getHorizontalDistance(); // don't use slant phase.getDistance();
            return (value == Channel.NULL_DIST) ? NULL_DATADOUBLE : new DataDouble(value);
          case AZIMUTH: // solution dependent
            //return phase.getChannelObj().azimuth;
            value = phase.getAzimuth();
            return (Double.isNaN(value)) ? NULL_DATADOUBLE : new DataDouble(value);
          case EMA:
            return ((DataDouble)phase.emergenceAngle);
          case WEIGHT:
            return ((DataDouble) phase.weightOut);
          case IN_WGT:
            return ((DataDouble) phase.weightIn);
            //return (phase.weightIn.isValidNumber()) ? new DataDouble(PhaseDescription.toWeight(phase.getInWgt())) : NULL_DATADOUBLE;
          case IMPORTANCE:
            return ((DataDouble) phase.importance);
          case AUTH:
            return new DataString(phase.authority.toString());
          case SOURCE:
            return new DataString(phase.source.toString());
          case COMMENT:
            return new DataString(phase.comment.toString());
          case PROCESSING:
            return new DataString(phase.processingState.toString());
          case DELETE:
            return new DataString(String.valueOf(phase.isDeleted()));
          case EVID:
            return (phase.getAssociatedSolution() == null) ? NULL_DATALONG : phase.getAssociatedSolution().getId();
          case ORID:
            if (phase instanceof PhaseTN) {
              SolutionTN solTN = (SolutionTN) phase.getAssociatedSolution();
              return (solTN != null) ?
                 solTN.orid : NULL_DATALONG;
            }
            else return NULL_DATALONG;
          case ARID:
            if (phase instanceof PhaseTN) {
              PhaseTN phaseTN = (PhaseTN) phase;
              return (phaseTN.getArid() > 0) ?
                  new DataLong(phaseTN.getArid()) : NULL_DATALONG;
            }
            else return NULL_DATALONG;
          default:
            return "";
        }
    }

    public void setValueAt(Object value, int rowIndex, int colIndex) {
        // fireCellUpdate always?
        if (! cellChangeVerified(value, rowIndex, colIndex)) return;
        Phase phase = getPhase(rowIndex);
        boolean validCase = true;
        boolean effectsLocation = true;
      try {
        switch (colIndex) {
          case NET:
            phase.getChannelObj().setNet(value.toString());
            break;
          case STA:
            phase.getChannelObj().setSta(value.toString());
            break;
          case CHN:
            String chn = value.toString();
            phase.getChannelObj().setChannel(chn);
            // kludge need to have props set or new subclass for implementing particulars
            if (chn.length() > 0 && phase.getChannelObj().getLocation().equals(defaultChannelLocation)) {
              setValueAt(getSeedchanMap(chn), rowIndex, SEED);
            }
            //phase.getChannelObj().setChannel(value.toString());
            break;
          case AU:
            phase.getChannelObj().setAuth(value.toString());
            break;
          case SUBSRC:
            effectsLocation = false;
            phase.getChannelObj().setSubsource(value.toString());
            break;
          case CHNSRC:
            phase.getChannelObj().setChannelsrc(value.toString());
            break;
          case SEED:
            phase.getChannelObj().setSeedchan(value.toString());
            break;
          case LOC:
            phase.getChannelObj().setLocation(value.toString());
            break;
          case PHASE:
            phase.description.iphase = value.toString();
            break;
          case EI:
            phase.description.ei = value.toString();
            break;
          case FM:
            effectsLocation = false;
            phase.description.fm = value.toString();
            break;
          case QUALITY:
            //phase.description.setQuality(valueToDouble(value));
            phase.description.setWeight(valueToInt(value));
            // quality is mapped to descriptor.weight expects int value here:
            Double qWt = Double.valueOf(phase.description.getQuality());
            setValueAt(qWt,rowIndex,IN_WGT); // force synch of in_wgt with quality ?
            break;
          case DATETIME:
            phase.setTime(((DataDouble)value).doubleValue());
            break;
          // RESULTS CALCULATED SHOULD NOT BE CHANGED
          // SET isEditable == false BY DEFAULT IN CONSTANTS
          case RESIDUAL:
            effectsLocation = false;
            phase.residual.setValue(value);
            break;
          case DELAY:
            //effectsLocation = true; // ? if a correction is added ?
            phase.delay.setValue(value);
            break;
          case DISTANCE:
            effectsLocation = false;
            //phase.getChannelObj().dist.setValue(value);
            //phase.setDistance(((DataDouble)value).doubleValue()); // aww 06/11/2004
            phase.setHorizontalDistance(((DataDouble)value).doubleValue()); // aww 06/11/2004
            break;
          case AZIMUTH:
            effectsLocation = false;
            //phase.getChannelObj().azimuth.setValue(value);
            phase.setAzimuth(((DataDouble)value).doubleValue());
            break;
          case EMA:
            effectsLocation = false;
            phase.emergenceAngle.setValue(value);
            break;
          case WEIGHT:
            effectsLocation = false;
            phase.weightOut.setValue(value);
            break;
          case IN_WGT:
            phase.weightIn.setValue(value);
            break;
          case IMPORTANCE:
            effectsLocation = false;
            phase.importance.setValue(value);
            break;
          case AUTH:
            effectsLocation = false;
            phase.authority.setValue(value);
            break;
          case SOURCE:
            effectsLocation = false;
            phase.source.setValue(value);
            break;
          case COMMENT:
            effectsLocation = false;
            phase.comment.setValue(value);
            break;
          case PROCESSING:
            effectsLocation = false;
            phase.processingState.setValue(value);
            break;
          case DELETE:
            boolean isDeleted = Boolean.valueOf(value.toString()).booleanValue();
            //phase.setDeleteFlag(isDeleted);
            if (isDeleted) sol.delete(phase) ; // test 03/03 aww
            else sol.undelete(phase); // test 03/03 aww
            // redundant insurance in case delete nulled ref or another sol ref:
            if (! isDeleted) phase.associate(this.sol);
            //Below code only needed if values changed by delete operation
            //fireTableCellUpdated(rowIndex, EVID);
            //fireTableCellUpdated(rowIndex, ORID);
            break;
          case EVID:
            validCase = false;
            // don't do this, domino effects all
            //if (phase.getAssociatedSolution() != null) phase.getAssociatedSolution().setId(((DataLong) value).longValue());
            break;
          case ORID:
            validCase = false;
            // don't do this, domino effects all
            //if (phase instanceof PhaseTN) ((SolutionTN)(phase.getAssociatedSolution())).dbaseSetPrefOr(value);
            break;
          case ARID:
            // for generic instead use: Long.parseLong(value.toString())
            validCase = false;
            //  if (phase instanceof PhaseTN) {
            //    ((PhaseTN) phase).setArid(((DataObject)value).longValue());
            //    validCase = true;
            //  }
            break;
          default :
            validCase = false;
        }
      
      }
      catch (Throwable thrown) {
         System.out.println("Exception, attempt to setValueAt failed for " + getClass().getName());
         return;
      }
     
        if (validCase) {
            phase.setNeedsCommit(true);
            if (phase.getAssociatedSolution() != null && effectsLocation) {
              phase.getAssociatedSolution().setStale(true);
            }

            fireTableCellUpdated(rowIndex, colIndex);
            //if (colIndex != PROCESSING && !phase.isHuman() ) // revert final?
            //if (phase.processingState.toString().equals(STATE_AUTO_TAG))
            if (phase.isAuto())
                setValueAt(JasiProcessingConstants.STATE_HUMAN_TAG, rowIndex, PROCESSING);
        }
    }

    public JasiCommitableIF initRowValues(JasiCommitableIF aRow) {
        Phase newPhase = (Phase) super.initRowValues(aRow);
        // not null, datetime 
        newPhase.description.set("P", "i", "..", 0);
        return newPhase;
    }

}
