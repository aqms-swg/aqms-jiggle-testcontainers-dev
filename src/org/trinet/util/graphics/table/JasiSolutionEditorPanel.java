// TODO: Configurable beans for drop-in button filters or engine options?
// TODO: Convert property lookup to Preferences model
// TODO: property/menu settable option to reset default "stale" commit state after fresh load
// need listeners to enable/disable of button actions related to modified state?
// tablemodel listener for changes to rows to alter button state?
package org.trinet.util.graphics.table;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.text.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.plaf.basic.*;
import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;
import org.trinet.jasi.coda.CodaList;
//import org.trinet.mung.*;
import org.trinet.util.*;
import org.trinet.util.graphics.*;
import org.trinet.util.graphics.text.*;
public class JasiSolutionEditorPanel extends AbstractJasiSolutionEditorPanel {

    static public final String DEFAULT_EVENT_PROP_FILE_NAME = EventSelectionProperties.DEFAULT_FILENAME;
    protected JasiSolutionListPanel solPanel = null;
    private JOptionPane jopDateChooser = null;
    private JDialog jdDateChooser = null;
    private DateTimeChooserPanel dtcPanel = null;
    private boolean numberFieldIdInputEnabled = false;
    private EventSelectionProperties eventProps = null;

    public JasiSolutionEditorPanel() { }

    protected void setSplitPaneBorders(JSplitPane jsp) {
        initSplitPaneBorders(jsp, new Color(130,100,160));
    }

    protected void initEngineDelegate() {
      if (this.delegate == null) delegate = createHypoMagEngineDelegate(); //subclass should override this method
      super.initEngineDelegate();
    }
    protected HypoMagEngineDelegateIF createHypoMagEngineDelegate() {
      HypoMagEngineDelegateIF delegate = null;
      String name = editorProps.getProperty("pcsEngineDelegate");
      try {
        if (name != null && name.length() > 0) {
          delegate = (HypoMagEngineDelegateIF) Class.forName(name).newInstance();
          delegate.setDelegateProperties(editorProps);
        }
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
      if (delegate == null) {
        System.err.println("Unable to create engine delegate class:");
        System.err.println("Using properties file type: " +editorProps.getFiletype() +
                        "filename: " + editorProps.getFilename() +
                        " property: pcsEngineDelegate = "+name);
      }
      return delegate;
    }

    private EventSelectionProperties initEventProps() {
        eventProps = new EventSelectionProperties(); // constructor tries to load default files aka jasi subdir

        String propFileName = editorProps.getEventSelectionProps();
        if (propFileName == null) {
            // propFileName = (myPrefs == null) ? DEFAULT_EVENT_PROP_FILE_NAME : myPrefs.getString("eventPropFileName", DEFAULT_EVENT_PROP_FILE_NAME);
            propFileName = DEFAULT_EVENT_PROP_FILE_NAME; // aww 02/16/2005
        }

        String defaultPropFileName = editorProps.getEventSelectionDefaultProps();
        eventProps.initialize(
                EnvironmentInfo.getApplicationName().toLowerCase(),
                propFileName,
                defaultPropFileName
        );

        /*
        eventProps.setFiletype(EnvironmentInfo.getApplicationName().toLowerCase());
        eventProps.setFilename(propFileName);
        eventProps.reset(); // aww 11/10/2004 method sets REQUIRED props
        // if REQUIRED props are not configured for right behavior must do:
        //readDefaultProperties();
        //readUserProperties(); // overrides any set above
        // -aww
        eventProps.setup(); // aww 11/05/2004
        */
        Debug.println("DEBUG JSEP event prop file: " + eventProps.getUserPropertiesFileName());
        return eventProps;
    }

    public void setList(GenericSolutionList solList) {
        if (solList != null)
            aLog.logTextnl("JSEP Input SolutionList size : " + solList.size());
        solPanel.setList(solList);
    }

    // need a EventSelector bean with property editor gui component.
    public void setEventSelectionProperties(EventSelectionProperties esp) { eventProps = esp;}

    protected boolean hasModifiedTable() {
        return (solPanel == null) ? false : solPanel.hasModifiedTable();
    }

    protected void addTableModelListener(TableModelListener tml) {
        if (solPanel != null) solPanel.addTableModelListener(tml);
    }

    protected void loadData() {
       if (JOptionPane.YES_OPTION == getDataListLoadOption()) loadSolutionList();
    }

    public void loadSolutionList() {

        if (solPanel != null && solPanel.isCommitable()) solPanel.commit(); // 04/09/2007 aww

        if (eventProps == null) eventProps = initEventProps();

        Container component = getTopLevelAncestor();

        // added code patch below to to reset ROWNUM limit if set too large. - aww 2/7/2005
        int maxRows = eventProps.getMaxCatalogRows();
        if (maxRows > DateTimeChooserPanel.MAX_ROWS) {
          //if (component instanceof Window)
            InfoDialog.informUser(null, "WARNING",
              "<html>Input EventSelectionProperty catalogMaxRows TOO large:" + maxRows + "<br>"+
              "Resetting value to allowed maximum of"+DateTimeChooserPanel.MAX_ROWS+ ".</html>", null);
          maxRows = DateTimeChooserPanel.MAX_ROWS;
          eventProps.setMaxCatalogRows(maxRows);
          if (dtcPanel != null) dtcPanel.resetMaxRows();
        }

        Debug.println("JSEP loadSolutionList eventProps:\n" + eventProps.listToString());

        GenericSolutionList solList = new GenericSolutionList();
        solList.setProperties(eventProps);
        solList.fetchByProperties(); // could cause solution stale state

        // added code patch below to inform user about query ROWNUM limit. - aww 2/7/2005
        if ( maxRows > 0 && solList.size() == maxRows ) { // SQL query rownum == catalogMaxRows
          //if (component instanceof Window)
            InfoDialog.informUser(null, "WARNING",
              "<html>Catalog MaxRows returned = " + maxRows + "<br>" +
              "More matching DB events may exist for input event filter.<br>" +
              "Reset max row value or change the date range bound.<br></html>", null);
        } // end of code patch

        for (int idx=0;idx<solList.size();idx++) {
            Solution tmpSol = (Solution)solList.get(idx);
            Magnitude prefMag = tmpSol.getPreferredMagnitude();
            if (
              (tmpSol.isStale() || tmpSol.getNeedsCommit() || tmpSol.hasChanged())
              ||
              ((prefMag != null) && (prefMag.getNeedsCommit() || prefMag.isStale() || prefMag.hasChanged()))
            ) {
              System.err.print("Assertion new solution unmodified FAILED: "+
                idx);
              System.err.println(
                " JSEP DEBUG loadSolutionList " +idx+
                " Sol isStale: "+tmpSol.isStale()+
                " Sol needsCommit: "+tmpSol.getNeedsCommit()+
                " Sol hasChanged: "+tmpSol.hasChanged() +
                " Mag isStale: "+prefMag.isStale()+
                " Mag needsCommit: "+prefMag.getNeedsCommit()+
                " Mag hasChanged: "+prefMag.hasChanged()
              );
              // aww reset changed states on load, insurance
              tmpSol.setStale(false);
              tmpSol.setNeedsCommit(false);
              prefMag.setStale(false);
              prefMag.setNeedsCommit(false);

            }
        }
        boolean modified = solPanel.hasModifiedTable(); // save state before load from db aww 03/03
        setList(solList); // runs in thread
        if (solList.size() == 0) {
          //Container component = getTopLevelAncestor();
          //if (component instanceof Window)
            InfoDialog.informUser(component, "INFO",
              "No solutions satisfy event selection properties", null);
        }
        // Table thread should unset modified after completion and update of status
        // we get here before table creation thread completes, thread join wait here?
        // data from db so no need to force commit yet? aww 03/03
        if (! modified) solPanel.unsetTableModified();
    }

    protected JComponent createDataComponent() {
        solPanel = createSolutionPanel();
        solPanel.whereEngine = AbstractJasiSolutionEditorPanel.whereEngine; // use a common static WhereIsEngine -aww
        //solPanel.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION); // default single
        JPanel topPanel = new JPanel();
        topPanel.setPreferredSize(
            new Dimension(PREFERRED_PANEL_WIDTH,PREFERRED_PANEL_HEIGHT));
        topPanel.setLayout(new BorderLayout());
        topPanel.add(solPanel, BorderLayout.CENTER);
        addLowerControlsToPanel(topPanel);
        return topPanel;
    }

    protected Box createUpperControlBox() {
        Box inputBox = super.createUpperControlBox();
        if (inputBox == null) inputBox = Box.createHorizontalBox();
        inputBox.add(createOmega13Button());
        inputBox.add(createFinalizeButton());
        inputBox.add(createDateRangeButton());
        if (numberFieldIdInputEnabled) {
            inputBox.add(Box.createHorizontalStrut(20));
            JLabel aLabel = new JLabel("Fetch ID:");
            aLabel.setToolTipText("Scroll to table row with id, else load event id from data source.");
            inputBox.add(aLabel);
            //inputBox.add(createNumberTextField());
            inputBox.add(createIdComboBox());
        }
        inputBox.add(Box.createGlue());
        return inputBox;
    }
    protected JButton createDateRangeButton() {
        JButton jbDates = new JButton("DateRange");
        jbDates.setToolTipText("Choose new date range for Solutions.");
        jbDates.setActionCommand("Dates");
        jbDates.setMargin(inset);
        jbDates.setEnabled(true);
        jbDates.setVisible(true);
        jbDates.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if (jopDateChooser == null) initDateTimeChooser();
                    jdDateChooser = jopDateChooser.createDialog(
                            getTopLevelAncestor(),"Select Solution List Date Range");
                    jdDateChooser.pack();
                    jdDateChooser.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
                    jdDateChooser.setVisible(true);
                Object selectedValue = jopDateChooser.getValue();
                if(selectedValue == null) return;
                if(selectedValue instanceof Integer) {
                    if (JOptionPane.OK_OPTION == ((Integer)selectedValue).intValue()){
                        // reset properties and prefs
                        if (eventProps == null) eventProps = initEventProps();
                        eventProps.setMaxCatalogRows(dtcPanel.getMaxRows()); // comboBox added to dtcPanel -aww
                        //
                        setListSelectionTimeRange(dtcPanel.getStartDateTime(), dtcPanel.getEndDateTime());
                        loadSolutionList();
                        if (editorProps.getBoolean("packWindowOnSolutionLoad")) {
                           ((Window) getTopLevelAncestor()).pack();
                        }
                    }
                }
            }
        });
        return jbDates;
    }

    private JButton createOmega13Button() {
        JButton jbOmega13= new JButton("Import");
        jbOmega13.setToolTipText("Import table data from a formatted file.");
        jbOmega13.setActionCommand("Import");
        jbOmega13.setMargin(inset);
        jbOmega13.setEnabled(true);
        jbOmega13.addActionListener(new ActionListener() {
            private JOptionPane jop;
            private ButtonGroup bg;
            private void initOptionPane() {
              JRadioButton mung = new JRadioButton("DEFAULT");
              mung.setActionCommand("org.trinet.mung.MungDefaultSolutionListParser"); // should set by input property
              mung.setEnabled(true);
              mung.setSelected(true);
              JRadioButton custom = new JRadioButton("CUSTOM");
              String customName = editorProps.getProperty("customSolutionListParserName");
              if (customName != null) {
                // e.g. MungMoldySolutionListParser ?
                custom.setActionCommand(customName);
                custom.setToolTipText(customName);
                custom.setEnabled(true);
              }
              JRadioButton xml = new JRadioButton("XML");
              StringBuffer sb = new StringBuffer(128);
              sb.append(getClass().getPackage().getName()).append(".");
              sb.append("JasiXmlSolutionListParser");
              xml.setActionCommand(sb.toString());
              xml.setEnabled(false);
              //JRadioButton hinv = new JRadioButton("HYPARC");
              //hinv.setActionCommand("JasiHypArcParser");
              //hinv.setEnabled(false);
              bg = new ButtonGroup();
              bg.add(mung);
              bg.add(xml);
              bg.add(custom);
              //bg.add(hinv);
              JPanel optionPanel = new JPanel();
              optionPanel.setLayout(new BoxLayout(optionPanel,BoxLayout.Y_AXIS));
              optionPanel.add(new JLabel("Select Input filter"));
              optionPanel.add(mung);
              optionPanel.add(xml);
              //optionPanel.add(hinv);
              optionPanel.add(custom);
              jop = new JOptionPane(optionPanel,JOptionPane.PLAIN_MESSAGE,
                 JOptionPane.OK_CANCEL_OPTION);
            }
            private Class getImportFilterClass() {
              if (bg == null) initOptionPane();
              Class aClass = null;
              jop.createDialog(
                    JasiSolutionEditorPanel.this.getTopLevelAncestor(),
                    "Select input file filter type"
                ).setVisible(true);
              if (((Integer)jop.getValue()).intValue() == JOptionPane.YES_OPTION) {
                ButtonModel bm = bg.getSelection();
                String className = (bm == null) ? null : bm.getActionCommand();
                if (className != null) {
                  try {
                    aClass = Class.forName(className);
                  }
                  catch (Exception ex) {
                    System.err.println(ex.toString());
                  }
                }
              }
              return aClass;
            }


            public void actionPerformed(ActionEvent evt) {
              try {
                Class parser = getImportFilterClass();
                if (parser != null) {
                    SolutionListParserIF sp = (SolutionListParserIF) parser.newInstance();
                    sp.setProperties(editorProps);
                    sp.setGraphicsOwner(JasiSolutionEditorPanel.this);
                    GenericSolutionList solList = sp.parseSolutions();
                    if (solList != null ) {
                       Iterator iter = solList.iterator();
                       while (iter.hasNext()) {
                         solPanel.insertRow((Solution)iter.next());
                       }
                    }
                }
                else
                  aLog.logTextnl("Aborted data import");
              }
              catch (Exception ex) {
                ex.printStackTrace();
              }
            }
        });
        return jbOmega13;
    }

    boolean finalize = false;
    public void setFinalize(boolean tf) { finalize = tf; }
    protected JButton createFinalizeButton() {
        JButton jbFinalize= new JButton("Finalize");
        jbFinalize.setToolTipText("Finalize commit of selected(s) solution");
        jbFinalize.setActionCommand("Finalize");
        jbFinalize.setMargin(inset);
        jbFinalize.setEnabled(finalize);
        jbFinalize.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
              if (solPanel == null || solPanel.getList() == null ||
                      solPanel.getList().size() == 0) {
                InfoDialog.informUser(getTopLevelAncestor(), "INFO",
                           "No solutions", null);
                return;
              }
              if (solPanel.hasNullAssocPanel()) {
                InfoDialog.informUser(getTopLevelAncestor(), "INFO",
                           "You must first load the data panel of the selected solution before you can commit it!", null);
                return;
              }

              //Solution sol = (Solution) solPanel.getSelectedRow(); // removed 02/10/2005 -aww
              Solution sol = null;

              JasiCommitableIF [] jc = solPanel.getSelectedRows();  // added multi-row -aww 02/10/2005
              if (jc == null || jc.length == 0) {
                InfoDialog.informUser(getTopLevelAncestor(), "INFO",
                           "No solution selected", null);
                return;
              }
              else {
                boolean status = false;
                for (int idx = 0; idx < jc.length; idx++) {
                    try {
                        // removed finalCommit() below because it does cloneAssocAmO 02/11/2005 
                        // finalCommit also does TPP state posting for alarms !!!
                        //status = ((Solution)jc[idx]).finalCommit();
                        sol = (Solution) jc[idx];
                        if (sol.getPhaseList().size() == 0)  {
                          if ( ! confirm("Solution has NO associated arrival picks,  Continue with commit?") ) continue;
                        }
                        else {
                          sol.setProcessingState(JasiProcessingConstants.STATE_FINAL_TAG);
                          status = sol.commit(); // 02/11/2005 should only update old, not write new orid
                        }
                    }
                    catch (JasiCommitException ex) {
                        InfoDialog.informUser(getTopLevelAncestor(), "INFO",
                            ex.toString(), null);
                        sol.setProcessingState(JasiProcessingConstants.STATE_HUMAN_TAG); // test -aww 02/11/2005
                    }
                    finally {
                        aLog.logTextnl("Final commit status: " + status+
                             " for id: " + sol.getIdentifier());
                    }
                }
              }
            }
        });
        return jbFinalize;
    }
    protected void setComponentUpdateState(boolean tf) {
        if (solPanel != null) solPanel.setUpdateDB(tf);
    }
    protected void setComponentCommitState(boolean tf) {
        if (solPanel != null) solPanel.setCommitEnabled(tf);
    }
    protected void enableComponentCommitButton(boolean tf) {
        if (solPanel != null) solPanel.setCommitButtonEnabled(tf);
    }
    public void commit() {
        if (solPanel != null) solPanel.commit();
    }
    // For this and other list panels, table column name list
    // should be defined by parsing strings from property/preferences.
    protected JasiSolutionListPanel createSolutionPanel() {
        JasiSolutionListPanel aPanel =
            new JasiSolutionListPanel();
            /*
            new String [] {
            "ID", "DATETIME", "LAT", "LON", "MAG", "MTYP", "MMETH", "Z", "AUTH", "SRC",
            "GAP", "DIST", "RMS", "ERR_T", "ERR_H", "ERR_Z", "OBS", "USED", "S", "FM",
            "Q", "V", "ETYPE", "PFLG", "ZF", "HF", "TF", "WRECS", "PR", "RMK",
            "DFLG", "STALE", "BFLG","ORID","MAGID","COMMID", "MECID","EXT_I", "HDATUM", "VDATUM",
            "TYPE", "METHOD", "CMODEL", "VMODEL", "ERR_LAT", "ERR_LON"
        });
        */

        aPanel.setPanelName("Solution");
        aPanel.setTextLogger(getTextLogger());
        //aPanel.addEngineDelegate(delegate);
        aPanel.addEngineDelegate(this);
        //this.addPropertyChangeListener(EngineIF.SOLVED, aPanel);
        this.addPropertyChangeListener(aPanel); // test solListPanel listen for all prop changes to this aww 04/03
        aPanel.addPropertyChangeListener(this);
        //aPanel.addAncestorListener(this);
        aPanel.setAutoSolve(autoSolve); // set solPanel setting to input property, else default -aww
        // monitor deletes of readings in mag amp,coda panel
        Solution.listenToMagReadingListState(true);
        return aPanel;
    }
    public boolean hasSelectedSolution() {
       return (solPanel.getSelectedRowIndex() > -1);
    }
    protected Solution getSelectedSolution() {
      if (hasSelectedSolution()) {
        setSelectedSolution((Solution) solPanel.getSelectedRow());
        Debug.println("Setting solution from associated SolPanel selection.");
        return super.getSelectedSolution();
      }
      else {
        InfoDialog.informUser(getTopLevelAncestor(), "INFO",
              "Must Select solution row id!", null);
        return null;
      }
    }
    public void setListSelectionTimeRange(DateTime startTime, DateTime endTime) {
    // Need new signature above, add catalogMaxRows parameter, its value is from a comboBox added to chooser panel
        if (eventProps == null) eventProps = initEventProps();
        if (startTime == null || endTime == null)  {
          System.out.println("Event time range from prior settings of users event properties.");
        }
        else {
          String selectStr = " startTime = " + startTime.toString() + "\n   endTime = " + endTime.toString();
          System.out.println(selectStr);
          eventProps.setTimeSpan(startTime.getTrueSeconds(), endTime.getTrueSeconds()); // changed from nominal to true secs for UTC -aww 2008/02/07
          if ( confirm("Permanently save event selection time range to your user preferences.\n" + selectStr) ) {
            System.out.println("Saving new event selection properties time range from specified input.");
            eventProps.saveProperties("JSEP startTime/endTime event selection range update");
            /* aww removed prefs 02/16/2005
            if (myPrefs != null) {
              myPrefs.putDouble("defaultEventStartTime", startTime.getTrueSeconds()); // changed from nominal to true secs for UTC -aww 2008/02/07
              myPrefs.putDouble("defaultEventEndTime", endTime.getTrueSeconds()); // changed from nominal to true secs for UTC -aww 2008/02/07
            }
            */
          }
        }
        //if (debug) System.out.println("DEBUG props: "); eventProps.list(System.out);
        if (debug) System.out.println("JSEP setListSelectionTimeRange eventProps:\n" + eventProps.listToString());
    }

    private class DateTimeChooserPanel extends JPanel {
        private static final double SECS_PER_HR  = 3600.;
        private static final double SECS_PER_DAY = 86400.;
        private static final double SECS_PER_WEEK = 7. * 86400.;
        private static final int HR = 0;
        private static final int DAY = 1;
        private int roundMode = HR;
        private double defaultRoundSecs = SECS_PER_HR;
        private double defaultInterval = SECS_PER_DAY;
        private DateTimeChooser dtcStart = null;
        private DateTimeChooser dtcEnd = null;

        private static final int MAX_ROWS = 10000; // if changed edit chooser JComboBox list choices 
        private JComboBox maxRowsChooser = null;

        DateTimeChooserPanel () {
            setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
            int yrsBack = editorProps.getInt("dateRangeYearsBack");
            if (yrsBack == 0) yrsBack = 5;
            int yrsFuture = editorProps.getInt("dateRangeYearsFuture");
            if (yrsFuture == 0) yrsFuture = 5;
            dtcStart =
              new DateTimeChooser(new DateTime(eventProps.getStartAbsolute(), true), "Start Time", yrsBack, yrsFuture, true); // UTC time -aww 2008/02/13
            dtcEnd =
              new DateTimeChooser(new DateTime(eventProps.getEndAbsolute(), true), "End Time", yrsBack, yrsFuture, true); // UTC time -aww 2008/02/13
            add(dtcStart);
            add(dtcEnd);

            initTimes();

            configureMaxRows();
        }

        private void configureMaxRows() {
            JPanel jp = new JPanel();
            jp.add( new JLabel("Catalog max rows: ") );
            maxRowsChooser = new JComboBox( new String [] { "Props", "50", "100", "250", "500", String.valueOf(MAX_ROWS) } );
            maxRowsChooser.setEditable(false);
            maxRowsChooser.setSelectedItem("Props");
            jp.add(maxRowsChooser);
            add(jp);
        }

        void setDayRoundMode() {
          roundMode = DAY;
          defaultRoundSecs = SECS_PER_DAY;
        }
        void setHourRoundMode() {
          roundMode = HR;
          defaultRoundSecs = SECS_PER_HR;
        }
        void setDefaultInterval(double secs) {
          defaultInterval = secs;
        }
        private void initTimes() {
           double dt = 0.; // true secs
           try {
             // initialize settings from last setting in eventProperties
             dt = eventProps.getStartAbsolute();
             dtcStart.setTrueSeconds(dt); // 0 if not defined // changed from nominal to true secs for UTC -aww 2008/02/07
             dt = eventProps.getEndAbsolute();
             dtcEnd.setTrueSeconds(dt); // 0 if not defined // changed from nominal to true secs for UTC -aww 2008/02/07
           }
           catch (Exception ex) {
             System.out.println("No start/end time set in event properties; default is 0 seconds true time.");
             dt = 0.; // true secs
             dtcStart.setTrueSeconds(dt); // changed from nominal to true secs for UTC -aww 2008/02/07
             dtcEnd.setTrueSeconds(dt); // changed from nominal to true secs for UTC -aww 2008/02/07
             /*
             if (myPrefs != null) {
               dt = myPrefs.getDouble("defaultEventStartTime", 0.);
               dtcStart.setTrueSeconds(dt); // changed from nominal to true secs for UTC -aww 2008/02/07
               dt = myPrefs.getDouble("defaultEventEndTime", 0.);
               dtcEnd.setTrueSeconds(dt); // changed from nominal to true secs for UTC -aww 2008/02/07
             }
             */
           }
        }
           /*
           // Below code if inserted in catch block in method above could set time window
           // that straddles the origin times for events in already in event solution list
             dt = ((double)System.currentTimeMillis())/1000.;
             dt = roundToDefaultFloor(dt+defaultRoundSecs);
             dtcEnd.setTrueSeconds(dt); // changed from nominal to true secs for UTC -aww 2008/02/07
             dtcStart.setTrueSeconds(dt - defaultInterval); // changed from nominal to true secs for UTC -aww 2008/02/07
             if (solPanel != null && solPanel.getList().size() > 0) {
               java.util.List aList = solPanel.getList();
               if (aList != null && aList.size() > 0) {
                 double dt = ((Solution)aList.get(0)).getTime();
                 dtcStart.setTrueSeconds(roundToDefaultFloor(dt)); // changed from nominal to true secs for UTC -aww 2008/02/07
                 dt = ((Solution)aList.get(aList.size()-1)).getTime() + defaultRoundSecs;
                 dtcEnd.setTrueSeconds(roundToDefaultFloor(dt)); // changed from nominal to true secs for UTC -aww 2008/02/07
               }
             }
           */

        private double roundToDefaultFloor(double dt) {
          if (roundMode == DAY) return roundToDay(dt);
          else return roundToHour(dt);
        }
        private double roundToDay(double dt) {
          return (Math.floor(dt/SECS_PER_DAY) ) * SECS_PER_DAY;
        }
        private double roundToHour(double dt) {
          return (Math.floor(dt/SECS_PER_HR)) * SECS_PER_HR;
        }
        double getStartSeconds() { return dtcStart.getTrueSeconds();} // changed from nominal to true secs for UTC -aww 2008/02/07
        double getEndSeconds()   { return dtcEnd.getTrueSeconds();} // changed from nominal to true secs for UTC -aww 2008/02/07

        DateTime getStartDateTime() { return dtcStart.getDateTime();}
        DateTime getEndDateTime()   { return dtcEnd.getDateTime();}

        int getMaxRows() { 
          String count = (String) maxRowsChooser.getSelectedItem();
          if (count.equals("Props")) {
            return ( eventProps != null) ? eventProps.getMaxCatalogRows() : 0;
          }
          else {
            return (count != null) ? Integer.parseInt(count) : 0;
          }
        }
        void resetMaxRows() {
          maxRowsChooser.setSelectedItem("Props");
        }
    }

    protected void initDateTimeChooser() {
        dtcPanel = new DateTimeChooserPanel();
        jopDateChooser = new JOptionPane(dtcPanel,JOptionPane.PLAIN_MESSAGE,
                 JOptionPane.OK_CANCEL_OPTION);
    }

    public void setNumberFieldIdInputEnabled(boolean tf) {
        numberFieldIdInputEnabled = tf;
    }

    private JComboBox createIdComboBox() {
        final ComboBoxEditor cbEditor =
           new BasicComboBoxEditor () {
            {
              editor = createNumberTextField();
            }
            public Component getEditorComponent() {
              return editor;
            }
            public Object getItem() {
              return ((NumberTextField)editor).getValue();
            }
        };
        final JComboBox comboBox = new JComboBox();
        comboBox.setEditable(true);
        comboBox.setEditor(cbEditor);
        comboBox.setMaximumSize(new Dimension(60,20));
        comboBox.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
              String actionCommand = evt.getActionCommand();
              //entering new data in text field generates two events
              //with actionCommands: comboBoxEdited comboBoxChanged
              int idx =   comboBox.getSelectedIndex();
              Number aNumber = (Number) comboBox.getEditor().getItem();
              if (aNumber != null) {
                long solutionId = aNumber.longValue();
                if (((GenericSolutionList) solPanel.getList()).contains(solutionId)) {
                  if (actionCommand.equals("comboBoxChanged")) {
                    solPanel.setSelectedId(solutionId);
                    solPanel.resetScrollBarPosition(solPanel.getSelectedRowIndex());
                    return;
                  }
                }
                else {
                  boolean added = fetchSolutionById(solutionId);
                  if (added) {
                    if (idx < 0 ) {
                      ((DefaultComboBoxModel) comboBox.getModel()).addElement(aNumber);
                      aLog.logTextnl("Added to table model list id: " + solutionId);
                    }
                    //((Window) getTopLevelAncestor()).pack(); // is this needed ?
                  }
                  else {
                    aLog.logTextnl("Database fetch failure for id: " + solutionId);
                  }
                }
                comboBox.getToolkit().beep();
              }
            }
        });
        return comboBox;
    }
    private NumberTextField createNumberTextField() {
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance();
        df.setDecimalSeparatorAlwaysShown(false);
        df.setGroupingUsed(false);
        df.setParseIntegerOnly(true);
        df.applyPattern("#0");
        NumberTextField idField =
            new NumberTextFieldFixed(12,df);
        idField.setMaximumSize(new Dimension(100,30));
        //idField.setToolTipText("Scroll to id in table, else load data from database"); // removed to make more universal
        idField.setAddUndo();
        /*
        idField.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent evt) {
            String fieldText = "";
            try {
              aLog.logTextnl(aNameLabel.getText() + " IdField action fetching "); // +evt.toString());
              NumberTextField evtIdField = (NumberTextField) evt.getSource();
              fieldText = evtIdField.getText();
              fetchSolutionById(evtIdField.getLongValue());
            }
            catch (InvalidInputException ex) {
              aLog.logTextnl("Must enter valid solution id");
            }
            catch (NullPointerException ex) {
              aLog.logTextnl("Must enter valid solution id");
            }
            finally {
              aLog.logTextnl("input field new solution id = " + fieldText);
            }
          }
        });
        */
        idField.setNullAllowed(true);
        return idField;
    }

    protected boolean fetchSolutionById(long id) {
            aLog.logTextnl("fetching id : " + id);
       return solPanel.fetchById(id);
    }
    public void propertyChange(PropertyChangeEvent e) {
        super.propertyChange(e);
        // set solution dummy flag state
        String propName = e.getPropertyName();
        if ((e.getSource() == solPanel) && propName.equals("hasAssocPanel")) {
          // boolean oldEnableMag = enableMagButton;
          enableMagButton = (hasMagnitudeService() && hasLocationService());
          /*
          firePropertyPropertyChange("enableMagButton",
                          (oldEnableMag) ? Boolean.TRUE : Boolean.FALSE,
                          (enableMagButton) ? Boolean.TRUE : Boolean.FALSE
          );
          */
          jbMag.setEnabled(enableMagButton);
          jbAllMags.setEnabled(enableAllMagsCalc && enableMagButton);

          if (solPanel.jsaePanel.engineControlsEnabled) { // what's this ?
            solPanel.jsaePanel.jbMag.setEnabled(enableMagButton);
            solPanel.jsaePanel.jbAllMags.setEnabled(enableMagButton && enableAllMagsCalc);
          }
        }
        if (propName.equals(EngineIF.SOLVED)) {
           Object value = e.getNewValue();
           if (value instanceof Solution) {
              Solution sol = (Solution) value;
              //reportSummaryState();
              if ( ! (sol.isStale() || ! sol.isValid() || sol.isDummy()) ) {
                 if (solPanel.hasNullAssocPanel()) {
                   //Debug.println("propertyChange wasSolved IS invoking calcMag()");
                   // 11/01/2002 could be done by superclass for all cases:
                   //calcMag(); // to deal with an invalid preferred mag after relocation but before commit
                   // to deal with an invalid preferred mag after relocation but before commit
                   if (autoSolve) calcMag(); // do not solve by default, use autoSolve property 6/28/2004 -aww
                 }
                 //else don't autosolve selected mag in assoc maglist panel may not be preferred mag
                 //force user to resolve stale mags in assoc maglist panel
              }
              else {
                 //Debug.println("propertyChange wasSolved NOT invoking calcMag()");
              }
           }
        }
    }

    // override super
    protected boolean locate() {
        Solution sol = getSelectedSolution();
        if (sol == null) {
          aLog.logTextnl("Null solution; No associated solution set to locate " + getClass().getName());
          return false;
        }
        if ( (solPanel.jsaePanel != null) && (solPanel.jsaePanel.getSelectedSolution() == sol) ) {
          // Note: if tablemodel lists are aliased directly to external sol lists
          // then I don't need below update here to synch modified lists
          // update if same sol, ignore others, jmaePanel is not checked for same sol.
          solPanel.updateAssocLists(false); // false == don't autosolve upon update
        }
        resolveSolDataLists(sol);
        // remove leftover magnitude reference, if any, from another solution attempt
        //if (aMag != null && ! aMag.isAssignedTo(sol)) aMag = null;
        aMag = null; // aww is this necessary?
        return super.locate();
    }
    private void resolveSolDataLists(Solution sol) {
        if (sol != null && sol.getPhaseList().size() == 0) {
          System.out.println("Loading phase list from db");
          sol.loadPhaseList();
        }
    }

    // overrides superclass method so as to use prefmag if selected mag == null
    // Note: invoke after locate().
    // else need here to deal with list update situation as above.
    // update may be required to synch the mag amp lists with any additions.
    // Note: tablemodel lists are aliased directly to external sol lists
    // then I don't need update here to synch modified lists
    // use the selected MagPanel row only if its sol here matches selected sol
    // user must remember to select correct row in mag table.
    // if mismatched sol use solution's prefmag
    // User can flip prefmag flags in table if mag type change is desired.
    protected boolean calcMag() {
        Solution sol = getSelectedSolution();
        if (sol == null) {
          aLog.logTextnl("Null solution; No solution set for magnitude calculation " + getClass().getName());
          return false;
        }
        //update list before load from db
        if ( (solPanel.jsaePanel != null) && (solPanel.jsaePanel.getSelectedSolution() == sol) ) {
          // update if same sol, ignore others, jmaePanel is not checked for same sol.
          solPanel.updateAssocLists(false); // false == don't autosolve upon update
          if (solPanel.jsaePanel.magPanel != null) {
              Magnitude mag = (Magnitude) solPanel.jsaePanel.magPanel.getSelectedRow();
              /*
              if (magPanel.jmaePanel.getSelectedMagnitude() == mag) {
                //magPanel.updateAssocLists(false);  // above solPanel update does both phase and this update - aww
              }
              */
              System.out.println("Setting magnitude from associated MagPanel selection.");
              setSelectedMagnitude(mag);
          }
        }
        if (getSelectedMagnitude() == null) {
          if (sol.getPreferredMagnitude() == null) {
             System.out.println("Null prefmag, loading magnitude list from db");
             sol.loadMagList();
          }
          setSelectedMagnitude(sol.getPreferredMagnitude());
        }
        //resolveMagDataLists(getSelectedMagnitude()); // removed 04/28/2005 -aww
        return super.calcMag(); // sets new prefmag?
    }

    /*
    // REMOVED METHOD ! LET HypoMagSolutionDelegate HANDLE IT ! -aww 04/28/2005
    private void resolveMagDataLists(Magnitude mag) {
      if (mag == null) return;
      JasiReadingListIF jrl = mag.getReadingList();
      if (jrl.size() <= 0) {
        System.out.println("Empty data list, loading magnitude data from db");
        // below clear not needed if all data replaced by equiv by time ?
        // to avoid possible data duplication:
        JasiCommitableListIF aList = getSelectedSolution().getListFor(jrl);
        if (aList.size() > 0) {
          //aList.clear(); // maybe not ? 08/04/2004 -aww 
          // IF NOT DONE HERE LET DELEGATE TO IT BY PROPERTY SETTING SWITCH -aww 04/28/2005
          // clear to avoid duplicate solution readings if selected preferred mag is loaded? 
          aList.clear(false); // try it without notify of existing prefmag -aww?
          // assuming load associates with sol, ok if mag stale since recalc:
          mag.loadReadingList();
        }
      }
    }
    */

    // used by calcAllMags:
    protected void initSolutionMagList(Solution sol) {
      // prompt just in case revisiting uncommitted solution
      if ( solPanel.jsaePanel != null) {
        if (solPanel.jsaePanel.getSelectedSolution() == sol) return;
        if ( ! confirm("<html>Reload magnitude data from database?"+
                "<p>Clicking YES <b>destroys</b> data user edited or inserted (in GUI tables).</html>") ) return;
      }
      sol.loadMagList();
    }
} // end of class
    /*
    // old code below can be deleted once above proved working
      if (mag.isCodaMag()) {
        if (mag.getCodaList().size() <= 0) {
          System.out.println("Empty coda list, attempting load of magnitude coda list from db");
          // to avoid possible duplicates; replace equiv by time aww 10/29/2002
          CodaList aList = getSelectedSolution().getCodaList();
          if (aList.size() > 0) aList.clear();
          // load side-effect associates with sol, ok if mag stale since recalc:
          //mag.loadCodaList(); // old way
          mag.loadReadingList();
        }
      }
      else if ( mag.isAmpMag() ) {
        if (mag.getAmpList().size() <= 0) {
          System.out.println("Empty amp list, attempting load of magnitude amp list from db");
          // to avoid possible duplicates; replace equiv by time aww 10/29/2002
          AmpList aList = getSelectedSolution().getAmpList();
          if (aList.size() > 0) aList.clear();
          // load side-effect associates with sol, ok if mag stale since recalc:
          //mag.loadAmpList(); // old way
          mag.loadReadingList();
        }
        //System.out.println("JSEP.calcMag DEBUG mag.ampList.size(): "+mag.getAmpList().size()+"mag.sol: "+mag.sol.toString());
      }
    */

