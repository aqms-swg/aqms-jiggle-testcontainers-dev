//TODO: Implement a button option to clear Solution data lists to gc 
//      heap memory bloat after processing many events in editor still
//      on list.  Clear those where getNeedsCommit=false, else pop confirm
//      dialog to avoid accidently trashing data that should be commited
//      after user edits.
package org.trinet.util.graphics.table;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;
import org.trinet.jasi.TN.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;
import org.trinet.util.gazetteer.*;
import org.trinet.util.gazetteer.TN.*;
import org.trinet.util.graphics.*;
import org.trinet.util.graphics.text.*;

/** Displays a GenericSolutionList in a JTable in a JPanel and allows table data to be sent to database.
*/
public class JasiSolutionListPanel extends AbstractJasiListPanel implements ListSelectionListener {
    protected JasiSolutionAssocEditorPanel jsaePanel = null;
    protected static WhereIsEngine whereEngine = null;
    //private JOptionPane jopJSAEP = null;
    private JDialog assocDataDialog = null;
    //private Dialog assocDataDialog = null;
    private boolean autoCloseDialogs = true; // by default close open dialog upon row selection change
    private JButton jbJSAEP = null;
    private JButton jbWheres = null;
    private JButton jbCommitAll = null;
    private HypoMagEngineDelegateIF engineDelegate = null;

    /*
      protected String [] defaultColumnOrderNames = { "ID",
            "DATETIME", "LAT", "LON", "MAG", "MTYP", "MMETH", "Z", "AUTH", "SRC", "GAP", "DIST",
            "RMS", "ERR_T", "ERR_H", "ERR_Z", "OBS", "USED", "S", "FM", "Q", "V",
            "ETYPE", "PFLG", "ZF", "HF", "TF", "WRECS", "PR", "RMK" };

    */
/** Constructor sets defaults GenericSolutionList == null, JTextArea == null, updateDB == false.
* @param solutionList org.trinet.jasi.GenericSolutionList can be null.
* @param textArea javax.swing.JTextArea to which output is appended can be null.
* @param updateDB true == allows table edits and database updates.
*/
    public JasiSolutionListPanel() {this(null,false);}

    public JasiSolutionListPanel(JTextArea textArea, boolean updateDB) {
        super(textArea,updateDB);
        initPanel();
    }

    private MouseAdapter tblMouseListener = new MouseAdapter() {
        public void mousePressed(MouseEvent e) {
            if ( e.getClickCount() > 1) jbJSAEP.doClick(); 
        }
    };

    protected void createTable() {
        JTable tbl = null;
        if (dataTable != null) {
            //dataTable.getSelectionModel().removeListSelectionListener(this);
            tbl = dataTable.getRowHeader();
            if (tbl != null) tbl.removeMouseListener(tblMouseListener);
        }

        super.createTable();

        if (dataTable != null) {
            dataTable.getSelectionModel().addListSelectionListener(this); // if selection changes whatever
            tbl = dataTable.getRowHeader();
            if (tbl != null) tbl.addMouseListener(tblMouseListener);
        }
    }

    protected JMenuBar createMenuBar() { // added menu option 06/29/2004 -aww
        JMenuBar menuBar = super.createMenuBar();
        JMenu menu = menuBar.getMenu(1);
        JCheckBoxMenuItem cbMenuItem = 
          new JCheckBoxMenuItem("Auto Close Dialogs"); 
        cbMenuItem.setActionCommand("autoCloseDialogs");
        boolean tf = autoCloseDialogs; // this.dataTable.inputProperties.getBoolean("autoCloseDialogs");
        cbMenuItem.setSelected(tf);
        autoCloseDialogs = tf;
        cbMenuItem.addItemListener( new ItemListener() {
          public void itemStateChanged(ItemEvent e) {
            autoCloseDialogs = (e.getStateChange() == ItemEvent.SELECTED);
          }
        });
        menu.add(cbMenuItem);
        return menuBar;
    }

    // implement ListSelectionListener for above
    public void valueChanged(ListSelectionEvent e) {
      if (e.getValueIsAdjusting()) return;
      if (e.getFirstIndex() == e.getLastIndex()) return; // aww 10/7/2002?
      if (isListUpdateNeeded()) {
        Debug.println("Solution selection changed - updating assoc lists for prior selection?"); 
        updateAssocLists(true);
      }
      // aww 06/29/2004 force close of dialogs:
      if (autoCloseDialogs && assocDataDialog != null && assocDataDialog.isShowing()) assocDataDialog.dispose(); // aww test


      if (isCommitable()) {
        int selectedRowIdx = getSelectedRowIndex();
        if (selectedRowIdx >= 0 )
            aLog.logTextnl(" Your new selection: " + ((Solution)getSelectedRow()).toNeatString());
        int rowIdx = e.getFirstIndex();
        rowIdx = (selectedRowIdx == rowIdx) ?
            sortedToModelRowIndex(e.getLastIndex()) : sortedToModelRowIndex(rowIdx);
        if (rowIdx == -1) rowIdx = sortedToModelRowIndex(selectedRowIdx); // added 10/30/2002 aww
        if (rowIdx == -1) {
            aLog.logTextnl("No row selection");
            return;
        }

        //System.out.println("JSLP debug selectedRowIdx: " + selectedRowIdx + " rowIndex: " +
        //     rowIdx + " e: " + e.getFirstIndex() + " " + e.getLastIndex()  +
        //     " sorted: " + sortedToModelRowIndex(rowIdx));
        Solution oldSol = (Solution) getList().get(rowIdx);
        if (oldSol.getNeedsCommit()) { // commenting this out avoids hasChanged test 
        //if (oldSol.needsCommit) { // alternative to above avoids confirm when hasChanged==true 03/03 aww
            // added test to query only for single selections, not multiple choice - aww 02/09/2005
            aLog.logTextnl("Row selection changed, COMMIT is needed to save all changes to old selection: " +
                    oldSol.getId().longValue());
            if (
                 (dataTable.getSelectionModel().getSelectionMode() == ListSelectionModel.SINGLE_SELECTION) &&
                 commitConfirmed("Prior selection " + oldSol.getId().longValue() +
                                 " has changed data, commit it to database?")
               ) {

                // ? resolve row stale state here or in updateList() with assocList panel mag ?
                // 03/03 aww added extra forced solve here in case assocList update, has no bearing?
                doEngineSolve(oldSol);
                resolveUpdateResults(getModel().updateDB(rowIdx));
            }
        //}
        }
      }
    }
    protected void initPanel() {
        //if (this.tableColumnOrderByName == null) this.tableColumnOrderByName = JasiSolutionListTableConstants.columnNames;
        this.dataTable = new JasiSolutionListTable(new JasiSolutionListTableModel());
        //setCommitEnabled(true);
        //setUpdateDB(true);
        super.initPanel();
    }

    protected void addEngineDelegate(EngineDelegateIF delegate) {
        engineDelegate = (HypoMagEngineDelegateIF) delegate;
        if (jsaePanel != null) jsaePanel.addEngineDelegate(engineDelegate);
    }
    private void initJSAEP() {
        jsaePanel = new JasiSolutionAssocEditorPanel();
        jsaePanel.setUpdateEnabled(hasUpdateEnabled());
        jsaePanel.setCommitEnabled(hasCommitEnabled());
        jsaePanel.setCommitButtonEnabled(false);
        jsaePanel.setPanelLoadOptions(true, true);
        jsaePanel.setPanelNameLabel("");
        // messy until controls moved to standalone toolbar
        jsaePanel.addEngineDelegate(engineDelegate); // precedes initPanel
        //adds jsaePanel as listener to SolutionListEditorPanel see below. 
        // aww removed WaveMagEnabled true state on 07/14/2004
        jsaePanel.setWaveMagButtonEnabled(false); // allows waveform generation of a new mag, do b4 init here - aww 05/2004
        jsaePanel.initPanel();
        jsaePanel.setAutoSolve(autoSolve); // uses this SolutionPanel value, set when created -aww
        jsaePanel.getTextArea().setDocument(getTextArea().getDocument()); // shared text document for multiple textAreas 
        //if jsaePanel is not added listener to SolutionListEditorPanel then: 
        //jsaePanel.addPropertyChangeListener(EngineIF.SOLVED, this);
        jsaePanel.addPropertyChangeListener("trialLoc", this); // test 04/03 aww
        jsaePanel.addPropertyChangeListener("fixLoc", this); // test 04/03 aww
        jsaePanel.addPropertyChangeListener("magTableChanged", this); // test 03/24/2005 aww
        //jopJSAEP = new JOptionPane(jsaePanel, JOptionPane.PLAIN_MESSAGE, JOptionPane.DEFAULT_OPTION, null, null);
        //jsaePanel.jbMag.setEnabled(true); // done in JSEP property change listener method?
        jbCommit.setEnabled(commitButtonEnabled); // aww 2007/08/22
        jbCommitAll.setEnabled(commitButtonEnabled); // aww 2007/08/22
        firePropertyChange("hasAssocPanel", Boolean.FALSE, Boolean.TRUE);
    }
    
    protected JPanel createButtonPanel() {
        jbJSAEP = new JButton("Data");
        jbJSAEP.setToolTipText("Show data associated with selected solution.");
        jbJSAEP.setActionCommand("JSAEP");
        jbJSAEP.setMargin(inset);
        jbJSAEP.setEnabled(false);
        jbJSAEP.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
              Solution sol = (Solution)getSelectedRow();
              if (sol == null) {
                InfoDialog.informUser(getTopLevelAncestor(), "INFO",
                           "No solutions selected", statusLabel);
                return;
              }
              else { 
                if (jsaePanel == null) {
                    initJSAEP();
                }
                if (resolvePriorSolution(sol) || ! hasAssocPanelData(sol)) loadAssocPanelData(sol);
                if (jsaePanel.loadCanceled()) {
                  System.out.println("Load canceled");
                  return;
                }
                // 12/13/02 aww about modal selection on/off
                // what about get Frame ancestor owning toolbar
                // and making it the dialog's owner?
                createAssocPanelDialog(sol).setVisible(true); // made non-modal as per Kate request 06/29/2004
                // check if this is needed see WindowClosingListener:
            //?    informListeners(sol, false); // 1/10/03 aww added // removed 06/29/2004 aww
                //jopJSAEP.setValue(null); //resets pane after return
              }
            }
            private void informListeners(Solution sol, boolean solveWhenStale) {
                // after dialog returns, check for edits, if any signal
                if (sol.getNeedsCommit() || jsaePanel.hasModifiedTable()) {
                  // update now in lieu of doing this when selection changes
                  updateAssocLists(false);
                  int idx = sortedToModelRowIndex(getSelectedRowIndex());
                  //System.out.println("Firing rows updated : " + idx);
                  if (idx > -1) getModel().fireTableRowsUpdated(idx, idx);
                }
                // Could add solveWhenStale property test here, set through menu/properties,
                // to replace code below (always solves):
                // if (solveWhenStale) doEngineSolve(sol); // aww 06/29/2004 replaced by below
                if (autoSolve && solveWhenStale) doEngineSolve(sol); // aww require autoSolve 06/29/2004
            }
            private boolean hasAssocPanelData(Solution sol) {
              return ( (sol.getAmpList().size() + sol.getCodaList().size() +
                sol.getMagList().size() + sol.getPhaseList().size() +
                sol.getAlternateSolutions().size() ) > 0);
            }

            private boolean resolvePriorSolution(Solution sol) {
              Solution oldSol =  jsaePanel.getSelectedSolution();
              if (oldSol != null && sol != oldSol) { // should a forced reload from db null old?
                //aLog.logTextnl("JSLP DEBUG oldSol: " + oldSol.toString());
                //if (oldSol.getNeedsCommit() || jsaePanel.hasModifiedTable()) {
                  //aLog.logText("oldsol needsCommit: " + oldSol.getNeedsCommit());
                  //aLog.logTextnl(" jsaemodified: " + jsaePanel.hasModifiedTable());
                  updateAssocLists(true); // does a dialog to update; automate?
                //}
              }
              return ((sol != oldSol ) ? true : false);
            }
            private void loadAssocPanelData(Solution sol) {
              jsaePanel.setSelectedSolution(sol);
              jsaePanel.setPanelNameLabel("Solution id: " + sol.getId());
              jsaePanel.loadData();
            }
            private Dialog createAssocPanelDialog(final Solution sol) {
              if (assocDataDialog == null) {
                //assocDataDialog = jopJSAEP.createDialog(getTopLevelAncestor(), null);
                //assocDataDialog.getContentPane().add(jopJSAEP); // ok button at bottom takes up to much space
                //assocDataDialog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
                //removed below line and replace with logic below 6/03 aww
                //assocDataDialog = new Dialog((Frame)getTopLevelAncestor(), "", true);
                Container tla = getTopLevelAncestor();
                if (tla instanceof Frame) 
                  //assocDataDialog = new Dialog((Frame) tla, "" , true); // aww 06/29/2004 - kate wanted non-modal
                  assocDataDialog = new JDialog((Frame) tla, "" , false); // aww 06/29/2004 try non-modal, trouble though
                else {
                  //assocDataDialog = new Dialog((Dialog) tla, "" , true); // aww 06/29/2004
                  assocDataDialog = new JDialog((Dialog) tla, "" , false); // aww 06/29/2004 try non-modal, trouble though
                }
                assocDataDialog.setResizable(true);
                Rectangle bnds = getTopLevelAncestor().getBounds();
                // 12/15/02 aww init dialog at partial height
                //bnds.setSize(bnds.width,jsaePanel.PREFERRED_PANEL_HEIGHT + 100); // text panel + divider
                assocDataDialog.setBounds(bnds);
                //assocDataDialog.add(jsaePanel); // instead of optionPane
                assocDataDialog.getContentPane().add(jsaePanel); // instead of optionPane
                assocDataDialog.addWindowListener(new WindowAdapter() {
                  public void windowClosing(WindowEvent e) {
                    try {
                      informListeners(sol, true);
                    }
                    catch(Throwable t) {
                      t.printStackTrace();
                    }
                    finally {
                      assocDataDialog.setVisible(false);
                    }
                  }
                });
              }
              //assocDataDialog.setTitle("Data for solution id:" + sol.getId());
              assocDataDialog.setTitle(sol.toNeatString()+ " " + sol.toShortErrorString());
              if (jsaePanel.magPanel != null && jsaePanel.magPanel.getRowCount() == 1) {
                jsaePanel.magPanel.setSelectedRow(0);
              }
              return assocDataDialog;
            }
            private void printDebug(Solution sol) {
              aLog.logTextnl("DEBUG JSLP b4 sol: " + sol.toString());
              aLog.logTextnl("fromDbase: " + ((SolutionTN) sol).isFromDataSource());
              aLog.logTextnl("isStale: " + ((SolutionTN) sol).isStale());
              aLog.logTextnl("hasChanged: " + ((SolutionTN) sol).hasChanged());
              aLog.logTextnl("needsCommit: " + ((SolutionTN) sol).getNeedsCommit());
              aLog.logTextnl("magnitude.hasChanged: " + sol.magnitude.hasChanged());
            }
        });

        jbWheres = new JButton("Where");
        jbWheres.setToolTipText("Report closest to selected row in table.");
        jbWheres.setActionCommand("Where");
        jbWheres.setMargin(inset);
        jbWheres.setEnabled(false);
        jbWheres.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                int rowid = getSelectedRowIndex();
                if (rowid >= 0) {
                    rowid = sortedToModelRowIndex(rowid);
                    JasiSolutionListTableModel model =
                        (JasiSolutionListTableModel) getModel();
                    double lat = ((DataObject) model.getValueAt(rowid, model.LAT)).doubleValue();
                    double lon = ((DataObject) model.getValueAt(rowid, model.LON)).doubleValue();
                    long id = ((DataObject) model.getValueAt(rowid, model.ID)).longValue();
                    Connection conn = DataSource.getConnection();
                    if (conn != null) {
                        if (whereEngine == null) {
                          whereEngine = WhereIsEngine.create();
                        }
                        whereEngine.setConnection(conn);
                        String whereString = whereEngine.where(lat, lon);
                        aLog.logTextnl("JasiSolutionListPanel Where : " + id + "\n" + whereString);
                        JOptionPane.showMessageDialog(getTopLevelAncestor(),
                                 whereString, "Where", JOptionPane.PLAIN_MESSAGE);
                    }
                }
                else {
                        JOptionPane.showMessageDialog(getTopLevelAncestor(),
                                 "No selected solution row.", "Where", JOptionPane.PLAIN_MESSAGE);
                        return;
                }
            }
        });

        // Added CommitAll button - aww 02/09/2005
        jbCommitAll = new JButton("CommitAll");
        jbCommitAll.setToolTipText("Commit all rows in catalog to database.");
        jbCommitAll.setActionCommand("CommitAll");
        jbCommitAll.setMargin(inset);
        jbCommitAll.setEnabled(false);
        jbCommitAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if (dataTable == null) return;
                if (! isCommitable() ) {
                    InfoDialog.informUser(getParent(), "ERROR: "+ panelName + " is NOT commitable", null);
                }
                else if ( commitConfirmed("Commit to database ALL rows in Solution catalog?") ) {
                    aLog.logTextnl("Commit all database update count: " + dataTable.updateDB());
                }
            }
        });

        JPanel aPanel = super.createButtonPanel();
        aPanel.add(jbCommitAll);
        aPanel.add(jbWheres);
        aPanel.add(jbJSAEP);
        return aPanel;
    }
    protected boolean canCommit() {
        boolean status = super.canCommit();
        if (! status) return false;
        // moved prompt here to catch case of solution needing commit -aww
        Solution sol = (Solution) getSelectedRow();
        if (sol == null)  {
          //InfoDialog.informUser(getParent(), "ERROR", "No Solution rows selected to commit", null);
          System.out.println("INFO: No selected Solution to commit.");
          return false;
        }
        if (sol.getNeedsCommit() || hasModifiedTable()) { // Verify all commits (in case of accidently pressed button)
          //System.out.println("canCommit? "+sol.getSummaryStateString(sol,sol.magnitude)+"hasModifiedTable: "+hasModifiedTable());
          status = commitConfirmed("Commit modified Solution");
          if (status && isStale()) status = commitConfirmed("Commit STALE (solution,magnitude) data");
        }
        else { 
          aLog.logTextnl("Selected Solution table is unchanged, no need to commit.");
          //InfoDialog.informUser(this, "INFO", panelName+": Unchanged Solution data, commit cancelled.", statusLabel);
          status = false; // 04/08/2007 -aww
        }
        if (status && sol.getPhaseList().size() == 0)  { // 2007/08/22 -aww
            if (JOptionPane.YES_OPTION !=
                   JOptionPane.showConfirmDialog(this,
                       "Commitable event has NO associated picks (? db data NOT loaded?), Do you still want to commit?", 
                       "EVENT ORIGIN IS MISSING ASSOCIATED PHASE DATA",
                       JOptionPane.YES_NO_OPTION,
                       JOptionPane.QUESTION_MESSAGE) ) status = false;
        }
        return status;
    }
    // commit from the top of the solution heirarchy
    public int commit() {
      long id = getSelectedId(); // aww 07/14/2004 for kate;
      if (jsaePanel != null) jsaePanel.commit();
      int count = super.commit();
      setSelectedId(id); // aww 07/14/2004 for kate;
      return count;
    }
    protected int updateDB() {
        if (! canUpdateDB()) return -1;
        int nrow = updateDBSolutions(); // override default dataTable.updateDB() in super
        resolveUpdateResults(nrow);
        return nrow;
    }
    private int updateDBSolutions() {
      // always commit any selected rows
      int [] selectedTableRowsIndices = getSelectedRowsIndices();
      boolean status = true;
      if (selectedTableRowsIndices == null || selectedTableRowsIndices.length == 0) {
          // no selections, prompt for action; either commit ALL, or abort to force selection
          // status = rejectAction("No Solution rows selected, commit ALL Solutions with data modifications?");
          InfoDialog.informUser(getParent(), "ERROR", "No Solution rows selected to commit", null);
          return (status) ?  0 : dataTable.updateDB(); // rejected or not updateDB all rows
      }
      /*
      // now check for solutions unselected but needing commit, insurance for lazy users?
      status = hasModifiedDataBeenSelected(getSelectedRows(),true);
      if (! status) {
        status = rejectAction("Some unselected Solutions have modified data, commit these rows?"); 
      } 
      */
      return (status) ? selectiveUpdateDB(selectedTableRowsIndices): dataTable.updateDB(); 
    }
    private int selectiveUpdateDB(int [] selectedTableRowsIndices) {
      int selectedCount = selectedTableRowsIndices.length;
      int updateCount = 0;
      for (int idx=0; idx<selectedCount; idx++) {
        updateCount += dataTable.updateDB(selectedTableRowsIndices[idx]);
        //System.out.println("JSLP DEBUG selective update row idx: "+selectedTableRows[idx]+" commited: "+(updateCount>0));
      }
      return updateCount;
    }

    protected void showContentButtons() {
        super.showContentButtons();
        //jbCommitAll.setVisible(true); // aww 2007/08/22
        jbWheres.setVisible(true);
        jbJSAEP.setVisible(true);
    }

    protected void hideContentButtons() {
        super.hideContentButtons();
        //jbCommitAll.setVisible(false); // aww 2007/08/22
        jbWheres.setVisible(false);
        jbJSAEP.setVisible(false);
    }

    protected void enableModifyButtons() {
        super.enableModifyButtons();
        jbCommit.setEnabled(! hasNullAssocPanel()); // aww 2007/08/22
        jbCommitAll.setEnabled(! hasNullAssocPanel()); // aww 2007/08/22
    }

    protected void disableModifyButtons() {
        super.disableModifyButtons();
        jbCommit.setEnabled(false); // aww 2007/08/22
        jbCommitAll.setEnabled(false); // aww 2007/08/22
    }

    protected void showModifyButtons() {
        super.showModifyButtons();
        jbCommitAll.setVisible(true);
    }
    protected void hideModifyButtons() {
        super.hideModifyButtons();
        jbCommitAll.setVisible(false);
    }

    protected void enableContentButtons() {
        super.enableContentButtons();
        //jbCommitAll.setEnabled(true); // aww 2007/08/22
        jbWheres.setEnabled(true);
        jbJSAEP.setEnabled(true);
    }
    protected void disableContentButtons() {
        super.disableContentButtons();
        //jbCommitAll.setEnabled(false);
        jbWheres.setEnabled(false);
        jbJSAEP.setEnabled(false);
    }
    protected boolean insertRow() { 
        Solution newSol = Solution.create();
        getModel().initRowValues(newSol);
        return insertRow(newSol);
    }
    // Could make solveWhenStale an optional property set through menu/properties
    protected void updateAssocLists(boolean solveWhenStale) {
      //System.out.println("DEBUG JSLP updateAssocLists hasModifiedAssocPanel(): "
      //   + hasModifiedAssocPanel() + " solveWhenStale: " + solveWhenStale);
      if (hasModifiedAssocPanel()) {
         // current selected Solution of this panel may not be that assigned to the assocPanels
        jsaePanel.updateLists(false); // 12/17/2002 prevent recursive solve - aww
        //if (solveWhenStale) jsaePanel.doEngineSolve(); // aww 06/29/2004 replaced by below
        if (autoSolve && solveWhenStale) jsaePanel.doEngineSolve(); // require autoSolve aww 06/29/2004
      }
      // ? resolve current selectedRow stale state here, or when selection changes, if sol not that of assocPanel?
    }
    protected int updateList() {
      return updateLists(true);
    }
    protected int updateLists(boolean solveWhenStale) {
      updateAssocLists(solveWhenStale);
      // resolve selectedRow stale state here, in method above, or when selection changes?
      return super.updateList();
    }
    /*
    protected int updateList() {
      if (! confirmUpdate()) return 0;
      return updateLists(true);
    }
    protected int updateLists(boolean solveWhenStale) {
      updateAssocLists(solveWhenStale);
      return synchInputList();
    }
    */
    protected int synchInputList() {
      // This method is invoked by input text parser after doing a list.addOrReplace(row) 
      int status = super.synchInputList();
      GenericSolutionList aList = (GenericSolutionList) getList();
      // Force sort here:
      // May effect behavior of panel Sort and Insert button row actions?
      aList.sortByTime(); // added test sort here for kate - aww 02/08/2005 
      return status;
    }
    /*
    protected int synchInputList() {
      Debug.println("DEBUG updating panel list for " + panelName);
      if (! isListUpdateNeeded()) {
        setListUpdateNeeded(false);
        return 0;
      }
      GenericSolutionList aList = (GenericSolutionList) getList();
      if (inputList == aList) return 0;
      for (int idx=0; idx < aList.size(); idx++){
        Solution sol = aList.getSolution(idx);
        // could use addOrReplace(sol) logic here instead if sol was "cloned"
        boolean retVal = inputList.add(sol);
      }
      setListUpdateNeeded(false);
      return aList.size();
    }
    */

    protected void stripRows() {
      if (jopStrip == null) jopStrip = new StripOptionPane();
      jopStrip.presentDialog();
      String choiceStr = jopStrip.getSelectionString();
      if (choiceStr.equals("RMS") ) {
        double rmsCut = jopStrip.getRMSCutoffValue();
        aLog.logText("rmsCutoff: " +rmsCut+ " for " + panelName);
        // need strip methods to return counts for setting modified state
        ((JasiSolutionListTableModel) getModel()).stripByResidual(rmsCut);
      }
    }

    boolean autoSolve = false; // optional panel property, but not a menu setting yet
    public void setAutoSolve(boolean tf) { autoSolve = tf;}
    public boolean isAutoSolve() { return autoSolve;}

    protected boolean hasNullAssocPanel() {
         return (jsaePanel == null);
    }
 
    protected boolean hasModifiedAssocPanel() {
        return (jsaePanel != null) ? jsaePanel.hasModifiedTable() : false;
    }

    public boolean isStale() {
      // are any selected rows on list stale?
      JasiCommitableIF []  anArray = getSelectedRows();
      int selectedCount = (anArray == null) ? 0 : anArray.length;
      if (selectedCount == 0) {
        // use below if total table commit is enabled by updateDBSolutions():
        //return hasStaleSolutionOrPrefMag();
        return false;
      }
      // Look for data needing engine work:
      Solution sol = null;
      Magnitude pMag = null;
      for (int idx=0; idx < selectedCount; idx++) {
        sol = (Solution) anArray[idx]; 
        pMag = sol.getPreferredMagnitude();
        //if ( sol.isStale() || ((pMag == null) ? false : (pMag.isStale() && !pMag.algorithm.equalsValue("HAND")) )) // HAND check -aww 2008/03/19
        // above doesn't remind user when both mag and associated amps are HAND entered but not solved for new value, like a HAND entered Ml 
        if ( sol.isStale() || ((pMag == null) ? false : pMag.isStale()))
            return true;
      }
      return false;
    }
    protected void setStale(boolean tf) {
      // only to force synch a cloned model list
      if (tf) setListUpdateNeeded(tf);
    }

    public boolean hasStaleSolutionOrPrefMag() {
      GenericSolutionList modelList = (GenericSolutionList) getList();
      if (modelList == null) return false;
      boolean retVal = false;
      for (int idx=0; idx < modelList.size(); idx++){
        Solution sol = (Solution) modelList.get(idx);
        if (sol.isStale() || sol.getPreferredMagnitude().isStale()) {
          retVal = true; 
          //System.err.println("Stale sol: " + sol.toNeatString());
          break;
        }
      }
      return retVal;
    }
    protected long getSelectedId() {
       Solution sol = (Solution) getSelectedRow();
       // added feedback of selection to inputlist, but is it necessary?
       if (inputList != null && (inputList instanceof SolutionList)) {
         ((SolutionList)inputList).setSelected(sol);
       }
       return (sol == null) ? 0l : sol.getId().longValue();
    }
    protected void setSelectedId(long id) {
       Solution [] solArray =
          (Solution [] ) ((GenericSolutionList) getList()).getArray();
       for (int idx = 0; idx < solArray.length; idx++) {
           if (solArray[idx].getId().longValue() == id) {
               setSelectedRow(modelToSortedRowIndex(idx));
               // added feedback of selection to inputlist, but is it necessary?
               if (inputList != null && (inputList instanceof SolutionList)) {
                 ((SolutionList)inputList).setSelected(solArray[idx]);
               }
               break;
           }
       }
    }
    protected boolean fetchById(long id) {
        Solution sol = Solution.create().getById(id);
        if (sol == null) {
            aLog.logTextnl("JasiSolutionListPanel : No solution found in database for input id");
            return false;
        }
        /* add to model, not to inputList
        boolean retVal = ((GenericSolutionList) getList()).add(sol);
        setSelectedRow(sol);
        return retVal;
        */
        boolean modified = hasModifiedTable(); // save save before load from db
        boolean status = insertRow(sol);
        updateStatus();
        // Table thread should unset table modified if new, else
        // fromDb so no need to force commit yet? 03/03 aww?
        if (! modified) unsetTableModified();
        return status;
    }
    protected ActionListener createSaveTableActionListener() {
      return
        new AbstractJasiListPanel.SaveTableActionListener() {
          public void actionPerformed(ActionEvent evt) {
            statusLabel.setText(" ");
            if (getTable().getRowCount() <= 0)  {
              InfoDialog.informUser(getParent(), "ERROR",
                  panelName + ": No rows in table to save.",
                  statusLabel);
              return;
            }

            boolean success =
              saveToTextFile.save(JasiSolutionListPanel.this, getFrame());
            System.out.println("File write for " + panelName);
            if (success) {
              if (saveToTextFile.cascade() && jsaePanel != null) {
                long solId = getSelectedId();
                if (solId > 0) {
                  Solution assocSol =  jsaePanel.getSelectedSolution();
                  System.out.println("id assoc:" +assocSol.getId().longValue()+  
                                  " selected: " +solId);
                  if (assocSol.getId().longValue() == solId) {
                    success = saveAssocDataToFile();
                  }
                }
              }
            }
            else {
              InfoDialog.informUser(getParent(), "ERROR",
                panelName + ": Save to file FAILURE - see output text.",
                statusLabel);
            }
          }
          private boolean saveAssocDataToFile() {
            boolean success = true;
            if (jsaePanel.phasePanel != null) {
              System.out.println("writing phases ...");
              success =
                success && saveToTextFile.save(jsaePanel.phasePanel,getFrame(),false);
            }
            if (jsaePanel.magPanel != null) {
              success =
                success && saveToTextFile.save(jsaePanel.magPanel,getFrame(), false);
              if (jsaePanel.magPanel.jmaePanel != null) {
                System.out.println("writing mags ...");
                if (jsaePanel.magPanel.jmaePanel.ampPanel != null) {
                  System.out.println("writing mag amps ...");
                  success =
                    success && saveToTextFile.save(jsaePanel.magPanel.jmaePanel.ampPanel,getFrame(), false);
                }
                if (jsaePanel.magPanel.jmaePanel.codaPanel != null) {
                  System.out.println("writing mag codas ...");
                  success =
                    success && saveToTextFile.save(jsaePanel.magPanel.jmaePanel.codaPanel,getFrame(), false);
                }
              }
            }
            System.out.println("File write DONE status: " + success); 
            return success;
          }
      };
    }

// override PropertyChangeListener method in parent
    public void propertyChange(PropertyChangeEvent e) {
        debugProperty(e);
        String propName = e.getPropertyName();
        if (propName.equals("trialLoc") ||
            propName.equals("fixLoc") ) {
          int modelRowIdx = getSelectedRowModelIndex();
          if (modelRowIdx < 0) return;
          getModel().fireTableRowsUpdated(modelRowIdx,modelRowIdx);
        }
        else if (propName.equals(EngineIF.SOLVED) || propName.equals("magTableChanged")) {
          if (dataTable.getRowCount() <= 0) return;
          Object newValue = e.getNewValue();
          Solution solvedSol = null;
          if (newValue instanceof Solution) {
            solvedSol = (Solution) newValue;
            /* 02/03 Trial setMagnitude stale that depend on solution solved
            // As shortcut same logic put in Solution class setStale
            // prior to solving for new location.
            MagList magList = solvedSol.getMagList();
            for (int idx=0;idx<magList.size(); idx++) {
               Magnitude mag = (Magnitude) magList.get(idx);
               if (mag.dependsOnOrigin()) mag.setStale(true);
            } 
            */
          }
          else if (newValue instanceof Magnitude) {
            solvedSol = ((Magnitude) newValue).getAssociatedSolution();
          }
          if (solvedSol == null) {
            System.err.println(panelName+" : Warning wasSolved null result");
            return;
          }
          if (assocDataDialog != null) {
            if (jsaePanel.getSelectedSolution() == solvedSol)
              assocDataDialog.setTitle(solvedSol.toNeatString()+ " " + solvedSol.toShortErrorString());
          }
          int tableRowIdx = getRowIndexOf(solvedSol);
          if (tableRowIdx < 0) return;
          int modelRowIdx = sortedToModelRowIndex(tableRowIdx);
          getModel().fireTableRowsUpdated(modelRowIdx,modelRowIdx);
          // above could be listened for by model listener to do reset below:
          dataTable.resetTableCellWidths(tableRowIdx);
        }
    }

    protected void doEngineSolve(Solution sol) {

        if (engineDelegate == null) return;
        //
        // Assumes all of sol's prefmags were loaded, pathology occurs if sol's LatLonZ was changed
        // and associated panel data were not loaded, then origin dependent prefmags in db would be
        // stale and associated with this sol's prior orid (those premags would not be updated here).
        //
        boolean stalePrefMag = sol.hasStalePrefMag();

        if (sol.isStale() || stalePrefMag ) {

            if (! isAutoSolve() &&
                rejectAction("Solve for location/magnitude using updated data for id: " +
                    sol.getId().longValue()) ) {
                        return;
            }

            String [] magTypes =
                ((HypoMagDelegatePropertyList) engineDelegate.getDelegateProperties()).getMagMethodTypes(); // ???
            if (magTypes.length == 0 && stalePrefMag) {
              InfoDialog.informUser(getTopLevelAncestor(), "INFO",
                  "Stale prefmag and no delegate magnitude methods defined!" , statusLabel);
            }

            // First solve for new location
            if (sol.isStale()) {
                engineDelegate.solve(sol, null);
            }

            if (stalePrefMag) { // solve for new magnitude if stale
                Magnitude mag = null;
                for (int i = 0; i<magTypes.length; i++) { 
                    mag = sol.getPrefMagOfType(magTypes[i]);
                    if (mag == null || mag.hasNullValue() || ! mag.isStale()) continue;

                    engineDelegate.solve(sol, mag); // do a sudden-death boolean test for break here? 
                }
            }
        }

        /* Previous code only does update of the sol's event preferred magnitude
        Magnitude prefMag = sol.getPreferredMagnitude();
        if (sol.isStale() || (prefMag != null && prefMag.isStale()) ) {
          if (! isAutoSolve() &&
              rejectAction("Solve for solution using updated list data for id: " + sol.getId().longValue()) ) {
                  return;
          }
          // since changing solution origin, will change mag usually:
          if (prefMag != null && prefMag.dependsOnOrigin()) engineDelegate.solve(sol, prefMag);
          else {
             engineDelegate.solve(sol, null);
             aLog.logTextnl("Prefmag is null or does not depend on Solution origin.");
          }
        }
        */
    }
} // end of JasiSolutionListPanel class
