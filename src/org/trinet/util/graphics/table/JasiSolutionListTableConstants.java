package org.trinet.util.graphics.table;
import org.trinet.jasi.*;
import org.trinet.jdbc.datatypes.*;

/** Define constants used by a SolutionListTableModel. These are the columns in a
 *  JTable that contains a SolutionList. */
public interface JasiSolutionListTableConstants {
    public static final int MAX_FIELDS         = 59;
    public static final int ID                 = 0;
    public static final int EXTERNALID         = 1;
    public static final int DATETIME           = 2;
    public static final int LAT                = 3;
    public static final int LON                = 4;
    public static final int DEPTH              = 5;
    public static final int HORIZDATUM         = 6;
    public static final int VERTDATUM          = 7;
    public static final int TYPE               = 8;
    public static final int METHOD             = 9;
    public static final int CRUSTMODEL         = 10;
    public static final int VELMODEL           = 11;
    public static final int AUTHORITY          = 12;
    public static final int SOURCE             = 13;
    public static final int GAP                = 14;
    public static final int DISTANCE           = 15;
    public static final int RMS                = 16;
    public static final int ERRORTIME          = 17;
    public static final int ERRORHORIZ         = 18;
    public static final int ERRORVERT          = 19;
    public static final int ERRORLAT           = 20;
    public static final int ERRORLON           = 21;
    public static final int TOTALREADINGS      = 22;
    public static final int USEDREADINGS       = 23;
    public static final int SREADINGS          = 24;
    public static final int FIRSTMOTIONS       = 25;
    public static final int QUALITY            = 26;
    public static final int VALIDFLAG          = 27;
    public static final int EVENTTYPE          = 28;
    public static final int PROCESSING         = 29;
    public static final int DEPTHFIXED         = 30;
    public static final int LOCATIONFIXED      = 31;
    public static final int TIMEFIXED          = 32;
    public static final int WAVERECORDS        = 33;
    public static final int MAGNITUDE          = 34;
    public static final int PRIORITY           = 35; 
    public static final int MAGTYPE            = 36;
    public static final int COMMENT            = 37;
    public static final int DELETE             = 38;
    public static final int STALE              = 39;
    public static final int BOGUS              = 40;
    public static final int PREFOR             = 41;
    public static final int PREFMAG            = 42;
    public static final int PREFMEC            = 43;
    public static final int COMMID             = 44;
    public static final int ORID               = 45;
    public static final int EAUTH              = 46;
    public static final int ESRC               = 47;
    public static final int VERSION            = 48;
    public static final int OWHO               = 49;
    public static final int MWHO               = 50;
    public static final int MAGALGO            = 51;
    public static final int MAGOBS             = 52;
    public static final int MAGSTA             = 53;
    public static final int LDDATE             = 54;
    public static final int CMODNAME           = 55;  // added by aww 03/16/15
    public static final int CMODTYPE           = 56;  // added by aww 03/16/15
    public static final int MDEPTH             = 57;  // added by aww 03/16/15
    public static final int GTYPE              = 58;  // added by aww 03/16/15
    /* Note TableModel must match getValue(col) return types to types declared here */
    public static final Class [] columnClasses = {
        DataLong.class, DataString.class, DataDouble.class, DataDouble.class, DataDouble.class,
        DataDouble.class, DataString.class, DataString.class, DataString.class, DataString.class,
        DataString.class, DataString.class, DataString.class, DataString.class, DataDouble.class,
        DataDouble.class, DataDouble.class, DataDouble.class, DataDouble.class, DataDouble.class,
        DataDouble.class, DataDouble.class, DataLong.class, DataLong.class, DataLong.class,
        DataLong.class, DataDouble.class, Boolean.class, String.class, DataString.class,
        DataBoolean.class, DataBoolean.class, DataBoolean.class, DataLong.class, DataDouble.class,
        DataDouble.class, String.class, String.class, Boolean.class, Boolean.class,
        Boolean.class, DataLong.class, DataLong.class, DataLong.class, DataLong.class,
        DataLong.class, DataString.class, DataString.class, DataLong.class,DataString.class,
        DataString.class, String.class, DataLong.class, DataLong.class, DataString.class,
        DataString.class, DataString.class, DataDouble.class, DataString.class
    };

    public static final int [] columnFractionDigits = {
        0, 0, 2, 5, 5,
        2, 0, 0, 0, 0,
        0, 0, 0, 0, 1,
        2, 3, 4, 3, 3,
        5, 5, 0, 0, 0,
        0, 2, 0, 0, 0,
        0, 0, 0, 0, 2,
        1, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 2, 0
    };

        // "ZF tf 1,0","HF tf 1,0","TF tf 1,0",
        // "V validFlag=select binary", "D dummyFlag=bogus binary",
        // possible consider: altSolList.size,altMagList.size 
    public static final String [] columnNames = {
        "ID", "EXT_I", "DATETIME", "LAT", "LON",
        "Z", "HDAT", "VDAT", "TYPE", "METHOD",
        "CM", "VM", "AU", "SRC", "GAP",
        "DIST", "RMS", "ERR_T", "ERR_H", "ERR_Z",
        "ERR_LAT", "ERR_LON", "OBS", "USED", "S",
        "FM", "Q", "V", "ETYPE", "PFLG",
        "ZF", "HF", "TF", "WRECS", "MAG",
        "PR", "MT", "RMK", "DFLG", "STALE",
        "BFLG","PREFOR","MAGID","MECID","COMMID",
        "ORID", "EAU", "ESRC","VERS","OWHO",
        "MWHO", "MMETH","MOBS","MSTA","LDDATE"
    };

    public static final boolean [] columnAKey = {
        true, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false, false,
        false, false, false, false
    };

    public static final boolean [] columnEditable = {
        false, false, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, false, true,
        false, true, true, true, true,
        true, false, false, false, false,
        false, true, true, false, true,
        true, true, true, true, false,
        true, true, true, true
    };

    public static final boolean [] showColumn = {
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, true, true
    };
 
    public static final boolean [] columnNullable = {
        false, true, false, false, false,
        true, true, true, false, true,
        true, true, false, true, true,
        true, true, true, true, true,
        true, true, true, true, true,
        true, true, false, false, false,
        false, false, false, true, true,
        true, true, true, false, false,
        false, true, true, true, true,
        true, true, false, true, true, true,
        true, true, true, true, false,
        true, true, true, true
    };

    public static final int [] cellWidthPadding = {
        0, 0, 0, 0, 0,
        0, 0, 0, 10, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 10, 0,
        0, 0, 0, 0, 0,
        0, 10, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 10
    };
}
