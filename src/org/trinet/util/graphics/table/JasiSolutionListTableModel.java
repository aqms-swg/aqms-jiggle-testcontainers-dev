package org.trinet.util.graphics.table;
import java.util.*;
import javax.swing.event.*;
import org.trinet.jasi.*;
import org.trinet.jasi.TN.*;
import org.trinet.jdbc.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.*;

/** Model used to represent a list of jasi Solution objects (GenericSolutionList)
 *  in a JTable. */
public class JasiSolutionListTableModel extends AbstractJasiListTableModel
             implements JasiSolutionListTableConstants {

    private static JasiMagListTableModel jmtb = null;

    public JasiSolutionListTableModel() {
        super();
        maxFields = JasiSolutionListTableConstants.MAX_FIELDS;
        deleteColumnIndex = JasiSolutionListTableConstants.DELETE;
        tableColumnNames = JasiSolutionListTableConstants.columnNames; 
        tableColumnClasses = JasiSolutionListTableConstants.columnClasses; 
        tableColumnFractionDigits = JasiSolutionListTableConstants.columnFractionDigits;
        tableColumnAKey = JasiSolutionListTableConstants.columnAKey;
        tableColumnEditable = JasiSolutionListTableConstants.columnEditable;
        tableColumnNullable = JasiSolutionListTableConstants.columnNullable;
        tableColumnShown = JasiSolutionListTableConstants.showColumn;
        tableColumnCellWidthPadding = JasiSolutionListTableConstants.cellWidthPadding;
        EventTypeMap3.getMap(); // init map values possibly
    }


    public JasiSolutionListTableModel(GenericSolutionList solutionList) {
        this(solutionList, false);
    }

    public JasiSolutionListTableModel(GenericSolutionList solutionList, boolean cellEditable) {
        super(solutionList,cellEditable);
    }

    public void createList() {
        if (modelList != null)
            ((GenericSolutionList) modelList).fetchByProperties();
        //fireTableDataChanged(); // aww 1/03?
    }

    protected Solution getSolution(int rowIndex) {
        return (Solution) getJasiCommitable(rowIndex);
    }

    public Object getValueAt(int rowIndex, int colIndex) {
        Solution sl = getSolution(rowIndex);
        switch (colIndex) {
           case ID:
                return sl.id;
           case EXTERNALID:
                return sl.externalId;
           case DATETIME:
                return sl.datetime;
           case LAT:
                return sl.lat;
           case LON:
                return sl.lon;
           case DEPTH:
                return sl.depth;
           case MDEPTH:
                return sl.mdepth;
           case HORIZDATUM:
                return sl.horizDatum;
           case VERTDATUM:
                return sl.vertDatum;
           case TYPE:
                //"H"=hypocenter,"C"=centroid,"A"=amplitude
                return sl.type;
           case METHOD:
                return sl.algorithm;
           case CRUSTMODEL:
                return sl.crustModel;
           case VELMODEL:
                return sl.velModel;
           case EAUTH:
                return sl.eventAuthority;
           case ESRC:
                return sl.eventSource;
           case AUTHORITY:
                return sl.authority;
           case SOURCE:
                return sl.source;
           case GAP:
                return sl.gap;
           case DISTANCE:
                return sl.distance;
           case RMS:
                return sl.rms;
           case ERRORTIME:
                return sl.errorTime;
           case ERRORHORIZ:
                return sl.errorHoriz;
           case ERRORVERT:
                return sl.errorVert;
           case ERRORLAT:
                return sl.errorLat;
           case ERRORLON:
                return sl.errorLon;
           case TOTALREADINGS:
                return sl.totalReadings;
           case USEDREADINGS:
                return sl.usedReadings;
           case SREADINGS:
                return sl.sReadings;
           case FIRSTMOTIONS:
                return sl.firstMotions;
           case QUALITY:
                return sl.quality;
           case VALIDFLAG:
                return (sl.validFlag.longValue() == 1l) ? Boolean.TRUE: Boolean.FALSE;
           case EVENTTYPE:
                return sl.eventType.toString();
           case GTYPE:
                return sl.gtype;
           case PROCESSING:
                return sl.processingState;
           case DEPTHFIXED:
                return sl.depthFixed;
           case LOCATIONFIXED:
                return sl.locationFixed;
           case TIMEFIXED:
                return sl.timeFixed;
           case WAVERECORDS:
                sl.countWaveforms(); // aww 02/17/2005 test
                return sl.waveRecords;
           case MAGNITUDE:
                return (sl.magnitude == null) ? NULL_DATADOUBLE : sl.magnitude.value;
           case MAGTYPE:
                return (sl.magnitude == null) ? null : sl.magnitude.getTypeString();
           case MAGALGO:
                return (sl.magnitude == null) ? null : sl.magnitude.algorithm.toString();
           case MAGOBS: // returns String
                return (sl.magnitude == null)  ? NULL_DATALONG : sl.magnitude.usedChnls;
           case MAGSTA: // returns String
                return (sl.magnitude == null)  ? NULL_DATALONG : sl.magnitude.usedStations;
           case PRIORITY:
                return sl.priority;
           case COMMENT:
                return sl.getComment();
           case DELETE :
                return (sl.isDeleted()) ? Boolean.TRUE : Boolean.FALSE;
           case STALE :
                return (sl.isStale()) ? Boolean.TRUE : Boolean.FALSE;
           case BOGUS :
                return (sl.dummyFlag.longValue() == 1l) ? Boolean.TRUE: Boolean.FALSE;
           case PREFOR:
                long id = 0l;
                if (sl instanceof DbOriginAssocIF) {
                  DataLong dl = (DataLong) sl.getPreferredOriginId();;
                  id = (dl.isNull()) ? 0l : dl.longValue();
                }
                return (id > 0l) ?  new DataLong(id) : NULL_DATALONG;
           case PREFMAG:
                id = 0l;
                if (sl instanceof DbMagnitudeAssocIF) {
                  id = ((DbMagnitudeAssocIF) sl).getMagidValue();
                  return (id > 0l) ?  new DataLong(id) : NULL_DATALONG;
                } else {
                  return
                    (sl.magnitude != null) ? sl.magnitude.magid : NULL_DATALONG;
                }
           case PREFMEC:
                // not implemented in IF yet like DbEventAssocIF
                id = 0l;
                if (sl instanceof SolutionTN) {
                  id = ((SolutionTN)sl).getPrefmecValue();
                }
                return (id > 0l) ?  new DataLong(id) : NULL_DATALONG;
           case COMMID:
                // not implemented in IF yet like DbEventAssocIF
                id = 0l;
                if (sl instanceof SolutionTN) {
                  id = ((SolutionTN)sl).getCommidValue();
                }
                return (id > 0l) ?  new DataLong(id) : NULL_DATALONG;
           case ORID:
                id = 0l;
                if (sl instanceof DbOriginAssocIF) {
                  id = ((DbOriginAssocIF)sl).getOridValue();
                }
                return (id > 0l) ?  new DataLong(id) : NULL_DATALONG;
           case VERSION:
                //id = sl.getVersion();
                id = sl.getSourceVersion();
                return (id > -1) ?  new DataLong(id) : NULL_DATALONG;
           case OWHO:
                return sl.getWho();
           case MWHO:
                return (sl.magnitude == null) ? NULL_DATASTRING : sl.magnitude.getWho();
           case LDDATE:
                return new DataString(sl.getTimeStamp().toDateString().substring(0,19));
           default:
                return null;
        }
    }

    public boolean isCellEditable(int rowIndex, int colIndex) {
        // Treat validFlag as alias for deleteFlag - special Solution case here
        // added 02/09/2005 -aww
        return 
            super.isCellEditable(rowIndex, colIndex) || (colIndex == VALIDFLAG);
    }

    public void setValueAt(Object value, int rowIndex, int colIndex) {
        // fireCellUpdate always?
        if (! cellChangeVerified(value, rowIndex, colIndex)) return;
        Solution sl = getSolution(rowIndex);
        boolean validCase = true;
        switch (colIndex) {
           case ID:
                validCase = false;
                //sl.id.setValue(value);
                break;
           case EXTERNALID:
                sl.externalId.setValue(value);
                break;
           case DATETIME:
                sl.datetime.setValue(value);
                break;
           case LAT:
                sl.lat.setValue(value);
                break;
           case LON:
                sl.lon.setValue(value);
                break;
           case DEPTH:
                sl.depth.setValue(value);
                break;
           case MDEPTH:
                sl.mdepth.setValue(value);
                break;
           case HORIZDATUM:
                sl.horizDatum.setValue(value);
                break;
           case VERTDATUM:
                sl.vertDatum.setValue(value);
                break;
           case TYPE:
                sl.type.setValue(value);
                break;
           case METHOD:
                sl.algorithm.setValue(value);
                break;
           case CRUSTMODEL:
                sl.crustModel.setValue(value);
                break;
           case VELMODEL:
                sl.velModel.setValue(value);
                break;
           case EAUTH:
                sl.eventAuthority.setValue(value);
                break;
           case ESRC:
                sl.eventSource.setValue(value);
                break;
           case AUTHORITY:
                sl.authority.setValue(value);
                break;
           case SOURCE:
                sl.source.setValue(value);
                break;
           case GAP:
                sl.gap.setValue(value);
                break;
           case DISTANCE:
                sl.distance.setValue(value);
                break;
           case RMS:
                sl.rms.setValue(value);
                break;
           case ERRORTIME:
                sl.errorTime.setValue(value);
                break;
           case ERRORHORIZ:
                sl.errorHoriz.setValue(value);
                break;
           case ERRORVERT:
                sl.errorVert.setValue(value);
                break;
           case ERRORLAT:
                sl.errorLat.setValue(value);
                break;
           case ERRORLON:
                sl.errorLon.setValue(value);
                break;
           case TOTALREADINGS:
                sl.totalReadings.setValue(value);
                break;
           case USEDREADINGS:
                sl.usedReadings.setValue(value);
                break;
           case SREADINGS:
                sl.sReadings.setValue(value);
                break;
           case FIRSTMOTIONS:
                sl.firstMotions.setValue(value);
                break;
           case QUALITY:
                sl.quality.setValue(value);
                break;
           case VALIDFLAG:
                // Note isCellEditable(i,i) override allows validFlag editing when deleteFlag==true 
                // otherwise only would be editable if undeleted by deleteFlag - aww 02/09/2005 
                if (((Boolean) value).booleanValue()) {
                    sl.validFlag.setValue(1l);
                    if (sl.isDeleted()) // undelete sol
                      setValueAt( Boolean.FALSE, rowIndex, DELETE); 
                }
                else {
                    sl.validFlag.setValue(0l);
                    if (! sl.isDeleted()) //  delete sol
                      setValueAt( Boolean.TRUE, rowIndex, DELETE); 
                }
                break;
           case EVENTTYPE:
                sl.eventType.setValue(value);
                break;
           case GTYPE:
                sl.gtype.setValue(value);
                break;
           case PROCESSING:
                sl.processingState.setValue(value);
                break;
           case DEPTHFIXED:
                sl.depthFixed.setValue(((Boolean)value).booleanValue());
                break;
           case LOCATIONFIXED:
                sl.locationFixed.setValue(((Boolean)value).booleanValue());
                sl.setStale(false); // test to allow mag calc - aww 5/10/2005
                break;
           case TIMEFIXED:
                sl.timeFixed.setValue(((Boolean)value).booleanValue());
                break;
           case WAVERECORDS:
                sl.waveRecords.setValue(value);
                //sl.waveRecords.setValue(sl.countWaveforms()); // aww 02/17/2005 test
                break;
           case MAGNITUDE:
                // kludge to allow setting prefmag for new Solution data entered by hand, lacking a prefmag
                if (sl.getPreferredMagnitude() == null) {
                  createNewPrefMag(sl); // aww 03/28/2005
                }
                if (sl.magnitude != null) sl.magnitude.value.setValue(value); // aww 03/24/2005
                break;
           case MAGTYPE:
                // kludge to allow setting prefmag for new Solution data entered by hand, lacking a prefmag
                if (sl.getPreferredMagnitude() == null) {
                  createNewPrefMag(sl); // aww 03/28/2005
                }
                if (sl.magnitude != null) { // aww 03/24/2005
                  if (NullValueDb.isEmpty(value.toString())) {
                      sl.magnitude.setType("x"); // unknown type
                  } 
                  else {
                      sl.magnitude.setType(value.toString());
                  }
                  sl.magnitude.algorithm.setValue("HAND"); // added 05/29/2007 -aww
                  sl.setPrefMagOfType(sl.magnitude); // added 05/29/2007 -aww
                }
                break;
           case MAGALGO:
                // kludge to allow setting prefmag for new Solution data entered by hand, lacking a prefmag
                if (sl.getPreferredMagnitude() == null) {
                  createNewPrefMag(sl);
                }
                if (sl.magnitude != null) {
                  if (NullValueDb.isEmpty(value.toString())) {
                    sl.magnitude.algorithm.setValue("HAND");
                  } 
                  else {
                    sl.magnitude.algorithm.setValue(value.toString());
                  }
                }
                break;
           case MAGSTA:
                // kludge to allow setting prefmag for new Solution data entered by hand, lacking a prefmag
                if (sl.getPreferredMagnitude() == null) {
                  createNewPrefMag(sl);
                }
                if (sl.magnitude != null) sl.magnitude.usedStations.setValue(value); 
                break;
           case MAGOBS:
                // kludge to allow setting prefmag for new Solution data entered by hand, lacking a prefmag
                if (sl.getPreferredMagnitude() == null) {
                  createNewPrefMag(sl);
                }
                if (sl.magnitude != null) sl.magnitude.usedChnls.setValue(value); 
                break;
           case PRIORITY:
                sl.priority.setValue(value);
                break;
           case COMMENT:
                sl.setComment(value.toString());
                break;
           case DELETE:
                // added logic to toggle VALIDFLAG - aww 02/09/2005
                if (((Boolean) value).booleanValue()) {
                    sl.setDeleteFlag(true);
                    setValueAt( Boolean.FALSE, rowIndex, VALIDFLAG);
                }
                else {
                    sl.undelete();
                    setValueAt( Boolean.TRUE, rowIndex, VALIDFLAG);
                }
                break;
           case STALE:
                if (((Boolean) value).booleanValue()) sl.setStale(true);
                else validCase = false;
                break;
           case BOGUS :
                if (((Boolean) value).booleanValue()) sl.dummyFlag.setValue(1);
                else sl.dummyFlag.setValue(0);
                break;
           case PREFOR:
                validCase = false;
                break;
           case PREFMAG:
                validCase = false;
                break;
           case PREFMEC:
                validCase = false;
                break;
           case COMMID:
                validCase = false;
                break;
           case ORID:
                validCase = false;
                break;
           case VERSION:
                validCase = false;
                break;
           case OWHO:
                validCase = false;
                //sl.setWho(value.toString());
                break;
           case MWHO:
                validCase = false;
                //sl.magnitude.setWho(value.toString());
                break;
           case LDDATE:
                validCase=false;
                break;
           default :
                validCase = false;
        }
        if (validCase) {
            sl.setNeedsCommit(true);  // for any change
            fireTableCellUpdated(rowIndex, colIndex);
            //if (colIndex != PROCESSING && !sl.isHuman() ) // revert final?
            //if (sl.processingState.toString().equals(STATE_AUTO_TAG))
            if (sl.isAuto())
                setValueAt(JasiProcessingConstants.STATE_HUMAN_TAG, rowIndex, PROCESSING);
        }
    }

    public JasiCommitableIF initRowValues(JasiCommitableIF newRow) {
        Solution newSol = (Solution) super.initRowValues(newRow);
        newSol.setUniqueId(); // burn an event id
        newSol.setParentId(newSol.getId().longValue()); // commit side effect
        // not null, datetime,lat,lon
        newSol.datetime.setValue(new DateTime().getTrueSeconds()); // set to current true UTC time - aww 2008/02/08
        newSol.setLatLonZ(0.,0.,0.);
        newSol.validFlag.setValue(0);
        newSol.dummyFlag.setValue(1);
        newSol.eventType.setValue(EventTypeMap3.toJasiType(EventTypeMap3.EARTHQUAKE));
        newSol.gtype.setValue(GTypeMap.LOCAL);
        newSol.eventAuthority.setValue(newSol.authority);
        newSol.eventSource.setValue(newSol.source);
        return newSol;
    }

    // need to have updateList flag set upon insert/delete but reset on update?
    public boolean insertRow(int rowIndex, JasiCommitableIF jasiRow) {
       if (jasiRow == null) return false;
       //this.sol.addAltSolution((Solution) jasiRow); // sol has an altSolutionList proposed
       return super.insertRow(rowIndex, jasiRow); // this model list
    }

    /** Override if change of behavior needed for SolutionList case 
     * set true only toggles Solution deleteFlag leaving it in
     * list so invoking a commit on it can do a doCommitDelete()
     * on virtually deleted solution.
     */
    protected boolean deleteSolutionFromDataSource = true; // set by menu/property?
    protected void setDeleteSolutionFromDataSource(boolean tf) { // could set from panel property/menu?
      deleteSolutionFromDataSource = tf;
      this.deleteRemovesRowFromList = !tf;
    }
    public boolean deleteRow(int rowIndex) {
        // below forces setting property deleteRemovesRowFromList declared in super class:
        // Commitables need a commit/lddate timestamp or query to identify virgin (key not in db) -aww
        this.deleteRemovesRowFromList = ! deleteSolutionFromDataSource;  // aww 02/04/2005 
        boolean retVal = super.deleteRow(rowIndex);
        //Could set oldSol flags then invoke a server stored procedure to delete
        //source rows associated with a primary or secondary solution?
        //Solution oldSol = getSolution(rowIndex);
        //if (deleteSolutionFromDataSource) {
        //do below if altSolutionList implemented?
        //  this.sol.deleteAltSolution(oldSol);
        //}
        return retVal;
    }

    public int stripByResidual(double value) {
      if (value == 0. || Double.isNaN(value)) return 0;
      if (Math.abs(value) <= 0.) return 0;
      Solution [] sols = (Solution []) ((GenericSolutionList) modelList).getArray();
      int count = 0;
      for (int idx = 0; idx < sols.length; idx++) {
        if ( ! sols[idx].rms.isNull() && (Math.abs(sols[idx].rms.doubleValue()) >= value)) {
          sols[idx].setDeleteFlag(true);
          count++;
          fireTableRowsUpdated(idx, idx);
        }
      }
      return count;
    }
    public boolean copyRowAt(int rowIndex, boolean after) {
      if (rowIndex < 0 || rowIndex > getRowCount()-1) return false;
      boolean retVal = false;
      Solution jcRow = (Solution) modelList.get(rowIndex);
      Solution newRow = null;
      try {
        newRow =
          (Solution) JasiObject.newInstance(jcRow.getClass().getName());
        initRowValues(newRow);
        newRow.setTime(jcRow.getTime());
        if (after) rowIndex++;
        modelList.add(rowIndex, newRow);
        if (logChanges) aLog.logTextnl("copied " + jcRow.toString());
        //fireTableRowsInserted(rowIndex, rowIndex);
        retVal = true;
      }
      catch (Exception ex) {
        ex.printStackTrace();
      }
      return retVal;
    }

    // kludge to allow kate setting prefmag for new Solution data entered by hand, lacking a prefmag
    private void createNewPrefMag(Solution sol) { // aww 03/28/2005
        //if (sol.getPreferredMagnitude() != null) return;
        if (jmtb == null) jmtb = new JasiMagListTableModel();
        jmtb.setProperties(getProperties());
        jmtb.setAssocSolution(sol);
        Magnitude mag = Magnitude.create();
        jmtb.initRowValues(mag);
        sol.setPreferredMagnitude(mag);
        mag.setNeedsCommit(true); // added for insurance -aww 2008/03/18
    }

}
