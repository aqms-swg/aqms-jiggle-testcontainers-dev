package org.trinet.util.graphics.table;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import org.trinet.jasi.*;
import org.trinet.util.*;

public class JasiWaveformListPanel extends AbstractJasiSolutionAssocPanel {
// table data model is aliased wavefromList reference in the class's constructor.
    public JasiWaveformListPanel() {this(null,false);}
    public JasiWaveformListPanel(JTextArea textArea, boolean updateDB) {
        super(textArea,updateDB);
        initPanel();
    }
    protected void initPanel() { 
        //if (tableColumnOrderByName == null) this.tableColumnOrderByName = JasiWaveformListTableConstants.columnNames;
        this.dataTable = new JasiWaveformListTable(new JasiWaveformListTableModel());
        super.initPanel();
    }
    protected void setList(Solution sol) { 
        aSol = sol;
        setList((ChannelableList)sol.getWaveformList());
    }

    protected void stripRows() { } // noop
 
    protected boolean insertRow() { return false; } // noop
}
