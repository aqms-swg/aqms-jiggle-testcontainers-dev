package org.trinet.util.graphics.table;
import java.util.*;
import org.trinet.jasi.*;
import org.trinet.jasi.TN.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.util.GenericPropertyList;

/** Model represents a list of jasi Waveform objects (WaveformList) in a JTable. */
//public class JasiWaveformListTableModel extends AbstractJasiReadingTableModel 
public class JasiWaveformListTableModel extends AbstractJasiListTableModel
             implements JasiWaveformListTableConstants {

    public JasiWaveformListTableModel() {
        super();
        maxFields = JasiWaveformListTableConstants.MAX_FIELDS;
        deleteColumnIndex = -1;
        tableColumnNames = JasiWaveformListTableConstants.columnNames; 
        tableColumnClasses = JasiWaveformListTableConstants.columnClasses; 
        tableColumnFractionDigits = JasiWaveformListTableConstants.columnFractionDigits;
        tableColumnAKey = JasiWaveformListTableConstants.columnAKey;
        tableColumnEditable = JasiWaveformListTableConstants.columnEditable;
        tableColumnNullable = JasiWaveformListTableConstants.columnNullable;
        tableColumnShown = JasiWaveformListTableConstants.showColumn;
        tableColumnCellWidthPadding = JasiWaveformListTableConstants.cellWidthPadding;
    }

    public JasiWaveformListTableModel(ChannelableList wfList) {
        this(wfList, false);
    }

    public JasiWaveformListTableModel(Solution sol, boolean isCellEditable) {
        this((ChannelableList)sol.getWaveformList(), isCellEditable);
    }

    public JasiWaveformListTableModel(ChannelableList wfList, boolean isCellEditable) {
        this();
        setList(wfList);
        setCellEditable(isCellEditable);
    }

    protected Waveform getWaveform(int rowIndex) {
        return (Waveform) getJasiCommitable(rowIndex);
    }

    public Object getValueAt(int rowIndex, int colIndex) {
        Waveform wf = getWaveform(rowIndex);
        switch (colIndex) {
          case NET:
            return new DataString(wf.getChannelObj().getNet());
          case STA:
            return new DataString(wf.getChannelObj().getSta());
          case CHN:
            return new DataString(wf.getChannelObj().getChannel());
          case AU:
            return new DataString(wf.getChannelObj().getAuth());
          case SUBSRC:
            return new DataString(wf.getChannelObj().getSubsource());
          case CHNSRC:
            return new DataString(wf.getChannelObj().getChannelsrc());
          case SEED:
            return new DataString(wf.getChannelObj().getSeedchan());
          case LOC:
            return new DataString(wf.getChannelObj().getLocation());
          case WFID:
            if (wf instanceof Wavelet) {
              long id = ((Wavelet)wf).getId();
              return (id == 0) ? NULL_DATALONG : new DataLong(id); 
            }
          default:
            return "";
        }
    }

    public void setValueAt(Object value, int rowIndex, int colIndex) {
        // fireCellUpdate always?
        if (! cellChangeVerified(value, rowIndex, colIndex)) return;
        Waveform wf = getWaveform(rowIndex);
        boolean validCase = true;
      try {
        switch (colIndex) {
          case NET:
            wf.getChannelObj().setNet(value.toString());
            break;
          case STA:
            wf.getChannelObj().setSta(value.toString());
            break;
          case CHN:
            String chn = value.toString();
            wf.getChannelObj().setChannel(chn);
            // kludge need to have props set or new subclass for implementing particulars
            if (chn.length() > 0 && wf.getChannelObj().getLocation().equals(defaultChannelLocation)) {
              setValueAt(getSeedchanMap(chn), rowIndex, SEED);
            }
            //wf.getChannelObj().setChannel(value.toString());
            break;
          case AU:
            wf.getChannelObj().setAuth(value.toString());
            break;
          case SUBSRC:
            wf.getChannelObj().setSubsource(value.toString());
            break;
          case CHNSRC:
            wf.getChannelObj().setChannelsrc(value.toString());
            break;
          case SEED:
            wf.getChannelObj().setSeedchan(value.toString());
            break;
          case LOC:
            wf.getChannelObj().setLocation(value.toString());
            break;
          //case EVID:
            //validCase = false;
            //break;
          case WFID:
            validCase = false;
            break;
          default :
            validCase = false;
        }
      
      }
      catch (Throwable thrown) {
         System.out.println("Exception, attempt to setValueAt failed for " + getClass().getName());
         return;
      }
     
        if (validCase) {
            fireTableCellUpdated(rowIndex, colIndex);
        }
    }

    public static final String DUMMY_SITE_CODE = "XXX";
    public static final String DEFAULT_CHANNEL_LOCATION = "00";
    public static final String [] DEFAULT_SEEDCHAN_MAP = {"EHZ", "EHN", "EHE"};
    public static final String [] DEFAULT_CHANNEL_MAP  = {"Z", "N", "E"};

    protected String dummyStaCode = DUMMY_SITE_CODE;
    protected String dummySeedchanCode = DUMMY_SITE_CODE;
    protected String defaultChannelLocation = DEFAULT_CHANNEL_LOCATION;
    protected String [] defaultSeedchanMap   = DEFAULT_SEEDCHAN_MAP;
    protected String [] defaultChannelMap = DEFAULT_CHANNEL_MAP;

    //Flags whether replaced readings in model should test for time match
    protected boolean replaceByTime = false;

    public boolean copyRowAt(int rowIndex, boolean after) {
        return false;
    }
    public JasiCommitableIF initRowValues(JasiCommitableIF aRow) {
        return null;
    }

    public void setProperties(GenericPropertyList gpl) {
       super.setProperties(gpl);
       defaultChannelLocation = gpl.getProperty("defaultChannelLocation", defaultChannelLocation);
       dummyStaCode = gpl.getProperty("dummyStaCode", dummyStaCode);
       dummySeedchanCode = gpl.getProperty("dummySeedchanCode", dummySeedchanCode);

       String [] dummyArray = gpl.getStringArray("channelMap");
       if (dummyArray.length > 0) defaultChannelMap = dummyArray; // reset from current value
       dummyArray = gpl.getStringArray("seedchanMap");
       if (dummyArray.length > 0) defaultSeedchanMap = dummyArray; // reset from current value

    }
    protected String getSeedchanMap(String chan) {
      int count = defaultChannelMap.length;
      for (int idx = 0; idx < count; idx++) {
        if ( defaultChannelMap[idx].equals(chan) ||
             defaultSeedchanMap[idx].equals(chan) ) return defaultSeedchanMap[idx];
      }
      // not defined, return input?
      return chan; // or dummySeedchanCode;
    }

    // override superclass noop to define initial list sort as a time sort
    protected void sortList() {
        ((ChannelableList) modelList).sortByStation();
    }

    /* Override here depends behavior of indexOf using jc.equals() in superclass.
     * Looks for equivalent(r), (cf. getLikeWithSameTime(jr)?) 
     */
    protected boolean addOrReplaceRow(int rowIndex, JasiCommitableIF jasiCommitable) {
        return true;
    }

    // Added override below as test - aww 02/03
    public boolean insertRow (int rowIndex, JasiCommitableIF jc) {
       // Equivalent addOrReplace for amps, codas, phase test for 
       // similarity of contents rather than object identity.
       // indexOfEquivalent vs. indexOf in AbstractCommitableListIF
       // overrides method in super class.
       return addOrReplaceRow(rowIndex, jc);
    }

}
