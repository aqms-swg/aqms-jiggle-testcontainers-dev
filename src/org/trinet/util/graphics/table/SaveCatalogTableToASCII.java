package org.trinet.util.graphics.table;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import org.trinet.jdbc.datatypes.*;
import org.trinet.jdbc.StringSQL;
import org.trinet.util.DateTime;

public class SaveCatalogTableToASCII implements ActionListener {
     JDialog aDialog = null;
     boolean canceled = false;

     public void save(CatalogTable adapter, Frame f) {
         FileDialog fd = new FileDialog(f, "Save content to file", FileDialog.SAVE);
         fd.setVisible(true);
         if (fd.getFile() == null)
             return;
         JRadioButton tabDelimited = null;
         JRadioButton commaDelimited = null;
         JCheckBox includeHeaderRow = null;
         if (aDialog == null) {
             aDialog = new JDialog(f, "Table to ASCII file", true);
            
             tabDelimited = new JRadioButton("Tab delimited");
             tabDelimited.setSelected(true);
             commaDelimited = new JRadioButton("Comma delimited");
             JRadioButton spaceDelimited = new JRadioButton("Space delimited");
             ButtonGroup group = new ButtonGroup();
             group.add(tabDelimited);
             group.add(commaDelimited);
             group.add(spaceDelimited);
             includeHeaderRow = new JCheckBox("Include header row");
             includeHeaderRow.setSelected(true);
             JButton OKButton = new JButton("Save");
             OKButton.setActionCommand("Save");
             OKButton.addActionListener(this);
             JButton cancelButton = new JButton("Cancel");
             cancelButton.setActionCommand("Cancel");
             cancelButton.addActionListener(this);
             JPanel buttonPanel = new JPanel();
             buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
             buttonPanel.add(OKButton);
             buttonPanel.add(cancelButton);
             JPanel radioButtons = new JPanel();
             radioButtons.setLayout(new GridLayout(5,1,5,5));
             radioButtons.add(new JLabel("File format"));
             radioButtons.add(tabDelimited);
             radioButtons.add(commaDelimited);
             radioButtons.add(spaceDelimited);
             radioButtons.add(includeHeaderRow);

             Container panel = aDialog.getContentPane();
             panel.setLayout(new BorderLayout(5, 5));
             panel.add(radioButtons, BorderLayout.CENTER);
             panel.add(buttonPanel, BorderLayout.SOUTH);
        }
        
        aDialog.pack();
        aDialog.setVisible(true);
        if (canceled)
            return;
//        String delimiter = tabDelimited.isSelected()?"\t":",";
        String delimiter = " ";
        if (tabDelimited.isSelected()) delimiter = "\t";
        if (commaDelimited.isSelected()) delimiter = ",";
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(fd.getDirectory() + fd.getFile()));
            if (includeHeaderRow.isSelected()) {
                if (adapter.getTable().getColumnCount()>0) {
                    bw.write(adapter.getRowHeader().getColumnName(0));
                    bw.write(delimiter + adapter.getTable().getColumnName(0));
                    for (int ii = 1; ii < adapter.getTable().getColumnCount(); ii++)
                        bw.write(delimiter+adapter.getTable().getColumnName(ii));
                    bw.newLine();
                }
            }
            // System.out.println("Model column count : "+ adapter.getTable().getColumnCount());
            if (adapter.getTable().getColumnCount()>0) {
                for (int ii = 0; ii < adapter.getTable().getRowCount(); ii++) {
                    bw.write(StringSQL.valueOf(adapter.getRowHeader().getValueAt(ii,0)));
                    bw.write(delimiter + StringSQL.valueOf( adapter.getTable().getValueAt(ii, 0) ));
                    for (int jj = 1; jj < adapter.getTable().getColumnCount(); jj++)
                        if ( adapter.getTable().getColumnName(jj).equalsIgnoreCase("datetime") ) {
                            bw.write(delimiter +
                                    StringSQL.valueOf(
                                        new DateTime(((DataDouble)adapter.getTable().getValueAt(ii, jj)).doubleValue(), true)
                                        .toDateString("yyyy-MM-dd HH:mm:ss.SSS")));
                        } else {
                            bw.write(delimiter + StringSQL.valueOf( adapter.getTable().getValueAt(ii, jj) ));
                        }
                    bw.newLine();
                }
            }
            bw.close ();
        } catch (IOException ex) {
        }
        System.out.println("Saved Catalog table to ASCII file : "+fd.getDirectory()+fd.getFile() );
    }

    public void actionPerformed(ActionEvent event) {
        if (event.getActionCommand().equals("cancel")) canceled = true;
        aDialog.setVisible(false);
    }
 
    /*
    static public void main(String args[]) {
        SaveCatalogTableToASCII saveTable = new SaveCatalogTableToASCII ();
        saveTable.save(null, new JFrame());
    }
    */
}
