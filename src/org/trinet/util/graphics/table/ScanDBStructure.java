package org.trinet.util.graphics.table;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.table.*;
import javax.swing.tree.*;
import org.trinet.jdbc.*;
// Allan Walter modified original class by Oivind Kolloen
public class ScanDBStructure extends JFrame implements ActionListener {
    protected DefaultMutableTreeNode topOfTree = new DefaultMutableTreeNode ("Database");
    protected JTree dbStructure = new JTree(topOfTree);
    protected JSplitPane splitPane = new JSplitPane();
    protected JPanel extraInfoPane = new JPanel ();
    protected String[] nodeInfo;
    JDBCAdapter jdbcA;
    private String[] columnNames = {"Table", "Column", "DataType", "TypeName", "Size", "Decimal", "Default","Position", "Null"};

    public ScanDBStructure (String s, String schemaName, JDBCAdapter jdbcA) {
//Do frame stuff.
    super(s);
    this.jdbcA = jdbcA;
    addWindowListener(new WindowAdapter() {
        public void windowClosing(WindowEvent e) {
        setVisible (false);
        dispose ();
        }
    });
// setIconImage ((new ImageIcon(getClass().getResource( "/home/tpp/bin/20x20/Binocular.gif" ))).getImage());
    setIconImage ((new ImageIcon("/home/tpp/bin/20x20/Binocular.gif")).getImage());
    buildTree (topOfTree, schemaName, jdbcA);
    dbStructure.setCellRenderer (new myTreeCellRenderer ());
    dbStructure.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
    dbStructure.addTreeSelectionListener(new MyTreeSelectionListener());
    JScrollPane structPane = new JScrollPane (dbStructure);
// Lay out structDB pane
    JPanel structDBPane = new JPanel ();
    structDBPane.setLayout (new BorderLayout ());
    structDBPane.setBorder(new BevelBorder(BevelBorder.LOWERED));
    structDBPane.add (new JLabel ("Database Schema Structure"), BorderLayout.NORTH);
    structDBPane.add (structPane, BorderLayout.CENTER);

// Lay out extra info pane
    extraInfoPane.setLayout (new BorderLayout ());
    extraInfoPane.setBorder(new BevelBorder(BevelBorder.LOWERED));
    extraInfoPane.add (new JLabel ("Table Column Field Data"), BorderLayout.NORTH);
//  extraInfoPane.addPropertyChangeListener(this); // perhaps Observable/Observer ?
//  extraInfoPane.add (extraPane, BorderLayout.CENTER);

// Create a split pane with the two scroll panes in it.
    splitPane.setLeftComponent(structDBPane);
    splitPane.setRightComponent(extraInfoPane);
    splitPane.setOneTouchExpandable(true);

//Lay out the master content pane.
    JPanel masterContentPane = new JPanel();
    masterContentPane.setLayout(new BorderLayout());
    masterContentPane.setPreferredSize(new Dimension(600, 300));
    masterContentPane.add(splitPane, BorderLayout.CENTER);
    setContentPane(masterContentPane);
    }

    public class MyTreeSelectionListener implements TreeSelectionListener {
    public void valueChanged(TreeSelectionEvent e) {
        JTable tableRowHead = null; 
        JTable extraInfo = new JTable();
// Node is field descriptor
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) (e.getPath().getLastPathComponent());

        try {
        nodeInfo = ((treeNodedbfield)node.getUserObject()).getData();
            if (((treeNodedbfield)node.getUserObject()).getLevel()==3) {
            AbstractTableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() { return columnNames.length; }
            public int getRowCount() { return 1;}
            public Object getValueAt(int row, int col) { 
                return nodeInfo[col];
            }
            public String getColumnName (int column) {
                return columnNames[column];
                }
            };
            extraInfo.setModel (dataModel);

            extraInfo.setAutoResizeMode(extraInfo.AUTO_RESIZE_OFF );
            extraInfo.sizeColumnsToFit(extraInfo.AUTO_RESIZE_NEXT_COLUMN );
//          extraInfo.getTableHeader().setUpdateTableInRealTime(false);
            int fontSize = 12;
            Font font = new Font("Monospaced", Font.PLAIN, fontSize);
            extraInfo.getTableHeader().setFont(font);
            extraInfo.setShowGrid(false);
// Make second table for row header
            TableColumnModel tcm = extraInfo.getColumnModel();
            TableColumnModel tcm2 = new DefaultTableColumnModel();
            TableColumn col;
            col = tcm.getColumn(tcm.getColumnIndex((Object) columnNames[0]));
            tcm.removeColumn(col);
            tcm2.addColumn(col);
            col = tcm.getColumn(tcm.getColumnIndex((Object) columnNames[1]));
            tcm.removeColumn(col);
            tcm2.addColumn(col);
            tableRowHead = new JTable();
            tableRowHead.setModel(dataModel);
            tableRowHead.setColumnModel(tcm2);
            tableRowHead.setAutoResizeMode( tableRowHead.AUTO_RESIZE_OFF );
            tableRowHead.sizeColumnsToFit(tableRowHead.AUTO_RESIZE_NEXT_COLUMN );
//          tableRowHead.getTableHeader().setUpdateTableInRealTime(false);
            tableRowHead.getTableHeader().setForeground(Color.red);
            tableRowHead.getTableHeader().setBackground(new Color(.85f,.85f,.85f));
            tableRowHead.getTableHeader().setFont(font);
            tableRowHead.setShowGrid(false);
    
// Setup format patterns and number renderers
// Assign editor/renderers to table columns 
            for (int i = 0; i<extraInfo.getColumnCount(); i++) {
                tcm.getColumn(i).setCellRenderer(new TextCellRenderer(font));
                tcm.getColumn(i).setCellEditor(new TextCellEditor(font));
            }
// size scrollable main table columns
            initColumnSizes(extraInfo);

// setup main table selection/color attributes
            extraInfo.setCellSelectionEnabled(false);
            extraInfo.setColumnSelectionAllowed(false);
            extraInfo.setForeground(Color.black);
            extraInfo.setBackground(new Color(1.f,1.f,0.7f));

// assign rowheader renderer (string type)
            for (int i = 0; i<tableRowHead.getColumnCount(); i++) {
                tcm2.getColumn(i).setCellRenderer(new TextCellRenderer(font));
            }

// size rowheader columns
            initColumnSizes(tableRowHead);

// setup rowheader selection/color attributes
            tableRowHead.setCellSelectionEnabled(false);
            tableRowHead.setRowSelectionAllowed(false);
            tableRowHead.setColumnSelectionAllowed(false);
//          tableRowHead.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tableRowHead.setForeground(Color.black);
            tableRowHead.setBackground(Color.yellow);
    
// size rowheader and put in scrollpane
            tableRowHead.setPreferredScrollableViewportSize(tableRowHead.getPreferredSize());
// size the table viewport
            Dimension vSize = extraInfo.getPreferredSize();
            extraInfo.setPreferredScrollableViewportSize(vSize);
        } else
            extraInfo.setModel (new DefaultTableModel());
        } catch (Exception ex) {
        extraInfo.setModel (new DefaultTableModel());
        }
        JScrollPane extraPane = new JScrollPane (extraInfo);
        if (tableRowHead != null) {
        extraPane.setRowHeaderView(tableRowHead);
        extraPane.setCorner(JScrollPane.UPPER_LEFT_CORNER, tableRowHead.getTableHeader());
        }
        if (extraInfoPane.getComponentCount() == 2) {
        extraInfoPane.remove(1);
        }
        extraInfoPane.add (extraPane, BorderLayout.CENTER);
        if (ScanDBStructure.this.splitPane != null) {
        if (tableRowHead == null) {
            int jloc = ScanDBStructure.this.splitPane.getMaximumDividerLocation();
            ScanDBStructure.this.splitPane.setDividerLocation(jloc);
        }
        else {
            int iloc = ScanDBStructure.this.splitPane.getDividerLocation();
            int minloc = ScanDBStructure.this.splitPane.getMinimumDividerLocation();
            int maxloc = ScanDBStructure.this.splitPane.getMaximumDividerLocation();
            if (iloc > (maxloc+minloc)/2) ScanDBStructure.this.splitPane.setDividerLocation(minloc);
        }
        ScanDBStructure.this.splitPane.revalidate();
        }
    }
    }

    private void initColumnSizes(JTable table) {
    TableColumn column = null;
    Component comp = null;
    int headerWidth = 0;
    int cellWidth = 0;

    int colCount = table.getColumnCount();
    int rowCount = table.getRowCount();  // perhaps would be better to used subset for speed?
    for (int i = 0; i < colCount; i++) {
        column = table.getColumnModel().getColumn(i);
        comp = column.getHeaderRenderer().
        getTableCellRendererComponent(table, column.getHeaderValue(), false, false, 0, 0);
        headerWidth = comp.getPreferredSize().width;
        int maxCellWidth = 0;
        for (int j = 0; j < rowCount; j++) {
        comp = table.getCellRenderer(j,i).getTableCellRendererComponent(
            table, table.getValueAt(j,i), false, false, j, i);
            cellWidth = comp.getPreferredSize().width;
        if (cellWidth > maxCellWidth) maxCellWidth = cellWidth;
        }
        column.setPreferredWidth(Math.max(headerWidth, maxCellWidth));
    }
    } 

    private void fillDB (DefaultMutableTreeNode db, String schemaName, JDBCAdapter jdbcA) {
    String[] tables = jdbcA.getTableNames(schemaName.toUpperCase());
    Thread.currentThread().yield();
    for (int i=0; i<tables.length; i++) {
        DefaultMutableTreeNode treeTable = new DefaultMutableTreeNode (new treeNodedbfield (tables[i], 2));
        fillTable (treeTable, tables[i], jdbcA);
        db.add (treeTable);
    }
    }

    private void fillTable (DefaultMutableTreeNode treeTable,  String tableName, JDBCAdapter jdbcA) {
    ColumnData [] cda = jdbcA.getColumnData(tableName);
    if (cda.length <= 0) return;
//  Debug.println("cda.length:" + cda.length);
    ArrayList fields = new ArrayList(cda.length);
    for (int i = 0; i < cda.length; i++) {
//      Debug.println(cda[i].toString());
        fields.add((Object) cda[i].toStringArray());
    }
    Thread.currentThread().yield();
    for (int i=0; i<fields.size(); i++) {
        DefaultMutableTreeNode field = new DefaultMutableTreeNode
         (new treeNodedbfield ((String[])fields.get(i), 3));
        treeTable.add(field);
    }
    }

    private void buildTree (DefaultMutableTreeNode top, String schemaName, JDBCAdapter jdbcA) {
    ProgressMonitor pm = new ProgressMonitor (this, "Getting data from database", schemaName, 0, 1000);
    pm.setMillisToPopup(10);
    Thread.currentThread().yield();
    DefaultMutableTreeNode db = new DefaultMutableTreeNode (new treeNodedbfield (schemaName, 1));
    fillDB (db, schemaName, jdbcA);
    top.add (db);
    pm.close();
    }

    public void actionPerformed (ActionEvent ae) {
    }

    private class treeNodedbfield {
    private String[] data = null;
    int level = 0;

    public treeNodedbfield (String[] input, int level) {
        data = input;
        this.level = level;
    }
    public treeNodedbfield (String input, int level) {
        data = new String[1];
        data[0] = input;
        this.level = level;
    }

    public String toString() {
        if (data.length == 1) return data[0];
        else return data[1];
    }

    public String[] getData() {
        return data;
    }

    public int getLevel() {
        return level;
    }
    }

    public class myTreeCellRenderer extends DefaultTreeCellRenderer {
//  private ImageIcon dbIcon = new ImageIcon(getClass().getResource( "/home/tpp/bin/20x20/Data.gif" ));
//  private ImageIcon dbTable = new ImageIcon(getClass().getResource( "/home/tpp/bin/20x20/Sheet.gif" ));
//  private ImageIcon dbColumn = new ImageIcon(getClass().getResource( "/home/tpp/bin/20x20/Column.gif" ));
//  private ImageIcon dbRoot = new ImageIcon(getClass().getResource( "/home/tpp/bin/20x20/Workstation.gif" ));
    private ImageIcon dbIcon = new ImageIcon("/home/tpp/bin/20x20/Data.gif" );
    private ImageIcon dbTable = new ImageIcon("/home/tpp/bin/20x20/Sheet.gif" );
    private ImageIcon dbColumn = new ImageIcon("/home/tpp/bin/20x20/Column.gif");
    private ImageIcon dbRoot = new ImageIcon("/home/tpp/bin/20x20/Workstation.gif");
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded,
            boolean leaf, int row, boolean hasFocus) {
        Component c = super.getTreeCellRendererComponent (tree, value, sel, expanded, leaf, row, hasFocus);
        try {
        int level = ((treeNodedbfield)(((DefaultMutableTreeNode)value).getUserObject())).getLevel();
        if (level==1) setIcon (dbIcon);
        else if (level==2) setIcon (dbTable);
        else if (level==3) setIcon (dbColumn);
        } catch (Exception e) {
        setIcon (dbRoot);
        }
        return c;
    }
    }

    /*
    public static void main(String[] args) {
    JDBCAdapter jdbcA = new JDBCAdapter();
        //TestDataSource tds = TestDataSource.create("k2");
        jdbcA.connect("db-url-string","db-driver-string", "user", "passwd");
    ScanDBStructure frame = new ScanDBStructure("Database Schema Structure", "TRINETDB", jdbcA);
    frame.pack();
    frame.show();
    }
    */
}

