package org.trinet.util.graphics.table;
public class SequencedTables {
    private static final String [] columnNames = { "EVID", "EVID", "ORID", "ORID", "MAGID", "ARID",
		 "AMPID", "COMMID", "MECID", "COID", "WFID"};
    private static final String [] tableNames = {"EVENT", "SIG_EVENT", "ORIGIN", "ORIG_ERROR", "NETMAG", "ARRIVAL",
		 "AMP", "REMARK", "MEC", "CODA", "WAVEFORM"};

    private static final String [] sequenceNames = { "EVSEQ", "EVSEQ", "ORSEQ", "ORSEQ", "MAGSEQ", "ARSEQ",
		 "AMPSEQ", "COMMSEQ", "MECSEQ", "COSEQ", "WASEQ"};

    private static final int MAX_SEQUENCE_TABLES = tableNames.length;

    public final static String getColumnName(int index) {
	if (index < 0 || index > MAX_SEQUENCE_TABLES) return null;
	return columnNames[index];
    }

    public final static String getTableName(int index) {
	if (index < 0 || index > MAX_SEQUENCE_TABLES) return null;
	return tableNames[index];
    }

    public final static String getSequenceName(int index) {
	if (index < 0 || index > MAX_SEQUENCE_TABLES) return null;
	return sequenceNames[index];
    }

    public final static String getColumnName(String table ) {
	  for (int i = 0; i < MAX_SEQUENCE_TABLES; i++) {
	    if (table.trim().equalsIgnoreCase(tableNames[i])) {
	      return columnNames[i];
	    }
	  }
	  return null;
    }

    public final static String getSequenceName(String table ) {
	  for (int i = 0; i < MAX_SEQUENCE_TABLES; i++) {
	    if (table.trim().equalsIgnoreCase(tableNames[i])) {
	      return sequenceNames[i];
	    }
	  }
	  return null;
    }
}
