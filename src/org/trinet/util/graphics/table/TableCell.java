package org.trinet.util.graphics.table;
import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
public class TableCell {

    public static boolean resetRowColumnSizes(JTable table, int irow, int icol) {
        if (table == null) return false;
        int rowCount = table.getRowCount();
        if (irow < 0 || irow > rowCount) return false;
        TableColumn column = null;
        Component comp = null;
        int headerWidth = 0;
        int cellWidth = 0;
          column = table.getColumnModel().getColumn(icol);
          comp = column.getHeaderRenderer().getTableCellRendererComponent(
                          table, column.getHeaderValue(), true, true, 0, 0);

          Insets insets =  ((Container) comp).getInsets();
          int insetPads = insets.left + insets.right;

          headerWidth = comp.getPreferredSize().width + insetPads;

          int maxCellWidth = 0;
          comp = table.getCellRenderer(irow, icol).getTableCellRendererComponent(
                       table, table.getValueAt(irow, icol), true, true, irow, icol);
          cellWidth = comp.getPreferredSize().width + insetPads;

          if (cellWidth > maxCellWidth) maxCellWidth = cellWidth;
          cellWidth = Math.max(headerWidth, maxCellWidth);
          // If current width is larger let it be ? column.setPreferredWidth(Math.max(column.getPreferredWidth(), cellWidth));
          column.setPreferredWidth(cellWidth);

        return true;
    } 

    public static boolean resetRowColumnSizes(JTable table, int irow) {
        if (table == null) return false;
        int rowCount = table.getRowCount();
        if (irow < 0 || irow > rowCount) return false;
        TableColumn column = null;
        Component comp = null;
        int headerWidth = 0;
        int cellWidth = 0;
        int colCount = table.getColumnCount();
        int insetPads = 0;
        int maxCellWidth = 0;
        for (int i = 0; i < colCount; i++) {
          column = table.getColumnModel().getColumn(i);
          comp = column.getHeaderRenderer().getTableCellRendererComponent(
                                 table, column.getHeaderValue(), true, true, 0, 0);
          if (i == 0) {
            Insets insets =  ((Container) comp).getInsets();
            insetPads = insets.left + insets.right;
          }
          headerWidth = comp.getPreferredSize().width;
          maxCellWidth = 0;
          comp = table.getCellRenderer(irow,i).getTableCellRendererComponent(
                       table, table.getValueAt(irow,i), true, true, irow, i);
          cellWidth = comp.getPreferredSize().width;
          if (cellWidth > maxCellWidth) maxCellWidth = cellWidth;

          column.setPreferredWidth(Math.max(headerWidth, maxCellWidth) + insetPads);
        }
        return true;
    } 

    public static int getRenderedCellWidth(JTable table, int irow, int icol) {
        if (table == null) return -1;
        int rowCount = table.getRowCount();
        if (irow < 0 || irow > rowCount) return -1;
        int colCount = table.getColumnCount();
        if (icol < 0 || icol > colCount) return -1;
        TableColumn column = table.getColumnModel().getColumn(icol);
        if (column == null ) return -1;
        Component comp = table.getCellRenderer(irow,icol).getTableCellRendererComponent(
                table, table.getValueAt(irow,icol), true, true, irow, icol);

        Insets insets =  ((Container) comp).getInsets();

        return comp.getPreferredSize().width + insets.left + insets.right;
    } 

    public static int getPreferredRowWidth(JTable table, int irow) {
        if (table == null) return -1;
        int rowCount = table.getRowCount();
        if (irow < 0 || irow > rowCount) return -1;
        TableColumn column = null;
        Component comp = null;
        int headerWidth = 0;
        int cellWidth = 0;
        int maxWidth = 0;
        int colCount = table.getColumnCount();
        int insetPads = 0;
        int maxCellWidth = 0;
        for (int i = 0; i < colCount; i++) {
          column = table.getColumnModel().getColumn(i);
          comp = column.getHeaderRenderer().getTableCellRendererComponent(
                                 table, column.getHeaderValue(), true, true, 0, 0);
          if (i == 0) {
            Insets insets =  ((Container) comp).getInsets();
            insetPads = insets.left + insets.right;
          }
          headerWidth = comp.getPreferredSize().width;
          maxCellWidth = 0;
          comp = table.getCellRenderer(irow,i).getTableCellRendererComponent(
                                 table, table.getValueAt(irow,i), true, true, irow, i);
          cellWidth = comp.getPreferredSize().width;
          if (cellWidth > maxCellWidth) maxCellWidth = cellWidth;

          maxWidth += Math.max(headerWidth, maxCellWidth) + insetPads;
        }
        return maxWidth;
    }
}
