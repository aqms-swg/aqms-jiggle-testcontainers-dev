package org.trinet.util.graphics.table;
import java.util.*;

import org.trinet.util.*;
import org.trinet.jasi.*;

public interface TableRowTextParserIF {
  public void setProperties(GenericPropertyList props);
  public ArrayList parseTableRowList(String text);
  public JasiCommitableIF createRow();
  public int parseCount();
  public boolean hasParseErrors();
  public String getFormatTipText();
  public void setFormatTipText(String formatTip);
  public boolean parseDateTime(String dateString);  // sets date,hr,min,sec
  // sets default date only implementation defaults to using last parsed time values
  public void setDefaultDateString(String dateString);
}
