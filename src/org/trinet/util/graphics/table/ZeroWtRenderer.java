package org.trinet.util.graphics.table;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.table.*;
import org.trinet.jdbc.datatypes.*;
public class ZeroWtRenderer extends FormattedNumberRenderer {

  Color zeroWtColor = defaultBackground;

  public ZeroWtRenderer() { 
    this(null, null);
  }
  public ZeroWtRenderer(Font font) { 
    this(null, font);
  }
  public ZeroWtRenderer(String pattern) { 
    this(pattern, null);
  }
  public ZeroWtRenderer(String pattern , Font font) { 
    super(pattern, font);
  }
  public void setZeroWtColor(Color aColor) {
    this.zeroWtColor = aColor;
  }
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
                 int row, int col) {
    Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
    double testValue = 0.;
    if (value instanceof DataNumber) testValue = ((DataNumber)value).doubleValue();
    else if (value instanceof Number) testValue = ((Number)value).doubleValue();
    if (testValue > 0.) setBackground(defaultBackground);
    else setBackground((zeroWtColor == null) ? defaultBackground : zeroWtColor); 
    return comp;
  }
}
