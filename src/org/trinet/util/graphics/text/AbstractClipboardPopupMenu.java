/*
 * Note: could develop text field undo/redo menu option
 * Put text document undoableEdit listeners and add edits to undo manager
 * in an extended table cell editor component class installed on text columns
 * class needs UndoManager member and text component document listener methods
 * Simple case:
 * Initialize undo manager for selected table column editor
 * Enable menu undo/redo accordingly
 * Invoke textField editor in cell, listeners add edits to manager 
 * Compound edit all changes to text field (toggle between menu state)
 * Exiting cell kills all edits in undo manager, no cross cell edits
*/
package org.trinet.util.graphics.text;
import java.awt.*;
import java.awt.event.*;
import java.awt.datatransfer.*;
import javax.swing.*;
public abstract class AbstractClipboardPopupMenu extends JPopupMenu implements ActionListener {

    protected MouseListener popupMouseListener = new PopupMouseListener();
    protected Component aComp;
    protected Toolkit toolkit;
    protected Clipboard clip;

    public AbstractClipboardPopupMenu() { 
        super();
        configureMenu();
    }
    public AbstractClipboardPopupMenu(Component aComp) { 
        this(aComp, null);
    }
    public AbstractClipboardPopupMenu(String text) { 
        this(null, text);
    }
    public AbstractClipboardPopupMenu(Component aComp, String label) { 
        this();
        if (aComp != null) setComponent(aComp);
        if (label != null) setLabel(label);
        toolkit = getToolkit();
        clip = toolkit.getSystemClipboard();
    }

    protected abstract void configureMenu();

    public void setComponent(Component aComp) {
        if (this.aComp != null) this.aComp.removeMouseListener(popupMouseListener);
        this.aComp = aComp;
        if (aComp != null) addMouseListenerToComponent(aComp);
    }
    protected void addMouseListenerToComponent(Component aComp) {
        aComp.addMouseListener(popupMouseListener);
    }
    public abstract void actionPerformed(ActionEvent e);

    class PopupMouseListener extends MouseAdapter {
        public void mousePressed(MouseEvent e) {
            checkShowPopup(e);
        }
        public void mouseReleased(MouseEvent e) {
            checkShowPopup(e);
        }
        protected void checkShowPopup(MouseEvent e) {
            if (e.isPopupTrigger()) {
              configureMenuItemStates(aComp);
              AbstractClipboardPopupMenu.this.show(e.getComponent(),
                                e.getX(), e.getY());
            }
        }
    }

    protected void configureMenuItemStates(Component aComp) {}

    protected String getClipboardString(Clipboard clip) {
       Transferable clipData = clip.getContents(this);
       if (clipData == null) return null; 
       try {
           return (clipData.isDataFlavorSupported(DataFlavor.stringFlavor)) ?
             (String) clipData.getTransferData(DataFlavor.stringFlavor) : null;
       }
       catch (Exception ex) {
           ex.printStackTrace();
           toolkit.beep();
       }
       return null;
    }
}
