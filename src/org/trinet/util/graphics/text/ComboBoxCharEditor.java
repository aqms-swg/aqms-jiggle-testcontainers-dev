package org.trinet.util.graphics.text;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.text.*;

public class ComboBoxCharEditor implements ComboBoxEditor, FocusListener {
  protected int fontSize=12;
  protected int length;
  protected Font font;
  protected JTextField editorComponent;

  public ComboBoxCharEditor() { 
    this(null);
  }
  
  public ComboBoxCharEditor(Font font) { 
    if (font != null) this.font = font;
    else this.font = new Font("Monospaced", Font.PLAIN, fontSize);

    editorComponent = new JTextField(new LetterDigitDocument(1), null, 1) {

        public Dimension getPreferredSize() {
            Dimension size =  super.getPreferredSize();
            Insets insets = super.getInsets();
            size.width += (insets.left  + insets.right);
            return size;
        }
    };
    editorComponent.setFont(font);
    editorComponent.setHorizontalAlignment(JTextField.LEFT);
    editorComponent.addFocusListener(this);
  }
  
  public void addActionListener(ActionListener al) {
    editorComponent.addActionListener(al);
  }

  public void removeActionListener(ActionListener al) {
    editorComponent.removeActionListener(al);
  }

  public Component getEditorComponent() {
    return editorComponent;
  }

  public void selectAll() {
    editorComponent.selectAll();
  }

  public Object getItem() {
    String value = editorComponent.getText();
    return (value.length() > 0 ) ? Character.valueOf(value.charAt(0)) : null; 
  }

  public void setItem(Object item) {
    if (item != null)  editorComponent.setText(item.toString());
    else  editorComponent.setText("");
  }

  public void focusGained(FocusEvent fe) {
    selectAll();
  }

  public void focusLost(FocusEvent fe) {
  }

}
