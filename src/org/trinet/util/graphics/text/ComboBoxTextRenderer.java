package org.trinet.util.graphics.text;
import javax.swing.*;
import java.awt.*;
import java.util.*;
import java.text.*;

public class ComboBoxTextRenderer extends JLabel implements ListCellRenderer {
  int fontSize=12;
  int length = 0;
  Font font = null;

  public ComboBoxTextRenderer() { 
    this(null, 0, 0);
  }
  
  public ComboBoxTextRenderer(int fontSize) { 
    this(null, fontSize, 0);
  }

  public ComboBoxTextRenderer(int fontSize, int length) { 
    this(null, fontSize, length);
  }
  
  public ComboBoxTextRenderer(Font font) { 
    this(font, 0, 0);
  }
  
  public ComboBoxTextRenderer(Font font, int fontSize) { 
    this(font, fontSize, 0);
  }
  
  public ComboBoxTextRenderer(Font font, int fontSize, int length) { 
    if (fontSize > 0) this.fontSize = fontSize;
    if (length > 0) this.length = length;
    if (font != null) {
	if (fontSize > 0) this.font = font.deriveFont((float) fontSize);
	else this.font = font;
    }
    else this.font = new Font("Monospaced", Font.PLAIN, fontSize);
    setFont(font);
    setHorizontalAlignment(JLabel.LEFT);
    setForeground(Color.black);
  }
  
  public int getFontSize() {
    return fontSize;
  }

  public void setFontSize(int fontSize) {
    if (fontSize > 0) this.fontSize = fontSize;
    font = font.deriveFont((float) fontSize);
    setFont(font);
  }
 
  public Component getListCellRendererComponent( JList jlist, Object value,
			 int index, boolean isSelected, boolean cellHasFocus) {
    setText(value == null ? "" : value.toString());
    return this;
  }

}
