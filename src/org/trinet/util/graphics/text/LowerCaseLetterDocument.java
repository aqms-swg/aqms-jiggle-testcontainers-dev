package org.trinet.util.graphics.text;
import javax.swing.text.*; 

import java.awt.Toolkit;
import java.text.*;

public class LowerCaseLetterDocument extends LetterDocument {
    public LowerCaseLetterDocument() {}

    public LowerCaseLetterDocument(int cols) {
	strLength = cols;
    }

    public void insertString(int offs, String str, AttributeSet a) 
        throws BadLocationException {
        super.insertString(offs, str.toLowerCase(), a);
    }
}
