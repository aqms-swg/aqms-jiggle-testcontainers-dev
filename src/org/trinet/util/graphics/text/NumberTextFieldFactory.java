package org.trinet.util.graphics.text;
import java.awt.*;
import java.text.*;
import org.trinet.util.graphics.text.*;

public class NumberTextFieldFactory {
    protected static final String DEFAULT_PATTERN = "#.#";
    protected static final Font DEFAULT_FONT = new Font("Monospaced", Font.PLAIN, 12);

    protected static DecimalFormat createFormat(String pattern, boolean parseIntegerOnly) {
	String formatPattern = pattern;
	if (formatPattern == null) formatPattern = DEFAULT_PATTERN;
	DecimalFormat numberFormat  = (DecimalFormat) NumberFormat.getNumberInstance();
	if (numberFormat instanceof DecimalFormat) {
	   ((DecimalFormat) numberFormat).setGroupingUsed(false);
	   ((DecimalFormat) numberFormat).setDecimalSeparatorAlwaysShown(false);
	   ((DecimalFormat) numberFormat).applyPattern(formatPattern);
	   ((DecimalFormat) numberFormat).setParseIntegerOnly(parseIntegerOnly);
	}
	else throw new ClassCastException("createFormat - cannot create DecimalFormat.");	
	return numberFormat;
    }

    protected static NumberTextField createNumberTextField(int columns, DecimalFormat numberFormat, Font font,
								boolean absoluteValueOnly) {
//        FormattedNumberDocument fnd = new FormattedNumberDocument(numberFormat, columns, absoluteValueOnly); 
//	NumberTextField textField = new NumberTextField(fnd, columns+1, numberFormat );
        NumberTextField textField = new NumberTextField(
            new FormattedNumberDocument(numberFormat, columns, absoluteValueOnly), columns+1, numberFormat );
	if (font == null) textField.setFont(DEFAULT_FONT);
	else textField.setFont(font);
	return textField;
    }

    public static NumberTextField createInputField(int fieldColumns, boolean fieldIntegerOnly) {
	return createNumberTextField(fieldColumns, createFormat(DEFAULT_PATTERN, fieldIntegerOnly), DEFAULT_FONT, false);
    }

    public static NumberTextField createInputField(int fieldColumns, boolean fieldIntegerOnly,
				String fieldPattern, Font fieldFont, boolean absoluteValueOnly) {
	return createNumberTextField(fieldColumns, createFormat(fieldPattern, fieldIntegerOnly), fieldFont, absoluteValueOnly);
    }

} // end of NumberTextFieldFactory class

