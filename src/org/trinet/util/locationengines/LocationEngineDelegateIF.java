package org.trinet.util.locationengines;
import org.trinet.jasi.*;
import org.trinet.jasi.engines.*;
import org.trinet.util.gazetteer.*;
public interface LocationEngineDelegateIF extends SolutionEngineDelegateIF {
  public boolean hasLocationService();
  public LocationEngineIF getLocationEngine();
  public boolean locate(Solution sol);
  public void setChannelList(ChannelList chanList);
}
