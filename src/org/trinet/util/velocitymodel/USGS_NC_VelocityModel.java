package org.trinet.util.velocitymodel;

/** Hypoinv USGS Menlo Park model used to locate NC earthquakes. */
public class USGS_NC_VelocityModel extends UniformFlatLayerVelocityModel {

    //public static final double [] BOUNDARY_DEPTHS   = {0.0, 3.5, 23., 27.0 }; // removed per Lombard request 2009/03/19
    //public static final double [] LAYER_VELOCITIES  = {2.7, 5.7, 6.9, 8.05 }; // removed per Lombard request 2009/03/19

    public static final double [] BOUNDARY_DEPTHS   = {0.0, 3.5, 15., 25.0 }; // added per Lombard request 2009/03/19
    public static final double [] LAYER_VELOCITIES  = {4.0, 5.9, 6.8, 8.05 }; // added per Lombard request 2009/03/19

    public static final double DEFAULT_PS_RATIO = 1.78;


    public USGS_NC_VelocityModel()  {
        super("USGS_NC", BOUNDARY_DEPTHS, LAYER_VELOCITIES, DEFAULT_PS_RATIO);
    }

}
