package org.trinet.waveserver.rt;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;

import org.trinet.util.gazetteer.LatLonZ;
import org.trinet.jasi.ChannelCorr;
import org.trinet.jasi.ChannelResponse;
import org.trinet.util.Bits;
import org.trinet.util.CorrTypeIdIF;
import org.trinet.util.DateRange;

/** Implementation of the WaveClient/Server API Channel class.
* This subclass of TrinetSerial defines the attributes of a seismic network-station-channel.
* Data members can be imported from an serialized input byte stream or exported to a output byte stream.
* TrinetSerial subclasses are used by the WaveClient/Server classes to transport data values
* over the network. In the WaveClient application, as a result of a TCPMessage object being sent/received,
* Channel object data member values are converted to/from the byte stream protocol inside of Packet objects
* which define the message content that are transported through a TCPConn object socket connection.
* @see Packet
* @see TCPMessage
* @see TrinetSerial
* @see WaveClient
*/
public class Channel extends TrinetSerial implements Cloneable, Comparable {
/** Maximum total bytes allowed for serial transfer of data members over the network */
    public static final int MAX_SERIAL_BYTES = 128;

    public static final int NEW_MAX_BYTES_IN_NETWORK_STRING = 3; // new server format 01/18/2006 aww
    public static final int NEW_MAX_BYTES_IN_STATION_STRING = 6;
    public static final int NEW_MAX_BYTES_IN_CHANNEL_STRING = 4;
    public static final int NEW_MAX_BYTES_IN_LOCATION_STRING = 3;
    public static final int NEW_MAX_BYTES_IN_GAIN_UNITS_STRING = 20;

    public static final int OLD_MAX_BYTES_IN_NETWORK_STRING = 10; // old server format 01/18/2006 aww
    public static final int OLD_MAX_BYTES_IN_STATION_STRING = 10;
    public static final int OLD_MAX_BYTES_IN_CHANNEL_STRING = 10;
    public static final int OLD_MAX_BYTES_IN_LOCATION_STRING = 10;

    /** Set flag true to switch from old to new channel field byte buffer lengths, default is false. */
    public static boolean new_wire_format = false;

/** Maximum number of characters in a network identifier */
    private final int MAX_BYTES_IN_NETWORK_STRING = (new_wire_format) ?
        NEW_MAX_BYTES_IN_NETWORK_STRING : OLD_MAX_BYTES_IN_NETWORK_STRING ;
/** Maximum number of characters in a station identifier */
    private final int MAX_BYTES_IN_STATION_STRING = (new_wire_format) ?
        NEW_MAX_BYTES_IN_STATION_STRING : OLD_MAX_BYTES_IN_STATION_STRING ;
/** Maximum number of characters in a channel identifier */
    private final int MAX_BYTES_IN_CHANNEL_STRING = (new_wire_format) ?
        NEW_MAX_BYTES_IN_CHANNEL_STRING : OLD_MAX_BYTES_IN_CHANNEL_STRING ;
/** Maximum number of characters in a location identifier */
    private final int MAX_BYTES_IN_LOCATION_STRING = (new_wire_format) ?
        NEW_MAX_BYTES_IN_LOCATION_STRING : OLD_MAX_BYTES_IN_LOCATION_STRING ;

    private final int MAX_BYTES_IN_GAIN_UNITS_STRING = NEW_MAX_BYTES_IN_GAIN_UNITS_STRING;

/** Maximum number of characters in a type identifier */
    public static final int MAX_BYTES_IN_TELTYPE_STRING =  4;

    byte [] network = new byte [MAX_BYTES_IN_NETWORK_STRING];
    byte [] station = new byte [MAX_BYTES_IN_STATION_STRING];
    byte [] channel = new byte [MAX_BYTES_IN_CHANNEL_STRING];
    byte [] location = new byte [MAX_BYTES_IN_LOCATION_STRING];
    int teltype = 3; // 0 BB (BH,HH), 1 SM (HN), 2 SP(EH), 3=UNKNOWN;
    byte [] gainUnits = new byte [MAX_BYTES_IN_GAIN_UNITS_STRING];

/**  Samples per second. */
    double sampleRate;
/** Station channel latitude decimal degrees. */
    double latitude;
/** Station channel longitude decimal degrees. */
    double longitude;
/** Station channel elevation kilometers. */
    double elevation;
/** Station channel gain. */
    double gain;
/** Station channel Ml magnitude correction. */
    double mlCorrection;
/** Station channel Me magnitude correction. */
    double meCorrection;

/** Default constructor, initializes size of serial byte output buffer.*/
    public Channel() { super(MAX_SERIAL_BYTES); }

/** Constructor invokes the default constructor, initializes the network, station, and channel members.
* @exception java.lang.IllegalArgumentException input String length exceed the MAX_BYTExxxx size of the data member.
*/
    public Channel(String network, String station, String channel) {
        this(network,station,channel,"");
    }
    public Channel(String network, String station, String channel, String location) {
        this();

        if ( (network == null) || (station == null) || (channel == null) || (location == null) )
          throw new NullPointerException("Channel constructor null input network, station, channel or location string");
        byte [] byteArray = network.getBytes();
        if (byteArray.length > MAX_BYTES_IN_NETWORK_STRING)
                throw new IllegalArgumentException("Channel construction network String exceeds max length:\""+network+"\"");
        System.arraycopy(byteArray, 0, this.network, 0, byteArray.length);

        byteArray = station.getBytes();
        if (byteArray.length > MAX_BYTES_IN_STATION_STRING)
                 throw new IllegalArgumentException("Channel construction station String exceeds max length:\""+station+"\"");
        System.arraycopy(byteArray, 0, this.station, 0, byteArray.length);

        byteArray = channel.getBytes();
        if (byteArray.length > MAX_BYTES_IN_CHANNEL_STRING)
                 throw new IllegalArgumentException("Channel construction channel String exceeds max length:\""+channel+"\"");
        System.arraycopy(byteArray, 0, this.channel, 0, byteArray.length);

        byteArray = location.getBytes();
        if (byteArray.length > MAX_BYTES_IN_LOCATION_STRING)
                 throw new IllegalArgumentException("Channel construction location String exceeds max length:\""+location+"\"");
        System.arraycopy(byteArray, 0, this.location, 0, byteArray.length);

    }

/** Constructor initializes the data members values from an input array containing a network serialized form of the data members.
* @see TrinetSerial#fromByteArray(byte [])
*/
    public Channel(byte [] buffer) throws IOException {
        this();
        fromByteArray(buffer);
    }

/** Sets data members to values read from a network serialized form of these data values in the specified input stream.
* @see #writeDataMembers
* @see TrinetSerial#fromInputStream(InputStream)
* @see TrinetSerial#fromByteArray(byte [], int, int)
*/
    void readDataMembers(DataInputStream dataIn) throws IOException{
            dataIn.readFully(network);
            dataIn.readFully(station);
            dataIn.readFully(channel);
            // uncommented below when when new data format is sent over wire 05/03 aww
            dataIn.readFully(location);
            //
            sampleRate = dataIn.readDouble();
            latitude = dataIn.readDouble();
            longitude = dataIn.readDouble();
            elevation = dataIn.readDouble();
            gain = dataIn.readDouble();
            mlCorrection = dataIn.readDouble();
            meCorrection = dataIn.readDouble();
            teltype = dataIn.readInt();
            dataIn.readFully(gainUnits);
            // default to the jasi string when first byte is 0 -aww 20141109
            if (gainUnits[0] == 0 ) {
                Arrays.fill(gainUnits, (byte) 0);
                System.arraycopy("unknown".getBytes(), 0, gainUnits, 0, 7);
            }
    }

/** Write the data members in a network serialized form to the specified output stream.
* @see #readDataMembers
* @see TrinetSerial#toOutputStream(OutputStream)
* @see TrinetSerial#toByteArray()
*/
    void writeDataMembers(DataOutputStream dataOut) throws IOException {
            dataOut.write(network, 0, MAX_BYTES_IN_NETWORK_STRING);
            dataOut.write(station, 0, MAX_BYTES_IN_STATION_STRING);
            dataOut.write(channel, 0, MAX_BYTES_IN_CHANNEL_STRING);
            dataOut.write(location, 0, MAX_BYTES_IN_LOCATION_STRING);
            //
            dataOut.writeDouble(sampleRate);
            dataOut.writeDouble(latitude);
            dataOut.writeDouble(longitude);
            dataOut.writeDouble(elevation);
            dataOut.writeDouble(gain);
            dataOut.writeDouble(mlCorrection);
            dataOut.writeDouble(meCorrection);
            dataOut.writeInt(teltype);
            dataOut.write(gainUnits, 0, MAX_BYTES_IN_GAIN_UNITS_STRING);

            dataOut.flush();
    }

/** A "deep" copy, no shared member object references.  */
    public Object clone() {
        Channel chnl = null;
        try {
            chnl = (Channel) super.clone();
            chnl.network = (byte []) network.clone();
            chnl.station = (byte []) station.clone();
            chnl.channel = (byte []) channel.clone();
            chnl.location = (byte []) location.clone();
            chnl.gainUnits = (byte []) gainUnits.clone();
        }
        catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
        return chnl;
    }

/** Input object must be an instance of Channel.
*   Compares this object's and the input's network, station, channel, and location data members;
*   if either has an undefined location member, only compares the network,station, and channel data members.
* @return <pre>
*  0 if this object and the input object have equivalent network, station, channel, and location data values.
* -1 if any of the same member values of this object, in the same order, are less than the input's.
*  1 if any of the same member values of this object, in the same order, are greater than the input's.
* </pre>
* @exception java.lang.ClassCastException input object is not a instance of this class type.
*/
    public int compareTo(Object object) {
        if (this == object) return 0;
        Channel chnl = (Channel) object;
        int retVal = compareToZero(network, chnl.network);
        if (retVal != 0) return retVal;
        retVal = compareToZero(station, chnl.station);
        if (retVal != 0) return retVal;
        retVal = compareToZero(channel, chnl.channel);
        if (retVal != 0) return retVal;
        // if either location not specified (null string), compare net-sta-chan strings, Patrick's C++ implementation?
        return ( (location[0] == 0) || (chnl.location[0] == 0) ) ?
            retVal : compareToZero(location, chnl.location);
     }

/** Returns true if this object and the input have equivalent network, station, channel, and location
*   data members, if either has an undefined location member, only compares their network, station, and channel
*   members for equivalence.
*/
     public boolean equalsId(Channel object) {
        return (compareTo(object) == 0);
     }

/** Returns true only if the input object is an instance of this class and all data member values are equivalent.
*/
     public boolean equals(Object object) {
        if (this == object) return true;
        if (! super.equals(object)) return false;
        Channel chnl = (Channel) object;
        return
           ( equalsId(chnl) &&
            (teltype == chnl.teltype) &&
            (compareToZero(gainUnits, chnl.gainUnits) == 0) &&
            (sampleRate == chnl.sampleRate) &&
            (latitude == chnl.latitude) &&
            (longitude == chnl.longitude) &&
            (elevation == chnl.elevation) &&
            (gain == chnl.gain) &&
            (mlCorrection == chnl.mlCorrection) &&
            (meCorrection == chnl.meCorrection)
           ) ? true : false;
     }

/** Returns String a "space" delimited data members */
    public String toString() {
        StringBuffer sb = new StringBuffer(256);
        sb.append("Channel ");
        sb.append(toIdString(' '));
        sb.append(" ");
        sb.append(sampleRate);
        sb.append(" ");
        sb.append(latitude);
        sb.append(" ");
        sb.append(longitude);
        sb.append(" ");
        sb.append(elevation);
        sb.append(" ");
        sb.append(gain);
        sb.append(" ");
        sb.append((gainUnits[0] == 0 ) ? "unknown" : new String(gainUnits));
        sb.append(" ");
        sb.append(mlCorrection);
        sb.append(" ");
        sb.append(meCorrection);
        sb.append(" ");
        sb.append(teltype);

        return sb.toString();
    }

/** For debuging only, prints contents of the identifying byte arrays as "dot" delimited Strings.
* Strings have same length as buffers; does not truncate Strings at first zero-byte.
*/
    public void dumpIdString() {
        StringBuffer sb = new StringBuffer(32);
        sb.append(network);
        sb.append(".");
        sb.append(station);
        sb.append(".");
        sb.append(channel);
        sb.append(".");
        sb.append(new String(location).replace(' ', '-'));
        System.out.println("\"" + sb.toString() + "\"");
    }

/** Returns String of "period" delimited network, station, channel, and location data members.
*/
    public String toIdString() {
        return toIdString('.');
    }
/** Returns String of network, station, channel, and location data members delimited by the input character.
*/
    public String toIdString(char delimiter) {
        StringBuffer sb = new StringBuffer(32);
        for (int i=0; i < network.length; i++) {
          if (network[i] == 0) break;
          sb.append((char)network[i]);
        }
        sb.append(delimiter);
        for (int i=0; i < station.length; i++) {
          if (station[i] == 0) break;
          sb.append((char)station[i]);
        }
        sb.append(delimiter);
        for (int i=0; i < channel.length; i++) {
          if (channel[i] == 0) break;
          sb.append((char)channel[i]);
        }
        sb.append(delimiter);
        if (location.length > 0) {
          for (int i=0; i < location.length; i++) {
            if (location[i] == 0) break;
            if ( (char) location[i] == ' ') sb.append('-');
            else sb.append((char)location[i]);
          }
        }
        return sb.toString();
    }

/** Convenience wrapper of System.out.println(toIdString()).*/
    public void printId() {
        System.out.println(toIdString());
    }

/** Convenience wrapper of System.out.println(toString()).*/
    public void print() {
        System.out.println(toString());
    }

    /** Return a org.trinet.jasi.Channel for this Channel.
     * @see org.trinet.jasi.Channel */
    public org.trinet.jasi.Channel toJasiChannel() {
      org.trinet.jasi.Channel ch = org.trinet.jasi.Channel.create();

      ch.setNet(Bits.bytesToString(network));
      ch.setSta(Bits.bytesToString(station));
      ch.setSeedchan(Bits.bytesToString(channel));
      ch.setLocation(Bits.bytesToString(location));

// the Wavepool seems to return '0' for all these...
      ch.setSampleRate(sampleRate);
      // Z in LatLonZ references a depth positive downward
      ch.setLatLonZ(new LatLonZ(latitude, longitude, -elevation)); // sign change here aww 6/15/2004

      // aww modified for response, correction maps in jasi Channel
      //ch.gain.setValue(gain); //aww
      DateRange dr = new DateRange(); // implied no limit
      ChannelResponse cr = ChannelResponse.create();
      cr.setDateRange(dr);
      cr.setChannelId(ch);
      cr.setGainValue(gain);
      cr.setGainUnits(new String(gainUnits));
      ch.setResponse(cr);

      //ch.meCorr.setValue(meCorrection); //aww
      ChannelCorr cc = ChannelCorr.create();
      cc.setDateRange(dr);
      cc.setChannelId(ch);
      cc.setDateRange(dr);
      cc.setCorrType(CorrTypeIdIF.ME);
      cc.setCorrValue(meCorrection);
      //CorrTypeIdIF.FINALIZED CorrTypeIdIF.ESTIMATED CorrTypeIdIF.DEFAULTED
      //Which should be the setting for corrStatus be below ?
      cc.setCorrStatus(CorrTypeIdIF.DEFAULTED); // ?

      //ch.mlCorr.setValue(mlCorrection); //aww
      cc = ChannelCorr.create();
      cc.setDateRange(dr);
      cc.setChannelId(ch);
      cc.setCorrType(CorrTypeIdIF.ML);
      cc.setCorrValue(mlCorrection);
      //CorrTypeIdIF.FINALIZED CorrTypeIdIF.ESTIMATED CorrTypeIdIF.DEFAULTED
      //Which should be the setting for corrStatus be below ?
      cc.setCorrStatus(CorrTypeIdIF.DEFAULTED); // ?

      return ch;

    }
}
