package org.trinet.waveserver.rt;
import java.util.*;
import org.trinet.util.DateTime;

/** Implementation of the TRINET WaveClient API DataCounts class.
* This class represents a container for time series of contiguous data
* The data are described by a starting timestamp and List of data objects.
* The List objects are typically immutable, Integer objects representing
* observed digital counts amplitude of a time series sample. The number
* of samples is assumed to be equivalent to the List size.
* A uniform sampling rate is assumed. Neither the sampling rate nor the data
* source is known to this class, this association must be managed externally.
* @see Waveform
* @see WaveClient
*/
public class DataCounts implements Cloneable, Comparable {

/** Start time of the first data sample in the data List. */
    DateTime startTimestamp = null; // for UTC -aww 2008/04/01

//  int numberOfSamples; // member an artifact from C++ version, instead just use data.size()

/** List of data sample objects. */
    List dataList = null;

/** Constructor creates empty sample List of the specified minimum capacity whose
* first sample time is that specified by a clone of the input timestamp. */
    DataCounts(DateTime startTimestamp, int minCapacity) { // for UTC -aww 2008/04/01
        this.startTimestamp = (DateTime) startTimestamp.clone();
        dataList = new ArrayList(minCapacity);
    }

/** Constructor initializes data member with aliases to the input references. */
    DataCounts(DateTime startTimestamp, List dataList) { // for UTC -aww 2008/04/01
        this.startTimestamp = (DateTime) startTimestamp.clone();
        this.dataList = dataList;
    }

/** Constructor does a shallow clone of the of the input object's data members.
* The timestamp and list objects are cloned, the list contents are aliased. */
    DataCounts(DataCounts dc) {
        startTimestamp = (DateTime) dc.startTimestamp.clone(); // for UTC -aww 2008/04/01
        dataList = (List) ((ArrayList) dc.dataList).clone();
    }

/** Returns the value of the starting sample timestamp in seconds since java.util.Date(0). */
    public double getStartTimeSecs() {
        return startTimestamp.getTrueSeconds(); // for UTC -aww 2008/04/01
    }

/** Creates shallow clone of the timestamp and a shallow clone of the sample list. 
*   The list contents are not cloned.
*/
    protected Object clone() {
        DataCounts dc = null;
        try {
            dc = (DataCounts) super.clone();
            dc.startTimestamp = (DateTime) startTimestamp.clone(); // for UTC -aww 2008/04/01
            dc.dataList = (List) ((ArrayList) dataList).clone();
        }
        catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
            return null;
        }
/* example of a deep copy list for Integer objects - don't copy unless proven necessary
        Iterator iter = dataList.iterator();
        dc.dataList.clear();
        while (iter.hasNext()) {
            dc.dataList.add(new Integer( ((Integer) iter.next()).intValue() ));
        }
*/
        return dc;
    }

/** Input object must be an instance of DataCounts
* Only the starting timestamp members are compared;
* values of the data samples in the List are not compared. 
* @return <pre>
* -1 this object's starting timestamp value is less than the input object's value.
*  0 this object's timestamp value is equivalent to the input object's value.
*  1 this object's timestamp value is greater than the input object's value.
* </pre>
* @exception java.lang.NullPointerException input object is null
* @exception java.lang.ClassCastException input object is not an instance of this class.
*/
    public int compareTo(Object object) {
        return startTimestamp.compareTo( ((DataCounts) object).startTimestamp);
    }

/** Returns true only if the input object is an instance of this class and its timestamp
*   and its data list element values are all equivalent to this object's values.
*/
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        DataCounts dc = (DataCounts) object;
        return (startTimestamp.equals(dc.startTimestamp) && dataList.equals(dc.dataList)) ? true : false;
    }

/** Returns String concatenating the labeled values of the the timestamp 
* and the number of declared samples in the list.
*/
    public String toString() {
        return "startTimestamp: " + startTimestamp + " sampleListSize: " + dataList.size();
    }
}
