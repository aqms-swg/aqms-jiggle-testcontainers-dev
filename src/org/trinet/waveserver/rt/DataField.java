package org.trinet.waveserver.rt;
import java.io.*;
import java.util.*;

/** Implementation of a DataField class of the WaveClient/Server API.
* This subclass of TrinetSerial encapsulates the data values that comprise a TCPMessage.
* A DataField instance is categorized by type, length, and value data members.
* Possible "types" of data "values" are described by the DataFieldConstants interface.
* Methods are provided for setting/retrieving the different types of data values.
* A TCPMessage contains a collection of one or more DataField objects whose values
* are read/written from/to the serialized byte stream of Packet objects that comprise the
* TCPMessage and are constructed by a TCPConn object at the request of the WaveClient/Server.
* @see DataFieldConstants
* @see Packet
* @see TCPConn
* @see TCPMessage
* @see WaveClient
*/
public class DataField extends TrinetSerial implements Cloneable, DataFieldConstants {

/** Identifier tag associating a data "type" with the serialized byte represention of a data value. */
    int dataType;

/** Bytes in the data value */
    int dataLength;

/** The data value represented in its serialized stream network format */
    byte [] dataValue;

/** Default constructor initializes data type to DF_NONE. */
    DataField() {
        dataType = DF_NONE;
    }

/** Constructor initializes the data members from an input array containing a network serialized form of the data members. 
* @exception java.io.IOException error occurred parsing the data member values form the byte array stream.
* @see TrinetSerial#fromByteArray(byte [])
*/
    DataField(byte [] inBuffer) throws IOException {
        fromByteArray(inBuffer);
    }


/** Sets data members to values read from a network serialized form of these data values in the specified input stream. 
* @exception java.io.IOException error occurred parsing the member values from stream, or parsed value violates constraints.
*/
    void readDataMembers(DataInputStream dataIn) throws IOException {
        dataType = dataIn.readInt();
        dataLength = dataIn.readInt();

        switch (dataType) {
            case DF_NONE:
              throw new IOException("DataField.readDataMembers() cannot have data type of DF_NONE");
            case DF_INTEGER:
              if (dataLength != DF_SIZE_OF_INT) {
                throw new IOException("DataField.readDataMembers() Invalid integer size");
              }
              break;
            case DF_DOUBLE:
              if (dataLength != DF_SIZE_OF_DOUBLE) {
                throw new IOException("DataField.readDataMembers() Invalid double size");
              }
              break;
            case DF_STRING:
            case DF_BINARY:
              if ((dataLength < 0) || (dataLength > DF_MAX_DATA_BYTES)) {
                throw new IOException("DataField.readDataMembers() Invalid string/binary size");
              }
              break;
            default:
                throw new IOException("DataField.readDataMembers() Invalid data type identifier");
        }

        dataValue = new byte[dataLength];
        dataIn.readFully(dataValue, 0, dataLength);
        super.defaultBytesInOutputBuffer = DF_HEADER_BYTES + dataLength;
    }

/** Set the value type to DF_INTEGER and its length to DF_SIZE_OF_INT. Sets value to the input value. 
* @exception java.io.IOException input value cannot by represented by a byte output stream value.
*/
    void setValue(int input) throws IOException {

        dataType = DF_INTEGER;
        dataLength = DF_SIZE_OF_INT;
        super.defaultBytesInOutputBuffer = DF_HEADER_BYTES + DF_SIZE_OF_INT;

        ByteArrayOutputStream bytesOutStream = new ByteArrayOutputStream(DF_SIZE_OF_INT);
        DataOutputStream dataOut = new DataOutputStream(bytesOutStream);

        try {
            dataOut.writeInt(input);
            dataOut.flush();
            dataValue = bytesOutStream.toByteArray();
        }
        finally {
            try {
                dataOut.close();
            }
            catch (IOException ex) {ex.printStackTrace();}
        }
    }

/** Set the value type, length appropiate for DF_DOUBLE. Sets value to the input value. 
* @exception java.io.IOException input value cannot by represented by a byte output stream.
*/
    void setValue(double input) throws IOException {

        dataType = DF_DOUBLE;
        dataLength = DF_SIZE_OF_DOUBLE;
        super.defaultBytesInOutputBuffer = DF_HEADER_BYTES + DF_SIZE_OF_DOUBLE;

        ByteArrayOutputStream bytesOutStream = new ByteArrayOutputStream(DF_SIZE_OF_DOUBLE);
        DataOutputStream dataOut = new DataOutputStream(bytesOutStream);

        try {
            dataOut.writeDouble(input);
            dataOut.flush();
            dataValue = bytesOutStream.toByteArray();
        }
        finally {
            try {
                dataOut.close();
            }
            catch (IOException ex) {ex.printStackTrace();}
        }
    }

/** Set the value type, length appropiate for DF_STRING. Sets value to a copy of the input string. 
* @exception java.lang.IllegalArgumentException str.length()>DF_MAX_DATA_BYTES.
* @exception java.lang.NullPointerException input String is null.
*/
    void setValue(String inString) {

        int inLength = inString.length();
        if (inLength > DF_MAX_DATA_BYTES) {
            throw new IllegalArgumentException("DataField setValue(): Error input String too long");
        }

        dataType = DF_STRING;
        dataLength = inLength;
        super.defaultBytesInOutputBuffer = DF_HEADER_BYTES + inLength;

        dataValue = inString.getBytes();

    }

/** Set the value type, length appropiate for DF_BINARY. Sets value to a copy of the input buffer.
* @exception java.lang.IllegalArgumentException str.length()>DF_MAX_DATA_BYTES.
* @exception java.lang.NullPointerException input buffer is null.
*/
    void setValue(byte [] inBuffer) throws IllegalArgumentException {
        int inLength = inBuffer.length;
        if (inLength > DF_MAX_DATA_BYTES) {
            throw new IllegalArgumentException("DataField setValue(): Error input byte buffer too long");
        }

        dataType = DF_BINARY;
        dataLength = inLength;
        super.defaultBytesInOutputBuffer = DF_HEADER_BYTES + inLength;

        dataValue = new byte[inLength];
        System.arraycopy(inBuffer, 0, dataValue, 0, inLength);
    }

/** Returns an object encapsulating the "value" of the the data value member.
* Returns either an Integer, Double, String, or a byte [] based upon the declared data type.
* @exception java.io.IOException data value cannot be parsed as inferred from the declared type.
* @exception UnknownTrinetDataTypeException data type is not defined in the DataFieldConstants interface
* @see DataFieldConstants
*/
    Object getValue() throws IOException, UnknownTrinetDataTypeException {
        Object retVal = null;
        switch (dataType) {
            case DF_INTEGER:
                DataInputStream dataIn = new DataInputStream(new ByteArrayInputStream(dataValue));
                try {
                    retVal = Integer.valueOf(dataIn.readInt());
                }
                finally {
                    try {
                        dataIn.close();
                    }
                    catch (IOException ex) {ex.printStackTrace();}
                }
                break;
            case DF_DOUBLE:
                dataIn = new DataInputStream(new ByteArrayInputStream(dataValue));
                try {
                    retVal = Double.valueOf(dataIn.readDouble());
                }
                finally {
                    try {
                        dataIn.close();
                    }
                    catch (IOException ex) {ex.printStackTrace();}
                }
                break;
            case DF_STRING:
                retVal = new String(dataValue, 0, dataLength);
                break;
            case DF_BINARY:
                byte [] byteArray = new byte [dataLength];
                System.arraycopy(dataValue, 0, byteArray, 0, dataLength);
                retVal = byteArray;
                break;
            default:
                throw new UnknownTrinetDataTypeException("Datafield getValue(): Error unknown type:" + dataType);
        }
        return retVal;
    }

/** Writes the data members values in a network serialized form to the specified output stream.
* @exception java.io.IOException error occurred writing the data to the stream.
*/
    void writeDataMembers(DataOutputStream dataOut) throws IOException {
        dataOut.writeInt(dataType);
        dataOut.writeInt(dataLength);
        dataOut.write(dataValue, 0, dataLength);
    }

/** Returns the total bytes length of the serialized member values (data_type bytes + value_length bytes + value bytes).
*/
    public int getSerializedFieldLength() {
        return dataLength + 2*DF_SIZE_OF_INT;
    }
    
/** Returns the total bytes length of only the data value member. */
    public int getDataLength() {
        return dataLength;
    }

/** Returns value of data type member. */
    public int getDataType() {
        return dataType;
    }

/** Returns true if data type == DF_NONE. */
    public boolean isNull() {
        return (dataType == DF_NONE);
    }

/** Returns true only if the input object is an instance of this class and 
*   its type, length, and "value" members have equivalent values.
*/
    public boolean equals(Object object) {
        if (this == object) return true;
        else if (! super.equals(object) ) return false;

        DataField df = (DataField) object;
        return ( (dataType == df.dataType) &&
             (dataLength == df.dataLength) &&
             Arrays.equals(dataValue, df.dataValue) ) ? true : false;
    }

/** Creates a deep copy of this object. */
    public Object clone() {
        DataField df = null;
        try {
            df = (DataField) super.clone();
            df.dataValue = (byte []) dataValue.clone(); // creates image of old array as new array object
        }
        catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
        return df;
    }

/** Returns String concatenation of the labeled values of data type, length, and getValue().toString(). */
    public String toString() {
        String retVal =  "type: " + dataType + " length: " + dataLength;
        try {
            if (dataType != DF_BINARY) {
                retVal += " value: " + getValue().toString(); 
            }
            else {
                retVal += " value: DF_BINARY byte array";
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return retVal;
    }
}
