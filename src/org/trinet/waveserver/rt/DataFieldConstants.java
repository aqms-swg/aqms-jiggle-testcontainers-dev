package org.trinet.waveserver.rt;
/** Interface of constants defining TRINET WaveClient/Server API DataField class data member attributes.*/
public interface DataFieldConstants {
    public static final int DF_NONE = 0;
    public static final int DF_INTEGER = 1;
    public static final int DF_DOUBLE = 2;
    public static final int DF_STRING = 3;
    public static final int DF_BINARY = 4;
    public static final int DF_SIZE_OF_INT = 4;
    public static final int DF_SIZE_OF_DOUBLE = 8;
    public final static int DF_MAX_SERIAL_BYTES = 8192;
    public final static int DF_HEADER_BYTES = 8;
    public final static int DF_MAX_DATA_BYTES = DF_MAX_SERIAL_BYTES - DF_HEADER_BYTES;
}

