package org.trinet.waveserver.rt;
import java.util.*;
import org.trinet.util.DateTime;

/** Implementation of the TRINET WaveClient SimpleTimeRange class.
* This class is similar to the TimeWindow class except it lacks network
* "serialization" implementation. Both class implement the TimeRange interface.
* The implementationof the TimeWindow class interface by delegating to this class.
* The WaveClient class uses the TimeRange implementions to define the contiguous
* waveform timeseries start and end times for a specific seismic channel.
* A WaveClient getTimes() request returns a collection of these objects. 
*<p>
*<b> Note public constructor and getXXX methods alias the data member references,
*    thus the member data reflect any external modification to the aliases.
*    If preservation of the original data is required, first clone the external alias
*    before modifying its contents.
*</b>
*
*/
public class SimpleTimeRange implements TimeRange, Cloneable, Comparable {

/** Starting time */
    DateTime startTimestamp = null; // for UTC -aww 2008/04/01

/** Ending time */
    DateTime endTimestamp = null; // for UTC -aww 2008/04/01

/** Constructor aliases starting and ending timestamp members to the input object reference values.
* @exception java.lang.IllegalArgumentException <pre>
*  startTimestamp == null || endTimestamp  == null
*  endTimestamp < startTimestamp
*  </pre>
*/
    SimpleTimeRange(DateTime startTimestamp, DateTime endTimestamp) { // for UTC -aww 2008/04/01
        this.startTimestamp = startTimestamp;
        this.endTimestamp = endTimestamp;
        if (! isValid())
            throw new IllegalArgumentException("SimpleTimeRange constructor start or end times violate input constraints");
    }

/** Returns an alias to the this instance. */
    public TimeRange getTimeRange() {
        return this;
    }

/** Returns an alias to the start timestamp reference. */
    public DateTime getStartTimestamp() { // for UTC -aww 2008/04/01
        return startTimestamp;
    }

/** Returns an alias to the end timestamp reference. */
    public DateTime getEndTimestamp() { // for UTC -aww 2008/04/01
        return endTimestamp;
    }

/** Returns the values of the start time in seconds relative to Date(0l) == GMT Jan 1, 1970. */
    public double getStartTimeSecs() {
        return startTimestamp.getTrueSeconds(); // for UTC -aww 2008/04/01
    }

/** Returns the values of the end time in seconds relative to Date(0l) == GMT Jan 1, 1970. */
    public double getEndTimeSecs() {
        return endTimestamp.getTrueSeconds(); // for UTC -aww 2008/04/01
    }

/** Returns the timegap seconds between the start time of the input TimeRange and the end time of this instance. */
    public double timeGapSeconds(TimeRange timeWindowNext) {
        return timeWindowNext.getStartTimeSecs() - this.getEndTimeSecs(); // for UTC -aww 2008/04/01
    }

/** Returns the timegap seconds between the input timestamp and the end time of this instance. */
    public double timeGapSeconds(DateTime nextStartTimestamp) { // for UTC -aww 2008/04/01
        return nextStartTimestamp.getTrueSeconds() - getEndTimeSecs();
    }

/** True if start time is after date. */
    public boolean after(Date date) {
        return startTimestamp.after(date);
    }

/** True if start time is after Timewindow end time. */
    public boolean after(TimeRange timeRange) {
        return after(timeRange.getEndTimestamp()); 
    }

/** True if ending time is before date. */
    public boolean before(Date date) {
        return endTimestamp.before(date);
    }

/** True if ending time is before date. */
    public boolean before(TimeRange timeRange) {
        return before(timeRange.getStartTimestamp()); 
    }

/** True if this object's time range excludes input date. */
    public boolean excludes(Date date) {
        return ! includes(date);
    }

/** True if this object's time range excludes input object's range. */
    public boolean excludes(TimeRange timeRange) {
        return ! overlaps(timeRange.getStartTimestamp(), timeRange.getEndTimestamp());
    }

/** True if this object's time range excludes input range. */
    public boolean excludes(Date start, Date end) {
        return ! overlaps(start, end);
    }

/** True if this object's time range includes input object's range. */
    public boolean includes(TimeRange timeRange) {
        return includes(timeRange.getStartTimestamp(), timeRange.getEndTimestamp()); 
    }

/** True if this object's time range includes input range. */
    public boolean includes(Date start, Date end) {
        return ! ( startTimestamp.after(start) || endTimestamp.before(end) );
    }

/** True if this object's time range includes input date. */
    public boolean includes(Date date) {
        return ! ( startTimestamp.after(date) || endTimestamp.before(date) );
    }

/** True if this object's time range overlaps input object's range. */
    public boolean overlaps(TimeRange timeRange) {
        return overlaps(timeRange.getStartTimestamp(), timeRange.getEndTimestamp());
    }

/** True if this object's time range overlaps input range. */
    public boolean overlaps(Date start, Date end) {
        return ! (startTimestamp.after(end) || endTimestamp.before(start) );
    }

/** True if this object's time range lies within input object's range. */
    public boolean within(TimeRange timeRange) {
        return timeRange.includes(this); 
    }

/** True if this object's time range lies within input range. */
    public boolean within(Date start, Date end) {
        return ! ( startTimestamp.before(start) || endTimestamp.after(end) );
    }

/** Returns a "deep copy" of this instance. */
    public Object clone() {
        SimpleTimeRange timeRange = null;
        try {
            timeRange = (SimpleTimeRange) super.clone();
            timeRange.startTimestamp =  (DateTime) startTimestamp.clone(); // for UTC -aww 2008/04/01
            timeRange.endTimestamp = (DateTime) endTimestamp.clone(); // for UTC -aww 2008/04/01
        }
        catch (CloneNotSupportedException ex) {
            ex.printStackTrace();
        }
        return timeRange;
    }

/** Returns true only if the input object is an instance of TimeRange and
* its starting and ending timestamps are equivalent to this object's values.
*/
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || ! (object instanceof TimeRange)) return false;
        TimeRange timeRange = (TimeRange) object;
        return (startTimestamp.equals(timeRange.getStartTimestamp()) &&
                endTimestamp.equals(timeRange.getEndTimestamp())) ? true : false;
    }

/** Input object must in an instance of TimeRange. 
* @return <pre>
* -1 this object's starting timestamp value is less than the input object's value,
*    or it is equivalent and this object's ending timestamp value is less than the input object's value.<br>
*  0 this object's starting and ending timestamp values are equivalent to the input object's values.<br>
*  1 this object's starting timestamp value is greater than the input object's value,
*    or it is equivalent and its ending timestamp value is greater than the input object's value.
* </pre>
* @exception java.lang.NullPointerException input object is null
* @exception java.lang.ClassCastException input object is not an instance of TimeRange.
*/
    public int compareTo(Object object) {
        TimeRange timeRange = (TimeRange) object;
        int retVal = startTimestamp.compareTo(timeRange.getStartTimestamp());
        if (retVal != 0) return retVal;
        return (endTimestamp.compareTo(timeRange.getEndTimestamp()) < 0 ) ? -1 : 1;
    }

/** Returns true if endTimetamp < startTimestamp. */
    public boolean isReversed() {
        return (startTimestamp.compareTo(endTimestamp) > 0);
    }

/** Returns true if ! isReversed(), and neither timestamp is null. */
    public boolean isValid() {
        if (startTimestamp == null || endTimestamp == null) return false;
        return ! isReversed();
    }

/** Returns String concatenation of starting and ending timestamps 
* These times are separated by " => ".
*/
    public String toString() {
        StringBuffer sb = new StringBuffer(80);
        sb.append("TimeRange ");
        sb.append(startTimestamp);
        sb.append(" => ");
        sb.append(endTimestamp);
        return sb.toString();
    }

/** Convenience wrapper for System.out.println(toString()). */
    public void print() {
        System.out.println(toString());
    }
}
