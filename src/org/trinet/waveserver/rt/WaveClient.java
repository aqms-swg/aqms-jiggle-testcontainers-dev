package org.trinet.waveserver.rt;
import java.util.*;
import java.io.*;
import java.net.*;
import org.trinet.jasi.JasiTimeBoundedData;
import org.trinet.util.*;
import org.trinet.jasi.seed.SeedReader;

/* NOTE - A patch to fix verbose logging of data recovery failure:
        if (retVal == TN_FAILURE) retVal = TN_NODATA;   // TEMPORARY PATCH UNTIL DWS CODES FIXED
   is inserted in all doGetXXX() routines for CASE TN_TCP_ERROR_RESP messages values.
*/

/**
 * Note: Should probably be named WaveClientTN (see super createWaveClient()).
 *
 * Implementations of a WaveClient API adapted with modifications from the
* original "TRINET C++ WaveClient API" by Patrick Small. Methods allow the
* recovery of data from a list of server connections created using the
* addServer() methods. The getXXX() methods can then be used to retrieve data
* from the server list. Note that the getXXX() methods usually return the data
* into a user initialized input parameter object and the methods return a status
* code indicating success or failure. Some methods throw exceptions if an error
* is encountered during processing. Refer the the TriNet WaveClient/WaveServer
* documentation for further descriptions of the TCP message protocols and the
* roles served by the classes in this package.
*<p>
*<b> Note - some methods may alias input/output parameter object references,
*    when aliased, the data members reflect any external modification to the data.
*    If preservation of the original object data is required, as a precaution clone
*    the data of the external reference before modifying or setting the data values.
*</b>
*
* @see TrinetReturnCodes
* @see TrinetTCPMessageTypes
* @see TCPConn
* @see TCPMessage */
public class WaveClient extends org.trinet.util.WaveClient implements AutoCloseable, TrinetReturnCodes, TrinetTCPMessageTypes {

    
  /** Constant identifying rapid wave server default pool size in milliseconds */
      public static final long RWS_MAX_MILLISECS_IN_POOL = 8640000l; // 1 day

  /** Constant identifying rapid wave server (memory based) default port number*/
      public static final int RWS_PORT_ID = 6500;

  /** Constant identifying disk wave server (file based) default port number*/
      public static final int DWS_PORT_ID = 6501;

  /** Constant identifying primary server role */
      public static final int TN_PRIMARY = 1;

  /** Maximum message sequence number value. */
      public static final int DEFAULT_MAX_SEQNUM = 2048;

  /** Initial collection size buckets. */
      private static final int INITIAL_CAPACITY = 7;

  /** Collection storing server connections. */
    //private Map servers = new Hashtable(INITIAL_CAPACITY);
    private ArrayList servers = new ArrayList(INITIAL_CAPACITY);

/** TCPMessage number generator */
    private Counter sequenceNumberCounter = null;

    private BenchMark bm = null;

/** Default constructor, default waveform modes: ignore time gaps and verifies waveforms.
*   Initializes request message sequence number to zero.
*/
    public WaveClient() {
        sequenceNumberCounter = new Counter(0, DEFAULT_MAX_SEQNUM);
    }

/** Default constructor, sets mode to ignore time gaps, returning all time segments.
*   Initializes request message sequence number to zero.
*   @param maxRetries          number of server reconnections to attempt if connection is broken or input timesout.
*   @param maxTimeoutMillisecs timeout milliseconds to wait for input to complete before attempting a reconnection.
*   @param verifyWaveforms     true == (default) check all packet headers for data consistency; false == accept packets as is.
*   @param truncateAtTimeGap   false == (default) return all time-series segments; false == return only time-series before 1st gap.
*   @exception java.lang.IllegalArgumentException input parameter < 0
*/
    public WaveClient(int maxRetries, int maxTimeoutMilliSecs, boolean verifyWaveforms, boolean truncateAtTimeGap) {
        if (maxRetries < 0 || maxTimeoutMilliSecs < 0)
            throw new IllegalArgumentException("WaveClient constructor invoked with input parameter < 0");

//  removed -  @param maxSequenceNumber   maximum TCPMessage sequence number for the generated request messages.
//        maxSequence < 0 throw new Illegal ...
//        sequenceNumberCounter = new Counter(0, maxSequenceNumber);

        setMaxRetries(maxRetries);
        setMaxTimeoutMilliSecs(maxTimeoutMilliSecs);
        setWaveformVerify(verifyWaveforms);
        setTruncateAtTimeGap(truncateAtTimeGap);
        sequenceNumberCounter = new Counter(0, DEFAULT_MAX_SEQNUM);
    }
/** constructor sets client properties to the values parsed from the specified input property file.<p>
*<b>
* Valid properties:<br>
*</b>
* <pre>
* keyString                valueString<br>
* server            name:port name2:port name3:port ... ("\" at EOL continues theinput on next line)
* verifyWaveforms   true or false                       (default=true)
* truncateAtTimeGap true or false                       (default=false)
* connectTimeout    int >0                              (default=10000 ms)
* maxTimeout        int >= 0                            (default=30000 ms, 0 => infinite, socket reads)
* maxRetries        int >= 0                            (default=3 reconnects attempted on socket error)
* </pre>
*   @exception WaveClientException input property file not found; invalid input property specification
*
*
*/
    public WaveClient(String propertyFileName) throws WaveClientException {
        setup(propertyFileName);
    }

/** Setup the waveserver using properties from this property file.
 *<b>
 * Valid properties:<br>
 *</b>
 * <pre>
 * keyString                valueString<br>
 * server            name:port name2:port name3:port ... ("\" at EOL continues the input on next line)
 * verifyWaveforms   true or false                       (default=true)
 * truncateAtTimeGap true or false                       (default=false)
 * connectTimeout    int >0                              (default=10000 ms)
 * maxTimeout        int >= 0                            (default=30000 ms, 0 => infinite, socket reads)
 * maxRetries        int >= 0                            (default=3 reconnects attempted on socket error)
 * </pre>
 *   @exception WaveClientException input property file not found; invalid input property specification
 */
    public void setup(String propertyFileName) throws WaveClientException {

        int maxSequenceNumber = DEFAULT_MAX_SEQNUM;
        String keyStr = null;
        String valueStr = null;
        String prefix = "waveclient." + name + ".";
        try {

            PropertyResourceBundle propRsrcBdl = new PropertyResourceBundle(new FileInputStream(propertyFileName));
            Enumeration keys = propRsrcBdl.getKeys();

            while (keys.hasMoreElements()) {

                keyStr = (String) keys.nextElement();
                valueStr = propRsrcBdl.getString(keyStr);

                if (keyStr.equalsIgnoreCase(prefix+"debug")) {
                    debug = Boolean.getBoolean(valueStr);
                }
                else if (keyStr.equalsIgnoreCase(prefix+"servers")) {
                    StringTokenizer strToke = new StringTokenizer(valueStr, " :\t\n\r\f");
                    if ( strToke.countTokens()%2 != 0 )
                        throw new WaveClientException("invalid server property string; must be serverName:portNumber pairs");
                    while (strToke.hasMoreTokens()) {
                        String serverName = strToke.nextToken();
                        int serverPort = Integer.parseInt(strToke.nextToken());
                        this.addServer(serverName, serverPort);
                    }
                }
                else if (keyStr.equalsIgnoreCase(prefix+"verifyWaveforms")) {
                    if (valueStr.equalsIgnoreCase("false")) verifyWaveforms = false;
                }
                else if (keyStr.equalsIgnoreCase(prefix+"truncateAtTimeGap")) {
                    if (valueStr.equalsIgnoreCase("true")) truncateAtTimeGap = true;
                }
                else if (keyStr.equalsIgnoreCase(prefix+"maxTimeout")) {
                    setMaxTimeoutMilliSecs(Integer.parseInt(valueStr));
                }
                else if (keyStr.equalsIgnoreCase(prefix+"connectTimeout")) {
                    setConnectTimeout(Integer.parseInt(valueStr));
                }
                else if (keyStr.equalsIgnoreCase(prefix+"maxRetries")) {
                    setMaxRetries(Integer.parseInt(valueStr));
                }
                else if (keyStr.equalsIgnoreCase(prefix+"maxSequence")) {
                    maxSequenceNumber = Integer.parseInt(valueStr);
                }
            }
            sequenceNumberCounter = new Counter(0, maxSequenceNumber);
        }
        catch (Exception ex)  {
            System.err.print("ERROR >>> WaveClient.setup() caught exception ");
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
            throw new WaveClientException("WaveClient setup failed; propertyFile: " + propertyFileName  +
            " key: " + keyStr + " value: " + valueStr);
        }
    }

    public void setDebug(boolean tf) {
        debug = tf;
        TCPConn.debug = debug;
        JasiTimeBoundedData.debug = debug;
    }

/** Returns the maximum TCPMessage number allowed for the generated request messages. */
    public int getMaxSequenceNumber() {
        return sequenceNumberCounter.rolloverValue;
    }


/** Sets the maximum timeout milliseconds to allowed for input from a server responses to complete
*   before an java.io.InterruptedIOException is thrown by client socket.
*   @exception java.lang.IllegalArgumentException input parameter < 0
*/
    public int setMaxTimeoutMilliSecs(int millisecs) {
        if (millisecs < 0)
            throw new IllegalArgumentException("WaveClient.setMaxTimeoutMilliSecs(int) input parameter < 0");
        maxTimeout = millisecs;
        //Collection connList = servers.values();
        //Iterator iter = connList.iterator();
        Iterator iter = servers.iterator();
        while(iter.hasNext()){
            ((TCPConnClient) iter.next()).setDefaultTimeout(maxTimeout);
        }
        return maxTimeout;
    }


/** Returns the number of servers currently known by this WaveClient
*/
    public int numberOfServers() {
        return servers.size();

    }
/** Returns an array of Strings each string describes a server known by this WaveClient.
*/
    public String [] listServers() {
        String [] clients = new String [servers.size()];
        int index = 0;
        //Set keySet = servers.keySet();
        //Iterator iter = keySet.iterator();
        //while (iter.hasNext()) {
        //    clients[index] = ((Client) iter.next()).toString();
        //    index++;
        //}
        Iterator iter = servers.iterator();
        while (iter.hasNext()) {
            clients[index] = ((TCPConnClient) iter.next()).client.toString();
            index++;
        }
        return clients;
    }

/** Adds the specified host and port number to the polled servers collection.
* Returns true if successful, false if error occurs, or server already is in list.
*/
    public boolean addServer(String host, int port) {
        return addServer(new Client(host, port)); // InetAddress ?
    }

/** Adds the host and port number specified in the input Client object to the servers collection.
* Servers in the collection are polled sequentially to locate the requested  data.
* If a server does not have the data, the next server in the iteration, if any is polled to satisfy a data request.
* Returns true if successful, false if error occurs, or server already is in list.
*/
    public boolean addServer(final Client client) {

        boolean retVal = true;

        TCPConnClient conn = null;

        try {
            if (client == null)
                throw new NullPointerException("WaveClient.addServer() input Client parameter is null");

            int idx = -1;
            for (int i=0; i<servers.size(); i++) {
                conn = (TCPConnClient) servers.get(i);
                if (conn.client.equals(client)) {
                    idx = i;
                }
            }

            //if (servers.containsKey(client)) {
            if ( idx > -1 ) {
                System.err.println("ERROR >>> WaveClient.addServer() : " + client.toString() + " already in list of servers");
                return false;
            }

            /*
            final SwingWorker sw = new SwingWorker() {
                public Object construct() { 
                   TCPConn conn = null;
                   try {
                       conn = new TCPConnClient(client);
                       conn.setDefaultTimeout(maxTimeout);
                   }
                   catch (UnknownHostException ex) {
                       System.err.println("ERROR >>> WaveClient.addServer() Unknown host: " + client.host + " port: " + client.port);
                   }
                   catch (IOException ex) {
                       System.err.println("ERROR >>> WaveClient.addServer() IO error connecting to " + client.host + " port: " + client.port);
                       System.err.println(ex.toString());
                   }
                   return conn;
                }
            };
            sw.start();

            // Now run timer on new daemon thread after timeout
            java.util.Timer timer = new java.util.Timer(true);
            timer.schedule(
                new TimerTask() {
                  public void run() {
                      System.err.println("ERROR >>> WaveClient.addServer() host: " + client.host + " port: " + client.port);
                      System.err.println("          .... interrupting after timeout (ms) : " +  connectTimeout ); 
                      // connectTimeout is not the same as maxTimeout, that's for open connection reading input on its socket 
                      sw.interrupt(); // addServer thread 
                  }
                }, (long) connectTimeout 
            );

            try {
               conn = (TCPConn) sw.get();
            }
            catch (Exception ex) {
                System.err.println("TCPConnClient connect ERROR >>> " + ex.getMessage());
            }

            timer.cancel();
            timer = null;
            */
                   try {
                       //conn = new TCPConnClient(client);
                       conn = new TCPConnClient();
                       conn.client = client;
                       conn.connectTimeout = getConnectTimeout();
                       conn.createSocket(client.host, client.port);
                       conn.setDefaultTimeout(maxTimeout);
                   }
                   catch (UnknownHostException ex) {
                       retVal = false;
                       System.err.println("ERROR >>> WaveClient.addServer() Unknown host: " + client.host + " port: " + client.port);
                   }
                   catch (IOException ex) {
                       retVal = false;
                       System.err.println("ERROR >>> WaveClient.addServer() IO error connecting to " + client.host + " port: " + client.port);
                       System.err.println(ex.toString());
                   }

            if (retVal) TCPConn.debug = this.debug;
        }
        catch (NullPointerException ex) {
            System.err.println("ERROR >>> WaveClient.addServer() input Client parameter is null");
            retVal = false;
        }
        catch (Exception ex) {
            System.err.println("ERROR >>> WaveClient.addServer() caught exception connecting to " +client.host+ " port: " +client.port);
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
            retVal = false;
        }

        if (retVal == true) {
            //if (servers.put(client, conn) != null) {
            //    System.err.println("ERROR >>> WaveClient.addServer() : " + conn.toString() + " already in list of servers");
            //    retVal =  false;
            //}
            servers.add(conn);
        }
        return retVal;
    }

/** Removes the specified host and port number from the polled server collection.
*   Returns true if successful, false if an error occurs or input parameter is null.
*/
    public boolean removeServer(String host, int port) {
        return removeServer(new Client(host, port)); // InetAddress ?
    }

/** Removes the host and port number specified in the specified Client object from the polled
* server collection and closes its socket connection.
* Returns true if successful, false if an error occurs or input parameter is null.
*/
    public boolean removeServer(Client client) {

        if (client == null) {
            System.err.println("ERROR >>> WaveClient removeServer() input Client parameter is null");
            return false;
        }

        //TCPConn cc = (TCPConn) servers.get(client);
        TCPConnClient cc = null;
        int idx = -1;
        for (int i=0; i<servers.size(); i++) {
            cc = (TCPConnClient) servers.get(i);
            if (cc.client.equals(client)) {
                idx = i;
            }
        }

        //if (cc == null) {
        if (idx == -1) {
            System.err.println("ERROR >>> WaveClient.removeServer() server: " + client.toString() + " not found");
            return false;
        }
        else {
            cc.close();
            //servers.remove(client);
            servers.remove(idx);
        }
        return true;
    }

/** Closes all connections for the entire server list. */
    public void close() {
        // get all available servers
        //Collection connList = servers.values();
        //Iterator iter = connList.iterator();
        Iterator iter = servers.iterator();
        while (iter.hasNext()) {
            ((TCPConnClient) iter.next()).close();
        }
    }

/** Requests time series data for the specified data channel, start time and duration.
* The channel location is passed as empty string into the overload of this method.
* If data is found the data is converted into FloatTimeSeries objects returned as elements added to the input collection.
* The input collection is cleared and new data appended.
* Returns TN_SUCCESS if successful, TN_NODATA if no data are found, else an TRINET error return code.
* <b>By default data packet headers are checked for data consistency, for faster response disable verification.</b>
* @see #setWaveformVerify(boolean)
* @see #isVerifyWaveforms()
* @see #getData(Channel, TimeWindow, Waveform)
* @exception java.lang.NullPointerException input collection parameter is null
* @exception java.lang.IllegalArgumentException Valid Channel/TimeWindow cannot be constructed from the input parameters.
*/
    public int getData(String net, String sta, String chn,
                       Date beginTimestamp, int durationSeconds, Collection timeSeriesList) {
      return getData(net, sta, chn, "", beginTimestamp, durationSeconds, timeSeriesList);
    }

    // overload method to utilize location in Channel description - AWW 5/30/2003
    public int getData(String net, String sta, String chn, String location,
                       Date beginTimestamp, int durationSeconds, Collection timeSeriesList) {
        if (timeSeriesList == null)
            throw new NullPointerException("WaveClient.getData() input FloatTimeSeries collection parameter is null");
        // Clear any existing FloatTimeSeries structures
        timeSeriesList.clear();

        Channel chan = null;
        try {
            chan = new Channel(net, sta, chn, location);
        }
        catch (IllegalArgumentException ex) {
            System.err.println("ERROR >>> WaveClient.getData(): " + ex.getMessage());
            ex.fillInStackTrace();
            throw ex;
        }

        TimeWindow timeWindow = null;
        DateTime startTime = (beginTimestamp instanceof DateTime) ? (DateTime) beginTimestamp : new DateTime(beginTimestamp); // UTC -aww 2008/04/01
        try {
            timeWindow = new TimeWindow( startTime, new DateTime( startTime.getTrueSeconds() + durationSeconds, true) ); // UTC -aww 2008/04/01
        }
        catch (IllegalArgumentException ex) {
            System.err.println("ERROR >>> WaveClient.getData(): " + ex.getMessage());
            ex.fillInStackTrace();
            throw ex;
        }

        Waveform waveform = new Waveform();

        // Retrieve the waveform data
        int retVal = getData(chan, timeWindow, waveform);
        if (retVal < TN_SUCCESS) {
            if (retVal == TN_NODATA) return retVal;
        }

        if (! waveform.trim(timeWindow)) {
            System.err.println("ERROR >>> WaveClient.getData() Unable to trim waveform to time window");
            return TN_FAILURE;
        }
        chan.sampleRate =  waveform.getSampleRate();
        if (chan.sampleRate <= 0.0) {
            System.err.println("ERROR >>> WaveClient.getData() waveform sample rate is undefined.");
            return TN_FAILURE;
        }

        List dataCountsList = new ArrayList();

        retVal = TN_SUCCESS;

        try {
            while (true) {
                Waveform waveformSegment = waveform.next();
                if (waveformSegment == null) {
                    if (timeSeriesList.size() > 0) retVal = TN_SUCCESS;
                    else {
                        retVal = TN_NODATA;
                        System.err.println("ERROR >>> WaveClient.getData() end of data found for channel: " + chan.toString());
                    }
                    break;
                }
                if (! waveformSegment.getCounts(dataCountsList)) {
                    System.err.println("ERROR >>> WaveClient.getData() Unable to get counts from waveform." );
                    retVal = TN_FAILURE;
                    break;
                }
                if (dataCountsList.size() != 1) {
                    System.err.println("ERROR >>> WaveClient.getData() count list has " + dataCountsList.size() + " elements");
                    retVal = TN_FAILURE;
                    break;
                }
                // Note - Only one element, don't need iterator for list now.
                DataCounts dataCounts = (DataCounts) dataCountsList.get(0);
                if (dataCounts.dataList == null) {
                    System.err.println("ERROR >>> WaveClient.getData() null data counts list.");
                    retVal = TN_FAILURE;
                    break;
                }
                // Changed block below to avoid extra object conversion re changes to Waveform - aww

                /*
                // Convert DataCount List of Number object counts to primitive array of counts
                    int numberOfSamples = dataCounts.dataList.size();
                    float [] samples = new float [numberOfSamples];
                    for (int index = 0; index < numberOfSamples; index++) {
                        samples[index] = ((Float) dataCounts.dataList.get(index)).floatValue();
                    }
                    FloatTimeSeries timeSeries =
                      new FloatTimeSeries(
                          dataCounts.startTimestamp,
                          new DateTime(dataCounts.getStartTimeSecs()+(numberOfSamples-1)/waveform.getSampleRate(), true),
                          samples
                      );
                */
                int count = 0;
                int size = dataCounts.dataList.size();
                for (int ii = 0; ii < size; ii++) {
                    count += ((float []) dataCounts.dataList.get(ii)).length;
                }
                float [] samples =  new float[count];

                count = 0;
                float [] f = null;
                for (int ii = 0; ii < size; ii++) {
                    f = (float []) dataCounts.dataList.get(ii);
                    System.arraycopy(f, 0, samples, count, f.length);
                    count += f.length;
                }
                int numberOfSamples = samples.length;
                FloatTimeSeries timeSeries =
                    new FloatTimeSeries( // UTC -aww 2008/04/01
                            dataCounts.startTimestamp,
                            new DateTime(dataCounts.getStartTimeSecs()+(numberOfSamples-1)/waveform.getSampleRate(), true),
                            samples
                    );
                timeSeriesList.add(timeSeries);
            }
        }
        catch (WaveformDataException ex) {
            System.err.print("ERROR >>> WaveClient.getData() caught WaveformDataException ");
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
            retVal = TN_FAILURE;
        }
        catch (Exception ex) {
            System.err.print("ERROR >>> WaveClient.getData() caught exception ");
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
            retVal = TN_FAILURE;
        }
        return retVal;
    }

/** Requests a list of TimeRange objects describing the available timeseries for the specified channel string descriptors.
* Clears the input TimeRange list and appends the data returned in the server response, if any, to this list.
* The channel location is passed as empty string into the overload of this method.
* Returns TN_SUCCESS if successful, TN_NODATA if no data is found, else a TRINET error return code.
* @exception java.lang.NullPointerException input collection parameter is null
* @exception java.lang.IllegalArgumentException Valid Channel cannot be constructed from the input parameters.
*/
    public int getTimes(String net, String sta, String chn, Collection timeRangeList) {
        return getTimes(net, sta, chn, "", timeRangeList);
    }
    // overload method to utilize location in Channel description - AWW 5/30/2003
    public int getTimes(String net, String sta, String chn, String location, Collection timeRangeList) {
        if (timeRangeList == null)
            throw new NullPointerException("WaveClient.getTimes() input TimeRange collection parameter is null");
        // Clear any existing TimeRange structures
        timeRangeList.clear();

        Channel chan = null;
        try {
            chan = new Channel(net, sta, chn, location);
        }
        catch (IllegalArgumentException ex) {
            System.err.println("ERROR >>> WaveClient.getTimes(): " + ex.getMessage());
            ex.fillInStackTrace();
            throw ex;
        }

        List timeWindowList = new ArrayList();
        int retVal = this.getTimes(chan, timeWindowList);
        if (retVal < TN_SUCCESS) return retVal;

        Iterator iter = timeWindowList.iterator();
        while (iter.hasNext()) {
            TimeWindow timeWindow = (TimeWindow) iter.next();
            timeRangeList.add(timeWindow.getTimeRange());
        }
        return retVal;
    }

/** Requests the sample rate for the seismic channel specified by the input string parameters.
* The channel location is passed as empty string into the overload of this method.
* Returns the sample rate data returned in the server response.
* Returns Double.NaN if no data was available.
* @exception java.lang.IllegalArgumentException Valid Channel cannot be constructed from the input parameters.
* @exception WaveClientException error occurred recovering sample rate data
*/
    public double getSampleRate (String net, String sta, String chn) throws WaveClientException {
        return getSampleRate (net, sta, chn, "");
    }
    // overload method to utilize location in Channel description - AWW 5/30/2003
    public double getSampleRate (String net, String sta, String chn, String location) throws WaveClientException {
        Channel chan = null;
        try {
            chan = new Channel(net, sta, chn, location);
        }
        catch (IllegalArgumentException ex) {
            System.err.println("ERROR >>> WaveClient.getSampleRate(): " + ex.getMessage());
            ex.fillInStackTrace();
            throw ex;
        }

        int status = getSampleRate(chan);
        if (status < TN_SUCCESS) {
            if (status == TN_NODATA) return Double.NaN;
            else throw new WaveClientException("WaveClient.getSampleRate() returned error code: " + getStatusMessage(status));
        }
        return chan.sampleRate;
    }

/** Closes the connection object's socket and attempts to re-establish new socket connection with the same host and port.
* Returns true upon success, false otherwise.
*/
    private boolean reconnect(TCPConn conn, String msg) {
        boolean retVal = true;
        try {
            if (debug) System.out.println(msg + " attempting WaveClient.reconnect to " + conn.socket.toString());
            conn.reset(); //close done by reset: conn.close();
            retVal = conn.socket.isConnected();
            if (debug) System.out.println("INFO: WaveClient reconnect successful: " + retVal);
        }
        catch (IOException ex) {
            System.err.println(" ERROR >>> " + msg + " WaveClient.reconnect() io error reconnecting to " + conn.socket.toString());
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
            retVal = false;
        }
        catch (Exception ex) {
            System.err.println(" ERROR >>> " + msg + " WaveClient.reconnect() exception reconnecting to " + conn.socket.toString());
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
            retVal = false;
        }

        // induce delay here before doing more socket i/o, perhaps waveserver needs time to recoup ?
        // this doesn't seem to reduce EOF exceptions -aww 2010/08/06
        try {
            Thread.currentThread().sleep(100l); // 100 ms
        }
        catch (InterruptedException ex) {
            System.err.println(ex.getMessage() + " WaveClient.reconnect sleep");
            //ex.printStackTrace();
        }

        return retVal;
    }

/** Attempts to sends the input TCPMessage to the server host described by the TCPConn socket.
* Returns true if successful, false otherwise.
*/
    private static boolean sendRequest(TCPConnClient conn, TCPMessage requestMessage) {
        if (conn.debug) {
            System.out.println("DEBUG >>> WaveClient sendRequest calling conn.send at " + new DateTime());
        }
        return conn.send(requestMessage); 
    }

/** Returns a TCPMessage from the server host described by the TCPConn socket.
* If no response if completed by the specified timeout interval the request is aborted.
* Returns null if unsuccessful.
*/
    private static TCPMessage receiveResponse(TCPConnClient conn, int timeoutMilliSecs) {
        TCPMessage responseMessage = null;
        try {
            if (conn.debug) {
                System.out.println("DEBUG >>> WaveClient receiveResponse calling conn.receive at " + new DateTime());
            }
            responseMessage = conn.receive(timeoutMilliSecs);
            if (conn.debug) {
                System.out.println("DEBUG >>> WaveClient receiveResponse returned from conn.receive at " + new DateTime());
            }
        }
        catch (TCPConnException ex) {
            System.err.print("ERROR >>> WaveClient.receiveResponse() caught TCPConnException exception ");
            System.err.println(ex.getMessage());
            //ex.printStackTrace(); // for debug
        }
        catch (Exception ex) {
            System.err.print("ERROR >>> WaveClient.receiveResponse() caught exception ");
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
        }
        return responseMessage;
    }

/** Requests data from the server host described by the input TCPConn via a requesting TCPMessage.
* If no response if completed by the set number of connection retries the request is aborted.
* Returns the server generated response TCPMessage, or null if unsuccessful.
*/
    private TCPMessage requestReply(TCPConnClient conn, TCPMessage requestMessage) {

        //if (debug) {
        //    System.out.println("DEBUG >>> WaveClient.requestReply() maxRetries: "+ getMaxRetries() +" for request: "+ requestMessage.toString());
        //}

        TCPMessage responseMessage = null;
          // responseMessage = new TCPMessage(TN_TCP_ERROR_RESP, 2);
          // responseMessage.appendField(requestMessage.nextFieldValueAsInt()); // request sequence #
          // responseMessage.appendField(TN_NODATA); // or TN_FAILURE or TN_EOF ?
      OUTER:


        for (int retry = 0; retry <= getMaxRetries(); retry++) {

            responseMessage = null;

            if (!sendRequest(conn, requestMessage)) { // send failed
                System.err.println("ERROR >>> WaveClient.requestReply() Unable to send request message to server");
                if (! reconnect(conn, "sendRequestFailed"))  {
                  System.err.println("ERROR >>> WaveClient.requestReply() attempt: " + String.valueOf(retry+1) + " reconnection failed");
                  break OUTER; // can't reconnect bail
                }
                continue OUTER; // resend request up to maxRetries
            }
            // Sent ok to server, now go get server answer (retry on timeout, else bail on error, try reconnect and request again if EOF)
            INNER:
            while (true) {

                responseMessage = receiveResponse(conn, maxTimeout);

                if (responseMessage == null) { // message receive failed
                   /*
                   if (!reconnect(conn, "receiveResponseFailed")) {
                       System.err.println("ERROR >>> WaveClient.requestReply() attempt: " + String.valueOf(retry+1) + " NULL msg reconnection failed");
                   }
                   */
                   break OUTER; // out of processing loop
                }
                // checked returned msg 

                int msgType = responseMessage.getMessageType(); 

                if ( (msgType != TN_TCP_ERROR_RESP) && (msgType != TN_TCP_TIMEOUT_RESP) ) break OUTER; // done, assume valid GET_XXX_RESP

                // Is it a socket timeout?
                if (msgType == TN_TCP_TIMEOUT_RESP) { // -aww 2010/08/06
                       if (debug) {
                         System.err.println("DEBUG >>> WaveClient.requestReply() TN_TCP_TIMEOUT_RESP at:" + maxTimeout + "ms retry # " +
                                 String.valueOf(retry+1) + " at " + new DateTime());
                         System.err.println("  for request: " +requestMessage);
                       }
                       try {
                           // skip over socket input stream available bytes, if ever > 0, after a timeout?
                           System.out.println("ERROR >>> WaveClient.requestReply() jumpToEndOfInput, total bytes read/skipped: "  + conn.jumpToEndOfInput());
                       }
                       catch (IOException ex) {
                           System.err.println("ERROR >>> WaveClient.requestReply() jumpToEndOfInput " + ex.getMessage());
                           //ex.printStackTrace();
                       }
                       /* Don't reconnect some servers like PWS takes 30 secs to init -aww 2010/08/27
                       if (!reconnect(conn, "timeoutResetFailed")) { // -aww 2010/08/06
                           System.err.println("ERROR >>> WaveClient.requestReply() attempt: " + String.valueOf(retry+1) + " reconnection failed");
                           break OUTER; // can't reconnect bail // -aww 2010/08/06
                       }
                       
                       */
                       
                       //continue INNER; // do new retry receive attempt after timeout
                       // Do not do a new request attempt after timeout (break inner) because INPUTSTREAM
                       // of socket is not flushed before next request which causes offset in the request/reply id matching
                       //break INNER;

                       // 2015-08-04 -aww try outer break to force alternative server
                       break OUTER;
                }
                      
                // Check msg status code
                int istat = TN_SUCCESS;
                int iseq = 0;
                try {
                       iseq = responseMessage.nextFieldValueAsInt(); // msg seq #
                       istat = responseMessage.nextFieldValueAsInt(); // msg status code
                       responseMessage.setFieldIterator(); // reset iterator here for verify response upon return
                }
                catch (IOException ex) {
                       System.err.print("ERROR >>> WaveClient.requestReply() IOException after receiveResponse ");
                       System.err.println(ex.getMessage());
                       //ex.printStackTrace();
                }

                if (debug) { // Report code
                       System.err.println("ERROR >>> WaveClient.requestReply() msgType: " + msgType + " seqId# " + iseq + " " +
                               getStatusMessage(istat) + " for retry attempt: " + String.valueOf(retry+1));
                }

                // Ignore code of TN_NODATA, treat like success 
                if (istat == TN_NODATA || istat == TN_SUCCESS) break OUTER;

                // EOF status code on read, istat == TN_EOF, the socket connection probably is closed, why?
                // assume EOF is a lost connection, try a reconnect, then new sendRequest
                if (istat == TN_EOF && reconnect(conn, "responseEOFReconnectFailure at "+new DateTime())) break INNER; // reconnect for retry;

                break OUTER; // other type of error, bail

              } // End of While loop INNER do another receiveResponse iteration retry

        } // End of For loop OUTER, do another sendRequest iteration retry

        // After loop
        if (responseMessage == null) { // Serious error? try once more to reconnect for next next method invocation
            if (! reconnect(conn, "finalSend/ReplyFailed")) {
                System.err.println("ERROR >>> WaveClient.requestReply() NULL return, final reconnection failed");
            }
        }
        else if (responseMessage.getMessageType() == TN_TCP_TIMEOUT_RESP) {  // -aww 2010/08/06
          //
          try {
              responseMessage = new TCPMessage(TN_TCP_ERROR_RESP, 2);
              responseMessage.appendField(requestMessage.nextFieldValueAsInt()); // request sequence #
              responseMessage.appendField(TN_TIMEOUT); // or TN_NODATA or TN_FAILURE or TN_EOF ?
          }
          catch (IOException ex) {
              System.err.println("ERROR >>> WaveClient.requestReply() " + ex.getMessage());
              //ex.printStackTrace();
          }

          //
          // Force a reconnect to purge receive buffer after a read timeout?
          // but reconnect for some servers e.g. PWS takes 30 secs to init, so set "reconnectOnTimeout" true
          // only for those servers that can do  a "fast" reconnect -aww
          //if (reconnectOnTimeout && ! reconnect(conn, "finalTimeoutResetFailed")) {
          //      System.err.println("ERROR >>> WaveClient.requestReply() NULL return, final reconnection failed");
          //}
          //
        }

        return responseMessage;
    }

/** Returns the TCMessage constructed from the specified input values.
* @exception WaveClientException message construction failed.
*/
    private static TCPMessage prepareMessage(int messageType, int requestSequenceNumber, TrinetSerial [] fields)
                 throws WaveClientException {

        TCPMessage requestMessage = null;

        try {
            if (fields == null)
                throw new NullPointerException("WaveClient.prepareMessage(...) null fields [] input parameter.");
            int fieldsLength = fields.length;
            // Construct the get data request message, prepare the request parameters
            requestMessage = new TCPMessage(messageType, fieldsLength);
            requestMessage.appendField(requestSequenceNumber);
            for (int index = 0; index < fieldsLength; index++) {
                requestMessage.appendField(fields[index].toByteArray());
            }
        }
        catch (Exception ex) {
            System.err.print("ERROR >>> WaveClient.prepareMessage() caught Exception ");
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
            throw new WaveClientException("ERROR >>> WaveClient.prepareMessage() caught exception creating request message fields");
        }
        return requestMessage;
    }

/** Returns the reference to the input responseMessage TCPMessage reference.
* @exception WaveClientException input parameter response message is null.
* @exceptionWaveClientIdException response sequence number does not match the request sequence number.
* @exception java.io.IOException response message sequence number cannot be parsed.
*/
    private static TCPMessage verifyResponseMessage(TCPConnClient conn, TCPMessage responseMessage, int requestSequenceNumber)
                   throws IOException, WaveClientException {
        TCPMessage msg = responseMessage;
        if (msg == null) {
            //throw new WaveClientException("WaveClient.verifyResponseMessage() null message");
            //System.err.println("    WARNING! waveserver responseMessage NULL");  // debug
            msg = new TCPMessage(TN_TCP_ERROR_RESP, 2);
            msg.appendField(0); // bogus sequence number
            msg.appendField(TN_EOF);
        }

        // Verify the message sequence number this is first data field in message
        int responseSequenceNumber = msg.nextFieldValueAsInt();

        // If next sendRequest is sent, and requestReply has not yet emptied receive buffer (like timeout) we get this:
        if (msg.messageType != TN_TCP_ERROR_RESP) {
            if (responseSequenceNumber > 0 && responseSequenceNumber != requestSequenceNumber) {
                long skipped = 0l;
                try {
                    // will bytes skip here resolve buffer offset for future message request?
                    skipped = conn.jumpToEndOfInput();
                }
                catch (IOException ex) {
                    System.err.println("ERROR >>> WaveClient.verifyResponseMessage(...) jumpToEndOfInput " + ex.getMessage());
                    //ex.printStackTrace();
                }
                throw new WaveClientIdException(" verifyResponseMessage() responseId != requestId: "
                                             + responseSequenceNumber + " != " + requestSequenceNumber + ", read bytes skipped: " + skipped);
            }
        }

        return msg;
    }

/** Generates a request message, requests a server reply, then checks the server reponse for validity.
* Returns the response message.
* @exception WaveClientException error processing message.
* @exception java.io.IOException connection error or error parsing serialized data stream.
*/
    private TCPMessage processMessage(TCPConnClient conn, int requestMessageType, TrinetSerial [] fields)
             throws IOException, WaveClientException {
        // Create a new message identifier
        int requestSequenceNumber = sequenceNumberCounter.plus();
        if (debug) {
            System.err.println("DEBUG >>> WaveClient processMessage() req seq#,msgType : " +requestSequenceNumber + " " +  requestMessageType); 
            //TN_TCP_GETDATA_REQ   = 3000;
            //TN_TCP_GETTIMES_REQ  = 3001;
            //TN_TCP_GETRATE_REQ   = 3002;
            //TN_TCP_GETCHAN_REQ   = 3003;
            //TN_TCP_ERROR_RESP    = 3004;
            //TN_TCP_GETDATA_RESP  = 3005;
            //TN_TCP_GETTIMES_RESP = 3006;
            //TN_TCP_GETRATE_RESP  = 3007;
            //TN_TCP_GETCHAN_RESP  = 3008;
            //TN_TCP_TIMEOUT_RESP  = 3099; // added -aww 2010/08/06
        }
        // Create a request message type
        TCPMessage requestMessage = prepareMessage(requestMessageType, requestSequenceNumber, fields);

        // Submit the request, check for response
        TCPMessage responseMessage = requestReply(conn, requestMessage);

        // Check validity of message, if no exception thrown, return response message data
        return verifyResponseMessage(conn, responseMessage, requestSequenceNumber);
    }

/** Processes a "getData" request message type.
* Returns TN_SUCCESS if the server response provided data, else returns an error code.
*/
    private int doGetData(TCPConnClient conn, Channel chan, TimeWindow requestedTimeWindow, Collection dataSegmentList) {
        // Prepare the request parameters
        TrinetSerial [] fields = new TrinetSerial[2];
        fields[0] = chan;
        fields[1] = requestedTimeWindow;
        int retVal = TN_FAILURE;
        try {
            // Check validity of message and get its content: either error code or data
            TCPMessage responseMessage = processMessage(conn, TN_TCP_GETDATA_REQ, fields);
            switch (responseMessage.getMessageType()) {
                case TN_TCP_ERROR_RESP:
                    retVal = responseMessage.nextFieldValueAsInt();
                    int tmp = retVal;
                    // PATCH BELOW VALUES IN UNTIL DWS RETURN CODES FIXED
                    if (retVal == TN_FAILURE) retVal = TN_NODATA;
                    else if (retVal == TN_SUCCESS) retVal = TN_NODATA;  // 2014/11/10 -aww
                    //else if (retVal == TN_EOF) retVal = TN_NODATA;
                    if (debug) {
                        bm.printTimeStamp("DEBUG >>> WaveClient doGetData(...) TN_TCP_ERROR_RESP status: " + tmp +
                                ", reset to TN_NODATA", System.err);
                    }
                    break;
                case TN_TCP_GETDATA_RESP:
                    chan.sampleRate = responseMessage.nextFieldValueAsDouble();
                    int numberOfSegments = responseMessage.nextFieldValueAsInt();
                    if (debug) {
                        bm.printTimeStamp("DEBUG >>> WaveClient doGetData(...) numberOfSegments: " + numberOfSegments, System.err);
                    }
                    for (; numberOfSegments > 0; numberOfSegments--) {
                        dataSegmentList.add(new DataSegment(responseMessage.nextFieldValueAsBytes()));
                    }
                    retVal = TN_SUCCESS;
                    break;
                default:
                    System.err.println("ERROR >>> WaveClient.doGetData() Unrecognized response message");
            }
        }
        catch (WaveClientException ex) {
            System.err.print("ERROR >>> WaveClient.doGetData() WaveClientException " + chan.toIdString() + ":");
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
        }
        catch (IOException ex) {
            System.err.print("ERROR >>> WaveClient.doGetData() IOException " + chan.toIdString() + ":");
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
        }
        catch (Exception ex) {
            System.err.print("ERROR >>> WaveClient.doGetData() caught exception " + chan.toIdString() + ":");
            if (debug) ex.printStackTrace();
            else System.err.println(ex.toString());
        }
        return retVal;
    }

/** Processes a "getSampleRate" request message type.
* Returns TN_SUCCESS if the server response provided data, else returns an error code.
*/
    private int doGetSampleRate(TCPConnClient conn, Channel chan) {
        // Prepare the request parameters
        TrinetSerial [] fields = new TrinetSerial[1];
        fields[0] = chan;
        int retVal = TN_FAILURE;
        try {
            // Check validity of message and get its content: either error code or data
            TCPMessage responseMessage = processMessage(conn, TN_TCP_GETRATE_REQ, fields);
            switch (responseMessage.getMessageType()) {
                case TN_TCP_ERROR_RESP:
                    retVal = responseMessage.nextFieldValueAsInt();
                    if (retVal == TN_FAILURE) retVal = TN_NODATA;  // TEMPORARY PATCH UNTIL DWS CODES FIXED
                    break;
                case TN_TCP_GETRATE_RESP:
                    chan.sampleRate = responseMessage.nextFieldValueAsDouble();
                    retVal = TN_SUCCESS;
                    break;
                default:
                    System.err.println("ERROR >>> WaveClient.doGetSampleRate() Unrecognized response message");
            }
        }
        catch (WaveClientException ex) {
            System.err.print("ERROR >>> WaveClient.doGetSampleRate() WaveClientException ");
            System.err.println(ex.getMessage());
            //if (debug) ex.printStackTrace();
        }
        catch (IOException ex) {
            System.err.print("ERROR >>> WaveClient.doGetSampleRate() IOException ");
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
        }
        catch (Exception ex) {
            System.err.print("ERROR >>> WaveClient.doGetSampleRate() caught exception ");
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
        }
        return retVal;
    }

/** Processes a "getChannels" request message type.
* Returns TN_SUCCESS if the server response provided data, else returns an error code.
*/
    private int doGetChannels(TCPConnClient conn, Collection channelList) {
        // Prepare the request parameters
        TrinetSerial [] fields = new TrinetSerial[0];
        int retVal = TN_FAILURE;
        try {
            // Check validity of message and get its content: either error code or data
            TCPMessage responseMessage = processMessage(conn, TN_TCP_GETCHAN_REQ, fields);
            switch (responseMessage.getMessageType()) {
                case TN_TCP_ERROR_RESP:
                    retVal = responseMessage.nextFieldValueAsInt();
                    if (retVal == TN_FAILURE) retVal = TN_NODATA;  // TEMPORARY PATCH UNTIL DWS CODES FIXED
                    break;
                case TN_TCP_GETCHAN_RESP:
                    int numberOfChannels = responseMessage.nextFieldValueAsInt();
                    for (; numberOfChannels > 0; numberOfChannels--) {
                        channelList.add(new Channel(responseMessage.nextFieldValueAsBytes()));
                    }
                    retVal = TN_SUCCESS;
                    break;
                default:
                  System.err.println("ERROR >>> WaveClient.doGetChannels() Unrecognized response message");
            }
        }
        catch (WaveClientException ex) {
            System.err.print("ERROR >>> WaveClient.doGetChannels() WaveClientException ");
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
        }
        catch (IOException ex) {
            System.err.print("ERROR >>> WaveClient.doGetChannels() IOException ");
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
        }
        catch (Exception ex) {
            System.err.print("ERROR >>> WaveClient.doGetChannels() caught exception ");
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
        }
        return retVal;
    }

/** Processes a "getTimes" request message type.
* Returns TN_SUCCESS if the server response provided data, else returns an error code.
*/
    private int doGetTimes(TCPConnClient conn, Channel chan, Collection timeWindowList) {
        // Prepare the request parameters
        TrinetSerial [] fields = new TrinetSerial[1];
        fields[0] = chan;
        int retVal = TN_FAILURE;
        try {
            // Check validity of message and get its content: either error code or data
            TCPMessage responseMessage = processMessage(conn, TN_TCP_GETTIMES_REQ, fields);
            switch (responseMessage.getMessageType()) {
                case TN_TCP_ERROR_RESP:
                    retVal = responseMessage.nextFieldValueAsInt();
                    if (retVal == TN_FAILURE) retVal = TN_NODATA;  // TEMPORARY PATCH UNTIL DWS CODES FIXED
                    break;
                case TN_TCP_GETTIMES_RESP:
                    int numberOfWindows = responseMessage.nextFieldValueAsInt();
                    try {
                        for (; numberOfWindows > 0; numberOfWindows--) {
                            timeWindowList.add(new TimeWindow(responseMessage.nextFieldValueAsBytes()));
                        }
                        retVal = TN_SUCCESS;
                    }
                    catch (IllegalArgumentException ex) {
                        System.err.println("ERROR >>> WaveClient.doGetTimes(): " + ex.toString());
                        retVal = TN_FAILURE;
                    }
                    break;
                default:
                    System.err.println("ERROR >>> WaveClient.doGetTimes() Unrecognized response message");
            }
        }
        catch (WaveClientException ex) {
            System.err.print("ERROR >>> WaveClient.doGetTimes() WaveClientException ");
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
        }
        catch (IOException ex) {
            System.err.print("ERROR >>> WaveClient.doGetTimes() IOException message ");
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
        }
        catch (Exception ex) {
            System.err.print("ERROR >>> WaveClient.doGetTimes() caught exception ");
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
        }
        return retVal;
    }

/** Checks the input TimeWindow collection for temporal integrity.
* @return false found an invalid TimeWindow
*/
    static boolean verifyTimeWindows(Collection timeWindowList) {
        boolean retVal = true;
        Iterator iter = timeWindowList.iterator();
        int offset = 0;
        while(iter.hasNext()) {
          TimeWindow timeWindow = (TimeWindow) iter.next();
          if (! timeWindow.isValid() ) {
            retVal = false;
            System.err.println("WARNING ! WaveClient.verifyTimeWindows() Invalid time window detected at offset: " + offset);
            System.err.println(timeWindow.toString());
            break;
          }
          offset++;
        }
        return retVal;
    }

/** Error threshold seconds allowed between contiguous time series samples. */
    static double getEpsilon(double sampleRate) {
        return (1 + Waveform.TIME_EPSILON)/sampleRate;
    }

/** Merges contiguous TimeWindow data found in the input collection thereby reducing the total number of elements
* used to generate any data requests using this collection.
* Returns the revised collection.
*/
    static Collection mergeTimeWindows(Collection timeWindowList, double sampleRate) {
        if (timeWindowList.isEmpty()) return timeWindowList;
        TreeSet timeWindowSet = new TreeSet(timeWindowList);
        Iterator iter = timeWindowSet.iterator();
        Outer:
        while (iter.hasNext()) {                                                     // iterate over window set
            TimeWindow timeWindow = (TimeWindow) iter.next();                        // get first time window
            Inner:
            while (iter.hasNext()) {                                                 // look for another window, else done.
                TimeWindow timeWindowNext = (TimeWindow) iter.next();                // get next time window for comparison
                if (timeWindowNext.after(timeWindow)) {                              // next window temporally follows previous
                    if (timeWindow.timeGapSeconds(timeWindowNext) < getEpsilon(sampleRate)) { // small gap between
                        ((SimpleTimeRange) timeWindow.getTimeRange()).endTimestamp =
                                                    timeWindowNext.getEndTimestamp();// merge next time window into current
                        iter.remove();                                               // delete next window
                        continue Inner;                                              // loop for next window in set
                    }
                    else continue Outer;                                             // large gap keep both windows, get next pair
                }
                else {                                                               // Time windows overlap.
                    DateTime endTimeNext = timeWindowNext.getEndTimestamp(); // UTC -aww 2008/04/01
                    if (endTimeNext.after(timeWindow.getEndTimestamp())){            // Partial overlap between windows
                        ((SimpleTimeRange) timeWindow.getTimeRange()).endTimestamp = endTimeNext; // merge next into current window
                    }
                    iter.remove();                                                   // delete next, complete or partial overlap
                    continue Inner;                                                  // loop for next window in set
                }
            }
            break Outer;                                                             // no more windows in set
        }
        // Clear out original collection and reload with new set contents
        timeWindowList.clear();
        iter = timeWindowSet.iterator();
        while (iter.hasNext()) {
            timeWindowList.add(iter.next());
        }
        return timeWindowList; // revised collection
    }

/** Returns the time gap seconds found between the dataSegmentNext start time and the dataSegmentLast end time.*/
    static double timeGapSeconds(DataSegment dataSegmentLast, DataSegment dataSegmentNext, double sampleRate) {
        return dataSegmentNext.getStartTimeSecs() - dataSegmentLast.getStartTimeSecs()
                         - (double) (dataSegmentLast.numberOfSamples - 1)/sampleRate;
    }

/** Returns the time gap seconds found between the input TimeWindow and the DataSegment start times.*/
    static double timeGapSeconds(TimeWindow timeWindow, DataSegment dataSegment) {
        return dataSegment.getStartTimeSecs() - timeWindow.getStartTimeSecs();
    }

/** Returns the time of the last sample in the input DataSegment.*/
    static DateTime endTime(DataSegment dataSegment, double sampleRate) { // UTC -aww 2008/04/01
        return new DateTime(dataSegment.getStartTimeSecs() + (dataSegment.numberOfSamples-1)/sampleRate, true);
    }

/** Checks the time spane of the input data collection, presumably returned by a server response message,
* against that of the input TimeWindow, presumably the requested time range.
* Returns true if the data times span a range equal to or greater than that of the TimeWindow.
*/
    static boolean haveRequestedData(TimeWindow timeWindow, Collection dataSegmentList, double sampleRate) {
        if (dataSegmentList.isEmpty()) return false;

        // Check for time tear between request window start and first data segment start
        Iterator iter = dataSegmentList.iterator();
        DataSegment dataSegment = (DataSegment) iter.next();

        if (dataSegment.startTimestamp.after(timeWindow.getStartTimestamp())) {
            if (timeGapSeconds(timeWindow, dataSegment) > getEpsilon(sampleRate)) {
                return false; // need more data at begining of window
            }
        }

        // Check for time tear between elements in the data segment list
        while (iter.hasNext()) {
            DataSegment dataSegmentNext = (DataSegment) iter.next();
            if( timeGapSeconds(dataSegment, dataSegmentNext, sampleRate) > getEpsilon(sampleRate) ) return false; // have time tear
            dataSegment = dataSegmentNext;
        }

        // Check for time tear between requested window end time and last data segment end time
        DateTime dataSegmentEndTimestamp = endTime(dataSegment, sampleRate); // UTC -aww 2008/04/01
        return (timeWindow.timeGapSeconds(dataSegmentEndTimestamp) > getEpsilon(sampleRate) ) ? true : false; // need data at end
    }

/** Returns a collection of minSEED packets, if any,  for the specified input channel descriptors, starting time, and duration.
*  The channel location is passed as empty string into the overload of this method.
*  Data returned by the server response is truncated at the first time gap if isTruncateAtTimeGap() == true.
*  @exception java.lang.IllegalArgumentException Valid Channel/TimeWindow cannot be constructed from the input parameters.
*  @exception WaveClientException error occurred during packet processing.
*/
    public List getPacketData(String net, String sta, String chn,
                              Date beginTimestamp, int durationSeconds) throws WaveClientException {
        return getPacketData(net, sta, chn, "",  beginTimestamp, durationSeconds);
    }
    // overload method to utilize location in Channel description - AWW 5/30/2003
    public List getPacketData(String net, String sta, String chn, String location,
                              Date beginTimestamp, int durationSeconds) throws WaveClientException {
        Channel chan = null;
        try {
            chan = new Channel(net, sta, chn, location);
        }
        catch (IllegalArgumentException ex) {
            System.err.println("ERROR >>> WaveClient.getPacketData(): " + ex.getMessage());
            ex.fillInStackTrace();
            throw ex;
        }

        TimeWindow timeWindow = null;
        DateTime startTime = (beginTimestamp instanceof DateTime) ? (DateTime) beginTimestamp : new DateTime(beginTimestamp); // UTC -aww 2008/04/01
        try {
            timeWindow = new TimeWindow( startTime, new DateTime(startTime.getTrueSeconds() + durationSeconds, true) ); // UTC -aww 2008/04/01
        }
        catch (IllegalArgumentException ex) {
            System.err.println("ERROR >>> WaveClient.getPacketData(): " + ex.getMessage());
            ex.fillInStackTrace();
            throw ex;
        }

        return getPacketData(chan, timeWindow);
    }

/** Returns a collection of miniSEED packets, if any, for the specified input Channel and TimeWindow data.
*  Data returned by the server response is truncated at the first time gap if isTruncateAtTimeGap() == true.
*  Returns null if no data was found..
*  <b>By default data packet headers are checked for data consistency, for faster response disable verification.</b>
*  @see #setWaveformVerify(boolean)
*  @see #isVerifyWaveforms()
*  @see #getData(Channel, TimeWindow, Waveform)
*  @exception java.lang.NullPointerException null input parameter
*  @exception WaveClientException error occurred during processing of packet data.
*/
    public List getPacketData(Channel chan, TimeWindow timeWindow) throws WaveClientException {
        if (chan == null)
            throw new NullPointerException("WaveClient.getPacketData(Channel, TimeWindow) null Channel input parameter");
        if (timeWindow == null)
            throw new NullPointerException("WaveClient.getPacketData(Channel, TimeWindow) null TimeWindow input parameter");
        Waveform wave = new Waveform();
        int status = getData(chan, timeWindow, wave);
        if (status < TN_SUCCESS) {
            if(status == TN_NODATA) return null;
            else throw new WaveClientException("getPacketData() getData() error code: " + getStatusMessage(status));
        }
        return wave.getMiniSEEDPackets();
    }

/** Retrieves from the server hosts the time-series data for the channel and time specified in the input Waveform instance.
*  A WFSegment collection constructed from the retrieved MiniSEED packet data is assigned to the input wavefrom object.
*  Time-series data returned is truncated at the first time gap if isTruncateAtTimeGap() == true.
*  Returns TN_SUCCESS if data was found, else TN_NODATA or other TRINET error code.
*  <b>By default data packet headers are checked for data consistency, for faster response disable verification.</b>
*  @see #getJasiWaveformDataSeries(org.trinet.jasi.Waveform jasiWaveform)
*  @see #setWaveformVerify(boolean)
*  @see #isVerifyWaveforms()
*  @see #getData(Channel, TimeWindow, Waveform)
*  @see org.trinet.jasi.SeedReader#getWFSegments(Collection)
*  @exception java.lang.NullPointerException input Waveform null or has null data member
*  @exception java.lang.IllegalArgumentException input Waveform channel or time data violate constraints
*/
    public int getJasiWaveformDataRaw(org.trinet.jasi.Waveform jasiWaveform) {
        if (jasiWaveform == null)
            throw new NullPointerException("WaveClient.getJasiWaveformDataRaw(jasi.Waveform) null input parameter");

        org.trinet.jasi.ChannelName channelId = jasiWaveform.getChannelObj().getChannelName();
        //
        // AWW below added channelName.getLocation() to new Channel constructor aww 5/30/2003
        Channel chan = new Channel(channelId.getNet(), channelId.getSta(),
                                   channelId.getSeedchan(), channelId.getLocation());

        TimeWindow timeWindow =
            new TimeWindow( new DateTime(jasiWaveform.getEpochStart(), true), new DateTime(jasiWaveform.getEpochEnd(), true)); // UTC -aww 2008/04/01

        Waveform wave = new Waveform();

        int status = TN_FAILURE;
        try { // 02/22/2007 -aww catch here to see what channel request caused exception
            status = getData(chan, timeWindow, wave);
            if (status < TN_SUCCESS) {
                if (status == TN_NODATA) return status;
                System.err.println("ERROR >>> WaveClient.getJasiWaveformDataRaw() getData() error code: " + getStatusMessage(status));
                return status;
            }
        }
        catch (TCPConnException ex) {
            //failureCount++;
            System.err.println("ERROR >>> FAILURE getJasiWaveformDataRaw getData for channel: " + chan.toIdString());
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
            return status;
        }

        jasiWaveform.setFormat(Waveform.MINISEED_TYPE);
//        wave.dataFormat;
//      jasiWaveform.setEncoding(Waveform.STEIM1);
        jasiWaveform.setEncoding(wave.dataEncoding);

        jasiWaveform.setSampleRate(wave.getSampleRate());
        jasiWaveform.setSegmentList(new ArrayList(SeedReader.getWFSegments(wave.getMiniSEEDPackets())));
        //jasiWaveform.trimSpanToWFSegmentSpan();  // test this trim -aww 2009/08/06

        return status;
    }

/** Retrieves from the server hosts the time-series data for the channel and time specified in the input Waveform instance.
*  A WFSegment collection constructed from a FloatTimeSeries collection derived from the original raw data packets (MiniSEED).
*  The original packet header format information is not preserved.
*  Time-series data returned is truncated at the first time gap if isTruncateAtTimeGap() == true.
*  Returns TN_SUCCESS if data was found, else TN_NODATA or other TRINET error code.
*  <b>By default data packet headers are checked for data consistency, for faster response disable verification.</b>
*  @see #getJasiWaveformDataRaw(org.trinet.jasi.Waveform jasiWaveform)
*  @see #setWaveformVerify(boolean)
*  @see #isVerifyWaveforms()
*  @see #getData(Channel, TimeWindow, Waveform)
*  @see org.trinet.jasi.SeedReader#getWFSegments(Collection)
*  @exception java.lang.NullPointerException input Waveform null or has null data member
*  @exception java.lang.IllegalArgumentException input Waveform channel or time data violate constraints
*/
    public int getJasiWaveformDataSeries(org.trinet.jasi.Waveform jasiWaveform) {
        if (jasiWaveform == null)
            throw new NullPointerException("WaveClient.JasiWaveformDataSeries(jasi.Waveform) null input parameter");

        ArrayList timeSeriesList = new ArrayList();

        org.trinet.jasi.ChannelName channelId = jasiWaveform.getChannelObj().getChannelName();
        double startTime = jasiWaveform.getEpochStart();
        double endTime = jasiWaveform.getEpochEnd();
    // used overloaded method to utilize location in Channel description - AWW 5/30/2003
        int status = getData(channelId.getNet(), channelId.getSta(), channelId.getSeedchan(),
                             channelId.getLocation(), // aww 5/30/2003 use location field
                             new DateTime(startTime, true), (int) Math.round(endTime - startTime), // -aww 2008/02/10
                             timeSeriesList);
        if (status < TN_SUCCESS) {
            if (status == TN_NODATA) return status;
            System.err.println("ERROR >>> WaveClient.getJasiWaveformDataSeries() getData() error code: " + getStatusMessage(status));
            return status;
        }
        int numberOfElements = timeSeriesList.size();
        jasiWaveform.setSegmentList(new ArrayList(numberOfElements));
        if (numberOfElements <= 0) return status;

        Iterator iter = timeSeriesList.iterator();
        final int BYTES_PER_SAMPLE = 4;
        org.trinet.jasi.WFSegment wfSeg = null;
        while (iter.hasNext()) {
            FloatTimeSeries ts = (FloatTimeSeries) iter.next();
            wfSeg = new org.trinet.jasi.WFSegment();
            wfSeg.bytesPerSample = BYTES_PER_SAMPLE;
            wfSeg.setChannelObj(jasiWaveform.getChannelObj());

            wfSeg.setStart(ts.startTimestamp.getTrueSeconds()); // -aww 2008/04/01
            wfSeg.setEnd(ts.endTimestamp.getTrueSeconds()); // -aww 2008/04/01
            //D.N.E. removed  wfSeg.lenSecs = wfSeg.tend - wfSeg.tstart;
            //D.N.E. removed  wfSeg.length = ts.samples.length * BYTES_PER_SAMPLE;

            wfSeg.samplesExpected = ts.samples.length;
            wfSeg.setSampleInterval(ts.getSampleRate()); // wfSeg.lenSecs/(wfSeg.getSampleCount() - 1));
            wfSeg.setTimeSeries(ts.samples);
            jasiWaveform.getSegmentList().add(wfSeg);
        }

        if (wfSeg != null) jasiWaveform.setSampleRate((double)Math.round(1.0/wfSeg.getSampleInterval()));
        jasiWaveform.setFormat(Waveform.MINISEED_TYPE);
        jasiWaveform.setEncoding(Waveform.STEIM1);

        return status;
    }

/** Requests timeseries data for the specified input Channel and TimeWindow time range.
*  Inputs the data returned in the server response, if any, to the input Waveform object.
*  The passed Waveform object should be constructed with the default Waveform constructor.
*  Trims the returned data at the first time gap if isTruncateAtTimeGap() == true.
*  Returns TN_SUCCESS if successful, else TN_NODATA, or an error code.
*  <b>By default data packet headers are checked for data consistency, for faster response disable verification.</b>
*  @see #setWaveformVerify(boolean)
*  @see #isVerifyWaveforms()
*  @exception java.lang.NullPointerException null input parameter
*/
    public int getData(Channel chan, TimeWindow timeWindow, Waveform waveform) {
        if (chan == null)
            throw new NullPointerException("WaveClient.getData(Channel, TimeWindow, Waveform) null Channel parameter");
        if (timeWindow == null)
            throw new NullPointerException("WaveClient.getData(Channel, TimeWindow, Waveform) null TimeWindow parameter");
        if (waveform == null)
            throw new NullPointerException("WaveClient.getData(Channel, TimeWindow, Waveform) null Waveform parameter");

        int retVal = TN_SUCCESS;

        Collection dataSegmentList = new TreeSet();

        // Request all available data from each server
        //Collection connList = servers.values();
        //Iterator iter = connList.iterator();
        Iterator iter = servers.iterator();

        if (debug) bm = new BenchMark();
        while (iter.hasNext()) {
            TCPConnClient conn = (TCPConnClient) iter.next();
            if (conn == null || conn.socket == null || conn.socket.getInetAddress() == null) {
                  System.err.println("ERROR >>> WaveClient.getData() conn socket null or not connected, skipping to next");
                  continue;                  
            }
            if (debug) {
                System.err.println("\nINFO >>> WaveClient getData(...) Polling : " +
                     conn.socket.getInetAddress().getHostName() +
                    ":" + conn.socket.getPort() + " " + chan.toIdString() + " for time: " + timeWindow.toString() );
            }

            retVal = doGetData(conn, chan, timeWindow, dataSegmentList);

            if (debug) {
                bm.printTimeStamp("DEBUG >>> WaveClient getData(...) doGetData status: " + retVal, System.err);
                bm.reset();
            }

            if (retVal < TN_SUCCESS) {
              if (retVal != TN_NODATA && retVal != TN_EOF) {
                System.err.println("ERROR >>> WaveClient.getData() doGetData returned error code:" + getStatusMessage(retVal));
                System.err.println("ERROR >>> WaveClient getData() after polling : " +
                     conn.socket.getInetAddress().getHostName() +
                    ":" + conn.socket.getPort() + " " + chan.toIdString() + " for time: " + timeWindow.toString() );
//              return retVal; // ignore error, continue iterating for now.
              }
            }
            //System.err.println("WaveClient doGetData() returned status code:" + getStatusMessage(retVal));

          // The following ordering of the packets assumes:
          //   1) Multiple servers may have data packets for the specified channel.
          //   2) The SEED packets for a particular time window of data are identical across all wave servers.
          //   3) Each wave server could be missing one or more packets that other servers may possess.
          // Check if the request has been satisfied
            if (haveRequestedData(timeWindow, dataSegmentList, chan.sampleRate)) break; // done with request
            if (debug) {
                System.out.println("INFO >>> WaveClient getData(...)         : " +
                     conn.socket.getInetAddress().getHostName() +
                    ":" + conn.socket.getPort() + " " + chan.toIdString() + " for time: " + timeWindow.toString() +
                    " Missing data for request");
            }
        }

        if (dataSegmentList.isEmpty()) {
            if (debug)
                System.err.println("INFO >>>  WaveClient getData(...) dataSegmentList is empty for: " + chan.toIdString() +
                        " returning TN_NODATA at "+ new DateTime());
            return TN_NODATA;
        }

//        dataSegmentList = new ArrayList(dataSegmentList);  // pass as defined, a Set, or convert to a List?

        try {
            waveform.setData(dataSegmentList, verifyWaveforms); // set verifyWaveforms false to speed up transfer
        }
        catch (WaveformDataException ex) {
            System.err.print("ERROR >>> WaveClient.getData() Unable to construct modify waveform object ");
            System.err.println(ex.getMessage());
            //ex.printStackTrace();
            return TN_FAILURE;
        }
        catch (Exception ex) {
            System.err.print("ERROR >>> WaveClient.getData() caught exception ");
            System.err.println(ex.toString());
            //ex.printStackTrace();
            return TN_FAILURE;
        }

        // Trim to the first data include time tear if necessary
        if (isTruncateAtTimeGap() && waveform.hasTimeGaps() ) {
            waveform.trimAtFirstGap();
        }
        return TN_SUCCESS;
    }

/** Requests the sample rate for the specified input seismic Channel.
* Sets the Channel sample rate to the data returned in the server response, if any.
* Returns TN_SUCCESS if successful, else TN_NODATA, or an error code.
* @exception java.lang.NullPointerException null input parameter
*/
    public int getSampleRate(Channel chan) {
        if (chan == null)
            throw new NullPointerException("WaveClient.getSampleRate(Channel) null input Channel reference");

        int retVal = TN_SUCCESS;

        chan.sampleRate = 0.0;

        // Request all available data from each server
        //Collection connList = servers.values();
        //Iterator iter = connList.iterator();
        Iterator iter = servers.iterator();

        while (iter.hasNext()) {
            retVal = doGetSampleRate((TCPConnClient) iter.next(), chan);
            if (retVal == TN_SUCCESS) return TN_SUCCESS;
        }
        return retVal; // TN_NODATA;
    }

/** Requests a list of org.trinet.waveserver.rt.Channel objects describing known
 * seismic channels for which timeseries data is available. Note: these are NOT
 * org.trinet.jasi.Channel objects. Clears the input Channel list and appends the
 * data returned in the server response, if any, to this list.
* Returns TN_SUCCESS if successful, else TN_NODATA, or an error code.
* @exception java.lang.NullPointerException null input parameter
*/
    public int getChannels(Collection channelList) {
        if (channelList == null)
            throw new NullPointerException("WaveClient.getChannels(Collection) null input collection reference");

        int retVal = TN_SUCCESS;

        // Clear out any existing channels in the list
        channelList.clear();

        TreeSet channelSet = new TreeSet();

        // Request all available data from each server
        //Collection connList = servers.values();
        //Iterator iter = connList.iterator();
        Iterator iter = servers.iterator();
        while (iter.hasNext()) {
            retVal = doGetChannels((TCPConnClient) iter.next(), channelSet);
            if (retVal < TN_SUCCESS && retVal != TN_NODATA ) {
                System.err.println("ERROR >>> WaveClient.getChannels() doGetChannels returned error code:" + getStatusMessage(retVal));
//              return retVal; // continue for now.
            }
        }
        if (channelSet.isEmpty()) return TN_NODATA;

        iter = channelSet.iterator();
        while (iter.hasNext()) {
           channelList.add(iter.next());
        }
        return TN_SUCCESS;
    }
    /** Returns a org.trinet.jasi.ChannelList containing known
     * seismic channels for which timeseries data is available from the waveserver
     * set in this object.
    */
    public org.trinet.jasi.ChannelList getJasiChannels() {
      ArrayList list = new ArrayList();   // to contain waveserver.rt.Channels
      org.trinet.jasi.ChannelList jlist = new org.trinet.jasi.ChannelList();

      if (getChannels(list) == TN_SUCCESS) {
        Channel ch[] = (Channel[]) list.toArray(new Channel[0]);
        for (int i = 0; i<ch.length; i++) {
          jlist.add(ch[i].toJasiChannel());
        }
      }
      return jlist;
    }
/** Requests a list of TimeWindow objects describing the available timeseries for the input seismic Channel.
* Clears the input TimeWindow list and appends the data returned in the server response, if any, to this list.
* Returns TN_SUCCESS if successful, else TN_NODATA, or an error code.
* @exception java.lang.NullPointerException null input parameter
*/
    public int getTimes(Channel chan, Collection timeWindowList) {
        if (chan == null)
            throw new NullPointerException("WaveClient.getTimes(Channel, Collection) null input Channel reference");
        if (timeWindowList == null)
            throw new NullPointerException("WaveClient.getTimes(Collection) null input collection reference");

        // Clear out the existing time windows
        timeWindowList.clear();
        // Request all available data from each server
        //Collection connList = servers.values();
        //Iterator iter = connList.iterator();
        Iterator iter = servers.iterator();
        int retVal = TN_SUCCESS;
        while (iter.hasNext()) {
            TCPConnClient conn = (TCPConnClient) iter.next();
            retVal = doGetTimes(conn, chan, timeWindowList);
            if (retVal < TN_SUCCESS && retVal != TN_NODATA ) {
                System.err.println("ERROR >>> WaveClient.getTimes() doGetTimes returned error code:" + getStatusMessage(retVal));
//              return retVal; // continue for now.
            }
        }
        if (timeWindowList.isEmpty()) return TN_NODATA;

        // Verify that the list of time windows is valid
        if (! verifyTimeWindows(timeWindowList)) {
            System.err.println( "ERROR >>> WaveClient.getTimes() time window list failed verification");
            return TN_FAILURE;
        }
        // Retrieve the sample rate for this channel
        if (getSampleRate(chan) < TN_SUCCESS) {
           System.err.println("ERROR >>> WaveClient.getTimes() cannot retrieve sample rate");
           return TN_FAILURE;
        }
        // Merge adjacent time windows using sample rate
        mergeTimeWindows(timeWindowList, chan.sampleRate);

        // Trim to the first time window if necessary
        if (isTruncateAtTimeGap() && (timeWindowList.size() > 1)) {
           iter = timeWindowList.iterator();
           TimeWindow timeWindow = (TimeWindow) iter.next();
           timeWindowList.clear();
           timeWindowList.add(timeWindow);
        }
        return TN_SUCCESS;
    }

/** Print to System.err the Trinet status code descriptive message. */
    public static String getStatusMessage(int status) {
        StringBuffer sb = new StringBuffer(132);
        sb.append("TN status message: ");
        switch (status) {
            case TN_SUCCESS    :
                sb.append("True/Success");
                break;
            case TN_FAILURE    :
                sb.append("False/Failure");
                break;
            case TN_EOF        :
                sb.append("End of File");
                break;
            case TN_SIGNAL     :
                sb.append("Signal");
                break;
            case TN_NODATA     :
                sb.append("No Data");
                break;
            case TN_NOTVALID   :
                sb.append("Not Valid");
                break;
            case TN_TIMEOUT    :
                sb.append("Timeout");
                break;
            case TN_BEGIN      :
                sb.append("Begin");
                break;
            case TN_END        :
                sb.append("End");
                break;
            case TN_PARENT     :
                sb.append("Parent");
                break;
            case TN_CHILD      :
                sb.append("Child");
                break;
            case TN_FAIL_WRITE :
                sb.append("Write Failure");
                break;
            case TN_FAIL_READ  :
                sb.append("Read Failure");
                break;
            case TN_BAD_SAMPRATE :
                sb.append("Bad sample rate");
                break;
            default:
                sb.append("Unknown return code: " + String.valueOf(status));
        }

        // Add timestamp at end of text message -aww 2010/08/06
        sb.append(" at ").append(new DateTime().toString());

        return sb.toString();
    }

/* Returns a List of the server connections in preferred polling order
* to satify a data request.
* @see #getRole()
    List getServerPollList() {
        return getServerPollList(System.currentTimeMillis());
    }

/* Returns a List of the server connections in preferred polling order
* to satify a data request.
* @see #getRole()
    List getServerPollList(long startTimeMilliSeconds) {
        // get all available servers
        Collection connList = servers.values();
        List serverPollList = new ArrayList(connList.size());

        Iterator iter = connList.iterator();
        while (iter.hasNext()) {                                   // puts both the dws and rws of primary at beginning of list
            TCPConnClient connClient = (TCPConnClient) iter.next();
            if ( getRole(connClient) == TN_PRIMARY) {
                if ( (System.currentTimeMillis() - startTimeMilliSeconds) < RWS_MAX_MILLISECS_IN_POOL) { // put rws first
                    if (connClient.getPort() == RWS_PORT_ID) serverPollList.add(0, connClient);
                    else serverPollList.add(connClient);
                }
                else {                                                                                     // put dws first
                    if (connClient.getPort() == DWS_PORT_ID) serverPollList.add(0, connClient);
                    else serverPollList.add(connClient);
                }
                iter.remove();
            }
        }
        serverPollList.addAll(connList);
        return serverPollList;
    }
*/

/**
* Returns the role of the server specified by the input TCPConnClient object.
* The role is used by the WaveClient to determine the order of preferred
* polling of the server hosts to satify a data request.
*/
    public int getRole(TCPConnClient conn) {
        if (doGetRole(conn) == TN_PRIMARY) return 1;
        else return 1;
    }

/* Processes a "getRole" request message type.
* Returns the server preferred role, else returns an error code.
*/
    private int doGetRole(TCPConnClient conn) {
       return 0;
    }

/* comment out for now until implementation by server on rt systems
        // Prepare the request parameters
        TrinetSerial [] fields = new TrinetSerial[0];
        int retVal = TN_FAILURE;
        try {
            // Check validity of message and get its content: either error code or data
            TCPMessage responseMessage = processMessage(conn, TN_TCP_GETROLE_REQ, fields);
            switch (responseMessage.getMessageType()) {
                case TN_TCP_ERROR_RESP:
                case TN_TCP_GETROLE_RESP:
                    retVal = responseMessage.nextFieldValueAsInt();
                    if (retVal == TN_FAILURE) retVal = TN_NODATA;  // TEMPORARY PATCH UNTIL DWS CODES FIXED
                    break;
                default:
                    System.err.println("ERROR >>> WaveClient.doGetRole() Unrecognized response message");
            }
        }
        catch (WaveClientException ex) {
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
        catch (IOException ex) {
            System.err.println("ERROR >>> WaveClient.doGetRole() Failure parsing response message");
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
        catch (Exception ex) {
            System.err.println("ERROR >>> WaveClient.doGetRole() caught exception.");
            ex.printStackTrace();
        }
        return retVal;
    }
*/
}
