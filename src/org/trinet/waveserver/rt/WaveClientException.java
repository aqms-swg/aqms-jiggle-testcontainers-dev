package org.trinet.waveserver.rt;
public class WaveClientException extends RuntimeException {
    WaveClientException() {
        super();
    }
 
    WaveClientException(String message) {
        super(message);
    }
}
