package org.trinet.web;

import java.awt.*;
import java.io.*;
import java.awt.event.*;
import javax.swing.*;

import org.trinet.jasi.*;
import org.trinet.util.*;

/** * Create gif format snapshot of an event showing the first 'n' channels with phase picks.
 */
// 6/4/04 -DDG  changed to get connection info from the properties file rather than use
// TestDataSource which was hardwired to a dbase/username/passwd.

/*
UNIX:
/home/tpp/bin/javarun_jar org.trinet.web.SnapGif 9083992 15 120 /home/tpp/properties 9083992.gif

PC:
java -cp T:\CodeSpace\TPP\bin\jiggle.jar org.trinet.web.SnapGif 9083992 15 120 T:\CodeSpace\TPP\properties 9083992.gif
 */
public class SnapGif extends SnapShot {

    public static void main(String args[]) {

        if (args.length < 1) { // not enough args
          System.out.println("Usage: SnapGif <evid> [tracesPerPage] [secsPerPage] [property-file] [out-file]");
          System.out.println("     defaults: <evid> ["+ntraces+"] ["+maxSecs+"] ["+propertyFileName+"] [<evid>.gif]");
          System.out.println("Set tracesPerPage, secsPerPage command line value <= 0, to instead use the value of");
          System.out.println("properties 'tracesPerPage', 'secsPerPage' in the properties file, else the defaults.");

          System.exit(-1);

        }

        BenchMark bm = new BenchMark();
        bm.reset();

        // event ID
        long evid = Long.parseLong(args[0]);
    
        // read in the properties file
        if (args.length > 3) propertyFileName = args[3]; // aww override default name
        setProperties(propertyFileName);

        // Override properties with values specified on command line
        // # of traces
        int ival = 0;
        if (args.length > 1) {
          ival = Integer.parseInt(args[1]);
          if (ival > 0) ntraces = ival;
        }
        // max. seconds to plot
        if (args.length > 2) {
          ival = Integer.parseInt(args[2]);
          if (ival > 0) maxSecs = ival;
        }
        if (debug) System.out.println("ntraces: " + ntraces +" maxSecs: "+maxSecs);
        
        // destination .gif file
        String gifFile = System.getProperty("user.dir") + GenericPropertyList.FILE_SEP + evid + ".gif";  //default
        if (args.length > 4) {
            gifFile = args[4];
        }
        System.out.println("Output file = "+gifFile);
                
        System.out.println("Making db connection...");
        DataSource init = new DataSource();
        DataSource.set(getProperties().getDbaseDescription()); // aww 2008/06/04
        if (debug) System.out.println(init.toString());

        // Get and setup the wave client
        setupWaveClient();

        // ////////////////////////////////////////////////////////////////////
        // Makes the .gif file, returns the grapical component in case you want to display it below
        Component view = makeGif(evid, ntraces, maxSecs, gifFile);

        // Return codes:
        if (view == null) {
            bm.printTimeStamp("Error: makeGif view component null, exit status = -1", System.err);
            System.exit(-1);        // failure
        } else {
            bm.printTimeStamp("makeGif view completed, exit status = 0", System.err);
            if (!debug) System.exit(0);        // success
        }
        
//            JFrame frame = new JFrame("SnapGif of "+evid);
//
//            frame.addWindowListener(new WindowAdapter() {
//                public void windowClosing(WindowEvent e) {System.exit(-3);}
//            });
//
//            frame.getContentPane().add( view );            // add scroller to frame
//
//            frame.setVisible(true);
//            frame.pack();

//            System.exit(0);        // success
    }

    public static Component makeGif(long id, int ntraces, int maxSecs, String outFile) {

        // must make instance because makeViewWithPhases() is not static
        SnapShot snapShot =  new SnapShot();
        Component view = null;
        if (makeViewMode < 2) {
            view = snapShot.makeViewWithPhases(id, ntraces, maxSecs);
        }
        else {
            view = snapShot.makeViewWithPhasesFast(id, ntraces, maxSecs);
        }
        //encodeGifFile(view, outFile);
        //encodeGifFile2(view, outFile);
        return encodeImageFile(view, outFile) ? view : null;
    }

} // end of class
