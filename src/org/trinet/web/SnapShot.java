package org.trinet.web;

import java.awt.*;
import java.util.*;
import java.io.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.imageio.*;
import javax.swing.*;
import org.trinet.filters.ButterworthFilterSMC;
import org.trinet.jasi.*;
import org.trinet.util.FilterIF;
import org.trinet.util.LeapSeconds;
import org.trinet.util.TimeSpan;
import org.trinet.jiggle.*;

import Acme.JPM.Encoders.GifEncoder;

/**
 * Make a snapshot on the screen of the first 'n' channels with phase picks. <p>
 *
 *  Usage: SnapShot [evid] [ntraces] [max_secs] [prop-file]
 *
 */

 /*
  *  THIS IS THE PARENT CLASS FOR PRODUCTION CLASSES LIKE
  *   - SnapGif
  *   - SnapGifFromList
  *   - SnapTrigger
 */
public class SnapShot {

    protected static MasterView  mv = null;            // the master view

    protected static final int NTRACES = 15;
    protected static int ntraces = NTRACES;

    protected static int maxSecs = 120;
    protected static int makeViewMode = 0;

    protected static String waveServerFile = "";

    protected static boolean debug = true;
    protected static boolean dumpMasterView = false;
    protected static boolean doImageIO = true;

    protected static JiggleProperties properties;

    protected static WFGroupPanel wfgPanel = new WFGroupPanel() ;

    protected static Dimension singlePanelSize = new Dimension(640, 60);

    protected static String propertyFileName = "snapshot.props";  // default name

    protected static String fontName = "Serif";
    protected static int fontSize = 18; 
    protected static int fontStyle = Font.BOLD; 
    protected static Color fontFgColor = Color.red; 
    protected static Color fontBgColor = new Color(Integer.parseInt("e8e8e8",16)); // light gray shade 

    public static Font labelFont = new Font(fontName, fontStyle, fontSize);

    // Don't load extraneous channel info (corrections, etc.) it takes a lot longer.
    {
      Channel.setLoadOnlyLatLonZ();
    }

    public static void main(String args[]) {

        if (args.length < 1) { 
          System.out.println("Usage: SnapShot <evid> [tracesPerPage] [secsPerPage] [property-file] [out-file] [displayInFrame]");
          System.out.println("      defaults: <evid> ["+ntraces+"] ["+maxSecs+"] ["+propertyFileName+"] [<evid>.gif] [false]");
          System.out.println("Set tracesPerPage, secsPerPage command line value <= 0, to instead use the value of");
          System.out.println("properties 'tracesPerPage', 'secsPerPage' in the properties file, else the defaults.");
          System.exit(0);
        }

        // event ID
        long evid = Long.parseLong(args[0]);
         System.out.println("Processing evid: "+ evid);

         // read in the properties file
        if (args.length > 3) {
            propertyFileName = args[3]; // aww override default name
        }
        setProperties(propertyFileName);

        // Override properties with values specified on command line
        // # of traces
        int ival = 0;
        if (args.length > 1) {
          ival = Integer.parseInt(args[1]);
          if (ival > 0) ntraces = ival;
        }
        // max. seconds to plot
        if (args.length > 2) {
          ival = Integer.parseInt(args[2]);
          if (ival > 0) maxSecs = ival;
        }

        System.out.println("Making db connection...");
        new DataSource();
        //init.getSource().configure(getProperties().getDbaseDescription());
        DataSource.set(getProperties().getDbaseDescription()); // aww 2008/06/04

        // Get and setup the wave client
        setupWaveClient();

        SnapShot snapShot =  new SnapShot();
        Component view = snapShot.makeViewWithPhases(evid, ntraces, maxSecs);

        if (view == null) {
          System.err.println("Error: Could not make snapshot view with phases.");
          System.exit(-1);
        }

        String fileName = (args.length > 4) ? args[4] : evid + ".gif";  

        SnapShot.encodeImageFile(view, fileName);

        if (args.length < 5 || !args[5].equalsIgnoreCase("true")) System.exit(0);

        // make a frame
        JFrame frame = new JFrame("SnapShot of "+evid);

        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {System.exit(0);}
        });

        frame.getContentPane().add( view );            // add scroller to frame
        frame.setVisible(true);
        frame.pack();

    }

/** Setup the WaveClient definition. */
    public static void setupWaveClient() {
      //
      org.trinet.jiggle.WaveServerGroup waveClientGrp = null;
      try {
        // Make a WaveClient
        getProperties().setupWaveServerGroup(); // added since default properties setup no longer does this by default 2009/01/05 -aww
        waveClientGrp = getProperties().getWaveServerGroup();  // note: need JiggleProperties not Generic

        if (waveClientGrp == null || waveClientGrp.numberOfServers() <= 0) {
          getProperties().dumpProperties();
          if (waveClientGrp == null) System.err.println("Error: SnapShot setupWaveClient, WaveServerGroup (waveClientGrp) is null");
          else System.err.println("Error: SnapShot waveClientGrp number of servers = " + waveClientGrp.numberOfServers());
          System.err.println(
              "    getDataFromWaveServer Error: no or bad waveservers in properties file. " + getProperties().getFilename());
          System.err.println("    waveServerGroupList = " + getProperties().getProperty("waveServerGroupList"));
          System.err.println("    currentWaveServerGroup = " + getProperties().getProperty("currentWaveServerGroup"));
          System.exit(-1);
        }
        //if (debug) {
          System.err.println("SnapShot setupWaveClient waveServerGroupList = " + getProperties().getProperty("waveServerGroupList"));
          System.err.println("SnapShot setupWaveClient currentWaveServerGroup = " + getProperties().getProperty("currentWaveServerGroup"));
        //}

        // Important step - Tell Waveform's where to get data
        int waveSrc = getProperties().getInt("waveformReadMode", -1);
        if ( waveSrc == AbstractWaveform.LoadFromDataSource) {
            if (DataSource.getSource() == null) getProperties().setupDataSource();
            AbstractWaveform.setWaveDataSource(DataSource.getSource());
        } else if (waveSrc == AbstractWaveform.LoadFromWaveServer) {
            AbstractWaveform.setWaveDataSource(waveClientGrp);
        }
       } catch (Exception ex) {
        System.err.println(ex.toString());
        ex.printStackTrace();
       }
       //AbstractWaveform.setWaveDataSource(getProperties().getWaveSource());
    }

    /**
     * Create a snapshot based on a list of channels
    */
    public Component makeViewFromList(long evid, ChannelableList clist, int maxSecs) {

      // Load the Solution
      Solution sol = Solution.create().getById(evid);
      double preEvent = 0.0;           // traces will start at OT

      // no solution
      if (sol == null) {
        System.err.println("SnapShot: makeViewFromList, found no event with id: " + evid);
        return null;
      }
          System.out.println("Event: " + sol.toNeatString());

      ArrayList ctwList = new ArrayList(clist.size());

      // traces will start at OT
      double start = sol.datetime.doubleValue() - preEvent;
      double end = start + maxSecs;

      // make a list of channel time windows
      ChannelTimeWindow ctw;
      // just in case input is not list of Channels, convert clist -aww
      Channelable [] ch = clist.getChannelableArray(); // aww 03/04/2005
      for (int i = 0; i<ch.length;i++) {
        ctw = new ChannelTimeWindow(ch[i].getChannelObj(), start, end);
        ctwList.add(ctw);
      }

      // define the Master View
      MasterView mv = new MasterView();
      if (debug) mv.setDebug(true);

      mv.phaseRetrieveFlag = true;  // getProperties().getBoolean("masterViewRetrievePhases", true);
      mv.ampRetrieveFlag = getProperties().getBoolean("masterViewRetrieveAmps", true);
      mv.codaRetrieveFlag = getProperties().getBoolean("masterViewRetrieveCodas", true);
      mv.loadSpectralAmps = getProperties().getBoolean("masterViewLoadSpectralAmps", false);
      mv.loadPeakGroundAmps = getProperties().getBoolean("masterViewLoadPeakGroundAmps", false);
      mv.loadPrefMags = false;

      int mode = MasterView.AlignOnTime;
      if (getProperties().isSpecified("masterViewWaveformAlignMode")) mode = getProperties().getInt("masterViewWaveformAlignMode");
      mv.setAlignmentMode(mode);
      if (getProperties().isSpecified("masterViewWaveformAlignVel")) mv.setAlignmentVelocity(getProperties().getDouble("masterViewWaveformAlignVel"));

      //mv.addSolution(sol);
      mv.addSolution(sol, false);
      mv.setSelectedSolution(sol);
      mv.setWaveformLoadMode(MasterView.LoadAllInForeground);
      System.out.println("SnapShot: mv defined by channel time window list (loads waveforms) ...");
      mv.defineByChannelTimeWindowList(ctwList);
      mv.distanceSort();
      // mv.loadWaveforms(); redundant, load is done by defineByChannelTimeWindowList above -aww 2009/01/12

      return makeView(mv);

    }
  
    public JPanel makeViewWithPhases(long evid, int ntraces, int maxSecs) {

            // Load the Solution
            Solution sol = Solution.create().getById(evid);

            // no solution
            if (sol == null) {
              System.err.println("SnapShot: makeViewWithPhases, found no event with id: " + evid);
              return null;
            }
                System.out.println("Event: "+sol.toString());

            // Phase list loading will be done by model sol.loadPhaseList();
            //if (debug) System.out.println("DEBUG: SnapShot makeViewWithPhases phases= "+sol.phaseList.size());            
            
            ChannelTimeWindowModel model = new PicksOnlyChannelTimeModel(sol);
            //System.out.println("SnapShot setting PicksOnlyChannelTimeModel properties...");
            model.setProperties(properties); // allows user to override defaults -aww 2012/03/02
            //NOTE: PicksOnlyModel default maxWindow=600 secs when no maxWindowSize property value is declared 
            if (maxSecs > 0) {
                System.out.println("SnapShot: resetting PicksOnly model max window size to: " + maxSecs);
                model.setMaxWindowSize((double)maxSecs);
            }

            // define the Master View
            MasterView mv = new MasterView();
            if (debug) mv.setDebug(true);
            mv.phaseRetrieveFlag = true;  // getProperties().getBoolean("masterViewRetrievePhases", true);
            mv.ampRetrieveFlag = getProperties().getBoolean("masterViewRetrieveAmps", true);
            mv.codaRetrieveFlag = getProperties().getBoolean("masterViewRetrieveCodas", true);
            mv.loadSpectralAmps = getProperties().getBoolean("masterViewLoadSpectralAmps", false);
            mv.loadPeakGroundAmps = getProperties().getBoolean("masterViewLoadPeakGroundAmps", false);
            mv.loadPrefMags = false;

            int mode = MasterView.AlignOnTime;
            if (getProperties().isSpecified("masterViewWaveformAlignMode")) mode = getProperties().getInt("masterViewWaveformAlignMode");
            mv.setAlignmentMode(mode);
            if (getProperties().isSpecified("masterViewWaveformAlignVel")) mv.setAlignmentVelocity(getProperties().getDouble("masterViewWaveformAlignVel"));

            //mv.addSolution(sol);
            mv.addSolution(sol, false);
            mv.setSelectedSolution(sol);
            model.setMaxChannels(ntraces);
            mv.setWaveformLoadMode(MasterView.LoadAllInForeground);
            System.out.println("SnapShot: mv defined by PicksOnly model (loads waveforms) ...");
            mv.defineByChannelTimeWindowModel(model, sol); // include sol to include picks

            mv.distanceSort();
          
            // mv.loadWaveforms(); redundant, load is done by defineByChannelTimeWindowModel above -aww 2009/01/12
                System.out.println("SnapShot: mv total wf count = "+mv.getWaveformCount());

            return makeView(mv);

    }

    public JPanel makeViewWithPhasesFast(long evid, int ntraces, int maxSecs) {
            
                // make preOT time 15% of maxSec up to a max of 20 sec
            double preOT = maxSecs * 0.15 ;
            if (preOT > 20.0) preOT = 20.0;
            double postOT = maxSecs - preOT;
            double maxDist = 99999.;
            
            // Load the Solution
            Solution sol = Solution.create().getById(evid);

            // no solution
            if (sol == null) {
              System.err.println("SnapShot: makeViewWithPhasesFast, found no event with id: " + evid);
              return null;
            }
                System.out.println("Event: "+sol.toString());

            double ot   = sol.datetime.doubleValue();
            TimeSpan ts = new TimeSpan(ot - preOT, ot + postOT);


            // define the Master View
            MasterView mv = new MasterView();
            if (debug) mv.setDebug(true);

//            mv.addSolution(sol);
//            mv.addSolution(sol, false);
//            mv.setSelectedSolution(sol);
//            model.setMaxChannels(ntraces);
//            ChannelTimeWindowModel model = new PicksOnlyChannelTimeModel(sol);
//            mv.defineByChannelTimeWindowModel(model);
//            mv.setLoadChannelData(true);
//            mv.loadChannelData();

            mv.setWaveformLoadMode(MasterView.LoadAllInForeground);
            mv.loadPrefMags = false;
            System.out.println("SnapShot: mv defined by phase list");
            mv.defineByPhaseList(sol, maxDist, ntraces, ts);
            System.out.println("SnapShot: mv loading waveforms...");
            mv.loadWaveforms();
            System.out.println("SnapShot: mv total wf count = "+mv.getWaveformCount());

            return makeView(mv);

    }

    public JPanel makeView(long evid, int ntraces, int maxSecs) {

            System.out.println("SnapShot: makeView evid = "+evid+ " ntrace: " + ntraces + " maxSecs: " + maxSecs);

        // Make the "superset" MasterView, need to know what's available
        MasterView mv1 = new MasterView();
        if (debug) mv1.setDebug(true);
        mv1.phaseRetrieveFlag = true;  // getProperties().getBoolean("masterViewRetrievePhases", true);
        mv1.ampRetrieveFlag = getProperties().getBoolean("masterViewRetrieveAmps", true);
        mv1.codaRetrieveFlag = getProperties().getBoolean("masterViewRetrieveCodas", true);
        mv1.loadSpectralAmps = getProperties().getBoolean("masterViewLoadSpectralAmps", false);
        mv1.loadPeakGroundAmps = getProperties().getBoolean("masterViewLoadPeakGroundAmps", false);
        mv1.loadPrefMags = false;

        // Don't load waveforms or channel data until we've winnowed down to channels with phases
        mv1.setWaveformLoadMode(MasterView.LoadNone);
        mv1.setLoadChannelData(false);
        boolean status = mv1.defineByDataSource(evid);
        if (!status) return null;        // no such event

        if (mv1.wfvList.size() == 0) {
          JPanel jp = new JPanel();
          jp.add(new Label("No waveform views"));                // bail if no wfViews
          return jp;
        }
            System.out.println("SnapShot: Total wf views = "+mv1.wfvList.size());

        // Create MasterView of channels with phases
        MasterView mv2 = MasterView.withPhasesOnly(mv1) ;
        if (debug) mv2.setDebug(true);
        if (mv2.wfvList.size() == 0) {
                System.out.println("SnapShot: No phases found.");
            JPanel jp = new JPanel();
            jp.add(new Label("No waveform have phases"));                // bail if no wfViews
            return jp;            // bail if no wfViews
        }
            System.out.println("SnapShot: Total wfviews with phases = "+ mv2.wfvList.size());

        // lookup lat/lon of the channels in the MasterView
        //  mv2.setLoadChannelData(true);
        //  mv2.loadChannelData();
            System.out.println("SnapShot: Distance sorting...");
        mv2.distanceSort();

        // limit to 'ntraces'
        MasterView mv = MasterView.subset(mv2, ntraces);

          System.out.println("SnapShot: Loading waveforms... "+ mv.wfvList.size());
        mv.setWaveformLoadMode(MasterView.LoadAllInForeground);

        int mode = MasterView.AlignOnTime;
        if (getProperties().isSpecified("masterViewWaveformAlignMode")) mode = getProperties().getInt("masterViewWaveformAlignMode");
        mv.setAlignmentMode(mode);
        if (getProperties().isSpecified("masterViewWaveformAlignVel")) mv.setAlignmentVelocity(getProperties().getDouble("masterViewWaveformAlignVel"));
        mv.loadWaveforms();

        return makeView(mv);

    }

    public void setWaveformPanelProperties() {
      wfgPanel.showPhaseDescriptions(getProperties().getBoolean("showPhaseDescriptions", true));
      wfgPanel.setShowPhaseCues(getProperties().getBoolean("showPhaseCues", true));
      wfgPanel.setShowSegments(getProperties().getBoolean("showSegments", false));
      wfgPanel.setTimeTicksFlag(getProperties().getInt("wfpanel.timeTicksFlag", 0));
      wfgPanel.setShowTimeLabel(getProperties().getBoolean("wfpanel.showTimeScaleLabel",true));
      if (getProperties().getBoolean("showAlternateWf", false)) {
        System.out.println("Snapshot: Filtered waveforms");
        WFPanel mwfp[] = wfgPanel.getWFPanelList().getArray();
        for ( int i=0; i < mwfp.length; i++) {
             mwfp[i].setFilter(getWaveformFilter());
             mwfp[i].setShowAlternateWf(true);
             //mwfp[i].scalePanel();
             //mwfp[i].dumpScaleValues();
        }
      }
    }

    protected FilterIF getWaveformFilter() {
        int rate = 100; // default at 100 sps
        String bwfType = getProperties().getProperty("filterType", "HIGHPASS");
        double bwfLoCut = getProperties().getDouble("filterCornerLowFreq", 1.);
        double bwfHiCut = getProperties().getDouble("filterCornerHighFreq", 20.);
        int bwfOrder = getProperties().getInt("filterOrder", 4);
        boolean bwfReverse = getProperties().getBoolean("filterReversed", false);
        ButterworthFilterSMC bwf = (bwfType.equalsIgnoreCase("BANDPASS")) ?
            ButterworthFilterSMC.createBandpass(rate, bwfLoCut, bwfHiCut, bwfOrder, bwfReverse) :
            ButterworthFilterSMC.createHighpass(rate, bwfLoCut, bwfOrder, bwfReverse);
        return bwf;
    }

    private void setVelocityModel(MasterView mv) { // method used in makeView(mv) below - aww 2008/08/11
      VelocityModelList vmList = getProperties().getVelocityModelList();
      String [] names =  vmList.getModelNames();
      if (names != null && names.length > 1) { // ok, we have a list of models
          String modelName = getProperties().getDefaultVelocityModelName(); // default to the default model
          for (int idx = 0; idx < names.length; idx++) {
            if ( mv.getSelectedSolution().isInsideRegion(names[idx]) ) {
                  System.out.println("SnapShot: TravelTime velocity model reset to : "+ names[idx]);
              modelName = names[idx];  // change to model name matching a named region found in GAZETTEER_REGION table
              break;
            }
          }
          TravelTime.setDefaultModel( vmList.getByName(modelName) );
      }
    }

    /**
     *  Make a master view Panel. If 'showPicks' is true show the phase picks
     * otherwise don't.
     */
    public JPanel makeView(MasterView mv) {

            System.out.println("SnapShot: Making wf group panel view...");

        // don't want red time ticks so ignore clock quality
        mv.setClockQualityThreshold(0.0);

        // If multiple velocity models are declared, whenever a solution's location is inside a velocity model named region
        // then change tTravelTime's static model to this region named velocity model, before creating the WFPanels - aww 2008/08/11
        setVelocityModel(mv);

        // add panels to group panel they won't be rendered until 'paint' is called
        wfgPanel.setMasterView(mv, false); // set true, to experiment with active -aww  
        wfgPanel.setSinglePanelSize(singlePanelSize);
        setWaveformPanelProperties();

        //limit time shown to maxSecs;
        if (mv.timeSpan.size() > getMaxSeconds() && getMaxSeconds() > 0) {
            // ! this can cause wfs to disappear (clipped at time > start+maxSeconds) if the solution datetime is set to bogus earliest trigger time
            System.out.println("SnapShot Reducing viewport to maxSeconds: " + getMaxSeconds() + " reset from a mv timespan of: " +  mv.timeSpan.size());
            // 2/14/2000 changed to OT - 15 secs. Bogus waveform retrievals made starting with earliest data shift all real data out of frame.
            double startTime = mv.getSelectedSolution().getTime() - 15.0;
            TimeSpan ts = new TimeSpan(startTime, startTime+getMaxSeconds());
            wfgPanel.setTimeBounds(ts);
            mv.setAllViewSpans(ts,false);
        }
        //
        else if (makeViewMode > 0 && mv.timeSpan.size() < getMaxSeconds()) {
            System.out.println("SnapShot Expanding viewport to maxSeconds: " + getMaxSeconds() + " reset from a mv timespan of: " +  mv.timeSpan.size());
            TimeSpan ts = mv.getViewSpan();
            ts.setEnd(ts.getStart()+getMaxSeconds());  // aliased to current masterview object
            wfgPanel.setTimeBounds(ts);
            mv.setAllViewSpans(ts,false);
        }
        //
        if (dumpMasterView) mv.dump();

        // Note: could derive a JScrollPane with row header like WFScroller but need viewport row header scrollbar increment ?
        // make another panel to support a border
        JPanel borderPanel = new JPanel();
        //borderPanel.setBorder(BorderFactory.createRaisedBevelBorder());
        borderPanel.setLayout(new BorderLayout());
        borderPanel.add(wfgPanel, BorderLayout.CENTER);
        borderPanel.setBackground(fontBgColor);

        // make a header label label
        String str = mv.getSelectedSolution().toSummaryString().trim();

        JLabel slabel = new JLabel(str);
        if (debug) System.out.println("SnapShot: makeView header font: " + labelFont);
        slabel.setFont(labelFont);
        slabel.setForeground(fontFgColor);
        //label.setBackground(fontBgColor);
        //label.setOpaque(true);

        //if (! getProperties().getBoolean("wfpanel.showTimeScaleLabel", true)) { // always show the extra info ?
            Box vbox = Box.createVerticalBox();

            Box hbox = Box.createHorizontalBox();
            hbox.add(Box.createHorizontalGlue());
            hbox.add(slabel);
            hbox.add(Box.createHorizontalGlue());
            vbox.add(hbox);

            hbox = Box.createHorizontalBox();
            String time = LeapSeconds.trueToString(mv.getViewSpan().getStart(), "HH:mm:ss.fff");
            JLabel tlabel = new JLabel(time.substring(0,11));
            tlabel.setForeground(fontFgColor);
            hbox.add(tlabel);
            hbox.add(Box.createHorizontalGlue());

            String tag = getProperties().getProperty("plotHeaderLabel", "");
            tlabel = new JLabel(String.format("%s Timespan=%4d s ", tag, Math.round(mv.getViewSpan().getDuration())));
            tlabel.setForeground(fontFgColor);
            hbox.add(tlabel);

            if ( getProperties().getBoolean("showAlternateWf", false) &&
                !getProperties().getProperty("wfpanel.channelLabel.form","filter").equals("filter") ) {
                 if (wfgPanel.getWFPanelList().size() > 0) {
                     tag = ((WFPanel)wfgPanel.getWFPanelList().get(0)).getWf().getFilterName();
                     tag += (getProperties().getBoolean("filterReversed", false)) ? " Reversed" : "";
                     tlabel = new JLabel(tag);
                     tlabel.setForeground(fontFgColor);
                     hbox.add(tlabel);
                     //hbox.add(Box.createHorizontalGlue());
                 }
            }

            if (getProperties().getProperty("wfpanel.scaleBy","").equalsIgnoreCase("noise")) {
                String ntype = "";
                if (WFSegment.scanNoiseType == 0) ntype = "AVG";
                else if (WFSegment.scanNoiseType == 1) ntype = "RMS";
                else if (WFSegment.scanNoiseType == 2) ntype = "CODA"; 
                tlabel = new JLabel(
                  String.format(" (amp min/max is %s noise over %3.1f s x %3.1f)",
                      ntype,
                      //getProperties().getDouble("wfpanel.noiseScanSecs","6."
                      WFDataBox.NOISE_SCAN_SECS,
                      //getProperties().getDouble("wfpanel.noiseScalar","10."),
                      WFDataBox.NOISE_SCALAR
                  )
                );
                tlabel.setForeground(fontFgColor);
                hbox.add(tlabel);
                //hbox.add(Box.createHorizontalGlue());
            }
            else if (getProperties().getProperty("wfpanel.scaleBy","").equalsIgnoreCase("gain")) {
                tlabel = new JLabel(
                  String.format(" (amp min/max for vel=%7.5f cm/s, for acc=%6.4f cm/s/s)",
                        WFDataBox.MAX_ACC_GAIN,
                        WFDataBox.MAX_VEL_GAIN
                  )
                );
                tlabel.setForeground(fontFgColor);
                hbox.add(tlabel);
                //hbox.add(Box.createHorizontalGlue());
            }

            hbox.add(Box.createHorizontalGlue());
            time = LeapSeconds.trueToString(mv.getViewSpan().getEnd(), "HH:mm:ss.fff");
            tlabel.setForeground(fontFgColor);
            tlabel = new JLabel(time.substring(0,11));
            hbox.add(tlabel);
            tlabel.setForeground(fontFgColor);

            vbox.add(hbox);
            borderPanel.add(vbox, BorderLayout.NORTH);

        //}
        //else borderPanel.add(slabel, BorderLayout.NORTH); // old style 1-line header

        //wfgPanel.resetPanelBoxSize(); // ? for amp scaling ? 

        return borderPanel;

    }

    /**
     *
     */
    public static boolean encodeGifFile(Component view, String gifFile) {

        if (view == null) return false;

        try {
            // component must be in a frame for this to work
            JFrame frame =  new JFrame();
            frame.add(view); // add view to frame content
            //frame.setSize(view.getSize());
            frame.pack();
            
            // Creates an off-screen drawable image to be used for double buffering. (it will be blank)
            // image must be SAME size as view
            if (debug) System.out.println("SnapShot encodeGifFile: making image..."+view.getWidth()+"/"+ view.getHeight());
            Image image = view.createImage(view.getWidth(), view.getHeight());
            
            // get the graphics context of the image
            if (debug) System.out.println("Getting graphics object...");
            Graphics g = image.getGraphics();

            // copy the frame into the image's graphics context
            if (debug) System.out.println("Printing graphics object...");
            view.printAll(g);  // walk down whole component tree
            g.dispose();

            // open the output file
            if (debug) System.out.println("Opening file...");
            File file = new File(gifFile);
            FileOutputStream out = new FileOutputStream(file);

            // create the encoder and encode it
            if (debug) System.out.println("Encoding...");
            GifEncoder encoder = new GifEncoder(image, out);
            encoder.encode();

            if (debug) System.out.println("Flushing...");
            out.flush();

            if (debug) System.out.println("Close file...");
            out.close();

        } catch (FileNotFoundException exc) {
            System.err.println("Error: SnapShot encodeGifFile, file not found: " + gifFile);
            return false;
        } catch (IOException exc) {
            System.err.println("Error: SnapShot encodeGifFile "+ exc.toString());
            return false;
        } catch (OutOfMemoryError err) {
            System.err.println("Error: SnapShot encodeGifFile, Out of memory error: "+ err.toString());
            return false;
        }

        return true;
    }

    public static boolean encodeImageFile(Component view, String outFile) {

        if (view == null) return false;
        Dimension d = view.getPreferredSize();
        //if (debug) System.out.println("SnapShot encodImageFile view preferredSize: " + d + " to outFile: " + outFile);
        view.setSize(d);

        //
        // Note to pack view into a JFrame, if headless system, we need XvLib app running on linux,solaris
        //
        // ? one of these 3 settings effects image displayable
        // but still seeing the offsets in time scale or panel amp center
        // Still need to find which one does what here
        if (debug) System.out.println("Snapshot encodeImageFile: validating view components...");
        ((JComponent)view).setDoubleBuffered(false); // ??
        view.addNotify(); // ? displayable ??
        view.validate(); // ?  layout ??

        // Try a set size test here ? does it clear up amp clipping offset ?
        d = view.getPreferredSize();
        ((JComponent)view).setPreferredSize(new Dimension(d.width+2,d.height+2));

        //if (debug) System.out.println("SnapShot encodImageFile view preferredSize: " + d + " to outFile: " + outFile);

        boolean status = true;
        FileOutputStream fos = null;
        File file = null;
        try {
            BufferedImage image = new BufferedImage(d.width, d.height, BufferedImage.TYPE_INT_RGB);
            //BufferedImage image = createImage((JComponent) view, BufferedImage.TYPE_INT_RGB);
            Graphics g = image.getGraphics();
            /*
            if (! view.isDisplayable()) {
                if (debug) System.out.println("Snapshot encodeImageFile: View not displayable, laying out view components...");
                layoutComponent(view);
            }
            if (!view.isOpaque()) {
              if (debug) System.out.println("Snapshot encodeImageFile: view is not opaque, filling image background!");
              g.setColor( view.getBackground() );
              g.fillRect(0, 0, d.width, d.height);
            }
            */
            //view.paint(g); // test 2013/03/05
            view.printAll(g);
            g.dispose();

            String ityp = outFile.substring(outFile.lastIndexOf(".")+1);
            if (doImageIO) {
              boolean tf = ImageIO.getImageReadersByFormatName(ityp).hasNext();
              if (!tf) System.err.println("SnapShot ImageIO cannot read format: " + ityp);
              tf = ImageIO.getImageWritersByFormatName(ityp).hasNext();
              if (!tf) System.err.println("SnapShot ImageIO cannot write format: " + ityp);
              tf = ImageIO.getImageWritersBySuffix(ityp).hasNext();
              if (!tf) System.err.println("SnapShot ImageIO cannot write file: " + ityp);
              if (!tf && doImageIO) { 
                doImageIO = false;
                System.err.println("Known ImageIO formats: "+Arrays.asList(ImageIO.getWriterFormatNames()));
                if (ityp.equalsIgnoreCase("gif")) System.err.println("SnapShot encodImageFile defaulting to: Acme.JPM.Encoders.GifEncoder");
              }
            }

            file = new File(outFile);
            file = file.getCanonicalFile();
            if ( ! file.getParentFile().exists()) {
                System.err.println("SnapShot encodeImageFile, error out file path D.N.E.:)" + file);
                status = false;
            }
            else if ( file.exists() && ! file.canWrite() ) {
                System.err.println("SnapShot encodeImageFile, error out file already exists, but not rewriteable, check permissions,owner: " + file);
                status = false;
            }
            else {
              if (doImageIO) {
                fos = new FileOutputStream(outFile);
                status = ImageIO.write(image, ityp, fos);
                //status = ImageIO.write(image, ityp, file); // is there is difference in result file vs. fos?
              }
              else if (ityp.equalsIgnoreCase("gif")) {
                fos = new FileOutputStream(outFile);
                new GifEncoder(image, fos).encode();
              }
              else {
                System.err.println("Unknown image writer format: " + ityp +
                        " Known formats: " + Arrays.asList(ImageIO.getWriterFormatNames()));
                status = false;
              }
            }

        } catch (FileNotFoundException exc) {
            System.err.println("Snapshot encodeImageFile, file not found, or unopenable: " + file);
            System.err.println("  check out dir permissions, or existing file ownership");
            status = false;
        } catch (IOException exc) {
            System.err.println("Snapshot encodeImageFile, IO error:");
            exc.printStackTrace();
            status = false;
        } catch (OutOfMemoryError exc) {
            System.err.println("Snapshot encodeImageFile, Out of memory error:");
            exc.printStackTrace();
            status = false;
        } catch (Exception exc) {
            exc.printStackTrace();
            status = false;
        }
        finally {
            if (fos != null) {
                try {
                    //if (debug) System.out.println("Snapshot: Flushing and closing output stream...");
                    fos.flush();
                    fos.close();
                }
                catch (IOException exc) { }
            }
        }

        if (status) System.out.println("SnapShot encodeImageFile, SUCCESS writing: "+ file); 
        else System.out.println("SnapShot encodeImageFile, FAILED writing: "+ file); 

        return status;
    }

/*
    private static void layoutComponent(Component component) {
        synchronized (component.getTreeLock()) {
            component.doLayout();
            if (component instanceof Container) {
                for (Component child : ((Container)component).getComponents()) {
                    layoutComponent(child);
                }
            }
        }
    }
*/

/*
    public static BufferedImage createImage(JComponent component, int imageType) {

        Dimension componentSize = component.getPreferredSize();
        component.setDoubleBuffered(false);
        component.setSize(componentSize);
        component.addNotify();
        component.validate();

        BufferedImage img = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().createCompatibleImage(
                component.getSize().width, component.getSize().height
        );

        Graphics2D g2d = img.createGraphics();
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, img.getWidth(), img.getHeight());
        component.print(g2d);
        g2d.dispose();

        return img;
    }
*/

 
/*
   public static boolean encodeGifFile2(Component view, String gifFile) {

        if (view == null) return false;

        try {
            // component must be in a frame for this to work
            Dimension viewSize = view.getSize();
            JFrame frame =  new JFrame();
            frame.getContentPane().add( view );            // add scroller to frame
            frame.setSize(viewSize);
            frame.pack();
            // Creates an off-screen drawable image to be used for double buffering. (it will be blank)
            if (debug) System.out.println("making image...");
            Image image = view.createImage(view.getWidth(), view.getHeight());
            Graphics g = image.getGraphics();
            view.printAll(g);  // walk down whole component tree
        
            // open the output file
            if (debug) System.out.println("opening file...");
            File file = new File(gifFile);
             FileOutputStream out = new FileOutputStream(file);
             
            // create the encoder
            if (debug) System.out.println("encoding 2...");
            com.gif4j.light.GifEncoder.encode(image, out);
            out.flush();
            out.close();
            
//            String comment = "";
//            saveImageAsGif(image, comment, file);
            
        } catch (FileNotFoundException exc) {
            System.err.println("File not found: " + gifFile);
            return false;
        } catch (IOException exc) {
            System.err.println("IO error: "+ exc.toString());
            return false;
        } catch (OutOfMemoryError err) {
            System.err.println("Out of memory error: "+ err.toString());
            return false;
        }

//        } catch (InterruptedException exc) {
//            System.err.println("Interrupted: "+ exc.toString());
//            return false;
//        }
        
        return true;
   }
*/
    
//   public static void saveImageAsGif(Image image, String comment, File fileToSave)
//   throws IOException, InterruptedException {
//                // create new GifImage
//                GifImage gifImage = new GifImage();
//                // create new GifFrame
//                GifFrame gifFrame = new GifFrame(image);
//                // add GifFrame to GifImage
//                gifImage.addGifFrame(gifFrame);
//                // add our comment
//                gifImage.addComment(comment);
//                // save gifImage
//                com.gif4j.light.GifEncoder.encode(gifImage, fileToSave);
//        }
//

    public static void setMaxSeconds(int secs) {
        maxSecs = secs;
    }
    public static int getMaxSeconds() {
        return maxSecs;
    }

    public static void setSinglePanelSize(Dimension size) {
        singlePanelSize = size;
    }

    /**
     * Set the properties.
     * 
     * @param fileName the file name which may be an absolute path or relative to
     *                 the default user and system paths.
     */
    public static void setProperties(String fileName) {
        final JiggleProperties props;
        final File file = new File(fileName);
        if (file.exists()) { // if the file exists
            // use the absolute path
            fileName = file.getAbsolutePath();
            props = new JiggleProperties(fileName, null);
        } else {
            props = new JiggleProperties(fileName);
        }
        System.out.printf("Properties file = %s\n", props.getUserPropertiesFileName());
        setProperties(props);
    }

    public static void setProperties(JiggleProperties props) {

        properties = props;

        if (props.isSpecified("debug"))
            debug = props.getBoolean("debug");

        if (props.isSpecified("dumpMasterView"))
            dumpMasterView = props.getBoolean("dumpMasterView");

        if (props.getBoolean("dumpProperties")) {
            System.out.println("SnapShot dump of properties:");
            props.dumpProperties();
        }

        if (props.isSpecified("wfpanel.singlePanelSize")) {
          String[] ir = props.getStringArray("wfpanel.singlePanelSize");
          if (ir.length == 2) {
            try { 
              int width = Integer.parseInt(ir[0]);
              int height = Integer.parseInt(ir[1]);
              if (width > 100 && height > 20) {
                 setSinglePanelSize(new Dimension(width, height));
              }
            }
            catch (NumberFormatException ex) {
                System.err.println("Error: SnapShot setProperties for wfpanel.singlePanelSize=" + props.getProperty("wfpanel.singlePanelSize"));
            }
          }
          else {
                System.err.println("Error: SnapShot setProperties for wfpanel.singlePanelSize=" + props.getProperty("wfpanel.singlePanelSize"));
          }
        }

        if (props.isSpecified("summaryLabel.font.fg")) {
            fontFgColor = props.getColor("summaryLabel.font.fg");

        }
        if (props.isSpecified("summaryLabel.font.bg")) {
            fontBgColor = props.getColor("summaryLabel.font.bg");
        }
        if (props.isSpecified("summaryLabel.font.size")) { 
            fontSize = props.getInt("summaryLabel.font.size", 18); 
        }
        if (props.isSpecified("summaryLabel.font.style")) {
            String style = props.getProperty("summaryLabel.font.style", "plain");
            if (style.equalsIgnoreCase("bold"))  fontStyle = Font.BOLD; 
            else if (style.equalsIgnoreCase("italic"))  fontStyle = Font.ITALIC; 
            else if (style.equalsIgnoreCase("plain")) fontStyle = Font.PLAIN; 
        }
        if (props.isSpecified("summaryLabel.font.name")) {
            //e.g. Serif SansSerif Monospaced...
            fontName = props.getProperty("summaryLabel.font.name", "Serif"); 
            labelFont = new Font(fontName, fontStyle, fontSize);
        }

        if (props.isSpecified("makeViewMode"))
          makeViewMode = props.getInt("makeViewMode");

        if (props.isSpecified("secsPerPage"))
          maxSecs = Math.abs(props.getInt("secsPerPage"));

        if (props.isSpecified("tracesPerPage"))
          ntraces = props.getInt("tracesPerPage");

        if (props.isSpecified("doImageIO"))
            doImageIO = props.getBoolean("doImageIO");
    }

    public static JiggleProperties getProperties() {
        return properties;
    }

} // end of class
