package org.trinet.web;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.*;
import java.io.*;
import javax.imageio.*;
import javax.swing.*;

import org.trinet.jasi.*;
import org.trinet.util.gazetteer.*;
import org.trinet.jiggle.MasterView;
import org.trinet.util.GenericPropertyList;

//import Acme.JPM.Encoders.GifEncoder;

/**
 * Make a Gif snapshot for a trigger.<p>
 * Usage: SnapTrigger <evid> [ntraces] [max_secs] [property-file] [out-file]<p>
 * The property file must contain connection information for the database and
 * the waveservers. Both triggered channels and channel in triggered subnets are included
 * in a trigger. The are found in the table TRIG_CHANNEL.
 * If the total number channels exceeds the value of 'ntraces' the non-triggered
 * channels will be tossed out. If there are still too many the more distant ones are deleted.
 */

 // 6/4/04 -DDG  changed to get connection info from the properties file rather than use
 // TestDataSource which was hardwired to a dbase/username/passwd.

public class SnapTrigger extends SnapShot {

    // Absolute max channels to plot, ever. 
    // This is a reaction to running out of memory
    // when trying top make a gif for a 239 channel trigger.
    protected static final int MAXSIZE = 180;

    public Component makeViewOfTrigger(long evid, int ntraces, int maxSecs) {

        // Create a time-ordered list of channels triggered for this evid using the Trig_Channel table.
        TriggerTN trigger = new TriggerTN(evid);
        return makeViewOfTrigger(trigger, ntraces, maxSecs);
    }

    /** Make snapshot of this trigger. 
     * Includes channels that were not triggered but were in
     * triggered subnets.  If this set exceeds 'ntraces' the set will be 
     * trimmed back to include only triggered channels. If that set is too big
     * the list will be truncated. */    
    public Component makeViewOfTrigger(TriggerTN trigger, int ntraces, int maxSecs) {

        if (trigger == null) return null;

        // retreive the list of CTW's
        ChannelableList windowList = (ChannelableList) trigger.getChannelTimeWindows();

        if (windowList == null || windowList.size() == 0) {
           System.out.println("No channels in trigger list!");
           return null;
        }

        // TOO MANY, ONLY GET TRIGGERED CHANNELS
        if (windowList.size() > ntraces) {
            System.out.println("SnapTrigger: Trigger has more channels " + windowList.size() +
                    " than requested count: " + ntraces + " removing untriggered channels");
            windowList = (ChannelableList) trigger.getTriggeredChannelTimeWindows();
            if (windowList.size() > ntraces) {
                // STILL TOO MANY, TRUNCATE LIST (assumes sorted by time)
                System.out.println("! Truncating end of list of " + windowList.size()+
                                " triggered channels to requested count.");
                for (int i = ntraces; i < windowList.size(); i++) {
                    windowList.remove(i);
                    windowList.trimToSize();   // tidy up memory
                }
            }
        }
        else {
            windowList.sort(new TriggerChannelTimeWindowModel.TriggerChannelTimeWindowSorter(1));
        }

        mv = new MasterView();
        mv.phaseRetrieveFlag =getProperties().getBoolean("masterViewRetrievePhases", true);
        mv.ampRetrieveFlag = getProperties().getBoolean("masterViewRetrieveAmps", true);
        mv.codaRetrieveFlag = getProperties().getBoolean("masterViewRetrieveCodas", true);
        mv.loadSpectralAmps = getProperties().getBoolean("masterViewLoadSpectralAmps", false);
        mv.loadPeakGroundAmps = getProperties().getBoolean("masterViewLoadPeakGroundAmps", false);

        mv.solList.add(trigger.sol);
        mv.solList.setSelected(trigger.sol);

        mv.setLoadChannelData(true);
        mv.setWaveformLoadMode(MasterView.LoadAllInForeground);

        //
        mv.defineByTriggerChannelTimeWindowList(windowList); // loads waveforms
        // set the location to the earliest trigger time
        TriggerChannelTimeWindow tctw  = trigger.getEarliestChannel();
        Channel chan1 = tctw.getChannelObj();
        chan1 = chan1.lookUp(chan1, trigger.sol.getDateTime()); // lookup location

        // use earliest channel location as trial trigger location
        if (getProperties().getProperty("triggerSortOrder","dist").toLowerCase().startsWith("dist")) {
            LatLonZ loc = chan1.getLatLonZ();
            trigger.sol.lat.setValue(loc.getLat());
            trigger.sol.lon.setValue(loc.getLon());
            //trigger.sol.depth.setValue(0.1);
            trigger.sol.depth.setValue(loc.getZ()+5.0); // changed default to 5 km less station elev -aww 2015/10/09
            trigger.sol.mdepth.setValue(5.0); // aww changed default to 5 km -aww 2015/10/09
            // reset origin time to earliest trigger time? bogus early pick can result in missing wfs!
            //trigger.sol.setTime(tctw.triggerTime); // removed 04/14/2014 -aww blank waveforms when early pick is before main energy on/off times
            mv.distanceSort();
        }

        return makeView(mv);

    }

    public Component makeGif(long id, int ntraces, int maxSecs, String outFile) {
        // trigger view
        Component view = makeViewOfTrigger(id, ntraces, maxSecs);
        //encodeGifFile(view, outFile);
        return encodeImageFile(view, outFile) ? view : null;
    }

/*
    public boolean encodeGifFile(Component view, String gifFile) {

        if (view == null) return false;

        try {
            // component must be in a frame for this to work
            JFrame frame =  new JFrame();
                frame.getContentPane().add( view );            // add scroller to frame
                frame.setSize(view.getSize());
                frame.pack();
                
            // Create an image (it will be blank)
            if (debug) System.out.println("making image..."+view.getWidth()+"/"+ view.getHeight());

            Image image = view.createImage(view.getWidth(), view.getHeight());

            // get the graphics context of the image
            if (debug) System.out.println("getting graphics object...");
            Graphics g = image.getGraphics();


            // copy the frame into the image's graphics context
            if (debug) System.out.println("printing graphics object...");
            view.print(g);

            // open the output file
            if (debug) System.out.println("opening file...");
            File file = new File(gifFile);
             FileOutputStream out = new FileOutputStream(file);


            if (debug) System.out.println("encoding..."); 

            // create the encoder
            GifEncoder encoder = new GifEncoder(image, out);

            // encode it
            encoder.encode();

            if (debug) System.out.println("Flushing...");

            out.flush();
            if (debug) System.out.println("Close file...");
            out.close();


        } catch (FileNotFoundException exc) {
            System.err.println("File not found: " + gifFile);
            return false;
        } catch (IOException exc) {
            System.err.println("IO error: "+ exc.toString());
            return false;
        } catch (OutOfMemoryError err) {
            System.err.println("Out of memory error: "+ err.toString());
            return false;
        }

        return true;
    }
*/

    // /////////////////////////////////////////////////////////////////////////////
    public static void main(String args[]) {

        if (args.length < 1) { // not enough args
          System.out.println("Usage: SnapTrigger <evid> [ntraces] [max_secs] [property-file] [out-file]");
          System.out.println("         defaults: <evid> ["+ntraces+"] ["+maxSecs+"] ["+propertyFileName+"] [<evid>.gif]");
          System.exit(-1);
        }

        // event ID
        long evid = Long.parseLong(args[0]);
    
        // read in the properties file
        if (args.length > 3) propertyFileName = args[3]; // aww override default name
        setProperties(propertyFileName);

        // Override properties with values specified on command line
        // # of traces
        int ival = 0;
        if (args.length > 1) {
          ival = Integer.parseInt(args[1]);
          if (ival > 0) ntraces = ival;
        }

        // set absolute max
        if (ntraces > MAXSIZE) {
          ntraces = MAXSIZE;
          System.out.println("<ntraces> exceeds max. allowed, setting to "+MAXSIZE);
        }

        // max. seconds to plot
        if (args.length > 2) {
          ival = Integer.parseInt(args[2]);
          if (ival > 0) maxSecs = ival;
        }

        // destination .gif file
        String gifFile = System.getProperty("user.dir") + GenericPropertyList.FILE_SEP + evid + ".gif";  //default
        if (args.length > 4) {
            gifFile = args[4];
        }

        System.out.println("Making connection...");
        new DataSource();
        // Set connection based on properties file info
        //DataSource.getSource().configure(getProperties().getDbaseDescription());
        DataSource.set(getProperties().getDbaseDescription()); // aww 2008/06/04

        // Get and setup the wave client
        setupWaveClient();

        setupHeadless();

        // Makes the .gif file, returns the grapical component in case you want
        // to display it below
        SnapTrigger snapTrigger = new SnapTrigger();
        Component view = snapTrigger.makeGif(evid, ntraces, maxSecs, gifFile);

        /* make a frame
        if (view != null) {
            JFrame frame = new JFrame("SnapTrigger of "+evid);
            frame.addWindowListener(
               new WindowAdapter() {
                  public void windowClosing(WindowEvent e) {
                    System.exit(-3);
                  }
               }
            );
            frame.getContentPane().add(new JScrollPane(view));  // add scroller to frame
            view.setSize(new Dimension(800, 1200));
            frame.pack();
            frame.setVisible(true);
        }
        */

        // Return codes:
        if (view == null) {
            System.err.println("exit status = -1");
            System.exit(-1);        // failure
        } else {
            if (debug) {
               System.err.println("exit status = 0");
            }
            System.exit(0);        // success
        }

    }

    private static void setupHeadless() {
        // Call this BEFORE the toolkit has been initialized, that is,
        // before Toolkit.getDefaultToolkit() has been called.
        System.setProperty("java.awt.headless", "true");
        // This triggers creation of the toolkit.
        // Because java.awt.headless property is set to true, this will be an instance of headless toolkit.
        // Check whether the application is running in headless mode.
        GraphicsEnvironment.getLocalGraphicsEnvironment();
        System.out.println("Headless mode: " + GraphicsEnvironment.isHeadless());
    }

} // end of class
