package org.trinet.web;

import java.sql.*;
import java.util.*;

import org.trinet.jdbc.*;
import org.trinet.jasi.*;
import org.trinet.util.TimeSpan;

/**
 * Returns a collection of ChannelTimeWindows for a triggered event
 * based on the entries in the 'trig_channel' table produce by the Earthworm
 * trigger program CarlTrig.
 * 
 * http://folkworm.ceri.memphis.edu/ew-doc/ovr/carltrig_ovr.html
 *
 * The trigger times are always on a whole second and occur after the energy.
 *
 * Created: Mon Oct 16 13:15:10 2000
 *
 * @author Doug Given
 * @version
 */
// 5/30/07 DDG - changed all returns from Collection to ChannelableList.
public class TriggerTN {

       public Solution sol;
       Connection conn = DataSource.getConnection();

       /** A colloction of TriggerChannelTimeWindow objects */
       ChannelableList ctwList;
       
       /** Time window that contains all trigger window. I.e. maximal window. */
       TimeSpan timeSpan = new TimeSpan();

       public static double preTrigSecs = 10.;
       public static double postTrigSecs = 5.;

    /**
     *  Create a Solution object. DataSource must be established first.
     */
    public TriggerTN() {
        //        setConnection(DataSource.conn);        // use default connection

    }

    public TriggerTN(Solution sol) {
          setSolution(sol);
    }
/** Creates a list of channels that were triggered for this event ID. */
    public TriggerTN(long evid) {

        Solution sol = Solution.create().getById(evid);
        setSolution(sol);
    }

    /**
     *  Create a trigger object. DataSource must be established first.
     */
    public TriggerTN(Connection conn)
    {
         setConnection(conn);
    }

    public void setConnection(Connection conn) {
      this.conn = conn;
    }

    /** Sets the solution and produces the triggered channel list.*/
    public void setSolution(Solution sol) {
      this.sol = sol;

      // System.out.println("TriggerTN sol = "+sol.toString());
      ctwList = getBySolution(sol);

      TriggerChannelTimeWindow array[] =
              (TriggerChannelTimeWindow[]) ctwList.toArray(new TriggerChannelTimeWindow[ctwList.size()]);

      boolean isStale = sol.isStale();
      boolean needsCommit = sol.getNeedsCommit();
      for (int idx=0; idx<array.length; idx++) {
          if (array[idx].triggerTime != 0.0) {
              Phase ph = Phase.create();
              ph.setChannelObj(array[idx].getChannelObj());
              ph.setTime(array[idx].triggerTime);
              ph.description = new PhaseDescription("P", "w", ".", 4);
              ph.setProcessingState(JasiProcessingConstants.STATE_AUTO_TAG);
              ph.assign(sol);
              sol.getPhaseList().addOrReplace(ph);
          }
      }

      // reset flags after adding "pseduo-phases"
      sol.setStale(isStale);
      sol.setNeedsCommit(needsCommit);

    }

    public TimeSpan getTimeSpan() {
        return timeSpan;
    }
    
    /** Return the Collection of TriggerChannelTimeWindows for channels that
     * were triggered during this event trigger. This excludes channels that
     * were not triggered but were in triggered subnets. */
//    public Collection getTriggeredChannelTimeWindows() {
    public ChannelableList getTriggeredChannelTimeWindows() {
        //ArrayList tlist = new ArrayList();
        ChannelableList tlist = new ChannelableList();
        TriggerChannelTimeWindow ctw[] =
              (TriggerChannelTimeWindow[]) ctwList.toArray(new TriggerChannelTimeWindow[0]);

           if (ctw.length == 0) return tlist;

           for (int i = 0; i < ctw.length; i++) {
               if (ctw[i].triggerTime != 0.0) tlist.add(ctw[i]);
               timeSpan.include(ctw[i].getTimeSpan());    // keep track of maximal window
           }
           
       return tlist;
    }

    /** Return the Collection of TriggerChannelTimeWindows that go with this
    * trigger. This includes channels that were not triggered but were in
    * triggered subnets.  */
    public ChannelableList getChannelTimeWindows() {
       return ctwList;
    }

    /** Return the TriggerChannelTimeWindow with the earliest trigger time.
    * If there are no trigger times the first TriggerChannelTimeWindow in the list
    * is returned. If there is no list, a virgin TriggerChannelTimeWindow is returned. */
    public TriggerChannelTimeWindow getEarliestChannel() {

           TriggerChannelTimeWindow ctw = new TriggerChannelTimeWindow();

           TriggerChannelTimeWindow array[] =
              (TriggerChannelTimeWindow[]) ctwList.toArray(new TriggerChannelTimeWindow[0]);

           if (array.length == 0) return ctw;

           double earliest = Double.MAX_VALUE;

           for (int i = 0; i < array.length; i++) {
             //if (array[i].triggerTime != 0.0 && // some channels may have bad trigger times, so try > 2000/01/01 for test default -aww
             if (array[i].triggerTime > 946684822.0 && array[i].triggerTime < earliest) {
                 ctw = array[i];
                 earliest = ctw.triggerTime;
             }
           }

      if (earliest == Double.MAX_VALUE) ctw = array[0];
      return ctw;
    }
    /**
     * Returns a collection of ChannelTimeWindows for this triggered event
     * based on the entries in the 'trig_channel' table produce by the Earthworm
     * trigger program.  Collection will be ordered by trigger time. */
    protected ChannelableList getBySolution(Solution sol) {
        if (sol == null) return null;
        return getById(sol.id.longValue());
    }
/**
 * Returns a collection of ChannelTimeWindows for this triggered event
 * based on the entries in the 'trig_channel' table produce by the Earthworm
 * trigger program.  Collection will be ordered by trigger time.*/
 /*
SQL> describe Trig_Channel
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 TRIGID                                    NOT NULL NUMBER(15)
 NTID                                      NOT NULL NUMBER(15)
 AUTH                                      NOT NULL VARCHAR2(15)
 SUBSOURCE                                          VARCHAR2(8)
 STA                                       NOT NULL VARCHAR2(6)
 NET                                                VARCHAR2(8)
 CHANNEL                                            VARCHAR2(3)
 CHANNELSRC                                         VARCHAR2(8)
 SEEDCHAN                                           VARCHAR2(3)
 LOCATION                                           VARCHAR2(2)
 TRIGFLAG                                           VARCHAR2(1)
 DATETIME                                  NOT NULL NUMBER(25,10)
 SAVESTART                                 NOT NULL NUMBER(25,10)
 SAVEEND                                   NOT NULL NUMBER(25,10)
 RFLAG                                              VARCHAR2(2)
 LDDATE                                             DATE
 */
    protected ChannelableList getById(long evid) {
        // switched query's column order put savestart,savend before datetime -aww 2009/08/07
        String sql = "Select net, sta, seedchan, channel, channelsrc, location, auth, subsource, trigflag," + // 9 cols
            "truetime.getEpoch(savestart,'UTC'), truetime.getEpoch(saveend,'UTC'), truetime.getEpoch(datetime,'UTC'), rflag " + // 4 cols, 3 UTC time
            " from Trig_Channel where ntid = " +
            "(Select ntid from AssocNtE where evid = "+evid+ ") "+
            " order by datetime ";

        return getBySQL( conn, sql );
    }

    /**
     * Returns a collection of ChannelTimeWindows for this triggered event
     * based on the entries in the 'trig_channel' table produce by the Earthworm
     * trigger program.  */
    protected ChannelableList getBySQL(Connection connection, String sql)
    {

        ctwList = new ChannelableList();


        // Debug
        //System.out.println("getBYSQL(Connection, String) SQL\n: "+sql);
        ResultSet rs = null;
        Statement sm = null;

        try {

         if (connection == null) conn = DataSource.getConnection();

            if ( connection.isClosed() )        // check that valid connection exists
                {
                    System.err.println("* DataSource connection is closed");
                    return null;
                }

            // ** Removed: don't use Oracle locking
            // do "select for update" if writeBack is enabled
            //            ExecuteSQL.setSelectForUpdate(DataSource.isWriteBackEnabled());

            sm = connection.createStatement();

            rs = sm.executeQuery(sql);

            while ( rs.next() ) {
                ctwList.add( parseResultSet(rs));
            }
        }
        catch (SQLException ex) {
            System.out.println("getBYSQL(Connection, String) SQL=\n"+sql);
            System.err.println(ex);
            ex.printStackTrace();
        }
        finally {
            try {
              if (rs != null) rs.close();
              if (sm != null) sm.close();
            }
            catch (SQLException ex) { }
        }

        return ctwList;
    }

/**
 * Parse a resultset row that contains the results of a join + some
 * stored procedures
 */
protected TriggerChannelTimeWindow parseResultSet(ResultSet rs)
{
     TriggerChannelTimeWindow ctw;
        // parse row contents into new channel object
        Channel ch = Channel.create();
 //  net, sta, seedchan, channel, channelsrc, location, "+
//   trigflag, datetime, savestart, saveend, rflag
        try {
            ch.setNet(rs.getString("NET"));
            ch.setSta(rs.getString("STA"));
            ch.setChannel(rs.getString("CHANNEL"));
            ch.setAuth(rs.getString("AUTH"));
            ch.setSubsource(rs.getString("SUBSOURCE"));
            ch.setChannelsrc(rs.getString("CHANNELSRC"));
            ch.setSeedchan(rs.getString("SEEDCHAN"));
            ch.setLocation(rs.getString("LOCATION"));

            double start = rs.getDouble(10); // savestart is col 11 in TRIG_CHANNEL 
            double end   = rs.getDouble(11); // and saveend is col 12 in TRIG_CHANNEL 
        
            ctw = new TriggerChannelTimeWindow(ch, start-preTrigSecs, end+postTrigSecs);
            // NOTE: 2010/02/17 moved to line below to after setTriggerTime:
            //if (ctw.triggerTime != 0.0) timeSpan.include(ctw.getTimeSpan()); // tracks max span -aww 2009/08/07
                 
            //ctw.setTriggerTime(rs.getDouble("datetime"));  // only for "t" rows removed 2008/02/07 - aww  
            ctw.setTriggerTime(rs.getDouble(12));  // datetime TRIG_CHANNEL table col 10 - for leap secs UTC 2008/02/07 - aww  
            //if (ctw.triggerTime != 0.0)  // removed condition so logic same as getTriggeredChannelTimeWidows method
                timeSpan.include(ctw.getTimeSpan()); // tracks max span -aww 2009/08/07

            ctw.setTriggerType(rs.getString("trigflag"));  // "t" or "m"

        }
        catch (SQLException ex) {
            System.err.println(ex);
            ex.printStackTrace();
         return new TriggerChannelTimeWindow(ch);
        }

        return ctw;
    }

    /*
    public static void main(String args[]) {
        System.out.println("Making connection...");
        DataSource init = TestDataSource.create();  // make connection

        long evid = 9585741;

        System.out.println(" Getting trigger evid = "+evid);

        Solution sol = Solution.create().getById(evid);

        TriggerTN trig = new TriggerTN(sol);

        ArrayList list = (ArrayList) trig.getChannelTimeWindows();

        for (int i = 0; i<list.size(); i++) {
            System.err.println(list.get(i).toString());
        }

    }
    */
} // Trigger
