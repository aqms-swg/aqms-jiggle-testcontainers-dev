package org.trinet.web;

import java.text.*;
import java.util.*;
import java.net.URL;
import java.sql.*;
import java.io.*;

import org.trinet.jdbc.*;
import org.trinet.util.*;
import org.trinet.jasi.*;
import org.trinet.util.Format;                // CoreJava printf-like Format class

/**
 * Make the files necessary to support the TriNet event and waveform review web pages.
 */


public class WebCat {
    // should be generallized and read from a property file of arg...
    static double hoursback = 24.0;
    static String checkFilePath       = "./eventfiles/";
    static String destinationFilePath = "./eventfiles/temp/";

    static double highlightMag = 2.95;
    static String highlightColor = "red";


    /** Make an empty WebCat */
    public  WebCat() { }

    /** Show usage summary */
    public static void showUsage() {
        System.out.println("Usage: java WebCat [hours-back] [check-path]  [destination]");
        System.out.println("Defaults:          ["+hoursback +"] ["+ checkFilePath+ "] ["+destinationFilePath+"]");
    }

/**
 * Main for testing: % WebCat [hours-back]  (default = 1)
 */
    public static void main(String args[]) {

        final int secondsPerHour = 3600;

        // ?
        /* Java WIERDNESS note:
            <boron>% java WebCat ?
            args[0] = /j/
            :. must use equalsIgnoreCase("?") else it looks like "j"!!!
        */
        if (args.length == 0 || args[0].equalsIgnoreCase("?")) {        // no args on command-line value
            showUsage();
            System.exit(0);
        }

        // days back
        if (args.length > 0) {        // translate epoch second on command-line value
            try {
                Double val = Double.valueOf(args[0]);            // convert arg String to 'double'
                hoursback = (double) val.doubleValue();
            } catch (NumberFormatException ex) {
                showUsage();
                System.exit(0);
            }
        }

        // check path
        if (args.length > 1) {
            checkFilePath = args[1];
        }

        // destination
        if (args.length > 2) {
            destinationFilePath = args[2];
        }


        System.out.println("Making connection...");
        TestDataSource.create();  // make connection

        double now = new DateTime().getTrueSeconds(); // UTC -aww 2008/02/12
        double then = now - (secondsPerHour * hoursback);        // convert to seconds

        System.out.println("Fetching last "+hoursback+" hours...");

        SolutionList sl = new SolutionList();
        sl.fetchValidByTime(then, now);

        // reverse chronological order
        sl = (SolutionList) sl.getTimeSortedList(SolutionList.DESCENDING);

        Solution sol[] = sl.getArray();
        System.out.println("Events found: "+sol.length);

        // dump the index html file
        try {

            FileOutputStream out = new FileOutputStream("catBody.html");

            PrintStream prt = new PrintStream(out);

            prt.println(writeCatBody(sol));

            prt.close();


        } catch (FileNotFoundException ex) {
          ex.printStackTrace();
        } catch (Exception ex) {
          ex.printStackTrace();
        }

        // check for missing individual event files, make them if needed
        for (int i = 0; i < sol.length; i++)
            {
                if (WebCat.htmlIsOk(sol[i])) {
                    System.out.println("-File exists for: "+sol[i].toSummaryString());
                } else {
                    System.out.println("+Making file for: "+sol[i].toSummaryString());
                    WebCat.makeHtml(sol[i]);
                }
            }

        // done
        System.exit(0);
    }

/** Whatever "ok" means. Right now just means the .html file EXISTS for this id
#.  This is used to determine if we should make one or not. May want to check
size, time or something else. */
public static boolean htmlIsOk(Solution sol) {

        File file1 = new File(checkFilePath+sol.id.longValue()+".html");
        File file2 = new File(checkFilePath+sol.id.longValue()+".gif");
        return file1.exists() && file2.exists();

}

/**
 * Make a .gif and a .html file for this solution. Assumes both will be in the directory
 * 'checkFilePath'.
 */
public static void makeHtml(Solution sol) {

    long id = sol.id.longValue();

    // assumes .html is in "./eventfiles" or some such subdir to main page
    String htmlFile = destinationFilePath + id + ".html";

    // make a .html file that points to the gif
    try {
        FileOutputStream out = new FileOutputStream(htmlFile);
        PrintStream prt = new PrintStream(out);

        // Could insert other .html content here

        // assumes .gif is in same dir as .html file
        prt.println("<img src=\"" +id+ ".gif\">");

        prt.close();

    } catch (FileNotFoundException ex) {
        ex.printStackTrace();
    } catch (Exception ex) {
        ex.printStackTrace();
    }

    int ntraces = 15;
    int maxSecs = 120;
    String gifFile = destinationFilePath + id + ".gif";
    SnapGif sn = new SnapGif();
    sn.makeGif(id, ntraces, maxSecs, gifFile);

}


public static String writeCatBody(Solution[] sol) {

    String str = ("<PRE><FONT SIZE=+1>");
    str += catHeader()+"\n";

    String clr0, clr1;

    for (int i = 0; i < sol.length; i++)
        {
            if (sol[i].magnitude.value.doubleValue() > highlightMag) {
                clr0 = "<FONT COLOR="+highlightColor+">";
                clr1 = "</FONT>";
            } else {
                clr0 = "";
                clr1 = "";
            }

            str += clr0+htmlString(sol[i])+clr1+"\n";
        }
        str += ("</FONT></PRE>");

        return str;
}

    public static String catHeader() {
        return "    ID#    MAG      DATE    TIME(UTC)   LAT       LON     Z    #   RMS GAP TP C SRC\n"+
           "                 year/mo/dy hr:mn:sc    deg       deg    km  PHS";
    }

    public static String htmlString(Solution sol) {
        //    ID#    mag  year/mo/dy hr:mn:sc latitude longitude  dep
        // 9514285  2.2 Ml 1999/11/22 01:11:22  36.0736 -117.8523  1.8
        // 9520825  0.8 Ml 1999/12/01 21:12:12  33.9418 -116.4998  7.0   6 5.81
        //    ID#    MAG      DATE    TIME(UTC)   LAT       LON     Z    #  RMS
        //                 year/mo/dy hr:mn:sc    deg       deg    km  PHS

        // 9520825  0.8 Ml 1999/12/01 21:12:12  33.9418 -116.4998  7.0   6 5.81 360 le H SRC
        //    ID#    MAG      DATE    TIME(UTC)   LAT       LON     Z    #  RMS GAP TP C
        //                 year/mo/dy hr:mn:sc    deg       deg    km  PHS

        String cgi = "cgi-bin/makeView.cgi?EVID=";        // the cgi script name
        long evid = sol.id.longValue();                // the event #
        String target = "zoom";                // target frame
        String tag0 = "<a href=\""+cgi+evid+"\" target=\""+target+"\">";
        String tag1 = "</a>";

         Format df0 = new Format("%8d");            // CORE Java Format class
         Format df1 = new Format("%5.4f");
         Format df2 = new Format("%7.4f");
         Format df3 = new Format("%4.1f");
         Format df4 = new Format("%5.2f");
         Format df5 = new Format("%3d");

         String dtStr = new DateTime(sol.datetime.doubleValue(), true).toDateString("yyyy/MM/dd HH:mm:ss"); // UTC -aww 2008/02/12

         String str = tag0 + df0.form(evid) + tag1+ " " +
         df3.form(sol.magnitude.value.doubleValue()) + " " +
         sol.magnitude.getTypeString() +" "+
         dtStr + "  " +
         df1.form(sol.lat.doubleValue()) + " " +
         df2.form(sol.lon.doubleValue()) + " " +
         df3.form(sol.depth.doubleValue()) + " "+
         df5.form(sol.usedReadings.intValue()) +" "+
         df4.form(sol.rms.doubleValue())+" "+
         df5.form(sol.gap.intValue())+" "+
         sol.eventType.toString() +" "+
         sol.processingState.toString()+ " "+
         sol.source.toString().concat("     ").substring(0, 4);   // truncate to 3 chars

         return str;
    }
} // end of class
    

