package org.trinet.web;

import java.text.*;
import java.util.*;
import java.net.URL;
import java.sql.*;
import java.io.*;

import org.trinet.jdbc.*;
import org.trinet.util.*;
import org.trinet.jasi.*;
import org.trinet.util.Format;      // CoreJava printf-like Format class

import org.trinet.waveserver.rt.WaveClient;

/**
 * Make the files necessary to support the TriNet event and waveform review web pages.
 */

 //
 //          THIS CLASS IS OBSOLETE
 //

public class WebSnaps

{
    // should be generallized and read from a property file or arg...
    static double hoursback = 24.0;
    static String checkFilePath       = "./eventfiles/done";
    static String destinationFilePath = "./eventfiles/new/";

    //    static boolean fromWaveServer = false;
    static String waveServerFile = "";

    static String host;
    static String dbasename;
    static String defaulthost = "serverma";
    static String defaultdbase = defaulthost+"db";

    // snap shot dimensions
    static int ntraces = 15;
    static int maxSecs = 120;

/**  */
public  WebSnaps ()
{

}
    /** Show usage summary */
    public static void showUsage () {
      System.out.println ("Usage: java WebSnaps [hours-back] [check-path] [destination] [waveServer-file] [dbase-host] [dbase-name]");
      System.out.println ("Defaults:            ["+hoursback +"] ["
                  + checkFilePath+ "] ["+destinationFilePath+"] ["+defaulthost+"] [<host>db]");
    }

/**
 * Main: % WebSnaps [hours-back]  (default = 1)
 */
    public static void main (String args[]) {
    // ?
    /* Java WIERDNESS note:
        <boron>% java WebCat ?
        args[0] = /j/
        :. must use equalsIgnoreCase("?") else it looks like "j"!!!
    */
    if (args.length == 0 || args[0].equalsIgnoreCase("?")) {    // no args on command-line value
        showUsage();
        System.exit (0);
    }

    // days back
    if (args.length > 0) {  // translate epoch second on command-line value
        try {
        Double val = Double.valueOf(args[0]);       // convert arg String to 'double'
        hoursback = (double) val.doubleValue();
        } catch (NumberFormatException ex) {
        showUsage();
        System.exit (0);
        }
    }

    // check path
    if (args.length > 1) {
        checkFilePath = args[1];
    }

    // destination
    if (args.length > 2) {
        destinationFilePath = args[2];
    }

    // get data from waveServer?
    if (args.length > 3) {

       waveServerFile = args[3];
    }


    host = defaulthost; // default
    if (args.length > 4) {
        host = args[4];
    }

    dbasename = host+"db";  // default
    if (args.length > 5) {
        dbasename = args[5];
    }


    if (waveServerFile != "") {

       System.out.println ("Get time series from waveServer: file = "+
                   waveServerFile);

       WaveClient waveClient = null;

       try {
           // Make a WaveClient

           System.out.println ("Creating WaveClient using: "+waveServerFile);

           waveClient = new WaveClient(waveServerFile); // property file name

           int nservers = waveClient.numberOfServers();
           if (nservers <= 0) {
           System.err.println("getDataFromWaveServer Error:"+
                      " no data servers specified in input file: " +
                      waveServerFile);
           System.exit(-1);
           }

        }
       catch (Exception ex) {
           System.err.println(ex.toString());
           ex.printStackTrace();
       }

       if (waveClient != null) AbstractWaveform.setWaveDataSource(waveClient);

    }

    System.out.println ("checkFilePath       = "+checkFilePath);
    System.out.println ("destinationFilePath = "+destinationFilePath);

    System.out.println ("Making connection...");
    TestDataSource.create(host, dbasename);  // make connection

    EventSelectionProperties props = new EventSelectionProperties();
    //props.setFiletype("jiggle"); // test file i/o in the user's home "jiggle" subdir

    // set time properties
    props.setTimeWindow(hoursback, TimeUnits.getUnitsName(TimeUnits.HOUR));

    // don't do triggers (they are done by the TRP scripts)
    //props.setProperty(EventSelectionProperties.prefix+"trigger", "FALSE");
    props.setEventTypesExcluded("trigger");

    Solution sol[] = (Solution[])Solution.create().getByProperties(props).toArray(new Solution[0]);

    // must put into a solList to sort
    SolutionList sl = new SolutionList();
    sl.addSolutions(sol);

    // reverse chronological order
    sl = (SolutionList)sl.getTimeSortedList(SolutionList.DESCENDING);

    // and back out again...
    sol = sl.getArray();
    System.out.println ("Events found: "+sol.length);

    // check for missing individual event files, make them if needed
    // TODO: could add time check to see if file needs updating
    for (int i = 0; i < sol.length; i++)
        {
        // .HTML
        if (htmlIsOk(sol[i])) {
            System.out.println ("-HTML file exists for: "+sol[i].toSummaryString());
        } else {
            System.out.println ("+Making HTML file for: "+sol[i].toSummaryString());
            makeHtml(sol[i]);
        }
        // .GIF
        if (gifIsOk(sol[i])) {
            System.out.println ("-GIF  file exists for: "+sol[i].toSummaryString());
        } else {
            System.out.println ("+Making GIF  file for: "+sol[i].toSummaryString());
            makeGif(sol[i]);
        }
        }

    // done
    System.exit(0);
    }

/** Whatever "ok" means. Right now just means the file EXISTS for this id
#.  This is used to determine if we should make one or not. May want to check
size, time or something else. */
public static boolean htmlIsOk (Solution sol) {

    File file1 = new File(checkFilePath+sol.id.longValue()+".html");
    //  File file2 = new File(checkFilePath+sol.id.longValue()+".gif");
    return file1.exists();

}

/** Whatever "ok" means. Right now just means the file EXISTS for this id
#.  This is used to determine if we should make one or not. May want to check
size, time or something else. */
public static boolean gifIsOk (Solution sol) {

    //  File file1 = new File(checkFilePath+sol.id.longValue()+".html");
    File file2 = new File(checkFilePath+sol.id.longValue()+".gif");
    return file2.exists();

}
/**
 * Make a .gif and a .html file for this solution. Assumes both will be in the directory
 * 'checkFilePath'.
 */
public static void makeHtml (Solution sol) {

    long id = sol.id.longValue();

    // assumes .html is in "./eventfiles" or some such subdir to main page
    String htmlFile = destinationFilePath + id + ".html";

    // make a .html file that points to the gif
    try {
    FileOutputStream out = new FileOutputStream(htmlFile);
    PrintStream prt = new PrintStream(out);

    // Could insert other .html content here

    // assumes .gif is in same dir as .html file
    prt.println ("<img src=\"" +id+ ".gif\" ALT=\"No Waveforms Available\">");
    prt.println ("<hr>");
    prt.println ("<A HREF=\""+id+".email\">E-mail text</A>");

    prt.close();

    } catch (FileNotFoundException ex) {
    ex.printStackTrace();
    } catch (Exception ex) {
    ex.printStackTrace();
    }
}

/**
 * Make a .gif file for this solution. Puts it in 'destinationFilePath'
 */
public static void makeGif (Solution sol) {

    long id =  sol.id.longValue();
    String gifFile = destinationFilePath + id + ".gif";


    SnapGif.makeGif (id, ntraces, maxSecs, gifFile);

}

} // end of class


