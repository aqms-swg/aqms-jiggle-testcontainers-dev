package org.usgs.util;

import java.io.IOException;

public class  ByteStreamParser extends java.io.ByteArrayInputStream
{

  public static final int BYTES_PER_LONG = 8;
  public static final int BYTES_PER_INT  = 4;
  public static final int BYTES_PER_SHORT= 2;
  public static final int BITS_PER_BYTE =  8;

  public ByteStreamParser(byte[] baBuffer)
  {
    super(baBuffer);
  }

  public ByteStreamParser(byte[] baBuffer, int iOffset, int iLen)
  {
    super(baBuffer, iOffset, iLen);
  }

  public double ReadDouble() throws java.io.IOException
  {
    // copied from
    return(Double.longBitsToDouble(ReadLong()));
  }

  public String ReadString(int iMaxLen) throws java.io.IOException
  {
    byte [] caString = new byte[iMaxLen + 1];
    int iTemp;
    int i;

    for(i = 0; i < iMaxLen; i++)
    {
      if((iTemp = this.read()) == -1)
      {
        IOException ioex = new java.io.EOFException("Error reading byte[" + i + "] in ByteStreamParser::ReadLong()");
        throw ioex;
      }
      else
      {
        caString[i] = (byte)iTemp;
      }
    }
    // null terminate
    caString[i] = 0x00;

    return(new String(caString,0,i));
  }   // end ReadString


  public long ReadLong()  throws java.io.IOException
  {
    long iOut = 0;
    int  iTemp;

    for(int i = 0; i < BYTES_PER_LONG; i++)
    {
      if((iTemp = this.read()) == -1)
      {
        IOException ioex = new java.io.EOFException("Error reading byte[" + i + "] in ByteStreamParser::ReadLong()");
        throw ioex;
      }
      else
      {
        iOut |= ((long)iTemp) << ((BYTES_PER_LONG - 1 - i) * BITS_PER_BYTE);
      }
    }
    return(iOut);
  }

  public double ReadFloat() throws java.io.IOException
  {
    // copied from
    return(Double.longBitsToDouble(ReadLong()));
  }

  public int ReadInt()  throws java.io.IOException
  {
    int iOut = 0;
    int  iTemp;

    for(int i = 0; i < BYTES_PER_INT; i++)
    {
      if((iTemp = this.read()) == -1)
      {
        IOException ioex = new java.io.EOFException("Error reading byte[" + i + "] in ByteStreamParser::ReadInt()");
        throw ioex;
      }
      else
      {
        iOut |= (int)(iTemp) << ((BYTES_PER_INT - 1 - i) * BITS_PER_BYTE);
      }
    }
    return(iOut);
  }


  public short ReadShort()  throws java.io.IOException
  {
    short iOut = 0;
    int  iTemp;

    for(int i = 0; i < BYTES_PER_SHORT; i++)
    {
      if((iTemp = this.read()) == -1)
      {
        IOException ioex = new java.io.EOFException("Error reading byte[" + i + "] in ByteStreamParser::ReadShort()");
        throw ioex;
      }
      else
      {
        iOut |= ((short)iTemp) << ((BYTES_PER_SHORT - 1 - i)*BITS_PER_BYTE);
      }
    }
    return(iOut);
  }


  public static double ReadDouble(byte[] baBytes)
  {
    // copied from
    return(Double.longBitsToDouble(ReadLong(baBytes)));
  }

  public static long ReadLong(byte[] baBytes)
  {
    long iOut = 0;

    for(int i = 0; i < BYTES_PER_LONG; i++)
        iOut |= ((long)baBytes[i] & 0xff) << ((BYTES_PER_LONG - 1 - i)*BITS_PER_BYTE);

    return(iOut);
  }

  public static float ReadFloat(byte[] baBytes)
  {
    // copied from
    return(Float.intBitsToFloat(ReadInt(baBytes)));
  }

  public static int ReadInt(byte[] baBytes)
  {
    int iOut = 0;

    for(int i = 0; i < BYTES_PER_INT; i++)
        iOut |= ((int)baBytes[i] & 0xff) << ((BYTES_PER_INT - 1 - i)*BITS_PER_BYTE);

    return(iOut);
  }

  public static short ReadShort(byte[] baBytes)
  {
    short iOut = 0;

    for(int i = 0; i < BYTES_PER_SHORT; i++)
        iOut |= ((short)baBytes[i] & 0xff) << ((BYTES_PER_SHORT - 1 - i)*BITS_PER_BYTE);

    return(iOut);
  }


public static byte[] WriteLong(long iNum)
{
  byte [] BytesOut = new byte[8];
  BytesOut[0] = (byte)((iNum >> (7 * 8)) & 0xff);
  BytesOut[1] = (byte)((iNum >> (6 * 8)) & 0xff);
  BytesOut[2] = (byte)((iNum >> (5 * 8)) & 0xff);
  BytesOut[3] = (byte)((iNum >> (4 * 8)) & 0xff);
  BytesOut[4] = (byte)((iNum >> 3 * 8) & 0xff);
  BytesOut[5] = (byte)((iNum >> 2 * 8) & 0xff);
  BytesOut[6] = (byte)((iNum >> 1 * 8) & 0xff);
  BytesOut[7] =  (byte)(iNum           & 0xff);

  return(BytesOut);
}  // end WriteLong

public static byte[] WriteInt(int iNum)
{
  byte [] BytesOut = new byte[4];
  BytesOut[0] = (byte)((iNum >> 3 * 8) & 0xff);
  BytesOut[1] = (byte)((iNum >> 2 * 8) & 0xff);
  BytesOut[2] = (byte)((iNum >> 1 * 8) & 0xff);
  BytesOut[3] = (byte)( iNum           & 0xff);

  return(BytesOut);
}  // end WriteInt


public static byte[] WriteShort(short iNum)
{
  byte [] BytesOut = new byte[2];
  BytesOut[0] = (byte)((iNum >> 1 * 8) & 0xff);
  BytesOut[1] = (byte)( iNum           & 0xff);

  return(BytesOut);
}  // end WriteShort

public static byte[] WriteDouble(double dNum)
{
   return(WriteLong(Double.doubleToLongBits(dNum)));
}  // end WriteShort

public static byte[] WriteFloat(float dNum)
{
   return(WriteLong(Float.floatToIntBits(dNum)));
}  // end WriteShort

public static double SwapDouble(double dNum)
{
  return(Double.longBitsToDouble(SwapLong(Double.doubleToLongBits(dNum))));
}

public static float SwapFloat(float dNum)
{
  return(Float.intBitsToFloat(SwapInt(Float.floatToIntBits(dNum))));
}

public static long SwapLong(long iNum)
{
  return(ReadLong(SwapBytes(WriteLong(iNum))));
}

public static int SwapInt(int iNum)
{
  return(ReadInt(SwapBytes(WriteInt(iNum))));
}

public static short SwapShort(short iNum)
{
  return(ReadShort(SwapBytes(WriteShort(iNum))));
}

public static byte[] SwapBytes(byte[] baBytes)
{
  int iNumBytes = baBytes.length;  // this one is for more readable code
  int iNumSwaps = iNumBytes / 2;  /* we only have to do half as many
                                     swaps as there are bytes */
  int iIndexLastByte = iNumBytes - 1;
  byte byteTemp;

  for(int i=0; i < iNumSwaps; i++)
  {
    byteTemp                    = baBytes[i];
    baBytes[i]                  = baBytes[iIndexLastByte - i];
    baBytes[iIndexLastByte - i] = byteTemp;
  }

  return(baBytes);
}  // end SwapBytes()




}  // end class ByteStreamParser
