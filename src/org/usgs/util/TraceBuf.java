package org.usgs.util;


import org.usgs.util.ByteStreamParser;
import java.io.IOException;
import org.trinet.jasi.WFSegment;
import org.trinet.jasi.Channel;
import org.trinet.jdbc.datatypes.DataDouble;
/**
 * <p>Title: TraceBuf</p>
 * <p>Description: Class for encapsulating an EW trace_buf packet</p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: USGS</p>
 * @author DK
 * @version 0.99
 */

public class TraceBuf {

    public static final int  TRACE_STA_LEN    =  7;
    public static final int  TRACE_CHAN_LEN   =  9;
    public static final int  TRACE_NET_LEN    =  9;
    public static final int  TRACE_LOC_LEN    =  3;
    public static final int  TRACE_HEADER_LEN = 64;
    public static final int  TRACE_DT_LEN     =  3;
    public static final int  TRACE_QUAL_LEN   =  2;

    public static final int  TRACEBUF_TYPE_DOUBLE = 1;
    public static final int  TRACEBUF_TYPE_FLOAT  = 2;
    public static final int  TRACEBUF_TYPE_LONG   = 3;
    public static final int  TRACEBUF_TYPE_INT    = 4;
    public static final int  TRACEBUF_TYPE_SHORT  = 5;


    protected boolean bHeaderIsValid;
    protected boolean bSwapBinaryData;

    public int    iNumSamples;
    public double tStart;
    public double tEnd;
    public double dSampleRate;
    public String sSta;
    public String sNet;
    public String sChan;
    public String sDataType;
    public String sQuality;
    protected short[] shortArray;
    protected int[]   intArray;
    protected long[]  longArray;
    protected Channel chan;

    private int iDataType;
    private int iDataByteSize;
    protected boolean bFloatingSampleType;

    public TraceBuf()
    {
      bHeaderIsValid = false;
    }

    public TraceBuf(ByteStreamParser bspStream)
    {
      this();
      ParseHeader(bspStream);
    }

    /**
     * Expects a 64 byte header.
     * On failure, returns false.
     * On success:
     *     1) Fills in header
     *     2) Determines byte order and swaps if neccessary
     *     3) Marks the header as valid
     *     4) Allocates array for data
     *     5) Returns true
     */

  public int ParseHeader(ByteStreamParser isBytes)
  {

    try
    {
      isBytes.skip(4);                                    // pinno
      iNumSamples  = isBytes.ReadInt();                   // nsamp
      tStart       = isBytes.ReadDouble();                // starttime
      tEnd         = isBytes.ReadDouble();                // endtime
      dSampleRate  = isBytes.ReadDouble();                // samprate
      sSta         = isBytes.ReadString(TRACE_STA_LEN);   // sta
      sNet         = isBytes.ReadString(TRACE_NET_LEN);   // net
      sChan        = isBytes.ReadString(TRACE_CHAN_LEN);  // chan
      sDataType    = isBytes.ReadString(TRACE_DT_LEN);    // datatype
      sQuality     = isBytes.ReadString(TRACE_QUAL_LEN);  // qual
      isBytes.skip(2);                                    // pad
    }
    catch (IOException ex)
    {
      return(1);
    }

    /* figure out if we swap */
    // check the datatype
    if(sDataType.charAt(0) == 'i' || sDataType.charAt(0) == 'f')
    {
      // it's an intel format, we need to byteswap the binary stuff.
      bSwapBinaryData = true;
      tStart = ByteStreamParser.SwapDouble(tStart);
      tEnd   = ByteStreamParser.SwapDouble(tEnd);
      iNumSamples = ByteStreamParser.SwapInt(iNumSamples);
      dSampleRate = ByteStreamParser.SwapDouble(dSampleRate);
    }

    switch(sDataType.charAt(0))
    {
      case 'i':
      case 's':
      {
        bFloatingSampleType = false;
        break;
      }
      case 'f':
      case 't':
      {
        bFloatingSampleType = true;
        break;
      }
      default:
      {
        // Unsupported datatype
        return(-1);
      }
    } // end switch(sDataType[0])

    switch(sDataType.charAt(1))
    {
      case '2':
      {
        if(bFloatingSampleType)
        {
          // unSupported datatype
          return(-1);
        }
        else
        {
          iDataType     = TRACEBUF_TYPE_SHORT;
          iDataByteSize = ByteStreamParser.BYTES_PER_SHORT;
        }
        break;
      }
      case '4':
      {
        if(bFloatingSampleType)
        {
          iDataType = TRACEBUF_TYPE_FLOAT;
          iDataByteSize = ByteStreamParser.BYTES_PER_INT;
        }
        else
        {
          iDataType = TRACEBUF_TYPE_INT;
          iDataByteSize = ByteStreamParser.BYTES_PER_INT;
        }
        break;
      }
      case '8':
      {
        if(bFloatingSampleType)
        {
          iDataType = TRACEBUF_TYPE_DOUBLE;
          iDataByteSize = ByteStreamParser.BYTES_PER_LONG;
        }
        else
        {
          iDataType = TRACEBUF_TYPE_LONG;
          iDataByteSize = ByteStreamParser.BYTES_PER_LONG;
        }
        break;
      }
      default:
      {
          // unSupported datatype
          return(-1);
      }
    } // end switch(sDataType[1])

    /* perform a CheckSumish kind of calculation on the header
        ensure that the tracebuf ends within 5 samples of the given endtime.
        DK 2002/03/18
       *******************************************************************/
    double tShouldEnd = tStart + (iNumSamples - 1)/ dSampleRate;
    double dFudgeFactor = 5.0 / dSampleRate;
    if(tShouldEnd != tEnd)
    {
      if(tEnd < (tShouldEnd - dFudgeFactor)  ||  tEnd > (tShouldEnd + dFudgeFactor))
      {
        return(-1);
      }
    }


    bHeaderIsValid = true;
    if(iDataByteSize == 2)
      shortArray = new short[iNumSamples];
    else if(iDataByteSize == 4)
      intArray = new int[iNumSamples];
    else if(iDataByteSize == 8)
      longArray = new long[iNumSamples];

    return(0);
  } // end ParseHeader()


  public int ParseSampleData(ByteStreamParser isBytes)
  {

    int i;

    if(!this.bHeaderIsValid)
      return(-1);

    try
    {
      if(this.iDataByteSize == 8)
      {
        for(i=0; i < this.iNumSamples;  i++)
        {
          longArray[i] = isBytes.ReadLong();
        }
        if(bSwapBinaryData)
        {
          for(i=0; i < this.iNumSamples;  i++)
          {
            longArray[i] = ByteStreamParser.SwapLong(longArray[i]);
          }
        }
      }
      else if(this.iDataByteSize == 4)
      {
        for(i=0; i < this.iNumSamples;  i++)
        {
          intArray[i] = isBytes.ReadInt();
        }
        if(bSwapBinaryData)
        {
          for(i=0; i < this.iNumSamples;  i++)
          {
            intArray[i] = ByteStreamParser.SwapInt(intArray[i]);
          }
        }
      }
      else if(this.iDataByteSize == 2)
      {
        for(i=0; i < this.iNumSamples;  i++)
        {
          shortArray[i] = isBytes.ReadShort();
        }
        if(bSwapBinaryData)
        {
          for(i=0; i < this.iNumSamples;  i++)
          {
            shortArray[i] = ByteStreamParser.SwapShort(shortArray[i]);
          }
        }
      }
      else
      {
        return(-1);
      }
    }
    catch (IOException ex)
    {
      return(1);
    }

    return(0);
  }  // end TraceBuf::ParseSampleData()

  public WFSegment toWFSegment()
  {
    int i;

    if(!this.bHeaderIsValid)
      return(null);

    float[] dArray = new float[this.iNumSamples];

    if(this.iDataType == this.TRACEBUF_TYPE_DOUBLE ||
       this.iDataType == this.TRACEBUF_TYPE_LONG)
    {
      // use longArray
      if(longArray.length != this.iNumSamples)
         return(null);  // bad data

      if(this.iDataType == this.TRACEBUF_TYPE_LONG)
      {
        for(i=0; i < longArray.length; i++)
          dArray[i] = (float)(longArray[i]);
      }
      else  // TYPE_DOUBLE
      {
        for(i=0; i < longArray.length; i++)
          dArray[i] = (float)(Double.longBitsToDouble(longArray[i]));
      }
    }  // end if double || long
    else if(this.iDataType == this.TRACEBUF_TYPE_FLOAT ||
            this.iDataType == this.TRACEBUF_TYPE_INT)
    {
      // use intArray
      if(intArray.length != this.iNumSamples)
         return(null);  // bad data

      if(this.iDataType == this.TRACEBUF_TYPE_INT)
      {
        for(i=0; i < intArray.length; i++)
          dArray[i] = (float)(intArray[i]);
      }
      else  // TYPE_FLOAT
      {
        for(i=0; i < intArray.length; i++)
          dArray[i] = (float)(Float.intBitsToFloat(intArray[i]));
      }
    }  // end if float || int
    else if(this.iDataType == this.TRACEBUF_TYPE_SHORT)
    {
      // use shortArray
     if(shortArray.length != this.iNumSamples)
         return(null);  // bad data

     for(i=0; i < shortArray.length; i++)
       dArray[i] = (float)(shortArray[i]);
    }  // end if short
    else
    {
      // unsupported or uninitialized size
      return(null);
    }

    if(chan == null)
    {
      Channel.create().setChannelName(sNet, sSta, sChan,
                      Channel.UNKNOWN_LOCATION);
    }

    WFSegment wfseg = new WFSegment(chan,dArray);

    wfseg.setSampleInterval(1.0 / dSampleRate);
    // wfseg.bytesPerSample = this.iDataByteSize;
    wfseg.setIsFiltered(false);
    wfseg.setStart(new DataDouble(tStart));
    wfseg.setEnd(new DataDouble(tEnd));

    return(wfseg);
  }  // end TraceBuf::toWFSegment()

  public void SetChannel(Channel IN_chan)
  {
    chan = IN_chan;
  }  // end TraceBuf::SetChannel

  /**
   * Returns the Channel object used by this tracebuf (not a copy)
   */
  public Channel GetChannel()
  {
    return(chan);  // note this is the object, not a copy
  }  // end TraceBuf::GetChannel

}  // end class TraceBuf
