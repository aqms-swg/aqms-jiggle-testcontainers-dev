#!/bin/bash

# build_jiggle.sh
# This script builds the jiggle.jar executable from compiled .class files and .jar external libraries.
# This script lives in the bin/ directory
# Assumes a "manifest.txt" file lives in the bin/ directory

if [ "$#" -ne 1 ]
then
   echo "Usage: ./build_jiggle.sh <java_version>"
   echo "Possible values of <java_version>: 8, 11"
   exit 1
fi

# Java version from command line
JAVA_VER=$1
echo "JAVA_VER: 1."${JAVA_VER}

case "${JAVA_VER}" in
   8 )
      echo "8"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
#      export JAVA_HOME=`/usr/libexec/java_home -v 1.8.0_202`
      ;;
   11 )
      echo "Using JDK 11"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.11`
      ;;
   * )
      echo "8"
      export JAVA_HOME=`/usr/libexec/java_home -v 1.8`
#      export JAVA_HOME=`/usr/libexec/java_home -v 1.8.0_202`
      ;;
esac

# Input directory for .java source files
SRC_DIR="./"

# Input directory for .jar external libraries
LIB_DIR="../lib/"

# Output directory for .class files
CLASS_DIR="./out_classes_java"${JAVA_VER}
echo "CLASS_DIR: "${CLASS_DIR}

# Oracle database driver jar name
# JDBC_FILE="ojdbc"${JAVA_VER}".jar"
# echo "JDBC_FILE: "${JDBC_FILE}

javac -version
# CLASS_PATH="${SRC_DIR}*:${LIB_DIR}postgresql-42.2.5.jar:${LIB_DIR}${JDBC_FILE}:${LIB_DIR}acme.jar:${LIB_DIR}openmap.jar:${LIB_DIR}isti.openmap.all.jar:${LIB_DIR}seed-pdcc.jar:${LIB_DIR}looks-2.0.4.jar:${LIB_DIR}forms-1.0.7.jar:${LIB_DIR}colt.jar:${LIB_DIR}swarm.jar:${LIB_DIR}usgs.jar:${LIB_DIR}jregex1.2_01.jar:${LIB_DIR}pljava-api-1.5.8.jar:${LIB_DIR}junit-jupiter-api-5.8.1.jar:${LIB_DIR}junit-jupiter-engine-5.8.1.jar:${LIB_DIR}junit-platform-commons-1.8.1.jar:${LIB_DIR}hamcrest-core-1.3.jar"
# ls "${CLASS_DIR}"
# ls "${CLASS_DIR}/test"
# java -ea -Dfile.encoding=UTF-8 -classpath ${CLASS_PATH} "${CLASS_DIR}/testy/HelloWorldTest"
java -jar "${LIB_DIR}junit-platform-console-standalone-1.3.1.jar" --class-path "${CLASS_DIR}" --include-classname=.* --scan-classpath --include-engine=junit-jupiter --exclude-engine=junit-vintage --disable-ansi-colors  --details-theme=ascii --fail-if-no-tests --reports-dir="${SRC_DIR}/test/reports"

# Other parameters we could add
# --details=verbose