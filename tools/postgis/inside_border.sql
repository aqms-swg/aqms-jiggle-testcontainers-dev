-- Returns 0 if outside, 1 if inside, 2 if on the defined region border.
-- Double array should be in (lat,long) order
-- Uses "linestring" when input contains 3 lat/longs (i.e. looks for border)
-- Uses "polygon" when 4 or more lat/longs. Polygon must be closed.
-- Using exceptions instead of return code "-1" on error as Jiggle checks for "<> 0" which results in "true". Exception is displayed by Jiggle.
-- Sample usage: select * from geo_region.inside_border(array[[37.3642, -121.90188], [ 37.2583, -121.7986], [37.3320, -122.0553], [37.3642, -121.90188]],37.3331, -121.8974)
CREATE OR REPLACE FUNCTION geo_region.inside_border(p_latlon double precision[], p_lat double precision, p_lon double precision)
    RETURNS integer
AS $BODY$
DECLARE
    -- Constants
    c_SRID integer = 4326;
    c_geometry_polygon varchar = 'POLYGON'; -- Use polygon when 4 or more points are used
    c_geometry_line varchar = 'LINESTRING'; -- Use linestring when 3 points are used
    c_is_outside integer = 0;
    c_is_inside integer = 1;
    c_is_border integer = 2;

    -- Local variables    
    l_location_point geometry; -- geometry location based on p_lat and p_lon
    
    -- Loop on p_latlon array
    l_arrLatLong double precision[]; 
    l_latlong_size INTEGER = array_length(p_latlon, 1);
    l_long double precision;
    l_lat double precision;
    
    l_is_linestring boolean = FALSE;
    l_geometry_text varchar = ''; -- text form of p_latlon
    l_region geometry; -- geometry formed from p_latlong
    
    l_result_contains boolean = FALSE;
    l_result_intersect boolean = FALSE;
    l_result integer = 0;
    
BEGIN
    IF (l_latlong_size < 3) THEN
		RAISE EXCEPTION 'Lat/lon list cannot have less than 3 items, but used with %', l_latlong_size;
	END IF;

	IF (p_lat IS NULL OR p_lon IS NULL) THEN
		RAISE EXCEPTION 'Latitude or Longitude cannot be null';
	END IF;
	
    -- SRID 4326 is WGS84
    l_location_point := ST_SetSRID(ST_Point(p_lon, p_lat), c_SRID);
    
    -- Create a list of points based on p_latlon
    FOREACH l_arrLatLong SLICE 1 IN ARRAY p_latlon 
    LOOP
        l_lat = l_arrLatLong[1];
        l_long = l_arrLatLong[2];
        
        IF (l_geometry_text != '') THEN
            l_geometry_text = l_geometry_text || ',';        
        END IF;
        
        l_geometry_text = l_geometry_text || l_long || ' ' || l_lat;
    END LOOP;
       
    IF (l_latlong_size > 3) THEN
        -- Use polygon
        l_geometry_text = c_geometry_polygon || '((' || l_geometry_text || '))';
    ELSE
        -- Use linestring when there are 3 points
        l_is_linestring = TRUE;
        l_geometry_text = c_geometry_line || '(' || l_geometry_text || ')';
    END IF;
    
    -- RAISE NOTICE 'inside_border geometry %, point lat %, point long %', l_geometry_text, p_lat, p_lon;

    -- Create geometry from list of lat/longs
    l_region = ST_GeomFromText(l_geometry_text, c_SRID);
    
    IF (ST_IsValid(l_region) = TRUE) THEN
        IF (l_is_linestring = TRUE) THEN
            -- Check if the point is on a linestring

            -- ST_Contains returns false on vertices for linestring. Treat all matches as on the border
            l_result_intersect := ST_Intersects(l_region, l_location_point);
            IF (l_result_intersect = TRUE) THEN
                l_result = c_is_border;
            ELSE l_result = c_is_outside;
            END IF;
        ELSE 
            -- Check to see if the point is in a polygon

            -- contains returns false when the point is on the border
            l_result_contains := ST_Contains(l_region, l_location_point);

            IF (l_result_contains = FALSE) THEN

                -- intersect returns true when the point is on the border
                l_result_intersect := ST_Intersects(l_region, l_location_point);

                IF (l_result_intersect = TRUE) THEN
                    l_result = c_is_border;
                ELSE l_result = c_is_outside;
                END IF;
            ELSE 
                l_result = c_is_inside;
            END IF;
        END IF;
    ELSE
        RAISE EXCEPTION 'Invalid geometry %', ST_IsValidReason(l_region); 
    END IF;

    RETURN l_result;
EXCEPTION 
    WHEN RAISE_EXCEPTION THEN
        -- Handle input errors raised by the function
        RAISE EXCEPTION 'Invalid input. Error message: %', SQLERRM;    
    WHEN OTHERS THEN
        -- Other exception that may be from Postgis
        RAISE EXCEPTION 'Error in geo_region.inside_border. Error message: %', SQLERRM;
END;
$BODY$ LANGUAGE plpgsql;
