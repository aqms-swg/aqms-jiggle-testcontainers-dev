#!/usr/bin/env python

import psycopg2
import psycopg2.extras
import os
import subprocess

"""
Script to import Oracle exported CSV files to Postgresql
- Add data files to <repo>/aqms-jiggle/util/postgres_import/datafiles
- This script pre-processes files to remove padded space after ',' delimiter and removes empty string ""
- Set "PGPASSWORD" environment variable if needed. 
- Before running, install psycopg2 - "pip install psycopg2"

The following should be handled by the Postgres 12 Docker container, but added here as notes:
- In CHANNELMAP_CODAPARMS, flip the column order such that SUMMARY_WT is before LDDATE to match the data file.
- D_Comment.description should be varchar(73) to match the data file.
- Needs a superuser 'aqms_superuser' with password 'password!'
- See docker-playground\postgres12-jiggle-support\README.md for more table changes.

"""

if __name__ == "__main__":

    dbUser = "aqms_superuser"
    dbPassword = "password!"
    dbName = "myarch"
    dbPort = "5432"

    os.environ['PGPASSWORD'] = dbPassword

    db = psycopg2.connect(database=dbName, user=dbUser, password=dbPassword, host='localhost', port=dbPort)
    db.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)

    cursor = db.cursor()

    filepath = os.path.join(os.getcwd(), "datafiles")

    entries = os.listdir(filepath)
    for filename in entries:
        fullpath = os.path.join(filepath, filename)
        
        ## pre-process file to remove space after comma delimiter
        with open(fullpath, 'r') as file:
            filedata = file.read()
                
        filedata = filedata.replace('", "', '","')
        filedata = filedata.replace(',""', ',')
        with open(fullpath, 'w') as file:
            if 'ORA-00936' in filedata:
                print("File with error - likely because the table was empty on export")
                pass
            else:
                file.write(filedata)

        with open(fullpath, 'r') as f:
            tablename = os.path.splitext(filename)[0] # get filename

            try:
                cursor.execute("alter table " + tablename + " disable trigger all;")
                cursor.execute("delete from " + tablename + ";")

                deli = ","
                fileType = "csv"
                sqlcommand = f'''\copy {tablename} FROM '{fullpath}' WITH DELIMITER '{deli}' NULL AS '' {fileType};'''

                subprocess.run(['psql', '-U', dbUser, '-d', dbName, '-p', dbPort, '-c', sqlcommand])

                cursor.execute("alter table " + tablename + " enable trigger all;")

                print("Processed: " + tablename)
            except psycopg2.errors.UndefinedTable:
                print("Table " + tablename + " does not exist")

